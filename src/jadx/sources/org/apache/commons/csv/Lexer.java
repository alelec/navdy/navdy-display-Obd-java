package org.apache.commons.csv;

final class Lexer implements java.io.Closeable {
    private static final char DISABLED = '\ufffe';
    private final char commentStart;
    private final char delimiter;
    private final char escape;
    private final boolean ignoreEmptyLines;
    private final boolean ignoreSurroundingSpaces;
    private final char quoteChar;
    private final org.apache.commons.csv.ExtendedBufferedReader reader;

    Lexer(org.apache.commons.csv.CSVFormat format, org.apache.commons.csv.ExtendedBufferedReader reader2) {
        this.reader = reader2;
        this.delimiter = format.getDelimiter();
        this.escape = mapNullToDisabled(format.getEscapeCharacter());
        this.quoteChar = mapNullToDisabled(format.getQuoteCharacter());
        this.commentStart = mapNullToDisabled(format.getCommentMarker());
        this.ignoreSurroundingSpaces = format.getIgnoreSurroundingSpaces();
        this.ignoreEmptyLines = format.getIgnoreEmptyLines();
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: CFG modification limit reached, blocks count: 149 */
    public org.apache.commons.csv.Token nextToken(org.apache.commons.csv.Token token) throws java.io.IOException {
        int lastChar = this.reader.getLastChar();
        int c = this.reader.read();
        boolean eol = readEndOfLine(c);
        if (this.ignoreEmptyLines) {
            while (true) {
                if (!eol || !isStartOfLine(lastChar)) {
                    break;
                }
                lastChar = c;
                c = this.reader.read();
                eol = readEndOfLine(c);
                if (isEndOfFile(c)) {
                    token.type = org.apache.commons.csv.Token.Type.EOF;
                    break;
                }
            }
            return token;
        }
        if (isEndOfFile(lastChar) || (!isDelimiter(lastChar) && isEndOfFile(c))) {
            token.type = org.apache.commons.csv.Token.Type.EOF;
            return token;
        } else if (!isStartOfLine(lastChar) || !isCommentStart(c)) {
            while (token.type == org.apache.commons.csv.Token.Type.INVALID) {
                if (this.ignoreSurroundingSpaces) {
                    while (isWhitespace(c) && !eol) {
                        c = this.reader.read();
                        eol = readEndOfLine(c);
                    }
                }
                if (isDelimiter(c)) {
                    token.type = org.apache.commons.csv.Token.Type.TOKEN;
                } else if (eol) {
                    token.type = org.apache.commons.csv.Token.Type.EORECORD;
                } else if (isQuoteChar(c)) {
                    parseEncapsulatedToken(token);
                } else if (isEndOfFile(c)) {
                    token.type = org.apache.commons.csv.Token.Type.EOF;
                    token.isReady = true;
                } else {
                    parseSimpleToken(token, c);
                }
            }
            return token;
        } else {
            java.lang.String line = this.reader.readLine();
            if (line == null) {
                token.type = org.apache.commons.csv.Token.Type.EOF;
            } else {
                token.content.append(line.trim());
                token.type = org.apache.commons.csv.Token.Type.COMMENT;
            }
            return token;
        }
    }

    private org.apache.commons.csv.Token parseSimpleToken(org.apache.commons.csv.Token token, int ch2) throws java.io.IOException {
        while (true) {
            if (readEndOfLine(ch2)) {
                token.type = org.apache.commons.csv.Token.Type.EORECORD;
                break;
            } else if (isEndOfFile(ch2)) {
                token.type = org.apache.commons.csv.Token.Type.EOF;
                token.isReady = true;
                break;
            } else if (isDelimiter(ch2)) {
                token.type = org.apache.commons.csv.Token.Type.TOKEN;
                break;
            } else if (isEscape(ch2)) {
                int unescaped = readEscape();
                if (unescaped == -1) {
                    token.content.append((char) ch2).append((char) this.reader.getLastChar());
                } else {
                    token.content.append((char) unescaped);
                }
                ch2 = this.reader.read();
            } else {
                token.content.append((char) ch2);
                ch2 = this.reader.read();
            }
        }
        if (this.ignoreSurroundingSpaces) {
            trimTrailingSpaces(token.content);
        }
        return token;
    }

    private org.apache.commons.csv.Token parseEncapsulatedToken(org.apache.commons.csv.Token token) throws java.io.IOException {
        int c;
        long startLineNumber = getCurrentLineNumber();
        while (true) {
            int c2 = this.reader.read();
            if (isEscape(c2)) {
                int unescaped = readEscape();
                if (unescaped == -1) {
                    token.content.append((char) c2).append((char) this.reader.getLastChar());
                } else {
                    token.content.append((char) unescaped);
                }
            } else if (isQuoteChar(c2)) {
                if (isQuoteChar(this.reader.lookAhead())) {
                    token.content.append((char) this.reader.read());
                } else {
                    do {
                        c = this.reader.read();
                        if (isDelimiter(c)) {
                            token.type = org.apache.commons.csv.Token.Type.TOKEN;
                        } else if (isEndOfFile(c)) {
                            token.type = org.apache.commons.csv.Token.Type.EOF;
                            token.isReady = true;
                        } else if (readEndOfLine(c)) {
                            token.type = org.apache.commons.csv.Token.Type.EORECORD;
                        }
                        return token;
                    } while (isWhitespace(c));
                    throw new java.io.IOException("(line " + getCurrentLineNumber() + ") invalid char between encapsulated token and delimiter");
                }
            } else if (isEndOfFile(c2)) {
                throw new java.io.IOException("(startline " + startLineNumber + ") EOF reached before encapsulated token finished");
            } else {
                token.content.append((char) c2);
            }
        }
    }

    private char mapNullToDisabled(java.lang.Character c) {
        return c == null ? DISABLED : c.charValue();
    }

    /* access modifiers changed from: 0000 */
    public long getCurrentLineNumber() {
        return this.reader.getCurrentLineNumber();
    }

    /* access modifiers changed from: 0000 */
    public int readEscape() throws java.io.IOException {
        int ch2 = this.reader.read();
        switch (ch2) {
            case -1:
                throw new java.io.IOException("EOF whilst processing escape sequence");
            case 8:
            case 9:
            case 10:
            case 12:
            case 13:
                return ch2;
            case 98:
                return 8;
            case 102:
                return 12;
            case 110:
                return 10;
            case 114:
                return 13;
            case 116:
                return 9;
            default:
                if (isMetaChar(ch2)) {
                    return ch2;
                }
                return -1;
        }
    }

    /* access modifiers changed from: 0000 */
    public void trimTrailingSpaces(java.lang.StringBuilder buffer) {
        int length = buffer.length();
        while (length > 0 && java.lang.Character.isWhitespace(buffer.charAt(length - 1))) {
            length--;
        }
        if (length != buffer.length()) {
            buffer.setLength(length);
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean readEndOfLine(int ch2) throws java.io.IOException {
        if (ch2 == 13 && this.reader.lookAhead() == 10) {
            ch2 = this.reader.read();
        }
        return ch2 == 10 || ch2 == 13;
    }

    /* access modifiers changed from: 0000 */
    public boolean isClosed() {
        return this.reader.isClosed();
    }

    /* access modifiers changed from: 0000 */
    public boolean isWhitespace(int ch2) {
        return !isDelimiter(ch2) && java.lang.Character.isWhitespace((char) ch2);
    }

    /* access modifiers changed from: 0000 */
    public boolean isStartOfLine(int ch2) {
        return ch2 == 10 || ch2 == 13 || ch2 == -2;
    }

    /* access modifiers changed from: 0000 */
    public boolean isEndOfFile(int ch2) {
        return ch2 == -1;
    }

    /* access modifiers changed from: 0000 */
    public boolean isDelimiter(int ch2) {
        return ch2 == this.delimiter;
    }

    /* access modifiers changed from: 0000 */
    public boolean isEscape(int ch2) {
        return ch2 == this.escape;
    }

    /* access modifiers changed from: 0000 */
    public boolean isQuoteChar(int ch2) {
        return ch2 == this.quoteChar;
    }

    /* access modifiers changed from: 0000 */
    public boolean isCommentStart(int ch2) {
        return ch2 == this.commentStart;
    }

    private boolean isMetaChar(int ch2) {
        return ch2 == this.delimiter || ch2 == this.escape || ch2 == this.quoteChar || ch2 == this.commentStart;
    }

    public void close() throws java.io.IOException {
        this.reader.close();
    }
}
