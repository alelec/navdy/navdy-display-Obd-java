package org.apache.commons.csv;

public final class CSVRecord implements java.io.Serializable, java.lang.Iterable<java.lang.String> {
    private static final java.lang.String[] EMPTY_STRING_ARRAY = new java.lang.String[0];
    private static final long serialVersionUID = 1;
    private final java.lang.String comment;
    private final java.util.Map<java.lang.String, java.lang.Integer> mapping;
    private final long recordNumber;
    private final java.lang.String[] values;

    CSVRecord(java.lang.String[] values2, java.util.Map<java.lang.String, java.lang.Integer> mapping2, java.lang.String comment2, long recordNumber2) {
        this.recordNumber = recordNumber2;
        if (values2 == null) {
            values2 = EMPTY_STRING_ARRAY;
        }
        this.values = values2;
        this.mapping = mapping2;
        this.comment = comment2;
    }

    public java.lang.String get(java.lang.Enum<?> e) {
        return get(e.toString());
    }

    public java.lang.String get(int i) {
        return this.values[i];
    }

    public java.lang.String get(java.lang.String name) {
        if (this.mapping == null) {
            throw new java.lang.IllegalStateException("No header mapping was specified, the record values can't be accessed by name");
        }
        java.lang.Integer index = (java.lang.Integer) this.mapping.get(name);
        if (index == null) {
            throw new java.lang.IllegalArgumentException(java.lang.String.format("Mapping for %s not found, expected one of %s", new java.lang.Object[]{name, this.mapping.keySet()}));
        }
        try {
            return this.values[index.intValue()];
        } catch (java.lang.ArrayIndexOutOfBoundsException e) {
            throw new java.lang.IllegalArgumentException(java.lang.String.format("Index for header '%s' is %d but CSVRecord only has %d values!", new java.lang.Object[]{name, index, java.lang.Integer.valueOf(this.values.length)}));
        }
    }

    public java.lang.String getComment() {
        return this.comment;
    }

    public long getRecordNumber() {
        return this.recordNumber;
    }

    public boolean isConsistent() {
        return this.mapping == null || this.mapping.size() == this.values.length;
    }

    public boolean isMapped(java.lang.String name) {
        return this.mapping != null && this.mapping.containsKey(name);
    }

    public boolean isSet(java.lang.String name) {
        return isMapped(name) && ((java.lang.Integer) this.mapping.get(name)).intValue() < this.values.length;
    }

    public java.util.Iterator<java.lang.String> iterator() {
        return toList().iterator();
    }

    /* access modifiers changed from: 0000 */
    public <M extends java.util.Map<java.lang.String, java.lang.String>> M putIn(M map) {
        if (this.mapping != null) {
            for (java.util.Map.Entry<java.lang.String, java.lang.Integer> entry : this.mapping.entrySet()) {
                int col = ((java.lang.Integer) entry.getValue()).intValue();
                if (col < this.values.length) {
                    map.put(entry.getKey(), this.values[col]);
                }
            }
        }
        return map;
    }

    public int size() {
        return this.values.length;
    }

    private java.util.List<java.lang.String> toList() {
        return java.util.Arrays.asList(this.values);
    }

    public java.util.Map<java.lang.String, java.lang.String> toMap() {
        return putIn(new java.util.HashMap(this.values.length));
    }

    public java.lang.String toString() {
        return java.util.Arrays.toString(this.values);
    }

    /* access modifiers changed from: 0000 */
    public java.lang.String[] values() {
        return this.values;
    }
}
