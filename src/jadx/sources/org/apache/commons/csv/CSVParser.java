package org.apache.commons.csv;

public final class CSVParser implements java.lang.Iterable<org.apache.commons.csv.CSVRecord>, java.io.Closeable {
    private final org.apache.commons.csv.CSVFormat format;
    private final java.util.Map<java.lang.String, java.lang.Integer> headerMap;
    private final org.apache.commons.csv.Lexer lexer;
    private final java.util.List<java.lang.String> record = new java.util.ArrayList();
    private long recordNumber;
    private final org.apache.commons.csv.Token reusableToken = new org.apache.commons.csv.Token();

    public static org.apache.commons.csv.CSVParser parse(java.io.File file, java.nio.charset.Charset charset, org.apache.commons.csv.CSVFormat format2) throws java.io.IOException {
        org.apache.commons.csv.Assertions.notNull(file, ch.qos.logback.core.joran.action.Action.FILE_ATTRIBUTE);
        org.apache.commons.csv.Assertions.notNull(format2, "format");
        return new org.apache.commons.csv.CSVParser(new java.io.InputStreamReader(new java.io.FileInputStream(file), charset), format2);
    }

    public static org.apache.commons.csv.CSVParser parse(java.lang.String string, org.apache.commons.csv.CSVFormat format2) throws java.io.IOException {
        org.apache.commons.csv.Assertions.notNull(string, "string");
        org.apache.commons.csv.Assertions.notNull(format2, "format");
        return new org.apache.commons.csv.CSVParser(new java.io.StringReader(string), format2);
    }

    public static org.apache.commons.csv.CSVParser parse(java.net.URL url, java.nio.charset.Charset charset, org.apache.commons.csv.CSVFormat format2) throws java.io.IOException {
        org.apache.commons.csv.Assertions.notNull(url, "url");
        org.apache.commons.csv.Assertions.notNull(charset, "charset");
        org.apache.commons.csv.Assertions.notNull(format2, "format");
        return new org.apache.commons.csv.CSVParser(new java.io.InputStreamReader(url.openStream(), charset), format2);
    }

    public CSVParser(java.io.Reader reader, org.apache.commons.csv.CSVFormat format2) throws java.io.IOException {
        org.apache.commons.csv.Assertions.notNull(reader, "reader");
        org.apache.commons.csv.Assertions.notNull(format2, "format");
        this.format = format2;
        this.lexer = new org.apache.commons.csv.Lexer(format2, new org.apache.commons.csv.ExtendedBufferedReader(reader));
        this.headerMap = initializeHeader();
    }

    private void addRecordValue() {
        java.lang.String input = this.reusableToken.content.toString();
        java.lang.String nullString = this.format.getNullString();
        if (nullString == null) {
            this.record.add(input);
            return;
        }
        java.util.List<java.lang.String> list = this.record;
        if (input.equalsIgnoreCase(nullString)) {
            input = null;
        }
        list.add(input);
    }

    public void close() throws java.io.IOException {
        if (this.lexer != null) {
            this.lexer.close();
        }
    }

    public long getCurrentLineNumber() {
        return this.lexer.getCurrentLineNumber();
    }

    public java.util.Map<java.lang.String, java.lang.Integer> getHeaderMap() {
        if (this.headerMap == null) {
            return null;
        }
        return new java.util.LinkedHashMap(this.headerMap);
    }

    public long getRecordNumber() {
        return this.recordNumber;
    }

    public java.util.List<org.apache.commons.csv.CSVRecord> getRecords() throws java.io.IOException {
        java.util.List<org.apache.commons.csv.CSVRecord> records = new java.util.ArrayList<>();
        while (true) {
            org.apache.commons.csv.CSVRecord rec = nextRecord();
            if (rec == null) {
                return records;
            }
            records.add(rec);
        }
    }

    private java.util.Map<java.lang.String, java.lang.Integer> initializeHeader() throws java.io.IOException {
        java.util.Map<java.lang.String, java.lang.Integer> hdrMap = null;
        java.lang.String[] formatHeader = this.format.getHeader();
        if (formatHeader != null) {
            hdrMap = new java.util.LinkedHashMap<>();
            java.lang.String[] headerRecord = null;
            if (formatHeader.length == 0) {
                org.apache.commons.csv.CSVRecord nextRecord = nextRecord();
                if (nextRecord != null) {
                    headerRecord = nextRecord.values();
                }
            } else {
                if (this.format.getSkipHeaderRecord()) {
                    nextRecord();
                }
                headerRecord = formatHeader;
            }
            if (headerRecord != null) {
                int i = 0;
                while (i < headerRecord.length) {
                    java.lang.String header = headerRecord[i];
                    boolean containsHeader = hdrMap.containsKey(header);
                    boolean emptyHeader = header == null || header.trim().isEmpty();
                    if (!containsHeader || (emptyHeader && (!emptyHeader || this.format.getAllowMissingColumnNames()))) {
                        hdrMap.put(header, java.lang.Integer.valueOf(i));
                        i++;
                    } else {
                        throw new java.lang.IllegalArgumentException("The header contains a duplicate name: \"" + header + "\" in " + java.util.Arrays.toString(headerRecord));
                    }
                }
            }
        }
        return hdrMap;
    }

    public boolean isClosed() {
        return this.lexer.isClosed();
    }

    public java.util.Iterator<org.apache.commons.csv.CSVRecord> iterator() {
        return new java.util.Iterator<org.apache.commons.csv.CSVRecord>() {
            private org.apache.commons.csv.CSVRecord current;

            private org.apache.commons.csv.CSVRecord getNextRecord() {
                try {
                    return org.apache.commons.csv.CSVParser.this.nextRecord();
                } catch (java.io.IOException e) {
                    throw new java.lang.RuntimeException(e);
                }
            }

            public boolean hasNext() {
                if (org.apache.commons.csv.CSVParser.this.isClosed()) {
                    return false;
                }
                if (this.current == null) {
                    this.current = getNextRecord();
                }
                if (this.current != null) {
                    return true;
                }
                return false;
            }

            public org.apache.commons.csv.CSVRecord next() {
                if (org.apache.commons.csv.CSVParser.this.isClosed()) {
                    throw new java.util.NoSuchElementException("CSVParser has been closed");
                }
                org.apache.commons.csv.CSVRecord next = this.current;
                this.current = null;
                if (next == null) {
                    next = getNextRecord();
                    if (next == null) {
                        throw new java.util.NoSuchElementException("No more CSV records available");
                    }
                }
                return next;
            }

            public void remove() {
                throw new java.lang.UnsupportedOperationException();
            }
        };
    }

    /* access modifiers changed from: 0000 */
    public org.apache.commons.csv.CSVRecord nextRecord() throws java.io.IOException {
        this.record.clear();
        java.lang.StringBuilder sb = null;
        do {
            this.reusableToken.reset();
            this.lexer.nextToken(this.reusableToken);
            switch (this.reusableToken.type) {
                case TOKEN:
                    addRecordValue();
                    break;
                case EORECORD:
                    addRecordValue();
                    break;
                case EOF:
                    if (this.reusableToken.isReady) {
                        addRecordValue();
                        break;
                    }
                    break;
                case INVALID:
                    throw new java.io.IOException("(line " + getCurrentLineNumber() + ") invalid parse sequence");
                case COMMENT:
                    if (sb == null) {
                        sb = new java.lang.StringBuilder();
                    } else {
                        sb.append(10);
                    }
                    sb.append(this.reusableToken.content);
                    this.reusableToken.type = org.apache.commons.csv.Token.Type.TOKEN;
                    break;
                default:
                    throw new java.lang.IllegalStateException("Unexpected Token type: " + this.reusableToken.type);
            }
        } while (this.reusableToken.type == org.apache.commons.csv.Token.Type.TOKEN);
        if (this.record.isEmpty()) {
            return null;
        }
        this.recordNumber++;
        return new org.apache.commons.csv.CSVRecord((java.lang.String[]) this.record.toArray(new java.lang.String[this.record.size()]), this.headerMap, sb == null ? null : sb.toString(), this.recordNumber);
    }
}
