package org.apache.commons.csv;

public final class CSVFormat implements java.io.Serializable {
    public static final org.apache.commons.csv.CSVFormat DEFAULT = new org.apache.commons.csv.CSVFormat(ch.qos.logback.core.CoreConstants.COMMA_CHAR, org.apache.commons.csv.Constants.DOUBLE_QUOTE_CHAR, null, null, null, false, true, "\r\n", null, null, false, false);
    public static final org.apache.commons.csv.CSVFormat EXCEL = DEFAULT.withIgnoreEmptyLines(false);
    public static final org.apache.commons.csv.CSVFormat MYSQL = DEFAULT.withDelimiter(9).withEscape((char) ch.qos.logback.core.CoreConstants.ESCAPE_CHAR).withIgnoreEmptyLines(false).withQuote((java.lang.Character) null).withRecordSeparator(10);
    public static final org.apache.commons.csv.CSVFormat RFC4180 = DEFAULT.withIgnoreEmptyLines(false);
    public static final org.apache.commons.csv.CSVFormat TDF = DEFAULT.withDelimiter(9).withIgnoreSurroundingSpaces(true);
    private static final long serialVersionUID = 1;
    private final boolean allowMissingColumnNames;
    private final java.lang.Character commentMarker;
    private final char delimiter;
    private final java.lang.Character escapeCharacter;
    private final java.lang.String[] header;
    private final boolean ignoreEmptyLines;
    private final boolean ignoreSurroundingSpaces;
    private final java.lang.String nullString;
    private final java.lang.Character quoteCharacter;
    private final org.apache.commons.csv.QuoteMode quoteMode;
    private final java.lang.String recordSeparator;
    private final boolean skipHeaderRecord;

    private static boolean isLineBreak(char c) {
        return c == 10 || c == 13;
    }

    private static boolean isLineBreak(java.lang.Character c) {
        return c != null && isLineBreak(c.charValue());
    }

    public static org.apache.commons.csv.CSVFormat newFormat(char delimiter2) {
        return new org.apache.commons.csv.CSVFormat(delimiter2, null, null, null, null, false, false, null, null, null, false, false);
    }

    private CSVFormat(char delimiter2, java.lang.Character quoteChar, org.apache.commons.csv.QuoteMode quoteMode2, java.lang.Character commentStart, java.lang.Character escape, boolean ignoreSurroundingSpaces2, boolean ignoreEmptyLines2, java.lang.String recordSeparator2, java.lang.String nullString2, java.lang.String[] header2, boolean skipHeaderRecord2, boolean allowMissingColumnNames2) {
        if (isLineBreak(delimiter2)) {
            throw new java.lang.IllegalArgumentException("The delimiter cannot be a line break");
        }
        this.delimiter = delimiter2;
        this.quoteCharacter = quoteChar;
        this.quoteMode = quoteMode2;
        this.commentMarker = commentStart;
        this.escapeCharacter = escape;
        this.ignoreSurroundingSpaces = ignoreSurroundingSpaces2;
        this.allowMissingColumnNames = allowMissingColumnNames2;
        this.ignoreEmptyLines = ignoreEmptyLines2;
        this.recordSeparator = recordSeparator2;
        this.nullString = nullString2;
        if (header2 == null) {
            this.header = null;
        } else {
            java.util.Set<java.lang.String> dupCheck = new java.util.HashSet<>();
            for (java.lang.String hdr : header2) {
                if (!dupCheck.add(hdr)) {
                    throw new java.lang.IllegalArgumentException("The header contains a duplicate entry: '" + hdr + "' in " + java.util.Arrays.toString(header2));
                }
            }
            this.header = (java.lang.String[]) header2.clone();
        }
        this.skipHeaderRecord = skipHeaderRecord2;
        validate();
    }

    public boolean equals(java.lang.Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        org.apache.commons.csv.CSVFormat other = (org.apache.commons.csv.CSVFormat) obj;
        if (this.delimiter != other.delimiter) {
            return false;
        }
        if (this.quoteMode != other.quoteMode) {
            return false;
        }
        if (this.quoteCharacter == null) {
            if (other.quoteCharacter != null) {
                return false;
            }
        } else if (!this.quoteCharacter.equals(other.quoteCharacter)) {
            return false;
        }
        if (this.commentMarker == null) {
            if (other.commentMarker != null) {
                return false;
            }
        } else if (!this.commentMarker.equals(other.commentMarker)) {
            return false;
        }
        if (this.escapeCharacter == null) {
            if (other.escapeCharacter != null) {
                return false;
            }
        } else if (!this.escapeCharacter.equals(other.escapeCharacter)) {
            return false;
        }
        if (this.nullString == null) {
            if (other.nullString != null) {
                return false;
            }
        } else if (!this.nullString.equals(other.nullString)) {
            return false;
        }
        if (!java.util.Arrays.equals(this.header, other.header)) {
            return false;
        }
        if (this.ignoreSurroundingSpaces != other.ignoreSurroundingSpaces) {
            return false;
        }
        if (this.ignoreEmptyLines != other.ignoreEmptyLines) {
            return false;
        }
        if (this.skipHeaderRecord != other.skipHeaderRecord) {
            return false;
        }
        if (this.recordSeparator == null) {
            if (other.recordSeparator != null) {
                return false;
            }
            return true;
        } else if (!this.recordSeparator.equals(other.recordSeparator)) {
            return false;
        } else {
            return true;
        }
    }

    public java.lang.String format(java.lang.Object... values) {
        java.io.StringWriter out = new java.io.StringWriter();
        try {
            new org.apache.commons.csv.CSVPrinter(out, this).printRecord(values);
            return out.toString().trim();
        } catch (java.io.IOException e) {
            throw new java.lang.IllegalStateException(e);
        }
    }

    public java.lang.Character getCommentMarker() {
        return this.commentMarker;
    }

    public char getDelimiter() {
        return this.delimiter;
    }

    public java.lang.Character getEscapeCharacter() {
        return this.escapeCharacter;
    }

    public java.lang.String[] getHeader() {
        if (this.header != null) {
            return (java.lang.String[]) this.header.clone();
        }
        return null;
    }

    public boolean getAllowMissingColumnNames() {
        return this.allowMissingColumnNames;
    }

    public boolean getIgnoreEmptyLines() {
        return this.ignoreEmptyLines;
    }

    public boolean getIgnoreSurroundingSpaces() {
        return this.ignoreSurroundingSpaces;
    }

    public java.lang.String getNullString() {
        return this.nullString;
    }

    public java.lang.Character getQuoteCharacter() {
        return this.quoteCharacter;
    }

    public org.apache.commons.csv.QuoteMode getQuoteMode() {
        return this.quoteMode;
    }

    public java.lang.String getRecordSeparator() {
        return this.recordSeparator;
    }

    public boolean getSkipHeaderRecord() {
        return this.skipHeaderRecord;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3 = 1231;
        int i4 = 0;
        int hashCode = (((((((((((this.delimiter + 31) * 31) + (this.quoteMode == null ? 0 : this.quoteMode.hashCode())) * 31) + (this.quoteCharacter == null ? 0 : this.quoteCharacter.hashCode())) * 31) + (this.commentMarker == null ? 0 : this.commentMarker.hashCode())) * 31) + (this.escapeCharacter == null ? 0 : this.escapeCharacter.hashCode())) * 31) + (this.nullString == null ? 0 : this.nullString.hashCode())) * 31;
        if (this.ignoreSurroundingSpaces) {
            i = 1231;
        } else {
            i = 1237;
        }
        int i5 = (hashCode + i) * 31;
        if (this.ignoreEmptyLines) {
            i2 = 1231;
        } else {
            i2 = 1237;
        }
        int i6 = (i5 + i2) * 31;
        if (!this.skipHeaderRecord) {
            i3 = 1237;
        }
        int i7 = (i6 + i3) * 31;
        if (this.recordSeparator != null) {
            i4 = this.recordSeparator.hashCode();
        }
        return ((i7 + i4) * 31) + java.util.Arrays.hashCode(this.header);
    }

    public boolean isCommentMarkerSet() {
        return this.commentMarker != null;
    }

    public boolean isEscapeCharacterSet() {
        return this.escapeCharacter != null;
    }

    public boolean isNullStringSet() {
        return this.nullString != null;
    }

    public boolean isQuoteCharacterSet() {
        return this.quoteCharacter != null;
    }

    public org.apache.commons.csv.CSVParser parse(java.io.Reader in) throws java.io.IOException {
        return new org.apache.commons.csv.CSVParser(in, this);
    }

    public org.apache.commons.csv.CSVPrinter print(java.lang.Appendable out) throws java.io.IOException {
        return new org.apache.commons.csv.CSVPrinter(out, this);
    }

    public java.lang.String toString() {
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        sb.append("Delimiter=<").append(this.delimiter).append('>');
        if (isEscapeCharacterSet()) {
            sb.append(' ');
            sb.append("Escape=<").append(this.escapeCharacter).append('>');
        }
        if (isQuoteCharacterSet()) {
            sb.append(' ');
            sb.append("QuoteChar=<").append(this.quoteCharacter).append('>');
        }
        if (isCommentMarkerSet()) {
            sb.append(' ');
            sb.append("CommentStart=<").append(this.commentMarker).append('>');
        }
        if (isNullStringSet()) {
            sb.append(' ');
            sb.append("NullString=<").append(this.nullString).append('>');
        }
        if (this.recordSeparator != null) {
            sb.append(' ');
            sb.append("RecordSeparator=<").append(this.recordSeparator).append('>');
        }
        if (getIgnoreEmptyLines()) {
            sb.append(" EmptyLines:ignored");
        }
        if (getIgnoreSurroundingSpaces()) {
            sb.append(" SurroundingSpaces:ignored");
        }
        sb.append(" SkipHeaderRecord:").append(this.skipHeaderRecord);
        if (this.header != null) {
            sb.append(' ');
            sb.append("Header:").append(java.util.Arrays.toString(this.header));
        }
        return sb.toString();
    }

    private void validate() throws java.lang.IllegalArgumentException {
        if (this.quoteCharacter != null && this.delimiter == this.quoteCharacter.charValue()) {
            throw new java.lang.IllegalArgumentException("The quoteChar character and the delimiter cannot be the same ('" + this.quoteCharacter + "')");
        } else if (this.escapeCharacter != null && this.delimiter == this.escapeCharacter.charValue()) {
            throw new java.lang.IllegalArgumentException("The escape character and the delimiter cannot be the same ('" + this.escapeCharacter + "')");
        } else if (this.commentMarker != null && this.delimiter == this.commentMarker.charValue()) {
            throw new java.lang.IllegalArgumentException("The comment start character and the delimiter cannot be the same ('" + this.commentMarker + "')");
        } else if (this.quoteCharacter != null && this.quoteCharacter.equals(this.commentMarker)) {
            throw new java.lang.IllegalArgumentException("The comment start character and the quoteChar cannot be the same ('" + this.commentMarker + "')");
        } else if (this.escapeCharacter != null && this.escapeCharacter.equals(this.commentMarker)) {
            throw new java.lang.IllegalArgumentException("The comment start and the escape character cannot be the same ('" + this.commentMarker + "')");
        } else if (this.escapeCharacter == null && this.quoteMode == org.apache.commons.csv.QuoteMode.NONE) {
            throw new java.lang.IllegalArgumentException("No quotes mode set but no escape character is set");
        }
    }

    public org.apache.commons.csv.CSVFormat withCommentMarker(char commentMarker2) {
        return withCommentMarker(java.lang.Character.valueOf(commentMarker2));
    }

    public org.apache.commons.csv.CSVFormat withCommentMarker(java.lang.Character commentMarker2) {
        if (isLineBreak(commentMarker2)) {
            throw new java.lang.IllegalArgumentException("The comment start marker character cannot be a line break");
        }
        return new org.apache.commons.csv.CSVFormat(this.delimiter, this.quoteCharacter, this.quoteMode, commentMarker2, this.escapeCharacter, this.ignoreSurroundingSpaces, this.ignoreEmptyLines, this.recordSeparator, this.nullString, this.header, this.skipHeaderRecord, this.allowMissingColumnNames);
    }

    public org.apache.commons.csv.CSVFormat withDelimiter(char delimiter2) {
        if (isLineBreak(delimiter2)) {
            throw new java.lang.IllegalArgumentException("The delimiter cannot be a line break");
        }
        return new org.apache.commons.csv.CSVFormat(delimiter2, this.quoteCharacter, this.quoteMode, this.commentMarker, this.escapeCharacter, this.ignoreSurroundingSpaces, this.ignoreEmptyLines, this.recordSeparator, this.nullString, this.header, this.skipHeaderRecord, this.allowMissingColumnNames);
    }

    public org.apache.commons.csv.CSVFormat withEscape(char escape) {
        return withEscape(java.lang.Character.valueOf(escape));
    }

    public org.apache.commons.csv.CSVFormat withEscape(java.lang.Character escape) {
        if (isLineBreak(escape)) {
            throw new java.lang.IllegalArgumentException("The escape character cannot be a line break");
        }
        return new org.apache.commons.csv.CSVFormat(this.delimiter, this.quoteCharacter, this.quoteMode, this.commentMarker, escape, this.ignoreSurroundingSpaces, this.ignoreEmptyLines, this.recordSeparator, this.nullString, this.header, this.skipHeaderRecord, this.allowMissingColumnNames);
    }

    public org.apache.commons.csv.CSVFormat withHeader(java.lang.String... header2) {
        return new org.apache.commons.csv.CSVFormat(this.delimiter, this.quoteCharacter, this.quoteMode, this.commentMarker, this.escapeCharacter, this.ignoreSurroundingSpaces, this.ignoreEmptyLines, this.recordSeparator, this.nullString, header2, this.skipHeaderRecord, this.allowMissingColumnNames);
    }

    public org.apache.commons.csv.CSVFormat withAllowMissingColumnNames(boolean allowMissingColumnNames2) {
        return new org.apache.commons.csv.CSVFormat(this.delimiter, this.quoteCharacter, this.quoteMode, this.commentMarker, this.escapeCharacter, this.ignoreSurroundingSpaces, this.ignoreEmptyLines, this.recordSeparator, this.nullString, this.header, this.skipHeaderRecord, allowMissingColumnNames2);
    }

    public org.apache.commons.csv.CSVFormat withIgnoreEmptyLines(boolean ignoreEmptyLines2) {
        return new org.apache.commons.csv.CSVFormat(this.delimiter, this.quoteCharacter, this.quoteMode, this.commentMarker, this.escapeCharacter, this.ignoreSurroundingSpaces, ignoreEmptyLines2, this.recordSeparator, this.nullString, this.header, this.skipHeaderRecord, this.allowMissingColumnNames);
    }

    public org.apache.commons.csv.CSVFormat withIgnoreSurroundingSpaces(boolean ignoreSurroundingSpaces2) {
        return new org.apache.commons.csv.CSVFormat(this.delimiter, this.quoteCharacter, this.quoteMode, this.commentMarker, this.escapeCharacter, ignoreSurroundingSpaces2, this.ignoreEmptyLines, this.recordSeparator, this.nullString, this.header, this.skipHeaderRecord, this.allowMissingColumnNames);
    }

    public org.apache.commons.csv.CSVFormat withNullString(java.lang.String nullString2) {
        return new org.apache.commons.csv.CSVFormat(this.delimiter, this.quoteCharacter, this.quoteMode, this.commentMarker, this.escapeCharacter, this.ignoreSurroundingSpaces, this.ignoreEmptyLines, this.recordSeparator, nullString2, this.header, this.skipHeaderRecord, this.allowMissingColumnNames);
    }

    public org.apache.commons.csv.CSVFormat withQuote(char quoteChar) {
        return withQuote(java.lang.Character.valueOf(quoteChar));
    }

    public org.apache.commons.csv.CSVFormat withQuote(java.lang.Character quoteChar) {
        if (isLineBreak(quoteChar)) {
            throw new java.lang.IllegalArgumentException("The quoteChar cannot be a line break");
        }
        return new org.apache.commons.csv.CSVFormat(this.delimiter, quoteChar, this.quoteMode, this.commentMarker, this.escapeCharacter, this.ignoreSurroundingSpaces, this.ignoreEmptyLines, this.recordSeparator, this.nullString, this.header, this.skipHeaderRecord, this.allowMissingColumnNames);
    }

    public org.apache.commons.csv.CSVFormat withQuoteMode(org.apache.commons.csv.QuoteMode quoteModePolicy) {
        return new org.apache.commons.csv.CSVFormat(this.delimiter, this.quoteCharacter, quoteModePolicy, this.commentMarker, this.escapeCharacter, this.ignoreSurroundingSpaces, this.ignoreEmptyLines, this.recordSeparator, this.nullString, this.header, this.skipHeaderRecord, this.allowMissingColumnNames);
    }

    public org.apache.commons.csv.CSVFormat withRecordSeparator(char recordSeparator2) {
        return withRecordSeparator(java.lang.String.valueOf(recordSeparator2));
    }

    public org.apache.commons.csv.CSVFormat withRecordSeparator(java.lang.String recordSeparator2) {
        return new org.apache.commons.csv.CSVFormat(this.delimiter, this.quoteCharacter, this.quoteMode, this.commentMarker, this.escapeCharacter, this.ignoreSurroundingSpaces, this.ignoreEmptyLines, recordSeparator2, this.nullString, this.header, this.skipHeaderRecord, this.allowMissingColumnNames);
    }

    public org.apache.commons.csv.CSVFormat withSkipHeaderRecord(boolean skipHeaderRecord2) {
        return new org.apache.commons.csv.CSVFormat(this.delimiter, this.quoteCharacter, this.quoteMode, this.commentMarker, this.escapeCharacter, this.ignoreSurroundingSpaces, this.ignoreEmptyLines, this.recordSeparator, this.nullString, this.header, skipHeaderRecord2, this.allowMissingColumnNames);
    }
}
