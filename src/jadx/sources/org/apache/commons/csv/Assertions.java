package org.apache.commons.csv;

final class Assertions {
    private Assertions() {
    }

    public static void notNull(java.lang.Object parameter, java.lang.String parameterName) {
        if (parameter == null) {
            throw new java.lang.IllegalArgumentException("Parameter '" + parameterName + "' must not be null!");
        }
    }
}
