package org.apache.commons.csv;

final class ExtendedBufferedReader extends java.io.BufferedReader {
    private boolean closed;
    private long eolCounter = 0;
    private int lastChar = -2;

    ExtendedBufferedReader(java.io.Reader reader) {
        super(reader);
    }

    public int read() throws java.io.IOException {
        int current = super.read();
        if (current == 13 || (current == 10 && this.lastChar != 13)) {
            this.eolCounter++;
        }
        this.lastChar = current;
        return this.lastChar;
    }

    /* access modifiers changed from: 0000 */
    public int getLastChar() {
        return this.lastChar;
    }

    public int read(char[] buf, int offset, int length) throws java.io.IOException {
        if (length == 0) {
            return 0;
        }
        int len = super.read(buf, offset, length);
        if (len > 0) {
            int i = offset;
            while (i < offset + len) {
                char ch2 = buf[i];
                if (ch2 == 10) {
                    if (13 != (i > 0 ? buf[i - 1] : this.lastChar)) {
                        this.eolCounter++;
                    }
                } else if (ch2 == 13) {
                    this.eolCounter++;
                }
                i++;
            }
            this.lastChar = buf[(offset + len) - 1];
            return len;
        } else if (len != -1) {
            return len;
        } else {
            this.lastChar = -1;
            return len;
        }
    }

    public java.lang.String readLine() throws java.io.IOException {
        java.lang.String line = super.readLine();
        if (line != null) {
            this.lastChar = 10;
            this.eolCounter++;
        } else {
            this.lastChar = -1;
        }
        return line;
    }

    /* access modifiers changed from: 0000 */
    public int lookAhead() throws java.io.IOException {
        super.mark(1);
        int c = super.read();
        super.reset();
        return c;
    }

    /* access modifiers changed from: 0000 */
    public long getCurrentLineNumber() {
        if (this.lastChar == 13 || this.lastChar == 10 || this.lastChar == -2 || this.lastChar == -1) {
            return this.eolCounter;
        }
        return this.eolCounter + 1;
    }

    public boolean isClosed() {
        return this.closed;
    }

    public void close() throws java.io.IOException {
        this.closed = true;
        this.lastChar = -1;
        super.close();
    }
}
