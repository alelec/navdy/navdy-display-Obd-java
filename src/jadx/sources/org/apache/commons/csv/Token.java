package org.apache.commons.csv;

final class Token {
    private static final int INITIAL_TOKEN_LENGTH = 50;
    final java.lang.StringBuilder content = new java.lang.StringBuilder(50);
    boolean isReady;
    org.apache.commons.csv.Token.Type type = org.apache.commons.csv.Token.Type.INVALID;

    enum Type {
        INVALID,
        TOKEN,
        EOF,
        EORECORD,
        COMMENT
    }

    Token() {
    }

    /* access modifiers changed from: 0000 */
    public void reset() {
        this.content.setLength(0);
        this.type = org.apache.commons.csv.Token.Type.INVALID;
        this.isReady = false;
    }

    public java.lang.String toString() {
        return this.type.name() + " [" + this.content.toString() + "]";
    }
}
