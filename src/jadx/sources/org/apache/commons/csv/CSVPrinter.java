package org.apache.commons.csv;

public final class CSVPrinter implements java.io.Flushable, java.io.Closeable {
    private final org.apache.commons.csv.CSVFormat format;
    private boolean newRecord = true;
    private final java.lang.Appendable out;

    public CSVPrinter(java.lang.Appendable out2, org.apache.commons.csv.CSVFormat format2) throws java.io.IOException {
        org.apache.commons.csv.Assertions.notNull(out2, "out");
        org.apache.commons.csv.Assertions.notNull(format2, "format");
        this.out = out2;
        this.format = format2;
        if (format2.getHeader() != null) {
            printRecord((java.lang.Object[]) format2.getHeader());
        }
    }

    public void close() throws java.io.IOException {
        if (this.out instanceof java.io.Closeable) {
            ((java.io.Closeable) this.out).close();
        }
    }

    public void flush() throws java.io.IOException {
        if (this.out instanceof java.io.Flushable) {
            ((java.io.Flushable) this.out).flush();
        }
    }

    public void print(java.lang.Object value) throws java.io.IOException {
        java.lang.String strValue;
        if (value == null) {
            java.lang.String nullString = this.format.getNullString();
            if (nullString == null) {
                strValue = "";
            } else {
                strValue = nullString;
            }
        } else {
            strValue = value.toString();
        }
        print(value, strValue, 0, strValue.length());
    }

    private void print(java.lang.Object object, java.lang.CharSequence value, int offset, int len) throws java.io.IOException {
        if (!this.newRecord) {
            this.out.append(this.format.getDelimiter());
        }
        if (this.format.isQuoteCharacterSet()) {
            printAndQuote(object, value, offset, len);
        } else if (this.format.isEscapeCharacterSet()) {
            printAndEscape(value, offset, len);
        } else {
            this.out.append(value, offset, offset + len);
        }
        this.newRecord = false;
    }

    private void printAndEscape(java.lang.CharSequence value, int offset, int len) throws java.io.IOException {
        int start = offset;
        int pos = offset;
        int end = offset + len;
        char delim = this.format.getDelimiter();
        char escape = this.format.getEscapeCharacter().charValue();
        while (pos < end) {
            char c = value.charAt(pos);
            if (c == 13 || c == 10 || c == delim || c == escape) {
                if (pos > start) {
                    this.out.append(value, start, pos);
                }
                if (c == 10) {
                    c = 'n';
                } else if (c == 13) {
                    c = 'r';
                }
                this.out.append(escape);
                this.out.append(c);
                start = pos + 1;
            }
            pos++;
        }
        if (pos > start) {
            this.out.append(value, start, pos);
        }
    }

    private void printAndQuote(java.lang.Object object, java.lang.CharSequence value, int offset, int len) throws java.io.IOException {
        boolean quote = false;
        int start = offset;
        int pos = offset;
        int end = offset + len;
        char delimChar = this.format.getDelimiter();
        char quoteChar = this.format.getQuoteCharacter().charValue();
        org.apache.commons.csv.QuoteMode quoteModePolicy = this.format.getQuoteMode();
        if (quoteModePolicy == null) {
            quoteModePolicy = org.apache.commons.csv.QuoteMode.MINIMAL;
        }
        switch (quoteModePolicy) {
            case ALL:
                quote = true;
                break;
            case NON_NUMERIC:
                if (object instanceof java.lang.Number) {
                    quote = false;
                    break;
                } else {
                    quote = true;
                    break;
                }
            case NONE:
                printAndEscape(value, offset, len);
                return;
            case MINIMAL:
                if (len > 0) {
                    char c = value.charAt(pos);
                    if (this.newRecord && (c < '0' || ((c > '9' && c < 'A') || ((c > 'Z' && c < 'a') || c > 'z')))) {
                        quote = true;
                    } else if (c <= '#') {
                        quote = true;
                    } else {
                        while (true) {
                            if (pos < end) {
                                char c2 = value.charAt(pos);
                                if (c2 == 10 || c2 == 13 || c2 == quoteChar || c2 == delimChar) {
                                    quote = true;
                                } else {
                                    pos++;
                                }
                            }
                        }
                        quote = true;
                        if (!quote) {
                            pos = end - 1;
                            if (value.charAt(pos) <= ' ') {
                                quote = true;
                            }
                        }
                    }
                } else if (this.newRecord) {
                    quote = true;
                }
                if (!quote) {
                    this.out.append(value, start, end);
                    return;
                }
                break;
            default:
                throw new java.lang.IllegalStateException("Unexpected Quote value: " + quoteModePolicy);
        }
        if (!quote) {
            this.out.append(value, start, end);
            return;
        }
        this.out.append(quoteChar);
        while (pos < end) {
            if (value.charAt(pos) == quoteChar) {
                this.out.append(value, start, pos + 1);
                start = pos;
            }
            pos++;
        }
        this.out.append(value, start, pos);
        this.out.append(quoteChar);
    }

    public void printComment(java.lang.String comment) throws java.io.IOException {
        if (this.format.isCommentMarkerSet()) {
            if (!this.newRecord) {
                println();
            }
            this.out.append(this.format.getCommentMarker().charValue());
            this.out.append(' ');
            int i = 0;
            while (i < comment.length()) {
                char c = comment.charAt(i);
                switch (c) {
                    case 10:
                        break;
                    case 13:
                        if (i + 1 < comment.length() && comment.charAt(i + 1) == 10) {
                            i++;
                            break;
                        }
                    default:
                        this.out.append(c);
                        continue;
                }
                println();
                this.out.append(this.format.getCommentMarker().charValue());
                this.out.append(' ');
                i++;
            }
            println();
        }
    }

    public void println() throws java.io.IOException {
        java.lang.String recordSeparator = this.format.getRecordSeparator();
        if (recordSeparator != null) {
            this.out.append(recordSeparator);
        }
        this.newRecord = true;
    }

    public void printRecord(java.lang.Iterable<?> values) throws java.io.IOException {
        for (java.lang.Object value : values) {
            print(value);
        }
        println();
    }

    public void printRecord(java.lang.Object... values) throws java.io.IOException {
        for (java.lang.Object value : values) {
            print(value);
        }
        println();
    }

    public void printRecords(java.lang.Iterable<?> values) throws java.io.IOException {
        for (java.lang.Object value : values) {
            if (value instanceof java.lang.Object[]) {
                printRecord((java.lang.Object[]) (java.lang.Object[]) value);
            } else if (value instanceof java.lang.Iterable) {
                printRecord((java.lang.Iterable) value);
            } else {
                printRecord(value);
            }
        }
    }

    public void printRecords(java.lang.Object... values) throws java.io.IOException {
        for (java.lang.Object value : values) {
            if (value instanceof java.lang.Object[]) {
                printRecord((java.lang.Object[]) (java.lang.Object[]) value);
            } else if (value instanceof java.lang.Iterable) {
                printRecord((java.lang.Iterable) value);
            } else {
                printRecord(value);
            }
        }
    }

    public void printRecords(java.sql.ResultSet resultSet) throws java.sql.SQLException, java.io.IOException {
        int columnCount = resultSet.getMetaData().getColumnCount();
        while (resultSet.next()) {
            for (int i = 1; i <= columnCount; i++) {
                print(resultSet.getString(i));
            }
            println();
        }
    }

    public java.lang.Appendable getOut() {
        return this.out;
    }
}
