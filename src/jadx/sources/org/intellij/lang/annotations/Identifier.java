package org.intellij.lang.annotations;

@org.intellij.lang.annotations.Pattern("\\p{javaJavaIdentifierStart}\\p{javaJavaIdentifierPart}*")
public @interface Identifier {
}
