package org.intellij.lang.annotations;

public @interface Subst {
    java.lang.String value();
}
