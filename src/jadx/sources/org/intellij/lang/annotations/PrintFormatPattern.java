package org.intellij.lang.annotations;

/* compiled from: PrintFormat */
class PrintFormatPattern {
    @org.intellij.lang.annotations.Language("RegExp")
    private static final java.lang.String ARG_INDEX = "(?:\\d+\\$)?";
    @org.intellij.lang.annotations.Language("RegExp")
    private static final java.lang.String CONVERSION = "(?:[tT])?(?:[a-zA-Z%])";
    @org.intellij.lang.annotations.Language("RegExp")
    private static final java.lang.String FLAGS = "(?:[-#+ 0,(<]*)?";
    @org.intellij.lang.annotations.Language("RegExp")
    private static final java.lang.String PRECISION = "(?:\\.\\d+)?";
    @org.intellij.lang.annotations.Language("RegExp")
    static final java.lang.String PRINT_FORMAT = "(?:[^%]|%%|(?:%(?:\\d+\\$)?(?:[-#+ 0,(<]*)?(?:\\d+)?(?:\\.\\d+)?(?:[tT])?(?:[a-zA-Z%])))*";
    @org.intellij.lang.annotations.Language("RegExp")
    private static final java.lang.String TEXT = "[^%]|%%";
    @org.intellij.lang.annotations.Language("RegExp")
    private static final java.lang.String WIDTH = "(?:\\d+)?";

    PrintFormatPattern() {
    }
}
