package org.slf4j.helpers;

public class BasicMarkerFactory implements org.slf4j.IMarkerFactory {
    private final java.util.concurrent.ConcurrentMap<java.lang.String, org.slf4j.Marker> markerMap = new java.util.concurrent.ConcurrentHashMap();

    public org.slf4j.Marker getMarker(java.lang.String name) {
        if (name == null) {
            throw new java.lang.IllegalArgumentException("Marker name cannot be null");
        }
        org.slf4j.Marker marker = (org.slf4j.Marker) this.markerMap.get(name);
        if (marker != null) {
            return marker;
        }
        org.slf4j.Marker marker2 = new org.slf4j.helpers.BasicMarker(name);
        org.slf4j.Marker oldMarker = (org.slf4j.Marker) this.markerMap.putIfAbsent(name, marker2);
        if (oldMarker != null) {
            return oldMarker;
        }
        return marker2;
    }

    public boolean exists(java.lang.String name) {
        if (name == null) {
            return false;
        }
        return this.markerMap.containsKey(name);
    }

    public boolean detachMarker(java.lang.String name) {
        if (name == null || this.markerMap.remove(name) == null) {
            return false;
        }
        return true;
    }

    public org.slf4j.Marker getDetachedMarker(java.lang.String name) {
        return new org.slf4j.helpers.BasicMarker(name);
    }
}
