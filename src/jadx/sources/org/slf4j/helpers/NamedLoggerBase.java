package org.slf4j.helpers;

abstract class NamedLoggerBase implements org.slf4j.Logger, java.io.Serializable {
    private static final long serialVersionUID = 7535258609338176893L;
    protected java.lang.String name;

    NamedLoggerBase() {
    }

    public java.lang.String getName() {
        return this.name;
    }

    /* access modifiers changed from: protected */
    public java.lang.Object readResolve() throws java.io.ObjectStreamException {
        return org.slf4j.LoggerFactory.getLogger(getName());
    }
}
