package org.slf4j.helpers;

public class BasicMDCAdapter implements org.slf4j.spi.MDCAdapter {
    private java.lang.InheritableThreadLocal<java.util.Map<java.lang.String, java.lang.String>> inheritableThreadLocal = new java.lang.InheritableThreadLocal<java.util.Map<java.lang.String, java.lang.String>>() {
        /* access modifiers changed from: protected */
        public java.util.Map<java.lang.String, java.lang.String> childValue(java.util.Map<java.lang.String, java.lang.String> parentValue) {
            if (parentValue == null) {
                return null;
            }
            return new java.util.HashMap(parentValue);
        }
    };

    public void put(java.lang.String key, java.lang.String val) {
        if (key == null) {
            throw new java.lang.IllegalArgumentException("key cannot be null");
        }
        java.util.Map<java.lang.String, java.lang.String> map = (java.util.Map) this.inheritableThreadLocal.get();
        if (map == null) {
            map = new java.util.HashMap<>();
            this.inheritableThreadLocal.set(map);
        }
        map.put(key, val);
    }

    public java.lang.String get(java.lang.String key) {
        java.util.Map<java.lang.String, java.lang.String> map = (java.util.Map) this.inheritableThreadLocal.get();
        if (map == null || key == null) {
            return null;
        }
        return (java.lang.String) map.get(key);
    }

    public void remove(java.lang.String key) {
        java.util.Map<java.lang.String, java.lang.String> map = (java.util.Map) this.inheritableThreadLocal.get();
        if (map != null) {
            map.remove(key);
        }
    }

    public void clear() {
        java.util.Map<java.lang.String, java.lang.String> map = (java.util.Map) this.inheritableThreadLocal.get();
        if (map != null) {
            map.clear();
            this.inheritableThreadLocal.remove();
        }
    }

    public java.util.Set<java.lang.String> getKeys() {
        java.util.Map<java.lang.String, java.lang.String> map = (java.util.Map) this.inheritableThreadLocal.get();
        if (map != null) {
            return map.keySet();
        }
        return null;
    }

    public java.util.Map<java.lang.String, java.lang.String> getCopyOfContextMap() {
        java.util.Map<java.lang.String, java.lang.String> oldMap = (java.util.Map) this.inheritableThreadLocal.get();
        if (oldMap != null) {
            return new java.util.HashMap(oldMap);
        }
        return null;
    }

    public void setContextMap(java.util.Map<java.lang.String, java.lang.String> contextMap) {
        this.inheritableThreadLocal.set(new java.util.HashMap(contextMap));
    }
}
