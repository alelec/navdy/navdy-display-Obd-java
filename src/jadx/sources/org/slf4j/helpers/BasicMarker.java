package org.slf4j.helpers;

public class BasicMarker implements org.slf4j.Marker {
    private static java.lang.String CLOSE = " ]";
    private static java.lang.String OPEN = "[ ";
    private static java.lang.String SEP = ", ";
    private static final long serialVersionUID = 1803952589649545191L;
    private final java.lang.String name;
    private java.util.List<org.slf4j.Marker> referenceList;

    BasicMarker(java.lang.String name2) {
        if (name2 == null) {
            throw new java.lang.IllegalArgumentException("A marker name cannot be null");
        }
        this.name = name2;
    }

    public java.lang.String getName() {
        return this.name;
    }

    public synchronized void add(org.slf4j.Marker reference) {
        if (reference == null) {
            throw new java.lang.IllegalArgumentException("A null value cannot be added to a Marker as reference.");
        } else if (!contains(reference)) {
            if (!reference.contains((org.slf4j.Marker) this)) {
                if (this.referenceList == null) {
                    this.referenceList = new java.util.Vector();
                }
                this.referenceList.add(reference);
            }
        }
    }

    public synchronized boolean hasReferences() {
        return this.referenceList != null && this.referenceList.size() > 0;
    }

    public boolean hasChildren() {
        return hasReferences();
    }

    public synchronized java.util.Iterator<org.slf4j.Marker> iterator() {
        java.util.Iterator<org.slf4j.Marker> it;
        if (this.referenceList != null) {
            it = this.referenceList.iterator();
        } else {
            it = java.util.Collections.emptyList().iterator();
        }
        return it;
    }

    public synchronized boolean remove(org.slf4j.Marker referenceToRemove) {
        boolean z = false;
        synchronized (this) {
            if (this.referenceList != null) {
                int size = this.referenceList.size();
                int i = 0;
                while (true) {
                    if (i >= size) {
                        break;
                    } else if (referenceToRemove.equals((org.slf4j.Marker) this.referenceList.get(i))) {
                        this.referenceList.remove(i);
                        z = true;
                        break;
                    } else {
                        i++;
                    }
                }
            }
        }
        return z;
    }

    public boolean contains(org.slf4j.Marker other) {
        if (other == null) {
            throw new java.lang.IllegalArgumentException("Other cannot be null");
        } else if (equals(other)) {
            return true;
        } else {
            if (hasReferences()) {
                for (org.slf4j.Marker ref : this.referenceList) {
                    if (ref.contains(other)) {
                        return true;
                    }
                }
            }
            return false;
        }
    }

    public boolean contains(java.lang.String name2) {
        if (name2 == null) {
            throw new java.lang.IllegalArgumentException("Other cannot be null");
        } else if (this.name.equals(name2)) {
            return true;
        } else {
            if (hasReferences()) {
                for (org.slf4j.Marker ref : this.referenceList) {
                    if (ref.contains(name2)) {
                        return true;
                    }
                }
            }
            return false;
        }
    }

    public boolean equals(java.lang.Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof org.slf4j.Marker)) {
            return false;
        }
        return this.name.equals(((org.slf4j.Marker) obj).getName());
    }

    public int hashCode() {
        return this.name.hashCode();
    }

    public java.lang.String toString() {
        if (!hasReferences()) {
            return getName();
        }
        java.util.Iterator<org.slf4j.Marker> it = iterator();
        java.lang.StringBuilder sb = new java.lang.StringBuilder(getName());
        sb.append(' ').append(OPEN);
        while (it.hasNext()) {
            sb.append(((org.slf4j.Marker) it.next()).getName());
            if (it.hasNext()) {
                sb.append(SEP);
            }
        }
        sb.append(CLOSE);
        return sb.toString();
    }
}
