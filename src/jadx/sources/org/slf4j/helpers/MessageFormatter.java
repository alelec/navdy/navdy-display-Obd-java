package org.slf4j.helpers;

public final class MessageFormatter {
    static final char DELIM_START = '{';
    static final char DELIM_STOP = '}';
    static final java.lang.String DELIM_STR = "{}";
    private static final char ESCAPE_CHAR = '\\';

    public static final org.slf4j.helpers.FormattingTuple format(java.lang.String messagePattern, java.lang.Object arg) {
        return arrayFormat(messagePattern, new java.lang.Object[]{arg});
    }

    public static final org.slf4j.helpers.FormattingTuple format(java.lang.String messagePattern, java.lang.Object arg1, java.lang.Object arg2) {
        return arrayFormat(messagePattern, new java.lang.Object[]{arg1, arg2});
    }

    static final java.lang.Throwable getThrowableCandidate(java.lang.Object[] argArray) {
        if (argArray == null || argArray.length == 0) {
            return null;
        }
        java.lang.Object lastEntry = argArray[argArray.length - 1];
        if (lastEntry instanceof java.lang.Throwable) {
            return (java.lang.Throwable) lastEntry;
        }
        return null;
    }

    public static final org.slf4j.helpers.FormattingTuple arrayFormat(java.lang.String messagePattern, java.lang.Object[] argArray) {
        java.lang.Throwable throwableCandidate = getThrowableCandidate(argArray);
        java.lang.Object[] args = argArray;
        if (throwableCandidate != null) {
            args = trimmedCopy(argArray);
        }
        return arrayFormat(messagePattern, args, throwableCandidate);
    }

    private static java.lang.Object[] trimmedCopy(java.lang.Object[] argArray) {
        if (argArray == null || argArray.length == 0) {
            throw new java.lang.IllegalStateException("non-sensical empty or null argument array");
        }
        int trimemdLen = argArray.length - 1;
        java.lang.Object[] trimmed = new java.lang.Object[trimemdLen];
        java.lang.System.arraycopy(argArray, 0, trimmed, 0, trimemdLen);
        return trimmed;
    }

    public static final org.slf4j.helpers.FormattingTuple arrayFormat(java.lang.String messagePattern, java.lang.Object[] argArray, java.lang.Throwable throwable) {
        if (messagePattern == null) {
            return new org.slf4j.helpers.FormattingTuple(null, argArray, throwable);
        }
        if (argArray == null) {
            return new org.slf4j.helpers.FormattingTuple(messagePattern);
        }
        int i = 0;
        java.lang.StringBuilder sbuf = new java.lang.StringBuilder(messagePattern.length() + 50);
        int L = 0;
        while (L < argArray.length) {
            int j = messagePattern.indexOf(DELIM_STR, i);
            if (j != -1) {
                if (!isEscapedDelimeter(messagePattern, j)) {
                    sbuf.append(messagePattern, i, j);
                    deeplyAppendParameter(sbuf, argArray[L], new java.util.HashMap());
                    i = j + 2;
                } else if (!isDoubleEscaped(messagePattern, j)) {
                    L--;
                    sbuf.append(messagePattern, i, j - 1);
                    sbuf.append('{');
                    i = j + 1;
                } else {
                    sbuf.append(messagePattern, i, j - 1);
                    deeplyAppendParameter(sbuf, argArray[L], new java.util.HashMap());
                    i = j + 2;
                }
                L++;
            } else if (i == 0) {
                return new org.slf4j.helpers.FormattingTuple(messagePattern, argArray, throwable);
            } else {
                sbuf.append(messagePattern, i, messagePattern.length());
                return new org.slf4j.helpers.FormattingTuple(sbuf.toString(), argArray, throwable);
            }
        }
        sbuf.append(messagePattern, i, messagePattern.length());
        return new org.slf4j.helpers.FormattingTuple(sbuf.toString(), argArray, throwable);
    }

    static final boolean isEscapedDelimeter(java.lang.String messagePattern, int delimeterStartIndex) {
        if (delimeterStartIndex != 0 && messagePattern.charAt(delimeterStartIndex - 1) == '\\') {
            return true;
        }
        return false;
    }

    static final boolean isDoubleEscaped(java.lang.String messagePattern, int delimeterStartIndex) {
        if (delimeterStartIndex < 2 || messagePattern.charAt(delimeterStartIndex - 2) != '\\') {
            return false;
        }
        return true;
    }

    private static void deeplyAppendParameter(java.lang.StringBuilder sbuf, java.lang.Object o, java.util.Map<java.lang.Object[], java.lang.Object> seenMap) {
        if (o == null) {
            sbuf.append("null");
        } else if (!o.getClass().isArray()) {
            safeObjectAppend(sbuf, o);
        } else if (o instanceof boolean[]) {
            booleanArrayAppend(sbuf, (boolean[]) o);
        } else if (o instanceof byte[]) {
            byteArrayAppend(sbuf, (byte[]) o);
        } else if (o instanceof char[]) {
            charArrayAppend(sbuf, (char[]) o);
        } else if (o instanceof short[]) {
            shortArrayAppend(sbuf, (short[]) o);
        } else if (o instanceof int[]) {
            intArrayAppend(sbuf, (int[]) o);
        } else if (o instanceof long[]) {
            longArrayAppend(sbuf, (long[]) o);
        } else if (o instanceof float[]) {
            floatArrayAppend(sbuf, (float[]) o);
        } else if (o instanceof double[]) {
            doubleArrayAppend(sbuf, (double[]) o);
        } else {
            objectArrayAppend(sbuf, (java.lang.Object[]) o, seenMap);
        }
    }

    private static void safeObjectAppend(java.lang.StringBuilder sbuf, java.lang.Object o) {
        try {
            sbuf.append(o.toString());
        } catch (Throwable t) {
            org.slf4j.helpers.Util.report("SLF4J: Failed toString() invocation on an object of type [" + o.getClass().getName() + "]", t);
            sbuf.append("[FAILED toString()]");
        }
    }

    private static void objectArrayAppend(java.lang.StringBuilder sbuf, java.lang.Object[] a, java.util.Map<java.lang.Object[], java.lang.Object> seenMap) {
        sbuf.append('[');
        if (!seenMap.containsKey(a)) {
            seenMap.put(a, null);
            int len = a.length;
            for (int i = 0; i < len; i++) {
                deeplyAppendParameter(sbuf, a[i], seenMap);
                if (i != len - 1) {
                    sbuf.append(", ");
                }
            }
            seenMap.remove(a);
        } else {
            sbuf.append("...");
        }
        sbuf.append(']');
    }

    private static void booleanArrayAppend(java.lang.StringBuilder sbuf, boolean[] a) {
        sbuf.append('[');
        int len = a.length;
        for (int i = 0; i < len; i++) {
            sbuf.append(a[i]);
            if (i != len - 1) {
                sbuf.append(", ");
            }
        }
        sbuf.append(']');
    }

    private static void byteArrayAppend(java.lang.StringBuilder sbuf, byte[] a) {
        sbuf.append('[');
        int len = a.length;
        for (int i = 0; i < len; i++) {
            sbuf.append(a[i]);
            if (i != len - 1) {
                sbuf.append(", ");
            }
        }
        sbuf.append(']');
    }

    private static void charArrayAppend(java.lang.StringBuilder sbuf, char[] a) {
        sbuf.append('[');
        int len = a.length;
        for (int i = 0; i < len; i++) {
            sbuf.append(a[i]);
            if (i != len - 1) {
                sbuf.append(", ");
            }
        }
        sbuf.append(']');
    }

    private static void shortArrayAppend(java.lang.StringBuilder sbuf, short[] a) {
        sbuf.append('[');
        int len = a.length;
        for (int i = 0; i < len; i++) {
            sbuf.append(a[i]);
            if (i != len - 1) {
                sbuf.append(", ");
            }
        }
        sbuf.append(']');
    }

    private static void intArrayAppend(java.lang.StringBuilder sbuf, int[] a) {
        sbuf.append('[');
        int len = a.length;
        for (int i = 0; i < len; i++) {
            sbuf.append(a[i]);
            if (i != len - 1) {
                sbuf.append(", ");
            }
        }
        sbuf.append(']');
    }

    private static void longArrayAppend(java.lang.StringBuilder sbuf, long[] a) {
        sbuf.append('[');
        int len = a.length;
        for (int i = 0; i < len; i++) {
            sbuf.append(a[i]);
            if (i != len - 1) {
                sbuf.append(", ");
            }
        }
        sbuf.append(']');
    }

    private static void floatArrayAppend(java.lang.StringBuilder sbuf, float[] a) {
        sbuf.append('[');
        int len = a.length;
        for (int i = 0; i < len; i++) {
            sbuf.append(a[i]);
            if (i != len - 1) {
                sbuf.append(", ");
            }
        }
        sbuf.append(']');
    }

    private static void doubleArrayAppend(java.lang.StringBuilder sbuf, double[] a) {
        sbuf.append('[');
        int len = a.length;
        for (int i = 0; i < len; i++) {
            sbuf.append(a[i]);
            if (i != len - 1) {
                sbuf.append(", ");
            }
        }
        sbuf.append(']');
    }
}
