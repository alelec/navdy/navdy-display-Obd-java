package org.slf4j.helpers;

public final class Util {
    private static org.slf4j.helpers.Util.ClassContextSecurityManager SECURITY_MANAGER;
    private static boolean SECURITY_MANAGER_CREATION_ALREADY_ATTEMPTED = false;

    private static final class ClassContextSecurityManager extends java.lang.SecurityManager {
        private ClassContextSecurityManager() {
        }

        /* access modifiers changed from: protected */
        public java.lang.Class<?>[] getClassContext() {
            return super.getClassContext();
        }
    }

    private Util() {
    }

    public static java.lang.String safeGetSystemProperty(java.lang.String key) {
        if (key == null) {
            throw new java.lang.IllegalArgumentException("null input");
        }
        java.lang.String result = null;
        try {
            return java.lang.System.getProperty(key);
        } catch (java.lang.SecurityException e) {
            return result;
        }
    }

    public static boolean safeGetBooleanSystemProperty(java.lang.String key) {
        java.lang.String value = safeGetSystemProperty(key);
        if (value == null) {
            return false;
        }
        return value.equalsIgnoreCase("true");
    }

    private static org.slf4j.helpers.Util.ClassContextSecurityManager getSecurityManager() {
        if (SECURITY_MANAGER != null) {
            return SECURITY_MANAGER;
        }
        if (SECURITY_MANAGER_CREATION_ALREADY_ATTEMPTED) {
            return null;
        }
        SECURITY_MANAGER = safeCreateSecurityManager();
        SECURITY_MANAGER_CREATION_ALREADY_ATTEMPTED = true;
        return SECURITY_MANAGER;
    }

    private static org.slf4j.helpers.Util.ClassContextSecurityManager safeCreateSecurityManager() {
        try {
            return new org.slf4j.helpers.Util.ClassContextSecurityManager();
        } catch (java.lang.SecurityException e) {
            return null;
        }
    }

    public static java.lang.Class<?> getCallingClass() {
        org.slf4j.helpers.Util.ClassContextSecurityManager securityManager = getSecurityManager();
        if (securityManager == null) {
            return null;
        }
        java.lang.Class<?>[] trace = securityManager.getClassContext();
        java.lang.String thisClassName = org.slf4j.helpers.Util.class.getName();
        int i = 0;
        while (i < trace.length && !thisClassName.equals(trace[i].getName())) {
            i++;
        }
        if (i < trace.length && i + 2 < trace.length) {
            return trace[i + 2];
        }
        throw new java.lang.IllegalStateException("Failed to find org.slf4j.helpers.Util or its caller in the stack; this should not happen");
    }

    public static final void report(java.lang.String msg, java.lang.Throwable t) {
        java.lang.System.err.println(msg);
        java.lang.System.err.println("Reported exception:");
        t.printStackTrace();
    }

    public static final void report(java.lang.String msg) {
        java.lang.System.err.println("SLF4J: " + msg);
    }
}
