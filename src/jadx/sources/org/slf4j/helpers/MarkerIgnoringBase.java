package org.slf4j.helpers;

public abstract class MarkerIgnoringBase extends org.slf4j.helpers.NamedLoggerBase implements org.slf4j.Logger {
    private static final long serialVersionUID = 9044267456635152283L;

    public /* bridge */ /* synthetic */ java.lang.String getName() {
        return super.getName();
    }

    public boolean isTraceEnabled(org.slf4j.Marker marker) {
        return isTraceEnabled();
    }

    public void trace(org.slf4j.Marker marker, java.lang.String msg) {
        trace(msg);
    }

    public void trace(org.slf4j.Marker marker, java.lang.String format, java.lang.Object arg) {
        trace(format, arg);
    }

    public void trace(org.slf4j.Marker marker, java.lang.String format, java.lang.Object arg1, java.lang.Object arg2) {
        trace(format, arg1, arg2);
    }

    public void trace(org.slf4j.Marker marker, java.lang.String format, java.lang.Object... arguments) {
        trace(format, arguments);
    }

    public void trace(org.slf4j.Marker marker, java.lang.String msg, java.lang.Throwable t) {
        trace(msg, t);
    }

    public boolean isDebugEnabled(org.slf4j.Marker marker) {
        return isDebugEnabled();
    }

    public void debug(org.slf4j.Marker marker, java.lang.String msg) {
        debug(msg);
    }

    public void debug(org.slf4j.Marker marker, java.lang.String format, java.lang.Object arg) {
        debug(format, arg);
    }

    public void debug(org.slf4j.Marker marker, java.lang.String format, java.lang.Object arg1, java.lang.Object arg2) {
        debug(format, arg1, arg2);
    }

    public void debug(org.slf4j.Marker marker, java.lang.String format, java.lang.Object... arguments) {
        debug(format, arguments);
    }

    public void debug(org.slf4j.Marker marker, java.lang.String msg, java.lang.Throwable t) {
        debug(msg, t);
    }

    public boolean isInfoEnabled(org.slf4j.Marker marker) {
        return isInfoEnabled();
    }

    public void info(org.slf4j.Marker marker, java.lang.String msg) {
        info(msg);
    }

    public void info(org.slf4j.Marker marker, java.lang.String format, java.lang.Object arg) {
        info(format, arg);
    }

    public void info(org.slf4j.Marker marker, java.lang.String format, java.lang.Object arg1, java.lang.Object arg2) {
        info(format, arg1, arg2);
    }

    public void info(org.slf4j.Marker marker, java.lang.String format, java.lang.Object... arguments) {
        info(format, arguments);
    }

    public void info(org.slf4j.Marker marker, java.lang.String msg, java.lang.Throwable t) {
        info(msg, t);
    }

    public boolean isWarnEnabled(org.slf4j.Marker marker) {
        return isWarnEnabled();
    }

    public void warn(org.slf4j.Marker marker, java.lang.String msg) {
        warn(msg);
    }

    public void warn(org.slf4j.Marker marker, java.lang.String format, java.lang.Object arg) {
        warn(format, arg);
    }

    public void warn(org.slf4j.Marker marker, java.lang.String format, java.lang.Object arg1, java.lang.Object arg2) {
        warn(format, arg1, arg2);
    }

    public void warn(org.slf4j.Marker marker, java.lang.String format, java.lang.Object... arguments) {
        warn(format, arguments);
    }

    public void warn(org.slf4j.Marker marker, java.lang.String msg, java.lang.Throwable t) {
        warn(msg, t);
    }

    public boolean isErrorEnabled(org.slf4j.Marker marker) {
        return isErrorEnabled();
    }

    public void error(org.slf4j.Marker marker, java.lang.String msg) {
        error(msg);
    }

    public void error(org.slf4j.Marker marker, java.lang.String format, java.lang.Object arg) {
        error(format, arg);
    }

    public void error(org.slf4j.Marker marker, java.lang.String format, java.lang.Object arg1, java.lang.Object arg2) {
        error(format, arg1, arg2);
    }

    public void error(org.slf4j.Marker marker, java.lang.String format, java.lang.Object... arguments) {
        error(format, arguments);
    }

    public void error(org.slf4j.Marker marker, java.lang.String msg, java.lang.Throwable t) {
        error(msg, t);
    }

    public java.lang.String toString() {
        return getClass().getName() + "(" + getName() + ")";
    }
}
