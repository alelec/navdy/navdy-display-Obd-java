package org.slf4j.helpers;

public class FormattingTuple {
    public static org.slf4j.helpers.FormattingTuple NULL = new org.slf4j.helpers.FormattingTuple(null);
    private java.lang.Object[] argArray;
    private java.lang.String message;
    private java.lang.Throwable throwable;

    public FormattingTuple(java.lang.String message2) {
        this(message2, null, null);
    }

    public FormattingTuple(java.lang.String message2, java.lang.Object[] argArray2, java.lang.Throwable throwable2) {
        this.message = message2;
        this.throwable = throwable2;
        this.argArray = argArray2;
    }

    public java.lang.String getMessage() {
        return this.message;
    }

    public java.lang.Object[] getArgArray() {
        return this.argArray;
    }

    public java.lang.Throwable getThrowable() {
        return this.throwable;
    }
}
