package org.slf4j.helpers;

public class NOPMDCAdapter implements org.slf4j.spi.MDCAdapter {
    public void clear() {
    }

    public java.lang.String get(java.lang.String key) {
        return null;
    }

    public void put(java.lang.String key, java.lang.String val) {
    }

    public void remove(java.lang.String key) {
    }

    public java.util.Map<java.lang.String, java.lang.String> getCopyOfContextMap() {
        return null;
    }

    public void setContextMap(java.util.Map<java.lang.String, java.lang.String> map) {
    }
}
