package org.slf4j.helpers;

public class SubstituteLoggerFactory implements org.slf4j.ILoggerFactory {
    final java.util.concurrent.LinkedBlockingQueue<org.slf4j.event.SubstituteLoggingEvent> eventQueue = new java.util.concurrent.LinkedBlockingQueue<>();
    final java.util.Map<java.lang.String, org.slf4j.helpers.SubstituteLogger> loggers = new java.util.HashMap();
    boolean postInitialization = false;

    public synchronized org.slf4j.Logger getLogger(java.lang.String name) {
        org.slf4j.helpers.SubstituteLogger logger;
        logger = (org.slf4j.helpers.SubstituteLogger) this.loggers.get(name);
        if (logger == null) {
            logger = new org.slf4j.helpers.SubstituteLogger(name, this.eventQueue, this.postInitialization);
            this.loggers.put(name, logger);
        }
        return logger;
    }

    public java.util.List<java.lang.String> getLoggerNames() {
        return new java.util.ArrayList(this.loggers.keySet());
    }

    public java.util.List<org.slf4j.helpers.SubstituteLogger> getLoggers() {
        return new java.util.ArrayList(this.loggers.values());
    }

    public java.util.concurrent.LinkedBlockingQueue<org.slf4j.event.SubstituteLoggingEvent> getEventQueue() {
        return this.eventQueue;
    }

    public void postInitialization() {
        this.postInitialization = true;
    }

    public void clear() {
        this.loggers.clear();
        this.eventQueue.clear();
    }
}
