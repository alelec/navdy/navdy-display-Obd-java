package org.slf4j.helpers;

public class SubstituteLogger implements org.slf4j.Logger {
    private volatile org.slf4j.Logger _delegate;
    private final boolean createdPostInitialization;
    private java.lang.Boolean delegateEventAware;
    private java.util.Queue<org.slf4j.event.SubstituteLoggingEvent> eventQueue;
    private org.slf4j.event.EventRecodingLogger eventRecodingLogger;
    private java.lang.reflect.Method logMethodCache;
    private final java.lang.String name;

    public SubstituteLogger(java.lang.String name2, java.util.Queue<org.slf4j.event.SubstituteLoggingEvent> eventQueue2, boolean createdPostInitialization2) {
        this.name = name2;
        this.eventQueue = eventQueue2;
        this.createdPostInitialization = createdPostInitialization2;
    }

    public java.lang.String getName() {
        return this.name;
    }

    public boolean isTraceEnabled() {
        return delegate().isTraceEnabled();
    }

    public void trace(java.lang.String msg) {
        delegate().trace(msg);
    }

    public void trace(java.lang.String format, java.lang.Object arg) {
        delegate().trace(format, arg);
    }

    public void trace(java.lang.String format, java.lang.Object arg1, java.lang.Object arg2) {
        delegate().trace(format, arg1, arg2);
    }

    public void trace(java.lang.String format, java.lang.Object... arguments) {
        delegate().trace(format, arguments);
    }

    public void trace(java.lang.String msg, java.lang.Throwable t) {
        delegate().trace(msg, t);
    }

    public boolean isTraceEnabled(org.slf4j.Marker marker) {
        return delegate().isTraceEnabled(marker);
    }

    public void trace(org.slf4j.Marker marker, java.lang.String msg) {
        delegate().trace(marker, msg);
    }

    public void trace(org.slf4j.Marker marker, java.lang.String format, java.lang.Object arg) {
        delegate().trace(marker, format, arg);
    }

    public void trace(org.slf4j.Marker marker, java.lang.String format, java.lang.Object arg1, java.lang.Object arg2) {
        delegate().trace(marker, format, arg1, arg2);
    }

    public void trace(org.slf4j.Marker marker, java.lang.String format, java.lang.Object... arguments) {
        delegate().trace(marker, format, arguments);
    }

    public void trace(org.slf4j.Marker marker, java.lang.String msg, java.lang.Throwable t) {
        delegate().trace(marker, msg, t);
    }

    public boolean isDebugEnabled() {
        return delegate().isDebugEnabled();
    }

    public void debug(java.lang.String msg) {
        delegate().debug(msg);
    }

    public void debug(java.lang.String format, java.lang.Object arg) {
        delegate().debug(format, arg);
    }

    public void debug(java.lang.String format, java.lang.Object arg1, java.lang.Object arg2) {
        delegate().debug(format, arg1, arg2);
    }

    public void debug(java.lang.String format, java.lang.Object... arguments) {
        delegate().debug(format, arguments);
    }

    public void debug(java.lang.String msg, java.lang.Throwable t) {
        delegate().debug(msg, t);
    }

    public boolean isDebugEnabled(org.slf4j.Marker marker) {
        return delegate().isDebugEnabled(marker);
    }

    public void debug(org.slf4j.Marker marker, java.lang.String msg) {
        delegate().debug(marker, msg);
    }

    public void debug(org.slf4j.Marker marker, java.lang.String format, java.lang.Object arg) {
        delegate().debug(marker, format, arg);
    }

    public void debug(org.slf4j.Marker marker, java.lang.String format, java.lang.Object arg1, java.lang.Object arg2) {
        delegate().debug(marker, format, arg1, arg2);
    }

    public void debug(org.slf4j.Marker marker, java.lang.String format, java.lang.Object... arguments) {
        delegate().debug(marker, format, arguments);
    }

    public void debug(org.slf4j.Marker marker, java.lang.String msg, java.lang.Throwable t) {
        delegate().debug(marker, msg, t);
    }

    public boolean isInfoEnabled() {
        return delegate().isInfoEnabled();
    }

    public void info(java.lang.String msg) {
        delegate().info(msg);
    }

    public void info(java.lang.String format, java.lang.Object arg) {
        delegate().info(format, arg);
    }

    public void info(java.lang.String format, java.lang.Object arg1, java.lang.Object arg2) {
        delegate().info(format, arg1, arg2);
    }

    public void info(java.lang.String format, java.lang.Object... arguments) {
        delegate().info(format, arguments);
    }

    public void info(java.lang.String msg, java.lang.Throwable t) {
        delegate().info(msg, t);
    }

    public boolean isInfoEnabled(org.slf4j.Marker marker) {
        return delegate().isInfoEnabled(marker);
    }

    public void info(org.slf4j.Marker marker, java.lang.String msg) {
        delegate().info(marker, msg);
    }

    public void info(org.slf4j.Marker marker, java.lang.String format, java.lang.Object arg) {
        delegate().info(marker, format, arg);
    }

    public void info(org.slf4j.Marker marker, java.lang.String format, java.lang.Object arg1, java.lang.Object arg2) {
        delegate().info(marker, format, arg1, arg2);
    }

    public void info(org.slf4j.Marker marker, java.lang.String format, java.lang.Object... arguments) {
        delegate().info(marker, format, arguments);
    }

    public void info(org.slf4j.Marker marker, java.lang.String msg, java.lang.Throwable t) {
        delegate().info(marker, msg, t);
    }

    public boolean isWarnEnabled() {
        return delegate().isWarnEnabled();
    }

    public void warn(java.lang.String msg) {
        delegate().warn(msg);
    }

    public void warn(java.lang.String format, java.lang.Object arg) {
        delegate().warn(format, arg);
    }

    public void warn(java.lang.String format, java.lang.Object arg1, java.lang.Object arg2) {
        delegate().warn(format, arg1, arg2);
    }

    public void warn(java.lang.String format, java.lang.Object... arguments) {
        delegate().warn(format, arguments);
    }

    public void warn(java.lang.String msg, java.lang.Throwable t) {
        delegate().warn(msg, t);
    }

    public boolean isWarnEnabled(org.slf4j.Marker marker) {
        return delegate().isWarnEnabled(marker);
    }

    public void warn(org.slf4j.Marker marker, java.lang.String msg) {
        delegate().warn(marker, msg);
    }

    public void warn(org.slf4j.Marker marker, java.lang.String format, java.lang.Object arg) {
        delegate().warn(marker, format, arg);
    }

    public void warn(org.slf4j.Marker marker, java.lang.String format, java.lang.Object arg1, java.lang.Object arg2) {
        delegate().warn(marker, format, arg1, arg2);
    }

    public void warn(org.slf4j.Marker marker, java.lang.String format, java.lang.Object... arguments) {
        delegate().warn(marker, format, arguments);
    }

    public void warn(org.slf4j.Marker marker, java.lang.String msg, java.lang.Throwable t) {
        delegate().warn(marker, msg, t);
    }

    public boolean isErrorEnabled() {
        return delegate().isErrorEnabled();
    }

    public void error(java.lang.String msg) {
        delegate().error(msg);
    }

    public void error(java.lang.String format, java.lang.Object arg) {
        delegate().error(format, arg);
    }

    public void error(java.lang.String format, java.lang.Object arg1, java.lang.Object arg2) {
        delegate().error(format, arg1, arg2);
    }

    public void error(java.lang.String format, java.lang.Object... arguments) {
        delegate().error(format, arguments);
    }

    public void error(java.lang.String msg, java.lang.Throwable t) {
        delegate().error(msg, t);
    }

    public boolean isErrorEnabled(org.slf4j.Marker marker) {
        return delegate().isErrorEnabled(marker);
    }

    public void error(org.slf4j.Marker marker, java.lang.String msg) {
        delegate().error(marker, msg);
    }

    public void error(org.slf4j.Marker marker, java.lang.String format, java.lang.Object arg) {
        delegate().error(marker, format, arg);
    }

    public void error(org.slf4j.Marker marker, java.lang.String format, java.lang.Object arg1, java.lang.Object arg2) {
        delegate().error(marker, format, arg1, arg2);
    }

    public void error(org.slf4j.Marker marker, java.lang.String format, java.lang.Object... arguments) {
        delegate().error(marker, format, arguments);
    }

    public void error(org.slf4j.Marker marker, java.lang.String msg, java.lang.Throwable t) {
        delegate().error(marker, msg, t);
    }

    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!this.name.equals(((org.slf4j.helpers.SubstituteLogger) o).name)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return this.name.hashCode();
    }

    /* access modifiers changed from: 0000 */
    public org.slf4j.Logger delegate() {
        if (this._delegate != null) {
            return this._delegate;
        }
        if (this.createdPostInitialization) {
            return org.slf4j.helpers.NOPLogger.NOP_LOGGER;
        }
        return getEventRecordingLogger();
    }

    private org.slf4j.Logger getEventRecordingLogger() {
        if (this.eventRecodingLogger == null) {
            this.eventRecodingLogger = new org.slf4j.event.EventRecodingLogger(this, this.eventQueue);
        }
        return this.eventRecodingLogger;
    }

    public void setDelegate(org.slf4j.Logger delegate) {
        this._delegate = delegate;
    }

    public boolean isDelegateEventAware() {
        if (this.delegateEventAware != null) {
            return this.delegateEventAware.booleanValue();
        }
        try {
            this.logMethodCache = this._delegate.getClass().getMethod("log", new java.lang.Class[]{org.slf4j.event.LoggingEvent.class});
            this.delegateEventAware = java.lang.Boolean.TRUE;
        } catch (java.lang.NoSuchMethodException e) {
            this.delegateEventAware = java.lang.Boolean.FALSE;
        }
        return this.delegateEventAware.booleanValue();
    }

    public void log(org.slf4j.event.LoggingEvent event) {
        if (isDelegateEventAware()) {
            try {
                this.logMethodCache.invoke(this._delegate, new java.lang.Object[]{event});
            } catch (java.lang.IllegalAccessException | java.lang.IllegalArgumentException | java.lang.reflect.InvocationTargetException e) {
            }
        }
    }

    public boolean isDelegateNull() {
        return this._delegate == null;
    }

    public boolean isDelegateNOP() {
        return this._delegate instanceof org.slf4j.helpers.NOPLogger;
    }
}
