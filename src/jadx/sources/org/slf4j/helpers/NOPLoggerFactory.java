package org.slf4j.helpers;

public class NOPLoggerFactory implements org.slf4j.ILoggerFactory {
    public org.slf4j.Logger getLogger(java.lang.String name) {
        return org.slf4j.helpers.NOPLogger.NOP_LOGGER;
    }
}
