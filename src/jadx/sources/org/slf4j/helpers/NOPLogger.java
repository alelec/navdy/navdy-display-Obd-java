package org.slf4j.helpers;

public class NOPLogger extends org.slf4j.helpers.MarkerIgnoringBase {
    public static final org.slf4j.helpers.NOPLogger NOP_LOGGER = new org.slf4j.helpers.NOPLogger();
    private static final long serialVersionUID = -517220405410904473L;

    protected NOPLogger() {
    }

    public java.lang.String getName() {
        return "NOP";
    }

    public final boolean isTraceEnabled() {
        return false;
    }

    public final void trace(java.lang.String msg) {
    }

    public final void trace(java.lang.String format, java.lang.Object arg) {
    }

    public final void trace(java.lang.String format, java.lang.Object arg1, java.lang.Object arg2) {
    }

    public final void trace(java.lang.String format, java.lang.Object... argArray) {
    }

    public final void trace(java.lang.String msg, java.lang.Throwable t) {
    }

    public final boolean isDebugEnabled() {
        return false;
    }

    public final void debug(java.lang.String msg) {
    }

    public final void debug(java.lang.String format, java.lang.Object arg) {
    }

    public final void debug(java.lang.String format, java.lang.Object arg1, java.lang.Object arg2) {
    }

    public final void debug(java.lang.String format, java.lang.Object... argArray) {
    }

    public final void debug(java.lang.String msg, java.lang.Throwable t) {
    }

    public final boolean isInfoEnabled() {
        return false;
    }

    public final void info(java.lang.String msg) {
    }

    public final void info(java.lang.String format, java.lang.Object arg1) {
    }

    public final void info(java.lang.String format, java.lang.Object arg1, java.lang.Object arg2) {
    }

    public final void info(java.lang.String format, java.lang.Object... argArray) {
    }

    public final void info(java.lang.String msg, java.lang.Throwable t) {
    }

    public final boolean isWarnEnabled() {
        return false;
    }

    public final void warn(java.lang.String msg) {
    }

    public final void warn(java.lang.String format, java.lang.Object arg1) {
    }

    public final void warn(java.lang.String format, java.lang.Object arg1, java.lang.Object arg2) {
    }

    public final void warn(java.lang.String format, java.lang.Object... argArray) {
    }

    public final void warn(java.lang.String msg, java.lang.Throwable t) {
    }

    public final boolean isErrorEnabled() {
        return false;
    }

    public final void error(java.lang.String msg) {
    }

    public final void error(java.lang.String format, java.lang.Object arg1) {
    }

    public final void error(java.lang.String format, java.lang.Object arg1, java.lang.Object arg2) {
    }

    public final void error(java.lang.String format, java.lang.Object... argArray) {
    }

    public final void error(java.lang.String msg, java.lang.Throwable t) {
    }
}
