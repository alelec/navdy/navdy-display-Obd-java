package org.slf4j.spi;

public interface MDCAdapter {
    void clear();

    java.lang.String get(java.lang.String str);

    java.util.Map<java.lang.String, java.lang.String> getCopyOfContextMap();

    void put(java.lang.String str, java.lang.String str2);

    void remove(java.lang.String str);

    void setContextMap(java.util.Map<java.lang.String, java.lang.String> map);
}
