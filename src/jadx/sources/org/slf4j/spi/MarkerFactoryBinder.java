package org.slf4j.spi;

public interface MarkerFactoryBinder {
    org.slf4j.IMarkerFactory getMarkerFactory();

    java.lang.String getMarkerFactoryClassStr();
}
