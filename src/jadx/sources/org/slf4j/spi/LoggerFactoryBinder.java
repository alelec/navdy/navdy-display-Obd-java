package org.slf4j.spi;

public interface LoggerFactoryBinder {
    org.slf4j.ILoggerFactory getLoggerFactory();

    java.lang.String getLoggerFactoryClassStr();
}
