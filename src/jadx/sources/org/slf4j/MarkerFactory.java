package org.slf4j;

public class MarkerFactory {
    static org.slf4j.IMarkerFactory MARKER_FACTORY;

    private MarkerFactory() {
    }

    private static org.slf4j.IMarkerFactory bwCompatibleGetMarkerFactoryFromBinder() throws java.lang.NoClassDefFoundError {
        try {
            return org.slf4j.impl.StaticMarkerBinder.getSingleton().getMarkerFactory();
        } catch (java.lang.NoSuchMethodError e) {
            return org.slf4j.impl.StaticMarkerBinder.SINGLETON.getMarkerFactory();
        }
    }

    static {
        try {
            MARKER_FACTORY = bwCompatibleGetMarkerFactoryFromBinder();
        } catch (java.lang.NoClassDefFoundError e) {
            MARKER_FACTORY = new org.slf4j.helpers.BasicMarkerFactory();
        } catch (java.lang.Exception e2) {
            org.slf4j.helpers.Util.report("Unexpected failure while binding MarkerFactory", e2);
        }
    }

    public static org.slf4j.Marker getMarker(java.lang.String name) {
        return MARKER_FACTORY.getMarker(name);
    }

    public static org.slf4j.Marker getDetachedMarker(java.lang.String name) {
        return MARKER_FACTORY.getDetachedMarker(name);
    }

    public static org.slf4j.IMarkerFactory getIMarkerFactory() {
        return MARKER_FACTORY;
    }
}
