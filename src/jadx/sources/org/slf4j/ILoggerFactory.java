package org.slf4j;

public interface ILoggerFactory {
    org.slf4j.Logger getLogger(java.lang.String str);
}
