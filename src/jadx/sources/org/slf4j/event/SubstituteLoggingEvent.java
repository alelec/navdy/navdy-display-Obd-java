package org.slf4j.event;

public class SubstituteLoggingEvent implements org.slf4j.event.LoggingEvent {
    java.lang.Object[] argArray;
    org.slf4j.event.Level level;
    org.slf4j.helpers.SubstituteLogger logger;
    java.lang.String loggerName;
    org.slf4j.Marker marker;
    java.lang.String message;
    java.lang.String threadName;
    java.lang.Throwable throwable;
    long timeStamp;

    public org.slf4j.event.Level getLevel() {
        return this.level;
    }

    public void setLevel(org.slf4j.event.Level level2) {
        this.level = level2;
    }

    public org.slf4j.Marker getMarker() {
        return this.marker;
    }

    public void setMarker(org.slf4j.Marker marker2) {
        this.marker = marker2;
    }

    public java.lang.String getLoggerName() {
        return this.loggerName;
    }

    public void setLoggerName(java.lang.String loggerName2) {
        this.loggerName = loggerName2;
    }

    public org.slf4j.helpers.SubstituteLogger getLogger() {
        return this.logger;
    }

    public void setLogger(org.slf4j.helpers.SubstituteLogger logger2) {
        this.logger = logger2;
    }

    public java.lang.String getMessage() {
        return this.message;
    }

    public void setMessage(java.lang.String message2) {
        this.message = message2;
    }

    public java.lang.Object[] getArgumentArray() {
        return this.argArray;
    }

    public void setArgumentArray(java.lang.Object[] argArray2) {
        this.argArray = argArray2;
    }

    public long getTimeStamp() {
        return this.timeStamp;
    }

    public void setTimeStamp(long timeStamp2) {
        this.timeStamp = timeStamp2;
    }

    public java.lang.String getThreadName() {
        return this.threadName;
    }

    public void setThreadName(java.lang.String threadName2) {
        this.threadName = threadName2;
    }

    public java.lang.Throwable getThrowable() {
        return this.throwable;
    }

    public void setThrowable(java.lang.Throwable throwable2) {
        this.throwable = throwable2;
    }
}
