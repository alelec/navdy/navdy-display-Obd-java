package org.slf4j.event;

public class EventRecodingLogger implements org.slf4j.Logger {
    java.util.Queue<org.slf4j.event.SubstituteLoggingEvent> eventQueue;
    org.slf4j.helpers.SubstituteLogger logger;
    java.lang.String name;

    public EventRecodingLogger(org.slf4j.helpers.SubstituteLogger logger2, java.util.Queue<org.slf4j.event.SubstituteLoggingEvent> eventQueue2) {
        this.logger = logger2;
        this.name = logger2.getName();
        this.eventQueue = eventQueue2;
    }

    public java.lang.String getName() {
        return this.name;
    }

    private void recordEvent(org.slf4j.event.Level level, java.lang.String msg, java.lang.Object[] args, java.lang.Throwable throwable) {
        recordEvent(level, null, msg, args, throwable);
    }

    private void recordEvent(org.slf4j.event.Level level, org.slf4j.Marker marker, java.lang.String msg, java.lang.Object[] args, java.lang.Throwable throwable) {
        org.slf4j.event.SubstituteLoggingEvent loggingEvent = new org.slf4j.event.SubstituteLoggingEvent();
        loggingEvent.setTimeStamp(java.lang.System.currentTimeMillis());
        loggingEvent.setLevel(level);
        loggingEvent.setLogger(this.logger);
        loggingEvent.setLoggerName(this.name);
        loggingEvent.setMessage(msg);
        loggingEvent.setArgumentArray(args);
        loggingEvent.setThrowable(throwable);
        loggingEvent.setThreadName(java.lang.Thread.currentThread().getName());
        this.eventQueue.add(loggingEvent);
    }

    public boolean isTraceEnabled() {
        return true;
    }

    public void trace(java.lang.String msg) {
        recordEvent(org.slf4j.event.Level.TRACE, msg, null, null);
    }

    public void trace(java.lang.String format, java.lang.Object arg) {
        recordEvent(org.slf4j.event.Level.TRACE, format, new java.lang.Object[]{arg}, null);
    }

    public void trace(java.lang.String format, java.lang.Object arg1, java.lang.Object arg2) {
        recordEvent(org.slf4j.event.Level.TRACE, format, new java.lang.Object[]{arg1, arg2}, null);
    }

    public void trace(java.lang.String format, java.lang.Object... arguments) {
        recordEvent(org.slf4j.event.Level.TRACE, format, arguments, null);
    }

    public void trace(java.lang.String msg, java.lang.Throwable t) {
        recordEvent(org.slf4j.event.Level.TRACE, msg, null, t);
    }

    public boolean isTraceEnabled(org.slf4j.Marker marker) {
        return true;
    }

    public void trace(org.slf4j.Marker marker, java.lang.String msg) {
        recordEvent(org.slf4j.event.Level.TRACE, marker, msg, null, null);
    }

    public void trace(org.slf4j.Marker marker, java.lang.String format, java.lang.Object arg) {
        recordEvent(org.slf4j.event.Level.TRACE, marker, format, new java.lang.Object[]{arg}, null);
    }

    public void trace(org.slf4j.Marker marker, java.lang.String format, java.lang.Object arg1, java.lang.Object arg2) {
        recordEvent(org.slf4j.event.Level.TRACE, marker, format, new java.lang.Object[]{arg1, arg2}, null);
    }

    public void trace(org.slf4j.Marker marker, java.lang.String format, java.lang.Object... argArray) {
        recordEvent(org.slf4j.event.Level.TRACE, marker, format, argArray, null);
    }

    public void trace(org.slf4j.Marker marker, java.lang.String msg, java.lang.Throwable t) {
        recordEvent(org.slf4j.event.Level.TRACE, marker, msg, null, t);
    }

    public boolean isDebugEnabled() {
        return true;
    }

    public void debug(java.lang.String msg) {
        recordEvent(org.slf4j.event.Level.TRACE, msg, null, null);
    }

    public void debug(java.lang.String format, java.lang.Object arg) {
        recordEvent(org.slf4j.event.Level.DEBUG, format, new java.lang.Object[]{arg}, null);
    }

    public void debug(java.lang.String format, java.lang.Object arg1, java.lang.Object arg2) {
        recordEvent(org.slf4j.event.Level.DEBUG, format, new java.lang.Object[]{arg1, arg2}, null);
    }

    public void debug(java.lang.String format, java.lang.Object... arguments) {
        recordEvent(org.slf4j.event.Level.DEBUG, format, arguments, null);
    }

    public void debug(java.lang.String msg, java.lang.Throwable t) {
        recordEvent(org.slf4j.event.Level.DEBUG, msg, null, t);
    }

    public boolean isDebugEnabled(org.slf4j.Marker marker) {
        return true;
    }

    public void debug(org.slf4j.Marker marker, java.lang.String msg) {
        recordEvent(org.slf4j.event.Level.DEBUG, marker, msg, null, null);
    }

    public void debug(org.slf4j.Marker marker, java.lang.String format, java.lang.Object arg) {
        recordEvent(org.slf4j.event.Level.DEBUG, marker, format, new java.lang.Object[]{arg}, null);
    }

    public void debug(org.slf4j.Marker marker, java.lang.String format, java.lang.Object arg1, java.lang.Object arg2) {
        recordEvent(org.slf4j.event.Level.DEBUG, marker, format, new java.lang.Object[]{arg1, arg2}, null);
    }

    public void debug(org.slf4j.Marker marker, java.lang.String format, java.lang.Object... arguments) {
        recordEvent(org.slf4j.event.Level.DEBUG, marker, format, arguments, null);
    }

    public void debug(org.slf4j.Marker marker, java.lang.String msg, java.lang.Throwable t) {
        recordEvent(org.slf4j.event.Level.DEBUG, marker, msg, null, t);
    }

    public boolean isInfoEnabled() {
        return true;
    }

    public void info(java.lang.String msg) {
        recordEvent(org.slf4j.event.Level.INFO, msg, null, null);
    }

    public void info(java.lang.String format, java.lang.Object arg) {
        recordEvent(org.slf4j.event.Level.INFO, format, new java.lang.Object[]{arg}, null);
    }

    public void info(java.lang.String format, java.lang.Object arg1, java.lang.Object arg2) {
        recordEvent(org.slf4j.event.Level.INFO, format, new java.lang.Object[]{arg1, arg2}, null);
    }

    public void info(java.lang.String format, java.lang.Object... arguments) {
        recordEvent(org.slf4j.event.Level.INFO, format, arguments, null);
    }

    public void info(java.lang.String msg, java.lang.Throwable t) {
        recordEvent(org.slf4j.event.Level.INFO, msg, null, t);
    }

    public boolean isInfoEnabled(org.slf4j.Marker marker) {
        return true;
    }

    public void info(org.slf4j.Marker marker, java.lang.String msg) {
        recordEvent(org.slf4j.event.Level.INFO, marker, msg, null, null);
    }

    public void info(org.slf4j.Marker marker, java.lang.String format, java.lang.Object arg) {
        recordEvent(org.slf4j.event.Level.INFO, marker, format, new java.lang.Object[]{arg}, null);
    }

    public void info(org.slf4j.Marker marker, java.lang.String format, java.lang.Object arg1, java.lang.Object arg2) {
        recordEvent(org.slf4j.event.Level.INFO, marker, format, new java.lang.Object[]{arg1, arg2}, null);
    }

    public void info(org.slf4j.Marker marker, java.lang.String format, java.lang.Object... arguments) {
        recordEvent(org.slf4j.event.Level.INFO, marker, format, arguments, null);
    }

    public void info(org.slf4j.Marker marker, java.lang.String msg, java.lang.Throwable t) {
        recordEvent(org.slf4j.event.Level.INFO, marker, msg, null, t);
    }

    public boolean isWarnEnabled() {
        return true;
    }

    public void warn(java.lang.String msg) {
        recordEvent(org.slf4j.event.Level.WARN, msg, null, null);
    }

    public void warn(java.lang.String format, java.lang.Object arg) {
        recordEvent(org.slf4j.event.Level.WARN, format, new java.lang.Object[]{arg}, null);
    }

    public void warn(java.lang.String format, java.lang.Object arg1, java.lang.Object arg2) {
        recordEvent(org.slf4j.event.Level.WARN, format, new java.lang.Object[]{arg1, arg2}, null);
    }

    public void warn(java.lang.String format, java.lang.Object... arguments) {
        recordEvent(org.slf4j.event.Level.WARN, format, arguments, null);
    }

    public void warn(java.lang.String msg, java.lang.Throwable t) {
        recordEvent(org.slf4j.event.Level.WARN, msg, null, t);
    }

    public boolean isWarnEnabled(org.slf4j.Marker marker) {
        return true;
    }

    public void warn(org.slf4j.Marker marker, java.lang.String msg) {
        recordEvent(org.slf4j.event.Level.WARN, msg, null, null);
    }

    public void warn(org.slf4j.Marker marker, java.lang.String format, java.lang.Object arg) {
        recordEvent(org.slf4j.event.Level.WARN, format, new java.lang.Object[]{arg}, null);
    }

    public void warn(org.slf4j.Marker marker, java.lang.String format, java.lang.Object arg1, java.lang.Object arg2) {
        recordEvent(org.slf4j.event.Level.WARN, marker, format, new java.lang.Object[]{arg1, arg2}, null);
    }

    public void warn(org.slf4j.Marker marker, java.lang.String format, java.lang.Object... arguments) {
        recordEvent(org.slf4j.event.Level.WARN, marker, format, arguments, null);
    }

    public void warn(org.slf4j.Marker marker, java.lang.String msg, java.lang.Throwable t) {
        recordEvent(org.slf4j.event.Level.WARN, marker, msg, null, t);
    }

    public boolean isErrorEnabled() {
        return true;
    }

    public void error(java.lang.String msg) {
        recordEvent(org.slf4j.event.Level.ERROR, msg, null, null);
    }

    public void error(java.lang.String format, java.lang.Object arg) {
        recordEvent(org.slf4j.event.Level.ERROR, format, new java.lang.Object[]{arg}, null);
    }

    public void error(java.lang.String format, java.lang.Object arg1, java.lang.Object arg2) {
        recordEvent(org.slf4j.event.Level.ERROR, format, new java.lang.Object[]{arg1, arg2}, null);
    }

    public void error(java.lang.String format, java.lang.Object... arguments) {
        recordEvent(org.slf4j.event.Level.ERROR, format, arguments, null);
    }

    public void error(java.lang.String msg, java.lang.Throwable t) {
        recordEvent(org.slf4j.event.Level.ERROR, msg, null, t);
    }

    public boolean isErrorEnabled(org.slf4j.Marker marker) {
        return true;
    }

    public void error(org.slf4j.Marker marker, java.lang.String msg) {
        recordEvent(org.slf4j.event.Level.ERROR, marker, msg, null, null);
    }

    public void error(org.slf4j.Marker marker, java.lang.String format, java.lang.Object arg) {
        recordEvent(org.slf4j.event.Level.ERROR, marker, format, new java.lang.Object[]{arg}, null);
    }

    public void error(org.slf4j.Marker marker, java.lang.String format, java.lang.Object arg1, java.lang.Object arg2) {
        recordEvent(org.slf4j.event.Level.ERROR, marker, format, new java.lang.Object[]{arg1, arg2}, null);
    }

    public void error(org.slf4j.Marker marker, java.lang.String format, java.lang.Object... arguments) {
        recordEvent(org.slf4j.event.Level.ERROR, marker, format, arguments, null);
    }

    public void error(org.slf4j.Marker marker, java.lang.String msg, java.lang.Throwable t) {
        recordEvent(org.slf4j.event.Level.ERROR, marker, msg, null, t);
    }
}
