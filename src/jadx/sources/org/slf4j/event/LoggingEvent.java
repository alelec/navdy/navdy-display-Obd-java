package org.slf4j.event;

public interface LoggingEvent {
    java.lang.Object[] getArgumentArray();

    org.slf4j.event.Level getLevel();

    java.lang.String getLoggerName();

    org.slf4j.Marker getMarker();

    java.lang.String getMessage();

    java.lang.String getThreadName();

    java.lang.Throwable getThrowable();

    long getTimeStamp();
}
