package org.slf4j;

public class MDC {
    static final java.lang.String NO_STATIC_MDC_BINDER_URL = "http://www.slf4j.org/codes.html#no_static_mdc_binder";
    static final java.lang.String NULL_MDCA_URL = "http://www.slf4j.org/codes.html#null_MDCA";
    static org.slf4j.spi.MDCAdapter mdcAdapter;

    public static class MDCCloseable implements java.io.Closeable {
        private final java.lang.String key;

        private MDCCloseable(java.lang.String key2) {
            this.key = key2;
        }

        public void close() {
            org.slf4j.MDC.remove(this.key);
        }
    }

    private MDC() {
    }

    private static org.slf4j.spi.MDCAdapter bwCompatibleGetMDCAdapterFromBinder() throws java.lang.NoClassDefFoundError {
        try {
            return org.slf4j.impl.StaticMDCBinder.getSingleton().getMDCA();
        } catch (java.lang.NoSuchMethodError e) {
            return org.slf4j.impl.StaticMDCBinder.SINGLETON.getMDCA();
        }
    }

    static {
        try {
            mdcAdapter = bwCompatibleGetMDCAdapterFromBinder();
        } catch (java.lang.NoClassDefFoundError ncde) {
            mdcAdapter = new org.slf4j.helpers.NOPMDCAdapter();
            java.lang.String msg = ncde.getMessage();
            if (msg == null || !msg.contains("StaticMDCBinder")) {
                throw ncde;
            }
            org.slf4j.helpers.Util.report("Failed to load class \"org.slf4j.impl.StaticMDCBinder\".");
            org.slf4j.helpers.Util.report("Defaulting to no-operation MDCAdapter implementation.");
            org.slf4j.helpers.Util.report("See http://www.slf4j.org/codes.html#no_static_mdc_binder for further details.");
        } catch (java.lang.Exception e) {
            org.slf4j.helpers.Util.report("MDC binding unsuccessful.", e);
        }
    }

    public static void put(java.lang.String key, java.lang.String val) throws java.lang.IllegalArgumentException {
        if (key == null) {
            throw new java.lang.IllegalArgumentException("key parameter cannot be null");
        } else if (mdcAdapter == null) {
            throw new java.lang.IllegalStateException("MDCAdapter cannot be null. See also http://www.slf4j.org/codes.html#null_MDCA");
        } else {
            mdcAdapter.put(key, val);
        }
    }

    public static org.slf4j.MDC.MDCCloseable putCloseable(java.lang.String key, java.lang.String val) throws java.lang.IllegalArgumentException {
        put(key, val);
        return new org.slf4j.MDC.MDCCloseable(key);
    }

    public static java.lang.String get(java.lang.String key) throws java.lang.IllegalArgumentException {
        if (key == null) {
            throw new java.lang.IllegalArgumentException("key parameter cannot be null");
        } else if (mdcAdapter != null) {
            return mdcAdapter.get(key);
        } else {
            throw new java.lang.IllegalStateException("MDCAdapter cannot be null. See also http://www.slf4j.org/codes.html#null_MDCA");
        }
    }

    public static void remove(java.lang.String key) throws java.lang.IllegalArgumentException {
        if (key == null) {
            throw new java.lang.IllegalArgumentException("key parameter cannot be null");
        } else if (mdcAdapter == null) {
            throw new java.lang.IllegalStateException("MDCAdapter cannot be null. See also http://www.slf4j.org/codes.html#null_MDCA");
        } else {
            mdcAdapter.remove(key);
        }
    }

    public static void clear() {
        if (mdcAdapter == null) {
            throw new java.lang.IllegalStateException("MDCAdapter cannot be null. See also http://www.slf4j.org/codes.html#null_MDCA");
        }
        mdcAdapter.clear();
    }

    public static java.util.Map<java.lang.String, java.lang.String> getCopyOfContextMap() {
        if (mdcAdapter != null) {
            return mdcAdapter.getCopyOfContextMap();
        }
        throw new java.lang.IllegalStateException("MDCAdapter cannot be null. See also http://www.slf4j.org/codes.html#null_MDCA");
    }

    public static void setContextMap(java.util.Map<java.lang.String, java.lang.String> contextMap) {
        if (mdcAdapter == null) {
            throw new java.lang.IllegalStateException("MDCAdapter cannot be null. See also http://www.slf4j.org/codes.html#null_MDCA");
        }
        mdcAdapter.setContextMap(contextMap);
    }

    public static org.slf4j.spi.MDCAdapter getMDCAdapter() {
        return mdcAdapter;
    }
}
