package org.slf4j.impl;

public class StaticMDCBinder {
    public static final org.slf4j.impl.StaticMDCBinder SINGLETON = new org.slf4j.impl.StaticMDCBinder();

    private StaticMDCBinder() {
    }

    public org.slf4j.spi.MDCAdapter getMDCA() {
        return new ch.qos.logback.classic.util.LogbackMDCAdapter();
    }

    public java.lang.String getMDCAdapterClassStr() {
        return ch.qos.logback.classic.util.LogbackMDCAdapter.class.getName();
    }
}
