package org.slf4j.impl;

public class StaticMarkerBinder implements org.slf4j.spi.MarkerFactoryBinder {
    public static final org.slf4j.impl.StaticMarkerBinder SINGLETON = new org.slf4j.impl.StaticMarkerBinder();
    final org.slf4j.IMarkerFactory markerFactory = new org.slf4j.helpers.BasicMarkerFactory();

    private StaticMarkerBinder() {
    }

    public org.slf4j.IMarkerFactory getMarkerFactory() {
        return this.markerFactory;
    }

    public java.lang.String getMarkerFactoryClassStr() {
        return org.slf4j.helpers.BasicMarkerFactory.class.getName();
    }
}
