package org.slf4j.impl;

public class StaticLoggerBinder implements org.slf4j.spi.LoggerFactoryBinder {
    private static java.lang.Object KEY = new java.lang.Object();
    static final java.lang.String NULL_CS_URL = "http://logback.qos.ch/codes.html#null_CS";
    public static java.lang.String REQUESTED_API_VERSION = "1.6";
    private static org.slf4j.impl.StaticLoggerBinder SINGLETON = new org.slf4j.impl.StaticLoggerBinder();
    private final ch.qos.logback.classic.util.ContextSelectorStaticBinder contextSelectorBinder = ch.qos.logback.classic.util.ContextSelectorStaticBinder.getSingleton();
    private ch.qos.logback.classic.LoggerContext defaultLoggerContext = new ch.qos.logback.classic.LoggerContext();
    private boolean initialized = false;

    static {
        SINGLETON.init();
    }

    private StaticLoggerBinder() {
        this.defaultLoggerContext.setName("default");
    }

    public static org.slf4j.impl.StaticLoggerBinder getSingleton() {
        return SINGLETON;
    }

    static void reset() {
        SINGLETON = new org.slf4j.impl.StaticLoggerBinder();
        SINGLETON.init();
    }

    public org.slf4j.ILoggerFactory getLoggerFactory() {
        if (!this.initialized) {
            return this.defaultLoggerContext;
        }
        if (this.contextSelectorBinder.getContextSelector() != null) {
            return this.contextSelectorBinder.getContextSelector().getLoggerContext();
        }
        throw new java.lang.IllegalStateException("contextSelector cannot be null. See also http://logback.qos.ch/codes.html#null_CS");
    }

    public java.lang.String getLoggerFactoryClassStr() {
        return this.contextSelectorBinder.getClass().getName();
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    public void init() {
        try {
            new ch.qos.logback.classic.util.ContextInitializer(this.defaultLoggerContext).autoConfig();
        } catch (ch.qos.logback.core.joran.spi.JoranException e) {
            org.slf4j.helpers.Util.report("Failed to auto configure default logger context", e);
        } catch (Throwable th) {
            org.slf4j.helpers.Util.report("Failed to instantiate [" + ch.qos.logback.classic.LoggerContext.class.getName() + "]", th);
            return;
        }
        if (!ch.qos.logback.core.status.StatusUtil.contextHasStatusListener(this.defaultLoggerContext)) {
            ch.qos.logback.core.util.StatusPrinter.printInCaseOfErrorsOrWarnings(this.defaultLoggerContext);
        }
        this.contextSelectorBinder.init(this.defaultLoggerContext, KEY);
        this.initialized = true;
    }
}
