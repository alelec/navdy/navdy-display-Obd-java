package org.slf4j;

public interface Logger {
    public static final java.lang.String ROOT_LOGGER_NAME = "ROOT";

    void debug(java.lang.String str);

    void debug(java.lang.String str, java.lang.Object obj);

    void debug(java.lang.String str, java.lang.Object obj, java.lang.Object obj2);

    void debug(java.lang.String str, java.lang.Throwable th);

    void debug(java.lang.String str, java.lang.Object... objArr);

    void debug(org.slf4j.Marker marker, java.lang.String str);

    void debug(org.slf4j.Marker marker, java.lang.String str, java.lang.Object obj);

    void debug(org.slf4j.Marker marker, java.lang.String str, java.lang.Object obj, java.lang.Object obj2);

    void debug(org.slf4j.Marker marker, java.lang.String str, java.lang.Throwable th);

    void debug(org.slf4j.Marker marker, java.lang.String str, java.lang.Object... objArr);

    void error(java.lang.String str);

    void error(java.lang.String str, java.lang.Object obj);

    void error(java.lang.String str, java.lang.Object obj, java.lang.Object obj2);

    void error(java.lang.String str, java.lang.Throwable th);

    void error(java.lang.String str, java.lang.Object... objArr);

    void error(org.slf4j.Marker marker, java.lang.String str);

    void error(org.slf4j.Marker marker, java.lang.String str, java.lang.Object obj);

    void error(org.slf4j.Marker marker, java.lang.String str, java.lang.Object obj, java.lang.Object obj2);

    void error(org.slf4j.Marker marker, java.lang.String str, java.lang.Throwable th);

    void error(org.slf4j.Marker marker, java.lang.String str, java.lang.Object... objArr);

    java.lang.String getName();

    void info(java.lang.String str);

    void info(java.lang.String str, java.lang.Object obj);

    void info(java.lang.String str, java.lang.Object obj, java.lang.Object obj2);

    void info(java.lang.String str, java.lang.Throwable th);

    void info(java.lang.String str, java.lang.Object... objArr);

    void info(org.slf4j.Marker marker, java.lang.String str);

    void info(org.slf4j.Marker marker, java.lang.String str, java.lang.Object obj);

    void info(org.slf4j.Marker marker, java.lang.String str, java.lang.Object obj, java.lang.Object obj2);

    void info(org.slf4j.Marker marker, java.lang.String str, java.lang.Throwable th);

    void info(org.slf4j.Marker marker, java.lang.String str, java.lang.Object... objArr);

    boolean isDebugEnabled();

    boolean isDebugEnabled(org.slf4j.Marker marker);

    boolean isErrorEnabled();

    boolean isErrorEnabled(org.slf4j.Marker marker);

    boolean isInfoEnabled();

    boolean isInfoEnabled(org.slf4j.Marker marker);

    boolean isTraceEnabled();

    boolean isTraceEnabled(org.slf4j.Marker marker);

    boolean isWarnEnabled();

    boolean isWarnEnabled(org.slf4j.Marker marker);

    void trace(java.lang.String str);

    void trace(java.lang.String str, java.lang.Object obj);

    void trace(java.lang.String str, java.lang.Object obj, java.lang.Object obj2);

    void trace(java.lang.String str, java.lang.Throwable th);

    void trace(java.lang.String str, java.lang.Object... objArr);

    void trace(org.slf4j.Marker marker, java.lang.String str);

    void trace(org.slf4j.Marker marker, java.lang.String str, java.lang.Object obj);

    void trace(org.slf4j.Marker marker, java.lang.String str, java.lang.Object obj, java.lang.Object obj2);

    void trace(org.slf4j.Marker marker, java.lang.String str, java.lang.Throwable th);

    void trace(org.slf4j.Marker marker, java.lang.String str, java.lang.Object... objArr);

    void warn(java.lang.String str);

    void warn(java.lang.String str, java.lang.Object obj);

    void warn(java.lang.String str, java.lang.Object obj, java.lang.Object obj2);

    void warn(java.lang.String str, java.lang.Throwable th);

    void warn(java.lang.String str, java.lang.Object... objArr);

    void warn(org.slf4j.Marker marker, java.lang.String str);

    void warn(org.slf4j.Marker marker, java.lang.String str, java.lang.Object obj);

    void warn(org.slf4j.Marker marker, java.lang.String str, java.lang.Object obj, java.lang.Object obj2);

    void warn(org.slf4j.Marker marker, java.lang.String str, java.lang.Throwable th);

    void warn(org.slf4j.Marker marker, java.lang.String str, java.lang.Object... objArr);
}
