package org.slf4j;

public final class LoggerFactory {
    private static final java.lang.String[] API_COMPATIBILITY_LIST = {"1.6", "1.7"};
    static final java.lang.String CODES_PREFIX = "http://www.slf4j.org/codes.html";
    static boolean DETECT_LOGGER_NAME_MISMATCH = org.slf4j.helpers.Util.safeGetBooleanSystemProperty(DETECT_LOGGER_NAME_MISMATCH_PROPERTY);
    static final java.lang.String DETECT_LOGGER_NAME_MISMATCH_PROPERTY = "slf4j.detectLoggerNameMismatch";
    static final int FAILED_INITIALIZATION = 2;
    static volatile int INITIALIZATION_STATE = 0;
    static final java.lang.String JAVA_VENDOR_PROPERTY = "java.vendor.url";
    static final java.lang.String LOGGER_NAME_MISMATCH_URL = "http://www.slf4j.org/codes.html#loggerNameMismatch";
    static final java.lang.String MULTIPLE_BINDINGS_URL = "http://www.slf4j.org/codes.html#multiple_bindings";
    static org.slf4j.helpers.NOPLoggerFactory NOP_FALLBACK_FACTORY = new org.slf4j.helpers.NOPLoggerFactory();
    static final int NOP_FALLBACK_INITIALIZATION = 4;
    static final java.lang.String NO_STATICLOGGERBINDER_URL = "http://www.slf4j.org/codes.html#StaticLoggerBinder";
    static final java.lang.String NULL_LF_URL = "http://www.slf4j.org/codes.html#null_LF";
    static final int ONGOING_INITIALIZATION = 1;
    static final java.lang.String REPLAY_URL = "http://www.slf4j.org/codes.html#replay";
    private static java.lang.String STATIC_LOGGER_BINDER_PATH = "org/slf4j/impl/StaticLoggerBinder.class";
    static final java.lang.String SUBSTITUTE_LOGGER_URL = "http://www.slf4j.org/codes.html#substituteLogger";
    static org.slf4j.helpers.SubstituteLoggerFactory SUBST_FACTORY = new org.slf4j.helpers.SubstituteLoggerFactory();
    static final int SUCCESSFUL_INITIALIZATION = 3;
    static final int UNINITIALIZED = 0;
    static final java.lang.String UNSUCCESSFUL_INIT_MSG = "org.slf4j.LoggerFactory could not be successfully initialized. See also http://www.slf4j.org/codes.html#unsuccessfulInit";
    static final java.lang.String UNSUCCESSFUL_INIT_URL = "http://www.slf4j.org/codes.html#unsuccessfulInit";
    static final java.lang.String VERSION_MISMATCH = "http://www.slf4j.org/codes.html#version_mismatch";

    private LoggerFactory() {
    }

    static void reset() {
        INITIALIZATION_STATE = 0;
    }

    private static final void performInitialization() {
        bind();
        if (INITIALIZATION_STATE == 3) {
            versionSanityCheck();
        }
    }

    private static boolean messageContainsOrgSlf4jImplStaticLoggerBinder(java.lang.String msg) {
        if (msg == null) {
            return false;
        }
        if (msg.contains("org/slf4j/impl/StaticLoggerBinder")) {
            return true;
        }
        if (msg.contains("org.slf4j.impl.StaticLoggerBinder")) {
            return true;
        }
        return false;
    }

    private static final void bind() {
        java.util.Set<java.net.URL> staticLoggerBinderPathSet = null;
        try {
            if (!isAndroid()) {
                staticLoggerBinderPathSet = findPossibleStaticLoggerBinderPathSet();
                reportMultipleBindingAmbiguity(staticLoggerBinderPathSet);
            }
            org.slf4j.impl.StaticLoggerBinder.getSingleton();
            INITIALIZATION_STATE = 3;
            reportActualBinding(staticLoggerBinderPathSet);
            fixSubstituteLoggers();
            replayEvents();
            SUBST_FACTORY.clear();
        } catch (java.lang.NoClassDefFoundError ncde) {
            if (messageContainsOrgSlf4jImplStaticLoggerBinder(ncde.getMessage())) {
                INITIALIZATION_STATE = 4;
                org.slf4j.helpers.Util.report("Failed to load class \"org.slf4j.impl.StaticLoggerBinder\".");
                org.slf4j.helpers.Util.report("Defaulting to no-operation (NOP) logger implementation");
                org.slf4j.helpers.Util.report("See http://www.slf4j.org/codes.html#StaticLoggerBinder for further details.");
                return;
            }
            failedBinding(ncde);
            throw ncde;
        } catch (java.lang.NoSuchMethodError nsme) {
            java.lang.String msg = nsme.getMessage();
            if (msg != null && msg.contains("org.slf4j.impl.StaticLoggerBinder.getSingleton()")) {
                INITIALIZATION_STATE = 2;
                org.slf4j.helpers.Util.report("slf4j-api 1.6.x (or later) is incompatible with this binding.");
                org.slf4j.helpers.Util.report("Your binding is version 1.5.5 or earlier.");
                org.slf4j.helpers.Util.report("Upgrade your binding to version 1.6.x.");
            }
            throw nsme;
        } catch (java.lang.Exception e) {
            failedBinding(e);
            throw new java.lang.IllegalStateException("Unexpected initialization failure", e);
        }
    }

    private static void fixSubstituteLoggers() {
        synchronized (SUBST_FACTORY) {
            SUBST_FACTORY.postInitialization();
            for (org.slf4j.helpers.SubstituteLogger substLogger : SUBST_FACTORY.getLoggers()) {
                substLogger.setDelegate(getLogger(substLogger.getName()));
            }
        }
    }

    static void failedBinding(java.lang.Throwable t) {
        INITIALIZATION_STATE = 2;
        org.slf4j.helpers.Util.report("Failed to instantiate SLF4J LoggerFactory", t);
    }

    private static void replayEvents() {
        java.util.concurrent.LinkedBlockingQueue<org.slf4j.event.SubstituteLoggingEvent> queue = SUBST_FACTORY.getEventQueue();
        int queueSize = queue.size();
        int count = 0;
        java.util.List<org.slf4j.event.SubstituteLoggingEvent> eventList = new java.util.ArrayList<>(128);
        while (queue.drainTo(eventList, 128) != 0) {
            for (org.slf4j.event.SubstituteLoggingEvent event : eventList) {
                replaySingleEvent(event);
                int count2 = count + 1;
                if (count == 0) {
                    emitReplayOrSubstituionWarning(event, queueSize);
                }
                count = count2;
            }
            eventList.clear();
        }
    }

    private static void emitReplayOrSubstituionWarning(org.slf4j.event.SubstituteLoggingEvent event, int queueSize) {
        if (event.getLogger().isDelegateEventAware()) {
            emitReplayWarning(queueSize);
        } else if (!event.getLogger().isDelegateNOP()) {
            emitSubstitutionWarning();
        }
    }

    private static void replaySingleEvent(org.slf4j.event.SubstituteLoggingEvent event) {
        if (event != null) {
            org.slf4j.helpers.SubstituteLogger substLogger = event.getLogger();
            java.lang.String loggerName = substLogger.getName();
            if (substLogger.isDelegateNull()) {
                throw new java.lang.IllegalStateException("Delegate logger cannot be null at this state.");
            } else if (substLogger.isDelegateNOP()) {
            } else {
                if (substLogger.isDelegateEventAware()) {
                    substLogger.log(event);
                } else {
                    org.slf4j.helpers.Util.report(loggerName);
                }
            }
        }
    }

    private static void emitSubstitutionWarning() {
        org.slf4j.helpers.Util.report("The following set of substitute loggers may have been accessed");
        org.slf4j.helpers.Util.report("during the initialization phase. Logging calls during this");
        org.slf4j.helpers.Util.report("phase were not honored. However, subsequent logging calls to these");
        org.slf4j.helpers.Util.report("loggers will work as normally expected.");
        org.slf4j.helpers.Util.report("See also http://www.slf4j.org/codes.html#substituteLogger");
    }

    private static void emitReplayWarning(int eventCount) {
        org.slf4j.helpers.Util.report("A number (" + eventCount + ") of logging calls during the initialization phase have been intercepted and are");
        org.slf4j.helpers.Util.report("now being replayed. These are subject to the filtering rules of the underlying logging system.");
        org.slf4j.helpers.Util.report("See also http://www.slf4j.org/codes.html#replay");
    }

    private static final void versionSanityCheck() {
        try {
            java.lang.String requested = org.slf4j.impl.StaticLoggerBinder.REQUESTED_API_VERSION;
            boolean match = false;
            for (java.lang.String aAPI_COMPATIBILITY_LIST : API_COMPATIBILITY_LIST) {
                if (requested.startsWith(aAPI_COMPATIBILITY_LIST)) {
                    match = true;
                }
            }
            if (!match) {
                org.slf4j.helpers.Util.report("The requested version " + requested + " by your slf4j binding is not compatible with " + java.util.Arrays.asList(API_COMPATIBILITY_LIST).toString());
                org.slf4j.helpers.Util.report("See http://www.slf4j.org/codes.html#version_mismatch for further details.");
            }
        } catch (java.lang.NoSuchFieldError e) {
        } catch (Throwable e2) {
            org.slf4j.helpers.Util.report("Unexpected problem occured during version sanity check", e2);
        }
    }

    static java.util.Set<java.net.URL> findPossibleStaticLoggerBinderPathSet() {
        java.util.Enumeration<java.net.URL> paths;
        java.util.Set<java.net.URL> staticLoggerBinderPathSet = new java.util.LinkedHashSet<>();
        try {
            java.lang.ClassLoader loggerFactoryClassLoader = org.slf4j.LoggerFactory.class.getClassLoader();
            if (loggerFactoryClassLoader == null) {
                paths = java.lang.ClassLoader.getSystemResources(STATIC_LOGGER_BINDER_PATH);
            } else {
                paths = loggerFactoryClassLoader.getResources(STATIC_LOGGER_BINDER_PATH);
            }
            while (paths.hasMoreElements()) {
                staticLoggerBinderPathSet.add((java.net.URL) paths.nextElement());
            }
        } catch (java.io.IOException ioe) {
            org.slf4j.helpers.Util.report("Error getting resources from path", ioe);
        }
        return staticLoggerBinderPathSet;
    }

    private static boolean isAmbiguousStaticLoggerBinderPathSet(java.util.Set<java.net.URL> binderPathSet) {
        return binderPathSet.size() > 1;
    }

    private static void reportMultipleBindingAmbiguity(java.util.Set<java.net.URL> binderPathSet) {
        if (isAmbiguousStaticLoggerBinderPathSet(binderPathSet)) {
            org.slf4j.helpers.Util.report("Class path contains multiple SLF4J bindings.");
            for (java.net.URL path : binderPathSet) {
                org.slf4j.helpers.Util.report("Found binding in [" + path + "]");
            }
            org.slf4j.helpers.Util.report("See http://www.slf4j.org/codes.html#multiple_bindings for an explanation.");
        }
    }

    private static boolean isAndroid() {
        java.lang.String vendor = org.slf4j.helpers.Util.safeGetSystemProperty(JAVA_VENDOR_PROPERTY);
        if (vendor == null) {
            return false;
        }
        return vendor.toLowerCase().contains("android");
    }

    private static void reportActualBinding(java.util.Set<java.net.URL> binderPathSet) {
        if (binderPathSet != null && isAmbiguousStaticLoggerBinderPathSet(binderPathSet)) {
            org.slf4j.helpers.Util.report("Actual binding is of type [" + org.slf4j.impl.StaticLoggerBinder.getSingleton().getLoggerFactoryClassStr() + "]");
        }
    }

    public static org.slf4j.Logger getLogger(java.lang.String name) {
        return getILoggerFactory().getLogger(name);
    }

    public static org.slf4j.Logger getLogger(java.lang.Class<?> clazz) {
        org.slf4j.Logger logger = getLogger(clazz.getName());
        if (DETECT_LOGGER_NAME_MISMATCH) {
            java.lang.Class<?> autoComputedCallingClass = org.slf4j.helpers.Util.getCallingClass();
            if (autoComputedCallingClass != null && nonMatchingClasses(clazz, autoComputedCallingClass)) {
                org.slf4j.helpers.Util.report(java.lang.String.format("Detected logger name mismatch. Given name: \"%s\"; computed name: \"%s\".", new java.lang.Object[]{logger.getName(), autoComputedCallingClass.getName()}));
                org.slf4j.helpers.Util.report("See http://www.slf4j.org/codes.html#loggerNameMismatch for an explanation");
            }
        }
        return logger;
    }

    private static boolean nonMatchingClasses(java.lang.Class<?> clazz, java.lang.Class<?> autoComputedCallingClass) {
        return !autoComputedCallingClass.isAssignableFrom(clazz);
    }

    public static org.slf4j.ILoggerFactory getILoggerFactory() {
        if (INITIALIZATION_STATE == 0) {
            synchronized (org.slf4j.LoggerFactory.class) {
                if (INITIALIZATION_STATE == 0) {
                    INITIALIZATION_STATE = 1;
                    performInitialization();
                }
            }
        }
        switch (INITIALIZATION_STATE) {
            case 1:
                return SUBST_FACTORY;
            case 2:
                throw new java.lang.IllegalStateException(UNSUCCESSFUL_INIT_MSG);
            case 3:
                return org.slf4j.impl.StaticLoggerBinder.getSingleton().getLoggerFactory();
            case 4:
                return NOP_FALLBACK_FACTORY;
            default:
                throw new java.lang.IllegalStateException("Unreachable code");
        }
    }
}
