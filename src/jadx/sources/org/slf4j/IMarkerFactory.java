package org.slf4j;

public interface IMarkerFactory {
    boolean detachMarker(java.lang.String str);

    boolean exists(java.lang.String str);

    org.slf4j.Marker getDetachedMarker(java.lang.String str);

    org.slf4j.Marker getMarker(java.lang.String str);
}
