package com.mindprod.ledatastream;

public final class LEDataInputStream implements java.io.DataInput {
    private static final java.lang.String EMBEDDED_COPYRIGHT = "copyright (c) 1999-2010 Roedy Green, Canadian Mind Products, http://mindprod.com";
    protected final java.io.DataInputStream dis;
    protected final java.io.InputStream is;
    protected final byte[] work = new byte[8];

    public LEDataInputStream(java.io.InputStream inputStream) {
        this.is = inputStream;
        this.dis = new java.io.DataInputStream(inputStream);
    }

    public static java.lang.String readUTF(java.io.DataInput dataInput) throws java.io.IOException {
        return java.io.DataInputStream.readUTF(dataInput);
    }

    public final void close() throws java.io.IOException {
        this.dis.close();
    }

    public final int read(byte[] bArr, int i, int i2) throws java.io.IOException {
        return this.is.read(bArr, i, i2);
    }

    public final boolean readBoolean() throws java.io.IOException {
        return this.dis.readBoolean();
    }

    public final byte readByte() throws java.io.IOException {
        return this.dis.readByte();
    }

    public final char readChar() throws java.io.IOException {
        this.dis.readFully(this.work, 0, 2);
        return (char) (((this.work[1] & com.navdy.obd.update.STNBootloaderChannel.ERROR_CODE_INVALID_RESPONSE) << 8) | (this.work[0] & com.navdy.obd.update.STNBootloaderChannel.ERROR_CODE_INVALID_RESPONSE));
    }

    public final double readDouble() throws java.io.IOException {
        return java.lang.Double.longBitsToDouble(readLong());
    }

    public final float readFloat() throws java.io.IOException {
        return java.lang.Float.intBitsToFloat(readInt());
    }

    public final void readFully(byte[] bArr) throws java.io.IOException {
        this.dis.readFully(bArr, 0, bArr.length);
    }

    public final void readFully(byte[] bArr, int i, int i2) throws java.io.IOException {
        this.dis.readFully(bArr, i, i2);
    }

    public final int readInt() throws java.io.IOException {
        this.dis.readFully(this.work, 0, 4);
        return (this.work[3] << 24) | ((this.work[2] & com.navdy.obd.update.STNBootloaderChannel.ERROR_CODE_INVALID_RESPONSE) << 16) | ((this.work[1] & com.navdy.obd.update.STNBootloaderChannel.ERROR_CODE_INVALID_RESPONSE) << 8) | (this.work[0] & com.navdy.obd.update.STNBootloaderChannel.ERROR_CODE_INVALID_RESPONSE);
    }

    public final java.lang.String readLine() throws java.io.IOException {
        return this.dis.readLine();
    }

    public final long readLong() throws java.io.IOException {
        this.dis.readFully(this.work, 0, 8);
        return (((long) this.work[7]) << 56) | (((long) (this.work[6] & com.navdy.obd.update.STNBootloaderChannel.ERROR_CODE_INVALID_RESPONSE)) << 48) | (((long) (this.work[5] & com.navdy.obd.update.STNBootloaderChannel.ERROR_CODE_INVALID_RESPONSE)) << 40) | (((long) (this.work[4] & com.navdy.obd.update.STNBootloaderChannel.ERROR_CODE_INVALID_RESPONSE)) << 32) | (((long) (this.work[3] & com.navdy.obd.update.STNBootloaderChannel.ERROR_CODE_INVALID_RESPONSE)) << 24) | (((long) (this.work[2] & com.navdy.obd.update.STNBootloaderChannel.ERROR_CODE_INVALID_RESPONSE)) << 16) | (((long) (this.work[1] & com.navdy.obd.update.STNBootloaderChannel.ERROR_CODE_INVALID_RESPONSE)) << 8) | ((long) (this.work[0] & com.navdy.obd.update.STNBootloaderChannel.ERROR_CODE_INVALID_RESPONSE));
    }

    public final short readShort() throws java.io.IOException {
        this.dis.readFully(this.work, 0, 2);
        return (short) (((this.work[1] & com.navdy.obd.update.STNBootloaderChannel.ERROR_CODE_INVALID_RESPONSE) << 8) | (this.work[0] & com.navdy.obd.update.STNBootloaderChannel.ERROR_CODE_INVALID_RESPONSE));
    }

    public final java.lang.String readUTF() throws java.io.IOException {
        return this.dis.readUTF();
    }

    public final int readUnsignedByte() throws java.io.IOException {
        return this.dis.readUnsignedByte();
    }

    public final int readUnsignedShort() throws java.io.IOException {
        this.dis.readFully(this.work, 0, 2);
        return ((this.work[1] & com.navdy.obd.update.STNBootloaderChannel.ERROR_CODE_INVALID_RESPONSE) << 8) | (this.work[0] & com.navdy.obd.update.STNBootloaderChannel.ERROR_CODE_INVALID_RESPONSE);
    }

    public final int skipBytes(int i) throws java.io.IOException {
        return this.dis.skipBytes(i);
    }
}
