package com.navdy.obd;

public class VehicleInfo implements android.os.Parcelable {
    public static final android.os.Parcelable.Creator<com.navdy.obd.VehicleInfo> CREATOR = new com.navdy.obd.VehicleInfo.Anon1();
    public final java.util.List<com.navdy.obd.ECU> ecus;
    public final boolean isCheckEngineLightOn;
    private com.navdy.obd.ECU primaryEcu;
    public final java.lang.String protocol;
    public final java.util.List<java.lang.String> troubleCodes;
    public final java.lang.String vin;

    static class Anon1 implements android.os.Parcelable.Creator<com.navdy.obd.VehicleInfo> {
        Anon1() {
        }

        public com.navdy.obd.VehicleInfo createFromParcel(android.os.Parcel source) {
            return new com.navdy.obd.VehicleInfo(source);
        }

        public com.navdy.obd.VehicleInfo[] newArray(int size) {
            return new com.navdy.obd.VehicleInfo[size];
        }
    }

    public VehicleInfo(java.lang.String vin2) {
        this(null, null, vin2, false, null);
    }

    public VehicleInfo(java.lang.String protocol2, java.util.List<com.navdy.obd.ECU> ecus2, java.lang.String vin2, boolean isCheckEngineLightOn2, java.util.List<java.lang.String> troubleCodes2) {
        this.protocol = protocol2;
        this.ecus = ecus2 != null ? java.util.Collections.unmodifiableList(ecus2) : null;
        this.vin = vin2;
        this.isCheckEngineLightOn = isCheckEngineLightOn2;
        this.troubleCodes = troubleCodes2;
    }

    public int describeContents() {
        return 0;
    }

    public VehicleInfo(android.os.Parcel in) {
        java.util.List<com.navdy.obd.ECU> ecus2 = new java.util.ArrayList<>();
        this.protocol = in.readString();
        this.vin = in.readString();
        in.readList(ecus2, null);
        this.ecus = java.util.Collections.unmodifiableList(ecus2);
        this.isCheckEngineLightOn = in.readByte() != 0;
        java.util.List<java.lang.String> troubleCodes2 = new java.util.ArrayList<>();
        in.readList(troubleCodes2, null);
        this.troubleCodes = java.util.Collections.unmodifiableList(troubleCodes2);
    }

    public com.navdy.obd.ECU getPrimaryEcu() {
        if (this.primaryEcu == null) {
            int bestPidCount = 0;
            com.navdy.obd.ECU bestEcu = null;
            for (com.navdy.obd.ECU ecu : this.ecus) {
                int pidCount = ecu.supportedPids.size();
                if (pidCount > bestPidCount && ecu.supportedPids.contains(13)) {
                    bestEcu = ecu;
                    bestPidCount = pidCount;
                }
            }
            this.primaryEcu = bestEcu;
        }
        return this.primaryEcu;
    }

    public java.util.List<com.navdy.obd.ECU> getEcus() {
        return this.ecus;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeString(this.protocol);
        dest.writeString(this.vin);
        dest.writeList(this.ecus);
        dest.writeByte((byte) (this.isCheckEngineLightOn ? 1 : 0));
        dest.writeList(this.troubleCodes);
    }
}
