package com.navdy.obd;

public class Utility {
    /* JADX WARNING: Incorrect type for immutable var: ssa=byte, code=int, for r4v0, types: [int, byte] */
    public static int updateCcittCrc(int crc, int data) {
        int x = ((crc >> 8) ^ data) & 255;
        int x2 = x ^ (x >> 4);
        return (((crc << 8) ^ (x2 << 12)) ^ (x2 << 5)) ^ x2;
    }

    public static int calculateCcittCrc(byte[] data) {
        int crc = 0;
        for (byte input : data) {
            crc = updateCcittCrc(crc, input);
        }
        return crc;
    }

    public static double calculateInstantaneousFuelConsumption(double maf, double vss) {
        if (vss > 0.0d) {
            return (34.0278d * maf) / vss;
        }
        return 0.0d;
    }

    public static java.lang.String convertInputStreamToString(java.io.InputStream inputStream) {
        try {
            java.io.ByteArrayOutputStream byteArrayOutputStream = new java.io.ByteArrayOutputStream();
            byte[] buffer = new byte[16384];
            while (true) {
                int n = inputStream.read(buffer);
                if (n == -1) {
                    return byteArrayOutputStream.toString();
                }
                byteArrayOutputStream.write(buffer, 0, n);
            }
        } catch (Throwable th) {
            return null;
        }
    }

    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r1v3
  assigns: []
  uses: []
  mth insns count: 28
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    public static int bytesToInt(byte[] byteArray, int offset, int length, boolean bigEndian) {
        if (byteArray == null || length <= 0 || length > 4 || byteArray.length < offset + length) {
            throw new java.lang.NumberFormatException("Incorrect data");
        }
        int data = 0;
        int i = 0;
        while (i < length) {
            int data2 = data + (byteArray[offset + (bigEndian ? i : (length - i) - 1)] & 255);
            int data3 = i < length + -1 ? data2 << 8 : data2;
            i++;
            data = data3;
        }
        return data;
    }

    public static boolean isUserBuild() {
        return "user".equals(android.os.Build.TYPE);
    }
}
