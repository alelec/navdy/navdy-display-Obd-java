package com.navdy.obd.j1939;

public class MonitorJ1939DataJob implements java.lang.Runnable {
    private static final java.lang.String ADD_PGN_FILTER_COMMAND = "STFPGA %s";
    private static final int CARRIAGE_RETURN = 13;
    public static final com.navdy.obd.command.ObdCommand CLEAR_ALL_PASS_FILTERS = new com.navdy.obd.command.ObdCommand("STFCA");
    private static com.navdy.obd.command.ObdCommand CLEAR_FILTERS_COMMAND = new com.navdy.obd.command.ObdCommand("Clear PGN filters", "STFPGC");
    public static final int HEADER_SIZE = 4;
    private static final java.lang.String MONITORING_COMMAND = "STM";
    public static final java.lang.String TAG = com.navdy.obd.ObdService.class.getSimpleName();
    public static final int TIMEOUT_MILLIS = ((int) java.util.concurrent.TimeUnit.SECONDS.toMillis(5));
    private static com.navdy.obd.command.ObdCommand TURN_OFF_SILENT_MONITORING_COMMAND = new com.navdy.obd.command.ObdCommand("TurnOffSilentMonitiroing", "ATCSM0");
    private static com.navdy.obd.command.ObdCommand TURN_ON_HEADERS = new com.navdy.obd.command.ObdCommand("Turn on Headers", "ATH1");
    private static final java.lang.String VALID_PGN_FILTER_REGEX = "[0-9A-Fa-f]{1,6}";
    private com.navdy.obd.io.IChannel channel;
    private com.navdy.obd.io.ObdCommandInterpreter commandInterpreter;
    private com.navdy.obd.j1939.MonitorJ1939DataJob.IDataObserver dataObserver;
    /* access modifiers changed from: private */
    public com.navdy.obd.ObdDataObserver obdDataObserver;
    private java.util.HashMap<java.lang.Integer, java.util.List<com.navdy.obd.j1939.Parameter>> parameterGroupLookup = new java.util.HashMap<>();
    private java.util.ArrayList<java.lang.Integer> parameterGroupsToMonitor = new java.util.ArrayList<>();

    class Anon1 implements com.navdy.obd.ObdJob.IListener {
        final /* synthetic */ com.navdy.obd.command.BatchCommand val$setupMonitoringCommand;

        Anon1(com.navdy.obd.command.BatchCommand batchCommand) {
            this.val$setupMonitoringCommand = batchCommand;
        }

        public void onComplete(com.navdy.obd.ObdJob job, boolean success) {
            android.util.Log.d(com.navdy.obd.j1939.MonitorJ1939DataJob.TAG, "ApplyFilter complete , Success :" + success);
            if (success) {
                for (com.navdy.obd.command.ICommand command : this.val$setupMonitoringCommand.getCommands()) {
                    com.navdy.obd.command.ObdCommand obdCommand = (com.navdy.obd.command.ObdCommand) command;
                    android.util.Log.d(com.navdy.obd.j1939.MonitorJ1939DataJob.TAG, "Response for " + obdCommand.getCommand() + " - " + obdCommand.getResponse().trim());
                }
                com.navdy.obd.j1939.MonitorJ1939DataJob.this.startMonitoring();
                return;
            }
            com.navdy.obd.j1939.MonitorJ1939DataJob.this.obdDataObserver.onError("Failed to apply PGN filters");
            android.util.Log.d(com.navdy.obd.j1939.MonitorJ1939DataJob.TAG, "Failed to apply the PGN filters, not initiating the monitoring");
        }
    }

    public interface IDataObserver {
        void onParameterGroupData(int i, java.util.List<com.navdy.obd.j1939.Parameter> list);
    }

    public MonitorJ1939DataJob(com.navdy.obd.io.IChannel channel2, java.util.List<com.navdy.obd.j1939.Parameter> parametersList, com.navdy.obd.ObdDataObserver logger, com.navdy.obd.j1939.MonitorJ1939DataJob.IDataObserver dataObserver2) {
        this.channel = channel2;
        this.obdDataObserver = logger;
        this.dataObserver = dataObserver2;
        try {
            this.commandInterpreter = new com.navdy.obd.io.ObdCommandInterpreter(channel2.getInputStream(), channel2.getOutputStream());
            this.commandInterpreter.setReadTimeOut(TIMEOUT_MILLIS);
            if (parametersList != null) {
                for (com.navdy.obd.j1939.Parameter parameter : parametersList) {
                    java.util.List<com.navdy.obd.j1939.Parameter> parameterGroup = (java.util.List) this.parameterGroupLookup.get(java.lang.Integer.valueOf(parameter.pgn));
                    if (parameterGroup == null) {
                        parameterGroup = new java.util.ArrayList<>();
                        this.parameterGroupLookup.put(java.lang.Integer.valueOf(parameter.pgn), parameterGroup);
                        this.parameterGroupsToMonitor.add(java.lang.Integer.valueOf(parameter.pgn));
                    }
                    parameterGroup.add(parameter);
                }
            }
        } catch (java.io.IOException e) {
            android.util.Log.e(TAG, "Error accessing streams from channel ", e);
        }
    }

    public void run() {
        if (this.commandInterpreter == null) {
            android.util.Log.e(TAG, "Command interpreter is not initialized properly");
            return;
        }
        try {
            android.util.Log.d(TAG, "Starting to monitor the J1939 data on the CAN bus");
            if (this.parameterGroupsToMonitor.size() == 0) {
                android.util.Log.e(TAG, "No parameters to be monitored, not monitoring");
                return;
            }
            com.navdy.obd.command.BatchCommand setupMonitoringCommand = new com.navdy.obd.command.BatchCommand(new com.navdy.obd.command.ICommand[0]);
            setupMonitoringCommand.add(new com.navdy.obd.command.ObdCommand("DUMMY", "\n"));
            setupMonitoringCommand.add(new com.navdy.obd.command.ObdCommand("STM\r"));
            setupMonitoringCommand.add(TURN_OFF_SILENT_MONITORING_COMMAND);
            setupMonitoringCommand.add(CLEAR_ALL_PASS_FILTERS);
            java.util.Iterator it = this.parameterGroupsToMonitor.iterator();
            while (it.hasNext()) {
                java.lang.String pgnHex = java.lang.Long.toHexString((long) ((java.lang.Integer) it.next()).intValue()).toUpperCase();
                com.navdy.obd.command.ObdCommand filterCommand = new com.navdy.obd.command.ObdCommand(java.lang.String.format(ADD_PGN_FILTER_COMMAND, new java.lang.Object[]{pgnHex}));
                android.util.Log.d(TAG, "Adding PGN filter with command " + filterCommand.getCommand());
                setupMonitoringCommand.add(filterCommand);
            }
            setupMonitoringCommand.add(TURN_ON_HEADERS);
            new com.navdy.obd.ObdJob(setupMonitoringCommand, this.channel, com.navdy.obd.Protocol.SAE_J1939_CAN_PROTOCOL, new com.navdy.obd.j1939.MonitorJ1939DataJob.Anon1(setupMonitoringCommand), this.obdDataObserver, false).run();
        } catch (java.lang.Exception exception) {
            android.util.Log.d(TAG, "Exception while adding filters " + exception);
            exception.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void startMonitoring() {
        int dataLength;
        android.util.Log.d(TAG, "startMonitoring ");
        try {
            this.commandInterpreter.write(MONITORING_COMMAND);
        } catch (java.io.IOException e) {
            android.util.Log.e(TAG, "IOException when writing the command");
        }
        this.obdDataObserver.onCommand(MONITORING_COMMAND);
        android.util.Log.d(TAG, MONITORING_COMMAND);
        while (true) {
            try {
                java.lang.String response = this.commandInterpreter.readLine();
                this.obdDataObserver.onResponse(response);
                byte[] bytes = new byte[(response.length() / 2)];
                com.navdy.obd.util.HexUtil.parseHexString(response, bytes, 0, bytes.length);
                byte b = bytes[0];
                int pgn = com.navdy.obd.Utility.bytesToInt(bytes, 1, 2, true);
                byte b2 = bytes[3];
                java.util.List<com.navdy.obd.j1939.Parameter> parameterGroup = (java.util.List) this.parameterGroupLookup.get(java.lang.Integer.valueOf(pgn));
                if (parameterGroup != null) {
                    for (com.navdy.obd.j1939.Parameter parameter : parameterGroup) {
                        if (parameter.lengthInBits >= 8) {
                            dataLength = parameter.lengthInBits / 8;
                        } else {
                            dataLength = 1;
                        }
                        if (parameter.getRecentData() == null) {
                            parameter.setRecentData(new byte[dataLength]);
                        }
                        java.lang.System.arraycopy(bytes, parameter.byteOffset + 4, parameter.getRecentData(), 0, dataLength);
                    }
                    if (this.dataObserver != null) {
                        this.dataObserver.onParameterGroupData(pgn, parameterGroup);
                    }
                }
            } catch (java.io.IOException e2) {
                android.util.Log.e(TAG, "Error while monitoring J1939 ", e2);
                this.obdDataObserver.onError("IOException while monitoring " + e2.toString());
                return;
            } catch (java.lang.InterruptedException e3) {
                android.util.Log.e(TAG, "Monitoring interrupted ", e3);
                this.obdDataObserver.onError("Monitoring was interrupted, finishing execution");
                return;
            }
        }
    }
}
