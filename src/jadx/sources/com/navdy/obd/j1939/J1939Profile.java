package com.navdy.obd.j1939;

public class J1939Profile {
    public static final long SPN_ENGINE_AVERAGE_FUEL_ECONOMY = 185;
    public static final long SPN_ENGINE_OIL_PRESSURE = 100;
    public static final long SPN_ENGINE_TRIP_FUEL = 5053;
    public static final long SPN_TOTAL_DISTANCE_TRAVELLED = 917;
    public static final java.lang.String TAG = com.navdy.obd.j1939.J1939Profile.class.getSimpleName();
    public static java.util.HashMap<java.lang.Integer, java.util.List<com.navdy.obd.j1939.Parameter>> obdPidToJ1939ParameterMapping = new java.util.HashMap<>();
    public static java.util.HashMap<java.lang.Long, java.lang.Integer> parameterToCustomPidMapping = new java.util.HashMap<>();

    class Column {
        public static final java.lang.String BIT_LENGTH = "BitLength";
        public static final java.lang.String BIT_OFFSET = "BitOffset";
        public static final java.lang.String BYTE_OFFSET = "ByteOffset";
        public static final java.lang.String MAX_VALUE = "MaxValue";
        public static final java.lang.String MIN_VALUE = "MinValue";
        public static final java.lang.String NAME = "Name";
        public static final java.lang.String OBD_PID = "OBD_Pid";
        public static final java.lang.String PGN = "PGN";
        public static final java.lang.String RESOLUTION = "Resolution";
        public static final java.lang.String SPN = "SPN";
        public static final java.lang.String VALUE_OFFSET = "ValueOffset";

        Column() {
        }
    }

    static {
        parameterToCustomPidMapping.put(java.lang.Long.valueOf(100), java.lang.Integer.valueOf(com.navdy.obd.Pids.ENGINE_OIL_PRESSURE));
        parameterToCustomPidMapping.put(java.lang.Long.valueOf(185), java.lang.Integer.valueOf(256));
        parameterToCustomPidMapping.put(java.lang.Long.valueOf(SPN_ENGINE_TRIP_FUEL), java.lang.Integer.valueOf(com.navdy.obd.Pids.ENGINE_TRIP_FUEL));
        parameterToCustomPidMapping.put(java.lang.Long.valueOf(917), java.lang.Integer.valueOf(com.navdy.obd.Pids.TOTAL_VEHICLE_DISTANCE));
    }

    /* access modifiers changed from: 0000 */
    public java.util.List<com.navdy.obd.j1939.Parameter> getParameters(int pid) {
        return (java.util.List) obdPidToJ1939ParameterMapping.get(java.lang.Integer.valueOf(pid));
    }

    /* JADX WARNING: Removed duplicated region for block: B:32:0x0188 A[SYNTHETIC, Splitter:B:32:0x0188] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x01ba A[SYNTHETIC, Splitter:B:46:0x01ba] */
    /* JADX WARNING: Removed duplicated region for block: B:62:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:20:0x0163=Splitter:B:20:0x0163, B:29:0x017d=Splitter:B:29:0x017d} */
    public void load(java.io.InputStream inputStream) {
        java.io.BufferedReader reader = null;
        try {
            java.io.BufferedReader bufferedReader = new java.io.BufferedReader(new java.io.InputStreamReader(inputStream));
            try {
                for (org.apache.commons.csv.CSVRecord record : org.apache.commons.csv.CSVFormat.DEFAULT.withHeader(new java.lang.String[0]).parse(bufferedReader)) {
                    java.lang.String name = record.get("Name");
                    long spn = java.lang.Long.parseLong(record.get(com.navdy.obd.j1939.J1939Profile.Column.SPN));
                    int pgn = java.lang.Integer.parseInt(record.get(com.navdy.obd.j1939.J1939Profile.Column.PGN));
                    int byteOffset = java.lang.Integer.parseInt(record.get("ByteOffset"));
                    int bitOffset = java.lang.Integer.parseInt(record.get("BitOffset"));
                    int bitLength = java.lang.Integer.parseInt(record.get("BitLength"));
                    int valueOffset = java.lang.Integer.parseInt(record.get("ValueOffset"));
                    double resolution = java.lang.Double.parseDouble(record.get("Resolution"));
                    double minValue = java.lang.Double.parseDouble(record.get("MinValue"));
                    double maxValue = java.lang.Double.parseDouble(record.get("MaxValue"));
                    int obd2Pid = java.lang.Integer.parseInt(record.get("OBD_Pid"));
                    com.navdy.obd.j1939.Parameter parameter = new com.navdy.obd.j1939.Parameter(name, spn, pgn, byteOffset, bitOffset, bitLength, (long) valueOffset, resolution, minValue, maxValue);
                    android.util.Log.d(TAG, "Parameter " + parameter + ", PID : " + obd2Pid);
                    if (obd2Pid == -1) {
                        android.util.Log.d(TAG, "PID is not defined for parameter :" + name);
                        if (parameterToCustomPidMapping.containsKey(java.lang.Long.valueOf(spn))) {
                            obd2Pid = ((java.lang.Integer) parameterToCustomPidMapping.get(java.lang.Long.valueOf(spn))).intValue();
                            android.util.Log.d(TAG, "Custom PID found " + obd2Pid);
                        } else {
                            android.util.Log.d(TAG, "No custom PID found either");
                        }
                    }
                    if (obd2Pid != -1) {
                        java.util.List<com.navdy.obd.j1939.Parameter> parameters = (java.util.List) obdPidToJ1939ParameterMapping.get(java.lang.Integer.valueOf(obd2Pid));
                        if (parameters == null) {
                            parameters = new java.util.ArrayList<>();
                            obdPidToJ1939ParameterMapping.put(java.lang.Integer.valueOf(obd2Pid), parameters);
                        }
                        parameters.add(parameter);
                    }
                }
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                        java.io.BufferedReader bufferedReader2 = bufferedReader;
                    } catch (java.io.IOException e) {
                        android.util.Log.d(TAG, "IOException closing the reader", e);
                        java.io.BufferedReader bufferedReader3 = bufferedReader;
                    }
                } else {
                    java.io.BufferedReader reader2 = bufferedReader;
                }
            } catch (java.lang.RuntimeException e2) {
                e = e2;
                reader = bufferedReader;
            } catch (java.io.IOException e3) {
                e = e3;
                reader = bufferedReader;
                android.util.Log.d(TAG, "IOException during loading the profile", e);
                if (reader == null) {
                }
            } catch (Throwable th) {
                th = th;
                reader = bufferedReader;
                if (reader != null) {
                }
                throw th;
            }
        } catch (java.lang.RuntimeException e4) {
            e = e4;
            try {
                android.util.Log.d(TAG, "RunTimeException during loading the profile", e);
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (java.io.IOException e5) {
                        android.util.Log.d(TAG, "IOException closing the reader", e5);
                    }
                }
            } catch (Throwable th2) {
                th = th2;
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (java.io.IOException e6) {
                        android.util.Log.d(TAG, "IOException closing the reader", e6);
                    }
                }
                throw th;
            }
        } catch (java.io.IOException e7) {
            e = e7;
            android.util.Log.d(TAG, "IOException during loading the profile", e);
            if (reader == null) {
                try {
                    reader.close();
                } catch (java.io.IOException e8) {
                    android.util.Log.d(TAG, "IOException closing the reader", e8);
                }
            }
        }
    }
}
