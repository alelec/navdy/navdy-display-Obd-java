package com.navdy.obd.j1939;

public class J1939ObdJobAdapter implements java.lang.Runnable, com.navdy.obd.j1939.MonitorJ1939DataJob.IDataObserver {
    public static com.navdy.obd.PidSet J1939SupportedObdPids = new com.navdy.obd.PidSet();
    public static final java.lang.String TAG = com.navdy.obd.ObdService.class.getSimpleName();
    private com.navdy.obd.io.IChannel channel;
    private com.navdy.obd.j1939.J1939ObdJobAdapter.J1939ObdJobListener listener;
    private com.navdy.obd.j1939.MonitorJ1939DataJob monitorJ1939DataJob;
    private java.util.List<com.navdy.obd.Pid> monitoredPidsList;
    private com.navdy.obd.ObdDataObserver obdDataObserver;
    public java.util.HashMap<java.lang.Long, com.navdy.obd.Pid> parameterToPidMapping;
    private com.navdy.obd.j1939.J1939Profile profile;
    private boolean started = false;
    private com.navdy.obd.VehicleStateManager vehicleStateManager;

    public interface J1939ObdJobListener {
        void onNewDataAvailable();
    }

    static {
        J1939SupportedObdPids.add(13);
        J1939SupportedObdPids.add(12);
        J1939SupportedObdPids.add(5);
        J1939SupportedObdPids.add(47);
        J1939SupportedObdPids.add(256);
        J1939SupportedObdPids.add(49);
        J1939SupportedObdPids.add((int) com.navdy.obd.Pids.ENGINE_OIL_PRESSURE);
        J1939SupportedObdPids.add((int) com.navdy.obd.Pids.ENGINE_TRIP_FUEL);
        J1939SupportedObdPids.add((int) com.navdy.obd.Pids.TOTAL_VEHICLE_DISTANCE);
    }

    public void onParameterGroupData(int pgn, java.util.List<com.navdy.obd.j1939.Parameter> parameters) {
        for (com.navdy.obd.j1939.Parameter params : parameters) {
            com.navdy.obd.Pid pid = (com.navdy.obd.Pid) this.parameterToPidMapping.get(java.lang.Long.valueOf(params.spn));
            byte[] data = params.getRecentData();
            if (data != null) {
                int dataLength = params.lengthInBits >= 8 ? params.lengthInBits / 8 : 1;
                if (dataLength >= 1) {
                    double value = (params.resolution * ((double) com.navdy.obd.Utility.bytesToInt(data, 0, dataLength, false))) + ((double) params.valueOffset);
                    pid.setTimeStamp(android.os.SystemClock.elapsedRealtime());
                    pid.setValue(value);
                }
            }
        }
        if (!this.started) {
            this.started = true;
            this.vehicleStateManager.update(this.monitoredPidsList);
        }
        this.vehicleStateManager.onScanComplete();
        if (this.listener != null) {
            this.listener.onNewDataAvailable();
        }
    }

    public java.util.List<com.navdy.obd.Pid> getMonitoredPidsList() {
        return this.monitoredPidsList;
    }

    public J1939ObdJobAdapter(com.navdy.obd.io.IChannel channel2, com.navdy.obd.j1939.J1939Profile profile2, java.util.List<com.navdy.obd.Pid> pids, com.navdy.obd.VehicleStateManager vehicleStateManager2, com.navdy.obd.ObdDataObserver logger) {
        this.obdDataObserver = logger;
        this.parameterToPidMapping = new java.util.HashMap<>();
        this.monitoredPidsList = pids;
        this.vehicleStateManager = vehicleStateManager2;
        this.channel = channel2;
        java.util.List<com.navdy.obd.j1939.Parameter> parametersToMonitor = new java.util.ArrayList<>();
        for (com.navdy.obd.Pid pid : pids) {
            java.util.List<com.navdy.obd.j1939.Parameter> parameters = profile2.getParameters(pid.getId());
            if (parameters != null) {
                parametersToMonitor.addAll(parameters);
                for (com.navdy.obd.j1939.Parameter param : parameters) {
                    this.parameterToPidMapping.put(java.lang.Long.valueOf(param.spn), pid);
                }
            }
        }
        this.monitorJ1939DataJob = new com.navdy.obd.j1939.MonitorJ1939DataJob(channel2, parametersToMonitor, this.obdDataObserver, this);
    }

    public void setListener(com.navdy.obd.j1939.J1939ObdJobAdapter.J1939ObdJobListener listener2) {
        this.listener = listener2;
    }

    public void run() {
        this.monitorJ1939DataJob.run();
        android.util.Log.d(TAG, "Monitoring finished, disconnecting the channel");
        if (this.channel != null) {
            this.channel.disconnect();
        }
    }
}
