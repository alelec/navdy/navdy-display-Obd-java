package com.navdy.obd.j1939;

public class Parameter {
    int bitOffset;
    int byteOffset;
    int lengthInBits;
    double maxValue;
    double minValue;
    public java.lang.String name;
    public int pgn;
    byte[] recentData;
    double resolution;
    public long spn;
    long valueOffset;

    public Parameter(java.lang.String name2, long spn2, int pgn2, int byteOffset2, int bitOffset2, int lengthInBits2, long valueOffset2, double resolution2, double minValue2, double maxValue2) {
        this.name = name2;
        this.spn = spn2;
        this.pgn = pgn2;
        this.byteOffset = byteOffset2;
        this.bitOffset = bitOffset2;
        this.lengthInBits = lengthInBits2;
        this.resolution = resolution2;
        this.valueOffset = valueOffset2;
        this.minValue = minValue2;
        this.maxValue = maxValue2;
    }

    public byte[] getRecentData() {
        return this.recentData;
    }

    public void setRecentData(byte[] recentData2) {
        this.recentData = recentData2;
    }

    public java.lang.String toString() {
        return "Parameter { name=" + this.name + "," + "\nSPN = " + this.spn + "," + "\nPGN = " + this.pgn + "," + "\nByteOffset = " + this.byteOffset + "," + "\nBitOffset = " + this.bitOffset + "," + "\nLength in bits = " + this.lengthInBits + "," + "\nResolution = " + this.resolution + "," + "\nValue offset = " + this.valueOffset + "," + "\nMin = " + this.minValue + "," + "\nMax = " + this.maxValue + "," + "}";
    }
}
