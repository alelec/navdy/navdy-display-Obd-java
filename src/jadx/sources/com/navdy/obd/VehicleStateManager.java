package com.navdy.obd;

public class VehicleStateManager {
    public static final java.lang.String TAG = com.navdy.obd.VehicleStateManager.class.getSimpleName();
    private com.navdy.obd.PidLookupTable mObdSnapshot = new com.navdy.obd.PidLookupTable(320);
    private com.navdy.obd.PidProcessorFactory mPidProcessorFactory;
    private android.util.SparseArray<com.navdy.obd.PidProcessor> mPidProcessors = new android.util.SparseArray<>(64);

    public void updateSupportedPids(com.navdy.obd.PidSet supportedPids) {
        if (this.mPidProcessorFactory != null) {
            for (java.lang.Integer pid : this.mPidProcessorFactory.getPidsHavingProcessors()) {
                com.navdy.obd.PidProcessor processor = this.mPidProcessorFactory.buildPidProcessorForPid(pid.intValue());
                if (processor != null && processor.isSupported(supportedPids)) {
                    this.mPidProcessors.append(pid.intValue(), processor);
                    if (!supportedPids.contains(pid.intValue())) {
                        supportedPids.add(pid.intValue());
                    }
                }
            }
        }
    }

    public VehicleStateManager(com.navdy.obd.PidProcessorFactory factory) {
        this.mPidProcessorFactory = factory;
    }

    public com.navdy.obd.PidLookupTable getObdSnapshot() {
        return this.mObdSnapshot;
    }

    public void update(java.util.List<com.navdy.obd.Pid> pidList) {
        this.mObdSnapshot.build(pidList);
    }

    public void onScanComplete() {
        for (int i = 0; i < this.mPidProcessors.size(); i++) {
            ((com.navdy.obd.PidProcessor) this.mPidProcessors.valueAt(i)).processPidValue(this.mObdSnapshot);
        }
    }

    public void resolveDependenciesForCustomPids(com.navdy.obd.ScanSchedule schedule) {
        if (schedule != null) {
            com.navdy.obd.ScanSchedule customPids = new com.navdy.obd.ScanSchedule();
            for (java.util.Map.Entry<java.lang.Integer, com.navdy.obd.ScanSchedule.Scan> entry : schedule.schedule.entrySet()) {
                com.navdy.obd.ScanSchedule.Scan scan = (com.navdy.obd.ScanSchedule.Scan) entry.getValue();
                if (scan.pid >= 256) {
                    com.navdy.obd.PidProcessor processor = (com.navdy.obd.PidProcessor) this.mPidProcessors.get(scan.pid);
                    if (processor != null) {
                        com.navdy.obd.PidSet dependencies = processor.resolveDependencies();
                        if (dependencies != null) {
                            customPids.addPids(dependencies.asList(), scan.scanInterval);
                        }
                    }
                }
            }
            if (customPids.size() > 0) {
                schedule.merge(customPids);
            }
        }
    }
}
