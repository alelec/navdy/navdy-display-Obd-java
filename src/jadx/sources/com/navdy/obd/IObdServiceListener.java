package com.navdy.obd;

public interface IObdServiceListener extends android.os.IInterface {

    public static abstract class Stub extends android.os.Binder implements com.navdy.obd.IObdServiceListener {
        private static final java.lang.String DESCRIPTOR = "com.navdy.obd.IObdServiceListener";
        static final int TRANSACTION_onConnectionStateChange = 1;
        static final int TRANSACTION_onRawData = 3;
        static final int TRANSACTION_onStatusMessage = 2;
        static final int TRANSACTION_scannedPids = 4;
        static final int TRANSACTION_scannedVIN = 6;
        static final int TRANSACTION_supportedPids = 5;

        private static class Proxy implements com.navdy.obd.IObdServiceListener {
            private android.os.IBinder mRemote;

            Proxy(android.os.IBinder remote) {
                this.mRemote = remote;
            }

            public android.os.IBinder asBinder() {
                return this.mRemote;
            }

            public java.lang.String getInterfaceDescriptor() {
                return com.navdy.obd.IObdServiceListener.Stub.DESCRIPTOR;
            }

            public void onConnectionStateChange(int newState) throws android.os.RemoteException {
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.obd.IObdServiceListener.Stub.DESCRIPTOR);
                    _data.writeInt(newState);
                    this.mRemote.transact(1, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void onStatusMessage(java.lang.String message) throws android.os.RemoteException {
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.obd.IObdServiceListener.Stub.DESCRIPTOR);
                    _data.writeString(message);
                    this.mRemote.transact(2, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void onRawData(java.lang.String data) throws android.os.RemoteException {
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.obd.IObdServiceListener.Stub.DESCRIPTOR);
                    _data.writeString(data);
                    this.mRemote.transact(3, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void scannedPids(java.util.List<com.navdy.obd.Pid> pids) throws android.os.RemoteException {
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.obd.IObdServiceListener.Stub.DESCRIPTOR);
                    _data.writeTypedList(pids);
                    this.mRemote.transact(4, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void supportedPids(java.util.List<com.navdy.obd.Pid> pids) throws android.os.RemoteException {
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.obd.IObdServiceListener.Stub.DESCRIPTOR);
                    _data.writeTypedList(pids);
                    this.mRemote.transact(5, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void scannedVIN(java.lang.String vin) throws android.os.RemoteException {
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.obd.IObdServiceListener.Stub.DESCRIPTOR);
                    _data.writeString(vin);
                    this.mRemote.transact(6, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }
        }

        public Stub() {
            attachInterface(this, DESCRIPTOR);
        }

        public static com.navdy.obd.IObdServiceListener asInterface(android.os.IBinder obj) {
            if (obj == null) {
                return null;
            }
            android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
            if (iin == null || !(iin instanceof com.navdy.obd.IObdServiceListener)) {
                return new com.navdy.obd.IObdServiceListener.Stub.Proxy(obj);
            }
            return (com.navdy.obd.IObdServiceListener) iin;
        }

        public android.os.IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException {
            switch (code) {
                case 1:
                    data.enforceInterface(DESCRIPTOR);
                    onConnectionStateChange(data.readInt());
                    reply.writeNoException();
                    return true;
                case 2:
                    data.enforceInterface(DESCRIPTOR);
                    onStatusMessage(data.readString());
                    reply.writeNoException();
                    return true;
                case 3:
                    data.enforceInterface(DESCRIPTOR);
                    onRawData(data.readString());
                    reply.writeNoException();
                    return true;
                case 4:
                    data.enforceInterface(DESCRIPTOR);
                    scannedPids(data.createTypedArrayList(com.navdy.obd.Pid.CREATOR));
                    reply.writeNoException();
                    return true;
                case 5:
                    data.enforceInterface(DESCRIPTOR);
                    supportedPids(data.createTypedArrayList(com.navdy.obd.Pid.CREATOR));
                    reply.writeNoException();
                    return true;
                case 6:
                    data.enforceInterface(DESCRIPTOR);
                    scannedVIN(data.readString());
                    reply.writeNoException();
                    return true;
                case 1598968902:
                    reply.writeString(DESCRIPTOR);
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    void onConnectionStateChange(int i) throws android.os.RemoteException;

    void onRawData(java.lang.String str) throws android.os.RemoteException;

    void onStatusMessage(java.lang.String str) throws android.os.RemoteException;

    void scannedPids(java.util.List<com.navdy.obd.Pid> list) throws android.os.RemoteException;

    void scannedVIN(java.lang.String str) throws android.os.RemoteException;

    void supportedPids(java.util.List<com.navdy.obd.Pid> list) throws android.os.RemoteException;
}
