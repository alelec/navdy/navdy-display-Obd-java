package com.navdy.obd;

public class SpeedPidProcessor extends com.navdy.obd.PidProcessor {
    public boolean isSupported(com.navdy.obd.PidSet supportedPids) {
        return supportedPids != null && supportedPids.contains(13);
    }

    public boolean processPidValue(com.navdy.obd.PidLookupTable vehicleState) {
        if (vehicleState.getPidValue(13) == 255.0d) {
            return vehicleState.updatePid(13, -2.147483648E9d);
        }
        return false;
    }

    public com.navdy.obd.PidSet resolveDependencies() {
        return null;
    }
}
