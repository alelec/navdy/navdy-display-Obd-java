package com.navdy.obd;

public interface IPidListener extends android.os.IInterface {

    public static abstract class Stub extends android.os.Binder implements com.navdy.obd.IPidListener {
        private static final java.lang.String DESCRIPTOR = "com.navdy.obd.IPidListener";
        static final int TRANSACTION_onConnectionStateChange = 2;
        static final int TRANSACTION_pidsChanged = 1;
        static final int TRANSACTION_pidsRead = 3;

        private static class Proxy implements com.navdy.obd.IPidListener {
            private android.os.IBinder mRemote;

            Proxy(android.os.IBinder remote) {
                this.mRemote = remote;
            }

            public android.os.IBinder asBinder() {
                return this.mRemote;
            }

            public java.lang.String getInterfaceDescriptor() {
                return com.navdy.obd.IPidListener.Stub.DESCRIPTOR;
            }

            public void pidsChanged(java.util.List<com.navdy.obd.Pid> pids) throws android.os.RemoteException {
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.obd.IPidListener.Stub.DESCRIPTOR);
                    _data.writeTypedList(pids);
                    this.mRemote.transact(1, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void onConnectionStateChange(int newState) throws android.os.RemoteException {
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.obd.IPidListener.Stub.DESCRIPTOR);
                    _data.writeInt(newState);
                    this.mRemote.transact(2, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void pidsRead(java.util.List<com.navdy.obd.Pid> readPids, java.util.List<com.navdy.obd.Pid> changedPids) throws android.os.RemoteException {
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.obd.IPidListener.Stub.DESCRIPTOR);
                    _data.writeTypedList(readPids);
                    _data.writeTypedList(changedPids);
                    this.mRemote.transact(3, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }
        }

        public Stub() {
            attachInterface(this, DESCRIPTOR);
        }

        public static com.navdy.obd.IPidListener asInterface(android.os.IBinder obj) {
            if (obj == null) {
                return null;
            }
            android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
            if (iin == null || !(iin instanceof com.navdy.obd.IPidListener)) {
                return new com.navdy.obd.IPidListener.Stub.Proxy(obj);
            }
            return (com.navdy.obd.IPidListener) iin;
        }

        public android.os.IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException {
            switch (code) {
                case 1:
                    data.enforceInterface(DESCRIPTOR);
                    pidsChanged(data.createTypedArrayList(com.navdy.obd.Pid.CREATOR));
                    reply.writeNoException();
                    return true;
                case 2:
                    data.enforceInterface(DESCRIPTOR);
                    onConnectionStateChange(data.readInt());
                    reply.writeNoException();
                    return true;
                case 3:
                    data.enforceInterface(DESCRIPTOR);
                    pidsRead(data.createTypedArrayList(com.navdy.obd.Pid.CREATOR), data.createTypedArrayList(com.navdy.obd.Pid.CREATOR));
                    reply.writeNoException();
                    return true;
                case 1598968902:
                    reply.writeString(DESCRIPTOR);
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    void onConnectionStateChange(int i) throws android.os.RemoteException;

    void pidsChanged(java.util.List<com.navdy.obd.Pid> list) throws android.os.RemoteException;

    void pidsRead(java.util.List<com.navdy.obd.Pid> list, java.util.List<com.navdy.obd.Pid> list2) throws android.os.RemoteException;
}
