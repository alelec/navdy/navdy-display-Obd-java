package com.navdy.obd;

public class ObdScanJob extends com.navdy.obd.ObdJob {
    private com.navdy.obd.ObdScanJob.IRawData listener;
    private boolean shuttingDown = false;

    public interface IRawData {
        void onRawData(java.lang.String str);
    }

    public ObdScanJob(com.navdy.obd.io.IChannel channel, com.navdy.obd.Protocol protocol, com.navdy.obd.ObdScanJob.IRawData listener2, com.navdy.obd.command.IObdDataObserver commandObserver) {
        super(com.navdy.obd.command.ObdCommand.SCAN_COMMAND, channel, protocol, commandObserver);
        this.listener = listener2;
    }

    public void cancel() {
        this.shuttingDown = true;
    }

    public void run() {
        Log.debug("Staring scan");
        java.io.InputStream input = null;
        try {
            input = this.channel.getInputStream();
            this.command.execute(input, this.channel.getOutputStream(), this.protocol, this.commandObserver);
        } catch (java.io.IOException e) {
            Log.error("Failed executing command {}", (java.lang.Object) this.command.getName());
        }
        if (input != null) {
            byte[] buffer = new byte[80];
            int bytesRead = 0;
            while (!this.shuttingDown && bytesRead >= 0) {
                try {
                    if (input.available() > 0) {
                        bytesRead = input.read(buffer);
                        if (bytesRead > 0) {
                            java.lang.String data = new java.lang.String(buffer, 0, bytesRead);
                            Log.debug("Scanned - {}", (java.lang.Object) data);
                            this.listener.onRawData(data);
                        }
                    } else {
                        java.lang.Thread.sleep(10);
                    }
                } catch (java.io.IOException e2) {
                    Log.debug("Exception while scanning", (java.lang.Throwable) e2);
                } catch (java.lang.InterruptedException e3) {
                    Log.debug("Interrupted");
                }
            }
        }
    }
}
