package com.navdy.obd.discovery;

public class BluetoothObdScanner extends com.navdy.obd.discovery.ObdChannelScanner {
    private static final java.lang.String TAG = "BluetoothObdScanner";
    private android.bluetooth.BluetoothAdapter mBtAdapter = android.bluetooth.BluetoothAdapter.getDefaultAdapter();
    private final android.content.BroadcastReceiver mReceiver = new com.navdy.obd.discovery.BluetoothObdScanner.Anon2();
    private boolean scanning = false;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.obd.discovery.BluetoothObdScanner.this.enumerateBondedDevices();
        }
    }

    class Anon2 extends android.content.BroadcastReceiver {
        Anon2() {
        }

        public void onReceive(android.content.Context context, android.content.Intent intent) {
            java.lang.String action = intent.getAction();
            if ("android.bluetooth.device.action.FOUND".equals(action) || "android.bluetooth.device.action.NAME_CHANGED".equals(action)) {
                android.bluetooth.BluetoothDevice device = (android.bluetooth.BluetoothDevice) intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
                android.util.Log.d(com.navdy.obd.discovery.BluetoothObdScanner.TAG, "Friendly name = " + intent.getStringExtra("android.bluetooth.device.extra.NAME"));
                if (device.getBondState() != 12 && com.navdy.obd.discovery.BluetoothObdScanner.this.isObdDevice(device)) {
                    com.navdy.obd.discovery.BluetoothObdScanner.this.mListener.onDiscovered(com.navdy.obd.discovery.BluetoothObdScanner.this, new com.navdy.obd.io.ChannelInfo(com.navdy.obd.io.ChannelInfo.ConnectionType.BLUETOOTH, device.getName(), device.getAddress()));
                }
            } else if ("android.bluetooth.adapter.action.DISCOVERY_FINISHED".equals(action)) {
                com.navdy.obd.discovery.BluetoothObdScanner.this.mListener.onScanStopped(com.navdy.obd.discovery.BluetoothObdScanner.this);
            }
        }
    }

    public BluetoothObdScanner(android.content.Context context, com.navdy.obd.discovery.ObdChannelScanner.Listener listener) {
        super(context, listener);
    }

    public boolean startScan() {
        if (!this.scanning) {
            this.scanning = true;
            this.mContext.registerReceiver(this.mReceiver, new android.content.IntentFilter("android.bluetooth.device.action.FOUND"));
            this.mContext.registerReceiver(this.mReceiver, new android.content.IntentFilter("android.bluetooth.adapter.action.DISCOVERY_FINISHED"));
            if (this.mBtAdapter != null) {
                if (this.mBtAdapter.isDiscovering()) {
                    this.mBtAdapter.cancelDiscovery();
                }
                this.mBtAdapter.startDiscovery();
                new android.os.Handler().post(new com.navdy.obd.discovery.BluetoothObdScanner.Anon1());
            }
        }
        return this.scanning;
    }

    public boolean stopScan() {
        if (this.scanning) {
            this.scanning = false;
            if (this.mBtAdapter != null) {
                this.mBtAdapter.cancelDiscovery();
            }
            this.mContext.unregisterReceiver(this.mReceiver);
        }
        return this.scanning;
    }

    /* access modifiers changed from: private */
    public void enumerateBondedDevices() {
        java.util.Set<android.bluetooth.BluetoothDevice> pairedDevices = this.mBtAdapter.getBondedDevices();
        if (pairedDevices.size() > 0) {
            for (android.bluetooth.BluetoothDevice device : pairedDevices) {
                if (isObdDevice(device)) {
                    this.mListener.onDiscovered(this, new com.navdy.obd.io.ChannelInfo(com.navdy.obd.io.ChannelInfo.ConnectionType.BLUETOOTH, device.getName(), device.getAddress(), true));
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public boolean isObdDevice(android.bluetooth.BluetoothDevice device) {
        return device.getName() != null && device.getName().toLowerCase(java.util.Locale.US).contains("obd");
    }
}
