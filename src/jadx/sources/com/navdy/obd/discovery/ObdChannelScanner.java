package com.navdy.obd.discovery;

public abstract class ObdChannelScanner {
    protected android.content.Context mContext;
    protected com.navdy.obd.discovery.ObdChannelScanner.Listener mListener;

    public interface Listener {
        void onDiscovered(com.navdy.obd.discovery.ObdChannelScanner obdChannelScanner, com.navdy.obd.io.ChannelInfo channelInfo);

        void onScanStarted(com.navdy.obd.discovery.ObdChannelScanner obdChannelScanner);

        void onScanStopped(com.navdy.obd.discovery.ObdChannelScanner obdChannelScanner);
    }

    public abstract boolean startScan();

    public abstract boolean stopScan();

    ObdChannelScanner(android.content.Context context, com.navdy.obd.discovery.ObdChannelScanner.Listener listener) {
        this.mContext = context;
        this.mListener = listener;
    }
}
