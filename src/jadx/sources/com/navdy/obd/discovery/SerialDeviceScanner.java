package com.navdy.obd.discovery;

public class SerialDeviceScanner extends com.navdy.obd.discovery.ObdChannelScanner {
    private static final java.lang.String SERIAL_DEVICE = "/dev/ttymxc3";
    private static final java.lang.String[] SERIAL_PORTS = {SERIAL_DEVICE};
    private static final java.lang.String TAG = "SerialDeviceScanner";

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.obd.discovery.SerialDeviceScanner.this.findDevices();
        }
    }

    public SerialDeviceScanner(android.content.Context context, com.navdy.obd.discovery.ObdChannelScanner.Listener listener) {
        super(context, listener);
    }

    public boolean startScan() {
        this.mListener.onScanStarted(this);
        new android.os.Handler().post(new com.navdy.obd.discovery.SerialDeviceScanner.Anon1());
        return true;
    }

    public boolean stopScan() {
        this.mListener.onScanStopped(this);
        return true;
    }

    /* access modifiers changed from: private */
    public void findDevices() {
        java.lang.String[] ports = SERIAL_PORTS;
        for (java.lang.String channelInfo : ports) {
            this.mListener.onDiscovered(this, new com.navdy.obd.io.ChannelInfo(com.navdy.obd.io.ChannelInfo.ConnectionType.SERIAL, "SerialPort", channelInfo));
        }
    }
}
