package com.navdy.obd.discovery;

public class MockObdScanner extends com.navdy.obd.discovery.ObdChannelScanner {

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.obd.discovery.MockObdScanner.this.mListener.onDiscovered(com.navdy.obd.discovery.MockObdScanner.this, com.navdy.obd.simulator.MockObdChannel.MOCK_CHANNEL_INFO);
        }
    }

    public MockObdScanner(android.content.Context context, com.navdy.obd.discovery.ObdChannelScanner.Listener listener) {
        super(context, listener);
    }

    public boolean startScan() {
        this.mListener.onScanStarted(this);
        new android.os.Handler().post(new com.navdy.obd.discovery.MockObdScanner.Anon1());
        return true;
    }

    public boolean stopScan() {
        this.mListener.onScanStopped(this);
        return true;
    }
}
