package com.navdy.obd.discovery;

public class GroupScanner extends com.navdy.obd.discovery.ObdChannelScanner implements com.navdy.obd.discovery.ObdChannelScanner.Listener {
    public static final java.lang.String DEFAULT_CHANNELS = "bluetooth,serial";
    public static final java.lang.String DEFAULT_CHANNELS_PROPERTY_NAME = "obd.channels";
    public static final java.lang.String DEFAULT_HUD_CHANNELS = "serial";
    private static final java.lang.String TAG = "GroupScanner";
    java.util.HashSet<com.navdy.obd.io.ChannelInfo> discoveredChannels = new java.util.HashSet<>();
    private java.util.List<com.navdy.obd.discovery.ObdChannelScanner> scanners = new java.util.ArrayList();

    public GroupScanner(android.content.Context context, com.navdy.obd.discovery.ObdChannelScanner.Listener listener) {
        super(context, listener);
        for (java.lang.String channelType : android.text.TextUtils.split(com.navdy.os.SystemProperties.get(DEFAULT_CHANNELS_PROPERTY_NAME, defaultChannels()), ",")) {
            com.navdy.obd.discovery.ObdChannelScanner scanner = instantiateScanner(context, channelType.trim());
            if (scanner != null) {
                this.scanners.add(scanner);
            }
        }
    }

    private com.navdy.obd.discovery.ObdChannelScanner instantiateScanner(android.content.Context context, java.lang.String channelType) {
        com.navdy.obd.io.ChannelInfo.ConnectionType type = null;
        if (!android.text.TextUtils.isEmpty(channelType)) {
            try {
                type = com.navdy.obd.io.ChannelInfo.ConnectionType.valueOf(channelType.toUpperCase(java.util.Locale.US));
            } catch (java.lang.IllegalArgumentException e) {
                android.util.Log.e(TAG, "Failed to parse channelType " + channelType, e);
            }
        }
        if (type == null) {
            return null;
        }
        switch (type) {
            case BLUETOOTH:
                return new com.navdy.obd.discovery.BluetoothObdScanner(context, this);
            case MOCK:
                return new com.navdy.obd.discovery.MockObdScanner(context, this);
            case SERIAL:
                return new com.navdy.obd.discovery.SerialDeviceScanner(context, this);
            default:
                return null;
        }
    }

    private java.lang.String defaultChannels() {
        if (android.os.Build.MODEL.equalsIgnoreCase("NAVDY_HUD-MX6DL") || android.os.Build.MODEL.equalsIgnoreCase("Display")) {
            return "serial";
        }
        return DEFAULT_CHANNELS;
    }

    public java.util.Set<com.navdy.obd.io.ChannelInfo> getDiscoveredChannels() {
        return this.discoveredChannels;
    }

    public boolean startScan() {
        this.discoveredChannels.clear();
        for (com.navdy.obd.discovery.ObdChannelScanner scanner : this.scanners) {
            scanner.startScan();
        }
        return true;
    }

    public boolean stopScan() {
        for (com.navdy.obd.discovery.ObdChannelScanner scanner : this.scanners) {
            scanner.stopScan();
        }
        return true;
    }

    public void onScanStarted(com.navdy.obd.discovery.ObdChannelScanner scanner) {
        if (this.mListener != null) {
            this.mListener.onScanStarted(this);
        }
    }

    public void onScanStopped(com.navdy.obd.discovery.ObdChannelScanner scanner) {
        if (this.mListener != null) {
            this.mListener.onScanStopped(this);
        }
    }

    public void onDiscovered(com.navdy.obd.discovery.ObdChannelScanner scanner, com.navdy.obd.io.ChannelInfo obdDevice) {
        this.discoveredChannels.add(obdDevice);
        if (this.mListener != null) {
            this.mListener.onDiscovered(scanner, obdDevice);
        }
    }
}
