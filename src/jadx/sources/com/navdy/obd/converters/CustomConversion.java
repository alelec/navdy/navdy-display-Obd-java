package com.navdy.obd.converters;

public class CustomConversion implements com.navdy.obd.converters.AbstractConversion {
    private parsii.eval.Expression expression;
    private com.navdy.obd.converters.CustomConversion.VariableOffset[] offsets;

    static class VariableOffset {
        /* access modifiers changed from: private */
        public int offset;
        /* access modifiers changed from: private */
        public parsii.eval.Variable variable;

        public VariableOffset(parsii.eval.Variable variable2, int offset2) {
            this.variable = variable2;
            this.offset = offset2;
        }
    }

    public CustomConversion(java.lang.String expression2) throws parsii.tokenizer.ParseException {
        parsii.eval.Scope scope = parsii.eval.Scope.create();
        this.expression = parsii.eval.Parser.parse(expression2, scope);
        java.util.Collection<parsii.eval.Variable> variables = scope.getLocalVariables();
        this.offsets = new com.navdy.obd.converters.CustomConversion.VariableOffset[variables.size()];
        int i = 0;
        for (parsii.eval.Variable variable : variables) {
            int offset = offset(variable.getName());
            if (offset < 0) {
                java.util.List<parsii.tokenizer.ParseError> errors = new java.util.ArrayList<>();
                errors.add(parsii.tokenizer.ParseError.error(parsii.tokenizer.Position.UNKNOWN, "Invalid variable " + variable.getName() + " in (" + expression2 + ")"));
                throw parsii.tokenizer.ParseException.create(errors);
            }
            int i2 = i + 1;
            this.offsets[i] = new com.navdy.obd.converters.CustomConversion.VariableOffset(variable, offset);
            i = i2;
        }
    }

    public double convert(byte[] rawData) {
        com.navdy.obd.converters.CustomConversion.VariableOffset[] variableOffsetArr;
        for (com.navdy.obd.converters.CustomConversion.VariableOffset var : this.offsets) {
            if (var.variable != null) {
                var.variable.setValue((double) (rawData[var.offset + 2] & com.navdy.obd.update.STNBootloaderChannel.ERROR_CODE_INVALID_RESPONSE));
            }
        }
        return this.expression.evaluate();
    }

    private int offset(java.lang.String variableName) {
        boolean twoDigit;
        int tens;
        int i = 1;
        if (variableName == null || variableName.length() > 2 || variableName.length() == 0) {
            return -1;
        }
        java.lang.String variableName2 = variableName.toUpperCase();
        if (variableName2.length() > 1) {
            twoDigit = true;
        } else {
            twoDigit = false;
        }
        if (twoDigit) {
            tens = ((variableName2.charAt(0) - 'A') + 1) * 26;
        } else {
            tens = 0;
        }
        if (!twoDigit) {
            i = 0;
        }
        return tens + (variableName2.charAt(i) - 'A');
    }
}
