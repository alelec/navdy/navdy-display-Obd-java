package com.navdy.obd.converters;

public class TemperatureConversion implements com.navdy.obd.converters.AbstractConversion {
    public double convert(byte[] rawData) {
        if (rawData != null) {
            return (double) ((rawData[2] & com.navdy.obd.update.STNBootloaderChannel.ERROR_CODE_INVALID_RESPONSE) - 40);
        }
        return 0.0d;
    }
}
