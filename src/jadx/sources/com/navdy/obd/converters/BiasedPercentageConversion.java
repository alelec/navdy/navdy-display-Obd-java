package com.navdy.obd.converters;

public class BiasedPercentageConversion extends com.navdy.obd.converters.LinearCombinationConversion {
    public BiasedPercentageConversion() {
        super(100, -128, 0, 128.0d);
    }
}
