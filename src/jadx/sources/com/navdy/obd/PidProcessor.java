package com.navdy.obd;

public abstract class PidProcessor {
    public abstract boolean isSupported(com.navdy.obd.PidSet pidSet);

    public abstract boolean processPidValue(com.navdy.obd.PidLookupTable pidLookupTable);

    public abstract com.navdy.obd.PidSet resolveDependencies();
}
