package com.navdy.obd.io;

public class ChannelInfo {
    private static final java.lang.String SEPARATOR = "|";
    private static final java.lang.String SEPARATOR_REGEX = "\\|";
    private java.lang.String address;
    private boolean bonded;
    private com.navdy.obd.io.ChannelInfo.ConnectionType connectionType;
    private java.lang.String name;

    public enum ConnectionType {
        SERIAL,
        BLUETOOTH,
        MOCK,
        NONE
    }

    public ChannelInfo(com.navdy.obd.io.ChannelInfo.ConnectionType connectionType2, java.lang.String name2, java.lang.String address2) {
        this(connectionType2, name2, address2, false);
    }

    public ChannelInfo(com.navdy.obd.io.ChannelInfo.ConnectionType connectionType2, java.lang.String name2, java.lang.String address2, boolean bonded2) {
        this.bonded = false;
        this.connectionType = connectionType2;
        this.name = name2;
        this.address = address2;
        this.bonded = bonded2;
    }

    public static com.navdy.obd.io.ChannelInfo parse(java.lang.String string) {
        if (string == null) {
            return null;
        }
        java.lang.String[] parts = string.split(SEPARATOR_REGEX);
        if (parts.length == 3) {
            return new com.navdy.obd.io.ChannelInfo(com.navdy.obd.io.ChannelInfo.ConnectionType.valueOf(parts[0]), parts[1], parts[2]);
        }
        return null;
    }

    public com.navdy.obd.io.ChannelInfo.ConnectionType getConnectionType() {
        return this.connectionType;
    }

    public java.lang.String getName() {
        return this.name;
    }

    public java.lang.String getAddress() {
        return this.address;
    }

    public boolean isBonded() {
        return this.bonded;
    }

    public java.lang.String asString() {
        return this.connectionType + SEPARATOR + this.name + SEPARATOR + this.address;
    }

    public java.lang.String toString() {
        return this.name + "(" + this.connectionType + ")\n" + this.address;
    }

    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        com.navdy.obd.io.ChannelInfo that = (com.navdy.obd.io.ChannelInfo) o;
        if (this.connectionType != that.connectionType || !this.address.equals(that.address)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (this.connectionType.hashCode() * 31) + this.address.hashCode();
    }
}
