package com.navdy.obd.io;

public class SerialChannel implements com.navdy.obd.io.IChannel {
    public static final int DEFAULT_BAUD_RATE = 9600;
    private static final java.lang.String DEFAULT_BAUD_RATE_PROP = "persist.sys.obd.baudrate";
    public static final java.lang.String SERIAL_SERIVCE = "serial";
    private static final java.lang.String TAG = "SerialChannel";
    private int baudRate;
    private final android.content.Context context;
    private com.navdy.hardware.SerialPort serialPort;
    private final com.navdy.obd.io.IChannelSink sink;
    private int state;

    private static class SerialInputStream extends java.io.InputStream {
        com.navdy.hardware.SerialPort serialPort;

        public SerialInputStream(com.navdy.hardware.SerialPort serialPort2) {
            this.serialPort = serialPort2;
        }

        public int read() throws java.io.IOException {
            java.nio.ByteBuffer buffer = java.nio.ByteBuffer.allocate(1);
            int result = this.serialPort.read(buffer);
            if (result < 0) {
                return result;
            }
            if (result == 0) {
                return -2;
            }
            return buffer.get() & 255;
        }

        public int read(byte[] b) throws java.io.IOException {
            java.nio.ByteBuffer buffer = java.nio.ByteBuffer.allocate(b.length);
            int length = this.serialPort.read(buffer);
            if (length > 0) {
                buffer.get(b, 0, length);
            }
            return length;
        }
    }

    private static class SerialOutputStream extends java.io.OutputStream {
        com.navdy.hardware.SerialPort serialPort;

        public SerialOutputStream(com.navdy.hardware.SerialPort serialPort2) {
            this.serialPort = serialPort2;
        }

        public void write(int oneByte) throws java.io.IOException {
            this.serialPort.write(java.nio.ByteBuffer.wrap(new byte[]{(byte) oneByte}), 1);
        }

        public void write(byte[] buffer, int offset, int count) throws java.io.IOException {
            this.serialPort.write(java.nio.ByteBuffer.wrap(buffer, offset, count), count);
        }

        public void write(byte[] b) throws java.io.IOException {
            this.serialPort.write(java.nio.ByteBuffer.wrap(b), b.length);
        }
    }

    public SerialChannel(android.content.Context context2, com.navdy.obd.io.IChannelSink sink2) {
        this(context2, sink2, DEFAULT_BAUD_RATE);
    }

    public SerialChannel(android.content.Context context2, com.navdy.obd.io.IChannelSink sink2, int baudRate2) {
        this.state = 0;
        this.baudRate = DEFAULT_BAUD_RATE;
        this.context = context2;
        this.sink = sink2;
        this.baudRate = baudRate2;
    }

    public int getState() {
        return this.state;
    }

    private void setState(int newState) {
        if (newState != this.state) {
            this.state = newState;
            if (this.sink != null) {
                this.sink.onStateChange(newState);
            }
        }
    }

    public void connect(java.lang.String address) {
        try {
            this.serialPort = new com.navdy.hardware.SerialPort(address);
            if (this.serialPort.open(this.baudRate)) {
                setState(2);
            }
        } catch (java.io.IOException e) {
            android.util.Log.e(TAG, "Failed to get fd for " + address, e);
        }
    }

    public void disconnect() {
        try {
            if (this.serialPort != null) {
                this.serialPort.close();
            }
            setState(0);
        } catch (java.io.IOException e) {
            android.util.Log.e(TAG, "Failed to close underlying file", e);
        }
    }

    public java.io.InputStream getInputStream() throws java.io.IOException {
        return new com.navdy.obd.io.SerialChannel.SerialInputStream(this.serialPort);
    }

    public java.io.OutputStream getOutputStream() throws java.io.IOException {
        return new com.navdy.obd.io.SerialChannel.SerialOutputStream(this.serialPort);
    }

    public com.navdy.hardware.SerialPort getSerialPort() {
        return this.serialPort;
    }
}
