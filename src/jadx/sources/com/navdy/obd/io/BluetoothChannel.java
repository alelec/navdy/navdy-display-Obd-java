package com.navdy.obd.io;

public class BluetoothChannel implements com.navdy.obd.io.IChannel {
    private static final boolean D = true;
    /* access modifiers changed from: private */
    public static final java.util.UUID SERIAL_UUID = java.util.UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    /* access modifiers changed from: private */
    public static final java.lang.String TAG = com.navdy.obd.io.BluetoothChannel.class.getSimpleName();
    private final android.bluetooth.BluetoothAdapter mAdapter = android.bluetooth.BluetoothAdapter.getDefaultAdapter();
    /* access modifiers changed from: private */
    public com.navdy.obd.io.BluetoothChannel.ConnectThread mConnectThread;
    private final com.navdy.obd.io.IChannelSink mSink;
    private android.bluetooth.BluetoothSocket mSocket;
    private int mState = 0;

    private class ConnectThread extends java.lang.Thread {
        private final android.bluetooth.BluetoothDevice mmDevice;
        private android.bluetooth.BluetoothSocket mmSocket;

        public ConnectThread(android.bluetooth.BluetoothDevice device) {
            this.mmDevice = device;
            android.bluetooth.BluetoothSocket tmp = null;
            try {
                tmp = device.createRfcommSocketToServiceRecord(com.navdy.obd.io.BluetoothChannel.SERIAL_UUID);
            } catch (java.io.IOException e) {
                android.util.Log.e(com.navdy.obd.io.BluetoothChannel.TAG, "create() failed", e);
            }
            this.mmSocket = tmp;
        }

        public void run() {
            android.util.Log.i(com.navdy.obd.io.BluetoothChannel.TAG, "BEGIN mConnectThread");
            setName("ConnectThread");
            try {
                this.mmSocket.connect();
            } catch (java.io.IOException e) {
                android.util.Log.e(com.navdy.obd.io.BluetoothChannel.TAG, "Exception connecting", e);
                boolean fallbackFailed = com.navdy.obd.io.BluetoothChannel.D;
                try {
                    android.util.Log.e(com.navdy.obd.io.BluetoothChannel.TAG, "trying fallback...");
                    this.mmSocket = (android.bluetooth.BluetoothSocket) this.mmDevice.getClass().getMethod("createRfcommSocket", new java.lang.Class[]{java.lang.Integer.TYPE}).invoke(this.mmDevice, new java.lang.Object[]{java.lang.Integer.valueOf(1)});
                    this.mmSocket.connect();
                    fallbackFailed = false;
                    android.util.Log.e(com.navdy.obd.io.BluetoothChannel.TAG, "Connected");
                } catch (java.lang.Exception e1) {
                    android.util.Log.e(com.navdy.obd.io.BluetoothChannel.TAG, "Failed to connect using fallback", e1);
                }
                if (fallbackFailed) {
                    com.navdy.obd.io.BluetoothChannel.this.connectionFailed();
                    try {
                        this.mmSocket.close();
                        return;
                    } catch (java.io.IOException e2) {
                        android.util.Log.e(com.navdy.obd.io.BluetoothChannel.TAG, "unable to close() socket during connection failure", e2);
                        return;
                    }
                }
            }
            synchronized (com.navdy.obd.io.BluetoothChannel.this) {
                com.navdy.obd.io.BluetoothChannel.this.mConnectThread = null;
            }
            com.navdy.obd.io.BluetoothChannel.this.connected(this.mmSocket, this.mmDevice);
        }

        public void cancel() {
            try {
                this.mmSocket.close();
            } catch (java.io.IOException e) {
                android.util.Log.e(com.navdy.obd.io.BluetoothChannel.TAG, "close() of connect socket failed", e);
            }
        }
    }

    public BluetoothChannel(com.navdy.obd.io.IChannelSink mSink2) {
        this.mSink = mSink2;
    }

    private synchronized void setState(int state) {
        android.util.Log.d(TAG, "setState() " + this.mState + " -> " + state);
        this.mState = state;
        this.mSink.onStateChange(state);
    }

    public synchronized int getState() {
        return this.mState;
    }

    public synchronized void connect(java.lang.String address) {
        android.bluetooth.BluetoothDevice device = this.mAdapter.getRemoteDevice(address);
        android.util.Log.d(TAG, "connect to: " + device);
        if (this.mState == 1 && this.mConnectThread != null) {
            this.mConnectThread.cancel();
            this.mConnectThread = null;
        }
        this.mConnectThread = new com.navdy.obd.io.BluetoothChannel.ConnectThread(device);
        this.mConnectThread.start();
        setState(1);
    }

    public synchronized void disconnect() {
        stop();
    }

    public synchronized void connected(android.bluetooth.BluetoothSocket socket, android.bluetooth.BluetoothDevice device) {
        android.util.Log.d(TAG, "connected");
        if (this.mConnectThread != null) {
            this.mConnectThread.cancel();
            this.mConnectThread = null;
        }
        this.mSocket = socket;
        this.mSink.onMessage("Connected to " + device.getName());
        setState(2);
    }

    public synchronized void stop() {
        android.util.Log.d(TAG, "stop");
        if (this.mConnectThread != null) {
            this.mConnectThread.cancel();
            this.mConnectThread = null;
        }
        if (this.mSocket != null) {
            try {
                this.mSocket.close();
                this.mSocket = null;
            } catch (java.io.IOException e) {
                android.util.Log.e(TAG, "Failed to close bluetooth socket", e);
                this.mSocket = null;
            } finally {
                this.mSocket = null;
            }
        }
        setState(0);
        return;
    }

    /* access modifiers changed from: private */
    public void connectionFailed() {
        this.mSink.onMessage("Unable to connect device");
        setState(0);
    }

    public java.io.InputStream getInputStream() throws java.io.IOException {
        return this.mSocket.getInputStream();
    }

    public java.io.OutputStream getOutputStream() throws java.io.IOException {
        return this.mSocket.getOutputStream();
    }
}
