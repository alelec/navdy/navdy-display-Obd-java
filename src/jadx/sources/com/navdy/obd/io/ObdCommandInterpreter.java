package com.navdy.obd.io;

public class ObdCommandInterpreter {
    private static final int CARRIAGE_RETURN = 13;
    static final org.slf4j.Logger Log = org.slf4j.LoggerFactory.getLogger(com.navdy.obd.io.ObdCommandInterpreter.class);
    public static final long NANOS_PER_MS = 1000000;
    public static final char TERMINAL_INPUT_INDICATOR = '>';
    public boolean debug = false;
    private boolean didGetTextResponse;
    private java.io.InputStream inputStream;
    private boolean isInterrupted;
    private byte[] lastCommandBytes;
    private int lastReadCharacter = -1;
    private java.io.OutputStream outputStream;
    private long readTimeOutMillis = 10000;
    private long readTimeOutNanos = ((this.readTimeOutMillis * 1000) * 1000);

    public ObdCommandInterpreter(java.io.InputStream inputStream2, java.io.OutputStream outputStream2) {
        this.inputStream = inputStream2;
        this.outputStream = outputStream2;
    }

    public void setReadTimeOut(int readTimeOutMillis2) {
        Log.debug("ObdCommandInterpreter : setReadTimeOut {} ", (java.lang.Object) java.lang.Integer.valueOf(readTimeOutMillis2));
        this.readTimeOutMillis = (long) readTimeOutMillis2;
        this.readTimeOutNanos = this.readTimeOutMillis * 1000000;
    }

    public int read() throws java.io.IOException {
        return this.inputStream.read();
    }

    public void write(java.lang.String command) throws java.io.IOException {
        Log.debug("Command {} ", (java.lang.Object) command);
        int length = command.length();
        this.lastCommandBytes = new byte[(length + 1)];
        java.lang.System.arraycopy(command.getBytes(), 0, this.lastCommandBytes, 0, length);
        this.lastCommandBytes[length] = 13;
        this.outputStream.write(this.lastCommandBytes);
        this.outputStream.flush();
    }

    public void write(byte[] commandBytes) throws java.io.IOException {
        this.outputStream.write(commandBytes);
        this.outputStream.flush();
        this.lastCommandBytes = commandBytes;
    }

    public java.lang.String readResponse() throws java.io.IOException, java.lang.InterruptedException {
        return readResponse(true);
    }

    public java.lang.String readLine() throws java.io.IOException, java.lang.InterruptedException {
        return readResponse(false);
    }

    public int read(byte[] buffer) throws java.io.IOException, java.lang.InterruptedException {
        return this.inputStream.read(buffer);
    }

    public java.lang.String readResponse(boolean readCompleteResponse) throws java.lang.InterruptedException, java.io.IOException {
        java.lang.StringBuilder responseBuilder = new java.lang.StringBuilder();
        this.didGetTextResponse = false;
        long start = java.lang.System.nanoTime();
        long elapsedTime = 0;
        while (!java.lang.Thread.interrupted()) {
            int read = this.inputStream.read();
            this.lastReadCharacter = read;
            if (read != -1 && this.lastReadCharacter != 62) {
                elapsedTime = java.lang.System.nanoTime() - start;
                if (elapsedTime < this.readTimeOutNanos) {
                    if (this.debug) {
                        Log.debug("Character {}", (java.lang.Object) java.lang.Character.valueOf((char) this.lastReadCharacter));
                    }
                    if (this.lastReadCharacter != -2) {
                        boolean validHexDigit = (this.lastReadCharacter >= 48 && this.lastReadCharacter <= 57) || (this.lastReadCharacter >= 65 && this.lastReadCharacter <= 70) || (this.lastReadCharacter >= 97 && this.lastReadCharacter <= 102);
                        boolean isWhiteSpace = this.lastReadCharacter == 32 || this.lastReadCharacter == 13;
                        if (!validHexDigit && !isWhiteSpace) {
                            this.didGetTextResponse = true;
                        }
                        if (this.didGetTextResponse || this.lastReadCharacter != 32) {
                            responseBuilder.append((char) this.lastReadCharacter);
                        }
                        if (!readCompleteResponse && this.lastReadCharacter == 13) {
                            break;
                        }
                    } else if (java.lang.Thread.interrupted()) {
                        this.isInterrupted = true;
                        throw new java.lang.InterruptedException();
                    }
                } else {
                    break;
                }
            } else {
                break;
            }
        }
        if (java.lang.Thread.interrupted()) {
            this.isInterrupted = true;
            Log.info("readResponse, Thread is interrupted");
            throw new java.lang.InterruptedException();
        } else if (elapsedTime > this.readTimeOutNanos) {
            long elapsedTime2 = elapsedTime / 1000000;
            Log.info("ObdCommandInterpreter : readResponse, timed out");
            throw new java.io.IOException("Timeout after " + elapsedTime2 + " ms");
        } else if (!readCompleteResponse || this.lastReadCharacter == 62) {
            return responseBuilder.toString();
        } else {
            throw new java.io.IOException("ObdCommandInterpreter : Unexpected end of stream");
        }
    }

    public java.lang.String clearTerminal() throws java.io.IOException, java.lang.InterruptedException, java.util.concurrent.TimeoutException {
        return clearTerminal(false, false);
    }

    public void clearTerminalBulk() throws java.lang.InterruptedException, java.util.concurrent.TimeoutException {
        clearTerminalBulk(false, false);
    }

    public java.lang.String clearTerminal(boolean mayInterrupt, boolean timed) throws java.io.IOException, java.lang.InterruptedException, java.util.concurrent.TimeoutException {
        Log.info("ObdCommandInterpreter : clearTerminal");
        java.lang.StringBuilder responseBuilder = new java.lang.StringBuilder("");
        long start = java.lang.System.nanoTime();
        while (true) {
            int read = this.inputStream.read();
            this.lastReadCharacter = read;
            if (read == -1 || java.lang.Thread.interrupted() || (timed && java.lang.System.nanoTime() - start > this.readTimeOutNanos)) {
                break;
            } else if (this.lastReadCharacter == -2) {
                Log.debug(" ObdCommandInterpreter : terminalClear, response {}", (java.lang.Object) responseBuilder.toString().trim());
                break;
            } else if (this.lastReadCharacter >= 0) {
                responseBuilder.append(this.lastReadCharacter);
            }
        }
        long elapsedTime = java.lang.System.nanoTime() - start;
        if (timed && elapsedTime > this.readTimeOutNanos) {
            throw new java.util.concurrent.TimeoutException("Clearing terminal timed out");
        } else if (java.lang.Thread.interrupted()) {
            this.isInterrupted = true;
            Log.debug("readResponse, Thread is interrupted");
            throw new java.lang.InterruptedException();
        } else {
            Log.debug("Clearing terminal took {} ms", (java.lang.Object) java.lang.Long.valueOf(elapsedTime));
            return responseBuilder.toString();
        }
    }

    public void clearTerminalBulk(boolean mayInterrupt, boolean timed) throws java.lang.InterruptedException, java.util.concurrent.TimeoutException {
        Log.info("ObdCommandInterpreter : clearTerminal, timed {}", (java.lang.Object) java.lang.Boolean.valueOf(timed));
        long start = java.lang.System.nanoTime();
        while (true) {
            if ((mayInterrupt && java.lang.Thread.interrupted()) || (timed && java.lang.System.nanoTime() - start > this.readTimeOutNanos)) {
                break;
            }
            try {
                int length = read(new byte[ch.qos.logback.classic.Level.INFO_INT]);
                Log.debug("clearTerminal length {} ", (java.lang.Object) java.lang.Integer.valueOf(length));
                if (length <= 0) {
                    Log.debug("Terminal clear");
                    break;
                }
            } catch (java.lang.InterruptedException e) {
                Log.error("Interrupted exception {}", (java.lang.Throwable) e);
            } catch (java.io.IOException e2) {
                Log.error("IOException exception {}", (java.lang.Throwable) e2);
            }
        }
        long elapsedTime = java.lang.System.nanoTime() - start;
        if (timed && elapsedTime > this.readTimeOutNanos) {
            throw new java.util.concurrent.TimeoutException("Clearing terminal timed out");
        } else if (mayInterrupt && java.lang.Thread.interrupted()) {
        } else {
            if (!mayInterrupt || !java.lang.Thread.interrupted()) {
                Log.debug("Clearing terminal took {} ms", (java.lang.Object) java.lang.Long.valueOf(elapsedTime));
                return;
            }
            this.isInterrupted = true;
            Log.debug("readResponse, Thread is interrupted");
            throw new java.lang.InterruptedException();
        }
    }

    public boolean didGetTextResponse() {
        return this.didGetTextResponse;
    }

    public byte[] getCommandBytes() {
        return this.lastCommandBytes;
    }
}
