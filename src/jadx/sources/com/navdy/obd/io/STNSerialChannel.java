package com.navdy.obd.io;

public class STNSerialChannel implements com.navdy.obd.io.IChannel, com.navdy.obd.io.IChannelSink {
    private static final int CARRIAGE_RETURN = 13;
    public static final int CONNECT_TIMEOUT_MILLIS = 60000;
    private static final int DEFAULT_BAUD_RATE = 9600;
    static final org.slf4j.Logger Log = org.slf4j.LoggerFactory.getLogger(com.navdy.obd.io.STNSerialChannel.class);
    public static final long NANOS_PER_MS = 1000000;
    public static final java.lang.String OK = "ok";
    /* access modifiers changed from: private */
    public static final int[] POSSIBLE_BAUD_RATES_TABLE = {9600, 19200, 38400, 57600, 115200, 230400, 460800};
    public static final java.lang.String SET_BAUD_RATE_COMMAND = "STSBR";
    public static final int TIMEOUT_MILLIS = 3000;
    private static java.util.concurrent.ExecutorService singleThreadedExecutor = java.util.concurrent.Executors.newSingleThreadExecutor();
    /* access modifiers changed from: private */
    public int baudRate = 9600;
    private final android.content.Context context;
    private java.util.concurrent.Future future;
    /* access modifiers changed from: private */
    public int lastConnectedBaudRate;
    private com.navdy.obd.io.SerialChannel serialChannel;
    private final com.navdy.obd.io.IChannelSink sink;
    private volatile int state = 0;

    class Anon1 implements java.lang.Runnable {
        final /* synthetic */ java.lang.String val$address;

        Anon1(java.lang.String str) {
            this.val$address = str;
        }

        public void run() {
            int[] access$Anon300;
            com.navdy.obd.io.STNSerialChannel.this.setState(1);
            java.util.ArrayList<java.lang.Integer> possibleBaudRatesList = new java.util.ArrayList<>();
            possibleBaudRatesList.add(java.lang.Integer.valueOf(com.navdy.obd.io.STNSerialChannel.this.lastConnectedBaudRate));
            if (com.navdy.obd.io.STNSerialChannel.this.baudRate != com.navdy.obd.io.STNSerialChannel.this.lastConnectedBaudRate) {
                possibleBaudRatesList.add(java.lang.Integer.valueOf(com.navdy.obd.io.STNSerialChannel.this.baudRate));
            }
            for (int possibleBaudRate : com.navdy.obd.io.STNSerialChannel.POSSIBLE_BAUD_RATES_TABLE) {
                if (!(possibleBaudRate == com.navdy.obd.io.STNSerialChannel.this.lastConnectedBaudRate || possibleBaudRate == com.navdy.obd.io.STNSerialChannel.this.baudRate)) {
                    possibleBaudRatesList.add(java.lang.Integer.valueOf(possibleBaudRate));
                }
            }
            int successBaudrate = -1;
            java.util.Iterator it = possibleBaudRatesList.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                int possibleBaudRate2 = ((java.lang.Integer) it.next()).intValue();
                com.navdy.obd.io.STNSerialChannel.Log.debug("Trying to connect at baud rate : " + possibleBaudRate2);
                boolean success = com.navdy.obd.io.STNSerialChannel.this.tryConnectingAtBaudRate(this.val$address, possibleBaudRate2);
                if (!java.lang.Thread.interrupted()) {
                    if (success) {
                        successBaudrate = possibleBaudRate2;
                        break;
                    }
                } else {
                    com.navdy.obd.io.STNSerialChannel.Log.debug("Connection attempt has been interrupted, Attempt , Baud rate : " + possibleBaudRate2 + ", Success : " + success);
                    com.navdy.obd.io.STNSerialChannel.this.setState(0);
                    return;
                }
            }
            if (successBaudrate == -1) {
                com.navdy.obd.io.STNSerialChannel.Log.debug("Not able to communicate with chip at any baud rate");
                com.navdy.obd.io.STNSerialChannel.this.setState(0);
            } else if (successBaudrate == com.navdy.obd.io.STNSerialChannel.this.baudRate) {
                com.navdy.obd.io.STNSerialChannel.this.onSuccess(this.val$address);
            } else {
                com.navdy.obd.io.STNSerialChannel.Log.debug("Successfully connected at Baud rate " + successBaudrate + " , Trying to change to required baud rate");
                if (com.navdy.obd.io.STNSerialChannel.this.setBaudRateAndConnect(this.val$address, successBaudrate, com.navdy.obd.io.STNSerialChannel.this.baudRate)) {
                    com.navdy.obd.io.STNSerialChannel.this.onSuccess(this.val$address);
                } else {
                    com.navdy.obd.io.STNSerialChannel.this.setState(0);
                }
            }
        }
    }

    public STNSerialChannel(android.content.Context context2, com.navdy.obd.io.IChannelSink sink2, int lastConnectedBaudRate2, int baudRate2) {
        this.context = context2;
        this.sink = sink2;
        this.baudRate = baudRate2;
        this.lastConnectedBaudRate = lastConnectedBaudRate2;
    }

    public int getState() {
        return this.state;
    }

    /* access modifiers changed from: private */
    public void setState(int newState) {
        if (newState != this.state) {
            this.state = newState;
            if (this.sink != null) {
                this.sink.onStateChange(newState);
            }
        }
    }

    public void connect(java.lang.String address) {
        Log.debug("connect " + address + ", Last connected Baud :" + this.lastConnectedBaudRate + ", Required :" + this.baudRate);
        if (this.state == 1) {
            setState(1);
            return;
        }
        this.future = singleThreadedExecutor.submit(new com.navdy.obd.io.STNSerialChannel.Anon1(address));
        try {
            this.future.get(ch.qos.logback.classic.turbo.ReconfigureOnChangeFilter.DEFAULT_REFRESH_PERIOD, java.util.concurrent.TimeUnit.MILLISECONDS);
            return;
        } catch (java.lang.InterruptedException e) {
            Log.error("Execution InterruptedException", (java.lang.Throwable) e);
            Log.debug("Cancel task " + this.future.cancel(true));
        } catch (java.util.concurrent.ExecutionException e2) {
            Log.error("ExecutionException during execution", (java.lang.Throwable) e2);
        } catch (java.util.concurrent.TimeoutException e3) {
            Log.error("TimeoutException during execution", (java.lang.Throwable) e3);
        }
        setState(0);
    }

    /* access modifiers changed from: private */
    public void onSuccess(java.lang.String address) {
        Log.debug("Success");
        this.serialChannel = new com.navdy.obd.io.SerialChannel(this.context, this, this.baudRate);
        this.serialChannel.connect(address);
    }

    /* access modifiers changed from: private */
    public boolean setBaudRateAndConnect(java.lang.String address, int currentBaudRate, int requiredBaud) {
        com.navdy.obd.io.SerialChannel serialChannel2 = new com.navdy.obd.io.SerialChannel(this.context, this, currentBaudRate);
        serialChannel2.connect(address);
        java.lang.StringBuilder commandBuilder = new java.lang.StringBuilder();
        commandBuilder.append(SET_BAUD_RATE_COMMAND).append(" ").append(requiredBaud);
        try {
            runCommand("ate0", serialChannel2, 3000);
            java.lang.String response = runCommand(commandBuilder.toString(), serialChannel2, 3000);
            if (!OK.equals(response)) {
                Log.debug("Setting baud rate , failed with response " + response);
                return false;
            }
            serialChannel2.disconnect();
            com.navdy.obd.io.SerialChannel serialChannel3 = new com.navdy.obd.io.SerialChannel(this.context, this, requiredBaud);
            serialChannel3.connect(address);
            try {
                runCommand("\n", serialChannel3, 3000);
                java.lang.String response2 = runCommand("stwbr", serialChannel3, 3000);
                if (android.text.TextUtils.isEmpty(response2) || !response2.trim().toLowerCase().equals(OK)) {
                    Log.debug("Unexpected response for STWBR command " + (android.text.TextUtils.isEmpty(response2) ? "null" : response2.trim()));
                    serialChannel3.disconnect();
                    return false;
                }
                return true;
            } catch (java.lang.Exception e) {
                Log.error("Exception while runCommand " + e.getMessage());
                return false;
            } finally {
                serialChannel3.disconnect();
            }
        } catch (java.lang.Exception e2) {
            Log.error("Exception while runCommand " + e2.getMessage());
            return false;
        } finally {
            serialChannel2.disconnect();
        }
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: private */
    public boolean tryConnectingAtBaudRate(java.lang.String address, int baudRate2) {
        com.navdy.obd.io.SerialChannel serialChannel2 = new com.navdy.obd.io.SerialChannel(this.context, this, baudRate2);
        serialChannel2.connect(address);
        try {
            java.lang.String runCommand = runCommand("ati", serialChannel2, 3000);
            serialChannel2.disconnect();
            return true;
        } catch (java.lang.Exception e) {
            Log.error("IOException while runCommand " + e.getMessage());
            serialChannel2.disconnect();
            return false;
        } catch (Throwable th) {
            serialChannel2.disconnect();
            throw th;
        }
    }

    private java.lang.String runCommand(java.lang.String command, com.navdy.obd.io.SerialChannel serialChannel2, long timeoutMillis) throws java.io.IOException, java.util.concurrent.TimeoutException, java.lang.InterruptedException {
        if (command == null || command.length() == 0) {
            Log.debug("Empty command " + command);
            return null;
        }
        int length = command.length();
        byte[] commandBytes = new byte[(length + 1)];
        java.lang.System.arraycopy(command.getBytes(), 0, commandBytes, 0, length);
        commandBytes[commandBytes.length - 1] = 13;
        java.io.InputStream inputStream = serialChannel2.getInputStream();
        java.io.OutputStream outputStream = serialChannel2.getOutputStream();
        outputStream.write(commandBytes);
        outputStream.flush();
        int b = -1;
        java.lang.StringBuilder responseBuilder = new java.lang.StringBuilder();
        boolean textResponse = false;
        long start = java.lang.System.nanoTime();
        long elapsedTime = 0;
        long timeoutNanos = timeoutMillis * 1000000;
        boolean setBaudRateCommand = command.startsWith(SET_BAUD_RATE_COMMAND);
        while (!java.lang.Thread.interrupted()) {
            b = inputStream.read();
            if (b != -1 && b != 62) {
                elapsedTime = java.lang.System.nanoTime() - start;
                if (elapsedTime >= timeoutNanos) {
                    break;
                } else if (b != -2) {
                    boolean validHexDigit = (b >= 48 && b <= 57) || (b >= 65 && b <= 70) || (b >= 97 && b <= 102);
                    boolean isWhiteSpace = b == 32 || b == 13;
                    if (!validHexDigit && !isWhiteSpace) {
                        textResponse = true;
                    }
                    if (textResponse || b != 32) {
                        responseBuilder.append((char) b);
                    }
                    if (setBaudRateCommand) {
                        java.lang.String response = responseBuilder.toString().trim().toLowerCase();
                        if (OK.equals(response)) {
                            return OK;
                        }
                        Log.error("Unexpected response for the set baud rate command : " + response);
                    } else {
                        continue;
                    }
                }
            } else {
                break;
            }
        }
        if (elapsedTime > timeoutNanos) {
            throw new java.io.IOException("Timeout after " + (elapsedTime / 1000000) + " ms");
        }
        Log.debug("Flushing out remaining data");
        int lastChar = b;
        while (true) {
            int b2 = inputStream.read();
            if (b2 == -1 || java.lang.Thread.interrupted()) {
                break;
            } else if (b2 == -2) {
                Log.debug("Read timed out during flushing");
                break;
            } else if (b2 >= 0) {
                lastChar = b2;
                responseBuilder.append((char) b2);
            }
        }
        if (java.lang.Thread.interrupted()) {
            Log.debug("Thread was interrupted ");
            throw new java.lang.InterruptedException();
        } else if (lastChar != 62) {
            throw new java.io.IOException("Unexpected end of stream");
        } else {
            Log.debug("Response for command : " + command + " is : " + responseBuilder.toString().trim());
            return responseBuilder.toString();
        }
    }

    public void disconnect() {
        if (this.serialChannel != null) {
            this.serialChannel.disconnect();
            return;
        }
        Log.debug("disconnect when trying to connect, aborting");
        this.future.cancel(true);
    }

    public java.io.InputStream getInputStream() throws java.io.IOException {
        if (this.serialChannel != null) {
            return this.serialChannel.getInputStream();
        }
        return null;
    }

    public java.io.OutputStream getOutputStream() throws java.io.IOException {
        if (this.serialChannel != null) {
            return this.serialChannel.getOutputStream();
        }
        return null;
    }

    public com.navdy.hardware.SerialPort getSerialPort() {
        if (this.serialChannel != null) {
            return this.serialChannel.getSerialPort();
        }
        return null;
    }

    public void onStateChange(int newState) {
        if (this.serialChannel != null) {
            setState(newState);
        }
    }

    public int getBaudRate() {
        return this.baudRate;
    }

    public void onMessage(java.lang.String message) {
        if (this.serialChannel != null) {
            Log.debug("Forwarding message to main sink , Message : " + message);
            if (this.sink != null) {
                this.sink.onMessage(message);
                return;
            }
            return;
        }
        Log.debug("Internal message , message : " + this.state);
    }
}
