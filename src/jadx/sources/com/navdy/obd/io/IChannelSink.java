package com.navdy.obd.io;

public interface IChannelSink {
    void onMessage(java.lang.String str);

    void onStateChange(int i);
}
