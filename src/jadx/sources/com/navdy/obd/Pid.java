package com.navdy.obd;

public class Pid implements android.os.Parcelable {
    public static final android.os.Parcelable.Creator<com.navdy.obd.Pid> CREATOR = new com.navdy.obd.Pid.Anon1();
    public static final int NO_DATA = Integer.MIN_VALUE;
    protected com.navdy.obd.Pid.DataType dataType;
    protected int id;
    protected java.lang.String name;
    protected long timeStamp;
    protected com.navdy.obd.Units units;
    protected double value;

    static class Anon1 implements android.os.Parcelable.Creator<com.navdy.obd.Pid> {
        Anon1() {
        }

        public com.navdy.obd.Pid createFromParcel(android.os.Parcel source) {
            return new com.navdy.obd.Pid(source);
        }

        public com.navdy.obd.Pid[] newArray(int size) {
            return new com.navdy.obd.Pid[size];
        }
    }

    enum DataType {
        INT,
        FLOAT,
        PERCENTAGE,
        BOOL
    }

    public Pid(int id2) {
        this(id2, null, 0.0d, com.navdy.obd.Pid.DataType.INT, com.navdy.obd.Units.NONE);
    }

    public Pid(int id2, java.lang.String name2) {
        this(id2, name2, 0.0d, com.navdy.obd.Pid.DataType.INT, com.navdy.obd.Units.NONE);
    }

    public Pid(int id2, java.lang.String name2, double value2, com.navdy.obd.Pid.DataType dataType2, com.navdy.obd.Units units2) {
        this(id2, name2, 0.0d, com.navdy.obd.Pid.DataType.INT, com.navdy.obd.Units.NONE, 0);
    }

    public Pid(int id2, java.lang.String name2, double value2, com.navdy.obd.Pid.DataType dataType2, com.navdy.obd.Units units2, long timeStamp2) {
        this.id = id2;
        this.name = name2;
        this.value = value2;
        this.dataType = dataType2;
        this.units = units2;
        this.timeStamp = timeStamp2;
    }

    public int getId() {
        return this.id;
    }

    public java.lang.String getName() {
        return this.name;
    }

    public double getValue() {
        return this.value;
    }

    public void setValue(double value2) {
        this.value = value2;
    }

    public com.navdy.obd.Pid.DataType getDataType() {
        return this.dataType;
    }

    public int describeContents() {
        return 0;
    }

    public Pid(android.os.Parcel in) {
        this(in.readInt(), in.readString(), in.readDouble(), com.navdy.obd.Pid.DataType.valueOf(in.readString()), com.navdy.obd.Units.valueOf(in.readString()), in.readLong());
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.name);
        dest.writeDouble(this.value);
        dest.writeString(this.dataType.name());
        dest.writeString(this.units.name());
        dest.writeLong(this.timeStamp);
    }

    public boolean equals(java.lang.Object o) {
        if (o instanceof com.navdy.obd.Pid) {
            return ((com.navdy.obd.Pid) o).id == this.id;
        }
        return super.equals(o);
    }

    public java.lang.String toString() {
        return "Pid{id=" + java.lang.Integer.toHexString(this.id) + ch.qos.logback.core.CoreConstants.CURLY_RIGHT;
    }

    public long getTimeStamp() {
        return this.timeStamp;
    }

    public void setTimeStamp(long timeStamp2) {
        this.timeStamp = timeStamp2;
    }
}
