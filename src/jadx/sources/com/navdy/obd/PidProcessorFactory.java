package com.navdy.obd;

public interface PidProcessorFactory {
    com.navdy.obd.PidProcessor buildPidProcessorForPid(int i);

    java.util.List<java.lang.Integer> getPidsHavingProcessors();
}
