package com.navdy.obd;

public class DefaultPidProcessorFactory implements com.navdy.obd.PidProcessorFactory {
    private static java.util.List<java.lang.Integer> PIDS_WITH_PRE_PROCESSORS = new java.util.ArrayList();

    static {
        PIDS_WITH_PRE_PROCESSORS.add(java.lang.Integer.valueOf(256));
        PIDS_WITH_PRE_PROCESSORS.add(java.lang.Integer.valueOf(47));
        PIDS_WITH_PRE_PROCESSORS.add(java.lang.Integer.valueOf(13));
    }

    public com.navdy.obd.PidProcessor buildPidProcessorForPid(int pid) {
        switch (pid) {
            case 13:
                return new com.navdy.obd.SpeedPidProcessor();
            case 47:
                return new com.navdy.obd.FuelLevelPidProcessor();
            case 256:
                return new com.navdy.obd.InstantFuelConsumptionPidProcessor();
            default:
                return null;
        }
    }

    public java.util.List<java.lang.Integer> getPidsHavingProcessors() {
        return PIDS_WITH_PRE_PROCESSORS;
    }
}
