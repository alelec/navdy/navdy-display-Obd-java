package com.navdy.obd;

public class ObdService extends android.app.Service implements com.navdy.obd.io.IChannelSink {
    private static final int BATTERY_CHECK_INTERVAL = 2000;
    public static final int BAUD_RATE = com.navdy.os.SystemProperties.getInt(BAUD_RATE_PROP, 460800);
    private static final java.lang.String BAUD_RATE_PROP = "persist.sys.obd.baudrate";
    public static final java.lang.String CAN_BUS_LOGGER = "com.navdy.obd.CanBusRaw";
    public static final boolean CAN_BUS_MONITORING_ENABLED = false;
    public static final int CAN_BUS_SAMPLING_INTERVAL = 1000;
    private static final int DEFAULT_MODE = com.navdy.os.SystemProperties.getInt(DEFAULT_MODE_PROP, 0);
    private static final java.lang.String DEFAULT_MODE_PROP = "persist.sys.obd.mode";
    public static final int DEFAULT_SCAN_INTERVAL = 500;
    public static final int GATHER_CAN_BUS_DATA_INTERVAL = 1000;
    public static final int MESSAGE_CHANGE_MODE = 9;
    public static final int MESSAGE_CHARGING = 4;
    public static final int MESSAGE_DISABLE_OBD_PID_SCAN = 5;
    public static final int MESSAGE_ENABLE_OBD_PID_SCAN = 6;
    public static final int MESSAGE_RESCAN = 3;
    public static final int MESSAGE_RESET_CHANNEL = 7;
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_TOAST = 2;
    public static final int MESSAGE_UPDATE_FIRMWARE = 8;
    public static final int MODE_ONE_READ_RESPONSE = 65;
    public static final int MONITOR_CAN_BUS_BATCH_INTERVAL = 30000;
    public static final int MONITOR_CAN_BUS_BATCH_SIZE = 5;
    public static final java.lang.String OBD_DEBUG_MODE = "persist.sys.obd.debug";
    public static final java.lang.String OVERRIDE_PROTOCOL = "persist.sys.obd.protocol";
    public static final java.lang.String PREFS_FILE_OBD_DEVICE = "ObdDevice";
    public static final java.lang.String PREFS_KEY_DEFAULT_CHANNEL_INFO = "DefaultChannelInfo";
    public static final java.lang.String PREFS_KEY_LAST_PROTOCOL = "Protocol";
    public static final java.lang.String PREFS_KEY_OBD_SCAN_ENABLED = "ObdCommunicationEnabled";
    public static final java.lang.String PREFS_KEY_OBD_SCAN_MODE = "ObdScanMode";
    public static final java.lang.String PREFS_LAST_CONNECTED_BAUD_RATE = "last_connected_baud_rate";
    /* access modifiers changed from: private */
    public static long RECONNECT_DELAY = java.util.concurrent.TimeUnit.SECONDS.toMillis(30);
    private static final int SCAN_INTERVAL = 10;
    /* access modifiers changed from: private */
    public static final java.lang.String TAG = com.navdy.obd.ObdService.class.getSimpleName();
    public static final java.lang.String TOAST = "toast";
    public static final java.lang.String VOLTAGE_FREQUENCY_PROPERTY = "persist.sys.obd.volt.freq";
    public static final int VOLTAGE_REPORT_INTERVAL = (com.navdy.os.SystemProperties.getInt(VOLTAGE_FREQUENCY_PROPERTY, 30) * 1000);
    public static final int VOLTAGE_THRESHOLD_MINIMUM_TIME_IN_SECONDS = 10;
    private static com.navdy.obd.ObdService obdService;
    private com.navdy.obd.IObdService.Stub apiEndpoint = new com.navdy.obd.ObdService.Anon2();
    /* access modifiers changed from: private */
    public com.navdy.obd.jobs.AutoConnect autoConnect;
    /* access modifiers changed from: private */
    public com.navdy.obd.ICanBusMonitoringListener canBusMonitoringListener;
    /* access modifiers changed from: private */
    public com.navdy.obd.ObdService.CANBusMonitoringStatus canBusMonitoringStatus = new com.navdy.obd.ObdService.CANBusMonitoringStatus();
    private com.navdy.obd.ICarService.Stub carApiEndpoint = new com.navdy.obd.ObdService.Anon1();
    /* access modifiers changed from: private */
    public com.navdy.obd.io.IChannel channel;
    private com.navdy.obd.io.ChannelInfo channelInfo;
    /* access modifiers changed from: private */
    public java.lang.String configurationName;
    /* access modifiers changed from: private */
    public int connectionState = 0;
    /* access modifiers changed from: private */
    public double currentBatteryVoltage = -1.0d;
    /* access modifiers changed from: private */
    public java.lang.String currentFirmwareVersion = null;
    /* access modifiers changed from: private */
    public int currentMode = -1;
    /* access modifiers changed from: private */
    public com.navdy.obd.ObdScanJob currentScan;
    private java.util.concurrent.ScheduledFuture currentSchedule;
    /* access modifiers changed from: private */
    public boolean debugMode;
    /* access modifiers changed from: private */
    public boolean deepSleep = false;
    /* access modifiers changed from: private */
    public com.navdy.obd.update.ObdDeviceFirmwareManager firmwareManager;
    /* access modifiers changed from: private */
    public java.lang.String firmwareUpdatePath;
    /* access modifiers changed from: private */
    public java.lang.String firmwareUpdateVersion;
    /* access modifiers changed from: private */
    public boolean gatherCanBusDataToLogs = false;
    /* access modifiers changed from: private */
    public volatile boolean isScanningEnabled = true;
    private com.navdy.obd.j1939.J1939Profile j1939Profile;
    private volatile int lastConnectedBaudRate = 0;
    /* access modifiers changed from: private */
    public long lastDisconnectTime = 0;
    /* access modifiers changed from: private */
    public long lastMonitorBatchCompletedTime = 0;
    /* access modifiers changed from: private */
    public long lastVoltageReport;
    /* access modifiers changed from: private */
    public final java.util.List<com.navdy.obd.IObdServiceListener> listeners = new java.util.ArrayList();
    protected android.content.SharedPreferences mSharedPrefs;
    private com.navdy.obd.VehicleStateManager mVehicleStateManager;
    /* access modifiers changed from: private */
    public int monitorCounter = 0;
    public boolean monitoringCanBusLimitReached = false;
    /* access modifiers changed from: private */
    public com.navdy.obd.ObdDataObserver obdDataObserver;
    /* access modifiers changed from: private */
    public boolean onMonitoringStatusReported = false;
    private final android.util.ArrayMap<android.os.IBinder, com.navdy.obd.PidListener> pidListeners = new android.util.ArrayMap<>();
    private com.navdy.obd.Profile profile;
    /* access modifiers changed from: private */
    public com.navdy.obd.Protocol protocol;
    /* access modifiers changed from: private */
    public boolean reconnectImmediately = false;
    /* access modifiers changed from: private */
    public final java.util.concurrent.ScheduledExecutorService scheduler = java.util.concurrent.Executors.newSingleThreadScheduledExecutor();
    /* access modifiers changed from: private */
    public com.navdy.obd.ObdService.ServiceHandler serviceHandler;
    private android.os.Looper serviceLooper;
    private com.navdy.obd.command.BatchCommand sleepWithWakeUpTriggersCommand;
    /* access modifiers changed from: private */
    public com.navdy.obd.VehicleInfo vehicleInfo;
    private java.util.concurrent.ScheduledFuture voltageMonitoring;
    /* access modifiers changed from: private */
    public com.navdy.obd.VoltageSettings voltageSettings;
    private final java.lang.Object voltageSettingsLock = new java.lang.Object();

    class Anon1 extends com.navdy.obd.ICarService.Stub {
        Anon1() {
        }

        public int getConnectionState() throws android.os.RemoteException {
            return com.navdy.obd.ObdService.this.connectionState;
        }

        public java.util.List<com.navdy.obd.Pid> getSupportedPids() throws android.os.RemoteException {
            if (com.navdy.obd.ObdService.this.currentMode == 0) {
                if (com.navdy.obd.ObdService.this.vehicleInfo != null) {
                    return com.navdy.obd.ObdService.this.vehicleInfo.getPrimaryEcu().supportedPids.asList();
                }
            } else if (com.navdy.obd.ObdService.this.currentMode == 1 && getConnectionState() == 2) {
                return com.navdy.obd.j1939.J1939ObdJobAdapter.J1939SupportedObdPids.asList();
            }
            return null;
        }

        public java.util.List<com.navdy.obd.ECU> getEcus() {
            if (com.navdy.obd.ObdService.this.vehicleInfo != null) {
                return com.navdy.obd.ObdService.this.vehicleInfo.getEcus();
            }
            return null;
        }

        public java.lang.String getProtocol() {
            return com.navdy.obd.ObdService.this.vehicleInfo.protocol;
        }

        public java.lang.String getVIN() {
            if (com.navdy.obd.ObdService.this.vehicleInfo != null) {
                return com.navdy.obd.ObdService.this.vehicleInfo.vin;
            }
            return null;
        }

        public java.util.List<com.navdy.obd.Pid> getReadings(java.util.List<com.navdy.obd.Pid> pids) throws android.os.RemoteException {
            if (com.navdy.obd.ObdService.this.autoConnect == null) {
                return null;
            }
            return com.navdy.obd.ObdService.this.autoConnect.getReadings(pids);
        }

        public void addListener(java.util.List<com.navdy.obd.Pid> pids, com.navdy.obd.IPidListener listener) throws android.os.RemoteException {
            com.navdy.obd.ObdService.this.addListener(pids, listener);
        }

        public void removeListener(com.navdy.obd.IPidListener listener) {
            com.navdy.obd.ObdService.this.removeListener(listener);
        }

        public java.lang.String getCurrentConfigurationName() throws android.os.RemoteException {
            return com.navdy.obd.ObdService.this.configurationName;
        }

        public boolean applyConfiguration(java.lang.String configuration) {
            try {
                com.navdy.obd.ObdService.this.applyConfigurationInternal(configuration);
                com.navdy.obd.ObdService.this.updateScan();
                return true;
            } catch (java.lang.IllegalArgumentException t) {
                com.navdy.obd.ObdService.this.updateScan();
                throw t;
            } catch (Throwable th) {
                if (1 != 0) {
                    com.navdy.obd.ObdService.this.applyFallbackConfiguration();
                }
                return false;
            }
        }

        public void updateScan(com.navdy.obd.ScanSchedule schedule, com.navdy.obd.IPidListener listener) throws android.os.RemoteException {
            com.navdy.obd.ObdService.this.addListener(schedule, listener);
        }

        public void rescan() {
            com.navdy.obd.ObdService.this.serviceHandler.sendEmptyMessage(3);
        }

        public double getBatteryVoltage() throws android.os.RemoteException {
            return com.navdy.obd.ObdService.this.currentBatteryVoltage;
        }

        public void setObdPidsScanningEnabled(boolean enable) throws android.os.RemoteException {
            if (enable == com.navdy.obd.ObdService.this.isScanningEnabled) {
                return;
            }
            if (enable) {
                com.navdy.obd.ObdService.this.serviceHandler.sendEmptyMessage(6);
            } else {
                com.navdy.obd.ObdService.this.serviceHandler.sendEmptyMessage(5);
            }
        }

        public boolean isObdPidsScanningEnabled() throws android.os.RemoteException {
            return com.navdy.obd.ObdService.this.isScanningEnabled;
        }

        public void sleep(boolean deep) throws android.os.RemoteException {
            android.util.Log.d(com.navdy.obd.ObdService.TAG, "sleep API is invoked");
            com.navdy.obd.ObdService.this.deepSleep = deep;
            com.navdy.obd.ObdService.this.postStateChange(5);
        }

        public void wakeup() throws android.os.RemoteException {
            if (com.navdy.obd.ObdService.this.connectionState == 5) {
                com.navdy.obd.ObdService.this.postStateChange(4);
            }
        }

        public void setVoltageSettings(com.navdy.obd.VoltageSettings settings) throws android.os.RemoteException {
            com.navdy.obd.ObdService.this.setVoltageSettings(settings);
        }

        public java.lang.String getObdChipFirmwareVersion() throws android.os.RemoteException {
            return com.navdy.obd.ObdService.this.currentFirmwareVersion;
        }

        public void updateFirmware(java.lang.String version, java.lang.String updateFilePath) throws android.os.RemoteException {
            android.util.Log.d(com.navdy.obd.ObdService.TAG, "updateFirmware , version :" + version + " , Path : " + updateFilePath);
            com.navdy.obd.ObdService.this.firmwareUpdateVersion = version;
            com.navdy.obd.ObdService.this.firmwareUpdatePath = updateFilePath;
            com.navdy.obd.ObdService.this.getHandler().sendEmptyMessage(8);
        }

        public int getMode() throws android.os.RemoteException {
            return com.navdy.obd.ObdService.this.currentMode;
        }

        public void setMode(int mode, boolean persistent) throws android.os.RemoteException {
            android.util.Log.d(com.navdy.obd.ObdService.TAG, "setMode : " + mode + ", Persistent : " + persistent);
            if (persistent) {
                android.util.Log.d(com.navdy.obd.ObdService.TAG, "Saving the mode");
                com.navdy.obd.ObdService.this.mSharedPrefs.edit().putInt(com.navdy.obd.ObdService.PREFS_KEY_OBD_SCAN_MODE, mode).apply();
            }
            com.navdy.obd.ObdService.this.getHandler().obtainMessage(9, mode, -1).sendToTarget();
        }

        public void setCANBusMonitoringListener(com.navdy.obd.ICanBusMonitoringListener listener) throws android.os.RemoteException {
            com.navdy.obd.ObdService.this.canBusMonitoringListener = listener;
        }

        public void startCanBusMonitoring() throws android.os.RemoteException {
            com.navdy.obd.ObdService.this.gatherCanBusDataToLogs = true;
            com.navdy.obd.ObdService.this.updateScan();
        }

        public void stopCanBusMonitoring() throws android.os.RemoteException {
            com.navdy.obd.ObdService.this.gatherCanBusDataToLogs = false;
            com.navdy.obd.ObdService.this.canBusMonitoringListener = null;
            com.navdy.obd.ObdService.this.updateScan();
        }

        public boolean isCheckEngineLightOn() throws android.os.RemoteException {
            if (com.navdy.obd.ObdService.this.vehicleInfo != null) {
                return com.navdy.obd.ObdService.this.vehicleInfo.isCheckEngineLightOn;
            }
            return false;
        }

        public java.util.List<java.lang.String> getTroubleCodes() throws android.os.RemoteException {
            if (com.navdy.obd.ObdService.this.vehicleInfo != null) {
                return com.navdy.obd.ObdService.this.vehicleInfo.troubleCodes;
            }
            return null;
        }
    }

    class Anon2 extends com.navdy.obd.IObdService.Stub {

        class Anon1 implements java.lang.Runnable {
            final /* synthetic */ com.navdy.obd.update.Update val$update;

            Anon1(com.navdy.obd.update.Update update) {
                this.val$update = update;
            }

            public void run() {
                android.util.Log.d(com.navdy.obd.ObdService.TAG, "Update success : " + com.navdy.obd.ObdService.this.firmwareManager.updateFirmware(this.val$update));
                com.navdy.obd.ObdService.this.reconnectImmediately = false;
                com.navdy.obd.ObdService.this.firmwareUpdateVersion = "";
                com.navdy.obd.ObdService.this.firmwareUpdatePath = "";
                com.navdy.obd.ObdService.this.forceReconnect();
            }
        }

        /* renamed from: com.navdy.obd.ObdService$Anon2$Anon2 reason: collision with other inner class name */
        class C0000Anon2 implements com.navdy.obd.ObdScanJob.IRawData {
            C0000Anon2() {
            }

            public void onRawData(java.lang.String data) {
                synchronized (com.navdy.obd.ObdService.this.listeners) {
                    for (com.navdy.obd.IObdServiceListener listener : com.navdy.obd.ObdService.this.listeners) {
                        try {
                            listener.onRawData(data);
                        } catch (android.os.RemoteException e) {
                            android.util.Log.e(com.navdy.obd.ObdService.TAG, "Error notifying listener", e);
                        }
                    }
                }
            }
        }

        Anon2() {
        }

        public void connect(java.lang.String address, com.navdy.obd.IObdServiceListener listener) throws android.os.RemoteException {
            synchronized (com.navdy.obd.ObdService.this.listeners) {
                com.navdy.obd.ObdService.this.listeners.add(listener);
            }
            com.navdy.obd.io.ChannelInfo info = com.navdy.obd.io.ChannelInfo.parse(address);
            if (info != null) {
                android.util.Log.d(com.navdy.obd.ObdService.TAG, "Attempting to connect to " + address);
                com.navdy.obd.ObdService.this.openChannel(info);
            }
        }

        public void disconnect() throws android.os.RemoteException {
            if (com.navdy.obd.ObdService.this.channel != null) {
                com.navdy.obd.ObdService.this.channel.disconnect();
            }
        }

        public int getState() throws android.os.RemoteException {
            return com.navdy.obd.ObdService.this.connectionState;
        }

        public java.lang.String getFirmwareVersion() {
            try {
                return com.navdy.obd.ObdService.this.currentFirmwareVersion;
            } catch (Throwable th) {
                return null;
            }
        }

        public boolean updateDeviceFirmware(java.lang.String version, java.lang.String updateFilePath) {
            try {
                if (com.navdy.obd.ObdService.this.firmwareManager == null || com.navdy.obd.ObdService.this.firmwareManager.isUpdatingFirmware()) {
                    if (com.navdy.obd.ObdService.this.firmwareManager != null) {
                        android.util.Log.d(com.navdy.obd.ObdService.TAG, "FirmwareManager is busy installing update already");
                    }
                    return true;
                }
                com.navdy.obd.update.Update update = new com.navdy.obd.update.Update();
                update.mVersion = version;
                update.mUpdateFilePath = updateFilePath;
                java.lang.Runnable updateJob = new com.navdy.obd.ObdService.Anon2.Anon1(update);
                com.navdy.obd.ObdService.this.cancelCurrentSchedule();
                com.navdy.obd.ObdService.this.cancelVoltageMonitoring();
                com.navdy.obd.ObdService.this.scheduler.submit(updateJob);
                return true;
            } catch (Throwable t) {
                android.util.Log.e(com.navdy.obd.ObdService.TAG, "Exception while updating the firmware :" + t);
            }
        }

        public void reset() {
            com.navdy.obd.ObdService.this.serviceHandler.sendEmptyMessage(7);
        }

        public void scanSupportedPids() throws android.os.RemoteException {
            com.navdy.obd.ObdService.this.scanSupportedPids();
        }

        public void scanVIN() throws android.os.RemoteException {
            com.navdy.obd.ObdService.this.scanVIN();
        }

        public java.lang.String readPid(int pid) throws android.os.RemoteException {
            try {
                com.navdy.obd.ObdService.this.synchronousCommand(new com.navdy.obd.command.ObdCommand(java.lang.String.format("01%02x1", new java.lang.Object[]{java.lang.Integer.valueOf(pid)})));
                return null;
            } catch (java.util.concurrent.ExecutionException e) {
                return null;
            }
        }

        public void scanPids(java.util.List<com.navdy.obd.Pid> pids, int intervalMs) throws android.os.RemoteException {
            com.navdy.obd.ObdService.this.scanPids(pids, intervalMs);
        }

        public void startRawScan() throws android.os.RemoteException {
            com.navdy.obd.ObdService.this.cancelCurrentSchedule();
            com.navdy.obd.ObdService.this.cancelVoltageMonitoring();
            com.navdy.obd.ObdService.this.currentScan = new com.navdy.obd.ObdScanJob(com.navdy.obd.ObdService.this.channel, com.navdy.obd.ObdService.this.protocol, new com.navdy.obd.ObdService.Anon2.C0000Anon2(), com.navdy.obd.ObdService.this.obdDataObserver);
            com.navdy.obd.ObdService.this.scheduler.submit(com.navdy.obd.ObdService.this.currentScan);
        }

        public void addListener(com.navdy.obd.IObdServiceListener listener) throws android.os.RemoteException {
            com.navdy.obd.ObdService.this.addListener(listener);
        }

        public void removeListener(com.navdy.obd.IObdServiceListener listener) throws android.os.RemoteException {
            com.navdy.obd.ObdService.this.removeListener(listener);
        }
    }

    class Anon3 implements com.navdy.obd.ObdJob.IListener {
        Anon3() {
        }

        public void onComplete(com.navdy.obd.ObdJob job, boolean success) {
            if (success) {
                com.navdy.obd.command.ICommand command = job.command;
                java.lang.String response = command.getResponse();
                android.util.Log.d(com.navdy.obd.ObdService.TAG, "Response of the Synchronous command " + (response != null ? response.trim() : "EMPTY") + ", Command : " + command);
            }
        }
    }

    class Anon4 implements com.navdy.obd.ObdJob.IListener {
        final /* synthetic */ com.navdy.obd.command.ReadVoltageCommand val$command;

        Anon4(com.navdy.obd.command.ReadVoltageCommand readVoltageCommand) {
            this.val$command = readVoltageCommand;
        }

        public void onComplete(com.navdy.obd.ObdJob job, boolean success) {
            if (success) {
                double voltage = (double) this.val$command.getVoltage();
                com.navdy.obd.ObdService.this.currentBatteryVoltage = voltage;
                long now = android.os.SystemClock.elapsedRealtime();
                if (com.navdy.obd.ObdService.this.lastVoltageReport == 0 || now - com.navdy.obd.ObdService.this.lastVoltageReport > ((long) com.navdy.obd.ObdService.VOLTAGE_REPORT_INTERVAL)) {
                    android.util.Log.i(com.navdy.obd.ObdService.TAG, "Battery voltage: " + voltage);
                    com.navdy.obd.ObdService.this.lastVoltageReport = now;
                }
                if (com.navdy.obd.ObdService.this.connectionState == 4 && com.navdy.obd.ObdService.this.isScanningEnabled) {
                    if (voltage <= ((double) com.navdy.obd.ObdService.this.voltageSettings.chargingVoltage) && !com.navdy.obd.ObdService.this.debugMode) {
                        return;
                    }
                    if (com.navdy.obd.ObdService.this.lastDisconnectTime == 0 || android.os.SystemClock.elapsedRealtime() - com.navdy.obd.ObdService.this.lastDisconnectTime > com.navdy.obd.ObdService.RECONNECT_DELAY) {
                        android.util.Log.i(com.navdy.obd.ObdService.TAG, "Battery now charging: " + voltage);
                        com.navdy.obd.ObdService.this.serviceHandler.sendEmptyMessage(4);
                        return;
                    }
                    android.util.Log.i(com.navdy.obd.ObdService.TAG, "Waiting for voltage to settle: " + voltage);
                }
            }
        }
    }

    class Anon5 implements com.navdy.obd.ObdJob.IListener {
        Anon5() {
        }

        public void onComplete(com.navdy.obd.ObdJob job, boolean success) {
            if (success) {
                android.util.Log.d(com.navdy.obd.ObdService.TAG, "Successfully able to put the Obd chip to sleep");
            }
        }
    }

    class Anon6 implements com.navdy.obd.j1939.J1939ObdJobAdapter.J1939ObdJobListener {
        final /* synthetic */ com.navdy.obd.j1939.J1939ObdJobAdapter val$jobdAdpater;

        Anon6(com.navdy.obd.j1939.J1939ObdJobAdapter j1939ObdJobAdapter) {
            this.val$jobdAdpater = j1939ObdJobAdapter;
        }

        public void onNewDataAvailable() {
            com.navdy.obd.ObdService.this.updateListenersWithNewData(this.val$jobdAdpater.getMonitoredPidsList());
        }
    }

    class Anon7 implements com.navdy.obd.jobs.ScanPidsJob.IListener {
        Anon7() {
        }

        public void onCanBusDataRead(com.navdy.obd.jobs.ScanPidsJob job) {
            com.navdy.obd.ObdService.this.updateListenersWithNewData(job.getFullPidsList());
        }

        public com.navdy.obd.command.CANBusMonitoringCommand getCanBusMonitoringCommand() {
            return com.navdy.obd.ObdService.this.canBusMonitoringStatus.canBusMonitoringCommand;
        }

        public void onCanBusMonitoringErrorDetected() {
            if (com.navdy.obd.ObdService.this.gatherCanBusDataToLogs) {
                com.navdy.obd.ObdService.this.gatherCanBusDataToLogs = false;
                if (com.navdy.obd.ObdService.this.canBusMonitoringListener != null) {
                    try {
                        com.navdy.obd.ObdService.this.canBusMonitoringListener.onCanBusMonitoringError(com.navdy.obd.ObdServiceInterface.CAN_BUS_MONITOR_ERROR_MESSAGE_BAD_STATE);
                        com.navdy.obd.ObdService.this.monitoringCanBusLimitReached = true;
                        com.navdy.obd.ObdService.this.obdDataObserver.onRawCanBusMessage("\n");
                    } catch (android.os.RemoteException e) {
                        android.util.Log.e(com.navdy.obd.ObdService.TAG, "RemoteException ", e);
                    }
                }
            }
        }

        public void onComplete(com.navdy.obd.ObdJob job, boolean success) {
            int quantized;
            java.util.List<com.navdy.obd.Pid> fullPids = ((com.navdy.obd.jobs.ScanPidsJob) job).getFullPidsList();
            if (com.navdy.obd.ObdService.this.gatherCanBusDataToLogs) {
                if (com.navdy.obd.command.GatherCANBusDataCommand.DEFAULT_MONITOR_COMMAND.isMonitoringFailureDetected()) {
                    android.util.Log.e(com.navdy.obd.ObdService.TAG, "Can bus monitoring failed, disabling the monitoring");
                    com.navdy.obd.command.GatherCANBusDataCommand.DEFAULT_MONITOR_COMMAND.reset();
                    com.navdy.obd.ObdService.this.gatherCanBusDataToLogs = false;
                    if (com.navdy.obd.ObdService.this.canBusMonitoringListener != null) {
                        try {
                            java.lang.String errorData = com.navdy.obd.command.GatherCANBusDataCommand.DEFAULT_MONITOR_COMMAND.getErrorData();
                            com.navdy.obd.ICanBusMonitoringListener access$Anon2400 = com.navdy.obd.ObdService.this.canBusMonitoringListener;
                            if (android.text.TextUtils.isEmpty(errorData)) {
                                errorData = com.navdy.obd.ObdServiceInterface.GENERIC_CAN_BUS_MONITOR_ERROR_MESSAGE;
                            }
                            access$Anon2400.onCanBusMonitoringError(errorData);
                        } catch (android.os.RemoteException e) {
                            android.util.Log.e(com.navdy.obd.ObdService.TAG, "RemoteException ", e);
                        }
                    }
                    com.navdy.obd.ObdService.this.updateScan();
                } else if (com.navdy.obd.command.GatherCANBusDataCommand.DEFAULT_MONITOR_COMMAND.getLastSampleSucceeded()) {
                    java.lang.StringBuilder stringBuilder = new java.lang.StringBuilder();
                    stringBuilder.append("OBD_DATA,");
                    for (com.navdy.obd.Pid pid : fullPids) {
                        stringBuilder.append(pid.getId() + ":" + pid.getValue()).append(",");
                    }
                    try {
                        if (com.navdy.obd.ObdService.this.canBusMonitoringListener != null) {
                            stringBuilder.append("Lat:" + com.navdy.obd.ObdService.this.canBusMonitoringListener.getLatitude() + ",").append("Long:" + com.navdy.obd.ObdService.this.canBusMonitoringListener.getLongitude() + ",").append("Speed:" + com.navdy.obd.ObdService.this.canBusMonitoringListener.getGpsSpeed());
                        }
                    } catch (java.lang.Exception e2) {
                        android.util.Log.e(com.navdy.obd.ObdService.TAG, "");
                    }
                    double lastSampleDataSize = (double) com.navdy.obd.command.GatherCANBusDataCommand.DEFAULT_MONITOR_COMMAND.getLastSampleDataSize();
                    if (!com.navdy.obd.ObdService.this.onMonitoringStatusReported && lastSampleDataSize > 0.0d) {
                        com.navdy.obd.ObdService.this.onMonitoringStatusReported = true;
                        if (lastSampleDataSize > 1000.0d) {
                            lastSampleDataSize = (double) java.lang.Math.round(lastSampleDataSize / 1000.0d);
                            quantized = (int) (java.lang.Math.round(lastSampleDataSize / 50.0d) * 50);
                        } else {
                            quantized = (int) lastSampleDataSize;
                        }
                        android.util.Log.d(com.navdy.obd.ObdService.TAG, "Monitor succeeded , Data size, Actual : " + lastSampleDataSize + ", Quantized : " + quantized);
                        if (com.navdy.obd.ObdService.this.canBusMonitoringListener != null) {
                            try {
                                com.navdy.obd.ObdService.this.canBusMonitoringListener.onCanBusMonitorSuccess(quantized);
                            } catch (android.os.RemoteException e3) {
                                android.util.Log.e(com.navdy.obd.ObdService.TAG, "Error reporting the can bus monitor success");
                            }
                        }
                    }
                    com.navdy.obd.ObdService.this.obdDataObserver.onRawCanBusMessage(stringBuilder.toString());
                    com.navdy.obd.ObdService.this.monitorCounter = com.navdy.obd.ObdService.this.monitorCounter + 1;
                    if (com.navdy.obd.ObdService.this.monitorCounter >= 5) {
                        android.util.Log.d(com.navdy.obd.ObdService.TAG, "Monitor Batch completed");
                        com.navdy.obd.ObdService.this.lastMonitorBatchCompletedTime = android.os.SystemClock.elapsedRealtime();
                        com.navdy.obd.ObdService.this.monitorCounter = 0;
                        com.navdy.obd.ObdService.this.updateScan();
                    }
                    com.navdy.obd.command.GatherCANBusDataCommand.DEFAULT_MONITOR_COMMAND.resetLastSampleStats();
                }
                if (com.navdy.obd.ObdService.this.canBusMonitoringListener != null) {
                    try {
                        if (com.navdy.obd.ObdService.this.canBusMonitoringListener.isMonitoringLimitReached()) {
                            com.navdy.obd.ObdService.this.monitoringCanBusLimitReached = true;
                            com.navdy.obd.ObdService.this.obdDataObserver.onRawCanBusMessage("\n");
                        }
                    } catch (android.os.RemoteException e4) {
                        e4.printStackTrace();
                    }
                }
            }
            com.navdy.obd.ObdService.this.updateListenersWithNewData(fullPids);
        }
    }

    class Anon8 implements java.lang.Runnable {
        Anon8() {
        }

        public void run() {
            synchronized (com.navdy.obd.ObdService.this.listeners) {
                java.lang.String vin = com.navdy.obd.ObdService.this.vehicleInfo != null ? com.navdy.obd.ObdService.this.vehicleInfo.vin : null;
                for (com.navdy.obd.IObdServiceListener listener : com.navdy.obd.ObdService.this.listeners) {
                    try {
                        listener.scannedVIN(vin);
                    } catch (android.os.RemoteException e) {
                        android.util.Log.e(com.navdy.obd.ObdService.TAG, "Error notifying listener", e);
                    }
                }
            }
        }
    }

    class Anon9 implements java.lang.Runnable {
        Anon9() {
        }

        public void run() {
            java.util.List<com.navdy.obd.Pid> pids = com.navdy.obd.ObdService.this.vehicleInfo.getPrimaryEcu().supportedPids.asList();
            synchronized (com.navdy.obd.ObdService.this.listeners) {
                for (com.navdy.obd.IObdServiceListener listener : com.navdy.obd.ObdService.this.listeners) {
                    try {
                        listener.supportedPids(pids);
                    } catch (android.os.RemoteException e) {
                        android.util.Log.e(com.navdy.obd.ObdService.TAG, "Error notifying listener", e);
                    }
                }
            }
        }
    }

    enum CANBusMonitoringState {
        UNKNOWN,
        UNAVAILABLE,
        SAMPLING,
        MONITORING
    }

    static class CANBusMonitoringStatus {
        com.navdy.obd.command.CANBusMonitoringCommand canBusMonitoringCommand;
        com.navdy.obd.ObdService.CANBusMonitoringState state = com.navdy.obd.ObdService.CANBusMonitoringState.UNKNOWN;

        CANBusMonitoringStatus() {
        }
    }

    private final class ServiceHandler extends android.os.Handler {
        /* synthetic */ ServiceHandler(com.navdy.obd.ObdService x0, android.os.Looper x1, com.navdy.obd.ObdService.Anon1 x2) {
            this(x1);
        }

        private ServiceHandler(android.os.Looper looper) {
            super(looper);
        }

        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                case 1:
                    com.navdy.obd.ObdService.this.handleStateChange(msg.arg1);
                    return;
                case 2:
                    android.util.Log.i(com.navdy.obd.ObdService.TAG, "MESSAGE_TOAST: " + msg.arg1);
                    for (com.navdy.obd.IObdServiceListener listener : com.navdy.obd.ObdService.this.listeners) {
                        try {
                            listener.onStatusMessage(msg.getData().getString("toast"));
                        } catch (android.os.RemoteException e) {
                            android.util.Log.e(com.navdy.obd.ObdService.TAG, "Failed to notify listener of statusMessage");
                        }
                    }
                    return;
                case 3:
                case 4:
                    if (com.navdy.obd.ObdService.this.connectionState == 4 && com.navdy.obd.ObdService.this.isScanningEnabled) {
                        android.util.Log.i(com.navdy.obd.ObdService.TAG, "Rescanning for ECUs");
                        com.navdy.obd.ObdService.this.handleStateChange(3);
                        return;
                    }
                    return;
                case 5:
                    android.util.Log.i(com.navdy.obd.ObdService.TAG, "Disabling PIDs scan");
                    com.navdy.obd.ObdService.this.mSharedPrefs.edit().putBoolean(com.navdy.obd.ObdService.PREFS_KEY_OBD_SCAN_ENABLED, false).apply();
                    com.navdy.obd.ObdService.this.isScanningEnabled = false;
                    if (com.navdy.obd.ObdService.this.connectionState == 2) {
                        com.navdy.obd.ObdService.this.cancelCurrentSchedule();
                        com.navdy.obd.ObdService.this.handleStateChange(4);
                        return;
                    }
                    return;
                case 6:
                    android.util.Log.i(com.navdy.obd.ObdService.TAG, "Enabling PIDs scan");
                    com.navdy.obd.ObdService.this.mSharedPrefs.edit().putBoolean(com.navdy.obd.ObdService.PREFS_KEY_OBD_SCAN_ENABLED, true).apply();
                    com.navdy.obd.ObdService.this.isScanningEnabled = true;
                    return;
                case 7:
                    com.navdy.obd.ObdService.this.resetChannel();
                    return;
                case 8:
                    com.navdy.obd.ObdService.this.cancelCurrentSchedule();
                    com.navdy.obd.ObdService.this.reconnectImmediately = true;
                    com.navdy.obd.ObdService.this.forceReconnect();
                    return;
                case 9:
                    com.navdy.obd.ObdService.this.cancelCurrentSchedule();
                    com.navdy.obd.ObdService.this.setMode(msg.arg1);
                    com.navdy.obd.ObdService.this.forceReconnect();
                    return;
                default:
                    return;
            }
        }
    }

    public ObdService() {
        setVoltageSettings(new com.navdy.obd.VoltageSettings());
    }

    /* access modifiers changed from: private */
    public void setVoltageSettings(com.navdy.obd.VoltageSettings settings) {
        if (settings == null) {
            android.util.Log.e(TAG, "Invalid voltage settings " + settings);
            return;
        }
        android.util.Log.d(TAG, "Setting voltage settings to " + settings);
        synchronized (this.voltageSettingsLock) {
            this.voltageSettings = settings;
        }
    }

    /* access modifiers changed from: private */
    public void setMode(int mode) {
        if (mode != this.currentMode) {
            this.currentMode = mode;
            android.util.Log.d(TAG, "currentMode " + (this.currentMode == 0 ? "OBD2" : "J1939"));
            if (this.currentMode == 1 && this.j1939Profile == null) {
                this.j1939Profile = new com.navdy.obd.j1939.J1939Profile();
                this.j1939Profile.load(getResources().openRawResource(com.navdy.obd.R.raw.j1939_profile));
            }
        }
    }

    public boolean reconnectImmediately() {
        return this.reconnectImmediately;
    }

    public void forceReconnect() {
        if (this.channel != null) {
            android.util.Log.d(TAG, "Disconnecting channel as its connected");
            this.channel.disconnect();
            return;
        }
        android.util.Log.d(TAG, "Triggering reconnect, current state : " + this.connectionState);
        this.autoConnect.triggerReconnect();
    }

    /* access modifiers changed from: private */
    public void handleStateChange(int state) {
        if (state != this.connectionState) {
            this.connectionState = state;
            android.util.Log.i(TAG, "Switching to state:" + state);
            switch (this.connectionState) {
                case 0:
                    cancelCurrentSchedule();
                    cancelVoltageMonitoring();
                    closeChannel();
                    this.lastDisconnectTime = android.os.SystemClock.elapsedRealtime();
                    break;
                case 2:
                    if (!this.isScanningEnabled) {
                        android.util.Log.d(TAG, "handleStateChange : New State is 'connected' but scanning is not enabled , moving back to 'idle' state");
                        postStateChange(4);
                        break;
                    } else {
                        updateScan();
                        break;
                    }
                case 3:
                    resetChannel();
                    break;
                case 4:
                    if (!this.reconnectImmediately) {
                        resetObdChip();
                        monitorBatteryVoltage();
                        break;
                    } else {
                        try {
                            this.apiEndpoint.updateDeviceFirmware(this.firmwareUpdateVersion, this.firmwareUpdatePath);
                        } catch (android.os.RemoteException e) {
                            android.util.Log.d(TAG, "Error updating firmware ", e);
                        }
                        this.reconnectImmediately = false;
                        break;
                    }
                case 5:
                    sleepWithVoltageWakeUpTriggers();
                    break;
            }
            synchronized (this.listeners) {
                for (int i = this.listeners.size() - 1; i >= 0; i--) {
                    try {
                        ((com.navdy.obd.IObdServiceListener) this.listeners.get(i)).onConnectionStateChange(state);
                    } catch (android.os.DeadObjectException e2) {
                        android.util.Log.e(TAG, "Removing dead obd service listener");
                        this.listeners.remove(i);
                    } catch (android.os.RemoteException e3) {
                        android.util.Log.e(TAG, "Failed to notify listener of connection state change");
                    }
                }
            }
            synchronized (this.pidListeners) {
                for (int i2 = this.pidListeners.size() - 1; i2 >= 0; i2--) {
                    try {
                        ((com.navdy.obd.PidListener) this.pidListeners.valueAt(i2)).listener.onConnectionStateChange(state);
                    } catch (android.os.DeadObjectException e4) {
                        android.util.Log.e(TAG, "Removing dead pid listener");
                        this.pidListeners.removeAt(i2);
                    } catch (android.os.RemoteException e5) {
                        android.util.Log.e(TAG, "Failed to notify listener of connection state change");
                    }
                }
            }
        }
    }

    public void onCreate() {
        super.onCreate();
        android.util.Log.i(TAG, "Creating service");
        obdService = this;
        this.profile = new com.navdy.obd.Profile();
        this.profile.load(getResources().openRawResource(com.navdy.obd.R.raw.default_profile));
        this.mSharedPrefs = getSharedPreferences(PREFS_FILE_OBD_DEVICE, 0);
        this.debugMode = com.navdy.os.SystemProperties.getBoolean(OBD_DEBUG_MODE, false);
        this.lastConnectedBaudRate = this.mSharedPrefs.getInt(PREFS_LAST_CONNECTED_BAUD_RATE, BAUD_RATE);
        this.isScanningEnabled = this.mSharedPrefs.getBoolean(PREFS_KEY_OBD_SCAN_ENABLED, true);
        setMode(this.mSharedPrefs.getInt(PREFS_KEY_OBD_SCAN_MODE, DEFAULT_MODE));
        this.obdDataObserver = new com.navdy.obd.ObdDataObserver();
        android.os.HandlerThread thread = new android.os.HandlerThread("ObdServiceHandlerThread");
        thread.start();
        this.serviceLooper = thread.getLooper();
        this.serviceHandler = new com.navdy.obd.ObdService.ServiceHandler(this, this.serviceLooper, null);
    }

    public android.os.IBinder onBind(android.content.Intent intent) {
        android.util.Log.d(TAG, "onBind - " + intent.getAction());
        if (com.navdy.obd.IObdService.class.getName().equals(intent.getAction())) {
            android.util.Log.d(TAG, "returning IObdService API endpoint");
            return this.apiEndpoint;
        } else if (!com.navdy.obd.ICarService.class.getName().equals(intent.getAction())) {
            return null;
        } else {
            android.util.Log.d(TAG, "returning ICarService API endpoint");
            return this.carApiEndpoint;
        }
    }

    public int onStartCommand(android.content.Intent intent, int flags, int startId) {
        if (intent != null) {
            java.lang.String action = intent.getAction();
            if (com.navdy.obd.ObdServiceInterface.ACTION_START_AUTO_CONNECT.equals(action)) {
                if (this.autoConnect == null) {
                    this.autoConnect = new com.navdy.obd.jobs.AutoConnect(this);
                    this.autoConnect.start();
                }
            } else if (com.navdy.obd.ObdServiceInterface.ACTION_STOP_AUTO_CONNECT.equals(action)) {
                if (this.autoConnect != null) {
                    this.autoConnect.stop();
                }
                this.autoConnect = null;
            } else if (com.navdy.obd.ObdServiceInterface.ACTION_RESCAN.equals(action)) {
                this.serviceHandler.sendEmptyMessage(3);
            }
        }
        return 1;
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.autoConnect != null) {
            this.autoConnect.stop();
        }
        cancelCurrentSchedule();
        cancelVoltageMonitoring();
        this.serviceLooper.quit();
        android.util.Log.i(TAG, "Destroying service");
    }

    /* access modifiers changed from: private */
    public void applyFallbackConfiguration() {
        java.io.InputStream is = getResources().openRawResource(com.navdy.obd.R.raw.default_stn_configuration);
        java.lang.String configuration = com.navdy.obd.Utility.convertInputStreamToString(is);
        try {
            is.close();
        } catch (java.io.IOException e) {
            android.util.Log.e(TAG, "Error closing the input stream while reading from asset file ", e);
        }
        try {
            applyConfigurationInternal(configuration);
        } catch (Throwable t) {
            android.util.Log.e(TAG, "Critical error, failed to apply fallback configuration", t);
        }
    }

    /* access modifiers changed from: private */
    public void applyConfigurationInternal(java.lang.String configuration) throws java.lang.IllegalArgumentException, java.util.concurrent.ExecutionException {
        android.util.Log.d(TAG, "Applying device configuration");
        android.util.Log.d(TAG, "Executing the write configuration command");
        synchronousCommand(new com.navdy.obd.command.WriteConfigurationCommand(configuration));
        resetChannel();
    }

    public android.os.Handler getHandler() {
        return this.serviceHandler;
    }

    public com.navdy.obd.io.ChannelInfo getDefaultChannelInfo() {
        com.navdy.obd.io.ChannelInfo info = null;
        try {
            return com.navdy.obd.io.ChannelInfo.parse(this.mSharedPrefs.getString(PREFS_KEY_DEFAULT_CHANNEL_INFO, null));
        } catch (java.lang.Exception e) {
            android.util.Log.e(TAG, "Exception " + e);
            return info;
        }
    }

    public java.lang.String sendCommand(java.lang.String command) throws android.os.RemoteException {
        try {
            return synchronousCommand(new com.navdy.obd.command.ObdCommand(command));
        } catch (java.util.concurrent.ExecutionException e) {
            return null;
        }
    }

    public void setDefaultChannelInfo(com.navdy.obd.io.ChannelInfo info) {
        try {
            this.mSharedPrefs.edit().putString(PREFS_KEY_DEFAULT_CHANNEL_INFO, info.asString()).apply();
        } catch (java.lang.Exception e) {
            android.util.Log.e(TAG, "Exception " + e);
        }
    }

    /* access modifiers changed from: private */
    public void cancelVoltageMonitoring() {
        if (this.voltageMonitoring != null) {
            this.voltageMonitoring.cancel(false);
            this.voltageMonitoring = null;
        }
        this.currentBatteryVoltage = -1.0d;
    }

    /* access modifiers changed from: private */
    public void cancelCurrentSchedule() {
        boolean z = true;
        if (this.currentSchedule != null) {
            java.util.concurrent.ScheduledFuture scheduledFuture = this.currentSchedule;
            if (this.currentMode != 1) {
                z = false;
            }
            scheduledFuture.cancel(z);
            this.currentSchedule = null;
        }
        if (this.currentScan != null) {
            this.currentScan.cancel();
            this.currentScan = null;
        }
    }

    /* access modifiers changed from: private */
    public java.lang.String synchronousCommand(com.navdy.obd.command.ICommand command) throws java.util.concurrent.ExecutionException {
        cancelCurrentSchedule();
        com.navdy.obd.io.IChannel obdChannel = this.channel;
        if (obdChannel == null) {
            android.util.Log.e(TAG, "Invalid channel - unable to execute command");
            return null;
        }
        try {
            this.scheduler.submit(new com.navdy.obd.ObdJob(command, obdChannel, this.protocol, new com.navdy.obd.ObdService.Anon3(), this.obdDataObserver)).get();
            return command.getResponse();
        } catch (java.lang.InterruptedException e) {
            android.util.Log.e(TAG, "Interrupted while executing command", e);
            return e.getMessage();
        } catch (java.util.concurrent.ExecutionException e2) {
            android.util.Log.e(TAG, "Error executing command", e2);
            throw e2;
        }
    }

    public void addListener(java.util.List<com.navdy.obd.Pid> pids, com.navdy.obd.IPidListener listener) {
        com.navdy.obd.ScanSchedule schedule = new com.navdy.obd.ScanSchedule();
        schedule.addPids(pids, 500);
        addListener(schedule, listener);
    }

    public void addListener(com.navdy.obd.ScanSchedule schedule, com.navdy.obd.IPidListener listener) {
        synchronized (this.pidListeners) {
            android.util.Log.i(TAG, "adding listener " + listener + " ibinder:" + listener.asBinder());
            this.pidListeners.put(listener.asBinder(), new com.navdy.obd.PidListener(listener, schedule));
        }
        updateScan();
    }

    public void removeListener(com.navdy.obd.IPidListener listener) {
        synchronized (this.pidListeners) {
            android.util.Log.i(TAG, "removing listener " + listener + " binder:" + listener.asBinder());
            this.pidListeners.remove(listener.asBinder());
        }
        updateScan();
    }

    /* access modifiers changed from: private */
    public void updateScan() {
        android.util.Log.e(TAG, "updateScan");
        this.monitoringCanBusLimitReached = false;
        if (this.connectionState != 2) {
            android.util.Log.i(TAG, "ignoring scan update - not connected");
        } else if (!this.isScanningEnabled) {
            android.util.Log.i(TAG, "ignoring scan update - not enabled");
        } else {
            com.navdy.obd.ScanSchedule mergedSchedule = new com.navdy.obd.ScanSchedule();
            long now = android.os.SystemClock.elapsedRealtime();
            if (this.canBusMonitoringStatus.state == com.navdy.obd.ObdService.CANBusMonitoringState.SAMPLING || this.canBusMonitoringStatus.state == com.navdy.obd.ObdService.CANBusMonitoringState.MONITORING) {
                this.gatherCanBusDataToLogs = false;
                mergedSchedule.addPid(1001, 1000);
            }
            if (this.canBusMonitoringStatus.state == com.navdy.obd.ObdService.CANBusMonitoringState.MONITORING) {
                java.util.List<java.lang.Integer> pids = this.canBusMonitoringStatus.canBusMonitoringCommand.getMonitoredPidsList();
                if (pids != null) {
                    for (java.lang.Integer pid : pids) {
                        mergedSchedule.remove(pid.intValue());
                    }
                }
            }
            if (this.gatherCanBusDataToLogs && (this.lastMonitorBatchCompletedTime == 0 || now - this.lastMonitorBatchCompletedTime > 30000)) {
                android.util.Log.i(TAG, "Starting new batch of monitoring");
                this.monitorCounter = 0;
                mergedSchedule.addPid(1000, 1000);
            } else if (!this.gatherCanBusDataToLogs) {
                this.monitorCounter = 0;
                this.lastMonitorBatchCompletedTime = 0;
            }
            synchronized (this.pidListeners) {
                for (int i = 0; i < this.pidListeners.size(); i++) {
                    mergedSchedule.merge(((com.navdy.obd.PidListener) this.pidListeners.valueAt(i)).schedule);
                }
            }
            if (!mergedSchedule.isEmpty()) {
                this.mVehicleStateManager.resolveDependenciesForCustomPids(mergedSchedule);
                switch (this.currentMode) {
                    case 0:
                        scanPids(mergedSchedule);
                        return;
                    case 1:
                        monitorCANJ1939(mergedSchedule);
                        return;
                    default:
                        return;
                }
            } else {
                android.util.Log.i(TAG, "cancelling scan schedule - no pids to scan");
                cancelCurrentSchedule();
            }
        }
    }

    public void addListener(com.navdy.obd.IObdServiceListener listener) {
        synchronized (this.listeners) {
            this.listeners.add(listener);
        }
    }

    public void removeListener(com.navdy.obd.IObdServiceListener listener) {
        synchronized (this.listeners) {
            this.listeners.remove(listener);
        }
    }

    public void resetObdChip() {
        android.util.Log.i(TAG, "Resetting the obd chip");
        cancelVoltageMonitoring();
        try {
            android.util.Log.i(TAG, "Turning off echo");
            synchronousCommand(new com.navdy.obd.command.BatchCommand(new com.navdy.obd.command.ObdCommand("DUMMY", "\r"), com.navdy.obd.command.ObdCommand.RESET_COMMAND, com.navdy.obd.command.ObdCommand.ECHO_OFF_COMMAND));
        } catch (java.util.concurrent.ExecutionException e) {
            android.util.Log.i(TAG, "Failed to turn off echo", e);
        }
        if (this.channel == null) {
            android.util.Log.i(TAG, "Failed to reset the obd chip, Channel is null");
            return;
        }
        this.currentFirmwareVersion = null;
        if (this.firmwareManager != null) {
            this.currentFirmwareVersion = this.firmwareManager.getFirmwareVersion();
        }
        android.util.Log.i(TAG, "Current firmware version of the Obd chip : " + this.currentFirmwareVersion);
    }

    public void monitorBatteryVoltage() {
        android.util.Log.i(TAG, "Starting to monitor voltage");
        if (this.channel == null) {
            android.util.Log.i(TAG, "Failed to start monitoring voltage");
            return;
        }
        com.navdy.obd.command.ReadVoltageCommand command = new com.navdy.obd.command.ReadVoltageCommand();
        java.util.concurrent.ScheduledExecutorService scheduledExecutorService = this.scheduler;
        this.voltageMonitoring = scheduledExecutorService.scheduleWithFixedDelay(new com.navdy.obd.ObdJob(command, this.channel, null, new com.navdy.obd.ObdService.Anon4(command), this.obdDataObserver), 0, 2000, java.util.concurrent.TimeUnit.MILLISECONDS);
    }

    public void sleepWithVoltageWakeUpTriggers() {
        cancelVoltageMonitoring();
        cancelCurrentSchedule();
        com.navdy.obd.command.BatchCommand sleepCommand = new com.navdy.obd.command.BatchCommand(com.navdy.obd.command.ObdCommand.ECHO_OFF_COMMAND);
        synchronized (this.voltageSettingsLock) {
            if (this.deepSleep) {
                com.navdy.obd.command.ObdCommand chargingVoltageTrigger = com.navdy.obd.command.ObdCommand.createSetVoltageLevelWakeupTriggerCommand(false, this.voltageSettings.chargingVoltage, 0);
                sleepCommand.add(com.navdy.obd.command.ObdCommand.TURN_OFF_VOLTAGE_DELTA_WAKEUP);
                sleepCommand.add(com.navdy.obd.command.ObdCommand.TURN_ON_VOLTAGE_LEVEL_WAKEUP);
                sleepCommand.add(chargingVoltageTrigger);
            } else {
                com.navdy.obd.command.ObdCommand lowVoltageTrigger = com.navdy.obd.command.ObdCommand.createSetVoltageLevelWakeupTriggerCommand(true, this.voltageSettings.lowBatteryVoltage, 10);
                sleepCommand.add(com.navdy.obd.command.ObdCommand.TURN_ON_VOLTAGE_DELTA_WAKEUP);
                sleepCommand.add(com.navdy.obd.command.ObdCommand.TURN_ON_VOLTAGE_LEVEL_WAKEUP);
                sleepCommand.add(lowVoltageTrigger);
            }
            sleepCommand.add(com.navdy.obd.command.ObdCommand.RESET_COMMAND);
            sleepCommand.add(com.navdy.obd.command.ObdCommand.ECHO_OFF_COMMAND);
        }
        android.util.Log.d(TAG, "Configuring sleep triggers:" + sleepCommand.getName());
        try {
            synchronousCommand(sleepCommand);
            com.navdy.obd.command.ObdCommand waitCommand = com.navdy.obd.command.ObdCommand.SLEEP_COMMAND;
            com.navdy.obd.io.IChannel obdChannel = this.channel;
            if (obdChannel == null) {
                android.util.Log.e(TAG, "Invalid channel - unable to execute command");
            } else {
                this.scheduler.submit(new com.navdy.obd.ObdJob(waitCommand, obdChannel, this.protocol, new com.navdy.obd.ObdService.Anon5(), this.obdDataObserver));
            }
        } catch (java.util.concurrent.ExecutionException e) {
            android.util.Log.i(TAG, "Failed to enable monitoring during sleep", e);
        }
    }

    public void scanPids(java.util.List<com.navdy.obd.Pid> pids, int intervalMs) {
        com.navdy.obd.ScanSchedule schedule = new com.navdy.obd.ScanSchedule();
        schedule.addPids(pids, intervalMs);
        scanPids(schedule);
    }

    public void monitorCANJ1939(com.navdy.obd.ScanSchedule scanSchedule) {
        if (this.currentSchedule == null || this.currentSchedule.isDone()) {
            cancelVoltageMonitoring();
            android.util.Log.d(TAG, "Starting to monitor the CAN bus for data, using SAE J1939");
            java.util.List<com.navdy.obd.ScanSchedule.Scan> scanList = scanSchedule.getScanList();
            java.util.List<com.navdy.obd.Pid> pids = new java.util.ArrayList<>();
            for (com.navdy.obd.ScanSchedule.Scan scan : scanList) {
                pids.add(new com.navdy.obd.Pid(scan.pid));
            }
            com.navdy.obd.j1939.J1939ObdJobAdapter jobdAdpater = new com.navdy.obd.j1939.J1939ObdJobAdapter(this.channel, this.j1939Profile, pids, this.mVehicleStateManager, this.obdDataObserver);
            jobdAdpater.setListener(new com.navdy.obd.ObdService.Anon6(jobdAdpater));
            this.currentSchedule = this.scheduler.schedule(jobdAdpater, 0, java.util.concurrent.TimeUnit.MILLISECONDS);
            return;
        }
        android.util.Log.d(TAG, "Monitoring is already running");
    }

    public void scanPids(com.navdy.obd.ScanSchedule schedule) {
        cancelCurrentSchedule();
        android.util.Log.d(TAG, "starting scan for " + schedule);
        java.util.concurrent.ScheduledExecutorService scheduledExecutorService = this.scheduler;
        this.currentSchedule = scheduledExecutorService.scheduleWithFixedDelay(new com.navdy.obd.jobs.ScanPidsJob(this.vehicleInfo, this.profile, schedule, this.mVehicleStateManager, this.channel, this.protocol, new com.navdy.obd.ObdService.Anon7(), this.obdDataObserver), 0, 10, java.util.concurrent.TimeUnit.MILLISECONDS);
    }

    /* access modifiers changed from: private */
    public void updateListenersWithNewData(java.util.List<com.navdy.obd.Pid> pidsData) {
        synchronized (this.listeners) {
            for (com.navdy.obd.IObdServiceListener listener : this.listeners) {
                try {
                    listener.scannedPids(pidsData);
                } catch (android.os.RemoteException e) {
                    android.util.Log.e(TAG, "Error notifying listener", e);
                }
            }
        }
        synchronized (this.pidListeners) {
            for (int i = this.pidListeners.size() - 1; i >= 0; i--) {
                try {
                    ((com.navdy.obd.PidListener) this.pidListeners.valueAt(i)).recordReadings(pidsData);
                } catch (android.os.DeadObjectException e2) {
                    android.util.Log.e(TAG, "Removing dead pid listener");
                    this.pidListeners.removeAt(i);
                } catch (android.os.RemoteException e3) {
                    android.util.Log.e(TAG, "Error notifying pid listener", e3);
                }
            }
        }
    }

    public void scanVIN() {
        this.scheduler.submit(new com.navdy.obd.ObdService.Anon8());
    }

    public void submitCommand(com.navdy.obd.command.ObdCommand command, com.navdy.obd.ObdJob.IListener listener) {
        this.scheduler.submit(new com.navdy.obd.ObdJob(command, this.channel, this.protocol, listener, this.obdDataObserver));
    }

    public void scanSupportedPids() {
        this.scheduler.submit(new com.navdy.obd.ObdService.Anon9());
    }

    private com.navdy.obd.Protocol getSavedProtocol() {
        int index = com.navdy.os.SystemProperties.getInt(OVERRIDE_PROTOCOL, this.mSharedPrefs.getInt(PREFS_KEY_LAST_PROTOCOL, 0));
        if (index <= 0 || index - 1 >= com.navdy.obd.Protocol.PROTOCOLS.length) {
            return null;
        }
        return com.navdy.obd.Protocol.PROTOCOLS[index - 1];
    }

    public void resetChannel() {
        com.navdy.obd.Protocol parse;
        android.util.Log.i(TAG, "Resetting channel");
        cancelCurrentSchedule();
        if (!this.isScanningEnabled) {
            postStateChange(4);
            return;
        }
        this.canBusMonitoringStatus.state = com.navdy.obd.ObdService.CANBusMonitoringState.UNKNOWN;
        this.onMonitoringStatusReported = false;
        int newState = 4;
        if (this.channel != null) {
            if (this.currentMode == 0) {
                try {
                    this.protocol = null;
                    com.navdy.obd.command.InitializeCommand initializeCommand = new com.navdy.obd.command.InitializeCommand(getSavedProtocol());
                    synchronousCommand(initializeCommand);
                    java.lang.String protocolResponse = initializeCommand.getProtocol();
                    if (android.text.TextUtils.isEmpty(protocolResponse)) {
                        parse = null;
                    } else {
                        parse = com.navdy.obd.Protocol.parse(protocolResponse);
                    }
                    this.protocol = parse;
                    android.util.Log.i(TAG, "Protocol response:" + protocolResponse);
                    if (this.protocol != null) {
                        this.configurationName = initializeCommand.getConfigurationName();
                        android.util.Log.i(TAG, "Configuration name :" + this.configurationName);
                        android.util.Log.i(TAG, "Initialized - " + initializeCommand.getResponse() + ", Protocol:" + this.protocol);
                        android.util.Log.i(TAG, "Scanning for VIN");
                        com.navdy.obd.command.VinCommand vinCommand = new com.navdy.obd.command.VinCommand();
                        synchronousCommand(vinCommand);
                        android.util.Log.i(TAG, "Scanning for ECUs");
                        com.navdy.obd.command.ValidPidsCommand command = new com.navdy.obd.command.ValidPidsCommand();
                        synchronousCommand(command);
                        java.util.List<com.navdy.obd.ECU> ecus = buildEcus(command);
                        com.navdy.obd.command.CheckDTCCommand checkDTCCommand = new com.navdy.obd.command.CheckDTCCommand();
                        if (ecus != null && ecus.size() > 0) {
                            checkDTCCommand.setExpectedResponses(ecus.size());
                        }
                        synchronousCommand(checkDTCCommand);
                        boolean isCheckEngineLightIsOn = checkDTCCommand.isCheckEngineLightOn();
                        java.util.List<java.lang.String> troubleCodes = null;
                        if (checkDTCCommand.getNumberOfTroubleCodes() > 0) {
                            com.navdy.obd.command.ReadTroubleCodesCommand readTroubleCodesCommand = new com.navdy.obd.command.ReadTroubleCodesCommand();
                            if (ecus != null && ecus.size() > 0) {
                                checkDTCCommand.setExpectedResponses(ecus.size());
                            }
                            synchronousCommand(readTroubleCodesCommand);
                            troubleCodes = readTroubleCodesCommand.getTroubleCodes();
                            if (troubleCodes != null) {
                                for (java.lang.String troubleCode : troubleCodes) {
                                    android.util.Log.d(TAG, troubleCode);
                                }
                            }
                        }
                        android.util.Log.i(TAG, "Building vehicle info");
                        this.vehicleInfo = build(this.protocol, ecus, vinCommand.getVIN(), isCheckEngineLightIsOn, troubleCodes);
                        dump(this.vehicleInfo);
                        com.navdy.obd.ECU primaryEcu = this.vehicleInfo.getPrimaryEcu();
                        if (primaryEcu != null) {
                            com.navdy.obd.PidSet pids = primaryEcu.supportedPids;
                            this.mVehicleStateManager = new com.navdy.obd.VehicleStateManager(new com.navdy.obd.DefaultPidProcessorFactory());
                            this.mVehicleStateManager.updateSupportedPids(pids);
                            this.mSharedPrefs.edit().putInt(PREFS_KEY_LAST_PROTOCOL, this.protocol.id).apply();
                            newState = 2;
                        } else {
                            android.util.Log.i(TAG, "No ECUs found - falling back to IDLE state");
                            this.lastDisconnectTime = android.os.SystemClock.elapsedRealtime();
                        }
                    }
                } catch (Throwable t) {
                    android.util.Log.e(TAG, "Error while resetting the channel ", t);
                }
            } else {
                com.navdy.obd.command.InitializeJ1939Command initializeCommand2 = new com.navdy.obd.command.InitializeJ1939Command();
                try {
                    synchronousCommand(initializeCommand2);
                    this.configurationName = initializeCommand2.getConfigurationName();
                    android.util.Log.i(TAG, "Configuration name :" + this.configurationName);
                    android.util.Log.i(TAG, "Scanning for VIN");
                    com.navdy.obd.command.VinCommand vinCommand2 = new com.navdy.obd.command.VinCommand();
                    try {
                        synchronousCommand(vinCommand2);
                    } catch (Throwable th) {
                        android.util.Log.i(TAG, "Failed to read VIN - trying to connect anyway");
                    }
                    this.vehicleInfo = new com.navdy.obd.VehicleInfo(vinCommand2.getVIN());
                    dump(this.vehicleInfo);
                    this.mVehicleStateManager = new com.navdy.obd.VehicleStateManager(new com.navdy.obd.DefaultPidProcessorFactory());
                    this.mVehicleStateManager.updateSupportedPids(com.navdy.obd.j1939.J1939ObdJobAdapter.J1939SupportedObdPids);
                    newState = 2;
                } catch (java.util.concurrent.ExecutionException e) {
                    android.util.Log.e(TAG, "Error while resetting the channel ", e);
                }
            }
        }
        postStateChange(newState);
    }

    public void onLogFileRollOver(java.lang.String loggerName, java.lang.String fileName) {
        android.util.Log.d(TAG, "onLogFileRollover Logger :" + loggerName + ", File name : " + fileName);
        if (CAN_BUS_LOGGER.equals(loggerName) && this.gatherCanBusDataToLogs) {
            this.monitoringCanBusLimitReached = false;
            android.util.Log.d(TAG, "onLogFileRollOver , new data file is available " + fileName);
            if (this.canBusMonitoringListener != null) {
                try {
                    this.canBusMonitoringListener.onNewDataAvailable(fileName);
                } catch (android.os.RemoteException e) {
                    android.util.Log.e(TAG, "RemoteException ", e);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void postStateChange(int newState) {
        this.serviceHandler.obtainMessage(1, newState, -1).sendToTarget();
    }

    private void dump(com.navdy.obd.VehicleInfo vehicleInfo2) {
        android.util.Log.i(TAG, "Vehicle Info:");
        android.util.Log.i(TAG, "Protocol:" + vehicleInfo2.protocol);
        android.util.Log.i(TAG, "VIN:" + vehicleInfo2.vin);
        if (vehicleInfo2.ecus != null) {
            android.util.Log.i(TAG, "ecus:" + vehicleInfo2.ecus.size());
            com.navdy.obd.ECU primaryEcu = vehicleInfo2.getPrimaryEcu();
            for (com.navdy.obd.ECU ecu : vehicleInfo2.ecus) {
                android.util.Log.i(TAG, "  ecu: 0x" + java.lang.Integer.toHexString(ecu.address) + (primaryEcu == ecu ? " PRIMARY" : ""));
                android.util.Log.i(TAG, "     : " + java.util.Arrays.toString(ecu.supportedPids.asList().toArray()));
            }
        }
        android.util.Log.i(TAG, "Check Engine light : " + (vehicleInfo2.isCheckEngineLightOn ? "ON" : "OFF"));
        if (vehicleInfo2.troubleCodes != null) {
            android.util.Log.i(TAG, "Trouble Codes");
            for (java.lang.String troubleCode : vehicleInfo2.troubleCodes) {
                android.util.Log.d(TAG, troubleCode);
            }
        }
    }

    public static java.util.List<com.navdy.obd.ECU> buildEcus(com.navdy.obd.command.ValidPidsCommand myCommand) {
        android.util.SparseArray<com.navdy.obd.ECU> ecusByAddress = new android.util.SparseArray<>();
        int offset = 0;
        for (com.navdy.obd.command.ObdCommand command : myCommand.supportedPids()) {
            for (int i = 0; i < command.getResponseCount(); i++) {
                com.navdy.obd.EcuResponse response = command.getResponse(i);
                int address = response.ecu;
                com.navdy.obd.ECU ecu = (com.navdy.obd.ECU) ecusByAddress.get(address);
                if (ecu == null) {
                    ecu = new com.navdy.obd.ECU(address, new com.navdy.obd.PidSet());
                    ecusByAddress.put(address, ecu);
                }
                if (response.data[0] != 65 || response.length < 6) {
                    android.util.Log.i(TAG, "Skipping " + java.lang.Integer.toHexString(response.data[0]) + " response for ecu " + java.lang.Integer.toHexString(address));
                } else {
                    int bitField = java.nio.ByteBuffer.wrap(response.data, 2, 4).getInt();
                    android.util.Log.i(TAG, "Merging " + java.lang.Integer.toHexString(bitField) + " for ecu " + java.lang.Integer.toHexString(address) + " at offset " + java.lang.Integer.toHexString(offset));
                    ecu.supportedPids.merge(new com.navdy.obd.PidSet((long) bitField, offset));
                }
            }
            offset += 32;
        }
        java.util.List<com.navdy.obd.ECU> ecus = new java.util.ArrayList<>();
        for (int i2 = 0; i2 < ecusByAddress.size(); i2++) {
            ecus.add(ecusByAddress.valueAt(i2));
        }
        return ecus;
    }

    static com.navdy.obd.VehicleInfo build(com.navdy.obd.Protocol protocol2, java.util.List<com.navdy.obd.ECU> ecus, java.lang.String vin, boolean isScanningEnabled2, java.util.List<java.lang.String> troubleCodes) {
        return new com.navdy.obd.VehicleInfo(protocol2 != null ? protocol2.name : "", ecus, vin, isScanningEnabled2, troubleCodes);
    }

    public com.navdy.obd.Protocol getProtocol() {
        return this.protocol;
    }

    public void openChannel(com.navdy.obd.io.ChannelInfo info) {
        if (info != null) {
            if (this.channelInfo == null || !this.channelInfo.equals(info)) {
                if (this.channel != null) {
                    this.channel.disconnect();
                    this.channel = null;
                }
                this.channelInfo = info;
                com.navdy.obd.io.ChannelInfo.ConnectionType channelType = info.getConnectionType();
                android.util.Log.i(TAG, "Opening channel " + info.asString());
                switch (channelType) {
                    case SERIAL:
                        this.channel = new com.navdy.obd.io.STNSerialChannel(this, this, this.lastConnectedBaudRate, BAUD_RATE);
                        if (this.firmwareManager != null && (this.firmwareManager instanceof com.navdy.obd.update.STNSerialDeviceFirmwareManager)) {
                            ((com.navdy.obd.update.STNSerialDeviceFirmwareManager) this.firmwareManager).init(this, this.channel);
                            break;
                        } else {
                            this.firmwareManager = new com.navdy.obd.update.STNSerialDeviceFirmwareManager(this, this.channel);
                            break;
                        }
                        break;
                    case BLUETOOTH:
                        this.channel = new com.navdy.obd.io.BluetoothChannel(this);
                        break;
                    case MOCK:
                        this.channel = new com.navdy.obd.simulator.MockObdChannel(this);
                        break;
                }
                if (this.firmwareManager == null) {
                    this.firmwareManager = new com.navdy.obd.update.DefaultFirmwareManager();
                }
                this.channel.connect(info.getAddress());
            }
        }
    }

    public void closeChannel() {
        this.vehicleInfo = null;
        this.protocol = null;
        this.currentBatteryVoltage = -1.0d;
        if (this.channelInfo != null) {
            this.channelInfo = null;
        }
        if (this.channel != null) {
            this.channel = null;
        }
    }

    public void onStateChange(int newState) {
        if (newState == 2) {
            if (this.channel instanceof com.navdy.obd.io.STNSerialChannel) {
                this.lastConnectedBaudRate = ((com.navdy.obd.io.STNSerialChannel) this.channel).getBaudRate();
                this.mSharedPrefs.edit().putInt(PREFS_LAST_CONNECTED_BAUD_RATE, this.lastConnectedBaudRate).apply();
            }
            newState = 4;
        }
        postStateChange(newState);
    }

    public void onMessage(java.lang.String message) {
        android.os.Message msg = this.serviceHandler.obtainMessage(2);
        android.os.Bundle bundle = new android.os.Bundle();
        bundle.putString("toast", message);
        msg.setData(bundle);
        this.serviceHandler.sendMessage(msg);
    }

    public static com.navdy.obd.ObdService getObdService() {
        return obdService;
    }
}
