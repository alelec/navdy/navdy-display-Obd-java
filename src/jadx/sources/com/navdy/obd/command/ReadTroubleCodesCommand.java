package com.navdy.obd.command;

public class ReadTroubleCodesCommand extends com.navdy.obd.command.ObdCommand {
    private static final java.lang.String[] TROUBLE_CATEGORY = {"P", "C", "B", "U"};

    public ReadTroubleCodesCommand() {
        super("03");
    }

    public java.util.List<java.lang.String> getTroubleCodes() {
        int responseCount = getResponseCount();
        java.util.List<java.lang.String> troubleCodes = new java.util.ArrayList<>();
        for (int i = 0; i < responseCount; i++) {
            com.navdy.obd.EcuResponse ecuResponse = getResponse(i);
            if (ecuResponse != null) {
                byte[] response = getResponse(i).data;
                java.lang.String responseAsString = java.util.Arrays.toString(response);
                Log.info("Response of READTroubleCodesCommand, ECU {} , Data {}", (java.lang.Object) java.lang.Integer.valueOf(ecuResponse.ecu), (java.lang.Object) responseAsString);
                if (response != null && response[0] == 67 && response.length >= 3) {
                    byte b = response[1] & com.navdy.obd.update.STNBootloaderChannel.ERROR_CODE_INVALID_RESPONSE;
                    for (int j = 2; j < response.length; j += 2) {
                        int firstHex = response[j] & 255;
                        int categoryIdentifierNibble = firstHex >> 4;
                        int secondNibble = firstHex & 15;
                        if (j + 1 >= response.length) {
                            break;
                        }
                        int secondHex = response[j + 1] & 255;
                        if (categoryIdentifierNibble <= 15) {
                            java.lang.String categoryIdentifier = TROUBLE_CATEGORY[categoryIdentifierNibble / 4] + java.lang.String.format("%d", new java.lang.Object[]{java.lang.Integer.valueOf(categoryIdentifierNibble % 4)});
                            if (firstHex == 0 && secondHex == 0) {
                                break;
                            }
                            troubleCodes.add(categoryIdentifier + java.lang.String.format("%x%02x", new java.lang.Object[]{java.lang.Integer.valueOf(secondNibble), java.lang.Integer.valueOf(secondHex)}));
                        }
                    }
                }
            }
        }
        return troubleCodes;
    }
}
