package com.navdy.obd.command;

public class VinCommand extends com.navdy.obd.command.ObdCommand {
    private static final int DATA_LEN = 4;
    private static final int HEADER_LEN = 3;
    static final org.slf4j.Logger Log = org.slf4j.LoggerFactory.getLogger(com.navdy.obd.command.VinCommand.class);
    private static final int VIN_LENGTH = 20;

    public VinCommand() {
        super("0902");
    }

    public java.lang.String getVIN() {
        byte[] response = getByteResponse();
        if (response == null) {
            return null;
        }
        try {
            if (response[0] != 73) {
                return new java.lang.String(response).trim();
            }
            if (response.length <= 23) {
                return new java.lang.String(response, 3, response.length - 3).trim();
            }
            java.lang.StringBuilder vin = new java.lang.StringBuilder();
            for (int pos = 3; pos + 4 <= response.length; pos += 7) {
                vin.append(new java.lang.String(response, pos, 4));
            }
            return vin.toString().trim();
        } catch (java.lang.Exception e) {
            Log.error("Failed to parse vin", (java.lang.Throwable) e);
            return null;
        }
    }
}
