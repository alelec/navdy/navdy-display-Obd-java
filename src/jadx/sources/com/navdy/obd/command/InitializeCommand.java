package com.navdy.obd.command;

public class InitializeCommand extends com.navdy.obd.command.BaseSTNInitializeCommand {
    private com.navdy.obd.command.ObdCommand readProtocolCommand = com.navdy.obd.command.ObdCommand.READ_PROTOCOL_COMMAND;
    private com.navdy.obd.command.VinCommand vinCommand = new com.navdy.obd.command.VinCommand();

    public InitializeCommand(com.navdy.obd.Protocol protocol) {
        super(com.navdy.obd.command.ObdCommand.TURN_OFF_VOLTAGE_DELTA_WAKEUP, com.navdy.obd.command.ObdCommand.RESET_COMMAND, com.navdy.obd.command.ObdCommand.ECHO_OFF_COMMAND, com.navdy.obd.command.ObdCommand.HEADERS_ON_COMMAND, com.navdy.obd.command.ObdCommand.SPACES_OFF_COMMAND, com.navdy.obd.command.ObdCommand.TURN_ON_FORMATTING_COMMAND, com.navdy.obd.command.ObdCommand.TURN_OFF_VOLTAGE_LEVEL_WAKEUP, com.navdy.obd.command.ObdCommand.ALLOW_LONG_MESSAGES);
        if (protocol != null) {
            add(new com.navdy.obd.command.ObdCommand("SetProtocol", "atsp A" + java.lang.Integer.toHexString(protocol.id)));
        } else {
            add(com.navdy.obd.command.ObdCommand.AUTO_PROTOCOL_COMMAND);
        }
        add(com.navdy.obd.command.ObdCommand.DETECT_PROTOCOL_COMMAND);
        add(this.readProtocolCommand);
    }

    public java.lang.String getProtocol() {
        return this.readProtocolCommand.getResponse();
    }
}
