package com.navdy.obd.command;

public class ReadVoltageCommand extends com.navdy.obd.command.ObdCommand {
    public ReadVoltageCommand() {
        super("atrv");
    }

    public float getVoltage() {
        java.lang.String response = getResponse().trim();
        if (response.endsWith("V")) {
            response = response.substring(0, response.length() - 1);
        }
        java.lang.Float voltage = java.lang.Float.valueOf(0.0f);
        try {
            voltage = java.lang.Float.valueOf(java.lang.Float.parseFloat(response));
        } catch (java.lang.NumberFormatException e) {
            Log.warn("Unable to parse ({}) as float voltage", (java.lang.Object) response);
        }
        return voltage.floatValue();
    }
}
