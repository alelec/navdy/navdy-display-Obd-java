package com.navdy.obd.command;

public class ObdCommand implements com.navdy.obd.command.ICommand {
    public static final com.navdy.obd.command.ObdCommand ALLOW_LONG_MESSAGES = new com.navdy.obd.command.ObdCommand("ATAL");
    public static final int ANY_ECU = -1;
    public static final com.navdy.obd.command.ObdCommand AUTO_PROTOCOL_COMMAND = new com.navdy.obd.command.ObdCommand("SP", "atsp A6");
    private static final int CARRIAGE_RETURN = 13;
    public static final com.navdy.obd.command.ObdCommand CLEAR_ALL_PASS_FILTERS_COMMAND = new com.navdy.obd.command.ObdCommand("STFCP");
    public static final com.navdy.obd.command.ObdCommand DETECT_PROTOCOL_COMMAND = new com.navdy.obd.command.ObdCommand("Detect", "0100");
    public static final com.navdy.obd.command.ObdCommand DEVICE_INFO_COMMAND = new com.navdy.obd.command.ObdCommand("ATI", "ATI");
    public static final com.navdy.obd.command.ObdCommand ECHO_OFF_COMMAND = new com.navdy.obd.command.ObdCommand("ECHO OFF", "ate0");
    public static final com.navdy.obd.command.ObdCommand HEADERS_OFF_COMMAND = new com.navdy.obd.command.ObdCommand("HDR OFF", "ath0");
    public static final com.navdy.obd.command.ObdCommand HEADERS_ON_COMMAND = new com.navdy.obd.command.ObdCommand("HDR ON", "ath1");
    static final org.slf4j.Logger Log = org.slf4j.LoggerFactory.getLogger(com.navdy.obd.command.ObdCommand.class);
    public static final long NANOS_PER_MS = 1000000;
    public static final int NO_DATA = Integer.MIN_VALUE;
    public static final int NO_FILTER = 0;
    public static final java.lang.String OK = "OK";
    public static final com.navdy.obd.command.ObdCommand READ_PROTOCOL_COMMAND = new com.navdy.obd.command.ObdCommand("DP", "atdpn");
    public static final com.navdy.obd.command.ObdCommand RESET_COMMAND = new com.navdy.obd.command.ObdCommand("INIT", "atws");
    public static final com.navdy.obd.command.ObdCommand SCAN_COMMAND = new com.navdy.obd.command.ObdCommand("SCAN", "atma");
    public static final com.navdy.obd.command.ObdCommand SLEEP_COMMAND = new com.navdy.obd.command.ObdCommand("STSLEEP");
    public static final com.navdy.obd.command.ObdCommand SPACES_OFF_COMMAND = new com.navdy.obd.command.ObdCommand("SPACE", "ats0");
    public static final long TIMEOUT_MILLS = 10000;
    public static final com.navdy.obd.command.ObdCommand TURN_OFF_FORMATTING_COMMAND = new com.navdy.obd.command.ObdCommand("CAN Formatting OFF", "ATCAF0");
    public static final com.navdy.obd.command.ObdCommand TURN_OFF_VOLTAGE_DELTA_WAKEUP = new com.navdy.obd.command.ObdCommand("STSLVG off");
    public static final com.navdy.obd.command.ObdCommand TURN_OFF_VOLTAGE_LEVEL_WAKEUP = new com.navdy.obd.command.ObdCommand("STSLVL off,off");
    public static final com.navdy.obd.command.ObdCommand TURN_ON_FORMATTING_COMMAND = new com.navdy.obd.command.ObdCommand("CAN Formatting OFF", "ATCAF1");
    public static final com.navdy.obd.command.ObdCommand TURN_ON_VOLTAGE_DELTA_WAKEUP = new com.navdy.obd.command.ObdCommand("STSLVG on");
    public static final com.navdy.obd.command.ObdCommand TURN_ON_VOLTAGE_LEVEL_WAKEUP = new com.navdy.obd.command.ObdCommand("STSLVL off,on");
    public static final int UNKNOWN = -1;
    public static final java.lang.String WAIT = "WAIT";
    private static java.util.concurrent.ExecutorService singleThreadedExecutor = java.util.concurrent.Executors.newSingleThreadExecutor();
    /* access modifiers changed from: private */
    public java.lang.String command;
    /* access modifiers changed from: private */
    public byte[] commandBytes;
    private com.navdy.obd.converters.AbstractConversion conversion;
    /* access modifiers changed from: private */
    public java.io.IOException error;
    /* access modifiers changed from: private */
    public int expectedResponses;
    private java.lang.String name;
    /* access modifiers changed from: private */
    public java.lang.String response;
    /* access modifiers changed from: private */
    public com.navdy.obd.EcuResponse[] responses;
    private int targetEcu;
    private long timeoutMillis;
    private long timeoutNanos;

    class Anon1 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.obd.command.IObdDataObserver val$commandObserver;
        final /* synthetic */ java.io.InputStream val$input;
        final /* synthetic */ java.io.OutputStream val$output;
        final /* synthetic */ com.navdy.obd.Protocol val$protocol;

        Anon1(java.io.InputStream inputStream, java.io.OutputStream outputStream, com.navdy.obd.command.IObdDataObserver iObdDataObserver, com.navdy.obd.Protocol protocol) {
            this.val$input = inputStream;
            this.val$output = outputStream;
            this.val$commandObserver = iObdDataObserver;
            this.val$protocol = protocol;
        }

        public void run() {
            try {
                com.navdy.obd.io.ObdCommandInterpreter obdCommandInterpreter = new com.navdy.obd.io.ObdCommandInterpreter(this.val$input, this.val$output);
                com.navdy.obd.command.ObdCommand.this.responses = null;
                com.navdy.obd.command.ObdCommand.this.response = null;
                if (com.navdy.obd.command.ObdCommand.this.commandBytes == null) {
                    obdCommandInterpreter.write(com.navdy.obd.command.ObdCommand.this.command + (com.navdy.obd.command.ObdCommand.this.expectedResponses != -1 ? java.lang.Character.valueOf((char) (com.navdy.obd.command.ObdCommand.this.expectedResponses + 48)) : ""));
                    com.navdy.obd.command.ObdCommand.this.commandBytes = obdCommandInterpreter.getCommandBytes();
                } else {
                    obdCommandInterpreter.write(com.navdy.obd.command.ObdCommand.this.commandBytes);
                }
                com.navdy.obd.command.ObdCommand.Log.debug("wrote: {}{} ", (java.lang.Object) com.navdy.obd.command.ObdCommand.this.command, com.navdy.obd.command.ObdCommand.this.expectedResponses != -1 ? java.lang.Integer.valueOf(com.navdy.obd.command.ObdCommand.this.expectedResponses) : "");
                if (this.val$commandObserver != null) {
                    this.val$commandObserver.onCommand(new java.lang.String(com.navdy.obd.command.ObdCommand.this.commandBytes));
                }
                com.navdy.obd.command.ObdCommand.this.processCommandResponse(this.val$protocol, obdCommandInterpreter, this.val$commandObserver);
            } catch (java.io.IOException ie) {
                com.navdy.obd.command.ObdCommand.this.error = ie;
                if (this.val$commandObserver != null) {
                    this.val$commandObserver.onError(ie.toString());
                }
            }
        }
    }

    public static com.navdy.obd.command.ObdCommand createSetVoltageLevelWakeupTriggerCommand(boolean below, float voltage, int seconds) {
        return new com.navdy.obd.command.ObdCommand("STSLVLW " + (below ? "<" : ">") + java.lang.Float.toString(voltage) + ", " + java.lang.Integer.toString(seconds));
    }

    public ObdCommand(java.lang.String name2, java.lang.String command2, com.navdy.obd.converters.AbstractConversion conversion2, long timeOut) {
        this(name2, command2, conversion2);
        this.timeoutMillis = timeOut;
        this.timeoutNanos = 1000000 * timeOut;
    }

    public ObdCommand(java.lang.String name2, java.lang.String command2, com.navdy.obd.converters.AbstractConversion conversion2) {
        this.expectedResponses = -1;
        this.targetEcu = -1;
        this.timeoutMillis = 10000;
        this.timeoutNanos = 10000000000L;
        this.name = name2;
        this.conversion = conversion2;
        setCommand(command2);
    }

    public ObdCommand(java.lang.String command2) {
        this(command2, command2, null);
    }

    public ObdCommand(java.lang.String name2, java.lang.String command2) {
        this(name2, command2, null);
    }

    public void setCommand(java.lang.String command2) {
        this.command = command2;
        this.commandBytes = null;
    }

    public com.navdy.obd.command.ObdCommand clone() {
        return new com.navdy.obd.command.ObdCommand(this.name, this.command, this.conversion, this.timeoutMillis);
    }

    public java.lang.String getCommand() {
        return this.command;
    }

    public void setExpectedResponses(int count) {
        if (count > 9 || count == 0 || count < -1) {
            throw new java.lang.IllegalArgumentException("invalid expected count " + count);
        }
        this.expectedResponses = count;
        this.commandBytes = null;
    }

    public void setTargetEcu(int ecuAddress) {
        if (ecuAddress != this.targetEcu) {
            this.targetEcu = ecuAddress;
        }
    }

    public int getTargetEcu() {
        return this.targetEcu;
    }

    public java.lang.String getName() {
        return this.name;
    }

    public byte[] getByteResponse() {
        return getByteResponse(0);
    }

    public byte[] getByteResponse(int filter) {
        return getEcuResponse(this.targetEcu, filter);
    }

    public byte[] getEcuResponse(int ecu) {
        return getEcuResponse(ecu, 0);
    }

    public byte[] getEcuResponse(int ecu, int filter) {
        if (this.responses == null) {
            return null;
        }
        for (com.navdy.obd.EcuResponse response2 : this.responses) {
            if (ecu == -1 || response2.ecu == ecu) {
                byte[] data = response2.data;
                if (filter == 0) {
                    return data;
                }
                if (data != null && data[0] == filter) {
                    return data;
                }
            }
        }
        return null;
    }

    public com.navdy.obd.EcuResponse getResponse(int i) {
        return this.responses[i];
    }

    public int getResponseCount() {
        if (this.responses != null) {
            return this.responses.length;
        }
        return 0;
    }

    public java.lang.String getResponse() {
        return this.response;
    }

    public void execute(java.io.InputStream input, java.io.OutputStream output, com.navdy.obd.command.IObdDataObserver commandObserver) throws java.io.IOException {
        execute(input, output, null, commandObserver);
    }

    public void execute(java.io.InputStream input, java.io.OutputStream output, com.navdy.obd.Protocol protocol, com.navdy.obd.command.IObdDataObserver commandObserver) throws java.io.IOException {
        this.error = null;
        java.util.concurrent.Future future = singleThreadedExecutor.submit(new com.navdy.obd.command.ObdCommand.Anon1(input, output, commandObserver, protocol));
        try {
            future.get(getCommandExecutionTimeOutInMillis(), java.util.concurrent.TimeUnit.MILLISECONDS);
            if (this.error != null) {
                throw this.error;
            }
        } catch (java.lang.InterruptedException e) {
            Log.error("Trying to cancel the task, Success : " + future.cancel(true));
            singleThreadedExecutor.shutdownNow();
            singleThreadedExecutor = java.util.concurrent.Executors.newSingleThreadExecutor();
            throw new java.io.IOException("Command IO thread was interrupted", e);
        } catch (java.util.concurrent.ExecutionException e2) {
            throw new java.io.IOException(e2.getCause());
        } catch (java.util.concurrent.TimeoutException e3) {
            Log.error("Trying to cancel the task, Success : " + future.cancel(true));
            singleThreadedExecutor.shutdownNow();
            singleThreadedExecutor = java.util.concurrent.Executors.newSingleThreadExecutor();
            throw new java.io.IOException(e3);
        }
    }

    /* access modifiers changed from: protected */
    public void processCommandResponse(com.navdy.obd.Protocol protocol, com.navdy.obd.io.ObdCommandInterpreter obdCommandInterpreter, com.navdy.obd.command.IObdDataObserver dataObserver) throws java.io.IOException {
        if (SLEEP_COMMAND == this) {
            Log.info("Reading response of Sleep command ");
            try {
                java.lang.String response2 = obdCommandInterpreter.readLine();
                if (response2 == null || response2.length() <= 0) {
                    Log.info("Empty response for SLEEP command");
                    throw new java.io.IOException("Invalid response while putting chip to sleep");
                }
                Log.info("Response {}", (java.lang.Object) response2.trim());
                if (response2.trim().equals(OK)) {
                    return;
                }
            } catch (java.lang.InterruptedException e) {
                Log.error("Interrupted while reading response for sleep command", (java.lang.Throwable) e);
                return;
            }
        }
        java.lang.StringBuilder responseBuilder = new java.lang.StringBuilder();
        try {
            java.lang.String response3 = obdCommandInterpreter.readResponse();
            boolean textResponse = obdCommandInterpreter.didGetTextResponse();
            responseBuilder.append(response3);
            if (this == RESET_COMMAND) {
                Log.info("Reset Command, Clearing the terminal");
                try {
                    responseBuilder.append(obdCommandInterpreter.clearTerminal());
                    textResponse = obdCommandInterpreter.didGetTextResponse();
                } catch (java.lang.InterruptedException e2) {
                    Log.error("Interrupted while clearing the terminal ", (java.lang.Throwable) e2);
                    return;
                } catch (java.util.concurrent.TimeoutException e3) {
                    Log.error("Clearing terminal timed out");
                    return;
                }
            }
            this.response = responseBuilder.toString();
            if (dataObserver != null) {
                dataObserver.onResponse(this.response);
            }
            this.responses = com.navdy.obd.ResponseParser.parse(textResponse, this.response, protocol);
        } catch (java.lang.InterruptedException e4) {
        }
    }

    /* access modifiers changed from: protected */
    public long getCommandExecutionTimeOutInMillis() {
        return this.timeoutMillis;
    }

    public double getDoubleValue() {
        byte[] data = getByteResponse();
        if (data != null) {
            try {
                if (data[0] == 65 && this.conversion != null) {
                    return this.conversion.convert(data);
                }
            } catch (java.lang.Exception e) {
                Log.debug("Exception converting " + new java.lang.String(data), (java.lang.Throwable) e);
            }
        }
        return -2.147483648E9d;
    }

    public int getIntValue() {
        return (int) getDoubleValue();
    }
}
