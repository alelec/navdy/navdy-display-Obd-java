package com.navdy.obd.command;

public class CANBusMonitoringCommand extends com.navdy.obd.command.BatchCommand {
    public static final int CAN_BUS_MONITOR_COMMAND_PID = 1001;
    private com.navdy.obd.command.CANBusDataProcessor canbusDataProcessor;
    java.util.ArrayList<java.lang.Integer> monitoredPids = new java.util.ArrayList<>();

    class Anon1 extends com.navdy.obd.command.ObdCommand {
        Anon1(java.lang.String name, java.lang.String command) {
            super(name, command);
        }

        /* access modifiers changed from: protected */
        public void processCommandResponse(com.navdy.obd.Protocol protocol, com.navdy.obd.io.ObdCommandInterpreter obdCommandInterpreter, com.navdy.obd.command.IObdDataObserver dataObserver) throws java.io.IOException {
            obdCommandInterpreter.debug = true;
            super.processCommandResponse(protocol, obdCommandInterpreter, dataObserver);
        }
    }

    public CANBusMonitoringCommand(java.util.List<com.navdy.obd.can.CANBusDataDescriptor> descriptors) {
        super(new com.navdy.obd.command.ICommand[0]);
        add(com.navdy.obd.command.ObdCommand.TURN_OFF_FORMATTING_COMMAND);
        add(com.navdy.obd.command.ObdCommand.CLEAR_ALL_PASS_FILTERS_COMMAND);
        java.util.HashSet<java.lang.String> uniqueHeaders = new java.util.HashSet<>();
        for (com.navdy.obd.can.CANBusDataDescriptor descriptor : descriptors) {
            this.monitoredPids.add(java.lang.Integer.valueOf(descriptor.obdPid));
            if (!uniqueHeaders.contains(descriptor.header)) {
                uniqueHeaders.add(descriptor.header);
                if (descriptor.header.length() == 3) {
                    add(new com.navdy.obd.command.ObdCommand("STFAP " + descriptor.header + ",7FF"));
                }
            }
        }
        this.canbusDataProcessor = new com.navdy.obd.command.CANBusDataProcessor(descriptors);
        add(this.canbusDataProcessor);
        add(new com.navdy.obd.command.CANBusMonitoringCommand.Anon1("Turn on formating", "ATCAF1"));
        add(com.navdy.obd.command.ObdCommand.CLEAR_ALL_PASS_FILTERS_COMMAND);
    }

    public void setCANBusDataListener(com.navdy.obd.command.CANBusDataProcessor.ICanBusDataListener dataListener) {
        this.canbusDataProcessor.setCANBusDataListener(dataListener);
    }

    public void reset() {
        this.canbusDataProcessor.reset();
    }

    public java.util.List<java.lang.Integer> getMonitoredPidsList() {
        return this.monitoredPids;
    }

    public void startMonitoring() {
        this.canbusDataProcessor.startMonitoring();
    }

    public boolean isSamplingComplete() {
        return this.canbusDataProcessor.isSamplingComplete();
    }

    public boolean isMonitoringFailed() {
        return this.canbusDataProcessor.isMonitoringFailed();
    }

    public boolean isSamplingSuccessful() {
        return this.canbusDataProcessor.isSamplingSuccessful();
    }
}
