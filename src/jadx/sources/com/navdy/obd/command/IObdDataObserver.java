package com.navdy.obd.command;

public interface IObdDataObserver {
    void onCommand(java.lang.String str);

    void onError(java.lang.String str);

    void onRawCanBusMessage(java.lang.String str);

    void onResponse(java.lang.String str);
}
