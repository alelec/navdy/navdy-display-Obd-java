package com.navdy.obd.command;

public class GatherCANBusDataCommand extends com.navdy.obd.command.ObdCommand {
    public static final int DEFAULT_MONITORING_DURATION = 2000;
    public static final com.navdy.obd.command.GatherCANBusDataCommand DEFAULT_MONITOR_COMMAND = new com.navdy.obd.command.GatherCANBusDataCommand(2000);
    public static final int GATHER_CAN_BUS_DATA_PID = 1000;
    static final org.slf4j.Logger Log = org.slf4j.LoggerFactory.getLogger(com.navdy.obd.command.GatherCANBusDataCommand.class);
    public static final int MAX_RETRY_COUNT = 3;
    public static final int MINIMUM_DATA_REQUIRED = 100;
    public static final com.navdy.obd.command.BatchCommand MONITOR_COMMAND = new com.navdy.obd.command.BatchCommand(new com.navdy.obd.command.ICommand[0]);
    public static final int MONITOR_COMMAND_EXECUTION_TIMEOUT = 5000;
    public static final int READ_TIME_OUT_MILLIS = 1000;
    public static final int TIME_OUT_TO_ = 2000;
    java.io.ByteArrayOutputStream baos;
    byte[] buffer;
    private java.lang.String errorData;
    private int failureCount;
    private com.navdy.obd.command.GatherCANBusDataCommand.LastSampleStats lastSampleStats;
    private long monitorDurationMilliseconds;
    private long monitorDurationNanos;

    static class Anon1 extends com.navdy.obd.command.ObdCommand {
        Anon1(java.lang.String name, java.lang.String command) {
            super(name, command);
        }

        /* access modifiers changed from: protected */
        public void processCommandResponse(com.navdy.obd.Protocol protocol, com.navdy.obd.io.ObdCommandInterpreter obdCommandInterpreter, com.navdy.obd.command.IObdDataObserver dataObserver) throws java.io.IOException {
            obdCommandInterpreter.debug = true;
            super.processCommandResponse(protocol, obdCommandInterpreter, dataObserver);
        }
    }

    public static class LastSampleStats {
        public long dataSizeCaptured;
        public boolean succeeded = false;

        public void reset() {
            this.succeeded = false;
            this.dataSizeCaptured = 0;
        }
    }

    static {
        MONITOR_COMMAND.add(TURN_OFF_FORMATTING_COMMAND);
        MONITOR_COMMAND.add(DEFAULT_MONITOR_COMMAND);
        MONITOR_COMMAND.add(new com.navdy.obd.command.GatherCANBusDataCommand.Anon1("Turn on formating", "ATCAF1"));
    }

    public GatherCANBusDataCommand() {
        this(2000);
    }

    public GatherCANBusDataCommand(long durationMillis) {
        super("CAN Monitoring", "ATMA");
        this.failureCount = 0;
        this.errorData = "";
        this.buffer = new byte[4096];
        this.baos = new java.io.ByteArrayOutputStream(512000);
        this.lastSampleStats = new com.navdy.obd.command.GatherCANBusDataCommand.LastSampleStats();
        this.monitorDurationMilliseconds = durationMillis;
        this.monitorDurationNanos = this.monitorDurationMilliseconds * 1000 * 1000;
    }

    /* access modifiers changed from: protected */
    public void processCommandResponse(com.navdy.obd.Protocol protocol, com.navdy.obd.io.ObdCommandInterpreter obdCommandInterpreter, com.navdy.obd.command.IObdDataObserver dataObserver) throws java.io.IOException {
        long start = java.lang.System.nanoTime();
        Log.info("Starting the CAN bus monitoring");
        obdCommandInterpreter.setReadTimeOut(1000);
        this.baos.reset();
        this.lastSampleStats.succeeded = false;
        int totalLength = 0;
        while (!java.lang.Thread.interrupted() && java.lang.System.nanoTime() - start < this.monitorDurationNanos) {
            try {
                int length = obdCommandInterpreter.read(this.buffer);
                if (length > 0) {
                    totalLength += length;
                    this.baos.write(this.buffer, 0, length);
                }
            } catch (java.lang.InterruptedException e) {
                Log.error("Interrupted exception {}", (java.lang.Throwable) e);
            } catch (java.io.IOException e2) {
                this.failureCount++;
                Log.error("IOException exception {}", (java.lang.Throwable) e2);
            }
        }
        if (this.failureCount > 0 && totalLength > 100) {
            this.errorData = null;
            this.failureCount = 0;
        }
        if (totalLength < 100) {
            if (totalLength == 0) {
                Log.info("No data read from the CAN bus, marking as failed");
            } else {
                java.lang.String newData = this.baos.toString().replace(13, 10);
                dataObserver.onRawCanBusMessage(newData);
                this.errorData = newData;
                Log.info("Very less data read from the CAN bus, marking as failed {}", (java.lang.Object) newData);
            }
            this.failureCount++;
        } else {
            java.lang.String newData2 = this.baos.toString();
            Log.debug("Total length {}, String length : {}", (java.lang.Object) java.lang.Integer.valueOf(totalLength), (java.lang.Object) java.lang.Integer.valueOf(newData2.length()));
            dataObserver.onRawCanBusMessage(newData2);
            this.lastSampleStats.succeeded = true;
            this.lastSampleStats.dataSizeCaptured = (long) totalLength;
        }
        if (!java.lang.Thread.interrupted()) {
            obdCommandInterpreter.write(new byte[]{65, 84, 73, 73, 73, 13});
            Log.debug("Clearing the terminal");
            try {
                obdCommandInterpreter.clearTerminalBulk();
                Log.debug("Terminal clear");
            } catch (java.lang.InterruptedException e3) {
                e3.printStackTrace();
                Log.debug("Interrupted while clearing terminal");
            } catch (java.util.concurrent.TimeoutException e4) {
                e4.printStackTrace();
                Log.debug("Timed out while clearing terminal");
            }
        }
    }

    /* access modifiers changed from: protected */
    public long getCommandExecutionTimeOutInMillis() {
        return 5000;
    }

    public boolean isMonitoringFailureDetected() {
        return this.failureCount > 3;
    }

    public void reset() {
        this.failureCount = 0;
        this.errorData = null;
        this.lastSampleStats.succeeded = false;
    }

    public long getLastSampleDataSize() {
        return this.lastSampleStats.dataSizeCaptured;
    }

    public boolean getLastSampleSucceeded() {
        return this.lastSampleStats.succeeded;
    }

    public void resetLastSampleStats() {
        this.lastSampleStats.reset();
    }

    public java.lang.String getErrorData() {
        return this.errorData;
    }
}
