package com.navdy.obd.command;

public interface ICommand {
    void execute(java.io.InputStream inputStream, java.io.OutputStream outputStream, com.navdy.obd.Protocol protocol, com.navdy.obd.command.IObdDataObserver iObdDataObserver) throws java.io.IOException;

    double getDoubleValue();

    int getIntValue();

    java.lang.String getName();

    java.lang.String getResponse();
}
