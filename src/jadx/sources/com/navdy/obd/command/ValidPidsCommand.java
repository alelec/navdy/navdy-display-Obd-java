package com.navdy.obd.command;

public class ValidPidsCommand implements com.navdy.obd.command.ICommand {
    static final org.slf4j.Logger Log = org.slf4j.LoggerFactory.getLogger(com.navdy.obd.command.ValidPidsCommand.class);
    public static int MAX_ADDRESS = 128;
    private java.util.List<com.navdy.obd.command.ObdCommand> supportedPids = new java.util.ArrayList();

    public java.lang.String getName() {
        return "validPids";
    }

    public java.util.List<com.navdy.obd.command.ObdCommand> supportedPids() {
        return this.supportedPids;
    }

    public void execute(java.io.InputStream input, java.io.OutputStream output, com.navdy.obd.Protocol protocol, com.navdy.obd.command.IObdDataObserver commandObserver) throws java.io.IOException {
        Log.debug("OBD - Scanning for valid pids with protocol = " + protocol);
        int address = 0;
        boolean done = false;
        while (!done && address <= MAX_ADDRESS) {
            java.lang.String cmd = java.lang.String.format("01%02x", new java.lang.Object[]{java.lang.Integer.valueOf(address)});
            Log.debug("OBD - Reading " + cmd);
            com.navdy.obd.command.ObdCommand command = new com.navdy.obd.command.ObdCommand(cmd);
            command.execute(input, output, protocol, commandObserver);
            java.lang.String response = command.getResponse();
            Log.debug("OBD - Response: " + (response != null ? response.replace(13, 10) : null));
            this.supportedPids.add(command);
            address += 32;
            if (!anyEcuSupportsMorePids(command)) {
                done = true;
            } else {
                done = false;
            }
        }
    }

    private boolean anyEcuSupportsMorePids(com.navdy.obd.command.ObdCommand command) {
        for (int i = 0; i < command.getResponseCount(); i++) {
            if (supportsMorePids(command.getResponse(i).data)) {
                return true;
            }
        }
        return false;
    }

    private boolean supportsMorePids(byte[] data) {
        if (data == null || data.length <= 5 || (data[5] & 1) == 0) {
            return false;
        }
        return true;
    }

    public java.lang.String getResponse() {
        return null;
    }

    public double getDoubleValue() {
        return 0.0d;
    }

    public int getIntValue() {
        return 0;
    }
}
