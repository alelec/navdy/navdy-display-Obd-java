package com.navdy.obd.command;

public class WriteConfigurationCommand extends com.navdy.obd.command.BatchCommand {
    static final org.slf4j.Logger Log = org.slf4j.LoggerFactory.getLogger(com.navdy.obd.command.WriteConfigurationCommand.class);
    public static final java.lang.String SET_DEVICE_INFO_COMMAND = "STSATI";

    public WriteConfigurationCommand(java.lang.String configuration) {
        super((java.util.List<com.navdy.obd.command.ICommand>) new java.util.ArrayList<com.navdy.obd.command.ICommand>());
        if (configuration == null) {
            throw new java.lang.IllegalArgumentException("Configuration file cannot be empty");
        } else if (!configuration.contains(SET_DEVICE_INFO_COMMAND)) {
            throw new java.lang.IllegalArgumentException("Configuration should set the device information to identify the configuration");
        } else {
            for (java.lang.String command : configuration.split("\n")) {
                add(new com.navdy.obd.command.ObdCommand(command));
            }
        }
    }

    public java.lang.String getResponse() {
        return super.getResponse();
    }
}
