package com.navdy.obd.command;

public class BaseSTNInitializeCommand extends com.navdy.obd.command.BatchCommand {
    private com.navdy.obd.command.ObdCommand deviceInfoCommand = com.navdy.obd.command.ObdCommand.DEVICE_INFO_COMMAND;

    public BaseSTNInitializeCommand(com.navdy.obd.command.ICommand... commands) {
        super(commands);
        add(this.deviceInfoCommand);
    }

    public java.lang.String getConfigurationName() {
        java.lang.String response = this.deviceInfoCommand.getResponse();
        if (response != null) {
            return response.trim();
        }
        return null;
    }
}
