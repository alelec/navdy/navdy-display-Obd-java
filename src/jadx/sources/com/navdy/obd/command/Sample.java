package com.navdy.obd.command;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0005R\u0012\u0010\u0006\u001a\u00020\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\b\u001a\u00020\u0007X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\t\u0010\n\"\u0004\b\u000b\u0010\fR\u0010\u0010\u0002\u001a\u00020\r8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000e\u001a\u00020\u000f8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0012\u0010\u0012\u001a\u00020\u00138\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"}, d2 = {"Lcom/navdy/obd/command/Sample;", "", "pid", "", "scanInterval", "(II)V", "lastSampleTimestamp", "", "lastSamplingTime", "getLastSamplingTime", "()J", "setLastSamplingTime", "(J)V", "Lcom/navdy/obd/Pid;", "samplingTimeStats", "Lcom/navdy/util/RunningStats;", "getScanInterval", "()I", "updated", "", "obd-service_release"}, k = 1, mv = {1, 1, 6})
/* compiled from: Sample.kt */
public final class Sample {
    @kotlin.jvm.JvmField
    public long lastSampleTimestamp;
    private long lastSamplingTime;
    @org.jetbrains.annotations.NotNull
    @kotlin.jvm.JvmField
    public final com.navdy.obd.Pid pid;
    @org.jetbrains.annotations.NotNull
    @kotlin.jvm.JvmField
    public final com.navdy.util.RunningStats samplingTimeStats = new com.navdy.util.RunningStats();
    private final int scanInterval;
    @kotlin.jvm.JvmField
    public boolean updated;

    public Sample(int pid2, int scanInterval2) {
        this.scanInterval = scanInterval2;
        this.pid = new com.navdy.obd.Pid(pid2);
    }

    public final int getScanInterval() {
        return this.scanInterval;
    }

    public final long getLastSamplingTime() {
        return this.lastSamplingTime;
    }

    public final void setLastSamplingTime(long j) {
        this.lastSamplingTime = j;
    }
}
