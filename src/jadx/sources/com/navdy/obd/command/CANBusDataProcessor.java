package com.navdy.obd.command;

public class CANBusDataProcessor extends com.navdy.obd.command.ObdCommand {
    public static final int MINIMUM_SAMPLES_NEEDED = 3;
    public static final int MONITORING_DURATION = 10000;
    public static final int READ_TIME_OUT_MILLIS = 1000;
    public static final int SAMPLING_DURATION = 250;
    java.io.ByteArrayOutputStream baos = new java.io.ByteArrayOutputStream(512000);
    byte[] buffer = new byte[8192];
    private com.navdy.obd.command.CANBusDataProcessor.ICanBusDataListener canBusDataListener;
    private java.util.HashMap<java.lang.String, com.navdy.obd.command.CANBusDataProcessor.HeaderData> headerToDataMapping = new java.util.HashMap<>();
    private boolean isSampling = true;
    private long monitorDurationMilliseconds;
    private long monitorDurationNanos;
    private boolean monitoringFailed = false;
    private int numberOfSuccessSamples = 0;
    private boolean samplingFailed = false;

    class HeaderData {
        int count = 0;
        java.util.ArrayList<com.navdy.obd.can.CANBusDataDescriptor> descriptorsList;
        java.lang.String header;
        int interval;
        long lastProcessedTime = 0;
        int length;

        HeaderData() {
        }
    }

    public interface ICanBusDataListener {
        void onCanBusDataRead(int i, double d);
    }

    public CANBusDataProcessor(java.util.List<com.navdy.obd.can.CANBusDataDescriptor> canBusDataDescriptors) {
        com.navdy.obd.command.CANBusDataProcessor.HeaderData headerData;
        super("STM");
        for (com.navdy.obd.can.CANBusDataDescriptor descriptor : canBusDataDescriptors) {
            java.lang.String header = descriptor.header;
            if (!this.headerToDataMapping.containsKey(header)) {
                headerData = new com.navdy.obd.command.CANBusDataProcessor.HeaderData();
                headerData.descriptorsList = new java.util.ArrayList<>(8);
                this.headerToDataMapping.put(header, headerData);
            } else {
                headerData = (com.navdy.obd.command.CANBusDataProcessor.HeaderData) this.headerToDataMapping.get(header);
            }
            headerData.descriptorsList.add(descriptor);
            headerData.interval = descriptor.dataFrequency;
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x0152  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x01d9  */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x01eb  */
    public void processCommandResponse(com.navdy.obd.Protocol protocol, com.navdy.obd.io.ObdCommandInterpreter obdCommandInterpreter, com.navdy.obd.command.IObdDataObserver dataObserver) throws java.io.IOException {
        boolean terminalClear;
        this.monitorDurationMilliseconds = this.isSampling ? 250 : 10000;
        this.monitorDurationNanos = this.monitorDurationMilliseconds * 1000 * 1000;
        long start = java.lang.System.nanoTime();
        Log.info("Starting the CAN bus monitoring");
        obdCommandInterpreter.setReadTimeOut(1000);
        this.baos.reset();
        int totalLength = 0;
        java.lang.StringBuilder headerBuilder = new java.lang.StringBuilder(3);
        int lineLength = 0;
        char[] dataArray = new char[32];
        resetHeaderStatsForSampling();
        while (java.lang.System.nanoTime() - start < this.monitorDurationNanos) {
            try {
                int length = obdCommandInterpreter.read(this.buffer);
                if (length > 0) {
                    totalLength += length;
                    int i = 0;
                    while (true) {
                        int lineLength2 = lineLength;
                        if (i >= length) {
                            lineLength = lineLength2;
                            break;
                        }
                        try {
                            char ch2 = (char) this.buffer[i];
                            if (!((ch2 >= '0' && ch2 <= '9') || (ch2 >= 'A' && ch2 <= 'F') || (ch2 >= 'a' && ch2 <= 'f')) && ch2 != 13 && ch2 != 10) {
                                Log.debug("Received an invalid character, ending monitoring , Char");
                                lineLength = lineLength2;
                                break;
                            }
                            if (ch2 == 13 || ch2 == 10) {
                                if (headerBuilder.length() == 3 && lineLength2 > 5) {
                                    processData(headerBuilder.toString(), dataArray, lineLength2 - 3);
                                }
                                lineLength = 0;
                                headerBuilder.setLength(0);
                            } else if (lineLength2 < 3) {
                                headerBuilder.append(ch2);
                                lineLength = lineLength2 + 1;
                            } else {
                                lineLength = lineLength2 + 1;
                                dataArray[lineLength2 - 3] = ch2;
                            }
                            i++;
                        } catch (java.lang.InterruptedException e) {
                            e = e;
                            int i2 = lineLength2;
                            Log.error("Interrupted exception {}", (java.lang.Throwable) e);
                            long elapsedTimeMillis = (java.lang.System.nanoTime() - start) / 1000000;
                            for (com.navdy.obd.command.CANBusDataProcessor.HeaderData headerData : this.headerToDataMapping.values()) {
                            }
                            if (!this.samplingFailed) {
                            }
                            terminalClear = false;
                            while (!terminalClear) {
                            }
                        } catch (java.io.IOException e2) {
                            e = e2;
                            int i3 = lineLength2;
                            Log.error("IOException exception {}", (java.lang.Throwable) e);
                            long elapsedTimeMillis2 = (java.lang.System.nanoTime() - start) / 1000000;
                            for (com.navdy.obd.command.CANBusDataProcessor.HeaderData headerData2 : this.headerToDataMapping.values()) {
                            }
                            if (!this.samplingFailed) {
                            }
                            terminalClear = false;
                            while (!terminalClear) {
                            }
                        }
                    }
                }
            } catch (java.lang.InterruptedException e3) {
                e = e3;
            } catch (java.io.IOException e4) {
                e = e4;
                Log.error("IOException exception {}", (java.lang.Throwable) e);
                long elapsedTimeMillis22 = (java.lang.System.nanoTime() - start) / 1000000;
                for (com.navdy.obd.command.CANBusDataProcessor.HeaderData headerData22 : this.headerToDataMapping.values()) {
                }
                if (!this.samplingFailed) {
                }
                terminalClear = false;
                while (!terminalClear) {
                }
            }
        }
        long elapsedTimeMillis222 = (java.lang.System.nanoTime() - start) / 1000000;
        for (com.navdy.obd.command.CANBusDataProcessor.HeaderData headerData222 : this.headerToDataMapping.values()) {
            int interval = headerData222.count == 0 ? 0 : (int) (elapsedTimeMillis222 / ((long) headerData222.count));
            Log.error("Header {}, Count {}, Freq :{}", headerBuilder.toString(), java.lang.Integer.valueOf(headerData222.count), java.lang.Integer.valueOf(interval));
            if (interval == 0 || interval > headerData222.interval) {
                if (this.isSampling) {
                    this.samplingFailed = true;
                } else {
                    this.monitoringFailed = true;
                }
            }
        }
        if (!this.samplingFailed) {
            this.numberOfSuccessSamples++;
        }
        terminalClear = false;
        while (!terminalClear) {
            obdCommandInterpreter.write(new byte[]{65, 84, 73, 73, 73, 13});
            Log.debug("Clearing the terminal");
            try {
                obdCommandInterpreter.clearTerminalBulk(false, true);
                terminalClear = true;
                Log.debug("Terminal clear");
            } catch (java.lang.InterruptedException e5) {
                e5.printStackTrace();
                Log.debug("Interrupted while clearing terminal");
            } catch (java.util.concurrent.TimeoutException e6) {
                Log.debug("Clearing the terminal timed out");
            }
        }
    }

    private void resetHeaderStatsForSampling() {
        for (com.navdy.obd.command.CANBusDataProcessor.HeaderData headerData : this.headerToDataMapping.values()) {
            headerData.count = 0;
        }
    }

    public void setCANBusDataListener(com.navdy.obd.command.CANBusDataProcessor.ICanBusDataListener canBusDataListener2) {
        this.canBusDataListener = canBusDataListener2;
    }

    private void processData(java.lang.String header, char[] data, int dataLength) {
        int bytesToInt;
        if (this.headerToDataMapping.containsKey(header)) {
            com.navdy.obd.command.CANBusDataProcessor.HeaderData headerData = (com.navdy.obd.command.CANBusDataProcessor.HeaderData) this.headerToDataMapping.get(header);
            headerData.count++;
            if (this.isSampling) {
                return;
            }
            if (headerData.lastProcessedTime == 0 || java.lang.System.currentTimeMillis() - headerData.lastProcessedTime > ((long) headerData.interval)) {
                headerData.lastProcessedTime = java.lang.System.currentTimeMillis();
                java.util.List<com.navdy.obd.can.CANBusDataDescriptor> descriptors = headerData.descriptorsList;
                if (descriptors != null) {
                    for (com.navdy.obd.can.CANBusDataDescriptor descriptor : descriptors) {
                        int byteOffset = descriptor.byteOffset;
                        int byteLength = descriptor.lengthInBits / 8;
                        if ((byteOffset + byteLength) * 2 < dataLength) {
                            byte[] byteData = parseHexArray(data, byteOffset * 2, byteLength * 2);
                            if (byteData != null) {
                                if (byteData.length == 1) {
                                    bytesToInt = byteData[0] & com.navdy.obd.update.STNBootloaderChannel.ERROR_CODE_INVALID_RESPONSE;
                                } else {
                                    bytesToInt = bytesToInt(byteData, 0, byteData.length, !descriptor.littleEndian);
                                }
                                this.canBusDataListener.onCanBusDataRead(descriptor.obdPid, ((double) bytesToInt) * descriptor.resolution);
                            }
                        }
                    }
                }
            }
        }
    }

    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r1v3
  assigns: []
  uses: []
  mth insns count: 28
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    public static int bytesToInt(byte[] byteArray, int offset, int length, boolean bigEndian) {
        if (byteArray == null || length <= 0 || length > 4 || byteArray.length < offset + length) {
            throw new java.lang.NumberFormatException("Incorrect data");
        }
        int data = 0;
        int i = 0;
        while (i < length) {
            int data2 = data + (byteArray[offset + (bigEndian ? i : (length - i) - 1)] & 255);
            int data3 = i < length + -1 ? data2 << 8 : data2;
            i++;
            data = data3;
        }
        return data;
    }

    public static byte[] parseHexArray(char[] hexArray, int position, int length) {
        byte[] byteData = new byte[(length / 2)];
        int bytePosition = 0;
        int i = position;
        while (true) {
            int bytePosition2 = bytePosition;
            if (i >= position + length) {
                return byteData;
            }
            bytePosition = bytePosition2 + 1;
            byteData[bytePosition2] = (byte) ((com.navdy.obd.util.HexUtil.parseHexDigit(hexArray[i]) << 4) + com.navdy.obd.util.HexUtil.parseHexDigit(hexArray[i + 1]));
            i += 2;
        }
    }

    public void reset() {
        this.samplingFailed = false;
        this.monitoringFailed = false;
        this.numberOfSuccessSamples = 0;
        resetHeaderStatsForSampling();
    }

    public boolean isSamplingComplete() {
        return this.samplingFailed || this.numberOfSuccessSamples >= 3;
    }

    public boolean isSamplingSuccessful() {
        return !this.samplingFailed && this.numberOfSuccessSamples >= 3;
    }

    public void startMonitoring() {
        this.isSampling = false;
    }

    public boolean isMonitoringFailed() {
        return this.monitoringFailed;
    }

    /* access modifiers changed from: protected */
    public long getCommandExecutionTimeOutInMillis() {
        return this.isSampling ? 10000 : 20000;
    }
}
