package com.navdy.obd.command;

public class CheckDTCCommand extends com.navdy.obd.command.ObdCommand {
    public CheckDTCCommand() {
        super("0101");
    }

    public boolean isCheckEngineLightOn() {
        int responseCount = getResponseCount();
        for (int i = 0; i < responseCount; i++) {
            com.navdy.obd.EcuResponse ecuResponse = getResponse(i);
            if (ecuResponse != null) {
                byte[] response = getResponse(i).data;
                if (response != null && response.length >= 3 && response[0] == 65 && response[1] == 1) {
                    int num = response[2] & 255;
                    if ((num & 128) > 0) {
                        Log.info("Check engine light is on , ECU {}, Number of DTCs set {}", (java.lang.Object) java.lang.Integer.valueOf(ecuResponse.ecu), (java.lang.Object) java.lang.Integer.valueOf(num - 128));
                        return true;
                    }
                }
                Log.info("Response of CheckDTCCommand, ECU {} , Data {}", (java.lang.Object) java.lang.Integer.valueOf(ecuResponse.ecu), (java.lang.Object) java.util.Arrays.toString(response));
            }
        }
        return false;
    }

    public int getNumberOfTroubleCodes() {
        int responseCount = getResponseCount();
        int numberOfTroubleCodes = 0;
        for (int i = 0; i < responseCount; i++) {
            com.navdy.obd.EcuResponse ecuResponse = getResponse(i);
            if (ecuResponse != null) {
                byte[] response = getResponse(i).data;
                if (response != null && response.length >= 3 && response[0] == 65 && response[1] == 1) {
                    int num = response[2] & 255;
                    numberOfTroubleCodes += num & android.support.v4.media.TransportMediator.KEYCODE_MEDIA_PAUSE;
                    Log.info("Check engine light is on , ECU {}, Number of DTCs set {}", (java.lang.Object) java.lang.Integer.valueOf(ecuResponse.ecu), (java.lang.Object) java.lang.Integer.valueOf(num - 128));
                }
                Log.info("Response of CheckDTCCommand, ECU {} , Data {}", (java.lang.Object) java.lang.Integer.valueOf(ecuResponse.ecu), (java.lang.Object) java.util.Arrays.toString(response));
            }
        }
        return numberOfTroubleCodes;
    }
}
