package com.navdy.obd.command;

public class ScheduledBatchCommand extends com.navdy.obd.command.BatchCommand {
    public java.util.List<com.navdy.obd.command.Sample> samples;

    public ScheduledBatchCommand(java.util.List<com.navdy.obd.ScanSchedule.Scan> scanList, com.navdy.obd.command.ICommand[] commands) {
        super(java.util.Arrays.asList(commands));
        if (scanList == null || commands.length != scanList.size()) {
            throw new java.lang.IllegalArgumentException("scanList length must match commands length");
        }
        this.samples = new java.util.ArrayList(scanList.size());
        for (int i = 0; i < scanList.size(); i++) {
            com.navdy.obd.ScanSchedule.Scan scan = (com.navdy.obd.ScanSchedule.Scan) scanList.get(i);
            this.samples.add(new com.navdy.obd.command.Sample(scan.pid, scan.scanInterval));
        }
    }

    public java.util.List<com.navdy.obd.Pid> getPids() {
        java.util.List<com.navdy.obd.Pid> result = new java.util.ArrayList<>(this.samples.size());
        for (int i = 0; i < this.samples.size(); i++) {
            result.add(((com.navdy.obd.command.Sample) this.samples.get(i)).pid);
        }
        return result;
    }

    public void execute(java.io.InputStream input, java.io.OutputStream output, com.navdy.obd.Protocol protocol, com.navdy.obd.command.IObdDataObserver commandObserver) throws java.io.IOException {
        long nextSampleTime = 10000;
        boolean needToSleep = true;
        while (needToSleep) {
            int l = this.commands.size();
            for (int i = 0; i < l; i++) {
                com.navdy.obd.command.Sample sample = (com.navdy.obd.command.Sample) this.samples.get(i);
                com.navdy.obd.command.ICommand command = (com.navdy.obd.command.ICommand) this.commands.get(i);
                long lastReading = sample.lastSampleTimestamp;
                int scanInterval = sample.getScanInterval();
                long startTime = android.os.SystemClock.elapsedRealtime();
                long timeToWait = ((long) scanInterval) - (startTime - lastReading);
                if (lastReading == 0 || timeToWait <= 0) {
                    command.execute(input, output, protocol, commandObserver);
                    long finishTime = android.os.SystemClock.elapsedRealtime();
                    sample.lastSampleTimestamp = startTime;
                    sample.setLastSamplingTime(finishTime - startTime);
                    sample.updated = true;
                    needToSleep = false;
                } else {
                    if (timeToWait < nextSampleTime) {
                        nextSampleTime = timeToWait;
                    }
                    sample.updated = false;
                }
            }
            if (needToSleep) {
                try {
                    java.lang.Thread.sleep(nextSampleTime);
                } catch (java.lang.InterruptedException e) {
                    needToSleep = false;
                }
            }
        }
    }
}
