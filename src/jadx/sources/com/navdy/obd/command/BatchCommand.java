package com.navdy.obd.command;

public class BatchCommand implements com.navdy.obd.command.ICommand {
    protected java.util.ArrayList<com.navdy.obd.command.ICommand> commands;

    public BatchCommand(com.navdy.obd.command.ICommand... commands2) {
        this(java.util.Arrays.asList(commands2));
    }

    public BatchCommand(java.util.List<com.navdy.obd.command.ICommand> commands2) {
        this.commands = new java.util.ArrayList<>();
        this.commands.addAll(commands2);
    }

    public void add(com.navdy.obd.command.ICommand command) {
        this.commands.add(command);
    }

    public java.util.List<com.navdy.obd.command.ICommand> getCommands() {
        return this.commands;
    }

    public java.lang.String getName() {
        java.lang.StringBuilder names = new java.lang.StringBuilder("batch(");
        java.util.Iterator it = this.commands.iterator();
        while (it.hasNext()) {
            names.append(((com.navdy.obd.command.ICommand) it.next()).getName());
            names.append(",");
        }
        names.append(")");
        return names.toString();
    }

    public void execute(java.io.InputStream input, java.io.OutputStream output, com.navdy.obd.Protocol protocol, com.navdy.obd.command.IObdDataObserver commandObserver) throws java.io.IOException {
        java.util.Iterator it = this.commands.iterator();
        while (it.hasNext()) {
            ((com.navdy.obd.command.ICommand) it.next()).execute(input, output, protocol, commandObserver);
        }
    }

    public double getDoubleValue() {
        return 0.0d;
    }

    public int getIntValue() {
        return 0;
    }

    public java.lang.String getResponse() {
        java.lang.StringBuilder responses = new java.lang.StringBuilder();
        java.util.Iterator it = this.commands.iterator();
        while (it.hasNext()) {
            java.lang.String response = ((com.navdy.obd.command.ICommand) it.next()).getResponse();
            responses.append(response != null ? response.trim() : null);
            responses.append(",");
        }
        return responses.toString();
    }
}
