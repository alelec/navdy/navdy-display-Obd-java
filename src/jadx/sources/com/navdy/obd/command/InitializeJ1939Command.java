package com.navdy.obd.command;

public class InitializeJ1939Command extends com.navdy.obd.command.BaseSTNInitializeCommand {
    private static com.navdy.obd.command.ObdCommand setProtocolCommand = new com.navdy.obd.command.ObdCommand("SetProtocol", "atsp" + java.lang.Integer.toHexString(10));

    public InitializeJ1939Command() {
        super(com.navdy.obd.command.ObdCommand.RESET_COMMAND, com.navdy.obd.command.ObdCommand.ECHO_OFF_COMMAND, com.navdy.obd.command.ObdCommand.SPACES_OFF_COMMAND, com.navdy.obd.command.ObdCommand.TURN_OFF_VOLTAGE_LEVEL_WAKEUP);
        add(setProtocolCommand);
    }
}
