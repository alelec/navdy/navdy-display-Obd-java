package com.navdy.obd;

public class Profile {
    static final org.slf4j.Logger Log = org.slf4j.LoggerFactory.getLogger(com.navdy.obd.Profile.class);
    public java.util.HashMap<java.lang.String, com.navdy.obd.command.ObdCommand> mapByName = new java.util.HashMap<>();
    public java.util.HashMap<java.lang.Integer, com.navdy.obd.command.ObdCommand> mapByPid = new java.util.HashMap<>();

    class Column {
        public static final java.lang.String EQUATION = "Equation";
        public static final java.lang.String HEADER = "Header";
        public static final java.lang.String MAX_VALUE = "Max Value";
        public static final java.lang.String MIN_VALUE = "Min Value";
        public static final java.lang.String MODE_AND_PID = "ModeAndPID";
        public static final java.lang.String NAME = "Name";
        public static final java.lang.String SHORT_NAME = "ShortName";
        public static final java.lang.String UNITS = "Units";

        Column() {
        }
    }

    public com.navdy.obd.command.ObdCommand lookup(java.lang.String id) {
        com.navdy.obd.command.ObdCommand command = (com.navdy.obd.command.ObdCommand) this.mapByName.get(id);
        if (command != null) {
            return command.clone();
        }
        return null;
    }

    public com.navdy.obd.command.ObdCommand lookup(int pid) {
        com.navdy.obd.command.ObdCommand command = (com.navdy.obd.command.ObdCommand) this.mapByPid.get(java.lang.Integer.valueOf(pid));
        if (command != null) {
            return command.clone();
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    public void add(java.lang.String modeAndPid, com.navdy.obd.command.ObdCommand command) {
        if (modeAndPid.startsWith("01")) {
            this.mapByPid.put(java.lang.Integer.valueOf(java.lang.Integer.parseInt(modeAndPid.substring(2, 4), 16)), command);
        }
        this.mapByName.put(command.getName(), command);
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x0098 A[SYNTHETIC, Splitter:B:30:0x0098] */
    public void load(java.io.InputStream csvProfile) {
        java.io.BufferedReader reader = null;
        try {
            java.io.BufferedReader reader2 = new java.io.BufferedReader(new java.io.InputStreamReader(csvProfile));
            try {
                for (org.apache.commons.csv.CSVRecord record : org.apache.commons.csv.CSVFormat.DEFAULT.withHeader(new java.lang.String[0]).parse(reader2)) {
                    java.lang.String name = record.get(com.navdy.obd.Profile.Column.SHORT_NAME);
                    java.lang.String modeAndPid = record.get(com.navdy.obd.Profile.Column.MODE_AND_PID);
                    java.lang.String equation = record.get(com.navdy.obd.Profile.Column.EQUATION);
                    try {
                        add(modeAndPid, new com.navdy.obd.command.ObdCommand(name, modeAndPid, new com.navdy.obd.converters.CustomConversion(equation)));
                    } catch (parsii.tokenizer.ParseException e) {
                        Log.error("Failed to parse equation (" + equation + ") for " + name);
                    }
                }
                if (reader2 != null) {
                    try {
                        reader2.close();
                        java.io.BufferedReader bufferedReader = reader2;
                    } catch (java.io.IOException e2) {
                        Log.error("Error closing reader", (java.lang.Throwable) e2);
                        java.io.BufferedReader bufferedReader2 = reader2;
                    }
                }
            } catch (java.io.IOException e3) {
                e = e3;
                reader = reader2;
            } catch (Throwable th) {
                th = th;
                reader = reader2;
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (java.io.IOException e4) {
                        Log.error("Error closing reader", (java.lang.Throwable) e4);
                    }
                }
                throw th;
            }
        } catch (java.io.IOException e5) {
            e = e5;
            try {
                Log.error("Error reading custom profile", (java.lang.Throwable) e);
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (java.io.IOException e6) {
                        Log.error("Error closing reader", (java.lang.Throwable) e6);
                    }
                }
            } catch (Throwable th2) {
                th = th2;
                if (reader != null) {
                }
                throw th;
            }
        }
    }
}
