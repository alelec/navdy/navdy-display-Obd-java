package com.navdy.obd;

public class ResponseParser {
    private static final char MULTI_LINE_CONTINUED = '2';
    private static final char MULTI_LINE_START = '1';
    private static final char SINGLE_LINE = '0';
    private static java.util.regex.Pattern groupPattern = java.util.regex.Pattern.compile("([0-9A-F]:)((?:[0-9A-F]{2})+)\\s");
    private static java.util.regex.Pattern multiLineResponse = java.util.regex.Pattern.compile("^([0-9A-F]{3})\\s?0:");

    public static com.navdy.obd.EcuResponse[] parse(boolean textResponse, java.lang.String response, com.navdy.obd.Protocol protocol) {
        java.util.regex.Matcher matcher = multiLineResponse.matcher(response);
        if (matcher.find()) {
            com.navdy.obd.EcuResponse[] result = new com.navdy.obd.EcuResponse[1];
            byte[] data = new byte[java.lang.Integer.parseInt(matcher.group(1), 16)];
            int offset = 0;
            java.util.regex.Matcher groupMatcher = groupPattern.matcher(response);
            while (groupMatcher.find()) {
                java.lang.String group = groupMatcher.group(2);
                int bytes = group.length() / 2;
                com.navdy.obd.util.HexUtil.parseHexString(group, data, offset, bytes);
                offset += bytes;
            }
            com.navdy.obd.EcuResponse ecuResponse = new com.navdy.obd.EcuResponse(0, data);
            result[0] = ecuResponse;
            return result;
        } else if (textResponse) {
            return null;
        } else {
            java.lang.String[] lines = response.split("\r");
            if (protocol == null) {
                java.lang.String data2 = join(lines);
                com.navdy.obd.EcuResponse[] result2 = {new com.navdy.obd.EcuResponse(0, true, data2.length() / 2)};
                result2[0].append(data2);
                return result2;
            }
            java.util.List<com.navdy.obd.EcuResponse> responses = new java.util.ArrayList<>();
            com.navdy.obd.Protocol.FrameFormat format = protocol.format;
            for (java.lang.String line : lines) {
                int ecu = java.lang.Integer.parseInt(line.substring(format.ecuStart, format.ecuEnd), 16);
                switch (protocol.id) {
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                        parseIsoFrame(ecu, line, format.ecuEnd, responses);
                        break;
                    default:
                        parseCanFrame(ecu, line, format.ecuEnd, responses);
                        break;
                }
            }
            if (responses.size() <= 0) {
                return null;
            }
            com.navdy.obd.EcuResponse[] result3 = new com.navdy.obd.EcuResponse[responses.size()];
            for (int i = 0; i < result3.length; i++) {
                com.navdy.obd.EcuResponse ecuResponse2 = (com.navdy.obd.EcuResponse) responses.get(i);
                ecuResponse2.flatten();
                result3[i] = ecuResponse2;
            }
            return result3;
        }
    }

    private static void parseIsoFrame(int ecu, java.lang.String line, int offset, java.util.List<com.navdy.obd.EcuResponse> responses) {
        com.navdy.obd.EcuResponse response = findEcuResponse(ecu, responses);
        if (response == null) {
            response = new com.navdy.obd.EcuResponse(ecu);
            responses.add(response);
        }
        response.addFrame(line.substring(offset));
    }

    private static void parseCanFrame(int ecu, java.lang.String line, int offset, java.util.List<com.navdy.obd.EcuResponse> responses) {
        int lenFieldLen = 1;
        int offset2 = offset + 1;
        char frameFlag = line.charAt(offset);
        switch (frameFlag) {
            case ch.qos.logback.core.net.SyslogConstants.LOG_LPR /*48*/:
            case com.navdy.obd.Pids.DISTANCE_TRAVELLED /*49*/:
                boolean multiline = frameFlag == '1';
                if (multiline) {
                    lenFieldLen = 3;
                }
                int len = java.lang.Integer.parseInt(line.substring(offset2, offset2 + lenFieldLen), 16);
                int offset3 = offset2 + lenFieldLen;
                com.navdy.obd.EcuResponse ecuResponse = new com.navdy.obd.EcuResponse(ecu, multiline, len);
                ecuResponse.append(line.substring(offset3, java.lang.Math.min((len * 2) + offset3, line.length())));
                responses.add(ecuResponse);
                return;
            case '2':
                int offset4 = offset2 + 1;
                com.navdy.obd.EcuResponse existingResponse = findEcuResponse(ecu, responses);
                if (existingResponse == null) {
                    throw new java.lang.IllegalStateException("missing starting ecu response");
                }
                existingResponse.append(line.substring(offset4));
                return;
            default:
                throw new java.lang.IllegalStateException("invalid frame flag");
        }
    }

    private static com.navdy.obd.EcuResponse findEcuResponse(int ecu, java.util.List<com.navdy.obd.EcuResponse> responses) {
        for (int i = 0; i < responses.size(); i++) {
            com.navdy.obd.EcuResponse response = (com.navdy.obd.EcuResponse) responses.get(i);
            if (response.ecu == ecu) {
                return response;
            }
        }
        return null;
    }

    private static java.lang.String join(java.lang.String[] strings) {
        if (strings == null || strings.length == 0) {
            return "";
        }
        java.lang.StringBuilder builder = new java.lang.StringBuilder();
        for (java.lang.String append : strings) {
            builder.append(append);
        }
        return builder.toString();
    }
}
