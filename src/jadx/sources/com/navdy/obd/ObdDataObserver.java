package com.navdy.obd;

public class ObdDataObserver implements com.navdy.obd.command.IObdDataObserver {
    static final org.slf4j.Logger CAN_RAW_LOG = org.slf4j.LoggerFactory.getLogger(com.navdy.obd.ObdService.CAN_BUS_LOGGER);
    static final org.slf4j.Logger OBD_RAW_LOG = org.slf4j.LoggerFactory.getLogger("com.navdy.obd.ObdRaw");

    public void onCommand(java.lang.String command) {
        OBD_RAW_LOG.debug("C,{}", (java.lang.Object) command);
    }

    public void onResponse(java.lang.String response) {
        OBD_RAW_LOG.debug("R,{}", (java.lang.Object) response);
    }

    public void onError(java.lang.String error) {
        OBD_RAW_LOG.error("E,{}", (java.lang.Object) error);
    }

    public void onRawCanBusMessage(java.lang.String message) {
        CAN_RAW_LOG.debug(message);
    }
}
