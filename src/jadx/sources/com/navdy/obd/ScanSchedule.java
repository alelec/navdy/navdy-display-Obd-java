package com.navdy.obd;

public final class ScanSchedule implements android.os.Parcelable {
    public static final android.os.Parcelable.Creator<com.navdy.obd.ScanSchedule> CREATOR = new com.navdy.obd.ScanSchedule.Anon1();
    java.util.Map<java.lang.Integer, com.navdy.obd.ScanSchedule.Scan> schedule;

    static class Anon1 implements android.os.Parcelable.Creator<com.navdy.obd.ScanSchedule> {
        Anon1() {
        }

        public com.navdy.obd.ScanSchedule createFromParcel(android.os.Parcel in) {
            return new com.navdy.obd.ScanSchedule(in, null);
        }

        public com.navdy.obd.ScanSchedule[] newArray(int size) {
            return new com.navdy.obd.ScanSchedule[size];
        }
    }

    public static class Scan {
        public int pid;
        public int scanInterval;

        public Scan(int pid2, int scanInterval2) {
            this.pid = pid2;
            this.scanInterval = scanInterval2;
        }
    }

    /* synthetic */ ScanSchedule(android.os.Parcel x0, com.navdy.obd.ScanSchedule.Anon1 x1) {
        this(x0);
    }

    public ScanSchedule() {
        this.schedule = new java.util.HashMap();
    }

    public ScanSchedule(com.navdy.obd.ScanSchedule source) {
        this.schedule = new java.util.HashMap(source.schedule);
    }

    private ScanSchedule(android.os.Parcel in) {
        readFromParcel(in);
    }

    public void merge(com.navdy.obd.ScanSchedule otherSchedule) {
        for (java.util.Map.Entry<java.lang.Integer, com.navdy.obd.ScanSchedule.Scan> entry : otherSchedule.schedule.entrySet()) {
            com.navdy.obd.ScanSchedule.Scan scan = (com.navdy.obd.ScanSchedule.Scan) entry.getValue();
            addPid(scan.pid, scan.scanInterval);
        }
    }

    public boolean isEmpty() {
        return this.schedule.isEmpty();
    }

    public void addPid(int pid, int scanInterval) {
        com.navdy.obd.ScanSchedule.Scan oldScan = (com.navdy.obd.ScanSchedule.Scan) this.schedule.get(java.lang.Integer.valueOf(pid));
        if (oldScan == null) {
            this.schedule.put(java.lang.Integer.valueOf(pid), new com.navdy.obd.ScanSchedule.Scan(pid, scanInterval));
        } else if (scanInterval < oldScan.scanInterval) {
            oldScan.scanInterval = scanInterval;
        }
    }

    public void addPids(java.util.List<com.navdy.obd.Pid> pids, int scanInterval) {
        for (com.navdy.obd.Pid pid : pids) {
            addPid(pid.getId(), scanInterval);
        }
    }

    public com.navdy.obd.ScanSchedule.Scan remove(int pid) {
        return (com.navdy.obd.ScanSchedule.Scan) this.schedule.remove(java.lang.Integer.valueOf(pid));
    }

    public int size() {
        return this.schedule.size();
    }

    public java.util.List<com.navdy.obd.ScanSchedule.Scan> getScanList() {
        return new java.util.ArrayList(this.schedule.values());
    }

    public void writeToParcel(android.os.Parcel out, int flags) {
        out.writeInt(this.schedule.size());
        for (java.util.Map.Entry<java.lang.Integer, com.navdy.obd.ScanSchedule.Scan> entry : this.schedule.entrySet()) {
            com.navdy.obd.ScanSchedule.Scan scan = (com.navdy.obd.ScanSchedule.Scan) entry.getValue();
            out.writeInt(scan.pid);
            out.writeInt(scan.scanInterval);
        }
    }

    public void readFromParcel(android.os.Parcel in) {
        this.schedule = new java.util.HashMap();
        int length = in.readInt();
        for (int i = 0; i < length; i++) {
            addPid(in.readInt(), in.readInt());
        }
    }

    public int describeContents() {
        return 0;
    }

    public java.lang.String toString() {
        java.lang.StringBuilder builder = new java.lang.StringBuilder("ScanSchedule{ schedule=");
        boolean first = true;
        if (this.schedule != null) {
            for (java.util.Map.Entry<java.lang.Integer, com.navdy.obd.ScanSchedule.Scan> entry : this.schedule.entrySet()) {
                com.navdy.obd.ScanSchedule.Scan scan = (com.navdy.obd.ScanSchedule.Scan) entry.getValue();
                if (!first) {
                    builder.append(",");
                } else {
                    first = false;
                }
                builder.append("[").append(scan.pid).append(", ").append(scan.scanInterval).append("ms]");
            }
        } else {
            builder.append("null");
        }
        builder.append(" }");
        return builder.toString();
    }
}
