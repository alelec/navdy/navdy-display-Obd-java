package com.navdy.obd.can;

public class CANBusDataDescriptor {
    public int bitOffset;
    public int byteOffset;
    public int dataFrequency;
    public java.lang.String header;
    public int lengthInBits;
    public boolean littleEndian;
    public double maxValue;
    public double minValue;
    public java.lang.String name;
    public int obdPid;
    public double resolution;
    public long valueOffset;

    public CANBusDataDescriptor(java.lang.String name2, java.lang.String header2, int byteOffset2, int bitOffset2, int lengthInBits2, double resolution2, long valueOffset2, double minValue2, double maxValue2, int obd2Pid, int dataFrequency2, boolean littleEndian2) {
        this.name = name2;
        this.header = header2;
        this.byteOffset = byteOffset2;
        this.bitOffset = bitOffset2;
        this.lengthInBits = lengthInBits2;
        this.resolution = resolution2;
        this.valueOffset = valueOffset2;
        this.minValue = minValue2;
        this.maxValue = maxValue2;
        this.obdPid = obd2Pid;
        this.dataFrequency = dataFrequency2;
        this.littleEndian = littleEndian2;
    }
}
