package com.navdy.obd;

public class CarState {
    android.util.SparseArray<com.navdy.obd.Pid> lastReadings = new android.util.SparseArray<>();

    public void recordReadings(java.util.List<com.navdy.obd.Pid> pids) {
        for (com.navdy.obd.Pid pid : pids) {
            recordReading(pid);
        }
    }

    public boolean recordReading(com.navdy.obd.Pid pid) {
        int id = pid.getId();
        double newValue = pid.getValue();
        long newTimeStamp = pid.getTimeStamp();
        if (newValue == -2.147483648E9d) {
            return false;
        }
        com.navdy.obd.Pid oldValue = (com.navdy.obd.Pid) this.lastReadings.get(id);
        if (oldValue == null) {
            com.navdy.obd.Pid copy = new com.navdy.obd.Pid(id);
            copy.setTimeStamp(newTimeStamp);
            copy.setValue(newValue);
            this.lastReadings.put(id, copy);
            return true;
        } else if (newValue == oldValue.getValue()) {
            return false;
        } else {
            oldValue.setTimeStamp(newTimeStamp);
            oldValue.setValue(newValue);
            return true;
        }
    }

    public java.util.List<com.navdy.obd.Pid> getReadings(java.util.List<com.navdy.obd.Pid> pids) {
        java.util.List<com.navdy.obd.Pid> readings = new java.util.ArrayList<>();
        for (com.navdy.obd.Pid pid : pids) {
            com.navdy.obd.Pid lastReading = (com.navdy.obd.Pid) this.lastReadings.get(pid.getId());
            if (!(lastReading == null || lastReading.getValue() == -2.147483648E9d)) {
                readings.add(lastReading);
            }
        }
        return readings;
    }
}
