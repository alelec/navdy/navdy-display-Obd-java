package com.navdy.obd;

public class PidListener {
    private com.navdy.obd.CarState currentState = new com.navdy.obd.CarState();
    public final com.navdy.obd.IPidListener listener;
    public final com.navdy.obd.PidSet monitoredPids;
    public final com.navdy.obd.ScanSchedule schedule;

    public PidListener(com.navdy.obd.IPidListener listener2, com.navdy.obd.ScanSchedule schedule2) {
        this.listener = listener2;
        this.schedule = schedule2;
        this.monitoredPids = new com.navdy.obd.PidSet(schedule2);
    }

    public void recordReadings(java.util.List<com.navdy.obd.Pid> pids) throws android.os.RemoteException {
        java.util.List<com.navdy.obd.Pid> changedPids = null;
        for (com.navdy.obd.Pid pid : pids) {
            if (this.monitoredPids.contains(pid) && this.currentState.recordReading(pid)) {
                if (changedPids == null) {
                    changedPids = new java.util.ArrayList<>();
                }
                changedPids.add(pid);
            }
        }
        if (changedPids != null) {
            this.listener.pidsChanged(changedPids);
        }
        this.listener.pidsRead(pids, changedPids);
    }
}
