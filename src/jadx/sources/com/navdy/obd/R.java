package com.navdy.obd;

public final class R {

    public static final class drawable {
        public static final int ic_launcher = 2130837505;
    }

    public static final class raw {
        public static final int default_profile = 2131034112;
        public static final int default_stn_configuration = 2131034113;
        public static final int j1939_profile = 2131034114;
        public static final int update = 2131034115;
    }

    public static final class string {
        public static final int app_name = 2131165185;
    }

    public static final class xml {
        public static final int units = 2130968577;
    }
}
