package com.navdy.obd;

public class ObdJob implements java.lang.Runnable {
    static final org.slf4j.Logger Log = org.slf4j.LoggerFactory.getLogger(com.navdy.obd.ObdJob.class);
    protected com.navdy.obd.io.IChannel channel;
    protected com.navdy.obd.command.ICommand command;
    protected com.navdy.obd.command.IObdDataObserver commandObserver;
    protected boolean disconnectUponFailure;
    protected com.navdy.obd.ObdJob.IListener listener;
    protected com.navdy.obd.Protocol protocol;

    public interface IListener {
        void onComplete(com.navdy.obd.ObdJob obdJob, boolean z);
    }

    public ObdJob(com.navdy.obd.command.ICommand command2, com.navdy.obd.io.IChannel channel2, com.navdy.obd.Protocol protocol2, com.navdy.obd.ObdJob.IListener listener2, com.navdy.obd.command.IObdDataObserver commandObserver2, boolean disconnectUponFailure2) {
        this.disconnectUponFailure = false;
        this.command = command2;
        this.channel = channel2;
        this.listener = listener2;
        this.protocol = protocol2;
        this.commandObserver = commandObserver2;
        this.disconnectUponFailure = disconnectUponFailure2;
    }

    public ObdJob(com.navdy.obd.command.ICommand command2, com.navdy.obd.io.IChannel channel2, com.navdy.obd.Protocol protocol2, com.navdy.obd.command.IObdDataObserver commandObserver2) {
        this(command2, channel2, protocol2, null, commandObserver2, true);
    }

    public ObdJob(com.navdy.obd.command.ICommand command2, com.navdy.obd.io.IChannel channel2, com.navdy.obd.Protocol protocol2, com.navdy.obd.ObdJob.IListener listener2, com.navdy.obd.command.IObdDataObserver commandObserver2) {
        this(command2, channel2, protocol2, listener2, commandObserver2, true);
    }

    /* access modifiers changed from: protected */
    public void postExecute() throws java.io.IOException {
    }

    public void run() {
        Log.debug("executing command {}", (java.lang.Object) this.command.getName());
        boolean succeeded = false;
        try {
            this.command.execute(this.channel.getInputStream(), this.channel.getOutputStream(), this.protocol, this.commandObserver);
            Log.debug("response {}", (java.lang.Object) this.command.getResponse());
            postExecute();
            succeeded = true;
        } catch (java.lang.Exception e) {
            if (this.command instanceof com.navdy.obd.command.InitializeCommand) {
                Log.error("InitializeCommand failed, with an IOException. " + e.getMessage());
            } else {
                Log.error("I/O error {} executing command {} - closing channel", e.toString(), this.command.getName(), e);
                Log.error("Response received {}", (java.lang.Object) this.command.getResponse());
            }
            if (this.disconnectUponFailure) {
                this.channel.disconnect();
            }
        }
        if (this.listener != null) {
            this.listener.onComplete(this, succeeded);
        }
    }
}
