package com.navdy.obd;

public class EcuResponse {
    public byte[] data;
    public final int ecu;
    private java.util.List<byte[]> frames;
    public int length;
    public boolean multiline;
    private int offset = 0;

    public EcuResponse(int ecu2, boolean multiline2, int length2) {
        this.ecu = ecu2;
        this.length = length2;
        this.multiline = multiline2;
    }

    public EcuResponse(int ecu2, byte[] data2) {
        this.ecu = ecu2;
        this.data = data2;
        this.multiline = true;
        this.length = data2.length;
    }

    public EcuResponse(int ecu2) {
        this.ecu = ecu2;
        this.multiline = false;
    }

    public void addFrame(java.lang.String hexString) {
        if (this.frames == null) {
            this.frames = new java.util.ArrayList();
        }
        byte[] bytes = new byte[(hexString.length() / 2)];
        com.navdy.obd.util.HexUtil.parseHexString(hexString, bytes, 0, bytes.length);
        this.frames.add(bytes);
    }

    public void flatten() {
        if (this.frames != null && this.frames.size() != 0) {
            if (this.frames.size() == 1) {
                this.data = (byte[]) this.frames.get(0);
            } else {
                java.io.ByteArrayOutputStream stream = new java.io.ByteArrayOutputStream();
                for (int i = 0; i < this.frames.size(); i++) {
                    byte[] frame = (byte[]) this.frames.get(i);
                    if (i == 0) {
                        stream.write(frame, 0, 2);
                    }
                    stream.write(frame, 3, frame.length - 4);
                }
                this.data = stream.toByteArray();
            }
            this.length = this.data.length;
        }
    }

    public void append(java.lang.String hexString) {
        if (this.data == null) {
            this.data = new byte[this.length];
        }
        int srcLen = hexString.length() / 2;
        com.navdy.obd.util.HexUtil.parseHexString(hexString, this.data, this.offset, java.lang.Math.min(srcLen, this.length - this.offset));
        this.offset += srcLen;
    }
}
