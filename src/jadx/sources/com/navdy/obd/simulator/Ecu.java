package com.navdy.obd.simulator;

public class Ecu {
    private java.util.HashMap<java.lang.String, com.navdy.obd.simulator.IPid> pids = new java.util.HashMap<>();

    class Pid implements com.navdy.obd.simulator.IPid {
        int bytes;
        java.lang.String code;
        com.navdy.obd.simulator.LinearInterpolation interpolator;
        java.lang.String name;

        Pid(java.lang.String name2, java.lang.String code2, int bytes2, com.navdy.obd.simulator.LinearInterpolation interpolator2) {
            this.name = name2;
            this.code = code2;
            this.bytes = bytes2;
            this.interpolator = interpolator2;
        }

        public java.lang.String getValue(long timestamp) {
            long value = this.interpolator.getValue(timestamp);
            if (this.bytes == 1) {
                return java.lang.String.format("%02x", new java.lang.Object[]{java.lang.Long.valueOf(255 & value)});
            } else if (this.bytes != 2) {
                return "";
            } else {
                return java.lang.String.format("%04x", new java.lang.Object[]{java.lang.Long.valueOf(65535 & value)});
            }
        }

        public java.lang.String getCode() {
            return this.code;
        }
    }

    class SupportedPids implements com.navdy.obd.simulator.IPid {
        private java.lang.String bitMask;

        SupportedPids(java.util.Set<java.lang.String> pidCodes) {
            byte[] bitField = new byte[4];
            for (java.lang.String code : pidCodes) {
                int id = java.lang.Integer.valueOf(code, 16).intValue() - 1;
                int pos = id / 8;
                bitField[pos] = (byte) (bitField[pos] | (1 << (7 - (id % 8))));
            }
            java.lang.StringBuilder hexString = new java.lang.StringBuilder();
            for (byte val : bitField) {
                if (val < 16) {
                    hexString.append('0');
                }
                hexString.append(java.lang.Integer.toHexString(val));
            }
            this.bitMask = hexString.toString();
        }

        public java.lang.String getValue(long timestamp) {
            return this.bitMask;
        }

        public java.lang.String getCode() {
            return "00";
        }
    }

    Ecu() {
        addPid(new com.navdy.obd.simulator.Ecu.Pid("Speed", "0d", 1, com.navdy.obd.simulator.LinearInterpolation.generate(50, 0, 100, 10000, 5000)));
        addPid(new com.navdy.obd.simulator.Ecu.Pid("RPM", "0c", 2, com.navdy.obd.simulator.LinearInterpolation.generate(50, 0, 16000, 10000, 500)));
        addPid(new com.navdy.obd.simulator.Ecu.Pid("Temperature", com.navdy.obd.update.STNSerialDeviceFirmwareManager.CURRENT_UPDATE_FILE_VERSION, 1, com.navdy.obd.simulator.LinearInterpolation.generate(50, 0, 255, 100000, 10000)));
        addPid(new com.navdy.obd.simulator.Ecu.SupportedPids(this.pids.keySet()));
    }

    public void addPid(com.navdy.obd.simulator.IPid pid) {
        this.pids.put(pid.getCode(), pid);
    }

    public boolean supports(java.lang.String pid) {
        return this.pids.keySet().contains(pid);
    }

    public java.lang.String readPid(java.lang.String code) {
        long timestamp = java.lang.System.currentTimeMillis();
        com.navdy.obd.simulator.IPid pid = (com.navdy.obd.simulator.IPid) this.pids.get(code);
        if (pid != null) {
            return pid.getValue(timestamp);
        }
        return "";
    }
}
