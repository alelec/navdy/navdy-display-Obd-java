package com.navdy.obd.simulator;

public class MockObdChannel implements com.navdy.obd.io.IChannel {
    public static final java.lang.String CONNECTION_NAME = "mock_obd_channel";
    static final org.slf4j.Logger Log = org.slf4j.LoggerFactory.getLogger(com.navdy.obd.simulator.MockObdChannel.class);
    public static com.navdy.obd.io.ChannelInfo MOCK_CHANNEL_INFO = new com.navdy.obd.io.ChannelInfo(com.navdy.obd.io.ChannelInfo.ConnectionType.MOCK, "simulated", CONNECTION_NAME);
    private java.io.PipedInputStream inputStream;
    private java.io.PipedOutputStream outputStream;
    private com.navdy.obd.simulator.Elm327 simulator;
    private com.navdy.obd.io.IChannelSink sink;
    private int state = 0;

    public MockObdChannel(com.navdy.obd.io.IChannelSink sink2) {
        this.sink = sink2;
    }

    public int getState() {
        return this.state;
    }

    private void setState(int newState) {
        this.state = newState;
        this.sink.onStateChange(newState);
    }

    public void connect(java.lang.String address) {
        if (!CONNECTION_NAME.equals(address)) {
            Log.error("Invalid address passed to channel");
        } else if (this.state == 0) {
            Log.debug("Connecting to mock obd channel");
            this.outputStream = new java.io.PipedOutputStream();
            this.inputStream = new java.io.PipedInputStream();
            try {
                this.simulator = new com.navdy.obd.simulator.Elm327(new java.io.PipedInputStream(this.outputStream), new java.io.PipedOutputStream(this.inputStream));
                this.simulator.start();
                setState(2);
            } catch (java.io.IOException e) {
                Log.error("Exception starting elm327", (java.lang.Throwable) e);
            }
        }
    }

    public void disconnect() {
        if (this.state != 0) {
            sendQuitCommand();
            safelyClose(this.outputStream);
            safelyClose(this.inputStream);
            if (this.simulator != null) {
                try {
                    this.simulator.join();
                } catch (java.lang.InterruptedException e) {
                    Log.debug("Interrupted joining simulator thread", (java.lang.Throwable) e);
                }
                this.simulator = null;
            }
            this.outputStream = null;
            this.inputStream = null;
            setState(0);
        }
    }

    private void sendQuitCommand() {
        if (this.outputStream != null) {
            try {
                this.outputStream.write("QUIT\r>".getBytes());
            } catch (java.io.IOException e) {
                Log.error("Exception sending quit command", (java.lang.Throwable) e);
            }
        }
    }

    private void safelyClose(java.io.Closeable stream) {
        if (stream != null) {
            try {
                stream.close();
            } catch (java.io.IOException e) {
                Log.error("Exception closing stream", (java.lang.Throwable) e);
            }
        }
    }

    public java.io.InputStream getInputStream() throws java.io.IOException {
        return this.inputStream;
    }

    public java.io.OutputStream getOutputStream() throws java.io.IOException {
        return this.outputStream;
    }
}
