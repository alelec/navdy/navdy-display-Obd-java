package com.navdy.obd.simulator;

public class LinearInterpolation {
    static final int MAX_SIZE = 100;
    private int count;
    long[] timestamps;
    long[] values;

    LinearInterpolation() {
        this.timestamps = new long[100];
        this.values = new long[100];
        this.count = 0;
        this.count = 1;
        this.timestamps[0] = 0;
        this.values[0] = 0;
    }

    static com.navdy.obd.simulator.LinearInterpolation generate(int samples, int min, int max, int avgDelay, int delayRange) {
        com.navdy.obd.simulator.LinearInterpolation result = new com.navdy.obd.simulator.LinearInterpolation();
        java.util.Random random = new java.util.Random();
        for (int i = 0; i < samples; i++) {
            result.add((long) ((random.nextInt((delayRange * 2) + 1) + avgDelay) - delayRange), (long) (random.nextInt((max - min) + 1) + min));
        }
        return result;
    }

    /* access modifiers changed from: 0000 */
    public long timeRange() {
        long maxTimestamp = this.timestamps[this.count - 1];
        if (maxTimestamp > 0) {
            return maxTimestamp;
        }
        return 1;
    }

    /* access modifiers changed from: 0000 */
    public long localTime(long timestamp) {
        long range = timeRange();
        long offset = timestamp % range;
        if ((timestamp / range) % 2 == 1) {
            return range - offset;
        }
        return offset;
    }

    /* access modifiers changed from: 0000 */
    public long getValue(long timestamp) {
        long localTimestamp = localTime(timestamp);
        int position = java.util.Arrays.binarySearch(this.timestamps, 0, this.count, localTimestamp);
        if (position >= 0) {
            return this.values[position];
        }
        int p1 = (-position) - 2;
        int p2 = p1 + 1;
        long t1 = this.timestamps[p1];
        long t2 = this.timestamps[p2];
        long v1 = this.values[p1];
        return (((this.values[p2] - v1) * (localTimestamp - t1)) / (t2 - t1)) + v1;
    }

    /* access modifiers changed from: 0000 */
    public void add(long delay, long value) {
        this.timestamps[this.count] = this.timestamps[this.count - 1] + delay;
        this.values[this.count] = value;
        this.count++;
    }
}
