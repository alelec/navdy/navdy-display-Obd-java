package com.navdy.obd.simulator;

public class Elm327 extends java.lang.Thread {
    static final org.slf4j.Logger Log = org.slf4j.LoggerFactory.getLogger(com.navdy.obd.simulator.Elm327.class);
    private com.navdy.obd.simulator.Ecu ecu = new com.navdy.obd.simulator.Ecu();
    private java.io.InputStream input;
    private java.io.OutputStream output;

    Elm327(java.io.InputStream input2, java.io.OutputStream output2) {
        this.input = input2;
        this.output = output2;
    }

    public void run() {
        try {
            Log.info("Starting Elm327 reader thread");
            java.io.BufferedReader reader = new java.io.BufferedReader(new java.io.InputStreamReader(this.input));
            java.io.OutputStreamWriter writer = new java.io.OutputStreamWriter(this.output);
            while (true) {
                java.lang.String command = reader.readLine();
                if (command != null) {
                    java.lang.String command2 = command.toLowerCase();
                    if (command2.startsWith("at")) {
                        handleCommand(writer, command2);
                    } else if (command2.startsWith("01")) {
                        handleRead(writer, command2);
                    } else if (command2.equals("quit")) {
                        Log.info("Quitting Elm327 reader thread");
                        return;
                    } else {
                        Log.warn("Unknown command {}", (java.lang.Object) command2);
                        writer.write("UNKNOWN COMMAND\r>");
                    }
                    writer.flush();
                } else {
                    return;
                }
            }
        } catch (java.io.IOException e) {
            Log.error("Exception - exiting Elm327 thread", (java.lang.Throwable) e);
        }
    }

    private void handleCommand(java.io.OutputStreamWriter writer, java.lang.String command) throws java.io.IOException {
        Log.debug("Handling command {}", (java.lang.Object) command);
        writer.write("OK\r>");
    }

    private void handleRead(java.io.OutputStreamWriter writer, java.lang.String command) throws java.io.IOException {
        Log.debug("Reading pid {}", (java.lang.Object) command);
        java.lang.StringBuilder response = new java.lang.StringBuilder();
        response.append("41");
        for (java.lang.String tail = command.substring(2).trim(); tail.length() >= 2; tail = tail.substring(2)) {
            java.lang.String pid = tail.substring(0, 2);
            if (this.ecu.supports(pid)) {
                response.append(pid);
                response.append(this.ecu.readPid(pid));
            }
        }
        response.append("\r>");
        writer.write(response.toString());
    }
}
