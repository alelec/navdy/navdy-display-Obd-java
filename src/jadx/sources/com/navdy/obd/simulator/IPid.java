package com.navdy.obd.simulator;

/* compiled from: Ecu */
interface IPid {
    java.lang.String getCode();

    java.lang.String getValue(long j);
}
