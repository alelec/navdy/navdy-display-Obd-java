package com.navdy.obd;

public class Protocol {
    public static final int CAN_11_250 = 8;
    public static final com.navdy.obd.Protocol CAN_11_250_PROTOCOL = new com.navdy.obd.Protocol(8, "ISO 15765-4 (CAN 11/250)", 0, 3);
    public static final int CAN_11_500 = 6;
    public static final com.navdy.obd.Protocol CAN_11_500_PROTOCOL = new com.navdy.obd.Protocol(6, "ISO 15765-4 (CAN 11/500)", 0, 3);
    public static final int CAN_29_250 = 9;
    public static final com.navdy.obd.Protocol CAN_29_250_PROTOCOL = new com.navdy.obd.Protocol(9, "ISO 15765-4 (CAN 29/250)", 6, 8);
    public static final int CAN_29_500 = 7;
    public static final com.navdy.obd.Protocol CAN_29_500_PROTOCOL = new com.navdy.obd.Protocol(7, "ISO 15765-4 (CAN 29/500)", 6, 8);
    public static final int ISO_14230_4_KWP = 4;
    public static final int ISO_14230_4_KWP_FAST = 5;
    public static final com.navdy.obd.Protocol ISO_14230_4_KWP_FAST_PROTOCOL = new com.navdy.obd.Protocol(5, "ISO 14230-4 KWP (fast)", 4, 6);
    public static final com.navdy.obd.Protocol ISO_14230_4_KWP_PROTOCOL = new com.navdy.obd.Protocol(4, "ISO 14230-4 KWP", 4, 6);
    public static final int ISO_9141_2 = 3;
    public static final com.navdy.obd.Protocol ISO_9141_2_PROTOCOL = new com.navdy.obd.Protocol(3, "ISO 9141-2", 4, 6);
    public static com.navdy.obd.Protocol[] PROTOCOLS = {SAE_J1850_PWM_PROTOCOL, SAE_J1850_VPW_PROTOCOL, ISO_9141_2_PROTOCOL, ISO_14230_4_KWP_PROTOCOL, ISO_14230_4_KWP_FAST_PROTOCOL, CAN_11_500_PROTOCOL, CAN_29_500_PROTOCOL, CAN_11_250_PROTOCOL, CAN_29_250_PROTOCOL, SAE_J1939_CAN_PROTOCOL};
    public static final int SAE_J1850_PWM = 2;
    public static final com.navdy.obd.Protocol SAE_J1850_PWM_PROTOCOL = new com.navdy.obd.Protocol(2, "SAE J1850 VPW", 4, 6);
    public static final int SAE_J1850_VPW = 1;
    public static final com.navdy.obd.Protocol SAE_J1850_VPW_PROTOCOL = new com.navdy.obd.Protocol(1, "SAE J1850 PWM", 4, 6);
    public static final int SAE_J1939_CAN = 10;
    public static final com.navdy.obd.Protocol SAE_J1939_CAN_PROTOCOL = new com.navdy.obd.Protocol(10, "SAE J1939 (CAN 29/250)", 6, 8);
    public final com.navdy.obd.Protocol.FrameFormat format;
    public final int id;
    public final java.lang.String name;

    static class FrameFormat {
        public final int ecuEnd;
        public final int ecuStart;

        public FrameFormat(int ecuStart2, int ecuEnd2) {
            this.ecuStart = ecuStart2;
            this.ecuEnd = ecuEnd2;
        }
    }

    public Protocol(int id2, java.lang.String name2, int ecuStart, int ecuEnd) {
        this.id = id2;
        this.name = name2;
        this.format = new com.navdy.obd.Protocol.FrameFormat(ecuStart, ecuEnd);
    }

    static com.navdy.obd.Protocol parse(java.lang.String protocolNumber) {
        if (protocolNumber.startsWith("A")) {
            protocolNumber = protocolNumber.substring(1);
        }
        try {
            int protocolIndex = java.lang.Integer.valueOf(protocolNumber.trim()).intValue();
            if (protocolIndex < 1 || protocolIndex > PROTOCOLS.length) {
                return null;
            }
            return PROTOCOLS[protocolIndex - 1];
        } catch (java.lang.NumberFormatException e) {
            return null;
        }
    }

    public java.lang.String toString() {
        return "Protocol{name='" + this.name + ch.qos.logback.core.CoreConstants.SINGLE_QUOTE_CHAR + ch.qos.logback.core.CoreConstants.CURLY_RIGHT;
    }
}
