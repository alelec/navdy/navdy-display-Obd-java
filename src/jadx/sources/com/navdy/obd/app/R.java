package com.navdy.obd.app;

public final class R {

    public static final class attr {
    }

    public static final class dimen {
        public static final int activity_horizontal_margin = 2131099648;
        public static final int activity_vertical_margin = 2131099649;
    }

    public static final class drawable {
        public static final int app_icon = 2130837504;
        public static final int ic_launcher = 2130837505;
    }

    public static final class id {
        public static final int action_settings = 2131361809;
        public static final int auto_connect = 2131361811;
        public static final int btn_upgrade = 2131361796;
        public static final int button_scan = 2131361805;
        public static final int button_send = 2131361808;
        public static final int edit_text_out = 2131361807;
        public static final int empty = 2131361804;
        public static final int flash = 2131361813;
        public static final int flash_upgrade = 2131361814;
        public static final int in = 2131361806;
        public static final int new_devices = 2131361803;
        public static final int paired_devices = 2131361800;
        public static final int paired_empty = 2131361801;
        public static final int scan = 2131361810;
        public static final int title_left_text = 2131361797;
        public static final int title_new_devices = 2131361802;
        public static final int title_paired_devices = 2131361799;
        public static final int title_right_text = 2131361798;
        public static final int txt_current_version = 2131361793;
        public static final int txt_current_version_label = 2131361792;
        public static final int txt_upgrade_version = 2131361795;
        public static final int txt_upgrade_version_label = 2131361794;
        public static final int upgrade = 2131361812;
    }

    public static final class layout {
        public static final int activity_upgrade_obd = 2130903040;
        public static final int custom_title = 2130903041;
        public static final int device_list = 2130903042;
        public static final int device_name = 2130903043;
        public static final int main = 2130903044;
        public static final int message = 2130903045;
    }

    public static final class menu {
        public static final int menu = 2131296256;
        public static final int option_menu = 2131296257;
    }

    public static final class raw {
        public static final int default_profile = 2131034112;
        public static final int default_stn_configuration = 2131034113;
        public static final int j1939_profile = 2131034114;
        public static final int update = 2131034115;
    }

    public static final class string {
        public static final int action_settings = 2131165184;
        public static final int app_name = 2131165185;
        public static final int auto_connect = 2131165186;
        public static final int bt_not_enabled_leaving = 2131165187;
        public static final int button_scan = 2131165188;
        public static final int connect = 2131165189;
        public static final int current_version = 2131165190;
        public static final int discoverable = 2131165191;
        public static final int flash_recover = 2131165192;
        public static final int flash_upgrade = 2131165193;
        public static final int hello_world = 2131165194;
        public static final int none_found = 2131165195;
        public static final int none_paired = 2131165196;
        public static final int not_connected = 2131165197;
        public static final int scanning = 2131165198;
        public static final int select_device = 2131165199;
        public static final int send = 2131165200;
        public static final int title_activity_upgrade_obd = 2131165201;
        public static final int title_connected_to = 2131165202;
        public static final int title_connecting = 2131165203;
        public static final int title_not_connected = 2131165204;
        public static final int title_other_devices = 2131165205;
        public static final int title_paired_devices = 2131165206;
        public static final int upgrade = 2131165207;
        public static final int upgrade_version = 2131165208;
    }

    public static final class style {
        public static final int AppTheme = 2131230720;
    }

    public static final class xml {
        public static final int device_filter = 2130968576;
        public static final int units = 2130968577;
    }
}
