package com.navdy.obd.app;

public final class BuildConfig {
    public static final java.lang.String APPLICATION_ID = "com.navdy.obd.app";
    public static final java.lang.String BUILD_TYPE = "debug";
    public static final boolean DEBUG = java.lang.Boolean.parseBoolean("true");
    public static final java.lang.String FLAVOR = "";
    public static final int VERSION_CODE = 3080;
    public static final java.lang.String VERSION_NAME = "1.0.3080-8c7e56a";
}
