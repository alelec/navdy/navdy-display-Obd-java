package com.navdy.obd.app;

public class UpgradeObdActivity extends android.app.Activity implements android.content.ServiceConnection {
    /* access modifiers changed from: private */
    public static final java.lang.String TAG = com.navdy.obd.app.UpgradeObdActivity.class.getSimpleName();
    @butterknife.InjectView(2131361793)
    protected android.widget.TextView mTxtCurrentVersion;
    @butterknife.InjectView(2131361795)
    protected android.widget.TextView mTxtUpgradeVersion;
    private com.navdy.obd.IObdService obdApi;

    static class Anon1 extends android.os.AsyncTask<java.lang.Void, java.lang.Void, java.lang.Void> {
        final /* synthetic */ android.content.Context val$context;
        final /* synthetic */ boolean val$recover;

        /* renamed from: com.navdy.obd.app.UpgradeObdActivity$Anon1$Anon1 reason: collision with other inner class name */
        class C0001Anon1 implements com.navdy.obd.io.IChannelSink {
            C0001Anon1() {
            }

            public void onStateChange(int newState) {
                android.util.Log.e(com.navdy.obd.app.UpgradeObdActivity.TAG, "Connected");
            }

            public void onMessage(java.lang.String message) {
            }
        }

        Anon1(android.content.Context context, boolean z) {
            this.val$context = context;
            this.val$recover = z;
        }

        /* access modifiers changed from: protected */
        public java.lang.Void doInBackground(java.lang.Void... params) {
            com.navdy.obd.io.SerialChannel serialChannel = new com.navdy.obd.io.SerialChannel(this.val$context, new com.navdy.obd.app.UpgradeObdActivity.Anon1.C0001Anon1());
            serialChannel.connect("/dev/ttymxc3");
            com.navdy.obd.update.STNSerialDeviceFirmwareManager stnSerialDeviceFirmwareManager = new com.navdy.obd.update.STNSerialDeviceFirmwareManager(null, serialChannel);
            try {
                java.lang.String outputPath = this.val$context.getFilesDir().getAbsolutePath() + java.io.File.separator + "update.bin";
                com.navdy.obd.app.UpgradeObdActivity.writeResourceToFile(this.val$context, com.navdy.obd.app.R.raw.update, outputPath);
                com.navdy.obd.update.Update update = new com.navdy.obd.update.Update();
                update.mVersion = "";
                update.mUpdateFilePath = outputPath;
                stnSerialDeviceFirmwareManager.update(update, serialChannel.getSerialPort(), !this.val$recover);
            } catch (Throwable e) {
                android.util.Log.e(com.navdy.obd.app.UpgradeObdActivity.TAG, "Exception updating the firmware " + e);
            }
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.navdy.obd.app.R.layout.activity_upgrade_obd);
        butterknife.ButterKnife.inject((java.lang.Object) this, (android.app.Activity) this);
        android.content.Intent intent = new android.content.Intent(this, com.navdy.obd.ObdService.class);
        intent.setAction(com.navdy.obd.IObdService.class.getName());
        bindService(intent, this, 0);
    }

    private void update() {
        if (this.obdApi == null) {
            com.navdy.obd.app.Utility.toastNotConnectedMessage(this);
            return;
        }
        int state = 0;
        try {
            state = this.obdApi.getState();
        } catch (android.os.RemoteException e) {
            android.util.Log.w(TAG, "Failed to read service state");
        }
        if (state != 2) {
            com.navdy.obd.app.Utility.toastNotConnectedMessage(this);
            return;
        }
        try {
            java.lang.String outputPath = getFilesDir().getAbsolutePath() + java.io.File.separator + "update.bin";
            writeResourceToFile(this, com.navdy.obd.app.R.raw.update, outputPath);
            this.obdApi.updateDeviceFirmware("3.2.0", outputPath);
        } catch (android.os.RemoteException e2) {
            android.util.Log.e(TAG, "Exception updating the firmware " + e2);
        }
    }

    static void writeResourceToFile(android.content.Context context, int resId, java.lang.String outputPath) {
        java.io.InputStream is = context.getResources().openRawResource(resId);
        java.io.File file = new java.io.File(outputPath);
        if (file.exists()) {
            file.delete();
        }
        try {
            byte[] buffer = new byte[is.available()];
            is.read(buffer);
            java.io.FileOutputStream fos = new java.io.FileOutputStream(file);
            fos.write(buffer);
            fos.flush();
            fos.close();
            is.close();
        } catch (java.io.IOException e) {
            android.util.Log.e(TAG, "Error writing resource to " + outputPath);
        }
    }

    public void onClick(android.view.View v) {
        update();
    }

    private void initView() {
        try {
            this.mTxtCurrentVersion.setText(this.obdApi.getFirmwareVersion());
        } catch (android.os.RemoteException e) {
            android.util.Log.e(TAG, "Error while getting firmware version " + e.getCause());
        }
    }

    public void onServiceConnected(android.content.ComponentName name, android.os.IBinder service) {
        android.util.Log.d(TAG, "Obd service connected");
        this.obdApi = com.navdy.obd.IObdService.Stub.asInterface(service);
        initView();
    }

    public void onServiceDisconnected(android.content.ComponentName name) {
        android.util.Log.d(TAG, "Obd service disconnected");
    }

    public static void flash(android.content.Context context, boolean recover) {
        new com.navdy.obd.app.UpgradeObdActivity.Anon1<>(context, recover).execute(new java.lang.Void[0]);
    }
}
