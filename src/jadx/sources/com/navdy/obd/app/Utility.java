package com.navdy.obd.app;

public class Utility {
    public static void toast(android.content.Context context, java.lang.String message) {
        android.widget.Toast.makeText(context, message, 0).show();
    }

    public static void toast(android.content.Context context, int res) {
        android.widget.Toast.makeText(context, res, 0).show();
    }

    public static void toastNotConnectedMessage(android.content.Context context) {
        android.widget.Toast.makeText(context, com.navdy.obd.app.R.string.not_connected, 0).show();
    }
}
