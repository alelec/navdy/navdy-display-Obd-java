package com.navdy.obd.app;

public class ObdTestActivity$$ViewInjector {
    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.obd.app.ObdTestActivity target, java.lang.Object source) {
        target.mConversationView = (android.widget.ListView) finder.findRequiredView(source, com.navdy.obd.app.R.id.in, "field 'mConversationView'");
        target.mOutEditText = (android.widget.EditText) finder.findRequiredView(source, com.navdy.obd.app.R.id.edit_text_out, "field 'mOutEditText'");
        target.mSendButton = (android.widget.Button) finder.findRequiredView(source, com.navdy.obd.app.R.id.button_send, "field 'mSendButton'");
    }

    public static void reset(com.navdy.obd.app.ObdTestActivity target) {
        target.mConversationView = null;
        target.mOutEditText = null;
        target.mSendButton = null;
    }
}
