package com.navdy.obd.app;

public class UpgradeObdActivity$$ViewInjector {
    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.obd.app.UpgradeObdActivity target, java.lang.Object source) {
        target.mTxtCurrentVersion = (android.widget.TextView) finder.findRequiredView(source, com.navdy.obd.app.R.id.txt_current_version, "field 'mTxtCurrentVersion'");
        target.mTxtUpgradeVersion = (android.widget.TextView) finder.findRequiredView(source, com.navdy.obd.app.R.id.txt_upgrade_version, "field 'mTxtUpgradeVersion'");
    }

    public static void reset(com.navdy.obd.app.UpgradeObdActivity target) {
        target.mTxtCurrentVersion = null;
        target.mTxtUpgradeVersion = null;
    }
}
