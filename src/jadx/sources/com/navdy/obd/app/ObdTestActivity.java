package com.navdy.obd.app;

public class ObdTestActivity extends android.app.Activity {
    private static final boolean D = true;
    private static final java.lang.String DISCONNECT_CMD = "disconnect";
    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(com.navdy.obd.app.ObdTestActivity.class);
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_TOAST = 4;
    public static final int MESSAGE_WRITE = 3;
    private static final java.lang.String RAW_CMD = "raw";
    private static final java.lang.String READ_CMD = "read ";
    private static final int REQUEST_CONNECT_DEVICE = 1;
    private static final int REQUEST_ENABLE_BT = 2;
    private static final java.lang.String RESET_CMD = "reset";
    private static final java.lang.String SCAN_CMD = "scan";
    private static final java.lang.String TAG = "ObdTestApp";
    public static final java.lang.String TOAST = "toast";
    /* access modifiers changed from: private */
    public com.navdy.obd.ICarService carApi;
    private android.content.ServiceConnection carServiceConnection = new com.navdy.obd.app.ObdTestActivity.Anon3();
    /* access modifiers changed from: private */
    public android.widget.ArrayAdapter<java.lang.String> mConversationArrayAdapter;
    @butterknife.InjectView(2131361806)
    protected android.widget.ListView mConversationView;
    /* access modifiers changed from: private */
    public final android.os.Handler mHandler = new com.navdy.obd.app.ObdTestActivity.Anon6();
    @butterknife.InjectView(2131361807)
    protected android.widget.EditText mOutEditText;
    private java.lang.StringBuffer mOutStringBuffer = null;
    @butterknife.InjectView(2131361808)
    protected android.widget.Button mSendButton;
    private android.widget.TextView.OnEditorActionListener mWriteListener = new com.navdy.obd.app.ObdTestActivity.Anon5();
    /* access modifiers changed from: private */
    public com.navdy.obd.IObdService obdApi;
    private com.navdy.obd.IObdServiceListener.Stub obdListener = new com.navdy.obd.app.ObdTestActivity.Anon1();
    private android.content.ServiceConnection serviceConnection = new com.navdy.obd.app.ObdTestActivity.Anon2();

    class Anon1 extends com.navdy.obd.IObdServiceListener.Stub {
        Anon1() {
        }

        public void onConnectionStateChange(int newState) throws android.os.RemoteException {
            android.util.Log.d(com.navdy.obd.app.ObdTestActivity.TAG, "StateChange:" + newState);
            com.navdy.obd.app.ObdTestActivity.this.mHandler.obtainMessage(1, newState, -1).sendToTarget();
        }

        public void onStatusMessage(java.lang.String message) throws android.os.RemoteException {
            android.os.Message msg = com.navdy.obd.app.ObdTestActivity.this.mHandler.obtainMessage(4);
            android.os.Bundle bundle = new android.os.Bundle();
            bundle.putString("toast", message);
            msg.setData(bundle);
            com.navdy.obd.app.ObdTestActivity.this.mHandler.sendMessage(msg);
        }

        public void onRawData(java.lang.String data) throws android.os.RemoteException {
            byte[] buffer = data.getBytes();
            com.navdy.obd.app.ObdTestActivity.this.mHandler.obtainMessage(2, buffer.length, -1, buffer).sendToTarget();
        }

        public void scannedPids(java.util.List<com.navdy.obd.Pid> pids) throws android.os.RemoteException {
            java.lang.StringBuilder message = new java.lang.StringBuilder();
            for (com.navdy.obd.Pid pid : pids) {
                message.append(java.lang.Integer.toHexString(pid.getId()));
                message.append("=");
                message.append(pid.getValue());
                message.append(" ");
            }
            byte[] buffer = message.toString().getBytes();
            com.navdy.obd.app.ObdTestActivity.this.mHandler.obtainMessage(2, buffer.length, -1, buffer).sendToTarget();
        }

        public void supportedPids(java.util.List<com.navdy.obd.Pid> list) throws android.os.RemoteException {
        }

        public void scannedVIN(java.lang.String vin) throws android.os.RemoteException {
        }
    }

    class Anon2 implements android.content.ServiceConnection {
        Anon2() {
        }

        public void onServiceConnected(android.content.ComponentName name, android.os.IBinder service) {
            android.util.Log.i(com.navdy.obd.app.ObdTestActivity.TAG, "Service connection established");
            com.navdy.obd.app.ObdTestActivity.this.obdApi = com.navdy.obd.IObdService.Stub.asInterface(service);
        }

        public void onServiceDisconnected(android.content.ComponentName name) {
            android.util.Log.i(com.navdy.obd.app.ObdTestActivity.TAG, "Service disconnected");
        }
    }

    class Anon3 implements android.content.ServiceConnection {
        Anon3() {
        }

        public void onServiceConnected(android.content.ComponentName name, android.os.IBinder service) {
            android.util.Log.i(com.navdy.obd.app.ObdTestActivity.TAG, "CarService connection established");
            com.navdy.obd.app.ObdTestActivity.this.carApi = com.navdy.obd.ICarService.Stub.asInterface(service);
        }

        public void onServiceDisconnected(android.content.ComponentName name) {
            android.util.Log.i(com.navdy.obd.app.ObdTestActivity.TAG, "CarService disconnected");
        }
    }

    class Anon4 implements android.view.View.OnClickListener {
        Anon4() {
        }

        public void onClick(android.view.View v) {
            com.navdy.obd.app.ObdTestActivity.this.sendMessage(((android.widget.TextView) com.navdy.obd.app.ObdTestActivity.this.findViewById(com.navdy.obd.app.R.id.edit_text_out)).getText().toString());
        }
    }

    class Anon5 implements android.widget.TextView.OnEditorActionListener {
        Anon5() {
        }

        public boolean onEditorAction(android.widget.TextView view, int actionId, android.view.KeyEvent event) {
            if ((actionId == 0 && event.getAction() == 1) || actionId == 4) {
                com.navdy.obd.app.ObdTestActivity.this.sendMessage(view.getText().toString());
            }
            android.util.Log.i(com.navdy.obd.app.ObdTestActivity.TAG, "END onEditorAction");
            return false;
        }
    }

    class Anon6 extends android.os.Handler {
        Anon6() {
        }

        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                case 1:
                    android.util.Log.i(com.navdy.obd.app.ObdTestActivity.TAG, "MESSAGE_STATE_CHANGE: " + msg.arg1);
                    switch (msg.arg1) {
                        case 2:
                            com.navdy.obd.app.ObdTestActivity.this.mConversationArrayAdapter.clear();
                            return;
                        default:
                            return;
                    }
                case 2:
                    com.navdy.obd.app.ObdTestActivity.this.mConversationArrayAdapter.add(new java.lang.String((byte[]) msg.obj, 0, msg.arg1));
                    return;
                case 3:
                    com.navdy.obd.app.ObdTestActivity.this.mConversationArrayAdapter.add("Me:  " + new java.lang.String((byte[]) msg.obj));
                    return;
                case 4:
                    android.widget.Toast.makeText(com.navdy.obd.app.ObdTestActivity.this.getApplicationContext(), msg.getData().getString("toast"), 0).show();
                    return;
                default:
                    return;
            }
        }
    }

    public void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        android.util.Log.e(TAG, "+++ ON CREATE +++");
        LOG.info("Testing logback logging");
        setContentView(com.navdy.obd.app.R.layout.main);
        butterknife.ButterKnife.inject((java.lang.Object) this, (android.app.Activity) this);
    }

    public void onStart() {
        super.onStart();
        android.util.Log.e(TAG, "++ ON START ++");
        if (this.mOutStringBuffer == null) {
            setupChat();
        }
    }

    public synchronized void onResume() {
        super.onResume();
        android.util.Log.e(TAG, "+ ON RESUME +");
    }

    private void setupChat() {
        android.util.Log.d(TAG, "setupChat()");
        this.mConversationArrayAdapter = new android.widget.ArrayAdapter<>(this, com.navdy.obd.app.R.layout.message);
        this.mConversationView.setAdapter(this.mConversationArrayAdapter);
        this.mOutEditText.setOnEditorActionListener(this.mWriteListener);
        this.mSendButton.setOnClickListener(new com.navdy.obd.app.ObdTestActivity.Anon4());
        this.mOutStringBuffer = new java.lang.StringBuffer("");
    }

    public synchronized void onPause() {
        super.onPause();
        android.util.Log.e(TAG, "- ON PAUSE -");
    }

    public void onStop() {
        super.onStop();
        android.util.Log.e(TAG, "-- ON STOP --");
    }

    public void onDestroy() {
        super.onDestroy();
        android.util.Log.e(TAG, "--- ON DESTROY ---");
        try {
            this.obdApi.removeListener(this.obdListener);
            unbindService(this.serviceConnection);
        } catch (Throwable t) {
            android.util.Log.w(TAG, "Failed to disconnect from service", t);
        }
    }

    /* access modifiers changed from: private */
    public void sendMessage(java.lang.String message) {
        int state = 0;
        try {
            state = this.obdApi.getState();
        } catch (android.os.RemoteException e) {
            android.util.Log.w(TAG, "Failed to read service state");
        }
        if (state != 2) {
            com.navdy.obd.app.Utility.toastNotConnectedMessage(this);
        } else if (message.length() > 0) {
            try {
                if (message.startsWith(SCAN_CMD)) {
                    java.util.List<com.navdy.obd.Pid> pids = new java.util.ArrayList<>();
                    pids.add(new com.navdy.obd.Pid(13));
                    pids.add(new com.navdy.obd.Pid(5));
                    pids.add(new com.navdy.obd.Pid(12));
                    pids.add(new com.navdy.obd.Pid(16));
                    this.obdApi.scanPids(pids, 1);
                } else if (message.startsWith(RAW_CMD)) {
                    this.obdApi.startRawScan();
                } else if (message.startsWith(READ_CMD)) {
                    java.lang.String response = this.obdApi.readPid(java.lang.Integer.valueOf(message.substring(READ_CMD.length())).intValue());
                    if (response != null) {
                        byte[] bytes = response.getBytes();
                        this.mHandler.obtainMessage(2, bytes.length, -1, bytes).sendToTarget();
                    }
                } else if (message.equals(RESET_CMD)) {
                    this.obdApi.reset();
                } else if (message.equals(DISCONNECT_CMD)) {
                    this.obdApi.disconnect();
                } else {
                    android.util.Log.e(TAG, "Unknown command " + message);
                }
            } catch (android.os.RemoteException e2) {
                android.util.Log.e(TAG, "Failed to send " + message + " command");
            }
            this.mOutStringBuffer.setLength(0);
            this.mOutEditText.setText(this.mOutStringBuffer);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, android.content.Intent data) {
        android.util.Log.d(TAG, "onActivityResult " + resultCode);
        switch (requestCode) {
            case 1:
                if (resultCode == -1) {
                    java.lang.String address = data.getExtras().getString(com.navdy.obd.app.DeviceListActivity.EXTRA_DEVICE_ADDRESS);
                    android.util.Log.d(TAG, "Attempting to connect to " + address);
                    try {
                        this.obdApi.connect(address, this.obdListener);
                        return;
                    } catch (android.os.RemoteException e) {
                        android.util.Log.e(TAG, "Failed to add listener", e);
                        return;
                    }
                } else {
                    return;
                }
            case 2:
                if (resultCode == -1) {
                    setupChat();
                    return;
                }
                android.util.Log.d(TAG, "BT not enabled");
                android.widget.Toast.makeText(this, com.navdy.obd.app.R.string.bt_not_enabled_leaving, 0).show();
                finish();
                return;
            default:
                return;
        }
    }

    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        getMenuInflater().inflate(com.navdy.obd.app.R.menu.option_menu, menu);
        return D;
    }

    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        switch (item.getItemId()) {
            case com.navdy.obd.app.R.id.scan /*2131361810*/:
                startActivityForResult(new android.content.Intent(this, com.navdy.obd.app.DeviceListActivity.class), 1);
                return D;
            case com.navdy.obd.app.R.id.auto_connect /*2131361811*/:
                com.navdy.obd.ObdServiceInterface.startObdService(this);
                return D;
            case com.navdy.obd.app.R.id.upgrade /*2131361812*/:
                startActivity(new android.content.Intent(this, com.navdy.obd.app.UpgradeObdActivity.class));
                return D;
            case com.navdy.obd.app.R.id.flash /*2131361813*/:
                com.navdy.obd.app.UpgradeObdActivity.flash(this, D);
                return D;
            case com.navdy.obd.app.R.id.flash_upgrade /*2131361814*/:
                com.navdy.obd.app.UpgradeObdActivity.flash(this, false);
                return D;
            default:
                return false;
        }
    }
}
