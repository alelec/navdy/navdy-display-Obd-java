package com.navdy.obd.app;

public class DeviceListActivity extends android.app.Activity implements com.navdy.obd.discovery.ObdChannelScanner.Listener {
    private static final boolean D = true;
    public static java.lang.String EXTRA_DEVICE_ADDRESS = "device_address";
    private static final java.lang.String TAG = "DeviceListActivity";
    private java.util.HashSet<java.lang.String> discoveredDevices;
    private android.widget.AdapterView.OnItemClickListener mDeviceClickListener = new com.navdy.obd.app.DeviceListActivity.Anon2();
    private android.widget.ArrayAdapter<com.navdy.obd.io.ChannelInfo> mNewDevicesArrayAdapter;
    private android.widget.ArrayAdapter<com.navdy.obd.io.ChannelInfo> mPairedDevicesArrayAdapter;
    /* access modifiers changed from: private */
    public com.navdy.obd.discovery.GroupScanner scanner;

    class Anon1 implements android.view.View.OnClickListener {
        Anon1() {
        }

        public void onClick(android.view.View v) {
            com.navdy.obd.app.DeviceListActivity.this.doDiscovery();
            v.setVisibility(8);
        }
    }

    class Anon2 implements android.widget.AdapterView.OnItemClickListener {
        Anon2() {
        }

        public void onItemClick(android.widget.AdapterView<?> av, android.view.View v, int position, long id) {
            com.navdy.obd.app.DeviceListActivity.this.scanner.stopScan();
            com.navdy.obd.io.ChannelInfo info = (com.navdy.obd.io.ChannelInfo) av.getAdapter().getItem(position);
            android.content.Intent intent = new android.content.Intent();
            intent.putExtra(com.navdy.obd.app.DeviceListActivity.EXTRA_DEVICE_ADDRESS, info.asString());
            com.navdy.obd.app.DeviceListActivity.this.setResult(-1, intent);
            com.navdy.obd.app.DeviceListActivity.this.finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(5);
        setContentView(com.navdy.obd.app.R.layout.device_list);
        setResult(0);
        ((android.widget.Button) findViewById(com.navdy.obd.app.R.id.button_scan)).setOnClickListener(new com.navdy.obd.app.DeviceListActivity.Anon1());
        this.discoveredDevices = new java.util.HashSet<>();
        this.mPairedDevicesArrayAdapter = new android.widget.ArrayAdapter<>(this, com.navdy.obd.app.R.layout.device_name);
        this.mNewDevicesArrayAdapter = new android.widget.ArrayAdapter<>(this, com.navdy.obd.app.R.layout.device_name);
        android.widget.ListView pairedListView = (android.widget.ListView) findViewById(com.navdy.obd.app.R.id.paired_devices);
        pairedListView.setAdapter(this.mPairedDevicesArrayAdapter);
        pairedListView.setOnItemClickListener(this.mDeviceClickListener);
        android.widget.ListView newDevicesListView = (android.widget.ListView) findViewById(com.navdy.obd.app.R.id.new_devices);
        newDevicesListView.setAdapter(this.mNewDevicesArrayAdapter);
        newDevicesListView.setOnItemClickListener(this.mDeviceClickListener);
        this.scanner = new com.navdy.obd.discovery.GroupScanner(this, this);
        ((android.widget.ListView) findViewById(com.navdy.obd.app.R.id.paired_devices)).setEmptyView(findViewById(com.navdy.obd.app.R.id.paired_empty));
        ((android.widget.ListView) findViewById(com.navdy.obd.app.R.id.new_devices)).setEmptyView(findViewById(com.navdy.obd.app.R.id.empty));
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.scanner.stopScan();
    }

    /* access modifiers changed from: private */
    public void doDiscovery() {
        android.util.Log.d(TAG, "doDiscovery()");
        setProgressBarIndeterminateVisibility(D);
        setTitle(com.navdy.obd.app.R.string.scanning);
        findViewById(com.navdy.obd.app.R.id.title_new_devices).setVisibility(0);
        this.scanner.startScan();
    }

    public void onScanStarted(com.navdy.obd.discovery.ObdChannelScanner scanner2) {
    }

    public void onScanStopped(com.navdy.obd.discovery.ObdChannelScanner scanner2) {
        setProgressBarIndeterminateVisibility(false);
        setTitle(com.navdy.obd.app.R.string.select_device);
    }

    public void onDiscovered(com.navdy.obd.discovery.ObdChannelScanner scanner2, com.navdy.obd.io.ChannelInfo device) {
        if (!this.discoveredDevices.add(device.getAddress())) {
            return;
        }
        if (device.isBonded()) {
            if (this.mPairedDevicesArrayAdapter.getCount() == 0) {
                findViewById(com.navdy.obd.app.R.id.title_paired_devices).setVisibility(0);
            }
            this.mPairedDevicesArrayAdapter.add(device);
            return;
        }
        this.mNewDevicesArrayAdapter.add(device);
    }
}
