package com.navdy.obd;

public class InstantFuelConsumptionPidProcessor extends com.navdy.obd.PidProcessor {
    public static final int FUEL_STATUS_OPEN_LOOP = 4;
    private static final double MINIMUM_FUEL_CONSUMPTION_WHEN_DECELERATING = 2.35214d;
    public static final java.lang.String TAG = com.navdy.obd.VehicleStateManager.class.getSimpleName();
    private boolean mHasFuelSystemStatus;
    private boolean mHasThrottlePosition;
    private double mMinThrottlePosition = 100.0d;

    public boolean isSupported(com.navdy.obd.PidSet supportedPids) {
        this.mHasFuelSystemStatus = supportedPids.contains(3);
        this.mHasThrottlePosition = supportedPids.contains(17);
        return supportedPids.contains(16) && supportedPids.contains(13);
    }

    public boolean processPidValue(com.navdy.obd.PidLookupTable vehicleState) {
        double cons;
        double maf = vehicleState.getPidValue(16);
        double vehicleSpeed = vehicleState.getPidValue(13);
        if (!this.mHasFuelSystemStatus || !this.mHasThrottlePosition) {
            cons = com.navdy.obd.Utility.calculateInstantaneousFuelConsumption(maf, vehicleSpeed);
        } else {
            double throttlePosition = vehicleState.getPidValue(17);
            short fuelSystemStatus = (short) ((int) vehicleState.getPidValue(3));
            if (throttlePosition < this.mMinThrottlePosition && throttlePosition > 0.0d) {
                this.mMinThrottlePosition = throttlePosition;
            }
            if (throttlePosition >= this.mMinThrottlePosition || fuelSystemStatus != 4) {
                cons = com.navdy.obd.Utility.calculateInstantaneousFuelConsumption(maf, vehicleSpeed);
            } else {
                android.util.Log.d(TAG, "Vehicle is decelerating");
                cons = MINIMUM_FUEL_CONSUMPTION_WHEN_DECELERATING;
            }
        }
        return vehicleState.updatePid(256, cons);
    }

    public com.navdy.obd.PidSet resolveDependencies() {
        com.navdy.obd.PidSet requiredPids = new com.navdy.obd.PidSet();
        requiredPids.add(16);
        requiredPids.add(13);
        if (this.mHasFuelSystemStatus) {
            requiredPids.add(3);
        }
        if (this.mHasThrottlePosition) {
            requiredPids.add(17);
        }
        return requiredPids;
    }
}
