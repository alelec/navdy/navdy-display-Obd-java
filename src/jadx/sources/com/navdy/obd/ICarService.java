package com.navdy.obd;

public interface ICarService extends android.os.IInterface {

    public static abstract class Stub extends android.os.Binder implements com.navdy.obd.ICarService {
        private static final java.lang.String DESCRIPTOR = "com.navdy.obd.ICarService";
        static final int TRANSACTION_addListener = 6;
        static final int TRANSACTION_applyConfiguration = 10;
        static final int TRANSACTION_getBatteryVoltage = 13;
        static final int TRANSACTION_getConnectionState = 1;
        static final int TRANSACTION_getCurrentConfigurationName = 9;
        static final int TRANSACTION_getEcus = 3;
        static final int TRANSACTION_getMode = 21;
        static final int TRANSACTION_getObdChipFirmwareVersion = 19;
        static final int TRANSACTION_getProtocol = 4;
        static final int TRANSACTION_getReadings = 5;
        static final int TRANSACTION_getSupportedPids = 2;
        static final int TRANSACTION_getTroubleCodes = 27;
        static final int TRANSACTION_getVIN = 8;
        static final int TRANSACTION_isCheckEngineLightOn = 26;
        static final int TRANSACTION_isObdPidsScanningEnabled = 15;
        static final int TRANSACTION_removeListener = 7;
        static final int TRANSACTION_rescan = 12;
        static final int TRANSACTION_setCANBusMonitoringListener = 23;
        static final int TRANSACTION_setMode = 22;
        static final int TRANSACTION_setObdPidsScanningEnabled = 14;
        static final int TRANSACTION_setVoltageSettings = 18;
        static final int TRANSACTION_sleep = 16;
        static final int TRANSACTION_startCanBusMonitoring = 24;
        static final int TRANSACTION_stopCanBusMonitoring = 25;
        static final int TRANSACTION_updateFirmware = 20;
        static final int TRANSACTION_updateScan = 11;
        static final int TRANSACTION_wakeup = 17;

        private static class Proxy implements com.navdy.obd.ICarService {
            private android.os.IBinder mRemote;

            Proxy(android.os.IBinder remote) {
                this.mRemote = remote;
            }

            public android.os.IBinder asBinder() {
                return this.mRemote;
            }

            public java.lang.String getInterfaceDescriptor() {
                return com.navdy.obd.ICarService.Stub.DESCRIPTOR;
            }

            public int getConnectionState() throws android.os.RemoteException {
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.obd.ICarService.Stub.DESCRIPTOR);
                    this.mRemote.transact(1, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public java.util.List<com.navdy.obd.Pid> getSupportedPids() throws android.os.RemoteException {
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.obd.ICarService.Stub.DESCRIPTOR);
                    this.mRemote.transact(2, _data, _reply, 0);
                    _reply.readException();
                    return _reply.createTypedArrayList(com.navdy.obd.Pid.CREATOR);
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public java.util.List<com.navdy.obd.ECU> getEcus() throws android.os.RemoteException {
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.obd.ICarService.Stub.DESCRIPTOR);
                    this.mRemote.transact(3, _data, _reply, 0);
                    _reply.readException();
                    return _reply.createTypedArrayList(com.navdy.obd.ECU.CREATOR);
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public java.lang.String getProtocol() throws android.os.RemoteException {
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.obd.ICarService.Stub.DESCRIPTOR);
                    this.mRemote.transact(4, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readString();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public java.util.List<com.navdy.obd.Pid> getReadings(java.util.List<com.navdy.obd.Pid> pids) throws android.os.RemoteException {
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.obd.ICarService.Stub.DESCRIPTOR);
                    _data.writeTypedList(pids);
                    this.mRemote.transact(5, _data, _reply, 0);
                    _reply.readException();
                    return _reply.createTypedArrayList(com.navdy.obd.Pid.CREATOR);
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void addListener(java.util.List<com.navdy.obd.Pid> pids, com.navdy.obd.IPidListener listener) throws android.os.RemoteException {
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.obd.ICarService.Stub.DESCRIPTOR);
                    _data.writeTypedList(pids);
                    _data.writeStrongBinder(listener != null ? listener.asBinder() : null);
                    this.mRemote.transact(6, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void removeListener(com.navdy.obd.IPidListener listener) throws android.os.RemoteException {
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.obd.ICarService.Stub.DESCRIPTOR);
                    _data.writeStrongBinder(listener != null ? listener.asBinder() : null);
                    this.mRemote.transact(7, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public java.lang.String getVIN() throws android.os.RemoteException {
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.obd.ICarService.Stub.DESCRIPTOR);
                    this.mRemote.transact(8, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readString();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public java.lang.String getCurrentConfigurationName() throws android.os.RemoteException {
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.obd.ICarService.Stub.DESCRIPTOR);
                    this.mRemote.transact(9, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readString();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean applyConfiguration(java.lang.String configuration) throws android.os.RemoteException {
                boolean _result = false;
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.obd.ICarService.Stub.DESCRIPTOR);
                    _data.writeString(configuration);
                    this.mRemote.transact(10, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void updateScan(com.navdy.obd.ScanSchedule schedule, com.navdy.obd.IPidListener listener) throws android.os.RemoteException {
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.obd.ICarService.Stub.DESCRIPTOR);
                    if (schedule != null) {
                        _data.writeInt(1);
                        schedule.writeToParcel(_data, 0);
                    } else {
                        _data.writeInt(0);
                    }
                    _data.writeStrongBinder(listener != null ? listener.asBinder() : null);
                    this.mRemote.transact(11, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void rescan() throws android.os.RemoteException {
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.obd.ICarService.Stub.DESCRIPTOR);
                    this.mRemote.transact(12, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public double getBatteryVoltage() throws android.os.RemoteException {
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.obd.ICarService.Stub.DESCRIPTOR);
                    this.mRemote.transact(13, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readDouble();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void setObdPidsScanningEnabled(boolean enabled) throws android.os.RemoteException {
                int i = 0;
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.obd.ICarService.Stub.DESCRIPTOR);
                    if (enabled) {
                        i = 1;
                    }
                    _data.writeInt(i);
                    this.mRemote.transact(14, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean isObdPidsScanningEnabled() throws android.os.RemoteException {
                boolean _result = false;
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.obd.ICarService.Stub.DESCRIPTOR);
                    this.mRemote.transact(15, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void sleep(boolean deep) throws android.os.RemoteException {
                int i = 0;
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.obd.ICarService.Stub.DESCRIPTOR);
                    if (deep) {
                        i = 1;
                    }
                    _data.writeInt(i);
                    this.mRemote.transact(16, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void wakeup() throws android.os.RemoteException {
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.obd.ICarService.Stub.DESCRIPTOR);
                    this.mRemote.transact(17, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void setVoltageSettings(com.navdy.obd.VoltageSettings settings) throws android.os.RemoteException {
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.obd.ICarService.Stub.DESCRIPTOR);
                    if (settings != null) {
                        _data.writeInt(1);
                        settings.writeToParcel(_data, 0);
                    } else {
                        _data.writeInt(0);
                    }
                    this.mRemote.transact(18, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public java.lang.String getObdChipFirmwareVersion() throws android.os.RemoteException {
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.obd.ICarService.Stub.DESCRIPTOR);
                    this.mRemote.transact(19, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readString();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void updateFirmware(java.lang.String version, java.lang.String updateFilePath) throws android.os.RemoteException {
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.obd.ICarService.Stub.DESCRIPTOR);
                    _data.writeString(version);
                    _data.writeString(updateFilePath);
                    this.mRemote.transact(20, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getMode() throws android.os.RemoteException {
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.obd.ICarService.Stub.DESCRIPTOR);
                    this.mRemote.transact(21, _data, _reply, 0);
                    _reply.readException();
                    return _reply.readInt();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void setMode(int mode, boolean persistent) throws android.os.RemoteException {
                int i = 0;
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.obd.ICarService.Stub.DESCRIPTOR);
                    _data.writeInt(mode);
                    if (persistent) {
                        i = 1;
                    }
                    _data.writeInt(i);
                    this.mRemote.transact(22, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void setCANBusMonitoringListener(com.navdy.obd.ICanBusMonitoringListener listner) throws android.os.RemoteException {
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.obd.ICarService.Stub.DESCRIPTOR);
                    _data.writeStrongBinder(listner != null ? listner.asBinder() : null);
                    this.mRemote.transact(23, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void startCanBusMonitoring() throws android.os.RemoteException {
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.obd.ICarService.Stub.DESCRIPTOR);
                    this.mRemote.transact(24, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void stopCanBusMonitoring() throws android.os.RemoteException {
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.obd.ICarService.Stub.DESCRIPTOR);
                    this.mRemote.transact(25, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean isCheckEngineLightOn() throws android.os.RemoteException {
                boolean _result = false;
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.obd.ICarService.Stub.DESCRIPTOR);
                    this.mRemote.transact(com.navdy.obd.ICarService.Stub.TRANSACTION_isCheckEngineLightOn, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public java.util.List<java.lang.String> getTroubleCodes() throws android.os.RemoteException {
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.obd.ICarService.Stub.DESCRIPTOR);
                    this.mRemote.transact(27, _data, _reply, 0);
                    _reply.readException();
                    return _reply.createStringArrayList();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }
        }

        public Stub() {
            attachInterface(this, DESCRIPTOR);
        }

        public static com.navdy.obd.ICarService asInterface(android.os.IBinder obj) {
            if (obj == null) {
                return null;
            }
            android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
            if (iin == null || !(iin instanceof com.navdy.obd.ICarService)) {
                return new com.navdy.obd.ICarService.Stub.Proxy(obj);
            }
            return (com.navdy.obd.ICarService) iin;
        }

        public android.os.IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException {
            boolean _arg1;
            com.navdy.obd.VoltageSettings _arg0;
            boolean _arg02;
            boolean _arg03;
            com.navdy.obd.ScanSchedule _arg04;
            int i = 0;
            switch (code) {
                case 1:
                    data.enforceInterface(DESCRIPTOR);
                    int _result = getConnectionState();
                    reply.writeNoException();
                    reply.writeInt(_result);
                    return true;
                case 2:
                    data.enforceInterface(DESCRIPTOR);
                    java.util.List<com.navdy.obd.Pid> _result2 = getSupportedPids();
                    reply.writeNoException();
                    reply.writeTypedList(_result2);
                    return true;
                case 3:
                    data.enforceInterface(DESCRIPTOR);
                    java.util.List<com.navdy.obd.ECU> _result3 = getEcus();
                    reply.writeNoException();
                    reply.writeTypedList(_result3);
                    return true;
                case 4:
                    data.enforceInterface(DESCRIPTOR);
                    java.lang.String _result4 = getProtocol();
                    reply.writeNoException();
                    reply.writeString(_result4);
                    return true;
                case 5:
                    data.enforceInterface(DESCRIPTOR);
                    java.util.List<com.navdy.obd.Pid> _result5 = getReadings(data.createTypedArrayList(com.navdy.obd.Pid.CREATOR));
                    reply.writeNoException();
                    reply.writeTypedList(_result5);
                    return true;
                case 6:
                    data.enforceInterface(DESCRIPTOR);
                    addListener(data.createTypedArrayList(com.navdy.obd.Pid.CREATOR), com.navdy.obd.IPidListener.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 7:
                    data.enforceInterface(DESCRIPTOR);
                    removeListener(com.navdy.obd.IPidListener.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 8:
                    data.enforceInterface(DESCRIPTOR);
                    java.lang.String _result6 = getVIN();
                    reply.writeNoException();
                    reply.writeString(_result6);
                    return true;
                case 9:
                    data.enforceInterface(DESCRIPTOR);
                    java.lang.String _result7 = getCurrentConfigurationName();
                    reply.writeNoException();
                    reply.writeString(_result7);
                    return true;
                case 10:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result8 = applyConfiguration(data.readString());
                    reply.writeNoException();
                    if (_result8) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 11:
                    data.enforceInterface(DESCRIPTOR);
                    if (data.readInt() != 0) {
                        _arg04 = (com.navdy.obd.ScanSchedule) com.navdy.obd.ScanSchedule.CREATOR.createFromParcel(data);
                    } else {
                        _arg04 = null;
                    }
                    updateScan(_arg04, com.navdy.obd.IPidListener.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 12:
                    data.enforceInterface(DESCRIPTOR);
                    rescan();
                    reply.writeNoException();
                    return true;
                case 13:
                    data.enforceInterface(DESCRIPTOR);
                    double _result9 = getBatteryVoltage();
                    reply.writeNoException();
                    reply.writeDouble(_result9);
                    return true;
                case 14:
                    data.enforceInterface(DESCRIPTOR);
                    if (data.readInt() != 0) {
                        _arg03 = true;
                    } else {
                        _arg03 = false;
                    }
                    setObdPidsScanningEnabled(_arg03);
                    reply.writeNoException();
                    return true;
                case 15:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result10 = isObdPidsScanningEnabled();
                    reply.writeNoException();
                    if (_result10) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 16:
                    data.enforceInterface(DESCRIPTOR);
                    if (data.readInt() != 0) {
                        _arg02 = true;
                    } else {
                        _arg02 = false;
                    }
                    sleep(_arg02);
                    reply.writeNoException();
                    return true;
                case 17:
                    data.enforceInterface(DESCRIPTOR);
                    wakeup();
                    reply.writeNoException();
                    return true;
                case 18:
                    data.enforceInterface(DESCRIPTOR);
                    if (data.readInt() != 0) {
                        _arg0 = (com.navdy.obd.VoltageSettings) com.navdy.obd.VoltageSettings.CREATOR.createFromParcel(data);
                    } else {
                        _arg0 = null;
                    }
                    setVoltageSettings(_arg0);
                    reply.writeNoException();
                    return true;
                case 19:
                    data.enforceInterface(DESCRIPTOR);
                    java.lang.String _result11 = getObdChipFirmwareVersion();
                    reply.writeNoException();
                    reply.writeString(_result11);
                    return true;
                case 20:
                    data.enforceInterface(DESCRIPTOR);
                    updateFirmware(data.readString(), data.readString());
                    reply.writeNoException();
                    return true;
                case 21:
                    data.enforceInterface(DESCRIPTOR);
                    int _result12 = getMode();
                    reply.writeNoException();
                    reply.writeInt(_result12);
                    return true;
                case 22:
                    data.enforceInterface(DESCRIPTOR);
                    int _arg05 = data.readInt();
                    if (data.readInt() != 0) {
                        _arg1 = true;
                    } else {
                        _arg1 = false;
                    }
                    setMode(_arg05, _arg1);
                    reply.writeNoException();
                    return true;
                case 23:
                    data.enforceInterface(DESCRIPTOR);
                    setCANBusMonitoringListener(com.navdy.obd.ICanBusMonitoringListener.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 24:
                    data.enforceInterface(DESCRIPTOR);
                    startCanBusMonitoring();
                    reply.writeNoException();
                    return true;
                case 25:
                    data.enforceInterface(DESCRIPTOR);
                    stopCanBusMonitoring();
                    reply.writeNoException();
                    return true;
                case TRANSACTION_isCheckEngineLightOn /*26*/:
                    data.enforceInterface(DESCRIPTOR);
                    boolean _result13 = isCheckEngineLightOn();
                    reply.writeNoException();
                    if (_result13) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 27:
                    data.enforceInterface(DESCRIPTOR);
                    java.util.List<java.lang.String> _result14 = getTroubleCodes();
                    reply.writeNoException();
                    reply.writeStringList(_result14);
                    return true;
                case 1598968902:
                    reply.writeString(DESCRIPTOR);
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    void addListener(java.util.List<com.navdy.obd.Pid> list, com.navdy.obd.IPidListener iPidListener) throws android.os.RemoteException;

    boolean applyConfiguration(java.lang.String str) throws android.os.RemoteException;

    double getBatteryVoltage() throws android.os.RemoteException;

    int getConnectionState() throws android.os.RemoteException;

    java.lang.String getCurrentConfigurationName() throws android.os.RemoteException;

    java.util.List<com.navdy.obd.ECU> getEcus() throws android.os.RemoteException;

    int getMode() throws android.os.RemoteException;

    java.lang.String getObdChipFirmwareVersion() throws android.os.RemoteException;

    java.lang.String getProtocol() throws android.os.RemoteException;

    java.util.List<com.navdy.obd.Pid> getReadings(java.util.List<com.navdy.obd.Pid> list) throws android.os.RemoteException;

    java.util.List<com.navdy.obd.Pid> getSupportedPids() throws android.os.RemoteException;

    java.util.List<java.lang.String> getTroubleCodes() throws android.os.RemoteException;

    java.lang.String getVIN() throws android.os.RemoteException;

    boolean isCheckEngineLightOn() throws android.os.RemoteException;

    boolean isObdPidsScanningEnabled() throws android.os.RemoteException;

    void removeListener(com.navdy.obd.IPidListener iPidListener) throws android.os.RemoteException;

    void rescan() throws android.os.RemoteException;

    void setCANBusMonitoringListener(com.navdy.obd.ICanBusMonitoringListener iCanBusMonitoringListener) throws android.os.RemoteException;

    void setMode(int i, boolean z) throws android.os.RemoteException;

    void setObdPidsScanningEnabled(boolean z) throws android.os.RemoteException;

    void setVoltageSettings(com.navdy.obd.VoltageSettings voltageSettings) throws android.os.RemoteException;

    void sleep(boolean z) throws android.os.RemoteException;

    void startCanBusMonitoring() throws android.os.RemoteException;

    void stopCanBusMonitoring() throws android.os.RemoteException;

    void updateFirmware(java.lang.String str, java.lang.String str2) throws android.os.RemoteException;

    void updateScan(com.navdy.obd.ScanSchedule scanSchedule, com.navdy.obd.IPidListener iPidListener) throws android.os.RemoteException;

    void wakeup() throws android.os.RemoteException;
}
