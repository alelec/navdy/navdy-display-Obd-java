package com.navdy.obd;

public enum Units {
    NONE,
    MILES_PER_HOUR(com.navdy.obd.Units.System.US),
    KILOMETERS_PER_HOUR(com.navdy.obd.Units.System.Metric);
    
    protected java.lang.String abbreviation;
    protected java.lang.String description;
    protected com.navdy.obd.Units.System system;

    enum System {
        private static final /* synthetic */ com.navdy.obd.Units.System[] $VALUES = null;
        public static final com.navdy.obd.Units.System Metric = null;
        public static final com.navdy.obd.Units.System None = null;
        public static final com.navdy.obd.Units.System US = null;

        private System(java.lang.String str, int i) {
        }

        public static com.navdy.obd.Units.System valueOf(java.lang.String name) {
            return (com.navdy.obd.Units.System) java.lang.Enum.valueOf(com.navdy.obd.Units.System.class, name);
        }

        public static com.navdy.obd.Units.System[] values() {
            return (com.navdy.obd.Units.System[]) $VALUES.clone();
        }

        static {
            None = new com.navdy.obd.Units.System("None", 0);
            Metric = new com.navdy.obd.Units.System("Metric", 1);
            US = new com.navdy.obd.Units.System("US", 2);
            $VALUES = new com.navdy.obd.Units.System[]{None, Metric, US};
        }
    }

    private Units(com.navdy.obd.Units.System system2) {
        this(r7, r8, "", "", system2);
    }

    private Units(java.lang.String abbreviation2, java.lang.String description2, com.navdy.obd.Units.System system2) {
        this.abbreviation = abbreviation2;
        this.description = description2;
        this.system = system2;
    }

    public java.lang.String getAbbreviation() {
        return this.abbreviation;
    }

    public void setAbbreviation(java.lang.String abbreviation2) {
        this.abbreviation = abbreviation2;
    }

    public java.lang.String getDescription() {
        return this.description;
    }

    public void setDescription(java.lang.String description2) {
        this.description = description2;
    }

    public com.navdy.obd.Units.System getSystem() {
        return this.system;
    }

    static void localize(android.content.res.XmlResourceParser xpp) throws org.xmlpull.v1.XmlPullParserException, java.io.IOException {
        com.navdy.obd.Units currentUnit = null;
        java.lang.StringBuilder stringBuilder = new java.lang.StringBuilder();
        int eventType = xpp.getEventType();
        while (eventType != 1) {
            java.lang.String tagName = xpp.getName();
            if (eventType == 2) {
                if ("unit".equals(tagName)) {
                    currentUnit = valueOf(xpp.getAttributeValue(null, "id"));
                }
            } else if (eventType == 3) {
                if (currentUnit != null) {
                    java.lang.String tagValue = stringBuilder.toString();
                    if ("abbreviation".equals(tagName)) {
                        currentUnit.setAbbreviation(tagValue);
                    } else if ("description".equals(tagName)) {
                        currentUnit.setDescription(tagValue);
                    }
                }
                if ("unit".equals(tagName)) {
                    currentUnit = null;
                }
                stringBuilder.setLength(0);
            } else if (eventType == 4) {
                stringBuilder.append(xpp.getText());
            }
            eventType = xpp.next();
        }
    }
}
