package com.navdy.obd;

public class PidLookupTable {
    private com.navdy.obd.Pid[] mSnapshot;

    public PidLookupTable(int capacity) {
        this.mSnapshot = new com.navdy.obd.Pid[capacity];
    }

    public void build(java.util.List<com.navdy.obd.Pid> pids) {
        this.mSnapshot = new com.navdy.obd.Pid[this.mSnapshot.length];
        for (com.navdy.obd.Pid pid : pids) {
            if (pid.getId() < this.mSnapshot.length) {
                this.mSnapshot[pid.getId()] = pid;
            }
        }
    }

    public com.navdy.obd.Pid getPid(int id) {
        if (validId(id)) {
            return this.mSnapshot[id];
        }
        return null;
    }

    private boolean validId(int id) {
        return id >= 0 && id < this.mSnapshot.length;
    }

    public double getPidValue(int id) {
        com.navdy.obd.Pid pid = getPid(id);
        if (pid != null) {
            return pid.getValue();
        }
        return -1.0d;
    }

    public boolean updatePid(int id, double value) {
        com.navdy.obd.Pid pid = getPid(id);
        boolean changed = false;
        if (pid != null) {
            changed = pid.getValue() != value;
            pid.setValue(value);
        }
        return changed;
    }
}
