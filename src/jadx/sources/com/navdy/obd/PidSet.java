package com.navdy.obd;

public class PidSet implements android.os.Parcelable {
    public static final android.os.Parcelable.Creator<com.navdy.obd.PidSet> CREATOR = new com.navdy.obd.PidSet.Anon1();
    public static final int MAX_PID = 320;
    private java.util.BitSet pids;

    static class Anon1 implements android.os.Parcelable.Creator<com.navdy.obd.PidSet> {
        Anon1() {
        }

        public com.navdy.obd.PidSet createFromParcel(android.os.Parcel source) {
            return new com.navdy.obd.PidSet(source);
        }

        public com.navdy.obd.PidSet[] newArray(int size) {
            return new com.navdy.obd.PidSet[size];
        }
    }

    public PidSet() {
        this.pids = new java.util.BitSet(320);
    }

    public PidSet(long bitField, int offset) {
        this();
        for (int bit = 0; bit < 32; bit++) {
            if ((bitField & ((long) (1 << (31 - bit)))) != 0) {
                this.pids.set(offset + bit + 1);
            }
        }
    }

    public PidSet(com.navdy.obd.ScanSchedule schedule) {
        this();
        for (java.util.Map.Entry<java.lang.Integer, com.navdy.obd.ScanSchedule.Scan> entry : schedule.schedule.entrySet()) {
            this.pids.set(((com.navdy.obd.ScanSchedule.Scan) entry.getValue()).pid);
        }
    }

    public PidSet(java.util.List<com.navdy.obd.Pid> pids2) {
        this();
        for (com.navdy.obd.Pid pid : pids2) {
            this.pids.set(pid.getId());
        }
    }

    public void add(com.navdy.obd.Pid pid) {
        add(pid.getId());
    }

    public void remove(int id) {
        this.pids.clear(id);
    }

    public void add(int id) {
        this.pids.set(id);
    }

    public boolean contains(com.navdy.obd.Pid pid) {
        return this.pids.get(pid.getId());
    }

    public boolean contains(int pidId) {
        return this.pids.get(pidId);
    }

    public void merge(com.navdy.obd.PidSet pidSet) {
        this.pids.or(pidSet.pids);
    }

    public void clear() {
        this.pids.clear();
    }

    public boolean isEmpty() {
        return this.pids.isEmpty();
    }

    public int size() {
        return this.pids.cardinality();
    }

    public java.util.List<com.navdy.obd.Pid> asList() {
        java.util.List<com.navdy.obd.Pid> list = new java.util.ArrayList<>(this.pids.cardinality());
        int index = 0;
        while (index < 320 && index != -1) {
            index = this.pids.nextSetBit(index);
            if (index != -1) {
                list.add(new com.navdy.obd.Pid(index));
                index++;
            }
        }
        return list;
    }

    public int nextPid(int id) {
        return this.pids.nextSetBit(id);
    }

    public int describeContents() {
        return 0;
    }

    public PidSet(android.os.Parcel in) {
        long[] bits = new long[in.readByte()];
        in.readLongArray(bits);
        this.pids = java.util.BitSet.valueOf(bits);
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        long[] bits = this.pids.toLongArray();
        dest.writeByte((byte) bits.length);
        dest.writeLongArray(bits);
    }

    public boolean equals(java.lang.Object o) {
        return (o instanceof com.navdy.obd.PidSet) && this.pids.equals(((com.navdy.obd.PidSet) o).pids);
    }
}
