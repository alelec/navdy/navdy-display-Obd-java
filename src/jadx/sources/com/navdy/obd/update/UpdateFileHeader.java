package com.navdy.obd.update;

public class UpdateFileHeader {
    public static final int IMAGE_TYPE_VALIDATION = 16;
    com.navdy.obd.update.UpdateFileHeader.FWImageDescriptor[] fwImageDescriptors;
    short fwImageDescriptorsCount;
    int version;

    public static class FWImageDescriptor {
        short errorFwIndex;
        long imageOffset;
        long imageSize;
        short imageType;
        short nextFwIndex;
    }
}
