package com.navdy.obd.update;

public class STNBootloaderChannel {
    public static final int ACK = 64;
    public static final byte[] CONNECT_BOOTLOADER_COMMAND = {3, 0, 0};
    public static final int DEFAULT_CHUNK_SIZE = 5120;
    public static final byte DLE = 5;
    public static final byte ERROR_CODE_CRC_FAILED = -2;
    public static final byte ERROR_CODE_INVALID_RESPONSE = -1;
    public static final byte ERROR_CODE_IO_TIMED_OUT = -3;
    public static final byte ERROR_CODE_VALIDATION_ERROR = -112;
    public static final byte ETX = 4;
    public static final byte[] GET_BOOTLOADER_VERSION_COMMAND = {6, 0, 0};
    public static final byte[] GET_FIRMWARE_STATUS_BOOTLOADER_COMMAND = {15, 0, 0};
    public static final int INDEX_COMMAND = 0;
    public static final int INDEX_DATA = 2;
    public static final int INDEX_DATA_LENGTH = 1;
    public static final int MAX_RETRIES = 10;
    public static final int MINIMUM_CHUNK_SIZE = 16;
    public static final byte[] RESET_BOOTLOADER_COMMAND = {2, 0, 0};
    public static final byte SEND_CHUNK_COMMAND = 49;
    public static final byte[] START_UPLOAD_COMMAND = {48, 0, 4, 0, 0, 0, 1};
    public static final byte STX = 85;
    private static final java.lang.String TAG = com.navdy.obd.update.STNBootloaderChannel.class.getSimpleName();
    private byte[] mChunkBuffer;
    private byte[] mPacketBuffer;
    private com.navdy.hardware.SerialPort mSerialPort;

    static class BootLoaderMessageError extends java.lang.Error {
        short errorCode;

        public BootLoaderMessageError(short errorCode2) {
            super("NACK");
            this.errorCode = errorCode2;
        }

        public java.lang.String toString() {
            java.lang.String errorMessage = "Bootloader error";
            switch (this.errorCode) {
                case -2:
                    errorMessage = "Invalid CRC in the response";
                    break;
                case -1:
                    errorMessage = "Invalid response from bootloader";
                    break;
            }
            return "Bootloader E: <" + this.errorCode + ">," + errorMessage;
        }
    }

    public STNBootloaderChannel(com.navdy.hardware.SerialPort port) {
        this.mSerialPort = port;
    }

    public boolean enterBootloaderMode() throws java.io.IOException {
        clearBuffers();
        try {
            write(CONNECT_BOOTLOADER_COMMAND);
            byte[] read = read();
            android.util.Log.e(TAG, "Entered boot mode");
            return true;
        } catch (com.navdy.obd.update.STNBootloaderChannel.BootLoaderMessageError er) {
            android.util.Log.e(TAG, er.toString());
            return false;
        }
    }

    public byte[] getVersion() throws com.navdy.obd.update.STNBootloaderChannel.BootLoaderMessageError, java.lang.Exception {
        write(GET_BOOTLOADER_VERSION_COMMAND);
        return read();
    }

    public boolean getFirmwareStatus() throws com.navdy.obd.update.STNBootloaderChannel.BootLoaderMessageError, java.lang.Exception {
        write(GET_FIRMWARE_STATUS_BOOTLOADER_COMMAND);
        byte[] data = read();
        return (data == null || data.length <= 0 || data[2] == 0) ? false : true;
    }

    public int startUpload(int imageSize) throws java.io.IOException, com.navdy.obd.update.STNBootloaderChannel.BootLoaderMessageError {
        byte[] command = new byte[START_UPLOAD_COMMAND.length];
        java.lang.System.arraycopy(START_UPLOAD_COMMAND, 0, command, 0, START_UPLOAD_COMMAND.length);
        int imageSize2 = imageSize & android.support.v4.view.ViewCompat.MEASURED_SIZE_MASK;
        command[3] = (byte) ((imageSize2 >> 16) & 255);
        command[4] = (byte) ((imageSize2 >> 8) & 255);
        command[5] = (byte) (imageSize2 & 255);
        write(command);
        return parseInt(read(), 2);
    }

    public int sendChunk(int chunkNum, byte[] chunk) throws com.navdy.obd.update.STNBootloaderChannel.BootLoaderMessageError, java.lang.Exception {
        int chunkLen = chunk.length;
        int dataLength = chunkLen + 5;
        if (this.mChunkBuffer == null || this.mChunkBuffer.length != dataLength) {
            this.mChunkBuffer = new byte[dataLength];
        }
        java.nio.ByteBuffer buffer = java.nio.ByteBuffer.wrap(this.mChunkBuffer);
        buffer.put(SEND_CHUNK_COMMAND);
        buffer.putShort((short) ((chunkLen + 2) & android.support.v4.internal.view.SupportMenu.USER_MASK));
        buffer.putShort((short) (chunkNum & android.support.v4.internal.view.SupportMenu.USER_MASK));
        buffer.put(chunk);
        buffer.flip();
        write(this.mChunkBuffer);
        return parseInt(read(), 2);
    }

    public boolean reset() throws java.lang.Exception, com.navdy.obd.update.STNBootloaderChannel.BootLoaderMessageError {
        clearBuffers();
        write(RESET_BOOTLOADER_COMMAND);
        return true;
    }

    public void write(byte[] data) throws java.io.IOException {
        if (data != null && data.length > 0) {
            int packetLength = (data.length * 2) + 5;
            if (this.mPacketBuffer == null || this.mPacketBuffer.length != packetLength) {
                this.mPacketBuffer = new byte[packetLength];
            }
            java.nio.ByteBuffer byteBuffer = java.nio.ByteBuffer.wrap(this.mPacketBuffer);
            byteBuffer.put(STX);
            byteBuffer.put(STX);
            for (byte input : data) {
                if (input == 85 || input == 5 || input == 4) {
                    byteBuffer.put(5);
                    byteBuffer.put(input);
                } else {
                    byteBuffer.put(input);
                }
            }
            byte[] crcBytes = crc(data);
            if (crcBytes[0] == 85 || crcBytes[0] == 5 || crcBytes[0] == 4) {
                byteBuffer.put(5);
            }
            byteBuffer.put(crcBytes[0]);
            if (crcBytes[1] == 85 || crcBytes[1] == 5 || crcBytes[1] == 4) {
                byteBuffer.put(5);
            }
            byteBuffer.put(crcBytes[1]);
            byteBuffer.put(4);
            byteBuffer.flip();
            this.mSerialPort.write(byteBuffer, byteBuffer.limit());
        }
    }

    public byte[] read() throws java.io.IOException {
        try {
            byte firstByte = readByte();
            byte secondByte = readByte();
            android.util.Log.d(TAG, "First byte :" + firstByte + ", Second byte :" + secondByte);
            if (firstByte == 85 && secondByte == 85) {
                java.nio.ByteBuffer output = java.nio.ByteBuffer.allocate(261);
                boolean escaped = false;
                while (true) {
                    byte b = readByte();
                    if (b == 4 && !escaped) {
                        break;
                    } else if (b != 5 || escaped) {
                        escaped = false;
                        output.put(b);
                    } else {
                        escaped = true;
                    }
                }
                output.flip();
                short command = (short) (output.get() & ERROR_CODE_INVALID_RESPONSE);
                android.util.Log.d(TAG, "Command " + command);
                if (command <= 0 || (command & 64) == 0) {
                    android.util.Log.d(TAG, "Command NACK");
                    short s = (short) output.get();
                    short errorCode = (short) (output.get() & ERROR_CODE_INVALID_RESPONSE);
                    android.util.Log.d(TAG, "Error code " + errorCode);
                    throw new com.navdy.obd.update.STNBootloaderChannel.BootLoaderMessageError(errorCode);
                }
                android.util.Log.d(TAG, "Command ACK");
                short dataLength = (short) (output.get() & ERROR_CODE_INVALID_RESPONSE);
                android.util.Log.d(TAG, "Data length " + dataLength);
                if (dataLength == 0) {
                    return null;
                }
                byte[] outputData = new byte[(dataLength + 2)];
                outputData[0] = (byte) (command & 255);
                outputData[1] = (byte) (dataLength & 255);
                output.get(outputData, 2, dataLength);
                byte crcH = output.get();
                byte crcL = output.get();
                byte[] dataCrc = crc(outputData);
                if (dataCrc[0] == crcH || dataCrc[1] == crcL) {
                    return outputData;
                }
                throw new com.navdy.obd.update.STNBootloaderChannel.BootLoaderMessageError(-2);
            }
            throw new com.navdy.obd.update.STNBootloaderChannel.BootLoaderMessageError(-1);
        } catch (java.util.concurrent.TimeoutException e) {
            android.util.Log.d(TAG, "IO Timed out");
            throw new com.navdy.obd.update.STNBootloaderChannel.BootLoaderMessageError(-3);
        }
    }

    private int parseInt(byte[] data, int startOffset) {
        return ((0 | ((short) (data[startOffset] & ERROR_CODE_INVALID_RESPONSE))) << 8) | ((short) (data[startOffset + 1] & ERROR_CODE_INVALID_RESPONSE));
    }

    private byte readByte() throws java.io.IOException, java.util.concurrent.TimeoutException {
        int tries = 0;
        boolean interrupted = false;
        do {
            java.nio.ByteBuffer buffer = java.nio.ByteBuffer.allocate(1);
            int result = this.mSerialPort.read(buffer);
            if (result < 0) {
                throw new java.io.IOException("EOF");
            } else if (result == 0) {
                android.util.Log.d(TAG, "Read timed out, retrying");
                tries++;
                if (tries >= 10) {
                    break;
                }
                interrupted = java.lang.Thread.interrupted();
            } else {
                return (byte) (buffer.get() & ERROR_CODE_INVALID_RESPONSE);
            }
        } while (!interrupted);
        if (interrupted) {
            throw new java.io.InterruptedIOException();
        }
        throw new java.util.concurrent.TimeoutException();
    }

    private byte[] crc(byte[] data) {
        int crc = com.navdy.obd.Utility.calculateCcittCrc(data);
        byte[] crcBytes = new byte[2];
        crcBytes[1] = (byte) (crc & 255);
        crcBytes[0] = (byte) ((crc >> 8) & 255);
        return crcBytes;
    }

    private void clearBuffers() {
        this.mChunkBuffer = null;
        this.mPacketBuffer = null;
    }
}
