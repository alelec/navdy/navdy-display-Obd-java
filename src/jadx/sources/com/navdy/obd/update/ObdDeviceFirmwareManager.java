package com.navdy.obd.update;

public interface ObdDeviceFirmwareManager {
    java.lang.String getFirmwareVersion();

    boolean isUpdatingFirmware();

    boolean updateFirmware(com.navdy.obd.update.Update update);
}
