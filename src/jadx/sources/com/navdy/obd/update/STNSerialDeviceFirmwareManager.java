package com.navdy.obd.update;

public class STNSerialDeviceFirmwareManager implements com.navdy.obd.update.ObdDeviceFirmwareManager {
    public static final byte BOOTLOADER_MAJOR_VERSION = 2;
    private static final int CARRIAGE_RETURN = 13;
    public static final java.lang.String CURRENT_UPDATE_FILE_VERSION = "05";
    private static final long DELAY_AFTER_RESET = 150;
    public static final java.lang.String FILE_SIGNATURE = "STNFWv";
    public static final java.util.regex.Pattern FIRMWARE_VERSION_PATTERN = java.util.regex.Pattern.compile("^STN(.+)\\s{1}v(.+)");
    public static final java.lang.String INFO_COMMAND = "sti";
    public static final int MAX_RETRIES_TO_CONNECT = 10;
    public static final java.lang.String RESET_COMMAND = "atz";
    public static final java.lang.String TAG = com.navdy.obd.update.STNSerialDeviceFirmwareManager.class.getSimpleName();
    public static final int UPDATE_HEADER_SIGNATURE_SIZE = 6;
    private com.navdy.obd.io.IChannel mChannel;
    private java.util.concurrent.atomic.AtomicBoolean mIsUpdating = new java.util.concurrent.atomic.AtomicBoolean(false);
    private com.navdy.obd.ObdService mObdService;

    public STNSerialDeviceFirmwareManager(com.navdy.obd.ObdService obdService, com.navdy.obd.io.IChannel channel) {
        init(obdService, channel);
    }

    public void init(com.navdy.obd.ObdService obdService, com.navdy.obd.io.IChannel channel) {
        this.mObdService = obdService;
        this.mChannel = channel;
    }

    public boolean updateFirmware(com.navdy.obd.update.Update update) {
        android.util.Log.d(TAG, "updateFirmware  : " + this.mChannel);
        if (this.mChannel == null || !(this.mChannel instanceof com.navdy.obd.io.STNSerialChannel)) {
            return false;
        }
        return update(update, ((com.navdy.obd.io.STNSerialChannel) this.mChannel).getSerialPort(), true);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:112:?, code lost:
        r35 = r5.getFirmwareStatus();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:113:0x0415, code lost:
        if (r35 == false) goto L_0x042e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:114:0x0417, code lost:
        android.util.Log.d(TAG, "After uploading the firmware is valid");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:115:0x041e, code lost:
        updateStopped(true, r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:116:0x0427, code lost:
        updateStopped(r15, r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:118:?, code lost:
        android.util.Log.d(TAG, "Invalid firmware, needs re installation");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:141:?, code lost:
        return r35;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x02d0, code lost:
        android.util.Log.d(TAG, "Sending chunk " + r8 + " failed, id mismatch :" + r10);
        updateStopped(true, r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:?, code lost:
        r0.close();
     */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x037b A[SYNTHETIC, Splitter:B:87:0x037b] */
    public boolean update(com.navdy.obd.update.Update update, com.navdy.hardware.SerialPort serialPort, boolean isInValidState) {
        com.navdy.obd.update.STNBootloaderChannel bootloaderChannel;
        boolean enteredBootMode;
        if (this.mIsUpdating.compareAndSet(false, true)) {
            com.navdy.obd.update.UpdateFileHeader headerFile = ensureFile(update.mUpdateFilePath);
            if (headerFile == null) {
                this.mIsUpdating.set(false);
                return false;
            }
            bootloaderChannel = new com.navdy.obd.update.STNBootloaderChannel(serialPort);
            enteredBootMode = false;
            java.io.FileInputStream reader = null;
            if (isInValidState) {
                try {
                    java.io.OutputStream out = this.mChannel.getOutputStream();
                    java.lang.String command = RESET_COMMAND;
                    int length = command.length();
                    byte[] bytes = new byte[(length + 1)];
                    java.lang.System.arraycopy(command.getBytes(), 0, bytes, 0, length);
                    bytes[length] = 13;
                    out.write(bytes);
                    out.flush();
                    java.lang.Thread.sleep(DELAY_AFTER_RESET);
                } catch (java.io.IOException e) {
                    android.util.Log.e(TAG, "Error closing the file reader" + e.getMessage());
                    e.printStackTrace();
                } catch (Throwable th) {
                    t = th;
                }
            }
            int tries = 0;
            while (!enteredBootMode && tries < 10) {
                tries++;
                enteredBootMode = bootloaderChannel.enterBootloaderMode();
                android.util.Log.d(TAG, "Enter boot mode , Success : " + enteredBootMode + ", Tries : " + tries);
            }
            if (!enteredBootMode) {
                android.util.Log.e(TAG, "Failed to enter boot mode, aborting update");
                updateStopped(false, bootloaderChannel);
                updateStopped(enteredBootMode, bootloaderChannel);
                return false;
            }
            byte[] version = bootloaderChannel.getVersion();
            android.util.Log.d(TAG, "Firmware status " + bootloaderChannel.getFirmwareStatus());
            byte majorVersion = version[2];
            android.util.Log.e(TAG, "Major version " + version[2] + ", Minor version " + version[3]);
            if (majorVersion != 2) {
                android.util.Log.e(TAG, "The bootloader version is not supported" + majorVersion);
                updateStopped(true, bootloaderChannel);
                updateStopped(enteredBootMode, bootloaderChannel);
                return false;
            }
            short s = headerFile.fwImageDescriptorsCount;
            short nextFwImageIndex = 0;
            loop1:
            while (true) {
                java.io.FileInputStream reader2 = reader;
                if (nextFwImageIndex == 255 || nextFwImageIndex < 0 || nextFwImageIndex >= s) {
                    break;
                }
                try {
                    android.util.Log.d(TAG, "Uploading fw image" + nextFwImageIndex);
                    com.navdy.obd.update.UpdateFileHeader.FWImageDescriptor desc = headerFile.fwImageDescriptors[nextFwImageIndex];
                    nextFwImageIndex = desc.nextFwIndex;
                    int imageSize = (int) (desc.imageSize & 16777215);
                    android.util.Log.d(TAG, "Image size " + imageSize);
                    int maxChunkSize = bootloaderChannel.startUpload(imageSize);
                    android.util.Log.d(TAG, "Max chunk size " + maxChunkSize);
                    if (maxChunkSize > 0) {
                        int chunkSize = java.lang.Math.min(com.navdy.obd.update.STNBootloaderChannel.DEFAULT_CHUNK_SIZE, maxChunkSize);
                        int chunkCount = imageSize / chunkSize;
                        int fullChunks = chunkCount;
                        int chunkCount2 = chunkCount + ((imageSize % chunkSize) / 16);
                        android.util.Log.d(TAG, "Total chunk count : " + chunkCount2);
                        java.io.File file = new java.io.File(update.mUpdateFilePath);
                        long offset = desc.imageOffset;
                        reader = new java.io.FileInputStream(file);
                        reader.skip(offset);
                        try {
                            byte[] buffer = new byte[(fullChunks > 0 ? chunkSize : 16)];
                            int chunk = 0;
                            while (chunk < chunkCount2) {
                                int size = chunkSize;
                                if (chunk >= fullChunks) {
                                    size = 16;
                                }
                                android.util.Log.d(TAG, "Writing chunk " + chunk + ", Size : " + size);
                                if (buffer.length != size) {
                                    buffer = new byte[size];
                                }
                                reader.read(buffer);
                                int chunkId = bootloaderChannel.sendChunk(chunk, buffer);
                                if (chunk != chunkId) {
                                    break loop1;
                                }
                                android.util.Log.d(TAG, "Successfully sent chunk " + chunkId);
                                chunk++;
                            }
                            if (reader != null) {
                                try {
                                    reader.close();
                                } catch (java.io.IOException e2) {
                                    android.util.Log.e(TAG, "Error closing the file reader" + e2.getMessage());
                                    e2.printStackTrace();
                                }
                            }
                        } catch (com.navdy.obd.update.STNBootloaderChannel.BootLoaderMessageError be) {
                            android.util.Log.e(TAG, "Error response from bootloader while sending chunk " + be);
                            if (desc.imageType == 16 && be.errorCode == -112) {
                                nextFwImageIndex = desc.errorFwIndex;
                            } else {
                                updateStopped(true, bootloaderChannel);
                                updateStopped(enteredBootMode, bootloaderChannel);
                                return false;
                            }
                        } finally {
                            if (reader != null) {
                                try {
                                    reader.close();
                                } catch (java.io.IOException e3) {
                                    android.util.Log.e(TAG, "Error closing the file reader" + e3.getMessage());
                                    e3.printStackTrace();
                                }
                            }
                        }
                    } else {
                        android.util.Log.e(TAG, "Start upload command failed, bootloader returned invalid max chunk size");
                        updateStopped(true, bootloaderChannel);
                        updateStopped(enteredBootMode, bootloaderChannel);
                        return false;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    java.io.FileInputStream fileInputStream = reader2;
                    updateStopped(enteredBootMode, bootloaderChannel);
                    throw th;
                }
            }
            updateStopped(true, bootloaderChannel);
            if (reader != null) {
                try {
                    reader.close();
                } catch (java.io.IOException e4) {
                    android.util.Log.e(TAG, "Error closing the file reader" + e4.getMessage());
                    e4.printStackTrace();
                }
            }
            updateStopped(enteredBootMode, bootloaderChannel);
            return false;
        }
        return false;
        try {
            android.util.Log.e(TAG, "Unhandled error during update process " + t);
            t.printStackTrace();
            updateStopped(enteredBootMode, bootloaderChannel);
            return false;
        } catch (Throwable th3) {
            th = th3;
            updateStopped(enteredBootMode, bootloaderChannel);
            throw th;
        }
        updateStopped(enteredBootMode, bootloaderChannel);
        return false;
    }

    private void updateStopped(boolean isInBootMode, com.navdy.obd.update.STNBootloaderChannel bootloaderChannel) {
        if (this.mIsUpdating.get()) {
            if (isInBootMode && bootloaderChannel != null) {
                try {
                    bootloaderChannel.reset();
                } catch (Throwable e) {
                    android.util.Log.e(TAG, "Exception while resetting the device");
                    e.printStackTrace();
                }
            }
            this.mIsUpdating.set(false);
        }
    }

    public boolean isUpdatingFirmware() {
        return this.mIsUpdating.get();
    }

    public java.lang.String getFirmwareVersion() {
        try {
            java.lang.String versionResponse = this.mObdService.sendCommand(INFO_COMMAND);
            if (!android.text.TextUtils.isEmpty(versionResponse)) {
                java.util.regex.Matcher matcher = FIRMWARE_VERSION_PATTERN.matcher(versionResponse);
                if (!matcher.find() || matcher.groupCount() != 2) {
                    return versionResponse;
                }
                java.lang.String compactVersion = matcher.group(2);
                java.lang.String group = matcher.group(0);
                android.util.Log.d(TAG, "STN device :" + matcher.group(1) + ", firmware :" + compactVersion);
                return compactVersion;
            }
        } catch (android.os.RemoteException e) {
            android.util.Log.e(TAG, "Exception " + e.getCause());
        }
        return null;
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:51:0x0254=Splitter:B:51:0x0254, B:59:0x027f=Splitter:B:59:0x027f} */
    private com.navdy.obd.update.UpdateFileHeader ensureFile(java.lang.String filePath) {
        java.io.File file = new java.io.File(filePath);
        if (!file.exists() || !file.isFile() || !file.canRead()) {
            return null;
        }
        java.io.FileInputStream fis = null;
        try {
            java.io.FileInputStream fis2 = new java.io.FileInputStream(file);
            try {
                java.io.DataInputStream dis = new java.io.DataInputStream(fis2);
                com.navdy.obd.update.UpdateFileHeader updateFileHeader = new com.navdy.obd.update.UpdateFileHeader();
                byte[] signatureData = new byte[6];
                dis.read(signatureData, 0, 6);
                java.lang.String signature = new java.lang.String(signatureData);
                android.util.Log.d(TAG, "Signature : " + signature);
                if (!signature.equals(FILE_SIGNATURE)) {
                    try {
                        fis2.close();
                        return null;
                    } catch (java.io.IOException e) {
                        android.util.Log.e(TAG, "Error closing the file input stream");
                        return null;
                    }
                } else {
                    byte[] versionData = new byte[2];
                    dis.read(versionData, 0, 2);
                    java.lang.String version = new java.lang.String(versionData);
                    android.util.Log.d(TAG, "File version : " + version);
                    if (!version.equals(CURRENT_UPDATE_FILE_VERSION)) {
                        try {
                            fis2.close();
                            return null;
                        } catch (java.io.IOException e2) {
                            android.util.Log.e(TAG, "Error closing the file input stream");
                            return null;
                        }
                    } else {
                        short deviceIdCount = (short) dis.readByte();
                        boolean stnDeviceFound = false;
                        for (short i = 0; i < deviceIdCount; i = (short) (i + 1)) {
                            if ((dis.readShort() & android.support.v4.internal.view.SupportMenu.USER_MASK) == 4368) {
                                stnDeviceFound = true;
                            }
                        }
                        if (!stnDeviceFound) {
                            android.util.Log.d(TAG, "Update not compatible with our Obd Chip");
                            try {
                                fis2.close();
                                return null;
                            } catch (java.io.IOException e3) {
                                android.util.Log.e(TAG, "Error closing the file input stream");
                                return null;
                            }
                        } else {
                            short imageDescriptorCount = (short) dis.readByte();
                            updateFileHeader.fwImageDescriptorsCount = imageDescriptorCount;
                            android.util.Log.d(TAG, "Number of image descriptors : " + updateFileHeader.fwImageDescriptorsCount);
                            com.navdy.obd.update.UpdateFileHeader.FWImageDescriptor[] descriptors = new com.navdy.obd.update.UpdateFileHeader.FWImageDescriptor[imageDescriptorCount];
                            for (short i2 = 0; i2 < imageDescriptorCount; i2 = (short) (i2 + 1)) {
                                android.util.Log.d(TAG, "---------------------------------------------\n\n");
                                com.navdy.obd.update.UpdateFileHeader.FWImageDescriptor descriptor = new com.navdy.obd.update.UpdateFileHeader.FWImageDescriptor();
                                descriptor.imageType = (short) (dis.readByte() & com.navdy.obd.update.STNBootloaderChannel.ERROR_CODE_INVALID_RESPONSE);
                                android.util.Log.d(TAG, "ImageType : " + descriptor.imageType);
                                short readByte = (short) dis.readByte();
                                descriptor.nextFwIndex = (short) (dis.readByte() & com.navdy.obd.update.STNBootloaderChannel.ERROR_CODE_INVALID_RESPONSE);
                                android.util.Log.d(TAG, "Next FW index : " + descriptor.nextFwIndex);
                                descriptor.errorFwIndex = (short) (dis.readByte() & com.navdy.obd.update.STNBootloaderChannel.ERROR_CODE_INVALID_RESPONSE);
                                android.util.Log.d(TAG, "Error FW index : " + descriptor.errorFwIndex);
                                descriptor.imageOffset = (long) dis.readInt();
                                android.util.Log.d(TAG, "Image offset : " + descriptor.imageOffset);
                                descriptor.imageSize = (long) dis.readInt();
                                android.util.Log.d(TAG, "Image size : " + descriptor.imageSize);
                                descriptors[i2] = descriptor;
                            }
                            updateFileHeader.fwImageDescriptors = descriptors;
                            try {
                                fis2.close();
                                return updateFileHeader;
                            } catch (java.io.IOException e4) {
                                android.util.Log.e(TAG, "Error closing the file input stream");
                                return updateFileHeader;
                            }
                        }
                    }
                }
            } catch (java.io.FileNotFoundException e5) {
                ie = e5;
                fis = fis2;
            } catch (java.lang.Exception e6) {
                e = e6;
                fis = fis2;
                android.util.Log.e(TAG, "Error while reading from update file " + e);
                try {
                    fis.close();
                } catch (java.io.IOException e7) {
                    android.util.Log.e(TAG, "Error closing the file input stream");
                }
                return null;
            } catch (Throwable th) {
                th = th;
                fis = fis2;
                try {
                    fis.close();
                } catch (java.io.IOException e8) {
                    android.util.Log.e(TAG, "Error closing the file input stream");
                }
                throw th;
            }
        } catch (java.io.FileNotFoundException e9) {
            ie = e9;
            try {
                android.util.Log.e(TAG, "IOException while reading from update file " + ie);
                try {
                    fis.close();
                } catch (java.io.IOException e10) {
                    android.util.Log.e(TAG, "Error closing the file input stream");
                }
                return null;
            } catch (Throwable th2) {
                th = th2;
                fis.close();
                throw th;
            }
        } catch (java.lang.Exception e11) {
            e = e11;
            android.util.Log.e(TAG, "Error while reading from update file " + e);
            fis.close();
            return null;
        }
    }
}
