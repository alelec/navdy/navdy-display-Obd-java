package com.navdy.obd.update;

public class DefaultFirmwareManager implements com.navdy.obd.update.ObdDeviceFirmwareManager {
    public java.lang.String getFirmwareVersion() {
        return "N/A";
    }

    public boolean updateFirmware(com.navdy.obd.update.Update update) {
        return false;
    }

    public boolean isUpdatingFirmware() {
        return false;
    }
}
