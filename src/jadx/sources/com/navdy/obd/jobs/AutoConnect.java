package com.navdy.obd.jobs;

public class AutoConnect implements java.lang.Runnable {
    private static final int FULL_SCAN_DELAY = 4000;
    private static final int INITIAL_SCAN_DELAY = 100;
    static final org.slf4j.Logger Log = org.slf4j.LoggerFactory.getLogger(com.navdy.obd.jobs.AutoConnect.class);
    public static final int MINIMAL_DELAY_TO_RECONNECT = 500;
    /* access modifiers changed from: private */
    public static final long RECONNECT_DELAY = java.util.concurrent.TimeUnit.SECONDS.toMillis(1);
    private static final long SLEEP_DELAY = java.util.concurrent.TimeUnit.SECONDS.toMillis(30);
    private com.navdy.obd.jobs.AutoConnect.AutoConnectState autoConnectState = com.navdy.obd.jobs.AutoConnect.AutoConnectState.SLEEPING;
    /* access modifiers changed from: private */
    public final com.navdy.obd.CarState carState;
    private com.navdy.obd.IObdServiceListener.Stub obdListener = new com.navdy.obd.jobs.AutoConnect.Anon3();
    private com.navdy.obd.IPidListener pidListener = new com.navdy.obd.jobs.AutoConnect.Anon1();
    private java.util.Queue<com.navdy.obd.io.ChannelInfo> possibleChannels;
    private com.navdy.obd.discovery.GroupScanner scanner;
    private com.navdy.obd.PidSet scanningPids;
    private com.navdy.obd.ObdService service;
    /* access modifiers changed from: private */
    public long sleepDelay = SLEEP_DELAY;
    private boolean started;

    class Anon1 extends com.navdy.obd.IPidListener.Stub {
        Anon1() {
        }

        public void pidsChanged(java.util.List<com.navdy.obd.Pid> list) throws android.os.RemoteException {
        }

        public void onConnectionStateChange(int newState) throws android.os.RemoteException {
        }

        public void pidsRead(java.util.List<com.navdy.obd.Pid> list, java.util.List<com.navdy.obd.Pid> list2) throws android.os.RemoteException {
        }
    }

    class Anon2 implements java.util.Comparator<com.navdy.obd.io.ChannelInfo> {
        Anon2() {
        }

        public int compare(com.navdy.obd.io.ChannelInfo lhs, com.navdy.obd.io.ChannelInfo rhs) {
            int lhsType = lhs.getConnectionType().ordinal();
            int rhsType = rhs.getConnectionType().ordinal();
            if (lhsType < rhsType) {
                return -1;
            }
            if (lhsType > rhsType) {
                return 1;
            }
            return lhs.getAddress().compareTo(rhs.getAddress());
        }
    }

    class Anon3 extends com.navdy.obd.IObdServiceListener.Stub {
        Anon3() {
        }

        public void onConnectionStateChange(int newState) throws android.os.RemoteException {
            if (newState == 0) {
                com.navdy.obd.jobs.AutoConnect.this.setState(com.navdy.obd.jobs.AutoConnect.AutoConnectState.DISCONNECTED);
            } else if (newState == 2) {
                com.navdy.obd.jobs.AutoConnect.this.sleepDelay = com.navdy.obd.jobs.AutoConnect.RECONNECT_DELAY;
                com.navdy.obd.jobs.AutoConnect.this.setState(com.navdy.obd.jobs.AutoConnect.AutoConnectState.CONNECTED);
                com.navdy.obd.jobs.AutoConnect.Log.info("Starting scanning");
                java.util.List<com.navdy.obd.Pid> initialSet = new java.util.ArrayList<>();
                initialSet.add(new com.navdy.obd.Pid(13));
                com.navdy.obd.jobs.AutoConnect.this.getReadings(initialSet);
            }
        }

        public void onStatusMessage(java.lang.String message) throws android.os.RemoteException {
        }

        public void onRawData(java.lang.String data) throws android.os.RemoteException {
        }

        public void scannedPids(java.util.List<com.navdy.obd.Pid> pids) throws android.os.RemoteException {
            com.navdy.obd.jobs.AutoConnect.Log.debug("Recording readings for " + (pids != null ? pids.size() : 0) + " pids");
            synchronized (com.navdy.obd.jobs.AutoConnect.this.carState) {
                com.navdy.obd.jobs.AutoConnect.this.carState.recordReadings(pids);
            }
        }

        public void supportedPids(java.util.List<com.navdy.obd.Pid> list) throws android.os.RemoteException {
        }

        public void scannedVIN(java.lang.String vin) {
        }
    }

    public enum AutoConnectState {
        DISCONNECTED,
        INITIAL_SCAN,
        FULL_SCAN,
        CONNECTING,
        CONNECTED,
        SLEEPING,
        STOPPING
    }

    public AutoConnect(com.navdy.obd.ObdService service2) {
        this.service = service2;
        this.carState = new com.navdy.obd.CarState();
        this.started = false;
    }

    public com.navdy.obd.jobs.AutoConnect.AutoConnectState getState() {
        return this.autoConnectState;
    }

    /* access modifiers changed from: private */
    public void setState(com.navdy.obd.jobs.AutoConnect.AutoConnectState newState) {
        if (newState != this.autoConnectState) {
            Log.info("Switching to state:" + newState.name());
            this.autoConnectState = newState;
        }
    }

    public java.util.List<com.navdy.obd.Pid> getReadings(java.util.List<com.navdy.obd.Pid> pids) {
        java.util.List<com.navdy.obd.Pid> readings;
        if (this.autoConnectState == com.navdy.obd.jobs.AutoConnect.AutoConnectState.CONNECTED && pidsChanged(pids)) {
            updateScan(pids);
        }
        synchronized (this.carState) {
            readings = this.carState.getReadings(pids);
        }
        return readings;
    }

    public void start() {
        if (!this.started) {
            this.started = true;
            this.service.addListener(this.obdListener);
            this.service.getHandler().post(this);
        }
    }

    public void stop() {
        if (this.started) {
            this.started = false;
            setState(com.navdy.obd.jobs.AutoConnect.AutoConnectState.STOPPING);
            this.service.removeListener((com.navdy.obd.IObdServiceListener) this.obdListener);
        }
    }

    private boolean pidsChanged(java.util.List<com.navdy.obd.Pid> pids) {
        if (this.scanningPids == null) {
            return true;
        }
        for (com.navdy.obd.Pid pid : pids) {
            if (!this.scanningPids.contains(pid)) {
                return true;
            }
        }
        return false;
    }

    private void updateScan(java.util.List<com.navdy.obd.Pid> pids) {
        this.scanningPids = new com.navdy.obd.PidSet(pids);
        Log.info("Starting to scan for " + pids.size() + " pids");
        this.service.addListener(pids, this.pidListener);
    }

    private java.util.LinkedList<com.navdy.obd.io.ChannelInfo> prioritizeChannels(java.util.Set<com.navdy.obd.io.ChannelInfo> channels) {
        com.navdy.obd.io.ChannelInfo[] channelArray = new com.navdy.obd.io.ChannelInfo[channels.size()];
        channels.toArray(channelArray);
        java.util.Arrays.sort(channelArray, new com.navdy.obd.jobs.AutoConnect.Anon2());
        java.util.LinkedList<com.navdy.obd.io.ChannelInfo> result = new java.util.LinkedList<>();
        for (com.navdy.obd.io.ChannelInfo add : channelArray) {
            result.add(add);
        }
        return result;
    }

    public void run() {
        if (this.autoConnectState != com.navdy.obd.jobs.AutoConnect.AutoConnectState.STOPPING) {
            com.navdy.obd.io.ChannelInfo info = null;
            switch (this.autoConnectState) {
                case DISCONNECTED:
                    Log.debug("AutoConnect: Checking for default channel");
                    this.scanningPids = null;
                    info = this.service.getDefaultChannelInfo();
                    if (info == null && this.possibleChannels != null) {
                        info = (com.navdy.obd.io.ChannelInfo) this.possibleChannels.poll();
                        if (info != null) {
                            Log.debug("Using next channel from scan");
                        }
                    }
                    if (info == null) {
                        Log.debug("No default found and exhausted previous scan - sleeping");
                        setState(com.navdy.obd.jobs.AutoConnect.AutoConnectState.SLEEPING);
                        break;
                    }
                    break;
                case SLEEPING:
                    setState(com.navdy.obd.jobs.AutoConnect.AutoConnectState.INITIAL_SCAN);
                    if (this.scanner == null) {
                        this.scanner = new com.navdy.obd.discovery.GroupScanner(this.service, null);
                    }
                    this.scanner.startScan();
                    break;
                case INITIAL_SCAN:
                    java.util.Set<com.navdy.obd.io.ChannelInfo> channels = this.scanner.getDiscoveredChannels();
                    if (!channels.isEmpty()) {
                        this.scanner.stopScan();
                        this.possibleChannels = prioritizeChannels(channels);
                        info = (com.navdy.obd.io.ChannelInfo) this.possibleChannels.poll();
                        break;
                    } else {
                        Log.debug("AutoConnect: No channels found after initial scan, waiting for FULL_SCAN");
                        setState(com.navdy.obd.jobs.AutoConnect.AutoConnectState.FULL_SCAN);
                        break;
                    }
                case FULL_SCAN:
                    java.util.Set<com.navdy.obd.io.ChannelInfo> channels2 = this.scanner.getDiscoveredChannels();
                    this.scanner.stopScan();
                    if (!channels2.isEmpty()) {
                        info = (com.navdy.obd.io.ChannelInfo) channels2.iterator().next();
                        break;
                    } else {
                        Log.debug("AutoConnect: No channels found after full scan, restarting scan process");
                        setState(com.navdy.obd.jobs.AutoConnect.AutoConnectState.DISCONNECTED);
                        break;
                    }
            }
            if (info != null) {
                Log.debug("AutoConnect: found channel " + info.asString());
                setState(com.navdy.obd.jobs.AutoConnect.AutoConnectState.CONNECTING);
                this.scanner = null;
                this.service.openChannel(info);
            }
            this.service.getHandler().postDelayed(this, getDelay());
        }
    }

    private long getDelay() {
        long delay = RECONNECT_DELAY;
        switch (this.autoConnectState) {
            case SLEEPING:
                if (!this.service.reconnectImmediately()) {
                    delay = this.sleepDelay;
                    break;
                } else {
                    delay = 500;
                    break;
                }
            case INITIAL_SCAN:
                delay = 100;
                break;
            case FULL_SCAN:
                delay = 4000;
                break;
        }
        if (this.service.reconnectImmediately()) {
            return 500;
        }
        return delay;
    }

    public void triggerReconnect() {
        this.service.getHandler().removeCallbacks(this);
        this.service.getHandler().post(this);
    }
}
