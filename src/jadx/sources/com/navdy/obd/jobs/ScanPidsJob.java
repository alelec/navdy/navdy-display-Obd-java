package com.navdy.obd.jobs;

public class ScanPidsJob extends com.navdy.obd.ObdJob {
    static final org.slf4j.Logger Log = org.slf4j.LoggerFactory.getLogger(com.navdy.obd.ObdJob.class);
    private static final int MAX_SCAN_COUNT_WITH_NO_DATA = 5;
    public static final int MINIMUM_SAMPLES_REQUIRED = 10;
    static final com.navdy.obd.command.ICommand NO_OP_COMMAND = new com.navdy.obd.jobs.ScanPidsJob.Anon1();
    public static final double SAMPLING_DATA_DELAY_THRESHOLD = 100.0d;
    /* access modifiers changed from: private */
    public java.util.List<com.navdy.obd.Pid> fullPids;
    /* access modifiers changed from: private */
    public java.util.HashMap<java.lang.Integer, com.navdy.obd.Pid> fullPidsLookup = new java.util.HashMap<>();
    boolean isGatheringCanBusData = false;
    private java.util.List<com.navdy.obd.Pid> sampledPids = new java.util.ArrayList();
    int scanCountWithNoData = 0;
    com.navdy.obd.ScanSchedule schedule;
    private boolean started = false;
    private com.navdy.obd.VehicleStateManager vehicleStateManager;

    static class Anon1 implements com.navdy.obd.command.ICommand {
        Anon1() {
        }

        public java.lang.String getName() {
            return "NO_OP";
        }

        public void execute(java.io.InputStream input, java.io.OutputStream output, com.navdy.obd.Protocol protocol, com.navdy.obd.command.IObdDataObserver commandObserver) throws java.io.IOException {
        }

        public double getDoubleValue() {
            return -2.147483648E9d;
        }

        public int getIntValue() {
            return Integer.MIN_VALUE;
        }

        public java.lang.String getResponse() {
            return "";
        }
    }

    class Anon2 implements com.navdy.obd.command.CANBusDataProcessor.ICanBusDataListener {
        final /* synthetic */ com.navdy.obd.jobs.ScanPidsJob.IListener val$listener;

        Anon2(com.navdy.obd.jobs.ScanPidsJob.IListener iListener) {
            this.val$listener = iListener;
        }

        public void onCanBusDataRead(int pid, double value) {
            if (com.navdy.obd.jobs.ScanPidsJob.this.fullPidsLookup.containsKey(java.lang.Integer.valueOf(pid))) {
                com.navdy.obd.Pid pidObject = (com.navdy.obd.Pid) com.navdy.obd.jobs.ScanPidsJob.this.fullPidsLookup.get(java.lang.Integer.valueOf(pid));
                pidObject.setTimeStamp(android.os.SystemClock.elapsedRealtime());
                pidObject.setValue(value);
                this.val$listener.onCanBusDataRead(com.navdy.obd.jobs.ScanPidsJob.this);
                return;
            }
            com.navdy.obd.Pid pidObject2 = new com.navdy.obd.Pid(pid);
            com.navdy.obd.jobs.ScanPidsJob.this.fullPids.add(pidObject2);
            com.navdy.obd.jobs.ScanPidsJob.this.fullPidsLookup.put(java.lang.Integer.valueOf(pid), pidObject2);
            pidObject2.setTimeStamp(android.os.SystemClock.elapsedRealtime());
            pidObject2.setValue(value);
            this.val$listener.onCanBusDataRead(com.navdy.obd.jobs.ScanPidsJob.this);
        }
    }

    public interface IListener extends com.navdy.obd.ObdJob.IListener {
        com.navdy.obd.command.CANBusMonitoringCommand getCanBusMonitoringCommand();

        void onCanBusDataRead(com.navdy.obd.jobs.ScanPidsJob scanPidsJob);

        void onCanBusMonitoringErrorDetected();
    }

    public ScanPidsJob(com.navdy.obd.VehicleInfo vehicleInfo, com.navdy.obd.Profile profile, com.navdy.obd.ScanSchedule schedule2, com.navdy.obd.VehicleStateManager vehicleStateManager2, com.navdy.obd.io.IChannel channel, com.navdy.obd.Protocol protocol, com.navdy.obd.jobs.ScanPidsJob.IListener listener, com.navdy.obd.command.IObdDataObserver obdDataObserver) {
        super(buildCommand(vehicleInfo, profile, schedule2, listener.getCanBusMonitoringCommand()), channel, protocol, listener, obdDataObserver);
        if (listener.getCanBusMonitoringCommand() != null) {
            listener.getCanBusMonitoringCommand().setCANBusDataListener(new com.navdy.obd.jobs.ScanPidsJob.Anon2(listener));
        }
        this.fullPids = ((com.navdy.obd.command.ScheduledBatchCommand) this.command).getPids();
        for (com.navdy.obd.Pid pid : this.fullPids) {
            if (pid.getId() == 1000) {
                this.isGatheringCanBusData = true;
            }
            this.fullPidsLookup.put(java.lang.Integer.valueOf(pid.getId()), pid);
        }
        this.schedule = schedule2;
        this.vehicleStateManager = vehicleStateManager2;
    }

    /* access modifiers changed from: protected */
    public void postExecute() throws java.io.IOException {
        if (!this.started) {
            this.started = true;
            this.vehicleStateManager.update(this.fullPids);
        }
        com.navdy.obd.command.ScheduledBatchCommand batchCommand = (com.navdy.obd.command.ScheduledBatchCommand) this.command;
        java.util.List<com.navdy.obd.command.ICommand> commands = batchCommand.getCommands();
        this.sampledPids.clear();
        boolean hasData = false;
        int length = this.fullPids.size();
        for (int i = 0; i < length; i++) {
            com.navdy.obd.Pid pid = (com.navdy.obd.Pid) this.fullPids.get(i);
            com.navdy.obd.command.Sample sample = (com.navdy.obd.command.Sample) batchCommand.samples.get(i);
            if (sample.updated) {
                this.sampledPids.add(pid);
                double value = getValue((com.navdy.obd.command.ICommand) commands.get(i), pid);
                if (value != -2.147483648E9d) {
                    hasData = true;
                }
                long samplingTime = sample.getLastSamplingTime();
                boolean outlierSample = false;
                if (samplingTime > 0) {
                    com.navdy.util.RunningStats sampleStats = sample.samplingTimeStats;
                    sample.samplingTimeStats.add((double) samplingTime);
                    if (sampleStats.count() > 10 && ((double) samplingTime) >= sampleStats.mean() + 100.0d) {
                        this.commandObserver.onError("DELAYED," + samplingTime + "," + sampleStats.count() + "," + sampleStats.mean() + "," + sampleStats.standardDeviation());
                        outlierSample = true;
                    }
                }
                pid.setTimeStamp(sample.lastSampleTimestamp);
                if (!outlierSample) {
                    pid.setValue(value);
                }
            }
        }
        if (hasData) {
            this.scanCountWithNoData = 0;
        } else {
            this.scanCountWithNoData++;
        }
        if (this.scanCountWithNoData >= 5) {
            if (this.isGatheringCanBusData) {
                ((com.navdy.obd.jobs.ScanPidsJob.IListener) this.listener).onCanBusMonitoringErrorDetected();
            }
            throw new java.io.IOException("Not getting any data from Obd for any PIDS");
        }
        this.vehicleStateManager.onScanComplete();
    }

    public java.util.List<com.navdy.obd.Pid> getSampledPids() {
        return this.sampledPids;
    }

    public java.util.List<com.navdy.obd.Pid> getFullPidsList() {
        return this.fullPids;
    }

    private double getValue(com.navdy.obd.command.ICommand command, com.navdy.obd.Pid pid) {
        com.navdy.obd.command.ObdCommand obdCommand = command instanceof com.navdy.obd.command.ObdCommand ? (com.navdy.obd.command.ObdCommand) command : null;
        if (pid.getId() != 47 || obdCommand == null || obdCommand.getResponseCount() <= 1) {
            return command.getDoubleValue();
        }
        int responses = obdCommand.getResponseCount();
        for (int i = 0; i < responses; i++) {
            obdCommand.setTargetEcu(obdCommand.getResponse(i).ecu);
            double val = obdCommand.getDoubleValue();
            if (val != -2.147483648E9d && val != 0.0d) {
                return val;
            }
        }
        return -2.147483648E9d;
    }

    private static com.navdy.obd.command.ScheduledBatchCommand buildCommand(com.navdy.obd.VehicleInfo vehicleInfo, com.navdy.obd.Profile profile, com.navdy.obd.ScanSchedule schedule2, com.navdy.obd.command.CANBusMonitoringCommand canBusMonitoringCommand) {
        int pidCount = schedule2.size();
        java.util.List<com.navdy.obd.ScanSchedule.Scan> scanList = schedule2.getScanList();
        java.util.List<com.navdy.obd.command.ICommand> commands = new java.util.ArrayList<>();
        for (int i = 0; i < pidCount; i++) {
            int pidId = ((com.navdy.obd.ScanSchedule.Scan) scanList.get(i)).pid;
            if (pidId == 1000) {
                Log.info("Added CAN bus gathering data command to the schedule");
                commands.add(com.navdy.obd.command.GatherCANBusDataCommand.MONITOR_COMMAND);
            } else if (pidId == 1001) {
                Log.info("Added CAN bus monitoring data command to the schedule");
                canBusMonitoringCommand.reset();
                commands.add(canBusMonitoringCommand);
            } else {
                com.navdy.obd.command.ObdCommand command = profile.lookup(pidId);
                if (command != null) {
                    int ecusSupportingPid = supportedEcuCount(vehicleInfo, pidId);
                    if (ecusSupportingPid > 1) {
                        command.setTargetEcu(bestEcu(vehicleInfo, pidId).address);
                        command.setExpectedResponses(ecusSupportingPid);
                        commands.add(command);
                    } else if (ecusSupportingPid == 1) {
                        command.setTargetEcu(-1);
                        command.setExpectedResponses(1);
                        commands.add(command);
                    } else {
                        Log.warn("Scanning for unsupported pid! " + pidId);
                        commands.add(NO_OP_COMMAND);
                    }
                } else {
                    commands.add(NO_OP_COMMAND);
                    Log.warn("Unable to find obd command to read , adding a NO op command" + pidId);
                }
            }
        }
        return new com.navdy.obd.command.ScheduledBatchCommand(scanList, (com.navdy.obd.command.ICommand[]) commands.toArray(new com.navdy.obd.command.ICommand[pidCount]));
    }

    private static com.navdy.obd.ECU bestEcu(com.navdy.obd.VehicleInfo vehicleInfo, int pid) {
        int bestScore = 0;
        com.navdy.obd.ECU bestEcu = null;
        for (com.navdy.obd.ECU ecu : vehicleInfo.ecus) {
            int score = ecu.supportedPids.size();
            if (ecu.supportedPids.contains(pid) && score > bestScore) {
                bestEcu = ecu;
                bestScore = score;
            }
        }
        return bestEcu;
    }

    private static int supportedEcuCount(com.navdy.obd.VehicleInfo vehicleInfo, int pid) {
        int count = 0;
        for (com.navdy.obd.ECU ecu : vehicleInfo.ecus) {
            if (ecu.supportedPids.contains(pid)) {
                count++;
            }
        }
        return count;
    }
}
