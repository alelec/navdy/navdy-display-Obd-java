package com.navdy.util;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0005\n\u0002\u0010\u0006\n\u0002\b\b\n\u0002\u0010\u0002\n\u0002\b\b\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\nJ\u0006\u0010\u0015\u001a\u00020\u0004J\u0006\u0010\u0016\u001a\u00020\nJ\u0006\u0010\u0017\u001a\u00020\u0013J\u0006\u0010\u0018\u001a\u00020\nJ\u0006\u0010\u0019\u001a\u00020\nJ\u000e\u0010\u001a\u001a\u00020\n2\u0006\u0010\u0014\u001a\u00020\nR\u001a\u0010\u0003\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001a\u0010\t\u001a\u00020\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u001a\u0010\u000f\u001a\u00020\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0010\u0010\f\"\u0004\b\u0011\u0010\u000e\u00a8\u0006\u001b"}, d2 = {"Lcom/navdy/util/RunningStats;", "", "()V", "n", "", "getN", "()J", "setN", "(J)V", "newM", "", "getNewM", "()D", "setNewM", "(D)V", "newV", "getNewV", "setNewV", "add", "", "value", "count", "mean", "reset", "standardDeviation", "variance", "zScore", "obd-service_release"}, k = 1, mv = {1, 1, 6})
/* compiled from: RunningStats.kt */
public final class RunningStats {
    private long n;
    private double newM;
    private double newV;

    public final long getN() {
        return this.n;
    }

    public final void setN(long j) {
        this.n = j;
    }

    public final double getNewM() {
        return this.newM;
    }

    public final void setNewM(double d) {
        this.newM = d;
    }

    public final double getNewV() {
        return this.newV;
    }

    public final void setNewV(double d) {
        this.newV = d;
    }

    public final void add(double value) {
        this.n++;
        if (this.n == 1) {
            this.newM = value;
            this.newV = 0.0d;
            return;
        }
        double oldM = this.newM;
        double oldV = this.newV;
        this.newM = ((value - oldM) / ((double) this.n)) + oldM;
        this.newV = ((value - oldM) * (value - this.newM)) + oldV;
    }

    public final long count() {
        return this.n;
    }

    public final double mean() {
        if (this.n > ((long) 0)) {
            return this.newM;
        }
        return 0.0d;
    }

    public final double variance() {
        if (this.n > ((long) 1)) {
            return this.newV / ((double) (this.n - ((long) 1)));
        }
        return 0.0d;
    }

    public final double standardDeviation() {
        return java.lang.Math.sqrt(variance());
    }

    public final double zScore(double value) {
        double standardDeviation = standardDeviation();
        double mean = mean();
        if (standardDeviation != 0.0d) {
            return java.lang.Math.abs((value - mean) / standardDeviation);
        }
        if (value == mean) {
            return 0.0d;
        }
        return 10.0d;
    }

    public final void reset() {
        this.n = 0;
        this.newM = 0.0d;
        this.newV = 0.0d;
    }
}
