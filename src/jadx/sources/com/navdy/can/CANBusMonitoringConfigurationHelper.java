package com.navdy.can;

public class CANBusMonitoringConfigurationHelper {
    private static util.Configuration[] CAR_DETAILS_CONFIGURATION_MAPPING = null;
    public static final java.lang.String CAR_DETAILS_CONFIGURATION_MAPPING_FILE = "configuration_mapping.csv";
    private static util.Configuration[] VIN_CONFIGURATION_MAPPING = null;
    public static final java.lang.String VIN_CONFIGURATION_MAPPING_FILE = "vin_configuration_mapping.csv";

    class Column {
        public static final java.lang.String BIT_LENGTH = "BitLength";
        public static final java.lang.String BIT_OFFSET = "BitOffset";
        public static final java.lang.String BYTE_OFFSET = "ByteOffset";
        public static final java.lang.String ENDIAN = "Endian";
        public static final java.lang.String FREQ = "Freq";
        public static final java.lang.String HEADER = "Header";
        public static final java.lang.String MAX_VALUE = "MaxValue";
        public static final java.lang.String MIN_VALUE = "MinValue";
        public static final java.lang.String NAME = "Name";
        public static final java.lang.String OBD_PID = "OBD_Pid";
        public static final java.lang.String RESOLUTION = "Resolution";
        public static final java.lang.String VALUE_OFFSET = "ValueOffset";

        Column() {
        }
    }

    public static com.navdy.obd.command.CANBusMonitoringCommand loadCANBusMonitoringSetupCommand(android.content.Context context, java.lang.String vin) {
        if (VIN_CONFIGURATION_MAPPING == null) {
            VIN_CONFIGURATION_MAPPING = util.Util.loadConfigurationMappingList(context, VIN_CONFIGURATION_MAPPING_FILE);
        }
        util.Configuration matchingConfiguration = util.Util.pickConfiguration(VIN_CONFIGURATION_MAPPING, vin);
        if (matchingConfiguration != null) {
            return loadCommandFromConfigurationFile(context, matchingConfiguration.configurationName);
        }
        return null;
    }

    public static com.navdy.obd.command.CANBusMonitoringCommand loadCANBusMonitoringSetupCommand(android.content.Context context, java.lang.String make, java.lang.String model, java.lang.String year) {
        if (CAR_DETAILS_CONFIGURATION_MAPPING == null) {
            CAR_DETAILS_CONFIGURATION_MAPPING = util.Util.loadConfigurationMappingList(context, VIN_CONFIGURATION_MAPPING_FILE);
        }
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        if (android.text.TextUtils.isEmpty(make)) {
            make = org.slf4j.Marker.ANY_MARKER;
        }
        java.lang.StringBuilder append = sb.append(make).append("_");
        if (android.text.TextUtils.isEmpty(model)) {
            model = org.slf4j.Marker.ANY_MARKER;
        }
        java.lang.StringBuilder append2 = append.append(model).append("_");
        if (android.text.TextUtils.isEmpty(year)) {
            year = org.slf4j.Marker.ANY_MARKER;
        }
        util.Configuration matchingConfiguration = util.Util.pickConfiguration(CAR_DETAILS_CONFIGURATION_MAPPING, append2.append(year).toString());
        if (matchingConfiguration != null) {
            return loadCommandFromConfigurationFile(context, matchingConfiguration.configurationName);
        }
        return null;
    }

    private static com.navdy.obd.command.CANBusMonitoringCommand loadCommandFromConfigurationFile(android.content.Context context, java.lang.String configurationName) {
        try {
            java.io.BufferedReader bufferedReader = new java.io.BufferedReader(new java.io.InputStreamReader(context.getAssets().open(configurationName)));
            java.lang.Iterable<org.apache.commons.csv.CSVRecord> records = org.apache.commons.csv.CSVFormat.DEFAULT.withHeader(new java.lang.String[0]).parse(bufferedReader);
            java.util.ArrayList<com.navdy.obd.can.CANBusDataDescriptor> descriptors = new java.util.ArrayList<>();
            for (org.apache.commons.csv.CSVRecord record : records) {
                java.lang.String name = record.get("Name");
                java.lang.String header = record.get("Header");
                int byteOffset = java.lang.Integer.parseInt(record.get("ByteOffset"));
                int bitOffset = java.lang.Integer.parseInt(record.get("BitOffset"));
                int bitLength = java.lang.Integer.parseInt(record.get("BitLength"));
                int valueOffset = java.lang.Integer.parseInt(record.get("ValueOffset"));
                long j = (long) valueOffset;
                java.util.ArrayList arrayList = descriptors;
                arrayList.add(new com.navdy.obd.can.CANBusDataDescriptor(name, header, byteOffset, bitOffset, bitLength, java.lang.Double.parseDouble(record.get("Resolution")), j, java.lang.Double.parseDouble(record.get("MinValue")), java.lang.Double.parseDouble(record.get("MaxValue")), java.lang.Integer.parseInt(record.get("OBD_Pid")), java.lang.Integer.parseInt(record.get(com.navdy.can.CANBusMonitoringConfigurationHelper.Column.FREQ)), "L".equals(record.get(com.navdy.can.CANBusMonitoringConfigurationHelper.Column.ENDIAN))));
            }
            com.navdy.obd.command.CANBusMonitoringCommand canBusMonitoringCommand = new com.navdy.obd.command.CANBusMonitoringCommand(descriptors);
            return canBusMonitoringCommand;
        } catch (java.io.IOException e) {
            return null;
        }
    }
}
