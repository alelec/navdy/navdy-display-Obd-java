package com.navdy.hardware;

public class SerialPort {
    private static final java.lang.String TAG = "SerialPort";
    private int mNativeContext;
    private final java.lang.String mPath;

    private static native void classInitNative();

    private native void native_close();

    private native boolean native_open(java.lang.String str, int i) throws java.io.IOException;

    private native int native_read_array(byte[] bArr, int i) throws java.io.IOException;

    private native int native_read_direct(java.nio.ByteBuffer byteBuffer, int i) throws java.io.IOException;

    private native void native_send_break();

    private native void native_write_array(byte[] bArr, int i) throws java.io.IOException;

    private native void native_write_direct(java.nio.ByteBuffer byteBuffer, int i) throws java.io.IOException;

    static {
        java.lang.System.loadLibrary("obd-service");
        classInitNative();
    }

    public SerialPort(java.lang.String name) {
        this.mPath = name;
    }

    public boolean open(int speed) throws java.io.IOException {
        return native_open(this.mPath, speed);
    }

    public void close() throws java.io.IOException {
        native_close();
    }

    public java.lang.String getPath() {
        return this.mPath;
    }

    public int read(java.nio.ByteBuffer buffer) throws java.io.IOException {
        if (buffer.isDirect()) {
            return native_read_direct(buffer, buffer.remaining());
        }
        if (buffer.hasArray()) {
            return native_read_array(buffer.array(), buffer.remaining());
        }
        throw new java.lang.IllegalArgumentException("buffer is not direct and has no array");
    }

    public void write(java.nio.ByteBuffer buffer, int length) throws java.io.IOException {
        if (buffer.isDirect()) {
            native_write_direct(buffer, length);
        } else if (buffer.hasArray()) {
            native_write_array(buffer.array(), length);
        } else {
            throw new java.lang.IllegalArgumentException("buffer is not direct and has no array");
        }
    }

    public void sendBreak() {
        native_send_break();
    }
}
