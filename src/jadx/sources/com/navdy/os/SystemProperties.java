package com.navdy.os;

public class SystemProperties {
    private static java.lang.Class<?> CLASS;

    static {
        try {
            CLASS = java.lang.Class.forName("android.os.SystemProperties");
        } catch (java.lang.ClassNotFoundException e) {
        }
    }

    public static java.lang.String get(java.lang.String key) {
        try {
            return (java.lang.String) CLASS.getMethod("get", new java.lang.Class[]{java.lang.String.class}).invoke(null, new java.lang.Object[]{key});
        } catch (java.lang.Exception e) {
            return null;
        }
    }

    public static java.lang.String get(java.lang.String key, java.lang.String def) {
        try {
            return (java.lang.String) CLASS.getMethod("get", new java.lang.Class[]{java.lang.String.class, java.lang.String.class}).invoke(null, new java.lang.Object[]{key, def});
        } catch (java.lang.Exception e) {
            return def;
        }
    }

    public static int getInt(java.lang.String key, int def) {
        try {
            return ((java.lang.Integer) CLASS.getMethod("getInt", new java.lang.Class[]{java.lang.String.class, java.lang.Integer.TYPE}).invoke(null, new java.lang.Object[]{key, java.lang.Integer.valueOf(def)})).intValue();
        } catch (java.lang.Exception e) {
            return def;
        }
    }

    public static long getLong(java.lang.String key, long def) {
        try {
            return ((java.lang.Long) CLASS.getMethod("getLong", new java.lang.Class[]{java.lang.String.class, java.lang.Long.TYPE}).invoke(null, new java.lang.Object[]{key, java.lang.Long.valueOf(def)})).longValue();
        } catch (java.lang.Exception e) {
            return def;
        }
    }

    public static boolean getBoolean(java.lang.String key, boolean def) {
        try {
            return ((java.lang.Boolean) CLASS.getMethod("getBoolean", new java.lang.Class[]{java.lang.String.class, java.lang.Boolean.TYPE}).invoke(null, new java.lang.Object[]{key, java.lang.Boolean.valueOf(def)})).booleanValue();
        } catch (java.lang.Exception e) {
            return def;
        }
    }

    private SystemProperties() {
    }
}
