package brut.common;

public class BrutException extends java.lang.Exception {
    public BrutException() {
    }

    public BrutException(java.lang.String str) {
        super(str);
    }

    public BrutException(java.lang.String str, java.lang.Throwable th) {
        super(str, th);
    }

    public BrutException(java.lang.Throwable th) {
        super(th);
    }
}
