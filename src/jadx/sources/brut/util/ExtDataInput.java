package brut.util;

public class ExtDataInput extends brut.util.DataInputDelegate {
    public ExtDataInput(java.io.DataInput dataInput) {
        super(dataInput);
    }

    public ExtDataInput(java.io.InputStream inputStream) {
        this((java.io.DataInput) new java.io.DataInputStream(inputStream));
    }

    public int[] readIntArray(int i) throws java.io.IOException {
        int[] iArr = new int[i];
        for (int i2 = 0; i2 < i; i2++) {
            iArr[i2] = readInt();
        }
        return iArr;
    }

    public java.lang.String readNulEndedString(int i, boolean z) throws java.io.IOException {
        int i2;
        java.lang.StringBuilder sb = new java.lang.StringBuilder(16);
        while (true) {
            i2 = i - 1;
            if (i == 0) {
                break;
            }
            short readShort = readShort();
            if (readShort == 0) {
                break;
            }
            sb.append((char) readShort);
            i = i2;
        }
        if (z) {
            skipBytes(i2 * 2);
        }
        return sb.toString();
    }

    public void skipCheckByte(byte b) throws java.io.IOException {
        byte readByte = readByte();
        if (readByte != b) {
            throw new java.io.IOException(java.lang.String.format("Expected: 0x%08x, got: 0x%08x", new java.lang.Object[]{java.lang.Byte.valueOf(b), java.lang.Byte.valueOf(readByte)}));
        }
    }

    public void skipCheckInt(int i) throws java.io.IOException {
        int readInt = readInt();
        if (readInt != i) {
            throw new java.io.IOException(java.lang.String.format("Expected: 0x%08x, got: 0x%08x", new java.lang.Object[]{java.lang.Integer.valueOf(i), java.lang.Integer.valueOf(readInt)}));
        }
    }

    public void skipCheckShort(short s) throws java.io.IOException {
        short readShort = readShort();
        if (readShort != s) {
            throw new java.io.IOException(java.lang.String.format("Expected: 0x%08x, got: 0x%08x", new java.lang.Object[]{java.lang.Short.valueOf(s), java.lang.Short.valueOf(readShort)}));
        }
    }

    public void skipInt() throws java.io.IOException {
        skipBytes(4);
    }
}
