package brut.util;

public abstract class DataInputDelegate implements java.io.DataInput {
    protected final java.io.DataInput mDelegate;

    public DataInputDelegate(java.io.DataInput dataInput) {
        this.mDelegate = dataInput;
    }

    public boolean readBoolean() throws java.io.IOException {
        return this.mDelegate.readBoolean();
    }

    public byte readByte() throws java.io.IOException {
        return this.mDelegate.readByte();
    }

    public char readChar() throws java.io.IOException {
        return this.mDelegate.readChar();
    }

    public double readDouble() throws java.io.IOException {
        return this.mDelegate.readDouble();
    }

    public float readFloat() throws java.io.IOException {
        return this.mDelegate.readFloat();
    }

    public void readFully(byte[] bArr) throws java.io.IOException {
        this.mDelegate.readFully(bArr);
    }

    public void readFully(byte[] bArr, int i, int i2) throws java.io.IOException {
        this.mDelegate.readFully(bArr, i, i2);
    }

    public int readInt() throws java.io.IOException {
        return this.mDelegate.readInt();
    }

    public java.lang.String readLine() throws java.io.IOException {
        return this.mDelegate.readLine();
    }

    public long readLong() throws java.io.IOException {
        return this.mDelegate.readLong();
    }

    public short readShort() throws java.io.IOException {
        return this.mDelegate.readShort();
    }

    public java.lang.String readUTF() throws java.io.IOException {
        return this.mDelegate.readUTF();
    }

    public int readUnsignedByte() throws java.io.IOException {
        return this.mDelegate.readUnsignedByte();
    }

    public int readUnsignedShort() throws java.io.IOException {
        return this.mDelegate.readUnsignedShort();
    }

    public int skipBytes(int i) throws java.io.IOException {
        return this.mDelegate.skipBytes(i);
    }
}
