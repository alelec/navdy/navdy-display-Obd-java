package brut.androlib;

public class AndrolibException extends brut.common.BrutException {
    public AndrolibException() {
    }

    public AndrolibException(java.lang.String str) {
        super(str);
    }

    public AndrolibException(java.lang.String str, java.lang.Throwable th) {
        super(str, th);
    }

    public AndrolibException(java.lang.Throwable th) {
        super(th);
    }
}
