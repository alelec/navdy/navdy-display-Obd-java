package brut.androlib.res.xml;

public final class ResXmlEncoders {
    public static java.lang.String encodeAsResXmlAttr(java.lang.String str) {
        if (str.isEmpty()) {
            return str;
        }
        char[] charArray = str.toCharArray();
        java.lang.StringBuilder sb = new java.lang.StringBuilder(str.length() + 10);
        switch (charArray[0]) {
            case android.support.v4.view.MotionEventCompat.AXIS_GENERIC_4 /*35*/:
            case '?':
            case '@':
                sb.append(ch.qos.logback.core.CoreConstants.ESCAPE_CHAR);
                break;
        }
        for (char c : charArray) {
            switch (c) {
                case 10:
                    sb.append("\\n");
                    continue;
                case android.support.v4.view.MotionEventCompat.AXIS_GENERIC_3 /*34*/:
                    sb.append("&quot;");
                    continue;
                case '\\':
                    sb.append(ch.qos.logback.core.CoreConstants.ESCAPE_CHAR);
                    break;
                default:
                    if (!isPrintableChar(c)) {
                        sb.append(java.lang.String.format("\\u%04x", new java.lang.Object[]{java.lang.Integer.valueOf(c)}));
                        continue;
                    }
                    break;
            }
            sb.append(c);
        }
        return sb.toString();
    }

    public static java.lang.String encodeAsXmlValue(java.lang.String str) {
        boolean z;
        boolean z2;
        if (str.isEmpty()) {
            return str;
        }
        char[] charArray = str.toCharArray();
        java.lang.StringBuilder sb = new java.lang.StringBuilder(str.length() + 10);
        switch (charArray[0]) {
            case android.support.v4.view.MotionEventCompat.AXIS_GENERIC_4 /*35*/:
            case '?':
            case '@':
                sb.append(ch.qos.logback.core.CoreConstants.ESCAPE_CHAR);
                break;
        }
        int length = charArray.length;
        int i = 0;
        boolean z3 = true;
        boolean z4 = false;
        int i2 = 0;
        boolean z5 = false;
        while (i < length) {
            char c = charArray[i];
            if (!z5) {
                if (c != ' ') {
                    switch (c) {
                        case 10:
                        case android.support.v4.view.MotionEventCompat.AXIS_GENERIC_8 /*39*/:
                            z2 = false;
                            z = true;
                            break;
                        case android.support.v4.view.MotionEventCompat.AXIS_GENERIC_3 /*34*/:
                            sb.append(ch.qos.logback.core.CoreConstants.ESCAPE_CHAR);
                            z = z4;
                            z2 = false;
                            break;
                        case '<':
                            if (!z4) {
                                z = z4;
                                z5 = true;
                                z2 = false;
                                break;
                            } else {
                                sb.insert(i2, '\"').append('\"');
                                z = z4;
                                z5 = true;
                                z2 = false;
                                break;
                            }
                        case '\\':
                            sb.append(ch.qos.logback.core.CoreConstants.ESCAPE_CHAR);
                            z = z4;
                            z2 = false;
                            break;
                        default:
                            if (isPrintableChar(c)) {
                                z = z4;
                                z2 = false;
                                break;
                            } else {
                                sb.append(java.lang.String.format("\\u%04x", new java.lang.Object[]{java.lang.Integer.valueOf(c)}));
                                z = z4;
                                z2 = false;
                                continue;
                            }
                    }
                } else {
                    if (z3) {
                        z4 = true;
                    }
                    z = z4;
                    z2 = true;
                }
            } else if (c == '>') {
                i2 = sb.length() + 1;
                z5 = false;
                z2 = z3;
                z = false;
            } else {
                boolean z6 = z3;
                z = z4;
                z2 = z6;
            }
            sb.append(c);
            i++;
            boolean z7 = z2;
            z4 = z;
            z3 = z7;
        }
        if (z4 || z3) {
            sb.insert(i2, '\"').append('\"');
        }
        return sb.toString();
    }

    public static java.lang.String enumerateNonPositionalSubstitutions(java.lang.String str) {
        java.util.List<java.lang.Integer> findNonPositionalSubstitutions = findNonPositionalSubstitutions(str, -1);
        if (findNonPositionalSubstitutions.size() < 2) {
            return str;
        }
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        int i = 0;
        int i2 = 0;
        for (java.lang.Integer intValue : findNonPositionalSubstitutions) {
            java.lang.Integer valueOf = java.lang.Integer.valueOf(intValue.intValue() + 1);
            int i3 = i + 1;
            sb.append(str.substring(i2, valueOf.intValue())).append(i3).append('$');
            i2 = valueOf.intValue();
            i = i3;
        }
        sb.append(str.substring(i2));
        return sb.toString();
    }

    public static java.lang.String escapeXmlChars(java.lang.String str) {
        return str.replace("&", "&amp;").replace("<", "&lt;");
    }

    private static java.util.List<java.lang.Integer> findNonPositionalSubstitutions(java.lang.String str, int i) {
        char charAt;
        int i2 = 0;
        int length = str.length();
        java.util.ArrayList arrayList = new java.util.ArrayList();
        int i3 = 0;
        while (true) {
            int indexOf = str.indexOf(37, i3);
            int i4 = indexOf + 1;
            if (i4 == 0 || i4 == length) {
                break;
            }
            i3 = i4 + 1;
            char charAt2 = str.charAt(i4);
            if (charAt2 != '%') {
                if (charAt2 >= '0' && charAt2 <= '9' && i3 < length) {
                    do {
                        int i5 = i3;
                        i3 = i5 + 1;
                        charAt = str.charAt(i5);
                        if (charAt < '0' || charAt > '9') {
                        }
                    } while (i3 < length);
                    if (charAt == '$') {
                        continue;
                    }
                }
                arrayList.add(java.lang.Integer.valueOf(indexOf));
                if (i != -1) {
                    i2++;
                    if (i2 >= i) {
                        break;
                    }
                } else {
                    continue;
                }
            }
        }
        return arrayList;
    }

    public static boolean hasMultipleNonPositionalSubstitutions(java.lang.String str) {
        return findNonPositionalSubstitutions(str, 2).size() > 1;
    }

    private static boolean isPrintableChar(char c) {
        java.lang.Character.UnicodeBlock of = java.lang.Character.UnicodeBlock.of(c);
        return (java.lang.Character.isISOControl(c) || c == 65535 || of == null || of == java.lang.Character.UnicodeBlock.SPECIALS) ? false : true;
    }
}
