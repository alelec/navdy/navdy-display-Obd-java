package brut.androlib.res.decoder;

public class AXmlResourceParser implements android.content.res.XmlResourceParser {
    private static final int ATTRIBUTE_IX_NAME = 1;
    private static final int ATTRIBUTE_IX_NAMESPACE_URI = 0;
    private static final int ATTRIBUTE_IX_VALUE_DATA = 4;
    private static final int ATTRIBUTE_IX_VALUE_STRING = 2;
    private static final int ATTRIBUTE_IX_VALUE_TYPE = 3;
    private static final int ATTRIBUTE_LENGHT = 5;
    private static final int CHUNK_AXML_FILE = 524291;
    private static final int CHUNK_RESOURCEIDS = 524672;
    private static final int CHUNK_XML_END_NAMESPACE = 1048833;
    private static final int CHUNK_XML_END_TAG = 1048835;
    private static final int CHUNK_XML_FIRST = 1048832;
    private static final int CHUNK_XML_LAST = 1048836;
    private static final int CHUNK_XML_START_NAMESPACE = 1048832;
    private static final int CHUNK_XML_START_TAG = 1048834;
    private static final int CHUNK_XML_TEXT = 1048836;
    private static final java.lang.String E_NOT_SUPPORTED = "Method is not supported.";
    private static final java.util.logging.Logger LOGGER = java.util.logging.Logger.getLogger(brut.androlib.res.decoder.AXmlResourceParser.class.getName());
    private brut.androlib.res.decoder.ResAttrDecoder mAttrDecoder;
    private brut.androlib.AndrolibException mFirstError;
    private int[] m_attributes;
    private int m_classAttribute;
    private boolean m_decreaseDepth;
    private int m_event;
    private int m_idAttribute;
    private int m_lineNumber;
    private int m_name;
    private int m_namespaceUri;
    private brut.androlib.res.decoder.AXmlResourceParser.NamespaceStack m_namespaces;
    private boolean m_operational;
    private brut.util.ExtDataInput m_reader;
    private int[] m_resourceIDs;
    private brut.androlib.res.decoder.StringBlock m_strings;
    private int m_styleAttribute;

    private static final class NamespaceStack {
        private int m_count;
        private int[] m_data = new int[32];
        private int m_dataLength;
        private int m_depth;

        private void ensureDataCapacity(int i) {
            int length = this.m_data.length - this.m_dataLength;
            if (length <= i) {
                int[] iArr = new int[((length + this.m_data.length) * 2)];
                java.lang.System.arraycopy(this.m_data, 0, iArr, 0, this.m_dataLength);
                this.m_data = iArr;
            }
        }

        private final int find(int i, boolean z) {
            if (this.m_dataLength == 0) {
                return -1;
            }
            int i2 = this.m_dataLength - 1;
            for (int i3 = this.m_depth; i3 != 0; i3--) {
                i2 -= 2;
                for (int i4 = this.m_data[i2]; i4 != 0; i4--) {
                    if (z) {
                        if (this.m_data[i2] == i) {
                            return this.m_data[i2 + 1];
                        }
                    } else if (this.m_data[i2 + 1] == i) {
                        return this.m_data[i2];
                    }
                    i2 -= 2;
                }
            }
            return -1;
        }

        private final int get(int i, boolean z) {
            if (this.m_dataLength == 0 || i < 0) {
                return -1;
            }
            int i2 = 0;
            int i3 = this.m_depth;
            while (i3 != 0) {
                int i4 = this.m_data[i2];
                if (i >= i4) {
                    i -= i4;
                    i2 += (i4 * 2) + 2;
                    i3--;
                } else {
                    int i5 = (i * 2) + 1 + i2;
                    if (!z) {
                        i5++;
                    }
                    return this.m_data[i5];
                }
            }
            return -1;
        }

        public final void decreaseDepth() {
            if (this.m_dataLength != 0) {
                int i = this.m_dataLength - 1;
                int i2 = this.m_data[i];
                if ((i - 1) - (i2 * 2) != 0) {
                    this.m_dataLength -= (i2 * 2) + 2;
                    this.m_count -= i2;
                    this.m_depth--;
                }
            }
        }

        public final int findPrefix(int i) {
            return find(i, false);
        }

        public final int findUri(int i) {
            return find(i, true);
        }

        public final int getAccumulatedCount(int i) {
            int i2 = 0;
            if (this.m_dataLength != 0 && i >= 0) {
                if (i > this.m_depth) {
                    i = this.m_depth;
                }
                int i3 = 0;
                while (i != 0) {
                    int i4 = this.m_data[i3];
                    i--;
                    i3 = (i4 * 2) + 2 + i3;
                    i2 += i4;
                }
            }
            return i2;
        }

        public final int getCurrentCount() {
            if (this.m_dataLength == 0) {
                return 0;
            }
            return this.m_data[this.m_dataLength - 1];
        }

        public final int getDepth() {
            return this.m_depth;
        }

        public final int getPrefix(int i) {
            return get(i, true);
        }

        public final int getTotalCount() {
            return this.m_count;
        }

        public final int getUri(int i) {
            return get(i, false);
        }

        public final void increaseDepth() {
            ensureDataCapacity(2);
            int i = this.m_dataLength;
            this.m_data[i] = 0;
            this.m_data[i + 1] = 0;
            this.m_dataLength += 2;
            this.m_depth++;
        }

        public final boolean pop() {
            if (this.m_dataLength == 0) {
                return false;
            }
            int i = this.m_dataLength - 1;
            int i2 = this.m_data[i];
            if (i2 == 0) {
                return false;
            }
            int i3 = i2 - 1;
            int i4 = i - 2;
            this.m_data[i4] = i3;
            this.m_data[i4 - ((i3 * 2) + 1)] = i3;
            this.m_dataLength -= 2;
            this.m_count--;
            return true;
        }

        public final boolean pop(int i, int i2) {
            if (this.m_dataLength == 0) {
                return false;
            }
            int i3 = this.m_dataLength - 1;
            int i4 = this.m_data[i3];
            int i5 = i3 - 2;
            int i6 = 0;
            while (i6 != i4) {
                if (this.m_data[i5] == i && this.m_data[i5 + 1] == i2) {
                    int i7 = i4 - 1;
                    if (i6 == 0) {
                        this.m_data[i5] = i7;
                        this.m_data[i5 - ((i7 * 2) + 1)] = i7;
                    } else {
                        this.m_data[i3] = i7;
                        this.m_data[i3 - ((i7 * 2) + 3)] = i7;
                        java.lang.System.arraycopy(this.m_data, i5 + 2, this.m_data, i5, this.m_dataLength - i5);
                    }
                    this.m_dataLength -= 2;
                    this.m_count--;
                    return true;
                }
                i6++;
                i5 -= 2;
            }
            return false;
        }

        public final void push(int i, int i2) {
            if (this.m_depth == 0) {
                increaseDepth();
            }
            ensureDataCapacity(2);
            int i3 = this.m_dataLength - 1;
            int i4 = this.m_data[i3];
            this.m_data[(i3 - 1) - (i4 * 2)] = i4 + 1;
            this.m_data[i3] = i;
            this.m_data[i3 + 1] = i2;
            this.m_data[i3 + 2] = i4 + 1;
            this.m_dataLength += 2;
            this.m_count++;
        }

        public final void reset() {
            this.m_dataLength = 0;
            this.m_count = 0;
            this.m_depth = 0;
        }
    }

    public AXmlResourceParser() {
        this.mAttrDecoder = new brut.androlib.res.decoder.ResAttrDecoder();
        this.m_operational = false;
        this.m_namespaces = new brut.androlib.res.decoder.AXmlResourceParser.NamespaceStack();
        resetEventInfo();
    }

    public AXmlResourceParser(java.io.InputStream inputStream) {
        this();
        open(inputStream);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00c1, code lost:
        throw new java.io.IOException("Invalid chunk type (" + r1 + ").");
     */
    private final void doNext() throws java.io.IOException {
        int readInt;
        if (this.m_strings == null) {
            this.m_reader.skipCheckInt(CHUNK_AXML_FILE);
            this.m_reader.skipInt();
            this.m_strings = brut.androlib.res.decoder.StringBlock.read(this.m_reader);
            this.m_namespaces.increaseDepth();
            this.m_operational = true;
        }
        if (this.m_event != 1) {
            int i = this.m_event;
            resetEventInfo();
            while (true) {
                if (this.m_decreaseDepth) {
                    this.m_decreaseDepth = false;
                    this.m_namespaces.decreaseDepth();
                }
                if (i == 3 && this.m_namespaces.getDepth() == 1 && this.m_namespaces.getCurrentCount() == 0) {
                    this.m_event = 1;
                    return;
                }
                int readInt2 = i == 0 ? CHUNK_XML_START_TAG : this.m_reader.readInt();
                if (readInt2 == CHUNK_RESOURCEIDS) {
                    readInt = this.m_reader.readInt();
                    if (readInt >= 8 && readInt % 4 == 0) {
                        this.m_resourceIDs = this.m_reader.readIntArray((readInt / 4) - 2);
                    }
                } else if (readInt2 >= 1048832 && readInt2 <= 1048836) {
                    if (readInt2 == CHUNK_XML_START_TAG && i == -1) {
                        this.m_event = 0;
                        return;
                    }
                    this.m_reader.skipInt();
                    int readInt3 = this.m_reader.readInt();
                    this.m_reader.skipInt();
                    if (readInt2 != 1048832 && readInt2 != CHUNK_XML_END_NAMESPACE) {
                        this.m_lineNumber = readInt3;
                        if (readInt2 == CHUNK_XML_START_TAG) {
                            this.m_namespaceUri = this.m_reader.readInt();
                            this.m_name = this.m_reader.readInt();
                            this.m_reader.skipInt();
                            int readInt4 = this.m_reader.readInt();
                            this.m_idAttribute = (readInt4 >>> 16) - 1;
                            int i2 = readInt4 & android.support.v4.internal.view.SupportMenu.USER_MASK;
                            this.m_classAttribute = this.m_reader.readInt();
                            this.m_styleAttribute = (this.m_classAttribute >>> 16) - 1;
                            this.m_classAttribute = (this.m_classAttribute & android.support.v4.internal.view.SupportMenu.USER_MASK) - 1;
                            this.m_attributes = this.m_reader.readIntArray(i2 * 5);
                            for (int i3 = 3; i3 < this.m_attributes.length; i3 += 5) {
                                this.m_attributes[i3] = this.m_attributes[i3] >>> 24;
                            }
                            this.m_namespaces.increaseDepth();
                            this.m_event = 2;
                            return;
                        } else if (readInt2 == CHUNK_XML_END_TAG) {
                            this.m_namespaceUri = this.m_reader.readInt();
                            this.m_name = this.m_reader.readInt();
                            this.m_event = 3;
                            this.m_decreaseDepth = true;
                            return;
                        } else if (readInt2 == 1048836) {
                            this.m_name = this.m_reader.readInt();
                            this.m_reader.skipInt();
                            this.m_reader.skipInt();
                            this.m_event = 4;
                            return;
                        }
                    } else if (readInt2 == 1048832) {
                        this.m_namespaces.push(this.m_reader.readInt(), this.m_reader.readInt());
                    } else {
                        this.m_reader.skipInt();
                        this.m_reader.skipInt();
                        this.m_namespaces.pop();
                    }
                }
            }
            throw new java.io.IOException("Invalid resource ids size (" + readInt + ").");
        }
    }

    private final int findAttribute(java.lang.String str, java.lang.String str2) {
        if (this.m_strings == null || str2 == null) {
            return -1;
        }
        int find = this.m_strings.find(str2);
        if (find == -1) {
            return -1;
        }
        int i = str != null ? this.m_strings.find(str) : -1;
        for (int i2 = 0; i2 != this.m_attributes.length; i2++) {
            if (find == this.m_attributes[i2 + 1] && (i == -1 || i == this.m_attributes[i2 + 0])) {
                return i2 / 5;
            }
        }
        return -1;
    }

    private final int getAttributeOffset(int i) {
        if (this.m_event != 2) {
            throw new java.lang.IndexOutOfBoundsException("Current event is not START_TAG.");
        }
        int i2 = i * 5;
        if (i2 < this.m_attributes.length) {
            return i2;
        }
        throw new java.lang.IndexOutOfBoundsException("Invalid attribute index (" + i + ").");
    }

    private final void resetEventInfo() {
        this.m_event = -1;
        this.m_lineNumber = -1;
        this.m_name = -1;
        this.m_namespaceUri = -1;
        this.m_attributes = null;
        this.m_idAttribute = -1;
        this.m_classAttribute = -1;
        this.m_styleAttribute = -1;
    }

    private void setFirstError(brut.androlib.AndrolibException androlibException) {
        if (this.mFirstError == null) {
            this.mFirstError = androlibException;
        }
    }

    public void close() {
        if (this.m_operational) {
            this.m_operational = false;
            this.m_reader = null;
            this.m_strings = null;
            this.m_resourceIDs = null;
            this.m_namespaces.reset();
            resetEventInfo();
        }
    }

    public void defineEntityReplacementText(java.lang.String str, java.lang.String str2) throws org.xmlpull.v1.XmlPullParserException {
        throw new org.xmlpull.v1.XmlPullParserException(E_NOT_SUPPORTED);
    }

    public brut.androlib.res.decoder.ResAttrDecoder getAttrDecoder() {
        return this.mAttrDecoder;
    }

    public boolean getAttributeBooleanValue(int i, boolean z) {
        return getAttributeIntValue(i, z ? 1 : 0) != 0;
    }

    public boolean getAttributeBooleanValue(java.lang.String str, java.lang.String str2, boolean z) {
        int findAttribute = findAttribute(str, str2);
        return findAttribute == -1 ? z : getAttributeBooleanValue(findAttribute, z);
    }

    public int getAttributeCount() {
        if (this.m_event != 2) {
            return -1;
        }
        return this.m_attributes.length / 5;
    }

    public float getAttributeFloatValue(int i, float f) {
        int attributeOffset = getAttributeOffset(i);
        return this.m_attributes[attributeOffset + 3] == 4 ? java.lang.Float.intBitsToFloat(this.m_attributes[attributeOffset + 4]) : f;
    }

    public float getAttributeFloatValue(java.lang.String str, java.lang.String str2, float f) {
        int findAttribute = findAttribute(str, str2);
        return findAttribute == -1 ? f : getAttributeFloatValue(findAttribute, f);
    }

    public int getAttributeIntValue(int i, int i2) {
        int attributeOffset = getAttributeOffset(i);
        int i3 = this.m_attributes[attributeOffset + 3];
        return (i3 < 16 || i3 > 31) ? i2 : this.m_attributes[attributeOffset + 4];
    }

    public int getAttributeIntValue(java.lang.String str, java.lang.String str2, int i) {
        int findAttribute = findAttribute(str, str2);
        return findAttribute == -1 ? i : getAttributeIntValue(findAttribute, i);
    }

    public int getAttributeListValue(int i, java.lang.String[] strArr, int i2) {
        return 0;
    }

    public int getAttributeListValue(java.lang.String str, java.lang.String str2, java.lang.String[] strArr, int i) {
        return 0;
    }

    public java.lang.String getAttributeName(int i) {
        int i2 = this.m_attributes[getAttributeOffset(i) + 1];
        return i2 == -1 ? "" : this.m_strings.getString(i2);
    }

    public int getAttributeNameResource(int i) {
        int i2 = this.m_attributes[getAttributeOffset(i) + 1];
        if (this.m_resourceIDs == null || i2 < 0 || i2 >= this.m_resourceIDs.length) {
            return 0;
        }
        return this.m_resourceIDs[i2];
    }

    public java.lang.String getAttributeNamespace(int i) {
        int i2 = this.m_attributes[getAttributeOffset(i) + 0];
        return i2 == -1 ? "" : this.m_strings.getString(i2);
    }

    public java.lang.String getAttributePrefix(int i) {
        int findPrefix = this.m_namespaces.findPrefix(this.m_attributes[getAttributeOffset(i) + 0]);
        return findPrefix == -1 ? "" : this.m_strings.getString(findPrefix);
    }

    public int getAttributeResourceValue(int i, int i2) {
        int attributeOffset = getAttributeOffset(i);
        return this.m_attributes[attributeOffset + 3] == 1 ? this.m_attributes[attributeOffset + 4] : i2;
    }

    public int getAttributeResourceValue(java.lang.String str, java.lang.String str2, int i) {
        int findAttribute = findAttribute(str, str2);
        return findAttribute == -1 ? i : getAttributeResourceValue(findAttribute, i);
    }

    public java.lang.String getAttributeType(int i) {
        return "CDATA";
    }

    public int getAttributeUnsignedIntValue(int i, int i2) {
        return getAttributeIntValue(i, i2);
    }

    public int getAttributeUnsignedIntValue(java.lang.String str, java.lang.String str2, int i) {
        int findAttribute = findAttribute(str, str2);
        return findAttribute == -1 ? i : getAttributeUnsignedIntValue(findAttribute, i);
    }

    public java.lang.String getAttributeValue(int i) {
        int attributeOffset = getAttributeOffset(i);
        int i2 = this.m_attributes[attributeOffset + 3];
        int i3 = this.m_attributes[attributeOffset + 4];
        int i4 = this.m_attributes[attributeOffset + 2];
        if (this.mAttrDecoder != null) {
            try {
                return this.mAttrDecoder.decode(i2, i3, i4 == -1 ? null : brut.androlib.res.xml.ResXmlEncoders.escapeXmlChars(this.m_strings.getString(i4)), getAttributeNameResource(i));
            } catch (brut.androlib.AndrolibException e) {
                setFirstError(e);
                LOGGER.log(java.util.logging.Level.WARNING, java.lang.String.format("Could not decode attr value, using undecoded value instead: ns=%s, name=%s, value=0x%08x", new java.lang.Object[]{getAttributePrefix(i), getAttributeName(i), java.lang.Integer.valueOf(i3)}), e);
            }
        }
        return android.util.TypedValue.coerceToString(i2, i3);
    }

    public java.lang.String getAttributeValue(java.lang.String str, java.lang.String str2) {
        int findAttribute = findAttribute(str, str2);
        if (findAttribute == -1) {
            return null;
        }
        return getAttributeValue(findAttribute);
    }

    public int getAttributeValueData(int i) {
        return this.m_attributes[getAttributeOffset(i) + 4];
    }

    public int getAttributeValueType(int i) {
        return this.m_attributes[getAttributeOffset(i) + 3];
    }

    public java.lang.String getClassAttribute() {
        if (this.m_classAttribute == -1) {
            return null;
        }
        return this.m_strings.getString(this.m_attributes[getAttributeOffset(this.m_classAttribute) + 2]);
    }

    public int getColumnNumber() {
        return -1;
    }

    public int getDepth() {
        return this.m_namespaces.getDepth() - 1;
    }

    public int getEventType() throws org.xmlpull.v1.XmlPullParserException {
        return this.m_event;
    }

    public boolean getFeature(java.lang.String str) {
        return false;
    }

    public brut.androlib.AndrolibException getFirstError() {
        return this.mFirstError;
    }

    public java.lang.String getIdAttribute() {
        if (this.m_idAttribute == -1) {
            return null;
        }
        return this.m_strings.getString(this.m_attributes[getAttributeOffset(this.m_idAttribute) + 2]);
    }

    public int getIdAttributeResourceValue(int i) {
        if (this.m_idAttribute == -1) {
            return i;
        }
        int attributeOffset = getAttributeOffset(this.m_idAttribute);
        return this.m_attributes[attributeOffset + 3] == 1 ? this.m_attributes[attributeOffset + 4] : i;
    }

    public java.lang.String getInputEncoding() {
        return null;
    }

    public int getLineNumber() {
        return this.m_lineNumber;
    }

    public java.lang.String getName() {
        if (this.m_name == -1 || (this.m_event != 2 && this.m_event != 3)) {
            return null;
        }
        return this.m_strings.getString(this.m_name);
    }

    public java.lang.String getNamespace() {
        return this.m_strings.getString(this.m_namespaceUri);
    }

    public java.lang.String getNamespace(java.lang.String str) {
        throw new java.lang.RuntimeException(E_NOT_SUPPORTED);
    }

    public int getNamespaceCount(int i) throws org.xmlpull.v1.XmlPullParserException {
        return this.m_namespaces.getAccumulatedCount(i);
    }

    public java.lang.String getNamespacePrefix(int i) throws org.xmlpull.v1.XmlPullParserException {
        return this.m_strings.getString(this.m_namespaces.getPrefix(i));
    }

    public java.lang.String getNamespaceUri(int i) throws org.xmlpull.v1.XmlPullParserException {
        return this.m_strings.getString(this.m_namespaces.getUri(i));
    }

    public java.lang.String getPositionDescription() {
        return "XML line #" + getLineNumber();
    }

    public java.lang.String getPrefix() {
        return this.m_strings.getString(this.m_namespaces.findPrefix(this.m_namespaceUri));
    }

    public java.lang.Object getProperty(java.lang.String str) {
        return null;
    }

    /* access modifiers changed from: 0000 */
    public final brut.androlib.res.decoder.StringBlock getStrings() {
        return this.m_strings;
    }

    public int getStyleAttribute() {
        if (this.m_styleAttribute == -1) {
            return 0;
        }
        return this.m_attributes[getAttributeOffset(this.m_styleAttribute) + 4];
    }

    public java.lang.String getText() {
        if (this.m_name == -1 || this.m_event != 4) {
            return null;
        }
        return this.m_strings.getString(this.m_name);
    }

    public char[] getTextCharacters(int[] iArr) {
        java.lang.String text = getText();
        if (text == null) {
            return null;
        }
        iArr[0] = 0;
        iArr[1] = text.length();
        char[] cArr = new char[text.length()];
        text.getChars(0, text.length(), cArr, 0);
        return cArr;
    }

    public boolean isAttributeDefault(int i) {
        return false;
    }

    public boolean isEmptyElementTag() throws org.xmlpull.v1.XmlPullParserException {
        return false;
    }

    public boolean isWhitespace() throws org.xmlpull.v1.XmlPullParserException {
        return false;
    }

    public int next() throws org.xmlpull.v1.XmlPullParserException, java.io.IOException {
        if (this.m_reader == null) {
            throw new org.xmlpull.v1.XmlPullParserException("Parser is not opened.", this, null);
        }
        try {
            doNext();
            return this.m_event;
        } catch (java.io.IOException e) {
            close();
            throw e;
        }
    }

    public int nextTag() throws org.xmlpull.v1.XmlPullParserException, java.io.IOException {
        int next = next();
        if (next == 4 && isWhitespace()) {
            next = next();
        }
        if (next == 2 || next == 3) {
            return next;
        }
        throw new org.xmlpull.v1.XmlPullParserException("Expected start or end tag.", this, null);
    }

    public java.lang.String nextText() throws org.xmlpull.v1.XmlPullParserException, java.io.IOException {
        if (getEventType() != 2) {
            throw new org.xmlpull.v1.XmlPullParserException("Parser must be on START_TAG to read next text.", this, null);
        }
        int next = next();
        if (next == 4) {
            java.lang.String text = getText();
            if (next() == 3) {
                return text;
            }
            throw new org.xmlpull.v1.XmlPullParserException("Event TEXT must be immediately followed by END_TAG.", this, null);
        } else if (next == 3) {
            return "";
        } else {
            throw new org.xmlpull.v1.XmlPullParserException("Parser must be on START_TAG or TEXT to read text.", this, null);
        }
    }

    public int nextToken() throws org.xmlpull.v1.XmlPullParserException, java.io.IOException {
        return next();
    }

    public void open(java.io.InputStream inputStream) {
        close();
        if (inputStream != null) {
            this.m_reader = new brut.util.ExtDataInput((java.io.DataInput) new com.mindprod.ledatastream.LEDataInputStream(inputStream));
        }
    }

    public void require(int i, java.lang.String str, java.lang.String str2) throws org.xmlpull.v1.XmlPullParserException, java.io.IOException {
        if (i != getEventType() || ((str != null && !str.equals(getNamespace())) || (str2 != null && !str2.equals(getName())))) {
            throw new org.xmlpull.v1.XmlPullParserException(TYPES[i] + " is expected.", this, null);
        }
    }

    public void setAttrDecoder(brut.androlib.res.decoder.ResAttrDecoder resAttrDecoder) {
        this.mAttrDecoder = resAttrDecoder;
    }

    public void setFeature(java.lang.String str, boolean z) throws org.xmlpull.v1.XmlPullParserException {
        throw new org.xmlpull.v1.XmlPullParserException(E_NOT_SUPPORTED);
    }

    public void setInput(java.io.InputStream inputStream, java.lang.String str) throws org.xmlpull.v1.XmlPullParserException {
        open(inputStream);
    }

    public void setInput(java.io.Reader reader) throws org.xmlpull.v1.XmlPullParserException {
        throw new org.xmlpull.v1.XmlPullParserException(E_NOT_SUPPORTED);
    }

    public void setProperty(java.lang.String str, java.lang.Object obj) throws org.xmlpull.v1.XmlPullParserException {
        throw new org.xmlpull.v1.XmlPullParserException(E_NOT_SUPPORTED);
    }
}
