package ch.qos.logback.core;

public class FileAppender<E> extends ch.qos.logback.core.OutputStreamAppender<E> {
    protected boolean append = true;
    protected java.lang.String fileName = null;
    private boolean initialized = false;
    private boolean lazyInit = false;
    private boolean prudent = false;

    private java.lang.String getAbsoluteFilePath(java.lang.String str) {
        return ch.qos.logback.core.util.EnvUtil.isAndroidOS() ? ch.qos.logback.core.util.FileUtil.prefixRelativePath(this.context.getProperty(ch.qos.logback.core.CoreConstants.DATA_DIR_KEY), str) : str;
    }

    /* JADX INFO: finally extract failed */
    private void safeWrite(E e) throws java.io.IOException {
        java.nio.channels.FileChannel channel = ((ch.qos.logback.core.recovery.ResilientFileOutputStream) getOutputStream()).getChannel();
        if (channel != null) {
            java.nio.channels.FileLock fileLock = null;
            try {
                java.nio.channels.FileLock lock = channel.lock();
                long position = channel.position();
                long size = channel.size();
                if (size != position) {
                    channel.position(size);
                }
                super.writeOut(e);
                if (lock != null) {
                    lock.release();
                }
            } catch (Throwable th) {
                if (fileLock != null) {
                    fileLock.release();
                }
                throw th;
            }
        }
    }

    public java.lang.String getFile() {
        return this.fileName;
    }

    public boolean getLazy() {
        return this.lazyInit;
    }

    public boolean isAppend() {
        return this.append;
    }

    public boolean isPrudent() {
        return this.prudent;
    }

    /* access modifiers changed from: protected */
    public boolean openFile(java.lang.String str) throws java.io.IOException {
        java.lang.String absoluteFilePath = getAbsoluteFilePath(str);
        synchronized (this.lock) {
            java.io.File file = new java.io.File(absoluteFilePath);
            if (ch.qos.logback.core.util.FileUtil.isParentDirectoryCreationRequired(file) && !ch.qos.logback.core.util.FileUtil.createMissingParentDirectories(file)) {
                addError("Failed to create parent directories for [" + file.getAbsolutePath() + "]");
            }
            ch.qos.logback.core.recovery.ResilientFileOutputStream resilientFileOutputStream = new ch.qos.logback.core.recovery.ResilientFileOutputStream(file, this.append);
            resilientFileOutputStream.setContext(this.context);
            setOutputStream(resilientFileOutputStream);
        }
        return true;
    }

    public final java.lang.String rawFileProperty() {
        return this.fileName;
    }

    public void setAppend(boolean z) {
        this.append = z;
    }

    public void setFile(java.lang.String str) {
        if (str == null) {
            this.fileName = null;
        } else {
            this.fileName = str.trim();
        }
    }

    public void setLazy(boolean z) {
        this.lazyInit = z;
    }

    public void setPrudent(boolean z) {
        this.prudent = z;
    }

    public void start() {
        boolean z = false;
        java.lang.String file = getFile();
        if (file != null) {
            java.lang.String absoluteFilePath = getAbsoluteFilePath(file);
            addInfo("File property is set to [" + absoluteFilePath + "]");
            if (this.prudent && !isAppend()) {
                setAppend(true);
                addWarn("Setting \"Append\" property to true on account of \"Prudent\" mode");
            }
            if (!this.lazyInit) {
                try {
                    openFile(absoluteFilePath);
                } catch (java.io.IOException e) {
                    addError("openFile(" + absoluteFilePath + "," + this.append + ") failed", e);
                    z = true;
                }
            } else {
                setOutputStream(new ch.qos.logback.core.NOPOutputStream());
            }
        } else {
            addError("\"File\" property not set for appender named [" + this.name + "]");
            z = true;
        }
        if (!z) {
            super.start();
        }
    }

    /* access modifiers changed from: protected */
    public void subAppend(E e) {
        if (!this.initialized && this.lazyInit) {
            this.initialized = true;
            try {
                openFile(getFile());
            } catch (java.io.IOException e2) {
                this.started = false;
                addError("openFile(" + this.fileName + "," + this.append + ") failed", e2);
            }
        }
        super.subAppend(e);
    }

    /* access modifiers changed from: protected */
    public void writeOut(E e) throws java.io.IOException {
        if (this.prudent) {
            safeWrite(e);
        } else {
            super.writeOut(e);
        }
    }
}
