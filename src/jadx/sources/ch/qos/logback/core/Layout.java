package ch.qos.logback.core;

public interface Layout<E> extends ch.qos.logback.core.spi.ContextAware, ch.qos.logback.core.spi.LifeCycle {
    java.lang.String doLayout(E e);

    java.lang.String getContentType();

    java.lang.String getFileFooter();

    java.lang.String getFileHeader();

    java.lang.String getPresentationFooter();

    java.lang.String getPresentationHeader();
}
