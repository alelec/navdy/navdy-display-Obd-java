package ch.qos.logback.core.read;

public class CyclicBufferAppender<E> extends ch.qos.logback.core.AppenderBase<E> {
    ch.qos.logback.core.helpers.CyclicBuffer<E> cb;
    int maxSize = 512;

    /* access modifiers changed from: protected */
    public void append(E e) {
        if (isStarted()) {
            this.cb.add(e);
        }
    }

    public E get(int i) {
        if (isStarted()) {
            return this.cb.get(i);
        }
        return null;
    }

    public int getLength() {
        if (isStarted()) {
            return this.cb.length();
        }
        return 0;
    }

    public int getMaxSize() {
        return this.maxSize;
    }

    public void reset() {
        this.cb.clear();
    }

    public void setMaxSize(int i) {
        this.maxSize = i;
    }

    public void start() {
        this.cb = new ch.qos.logback.core.helpers.CyclicBuffer<>(this.maxSize);
        super.start();
    }

    public void stop() {
        this.cb = null;
        super.stop();
    }
}
