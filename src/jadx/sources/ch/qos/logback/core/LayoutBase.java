package ch.qos.logback.core;

public abstract class LayoutBase<E> extends ch.qos.logback.core.spi.ContextAwareBase implements ch.qos.logback.core.Layout<E> {
    java.lang.String fileFooter;
    java.lang.String fileHeader;
    java.lang.String presentationFooter;
    java.lang.String presentationHeader;
    protected boolean started;

    public java.lang.String getContentType() {
        return "text/plain";
    }

    public ch.qos.logback.core.Context getContext() {
        return this.context;
    }

    public java.lang.String getFileFooter() {
        return this.fileFooter;
    }

    public java.lang.String getFileHeader() {
        return this.fileHeader;
    }

    public java.lang.String getPresentationFooter() {
        return this.presentationFooter;
    }

    public java.lang.String getPresentationHeader() {
        return this.presentationHeader;
    }

    public boolean isStarted() {
        return this.started;
    }

    public void setContext(ch.qos.logback.core.Context context) {
        this.context = context;
    }

    public void setFileFooter(java.lang.String str) {
        this.fileFooter = str;
    }

    public void setFileHeader(java.lang.String str) {
        this.fileHeader = str;
    }

    public void setPresentationFooter(java.lang.String str) {
        this.presentationFooter = str;
    }

    public void setPresentationHeader(java.lang.String str) {
        this.presentationHeader = str;
    }

    public void start() {
        this.started = true;
    }

    public void stop() {
        this.started = false;
    }
}
