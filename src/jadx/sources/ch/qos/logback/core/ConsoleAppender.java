package ch.qos.logback.core;

@java.lang.Deprecated
public class ConsoleAppender<E> extends ch.qos.logback.core.OutputStreamAppender<E> {
    protected ch.qos.logback.core.joran.spi.ConsoleTarget target = ch.qos.logback.core.joran.spi.ConsoleTarget.SystemOut;

    private void targetWarn(java.lang.String str) {
        ch.qos.logback.core.status.WarnStatus warnStatus = new ch.qos.logback.core.status.WarnStatus("[" + str + "] should be one of " + java.util.Arrays.toString(ch.qos.logback.core.joran.spi.ConsoleTarget.values()), this);
        warnStatus.add(new ch.qos.logback.core.status.WarnStatus("Using previously set target, System.out by default.", this));
        addStatus(warnStatus);
    }

    public java.lang.String getTarget() {
        return this.target.getName();
    }

    public void setTarget(java.lang.String str) {
        ch.qos.logback.core.joran.spi.ConsoleTarget findByName = ch.qos.logback.core.joran.spi.ConsoleTarget.findByName(str.trim());
        if (findByName == null) {
            targetWarn(str);
        } else {
            this.target = findByName;
        }
    }

    public void start() {
        setOutputStream(this.target.getStream());
        super.start();
    }
}
