package ch.qos.logback.core;

public abstract class UnsynchronizedAppenderBase<E> extends ch.qos.logback.core.spi.ContextAwareBase implements ch.qos.logback.core.Appender<E> {
    static final int ALLOWED_REPEATS = 3;
    private int exceptionCount = 0;
    private ch.qos.logback.core.spi.FilterAttachableImpl<E> fai = new ch.qos.logback.core.spi.FilterAttachableImpl<>();
    private java.lang.ThreadLocal<java.lang.Boolean> guard = new java.lang.ThreadLocal<>();
    protected java.lang.String name;
    protected boolean started = false;
    private int statusRepeatCount = 0;

    public void addFilter(ch.qos.logback.core.filter.Filter<E> filter) {
        this.fai.addFilter(filter);
    }

    /* access modifiers changed from: protected */
    public abstract void append(E e);

    public void clearAllFilters() {
        this.fai.clearAllFilters();
    }

    public void doAppend(E e) {
        if (!java.lang.Boolean.TRUE.equals(this.guard.get())) {
            try {
                this.guard.set(java.lang.Boolean.TRUE);
                if (!this.started) {
                    int i = this.statusRepeatCount;
                    this.statusRepeatCount = i + 1;
                    if (i < 3) {
                        addStatus(new ch.qos.logback.core.status.WarnStatus("Attempted to append to non started appender [" + this.name + "].", this));
                    }
                } else if (getFilterChainDecision(e) == ch.qos.logback.core.spi.FilterReply.DENY) {
                    this.guard.set(java.lang.Boolean.FALSE);
                } else {
                    append(e);
                    this.guard.set(java.lang.Boolean.FALSE);
                }
            } catch (java.lang.Exception e2) {
                int i2 = this.exceptionCount;
                this.exceptionCount = i2 + 1;
                if (i2 < 3) {
                    addError("Appender [" + this.name + "] failed to append.", e2);
                }
            } finally {
                this.guard.set(java.lang.Boolean.FALSE);
            }
        }
    }

    public java.util.List<ch.qos.logback.core.filter.Filter<E>> getCopyOfAttachedFiltersList() {
        return this.fai.getCopyOfAttachedFiltersList();
    }

    public ch.qos.logback.core.spi.FilterReply getFilterChainDecision(E e) {
        return this.fai.getFilterChainDecision(e);
    }

    public java.lang.String getName() {
        return this.name;
    }

    public boolean isStarted() {
        return this.started;
    }

    public void setName(java.lang.String str) {
        this.name = str;
    }

    public void start() {
        this.started = true;
    }

    public void stop() {
        this.started = false;
    }

    public java.lang.String toString() {
        return getClass().getName() + "[" + this.name + "]";
    }
}
