package ch.qos.logback.core;

public interface Context extends ch.qos.logback.core.spi.PropertyContainer {
    long getBirthTime();

    java.lang.Object getConfigurationLock();

    java.util.Map<java.lang.String, java.lang.String> getCopyOfPropertyMap();

    java.util.concurrent.ExecutorService getExecutorService();

    java.lang.String getName();

    java.lang.Object getObject(java.lang.String str);

    java.lang.String getProperty(java.lang.String str);

    ch.qos.logback.core.status.StatusManager getStatusManager();

    void putObject(java.lang.String str, java.lang.Object obj);

    void putProperty(java.lang.String str, java.lang.String str2);

    void register(ch.qos.logback.core.spi.LifeCycle lifeCycle);

    void setName(java.lang.String str);
}
