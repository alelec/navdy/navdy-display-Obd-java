package ch.qos.logback.core;

public class ContextBase implements ch.qos.logback.core.Context, ch.qos.logback.core.spi.LifeCycle {
    private long birthTime = java.lang.System.currentTimeMillis();
    ch.qos.logback.core.spi.LogbackLock configurationLock = new ch.qos.logback.core.spi.LogbackLock();
    private volatile java.util.concurrent.ExecutorService executorService;
    private ch.qos.logback.core.LifeCycleManager lifeCycleManager;
    private java.lang.String name;
    java.util.Map<java.lang.String, java.lang.Object> objectMap = new java.util.HashMap();
    java.util.Map<java.lang.String, java.lang.String> propertyMap = new java.util.HashMap();
    private ch.qos.logback.core.status.StatusManager sm = new ch.qos.logback.core.BasicStatusManager();
    private boolean started;

    private synchronized void stopExecutorService() {
        if (this.executorService != null) {
            ch.qos.logback.core.util.ExecutorServiceUtil.shutdown(this.executorService);
            this.executorService = null;
        }
    }

    public long getBirthTime() {
        return this.birthTime;
    }

    public java.lang.Object getConfigurationLock() {
        return this.configurationLock;
    }

    public java.util.Map<java.lang.String, java.lang.String> getCopyOfPropertyMap() {
        return new java.util.HashMap(this.propertyMap);
    }

    public java.util.concurrent.ExecutorService getExecutorService() {
        if (this.executorService == null) {
            synchronized (this) {
                if (this.executorService == null) {
                    this.executorService = ch.qos.logback.core.util.ExecutorServiceUtil.newExecutorService();
                }
            }
        }
        return this.executorService;
    }

    /* access modifiers changed from: 0000 */
    public synchronized ch.qos.logback.core.LifeCycleManager getLifeCycleManager() {
        if (this.lifeCycleManager == null) {
            this.lifeCycleManager = new ch.qos.logback.core.LifeCycleManager();
        }
        return this.lifeCycleManager;
    }

    public java.lang.String getName() {
        return this.name;
    }

    public java.lang.Object getObject(java.lang.String str) {
        return this.objectMap.get(str);
    }

    public java.lang.String getProperty(java.lang.String str) {
        return ch.qos.logback.core.CoreConstants.CONTEXT_NAME_KEY.equals(str) ? getName() : (java.lang.String) this.propertyMap.get(str);
    }

    public ch.qos.logback.core.status.StatusManager getStatusManager() {
        return this.sm;
    }

    public boolean isStarted() {
        return this.started;
    }

    public void putObject(java.lang.String str, java.lang.Object obj) {
        this.objectMap.put(str, obj);
    }

    public void putProperty(java.lang.String str, java.lang.String str2) {
        this.propertyMap.put(str, str2);
    }

    public void register(ch.qos.logback.core.spi.LifeCycle lifeCycle) {
        getLifeCycleManager().register(lifeCycle);
    }

    public void reset() {
        getLifeCycleManager().reset();
        this.propertyMap.clear();
        this.objectMap.clear();
    }

    public void setName(java.lang.String str) throws java.lang.IllegalStateException {
        if (str != null && str.equals(this.name)) {
            return;
        }
        if (this.name == null || "default".equals(this.name)) {
            this.name = str;
            return;
        }
        throw new java.lang.IllegalStateException("Context has been already given a name");
    }

    public void setStatusManager(ch.qos.logback.core.status.StatusManager statusManager) {
        if (statusManager == null) {
            throw new java.lang.IllegalArgumentException("null StatusManager not allowed");
        }
        this.sm = statusManager;
    }

    public void start() {
        this.started = true;
    }

    public void stop() {
        stopExecutorService();
        this.started = false;
    }

    public java.lang.String toString() {
        return this.name;
    }
}
