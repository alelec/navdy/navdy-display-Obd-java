package ch.qos.logback.core;

public class OutputStreamAppender<E> extends ch.qos.logback.core.UnsynchronizedAppenderBase<E> {
    protected ch.qos.logback.core.encoder.Encoder<E> encoder;
    protected ch.qos.logback.core.spi.LogbackLock lock = new ch.qos.logback.core.spi.LogbackLock();
    private java.io.OutputStream outputStream;

    /* access modifiers changed from: protected */
    public void append(E e) {
        if (isStarted()) {
            subAppend(e);
        }
    }

    /* access modifiers changed from: protected */
    public void closeOutputStream() {
        if (this.outputStream != null) {
            try {
                encoderClose();
                this.outputStream.close();
                this.outputStream = null;
            } catch (java.io.IOException e) {
                addStatus(new ch.qos.logback.core.status.ErrorStatus("Could not close output stream for OutputStreamAppender.", this, e));
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void encoderClose() {
        if (this.encoder != null && this.outputStream != null) {
            try {
                this.encoder.close();
            } catch (java.io.IOException e) {
                this.started = false;
                addStatus(new ch.qos.logback.core.status.ErrorStatus("Failed to write footer for appender named [" + this.name + "].", this, e));
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void encoderInit() {
        if (this.encoder != null && this.outputStream != null) {
            try {
                this.encoder.init(this.outputStream);
            } catch (java.io.IOException e) {
                this.started = false;
                addStatus(new ch.qos.logback.core.status.ErrorStatus("Failed to initialize encoder for appender named [" + this.name + "].", this, e));
            }
        }
    }

    public ch.qos.logback.core.encoder.Encoder<E> getEncoder() {
        return this.encoder;
    }

    public java.io.OutputStream getOutputStream() {
        return this.outputStream;
    }

    public void setEncoder(ch.qos.logback.core.encoder.Encoder<E> encoder2) {
        this.encoder = encoder2;
    }

    public void setLayout(ch.qos.logback.core.Layout<E> layout) {
        addWarn("This appender no longer admits a layout as a sub-component, set an encoder instead.");
        addWarn("To ensure compatibility, wrapping your layout in LayoutWrappingEncoder.");
        addWarn("See also http://logback.qos.ch/codes.html#layoutInsteadOfEncoder for details");
        ch.qos.logback.core.encoder.LayoutWrappingEncoder layoutWrappingEncoder = new ch.qos.logback.core.encoder.LayoutWrappingEncoder();
        layoutWrappingEncoder.setLayout(layout);
        layoutWrappingEncoder.setContext(this.context);
        this.encoder = layoutWrappingEncoder;
    }

    public void setOutputStream(java.io.OutputStream outputStream2) {
        synchronized (this.lock) {
            closeOutputStream();
            this.outputStream = outputStream2;
            if (this.encoder == null) {
                addWarn("Encoder has not been set. Cannot invoke its init method.");
            } else {
                encoderInit();
            }
        }
    }

    public void start() {
        int i = 0;
        if (this.encoder == null) {
            addStatus(new ch.qos.logback.core.status.ErrorStatus("No encoder set for the appender named \"" + this.name + "\".", this));
            i = 1;
        }
        if (this.outputStream == null) {
            addStatus(new ch.qos.logback.core.status.ErrorStatus("No output stream set for the appender named \"" + this.name + "\".", this));
            i++;
        }
        if (i == 0) {
            super.start();
        }
    }

    public void stop() {
        synchronized (this.lock) {
            closeOutputStream();
            super.stop();
        }
    }

    /* access modifiers changed from: protected */
    public void subAppend(E e) {
        if (isStarted()) {
            try {
                if (e instanceof ch.qos.logback.core.spi.DeferredProcessingAware) {
                    ((ch.qos.logback.core.spi.DeferredProcessingAware) e).prepareForDeferredProcessing();
                }
                synchronized (this.lock) {
                    writeOut(e);
                }
            } catch (java.io.IOException e2) {
                this.started = false;
                addStatus(new ch.qos.logback.core.status.ErrorStatus("IO failure in appender", this, e2));
            }
        }
    }

    /* access modifiers changed from: protected */
    public void writeOut(E e) throws java.io.IOException {
        this.encoder.doEncode(e);
    }
}
