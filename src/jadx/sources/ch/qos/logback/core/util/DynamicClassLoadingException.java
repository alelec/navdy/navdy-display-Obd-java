package ch.qos.logback.core.util;

public class DynamicClassLoadingException extends java.lang.Exception {
    private static final long serialVersionUID = 4962278449162476114L;

    public DynamicClassLoadingException(java.lang.String str, java.lang.Throwable th) {
        super(str, th);
    }
}
