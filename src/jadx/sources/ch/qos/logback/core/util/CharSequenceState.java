package ch.qos.logback.core.util;

class CharSequenceState {
    final char c;
    int occurrences = 1;

    public CharSequenceState(char c2) {
        this.c = c2;
    }

    /* access modifiers changed from: 0000 */
    public void incrementOccurrences() {
        this.occurrences++;
    }
}
