package ch.qos.logback.core.util;

public class ExecutorServiceUtil {
    public static java.util.concurrent.ExecutorService newExecutorService() {
        return new java.util.concurrent.ThreadPoolExecutor(ch.qos.logback.core.CoreConstants.CORE_POOL_SIZE, 32, 0, java.util.concurrent.TimeUnit.MILLISECONDS, new java.util.concurrent.SynchronousQueue());
    }

    public static void shutdown(java.util.concurrent.ExecutorService executorService) {
        executorService.shutdownNow();
    }
}
