package ch.qos.logback.core.util;

public class CachingDateFormatter {
    java.lang.String cachedStr = null;
    long lastTimestamp = -1;
    final java.text.SimpleDateFormat sdf;

    public CachingDateFormatter(java.lang.String str) {
        this.sdf = new java.text.SimpleDateFormat(str);
    }

    public final java.lang.String format(long j) {
        java.lang.String str;
        synchronized (this) {
            if (j != this.lastTimestamp) {
                this.lastTimestamp = j;
                this.cachedStr = this.sdf.format(new java.util.Date(j));
            }
            str = this.cachedStr;
        }
        return str;
    }

    public void setTimeZone(java.util.TimeZone timeZone) {
        this.sdf.setTimeZone(timeZone);
    }
}
