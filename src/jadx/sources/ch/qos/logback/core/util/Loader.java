package ch.qos.logback.core.util;

public class Loader {
    private static boolean HAS_GET_CLASS_LOADER_PERMISSION = false;
    public static final java.lang.String IGNORE_TCL_PROPERTY_NAME = "logback.ignoreTCL";
    static final java.lang.String TSTR = "Caught Exception while in Loader.getResource. This may be innocuous.";
    private static boolean ignoreTCL;

    static {
        ignoreTCL = false;
        HAS_GET_CLASS_LOADER_PERMISSION = false;
        java.lang.String systemProperty = ch.qos.logback.core.util.OptionHelper.getSystemProperty(IGNORE_TCL_PROPERTY_NAME, null);
        if (systemProperty != null) {
            ignoreTCL = java.lang.Boolean.valueOf(systemProperty).booleanValue();
        }
        HAS_GET_CLASS_LOADER_PERMISSION = ((java.lang.Boolean) java.security.AccessController.doPrivileged(new java.security.PrivilegedAction<java.lang.Boolean>() {
            public java.lang.Boolean run() {
                try {
                    java.security.AccessController.checkPermission(new java.lang.RuntimePermission("getClassLoader"));
                    return java.lang.Boolean.valueOf(true);
                } catch (java.lang.SecurityException e) {
                    return java.lang.Boolean.valueOf(false);
                }
            }
        })).booleanValue();
    }

    public static java.lang.ClassLoader getClassLoaderAsPrivileged(final java.lang.Class<?> cls) {
        if (!HAS_GET_CLASS_LOADER_PERMISSION) {
            return null;
        }
        return (java.lang.ClassLoader) java.security.AccessController.doPrivileged(new java.security.PrivilegedAction<java.lang.ClassLoader>() {
            public java.lang.ClassLoader run() {
                return cls.getClassLoader();
            }
        });
    }

    public static java.lang.ClassLoader getClassLoaderOfClass(java.lang.Class<?> cls) {
        java.lang.ClassLoader classLoader = cls.getClassLoader();
        return classLoader == null ? java.lang.ClassLoader.getSystemClassLoader() : classLoader;
    }

    public static java.lang.ClassLoader getClassLoaderOfObject(java.lang.Object obj) {
        if (obj != null) {
            return getClassLoaderOfClass(obj.getClass());
        }
        throw new java.lang.NullPointerException("Argument cannot be null");
    }

    public static java.net.URL getResource(java.lang.String str, java.lang.ClassLoader classLoader) {
        try {
            return classLoader.getResource(str);
        } catch (Throwable th) {
            return null;
        }
    }

    public static java.net.URL getResourceBySelfClassLoader(java.lang.String str) {
        return getResource(str, getClassLoaderOfClass(ch.qos.logback.core.util.Loader.class));
    }

    public static java.util.Set<java.net.URL> getResourceOccurrenceCount(java.lang.String str, java.lang.ClassLoader classLoader) throws java.io.IOException {
        java.util.HashSet hashSet = new java.util.HashSet();
        java.util.Enumeration resources = classLoader.getResources(str);
        while (resources.hasMoreElements()) {
            hashSet.add((java.net.URL) resources.nextElement());
        }
        return hashSet;
    }

    public static java.lang.ClassLoader getTCL() {
        return java.lang.Thread.currentThread().getContextClassLoader();
    }

    public static java.lang.Class<?> loadClass(java.lang.String str) throws java.lang.ClassNotFoundException {
        if (ignoreTCL) {
            return java.lang.Class.forName(str);
        }
        try {
            return getTCL().loadClass(str);
        } catch (Throwable th) {
            return java.lang.Class.forName(str);
        }
    }

    public static java.lang.Class<?> loadClass(java.lang.String str, ch.qos.logback.core.Context context) throws java.lang.ClassNotFoundException {
        return getClassLoaderOfObject(context).loadClass(str);
    }
}
