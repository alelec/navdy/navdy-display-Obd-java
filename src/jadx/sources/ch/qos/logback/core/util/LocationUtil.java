package ch.qos.logback.core.util;

public class LocationUtil {
    public static final java.lang.String CLASSPATH_SCHEME = "classpath:";
    public static final java.lang.String SCHEME_PATTERN = "^\\p{Alpha}[\\p{Alnum}+.-]*:.*$";

    public static java.net.URL urlForResource(java.lang.String str) throws java.net.MalformedURLException, java.io.FileNotFoundException {
        java.net.URL url;
        if (str == null) {
            throw new java.lang.NullPointerException("location is required");
        }
        if (!str.matches(SCHEME_PATTERN)) {
            url = ch.qos.logback.core.util.Loader.getResourceBySelfClassLoader(str);
        } else if (str.startsWith(CLASSPATH_SCHEME)) {
            java.lang.String substring = str.substring(CLASSPATH_SCHEME.length());
            if (substring.startsWith("/")) {
                substring = substring.substring(1);
            }
            if (substring.length() == 0) {
                throw new java.net.MalformedURLException("path is required");
            }
            url = ch.qos.logback.core.util.Loader.getResourceBySelfClassLoader(substring);
        } else {
            url = new java.net.URL(str);
        }
        if (url != null) {
            return url;
        }
        throw new java.io.FileNotFoundException(str);
    }
}
