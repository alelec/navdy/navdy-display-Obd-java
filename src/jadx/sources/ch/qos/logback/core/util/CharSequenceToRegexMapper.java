package ch.qos.logback.core.util;

class CharSequenceToRegexMapper {
    CharSequenceToRegexMapper() {
    }

    private java.lang.String number(int i) {
        return "\\d{" + i + "}";
    }

    /* access modifiers changed from: 0000 */
    public java.lang.String toRegex(ch.qos.logback.core.util.CharSequenceState charSequenceState) {
        int i = charSequenceState.occurrences;
        char c = charSequenceState.c;
        switch (charSequenceState.c) {
            case android.support.v4.view.MotionEventCompat.AXIS_GENERIC_8 /*39*/:
                if (i == 1) {
                    return "";
                }
                throw new java.lang.IllegalStateException("Too many single quotes");
            case android.support.v4.view.MotionEventCompat.AXIS_GENERIC_15 /*46*/:
                return "\\.";
            case 'D':
            case ch.qos.logback.core.CoreConstants.OOS_RESET_FREQUENCY /*70*/:
            case ch.qos.logback.core.net.SyslogConstants.LOG_CRON /*72*/:
            case 'K':
            case 'S':
            case 'W':
            case 'd':
            case ch.qos.logback.core.net.SyslogConstants.LOG_AUDIT /*104*/:
            case 'k':
            case 'm':
            case 's':
            case 'w':
            case 'y':
                return number(i);
            case 'E':
                return ".{2,12}";
            case 'G':
            case 'z':
                return ".*";
            case 'M':
                return i >= 3 ? ".{3,12}" : number(i);
            case 'Z':
                return "(\\+|-)\\d{4}";
            case '\\':
                throw new java.lang.IllegalStateException("Forward slashes are not allowed");
            case 'a':
                return ".{2}";
            default:
                return i == 1 ? "" + c : c + "{" + i + "}";
        }
    }
}
