package ch.qos.logback.core.util;

public class CloseUtil {
    public static void closeQuietly(java.io.Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (java.io.IOException e) {
            }
        }
    }

    public static void closeQuietly(java.net.ServerSocket serverSocket) {
        if (serverSocket != null) {
            try {
                serverSocket.close();
            } catch (java.io.IOException e) {
            }
        }
    }

    public static void closeQuietly(java.net.Socket socket) {
        if (socket != null) {
            try {
                socket.close();
            } catch (java.io.IOException e) {
            }
        }
    }
}
