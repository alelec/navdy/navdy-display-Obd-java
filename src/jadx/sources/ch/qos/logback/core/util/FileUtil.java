package ch.qos.logback.core.util;

public class FileUtil extends ch.qos.logback.core.spi.ContextAwareBase {
    static final int BUF_SIZE = 32768;

    public FileUtil(ch.qos.logback.core.Context context) {
        setContext(context);
    }

    public static boolean createMissingParentDirectories(java.io.File file) {
        java.io.File parentFile = file.getParentFile();
        if (parentFile == null) {
            throw new java.lang.IllegalStateException(file + " should not have a null parent");
        } else if (!parentFile.exists()) {
            return parentFile.mkdirs();
        } else {
            throw new java.lang.IllegalStateException(file + " should not have existing parent directory");
        }
    }

    public static java.net.URL fileToURL(java.io.File file) {
        try {
            return file.toURI().toURL();
        } catch (java.net.MalformedURLException e) {
            throw new java.lang.RuntimeException("Unexpected exception on file [" + file + "]", e);
        }
    }

    public static boolean isParentDirectoryCreationRequired(java.io.File file) {
        java.io.File parentFile = file.getParentFile();
        return parentFile != null && !parentFile.exists();
    }

    public static java.lang.String prefixRelativePath(java.lang.String str, java.lang.String str2) {
        return (str == null || ch.qos.logback.core.util.OptionHelper.isEmpty(str.trim()) || new java.io.File(str2).isAbsolute()) ? str2 : str + "/" + str2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0057 A[SYNTHETIC, Splitter:B:18:0x0057] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x005c A[SYNTHETIC, Splitter:B:21:0x005c] */
    public void copy(java.lang.String str, java.lang.String str2) throws ch.qos.logback.core.rolling.RolloverFailure {
        java.io.BufferedOutputStream bufferedOutputStream;
        java.io.BufferedInputStream bufferedInputStream;
        java.io.BufferedInputStream bufferedInputStream2 = null;
        try {
            java.io.BufferedInputStream bufferedInputStream3 = new java.io.BufferedInputStream(new java.io.FileInputStream(str));
            try {
                bufferedOutputStream = new java.io.BufferedOutputStream(new java.io.FileOutputStream(str2));
            } catch (java.io.IOException e) {
                e = e;
                bufferedOutputStream = null;
                bufferedInputStream2 = bufferedInputStream3;
                try {
                    java.lang.String str3 = "Failed to copy [" + str + "] to [" + str2 + "]";
                    addError(str3, e);
                    throw new ch.qos.logback.core.rolling.RolloverFailure(str3);
                } catch (Throwable th) {
                    th = th;
                    if (bufferedInputStream2 != null) {
                    }
                    if (bufferedOutputStream != null) {
                    }
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                bufferedOutputStream = null;
                bufferedInputStream2 = bufferedInputStream3;
                if (bufferedInputStream2 != null) {
                }
                if (bufferedOutputStream != null) {
                }
                throw th;
            }
            try {
                byte[] bArr = new byte[32768];
                while (true) {
                    int read = bufferedInputStream3.read(bArr);
                    if (read == -1) {
                        break;
                    }
                    bufferedOutputStream.write(bArr, 0, read);
                }
                bufferedInputStream3.close();
                bufferedInputStream = null;
            } catch (java.io.IOException e2) {
                e = e2;
                bufferedInputStream2 = bufferedInputStream3;
                java.lang.String str32 = "Failed to copy [" + str + "] to [" + str2 + "]";
                addError(str32, e);
                throw new ch.qos.logback.core.rolling.RolloverFailure(str32);
            } catch (Throwable th3) {
                th = th3;
                bufferedInputStream2 = bufferedInputStream3;
                if (bufferedInputStream2 != null) {
                }
                if (bufferedOutputStream != null) {
                }
                throw th;
            }
            try {
                bufferedOutputStream.close();
                java.io.BufferedOutputStream bufferedOutputStream2 = null;
                if (0 != 0) {
                    try {
                        bufferedInputStream.close();
                    } catch (java.io.IOException e3) {
                    }
                }
                if (0 != 0) {
                    try {
                        bufferedOutputStream2.close();
                    } catch (java.io.IOException e4) {
                    }
                }
            } catch (java.io.IOException e5) {
                e = e5;
                java.lang.String str322 = "Failed to copy [" + str + "] to [" + str2 + "]";
                addError(str322, e);
                throw new ch.qos.logback.core.rolling.RolloverFailure(str322);
            }
        } catch (java.io.IOException e6) {
            e = e6;
            bufferedOutputStream = null;
            java.lang.String str3222 = "Failed to copy [" + str + "] to [" + str2 + "]";
            addError(str3222, e);
            throw new ch.qos.logback.core.rolling.RolloverFailure(str3222);
        } catch (Throwable th4) {
            th = th4;
            bufferedOutputStream = null;
            if (bufferedInputStream2 != null) {
                try {
                    bufferedInputStream2.close();
                } catch (java.io.IOException e7) {
                }
            }
            if (bufferedOutputStream != null) {
                try {
                    bufferedOutputStream.close();
                } catch (java.io.IOException e8) {
                }
            }
            throw th;
        }
    }
}
