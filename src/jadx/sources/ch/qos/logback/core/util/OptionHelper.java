package ch.qos.logback.core.util;

public class OptionHelper {
    static final java.lang.String DELIM_DEFAULT = ":-";
    static final int DELIM_DEFAULT_LEN = 2;
    static final java.lang.String DELIM_START = "${";
    static final int DELIM_START_LEN = 2;
    static final char DELIM_STOP = '}';
    static final int DELIM_STOP_LEN = 1;
    static final java.lang.String _IS_UNDEFINED = "_IS_UNDEFINED";

    public static java.lang.String[] extractDefaultReplacement(java.lang.String str) {
        java.lang.String[] strArr = new java.lang.String[2];
        if (str != null) {
            strArr[0] = str;
            int indexOf = str.indexOf(":-");
            if (indexOf != -1) {
                strArr[0] = str.substring(0, indexOf);
                strArr[1] = str.substring(indexOf + 2);
            }
        }
        return strArr;
    }

    public static java.lang.String getAndroidSystemProperty(java.lang.String str) {
        boolean z = false;
        try {
            return ch.qos.logback.core.android.SystemPropertiesProxy.getInstance().get(str, null);
        } catch (java.lang.IllegalArgumentException e) {
            return z;
        }
    }

    public static java.lang.String getEnv(java.lang.String str) {
        try {
            return java.lang.System.getenv(str);
        } catch (java.lang.SecurityException e) {
            return null;
        }
    }

    public static java.util.Properties getSystemProperties() {
        try {
            return java.lang.System.getProperties();
        } catch (java.lang.SecurityException e) {
            return new java.util.Properties();
        }
    }

    public static java.lang.String getSystemProperty(java.lang.String str) {
        try {
            java.lang.String property = java.lang.System.getProperty(str);
            return property == null ? getAndroidSystemProperty(str) : property;
        } catch (java.lang.SecurityException e) {
            return null;
        }
    }

    public static java.lang.String getSystemProperty(java.lang.String str, java.lang.String str2) {
        try {
            return java.lang.System.getProperty(str, str2);
        } catch (java.lang.SecurityException e) {
            return str2;
        }
    }

    public static java.lang.Object instantiateByClassName(java.lang.String str, java.lang.Class<?> cls, ch.qos.logback.core.Context context) throws ch.qos.logback.core.util.IncompatibleClassException, ch.qos.logback.core.util.DynamicClassLoadingException {
        return instantiateByClassName(str, cls, ch.qos.logback.core.util.Loader.getClassLoaderOfObject(context));
    }

    public static java.lang.Object instantiateByClassName(java.lang.String str, java.lang.Class<?> cls, java.lang.ClassLoader classLoader) throws ch.qos.logback.core.util.IncompatibleClassException, ch.qos.logback.core.util.DynamicClassLoadingException {
        return instantiateByClassNameAndParameter(str, cls, classLoader, null, (java.lang.Object) null);
    }

    public static java.lang.Object instantiateByClassNameAndParameter(java.lang.String str, java.lang.Class<?> cls, ch.qos.logback.core.Context context, java.lang.Class<?> cls2, java.lang.Object obj) throws ch.qos.logback.core.util.IncompatibleClassException, ch.qos.logback.core.util.DynamicClassLoadingException {
        return instantiateByClassNameAndParameter(str, cls, ch.qos.logback.core.util.Loader.getClassLoaderOfObject(context), cls2, obj);
    }

    public static java.lang.Object instantiateByClassNameAndParameter(java.lang.String str, java.lang.Class<?> cls, java.lang.ClassLoader classLoader, java.lang.Class<?> cls2, java.lang.Object obj) throws ch.qos.logback.core.util.IncompatibleClassException, ch.qos.logback.core.util.DynamicClassLoadingException {
        if (str == null) {
            throw new java.lang.NullPointerException();
        }
        try {
            java.lang.Class loadClass = classLoader.loadClass(str);
            if (!cls.isAssignableFrom(loadClass)) {
                throw new ch.qos.logback.core.util.IncompatibleClassException(cls, loadClass);
            } else if (cls2 == null) {
                return loadClass.newInstance();
            } else {
                return loadClass.getConstructor(new java.lang.Class[]{cls2}).newInstance(new java.lang.Object[]{obj});
            }
        } catch (ch.qos.logback.core.util.IncompatibleClassException e) {
            throw e;
        } catch (Throwable th) {
            throw new ch.qos.logback.core.util.DynamicClassLoadingException("Failed to instantiate type " + str, th);
        }
    }

    public static boolean isEmpty(java.lang.String str) {
        return str == null || "".equals(str);
    }

    public static java.lang.String propertyLookup(java.lang.String str, ch.qos.logback.core.spi.PropertyContainer propertyContainer, ch.qos.logback.core.spi.PropertyContainer propertyContainer2) {
        java.lang.String property = propertyContainer.getProperty(str);
        if (property == null && propertyContainer2 != null) {
            property = propertyContainer2.getProperty(str);
        }
        if (property == null) {
            property = getSystemProperty(str, null);
        }
        return property == null ? getEnv(str) : property;
    }

    public static void setSystemProperties(ch.qos.logback.core.spi.ContextAware contextAware, java.util.Properties properties) {
        for (java.lang.String str : properties.keySet()) {
            setSystemProperty(contextAware, str, properties.getProperty(str));
        }
    }

    public static void setSystemProperty(ch.qos.logback.core.spi.ContextAware contextAware, java.lang.String str, java.lang.String str2) {
        try {
            java.lang.System.setProperty(str, str2);
        } catch (java.lang.SecurityException e) {
            contextAware.addError("Failed to set system property [" + str + "]", e);
        }
    }

    public static java.lang.String substVars(java.lang.String str, ch.qos.logback.core.spi.PropertyContainer propertyContainer) {
        return substVars(str, propertyContainer, null);
    }

    public static java.lang.String substVars(java.lang.String str, ch.qos.logback.core.spi.PropertyContainer propertyContainer, ch.qos.logback.core.spi.PropertyContainer propertyContainer2) {
        try {
            return ch.qos.logback.core.subst.NodeToStringTransformer.substituteVariable(str, propertyContainer, propertyContainer2);
        } catch (ch.qos.logback.core.spi.ScanException e) {
            throw new java.lang.IllegalArgumentException("Failed to parse input [" + str + "]", e);
        }
    }

    public static boolean toBoolean(java.lang.String str, boolean z) {
        if (str == null) {
            return z;
        }
        java.lang.String trim = str.trim();
        if ("true".equalsIgnoreCase(trim)) {
            return true;
        }
        if ("false".equalsIgnoreCase(trim)) {
            return false;
        }
        return z;
    }
}
