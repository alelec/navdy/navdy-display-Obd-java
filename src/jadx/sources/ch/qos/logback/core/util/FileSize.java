package ch.qos.logback.core.util;

public class FileSize {
    private static final int DOUBLE_GROUP = 1;
    private static final java.util.regex.Pattern FILE_SIZE_PATTERN = java.util.regex.Pattern.compile("([0-9]+)\\s*(|kb|mb|gb)s?", 2);
    static final long GB_COEFFICIENT = 1073741824;
    static final long KB_COEFFICIENT = 1024;
    private static final java.lang.String LENGTH_PART = "([0-9]+)";
    static final long MB_COEFFICIENT = 1048576;
    private static final int UNIT_GROUP = 2;
    private static final java.lang.String UNIT_PART = "(|kb|mb|gb)s?";
    final long size;

    FileSize(long j) {
        this.size = j;
    }

    public static ch.qos.logback.core.util.FileSize valueOf(java.lang.String str) {
        long j;
        java.util.regex.Matcher matcher = FILE_SIZE_PATTERN.matcher(str);
        if (matcher.matches()) {
            java.lang.String group = matcher.group(1);
            java.lang.String group2 = matcher.group(2);
            long longValue = java.lang.Long.valueOf(group).longValue();
            if (group2.equalsIgnoreCase("")) {
                j = 1;
            } else if (group2.equalsIgnoreCase("kb")) {
                j = 1024;
            } else if (group2.equalsIgnoreCase("mb")) {
                j = MB_COEFFICIENT;
            } else if (group2.equalsIgnoreCase("gb")) {
                j = GB_COEFFICIENT;
            } else {
                throw new java.lang.IllegalStateException("Unexpected " + group2);
            }
            return new ch.qos.logback.core.util.FileSize(j * longValue);
        }
        throw new java.lang.IllegalArgumentException("String value [" + str + "] is not in the expected format.");
    }

    public long getSize() {
        return this.size;
    }
}
