package ch.qos.logback.core.util;

public class IncompatibleClassException extends java.lang.Exception {
    private static final long serialVersionUID = -5823372159561159549L;
    java.lang.Class<?> obtainedClass;
    java.lang.Class<?> requestedClass;

    IncompatibleClassException(java.lang.Class<?> cls, java.lang.Class<?> cls2) {
        this.requestedClass = cls;
        this.obtainedClass = cls2;
    }
}
