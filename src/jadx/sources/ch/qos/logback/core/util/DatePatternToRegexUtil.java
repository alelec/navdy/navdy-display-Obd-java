package ch.qos.logback.core.util;

public class DatePatternToRegexUtil {
    final java.lang.String datePattern;
    final int datePatternLength;
    final ch.qos.logback.core.util.CharSequenceToRegexMapper regexMapper = new ch.qos.logback.core.util.CharSequenceToRegexMapper();

    public DatePatternToRegexUtil(java.lang.String str) {
        this.datePattern = str;
        this.datePatternLength = str.length();
    }

    private java.util.List<ch.qos.logback.core.util.CharSequenceState> tokenize() {
        java.util.ArrayList arrayList = new java.util.ArrayList();
        ch.qos.logback.core.util.CharSequenceState charSequenceState = null;
        for (int i = 0; i < this.datePatternLength; i++) {
            char charAt = this.datePattern.charAt(i);
            if (charSequenceState == null || charSequenceState.c != charAt) {
                charSequenceState = new ch.qos.logback.core.util.CharSequenceState(charAt);
                arrayList.add(charSequenceState);
            } else {
                charSequenceState.incrementOccurrences();
            }
        }
        return arrayList;
    }

    public java.lang.String toRegex() {
        java.util.List<ch.qos.logback.core.util.CharSequenceState> list = tokenize();
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        for (ch.qos.logback.core.util.CharSequenceState regex : list) {
            sb.append(this.regexMapper.toRegex(regex));
        }
        return sb.toString();
    }
}
