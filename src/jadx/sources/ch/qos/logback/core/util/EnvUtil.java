package ch.qos.logback.core.util;

public class EnvUtil {
    public static boolean isAndroidOS() {
        java.lang.String systemProperty = ch.qos.logback.core.util.OptionHelper.getSystemProperty("os.name");
        java.lang.String env = ch.qos.logback.core.util.OptionHelper.getEnv("ANDROID_ROOT");
        java.lang.String env2 = ch.qos.logback.core.util.OptionHelper.getEnv("ANDROID_DATA");
        return systemProperty != null && systemProperty.contains("Linux") && env != null && env.contains("/system") && env2 != null && env2.contains("/data");
    }

    public static boolean isJDK5() {
        return isJDK_N_OrHigher(5);
    }

    public static boolean isJDK6OrHigher() {
        return isJDK_N_OrHigher(6);
    }

    public static boolean isJDK7OrHigher() {
        return isJDK_N_OrHigher(7);
    }

    private static boolean isJDK_N_OrHigher(int i) {
        java.util.ArrayList<java.lang.String> arrayList = new java.util.ArrayList<>();
        for (int i2 = 0; i2 < 5; i2++) {
            arrayList.add("1." + (i + i2));
        }
        java.lang.String property = java.lang.System.getProperty("java.version");
        if (property == null) {
            return false;
        }
        for (java.lang.String startsWith : arrayList) {
            if (property.startsWith(startsWith)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isWindows() {
        return java.lang.System.getProperty("os.name").startsWith("Windows");
    }
}
