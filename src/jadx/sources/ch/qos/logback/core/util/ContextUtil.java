package ch.qos.logback.core.util;

public class ContextUtil extends ch.qos.logback.core.spi.ContextAwareBase {
    public ContextUtil(ch.qos.logback.core.Context context) {
        setContext(context);
    }

    public void addHostNameAsProperty() {
        this.context.putProperty(ch.qos.logback.core.CoreConstants.HOSTNAME_KEY, "localhost");
    }

    public void addProperties(java.util.Properties properties) {
        if (properties != null) {
            for (java.lang.String str : properties.keySet()) {
                this.context.putProperty(str, properties.getProperty(str));
            }
        }
    }
}
