package ch.qos.logback.core.util;

public class StatusPrinter {
    static ch.qos.logback.core.util.CachingDateFormatter cachingDateFormat = new ch.qos.logback.core.util.CachingDateFormatter("HH:mm:ss,SSS");
    private static java.io.PrintStream ps = java.lang.System.out;

    private static void appendThrowable(java.lang.StringBuilder sb, java.lang.Throwable th) {
        java.lang.String[] convert;
        for (java.lang.String str : ch.qos.logback.core.helpers.ThrowableToStringArray.convert(th)) {
            if (!str.startsWith(ch.qos.logback.core.CoreConstants.CAUSED_BY)) {
                if (java.lang.Character.isDigit(str.charAt(0))) {
                    sb.append("\t... ");
                } else {
                    sb.append("\tat ");
                }
            }
            sb.append(str).append(ch.qos.logback.core.CoreConstants.LINE_SEPARATOR);
        }
    }

    /* JADX WARNING: Incorrect type for immutable var: ssa=ch.qos.logback.core.status.Status, code=ch.qos.logback.core.status.Status<ch.qos.logback.core.status.Status>, for r6v0, types: [java.lang.Object, ch.qos.logback.core.status.Status<ch.qos.logback.core.status.Status>, ch.qos.logback.core.status.Status] */
    public static void buildStr(java.lang.StringBuilder sb, java.lang.String str, ch.qos.logback.core.status.Status<ch.qos.logback.core.status.Status> status) {
        java.lang.String str2 = status.hasChildren() ? str + "+ " : str + "|-";
        if (cachingDateFormat != null) {
            sb.append(cachingDateFormat.format(status.getDate().longValue())).append(" ");
        }
        sb.append(str2).append(status).append(ch.qos.logback.core.CoreConstants.LINE_SEPARATOR);
        if (status.getThrowable() != null) {
            appendThrowable(sb, status.getThrowable());
        }
        if (status.hasChildren()) {
            for (ch.qos.logback.core.status.Status buildStr : status) {
                buildStr(sb, str + "  ", buildStr);
            }
        }
    }

    private static void buildStrFromStatusList(java.lang.StringBuilder sb, java.util.List<ch.qos.logback.core.status.Status> list) {
        if (list != null) {
            for (ch.qos.logback.core.status.Status buildStr : list) {
                buildStr(sb, "", buildStr);
            }
        }
    }

    public static void print(ch.qos.logback.core.Context context) {
        print(context, 0);
    }

    public static void print(ch.qos.logback.core.Context context, long j) {
        if (context == null) {
            throw new java.lang.IllegalArgumentException("Context argument cannot be null");
        }
        ch.qos.logback.core.status.StatusManager statusManager = context.getStatusManager();
        if (statusManager == null) {
            ps.println("WARN: Context named \"" + context.getName() + "\" has no status manager");
        } else {
            print(statusManager, j);
        }
    }

    public static void print(ch.qos.logback.core.status.StatusManager statusManager) {
        print(statusManager, 0);
    }

    public static void print(ch.qos.logback.core.status.StatusManager statusManager, long j) {
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        buildStrFromStatusList(sb, ch.qos.logback.core.status.StatusUtil.filterStatusListByTimeThreshold(statusManager.getCopyOfStatusList(), j));
        ps.println(sb.toString());
    }

    public static void print(java.util.List<ch.qos.logback.core.status.Status> list) {
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        buildStrFromStatusList(sb, list);
        ps.println(sb.toString());
    }

    public static void printIfErrorsOccured(ch.qos.logback.core.Context context) {
        if (context == null) {
            throw new java.lang.IllegalArgumentException("Context argument cannot be null");
        }
        ch.qos.logback.core.status.StatusManager statusManager = context.getStatusManager();
        if (statusManager == null) {
            ps.println("WARN: Context named \"" + context.getName() + "\" has no status manager");
        } else if (new ch.qos.logback.core.status.StatusUtil(context).getHighestLevel(0) == 2) {
            print(statusManager);
        }
    }

    public static void printInCaseOfErrorsOrWarnings(ch.qos.logback.core.Context context) {
        printInCaseOfErrorsOrWarnings(context, 0);
    }

    public static void printInCaseOfErrorsOrWarnings(ch.qos.logback.core.Context context, long j) {
        if (context == null) {
            throw new java.lang.IllegalArgumentException("Context argument cannot be null");
        }
        ch.qos.logback.core.status.StatusManager statusManager = context.getStatusManager();
        if (statusManager == null) {
            ps.println("WARN: Context named \"" + context.getName() + "\" has no status manager");
        } else if (new ch.qos.logback.core.status.StatusUtil(context).getHighestLevel(j) >= 1) {
            print(statusManager, j);
        }
    }

    public static void setPrintStream(java.io.PrintStream printStream) {
        ps = printStream;
    }
}
