package ch.qos.logback.core.util;

public class PropertySetterException extends java.lang.Exception {
    private static final long serialVersionUID = -2771077768281663949L;

    public PropertySetterException(java.lang.String str) {
        super(str);
    }

    public PropertySetterException(java.lang.String str, java.lang.Throwable th) {
        super(str, th);
    }

    public PropertySetterException(java.lang.Throwable th) {
        super(th);
    }
}
