package ch.qos.logback.core.util;

public class StringCollectionUtil {
    public static void removeMatching(java.util.Collection<java.lang.String> collection, java.util.Collection<java.lang.String> collection2) {
        java.util.ArrayList arrayList = new java.util.ArrayList(collection.size());
        for (java.lang.String compile : collection2) {
            java.util.regex.Pattern compile2 = java.util.regex.Pattern.compile(compile);
            for (java.lang.String str : collection) {
                if (compile2.matcher(str).matches()) {
                    arrayList.add(str);
                }
            }
        }
        collection.removeAll(arrayList);
    }

    public static void removeMatching(java.util.Collection<java.lang.String> collection, java.lang.String... strArr) {
        removeMatching(collection, (java.util.Collection<java.lang.String>) java.util.Arrays.asList(strArr));
    }

    public static void retainMatching(java.util.Collection<java.lang.String> collection, java.util.Collection<java.lang.String> collection2) {
        if (!collection2.isEmpty()) {
            java.util.ArrayList arrayList = new java.util.ArrayList(collection.size());
            for (java.lang.String compile : collection2) {
                java.util.regex.Pattern compile2 = java.util.regex.Pattern.compile(compile);
                for (java.lang.String str : collection) {
                    if (compile2.matcher(str).matches()) {
                        arrayList.add(str);
                    }
                }
            }
            collection.retainAll(arrayList);
        }
    }

    public static void retainMatching(java.util.Collection<java.lang.String> collection, java.lang.String... strArr) {
        retainMatching(collection, (java.util.Collection<java.lang.String>) java.util.Arrays.asList(strArr));
    }
}
