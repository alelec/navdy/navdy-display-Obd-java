package ch.qos.logback.core.util;

public class ContentTypeUtil {
    public static java.lang.String getSubType(java.lang.String str) {
        if (str == null) {
            return null;
        }
        int indexOf = str.indexOf(47);
        if (indexOf == -1) {
            return null;
        }
        int i = indexOf + 1;
        if (i < str.length()) {
            return str.substring(i);
        }
        return null;
    }

    public static boolean isTextual(java.lang.String str) {
        if (str == null) {
            return false;
        }
        return str.startsWith("text");
    }
}
