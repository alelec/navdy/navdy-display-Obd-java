package ch.qos.logback.core;

public class AsyncAppenderBase<E> extends ch.qos.logback.core.UnsynchronizedAppenderBase<E> implements ch.qos.logback.core.spi.AppenderAttachable<E> {
    public static final int DEFAULT_QUEUE_SIZE = 256;
    static final int UNDEFINED = -1;
    ch.qos.logback.core.spi.AppenderAttachableImpl<E> aai = new ch.qos.logback.core.spi.AppenderAttachableImpl<>();
    int appenderCount = 0;
    java.util.concurrent.BlockingQueue<E> blockingQueue;
    int discardingThreshold = -1;
    int queueSize = 256;
    ch.qos.logback.core.AsyncAppenderBase.Worker worker = new ch.qos.logback.core.AsyncAppenderBase.Worker<>();

    class Worker extends java.lang.Thread {
        Worker() {
        }

        public void run() {
            ch.qos.logback.core.AsyncAppenderBase asyncAppenderBase = ch.qos.logback.core.AsyncAppenderBase.this;
            ch.qos.logback.core.spi.AppenderAttachableImpl<E> appenderAttachableImpl = asyncAppenderBase.aai;
            while (asyncAppenderBase.isStarted()) {
                try {
                    appenderAttachableImpl.appendLoopOnAppenders(asyncAppenderBase.blockingQueue.take());
                } catch (java.lang.InterruptedException e) {
                }
            }
            ch.qos.logback.core.AsyncAppenderBase.this.addInfo("Worker thread will flush remaining events before exiting.");
            for (E appendLoopOnAppenders : asyncAppenderBase.blockingQueue) {
                appenderAttachableImpl.appendLoopOnAppenders(appendLoopOnAppenders);
            }
            appenderAttachableImpl.detachAndStopAllAppenders();
        }
    }

    private boolean isQueueBelowDiscardingThreshold() {
        return this.blockingQueue.remainingCapacity() < this.discardingThreshold;
    }

    private void put(E e) {
        try {
            this.blockingQueue.put(e);
        } catch (java.lang.InterruptedException e2) {
        }
    }

    public void addAppender(ch.qos.logback.core.Appender<E> appender) {
        if (this.appenderCount == 0) {
            this.appenderCount++;
            addInfo("Attaching appender named [" + appender.getName() + "] to AsyncAppender.");
            this.aai.addAppender(appender);
            return;
        }
        addWarn("One and only one appender may be attached to AsyncAppender.");
        addWarn("Ignoring additional appender named [" + appender.getName() + "]");
    }

    /* access modifiers changed from: protected */
    public void append(E e) {
        if (!isQueueBelowDiscardingThreshold() || !isDiscardable(e)) {
            preprocess(e);
            put(e);
        }
    }

    public void detachAndStopAllAppenders() {
        this.aai.detachAndStopAllAppenders();
    }

    public boolean detachAppender(ch.qos.logback.core.Appender<E> appender) {
        return this.aai.detachAppender(appender);
    }

    public boolean detachAppender(java.lang.String str) {
        return this.aai.detachAppender(str);
    }

    public ch.qos.logback.core.Appender<E> getAppender(java.lang.String str) {
        return this.aai.getAppender(str);
    }

    public int getDiscardingThreshold() {
        return this.discardingThreshold;
    }

    public int getNumberOfElementsInQueue() {
        return this.blockingQueue.size();
    }

    public int getQueueSize() {
        return this.queueSize;
    }

    public int getRemainingCapacity() {
        return this.blockingQueue.remainingCapacity();
    }

    public boolean isAttached(ch.qos.logback.core.Appender<E> appender) {
        return this.aai.isAttached(appender);
    }

    /* access modifiers changed from: protected */
    public boolean isDiscardable(E e) {
        return false;
    }

    public java.util.Iterator<ch.qos.logback.core.Appender<E>> iteratorForAppenders() {
        return this.aai.iteratorForAppenders();
    }

    /* access modifiers changed from: protected */
    public void preprocess(E e) {
    }

    public void setDiscardingThreshold(int i) {
        this.discardingThreshold = i;
    }

    public void setQueueSize(int i) {
        this.queueSize = i;
    }

    public void start() {
        if (this.appenderCount == 0) {
            addError("No attached appenders found.");
        } else if (this.queueSize < 1) {
            addError("Invalid queue size [" + this.queueSize + "]");
        } else {
            this.blockingQueue = new java.util.concurrent.ArrayBlockingQueue(this.queueSize);
            if (this.discardingThreshold == -1) {
                this.discardingThreshold = this.queueSize / 5;
            }
            addInfo("Setting discardingThreshold to " + this.discardingThreshold);
            this.worker.setDaemon(true);
            this.worker.setName("AsyncAppender-Worker-" + this.worker.getName());
            super.start();
            this.worker.start();
        }
    }

    public void stop() {
        if (isStarted()) {
            super.stop();
            this.worker.interrupt();
            try {
                this.worker.join(1000);
            } catch (java.lang.InterruptedException e) {
                addError("Failed to join worker thread", e);
            }
        }
    }
}
