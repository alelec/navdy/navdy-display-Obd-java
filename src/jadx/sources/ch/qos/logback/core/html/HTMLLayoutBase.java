package ch.qos.logback.core.html;

public abstract class HTMLLayoutBase<E> extends ch.qos.logback.core.LayoutBase<E> {
    protected long counter = 0;
    protected ch.qos.logback.core.html.CssBuilder cssBuilder;
    protected ch.qos.logback.core.pattern.Converter<E> head;
    protected java.lang.String pattern;
    protected java.lang.String title = "Logback Log Messages";

    private void buildHeaderRowForTable(java.lang.StringBuilder sb) {
        ch.qos.logback.core.pattern.Converter<E> converter = this.head;
        sb.append("<tr class=\"header\">");
        sb.append(ch.qos.logback.core.CoreConstants.LINE_SEPARATOR);
        while (converter != null) {
            if (computeConverterName(converter) == null) {
                converter = converter.getNext();
            } else {
                sb.append("<td class=\"");
                sb.append(computeConverterName(converter));
                sb.append("\">");
                sb.append(computeConverterName(converter));
                sb.append("</td>");
                sb.append(ch.qos.logback.core.CoreConstants.LINE_SEPARATOR);
                converter = converter.getNext();
            }
        }
        sb.append("</tr>");
        sb.append(ch.qos.logback.core.CoreConstants.LINE_SEPARATOR);
    }

    /* access modifiers changed from: protected */
    public java.lang.String computeConverterName(ch.qos.logback.core.pattern.Converter converter) {
        java.lang.String simpleName = converter.getClass().getSimpleName();
        int indexOf = simpleName.indexOf("Converter");
        return indexOf == -1 ? simpleName : simpleName.substring(0, indexOf);
    }

    public java.lang.String getContentType() {
        return "text/html";
    }

    public ch.qos.logback.core.html.CssBuilder getCssBuilder() {
        return this.cssBuilder;
    }

    /* access modifiers changed from: protected */
    public abstract java.util.Map<java.lang.String, java.lang.String> getDefaultConverterMap();

    public java.util.Map<java.lang.String, java.lang.String> getEffectiveConverterMap() {
        java.util.HashMap hashMap = new java.util.HashMap();
        java.util.Map defaultConverterMap = getDefaultConverterMap();
        if (defaultConverterMap != null) {
            hashMap.putAll(defaultConverterMap);
        }
        ch.qos.logback.core.Context context = getContext();
        if (context != null) {
            java.util.Map map = (java.util.Map) context.getObject(ch.qos.logback.core.CoreConstants.PATTERN_RULE_REGISTRY);
            if (map != null) {
                hashMap.putAll(map);
            }
        }
        return hashMap;
    }

    public java.lang.String getFileFooter() {
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        sb.append(ch.qos.logback.core.CoreConstants.LINE_SEPARATOR);
        sb.append("</body></html>");
        return sb.toString();
    }

    public java.lang.String getFileHeader() {
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        sb.append("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\"");
        sb.append(" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">");
        sb.append(ch.qos.logback.core.CoreConstants.LINE_SEPARATOR);
        sb.append("<html>");
        sb.append(ch.qos.logback.core.CoreConstants.LINE_SEPARATOR);
        sb.append("  <head>");
        sb.append(ch.qos.logback.core.CoreConstants.LINE_SEPARATOR);
        sb.append("    <title>");
        sb.append(this.title);
        sb.append("</title>");
        sb.append(ch.qos.logback.core.CoreConstants.LINE_SEPARATOR);
        this.cssBuilder.addCss(sb);
        sb.append(ch.qos.logback.core.CoreConstants.LINE_SEPARATOR);
        sb.append("  </head>");
        sb.append(ch.qos.logback.core.CoreConstants.LINE_SEPARATOR);
        sb.append("<body>");
        sb.append(ch.qos.logback.core.CoreConstants.LINE_SEPARATOR);
        return sb.toString();
    }

    public java.lang.String getPattern() {
        return this.pattern;
    }

    public java.lang.String getPresentationFooter() {
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        sb.append("</table>");
        return sb.toString();
    }

    public java.lang.String getPresentationHeader() {
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        sb.append("<hr/>");
        sb.append(ch.qos.logback.core.CoreConstants.LINE_SEPARATOR);
        sb.append("<p>Log session start time ");
        sb.append(new java.util.Date());
        sb.append("</p><p></p>");
        sb.append(ch.qos.logback.core.CoreConstants.LINE_SEPARATOR);
        sb.append(ch.qos.logback.core.CoreConstants.LINE_SEPARATOR);
        sb.append("<table cellspacing=\"0\">");
        sb.append(ch.qos.logback.core.CoreConstants.LINE_SEPARATOR);
        buildHeaderRowForTable(sb);
        return sb.toString();
    }

    public java.lang.String getTitle() {
        return this.title;
    }

    public void setCssBuilder(ch.qos.logback.core.html.CssBuilder cssBuilder2) {
        this.cssBuilder = cssBuilder2;
    }

    public void setPattern(java.lang.String str) {
        this.pattern = str;
    }

    public void setTitle(java.lang.String str) {
        this.title = str;
    }

    public void start() {
        boolean z = false;
        try {
            ch.qos.logback.core.pattern.parser.Parser parser = new ch.qos.logback.core.pattern.parser.Parser(this.pattern);
            parser.setContext(getContext());
            this.head = parser.compile(parser.parse(), getEffectiveConverterMap());
            ch.qos.logback.core.pattern.ConverterUtil.startConverters(this.head);
        } catch (ch.qos.logback.core.spi.ScanException e) {
            addError("Incorrect pattern found", e);
            z = true;
        }
        if (!z) {
            this.started = true;
        }
    }

    /* access modifiers changed from: protected */
    public void startNewTableIfLimitReached(java.lang.StringBuilder sb) {
        if (this.counter >= 10000) {
            this.counter = 0;
            sb.append("</table>");
            sb.append(ch.qos.logback.core.CoreConstants.LINE_SEPARATOR);
            sb.append("<p></p>");
            sb.append("<table cellspacing=\"0\">");
            sb.append(ch.qos.logback.core.CoreConstants.LINE_SEPARATOR);
            buildHeaderRowForTable(sb);
        }
    }
}
