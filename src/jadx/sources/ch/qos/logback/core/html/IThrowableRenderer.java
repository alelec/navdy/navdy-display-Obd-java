package ch.qos.logback.core.html;

public interface IThrowableRenderer<E> {
    void render(java.lang.StringBuilder sb, E e);
}
