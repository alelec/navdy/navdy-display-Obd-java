package ch.qos.logback.core.html;

public interface CssBuilder {
    void addCss(java.lang.StringBuilder sb);
}
