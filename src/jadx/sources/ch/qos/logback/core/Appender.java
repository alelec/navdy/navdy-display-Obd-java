package ch.qos.logback.core;

public interface Appender<E> extends ch.qos.logback.core.spi.LifeCycle, ch.qos.logback.core.spi.ContextAware, ch.qos.logback.core.spi.FilterAttachable<E> {
    void doAppend(E e) throws ch.qos.logback.core.LogbackException;

    java.lang.String getName();

    void setName(java.lang.String str);
}
