package ch.qos.logback.core.pattern.util;

public class AlmostAsIsEscapeUtil extends ch.qos.logback.core.pattern.util.RestrictedEscapeUtil {
    public void escape(java.lang.String str, java.lang.StringBuffer stringBuffer, char c, int i) {
        super.escape("%)", stringBuffer, c, i);
    }
}
