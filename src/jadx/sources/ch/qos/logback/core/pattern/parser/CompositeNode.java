package ch.qos.logback.core.pattern.parser;

public class CompositeNode extends ch.qos.logback.core.pattern.parser.SimpleKeywordNode {
    ch.qos.logback.core.pattern.parser.Node childNode;

    CompositeNode(java.lang.String str) {
        super(2, str);
    }

    public boolean equals(java.lang.Object obj) {
        if (!super.equals(obj) || !(obj instanceof ch.qos.logback.core.pattern.parser.CompositeNode)) {
            return false;
        }
        ch.qos.logback.core.pattern.parser.CompositeNode compositeNode = (ch.qos.logback.core.pattern.parser.CompositeNode) obj;
        return this.childNode != null ? this.childNode.equals(compositeNode.childNode) : compositeNode.childNode == null;
    }

    public ch.qos.logback.core.pattern.parser.Node getChildNode() {
        return this.childNode;
    }

    public int hashCode() {
        return super.hashCode();
    }

    public void setChildNode(ch.qos.logback.core.pattern.parser.Node node) {
        this.childNode = node;
    }

    public java.lang.String toString() {
        java.lang.StringBuffer stringBuffer = new java.lang.StringBuffer();
        if (this.childNode != null) {
            stringBuffer.append("CompositeNode(" + this.childNode + ")");
        } else {
            stringBuffer.append("CompositeNode(no child)");
        }
        stringBuffer.append(printNext());
        return stringBuffer.toString();
    }
}
