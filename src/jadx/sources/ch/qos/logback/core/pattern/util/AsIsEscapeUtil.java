package ch.qos.logback.core.pattern.util;

public class AsIsEscapeUtil implements ch.qos.logback.core.pattern.util.IEscapeUtil {
    public void escape(java.lang.String str, java.lang.StringBuffer stringBuffer, char c, int i) {
        stringBuffer.append("\\");
        stringBuffer.append(c);
    }
}
