package ch.qos.logback.core.pattern.color;

@java.lang.Deprecated
public abstract class ForegroundCompositeConverterBase<E> extends ch.qos.logback.core.pattern.CompositeConverter<E> {
    private static final java.lang.String SET_DEFAULT_COLOR = "\u001b[0;39m";

    /* access modifiers changed from: protected */
    public abstract java.lang.String getForegroundColorCode(E e);

    /* access modifiers changed from: protected */
    public java.lang.String transform(E e, java.lang.String str) {
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        sb.append(ch.qos.logback.core.pattern.color.ANSIConstants.ESC_START);
        sb.append(getForegroundColorCode(e));
        sb.append(ch.qos.logback.core.pattern.color.ANSIConstants.ESC_END);
        sb.append(str);
        sb.append(SET_DEFAULT_COLOR);
        return sb.toString();
    }
}
