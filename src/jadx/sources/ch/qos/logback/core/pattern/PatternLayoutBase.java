package ch.qos.logback.core.pattern;

public abstract class PatternLayoutBase<E> extends ch.qos.logback.core.LayoutBase<E> {
    ch.qos.logback.core.pattern.Converter<E> head;
    java.util.Map<java.lang.String, java.lang.String> instanceConverterMap = new java.util.HashMap();
    protected boolean outputPatternAsHeader = false;
    java.lang.String pattern;
    protected ch.qos.logback.core.pattern.PostCompileProcessor<E> postCompileProcessor;

    public abstract java.util.Map<java.lang.String, java.lang.String> getDefaultConverterMap();

    public java.util.Map<java.lang.String, java.lang.String> getEffectiveConverterMap() {
        java.util.HashMap hashMap = new java.util.HashMap();
        java.util.Map defaultConverterMap = getDefaultConverterMap();
        if (defaultConverterMap != null) {
            hashMap.putAll(defaultConverterMap);
        }
        ch.qos.logback.core.Context context = getContext();
        if (context != null) {
            java.util.Map map = (java.util.Map) context.getObject(ch.qos.logback.core.CoreConstants.PATTERN_RULE_REGISTRY);
            if (map != null) {
                hashMap.putAll(map);
            }
        }
        hashMap.putAll(this.instanceConverterMap);
        return hashMap;
    }

    public java.util.Map<java.lang.String, java.lang.String> getInstanceConverterMap() {
        return this.instanceConverterMap;
    }

    public java.lang.String getPattern() {
        return this.pattern;
    }

    public java.lang.String getPresentationHeader() {
        return this.outputPatternAsHeader ? getPresentationHeaderPrefix() + this.pattern : super.getPresentationHeader();
    }

    /* access modifiers changed from: protected */
    public java.lang.String getPresentationHeaderPrefix() {
        return "";
    }

    public boolean isOutputPatternAsHeader() {
        return this.outputPatternAsHeader;
    }

    /* access modifiers changed from: protected */
    public void setContextForConverters(ch.qos.logback.core.pattern.Converter<E> converter) {
        ch.qos.logback.core.pattern.ConverterUtil.setContextForConverters(getContext(), converter);
    }

    public void setOutputPatternAsHeader(boolean z) {
        this.outputPatternAsHeader = z;
    }

    public void setPattern(java.lang.String str) {
        this.pattern = str;
    }

    public void setPostCompileProcessor(ch.qos.logback.core.pattern.PostCompileProcessor<E> postCompileProcessor2) {
        this.postCompileProcessor = postCompileProcessor2;
    }

    public void start() {
        if (this.pattern == null || this.pattern.length() == 0) {
            addError("Empty or null pattern.");
            return;
        }
        try {
            ch.qos.logback.core.pattern.parser.Parser parser = new ch.qos.logback.core.pattern.parser.Parser(this.pattern);
            if (getContext() != null) {
                parser.setContext(getContext());
            }
            this.head = parser.compile(parser.parse(), getEffectiveConverterMap());
            if (this.postCompileProcessor != null) {
                this.postCompileProcessor.process(this.head);
            }
            ch.qos.logback.core.pattern.ConverterUtil.setContextForConverters(getContext(), this.head);
            ch.qos.logback.core.pattern.ConverterUtil.startConverters(this.head);
            super.start();
        } catch (ch.qos.logback.core.spi.ScanException e) {
            getContext().getStatusManager().add((ch.qos.logback.core.status.Status) new ch.qos.logback.core.status.ErrorStatus("Failed to parse pattern \"" + getPattern() + "\".", this, e));
        }
    }

    public java.lang.String toString() {
        return getClass().getName() + "(\"" + getPattern() + "\")";
    }

    /* access modifiers changed from: protected */
    public java.lang.String writeLoopOnConverters(E e) {
        java.lang.StringBuilder sb = new java.lang.StringBuilder(128);
        for (ch.qos.logback.core.pattern.Converter<E> converter = this.head; converter != null; converter = converter.getNext()) {
            converter.write(sb, e);
        }
        return sb.toString();
    }
}
