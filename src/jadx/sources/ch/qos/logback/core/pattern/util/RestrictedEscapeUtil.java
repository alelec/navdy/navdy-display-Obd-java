package ch.qos.logback.core.pattern.util;

public class RestrictedEscapeUtil implements ch.qos.logback.core.pattern.util.IEscapeUtil {
    public void escape(java.lang.String str, java.lang.StringBuffer stringBuffer, char c, int i) {
        if (str.indexOf(c) >= 0) {
            stringBuffer.append(c);
            return;
        }
        stringBuffer.append("\\");
        stringBuffer.append(c);
    }
}
