package ch.qos.logback.core.pattern.parser;

public class FormattingNode extends ch.qos.logback.core.pattern.parser.Node {
    ch.qos.logback.core.pattern.FormatInfo formatInfo;

    FormattingNode(int i) {
        super(i);
    }

    FormattingNode(int i, java.lang.Object obj) {
        super(i, obj);
    }

    public boolean equals(java.lang.Object obj) {
        if (!super.equals(obj) || !(obj instanceof ch.qos.logback.core.pattern.parser.FormattingNode)) {
            return false;
        }
        ch.qos.logback.core.pattern.parser.FormattingNode formattingNode = (ch.qos.logback.core.pattern.parser.FormattingNode) obj;
        return this.formatInfo != null ? this.formatInfo.equals(formattingNode.formatInfo) : formattingNode.formatInfo == null;
    }

    public ch.qos.logback.core.pattern.FormatInfo getFormatInfo() {
        return this.formatInfo;
    }

    public int hashCode() {
        return (this.formatInfo != null ? this.formatInfo.hashCode() : 0) + (super.hashCode() * 31);
    }

    public void setFormatInfo(ch.qos.logback.core.pattern.FormatInfo formatInfo2) {
        this.formatInfo = formatInfo2;
    }
}
