package ch.qos.logback.core.pattern;

public abstract class DynamicConverter<E> extends ch.qos.logback.core.pattern.FormattingConverter<E> implements ch.qos.logback.core.spi.LifeCycle, ch.qos.logback.core.spi.ContextAware {
    ch.qos.logback.core.spi.ContextAwareBase cab = new ch.qos.logback.core.spi.ContextAwareBase(this);
    private java.util.List<java.lang.String> optionList;
    protected boolean started = false;

    public void addError(java.lang.String str) {
        this.cab.addError(str);
    }

    public void addError(java.lang.String str, java.lang.Throwable th) {
        this.cab.addError(str, th);
    }

    public void addInfo(java.lang.String str) {
        this.cab.addInfo(str);
    }

    public void addInfo(java.lang.String str, java.lang.Throwable th) {
        this.cab.addInfo(str, th);
    }

    public void addStatus(ch.qos.logback.core.status.Status status) {
        this.cab.addStatus(status);
    }

    public void addWarn(java.lang.String str) {
        this.cab.addWarn(str);
    }

    public void addWarn(java.lang.String str, java.lang.Throwable th) {
        this.cab.addWarn(str, th);
    }

    public ch.qos.logback.core.Context getContext() {
        return this.cab.getContext();
    }

    public java.lang.String getFirstOption() {
        if (this.optionList == null || this.optionList.size() == 0) {
            return null;
        }
        return (java.lang.String) this.optionList.get(0);
    }

    /* access modifiers changed from: protected */
    public java.util.List<java.lang.String> getOptionList() {
        return this.optionList;
    }

    public boolean isStarted() {
        return this.started;
    }

    public void setContext(ch.qos.logback.core.Context context) {
        this.cab.setContext(context);
    }

    public void setOptionList(java.util.List<java.lang.String> list) {
        this.optionList = list;
    }

    public void start() {
        this.started = true;
    }

    public void stop() {
        this.started = false;
    }
}
