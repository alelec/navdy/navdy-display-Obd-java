package ch.qos.logback.core.pattern.util;

public interface IEscapeUtil {
    void escape(java.lang.String str, java.lang.StringBuffer stringBuffer, char c, int i);
}
