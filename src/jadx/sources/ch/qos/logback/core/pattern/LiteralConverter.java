package ch.qos.logback.core.pattern;

public final class LiteralConverter<E> extends ch.qos.logback.core.pattern.Converter<E> {
    java.lang.String literal;

    public LiteralConverter(java.lang.String str) {
        this.literal = str;
    }

    public java.lang.String convert(E e) {
        return this.literal;
    }
}
