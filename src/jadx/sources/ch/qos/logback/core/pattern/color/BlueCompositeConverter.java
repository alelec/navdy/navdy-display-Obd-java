package ch.qos.logback.core.pattern.color;

@java.lang.Deprecated
public class BlueCompositeConverter<E> extends ch.qos.logback.core.pattern.color.ForegroundCompositeConverterBase<E> {
    /* access modifiers changed from: protected */
    public java.lang.String getForegroundColorCode(E e) {
        return ch.qos.logback.core.pattern.color.ANSIConstants.BLUE_FG;
    }
}
