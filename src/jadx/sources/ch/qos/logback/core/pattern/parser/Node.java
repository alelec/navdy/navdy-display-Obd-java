package ch.qos.logback.core.pattern.parser;

public class Node {
    static final int COMPOSITE_KEYWORD = 2;
    static final int LITERAL = 0;
    static final int SIMPLE_KEYWORD = 1;
    ch.qos.logback.core.pattern.parser.Node next;
    final int type;
    final java.lang.Object value;

    Node(int i) {
        this(i, null);
    }

    Node(int i, java.lang.Object obj) {
        this.type = i;
        this.value = obj;
    }

    public boolean equals(java.lang.Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ch.qos.logback.core.pattern.parser.Node)) {
            return false;
        }
        ch.qos.logback.core.pattern.parser.Node node = (ch.qos.logback.core.pattern.parser.Node) obj;
        if (this.type == node.type && (this.value == null ? node.value == null : this.value.equals(node.value))) {
            if (this.next != null) {
                if (this.next.equals(node.next)) {
                    return true;
                }
            } else if (node.next == null) {
                return true;
            }
        }
        return false;
    }

    public ch.qos.logback.core.pattern.parser.Node getNext() {
        return this.next;
    }

    public int getType() {
        return this.type;
    }

    public java.lang.Object getValue() {
        return this.value;
    }

    public int hashCode() {
        return (this.value != null ? this.value.hashCode() : 0) + (this.type * 31);
    }

    /* access modifiers changed from: 0000 */
    public java.lang.String printNext() {
        return this.next != null ? " -> " + this.next : "";
    }

    public void setNext(ch.qos.logback.core.pattern.parser.Node node) {
        this.next = node;
    }

    public java.lang.String toString() {
        java.lang.StringBuffer stringBuffer = new java.lang.StringBuffer();
        switch (this.type) {
            case 0:
                stringBuffer.append("LITERAL(" + this.value + ")");
                break;
            default:
                stringBuffer.append(super.toString());
                break;
        }
        stringBuffer.append(printNext());
        return stringBuffer.toString();
    }
}
