package ch.qos.logback.core.pattern;

public class SpacePadder {
    static final java.lang.String[] SPACES = {" ", "  ", "    ", "        ", "                ", "                                "};

    public static final void leftPad(java.lang.StringBuilder sb, java.lang.String str, int i) {
        int i2 = 0;
        if (str != null) {
            i2 = str.length();
        }
        if (i2 < i) {
            spacePad(sb, i - i2);
        }
        if (str != null) {
            sb.append(str);
        }
    }

    public static final void rightPad(java.lang.StringBuilder sb, java.lang.String str, int i) {
        int i2 = 0;
        if (str != null) {
            i2 = str.length();
        }
        if (str != null) {
            sb.append(str);
        }
        if (i2 < i) {
            spacePad(sb, i - i2);
        }
    }

    public static final void spacePad(java.lang.StringBuilder sb, int i) {
        while (i >= 32) {
            sb.append(SPACES[5]);
            i -= 32;
        }
        for (int i2 = 4; i2 >= 0; i2--) {
            if (((1 << i2) & i) != 0) {
                sb.append(SPACES[i2]);
            }
        }
    }
}
