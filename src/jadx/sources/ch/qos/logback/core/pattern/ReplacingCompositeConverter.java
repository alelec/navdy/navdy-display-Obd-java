package ch.qos.logback.core.pattern;

public class ReplacingCompositeConverter<E> extends ch.qos.logback.core.pattern.CompositeConverter<E> {
    java.util.regex.Pattern pattern;
    java.lang.String regex;
    java.lang.String replacement;

    public void start() {
        java.util.List optionList = getOptionList();
        if (optionList == null) {
            addError("at least two options are expected whereas you have declared none");
            return;
        }
        int size = optionList.size();
        if (size < 2) {
            addError("at least two options are expected whereas you have declared only " + size + "as [" + optionList + "]");
            return;
        }
        this.regex = (java.lang.String) optionList.get(0);
        this.pattern = java.util.regex.Pattern.compile(this.regex);
        this.replacement = (java.lang.String) optionList.get(1);
        super.start();
    }

    /* access modifiers changed from: protected */
    public java.lang.String transform(E e, java.lang.String str) {
        return !this.started ? str : this.pattern.matcher(str).replaceAll(this.replacement);
    }
}
