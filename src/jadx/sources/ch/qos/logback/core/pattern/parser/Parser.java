package ch.qos.logback.core.pattern.parser;

public class Parser<E> extends ch.qos.logback.core.spi.ContextAwareBase {
    public static final java.util.Map<java.lang.String, java.lang.String> DEFAULT_COMPOSITE_CONVERTER_MAP = new java.util.HashMap();
    public static final java.lang.String MISSING_RIGHT_PARENTHESIS = "http://logback.qos.ch/codes.html#missingRightParenthesis";
    public static final java.lang.String REPLACE_CONVERTER_WORD = "replace";
    int pointer;
    final java.util.List tokenList;

    static {
        DEFAULT_COMPOSITE_CONVERTER_MAP.put(ch.qos.logback.core.pattern.parser.Token.BARE_COMPOSITE_KEYWORD_TOKEN.getValue().toString(), ch.qos.logback.core.pattern.IdentityCompositeConverter.class.getName());
        DEFAULT_COMPOSITE_CONVERTER_MAP.put(REPLACE_CONVERTER_WORD, ch.qos.logback.core.pattern.ReplacingCompositeConverter.class.getName());
    }

    Parser(ch.qos.logback.core.pattern.parser.TokenStream tokenStream) throws ch.qos.logback.core.spi.ScanException {
        this.pointer = 0;
        this.tokenList = tokenStream.tokenize();
    }

    public Parser(java.lang.String str) throws ch.qos.logback.core.spi.ScanException {
        this(str, new ch.qos.logback.core.pattern.util.RegularEscapeUtil());
    }

    public Parser(java.lang.String str, ch.qos.logback.core.pattern.util.IEscapeUtil iEscapeUtil) throws ch.qos.logback.core.spi.ScanException {
        this.pointer = 0;
        try {
            this.tokenList = new ch.qos.logback.core.pattern.parser.TokenStream(str, iEscapeUtil).tokenize();
        } catch (java.lang.IllegalArgumentException e) {
            throw new ch.qos.logback.core.spi.ScanException("Failed to initialize Parser", e);
        }
    }

    /* access modifiers changed from: 0000 */
    public ch.qos.logback.core.pattern.parser.FormattingNode C() throws ch.qos.logback.core.spi.ScanException {
        ch.qos.logback.core.pattern.parser.Token curentToken = getCurentToken();
        expectNotNull(curentToken, "a LEFT_PARENTHESIS or KEYWORD");
        switch (curentToken.getType()) {
            case android.support.v4.view.PointerIconCompat.TYPE_WAIT /*1004*/:
                return SINGLE();
            case 1005:
                advanceTokenPointer();
                return COMPOSITE(curentToken.getValue().toString());
            default:
                throw new java.lang.IllegalStateException("Unexpected token " + curentToken);
        }
    }

    /* access modifiers changed from: 0000 */
    public ch.qos.logback.core.pattern.parser.FormattingNode COMPOSITE(java.lang.String str) throws ch.qos.logback.core.spi.ScanException {
        ch.qos.logback.core.pattern.parser.CompositeNode compositeNode = new ch.qos.logback.core.pattern.parser.CompositeNode(str);
        compositeNode.setChildNode(E());
        ch.qos.logback.core.pattern.parser.Token nextToken = getNextToken();
        if (nextToken == null || nextToken.getType() != 41) {
            java.lang.String str2 = "Expecting RIGHT_PARENTHESIS token but got " + nextToken;
            addError(str2);
            addError("See also http://logback.qos.ch/codes.html#missingRightParenthesis");
            throw new ch.qos.logback.core.spi.ScanException(str2);
        }
        ch.qos.logback.core.pattern.parser.Token curentToken = getCurentToken();
        if (curentToken != null && curentToken.getType() == 1006) {
            compositeNode.setOptions((java.util.List) curentToken.getValue());
            advanceTokenPointer();
        }
        return compositeNode;
    }

    /* access modifiers changed from: 0000 */
    public ch.qos.logback.core.pattern.parser.Node E() throws ch.qos.logback.core.spi.ScanException {
        ch.qos.logback.core.pattern.parser.Node T = T();
        if (T == null) {
            return null;
        }
        ch.qos.logback.core.pattern.parser.Node Eopt = Eopt();
        if (Eopt == null) {
            return T;
        }
        T.setNext(Eopt);
        return T;
    }

    /* access modifiers changed from: 0000 */
    public ch.qos.logback.core.pattern.parser.Node Eopt() throws ch.qos.logback.core.spi.ScanException {
        if (getCurentToken() == null) {
            return null;
        }
        return E();
    }

    /* access modifiers changed from: 0000 */
    public ch.qos.logback.core.pattern.parser.FormattingNode SINGLE() throws ch.qos.logback.core.spi.ScanException {
        ch.qos.logback.core.pattern.parser.SimpleKeywordNode simpleKeywordNode = new ch.qos.logback.core.pattern.parser.SimpleKeywordNode(getNextToken().getValue());
        ch.qos.logback.core.pattern.parser.Token curentToken = getCurentToken();
        if (curentToken != null && curentToken.getType() == 1006) {
            simpleKeywordNode.setOptions((java.util.List) curentToken.getValue());
            advanceTokenPointer();
        }
        return simpleKeywordNode;
    }

    /* access modifiers changed from: 0000 */
    public ch.qos.logback.core.pattern.parser.Node T() throws ch.qos.logback.core.spi.ScanException {
        ch.qos.logback.core.pattern.parser.Token curentToken = getCurentToken();
        expectNotNull(curentToken, "a LITERAL or '%'");
        switch (curentToken.getType()) {
            case android.support.v4.view.MotionEventCompat.AXIS_GENERIC_6 /*37*/:
                advanceTokenPointer();
                ch.qos.logback.core.pattern.parser.Token curentToken2 = getCurentToken();
                expectNotNull(curentToken2, "a FORMAT_MODIFIER, SIMPLE_KEYWORD or COMPOUND_KEYWORD");
                if (curentToken2.getType() != 1002) {
                    return C();
                }
                ch.qos.logback.core.pattern.FormatInfo valueOf = ch.qos.logback.core.pattern.FormatInfo.valueOf((java.lang.String) curentToken2.getValue());
                advanceTokenPointer();
                ch.qos.logback.core.pattern.parser.FormattingNode C = C();
                C.setFormatInfo(valueOf);
                return C;
            case 1000:
                advanceTokenPointer();
                return new ch.qos.logback.core.pattern.parser.Node(0, curentToken.getValue());
            default:
                return null;
        }
    }

    /* access modifiers changed from: 0000 */
    public void advanceTokenPointer() {
        this.pointer++;
    }

    public ch.qos.logback.core.pattern.Converter<E> compile(ch.qos.logback.core.pattern.parser.Node node, java.util.Map map) {
        ch.qos.logback.core.pattern.parser.Compiler compiler = new ch.qos.logback.core.pattern.parser.Compiler(node, map);
        compiler.setContext(this.context);
        return compiler.compile();
    }

    /* access modifiers changed from: 0000 */
    public void expectNotNull(ch.qos.logback.core.pattern.parser.Token token, java.lang.String str) {
        if (token == null) {
            throw new java.lang.IllegalStateException("All tokens consumed but was expecting " + str);
        }
    }

    /* access modifiers changed from: 0000 */
    public ch.qos.logback.core.pattern.parser.Token getCurentToken() {
        if (this.pointer < this.tokenList.size()) {
            return (ch.qos.logback.core.pattern.parser.Token) this.tokenList.get(this.pointer);
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    public ch.qos.logback.core.pattern.parser.Token getNextToken() {
        if (this.pointer >= this.tokenList.size()) {
            return null;
        }
        java.util.List list = this.tokenList;
        int i = this.pointer;
        this.pointer = i + 1;
        return (ch.qos.logback.core.pattern.parser.Token) list.get(i);
    }

    public ch.qos.logback.core.pattern.parser.Node parse() throws ch.qos.logback.core.spi.ScanException {
        return E();
    }
}
