package ch.qos.logback.core.pattern.color;

public class GrayCompositeConverter<E> extends ch.qos.logback.core.pattern.color.ForegroundCompositeConverterBase<E> {
    /* access modifiers changed from: protected */
    public java.lang.String getForegroundColorCode(E e) {
        return "1;30";
    }
}
