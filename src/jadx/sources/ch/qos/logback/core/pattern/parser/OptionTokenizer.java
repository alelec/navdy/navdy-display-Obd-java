package ch.qos.logback.core.pattern.parser;

public class OptionTokenizer {
    private static final int EXPECTING_STATE = 0;
    private static final int QUOTED_COLLECTING_STATE = 2;
    private static final int RAW_COLLECTING_STATE = 1;
    final ch.qos.logback.core.pattern.util.IEscapeUtil escapeUtil;
    final java.lang.String pattern;
    final int patternLength;
    char quoteChar;
    int state;
    final ch.qos.logback.core.pattern.parser.TokenStream tokenStream;

    OptionTokenizer(ch.qos.logback.core.pattern.parser.TokenStream tokenStream2) {
        this(tokenStream2, new ch.qos.logback.core.pattern.util.AsIsEscapeUtil());
    }

    OptionTokenizer(ch.qos.logback.core.pattern.parser.TokenStream tokenStream2, ch.qos.logback.core.pattern.util.IEscapeUtil iEscapeUtil) {
        this.state = 0;
        this.tokenStream = tokenStream2;
        this.pattern = tokenStream2.pattern;
        this.patternLength = tokenStream2.patternLength;
        this.escapeUtil = iEscapeUtil;
    }

    /* access modifiers changed from: 0000 */
    public void emitOptionToken(java.util.List<ch.qos.logback.core.pattern.parser.Token> list, java.util.List<java.lang.String> list2) {
        list.add(new ch.qos.logback.core.pattern.parser.Token(android.support.v4.view.PointerIconCompat.TYPE_CELL, list2));
        this.tokenStream.state = ch.qos.logback.core.pattern.parser.TokenStream.TokenizerState.LITERAL_STATE;
    }

    /* access modifiers changed from: 0000 */
    public void escape(java.lang.String str, java.lang.StringBuffer stringBuffer) {
        if (this.tokenStream.pointer < this.patternLength) {
            java.lang.String str2 = this.pattern;
            ch.qos.logback.core.pattern.parser.TokenStream tokenStream2 = this.tokenStream;
            int i = tokenStream2.pointer;
            tokenStream2.pointer = i + 1;
            this.escapeUtil.escape(str, stringBuffer, str2.charAt(i), this.tokenStream.pointer);
        }
    }

    /* access modifiers changed from: 0000 */
    public void tokenize(char c, java.util.List<ch.qos.logback.core.pattern.parser.Token> list) throws ch.qos.logback.core.spi.ScanException {
        java.lang.StringBuffer stringBuffer = new java.lang.StringBuffer();
        java.util.ArrayList arrayList = new java.util.ArrayList();
        while (this.tokenStream.pointer < this.patternLength) {
            switch (this.state) {
                case 0:
                    switch (c) {
                        case 9:
                        case 10:
                        case 13:
                        case ' ':
                        case android.support.v4.view.MotionEventCompat.AXIS_GENERIC_13 /*44*/:
                            break;
                        case android.support.v4.view.MotionEventCompat.AXIS_GENERIC_3 /*34*/:
                        case android.support.v4.view.MotionEventCompat.AXIS_GENERIC_8 /*39*/:
                            this.state = 2;
                            this.quoteChar = c;
                            break;
                        case '}':
                            emitOptionToken(list, arrayList);
                            return;
                        default:
                            stringBuffer.append(c);
                            this.state = 1;
                            break;
                    }
                case 1:
                    switch (c) {
                        case android.support.v4.view.MotionEventCompat.AXIS_GENERIC_13 /*44*/:
                            arrayList.add(stringBuffer.toString().trim());
                            stringBuffer.setLength(0);
                            this.state = 0;
                            break;
                        case '}':
                            arrayList.add(stringBuffer.toString().trim());
                            emitOptionToken(list, arrayList);
                            return;
                        default:
                            stringBuffer.append(c);
                            break;
                    }
                case 2:
                    if (c != this.quoteChar) {
                        if (c != '\\') {
                            stringBuffer.append(c);
                            break;
                        } else {
                            escape(java.lang.String.valueOf(this.quoteChar), stringBuffer);
                            break;
                        }
                    } else {
                        arrayList.add(stringBuffer.toString());
                        stringBuffer.setLength(0);
                        this.state = 0;
                        break;
                    }
            }
            c = this.pattern.charAt(this.tokenStream.pointer);
            this.tokenStream.pointer++;
        }
        if (c != '}') {
            throw new ch.qos.logback.core.spi.ScanException("Unexpected end of pattern string in OptionTokenizer");
        } else if (this.state == 0) {
            emitOptionToken(list, arrayList);
        } else if (this.state == 1) {
            arrayList.add(stringBuffer.toString().trim());
            emitOptionToken(list, arrayList);
        } else {
            throw new ch.qos.logback.core.spi.ScanException("Unexpected end of pattern string in OptionTokenizer");
        }
    }
}
