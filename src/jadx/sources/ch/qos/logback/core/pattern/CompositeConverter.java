package ch.qos.logback.core.pattern;

public abstract class CompositeConverter<E> extends ch.qos.logback.core.pattern.DynamicConverter<E> {
    ch.qos.logback.core.pattern.Converter<E> childConverter;

    public java.lang.String convert(E e) {
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        for (ch.qos.logback.core.pattern.Converter<E> converter = this.childConverter; converter != null; converter = converter.next) {
            converter.write(sb, e);
        }
        return transform(e, sb.toString());
    }

    public ch.qos.logback.core.pattern.Converter<E> getChildConverter() {
        return this.childConverter;
    }

    public void setChildConverter(ch.qos.logback.core.pattern.Converter<E> converter) {
        this.childConverter = converter;
    }

    public java.lang.String toString() {
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        sb.append("CompositeConverter<");
        if (this.formattingInfo != null) {
            sb.append(this.formattingInfo);
        }
        if (this.childConverter != null) {
            sb.append(", children: ").append(this.childConverter);
        }
        sb.append(">");
        return sb.toString();
    }

    /* access modifiers changed from: protected */
    public abstract java.lang.String transform(E e, java.lang.String str);
}
