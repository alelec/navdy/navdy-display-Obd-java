package ch.qos.logback.core.pattern;

public abstract class Converter<E> {
    ch.qos.logback.core.pattern.Converter<E> next;

    public abstract java.lang.String convert(E e);

    public final ch.qos.logback.core.pattern.Converter<E> getNext() {
        return this.next;
    }

    public final void setNext(ch.qos.logback.core.pattern.Converter<E> converter) {
        if (this.next != null) {
            throw new java.lang.IllegalStateException("Next converter has been already set");
        }
        this.next = converter;
    }

    public void write(java.lang.StringBuilder sb, E e) {
        sb.append(convert(e));
    }
}
