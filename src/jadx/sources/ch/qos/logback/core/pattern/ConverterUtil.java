package ch.qos.logback.core.pattern;

public class ConverterUtil {
    public static <E> ch.qos.logback.core.pattern.Converter<E> findTail(ch.qos.logback.core.pattern.Converter<E> converter) {
        while (converter != null) {
            ch.qos.logback.core.pattern.Converter<E> next = converter.getNext();
            if (next == null) {
                break;
            }
            converter = next;
        }
        return converter;
    }

    public static <E> void setContextForConverters(ch.qos.logback.core.Context context, ch.qos.logback.core.pattern.Converter<E> converter) {
        for (ch.qos.logback.core.pattern.Converter<E> converter2 = converter; converter2 != null; converter2 = converter2.getNext()) {
            if (converter2 instanceof ch.qos.logback.core.spi.ContextAware) {
                ((ch.qos.logback.core.spi.ContextAware) converter2).setContext(context);
            }
        }
    }

    public static <E> void startConverters(ch.qos.logback.core.pattern.Converter<E> converter) {
        for (ch.qos.logback.core.pattern.Converter<E> converter2 = converter; converter2 != null; converter2 = converter2.getNext()) {
            if (converter2 instanceof ch.qos.logback.core.pattern.CompositeConverter) {
                ch.qos.logback.core.pattern.CompositeConverter compositeConverter = (ch.qos.logback.core.pattern.CompositeConverter) converter2;
                startConverters(compositeConverter.childConverter);
                compositeConverter.start();
            } else if (converter2 instanceof ch.qos.logback.core.pattern.DynamicConverter) {
                ((ch.qos.logback.core.pattern.DynamicConverter) converter2).start();
            }
        }
    }
}
