package ch.qos.logback.core.pattern;

public abstract class FormattingConverter<E> extends ch.qos.logback.core.pattern.Converter<E> {
    static final int INITIAL_BUF_SIZE = 256;
    static final int MAX_CAPACITY = 1024;
    ch.qos.logback.core.pattern.FormatInfo formattingInfo;

    public final ch.qos.logback.core.pattern.FormatInfo getFormattingInfo() {
        return this.formattingInfo;
    }

    public final void setFormattingInfo(ch.qos.logback.core.pattern.FormatInfo formatInfo) {
        if (this.formattingInfo != null) {
            throw new java.lang.IllegalStateException("FormattingInfo has been already set");
        }
        this.formattingInfo = formatInfo;
    }

    public final void write(java.lang.StringBuilder sb, E e) {
        java.lang.String convert = convert(e);
        if (this.formattingInfo == null) {
            sb.append(convert);
            return;
        }
        int min = this.formattingInfo.getMin();
        int max = this.formattingInfo.getMax();
        if (convert != null) {
            int length = convert.length();
            if (length > max) {
                if (this.formattingInfo.isLeftTruncate()) {
                    sb.append(convert.substring(length - max));
                } else {
                    sb.append(convert.substring(0, max));
                }
            } else if (length >= min) {
                sb.append(convert);
            } else if (this.formattingInfo.isLeftPad()) {
                ch.qos.logback.core.pattern.SpacePadder.leftPad(sb, convert, min);
            } else {
                ch.qos.logback.core.pattern.SpacePadder.rightPad(sb, convert, min);
            }
        } else if (min > 0) {
            ch.qos.logback.core.pattern.SpacePadder.spacePad(sb, min);
        }
    }
}
