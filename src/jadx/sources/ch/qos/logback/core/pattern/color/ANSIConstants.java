package ch.qos.logback.core.pattern.color;

@java.lang.Deprecated
public class ANSIConstants {
    public static final java.lang.String BLACK_FG = "30";
    public static final java.lang.String BLUE_FG = "34";
    public static final java.lang.String BOLD = "1;";
    public static final java.lang.String CYAN_FG = "36";
    public static final java.lang.String DEFAULT_FG = "39";
    public static final java.lang.String ESC_END = "m";
    public static final java.lang.String ESC_START = "\u001b[";
    public static final java.lang.String GREEN_FG = "32";
    public static final java.lang.String MAGENTA_FG = "35";
    public static final java.lang.String RED_FG = "31";
    public static final java.lang.String WHITE_FG = "37";
    public static final java.lang.String YELLOW_FG = "33";
}
