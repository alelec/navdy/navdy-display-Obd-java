package ch.qos.logback.core.pattern.parser;

class Compiler<E> extends ch.qos.logback.core.spi.ContextAwareBase {
    final java.util.Map converterMap;
    ch.qos.logback.core.pattern.Converter<E> head;
    ch.qos.logback.core.pattern.Converter<E> tail;
    final ch.qos.logback.core.pattern.parser.Node top;

    Compiler(ch.qos.logback.core.pattern.parser.Node node, java.util.Map map) {
        this.top = node;
        this.converterMap = map;
    }

    private void addToList(ch.qos.logback.core.pattern.Converter<E> converter) {
        if (this.head == null) {
            this.tail = converter;
            this.head = converter;
            return;
        }
        this.tail.setNext(converter);
        this.tail = converter;
    }

    /* access modifiers changed from: 0000 */
    public ch.qos.logback.core.pattern.Converter<E> compile() {
        this.tail = null;
        this.head = null;
        for (ch.qos.logback.core.pattern.parser.Node node = this.top; node != null; node = node.next) {
            switch (node.type) {
                case 0:
                    addToList(new ch.qos.logback.core.pattern.LiteralConverter((java.lang.String) node.getValue()));
                    break;
                case 1:
                    ch.qos.logback.core.pattern.parser.SimpleKeywordNode simpleKeywordNode = (ch.qos.logback.core.pattern.parser.SimpleKeywordNode) node;
                    ch.qos.logback.core.pattern.DynamicConverter createConverter = createConverter(simpleKeywordNode);
                    if (createConverter == null) {
                        ch.qos.logback.core.pattern.LiteralConverter literalConverter = new ch.qos.logback.core.pattern.LiteralConverter("%PARSER_ERROR[" + simpleKeywordNode.getValue() + "]");
                        addStatus(new ch.qos.logback.core.status.ErrorStatus("[" + simpleKeywordNode.getValue() + "] is not a valid conversion word", this));
                        addToList(literalConverter);
                        break;
                    } else {
                        createConverter.setFormattingInfo(simpleKeywordNode.getFormatInfo());
                        createConverter.setOptionList(simpleKeywordNode.getOptions());
                        addToList(createConverter);
                        break;
                    }
                case 2:
                    ch.qos.logback.core.pattern.parser.CompositeNode compositeNode = (ch.qos.logback.core.pattern.parser.CompositeNode) node;
                    ch.qos.logback.core.pattern.CompositeConverter createCompositeConverter = createCompositeConverter(compositeNode);
                    if (createCompositeConverter != null) {
                        createCompositeConverter.setFormattingInfo(compositeNode.getFormatInfo());
                        createCompositeConverter.setOptionList(compositeNode.getOptions());
                        ch.qos.logback.core.pattern.parser.Compiler compiler = new ch.qos.logback.core.pattern.parser.Compiler(compositeNode.getChildNode(), this.converterMap);
                        compiler.setContext(this.context);
                        createCompositeConverter.setChildConverter(compiler.compile());
                        addToList(createCompositeConverter);
                        break;
                    } else {
                        addError("Failed to create converter for [%" + compositeNode.getValue() + "] keyword");
                        addToList(new ch.qos.logback.core.pattern.LiteralConverter("%PARSER_ERROR[" + compositeNode.getValue() + "]"));
                        break;
                    }
            }
        }
        return this.head;
    }

    /* access modifiers changed from: 0000 */
    public ch.qos.logback.core.pattern.CompositeConverter<E> createCompositeConverter(ch.qos.logback.core.pattern.parser.CompositeNode compositeNode) {
        java.lang.String str = (java.lang.String) compositeNode.getValue();
        java.lang.String str2 = (java.lang.String) this.converterMap.get(str);
        if (str2 != null) {
            try {
                return (ch.qos.logback.core.pattern.CompositeConverter) ch.qos.logback.core.util.OptionHelper.instantiateByClassName(str2, ch.qos.logback.core.pattern.CompositeConverter.class, this.context);
            } catch (java.lang.Exception e) {
                addError("Failed to instantiate converter class [" + str2 + "] as a composite converter for keyword [" + str + "]", e);
                return null;
            }
        } else {
            addError("There is no conversion class registered for composite conversion word [" + str + "]");
            return null;
        }
    }

    /* access modifiers changed from: 0000 */
    public ch.qos.logback.core.pattern.DynamicConverter<E> createConverter(ch.qos.logback.core.pattern.parser.SimpleKeywordNode simpleKeywordNode) {
        java.lang.String str = (java.lang.String) simpleKeywordNode.getValue();
        java.lang.String str2 = (java.lang.String) this.converterMap.get(str);
        if (str2 != null) {
            try {
                return (ch.qos.logback.core.pattern.DynamicConverter) ch.qos.logback.core.util.OptionHelper.instantiateByClassName(str2, ch.qos.logback.core.pattern.DynamicConverter.class, this.context);
            } catch (java.lang.Exception e) {
                addError("Failed to instantiate converter class [" + str2 + "] for keyword [" + str + "]", e);
                return null;
            }
        } else {
            addError("There is no conversion class registered for conversion word [" + str + "]");
            return null;
        }
    }
}
