package ch.qos.logback.core.pattern.parser;

class TokenStream {
    final ch.qos.logback.core.pattern.util.IEscapeUtil escapeUtil;
    final ch.qos.logback.core.pattern.util.IEscapeUtil optionEscapeUtil;
    final java.lang.String pattern;
    final int patternLength;
    int pointer;
    ch.qos.logback.core.pattern.parser.TokenStream.TokenizerState state;

    enum TokenizerState {
        LITERAL_STATE,
        FORMAT_MODIFIER_STATE,
        KEYWORD_STATE,
        OPTION_STATE,
        RIGHT_PARENTHESIS_STATE
    }

    TokenStream(java.lang.String str) {
        this(str, new ch.qos.logback.core.pattern.util.RegularEscapeUtil());
    }

    TokenStream(java.lang.String str, ch.qos.logback.core.pattern.util.IEscapeUtil iEscapeUtil) {
        this.optionEscapeUtil = new ch.qos.logback.core.pattern.util.RestrictedEscapeUtil();
        this.state = ch.qos.logback.core.pattern.parser.TokenStream.TokenizerState.LITERAL_STATE;
        this.pointer = 0;
        if (str == null || str.length() == 0) {
            throw new java.lang.IllegalArgumentException("null or empty pattern string not allowed");
        }
        this.pattern = str;
        this.patternLength = str.length();
        this.escapeUtil = iEscapeUtil;
    }

    private void addValuedToken(int i, java.lang.StringBuffer stringBuffer, java.util.List<ch.qos.logback.core.pattern.parser.Token> list) {
        if (stringBuffer.length() > 0) {
            list.add(new ch.qos.logback.core.pattern.parser.Token(i, stringBuffer.toString()));
            stringBuffer.setLength(0);
        }
    }

    private void handleFormatModifierState(char c, java.util.List<ch.qos.logback.core.pattern.parser.Token> list, java.lang.StringBuffer stringBuffer) {
        if (c == '(') {
            addValuedToken(android.support.v4.view.PointerIconCompat.TYPE_HAND, stringBuffer, list);
            list.add(ch.qos.logback.core.pattern.parser.Token.BARE_COMPOSITE_KEYWORD_TOKEN);
            this.state = ch.qos.logback.core.pattern.parser.TokenStream.TokenizerState.LITERAL_STATE;
        } else if (java.lang.Character.isJavaIdentifierStart(c)) {
            addValuedToken(android.support.v4.view.PointerIconCompat.TYPE_HAND, stringBuffer, list);
            this.state = ch.qos.logback.core.pattern.parser.TokenStream.TokenizerState.KEYWORD_STATE;
            stringBuffer.append(c);
        } else {
            stringBuffer.append(c);
        }
    }

    private void handleKeywordState(char c, java.util.List<ch.qos.logback.core.pattern.parser.Token> list, java.lang.StringBuffer stringBuffer) {
        if (java.lang.Character.isJavaIdentifierPart(c)) {
            stringBuffer.append(c);
        } else if (c == '{') {
            addValuedToken(android.support.v4.view.PointerIconCompat.TYPE_WAIT, stringBuffer, list);
            this.state = ch.qos.logback.core.pattern.parser.TokenStream.TokenizerState.OPTION_STATE;
        } else if (c == '(') {
            addValuedToken(1005, stringBuffer, list);
            this.state = ch.qos.logback.core.pattern.parser.TokenStream.TokenizerState.LITERAL_STATE;
        } else if (c == '%') {
            addValuedToken(android.support.v4.view.PointerIconCompat.TYPE_WAIT, stringBuffer, list);
            list.add(ch.qos.logback.core.pattern.parser.Token.PERCENT_TOKEN);
            this.state = ch.qos.logback.core.pattern.parser.TokenStream.TokenizerState.FORMAT_MODIFIER_STATE;
        } else if (c == ')') {
            addValuedToken(android.support.v4.view.PointerIconCompat.TYPE_WAIT, stringBuffer, list);
            this.state = ch.qos.logback.core.pattern.parser.TokenStream.TokenizerState.RIGHT_PARENTHESIS_STATE;
        } else {
            addValuedToken(android.support.v4.view.PointerIconCompat.TYPE_WAIT, stringBuffer, list);
            if (c != '\\') {
                stringBuffer.append(c);
            } else if (this.pointer < this.patternLength) {
                java.lang.String str = this.pattern;
                int i = this.pointer;
                this.pointer = i + 1;
                this.escapeUtil.escape("%()", stringBuffer, str.charAt(i), this.pointer);
            }
            this.state = ch.qos.logback.core.pattern.parser.TokenStream.TokenizerState.LITERAL_STATE;
        }
    }

    private void handleLiteralState(char c, java.util.List<ch.qos.logback.core.pattern.parser.Token> list, java.lang.StringBuffer stringBuffer) {
        switch (c) {
            case android.support.v4.view.MotionEventCompat.AXIS_GENERIC_6 /*37*/:
                addValuedToken(1000, stringBuffer, list);
                list.add(ch.qos.logback.core.pattern.parser.Token.PERCENT_TOKEN);
                this.state = ch.qos.logback.core.pattern.parser.TokenStream.TokenizerState.FORMAT_MODIFIER_STATE;
                return;
            case android.support.v4.view.MotionEventCompat.AXIS_GENERIC_10 /*41*/:
                addValuedToken(1000, stringBuffer, list);
                this.state = ch.qos.logback.core.pattern.parser.TokenStream.TokenizerState.RIGHT_PARENTHESIS_STATE;
                return;
            case '\\':
                escape("%()", stringBuffer);
                return;
            default:
                stringBuffer.append(c);
                return;
        }
    }

    private void handleRightParenthesisState(char c, java.util.List<ch.qos.logback.core.pattern.parser.Token> list, java.lang.StringBuffer stringBuffer) {
        list.add(ch.qos.logback.core.pattern.parser.Token.RIGHT_PARENTHESIS_TOKEN);
        switch (c) {
            case android.support.v4.view.MotionEventCompat.AXIS_GENERIC_10 /*41*/:
                return;
            case '\\':
                escape("%{}", stringBuffer);
                this.state = ch.qos.logback.core.pattern.parser.TokenStream.TokenizerState.LITERAL_STATE;
                return;
            case '{':
                this.state = ch.qos.logback.core.pattern.parser.TokenStream.TokenizerState.OPTION_STATE;
                return;
            default:
                stringBuffer.append(c);
                this.state = ch.qos.logback.core.pattern.parser.TokenStream.TokenizerState.LITERAL_STATE;
                return;
        }
    }

    private void processOption(char c, java.util.List<ch.qos.logback.core.pattern.parser.Token> list, java.lang.StringBuffer stringBuffer) throws ch.qos.logback.core.spi.ScanException {
        new ch.qos.logback.core.pattern.parser.OptionTokenizer(this).tokenize(c, list);
    }

    /* access modifiers changed from: 0000 */
    public void escape(java.lang.String str, java.lang.StringBuffer stringBuffer) {
        if (this.pointer < this.patternLength) {
            java.lang.String str2 = this.pattern;
            int i = this.pointer;
            this.pointer = i + 1;
            this.escapeUtil.escape(str, stringBuffer, str2.charAt(i), this.pointer);
        }
    }

    /* access modifiers changed from: 0000 */
    public void optionEscape(java.lang.String str, java.lang.StringBuffer stringBuffer) {
        if (this.pointer < this.patternLength) {
            java.lang.String str2 = this.pattern;
            int i = this.pointer;
            this.pointer = i + 1;
            this.optionEscapeUtil.escape(str, stringBuffer, str2.charAt(i), this.pointer);
        }
    }

    /* access modifiers changed from: 0000 */
    public java.util.List tokenize() throws ch.qos.logback.core.spi.ScanException {
        java.util.ArrayList arrayList = new java.util.ArrayList();
        java.lang.StringBuffer stringBuffer = new java.lang.StringBuffer();
        while (this.pointer < this.patternLength) {
            char charAt = this.pattern.charAt(this.pointer);
            this.pointer++;
            switch (this.state) {
                case LITERAL_STATE:
                    handleLiteralState(charAt, arrayList, stringBuffer);
                    break;
                case FORMAT_MODIFIER_STATE:
                    handleFormatModifierState(charAt, arrayList, stringBuffer);
                    break;
                case OPTION_STATE:
                    processOption(charAt, arrayList, stringBuffer);
                    break;
                case KEYWORD_STATE:
                    handleKeywordState(charAt, arrayList, stringBuffer);
                    break;
                case RIGHT_PARENTHESIS_STATE:
                    handleRightParenthesisState(charAt, arrayList, stringBuffer);
                    break;
            }
        }
        switch (this.state) {
            case LITERAL_STATE:
                addValuedToken(1000, stringBuffer, arrayList);
                break;
            case FORMAT_MODIFIER_STATE:
            case OPTION_STATE:
                throw new ch.qos.logback.core.spi.ScanException("Unexpected end of pattern string");
            case KEYWORD_STATE:
                arrayList.add(new ch.qos.logback.core.pattern.parser.Token(android.support.v4.view.PointerIconCompat.TYPE_WAIT, stringBuffer.toString()));
                break;
            case RIGHT_PARENTHESIS_STATE:
                arrayList.add(ch.qos.logback.core.pattern.parser.Token.RIGHT_PARENTHESIS_TOKEN);
                break;
        }
        return arrayList;
    }
}
