package ch.qos.logback.core.pattern;

public class PatternLayoutEncoderBase<E> extends ch.qos.logback.core.encoder.LayoutWrappingEncoder<E> {
    protected boolean outputPatternAsHeader = false;
    java.lang.String pattern;

    public java.lang.String getPattern() {
        return this.pattern;
    }

    public boolean isOutputPatternAsHeader() {
        return this.outputPatternAsHeader;
    }

    public boolean isOutputPatternAsPresentationHeader() {
        return this.outputPatternAsHeader;
    }

    public void setLayout(ch.qos.logback.core.Layout<E> layout) {
        throw new java.lang.UnsupportedOperationException("one cannot set the layout of " + getClass().getName());
    }

    public void setOutputPatternAsHeader(boolean z) {
        this.outputPatternAsHeader = z;
    }

    public void setOutputPatternAsPresentationHeader(boolean z) {
        addWarn("[outputPatternAsPresentationHeader] property is deprecated. Please use [outputPatternAsHeader] option instead.");
        this.outputPatternAsHeader = z;
    }

    public void setPattern(java.lang.String str) {
        this.pattern = str;
    }
}
