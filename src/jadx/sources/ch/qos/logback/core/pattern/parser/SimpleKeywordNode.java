package ch.qos.logback.core.pattern.parser;

public class SimpleKeywordNode extends ch.qos.logback.core.pattern.parser.FormattingNode {
    java.util.List<java.lang.String> optionList;

    protected SimpleKeywordNode(int i, java.lang.Object obj) {
        super(i, obj);
    }

    SimpleKeywordNode(java.lang.Object obj) {
        super(1, obj);
    }

    public boolean equals(java.lang.Object obj) {
        if (!super.equals(obj) || !(obj instanceof ch.qos.logback.core.pattern.parser.SimpleKeywordNode)) {
            return false;
        }
        ch.qos.logback.core.pattern.parser.SimpleKeywordNode simpleKeywordNode = (ch.qos.logback.core.pattern.parser.SimpleKeywordNode) obj;
        return this.optionList != null ? this.optionList.equals(simpleKeywordNode.optionList) : simpleKeywordNode.optionList == null;
    }

    public java.util.List<java.lang.String> getOptions() {
        return this.optionList;
    }

    public int hashCode() {
        return super.hashCode();
    }

    public void setOptions(java.util.List<java.lang.String> list) {
        this.optionList = list;
    }

    public java.lang.String toString() {
        java.lang.StringBuffer stringBuffer = new java.lang.StringBuffer();
        if (this.optionList == null) {
            stringBuffer.append("KeyWord(" + this.value + "," + this.formatInfo + ")");
        } else {
            stringBuffer.append("KeyWord(" + this.value + ", " + this.formatInfo + "," + this.optionList + ")");
        }
        stringBuffer.append(printNext());
        return stringBuffer.toString();
    }
}
