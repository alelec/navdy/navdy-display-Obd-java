package ch.qos.logback.core.pattern;

public interface PostCompileProcessor<E> {
    void process(ch.qos.logback.core.pattern.Converter<E> converter);
}
