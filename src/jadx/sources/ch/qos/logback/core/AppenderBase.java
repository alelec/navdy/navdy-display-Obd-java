package ch.qos.logback.core;

public abstract class AppenderBase<E> extends ch.qos.logback.core.spi.ContextAwareBase implements ch.qos.logback.core.Appender<E> {
    static final int ALLOWED_REPEATS = 5;
    private int exceptionCount = 0;
    private ch.qos.logback.core.spi.FilterAttachableImpl<E> fai = new ch.qos.logback.core.spi.FilterAttachableImpl<>();
    private boolean guard = false;
    protected java.lang.String name;
    protected boolean started = false;
    private int statusRepeatCount = 0;

    public void addFilter(ch.qos.logback.core.filter.Filter<E> filter) {
        this.fai.addFilter(filter);
    }

    /* access modifiers changed from: protected */
    public abstract void append(E e);

    public void clearAllFilters() {
        this.fai.clearAllFilters();
    }

    public synchronized void doAppend(E e) {
        if (!this.guard) {
            try {
                this.guard = true;
                if (!this.started) {
                    int i = this.statusRepeatCount;
                    this.statusRepeatCount = i + 1;
                    if (i < 5) {
                        addStatus(new ch.qos.logback.core.status.WarnStatus("Attempted to append to non started appender [" + this.name + "].", this));
                    }
                    this.guard = false;
                } else if (getFilterChainDecision(e) == ch.qos.logback.core.spi.FilterReply.DENY) {
                    this.guard = false;
                } else {
                    append(e);
                    this.guard = false;
                }
            } catch (java.lang.Exception e2) {
                int i2 = this.exceptionCount;
                this.exceptionCount = i2 + 1;
                if (i2 < 5) {
                    addError("Appender [" + this.name + "] failed to append.", e2);
                }
                this.guard = false;
            } catch (Throwable th) {
                this.guard = false;
                throw th;
            }
        }
    }

    public java.util.List<ch.qos.logback.core.filter.Filter<E>> getCopyOfAttachedFiltersList() {
        return this.fai.getCopyOfAttachedFiltersList();
    }

    public ch.qos.logback.core.spi.FilterReply getFilterChainDecision(E e) {
        return this.fai.getFilterChainDecision(e);
    }

    public java.lang.String getName() {
        return this.name;
    }

    public boolean isStarted() {
        return this.started;
    }

    public void setName(java.lang.String str) {
        this.name = str;
    }

    public void start() {
        this.started = true;
    }

    public void stop() {
        this.started = false;
    }

    public java.lang.String toString() {
        return getClass().getName() + "[" + this.name + "]";
    }
}
