package ch.qos.logback.core.encoder;

public class EventObjectInputStream<E> extends java.io.InputStream {
    java.util.List<E> buffer = new java.util.ArrayList();
    int index = 0;
    ch.qos.logback.core.encoder.NonClosableInputStream ncis;

    EventObjectInputStream(java.io.InputStream inputStream) throws java.io.IOException {
        this.ncis = new ch.qos.logback.core.encoder.NonClosableInputStream(inputStream);
    }

    private void internalReset() {
        this.index = 0;
        this.buffer.clear();
    }

    public int available() throws java.io.IOException {
        return this.ncis.available();
    }

    public void close() throws java.io.IOException {
        this.ncis.realClose();
    }

    /* access modifiers changed from: 0000 */
    public E getFromBuffer() {
        if (this.index >= this.buffer.size()) {
            return null;
        }
        java.util.List<E> list = this.buffer;
        int i = this.index;
        this.index = i + 1;
        return list.get(i);
    }

    public int read() throws java.io.IOException {
        throw new java.lang.UnsupportedOperationException("Only the readEvent method is supported.");
    }

    public E readEvent() throws java.io.IOException {
        E fromBuffer = getFromBuffer();
        if (fromBuffer != null) {
            return fromBuffer;
        }
        internalReset();
        int readHeader = readHeader();
        if (readHeader == -1) {
            return null;
        }
        readPayload(readHeader);
        readFooter(readHeader);
        return getFromBuffer();
    }

    /* access modifiers changed from: 0000 */
    public E readEvents(java.io.ObjectInputStream objectInputStream) throws java.io.IOException {
        java.lang.ClassNotFoundException e;
        E e2;
        try {
            e2 = objectInputStream.readObject();
            try {
                this.buffer.add(e2);
            } catch (java.lang.ClassNotFoundException e3) {
                e = e3;
                e.printStackTrace();
                return e2;
            }
        } catch (java.lang.ClassNotFoundException e4) {
            java.lang.ClassNotFoundException classNotFoundException = e4;
            e2 = null;
            e = classNotFoundException;
        }
        return e2;
    }

    /* access modifiers changed from: 0000 */
    public void readFooter(int i) throws java.io.IOException {
        byte[] bArr = new byte[8];
        if (this.ncis.read(bArr) == -1) {
            throw new java.lang.IllegalStateException("Looks like a corrupt stream");
        } else if (ch.qos.logback.core.encoder.ByteArrayUtil.readInt(bArr, 0) != 640373619) {
            throw new java.lang.IllegalStateException("Looks like a corrupt stream");
        } else if (ch.qos.logback.core.encoder.ByteArrayUtil.readInt(bArr, 4) != (640373619 ^ i)) {
            throw new java.lang.IllegalStateException("Invalid checksum");
        }
    }

    /* access modifiers changed from: 0000 */
    public int readHeader() throws java.io.IOException {
        int i = -1;
        byte[] bArr = new byte[16];
        if (this.ncis.read(bArr) != -1) {
            if (ch.qos.logback.core.encoder.ByteArrayUtil.readInt(bArr, 0) != 1853421169) {
                throw new java.lang.IllegalStateException("Does not look like data created by ObjectStreamEncoder");
            }
            i = ch.qos.logback.core.encoder.ByteArrayUtil.readInt(bArr, 4);
            if (ch.qos.logback.core.encoder.ByteArrayUtil.readInt(bArr, 12) != (1853421169 ^ i)) {
                throw new java.lang.IllegalStateException("Invalid checksum");
            }
        }
        return i;
    }

    /* access modifiers changed from: 0000 */
    public void readPayload(int i) throws java.io.IOException {
        java.util.ArrayList arrayList = new java.util.ArrayList(i);
        java.io.ObjectInputStream objectInputStream = new java.io.ObjectInputStream(this.ncis);
        for (int i2 = 0; i2 < i; i2++) {
            arrayList.add(readEvents(objectInputStream));
        }
        objectInputStream.close();
    }
}
