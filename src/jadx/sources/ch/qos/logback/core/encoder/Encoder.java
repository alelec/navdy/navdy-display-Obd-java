package ch.qos.logback.core.encoder;

public interface Encoder<E> extends ch.qos.logback.core.spi.ContextAware, ch.qos.logback.core.spi.LifeCycle {
    void close() throws java.io.IOException;

    void doEncode(E e) throws java.io.IOException;

    void init(java.io.OutputStream outputStream) throws java.io.IOException;
}
