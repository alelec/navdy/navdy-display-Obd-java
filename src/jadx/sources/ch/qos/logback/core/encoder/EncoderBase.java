package ch.qos.logback.core.encoder;

public abstract class EncoderBase<E> extends ch.qos.logback.core.spi.ContextAwareBase implements ch.qos.logback.core.encoder.Encoder<E> {
    protected java.io.OutputStream outputStream;
    protected boolean started;

    public void init(java.io.OutputStream outputStream2) throws java.io.IOException {
        this.outputStream = outputStream2;
    }

    public boolean isStarted() {
        return this.started;
    }

    public void start() {
        this.started = true;
    }

    public void stop() {
        this.started = false;
    }
}
