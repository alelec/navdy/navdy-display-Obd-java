package ch.qos.logback.core.encoder;

public class ObjectStreamEncoder<E> extends ch.qos.logback.core.encoder.EncoderBase<E> {
    public static final int START_PEBBLE = 1853421169;
    public static final int STOP_PEBBLE = 640373619;
    private int MAX_BUFFER_SIZE = 100;
    java.util.List<E> bufferList = new java.util.ArrayList(this.MAX_BUFFER_SIZE);

    public void close() throws java.io.IOException {
        writeBuffer();
    }

    public void doEncode(E e) throws java.io.IOException {
        this.bufferList.add(e);
        if (this.bufferList.size() == this.MAX_BUFFER_SIZE) {
            writeBuffer();
        }
    }

    public void init(java.io.OutputStream outputStream) throws java.io.IOException {
        super.init(outputStream);
        this.bufferList.clear();
    }

    /* access modifiers changed from: 0000 */
    public void writeBuffer() throws java.io.IOException {
        java.io.ByteArrayOutputStream byteArrayOutputStream = new java.io.ByteArrayOutputStream(10000);
        int size = this.bufferList.size();
        writeHeader(byteArrayOutputStream, size);
        java.io.ObjectOutputStream objectOutputStream = new java.io.ObjectOutputStream(byteArrayOutputStream);
        for (E writeObject : this.bufferList) {
            objectOutputStream.writeObject(writeObject);
        }
        this.bufferList.clear();
        objectOutputStream.flush();
        writeFooter(byteArrayOutputStream, size);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        objectOutputStream.close();
        writeEndPosition(byteArray);
        this.outputStream.write(byteArray);
    }

    /* access modifiers changed from: 0000 */
    public void writeEndPosition(byte[] bArr) {
        ch.qos.logback.core.encoder.ByteArrayUtil.writeInt(bArr, 8, bArr.length - 8);
    }

    /* access modifiers changed from: 0000 */
    public void writeFooter(java.io.ByteArrayOutputStream byteArrayOutputStream, int i) {
        ch.qos.logback.core.encoder.ByteArrayUtil.writeInt(byteArrayOutputStream, STOP_PEBBLE);
        ch.qos.logback.core.encoder.ByteArrayUtil.writeInt(byteArrayOutputStream, 640373619 ^ i);
    }

    /* access modifiers changed from: 0000 */
    public void writeHeader(java.io.ByteArrayOutputStream byteArrayOutputStream, int i) {
        ch.qos.logback.core.encoder.ByteArrayUtil.writeInt(byteArrayOutputStream, START_PEBBLE);
        ch.qos.logback.core.encoder.ByteArrayUtil.writeInt(byteArrayOutputStream, i);
        ch.qos.logback.core.encoder.ByteArrayUtil.writeInt(byteArrayOutputStream, 0);
        ch.qos.logback.core.encoder.ByteArrayUtil.writeInt(byteArrayOutputStream, 1853421169 ^ i);
    }
}
