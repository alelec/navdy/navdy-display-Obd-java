package ch.qos.logback.core.encoder;

public class EchoEncoder<E> extends ch.qos.logback.core.encoder.EncoderBase<E> {
    public void close() throws java.io.IOException {
    }

    public void doEncode(E e) throws java.io.IOException {
        this.outputStream.write((e + ch.qos.logback.core.CoreConstants.LINE_SEPARATOR).getBytes());
        this.outputStream.flush();
    }

    public void init(java.io.OutputStream outputStream) throws java.io.IOException {
        super.init(outputStream);
    }
}
