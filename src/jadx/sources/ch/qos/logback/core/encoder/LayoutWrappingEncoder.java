package ch.qos.logback.core.encoder;

public class LayoutWrappingEncoder<E> extends ch.qos.logback.core.encoder.EncoderBase<E> {
    private java.nio.charset.Charset charset;
    private boolean immediateFlush = true;
    protected ch.qos.logback.core.Layout<E> layout;

    private void appendIfNotNull(java.lang.StringBuilder sb, java.lang.String str) {
        if (str != null) {
            sb.append(str);
        }
    }

    private byte[] convertToBytes(java.lang.String str) {
        if (this.charset == null) {
            return str.getBytes();
        }
        try {
            return str.getBytes(this.charset.name());
        } catch (java.io.UnsupportedEncodingException e) {
            throw new java.lang.IllegalStateException("An existing charset cannot possibly be unsupported.");
        }
    }

    public void close() throws java.io.IOException {
        writeFooter();
    }

    public void doEncode(E e) throws java.io.IOException {
        this.outputStream.write(convertToBytes(this.layout.doLayout(e)));
        if (this.immediateFlush) {
            this.outputStream.flush();
        }
    }

    public java.nio.charset.Charset getCharset() {
        return this.charset;
    }

    public ch.qos.logback.core.Layout<E> getLayout() {
        return this.layout;
    }

    public void init(java.io.OutputStream outputStream) throws java.io.IOException {
        super.init(outputStream);
        writeHeader();
    }

    public boolean isImmediateFlush() {
        return this.immediateFlush;
    }

    public boolean isStarted() {
        return false;
    }

    public void setCharset(java.nio.charset.Charset charset2) {
        this.charset = charset2;
    }

    public void setImmediateFlush(boolean z) {
        this.immediateFlush = z;
    }

    public void setLayout(ch.qos.logback.core.Layout<E> layout2) {
        this.layout = layout2;
    }

    public void start() {
        this.started = true;
    }

    public void stop() {
        this.started = false;
        if (this.outputStream != null) {
            try {
                this.outputStream.flush();
            } catch (java.io.IOException e) {
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void writeFooter() throws java.io.IOException {
        if (this.layout != null && this.outputStream != null) {
            java.lang.StringBuilder sb = new java.lang.StringBuilder();
            appendIfNotNull(sb, this.layout.getPresentationFooter());
            appendIfNotNull(sb, this.layout.getFileFooter());
            if (sb.length() > 0) {
                this.outputStream.write(convertToBytes(sb.toString()));
                this.outputStream.flush();
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void writeHeader() throws java.io.IOException {
        if (this.layout != null && this.outputStream != null) {
            java.lang.StringBuilder sb = new java.lang.StringBuilder();
            appendIfNotNull(sb, this.layout.getFileHeader());
            appendIfNotNull(sb, this.layout.getPresentationHeader());
            if (sb.length() > 0) {
                sb.append(ch.qos.logback.core.CoreConstants.LINE_SEPARATOR);
                this.outputStream.write(convertToBytes(sb.toString()));
                this.outputStream.flush();
            }
        }
    }
}
