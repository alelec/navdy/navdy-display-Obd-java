package ch.qos.logback.core.encoder;

public class ByteArrayUtil {
    public static byte[] hexStringToByteArray(java.lang.String str) {
        byte[] bArr = new byte[(str.length() / 2)];
        for (int i = 0; i < bArr.length; i++) {
            int i2 = i * 2;
            bArr[i] = (byte) (java.lang.Integer.parseInt(str.substring(i2, i2 + 2), 16) & 255);
        }
        return bArr;
    }

    static int readInt(byte[] bArr, int i) {
        int i2 = 0;
        for (int i3 = 0; i3 < 4; i3++) {
            i2 += (bArr[i + i3] & com.navdy.obd.update.STNBootloaderChannel.ERROR_CODE_INVALID_RESPONSE) << (24 - (i3 * 8));
        }
        return i2;
    }

    public static java.lang.String toHexString(byte[] bArr) {
        java.lang.StringBuffer stringBuffer = new java.lang.StringBuffer();
        for (byte b : bArr) {
            java.lang.String hexString = java.lang.Integer.toHexString(b & com.navdy.obd.update.STNBootloaderChannel.ERROR_CODE_INVALID_RESPONSE);
            if (hexString.length() == 1) {
                stringBuffer.append('0');
            }
            stringBuffer.append(hexString);
        }
        return stringBuffer.toString();
    }

    static void writeInt(java.io.ByteArrayOutputStream byteArrayOutputStream, int i) {
        for (int i2 = 0; i2 < 4; i2++) {
            byteArrayOutputStream.write((byte) (i >>> (24 - (i2 * 8))));
        }
    }

    static void writeInt(byte[] bArr, int i, int i2) {
        for (int i3 = 0; i3 < 4; i3++) {
            bArr[i + i3] = (byte) (i2 >>> (24 - (i3 * 8)));
        }
    }
}
