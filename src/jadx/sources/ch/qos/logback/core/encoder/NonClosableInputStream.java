package ch.qos.logback.core.encoder;

public class NonClosableInputStream extends java.io.FilterInputStream {
    NonClosableInputStream(java.io.InputStream inputStream) {
        super(inputStream);
    }

    public void close() {
    }

    public void realClose() throws java.io.IOException {
        super.close();
    }
}
