package ch.qos.logback.core.recovery;

public abstract class ResilientOutputStreamBase extends java.io.OutputStream {
    static final int STATUS_COUNT_LIMIT = 8;
    private ch.qos.logback.core.Context context;
    private int noContextWarning = 0;
    protected java.io.OutputStream os;
    protected boolean presumedClean = true;
    private ch.qos.logback.core.recovery.RecoveryCoordinator recoveryCoordinator;
    private int statusCount = 0;

    private boolean isPresumedInError() {
        return this.recoveryCoordinator != null && !this.presumedClean;
    }

    private void postSuccessfulWrite() {
        if (this.recoveryCoordinator != null) {
            this.recoveryCoordinator = null;
            this.statusCount = 0;
            addStatus(new ch.qos.logback.core.status.InfoStatus("Recovered from IO failure on " + getDescription(), this));
        }
    }

    public void addStatus(ch.qos.logback.core.status.Status status) {
        if (this.context == null) {
            int i = this.noContextWarning;
            this.noContextWarning = i + 1;
            if (i == 0) {
                java.lang.System.out.println("LOGBACK: No context given for " + this);
                return;
            }
            return;
        }
        ch.qos.logback.core.status.StatusManager statusManager = this.context.getStatusManager();
        if (statusManager != null) {
            statusManager.add(status);
        }
    }

    /* access modifiers changed from: 0000 */
    public void addStatusIfCountNotOverLimit(ch.qos.logback.core.status.Status status) {
        this.statusCount++;
        if (this.statusCount < 8) {
            addStatus(status);
        }
        if (this.statusCount == 8) {
            addStatus(status);
            addStatus(new ch.qos.logback.core.status.InfoStatus("Will supress future messages regarding " + getDescription(), this));
        }
    }

    /* access modifiers changed from: 0000 */
    public void attemptRecovery() {
        try {
            close();
        } catch (java.io.IOException e) {
        }
        addStatusIfCountNotOverLimit(new ch.qos.logback.core.status.InfoStatus("Attempting to recover from IO failure on " + getDescription(), this));
        try {
            this.os = openNewOutputStream();
            this.presumedClean = true;
        } catch (java.io.IOException e2) {
            addStatusIfCountNotOverLimit(new ch.qos.logback.core.status.ErrorStatus("Failed to open " + getDescription(), this, e2));
        }
    }

    public void close() throws java.io.IOException {
        if (this.os != null) {
            this.os.close();
        }
    }

    public void flush() {
        if (this.os != null) {
            try {
                this.os.flush();
                postSuccessfulWrite();
            } catch (java.io.IOException e) {
                postIOFailure(e);
            }
        }
    }

    public ch.qos.logback.core.Context getContext() {
        return this.context;
    }

    /* access modifiers changed from: 0000 */
    public abstract java.lang.String getDescription();

    /* access modifiers changed from: 0000 */
    public abstract java.io.OutputStream openNewOutputStream() throws java.io.IOException;

    /* access modifiers changed from: 0000 */
    public void postIOFailure(java.io.IOException iOException) {
        addStatusIfCountNotOverLimit(new ch.qos.logback.core.status.ErrorStatus("IO failure while writing to " + getDescription(), this, iOException));
        this.presumedClean = false;
        if (this.recoveryCoordinator == null) {
            this.recoveryCoordinator = new ch.qos.logback.core.recovery.RecoveryCoordinator();
        }
    }

    public void setContext(ch.qos.logback.core.Context context2) {
        this.context = context2;
    }

    public void write(int i) {
        if (!isPresumedInError()) {
            try {
                this.os.write(i);
                postSuccessfulWrite();
            } catch (java.io.IOException e) {
                postIOFailure(e);
            }
        } else if (!this.recoveryCoordinator.isTooSoon()) {
            attemptRecovery();
        }
    }

    public void write(byte[] bArr, int i, int i2) {
        if (!isPresumedInError()) {
            try {
                this.os.write(bArr, i, i2);
                postSuccessfulWrite();
            } catch (java.io.IOException e) {
                postIOFailure(e);
            }
        } else if (!this.recoveryCoordinator.isTooSoon()) {
            attemptRecovery();
        }
    }
}
