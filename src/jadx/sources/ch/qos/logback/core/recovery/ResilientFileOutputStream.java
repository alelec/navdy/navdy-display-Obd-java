package ch.qos.logback.core.recovery;

public class ResilientFileOutputStream extends ch.qos.logback.core.recovery.ResilientOutputStreamBase {
    private java.io.File file;
    private java.io.FileOutputStream fos;

    public ResilientFileOutputStream(java.io.File file2, boolean z) throws java.io.FileNotFoundException {
        this.file = file2;
        this.fos = new java.io.FileOutputStream(file2, z);
        this.os = new java.io.BufferedOutputStream(this.fos);
        this.presumedClean = true;
    }

    public java.nio.channels.FileChannel getChannel() {
        if (this.os == null) {
            return null;
        }
        return this.fos.getChannel();
    }

    /* access modifiers changed from: 0000 */
    public java.lang.String getDescription() {
        return "file [" + this.file + "]";
    }

    public java.io.File getFile() {
        return this.file;
    }

    /* access modifiers changed from: 0000 */
    public java.io.OutputStream openNewOutputStream() throws java.io.IOException {
        this.fos = new java.io.FileOutputStream(this.file, true);
        return new java.io.BufferedOutputStream(this.fos);
    }

    public java.lang.String toString() {
        return "c.q.l.c.recovery.ResilientFileOutputStream@" + java.lang.System.identityHashCode(this);
    }
}
