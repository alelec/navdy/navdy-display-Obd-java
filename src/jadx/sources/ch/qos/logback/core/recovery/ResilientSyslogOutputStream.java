package ch.qos.logback.core.recovery;

public class ResilientSyslogOutputStream extends ch.qos.logback.core.recovery.ResilientOutputStreamBase {
    int port;
    java.lang.String syslogHost;

    public ResilientSyslogOutputStream(java.lang.String str, int i) throws java.net.UnknownHostException, java.net.SocketException {
        this.syslogHost = str;
        this.port = i;
        this.os = new ch.qos.logback.core.net.SyslogOutputStream(str, i);
        this.presumedClean = true;
    }

    /* access modifiers changed from: 0000 */
    public java.lang.String getDescription() {
        return "syslog [" + this.syslogHost + ":" + this.port + "]";
    }

    /* access modifiers changed from: 0000 */
    public java.io.OutputStream openNewOutputStream() throws java.io.IOException {
        return new ch.qos.logback.core.net.SyslogOutputStream(this.syslogHost, this.port);
    }

    public java.lang.String toString() {
        return "c.q.l.c.recovery.ResilientSyslogOutputStream@" + java.lang.System.identityHashCode(this);
    }
}
