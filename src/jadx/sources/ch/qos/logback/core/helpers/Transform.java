package ch.qos.logback.core.helpers;

public class Transform {
    private static final java.lang.String CDATA_EMBEDED_END = "]]>]]&gt;<![CDATA[";
    private static final java.lang.String CDATA_END = "]]>";
    private static final int CDATA_END_LEN = CDATA_END.length();
    private static final java.lang.String CDATA_PSEUDO_END = "]]&gt;";
    private static final java.lang.String CDATA_START = "<![CDATA[";

    public static void appendEscapingCDATA(java.lang.StringBuilder sb, java.lang.String str) {
        if (str != null) {
            int indexOf = str.indexOf(CDATA_END);
            if (indexOf < 0) {
                sb.append(str);
                return;
            }
            int i = 0;
            while (indexOf > -1) {
                sb.append(str.substring(i, indexOf));
                sb.append(CDATA_EMBEDED_END);
                i = CDATA_END_LEN + indexOf;
                if (i < str.length()) {
                    indexOf = str.indexOf(CDATA_END, i);
                } else {
                    return;
                }
            }
            sb.append(str.substring(i));
        }
    }

    public static java.lang.String escapeTags(java.lang.String str) {
        return (str == null || str.length() == 0) ? str : (str.indexOf("<") == -1 && str.indexOf(">") == -1) ? str : escapeTags(new java.lang.StringBuffer(str));
    }

    public static java.lang.String escapeTags(java.lang.StringBuffer stringBuffer) {
        for (int i = 0; i < stringBuffer.length(); i++) {
            char charAt = stringBuffer.charAt(i);
            if (charAt == '<') {
                stringBuffer.replace(i, i + 1, "&lt;");
            } else if (charAt == '>') {
                stringBuffer.replace(i, i + 1, "&gt;");
            }
        }
        return stringBuffer.toString();
    }
}
