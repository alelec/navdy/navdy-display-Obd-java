package ch.qos.logback.core.helpers;

public class ThrowableToStringArray {
    public static java.lang.String[] convert(java.lang.Throwable th) {
        java.util.LinkedList linkedList = new java.util.LinkedList();
        extract(linkedList, th, null);
        return (java.lang.String[]) linkedList.toArray(new java.lang.String[0]);
    }

    private static void extract(java.util.List<java.lang.String> list, java.lang.Throwable th, java.lang.StackTraceElement[] stackTraceElementArr) {
        java.lang.StackTraceElement[] stackTrace = th.getStackTrace();
        int findNumberOfCommonFrames = findNumberOfCommonFrames(stackTrace, stackTraceElementArr);
        list.add(formatFirstLine(th, stackTraceElementArr));
        for (int i = 0; i < stackTrace.length - findNumberOfCommonFrames; i++) {
            list.add("\tat " + stackTrace[i].toString());
        }
        if (findNumberOfCommonFrames != 0) {
            list.add("\t... " + findNumberOfCommonFrames + " common frames omitted");
        }
        java.lang.Throwable cause = th.getCause();
        if (cause != null) {
            extract(list, cause, stackTrace);
        }
    }

    private static int findNumberOfCommonFrames(java.lang.StackTraceElement[] stackTraceElementArr, java.lang.StackTraceElement[] stackTraceElementArr2) {
        int i = 0;
        if (stackTraceElementArr2 != null) {
            int length = stackTraceElementArr.length - 1;
            int length2 = stackTraceElementArr2.length - 1;
            while (length >= 0 && length2 >= 0 && stackTraceElementArr[length].equals(stackTraceElementArr2[length2])) {
                i++;
                length--;
                length2--;
            }
        }
        return i;
    }

    private static java.lang.String formatFirstLine(java.lang.Throwable th, java.lang.StackTraceElement[] stackTraceElementArr) {
        java.lang.String str = "";
        if (stackTraceElementArr != null) {
            str = ch.qos.logback.core.CoreConstants.CAUSED_BY;
        }
        java.lang.String str2 = str + th.getClass().getName();
        return th.getMessage() != null ? str2 + ": " + th.getMessage() : str2;
    }
}
