package ch.qos.logback.core.helpers;

public final class NOPAppender<E> extends ch.qos.logback.core.AppenderBase<E> {
    /* access modifiers changed from: protected */
    public void append(E e) {
    }
}
