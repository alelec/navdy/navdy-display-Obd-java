package ch.qos.logback.core.filter;

public abstract class AbstractMatcherFilter<E> extends ch.qos.logback.core.filter.Filter<E> {
    protected ch.qos.logback.core.spi.FilterReply onMatch = ch.qos.logback.core.spi.FilterReply.NEUTRAL;
    protected ch.qos.logback.core.spi.FilterReply onMismatch = ch.qos.logback.core.spi.FilterReply.NEUTRAL;

    public final ch.qos.logback.core.spi.FilterReply getOnMatch() {
        return this.onMatch;
    }

    public final ch.qos.logback.core.spi.FilterReply getOnMismatch() {
        return this.onMismatch;
    }

    public final void setOnMatch(ch.qos.logback.core.spi.FilterReply filterReply) {
        this.onMatch = filterReply;
    }

    public final void setOnMismatch(ch.qos.logback.core.spi.FilterReply filterReply) {
        this.onMismatch = filterReply;
    }
}
