package ch.qos.logback.core.filter;

public class EvaluatorFilter<E> extends ch.qos.logback.core.filter.AbstractMatcherFilter<E> {
    ch.qos.logback.core.boolex.EventEvaluator<E> evaluator;

    public ch.qos.logback.core.spi.FilterReply decide(E e) {
        if (!isStarted() || !this.evaluator.isStarted()) {
            return ch.qos.logback.core.spi.FilterReply.NEUTRAL;
        }
        try {
            return this.evaluator.evaluate(e) ? this.onMatch : this.onMismatch;
        } catch (ch.qos.logback.core.boolex.EvaluationException e2) {
            addError("Evaluator " + this.evaluator.getName() + " threw an exception", e2);
            return ch.qos.logback.core.spi.FilterReply.NEUTRAL;
        }
    }

    public ch.qos.logback.core.boolex.EventEvaluator<E> getEvaluator() {
        return this.evaluator;
    }

    public void setEvaluator(ch.qos.logback.core.boolex.EventEvaluator<E> eventEvaluator) {
        this.evaluator = eventEvaluator;
    }

    public void start() {
        if (this.evaluator != null) {
            super.start();
        } else {
            addError("No evaluator set for filter " + getName());
        }
    }
}
