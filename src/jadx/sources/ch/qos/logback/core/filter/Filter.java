package ch.qos.logback.core.filter;

public abstract class Filter<E> extends ch.qos.logback.core.spi.ContextAwareBase implements ch.qos.logback.core.spi.LifeCycle {
    private java.lang.String name;
    boolean start = false;

    public abstract ch.qos.logback.core.spi.FilterReply decide(E e);

    public java.lang.String getName() {
        return this.name;
    }

    public boolean isStarted() {
        return this.start;
    }

    public void setName(java.lang.String str) {
        this.name = str;
    }

    public void start() {
        this.start = true;
    }

    public void stop() {
        this.start = false;
    }
}
