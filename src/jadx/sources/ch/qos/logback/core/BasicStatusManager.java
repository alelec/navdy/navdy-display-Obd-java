package ch.qos.logback.core;

public class BasicStatusManager implements ch.qos.logback.core.status.StatusManager {
    public static final int MAX_HEADER_COUNT = 150;
    public static final int TAIL_SIZE = 150;
    int count = 0;
    int level = 0;
    protected final java.util.List<ch.qos.logback.core.status.Status> statusList = new java.util.ArrayList();
    protected final ch.qos.logback.core.spi.LogbackLock statusListLock = new ch.qos.logback.core.spi.LogbackLock();
    protected final java.util.List<ch.qos.logback.core.status.StatusListener> statusListenerList = new java.util.ArrayList();
    protected final ch.qos.logback.core.spi.LogbackLock statusListenerListLock = new ch.qos.logback.core.spi.LogbackLock();
    protected final ch.qos.logback.core.helpers.CyclicBuffer<ch.qos.logback.core.status.Status> tailBuffer = new ch.qos.logback.core.helpers.CyclicBuffer<>(150);

    private void fireStatusAddEvent(ch.qos.logback.core.status.Status status) {
        synchronized (this.statusListenerListLock) {
            for (ch.qos.logback.core.status.StatusListener addStatusEvent : this.statusListenerList) {
                addStatusEvent.addStatusEvent(status);
            }
        }
    }

    public void add(ch.qos.logback.core.status.Status status) {
        fireStatusAddEvent(status);
        this.count++;
        if (status.getLevel() > this.level) {
            this.level = status.getLevel();
        }
        synchronized (this.statusListLock) {
            if (this.statusList.size() < 150) {
                this.statusList.add(status);
            } else {
                this.tailBuffer.add(status);
            }
        }
    }

    public void add(ch.qos.logback.core.status.StatusListener statusListener) {
        synchronized (this.statusListenerListLock) {
            this.statusListenerList.add(statusListener);
        }
    }

    public boolean addUniquely(ch.qos.logback.core.status.StatusListener statusListener, java.lang.Object obj) {
        for (ch.qos.logback.core.status.StatusListener statusListener2 : getCopyOfStatusListenerList()) {
            if (statusListener2.getClass().isInstance(statusListener)) {
                add((ch.qos.logback.core.status.Status) new ch.qos.logback.core.status.WarnStatus("A previous listener of type [" + statusListener2.getClass() + "] has been already registered. Skipping double registration.", obj));
                return false;
            }
        }
        add(statusListener);
        return true;
    }

    public void clear() {
        synchronized (this.statusListLock) {
            this.count = 0;
            this.statusList.clear();
            this.tailBuffer.clear();
        }
    }

    public java.util.List<ch.qos.logback.core.status.Status> getCopyOfStatusList() {
        java.util.ArrayList arrayList;
        synchronized (this.statusListLock) {
            arrayList = new java.util.ArrayList(this.statusList);
            arrayList.addAll(this.tailBuffer.asList());
        }
        return arrayList;
    }

    public java.util.List<ch.qos.logback.core.status.StatusListener> getCopyOfStatusListenerList() {
        java.util.ArrayList arrayList;
        synchronized (this.statusListenerListLock) {
            arrayList = new java.util.ArrayList(this.statusListenerList);
        }
        return arrayList;
    }

    public int getCount() {
        return this.count;
    }

    public int getLevel() {
        return this.level;
    }

    public void remove(ch.qos.logback.core.status.StatusListener statusListener) {
        synchronized (this.statusListenerListLock) {
            this.statusListenerList.remove(statusListener);
        }
    }
}
