package ch.qos.logback.core.status;

public abstract class StatusBase implements ch.qos.logback.core.status.Status {
    private static final java.util.List<ch.qos.logback.core.status.Status> EMPTY_LIST = new java.util.ArrayList(0);
    java.util.List<ch.qos.logback.core.status.Status> childrenList;
    long date;
    int level;
    final java.lang.String message;
    final java.lang.Object origin;
    java.lang.Throwable throwable;

    StatusBase(int i, java.lang.String str, java.lang.Object obj) {
        this(i, str, obj, null);
    }

    StatusBase(int i, java.lang.String str, java.lang.Object obj, java.lang.Throwable th) {
        this.level = i;
        this.message = str;
        this.origin = obj;
        this.throwable = th;
        this.date = java.lang.System.currentTimeMillis();
    }

    public synchronized void add(ch.qos.logback.core.status.Status status) {
        if (status == null) {
            throw new java.lang.NullPointerException("Null values are not valid Status.");
        }
        if (this.childrenList == null) {
            this.childrenList = new java.util.ArrayList();
        }
        this.childrenList.add(status);
    }

    public boolean equals(java.lang.Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ch.qos.logback.core.status.StatusBase statusBase = (ch.qos.logback.core.status.StatusBase) obj;
        if (this.level != statusBase.level) {
            return false;
        }
        return this.message == null ? statusBase.message == null : this.message.equals(statusBase.message);
    }

    public java.lang.Long getDate() {
        return java.lang.Long.valueOf(this.date);
    }

    public synchronized int getEffectiveLevel() {
        int i;
        int i2 = this.level;
        java.util.Iterator it = iterator();
        i = i2;
        while (it.hasNext()) {
            int effectiveLevel = ((ch.qos.logback.core.status.Status) it.next()).getEffectiveLevel();
            if (effectiveLevel > i) {
                i = effectiveLevel;
            }
        }
        return i;
    }

    public int getLevel() {
        return this.level;
    }

    public java.lang.String getMessage() {
        return this.message;
    }

    public java.lang.Object getOrigin() {
        return this.origin;
    }

    public java.lang.Throwable getThrowable() {
        return this.throwable;
    }

    public synchronized boolean hasChildren() {
        return this.childrenList != null && this.childrenList.size() > 0;
    }

    public int hashCode() {
        return (this.message == null ? 0 : this.message.hashCode()) + ((this.level + 31) * 31);
    }

    public synchronized java.util.Iterator<ch.qos.logback.core.status.Status> iterator() {
        return this.childrenList != null ? this.childrenList.iterator() : EMPTY_LIST.iterator();
    }

    public synchronized boolean remove(ch.qos.logback.core.status.Status status) {
        return this.childrenList == null ? false : this.childrenList.remove(status);
    }

    public java.lang.String toString() {
        java.lang.StringBuffer stringBuffer = new java.lang.StringBuffer();
        switch (getEffectiveLevel()) {
            case 0:
                stringBuffer.append("INFO");
                break;
            case 1:
                stringBuffer.append("WARN");
                break;
            case 2:
                stringBuffer.append("ERROR");
                break;
        }
        if (this.origin != null) {
            stringBuffer.append(" in ");
            stringBuffer.append(this.origin);
            stringBuffer.append(" -");
        }
        stringBuffer.append(" ");
        stringBuffer.append(this.message);
        if (this.throwable != null) {
            stringBuffer.append(" ");
            stringBuffer.append(this.throwable);
        }
        return stringBuffer.toString();
    }
}
