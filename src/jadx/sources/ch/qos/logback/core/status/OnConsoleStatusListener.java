package ch.qos.logback.core.status;

public class OnConsoleStatusListener extends ch.qos.logback.core.status.OnPrintStreamStatusListenerBase {
    public static void addNewInstanceToContext(ch.qos.logback.core.Context context) {
        ch.qos.logback.core.status.OnConsoleStatusListener onConsoleStatusListener = new ch.qos.logback.core.status.OnConsoleStatusListener();
        onConsoleStatusListener.setContext(context);
        if (context.getStatusManager().addUniquely(onConsoleStatusListener, context)) {
            onConsoleStatusListener.start();
        }
    }

    public /* bridge */ /* synthetic */ void addStatusEvent(ch.qos.logback.core.status.Status status) {
        super.addStatusEvent(status);
    }

    /* access modifiers changed from: protected */
    public java.io.PrintStream getPrintStream() {
        return java.lang.System.out;
    }

    public /* bridge */ /* synthetic */ long getRetrospective() {
        return super.getRetrospective();
    }

    public /* bridge */ /* synthetic */ boolean isStarted() {
        return super.isStarted();
    }

    public /* bridge */ /* synthetic */ void setRetrospective(long j) {
        super.setRetrospective(j);
    }

    public /* bridge */ /* synthetic */ void start() {
        super.start();
    }

    public /* bridge */ /* synthetic */ void stop() {
        super.stop();
    }
}
