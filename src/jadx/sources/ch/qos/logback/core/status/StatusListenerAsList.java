package ch.qos.logback.core.status;

public class StatusListenerAsList implements ch.qos.logback.core.status.StatusListener {
    java.util.List<ch.qos.logback.core.status.Status> statusList = new java.util.ArrayList();

    public void addStatusEvent(ch.qos.logback.core.status.Status status) {
        this.statusList.add(status);
    }

    public java.util.List<ch.qos.logback.core.status.Status> getStatusList() {
        return this.statusList;
    }
}
