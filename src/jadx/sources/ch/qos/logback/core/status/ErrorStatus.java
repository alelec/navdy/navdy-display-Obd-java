package ch.qos.logback.core.status;

public class ErrorStatus extends ch.qos.logback.core.status.StatusBase {
    public ErrorStatus(java.lang.String str, java.lang.Object obj) {
        super(2, str, obj);
    }

    public ErrorStatus(java.lang.String str, java.lang.Object obj, java.lang.Throwable th) {
        super(2, str, obj, th);
    }
}
