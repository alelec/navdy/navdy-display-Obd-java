package ch.qos.logback.core.status;

public interface StatusListener {
    void addStatusEvent(ch.qos.logback.core.status.Status status);
}
