package ch.qos.logback.core.status;

public interface StatusManager {
    void add(ch.qos.logback.core.status.Status status);

    void add(ch.qos.logback.core.status.StatusListener statusListener);

    boolean addUniquely(ch.qos.logback.core.status.StatusListener statusListener, java.lang.Object obj);

    void clear();

    java.util.List<ch.qos.logback.core.status.Status> getCopyOfStatusList();

    java.util.List<ch.qos.logback.core.status.StatusListener> getCopyOfStatusListenerList();

    int getCount();

    void remove(ch.qos.logback.core.status.StatusListener statusListener);
}
