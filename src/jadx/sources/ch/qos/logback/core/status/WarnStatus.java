package ch.qos.logback.core.status;

public class WarnStatus extends ch.qos.logback.core.status.StatusBase {
    public WarnStatus(java.lang.String str, java.lang.Object obj) {
        super(1, str, obj);
    }

    public WarnStatus(java.lang.String str, java.lang.Object obj, java.lang.Throwable th) {
        super(1, str, obj, th);
    }
}
