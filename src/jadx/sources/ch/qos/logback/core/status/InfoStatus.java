package ch.qos.logback.core.status;

public class InfoStatus extends ch.qos.logback.core.status.StatusBase {
    public InfoStatus(java.lang.String str, java.lang.Object obj) {
        super(0, str, obj);
    }

    public InfoStatus(java.lang.String str, java.lang.Object obj, java.lang.Throwable th) {
        super(0, str, obj, th);
    }
}
