package ch.qos.logback.core.status;

public interface Status {
    public static final int ERROR = 2;
    public static final int INFO = 0;
    public static final int WARN = 1;

    void add(ch.qos.logback.core.status.Status status);

    java.lang.Long getDate();

    int getEffectiveLevel();

    int getLevel();

    java.lang.String getMessage();

    java.lang.Object getOrigin();

    java.lang.Throwable getThrowable();

    boolean hasChildren();

    java.util.Iterator<ch.qos.logback.core.status.Status> iterator();

    boolean remove(ch.qos.logback.core.status.Status status);
}
