package ch.qos.logback.core.status;

public class StatusUtil {
    ch.qos.logback.core.status.StatusManager sm;

    public StatusUtil(ch.qos.logback.core.Context context) {
        this.sm = context.getStatusManager();
    }

    public StatusUtil(ch.qos.logback.core.status.StatusManager statusManager) {
        this.sm = statusManager;
    }

    public static boolean contextHasStatusListener(ch.qos.logback.core.Context context) {
        ch.qos.logback.core.status.StatusManager statusManager = context.getStatusManager();
        if (statusManager == null) {
            return false;
        }
        java.util.List copyOfStatusListenerList = statusManager.getCopyOfStatusListenerList();
        return (copyOfStatusListenerList == null || copyOfStatusListenerList.size() == 0) ? false : true;
    }

    public static java.util.List<ch.qos.logback.core.status.Status> filterStatusListByTimeThreshold(java.util.List<ch.qos.logback.core.status.Status> list, long j) {
        java.util.ArrayList arrayList = new java.util.ArrayList();
        for (ch.qos.logback.core.status.Status status : list) {
            if (status.getDate().longValue() >= j) {
                arrayList.add(status);
            }
        }
        return arrayList;
    }

    public void addError(java.lang.Object obj, java.lang.String str, java.lang.Throwable th) {
        addStatus(new ch.qos.logback.core.status.ErrorStatus(str, obj, th));
    }

    public void addInfo(java.lang.Object obj, java.lang.String str) {
        addStatus(new ch.qos.logback.core.status.InfoStatus(str, obj));
    }

    public void addStatus(ch.qos.logback.core.status.Status status) {
        if (this.sm != null) {
            this.sm.add(status);
        }
    }

    public void addWarn(java.lang.Object obj, java.lang.String str) {
        addStatus(new ch.qos.logback.core.status.WarnStatus(str, obj));
    }

    public boolean containsException(java.lang.Class<?> cls) {
        for (ch.qos.logback.core.status.Status throwable : this.sm.getCopyOfStatusList()) {
            java.lang.Throwable throwable2 = throwable.getThrowable();
            if (throwable2 != null && throwable2.getClass().getName().equals(cls.getName())) {
                return true;
            }
        }
        return false;
    }

    public boolean containsMatch(int i, java.lang.String str) {
        return containsMatch(0, i, str);
    }

    public boolean containsMatch(long j, int i, java.lang.String str) {
        java.util.List<ch.qos.logback.core.status.Status> filterStatusListByTimeThreshold = filterStatusListByTimeThreshold(this.sm.getCopyOfStatusList(), j);
        java.util.regex.Pattern compile = java.util.regex.Pattern.compile(str);
        for (ch.qos.logback.core.status.Status status : filterStatusListByTimeThreshold) {
            if (i == status.getLevel() && compile.matcher(status.getMessage()).lookingAt()) {
                return true;
            }
        }
        return false;
    }

    public boolean containsMatch(java.lang.String str) {
        java.util.regex.Pattern compile = java.util.regex.Pattern.compile(str);
        for (ch.qos.logback.core.status.Status message : this.sm.getCopyOfStatusList()) {
            if (compile.matcher(message.getMessage()).lookingAt()) {
                return true;
            }
        }
        return false;
    }

    public int getHighestLevel(long j) {
        int i = 0;
        java.util.Iterator it = filterStatusListByTimeThreshold(this.sm.getCopyOfStatusList(), j).iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return i2;
            }
            ch.qos.logback.core.status.Status status = (ch.qos.logback.core.status.Status) it.next();
            i = status.getLevel() > i2 ? status.getLevel() : i2;
        }
    }

    public boolean hasXMLParsingErrors(long j) {
        return containsMatch(j, 2, ch.qos.logback.core.CoreConstants.XML_PARSING);
    }

    public boolean isErrorFree(long j) {
        return 2 > getHighestLevel(j);
    }

    public int matchCount(java.lang.String str) {
        int i = 0;
        java.util.regex.Pattern compile = java.util.regex.Pattern.compile(str);
        java.util.Iterator it = this.sm.getCopyOfStatusList().iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return i2;
            }
            i = compile.matcher(((ch.qos.logback.core.status.Status) it.next()).getMessage()).lookingAt() ? i2 + 1 : i2;
        }
    }

    public boolean noXMLParsingErrorsOccurred(long j) {
        return !hasXMLParsingErrors(j);
    }

    public long timeOfLastReset() {
        java.util.List copyOfStatusList = this.sm.getCopyOfStatusList();
        if (copyOfStatusList == null) {
            return -1;
        }
        for (int size = copyOfStatusList.size() - 1; size >= 0; size--) {
            ch.qos.logback.core.status.Status status = (ch.qos.logback.core.status.Status) copyOfStatusList.get(size);
            if (ch.qos.logback.core.CoreConstants.RESET_MSG_PREFIX.equals(status.getMessage())) {
                return status.getDate().longValue();
            }
        }
        return -1;
    }
}
