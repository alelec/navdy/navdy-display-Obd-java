package ch.qos.logback.core.status;

abstract class OnPrintStreamStatusListenerBase extends ch.qos.logback.core.spi.ContextAwareBase implements ch.qos.logback.core.status.StatusListener, ch.qos.logback.core.spi.LifeCycle {
    static final long DEFAULT_RETROSPECTIVE = 300;
    boolean isStarted = false;
    long retrospective = DEFAULT_RETROSPECTIVE;

    OnPrintStreamStatusListenerBase() {
    }

    private void print(ch.qos.logback.core.status.Status status) {
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        ch.qos.logback.core.util.StatusPrinter.buildStr(sb, "", status);
        getPrintStream().print(sb);
    }

    private void retrospectivePrint() {
        if (this.context != null) {
            long currentTimeMillis = java.lang.System.currentTimeMillis();
            for (ch.qos.logback.core.status.Status status : this.context.getStatusManager().getCopyOfStatusList()) {
                if (currentTimeMillis - status.getDate().longValue() < this.retrospective) {
                    print(status);
                }
            }
        }
    }

    public void addStatusEvent(ch.qos.logback.core.status.Status status) {
        if (this.isStarted) {
            print(status);
        }
    }

    /* access modifiers changed from: protected */
    public abstract java.io.PrintStream getPrintStream();

    public long getRetrospective() {
        return this.retrospective;
    }

    public boolean isStarted() {
        return this.isStarted;
    }

    public void setRetrospective(long j) {
        this.retrospective = j;
    }

    public void start() {
        this.isStarted = true;
        if (this.retrospective > 0) {
            retrospectivePrint();
        }
    }

    public void stop() {
        this.isStarted = false;
    }
}
