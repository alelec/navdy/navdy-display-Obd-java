package ch.qos.logback.core;

public abstract class PropertyDefinerBase extends ch.qos.logback.core.spi.ContextAwareBase implements ch.qos.logback.core.spi.PropertyDefiner {
    protected static java.lang.String booleanAsStr(boolean z) {
        return z ? java.lang.Boolean.TRUE.toString() : java.lang.Boolean.FALSE.toString();
    }
}
