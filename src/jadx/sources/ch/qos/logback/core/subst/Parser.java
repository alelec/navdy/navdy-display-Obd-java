package ch.qos.logback.core.subst;

public class Parser {
    int pointer = 0;
    final java.util.List<ch.qos.logback.core.subst.Token> tokenList;

    public Parser(java.util.List<ch.qos.logback.core.subst.Token> list) {
        this.tokenList = list;
    }

    private ch.qos.logback.core.subst.Node C() throws ch.qos.logback.core.spi.ScanException {
        ch.qos.logback.core.subst.Node E = E();
        if (isDefaultToken(peekAtCurentToken())) {
            advanceTokenPointer();
            E.append(makeNewLiteralNode(ch.qos.logback.core.CoreConstants.DEFAULT_VALUE_SEPARATOR));
            E.append(E());
        }
        return E;
    }

    private ch.qos.logback.core.subst.Node E() throws ch.qos.logback.core.spi.ScanException {
        ch.qos.logback.core.subst.Node T = T();
        if (T == null) {
            return null;
        }
        ch.qos.logback.core.subst.Node Eopt = Eopt();
        if (Eopt == null) {
            return T;
        }
        T.append(Eopt);
        return T;
    }

    private ch.qos.logback.core.subst.Node Eopt() throws ch.qos.logback.core.spi.ScanException {
        if (peekAtCurentToken() == null) {
            return null;
        }
        return E();
    }

    private ch.qos.logback.core.subst.Node T() throws ch.qos.logback.core.spi.ScanException {
        ch.qos.logback.core.subst.Token peekAtCurentToken = peekAtCurentToken();
        if (peekAtCurentToken == null) {
            return null;
        }
        switch (peekAtCurentToken.type) {
            case LITERAL:
                advanceTokenPointer();
                return makeNewLiteralNode(peekAtCurentToken.payload);
            case CURLY_LEFT:
                advanceTokenPointer();
                ch.qos.logback.core.subst.Node C = C();
                expectCurlyRight(peekAtCurentToken());
                advanceTokenPointer();
                ch.qos.logback.core.subst.Node makeNewLiteralNode = makeNewLiteralNode(ch.qos.logback.core.CoreConstants.LEFT_ACCOLADE);
                makeNewLiteralNode.append(C);
                makeNewLiteralNode.append(makeNewLiteralNode(ch.qos.logback.core.CoreConstants.RIGHT_ACCOLADE));
                return makeNewLiteralNode;
            case START:
                advanceTokenPointer();
                ch.qos.logback.core.subst.Node V = V();
                expectCurlyRight(peekAtCurentToken());
                advanceTokenPointer();
                return V;
            default:
                return null;
        }
    }

    private ch.qos.logback.core.subst.Node V() throws ch.qos.logback.core.spi.ScanException {
        ch.qos.logback.core.subst.Node node = new ch.qos.logback.core.subst.Node(ch.qos.logback.core.subst.Node.Type.VARIABLE, E());
        if (isDefaultToken(peekAtCurentToken())) {
            advanceTokenPointer();
            node.defaultPart = E();
        }
        return node;
    }

    private boolean isDefaultToken(ch.qos.logback.core.subst.Token token) {
        return token != null && token.type == ch.qos.logback.core.subst.Token.Type.DEFAULT;
    }

    private ch.qos.logback.core.subst.Node makeNewLiteralNode(java.lang.String str) {
        return new ch.qos.logback.core.subst.Node(ch.qos.logback.core.subst.Node.Type.LITERAL, str);
    }

    /* access modifiers changed from: 0000 */
    public void advanceTokenPointer() {
        this.pointer++;
    }

    /* access modifiers changed from: 0000 */
    public void expectCurlyRight(ch.qos.logback.core.subst.Token token) throws ch.qos.logback.core.spi.ScanException {
        expectNotNull(token, "}");
        if (token.type != ch.qos.logback.core.subst.Token.Type.CURLY_RIGHT) {
            throw new ch.qos.logback.core.spi.ScanException("Expecting }");
        }
    }

    /* access modifiers changed from: 0000 */
    public void expectNotNull(ch.qos.logback.core.subst.Token token, java.lang.String str) {
        if (token == null) {
            throw new java.lang.IllegalArgumentException("All tokens consumed but was expecting \"" + str + "\"");
        }
    }

    public ch.qos.logback.core.subst.Node parse() throws ch.qos.logback.core.spi.ScanException {
        return E();
    }

    /* access modifiers changed from: 0000 */
    public ch.qos.logback.core.subst.Token peekAtCurentToken() {
        if (this.pointer < this.tokenList.size()) {
            return (ch.qos.logback.core.subst.Token) this.tokenList.get(this.pointer);
        }
        return null;
    }
}
