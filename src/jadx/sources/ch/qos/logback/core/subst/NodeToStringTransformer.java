package ch.qos.logback.core.subst;

public class NodeToStringTransformer {
    final ch.qos.logback.core.subst.Node node;
    final ch.qos.logback.core.spi.PropertyContainer propertyContainer0;
    final ch.qos.logback.core.spi.PropertyContainer propertyContainer1;

    public NodeToStringTransformer(ch.qos.logback.core.subst.Node node2, ch.qos.logback.core.spi.PropertyContainer propertyContainer) {
        this(node2, propertyContainer, null);
    }

    public NodeToStringTransformer(ch.qos.logback.core.subst.Node node2, ch.qos.logback.core.spi.PropertyContainer propertyContainer, ch.qos.logback.core.spi.PropertyContainer propertyContainer2) {
        this.node = node2;
        this.propertyContainer0 = propertyContainer;
        this.propertyContainer1 = propertyContainer2;
    }

    private void compileNode(ch.qos.logback.core.subst.Node node2, java.lang.StringBuilder sb, java.util.Stack<ch.qos.logback.core.subst.Node> stack) throws ch.qos.logback.core.spi.ScanException {
        while (node2 != null) {
            switch (node2.type) {
                case LITERAL:
                    handleLiteral(node2, sb);
                    break;
                case VARIABLE:
                    handleVariable(node2, sb, stack);
                    break;
            }
            node2 = node2.next;
        }
    }

    private java.lang.String constructRecursionErrorMessage(java.util.Stack<ch.qos.logback.core.subst.Node> stack) {
        java.lang.StringBuilder sb = new java.lang.StringBuilder("Circular variable reference detected while parsing input [");
        java.util.Iterator it = stack.iterator();
        while (it.hasNext()) {
            ch.qos.logback.core.subst.Node node2 = (ch.qos.logback.core.subst.Node) it.next();
            sb.append("${").append(variableNodeValue(node2)).append("}");
            if (stack.lastElement() != node2) {
                sb.append(" --> ");
            }
        }
        sb.append("]");
        return sb.toString();
    }

    private boolean equalNodes(ch.qos.logback.core.subst.Node node2, ch.qos.logback.core.subst.Node node3) {
        if (node2.type != null && !node2.type.equals(node3.type)) {
            return false;
        }
        if (node2.payload == null || node2.payload.equals(node3.payload)) {
            return node2.defaultPart == null || node2.defaultPart.equals(node3.defaultPart);
        }
        return false;
    }

    private void handleLiteral(ch.qos.logback.core.subst.Node node2, java.lang.StringBuilder sb) {
        sb.append((java.lang.String) node2.payload);
    }

    private void handleVariable(ch.qos.logback.core.subst.Node node2, java.lang.StringBuilder sb, java.util.Stack<ch.qos.logback.core.subst.Node> stack) throws ch.qos.logback.core.spi.ScanException {
        if (haveVisitedNodeAlready(node2, stack)) {
            stack.push(node2);
            throw new java.lang.IllegalArgumentException(constructRecursionErrorMessage(stack));
        }
        stack.push(node2);
        java.lang.StringBuilder sb2 = new java.lang.StringBuilder();
        compileNode((ch.qos.logback.core.subst.Node) node2.payload, sb2, stack);
        java.lang.String sb3 = sb2.toString();
        java.lang.String lookupKey = lookupKey(sb3);
        if (lookupKey != null) {
            compileNode(tokenizeAndParseString(lookupKey), sb, stack);
            stack.pop();
        } else if (node2.defaultPart == null) {
            sb.append(sb3 + ch.qos.logback.core.CoreConstants.UNDEFINED_PROPERTY_SUFFIX);
            stack.pop();
        } else {
            ch.qos.logback.core.subst.Node node3 = (ch.qos.logback.core.subst.Node) node2.defaultPart;
            java.lang.StringBuilder sb4 = new java.lang.StringBuilder();
            compileNode(node3, sb4, stack);
            stack.pop();
            sb.append(sb4.toString());
        }
    }

    private boolean haveVisitedNodeAlready(ch.qos.logback.core.subst.Node node2, java.util.Stack<ch.qos.logback.core.subst.Node> stack) {
        java.util.Iterator it = stack.iterator();
        while (it.hasNext()) {
            if (equalNodes(node2, (ch.qos.logback.core.subst.Node) it.next())) {
                return true;
            }
        }
        return false;
    }

    private java.lang.String lookupKey(java.lang.String str) {
        java.lang.String property = this.propertyContainer0.getProperty(str);
        if (property != null) {
            return property;
        }
        if (this.propertyContainer1 != null) {
            java.lang.String property2 = this.propertyContainer1.getProperty(str);
            if (property2 != null) {
                return property2;
            }
        }
        java.lang.String systemProperty = ch.qos.logback.core.util.OptionHelper.getSystemProperty(str, null);
        if (systemProperty != null) {
            return systemProperty;
        }
        java.lang.String env = ch.qos.logback.core.util.OptionHelper.getEnv(str);
        if (env == null) {
            return null;
        }
        return env;
    }

    public static java.lang.String substituteVariable(java.lang.String str, ch.qos.logback.core.spi.PropertyContainer propertyContainer, ch.qos.logback.core.spi.PropertyContainer propertyContainer2) throws ch.qos.logback.core.spi.ScanException {
        return new ch.qos.logback.core.subst.NodeToStringTransformer(tokenizeAndParseString(str), propertyContainer, propertyContainer2).transform();
    }

    private static ch.qos.logback.core.subst.Node tokenizeAndParseString(java.lang.String str) throws ch.qos.logback.core.spi.ScanException {
        return new ch.qos.logback.core.subst.Parser(new ch.qos.logback.core.subst.Tokenizer(str).tokenize()).parse();
    }

    private java.lang.String variableNodeValue(ch.qos.logback.core.subst.Node node2) {
        return (java.lang.String) ((ch.qos.logback.core.subst.Node) node2.payload).payload;
    }

    public java.lang.String transform() throws ch.qos.logback.core.spi.ScanException {
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        compileNode(this.node, sb, new java.util.Stack());
        return sb.toString();
    }
}
