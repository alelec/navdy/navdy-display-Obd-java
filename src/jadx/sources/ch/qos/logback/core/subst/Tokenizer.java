package ch.qos.logback.core.subst;

public class Tokenizer {
    final java.lang.String pattern;
    final int patternLength;
    int pointer = 0;
    ch.qos.logback.core.subst.Tokenizer.TokenizerState state = ch.qos.logback.core.subst.Tokenizer.TokenizerState.LITERAL_STATE;

    enum TokenizerState {
        LITERAL_STATE,
        START_STATE,
        DEFAULT_VAL_STATE
    }

    public Tokenizer(java.lang.String str) {
        this.pattern = str;
        this.patternLength = str.length();
    }

    private void addLiteralToken(java.util.List<ch.qos.logback.core.subst.Token> list, java.lang.StringBuilder sb) {
        if (sb.length() != 0) {
            list.add(new ch.qos.logback.core.subst.Token(ch.qos.logback.core.subst.Token.Type.LITERAL, sb.toString()));
        }
    }

    private void handleDefaultValueState(char c, java.util.List<ch.qos.logback.core.subst.Token> list, java.lang.StringBuilder sb) {
        switch (c) {
            case android.support.v4.view.MotionEventCompat.AXIS_GENERIC_5 /*36*/:
                sb.append(ch.qos.logback.core.CoreConstants.COLON_CHAR);
                addLiteralToken(list, sb);
                sb.setLength(0);
                this.state = ch.qos.logback.core.subst.Tokenizer.TokenizerState.START_STATE;
                return;
            case android.support.v4.view.MotionEventCompat.AXIS_GENERIC_14 /*45*/:
                list.add(ch.qos.logback.core.subst.Token.DEFAULT_SEP_TOKEN);
                this.state = ch.qos.logback.core.subst.Tokenizer.TokenizerState.LITERAL_STATE;
                return;
            default:
                sb.append(ch.qos.logback.core.CoreConstants.COLON_CHAR).append(c);
                this.state = ch.qos.logback.core.subst.Tokenizer.TokenizerState.LITERAL_STATE;
                return;
        }
    }

    private void handleLiteralState(char c, java.util.List<ch.qos.logback.core.subst.Token> list, java.lang.StringBuilder sb) {
        if (c == '$') {
            addLiteralToken(list, sb);
            sb.setLength(0);
            this.state = ch.qos.logback.core.subst.Tokenizer.TokenizerState.START_STATE;
        } else if (c == ':') {
            addLiteralToken(list, sb);
            sb.setLength(0);
            this.state = ch.qos.logback.core.subst.Tokenizer.TokenizerState.DEFAULT_VAL_STATE;
        } else if (c == '{') {
            addLiteralToken(list, sb);
            list.add(ch.qos.logback.core.subst.Token.CURLY_LEFT_TOKEN);
            sb.setLength(0);
        } else if (c == '}') {
            addLiteralToken(list, sb);
            list.add(ch.qos.logback.core.subst.Token.CURLY_RIGHT_TOKEN);
            sb.setLength(0);
        } else {
            sb.append(c);
        }
    }

    private void handleStartState(char c, java.util.List<ch.qos.logback.core.subst.Token> list, java.lang.StringBuilder sb) {
        if (c == '{') {
            list.add(ch.qos.logback.core.subst.Token.START_TOKEN);
        } else {
            sb.append('$').append(c);
        }
        this.state = ch.qos.logback.core.subst.Tokenizer.TokenizerState.LITERAL_STATE;
    }

    /* access modifiers changed from: 0000 */
    public java.util.List<ch.qos.logback.core.subst.Token> tokenize() throws ch.qos.logback.core.spi.ScanException {
        java.util.ArrayList arrayList = new java.util.ArrayList();
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        while (this.pointer < this.patternLength) {
            char charAt = this.pattern.charAt(this.pointer);
            this.pointer++;
            switch (this.state) {
                case LITERAL_STATE:
                    handleLiteralState(charAt, arrayList, sb);
                    break;
                case START_STATE:
                    handleStartState(charAt, arrayList, sb);
                    break;
                case DEFAULT_VAL_STATE:
                    handleDefaultValueState(charAt, arrayList, sb);
                    break;
            }
        }
        switch (this.state) {
            case LITERAL_STATE:
                addLiteralToken(arrayList, sb);
                break;
            case START_STATE:
                throw new ch.qos.logback.core.spi.ScanException("Unexpected end of pattern string");
        }
        return arrayList;
    }
}
