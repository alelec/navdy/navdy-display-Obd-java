package ch.qos.logback.core.subst;

public class Token {
    public static final ch.qos.logback.core.subst.Token CURLY_LEFT_TOKEN = new ch.qos.logback.core.subst.Token(ch.qos.logback.core.subst.Token.Type.CURLY_LEFT, null);
    public static final ch.qos.logback.core.subst.Token CURLY_RIGHT_TOKEN = new ch.qos.logback.core.subst.Token(ch.qos.logback.core.subst.Token.Type.CURLY_RIGHT, null);
    public static final ch.qos.logback.core.subst.Token DEFAULT_SEP_TOKEN = new ch.qos.logback.core.subst.Token(ch.qos.logback.core.subst.Token.Type.DEFAULT, null);
    public static final ch.qos.logback.core.subst.Token START_TOKEN = new ch.qos.logback.core.subst.Token(ch.qos.logback.core.subst.Token.Type.START, null);
    java.lang.String payload;
    ch.qos.logback.core.subst.Token.Type type;

    public enum Type {
        LITERAL,
        START,
        CURLY_LEFT,
        CURLY_RIGHT,
        DEFAULT
    }

    public Token(ch.qos.logback.core.subst.Token.Type type2, java.lang.String str) {
        this.type = type2;
        this.payload = str;
    }

    public boolean equals(java.lang.Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        ch.qos.logback.core.subst.Token token = (ch.qos.logback.core.subst.Token) obj;
        if (this.type != token.type) {
            return false;
        }
        if (this.payload != null) {
            if (this.payload.equals(token.payload)) {
                return true;
            }
        } else if (token.payload == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = (this.type != null ? this.type.hashCode() : 0) * 31;
        if (this.payload != null) {
            i = this.payload.hashCode();
        }
        return hashCode + i;
    }

    public java.lang.String toString() {
        java.lang.String str = "Token{type=" + this.type;
        if (this.payload != null) {
            str = str + ", payload='" + this.payload + ch.qos.logback.core.CoreConstants.SINGLE_QUOTE_CHAR;
        }
        return str + ch.qos.logback.core.CoreConstants.CURLY_RIGHT;
    }
}
