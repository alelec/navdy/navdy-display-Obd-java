package ch.qos.logback.core.subst;

public class Node {
    java.lang.Object defaultPart;
    ch.qos.logback.core.subst.Node next;
    java.lang.Object payload;
    ch.qos.logback.core.subst.Node.Type type;

    enum Type {
        LITERAL,
        VARIABLE
    }

    public Node(ch.qos.logback.core.subst.Node.Type type2, java.lang.Object obj) {
        this.type = type2;
        this.payload = obj;
    }

    public Node(ch.qos.logback.core.subst.Node.Type type2, java.lang.Object obj, java.lang.Object obj2) {
        this.type = type2;
        this.payload = obj;
        this.defaultPart = obj2;
    }

    /* access modifiers changed from: 0000 */
    public void append(ch.qos.logback.core.subst.Node node) {
        if (node != null) {
            while (this.next != null) {
                this = this.next;
            }
            this.next = node;
        }
    }

    public void dump() {
        java.lang.System.out.print(toString());
        java.lang.System.out.print(" -> ");
        if (this.next != null) {
            this.next.dump();
        } else {
            java.lang.System.out.print(" null");
        }
    }

    public boolean equals(java.lang.Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        ch.qos.logback.core.subst.Node node = (ch.qos.logback.core.subst.Node) obj;
        if (this.type != node.type) {
            return false;
        }
        if (this.payload == null ? node.payload != null : !this.payload.equals(node.payload)) {
            return false;
        }
        if (this.defaultPart == null ? node.defaultPart != null : !this.defaultPart.equals(node.defaultPart)) {
            return false;
        }
        if (this.next != null) {
            if (this.next.equals(node.next)) {
                return true;
            }
        } else if (node.next == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = ((this.defaultPart != null ? this.defaultPart.hashCode() : 0) + (((this.payload != null ? this.payload.hashCode() : 0) + ((this.type != null ? this.type.hashCode() : 0) * 31)) * 31)) * 31;
        if (this.next != null) {
            i = this.next.hashCode();
        }
        return hashCode + i;
    }

    /* access modifiers changed from: 0000 */
    public void recursive(ch.qos.logback.core.subst.Node node, java.lang.StringBuilder sb) {
        while (node != null) {
            sb.append(node.toString()).append(" --> ");
            node = node.next;
        }
        sb.append("null ");
    }

    public void setNext(ch.qos.logback.core.subst.Node node) {
        this.next = node;
    }

    public java.lang.String toString() {
        switch (this.type) {
            case LITERAL:
                return "Node{type=" + this.type + ", payload='" + this.payload + "'}";
            case VARIABLE:
                java.lang.StringBuilder sb = new java.lang.StringBuilder();
                java.lang.StringBuilder sb2 = new java.lang.StringBuilder();
                if (this.defaultPart != null) {
                    recursive((ch.qos.logback.core.subst.Node) this.defaultPart, sb2);
                }
                recursive((ch.qos.logback.core.subst.Node) this.payload, sb);
                java.lang.String str = "Node{type=" + this.type + ", payload='" + sb.toString() + "'";
                if (this.defaultPart != null) {
                    str = str + ", defaultPart=" + sb2.toString();
                }
                return str + ch.qos.logback.core.CoreConstants.CURLY_RIGHT;
            default:
                return null;
        }
    }
}
