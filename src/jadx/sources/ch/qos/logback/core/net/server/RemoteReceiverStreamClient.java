package ch.qos.logback.core.net.server;

class RemoteReceiverStreamClient extends ch.qos.logback.core.spi.ContextAwareBase implements ch.qos.logback.core.net.server.RemoteReceiverClient {
    private final java.lang.String clientId;
    private final java.io.OutputStream outputStream;
    private java.util.concurrent.BlockingQueue<java.io.Serializable> queue;
    private final java.net.Socket socket;

    RemoteReceiverStreamClient(java.lang.String str, java.io.OutputStream outputStream2) {
        this.clientId = "client " + str + ": ";
        this.socket = null;
        this.outputStream = outputStream2;
    }

    public RemoteReceiverStreamClient(java.lang.String str, java.net.Socket socket2) {
        this.clientId = "client " + str + ": ";
        this.socket = socket2;
        this.outputStream = null;
    }

    private java.io.ObjectOutputStream createObjectOutputStream() throws java.io.IOException {
        return this.socket == null ? new java.io.ObjectOutputStream(this.outputStream) : new java.io.ObjectOutputStream(this.socket.getOutputStream());
    }

    public void close() {
        if (this.socket != null) {
            ch.qos.logback.core.util.CloseUtil.closeQuietly(this.socket);
        }
    }

    public boolean offer(java.io.Serializable serializable) {
        if (this.queue != null) {
            return this.queue.offer(serializable);
        }
        throw new java.lang.IllegalStateException("client has no event queue");
    }

    /* JADX WARNING: Removed duplicated region for block: B:35:0x00c2  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00fb  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x011f  */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:39:0x00e3=Splitter:B:39:0x00e3, B:32:0x00aa=Splitter:B:32:0x00aa} */
    public void run() {
        java.io.ObjectOutputStream objectOutputStream;
        java.io.ObjectOutputStream objectOutputStream2;
        int i;
        addInfo(this.clientId + "connected");
        try {
            objectOutputStream = createObjectOutputStream();
            int i2 = 0;
            while (!java.lang.Thread.currentThread().isInterrupted()) {
                try {
                    try {
                        objectOutputStream.writeObject((java.io.Serializable) this.queue.take());
                        objectOutputStream.flush();
                        int i3 = i2 + 1;
                        if (i3 >= 70) {
                            try {
                                objectOutputStream.reset();
                                i3 = 0;
                            } catch (java.lang.InterruptedException e) {
                                i = 0;
                                java.lang.Thread.currentThread().interrupt();
                                i2 = i;
                            }
                        }
                        i2 = i3;
                    } catch (java.lang.InterruptedException e2) {
                        i = i2;
                        java.lang.Thread.currentThread().interrupt();
                        i2 = i;
                    }
                } catch (java.net.SocketException e3) {
                    e = e3;
                    objectOutputStream2 = objectOutputStream;
                } catch (java.io.IOException e4) {
                    e = e4;
                    try {
                        addError(this.clientId + e);
                        if (objectOutputStream != null) {
                        }
                        close();
                        addInfo(this.clientId + "connection closed");
                    } catch (Throwable th) {
                        th = th;
                        if (objectOutputStream != null) {
                        }
                        close();
                        addInfo(this.clientId + "connection closed");
                        throw th;
                    }
                } catch (java.lang.RuntimeException e5) {
                    e = e5;
                    addError(this.clientId + e);
                    if (objectOutputStream != null) {
                    }
                    close();
                    addInfo(this.clientId + "connection closed");
                }
            }
            if (objectOutputStream != null) {
                ch.qos.logback.core.util.CloseUtil.closeQuietly((java.io.Closeable) objectOutputStream);
            }
            close();
            addInfo(this.clientId + "connection closed");
        } catch (java.net.SocketException e6) {
            e = e6;
            objectOutputStream2 = null;
            try {
                addInfo(this.clientId + e);
                if (objectOutputStream2 != null) {
                    ch.qos.logback.core.util.CloseUtil.closeQuietly((java.io.Closeable) objectOutputStream2);
                }
                close();
                addInfo(this.clientId + "connection closed");
            } catch (Throwable th2) {
                th = th2;
                objectOutputStream = objectOutputStream2;
                if (objectOutputStream != null) {
                    ch.qos.logback.core.util.CloseUtil.closeQuietly((java.io.Closeable) objectOutputStream);
                }
                close();
                addInfo(this.clientId + "connection closed");
                throw th;
            }
        } catch (java.io.IOException e7) {
            e = e7;
            objectOutputStream = null;
            addError(this.clientId + e);
            if (objectOutputStream != null) {
                ch.qos.logback.core.util.CloseUtil.closeQuietly((java.io.Closeable) objectOutputStream);
            }
            close();
            addInfo(this.clientId + "connection closed");
        } catch (java.lang.RuntimeException e8) {
            e = e8;
            objectOutputStream = null;
            addError(this.clientId + e);
            if (objectOutputStream != null) {
                ch.qos.logback.core.util.CloseUtil.closeQuietly((java.io.Closeable) objectOutputStream);
            }
            close();
            addInfo(this.clientId + "connection closed");
        } catch (Throwable th3) {
            th = th3;
            objectOutputStream = null;
            if (objectOutputStream != null) {
            }
            close();
            addInfo(this.clientId + "connection closed");
            throw th;
        }
    }

    public void setQueue(java.util.concurrent.BlockingQueue<java.io.Serializable> blockingQueue) {
        this.queue = blockingQueue;
    }
}
