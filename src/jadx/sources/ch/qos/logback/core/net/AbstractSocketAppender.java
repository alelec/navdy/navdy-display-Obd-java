package ch.qos.logback.core.net;

public abstract class AbstractSocketAppender<E> extends ch.qos.logback.core.AppenderBase<E> implements java.lang.Runnable, ch.qos.logback.core.net.SocketConnector.ExceptionHandler {
    private static final int DEFAULT_ACCEPT_CONNECTION_DELAY = 5000;
    private static final int DEFAULT_EVENT_DELAY_TIMEOUT = 100;
    public static final int DEFAULT_PORT = 4560;
    public static final int DEFAULT_QUEUE_SIZE = 128;
    public static final int DEFAULT_RECONNECTION_DELAY = 30000;
    private int acceptConnectionTimeout = 5000;
    private java.net.InetAddress address;
    private java.util.concurrent.Future<java.net.Socket> connectorTask;
    private ch.qos.logback.core.util.Duration eventDelayLimit = new ch.qos.logback.core.util.Duration(100);
    private java.lang.String peerId;
    private int port = DEFAULT_PORT;
    private java.util.concurrent.BlockingQueue<E> queue;
    private int queueSize = 128;
    private ch.qos.logback.core.util.Duration reconnectionDelay = new ch.qos.logback.core.util.Duration(30000);
    private java.lang.String remoteHost;
    private volatile java.net.Socket socket;
    private java.util.concurrent.Future<?> task;

    protected AbstractSocketAppender() {
    }

    @java.lang.Deprecated
    protected AbstractSocketAppender(java.lang.String str, int i) {
        this.remoteHost = str;
        this.port = i;
    }

    private java.util.concurrent.Future<java.net.Socket> activateConnector(ch.qos.logback.core.net.SocketConnector socketConnector) {
        try {
            return getContext().getExecutorService().submit(socketConnector);
        } catch (java.util.concurrent.RejectedExecutionException e) {
            return null;
        }
    }

    private ch.qos.logback.core.net.SocketConnector createConnector(java.net.InetAddress inetAddress, int i, int i2, long j) {
        ch.qos.logback.core.net.SocketConnector newConnector = newConnector(inetAddress, i, (long) i2, j);
        newConnector.setExceptionHandler(this);
        newConnector.setSocketFactory(getSocketFactory());
        return newConnector;
    }

    private void dispatchEvents() throws java.lang.InterruptedException {
        try {
            this.socket.setSoTimeout(this.acceptConnectionTimeout);
            java.io.ObjectOutputStream objectOutputStream = new java.io.ObjectOutputStream(this.socket.getOutputStream());
            this.socket.setSoTimeout(0);
            addInfo(this.peerId + "connection established");
            int i = 0;
            while (true) {
                java.lang.Object take = this.queue.take();
                postProcessEvent(take);
                objectOutputStream.writeObject(getPST().transform(take));
                objectOutputStream.flush();
                i++;
                if (i >= 70) {
                    objectOutputStream.reset();
                    i = 0;
                }
            }
        } catch (java.io.IOException e) {
            addInfo(this.peerId + "connection failed: " + e);
            ch.qos.logback.core.util.CloseUtil.closeQuietly(this.socket);
            this.socket = null;
            addInfo(this.peerId + "connection closed");
        } catch (Throwable th) {
            ch.qos.logback.core.util.CloseUtil.closeQuietly(this.socket);
            this.socket = null;
            addInfo(this.peerId + "connection closed");
            throw th;
        }
    }

    @java.lang.Deprecated
    protected static java.net.InetAddress getAddressByName(java.lang.String str) {
        try {
            return java.net.InetAddress.getByName(str);
        } catch (java.lang.Exception e) {
            return null;
        }
    }

    private java.net.Socket waitForConnectorToReturnASocket() throws java.lang.InterruptedException {
        try {
            java.net.Socket socket2 = (java.net.Socket) this.connectorTask.get();
            this.connectorTask = null;
            return socket2;
        } catch (java.util.concurrent.ExecutionException e) {
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void append(E e) {
        if (e != null && isStarted()) {
            try {
                if (!this.queue.offer(e, this.eventDelayLimit.getMilliseconds(), java.util.concurrent.TimeUnit.MILLISECONDS)) {
                    addInfo("Dropping event due to timeout limit of [" + this.eventDelayLimit + "] milliseconds being exceeded");
                }
            } catch (java.lang.InterruptedException e2) {
                addError("Interrupted while appending event to SocketAppender", e2);
            }
        }
    }

    public void connectionFailed(ch.qos.logback.core.net.SocketConnector socketConnector, java.lang.Exception exc) {
        if (exc instanceof java.lang.InterruptedException) {
            addInfo("connector interrupted");
        } else if (exc instanceof java.net.ConnectException) {
            addInfo(this.peerId + "connection refused");
        } else {
            addInfo(this.peerId + exc);
        }
    }

    public ch.qos.logback.core.util.Duration getEventDelayLimit() {
        return this.eventDelayLimit;
    }

    /* access modifiers changed from: protected */
    public abstract ch.qos.logback.core.spi.PreSerializationTransformer<E> getPST();

    public int getPort() {
        return this.port;
    }

    public int getQueueSize() {
        return this.queueSize;
    }

    public ch.qos.logback.core.util.Duration getReconnectionDelay() {
        return this.reconnectionDelay;
    }

    public java.lang.String getRemoteHost() {
        return this.remoteHost;
    }

    /* access modifiers changed from: protected */
    public javax.net.SocketFactory getSocketFactory() {
        return javax.net.SocketFactory.getDefault();
    }

    /* access modifiers changed from: 0000 */
    public java.util.concurrent.BlockingQueue<E> newBlockingQueue(int i) {
        return i <= 0 ? new java.util.concurrent.SynchronousQueue<>() : new java.util.concurrent.ArrayBlockingQueue<>(i);
    }

    /* access modifiers changed from: protected */
    public ch.qos.logback.core.net.SocketConnector newConnector(java.net.InetAddress inetAddress, int i, long j, long j2) {
        return new ch.qos.logback.core.net.DefaultSocketConnector(inetAddress, i, j, j2);
    }

    /* access modifiers changed from: protected */
    public abstract void postProcessEvent(E e);

    public final void run() {
        signalEntryInRunMethod();
        while (!java.lang.Thread.currentThread().isInterrupted()) {
            try {
                this.connectorTask = activateConnector(createConnector(this.address, this.port, 0, this.reconnectionDelay.getMilliseconds()));
                if (this.connectorTask != null) {
                    this.socket = waitForConnectorToReturnASocket();
                    if (this.socket == null) {
                        break;
                    }
                    dispatchEvents();
                } else {
                    break;
                }
            } catch (java.lang.InterruptedException e) {
            }
        }
        addInfo("shutting down");
    }

    /* access modifiers changed from: 0000 */
    public void setAcceptConnectionTimeout(int i) {
        this.acceptConnectionTimeout = i;
    }

    public void setEventDelayLimit(ch.qos.logback.core.util.Duration duration) {
        this.eventDelayLimit = duration;
    }

    public void setPort(int i) {
        this.port = i;
    }

    public void setQueueSize(int i) {
        this.queueSize = i;
    }

    public void setReconnectionDelay(ch.qos.logback.core.util.Duration duration) {
        this.reconnectionDelay = duration;
    }

    public void setRemoteHost(java.lang.String str) {
        this.remoteHost = str;
    }

    /* access modifiers changed from: protected */
    public void signalEntryInRunMethod() {
    }

    public void start() {
        if (!isStarted()) {
            int i = 0;
            if (this.port <= 0) {
                i = 1;
                addError("No port was configured for appender" + this.name + " For more information, please visit http://logback.qos.ch/codes.html#socket_no_port");
            }
            if (this.remoteHost == null) {
                i++;
                addError("No remote host was configured for appender" + this.name + " For more information, please visit http://logback.qos.ch/codes.html#socket_no_host");
            }
            if (this.queueSize < 0) {
                i++;
                addError("Queue size must be non-negative");
            }
            if (i == 0) {
                try {
                    this.address = java.net.InetAddress.getByName(this.remoteHost);
                } catch (java.net.UnknownHostException e) {
                    addError("unknown host: " + this.remoteHost);
                    i++;
                }
            }
            if (i == 0) {
                this.queue = newBlockingQueue(this.queueSize);
                this.peerId = "remote peer " + this.remoteHost + ":" + this.port + ": ";
                this.task = getContext().getExecutorService().submit(this);
                super.start();
            }
        }
    }

    public void stop() {
        if (isStarted()) {
            ch.qos.logback.core.util.CloseUtil.closeQuietly(this.socket);
            this.task.cancel(true);
            if (this.connectorTask != null) {
                this.connectorTask.cancel(true);
            }
            super.stop();
        }
    }
}
