package ch.qos.logback.core.net;

public class DefaultSocketConnector implements ch.qos.logback.core.net.SocketConnector {
    private final java.net.InetAddress address;
    private final ch.qos.logback.core.util.DelayStrategy delayStrategy;
    private ch.qos.logback.core.net.SocketConnector.ExceptionHandler exceptionHandler;
    private final int port;
    private javax.net.SocketFactory socketFactory;

    private static class ConsoleExceptionHandler implements ch.qos.logback.core.net.SocketConnector.ExceptionHandler {
        private ConsoleExceptionHandler() {
        }

        public void connectionFailed(ch.qos.logback.core.net.SocketConnector socketConnector, java.lang.Exception exc) {
            java.lang.System.out.println(exc);
        }
    }

    public DefaultSocketConnector(java.net.InetAddress inetAddress, int i, long j, long j2) {
        this(inetAddress, i, new ch.qos.logback.core.util.FixedDelay(j, j2));
    }

    public DefaultSocketConnector(java.net.InetAddress inetAddress, int i, ch.qos.logback.core.util.DelayStrategy delayStrategy2) {
        this.address = inetAddress;
        this.port = i;
        this.delayStrategy = delayStrategy2;
    }

    private java.net.Socket createSocket() {
        boolean z = false;
        try {
            return this.socketFactory.createSocket(this.address, this.port);
        } catch (java.io.IOException e) {
            this.exceptionHandler.connectionFailed(this, e);
            return z;
        }
    }

    private void useDefaultsForMissingFields() {
        if (this.exceptionHandler == null) {
            this.exceptionHandler = new ch.qos.logback.core.net.DefaultSocketConnector.ConsoleExceptionHandler();
        }
        if (this.socketFactory == null) {
            this.socketFactory = javax.net.SocketFactory.getDefault();
        }
    }

    public java.net.Socket call() throws java.lang.InterruptedException {
        useDefaultsForMissingFields();
        java.net.Socket createSocket = createSocket();
        while (createSocket == null && !java.lang.Thread.currentThread().isInterrupted()) {
            java.lang.Thread.sleep(this.delayStrategy.nextDelay());
            createSocket = createSocket();
        }
        return createSocket;
    }

    public void setExceptionHandler(ch.qos.logback.core.net.SocketConnector.ExceptionHandler exceptionHandler2) {
        this.exceptionHandler = exceptionHandler2;
    }

    public void setSocketFactory(javax.net.SocketFactory socketFactory2) {
        this.socketFactory = socketFactory2;
    }
}
