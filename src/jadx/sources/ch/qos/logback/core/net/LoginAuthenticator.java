package ch.qos.logback.core.net;

public class LoginAuthenticator extends javax.mail.Authenticator {
    java.lang.String password;
    java.lang.String username;

    LoginAuthenticator(java.lang.String str, java.lang.String str2) {
        this.username = str;
        this.password = str2;
    }

    public javax.mail.PasswordAuthentication getPasswordAuthentication() {
        return new javax.mail.PasswordAuthentication(this.username, this.password);
    }
}
