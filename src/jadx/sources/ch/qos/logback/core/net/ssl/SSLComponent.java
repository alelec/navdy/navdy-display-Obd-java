package ch.qos.logback.core.net.ssl;

public interface SSLComponent {
    ch.qos.logback.core.net.ssl.SSLConfiguration getSsl();

    void setSsl(ch.qos.logback.core.net.ssl.SSLConfiguration sSLConfiguration);
}
