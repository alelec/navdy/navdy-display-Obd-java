package ch.qos.logback.core.net.ssl;

public class KeyStoreFactoryBean {
    private java.lang.String location;
    private java.lang.String password;
    private java.lang.String provider;
    private java.lang.String type;

    private java.security.KeyStore newKeyStore() throws java.security.NoSuchAlgorithmException, java.security.NoSuchProviderException, java.security.KeyStoreException {
        return getProvider() != null ? java.security.KeyStore.getInstance(getType(), getProvider()) : java.security.KeyStore.getInstance(getType());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0054, code lost:
        throw new java.security.NoSuchProviderException("no such keystore provider: " + getProvider());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0055, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0056, code lost:
        r5 = r1;
        r1 = r0;
        r0 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x007c, code lost:
        throw new java.security.NoSuchAlgorithmException("no such keystore type: " + getType());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x009a, code lost:
        throw new java.security.KeyStoreException(getLocation() + ": file not found");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00c6, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00c7, code lost:
        r1.printStackTrace(java.lang.System.err);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00d2, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00d3, code lost:
        r5 = r1;
        r1 = r0;
        r0 = r5;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0037 A[ExcHandler: NoSuchProviderException (e java.security.NoSuchProviderException), Splitter:B:5:0x000f] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x005b A[SYNTHETIC, Splitter:B:22:0x005b] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x005f A[ExcHandler: NoSuchAlgorithmException (e java.security.NoSuchAlgorithmException), Splitter:B:5:0x000f] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x007d A[Catch:{ NoSuchProviderException -> 0x0037, NoSuchAlgorithmException -> 0x005f, FileNotFoundException -> 0x007d, Exception -> 0x00d2, all -> 0x0055 }, ExcHandler: FileNotFoundException (e java.io.FileNotFoundException), Splitter:B:5:0x000f] */
    public java.security.KeyStore createKeyStore() throws java.security.NoSuchProviderException, java.security.NoSuchAlgorithmException, java.security.KeyStoreException {
        java.io.InputStream inputStream;
        java.lang.Throwable th;
        if (getLocation() == null) {
            throw new java.lang.IllegalArgumentException("location is required");
        }
        java.io.InputStream inputStream2 = null;
        try {
            java.io.InputStream openStream = ch.qos.logback.core.util.LocationUtil.urlForResource(getLocation()).openStream();
            java.security.KeyStore newKeyStore = newKeyStore();
            newKeyStore.load(openStream, getPassword().toCharArray());
            if (openStream != null) {
                try {
                    openStream.close();
                } catch (java.io.IOException e) {
                    e.printStackTrace(java.lang.System.err);
                }
            }
            return newKeyStore;
        } catch (java.security.NoSuchProviderException e2) {
        } catch (java.security.NoSuchAlgorithmException e3) {
        } catch (java.io.FileNotFoundException e4) {
        } catch (java.lang.Exception e5) {
            java.lang.Exception exc = e5;
            inputStream = inputStream2;
            java.lang.Exception exc2 = exc;
            try {
                throw new java.security.KeyStoreException(getLocation() + ": " + exc2.getMessage(), exc2);
            } catch (Throwable th2) {
                th = th2;
                if (inputStream != null) {
                }
                throw th;
            }
        } catch (Throwable th3) {
            java.lang.Throwable th4 = th3;
            inputStream = inputStream2;
            th = th4;
            if (inputStream != null) {
            }
            throw th;
        }
    }

    public java.lang.String getLocation() {
        return this.location;
    }

    public java.lang.String getPassword() {
        return this.password == null ? ch.qos.logback.core.net.ssl.SSL.DEFAULT_KEYSTORE_PASSWORD : this.password;
    }

    public java.lang.String getProvider() {
        return this.provider;
    }

    public java.lang.String getType() {
        return this.type == null ? ch.qos.logback.core.net.ssl.SSL.DEFAULT_KEYSTORE_TYPE : this.type;
    }

    public void setLocation(java.lang.String str) {
        this.location = str;
    }

    public void setPassword(java.lang.String str) {
        this.password = str;
    }

    public void setProvider(java.lang.String str) {
        this.provider = str;
    }

    public void setType(java.lang.String str) {
        this.type = str;
    }
}
