package ch.qos.logback.core.net.ssl;

public class TrustManagerFactoryFactoryBean {
    private java.lang.String algorithm;
    private java.lang.String provider;

    public javax.net.ssl.TrustManagerFactory createTrustManagerFactory() throws java.security.NoSuchProviderException, java.security.NoSuchAlgorithmException {
        return getProvider() != null ? javax.net.ssl.TrustManagerFactory.getInstance(getAlgorithm(), getProvider()) : javax.net.ssl.TrustManagerFactory.getInstance(getAlgorithm());
    }

    public java.lang.String getAlgorithm() {
        return this.algorithm == null ? javax.net.ssl.TrustManagerFactory.getDefaultAlgorithm() : this.algorithm;
    }

    public java.lang.String getProvider() {
        return this.provider;
    }

    public void setAlgorithm(java.lang.String str) {
        this.algorithm = str;
    }

    public void setProvider(java.lang.String str) {
        this.provider = str;
    }
}
