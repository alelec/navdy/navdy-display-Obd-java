package ch.qos.logback.core.net.server;

public interface ClientVisitor<T extends ch.qos.logback.core.net.server.Client> {
    void visit(T t);
}
