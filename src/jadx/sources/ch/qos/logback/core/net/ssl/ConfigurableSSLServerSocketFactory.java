package ch.qos.logback.core.net.ssl;

public class ConfigurableSSLServerSocketFactory extends javax.net.ServerSocketFactory {
    private final javax.net.ssl.SSLServerSocketFactory delegate;
    private final ch.qos.logback.core.net.ssl.SSLParametersConfiguration parameters;

    public ConfigurableSSLServerSocketFactory(ch.qos.logback.core.net.ssl.SSLParametersConfiguration sSLParametersConfiguration, javax.net.ssl.SSLServerSocketFactory sSLServerSocketFactory) {
        this.parameters = sSLParametersConfiguration;
        this.delegate = sSLServerSocketFactory;
    }

    public java.net.ServerSocket createServerSocket(int i) throws java.io.IOException {
        javax.net.ssl.SSLServerSocket sSLServerSocket = (javax.net.ssl.SSLServerSocket) this.delegate.createServerSocket(i);
        this.parameters.configure(new ch.qos.logback.core.net.ssl.SSLConfigurableServerSocket(sSLServerSocket));
        return sSLServerSocket;
    }

    public java.net.ServerSocket createServerSocket(int i, int i2) throws java.io.IOException {
        javax.net.ssl.SSLServerSocket sSLServerSocket = (javax.net.ssl.SSLServerSocket) this.delegate.createServerSocket(i, i2);
        this.parameters.configure(new ch.qos.logback.core.net.ssl.SSLConfigurableServerSocket(sSLServerSocket));
        return sSLServerSocket;
    }

    public java.net.ServerSocket createServerSocket(int i, int i2, java.net.InetAddress inetAddress) throws java.io.IOException {
        javax.net.ssl.SSLServerSocket sSLServerSocket = (javax.net.ssl.SSLServerSocket) this.delegate.createServerSocket(i, i2, inetAddress);
        this.parameters.configure(new ch.qos.logback.core.net.ssl.SSLConfigurableServerSocket(sSLServerSocket));
        return sSLServerSocket;
    }
}
