package ch.qos.logback.core.net.server;

public abstract class ServerSocketListener<T extends ch.qos.logback.core.net.server.Client> implements ch.qos.logback.core.net.server.ServerListener<T> {
    private final java.net.ServerSocket serverSocket;

    public ServerSocketListener(java.net.ServerSocket serverSocket2) {
        this.serverSocket = serverSocket2;
    }

    private java.lang.String socketAddressToString(java.net.SocketAddress socketAddress) {
        java.lang.String obj = socketAddress.toString();
        int indexOf = obj.indexOf("/");
        return indexOf >= 0 ? obj.substring(indexOf + 1) : obj;
    }

    public T acceptClient() throws java.io.IOException {
        java.net.Socket accept = this.serverSocket.accept();
        return createClient(socketAddressToString(accept.getRemoteSocketAddress()), accept);
    }

    public void close() {
        ch.qos.logback.core.util.CloseUtil.closeQuietly(this.serverSocket);
    }

    /* access modifiers changed from: protected */
    public abstract T createClient(java.lang.String str, java.net.Socket socket) throws java.io.IOException;

    public java.lang.String toString() {
        return socketAddressToString(this.serverSocket.getLocalSocketAddress());
    }
}
