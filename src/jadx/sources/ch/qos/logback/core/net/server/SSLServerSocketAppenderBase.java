package ch.qos.logback.core.net.server;

public abstract class SSLServerSocketAppenderBase<E> extends ch.qos.logback.core.net.server.AbstractServerSocketAppender<E> implements ch.qos.logback.core.net.ssl.SSLComponent {
    private javax.net.ServerSocketFactory socketFactory;
    private ch.qos.logback.core.net.ssl.SSLConfiguration ssl;

    /* access modifiers changed from: protected */
    public javax.net.ServerSocketFactory getServerSocketFactory() {
        return this.socketFactory;
    }

    public ch.qos.logback.core.net.ssl.SSLConfiguration getSsl() {
        if (this.ssl == null) {
            this.ssl = new ch.qos.logback.core.net.ssl.SSLConfiguration();
        }
        return this.ssl;
    }

    public void setSsl(ch.qos.logback.core.net.ssl.SSLConfiguration sSLConfiguration) {
        this.ssl = sSLConfiguration;
    }

    public void start() {
        try {
            javax.net.ssl.SSLContext createContext = getSsl().createContext(this);
            ch.qos.logback.core.net.ssl.SSLParametersConfiguration parameters = getSsl().getParameters();
            parameters.setContext(getContext());
            this.socketFactory = new ch.qos.logback.core.net.ssl.ConfigurableSSLServerSocketFactory(parameters, createContext.getServerSocketFactory());
            super.start();
        } catch (java.lang.Exception e) {
            addError(e.getMessage(), e);
        }
    }
}
