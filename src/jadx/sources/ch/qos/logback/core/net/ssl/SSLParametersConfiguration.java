package ch.qos.logback.core.net.ssl;

public class SSLParametersConfiguration extends ch.qos.logback.core.spi.ContextAwareBase {
    private java.lang.String[] enabledCipherSuites;
    private java.lang.String[] enabledProtocols;
    private java.lang.String excludedCipherSuites;
    private java.lang.String excludedProtocols;
    private java.lang.String includedCipherSuites;
    private java.lang.String includedProtocols;
    private java.lang.Boolean needClientAuth;
    private java.lang.Boolean wantClientAuth;

    private java.lang.String[] enabledCipherSuites(java.lang.String[] strArr, java.lang.String[] strArr2) {
        if (this.enabledCipherSuites == null) {
            if (!ch.qos.logback.core.util.OptionHelper.isEmpty(getIncludedCipherSuites()) || !ch.qos.logback.core.util.OptionHelper.isEmpty(getExcludedCipherSuites())) {
                this.enabledCipherSuites = includedStrings(strArr, getIncludedCipherSuites(), getExcludedCipherSuites());
            } else {
                this.enabledCipherSuites = (java.lang.String[]) java.util.Arrays.copyOf(strArr2, strArr2.length);
            }
            java.lang.String[] strArr3 = this.enabledCipherSuites;
            int length = strArr3.length;
            for (int i = 0; i < length; i++) {
                addInfo("enabled cipher suite: " + strArr3[i]);
            }
        }
        return this.enabledCipherSuites;
    }

    private java.lang.String[] enabledProtocols(java.lang.String[] strArr, java.lang.String[] strArr2) {
        if (this.enabledProtocols == null) {
            if (!ch.qos.logback.core.util.OptionHelper.isEmpty(getIncludedProtocols()) || !ch.qos.logback.core.util.OptionHelper.isEmpty(getExcludedProtocols())) {
                this.enabledProtocols = includedStrings(strArr, getIncludedProtocols(), getExcludedProtocols());
            } else {
                this.enabledProtocols = (java.lang.String[]) java.util.Arrays.copyOf(strArr2, strArr2.length);
            }
            java.lang.String[] strArr3 = this.enabledProtocols;
            int length = strArr3.length;
            for (int i = 0; i < length; i++) {
                addInfo("enabled protocol: " + strArr3[i]);
            }
        }
        return this.enabledProtocols;
    }

    private java.lang.String[] includedStrings(java.lang.String[] strArr, java.lang.String str, java.lang.String str2) {
        java.util.ArrayList arrayList = new java.util.ArrayList(strArr.length);
        arrayList.addAll(java.util.Arrays.asList(strArr));
        if (str != null) {
            ch.qos.logback.core.util.StringCollectionUtil.retainMatching((java.util.Collection<java.lang.String>) arrayList, stringToArray(str));
        }
        if (str2 != null) {
            ch.qos.logback.core.util.StringCollectionUtil.removeMatching((java.util.Collection<java.lang.String>) arrayList, stringToArray(str2));
        }
        return (java.lang.String[]) arrayList.toArray(new java.lang.String[arrayList.size()]);
    }

    private java.lang.String[] stringToArray(java.lang.String str) {
        return str.split("\\s*,\\s*");
    }

    public void configure(ch.qos.logback.core.net.ssl.SSLConfigurable sSLConfigurable) {
        sSLConfigurable.setEnabledProtocols(enabledProtocols(sSLConfigurable.getSupportedProtocols(), sSLConfigurable.getDefaultProtocols()));
        sSLConfigurable.setEnabledCipherSuites(enabledCipherSuites(sSLConfigurable.getSupportedCipherSuites(), sSLConfigurable.getDefaultCipherSuites()));
        if (isNeedClientAuth() != null) {
            sSLConfigurable.setNeedClientAuth(isNeedClientAuth().booleanValue());
        }
        if (isWantClientAuth() != null) {
            sSLConfigurable.setWantClientAuth(isWantClientAuth().booleanValue());
        }
    }

    public java.lang.String getExcludedCipherSuites() {
        return this.excludedCipherSuites;
    }

    public java.lang.String getExcludedProtocols() {
        return this.excludedProtocols;
    }

    public java.lang.String getIncludedCipherSuites() {
        return this.includedCipherSuites;
    }

    public java.lang.String getIncludedProtocols() {
        return this.includedProtocols;
    }

    public java.lang.Boolean isNeedClientAuth() {
        return this.needClientAuth;
    }

    public java.lang.Boolean isWantClientAuth() {
        return this.wantClientAuth;
    }

    public void setExcludedCipherSuites(java.lang.String str) {
        this.excludedCipherSuites = str;
    }

    public void setExcludedProtocols(java.lang.String str) {
        this.excludedProtocols = str;
    }

    public void setIncludedCipherSuites(java.lang.String str) {
        this.includedCipherSuites = str;
    }

    public void setIncludedProtocols(java.lang.String str) {
        this.includedProtocols = str;
    }

    public void setNeedClientAuth(java.lang.Boolean bool) {
        this.needClientAuth = bool;
    }

    public void setWantClientAuth(java.lang.Boolean bool) {
        this.wantClientAuth = bool;
    }
}
