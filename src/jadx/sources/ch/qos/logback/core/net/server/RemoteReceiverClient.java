package ch.qos.logback.core.net.server;

interface RemoteReceiverClient extends ch.qos.logback.core.net.server.Client, ch.qos.logback.core.spi.ContextAware {
    boolean offer(java.io.Serializable serializable);

    void setQueue(java.util.concurrent.BlockingQueue<java.io.Serializable> blockingQueue);
}
