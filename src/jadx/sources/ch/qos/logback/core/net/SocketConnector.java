package ch.qos.logback.core.net;

public interface SocketConnector extends java.util.concurrent.Callable<java.net.Socket> {

    public interface ExceptionHandler {
        void connectionFailed(ch.qos.logback.core.net.SocketConnector socketConnector, java.lang.Exception exc);
    }

    java.net.Socket call() throws java.lang.InterruptedException;

    void setExceptionHandler(ch.qos.logback.core.net.SocketConnector.ExceptionHandler exceptionHandler);

    void setSocketFactory(javax.net.SocketFactory socketFactory);
}
