package ch.qos.logback.core.net;

public abstract class SMTPAppenderBase<E> extends ch.qos.logback.core.AppenderBase<E> {
    static javax.mail.internet.InternetAddress[] EMPTY_IA_ARRAY = new javax.mail.internet.InternetAddress[0];
    static final int MAX_DELAY_BETWEEN_STATUS_MESSAGES = 1228800000;
    boolean asynchronousSending = true;
    protected ch.qos.logback.core.spi.CyclicBufferTracker<E> cbTracker;
    private java.lang.String charsetEncoding = util.Util.UTF_8;
    int delayBetweenStatusMessages = 300000;
    protected ch.qos.logback.core.sift.Discriminator<E> discriminator = new ch.qos.logback.core.sift.DefaultDiscriminator();
    private int errorCount = 0;
    protected ch.qos.logback.core.boolex.EventEvaluator<E> eventEvaluator;
    private java.lang.String from;
    long lastTrackerStatusPrint = 0;
    protected ch.qos.logback.core.Layout<E> layout;
    java.lang.String localhost;
    protected javax.mail.internet.MimeMessage mimeMsg;
    java.lang.String password;
    private java.lang.String smtpHost;
    private int smtpPort = 25;
    private boolean ssl = false;
    private boolean starttls = false;
    protected ch.qos.logback.core.Layout<E> subjectLayout;
    private java.lang.String subjectStr = null;
    private java.util.List<ch.qos.logback.core.pattern.PatternLayoutBase<E>> toPatternLayoutList = new java.util.ArrayList();
    java.lang.String username;

    class SenderRunnable implements java.lang.Runnable {
        final ch.qos.logback.core.helpers.CyclicBuffer<E> cyclicBuffer;
        final E e;

        SenderRunnable(ch.qos.logback.core.helpers.CyclicBuffer<E> cyclicBuffer2, E e2) {
            this.cyclicBuffer = cyclicBuffer2;
            this.e = e2;
        }

        public void run() {
            ch.qos.logback.core.net.SMTPAppenderBase.this.sendBuffer(this.cyclicBuffer, this.e);
        }
    }

    private javax.mail.Session buildSessionFromProperties() {
        java.util.Properties properties = new java.util.Properties(ch.qos.logback.core.util.OptionHelper.getSystemProperties());
        if (this.smtpHost != null) {
            properties.put("mail.smtp.host", this.smtpHost);
        }
        properties.put("mail.smtp.port", java.lang.Integer.toString(this.smtpPort));
        if (this.localhost != null) {
            properties.put("mail.smtp.localhost", this.localhost);
        }
        ch.qos.logback.core.net.LoginAuthenticator loginAuthenticator = null;
        if (this.username != null) {
            loginAuthenticator = new ch.qos.logback.core.net.LoginAuthenticator(this.username, this.password);
            properties.put("mail.smtp.auth", "true");
        }
        if (!isSTARTTLS() || !isSSL()) {
            if (isSTARTTLS()) {
                properties.put("mail.smtp.starttls.enable", "true");
            }
            if (isSSL()) {
                properties.put("mail.smtp.socketFactory.port", java.lang.Integer.toString(this.smtpPort));
                properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
                properties.put("mail.smtp.socketFactory.fallback", "true");
            }
        } else {
            addError("Both SSL and StartTLS cannot be enabled simultaneously");
        }
        return javax.mail.Session.getInstance(properties, loginAuthenticator);
    }

    private java.util.List<javax.mail.internet.InternetAddress> parseAddress(E e) {
        int size = this.toPatternLayoutList.size();
        java.util.ArrayList arrayList = new java.util.ArrayList();
        int i = 0;
        while (i < size) {
            try {
                java.lang.String doLayout = ((ch.qos.logback.core.pattern.PatternLayoutBase) this.toPatternLayoutList.get(i)).doLayout(e);
                if (!(doLayout == null || doLayout.length() == 0)) {
                    arrayList.addAll(java.util.Arrays.asList(javax.mail.internet.InternetAddress.parse(doLayout, true)));
                }
                i++;
            } catch (javax.mail.internet.AddressException e2) {
                addError("Could not parse email address for [" + this.toPatternLayoutList.get(i) + "] for event [" + e + "]", e2);
                return arrayList;
            }
        }
        return arrayList;
    }

    public void addTo(java.lang.String str) {
        if (str == null || str.length() == 0) {
            throw new java.lang.IllegalArgumentException("Null or empty <to> property");
        }
        ch.qos.logback.core.pattern.PatternLayoutBase makeNewToPatternLayout = makeNewToPatternLayout(str.trim());
        makeNewToPatternLayout.setContext(this.context);
        makeNewToPatternLayout.start();
        this.toPatternLayoutList.add(makeNewToPatternLayout);
    }

    /* access modifiers changed from: protected */
    public void append(E e) {
        if (checkEntryConditions()) {
            java.lang.String discriminatingValue = this.discriminator.getDiscriminatingValue(e);
            long currentTimeMillis = java.lang.System.currentTimeMillis();
            ch.qos.logback.core.helpers.CyclicBuffer cyclicBuffer = (ch.qos.logback.core.helpers.CyclicBuffer) this.cbTracker.getOrCreate(discriminatingValue, currentTimeMillis);
            subAppend(cyclicBuffer, e);
            try {
                if (this.eventEvaluator.evaluate(e)) {
                    ch.qos.logback.core.helpers.CyclicBuffer cyclicBuffer2 = new ch.qos.logback.core.helpers.CyclicBuffer(cyclicBuffer);
                    cyclicBuffer.clear();
                    if (this.asynchronousSending) {
                        this.context.getExecutorService().execute(new ch.qos.logback.core.net.SMTPAppenderBase.SenderRunnable(cyclicBuffer2, e));
                    } else {
                        sendBuffer(cyclicBuffer2, e);
                    }
                }
            } catch (ch.qos.logback.core.boolex.EvaluationException e2) {
                this.errorCount++;
                if (this.errorCount < 4) {
                    addError("SMTPAppender's EventEvaluator threw an Exception-", e2);
                }
            }
            if (eventMarksEndOfLife(e)) {
                this.cbTracker.endOfLife(discriminatingValue);
            }
            this.cbTracker.removeStaleComponents(currentTimeMillis);
            if (this.lastTrackerStatusPrint + ((long) this.delayBetweenStatusMessages) < currentTimeMillis) {
                addInfo("SMTPAppender [" + this.name + "] is tracking [" + this.cbTracker.getComponentCount() + "] buffers");
                this.lastTrackerStatusPrint = currentTimeMillis;
                if (this.delayBetweenStatusMessages < MAX_DELAY_BETWEEN_STATUS_MESSAGES) {
                    this.delayBetweenStatusMessages *= 4;
                }
            }
        }
    }

    public boolean checkEntryConditions() {
        if (!this.started) {
            addError("Attempting to append to a non-started appender: " + getName());
            return false;
        } else if (this.mimeMsg == null) {
            addError("Message object not configured.");
            return false;
        } else if (this.eventEvaluator == null) {
            addError("No EventEvaluator is set for appender [" + this.name + "].");
            return false;
        } else if (this.layout != null) {
            return true;
        } else {
            addError("No layout set for appender named [" + this.name + "]. For more information, please visit http://logback.qos.ch/codes.html#smtp_no_layout");
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public abstract boolean eventMarksEndOfLife(E e);

    /* access modifiers changed from: protected */
    public abstract void fillBuffer(ch.qos.logback.core.helpers.CyclicBuffer<E> cyclicBuffer, java.lang.StringBuffer stringBuffer);

    /* access modifiers changed from: 0000 */
    public javax.mail.internet.InternetAddress getAddress(java.lang.String str) {
        try {
            return new javax.mail.internet.InternetAddress(str);
        } catch (javax.mail.internet.AddressException e) {
            addError("Could not parse address [" + str + "].", e);
            return null;
        }
    }

    public java.lang.String getCharsetEncoding() {
        return this.charsetEncoding;
    }

    public ch.qos.logback.core.spi.CyclicBufferTracker<E> getCyclicBufferTracker() {
        return this.cbTracker;
    }

    public ch.qos.logback.core.sift.Discriminator<E> getDiscriminator() {
        return this.discriminator;
    }

    public java.lang.String getFrom() {
        return this.from;
    }

    public ch.qos.logback.core.Layout<E> getLayout() {
        return this.layout;
    }

    public java.lang.String getLocalhost() {
        return this.localhost;
    }

    public javax.mail.Message getMessage() {
        return this.mimeMsg;
    }

    public java.lang.String getPassword() {
        return this.password;
    }

    public java.lang.String getSMTPHost() {
        return getSmtpHost();
    }

    public int getSMTPPort() {
        return getSmtpPort();
    }

    public java.lang.String getSmtpHost() {
        return this.smtpHost;
    }

    public int getSmtpPort() {
        return this.smtpPort;
    }

    public java.lang.String getSubject() {
        return this.subjectStr;
    }

    public java.util.List<java.lang.String> getToAsListOfString() {
        java.util.ArrayList arrayList = new java.util.ArrayList();
        for (ch.qos.logback.core.pattern.PatternLayoutBase pattern : this.toPatternLayoutList) {
            arrayList.add(pattern.getPattern());
        }
        return arrayList;
    }

    public java.util.List<ch.qos.logback.core.pattern.PatternLayoutBase<E>> getToList() {
        return this.toPatternLayoutList;
    }

    public java.lang.String getUsername() {
        return this.username;
    }

    public boolean isAsynchronousSending() {
        return this.asynchronousSending;
    }

    public boolean isSSL() {
        return this.ssl;
    }

    public boolean isSTARTTLS() {
        return this.starttls;
    }

    /* access modifiers changed from: protected */
    public abstract ch.qos.logback.core.pattern.PatternLayoutBase<E> makeNewToPatternLayout(java.lang.String str);

    /* access modifiers changed from: protected */
    public abstract ch.qos.logback.core.Layout<E> makeSubjectLayout(java.lang.String str);

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0067 A[Catch:{ Exception -> 0x00d8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x006f A[Catch:{ Exception -> 0x00d8 }] */
    public void sendBuffer(ch.qos.logback.core.helpers.CyclicBuffer<E> cyclicBuffer, E e) {
        java.lang.String str;
        java.util.List parseAddress;
        try {
            javax.mail.internet.MimeBodyPart mimeBodyPart = new javax.mail.internet.MimeBodyPart();
            java.lang.StringBuffer stringBuffer = new java.lang.StringBuffer();
            java.lang.String fileHeader = this.layout.getFileHeader();
            if (fileHeader != null) {
                stringBuffer.append(fileHeader);
            }
            java.lang.String presentationHeader = this.layout.getPresentationHeader();
            if (presentationHeader != null) {
                stringBuffer.append(presentationHeader);
            }
            fillBuffer(cyclicBuffer, stringBuffer);
            java.lang.String presentationFooter = this.layout.getPresentationFooter();
            if (presentationFooter != null) {
                stringBuffer.append(presentationFooter);
            }
            java.lang.String fileFooter = this.layout.getFileFooter();
            if (fileFooter != null) {
                stringBuffer.append(fileFooter);
            }
            java.lang.String str2 = "Undefined subject";
            if (this.subjectLayout != null) {
                str2 = this.subjectLayout.doLayout(e);
                int i = str2 != null ? str2.indexOf(10) : -1;
                if (i > -1) {
                    str = str2.substring(0, i);
                    this.mimeMsg.setSubject(str, this.charsetEncoding);
                    parseAddress = parseAddress(e);
                    if (!parseAddress.isEmpty()) {
                        addInfo("Empty destination address. Aborting email transmission");
                        return;
                    }
                    javax.mail.internet.InternetAddress[] internetAddressArr = (javax.mail.internet.InternetAddress[]) parseAddress.toArray(EMPTY_IA_ARRAY);
                    this.mimeMsg.setRecipients(javax.mail.Message.RecipientType.TO, internetAddressArr);
                    java.lang.String contentType = this.layout.getContentType();
                    if (ch.qos.logback.core.util.ContentTypeUtil.isTextual(contentType)) {
                        mimeBodyPart.setText(stringBuffer.toString(), this.charsetEncoding, ch.qos.logback.core.util.ContentTypeUtil.getSubType(contentType));
                    } else {
                        mimeBodyPart.setContent(stringBuffer.toString(), this.layout.getContentType());
                    }
                    javax.mail.internet.MimeMultipart mimeMultipart = new javax.mail.internet.MimeMultipart();
                    mimeMultipart.addBodyPart(mimeBodyPart);
                    this.mimeMsg.setContent(mimeMultipart);
                    this.mimeMsg.setSentDate(new java.util.Date());
                    addInfo("About to send out SMTP message \"" + str + "\" to " + java.util.Arrays.toString(internetAddressArr));
                    javax.mail.Transport.send(this.mimeMsg);
                    return;
                }
            }
            str = str2;
            this.mimeMsg.setSubject(str, this.charsetEncoding);
            parseAddress = parseAddress(e);
            if (!parseAddress.isEmpty()) {
            }
        } catch (java.lang.Exception e2) {
            addError("Error occurred while sending e-mail notification.", e2);
        }
    }

    public void setAsynchronousSending(boolean z) {
        this.asynchronousSending = z;
    }

    public void setCharsetEncoding(java.lang.String str) {
        this.charsetEncoding = str;
    }

    public void setCyclicBufferTracker(ch.qos.logback.core.spi.CyclicBufferTracker<E> cyclicBufferTracker) {
        this.cbTracker = cyclicBufferTracker;
    }

    public void setDiscriminator(ch.qos.logback.core.sift.Discriminator<E> discriminator2) {
        this.discriminator = discriminator2;
    }

    public void setEvaluator(ch.qos.logback.core.boolex.EventEvaluator<E> eventEvaluator2) {
        this.eventEvaluator = eventEvaluator2;
    }

    public void setFrom(java.lang.String str) {
        this.from = str;
    }

    public void setLayout(ch.qos.logback.core.Layout<E> layout2) {
        this.layout = layout2;
    }

    public void setLocalhost(java.lang.String str) {
        this.localhost = str;
    }

    public void setMessage(javax.mail.internet.MimeMessage mimeMessage) {
        this.mimeMsg = mimeMessage;
    }

    public void setPassword(java.lang.String str) {
        this.password = str;
    }

    public void setSMTPHost(java.lang.String str) {
        setSmtpHost(str);
    }

    public void setSMTPPort(int i) {
        setSmtpPort(i);
    }

    public void setSSL(boolean z) {
        this.ssl = z;
    }

    public void setSTARTTLS(boolean z) {
        this.starttls = z;
    }

    public void setSmtpHost(java.lang.String str) {
        this.smtpHost = str;
    }

    public void setSmtpPort(int i) {
        this.smtpPort = i;
    }

    public void setSubject(java.lang.String str) {
        this.subjectStr = str;
    }

    public void setUsername(java.lang.String str) {
        this.username = str;
    }

    public void start() {
        if (this.cbTracker == null) {
            this.cbTracker = new ch.qos.logback.core.spi.CyclicBufferTracker<>();
        }
        javax.mail.Session buildSessionFromProperties = buildSessionFromProperties();
        if (buildSessionFromProperties == null) {
            addError("Failed to obtain javax.mail.Session. Cannot start.");
            return;
        }
        this.mimeMsg = new javax.mail.internet.MimeMessage(buildSessionFromProperties);
        try {
            if (this.from != null) {
                this.mimeMsg.setFrom(getAddress(this.from));
            } else {
                this.mimeMsg.setFrom();
            }
            this.subjectLayout = makeSubjectLayout(this.subjectStr);
            this.started = true;
        } catch (javax.mail.MessagingException e) {
            addError("Could not activate SMTPAppender options.", e);
        }
    }

    public synchronized void stop() {
        this.started = false;
    }

    /* access modifiers changed from: protected */
    public abstract void subAppend(ch.qos.logback.core.helpers.CyclicBuffer<E> cyclicBuffer, E e);
}
