package ch.qos.logback.core.net.ssl;

public interface SSLConfigurable {
    java.lang.String[] getDefaultCipherSuites();

    java.lang.String[] getDefaultProtocols();

    java.lang.String[] getSupportedCipherSuites();

    java.lang.String[] getSupportedProtocols();

    void setEnabledCipherSuites(java.lang.String[] strArr);

    void setEnabledProtocols(java.lang.String[] strArr);

    void setNeedClientAuth(boolean z);

    void setWantClientAuth(boolean z);
}
