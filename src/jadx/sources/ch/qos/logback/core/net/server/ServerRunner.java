package ch.qos.logback.core.net.server;

public interface ServerRunner<T extends ch.qos.logback.core.net.server.Client> extends ch.qos.logback.core.spi.ContextAware, java.lang.Runnable {
    void accept(ch.qos.logback.core.net.server.ClientVisitor<T> clientVisitor);

    boolean isRunning();

    void stop() throws java.io.IOException;
}
