package ch.qos.logback.core.net.ssl;

public class SSLConfigurableServerSocket implements ch.qos.logback.core.net.ssl.SSLConfigurable {
    private final javax.net.ssl.SSLServerSocket delegate;

    public SSLConfigurableServerSocket(javax.net.ssl.SSLServerSocket sSLServerSocket) {
        this.delegate = sSLServerSocket;
    }

    public java.lang.String[] getDefaultCipherSuites() {
        return this.delegate.getEnabledCipherSuites();
    }

    public java.lang.String[] getDefaultProtocols() {
        return this.delegate.getEnabledProtocols();
    }

    public java.lang.String[] getSupportedCipherSuites() {
        return this.delegate.getSupportedCipherSuites();
    }

    public java.lang.String[] getSupportedProtocols() {
        return this.delegate.getSupportedProtocols();
    }

    public void setEnabledCipherSuites(java.lang.String[] strArr) {
        this.delegate.setEnabledCipherSuites(strArr);
    }

    public void setEnabledProtocols(java.lang.String[] strArr) {
        this.delegate.setEnabledProtocols(strArr);
    }

    public void setNeedClientAuth(boolean z) {
        this.delegate.setNeedClientAuth(z);
    }

    public void setWantClientAuth(boolean z) {
        this.delegate.setWantClientAuth(z);
    }
}
