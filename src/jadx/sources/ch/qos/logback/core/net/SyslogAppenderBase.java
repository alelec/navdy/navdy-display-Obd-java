package ch.qos.logback.core.net;

public abstract class SyslogAppenderBase<E> extends ch.qos.logback.core.AppenderBase<E> {
    static final int MAX_MESSAGE_SIZE_LIMIT = 65000;
    static final java.lang.String SYSLOG_LAYOUT_URL = "http://logback.qos.ch/codes.html#syslog_layout";
    java.lang.String facilityStr;
    boolean initialized = false;
    ch.qos.logback.core.Layout<E> layout;
    private boolean lazyInit = false;
    int maxMessageSize;
    int port = ch.qos.logback.core.net.SyslogConstants.SYSLOG_PORT;
    protected ch.qos.logback.core.net.SyslogOutputStream sos;
    protected java.lang.String suffixPattern;
    java.lang.String syslogHost;

    private boolean connect() {
        try {
            this.sos = new ch.qos.logback.core.net.SyslogOutputStream(this.syslogHost, this.port);
            int sendBufferSize = this.sos.getSendBufferSize();
            if (this.maxMessageSize == 0) {
                this.maxMessageSize = java.lang.Math.min(sendBufferSize, MAX_MESSAGE_SIZE_LIMIT);
                addInfo("Defaulting maxMessageSize to [" + this.maxMessageSize + "]");
            } else if (this.maxMessageSize > sendBufferSize) {
                addWarn("maxMessageSize of [" + this.maxMessageSize + "] is larger than the system defined datagram size of [" + sendBufferSize + "].");
                addWarn("This may result in dropped logs.");
            }
        } catch (java.net.UnknownHostException e) {
            addError("Could not create SyslogWriter", e);
        } catch (java.net.SocketException e2) {
            addWarn("Failed to bind to a random datagram socket. Will try to reconnect later.", e2);
        }
        return this.sos != null;
    }

    public static int facilityStringToint(java.lang.String str) {
        if ("KERN".equalsIgnoreCase(str)) {
            return 0;
        }
        if ("USER".equalsIgnoreCase(str)) {
            return 8;
        }
        if ("MAIL".equalsIgnoreCase(str)) {
            return 16;
        }
        if ("DAEMON".equalsIgnoreCase(str)) {
            return 24;
        }
        if ("AUTH".equalsIgnoreCase(str)) {
            return 32;
        }
        if ("SYSLOG".equalsIgnoreCase(str)) {
            return 40;
        }
        if ("LPR".equalsIgnoreCase(str)) {
            return 48;
        }
        if ("NEWS".equalsIgnoreCase(str)) {
            return 56;
        }
        if ("UUCP".equalsIgnoreCase(str)) {
            return 64;
        }
        if ("CRON".equalsIgnoreCase(str)) {
            return 72;
        }
        if ("AUTHPRIV".equalsIgnoreCase(str)) {
            return 80;
        }
        if ("FTP".equalsIgnoreCase(str)) {
            return 88;
        }
        if ("NTP".equalsIgnoreCase(str)) {
            return 96;
        }
        if ("AUDIT".equalsIgnoreCase(str)) {
            return ch.qos.logback.core.net.SyslogConstants.LOG_AUDIT;
        }
        if ("ALERT".equalsIgnoreCase(str)) {
            return ch.qos.logback.core.net.SyslogConstants.LOG_ALERT;
        }
        if ("CLOCK".equalsIgnoreCase(str)) {
            return ch.qos.logback.core.net.SyslogConstants.LOG_CLOCK;
        }
        if ("LOCAL0".equalsIgnoreCase(str)) {
            return 128;
        }
        if ("LOCAL1".equalsIgnoreCase(str)) {
            return ch.qos.logback.core.net.SyslogConstants.LOG_LOCAL1;
        }
        if ("LOCAL2".equalsIgnoreCase(str)) {
            return ch.qos.logback.core.net.SyslogConstants.LOG_LOCAL2;
        }
        if ("LOCAL3".equalsIgnoreCase(str)) {
            return ch.qos.logback.core.net.SyslogConstants.LOG_LOCAL3;
        }
        if ("LOCAL4".equalsIgnoreCase(str)) {
            return ch.qos.logback.core.net.SyslogConstants.LOG_LOCAL4;
        }
        if ("LOCAL5".equalsIgnoreCase(str)) {
            return ch.qos.logback.core.net.SyslogConstants.LOG_LOCAL5;
        }
        if ("LOCAL6".equalsIgnoreCase(str)) {
            return ch.qos.logback.core.net.SyslogConstants.LOG_LOCAL6;
        }
        if ("LOCAL7".equalsIgnoreCase(str)) {
            return ch.qos.logback.core.net.SyslogConstants.LOG_LOCAL7;
        }
        throw new java.lang.IllegalArgumentException(str + " is not a valid syslog facility string");
    }

    /* access modifiers changed from: protected */
    public void append(E e) {
        if (isStarted()) {
            if (!this.initialized && this.lazyInit) {
                this.initialized = true;
                connect();
            }
            if (this.sos != null) {
                try {
                    java.lang.String doLayout = this.layout.doLayout(e);
                    if (doLayout != null) {
                        if (doLayout.length() > this.maxMessageSize) {
                            doLayout = doLayout.substring(0, this.maxMessageSize);
                        }
                        this.sos.write(doLayout.getBytes());
                        this.sos.flush();
                        postProcess(e, this.sos);
                    }
                } catch (java.io.IOException e2) {
                    addError("Failed to send diagram to " + this.syslogHost, e2);
                }
            }
        }
    }

    public abstract ch.qos.logback.core.Layout<E> buildLayout();

    public java.lang.String getFacility() {
        return this.facilityStr;
    }

    public ch.qos.logback.core.Layout<E> getLayout() {
        return this.layout;
    }

    public boolean getLazy() {
        return this.lazyInit;
    }

    public int getMaxMessageSize() {
        return this.maxMessageSize;
    }

    public int getPort() {
        return this.port;
    }

    public abstract int getSeverityForEvent(java.lang.Object obj);

    public java.lang.String getSuffixPattern() {
        return this.suffixPattern;
    }

    public java.lang.String getSyslogHost() {
        return this.syslogHost;
    }

    /* access modifiers changed from: protected */
    public void postProcess(java.lang.Object obj, java.io.OutputStream outputStream) {
    }

    public void setFacility(java.lang.String str) {
        if (str != null) {
            str = str.trim();
        }
        this.facilityStr = str;
    }

    public void setLayout(ch.qos.logback.core.Layout<E> layout2) {
        addWarn("The layout of a SyslogAppender cannot be set directly. See also http://logback.qos.ch/codes.html#syslog_layout");
    }

    public void setLazy(boolean z) {
        this.lazyInit = z;
    }

    public void setMaxMessageSize(int i) {
        this.maxMessageSize = i;
    }

    public void setPort(int i) {
        this.port = i;
    }

    public void setSuffixPattern(java.lang.String str) {
        this.suffixPattern = str;
    }

    public void setSyslogHost(java.lang.String str) {
        this.syslogHost = str;
    }

    public void start() {
        int i = 0;
        if (this.facilityStr == null) {
            addError("The Facility option is mandatory");
            i = 1;
        }
        if (!this.lazyInit && !connect()) {
            i++;
        }
        if (this.layout == null) {
            this.layout = buildLayout();
        }
        if (i == 0) {
            super.start();
        }
    }

    public void stop() {
        this.sos.close();
        super.stop();
    }
}
