package ch.qos.logback.core.net.ssl;

public class KeyManagerFactoryFactoryBean {
    private java.lang.String algorithm;
    private java.lang.String provider;

    public javax.net.ssl.KeyManagerFactory createKeyManagerFactory() throws java.security.NoSuchProviderException, java.security.NoSuchAlgorithmException {
        return getProvider() != null ? javax.net.ssl.KeyManagerFactory.getInstance(getAlgorithm(), getProvider()) : javax.net.ssl.KeyManagerFactory.getInstance(getAlgorithm());
    }

    public java.lang.String getAlgorithm() {
        return this.algorithm == null ? javax.net.ssl.KeyManagerFactory.getDefaultAlgorithm() : this.algorithm;
    }

    public java.lang.String getProvider() {
        return this.provider;
    }

    public void setAlgorithm(java.lang.String str) {
        this.algorithm = str;
    }

    public void setProvider(java.lang.String str) {
        this.provider = str;
    }
}
