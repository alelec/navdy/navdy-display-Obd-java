package ch.qos.logback.core.net.ssl;

public class SecureRandomFactoryBean {
    private java.lang.String algorithm;
    private java.lang.String provider;

    public java.security.SecureRandom createSecureRandom() throws java.security.NoSuchProviderException, java.security.NoSuchAlgorithmException {
        try {
            return getProvider() != null ? java.security.SecureRandom.getInstance(getAlgorithm(), getProvider()) : java.security.SecureRandom.getInstance(getAlgorithm());
        } catch (java.security.NoSuchProviderException e) {
            throw new java.security.NoSuchProviderException("no such secure random provider: " + getProvider());
        } catch (java.security.NoSuchAlgorithmException e2) {
            throw new java.security.NoSuchAlgorithmException("no such secure random algorithm: " + getAlgorithm());
        }
    }

    public java.lang.String getAlgorithm() {
        return this.algorithm == null ? ch.qos.logback.core.net.ssl.SSL.DEFAULT_SECURE_RANDOM_ALGORITHM : this.algorithm;
    }

    public java.lang.String getProvider() {
        return this.provider;
    }

    public void setAlgorithm(java.lang.String str) {
        this.algorithm = str;
    }

    public void setProvider(java.lang.String str) {
        this.provider = str;
    }
}
