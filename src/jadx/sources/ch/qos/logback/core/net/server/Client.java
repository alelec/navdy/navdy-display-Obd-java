package ch.qos.logback.core.net.server;

public interface Client extends java.lang.Runnable, java.io.Closeable {
    void close();
}
