package ch.qos.logback.core.net.server;

public abstract class AbstractServerSocketAppender<E> extends ch.qos.logback.core.AppenderBase<E> {
    public static final int DEFAULT_BACKLOG = 50;
    public static final int DEFAULT_CLIENT_QUEUE_SIZE = 100;
    private java.lang.String address;
    private int backlog = 50;
    private int clientQueueSize = 100;
    private int port = ch.qos.logback.core.net.AbstractSocketAppender.DEFAULT_PORT;
    private ch.qos.logback.core.net.server.ServerRunner<ch.qos.logback.core.net.server.RemoteReceiverClient> runner;

    /* access modifiers changed from: protected */
    public void append(E e) {
        if (e != null) {
            postProcessEvent(e);
            final java.io.Serializable transform = getPST().transform(e);
            this.runner.accept(new ch.qos.logback.core.net.server.ClientVisitor<ch.qos.logback.core.net.server.RemoteReceiverClient>() {
                public void visit(ch.qos.logback.core.net.server.RemoteReceiverClient remoteReceiverClient) {
                    remoteReceiverClient.offer(transform);
                }
            });
        }
    }

    /* access modifiers changed from: protected */
    public ch.qos.logback.core.net.server.ServerListener<ch.qos.logback.core.net.server.RemoteReceiverClient> createServerListener(java.net.ServerSocket serverSocket) {
        return new ch.qos.logback.core.net.server.RemoteReceiverServerListener(serverSocket);
    }

    /* access modifiers changed from: protected */
    public ch.qos.logback.core.net.server.ServerRunner<ch.qos.logback.core.net.server.RemoteReceiverClient> createServerRunner(ch.qos.logback.core.net.server.ServerListener<ch.qos.logback.core.net.server.RemoteReceiverClient> serverListener, java.util.concurrent.Executor executor) {
        return new ch.qos.logback.core.net.server.RemoteReceiverServerRunner(serverListener, executor, getClientQueueSize());
    }

    public java.lang.String getAddress() {
        return this.address;
    }

    public int getBacklog() {
        return this.backlog;
    }

    public int getClientQueueSize() {
        return this.clientQueueSize;
    }

    /* access modifiers changed from: protected */
    public java.net.InetAddress getInetAddress() throws java.net.UnknownHostException {
        if (getAddress() == null) {
            return null;
        }
        return java.net.InetAddress.getByName(getAddress());
    }

    /* access modifiers changed from: protected */
    public abstract ch.qos.logback.core.spi.PreSerializationTransformer<E> getPST();

    public int getPort() {
        return this.port;
    }

    /* access modifiers changed from: protected */
    public javax.net.ServerSocketFactory getServerSocketFactory() throws java.lang.Exception {
        return javax.net.ServerSocketFactory.getDefault();
    }

    /* access modifiers changed from: protected */
    public abstract void postProcessEvent(E e);

    public void setAddress(java.lang.String str) {
        this.address = str;
    }

    public void setBacklog(int i) {
        this.backlog = i;
    }

    public void setClientQueueSize(int i) {
        this.clientQueueSize = i;
    }

    public void setPort(int i) {
        this.port = i;
    }

    public void start() {
        if (!isStarted()) {
            try {
                this.runner = createServerRunner(createServerListener(getServerSocketFactory().createServerSocket(getPort(), getBacklog(), getInetAddress())), getContext().getExecutorService());
                this.runner.setContext(getContext());
                getContext().getExecutorService().execute(this.runner);
                super.start();
            } catch (java.lang.Exception e) {
                addError("server startup error: " + e, e);
            }
        }
    }

    public void stop() {
        if (isStarted()) {
            try {
                this.runner.stop();
                super.stop();
            } catch (java.io.IOException e) {
                addError("server shutdown error: " + e, e);
            }
        }
    }
}
