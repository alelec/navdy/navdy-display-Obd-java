package ch.qos.logback.core.net;

public class SocketConnectorBase implements ch.qos.logback.core.net.SocketConnector {
    private final java.net.InetAddress address;
    private final java.util.concurrent.locks.Condition connectCondition;
    private ch.qos.logback.core.net.SocketConnectorBase.DelayStrategy delayStrategy;
    private ch.qos.logback.core.net.SocketConnector.ExceptionHandler exceptionHandler;
    private final java.util.concurrent.locks.Lock lock;
    private final int port;
    private java.net.Socket socket;
    private javax.net.SocketFactory socketFactory;

    private static class ConsoleExceptionHandler implements ch.qos.logback.core.net.SocketConnector.ExceptionHandler {
        private ConsoleExceptionHandler() {
        }

        public void connectionFailed(ch.qos.logback.core.net.SocketConnector socketConnector, java.lang.Exception exc) {
            java.lang.System.out.println(exc);
        }
    }

    public interface DelayStrategy {
        int nextDelay();
    }

    private static class FixedDelay implements ch.qos.logback.core.net.SocketConnectorBase.DelayStrategy {
        private int nextDelay;
        private final int retryDelay;

        public FixedDelay(int i, int i2) {
            this.nextDelay = i;
            this.retryDelay = i2;
        }

        public int nextDelay() {
            int i = this.nextDelay;
            this.nextDelay = this.retryDelay;
            return i;
        }
    }

    public SocketConnectorBase(java.net.InetAddress inetAddress, int i, int i2, int i3) {
        this(inetAddress, i, new ch.qos.logback.core.net.SocketConnectorBase.FixedDelay(i2, i3));
    }

    public SocketConnectorBase(java.net.InetAddress inetAddress, int i, ch.qos.logback.core.net.SocketConnectorBase.DelayStrategy delayStrategy2) {
        this.lock = new java.util.concurrent.locks.ReentrantLock();
        this.connectCondition = this.lock.newCondition();
        this.address = inetAddress;
        this.port = i;
        this.delayStrategy = delayStrategy2;
    }

    private void signalConnected() {
        this.lock.lock();
        try {
            this.connectCondition.signalAll();
        } finally {
            this.lock.unlock();
        }
    }

    public java.net.Socket awaitConnection() throws java.lang.InterruptedException {
        return awaitConnection(kotlin.jvm.internal.LongCompanionObject.MAX_VALUE);
    }

    public java.net.Socket awaitConnection(long j) throws java.lang.InterruptedException {
        this.lock.lock();
        boolean z = false;
        while (this.socket == null && !z) {
            try {
                z = !this.connectCondition.await(j, java.util.concurrent.TimeUnit.MILLISECONDS);
            } finally {
                this.lock.unlock();
            }
        }
        return this.socket;
    }

    public java.net.Socket call() throws java.lang.InterruptedException {
        return null;
    }

    public void run() {
        if (this.socket != null) {
            throw new java.lang.IllegalStateException("connector cannot be reused");
        }
        if (this.exceptionHandler == null) {
            this.exceptionHandler = new ch.qos.logback.core.net.SocketConnectorBase.ConsoleExceptionHandler();
        }
        if (this.socketFactory == null) {
            this.socketFactory = javax.net.SocketFactory.getDefault();
        }
        while (!java.lang.Thread.currentThread().isInterrupted()) {
            try {
                java.lang.Thread.sleep((long) this.delayStrategy.nextDelay());
                try {
                    this.socket = this.socketFactory.createSocket(this.address, this.port);
                    signalConnected();
                    return;
                } catch (java.lang.Exception e) {
                    this.exceptionHandler.connectionFailed(this, e);
                }
            } catch (java.lang.InterruptedException e2) {
                this.exceptionHandler.connectionFailed(this, e2);
                return;
            }
        }
    }

    public void setExceptionHandler(ch.qos.logback.core.net.SocketConnector.ExceptionHandler exceptionHandler2) {
        this.exceptionHandler = exceptionHandler2;
    }

    public void setSocketFactory(javax.net.SocketFactory socketFactory2) {
        this.socketFactory = socketFactory2;
    }
}
