package ch.qos.logback.core.net;

public class SyslogOutputStream extends java.io.OutputStream {
    private static final int MAX_LEN = 1024;
    private java.net.InetAddress address;
    private java.io.ByteArrayOutputStream baos = new java.io.ByteArrayOutputStream();
    private java.net.DatagramSocket ds;
    private final int port;

    public SyslogOutputStream(java.lang.String str, int i) throws java.net.UnknownHostException, java.net.SocketException {
        this.address = java.net.InetAddress.getByName(str);
        this.port = i;
        this.ds = new java.net.DatagramSocket();
    }

    public void close() {
        this.address = null;
        this.ds = null;
    }

    public void flush() throws java.io.IOException {
        byte[] byteArray = this.baos.toByteArray();
        java.net.DatagramPacket datagramPacket = new java.net.DatagramPacket(byteArray, byteArray.length, this.address, this.port);
        if (this.baos.size() > 1024) {
            this.baos = new java.io.ByteArrayOutputStream();
        } else {
            this.baos.reset();
        }
        if (byteArray.length != 0 && this.ds != null) {
            this.ds.send(datagramPacket);
        }
    }

    public int getPort() {
        return this.port;
    }

    /* access modifiers changed from: 0000 */
    public int getSendBufferSize() throws java.net.SocketException {
        return this.ds.getSendBufferSize();
    }

    public void write(int i) throws java.io.IOException {
        this.baos.write(i);
    }

    public void write(byte[] bArr, int i, int i2) throws java.io.IOException {
        this.baos.write(bArr, i, i2);
    }
}
