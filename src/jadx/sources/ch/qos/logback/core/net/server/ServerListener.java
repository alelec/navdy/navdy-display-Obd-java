package ch.qos.logback.core.net.server;

public interface ServerListener<T extends ch.qos.logback.core.net.server.Client> extends java.io.Closeable {
    T acceptClient() throws java.io.IOException, java.lang.InterruptedException;

    void close();
}
