package ch.qos.logback.core.net.ssl;

public class SSLConfiguration extends ch.qos.logback.core.net.ssl.SSLContextFactoryBean {
    private ch.qos.logback.core.net.ssl.SSLParametersConfiguration parameters;

    public ch.qos.logback.core.net.ssl.SSLParametersConfiguration getParameters() {
        if (this.parameters == null) {
            this.parameters = new ch.qos.logback.core.net.ssl.SSLParametersConfiguration();
        }
        return this.parameters;
    }

    public void setParameters(ch.qos.logback.core.net.ssl.SSLParametersConfiguration sSLParametersConfiguration) {
        this.parameters = sSLParametersConfiguration;
    }
}
