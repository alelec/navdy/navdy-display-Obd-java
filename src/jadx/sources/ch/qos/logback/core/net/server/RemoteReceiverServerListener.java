package ch.qos.logback.core.net.server;

class RemoteReceiverServerListener extends ch.qos.logback.core.net.server.ServerSocketListener<ch.qos.logback.core.net.server.RemoteReceiverClient> {
    public RemoteReceiverServerListener(java.net.ServerSocket serverSocket) {
        super(serverSocket);
    }

    /* access modifiers changed from: protected */
    public ch.qos.logback.core.net.server.RemoteReceiverClient createClient(java.lang.String str, java.net.Socket socket) throws java.io.IOException {
        return new ch.qos.logback.core.net.server.RemoteReceiverStreamClient(str, socket);
    }
}
