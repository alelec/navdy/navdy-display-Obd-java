package ch.qos.logback.core.net.server;

public abstract class ConcurrentServerRunner<T extends ch.qos.logback.core.net.server.Client> extends ch.qos.logback.core.spi.ContextAwareBase implements java.lang.Runnable, ch.qos.logback.core.net.server.ServerRunner<T> {
    private final java.util.Collection<T> clients = new java.util.ArrayList();
    private final java.util.concurrent.locks.Lock clientsLock = new java.util.concurrent.locks.ReentrantLock();
    private final java.util.concurrent.Executor executor;
    private final ch.qos.logback.core.net.server.ServerListener<T> listener;
    private boolean running;

    private class ClientWrapper implements ch.qos.logback.core.net.server.Client {
        private final T delegate;

        public ClientWrapper(T t) {
            this.delegate = t;
        }

        public void close() {
            this.delegate.close();
        }

        public void run() {
            ch.qos.logback.core.net.server.ConcurrentServerRunner.this.addClient(this.delegate);
            try {
                this.delegate.run();
            } finally {
                ch.qos.logback.core.net.server.ConcurrentServerRunner.this.removeClient(this.delegate);
            }
        }
    }

    public ConcurrentServerRunner(ch.qos.logback.core.net.server.ServerListener<T> serverListener, java.util.concurrent.Executor executor2) {
        this.listener = serverListener;
        this.executor = executor2;
    }

    /* access modifiers changed from: private */
    public void addClient(T t) {
        this.clientsLock.lock();
        try {
            this.clients.add(t);
        } finally {
            this.clientsLock.unlock();
        }
    }

    private java.util.Collection<T> copyClients() {
        this.clientsLock.lock();
        try {
            return new java.util.ArrayList(this.clients);
        } finally {
            this.clientsLock.unlock();
        }
    }

    /* access modifiers changed from: private */
    public void removeClient(T t) {
        this.clientsLock.lock();
        try {
            this.clients.remove(t);
        } finally {
            this.clientsLock.unlock();
        }
    }

    public void accept(ch.qos.logback.core.net.server.ClientVisitor<T> clientVisitor) {
        for (ch.qos.logback.core.net.server.Client client : copyClients()) {
            try {
                clientVisitor.visit(client);
            } catch (java.lang.RuntimeException e) {
                addError(client + ": " + e);
            }
        }
    }

    /* access modifiers changed from: protected */
    public abstract boolean configureClient(T t);

    public boolean isRunning() {
        return this.running;
    }

    public void run() {
        setRunning(true);
        try {
            addInfo("listening on " + this.listener);
            while (!java.lang.Thread.currentThread().isInterrupted()) {
                ch.qos.logback.core.net.server.Client acceptClient = this.listener.acceptClient();
                if (!configureClient(acceptClient)) {
                    addError(acceptClient + ": connection dropped");
                    acceptClient.close();
                } else {
                    try {
                        this.executor.execute(new ch.qos.logback.core.net.server.ConcurrentServerRunner.ClientWrapper(acceptClient));
                    } catch (java.util.concurrent.RejectedExecutionException e) {
                        addError(acceptClient + ": connection dropped");
                        acceptClient.close();
                    }
                }
            }
        } catch (java.lang.InterruptedException e2) {
        } catch (java.lang.Exception e3) {
            addError("listener: " + e3);
        }
        setRunning(false);
        addInfo("shutting down");
        this.listener.close();
    }

    /* access modifiers changed from: protected */
    public void setRunning(boolean z) {
        this.running = z;
    }

    public void stop() throws java.io.IOException {
        this.listener.close();
        accept(new ch.qos.logback.core.net.server.ClientVisitor<T>() {
            public void visit(T t) {
                t.close();
            }
        });
    }
}
