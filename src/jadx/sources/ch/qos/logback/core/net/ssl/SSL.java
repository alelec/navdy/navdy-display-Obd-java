package ch.qos.logback.core.net.ssl;

public interface SSL {
    public static final java.lang.String DEFAULT_KEYSTORE_PASSWORD = "changeit";
    public static final java.lang.String DEFAULT_KEYSTORE_TYPE = "JKS";
    public static final java.lang.String DEFAULT_PROTOCOL = "SSL";
    public static final java.lang.String DEFAULT_SECURE_RANDOM_ALGORITHM = "SHA1PRNG";
}
