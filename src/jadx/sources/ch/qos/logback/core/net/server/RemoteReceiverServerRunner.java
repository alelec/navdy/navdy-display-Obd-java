package ch.qos.logback.core.net.server;

class RemoteReceiverServerRunner extends ch.qos.logback.core.net.server.ConcurrentServerRunner<ch.qos.logback.core.net.server.RemoteReceiverClient> {
    private final int clientQueueSize;

    public RemoteReceiverServerRunner(ch.qos.logback.core.net.server.ServerListener<ch.qos.logback.core.net.server.RemoteReceiverClient> serverListener, java.util.concurrent.Executor executor, int i) {
        super(serverListener, executor);
        this.clientQueueSize = i;
    }

    /* access modifiers changed from: protected */
    public boolean configureClient(ch.qos.logback.core.net.server.RemoteReceiverClient remoteReceiverClient) {
        remoteReceiverClient.setContext(getContext());
        remoteReceiverClient.setQueue(new java.util.concurrent.ArrayBlockingQueue(this.clientQueueSize));
        return true;
    }
}
