package ch.qos.logback.core.net.ssl;

public class SSLContextFactoryBean {
    private static final java.lang.String JSSE_KEY_STORE_PROPERTY = "javax.net.ssl.keyStore";
    private static final java.lang.String JSSE_TRUST_STORE_PROPERTY = "javax.net.ssl.trustStore";
    private ch.qos.logback.core.net.ssl.KeyManagerFactoryFactoryBean keyManagerFactory;
    private ch.qos.logback.core.net.ssl.KeyStoreFactoryBean keyStore;
    private java.lang.String protocol;
    private java.lang.String provider;
    private ch.qos.logback.core.net.ssl.SecureRandomFactoryBean secureRandom;
    private ch.qos.logback.core.net.ssl.TrustManagerFactoryFactoryBean trustManagerFactory;
    private ch.qos.logback.core.net.ssl.KeyStoreFactoryBean trustStore;

    private javax.net.ssl.KeyManager[] createKeyManagers(ch.qos.logback.core.spi.ContextAware contextAware) throws java.security.NoSuchProviderException, java.security.NoSuchAlgorithmException, java.security.UnrecoverableKeyException, java.security.KeyStoreException {
        if (getKeyStore() == null) {
            return null;
        }
        java.security.KeyStore createKeyStore = getKeyStore().createKeyStore();
        contextAware.addInfo("key store of type '" + createKeyStore.getType() + "' provider '" + createKeyStore.getProvider() + "': " + getKeyStore().getLocation());
        javax.net.ssl.KeyManagerFactory createKeyManagerFactory = getKeyManagerFactory().createKeyManagerFactory();
        contextAware.addInfo("key manager algorithm '" + createKeyManagerFactory.getAlgorithm() + "' provider '" + createKeyManagerFactory.getProvider() + "'");
        createKeyManagerFactory.init(createKeyStore, getKeyStore().getPassword().toCharArray());
        return createKeyManagerFactory.getKeyManagers();
    }

    private java.security.SecureRandom createSecureRandom(ch.qos.logback.core.spi.ContextAware contextAware) throws java.security.NoSuchProviderException, java.security.NoSuchAlgorithmException {
        java.security.SecureRandom createSecureRandom = getSecureRandom().createSecureRandom();
        contextAware.addInfo("secure random algorithm '" + createSecureRandom.getAlgorithm() + "' provider '" + createSecureRandom.getProvider() + "'");
        return createSecureRandom;
    }

    private javax.net.ssl.TrustManager[] createTrustManagers(ch.qos.logback.core.spi.ContextAware contextAware) throws java.security.NoSuchProviderException, java.security.NoSuchAlgorithmException, java.security.KeyStoreException {
        if (getTrustStore() == null) {
            return null;
        }
        java.security.KeyStore createKeyStore = getTrustStore().createKeyStore();
        contextAware.addInfo("trust store of type '" + createKeyStore.getType() + "' provider '" + createKeyStore.getProvider() + "': " + getTrustStore().getLocation());
        javax.net.ssl.TrustManagerFactory createTrustManagerFactory = getTrustManagerFactory().createTrustManagerFactory();
        contextAware.addInfo("trust manager algorithm '" + createTrustManagerFactory.getAlgorithm() + "' provider '" + createTrustManagerFactory.getProvider() + "'");
        createTrustManagerFactory.init(createKeyStore);
        return createTrustManagerFactory.getTrustManagers();
    }

    private ch.qos.logback.core.net.ssl.KeyStoreFactoryBean keyStoreFromSystemProperties(java.lang.String str) {
        if (java.lang.System.getProperty(str) == null) {
            return null;
        }
        ch.qos.logback.core.net.ssl.KeyStoreFactoryBean keyStoreFactoryBean = new ch.qos.logback.core.net.ssl.KeyStoreFactoryBean();
        keyStoreFactoryBean.setLocation(locationFromSystemProperty(str));
        keyStoreFactoryBean.setProvider(java.lang.System.getProperty(str + "Provider"));
        keyStoreFactoryBean.setPassword(java.lang.System.getProperty(str + "Password"));
        keyStoreFactoryBean.setType(java.lang.System.getProperty(str + "Type"));
        return keyStoreFactoryBean;
    }

    private java.lang.String locationFromSystemProperty(java.lang.String str) {
        java.lang.String property = java.lang.System.getProperty(str);
        return (property == null || property.startsWith("file:")) ? property : "file:" + property;
    }

    public javax.net.ssl.SSLContext createContext(ch.qos.logback.core.spi.ContextAware contextAware) throws java.security.NoSuchProviderException, java.security.NoSuchAlgorithmException, java.security.KeyManagementException, java.security.UnrecoverableKeyException, java.security.KeyStoreException, java.security.cert.CertificateException {
        javax.net.ssl.SSLContext instance = getProvider() != null ? javax.net.ssl.SSLContext.getInstance(getProtocol(), getProvider()) : javax.net.ssl.SSLContext.getInstance(getProtocol());
        contextAware.addInfo("SSL protocol '" + instance.getProtocol() + "' provider '" + instance.getProvider() + "'");
        instance.init(createKeyManagers(contextAware), createTrustManagers(contextAware), createSecureRandom(contextAware));
        return instance;
    }

    public ch.qos.logback.core.net.ssl.KeyManagerFactoryFactoryBean getKeyManagerFactory() {
        return this.keyManagerFactory == null ? new ch.qos.logback.core.net.ssl.KeyManagerFactoryFactoryBean() : this.keyManagerFactory;
    }

    public ch.qos.logback.core.net.ssl.KeyStoreFactoryBean getKeyStore() {
        if (this.keyStore == null) {
            this.keyStore = keyStoreFromSystemProperties(JSSE_KEY_STORE_PROPERTY);
        }
        return this.keyStore;
    }

    public java.lang.String getProtocol() {
        return this.protocol == null ? ch.qos.logback.core.net.ssl.SSL.DEFAULT_PROTOCOL : this.protocol;
    }

    public java.lang.String getProvider() {
        return this.provider;
    }

    public ch.qos.logback.core.net.ssl.SecureRandomFactoryBean getSecureRandom() {
        return this.secureRandom == null ? new ch.qos.logback.core.net.ssl.SecureRandomFactoryBean() : this.secureRandom;
    }

    public ch.qos.logback.core.net.ssl.TrustManagerFactoryFactoryBean getTrustManagerFactory() {
        return this.trustManagerFactory == null ? new ch.qos.logback.core.net.ssl.TrustManagerFactoryFactoryBean() : this.trustManagerFactory;
    }

    public ch.qos.logback.core.net.ssl.KeyStoreFactoryBean getTrustStore() {
        if (this.trustStore == null) {
            this.trustStore = keyStoreFromSystemProperties(JSSE_TRUST_STORE_PROPERTY);
        }
        return this.trustStore;
    }

    public void setKeyManagerFactory(ch.qos.logback.core.net.ssl.KeyManagerFactoryFactoryBean keyManagerFactoryFactoryBean) {
        this.keyManagerFactory = keyManagerFactoryFactoryBean;
    }

    public void setKeyStore(ch.qos.logback.core.net.ssl.KeyStoreFactoryBean keyStoreFactoryBean) {
        this.keyStore = keyStoreFactoryBean;
    }

    public void setProtocol(java.lang.String str) {
        this.protocol = str;
    }

    public void setProvider(java.lang.String str) {
        this.provider = str;
    }

    public void setSecureRandom(ch.qos.logback.core.net.ssl.SecureRandomFactoryBean secureRandomFactoryBean) {
        this.secureRandom = secureRandomFactoryBean;
    }

    public void setTrustManagerFactory(ch.qos.logback.core.net.ssl.TrustManagerFactoryFactoryBean trustManagerFactoryFactoryBean) {
        this.trustManagerFactory = trustManagerFactoryFactoryBean;
    }

    public void setTrustStore(ch.qos.logback.core.net.ssl.KeyStoreFactoryBean keyStoreFactoryBean) {
        this.trustStore = keyStoreFactoryBean;
    }
}
