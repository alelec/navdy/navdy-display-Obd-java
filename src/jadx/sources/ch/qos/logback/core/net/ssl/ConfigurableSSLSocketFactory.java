package ch.qos.logback.core.net.ssl;

public class ConfigurableSSLSocketFactory extends javax.net.SocketFactory {
    private final javax.net.ssl.SSLSocketFactory delegate;
    private final ch.qos.logback.core.net.ssl.SSLParametersConfiguration parameters;

    public ConfigurableSSLSocketFactory(ch.qos.logback.core.net.ssl.SSLParametersConfiguration sSLParametersConfiguration, javax.net.ssl.SSLSocketFactory sSLSocketFactory) {
        this.parameters = sSLParametersConfiguration;
        this.delegate = sSLSocketFactory;
    }

    public java.net.Socket createSocket(java.lang.String str, int i) throws java.io.IOException, java.net.UnknownHostException {
        javax.net.ssl.SSLSocket sSLSocket = (javax.net.ssl.SSLSocket) this.delegate.createSocket(str, i);
        this.parameters.configure(new ch.qos.logback.core.net.ssl.SSLConfigurableSocket(sSLSocket));
        return sSLSocket;
    }

    public java.net.Socket createSocket(java.lang.String str, int i, java.net.InetAddress inetAddress, int i2) throws java.io.IOException, java.net.UnknownHostException {
        javax.net.ssl.SSLSocket sSLSocket = (javax.net.ssl.SSLSocket) this.delegate.createSocket(str, i, inetAddress, i2);
        this.parameters.configure(new ch.qos.logback.core.net.ssl.SSLConfigurableSocket(sSLSocket));
        return sSLSocket;
    }

    public java.net.Socket createSocket(java.net.InetAddress inetAddress, int i) throws java.io.IOException {
        javax.net.ssl.SSLSocket sSLSocket = (javax.net.ssl.SSLSocket) this.delegate.createSocket(inetAddress, i);
        this.parameters.configure(new ch.qos.logback.core.net.ssl.SSLConfigurableSocket(sSLSocket));
        return sSLSocket;
    }

    public java.net.Socket createSocket(java.net.InetAddress inetAddress, int i, java.net.InetAddress inetAddress2, int i2) throws java.io.IOException {
        javax.net.ssl.SSLSocket sSLSocket = (javax.net.ssl.SSLSocket) this.delegate.createSocket(inetAddress, i, inetAddress2, i2);
        this.parameters.configure(new ch.qos.logback.core.net.ssl.SSLConfigurableSocket(sSLSocket));
        return sSLSocket;
    }
}
