package ch.qos.logback.core.net.ssl;

public class SSLNestedComponentRegistryRules {
    public static void addDefaultNestedComponentRegistryRules(ch.qos.logback.core.joran.spi.DefaultNestedComponentRegistry defaultNestedComponentRegistry) {
        defaultNestedComponentRegistry.add(ch.qos.logback.core.net.ssl.SSLComponent.class, "ssl", ch.qos.logback.core.net.ssl.SSLConfiguration.class);
        defaultNestedComponentRegistry.add(ch.qos.logback.core.net.ssl.SSLConfiguration.class, "parameters", ch.qos.logback.core.net.ssl.SSLParametersConfiguration.class);
        defaultNestedComponentRegistry.add(ch.qos.logback.core.net.ssl.SSLConfiguration.class, "keyStore", ch.qos.logback.core.net.ssl.KeyStoreFactoryBean.class);
        defaultNestedComponentRegistry.add(ch.qos.logback.core.net.ssl.SSLConfiguration.class, "trustStore", ch.qos.logback.core.net.ssl.KeyStoreFactoryBean.class);
        defaultNestedComponentRegistry.add(ch.qos.logback.core.net.ssl.SSLConfiguration.class, "keyManagerFactory", ch.qos.logback.core.net.ssl.KeyManagerFactoryFactoryBean.class);
        defaultNestedComponentRegistry.add(ch.qos.logback.core.net.ssl.SSLConfiguration.class, "trustManagerFactory", ch.qos.logback.core.net.ssl.TrustManagerFactoryFactoryBean.class);
        defaultNestedComponentRegistry.add(ch.qos.logback.core.net.ssl.SSLConfiguration.class, "secureRandom", ch.qos.logback.core.net.ssl.SecureRandomFactoryBean.class);
    }
}
