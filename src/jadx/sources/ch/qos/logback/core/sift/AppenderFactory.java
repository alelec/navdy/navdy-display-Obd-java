package ch.qos.logback.core.sift;

public interface AppenderFactory<E> {
    ch.qos.logback.core.Appender<E> buildAppender(ch.qos.logback.core.Context context, java.lang.String str) throws ch.qos.logback.core.joran.spi.JoranException;
}
