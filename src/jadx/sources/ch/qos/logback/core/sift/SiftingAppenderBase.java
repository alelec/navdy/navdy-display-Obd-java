package ch.qos.logback.core.sift;

public abstract class SiftingAppenderBase<E> extends ch.qos.logback.core.AppenderBase<E> {
    ch.qos.logback.core.sift.AppenderFactory<E> appenderFactory;
    protected ch.qos.logback.core.sift.AppenderTracker<E> appenderTracker;
    ch.qos.logback.core.sift.Discriminator<E> discriminator;
    int maxAppenderCount = Integer.MAX_VALUE;
    ch.qos.logback.core.util.Duration timeout = new ch.qos.logback.core.util.Duration(1800000);

    /* access modifiers changed from: protected */
    public void append(E e) {
        if (isStarted()) {
            java.lang.String discriminatingValue = this.discriminator.getDiscriminatingValue(e);
            long timestamp = getTimestamp(e);
            ch.qos.logback.core.Appender appender = (ch.qos.logback.core.Appender) this.appenderTracker.getOrCreate(discriminatingValue, timestamp);
            if (eventMarksEndOfLife(e)) {
                this.appenderTracker.endOfLife(discriminatingValue);
            }
            this.appenderTracker.removeStaleComponents(timestamp);
            appender.doAppend(e);
        }
    }

    /* access modifiers changed from: protected */
    public abstract boolean eventMarksEndOfLife(E e);

    public ch.qos.logback.core.sift.AppenderTracker<E> getAppenderTracker() {
        return this.appenderTracker;
    }

    public ch.qos.logback.core.sift.Discriminator<E> getDiscriminator() {
        return this.discriminator;
    }

    public java.lang.String getDiscriminatorKey() {
        if (this.discriminator != null) {
            return this.discriminator.getKey();
        }
        return null;
    }

    public int getMaxAppenderCount() {
        return this.maxAppenderCount;
    }

    public ch.qos.logback.core.util.Duration getTimeout() {
        return this.timeout;
    }

    /* access modifiers changed from: protected */
    public abstract long getTimestamp(E e);

    public void setAppenderFactory(ch.qos.logback.core.sift.AppenderFactory<E> appenderFactory2) {
        this.appenderFactory = appenderFactory2;
    }

    public void setDiscriminator(ch.qos.logback.core.sift.Discriminator<E> discriminator2) {
        this.discriminator = discriminator2;
    }

    public void setMaxAppenderCount(int i) {
        this.maxAppenderCount = i;
    }

    public void setTimeout(ch.qos.logback.core.util.Duration duration) {
        this.timeout = duration;
    }

    public void start() {
        int i = 0;
        if (this.discriminator == null) {
            addError("Missing discriminator. Aborting");
            i = 1;
        }
        if (!this.discriminator.isStarted()) {
            addError("Discriminator has not started successfully. Aborting");
            i++;
        }
        if (this.appenderFactory == null) {
            addError("AppenderFactory has not been set. Aborting");
            i++;
        } else {
            this.appenderTracker = new ch.qos.logback.core.sift.AppenderTracker<>(this.context, this.appenderFactory);
            this.appenderTracker.setMaxComponents(this.maxAppenderCount);
            this.appenderTracker.setTimeout(this.timeout.getMilliseconds());
        }
        if (i == 0) {
            super.start();
        }
    }

    public void stop() {
        for (ch.qos.logback.core.Appender stop : this.appenderTracker.allComponents()) {
            stop.stop();
        }
    }
}
