package ch.qos.logback.core.sift;

public class AppenderTracker<E> extends ch.qos.logback.core.spi.AbstractComponentTracker<ch.qos.logback.core.Appender<E>> {
    final ch.qos.logback.core.sift.AppenderFactory<E> appenderFactory;
    final ch.qos.logback.core.Context context;
    final ch.qos.logback.core.spi.ContextAwareImpl contextAware;
    int nopaWarningCount = 0;

    public AppenderTracker(ch.qos.logback.core.Context context2, ch.qos.logback.core.sift.AppenderFactory<E> appenderFactory2) {
        this.context = context2;
        this.appenderFactory = appenderFactory2;
        this.contextAware = new ch.qos.logback.core.spi.ContextAwareImpl(context2, this);
    }

    private ch.qos.logback.core.helpers.NOPAppender<E> buildNOPAppender(java.lang.String str) {
        if (this.nopaWarningCount < 4) {
            this.nopaWarningCount++;
            this.contextAware.addError("Building NOPAppender for discriminating value [" + str + "]");
        }
        ch.qos.logback.core.helpers.NOPAppender<E> nOPAppender = new ch.qos.logback.core.helpers.NOPAppender<>();
        nOPAppender.setContext(this.context);
        nOPAppender.start();
        return nOPAppender;
    }

    /* access modifiers changed from: protected */
    public ch.qos.logback.core.Appender<E> buildComponent(java.lang.String str) {
        ch.qos.logback.core.Appender<E> appender = null;
        try {
            appender = this.appenderFactory.buildAppender(this.context, str);
        } catch (ch.qos.logback.core.joran.spi.JoranException e) {
            this.contextAware.addError("Error while building appender with discriminating value [" + str + "]");
        }
        return appender == null ? buildNOPAppender(str) : appender;
    }

    /* access modifiers changed from: protected */
    public boolean isComponentStale(ch.qos.logback.core.Appender<E> appender) {
        return !appender.isStarted();
    }

    /* access modifiers changed from: protected */
    public void processPriorToRemoval(ch.qos.logback.core.Appender<E> appender) {
        appender.stop();
    }
}
