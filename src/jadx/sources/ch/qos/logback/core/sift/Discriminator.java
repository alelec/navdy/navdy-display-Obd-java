package ch.qos.logback.core.sift;

public interface Discriminator<E> extends ch.qos.logback.core.spi.LifeCycle {
    java.lang.String getDiscriminatingValue(E e);

    java.lang.String getKey();
}
