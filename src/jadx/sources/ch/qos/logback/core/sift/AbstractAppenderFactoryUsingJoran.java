package ch.qos.logback.core.sift;

public abstract class AbstractAppenderFactoryUsingJoran<E> implements ch.qos.logback.core.sift.AppenderFactory<E> {
    final java.util.List<ch.qos.logback.core.joran.event.SaxEvent> eventList;
    protected java.lang.String key;
    protected java.util.Map<java.lang.String, java.lang.String> parentPropertyMap;

    protected AbstractAppenderFactoryUsingJoran(java.util.List<ch.qos.logback.core.joran.event.SaxEvent> list, java.lang.String str, java.util.Map<java.lang.String, java.lang.String> map) {
        this.eventList = removeSiftElement(list);
        this.key = str;
        this.parentPropertyMap = map;
    }

    public ch.qos.logback.core.Appender<E> buildAppender(ch.qos.logback.core.Context context, java.lang.String str) throws ch.qos.logback.core.joran.spi.JoranException {
        ch.qos.logback.core.sift.SiftingJoranConfiguratorBase siftingJoranConfigurator = getSiftingJoranConfigurator(str);
        siftingJoranConfigurator.setContext(context);
        siftingJoranConfigurator.doConfigure(this.eventList);
        return siftingJoranConfigurator.getAppender();
    }

    public java.util.List<ch.qos.logback.core.joran.event.SaxEvent> getEventList() {
        return this.eventList;
    }

    public abstract ch.qos.logback.core.sift.SiftingJoranConfiguratorBase<E> getSiftingJoranConfigurator(java.lang.String str);

    /* access modifiers changed from: 0000 */
    public java.util.List<ch.qos.logback.core.joran.event.SaxEvent> removeSiftElement(java.util.List<ch.qos.logback.core.joran.event.SaxEvent> list) {
        return list.subList(1, list.size() - 1);
    }
}
