package ch.qos.logback.core.sift;

public abstract class AbstractDiscriminator<E> extends ch.qos.logback.core.spi.ContextAwareBase implements ch.qos.logback.core.sift.Discriminator<E> {
    protected boolean started;

    public boolean isStarted() {
        return this.started;
    }

    public void start() {
        this.started = true;
    }

    public void stop() {
        this.started = false;
    }
}
