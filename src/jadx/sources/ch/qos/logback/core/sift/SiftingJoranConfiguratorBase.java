package ch.qos.logback.core.sift;

public abstract class SiftingJoranConfiguratorBase<E> extends ch.qos.logback.core.joran.GenericConfigurator {
    static final java.lang.String ONE_AND_ONLY_ONE_URL = "http://logback.qos.ch/codes.html#1andOnly1";
    int errorEmmissionCount = 0;
    protected final java.lang.String key;
    protected final java.util.Map<java.lang.String, java.lang.String> parentPropertyMap;
    protected final java.lang.String value;

    protected SiftingJoranConfiguratorBase(java.lang.String str, java.lang.String str2, java.util.Map<java.lang.String, java.lang.String> map) {
        this.key = str;
        this.value = str2;
        this.parentPropertyMap = map;
    }

    /* access modifiers changed from: protected */
    public void addImplicitRules(ch.qos.logback.core.joran.spi.Interpreter interpreter) {
        ch.qos.logback.core.joran.action.NestedComplexPropertyIA nestedComplexPropertyIA = new ch.qos.logback.core.joran.action.NestedComplexPropertyIA();
        nestedComplexPropertyIA.setContext(this.context);
        interpreter.addImplicitAction(nestedComplexPropertyIA);
        ch.qos.logback.core.joran.action.NestedBasicPropertyIA nestedBasicPropertyIA = new ch.qos.logback.core.joran.action.NestedBasicPropertyIA();
        nestedBasicPropertyIA.setContext(this.context);
        interpreter.addImplicitAction(nestedBasicPropertyIA);
    }

    /* access modifiers changed from: protected */
    public void addInstanceRules(ch.qos.logback.core.joran.spi.RuleStore ruleStore) {
        ruleStore.addRule(new ch.qos.logback.core.joran.spi.ElementSelector("configuration/property"), (ch.qos.logback.core.joran.action.Action) new ch.qos.logback.core.joran.action.PropertyAction());
        ruleStore.addRule(new ch.qos.logback.core.joran.spi.ElementSelector("configuration/timestamp"), (ch.qos.logback.core.joran.action.Action) new ch.qos.logback.core.joran.action.TimestampAction());
        ruleStore.addRule(new ch.qos.logback.core.joran.spi.ElementSelector("configuration/define"), (ch.qos.logback.core.joran.action.Action) new ch.qos.logback.core.joran.action.DefinePropertyAction());
    }

    public void doConfigure(java.util.List<ch.qos.logback.core.joran.event.SaxEvent> list) throws ch.qos.logback.core.joran.spi.JoranException {
        super.doConfigure(list);
    }

    public abstract ch.qos.logback.core.Appender<E> getAppender();

    /* access modifiers changed from: protected */
    public void oneAndOnlyOneCheck(java.util.Map<?, ?> map) {
        java.lang.String str = null;
        if (map.size() == 0) {
            this.errorEmmissionCount++;
            str = "No nested appenders found within the <sift> element in SiftingAppender.";
        } else if (map.size() > 1) {
            this.errorEmmissionCount++;
            str = "Only and only one appender can be nested the <sift> element in SiftingAppender. See also http://logback.qos.ch/codes.html#1andOnly1";
        }
        if (str != null && this.errorEmmissionCount < 4) {
            addError(str);
        }
    }

    public java.lang.String toString() {
        return getClass().getName() + "{" + this.key + "=" + this.value + ch.qos.logback.core.CoreConstants.CURLY_RIGHT;
    }
}
