package ch.qos.logback.core.joran.action;

public class PropertyAction extends ch.qos.logback.core.joran.action.Action {
    static java.lang.String INVALID_ATTRIBUTES = "In <property> element, either the \"file\" attribute alone, or the \"resource\" element alone, or both the \"name\" and \"value\" attributes must be set.";
    static final java.lang.String RESOURCE_ATTRIBUTE = "resource";

    public void begin(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext, java.lang.String str, org.xml.sax.Attributes attributes) {
        if ("substitutionProperty".equals(str)) {
            addWarn("[substitutionProperty] element has been deprecated. Please use the [property] element instead.");
        }
        java.lang.String value = attributes.getValue(ch.qos.logback.core.joran.action.Action.NAME_ATTRIBUTE);
        java.lang.String value2 = attributes.getValue("value");
        ch.qos.logback.core.joran.action.ActionUtil.Scope stringToScope = ch.qos.logback.core.joran.action.ActionUtil.stringToScope(attributes.getValue(ch.qos.logback.core.joran.action.Action.SCOPE_ATTRIBUTE));
        if (checkFileAttributeSanity(attributes)) {
            java.lang.String subst = interpretationContext.subst(attributes.getValue(ch.qos.logback.core.joran.action.Action.FILE_ATTRIBUTE));
            try {
                loadAndSetProperties(interpretationContext, new java.io.FileInputStream(subst), stringToScope);
            } catch (java.io.FileNotFoundException e) {
                addError("Could not find properties file [" + subst + "].");
            } catch (java.io.IOException e2) {
                addError("Could not read properties file [" + subst + "].", e2);
            }
        } else if (checkResourceAttributeSanity(attributes)) {
            java.lang.String subst2 = interpretationContext.subst(attributes.getValue(RESOURCE_ATTRIBUTE));
            java.net.URL resourceBySelfClassLoader = ch.qos.logback.core.util.Loader.getResourceBySelfClassLoader(subst2);
            if (resourceBySelfClassLoader == null) {
                addError("Could not find resource [" + subst2 + "].");
                return;
            }
            try {
                loadAndSetProperties(interpretationContext, resourceBySelfClassLoader.openStream(), stringToScope);
            } catch (java.io.IOException e3) {
                addError("Could not read resource file [" + subst2 + "].", e3);
            }
        } else if (checkValueNameAttributesSanity(attributes)) {
            ch.qos.logback.core.joran.action.ActionUtil.setProperty(interpretationContext, value, interpretationContext.subst(ch.qos.logback.core.pattern.util.RegularEscapeUtil.basicEscape(value2).trim()), stringToScope);
        } else {
            addError(INVALID_ATTRIBUTES);
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean checkFileAttributeSanity(org.xml.sax.Attributes attributes) {
        return !ch.qos.logback.core.util.OptionHelper.isEmpty(attributes.getValue(ch.qos.logback.core.joran.action.Action.FILE_ATTRIBUTE)) && ch.qos.logback.core.util.OptionHelper.isEmpty(attributes.getValue(ch.qos.logback.core.joran.action.Action.NAME_ATTRIBUTE)) && ch.qos.logback.core.util.OptionHelper.isEmpty(attributes.getValue("value")) && ch.qos.logback.core.util.OptionHelper.isEmpty(attributes.getValue(RESOURCE_ATTRIBUTE));
    }

    /* access modifiers changed from: 0000 */
    public boolean checkResourceAttributeSanity(org.xml.sax.Attributes attributes) {
        return !ch.qos.logback.core.util.OptionHelper.isEmpty(attributes.getValue(RESOURCE_ATTRIBUTE)) && ch.qos.logback.core.util.OptionHelper.isEmpty(attributes.getValue(ch.qos.logback.core.joran.action.Action.NAME_ATTRIBUTE)) && ch.qos.logback.core.util.OptionHelper.isEmpty(attributes.getValue("value")) && ch.qos.logback.core.util.OptionHelper.isEmpty(attributes.getValue(ch.qos.logback.core.joran.action.Action.FILE_ATTRIBUTE));
    }

    /* access modifiers changed from: 0000 */
    public boolean checkValueNameAttributesSanity(org.xml.sax.Attributes attributes) {
        return !ch.qos.logback.core.util.OptionHelper.isEmpty(attributes.getValue(ch.qos.logback.core.joran.action.Action.NAME_ATTRIBUTE)) && !ch.qos.logback.core.util.OptionHelper.isEmpty(attributes.getValue("value")) && ch.qos.logback.core.util.OptionHelper.isEmpty(attributes.getValue(ch.qos.logback.core.joran.action.Action.FILE_ATTRIBUTE)) && ch.qos.logback.core.util.OptionHelper.isEmpty(attributes.getValue(RESOURCE_ATTRIBUTE));
    }

    public void end(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext, java.lang.String str) {
    }

    public void finish(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext) {
    }

    /* access modifiers changed from: 0000 */
    public void loadAndSetProperties(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext, java.io.InputStream inputStream, ch.qos.logback.core.joran.action.ActionUtil.Scope scope) throws java.io.IOException {
        java.util.Properties properties = new java.util.Properties();
        properties.load(inputStream);
        inputStream.close();
        ch.qos.logback.core.joran.action.ActionUtil.setProperties(interpretationContext, properties, scope);
    }
}
