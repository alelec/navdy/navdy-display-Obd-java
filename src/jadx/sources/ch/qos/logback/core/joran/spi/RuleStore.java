package ch.qos.logback.core.joran.spi;

public interface RuleStore {
    void addRule(ch.qos.logback.core.joran.spi.ElementSelector elementSelector, ch.qos.logback.core.joran.action.Action action);

    void addRule(ch.qos.logback.core.joran.spi.ElementSelector elementSelector, java.lang.String str) throws java.lang.ClassNotFoundException;

    java.util.List<ch.qos.logback.core.joran.action.Action> matchActions(ch.qos.logback.core.joran.spi.ElementPath elementPath);
}
