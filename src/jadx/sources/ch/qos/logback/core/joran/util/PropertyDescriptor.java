package ch.qos.logback.core.joran.util;

public class PropertyDescriptor {
    private java.lang.String name;
    private java.lang.reflect.Method readMethod;
    private java.lang.Class<?> type;
    private java.lang.reflect.Method writeMethod;

    public PropertyDescriptor(java.lang.String str) {
        this.name = str;
    }

    public java.lang.String getName() {
        return this.name;
    }

    public java.lang.Class<?> getPropertyType() {
        return this.type;
    }

    public java.lang.reflect.Method getReadMethod() {
        return this.readMethod;
    }

    public java.lang.reflect.Method getWriteMethod() {
        return this.writeMethod;
    }

    public void setPropertyType(java.lang.Class<?> cls) {
        this.type = cls;
    }

    public void setReadMethod(java.lang.reflect.Method method) {
        this.readMethod = method;
    }

    public void setWriteMethod(java.lang.reflect.Method method) {
        this.writeMethod = method;
    }
}
