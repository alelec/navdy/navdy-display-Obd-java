package ch.qos.logback.core.joran.event;

public class StartEvent extends ch.qos.logback.core.joran.event.SaxEvent {
    public final org.xml.sax.Attributes attributes;
    public final ch.qos.logback.core.joran.spi.ElementPath elementPath;

    StartEvent(ch.qos.logback.core.joran.spi.ElementPath elementPath2, java.lang.String str, java.lang.String str2, java.lang.String str3, org.xml.sax.Attributes attributes2, org.xml.sax.Locator locator) {
        super(str, str2, str3, locator);
        this.attributes = new org.xml.sax.helpers.AttributesImpl(attributes2);
        this.elementPath = elementPath2;
    }

    public org.xml.sax.Attributes getAttributes() {
        return this.attributes;
    }

    public java.lang.String toString() {
        return "StartEvent(" + getQName() + ")  [" + this.locator.getLineNumber() + "," + this.locator.getColumnNumber() + "]";
    }
}
