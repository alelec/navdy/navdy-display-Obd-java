package ch.qos.logback.core.joran.event;

public class EndEvent extends ch.qos.logback.core.joran.event.SaxEvent {
    EndEvent(java.lang.String str, java.lang.String str2, java.lang.String str3, org.xml.sax.Locator locator) {
        super(str, str2, str3, locator);
    }

    public java.lang.String toString() {
        return "  EndEvent(" + getQName() + ")  [" + this.locator.getLineNumber() + "," + this.locator.getColumnNumber() + "]";
    }
}
