package ch.qos.logback.core.joran.event;

public class BodyEvent extends ch.qos.logback.core.joran.event.SaxEvent {
    private java.lang.String text;

    BodyEvent(java.lang.String str, org.xml.sax.Locator locator) {
        super(null, null, null, locator);
        this.text = str;
    }

    public void append(java.lang.String str) {
        this.text += str;
    }

    public java.lang.String getText() {
        return this.text != null ? this.text.trim() : this.text;
    }

    public java.lang.String toString() {
        return "BodyEvent(" + getText() + ")" + this.locator.getLineNumber() + "," + this.locator.getColumnNumber();
    }
}
