package ch.qos.logback.core.joran.util;

public class MethodDescriptor {
    private java.lang.reflect.Method method;
    private java.lang.String name;

    public MethodDescriptor(java.lang.String str, java.lang.reflect.Method method2) {
        this.name = str;
        this.method = method2;
    }

    public java.lang.reflect.Method getMethod() {
        return this.method;
    }

    public java.lang.String getName() {
        return this.name;
    }
}
