package ch.qos.logback.core.joran.action;

public abstract class ActionConst {
    public static final java.lang.String ACTION_CLASS_ATTRIBUTE = "actionClass";
    public static final java.lang.String ADDITIVITY_ATTRIBUTE = "additivity";
    public static final java.lang.String APPENDER_BAG = "APPENDER_BAG";
    public static final java.lang.String APPENDER_TAG = "appender";
    public static final java.lang.String CONVERSION_WORD_ATTRIBUTE = "conversionWord";
    public static final java.lang.String CONVERTER_CLASS_ATTRIBUTE = "converterClass";
    public static final java.lang.String FILTER_CHAIN_BAG = "FILTER_CHAIN_BAG";
    public static final java.lang.String INHERITED = "INHERITED";
    public static final java.lang.String LEVEL_ATTRIBUTE = "level";
    public static final java.lang.String NULL = "NULL";
    static final java.lang.Class<?>[] ONE_STRING_PARAM = {java.lang.String.class};
    public static final java.lang.String PATTERN_ATTRIBUTE = "pattern";
    public static final java.lang.String REF_ATTRIBUTE = "ref";
    public static final java.lang.String VALUE_ATTR = "value";
}
