package ch.qos.logback.core.joran.action;

public class NewRuleAction extends ch.qos.logback.core.joran.action.Action {
    boolean inError = false;

    public void begin(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext, java.lang.String str, org.xml.sax.Attributes attributes) {
        this.inError = false;
        java.lang.String value = attributes.getValue("pattern");
        java.lang.String value2 = attributes.getValue("actionClass");
        if (ch.qos.logback.core.util.OptionHelper.isEmpty(value)) {
            this.inError = true;
            addError("No 'pattern' attribute in <newRule>");
        } else if (ch.qos.logback.core.util.OptionHelper.isEmpty(value2)) {
            this.inError = true;
            addError("No 'actionClass' attribute in <newRule>");
        } else {
            try {
                addInfo("About to add new Joran parsing rule [" + value + "," + value2 + "].");
                interpretationContext.getJoranInterpreter().getRuleStore().addRule(new ch.qos.logback.core.joran.spi.ElementSelector(value), value2);
            } catch (java.lang.Exception e) {
                this.inError = true;
                addError("Could not add new Joran parsing rule [" + value + "," + value2 + "]");
            }
        }
    }

    public void end(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext, java.lang.String str) {
    }

    public void finish(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext) {
    }
}
