package ch.qos.logback.core.joran.spi;

public class NoAutoStartUtil {
    public static boolean notMarkedWithNoAutoStart(java.lang.Object obj) {
        if (obj == null) {
            return false;
        }
        return ((ch.qos.logback.core.joran.spi.NoAutoStart) obj.getClass().getAnnotation(ch.qos.logback.core.joran.spi.NoAutoStart.class)) == null;
    }
}
