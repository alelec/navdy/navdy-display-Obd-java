package ch.qos.logback.core.joran.event;

public interface InPlayListener {
    void inPlay(ch.qos.logback.core.joran.event.SaxEvent saxEvent);
}
