package ch.qos.logback.core.joran.spi;

public class EventPlayer {
    int currentIndex;
    java.util.List<ch.qos.logback.core.joran.event.SaxEvent> eventList;
    final ch.qos.logback.core.joran.spi.Interpreter interpreter;

    public EventPlayer(ch.qos.logback.core.joran.spi.Interpreter interpreter2) {
        this.interpreter = interpreter2;
    }

    public void addEventsDynamically(java.util.List<ch.qos.logback.core.joran.event.SaxEvent> list, int i) {
        this.eventList.addAll(this.currentIndex + i, list);
    }

    public java.util.List<ch.qos.logback.core.joran.event.SaxEvent> getCopyOfPlayerEventList() {
        return new java.util.ArrayList(this.eventList);
    }

    public void play(java.util.List<ch.qos.logback.core.joran.event.SaxEvent> list) {
        this.eventList = list;
        this.currentIndex = 0;
        while (this.currentIndex < this.eventList.size()) {
            ch.qos.logback.core.joran.event.SaxEvent saxEvent = (ch.qos.logback.core.joran.event.SaxEvent) this.eventList.get(this.currentIndex);
            if (saxEvent instanceof ch.qos.logback.core.joran.event.StartEvent) {
                this.interpreter.startElement((ch.qos.logback.core.joran.event.StartEvent) saxEvent);
                this.interpreter.getInterpretationContext().fireInPlay(saxEvent);
            }
            if (saxEvent instanceof ch.qos.logback.core.joran.event.BodyEvent) {
                this.interpreter.getInterpretationContext().fireInPlay(saxEvent);
                this.interpreter.characters((ch.qos.logback.core.joran.event.BodyEvent) saxEvent);
            }
            if (saxEvent instanceof ch.qos.logback.core.joran.event.EndEvent) {
                this.interpreter.getInterpretationContext().fireInPlay(saxEvent);
                this.interpreter.endElement((ch.qos.logback.core.joran.event.EndEvent) saxEvent);
            }
            this.currentIndex++;
        }
    }
}
