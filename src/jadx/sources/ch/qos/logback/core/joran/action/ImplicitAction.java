package ch.qos.logback.core.joran.action;

public abstract class ImplicitAction extends ch.qos.logback.core.joran.action.Action {
    public abstract boolean isApplicable(ch.qos.logback.core.joran.spi.ElementPath elementPath, org.xml.sax.Attributes attributes, ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext);
}
