package ch.qos.logback.core.joran.action;

public class StatusListenerAction extends ch.qos.logback.core.joran.action.Action {
    boolean inError = false;
    ch.qos.logback.core.status.StatusListener statusListener = null;

    public void begin(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext, java.lang.String str, org.xml.sax.Attributes attributes) throws ch.qos.logback.core.joran.spi.ActionException {
        this.inError = false;
        java.lang.String value = attributes.getValue(ch.qos.logback.core.joran.action.Action.CLASS_ATTRIBUTE);
        if (ch.qos.logback.core.util.OptionHelper.isEmpty(value)) {
            addError("Missing class name for statusListener. Near [" + str + "] line " + getLineNumber(interpretationContext));
            this.inError = true;
            return;
        }
        try {
            if (ch.qos.logback.core.status.OnConsoleStatusListener.class.getName().equals(value)) {
                ch.qos.logback.core.status.OnConsoleStatusListener.addNewInstanceToContext(this.context);
            } else {
                this.statusListener = (ch.qos.logback.core.status.StatusListener) ch.qos.logback.core.util.OptionHelper.instantiateByClassName(value, ch.qos.logback.core.status.StatusListener.class, this.context);
                interpretationContext.getContext().getStatusManager().add(this.statusListener);
                if (this.statusListener instanceof ch.qos.logback.core.spi.ContextAware) {
                    ((ch.qos.logback.core.spi.ContextAware) this.statusListener).setContext(this.context);
                }
            }
            addInfo("Added status listener of type [" + value + "]");
            interpretationContext.pushObject(this.statusListener);
        } catch (java.lang.Exception e) {
            this.inError = true;
            addError("Could not create an StatusListener of type [" + value + "].", e);
            throw new ch.qos.logback.core.joran.spi.ActionException(e);
        }
    }

    public void end(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext, java.lang.String str) {
        if (!this.inError) {
            if (this.statusListener instanceof ch.qos.logback.core.spi.LifeCycle) {
                ((ch.qos.logback.core.spi.LifeCycle) this.statusListener).start();
            }
            if (interpretationContext.peekObject() != this.statusListener) {
                addWarn("The object at the of the stack is not the statusListener pushed earlier.");
            } else {
                interpretationContext.popObject();
            }
        }
    }

    public void finish(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext) {
    }
}
