package ch.qos.logback.core.joran.spi;

public class HostClassAndPropertyDouble {
    final java.lang.Class<?> hostClass;
    final java.lang.String propertyName;

    public HostClassAndPropertyDouble(java.lang.Class<?> cls, java.lang.String str) {
        this.hostClass = cls;
        this.propertyName = str;
    }

    public boolean equals(java.lang.Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ch.qos.logback.core.joran.spi.HostClassAndPropertyDouble hostClassAndPropertyDouble = (ch.qos.logback.core.joran.spi.HostClassAndPropertyDouble) obj;
        if (this.hostClass == null) {
            if (hostClassAndPropertyDouble.hostClass != null) {
                return false;
            }
        } else if (!this.hostClass.equals(hostClassAndPropertyDouble.hostClass)) {
            return false;
        }
        return this.propertyName == null ? hostClassAndPropertyDouble.propertyName == null : this.propertyName.equals(hostClassAndPropertyDouble.propertyName);
    }

    public java.lang.Class<?> getHostClass() {
        return this.hostClass;
    }

    public java.lang.String getPropertyName() {
        return this.propertyName;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = ((this.hostClass == null ? 0 : this.hostClass.hashCode()) + 31) * 31;
        if (this.propertyName != null) {
            i = this.propertyName.hashCode();
        }
        return hashCode + i;
    }
}
