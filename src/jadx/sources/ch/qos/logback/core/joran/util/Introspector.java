package ch.qos.logback.core.joran.util;

public class Introspector {
    public static java.lang.String decapitalize(java.lang.String str) {
        if (str == null || str.length() == 0) {
            return str;
        }
        java.lang.String lowerCase = str.substring(0, 1).toLowerCase();
        return str.length() > 1 ? lowerCase + str.substring(1) : lowerCase;
    }

    public static ch.qos.logback.core.joran.util.MethodDescriptor[] getMethodDescriptors(java.lang.Class<?> cls) {
        java.lang.reflect.Method[] methods;
        java.util.ArrayList arrayList = new java.util.ArrayList();
        for (java.lang.reflect.Method method : cls.getMethods()) {
            arrayList.add(new ch.qos.logback.core.joran.util.MethodDescriptor(method.getName(), method));
        }
        return (ch.qos.logback.core.joran.util.MethodDescriptor[]) arrayList.toArray(new ch.qos.logback.core.joran.util.MethodDescriptor[0]);
    }

    public static ch.qos.logback.core.joran.util.PropertyDescriptor[] getPropertyDescriptors(java.lang.Class<?> cls) {
        java.lang.reflect.Method[] methods;
        int length = "set".length();
        java.util.HashMap hashMap = new java.util.HashMap();
        for (java.lang.reflect.Method method : cls.getMethods()) {
            java.lang.String name = method.getName();
            boolean z = name.startsWith("get") && name.length() > length;
            boolean z2 = name.startsWith("set") && name.length() > length;
            if (z || z2) {
                java.lang.String decapitalize = decapitalize(name.substring(length));
                ch.qos.logback.core.joran.util.PropertyDescriptor propertyDescriptor = (ch.qos.logback.core.joran.util.PropertyDescriptor) hashMap.get(decapitalize);
                if (propertyDescriptor == null) {
                    propertyDescriptor = new ch.qos.logback.core.joran.util.PropertyDescriptor(decapitalize);
                    hashMap.put(decapitalize, propertyDescriptor);
                }
                java.lang.Class[] parameterTypes = method.getParameterTypes();
                if (z2) {
                    if (parameterTypes.length == 1) {
                        propertyDescriptor.setWriteMethod(method);
                        propertyDescriptor.setPropertyType(parameterTypes[0]);
                    }
                } else if (z && parameterTypes.length == 0) {
                    propertyDescriptor.setReadMethod(method);
                    if (propertyDescriptor.getPropertyType() == null) {
                        propertyDescriptor.setPropertyType(method.getReturnType());
                    }
                }
            }
        }
        return (ch.qos.logback.core.joran.util.PropertyDescriptor[]) hashMap.values().toArray(new ch.qos.logback.core.joran.util.PropertyDescriptor[0]);
    }
}
