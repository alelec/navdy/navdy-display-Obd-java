package ch.qos.logback.core.joran.action;

public class TimestampAction extends ch.qos.logback.core.joran.action.Action {
    static java.lang.String CONTEXT_BIRTH = "contextBirth";
    static java.lang.String DATE_PATTERN_ATTRIBUTE = "datePattern";
    static java.lang.String TIME_REFERENCE_ATTRIBUTE = "timeReference";
    boolean inError = false;

    public void begin(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext, java.lang.String str, org.xml.sax.Attributes attributes) throws ch.qos.logback.core.joran.spi.ActionException {
        long currentTimeMillis;
        java.lang.String value = attributes.getValue(ch.qos.logback.core.joran.action.Action.KEY_ATTRIBUTE);
        if (ch.qos.logback.core.util.OptionHelper.isEmpty(value)) {
            addError("Attribute named [key] cannot be empty");
            this.inError = true;
        }
        java.lang.String value2 = attributes.getValue(DATE_PATTERN_ATTRIBUTE);
        if (ch.qos.logback.core.util.OptionHelper.isEmpty(value2)) {
            addError("Attribute named [" + DATE_PATTERN_ATTRIBUTE + "] cannot be empty");
            this.inError = true;
        }
        if (CONTEXT_BIRTH.equalsIgnoreCase(attributes.getValue(TIME_REFERENCE_ATTRIBUTE))) {
            addInfo("Using context birth as time reference.");
            currentTimeMillis = this.context.getBirthTime();
        } else {
            currentTimeMillis = java.lang.System.currentTimeMillis();
            addInfo("Using current interpretation time, i.e. now, as time reference.");
        }
        if (!this.inError) {
            ch.qos.logback.core.joran.action.ActionUtil.Scope stringToScope = ch.qos.logback.core.joran.action.ActionUtil.stringToScope(attributes.getValue(ch.qos.logback.core.joran.action.Action.SCOPE_ATTRIBUTE));
            java.lang.String format = new ch.qos.logback.core.util.CachingDateFormatter(value2).format(currentTimeMillis);
            addInfo("Adding property to the context with key=\"" + value + "\" and value=\"" + format + "\" to the " + stringToScope + " scope");
            ch.qos.logback.core.joran.action.ActionUtil.setProperty(interpretationContext, value, format, stringToScope);
        }
    }

    public void end(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext, java.lang.String str) throws ch.qos.logback.core.joran.spi.ActionException {
    }
}
