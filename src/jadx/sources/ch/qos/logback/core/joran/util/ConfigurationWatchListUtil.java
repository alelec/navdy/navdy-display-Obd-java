package ch.qos.logback.core.joran.util;

public class ConfigurationWatchListUtil {
    static final ch.qos.logback.core.joran.util.ConfigurationWatchListUtil origin = new ch.qos.logback.core.joran.util.ConfigurationWatchListUtil();

    private ConfigurationWatchListUtil() {
    }

    static void addInfo(ch.qos.logback.core.Context context, java.lang.String str) {
        addStatus(context, new ch.qos.logback.core.status.InfoStatus(str, origin));
    }

    static void addStatus(ch.qos.logback.core.Context context, ch.qos.logback.core.status.Status status) {
        if (context == null) {
            java.lang.System.out.println("Null context in " + ch.qos.logback.core.joran.spi.ConfigurationWatchList.class.getName());
            return;
        }
        ch.qos.logback.core.status.StatusManager statusManager = context.getStatusManager();
        if (statusManager != null) {
            statusManager.add(status);
        }
    }

    public static void addToWatchList(ch.qos.logback.core.Context context, java.net.URL url) {
        ch.qos.logback.core.joran.spi.ConfigurationWatchList configurationWatchList = getConfigurationWatchList(context);
        if (configurationWatchList == null) {
            addWarn(context, "Null ConfigurationWatchList. Cannot add " + url);
            return;
        }
        addInfo(context, "Adding [" + url + "] to configuration watch list.");
        configurationWatchList.addToWatchList(url);
    }

    static void addWarn(ch.qos.logback.core.Context context, java.lang.String str) {
        addStatus(context, new ch.qos.logback.core.status.WarnStatus(str, origin));
    }

    public static ch.qos.logback.core.joran.spi.ConfigurationWatchList getConfigurationWatchList(ch.qos.logback.core.Context context) {
        if (context == null) {
            return null;
        }
        return (ch.qos.logback.core.joran.spi.ConfigurationWatchList) context.getObject(ch.qos.logback.core.CoreConstants.CONFIGURATION_WATCH_LIST);
    }

    public static java.net.URL getMainWatchURL(ch.qos.logback.core.Context context) {
        ch.qos.logback.core.joran.spi.ConfigurationWatchList configurationWatchList = getConfigurationWatchList(context);
        if (configurationWatchList == null) {
            return null;
        }
        return configurationWatchList.getMainURL();
    }

    public static void setConfigurationWatchListResetFlag(ch.qos.logback.core.Context context, boolean z) {
        context.putObject(ch.qos.logback.core.CoreConstants.CONFIGURATION_WATCH_LIST_RESET, java.lang.Boolean.valueOf(z));
    }

    public static void setMainWatchURL(ch.qos.logback.core.Context context, java.net.URL url) {
        if (context != null) {
            ch.qos.logback.core.joran.spi.ConfigurationWatchList configurationWatchList = getConfigurationWatchList(context);
            if (configurationWatchList == null) {
                configurationWatchList = new ch.qos.logback.core.joran.spi.ConfigurationWatchList();
                configurationWatchList.setContext(context);
                context.putObject(ch.qos.logback.core.CoreConstants.CONFIGURATION_WATCH_LIST, configurationWatchList);
            } else {
                configurationWatchList.clear();
            }
            setConfigurationWatchListResetFlag(context, true);
            configurationWatchList.setMainURL(url);
        }
    }

    public static boolean wasConfigurationWatchListReset(ch.qos.logback.core.Context context) {
        if (context == null) {
            return false;
        }
        java.lang.Object object = context.getObject(ch.qos.logback.core.CoreConstants.CONFIGURATION_WATCH_LIST_RESET);
        if (object == null) {
            return false;
        }
        return ((java.lang.Boolean) object).booleanValue();
    }
}
