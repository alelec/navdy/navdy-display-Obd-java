package ch.qos.logback.core.joran.action;

public abstract class AbstractEventEvaluatorAction extends ch.qos.logback.core.joran.action.Action {
    ch.qos.logback.core.boolex.EventEvaluator<?> evaluator;
    boolean inError = false;

    public void begin(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext, java.lang.String str, org.xml.sax.Attributes attributes) {
        this.inError = false;
        this.evaluator = null;
        java.lang.String value = attributes.getValue(ch.qos.logback.core.joran.action.Action.CLASS_ATTRIBUTE);
        if (ch.qos.logback.core.util.OptionHelper.isEmpty(value)) {
            value = defaultClassName();
            addInfo("Assuming default evaluator class [" + value + "]");
        }
        java.lang.String str2 = value;
        if (ch.qos.logback.core.util.OptionHelper.isEmpty(str2)) {
            defaultClassName();
            this.inError = true;
            addError("Mandatory \"class\" attribute not set for <evaluator>");
            return;
        }
        java.lang.String value2 = attributes.getValue(ch.qos.logback.core.joran.action.Action.NAME_ATTRIBUTE);
        if (ch.qos.logback.core.util.OptionHelper.isEmpty(value2)) {
            this.inError = true;
            addError("Mandatory \"name\" attribute not set for <evaluator>");
            return;
        }
        try {
            this.evaluator = (ch.qos.logback.core.boolex.EventEvaluator) ch.qos.logback.core.util.OptionHelper.instantiateByClassName(str2, ch.qos.logback.core.boolex.EventEvaluator.class, this.context);
            this.evaluator.setContext(this.context);
            this.evaluator.setName(value2);
            interpretationContext.pushObject(this.evaluator);
            addInfo("Adding evaluator named [" + value2 + "] to the object stack");
        } catch (java.lang.Exception e) {
            this.inError = true;
            addError("Could not create evaluator of type " + str2 + "].", e);
        }
    }

    /* access modifiers changed from: protected */
    public abstract java.lang.String defaultClassName();

    public void end(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext, java.lang.String str) {
        if (!this.inError) {
            if (this.evaluator instanceof ch.qos.logback.core.spi.LifeCycle) {
                this.evaluator.start();
                addInfo("Starting evaluator named [" + this.evaluator.getName() + "]");
            }
            if (interpretationContext.peekObject() != this.evaluator) {
                addWarn("The object on the top the of the stack is not the evaluator pushed earlier.");
                return;
            }
            interpretationContext.popObject();
            try {
                java.util.Map map = (java.util.Map) this.context.getObject(ch.qos.logback.core.CoreConstants.EVALUATOR_MAP);
                if (map == null) {
                    addError("Could not find EvaluatorMap");
                } else {
                    map.put(this.evaluator.getName(), this.evaluator);
                }
            } catch (java.lang.Exception e) {
                addError("Could not set evaluator named [" + this.evaluator + "].", e);
            }
        }
    }

    public void finish(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext) {
    }
}
