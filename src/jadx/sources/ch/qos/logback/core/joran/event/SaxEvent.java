package ch.qos.logback.core.joran.event;

public class SaxEvent {
    public final java.lang.String localName;
    public final org.xml.sax.Locator locator;
    public final java.lang.String namespaceURI;
    public final java.lang.String qName;

    SaxEvent(java.lang.String str, java.lang.String str2, java.lang.String str3, org.xml.sax.Locator locator2) {
        this.namespaceURI = str;
        this.localName = str2;
        this.qName = str3;
        this.locator = new org.xml.sax.helpers.LocatorImpl(locator2);
    }

    public java.lang.String getLocalName() {
        return this.localName;
    }

    public org.xml.sax.Locator getLocator() {
        return this.locator;
    }

    public java.lang.String getNamespaceURI() {
        return this.namespaceURI;
    }

    public java.lang.String getQName() {
        return this.qName;
    }
}
