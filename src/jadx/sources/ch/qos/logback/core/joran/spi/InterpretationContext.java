package ch.qos.logback.core.joran.spi;

public class InterpretationContext extends ch.qos.logback.core.spi.ContextAwareBase implements ch.qos.logback.core.spi.PropertyContainer {
    ch.qos.logback.core.joran.spi.DefaultNestedComponentRegistry defaultNestedComponentRegistry = new ch.qos.logback.core.joran.spi.DefaultNestedComponentRegistry();
    ch.qos.logback.core.joran.spi.Interpreter joranInterpreter;
    final java.util.List<ch.qos.logback.core.joran.event.InPlayListener> listenerList = new java.util.ArrayList();
    java.util.Map<java.lang.String, java.lang.Object> objectMap;
    java.util.Stack<java.lang.Object> objectStack;
    java.util.Map<java.lang.String, java.lang.String> propertiesMap;

    public InterpretationContext(ch.qos.logback.core.Context context, ch.qos.logback.core.joran.spi.Interpreter interpreter) {
        this.context = context;
        this.joranInterpreter = interpreter;
        this.objectStack = new java.util.Stack<>();
        this.objectMap = new java.util.HashMap(5);
        this.propertiesMap = new java.util.HashMap(5);
    }

    public void addInPlayListener(ch.qos.logback.core.joran.event.InPlayListener inPlayListener) {
        if (this.listenerList.contains(inPlayListener)) {
            addWarn("InPlayListener " + inPlayListener + " has been already registered");
        } else {
            this.listenerList.add(inPlayListener);
        }
    }

    public void addSubstitutionProperties(java.util.Properties properties) {
        if (properties != null) {
            for (java.lang.String str : properties.keySet()) {
                addSubstitutionProperty(str, properties.getProperty(str));
            }
        }
    }

    public void addSubstitutionProperty(java.lang.String str, java.lang.String str2) {
        if (str != null && str2 != null) {
            this.propertiesMap.put(str, str2.trim());
        }
    }

    /* access modifiers changed from: 0000 */
    public void fireInPlay(ch.qos.logback.core.joran.event.SaxEvent saxEvent) {
        for (ch.qos.logback.core.joran.event.InPlayListener inPlay : this.listenerList) {
            inPlay.inPlay(saxEvent);
        }
    }

    public java.util.Map<java.lang.String, java.lang.String> getCopyOfPropertyMap() {
        return new java.util.HashMap(this.propertiesMap);
    }

    public ch.qos.logback.core.joran.spi.DefaultNestedComponentRegistry getDefaultNestedComponentRegistry() {
        return this.defaultNestedComponentRegistry;
    }

    public ch.qos.logback.core.joran.spi.Interpreter getJoranInterpreter() {
        return this.joranInterpreter;
    }

    public org.xml.sax.Locator getLocator() {
        return this.joranInterpreter.getLocator();
    }

    public java.lang.Object getObject(int i) {
        return this.objectStack.get(i);
    }

    public java.util.Map<java.lang.String, java.lang.Object> getObjectMap() {
        return this.objectMap;
    }

    public java.util.Stack<java.lang.Object> getObjectStack() {
        return this.objectStack;
    }

    public java.lang.String getProperty(java.lang.String str) {
        java.lang.String str2 = (java.lang.String) this.propertiesMap.get(str);
        return str2 != null ? str2 : this.context.getProperty(str);
    }

    public boolean isEmpty() {
        return this.objectStack.isEmpty();
    }

    public boolean isListenerListEmpty() {
        return this.listenerList.isEmpty();
    }

    public java.lang.Object peekObject() {
        return this.objectStack.peek();
    }

    public java.lang.Object popObject() {
        return this.objectStack.pop();
    }

    public void pushObject(java.lang.Object obj) {
        this.objectStack.push(obj);
    }

    public boolean removeInPlayListener(ch.qos.logback.core.joran.event.InPlayListener inPlayListener) {
        return this.listenerList.remove(inPlayListener);
    }

    /* access modifiers changed from: 0000 */
    public void setPropertiesMap(java.util.Map<java.lang.String, java.lang.String> map) {
        this.propertiesMap = map;
    }

    public java.lang.String subst(java.lang.String str) {
        if (str == null) {
            return null;
        }
        return ch.qos.logback.core.util.OptionHelper.substVars(str, this, this.context);
    }

    /* access modifiers changed from: 0000 */
    public java.lang.String updateLocationInfo(java.lang.String str) {
        org.xml.sax.Locator locator = this.joranInterpreter.getLocator();
        return locator != null ? str + locator.getLineNumber() + ":" + locator.getColumnNumber() : str;
    }
}
