package ch.qos.logback.core.joran.spi;

public class JoranException extends java.lang.Exception {
    private static final long serialVersionUID = 1112493363728774021L;

    public JoranException(java.lang.String str) {
        super(str);
    }

    public JoranException(java.lang.String str, java.lang.Throwable th) {
        super(str, th);
    }
}
