package ch.qos.logback.core.joran.action;

public class NestedBasicPropertyIA extends ch.qos.logback.core.joran.action.ImplicitAction {
    java.util.Stack<ch.qos.logback.core.joran.action.IADataForBasicProperty> actionDataStack = new java.util.Stack<>();

    public void begin(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext, java.lang.String str, org.xml.sax.Attributes attributes) {
    }

    public void body(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext, java.lang.String str) {
        java.lang.String subst = interpretationContext.subst(str);
        ch.qos.logback.core.joran.action.IADataForBasicProperty iADataForBasicProperty = (ch.qos.logback.core.joran.action.IADataForBasicProperty) this.actionDataStack.peek();
        switch (iADataForBasicProperty.aggregationType) {
            case AS_BASIC_PROPERTY:
                iADataForBasicProperty.parentBean.setProperty(iADataForBasicProperty.propertyName, subst);
                return;
            case AS_BASIC_PROPERTY_COLLECTION:
                iADataForBasicProperty.parentBean.addBasicProperty(iADataForBasicProperty.propertyName, subst);
                return;
            default:
                return;
        }
    }

    public void end(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext, java.lang.String str) {
        this.actionDataStack.pop();
    }

    public boolean isApplicable(ch.qos.logback.core.joran.spi.ElementPath elementPath, org.xml.sax.Attributes attributes, ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext) {
        java.lang.String peekLast = elementPath.peekLast();
        if (interpretationContext.isEmpty()) {
            return false;
        }
        ch.qos.logback.core.joran.util.PropertySetter propertySetter = new ch.qos.logback.core.joran.util.PropertySetter(interpretationContext.peekObject());
        propertySetter.setContext(this.context);
        ch.qos.logback.core.util.AggregationType computeAggregationType = propertySetter.computeAggregationType(peekLast);
        switch (computeAggregationType) {
            case NOT_FOUND:
            case AS_COMPLEX_PROPERTY:
            case AS_COMPLEX_PROPERTY_COLLECTION:
                return false;
            case AS_BASIC_PROPERTY:
            case AS_BASIC_PROPERTY_COLLECTION:
                this.actionDataStack.push(new ch.qos.logback.core.joran.action.IADataForBasicProperty(propertySetter, computeAggregationType, peekLast));
                return true;
            default:
                addError("PropertySetter.canContainComponent returned " + computeAggregationType);
                return false;
        }
    }
}
