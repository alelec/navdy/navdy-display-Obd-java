package ch.qos.logback.core.joran.action;

public class AppenderAction<E> extends ch.qos.logback.core.joran.action.Action {
    ch.qos.logback.core.Appender<E> appender;
    private boolean inError = false;

    private void warnDeprecated(java.lang.String str) {
        if (str.equals("ch.qos.logback.core.ConsoleAppender")) {
            addWarn("ConsoleAppender is deprecated for LogcatAppender");
        }
    }

    public void begin(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext, java.lang.String str, org.xml.sax.Attributes attributes) throws ch.qos.logback.core.joran.spi.ActionException {
        this.appender = null;
        this.inError = false;
        java.lang.String value = attributes.getValue(ch.qos.logback.core.joran.action.Action.CLASS_ATTRIBUTE);
        if (ch.qos.logback.core.util.OptionHelper.isEmpty(value)) {
            addError("Missing class name for appender. Near [" + str + "] line " + getLineNumber(interpretationContext));
            this.inError = true;
            return;
        }
        try {
            addInfo("About to instantiate appender of type [" + value + "]");
            warnDeprecated(value);
            this.appender = (ch.qos.logback.core.Appender) ch.qos.logback.core.util.OptionHelper.instantiateByClassName(value, ch.qos.logback.core.Appender.class, this.context);
            this.appender.setContext(this.context);
            java.lang.String subst = interpretationContext.subst(attributes.getValue(ch.qos.logback.core.joran.action.Action.NAME_ATTRIBUTE));
            if (ch.qos.logback.core.util.OptionHelper.isEmpty(subst)) {
                addWarn("No appender name given for appender of type " + value + "].");
            } else {
                this.appender.setName(subst);
                addInfo("Naming appender as [" + subst + "]");
            }
            ((java.util.HashMap) interpretationContext.getObjectMap().get(ch.qos.logback.core.joran.action.ActionConst.APPENDER_BAG)).put(subst, this.appender);
            interpretationContext.pushObject(this.appender);
        } catch (java.lang.Exception e) {
            this.inError = true;
            addError("Could not create an Appender of type [" + value + "].", e);
            throw new ch.qos.logback.core.joran.spi.ActionException(e);
        }
    }

    public void end(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext, java.lang.String str) {
        if (!this.inError) {
            if (this.appender instanceof ch.qos.logback.core.spi.LifeCycle) {
                this.appender.start();
            }
            if (interpretationContext.peekObject() != this.appender) {
                addWarn("The object at the of the stack is not the appender named [" + this.appender.getName() + "] pushed earlier.");
            } else {
                interpretationContext.popObject();
            }
        }
    }
}
