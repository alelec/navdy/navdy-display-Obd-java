package ch.qos.logback.core.joran.util;

public class IntrospectionException extends java.lang.RuntimeException {
    private static final long serialVersionUID = -6760181416658938878L;

    public IntrospectionException(java.lang.Exception exc) {
        super(exc);
    }

    public IntrospectionException(java.lang.String str) {
        super(str);
    }
}
