package ch.qos.logback.core.joran.spi;

@java.lang.Deprecated
public enum ConsoleTarget {
    SystemOut("System.out", new java.io.OutputStream() {
        public void flush() throws java.io.IOException {
            java.lang.System.out.flush();
        }

        public void write(int i) throws java.io.IOException {
            java.lang.System.out.write(i);
        }

        public void write(byte[] bArr) throws java.io.IOException {
            java.lang.System.out.write(bArr);
        }

        public void write(byte[] bArr, int i, int i2) throws java.io.IOException {
            java.lang.System.out.write(bArr, i, i2);
        }
    }),
    SystemErr("System.err", new java.io.OutputStream() {
        public void flush() throws java.io.IOException {
            java.lang.System.err.flush();
        }

        public void write(int i) throws java.io.IOException {
            java.lang.System.err.write(i);
        }

        public void write(byte[] bArr) throws java.io.IOException {
            java.lang.System.err.write(bArr);
        }

        public void write(byte[] bArr, int i, int i2) throws java.io.IOException {
            java.lang.System.err.write(bArr, i, i2);
        }
    });
    
    private final java.lang.String name;
    private final java.io.OutputStream stream;

    private ConsoleTarget(java.lang.String str, java.io.OutputStream outputStream) {
        this.name = str;
        this.stream = outputStream;
    }

    public static ch.qos.logback.core.joran.spi.ConsoleTarget findByName(java.lang.String str) {
        ch.qos.logback.core.joran.spi.ConsoleTarget[] values;
        for (ch.qos.logback.core.joran.spi.ConsoleTarget consoleTarget : values()) {
            if (consoleTarget.name.equalsIgnoreCase(str)) {
                return consoleTarget;
            }
        }
        return null;
    }

    public java.lang.String getName() {
        return this.name;
    }

    public java.io.OutputStream getStream() {
        return this.stream;
    }
}
