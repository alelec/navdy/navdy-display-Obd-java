package ch.qos.logback.core.joran.spi;

public class DefaultNestedComponentRegistry {
    java.util.Map<ch.qos.logback.core.joran.spi.HostClassAndPropertyDouble, java.lang.Class<?>> defaultComponentMap = new java.util.HashMap();

    private java.lang.Class<?> oneShotFind(java.lang.Class<?> cls, java.lang.String str) {
        return (java.lang.Class) this.defaultComponentMap.get(new ch.qos.logback.core.joran.spi.HostClassAndPropertyDouble(cls, str));
    }

    public void add(java.lang.Class<?> cls, java.lang.String str, java.lang.Class<?> cls2) {
        this.defaultComponentMap.put(new ch.qos.logback.core.joran.spi.HostClassAndPropertyDouble(cls, str.toLowerCase()), cls2);
    }

    public java.lang.Class<?> findDefaultComponentType(java.lang.Class<?> cls, java.lang.String str) {
        java.lang.String lowerCase = str.toLowerCase();
        while (cls != null) {
            java.lang.Class<?> oneShotFind = oneShotFind(cls, lowerCase);
            if (oneShotFind != null) {
                return oneShotFind;
            }
            cls = cls.getSuperclass();
        }
        return null;
    }
}
