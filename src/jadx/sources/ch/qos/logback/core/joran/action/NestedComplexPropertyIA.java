package ch.qos.logback.core.joran.action;

public class NestedComplexPropertyIA extends ch.qos.logback.core.joran.action.ImplicitAction {
    java.util.Stack<ch.qos.logback.core.joran.action.IADataForComplexProperty> actionDataStack = new java.util.Stack<>();

    public void begin(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext, java.lang.String str, org.xml.sax.Attributes attributes) {
        ch.qos.logback.core.joran.action.IADataForComplexProperty iADataForComplexProperty = (ch.qos.logback.core.joran.action.IADataForComplexProperty) this.actionDataStack.peek();
        java.lang.String subst = interpretationContext.subst(attributes.getValue(ch.qos.logback.core.joran.action.Action.CLASS_ATTRIBUTE));
        try {
            java.lang.Class classNameViaImplicitRules = !ch.qos.logback.core.util.OptionHelper.isEmpty(subst) ? ch.qos.logback.core.util.Loader.loadClass(subst, this.context) : iADataForComplexProperty.parentBean.getClassNameViaImplicitRules(iADataForComplexProperty.getComplexPropertyName(), iADataForComplexProperty.getAggregationType(), interpretationContext.getDefaultNestedComponentRegistry());
            if (classNameViaImplicitRules == null) {
                iADataForComplexProperty.inError = true;
                addError("Could not find an appropriate class for property [" + str + "]");
                return;
            }
            if (ch.qos.logback.core.util.OptionHelper.isEmpty(subst)) {
                addInfo("Assuming default type [" + classNameViaImplicitRules.getName() + "] for [" + str + "] property");
            }
            iADataForComplexProperty.setNestedComplexProperty(classNameViaImplicitRules.newInstance());
            if (iADataForComplexProperty.getNestedComplexProperty() instanceof ch.qos.logback.core.spi.ContextAware) {
                ((ch.qos.logback.core.spi.ContextAware) iADataForComplexProperty.getNestedComplexProperty()).setContext(this.context);
            }
            interpretationContext.pushObject(iADataForComplexProperty.getNestedComplexProperty());
        } catch (java.lang.Exception e) {
            iADataForComplexProperty.inError = true;
            addError("Could not create component [" + str + "] of type [" + subst + "]", e);
        }
    }

    public void end(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext, java.lang.String str) {
        ch.qos.logback.core.joran.action.IADataForComplexProperty iADataForComplexProperty = (ch.qos.logback.core.joran.action.IADataForComplexProperty) this.actionDataStack.pop();
        if (!iADataForComplexProperty.inError) {
            ch.qos.logback.core.joran.util.PropertySetter propertySetter = new ch.qos.logback.core.joran.util.PropertySetter(iADataForComplexProperty.getNestedComplexProperty());
            propertySetter.setContext(this.context);
            if (propertySetter.computeAggregationType("parent") == ch.qos.logback.core.util.AggregationType.AS_COMPLEX_PROPERTY) {
                propertySetter.setComplexProperty("parent", iADataForComplexProperty.parentBean.getObj());
            }
            java.lang.Object nestedComplexProperty = iADataForComplexProperty.getNestedComplexProperty();
            if ((nestedComplexProperty instanceof ch.qos.logback.core.spi.LifeCycle) && ch.qos.logback.core.joran.spi.NoAutoStartUtil.notMarkedWithNoAutoStart(nestedComplexProperty)) {
                ((ch.qos.logback.core.spi.LifeCycle) nestedComplexProperty).start();
            }
            if (interpretationContext.peekObject() != iADataForComplexProperty.getNestedComplexProperty()) {
                addError("The object on the top the of the stack is not the component pushed earlier.");
                return;
            }
            interpretationContext.popObject();
            switch (iADataForComplexProperty.aggregationType) {
                case AS_COMPLEX_PROPERTY_COLLECTION:
                    iADataForComplexProperty.parentBean.addComplexProperty(str, iADataForComplexProperty.getNestedComplexProperty());
                    return;
                case AS_COMPLEX_PROPERTY:
                    iADataForComplexProperty.parentBean.setComplexProperty(str, iADataForComplexProperty.getNestedComplexProperty());
                    return;
                default:
                    return;
            }
        }
    }

    public boolean isApplicable(ch.qos.logback.core.joran.spi.ElementPath elementPath, org.xml.sax.Attributes attributes, ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext) {
        java.lang.String peekLast = elementPath.peekLast();
        if (interpretationContext.isEmpty()) {
            return false;
        }
        ch.qos.logback.core.joran.util.PropertySetter propertySetter = new ch.qos.logback.core.joran.util.PropertySetter(interpretationContext.peekObject());
        propertySetter.setContext(this.context);
        ch.qos.logback.core.util.AggregationType computeAggregationType = propertySetter.computeAggregationType(peekLast);
        switch (computeAggregationType) {
            case NOT_FOUND:
            case AS_BASIC_PROPERTY:
            case AS_BASIC_PROPERTY_COLLECTION:
                return false;
            case AS_COMPLEX_PROPERTY_COLLECTION:
            case AS_COMPLEX_PROPERTY:
                this.actionDataStack.push(new ch.qos.logback.core.joran.action.IADataForComplexProperty(propertySetter, computeAggregationType, peekLast));
                return true;
            default:
                addError("PropertySetter.computeAggregationType returned " + computeAggregationType);
                return false;
        }
    }
}
