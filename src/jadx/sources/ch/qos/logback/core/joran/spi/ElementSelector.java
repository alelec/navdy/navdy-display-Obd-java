package ch.qos.logback.core.joran.spi;

public class ElementSelector extends ch.qos.logback.core.joran.spi.ElementPath {
    public ElementSelector() {
    }

    public ElementSelector(java.lang.String str) {
        super(str);
    }

    public ElementSelector(java.util.List<java.lang.String> list) {
        super(list);
    }

    private boolean equalityCheck(java.lang.String str, java.lang.String str2) {
        return str.equalsIgnoreCase(str2);
    }

    public boolean equals(java.lang.Object obj) {
        if (obj == null || !(obj instanceof ch.qos.logback.core.joran.spi.ElementSelector)) {
            return false;
        }
        ch.qos.logback.core.joran.spi.ElementSelector elementSelector = (ch.qos.logback.core.joran.spi.ElementSelector) obj;
        if (elementSelector.size() != size()) {
            return false;
        }
        int size = size();
        for (int i = 0; i < size; i++) {
            if (!equalityCheck(get(i), elementSelector.get(i))) {
                return false;
            }
        }
        return true;
    }

    public boolean fullPathMatch(ch.qos.logback.core.joran.spi.ElementPath elementPath) {
        if (elementPath.size() != size()) {
            return false;
        }
        int size = size();
        for (int i = 0; i < size; i++) {
            if (!equalityCheck(get(i), elementPath.get(i))) {
                return false;
            }
        }
        return true;
    }

    public int getPrefixMatchLength(ch.qos.logback.core.joran.spi.ElementPath elementPath) {
        int i = 0;
        if (elementPath == null) {
            return 0;
        }
        int size = this.partList.size();
        int size2 = elementPath.partList.size();
        if (size == 0 || size2 == 0) {
            return 0;
        }
        int i2 = size <= size2 ? size : size2;
        int i3 = 0;
        while (i3 < i2 && equalityCheck((java.lang.String) this.partList.get(i3), (java.lang.String) elementPath.partList.get(i3))) {
            i3++;
            i++;
        }
        return i;
    }

    public int getTailMatchLength(ch.qos.logback.core.joran.spi.ElementPath elementPath) {
        if (elementPath == null) {
            return 0;
        }
        int size = this.partList.size();
        int size2 = elementPath.partList.size();
        if (size == 0 || size2 == 0) {
            return 0;
        }
        int i = size <= size2 ? size : size2;
        int i2 = 1;
        int i3 = 0;
        while (i2 <= i && equalityCheck((java.lang.String) this.partList.get(size - i2), (java.lang.String) elementPath.partList.get(size2 - i2))) {
            i3++;
            i2++;
        }
        return i3;
    }

    public int hashCode() {
        int i = 0;
        for (int i2 = 0; i2 < size(); i2++) {
            i ^= get(i2).toLowerCase().hashCode();
        }
        return i;
    }

    public boolean isContainedIn(ch.qos.logback.core.joran.spi.ElementPath elementPath) {
        if (elementPath == null) {
            return false;
        }
        return elementPath.toStableString().contains(toStableString());
    }
}
