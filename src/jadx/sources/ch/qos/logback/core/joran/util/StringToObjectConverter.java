package ch.qos.logback.core.joran.util;

public class StringToObjectConverter {
    private static final java.lang.Class<?>[] STING_CLASS_PARAMETER = {java.lang.String.class};

    public static boolean canBeBuiltFromSimpleString(java.lang.Class<?> cls) {
        java.lang.Package packageR = cls.getPackage();
        if (cls.isPrimitive()) {
            return true;
        }
        return (packageR != null && "java.lang".equals(packageR.getName())) || followsTheValueOfConvention(cls) || cls.isEnum() || isOfTypeCharset(cls);
    }

    public static java.lang.Object convertArg(ch.qos.logback.core.spi.ContextAware contextAware, java.lang.String str, java.lang.Class<?> cls) {
        if (str == null) {
            return null;
        }
        java.lang.String trim = str.trim();
        if (java.lang.String.class.isAssignableFrom(cls)) {
            return trim;
        }
        if (java.lang.Integer.TYPE.isAssignableFrom(cls)) {
            return new java.lang.Integer(trim);
        }
        if (java.lang.Long.TYPE.isAssignableFrom(cls)) {
            return new java.lang.Long(trim);
        }
        if (java.lang.Float.TYPE.isAssignableFrom(cls)) {
            return new java.lang.Float(trim);
        }
        if (java.lang.Double.TYPE.isAssignableFrom(cls)) {
            return new java.lang.Double(trim);
        }
        if (java.lang.Boolean.TYPE.isAssignableFrom(cls)) {
            if ("true".equalsIgnoreCase(trim)) {
                return java.lang.Boolean.TRUE;
            }
            if ("false".equalsIgnoreCase(trim)) {
                return java.lang.Boolean.FALSE;
            }
            return null;
        } else if (cls.isEnum()) {
            return convertToEnum(contextAware, trim, cls);
        } else {
            if (followsTheValueOfConvention(cls)) {
                return convertByValueOfMethod(contextAware, cls, trim);
            }
            if (isOfTypeCharset(cls)) {
                return convertToCharset(contextAware, str);
            }
            return null;
        }
    }

    private static java.lang.Object convertByValueOfMethod(ch.qos.logback.core.spi.ContextAware contextAware, java.lang.Class<?> cls, java.lang.String str) {
        boolean z = false;
        try {
            return cls.getMethod(ch.qos.logback.core.CoreConstants.VALUE_OF, STING_CLASS_PARAMETER).invoke(null, new java.lang.Object[]{str});
        } catch (java.lang.Exception e) {
            contextAware.addError("Failed to invoke valueOf{} method in class [" + cls.getName() + "] with value [" + str + "]");
            return z;
        }
    }

    private static java.nio.charset.Charset convertToCharset(ch.qos.logback.core.spi.ContextAware contextAware, java.lang.String str) {
        try {
            return java.nio.charset.Charset.forName(str);
        } catch (java.nio.charset.UnsupportedCharsetException e) {
            contextAware.addError("Failed to get charset [" + str + "]", e);
            return null;
        }
    }

    private static java.lang.Object convertToEnum(ch.qos.logback.core.spi.ContextAware contextAware, java.lang.String str, java.lang.Class<? extends java.lang.Enum> cls) {
        return java.lang.Enum.valueOf(cls, str);
    }

    private static boolean followsTheValueOfConvention(java.lang.Class<?> cls) {
        try {
            if (java.lang.reflect.Modifier.isStatic(cls.getMethod(ch.qos.logback.core.CoreConstants.VALUE_OF, STING_CLASS_PARAMETER).getModifiers())) {
                return true;
            }
        } catch (java.lang.NoSuchMethodException | java.lang.SecurityException e) {
        }
        return false;
    }

    private static boolean isOfTypeCharset(java.lang.Class<?> cls) {
        return java.nio.charset.Charset.class.isAssignableFrom(cls);
    }

    /* access modifiers changed from: 0000 */
    public boolean isBuildableFromSimpleString() {
        return false;
    }
}
