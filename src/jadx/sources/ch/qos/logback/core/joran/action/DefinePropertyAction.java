package ch.qos.logback.core.joran.action;

public class DefinePropertyAction extends ch.qos.logback.core.joran.action.Action {
    ch.qos.logback.core.spi.PropertyDefiner definer;
    boolean inError;
    java.lang.String propertyName;
    ch.qos.logback.core.joran.action.ActionUtil.Scope scope;
    java.lang.String scopeStr;

    public void begin(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext, java.lang.String str, org.xml.sax.Attributes attributes) throws ch.qos.logback.core.joran.spi.ActionException {
        this.scopeStr = null;
        this.scope = null;
        this.propertyName = null;
        this.definer = null;
        this.inError = false;
        this.propertyName = attributes.getValue(ch.qos.logback.core.joran.action.Action.NAME_ATTRIBUTE);
        this.scopeStr = attributes.getValue(ch.qos.logback.core.joran.action.Action.SCOPE_ATTRIBUTE);
        this.scope = ch.qos.logback.core.joran.action.ActionUtil.stringToScope(this.scopeStr);
        if (ch.qos.logback.core.util.OptionHelper.isEmpty(this.propertyName)) {
            addError("Missing property name for property definer. Near [" + str + "] line " + getLineNumber(interpretationContext));
            this.inError = true;
            return;
        }
        java.lang.String value = attributes.getValue(ch.qos.logback.core.joran.action.Action.CLASS_ATTRIBUTE);
        if (ch.qos.logback.core.util.OptionHelper.isEmpty(value)) {
            addError("Missing class name for property definer. Near [" + str + "] line " + getLineNumber(interpretationContext));
            this.inError = true;
            return;
        }
        try {
            addInfo("About to instantiate property definer of type [" + value + "]");
            this.definer = (ch.qos.logback.core.spi.PropertyDefiner) ch.qos.logback.core.util.OptionHelper.instantiateByClassName(value, ch.qos.logback.core.spi.PropertyDefiner.class, this.context);
            this.definer.setContext(this.context);
            if (this.definer instanceof ch.qos.logback.core.spi.LifeCycle) {
                ((ch.qos.logback.core.spi.LifeCycle) this.definer).start();
            }
            interpretationContext.pushObject(this.definer);
        } catch (java.lang.Exception e) {
            this.inError = true;
            addError("Could not create an PropertyDefiner of type [" + value + "].", e);
            throw new ch.qos.logback.core.joran.spi.ActionException(e);
        }
    }

    public void end(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext, java.lang.String str) {
        if (!this.inError) {
            if (interpretationContext.peekObject() != this.definer) {
                addWarn("The object at the of the stack is not the property definer for property named [" + this.propertyName + "] pushed earlier.");
                return;
            }
            addInfo("Popping property definer for property named [" + this.propertyName + "] from the object stack");
            interpretationContext.popObject();
            java.lang.String propertyValue = this.definer.getPropertyValue();
            if (propertyValue != null) {
                ch.qos.logback.core.joran.action.ActionUtil.setProperty(interpretationContext, this.propertyName, propertyValue, this.scope);
            }
        }
    }
}
