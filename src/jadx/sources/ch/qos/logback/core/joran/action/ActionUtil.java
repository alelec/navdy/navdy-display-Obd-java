package ch.qos.logback.core.joran.action;

public class ActionUtil {

    public enum Scope {
        LOCAL,
        CONTEXT,
        SYSTEM
    }

    public static void setProperties(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext, java.util.Properties properties, ch.qos.logback.core.joran.action.ActionUtil.Scope scope) {
        switch (scope) {
            case LOCAL:
                interpretationContext.addSubstitutionProperties(properties);
                return;
            case CONTEXT:
                new ch.qos.logback.core.util.ContextUtil(interpretationContext.getContext()).addProperties(properties);
                return;
            case SYSTEM:
                ch.qos.logback.core.util.OptionHelper.setSystemProperties(interpretationContext, properties);
                return;
            default:
                return;
        }
    }

    public static void setProperty(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext, java.lang.String str, java.lang.String str2, ch.qos.logback.core.joran.action.ActionUtil.Scope scope) {
        switch (scope) {
            case LOCAL:
                interpretationContext.addSubstitutionProperty(str, str2);
                return;
            case CONTEXT:
                interpretationContext.getContext().putProperty(str, str2);
                return;
            case SYSTEM:
                ch.qos.logback.core.util.OptionHelper.setSystemProperty(interpretationContext, str, str2);
                return;
            default:
                return;
        }
    }

    public static ch.qos.logback.core.joran.action.ActionUtil.Scope stringToScope(java.lang.String str) {
        return ch.qos.logback.core.joran.action.ActionUtil.Scope.SYSTEM.toString().equalsIgnoreCase(str) ? ch.qos.logback.core.joran.action.ActionUtil.Scope.SYSTEM : ch.qos.logback.core.joran.action.ActionUtil.Scope.CONTEXT.toString().equalsIgnoreCase(str) ? ch.qos.logback.core.joran.action.ActionUtil.Scope.CONTEXT : ch.qos.logback.core.joran.action.ActionUtil.Scope.LOCAL;
    }
}
