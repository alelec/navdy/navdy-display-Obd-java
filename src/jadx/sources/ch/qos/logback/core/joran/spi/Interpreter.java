package ch.qos.logback.core.joran.spi;

public class Interpreter {
    private static java.util.List<ch.qos.logback.core.joran.action.Action> EMPTY_LIST = new java.util.Vector(0);
    java.util.Stack<java.util.List<ch.qos.logback.core.joran.action.Action>> actionListStack;
    private final ch.qos.logback.core.joran.spi.CAI_WithLocatorSupport cai;
    private ch.qos.logback.core.joran.spi.ElementPath elementPath;
    ch.qos.logback.core.joran.spi.EventPlayer eventPlayer;
    private final java.util.ArrayList<ch.qos.logback.core.joran.action.ImplicitAction> implicitActions;
    private final ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext;
    org.xml.sax.Locator locator;
    private final ch.qos.logback.core.joran.spi.RuleStore ruleStore;
    ch.qos.logback.core.joran.spi.ElementPath skip = null;

    public Interpreter(ch.qos.logback.core.Context context, ch.qos.logback.core.joran.spi.RuleStore ruleStore2, ch.qos.logback.core.joran.spi.ElementPath elementPath2) {
        this.cai = new ch.qos.logback.core.joran.spi.CAI_WithLocatorSupport(context, this);
        this.ruleStore = ruleStore2;
        this.interpretationContext = new ch.qos.logback.core.joran.spi.InterpretationContext(context, this);
        this.implicitActions = new java.util.ArrayList<>(3);
        this.elementPath = elementPath2;
        this.actionListStack = new java.util.Stack<>();
        this.eventPlayer = new ch.qos.logback.core.joran.spi.EventPlayer(this);
    }

    private void callBodyAction(java.util.List<ch.qos.logback.core.joran.action.Action> list, java.lang.String str) {
        if (list != null) {
            for (ch.qos.logback.core.joran.action.Action action : list) {
                try {
                    action.body(this.interpretationContext, str);
                } catch (ch.qos.logback.core.joran.spi.ActionException e) {
                    this.cai.addError("Exception in end() methd for action [" + action + "]", e);
                }
            }
        }
    }

    private void callEndAction(java.util.List<ch.qos.logback.core.joran.action.Action> list, java.lang.String str) {
        if (list != null) {
            for (ch.qos.logback.core.joran.action.Action end : list) {
                try {
                    end.end(this.interpretationContext, str);
                } catch (ch.qos.logback.core.joran.spi.ActionException e) {
                    this.cai.addError("ActionException in Action for tag [" + str + "]", e);
                } catch (java.lang.RuntimeException e2) {
                    this.cai.addError("RuntimeException in Action for tag [" + str + "]", e2);
                }
            }
        }
    }

    private void endElement(java.lang.String str, java.lang.String str2, java.lang.String str3) {
        java.util.List<ch.qos.logback.core.joran.action.Action> list = (java.util.List) this.actionListStack.pop();
        if (this.skip != null) {
            if (this.skip.equals(this.elementPath)) {
                this.skip = null;
            }
        } else if (list != EMPTY_LIST) {
            callEndAction(list, getTagName(str2, str3));
        }
        this.elementPath.pop();
    }

    private void pushEmptyActionList() {
        this.actionListStack.add(EMPTY_LIST);
    }

    private void startElement(java.lang.String str, java.lang.String str2, java.lang.String str3, org.xml.sax.Attributes attributes) {
        java.lang.String tagName = getTagName(str2, str3);
        this.elementPath.push(tagName);
        if (this.skip != null) {
            pushEmptyActionList();
            return;
        }
        java.util.List applicableActionList = getApplicableActionList(this.elementPath, attributes);
        if (applicableActionList != null) {
            this.actionListStack.add(applicableActionList);
            callBeginAction(applicableActionList, tagName, attributes);
            return;
        }
        pushEmptyActionList();
        this.cai.addError("no applicable action for [" + tagName + "], current ElementPath  is [" + this.elementPath + "]");
    }

    public void addImplicitAction(ch.qos.logback.core.joran.action.ImplicitAction implicitAction) {
        this.implicitActions.add(implicitAction);
    }

    /* access modifiers changed from: 0000 */
    public void callBeginAction(java.util.List<ch.qos.logback.core.joran.action.Action> list, java.lang.String str, org.xml.sax.Attributes attributes) {
        if (list != null) {
            for (ch.qos.logback.core.joran.action.Action begin : list) {
                try {
                    begin.begin(this.interpretationContext, str, attributes);
                } catch (ch.qos.logback.core.joran.spi.ActionException e) {
                    this.skip = this.elementPath.duplicate();
                    this.cai.addError("ActionException in Action for tag [" + str + "]", e);
                } catch (java.lang.RuntimeException e2) {
                    this.skip = this.elementPath.duplicate();
                    this.cai.addError("RuntimeException in Action for tag [" + str + "]", e2);
                }
            }
        }
    }

    public void characters(ch.qos.logback.core.joran.event.BodyEvent bodyEvent) {
        setDocumentLocator(bodyEvent.locator);
        java.lang.String text = bodyEvent.getText();
        java.util.List list = (java.util.List) this.actionListStack.peek();
        if (text != null) {
            java.lang.String trim = text.trim();
            if (trim.length() > 0) {
                callBodyAction(list, trim);
            }
        }
    }

    public void endElement(ch.qos.logback.core.joran.event.EndEvent endEvent) {
        setDocumentLocator(endEvent.locator);
        endElement(endEvent.namespaceURI, endEvent.localName, endEvent.qName);
    }

    /* access modifiers changed from: 0000 */
    public java.util.List<ch.qos.logback.core.joran.action.Action> getApplicableActionList(ch.qos.logback.core.joran.spi.ElementPath elementPath2, org.xml.sax.Attributes attributes) {
        java.util.List<ch.qos.logback.core.joran.action.Action> matchActions = this.ruleStore.matchActions(elementPath2);
        return matchActions == null ? lookupImplicitAction(elementPath2, attributes, this.interpretationContext) : matchActions;
    }

    public ch.qos.logback.core.joran.spi.EventPlayer getEventPlayer() {
        return this.eventPlayer;
    }

    public ch.qos.logback.core.joran.spi.InterpretationContext getExecutionContext() {
        return getInterpretationContext();
    }

    public ch.qos.logback.core.joran.spi.InterpretationContext getInterpretationContext() {
        return this.interpretationContext;
    }

    public org.xml.sax.Locator getLocator() {
        return this.locator;
    }

    public ch.qos.logback.core.joran.spi.RuleStore getRuleStore() {
        return this.ruleStore;
    }

    /* access modifiers changed from: 0000 */
    public java.lang.String getTagName(java.lang.String str, java.lang.String str2) {
        return (str == null || str.length() < 1) ? str2 : str;
    }

    /* access modifiers changed from: 0000 */
    public java.util.List<ch.qos.logback.core.joran.action.Action> lookupImplicitAction(ch.qos.logback.core.joran.spi.ElementPath elementPath2, org.xml.sax.Attributes attributes, ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext2) {
        int size = this.implicitActions.size();
        for (int i = 0; i < size; i++) {
            ch.qos.logback.core.joran.action.ImplicitAction implicitAction = (ch.qos.logback.core.joran.action.ImplicitAction) this.implicitActions.get(i);
            if (implicitAction.isApplicable(elementPath2, attributes, interpretationContext2)) {
                java.util.ArrayList arrayList = new java.util.ArrayList(1);
                arrayList.add(implicitAction);
                return arrayList;
            }
        }
        return null;
    }

    public void setDocumentLocator(org.xml.sax.Locator locator2) {
        this.locator = locator2;
    }

    public void setInterpretationContextPropertiesMap(java.util.Map<java.lang.String, java.lang.String> map) {
        this.interpretationContext.setPropertiesMap(map);
    }

    public void startDocument() {
    }

    public void startElement(ch.qos.logback.core.joran.event.StartEvent startEvent) {
        setDocumentLocator(startEvent.getLocator());
        startElement(startEvent.namespaceURI, startEvent.localName, startEvent.qName, startEvent.attributes);
    }
}
