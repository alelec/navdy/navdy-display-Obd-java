package ch.qos.logback.core.joran.spi;

public class SimpleRuleStore extends ch.qos.logback.core.spi.ContextAwareBase implements ch.qos.logback.core.joran.spi.RuleStore {
    static java.lang.String KLEENE_STAR = org.slf4j.Marker.ANY_MARKER;
    java.util.HashMap<ch.qos.logback.core.joran.spi.ElementSelector, java.util.List<ch.qos.logback.core.joran.action.Action>> rules = new java.util.HashMap<>();

    public SimpleRuleStore(ch.qos.logback.core.Context context) {
        setContext(context);
    }

    private boolean isKleeneStar(java.lang.String str) {
        return KLEENE_STAR.equals(str);
    }

    private boolean isSuffixPattern(ch.qos.logback.core.joran.spi.ElementSelector elementSelector) {
        return elementSelector.size() > 1 && elementSelector.get(0).equals(KLEENE_STAR);
    }

    public void addRule(ch.qos.logback.core.joran.spi.ElementSelector elementSelector, ch.qos.logback.core.joran.action.Action action) {
        action.setContext(this.context);
        java.util.List list = (java.util.List) this.rules.get(elementSelector);
        if (list == null) {
            list = new java.util.ArrayList();
            this.rules.put(elementSelector, list);
        }
        list.add(action);
    }

    public void addRule(ch.qos.logback.core.joran.spi.ElementSelector elementSelector, java.lang.String str) {
        ch.qos.logback.core.joran.action.Action action;
        try {
            action = (ch.qos.logback.core.joran.action.Action) ch.qos.logback.core.util.OptionHelper.instantiateByClassName(str, ch.qos.logback.core.joran.action.Action.class, this.context);
        } catch (java.lang.Exception e) {
            addError("Could not instantiate class [" + str + "]", e);
            action = null;
        }
        if (action != null) {
            addRule(elementSelector, action);
        }
    }

    /* access modifiers changed from: 0000 */
    public java.util.List<ch.qos.logback.core.joran.action.Action> fullPathMatch(ch.qos.logback.core.joran.spi.ElementPath elementPath) {
        for (ch.qos.logback.core.joran.spi.ElementSelector elementSelector : this.rules.keySet()) {
            if (elementSelector.fullPathMatch(elementPath)) {
                return (java.util.List) this.rules.get(elementSelector);
            }
        }
        return null;
    }

    public java.util.List<ch.qos.logback.core.joran.action.Action> matchActions(ch.qos.logback.core.joran.spi.ElementPath elementPath) {
        java.util.List<ch.qos.logback.core.joran.action.Action> fullPathMatch = fullPathMatch(elementPath);
        if (fullPathMatch != null) {
            return fullPathMatch;
        }
        java.util.List<ch.qos.logback.core.joran.action.Action> suffixMatch = suffixMatch(elementPath);
        if (suffixMatch != null) {
            return suffixMatch;
        }
        java.util.List<ch.qos.logback.core.joran.action.Action> prefixMatch = prefixMatch(elementPath);
        if (prefixMatch != null) {
            return prefixMatch;
        }
        java.util.List<ch.qos.logback.core.joran.action.Action> middleMatch = middleMatch(elementPath);
        if (middleMatch == null) {
            return null;
        }
        return middleMatch;
    }

    /* access modifiers changed from: 0000 */
    public java.util.List<ch.qos.logback.core.joran.action.Action> middleMatch(ch.qos.logback.core.joran.spi.ElementPath elementPath) {
        int i;
        ch.qos.logback.core.joran.spi.ElementSelector elementSelector = null;
        int i2 = 0;
        for (ch.qos.logback.core.joran.spi.ElementSelector elementSelector2 : this.rules.keySet()) {
            java.lang.String peekLast = elementSelector2.peekLast();
            java.lang.String str = elementSelector2.size() > 1 ? elementSelector2.get(0) : null;
            if (isKleeneStar(peekLast) && isKleeneStar(str)) {
                java.util.List copyOfPartList = elementSelector2.getCopyOfPartList();
                if (copyOfPartList.size() > 2) {
                    copyOfPartList.remove(0);
                    copyOfPartList.remove(copyOfPartList.size() - 1);
                }
                ch.qos.logback.core.joran.spi.ElementSelector elementSelector3 = new ch.qos.logback.core.joran.spi.ElementSelector(copyOfPartList);
                int i3 = elementSelector3.isContainedIn(elementPath) ? elementSelector3.size() : 0;
                if (i3 > i2) {
                    i = i3;
                    i2 = i;
                    elementSelector = elementSelector2;
                }
            }
            elementSelector2 = elementSelector;
            i = i2;
            i2 = i;
            elementSelector = elementSelector2;
        }
        if (elementSelector != null) {
            return (java.util.List) this.rules.get(elementSelector);
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    public java.util.List<ch.qos.logback.core.joran.action.Action> prefixMatch(ch.qos.logback.core.joran.spi.ElementPath elementPath) {
        int i;
        int i2 = 0;
        ch.qos.logback.core.joran.spi.ElementSelector elementSelector = null;
        for (ch.qos.logback.core.joran.spi.ElementSelector elementSelector2 : this.rules.keySet()) {
            if (isKleeneStar(elementSelector2.peekLast())) {
                int prefixMatchLength = elementSelector2.getPrefixMatchLength(elementPath);
                if (prefixMatchLength == elementSelector2.size() - 1 && prefixMatchLength > i2) {
                    i = prefixMatchLength;
                    i2 = i;
                    elementSelector = elementSelector2;
                }
            }
            elementSelector2 = elementSelector;
            i = i2;
            i2 = i;
            elementSelector = elementSelector2;
        }
        if (elementSelector != null) {
            return (java.util.List) this.rules.get(elementSelector);
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    public java.util.List<ch.qos.logback.core.joran.action.Action> suffixMatch(ch.qos.logback.core.joran.spi.ElementPath elementPath) {
        int i;
        int i2 = 0;
        ch.qos.logback.core.joran.spi.ElementSelector elementSelector = null;
        for (ch.qos.logback.core.joran.spi.ElementSelector elementSelector2 : this.rules.keySet()) {
            if (isSuffixPattern(elementSelector2)) {
                int tailMatchLength = elementSelector2.getTailMatchLength(elementPath);
                if (tailMatchLength > i2) {
                    i = tailMatchLength;
                    i2 = i;
                    elementSelector = elementSelector2;
                }
            }
            elementSelector2 = elementSelector;
            i = i2;
            i2 = i;
            elementSelector = elementSelector2;
        }
        if (elementSelector != null) {
            return (java.util.List) this.rules.get(elementSelector);
        }
        return null;
    }

    public java.lang.String toString() {
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        sb.append("SimpleRuleStore ( ").append("rules = ").append(this.rules).append("  ").append(" )");
        return sb.toString();
    }
}
