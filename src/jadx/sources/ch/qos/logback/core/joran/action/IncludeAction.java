package ch.qos.logback.core.joran.action;

public class IncludeAction extends ch.qos.logback.core.joran.action.AbstractIncludeAction {
    private static final java.lang.String CONFIG_TAG = "configuration";
    private static final java.lang.String INCLUDED_TAG = "included";
    private int eventOffset = 2;

    private java.lang.String getEventName(ch.qos.logback.core.joran.event.SaxEvent saxEvent) {
        return saxEvent.qName.length() > 0 ? saxEvent.qName : saxEvent.localName;
    }

    private java.io.InputStream openURL(java.net.URL url) {
        try {
            return url.openStream();
        } catch (java.io.IOException e) {
            if (!isOptional()) {
                addError("Failed to open [" + url.toString() + "]", e);
            }
            return null;
        }
    }

    private void trimHeadAndTail(ch.qos.logback.core.joran.event.SaxEventRecorder saxEventRecorder) {
        boolean z;
        boolean z2;
        java.util.List saxEventList = saxEventRecorder.getSaxEventList();
        if (saxEventList.size() != 0) {
            ch.qos.logback.core.joran.event.SaxEvent saxEvent = (ch.qos.logback.core.joran.event.SaxEvent) saxEventList.get(0);
            if (saxEvent != null) {
                java.lang.String eventName = getEventName(saxEvent);
                boolean equalsIgnoreCase = INCLUDED_TAG.equalsIgnoreCase(eventName);
                z2 = equalsIgnoreCase;
                z = CONFIG_TAG.equalsIgnoreCase(eventName);
            } else {
                z = false;
                z2 = false;
            }
            if (z2 || z) {
                saxEventList.remove(0);
                int size = saxEventList.size();
                if (size != 0) {
                    int i = size - 1;
                    ch.qos.logback.core.joran.event.SaxEvent saxEvent2 = (ch.qos.logback.core.joran.event.SaxEvent) saxEventList.get(i);
                    if (saxEvent2 != null) {
                        java.lang.String eventName2 = getEventName(saxEvent2);
                        if ((z2 && INCLUDED_TAG.equalsIgnoreCase(eventName2)) || (z && CONFIG_TAG.equalsIgnoreCase(eventName2))) {
                            saxEventList.remove(i);
                        }
                    }
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public ch.qos.logback.core.joran.event.SaxEventRecorder createRecorder(java.io.InputStream inputStream, java.net.URL url) {
        return new ch.qos.logback.core.joran.event.SaxEventRecorder(getContext());
    }

    /* access modifiers changed from: protected */
    public void processInclude(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext, java.net.URL url) throws ch.qos.logback.core.joran.spi.JoranException {
        java.io.InputStream openURL = openURL(url);
        if (openURL != null) {
            try {
                ch.qos.logback.core.joran.util.ConfigurationWatchListUtil.addToWatchList(getContext(), url);
                ch.qos.logback.core.joran.event.SaxEventRecorder createRecorder = createRecorder(openURL, url);
                createRecorder.setContext(getContext());
                createRecorder.recordEvents(openURL);
                trimHeadAndTail(createRecorder);
                interpretationContext.getJoranInterpreter().getEventPlayer().addEventsDynamically(createRecorder.getSaxEventList(), this.eventOffset);
            } catch (ch.qos.logback.core.joran.spi.JoranException e) {
                addError("Failed processing [" + url.toString() + "]", e);
                close(openURL);
                return;
            } catch (Throwable th) {
                close(openURL);
                throw th;
            }
        }
        close(openURL);
    }

    /* access modifiers changed from: protected */
    public void setEventOffset(int i) {
        this.eventOffset = i;
    }
}
