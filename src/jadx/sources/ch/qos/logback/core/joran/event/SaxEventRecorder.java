package ch.qos.logback.core.joran.event;

public class SaxEventRecorder extends org.xml.sax.helpers.DefaultHandler implements ch.qos.logback.core.spi.ContextAware {
    private final ch.qos.logback.core.spi.ContextAwareImpl cai;
    ch.qos.logback.core.joran.spi.ElementPath globalElementPath;
    private org.xml.sax.Locator locator;
    private java.util.List<ch.qos.logback.core.joran.event.SaxEvent> saxEventList;

    public SaxEventRecorder() {
        this.saxEventList = new java.util.ArrayList();
        this.globalElementPath = new ch.qos.logback.core.joran.spi.ElementPath();
        this.cai = new ch.qos.logback.core.spi.ContextAwareImpl(null, this);
    }

    public SaxEventRecorder(ch.qos.logback.core.Context context) {
        this.saxEventList = new java.util.ArrayList();
        this.globalElementPath = new ch.qos.logback.core.joran.spi.ElementPath();
        this.cai = new ch.qos.logback.core.spi.ContextAwareImpl(context, this);
    }

    private org.xmlpull.v1.sax2.Driver buildPullParser() throws ch.qos.logback.core.joran.spi.JoranException {
        try {
            org.xmlpull.v1.sax2.Driver driver = new org.xmlpull.v1.sax2.Driver();
            try {
                driver.setFeature("http://xml.org/sax/features/validation", false);
            } catch (org.xml.sax.SAXNotSupportedException e) {
            }
            driver.setFeature("http://xml.org/sax/features/namespaces", true);
            return driver;
        } catch (java.lang.Exception e2) {
            java.lang.String str = "Parser configuration error occurred";
            addError(str, e2);
            throw new ch.qos.logback.core.joran.spi.JoranException(str, e2);
        }
    }

    private void handleError(java.lang.String str, java.lang.Throwable th) throws ch.qos.logback.core.joran.spi.JoranException {
        addError(str, th);
        throw new ch.qos.logback.core.joran.spi.JoranException(str, th);
    }

    public void addError(java.lang.String str) {
        this.cai.addError(str);
    }

    public void addError(java.lang.String str, java.lang.Throwable th) {
        this.cai.addError(str, th);
    }

    public void addInfo(java.lang.String str) {
        this.cai.addInfo(str);
    }

    public void addInfo(java.lang.String str, java.lang.Throwable th) {
        this.cai.addInfo(str, th);
    }

    public void addStatus(ch.qos.logback.core.status.Status status) {
        this.cai.addStatus(status);
    }

    public void addWarn(java.lang.String str) {
        this.cai.addWarn(str);
    }

    public void addWarn(java.lang.String str, java.lang.Throwable th) {
        this.cai.addWarn(str, th);
    }

    public void characters(char[] cArr, int i, int i2) {
        java.lang.String str = new java.lang.String(cArr, i, i2);
        ch.qos.logback.core.joran.event.SaxEvent lastEvent = getLastEvent();
        if (lastEvent instanceof ch.qos.logback.core.joran.event.BodyEvent) {
            ((ch.qos.logback.core.joran.event.BodyEvent) lastEvent).append(str);
        } else if (!isSpaceOnly(str)) {
            this.saxEventList.add(new ch.qos.logback.core.joran.event.BodyEvent(str, getLocator()));
        }
    }

    public void endElement(java.lang.String str, java.lang.String str2, java.lang.String str3) {
        if (str3 == null || str3.length() == 0) {
        }
        this.saxEventList.add(new ch.qos.logback.core.joran.event.EndEvent(str, str2, str3, getLocator()));
        this.globalElementPath.pop();
    }

    public void error(org.xml.sax.SAXParseException sAXParseException) throws org.xml.sax.SAXException {
        addError("XML_PARSING - Parsing error on line " + sAXParseException.getLineNumber() + " and column " + sAXParseException.getColumnNumber(), sAXParseException);
    }

    public void fatalError(org.xml.sax.SAXParseException sAXParseException) throws org.xml.sax.SAXException {
        addError("XML_PARSING - Parsing fatal error on line " + sAXParseException.getLineNumber() + " and column " + sAXParseException.getColumnNumber(), sAXParseException);
    }

    public ch.qos.logback.core.Context getContext() {
        return this.cai.getContext();
    }

    /* access modifiers changed from: 0000 */
    public ch.qos.logback.core.joran.event.SaxEvent getLastEvent() {
        if (this.saxEventList.isEmpty()) {
            return null;
        }
        return (ch.qos.logback.core.joran.event.SaxEvent) this.saxEventList.get(this.saxEventList.size() - 1);
    }

    public org.xml.sax.Locator getLocator() {
        return this.locator;
    }

    public java.util.List<ch.qos.logback.core.joran.event.SaxEvent> getSaxEventList() {
        return this.saxEventList;
    }

    /* access modifiers changed from: 0000 */
    public java.lang.String getTagName(java.lang.String str, java.lang.String str2) {
        return (str == null || str.length() < 1) ? str2 : str;
    }

    /* access modifiers changed from: 0000 */
    public boolean isSpaceOnly(java.lang.String str) {
        return str.trim().length() == 0;
    }

    public java.util.List<ch.qos.logback.core.joran.event.SaxEvent> recordEvents(org.xml.sax.InputSource inputSource) throws ch.qos.logback.core.joran.spi.JoranException {
        org.xmlpull.v1.sax2.Driver buildPullParser = buildPullParser();
        try {
            buildPullParser.setContentHandler(this);
            buildPullParser.setErrorHandler(this);
            buildPullParser.parse(inputSource);
            return this.saxEventList;
        } catch (java.io.EOFException e) {
            handleError(e.getLocalizedMessage(), new org.xml.sax.SAXParseException(e.getLocalizedMessage(), this.locator, e));
            throw new java.lang.IllegalStateException("This point can never be reached");
        } catch (java.io.IOException e2) {
            handleError("I/O error occurred while parsing xml file", e2);
            throw new java.lang.IllegalStateException("This point can never be reached");
        } catch (org.xml.sax.SAXException e3) {
            throw new ch.qos.logback.core.joran.spi.JoranException("Problem parsing XML document. See previously reported errors.", e3);
        } catch (java.lang.Exception e4) {
            handleError("Unexpected exception while parsing XML document.", e4);
            throw new java.lang.IllegalStateException("This point can never be reached");
        }
    }

    public final void recordEvents(java.io.InputStream inputStream) throws ch.qos.logback.core.joran.spi.JoranException {
        recordEvents(new org.xml.sax.InputSource(inputStream));
    }

    public void setContext(ch.qos.logback.core.Context context) {
        this.cai.setContext(context);
    }

    public void setDocumentLocator(org.xml.sax.Locator locator2) {
        this.locator = locator2;
    }

    public void startDocument() {
    }

    public void startElement(java.lang.String str, java.lang.String str2, java.lang.String str3, org.xml.sax.Attributes attributes) {
        if (str3 == null || str3.length() == 0) {
        }
        this.globalElementPath.push(getTagName(str2, str3));
        this.saxEventList.add(new ch.qos.logback.core.joran.event.StartEvent(this.globalElementPath.duplicate(), str, str2, str3, attributes, getLocator()));
    }

    public void warning(org.xml.sax.SAXParseException sAXParseException) throws org.xml.sax.SAXException {
        addWarn("XML_PARSING - Parsing warning on line " + sAXParseException.getLineNumber() + " and column " + sAXParseException.getColumnNumber(), sAXParseException);
    }
}
