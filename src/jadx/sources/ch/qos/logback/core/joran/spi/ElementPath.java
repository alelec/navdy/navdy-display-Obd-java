package ch.qos.logback.core.joran.spi;

public class ElementPath {
    java.util.ArrayList<java.lang.String> partList = new java.util.ArrayList<>();

    public ElementPath() {
    }

    public ElementPath(java.lang.String str) {
        if (str != null) {
            java.lang.String[] split = str.split("/");
            if (split != null) {
                for (java.lang.String str2 : split) {
                    if (str2.length() > 0) {
                        this.partList.add(str2);
                    }
                }
            }
        }
    }

    public ElementPath(java.util.List<java.lang.String> list) {
        this.partList.addAll(list);
    }

    private boolean equalityCheck(java.lang.String str, java.lang.String str2) {
        return str.equalsIgnoreCase(str2);
    }

    public ch.qos.logback.core.joran.spi.ElementPath duplicate() {
        ch.qos.logback.core.joran.spi.ElementPath elementPath = new ch.qos.logback.core.joran.spi.ElementPath();
        elementPath.partList.addAll(this.partList);
        return elementPath;
    }

    public boolean equals(java.lang.Object obj) {
        if (obj == null || !(obj instanceof ch.qos.logback.core.joran.spi.ElementPath)) {
            return false;
        }
        ch.qos.logback.core.joran.spi.ElementPath elementPath = (ch.qos.logback.core.joran.spi.ElementPath) obj;
        if (elementPath.size() != size()) {
            return false;
        }
        int size = size();
        for (int i = 0; i < size; i++) {
            if (!equalityCheck(get(i), elementPath.get(i))) {
                return false;
            }
        }
        return true;
    }

    public java.lang.String get(int i) {
        return (java.lang.String) this.partList.get(i);
    }

    public java.util.List<java.lang.String> getCopyOfPartList() {
        return new java.util.ArrayList(this.partList);
    }

    public java.lang.String peekLast() {
        if (this.partList.isEmpty()) {
            return null;
        }
        return (java.lang.String) this.partList.get(this.partList.size() - 1);
    }

    public void pop() {
        if (!this.partList.isEmpty()) {
            this.partList.remove(this.partList.size() - 1);
        }
    }

    public void push(java.lang.String str) {
        this.partList.add(str);
    }

    public int size() {
        return this.partList.size();
    }

    /* access modifiers changed from: protected */
    public java.lang.String toStableString() {
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        java.util.Iterator it = this.partList.iterator();
        while (it.hasNext()) {
            sb.append("[").append((java.lang.String) it.next()).append("]");
        }
        return sb.toString();
    }

    public java.lang.String toString() {
        return toStableString();
    }
}
