package ch.qos.logback.core.joran.util;

public class PropertySetter extends ch.qos.logback.core.spi.ContextAwareBase {
    protected ch.qos.logback.core.joran.util.MethodDescriptor[] methodDescriptors;
    protected java.lang.Object obj;
    protected java.lang.Class<?> objClass;
    protected ch.qos.logback.core.joran.util.PropertyDescriptor[] propertyDescriptors;

    public PropertySetter(java.lang.Object obj2) {
        this.obj = obj2;
        this.objClass = obj2.getClass();
    }

    private java.lang.String capitalizeFirstLetter(java.lang.String str) {
        return str.substring(0, 1).toUpperCase() + str.substring(1);
    }

    private ch.qos.logback.core.util.AggregationType computeRawAggregationType(java.lang.reflect.Method method) {
        java.lang.Class parameterClassForMethod = getParameterClassForMethod(method);
        return parameterClassForMethod == null ? ch.qos.logback.core.util.AggregationType.NOT_FOUND : ch.qos.logback.core.joran.util.StringToObjectConverter.canBeBuiltFromSimpleString(parameterClassForMethod) ? ch.qos.logback.core.util.AggregationType.AS_BASIC_PROPERTY : ch.qos.logback.core.util.AggregationType.AS_COMPLEX_PROPERTY;
    }

    private java.lang.reflect.Method findAdderMethod(java.lang.String str) {
        return getMethod("add" + capitalizeFirstLetter(str));
    }

    private java.lang.reflect.Method findSetterMethod(java.lang.String str) {
        ch.qos.logback.core.joran.util.PropertyDescriptor propertyDescriptor = getPropertyDescriptor(ch.qos.logback.core.joran.util.Introspector.decapitalize(str));
        if (propertyDescriptor != null) {
            return propertyDescriptor.getWriteMethod();
        }
        return null;
    }

    private java.lang.Class<?> getParameterClassForMethod(java.lang.reflect.Method method) {
        if (method == null) {
            return null;
        }
        java.lang.Class<?>[] parameterTypes = method.getParameterTypes();
        if (parameterTypes.length == 1) {
            return parameterTypes[0];
        }
        return null;
    }

    private boolean isSanityCheckSuccessful(java.lang.String str, java.lang.reflect.Method method, java.lang.Class<?>[] clsArr, java.lang.Object obj2) {
        java.lang.Class cls = obj2.getClass();
        if (clsArr.length != 1) {
            addError("Wrong number of parameters in setter method for property [" + str + "] in " + this.obj.getClass().getName());
            return false;
        } else if (clsArr[0].isAssignableFrom(obj2.getClass())) {
            return true;
        } else {
            addError("A \"" + cls.getName() + "\" object is not assignable to a \"" + clsArr[0].getName() + "\" variable.");
            addError("The class \"" + clsArr[0].getName() + "\" was loaded by ");
            addError("[" + clsArr[0].getClassLoader() + "] whereas object of type ");
            addError("\"" + cls.getName() + "\" was loaded by [" + cls.getClassLoader() + "].");
            return false;
        }
    }

    private boolean isUnequivocallyInstantiable(java.lang.Class<?> cls) {
        if (cls.isInterface()) {
            return false;
        }
        try {
            return cls.newInstance() != null;
        } catch (java.lang.IllegalAccessException | java.lang.InstantiationException e) {
            return false;
        }
    }

    public void addBasicProperty(java.lang.String str, java.lang.String str2) {
        if (str2 != null) {
            java.lang.String capitalizeFirstLetter = capitalizeFirstLetter(str);
            java.lang.reflect.Method findAdderMethod = findAdderMethod(capitalizeFirstLetter);
            if (findAdderMethod == null) {
                addError("No adder for property [" + capitalizeFirstLetter + "].");
                return;
            }
            java.lang.Class[] parameterTypes = findAdderMethod.getParameterTypes();
            isSanityCheckSuccessful(capitalizeFirstLetter, findAdderMethod, parameterTypes, str2);
            try {
                if (ch.qos.logback.core.joran.util.StringToObjectConverter.convertArg(this, str2, parameterTypes[0]) != null) {
                    invokeMethodWithSingleParameterOnThisObject(findAdderMethod, str2);
                }
            } catch (Throwable th) {
                addError("Conversion to type [" + parameterTypes[0] + "] failed. ", th);
            }
        }
    }

    public void addComplexProperty(java.lang.String str, java.lang.Object obj2) {
        java.lang.reflect.Method findAdderMethod = findAdderMethod(str);
        if (findAdderMethod == null) {
            addError("Could not find method [add" + str + "] in class [" + this.objClass.getName() + "].");
        } else if (isSanityCheckSuccessful(str, findAdderMethod, findAdderMethod.getParameterTypes(), obj2)) {
            invokeMethodWithSingleParameterOnThisObject(findAdderMethod, obj2);
        }
    }

    public ch.qos.logback.core.util.AggregationType computeAggregationType(java.lang.String str) {
        java.lang.reflect.Method findAdderMethod = findAdderMethod(str);
        if (findAdderMethod != null) {
            switch (computeRawAggregationType(findAdderMethod)) {
                case NOT_FOUND:
                    return ch.qos.logback.core.util.AggregationType.NOT_FOUND;
                case AS_BASIC_PROPERTY:
                    return ch.qos.logback.core.util.AggregationType.AS_BASIC_PROPERTY_COLLECTION;
                case AS_COMPLEX_PROPERTY:
                    return ch.qos.logback.core.util.AggregationType.AS_COMPLEX_PROPERTY_COLLECTION;
            }
        }
        java.lang.reflect.Method findSetterMethod = findSetterMethod(str);
        return findSetterMethod != null ? computeRawAggregationType(findSetterMethod) : ch.qos.logback.core.util.AggregationType.NOT_FOUND;
    }

    /* access modifiers changed from: 0000 */
    public <T extends java.lang.annotation.Annotation> T getAnnotation(java.lang.String str, java.lang.Class<T> cls, java.lang.reflect.Method method) {
        if (method != null) {
            return method.getAnnotation(cls);
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    public java.lang.Class<?> getByConcreteType(java.lang.String str, java.lang.reflect.Method method) {
        java.lang.Class parameterClassForMethod = getParameterClassForMethod(method);
        if (parameterClassForMethod != null && isUnequivocallyInstantiable(parameterClassForMethod)) {
            return parameterClassForMethod;
        }
        return null;
    }

    public java.lang.Class<?> getClassNameViaImplicitRules(java.lang.String str, ch.qos.logback.core.util.AggregationType aggregationType, ch.qos.logback.core.joran.spi.DefaultNestedComponentRegistry defaultNestedComponentRegistry) {
        java.lang.Class<?> findDefaultComponentType = defaultNestedComponentRegistry.findDefaultComponentType(this.obj.getClass(), str);
        if (findDefaultComponentType != null) {
            return findDefaultComponentType;
        }
        java.lang.reflect.Method relevantMethod = getRelevantMethod(str, aggregationType);
        if (relevantMethod == null) {
            return null;
        }
        java.lang.Class<?> defaultClassNameByAnnonation = getDefaultClassNameByAnnonation(str, relevantMethod);
        return defaultClassNameByAnnonation == null ? getByConcreteType(str, relevantMethod) : defaultClassNameByAnnonation;
    }

    /* access modifiers changed from: 0000 */
    public java.lang.Class<?> getDefaultClassNameByAnnonation(java.lang.String str, java.lang.reflect.Method method) {
        ch.qos.logback.core.joran.spi.DefaultClass defaultClass = (ch.qos.logback.core.joran.spi.DefaultClass) getAnnotation(str, ch.qos.logback.core.joran.spi.DefaultClass.class, method);
        if (defaultClass != null) {
            return defaultClass.value();
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public java.lang.reflect.Method getMethod(java.lang.String str) {
        if (this.methodDescriptors == null) {
            introspect();
        }
        for (int i = 0; i < this.methodDescriptors.length; i++) {
            if (str.equals(this.methodDescriptors[i].getName())) {
                return this.methodDescriptors[i].getMethod();
            }
        }
        return null;
    }

    public java.lang.Object getObj() {
        return this.obj;
    }

    public java.lang.Class<?> getObjClass() {
        return this.objClass;
    }

    /* access modifiers changed from: protected */
    public ch.qos.logback.core.joran.util.PropertyDescriptor getPropertyDescriptor(java.lang.String str) {
        if (this.propertyDescriptors == null) {
            introspect();
        }
        for (int i = 0; i < this.propertyDescriptors.length; i++) {
            if (str.equals(this.propertyDescriptors[i].getName())) {
                return this.propertyDescriptors[i];
            }
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    public java.lang.reflect.Method getRelevantMethod(java.lang.String str, ch.qos.logback.core.util.AggregationType aggregationType) {
        java.lang.String capitalizeFirstLetter = capitalizeFirstLetter(str);
        if (aggregationType == ch.qos.logback.core.util.AggregationType.AS_COMPLEX_PROPERTY_COLLECTION) {
            return findAdderMethod(capitalizeFirstLetter);
        }
        if (aggregationType == ch.qos.logback.core.util.AggregationType.AS_COMPLEX_PROPERTY) {
            return findSetterMethod(capitalizeFirstLetter);
        }
        throw new java.lang.IllegalStateException(aggregationType + " not allowed here");
    }

    /* access modifiers changed from: protected */
    public void introspect() {
        try {
            this.propertyDescriptors = ch.qos.logback.core.joran.util.Introspector.getPropertyDescriptors(this.objClass);
            this.methodDescriptors = ch.qos.logback.core.joran.util.Introspector.getMethodDescriptors(this.objClass);
        } catch (ch.qos.logback.core.joran.util.IntrospectionException e) {
            addError("Failed to introspect " + this.obj + ": " + e.getMessage());
            this.propertyDescriptors = new ch.qos.logback.core.joran.util.PropertyDescriptor[0];
            this.methodDescriptors = new ch.qos.logback.core.joran.util.MethodDescriptor[0];
        }
    }

    /* access modifiers changed from: 0000 */
    public void invokeMethodWithSingleParameterOnThisObject(java.lang.reflect.Method method, java.lang.Object obj2) {
        java.lang.Class cls = obj2.getClass();
        try {
            method.invoke(this.obj, new java.lang.Object[]{obj2});
        } catch (java.lang.Exception e) {
            addError("Could not invoke method " + method.getName() + " in class " + this.obj.getClass().getName() + " with parameter of type " + cls.getName(), e);
        }
    }

    public void setComplexProperty(java.lang.String str, java.lang.Object obj2) {
        ch.qos.logback.core.joran.util.PropertyDescriptor propertyDescriptor = getPropertyDescriptor(ch.qos.logback.core.joran.util.Introspector.decapitalize(str));
        if (propertyDescriptor == null) {
            addWarn("Could not find PropertyDescriptor for [" + str + "] in " + this.objClass.getName());
            return;
        }
        java.lang.reflect.Method writeMethod = propertyDescriptor.getWriteMethod();
        if (writeMethod == null) {
            addWarn("Not setter method for property [" + str + "] in " + this.obj.getClass().getName());
        } else if (isSanityCheckSuccessful(str, writeMethod, writeMethod.getParameterTypes(), obj2)) {
            try {
                invokeMethodWithSingleParameterOnThisObject(writeMethod, obj2);
            } catch (java.lang.Exception e) {
                addError("Could not set component " + this.obj + " for parent component " + this.obj, e);
            }
        }
    }

    public void setProperty(ch.qos.logback.core.joran.util.PropertyDescriptor propertyDescriptor, java.lang.String str, java.lang.String str2) throws ch.qos.logback.core.util.PropertySetterException {
        java.lang.reflect.Method writeMethod = propertyDescriptor.getWriteMethod();
        if (writeMethod == null) {
            throw new ch.qos.logback.core.util.PropertySetterException("No setter for property [" + str + "].");
        }
        java.lang.Class[] parameterTypes = writeMethod.getParameterTypes();
        if (parameterTypes.length != 1) {
            throw new ch.qos.logback.core.util.PropertySetterException("#params for setter != 1");
        }
        try {
            java.lang.Object convertArg = ch.qos.logback.core.joran.util.StringToObjectConverter.convertArg(this, str2, parameterTypes[0]);
            if (convertArg == null) {
                throw new ch.qos.logback.core.util.PropertySetterException("Conversion to type [" + parameterTypes[0] + "] failed.");
            }
            try {
                writeMethod.invoke(this.obj, new java.lang.Object[]{convertArg});
            } catch (java.lang.Exception e) {
                throw new ch.qos.logback.core.util.PropertySetterException((java.lang.Throwable) e);
            }
        } catch (Throwable th) {
            throw new ch.qos.logback.core.util.PropertySetterException("Conversion to type [" + parameterTypes[0] + "] failed. ", th);
        }
    }

    public void setProperty(java.lang.String str, java.lang.String str2) {
        if (str2 != null) {
            java.lang.String decapitalize = ch.qos.logback.core.joran.util.Introspector.decapitalize(str);
            ch.qos.logback.core.joran.util.PropertyDescriptor propertyDescriptor = getPropertyDescriptor(decapitalize);
            if (propertyDescriptor == null) {
                addWarn("No such property [" + decapitalize + "] in " + this.objClass.getName() + ".");
                return;
            }
            try {
                setProperty(propertyDescriptor, decapitalize, str2);
            } catch (ch.qos.logback.core.util.PropertySetterException e) {
                addWarn("Failed to set property [" + decapitalize + "] to value \"" + str2 + "\". ", e);
            }
        }
    }
}
