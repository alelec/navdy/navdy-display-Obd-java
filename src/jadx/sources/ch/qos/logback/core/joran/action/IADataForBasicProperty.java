package ch.qos.logback.core.joran.action;

class IADataForBasicProperty {
    final ch.qos.logback.core.util.AggregationType aggregationType;
    boolean inError;
    final ch.qos.logback.core.joran.util.PropertySetter parentBean;
    final java.lang.String propertyName;

    IADataForBasicProperty(ch.qos.logback.core.joran.util.PropertySetter propertySetter, ch.qos.logback.core.util.AggregationType aggregationType2, java.lang.String str) {
        this.parentBean = propertySetter;
        this.aggregationType = aggregationType2;
        this.propertyName = str;
    }
}
