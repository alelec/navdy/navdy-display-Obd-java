package ch.qos.logback.core.joran.spi;

class CAI_WithLocatorSupport extends ch.qos.logback.core.spi.ContextAwareImpl {
    CAI_WithLocatorSupport(ch.qos.logback.core.Context context, ch.qos.logback.core.joran.spi.Interpreter interpreter) {
        super(context, interpreter);
    }

    /* access modifiers changed from: protected */
    public java.lang.Object getOrigin() {
        org.xml.sax.Locator locator = ((ch.qos.logback.core.joran.spi.Interpreter) super.getOrigin()).locator;
        return locator != null ? ch.qos.logback.core.joran.spi.Interpreter.class.getName() + "@" + locator.getLineNumber() + ":" + locator.getColumnNumber() : ch.qos.logback.core.joran.spi.Interpreter.class.getName() + "@NA:NA";
    }
}
