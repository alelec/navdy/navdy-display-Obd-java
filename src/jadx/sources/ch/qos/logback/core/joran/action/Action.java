package ch.qos.logback.core.joran.action;

public abstract class Action extends ch.qos.logback.core.spi.ContextAwareBase {
    public static final java.lang.String ACTION_CLASS_ATTRIBUTE = "actionClass";
    public static final java.lang.String CLASS_ATTRIBUTE = "class";
    public static final java.lang.String FILE_ATTRIBUTE = "file";
    public static final java.lang.String KEY_ATTRIBUTE = "key";
    public static final java.lang.String NAME_ATTRIBUTE = "name";
    public static final java.lang.String PATTERN_ATTRIBUTE = "pattern";
    public static final java.lang.String SCOPE_ATTRIBUTE = "scope";
    public static final java.lang.String VALUE_ATTRIBUTE = "value";

    public abstract void begin(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext, java.lang.String str, org.xml.sax.Attributes attributes) throws ch.qos.logback.core.joran.spi.ActionException;

    public void body(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext, java.lang.String str) throws ch.qos.logback.core.joran.spi.ActionException {
    }

    public abstract void end(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext, java.lang.String str) throws ch.qos.logback.core.joran.spi.ActionException;

    /* access modifiers changed from: protected */
    public int getColumnNumber(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext) {
        org.xml.sax.Locator locator = interpretationContext.getJoranInterpreter().getLocator();
        if (locator != null) {
            return locator.getColumnNumber();
        }
        return -1;
    }

    /* access modifiers changed from: protected */
    public java.lang.String getLineColStr(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext) {
        return "line: " + getLineNumber(interpretationContext) + ", column: " + getColumnNumber(interpretationContext);
    }

    /* access modifiers changed from: protected */
    public int getLineNumber(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext) {
        org.xml.sax.Locator locator = interpretationContext.getJoranInterpreter().getLocator();
        if (locator != null) {
            return locator.getLineNumber();
        }
        return -1;
    }

    public java.lang.String toString() {
        return getClass().getName();
    }
}
