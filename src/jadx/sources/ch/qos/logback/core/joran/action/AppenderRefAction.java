package ch.qos.logback.core.joran.action;

public class AppenderRefAction<E> extends ch.qos.logback.core.joran.action.Action {
    boolean inError = false;

    public void begin(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext, java.lang.String str, org.xml.sax.Attributes attributes) {
        this.inError = false;
        java.lang.Object peekObject = interpretationContext.peekObject();
        if (!(peekObject instanceof ch.qos.logback.core.spi.AppenderAttachable)) {
            java.lang.String str2 = "Could not find an AppenderAttachable at the top of execution stack. Near [" + str + "] line " + getLineNumber(interpretationContext);
            this.inError = true;
            addError(str2);
            return;
        }
        ch.qos.logback.core.spi.AppenderAttachable appenderAttachable = (ch.qos.logback.core.spi.AppenderAttachable) peekObject;
        java.lang.String subst = interpretationContext.subst(attributes.getValue(ch.qos.logback.core.joran.action.ActionConst.REF_ATTRIBUTE));
        if (ch.qos.logback.core.util.OptionHelper.isEmpty(subst)) {
            this.inError = true;
            addError("Missing appender ref attribute in <appender-ref> tag.");
            return;
        }
        ch.qos.logback.core.Appender appender = (ch.qos.logback.core.Appender) ((java.util.HashMap) interpretationContext.getObjectMap().get(ch.qos.logback.core.joran.action.ActionConst.APPENDER_BAG)).get(subst);
        if (appender == null) {
            java.lang.String str3 = "Could not find an appender named [" + subst + "]. Did you define it below instead of above in the configuration file?";
            this.inError = true;
            addError(str3);
            addError("See http://logback.qos.ch/codes.html#appender_order for more details.");
            return;
        }
        addInfo("Attaching appender named [" + subst + "] to " + appenderAttachable);
        appenderAttachable.addAppender(appender);
    }

    public void end(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext, java.lang.String str) {
    }
}
