package ch.qos.logback.core.joran.action;

public class ConversionRuleAction extends ch.qos.logback.core.joran.action.Action {
    boolean inError = false;

    public void begin(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext, java.lang.String str, org.xml.sax.Attributes attributes) {
        this.inError = false;
        java.lang.String value = attributes.getValue(ch.qos.logback.core.joran.action.ActionConst.CONVERSION_WORD_ATTRIBUTE);
        java.lang.String value2 = attributes.getValue(ch.qos.logback.core.joran.action.ActionConst.CONVERTER_CLASS_ATTRIBUTE);
        if (ch.qos.logback.core.util.OptionHelper.isEmpty(value)) {
            this.inError = true;
            addError("No 'conversionWord' attribute in <conversionRule>");
        } else if (ch.qos.logback.core.util.OptionHelper.isEmpty(value2)) {
            this.inError = true;
            interpretationContext.addError("No 'converterClass' attribute in <conversionRule>");
        } else {
            try {
                java.util.Map map = (java.util.Map) this.context.getObject(ch.qos.logback.core.CoreConstants.PATTERN_RULE_REGISTRY);
                if (map == null) {
                    map = new java.util.HashMap();
                    this.context.putObject(ch.qos.logback.core.CoreConstants.PATTERN_RULE_REGISTRY, map);
                }
                addInfo("registering conversion word " + value + " with class [" + value2 + "]");
                map.put(value, value2);
            } catch (java.lang.Exception e) {
                this.inError = true;
                addError("Could not add conversion rule to PatternLayout.");
            }
        }
    }

    public void end(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext, java.lang.String str) {
    }

    public void finish(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext) {
    }
}
