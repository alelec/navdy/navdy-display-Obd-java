package ch.qos.logback.core.joran;

public abstract class JoranConfiguratorBase extends ch.qos.logback.core.joran.GenericConfigurator {
    /* access modifiers changed from: protected */
    public void addImplicitRules(ch.qos.logback.core.joran.spi.Interpreter interpreter) {
        ch.qos.logback.core.joran.action.NestedComplexPropertyIA nestedComplexPropertyIA = new ch.qos.logback.core.joran.action.NestedComplexPropertyIA();
        nestedComplexPropertyIA.setContext(this.context);
        interpreter.addImplicitAction(nestedComplexPropertyIA);
        ch.qos.logback.core.joran.action.NestedBasicPropertyIA nestedBasicPropertyIA = new ch.qos.logback.core.joran.action.NestedBasicPropertyIA();
        nestedBasicPropertyIA.setContext(this.context);
        interpreter.addImplicitAction(nestedBasicPropertyIA);
    }

    /* access modifiers changed from: protected */
    public void addInstanceRules(ch.qos.logback.core.joran.spi.RuleStore ruleStore) {
        ruleStore.addRule(new ch.qos.logback.core.joran.spi.ElementSelector("configuration/property"), (ch.qos.logback.core.joran.action.Action) new ch.qos.logback.core.joran.action.PropertyAction());
        ruleStore.addRule(new ch.qos.logback.core.joran.spi.ElementSelector("configuration/substitutionProperty"), (ch.qos.logback.core.joran.action.Action) new ch.qos.logback.core.joran.action.PropertyAction());
        ruleStore.addRule(new ch.qos.logback.core.joran.spi.ElementSelector("configuration/timestamp"), (ch.qos.logback.core.joran.action.Action) new ch.qos.logback.core.joran.action.TimestampAction());
        ruleStore.addRule(new ch.qos.logback.core.joran.spi.ElementSelector("configuration/define"), (ch.qos.logback.core.joran.action.Action) new ch.qos.logback.core.joran.action.DefinePropertyAction());
        ruleStore.addRule(new ch.qos.logback.core.joran.spi.ElementSelector("configuration/conversionRule"), (ch.qos.logback.core.joran.action.Action) new ch.qos.logback.core.joran.action.ConversionRuleAction());
        ruleStore.addRule(new ch.qos.logback.core.joran.spi.ElementSelector("configuration/statusListener"), (ch.qos.logback.core.joran.action.Action) new ch.qos.logback.core.joran.action.StatusListenerAction());
        ruleStore.addRule(new ch.qos.logback.core.joran.spi.ElementSelector("configuration/appender"), (ch.qos.logback.core.joran.action.Action) new ch.qos.logback.core.joran.action.AppenderAction());
        ruleStore.addRule(new ch.qos.logback.core.joran.spi.ElementSelector("configuration/appender/appender-ref"), (ch.qos.logback.core.joran.action.Action) new ch.qos.logback.core.joran.action.AppenderRefAction());
        ruleStore.addRule(new ch.qos.logback.core.joran.spi.ElementSelector("configuration/newRule"), (ch.qos.logback.core.joran.action.Action) new ch.qos.logback.core.joran.action.NewRuleAction());
        ruleStore.addRule(new ch.qos.logback.core.joran.spi.ElementSelector("*/param"), (ch.qos.logback.core.joran.action.Action) new ch.qos.logback.core.joran.action.ParamAction());
    }

    /* access modifiers changed from: protected */
    public void buildInterpreter() {
        super.buildInterpreter();
        java.util.Map objectMap = this.interpreter.getInterpretationContext().getObjectMap();
        objectMap.put(ch.qos.logback.core.joran.action.ActionConst.APPENDER_BAG, new java.util.HashMap());
        objectMap.put(ch.qos.logback.core.joran.action.ActionConst.FILTER_CHAIN_BAG, new java.util.HashMap());
    }

    public java.util.List getErrorList() {
        return null;
    }

    public ch.qos.logback.core.joran.spi.InterpretationContext getInterpretationContext() {
        return this.interpreter.getInterpretationContext();
    }
}
