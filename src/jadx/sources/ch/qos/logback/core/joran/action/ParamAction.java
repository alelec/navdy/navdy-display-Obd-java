package ch.qos.logback.core.joran.action;

public class ParamAction extends ch.qos.logback.core.joran.action.Action {
    static java.lang.String NO_NAME = "No name attribute in <param> element";
    static java.lang.String NO_VALUE = "No value attribute in <param> element";
    boolean inError = false;

    public void begin(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext, java.lang.String str, org.xml.sax.Attributes attributes) {
        java.lang.String value = attributes.getValue(ch.qos.logback.core.joran.action.Action.NAME_ATTRIBUTE);
        java.lang.String value2 = attributes.getValue("value");
        if (value == null) {
            this.inError = true;
            addError(NO_NAME);
        } else if (value2 == null) {
            this.inError = true;
            addError(NO_VALUE);
        } else {
            java.lang.String trim = value2.trim();
            ch.qos.logback.core.joran.util.PropertySetter propertySetter = new ch.qos.logback.core.joran.util.PropertySetter(interpretationContext.peekObject());
            propertySetter.setContext(this.context);
            propertySetter.setProperty(interpretationContext.subst(value), interpretationContext.subst(trim));
        }
    }

    public void end(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext, java.lang.String str) {
    }

    public void finish(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext) {
    }
}
