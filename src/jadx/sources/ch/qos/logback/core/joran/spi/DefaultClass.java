package ch.qos.logback.core.joran.spi;

@java.lang.annotation.Target({java.lang.annotation.ElementType.METHOD})
@java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
public @interface DefaultClass {
    java.lang.Class<?> value();
}
