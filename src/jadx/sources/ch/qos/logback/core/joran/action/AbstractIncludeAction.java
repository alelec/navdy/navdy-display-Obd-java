package ch.qos.logback.core.joran.action;

public abstract class AbstractIncludeAction extends ch.qos.logback.core.joran.action.Action {
    private static final java.lang.String FILE_ATTR = "file";
    private static final java.lang.String OPTIONAL_ATTR = "optional";
    private static final java.lang.String RESOURCE_ATTR = "resource";
    private static final java.lang.String URL_ATTR = "url";
    private java.lang.String attributeInUse;
    private boolean optional;
    private java.net.URL urlInUse;

    /* JADX WARNING: Code restructure failed: missing block: B:12:?, code lost:
        return null;
     */
    private java.net.URL attributeToURL(java.lang.String str) {
        try {
            java.net.URL url = new java.net.URL(str);
            url.openStream().close();
            return url;
        } catch (java.net.MalformedURLException e) {
            if (!this.optional) {
                handleError("URL [" + str + "] is not well formed.", e);
            }
        } catch (java.io.IOException e2) {
            if (!this.optional) {
                handleError("URL [" + str + "] cannot be opened.", e2);
            }
        }
    }

    private boolean checkAttributes(org.xml.sax.Attributes attributes) {
        java.lang.String value = attributes.getValue("file");
        java.lang.String value2 = attributes.getValue(URL_ATTR);
        java.lang.String value3 = attributes.getValue(RESOURCE_ATTR);
        int i = !ch.qos.logback.core.util.OptionHelper.isEmpty(value) ? 1 : 0;
        if (!ch.qos.logback.core.util.OptionHelper.isEmpty(value2)) {
            i++;
        }
        if (!ch.qos.logback.core.util.OptionHelper.isEmpty(value3)) {
            i++;
        }
        if (i == 0) {
            handleError(java.lang.String.format("One of \"%1$s\", \"%2$s\" or \"%3$s\" attributes must be set.", new java.lang.Object[]{"file", RESOURCE_ATTR, URL_ATTR}), null);
            return false;
        } else if (i > 1) {
            handleError(java.lang.String.format("Only one of \"%1$s\", \"%2$s\" or \"%3$s\" attributes should be set.", new java.lang.Object[]{"file", RESOURCE_ATTR, URL_ATTR}), null);
            return false;
        } else if (i == 1) {
            return true;
        } else {
            throw new java.lang.IllegalStateException("Count value [" + i + "] is not expected");
        }
    }

    private java.net.URL filePathAsURL(java.lang.String str) {
        java.net.URL url = null;
        java.io.File file = new java.io.File(str);
        if (file.exists() && file.isFile()) {
            try {
                return file.toURI().toURL();
            } catch (java.net.MalformedURLException e) {
                e.printStackTrace();
                return url;
            }
        } else if (this.optional) {
            return url;
        } else {
            handleError("File does not exist [" + str + "]", new java.io.FileNotFoundException(str));
            return url;
        }
    }

    private java.net.URL getInputURL(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext, org.xml.sax.Attributes attributes) {
        java.lang.String value = attributes.getValue("file");
        java.lang.String value2 = attributes.getValue(URL_ATTR);
        java.lang.String value3 = attributes.getValue(RESOURCE_ATTR);
        if (!ch.qos.logback.core.util.OptionHelper.isEmpty(value)) {
            this.attributeInUse = interpretationContext.subst(value);
            return filePathAsURL(this.attributeInUse);
        } else if (!ch.qos.logback.core.util.OptionHelper.isEmpty(value2)) {
            this.attributeInUse = interpretationContext.subst(value2);
            return attributeToURL(this.attributeInUse);
        } else if (!ch.qos.logback.core.util.OptionHelper.isEmpty(value3)) {
            this.attributeInUse = interpretationContext.subst(value3);
            return resourceAsURL(this.attributeInUse);
        } else {
            throw new java.lang.IllegalStateException("A URL stream should have been returned");
        }
    }

    private java.net.URL resourceAsURL(java.lang.String str) {
        java.net.URL resourceBySelfClassLoader = ch.qos.logback.core.util.Loader.getResourceBySelfClassLoader(str);
        if (resourceBySelfClassLoader != null) {
            return resourceBySelfClassLoader;
        }
        if (this.optional) {
            return null;
        }
        handleError("Could not find resource corresponding to [" + str + "]", null);
        return null;
    }

    public void begin(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext, java.lang.String str, org.xml.sax.Attributes attributes) throws ch.qos.logback.core.joran.spi.ActionException {
        this.attributeInUse = null;
        this.optional = ch.qos.logback.core.util.OptionHelper.toBoolean(attributes.getValue(OPTIONAL_ATTR), false);
        if (checkAttributes(attributes)) {
            try {
                java.net.URL inputURL = getInputURL(interpretationContext, attributes);
                if (inputURL != null) {
                    processInclude(interpretationContext, inputURL);
                }
            } catch (ch.qos.logback.core.joran.spi.JoranException e) {
                handleError("Error while parsing " + this.attributeInUse, e);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void close(java.io.InputStream inputStream) {
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (java.io.IOException e) {
            }
        }
    }

    public void end(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext, java.lang.String str) throws ch.qos.logback.core.joran.spi.ActionException {
    }

    /* access modifiers changed from: protected */
    public java.lang.String getAttributeInUse() {
        return this.attributeInUse;
    }

    public java.net.URL getUrl() {
        return this.urlInUse;
    }

    /* access modifiers changed from: protected */
    public void handleError(java.lang.String str, java.lang.Exception exc) {
        addError(str, exc);
    }

    /* access modifiers changed from: protected */
    public boolean isOptional() {
        return this.optional;
    }

    /* access modifiers changed from: protected */
    public abstract void processInclude(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext, java.net.URL url) throws ch.qos.logback.core.joran.spi.JoranException;
}
