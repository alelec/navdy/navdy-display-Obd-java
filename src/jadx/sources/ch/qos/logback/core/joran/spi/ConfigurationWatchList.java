package ch.qos.logback.core.joran.spi;

public class ConfigurationWatchList extends ch.qos.logback.core.spi.ContextAwareBase {
    java.util.List<java.io.File> fileWatchList = new java.util.ArrayList();
    java.util.List<java.lang.Long> lastModifiedList = new java.util.ArrayList();
    java.net.URL mainURL;

    private void addAsFileToWatch(java.net.URL url) {
        java.io.File convertToFile = convertToFile(url);
        if (convertToFile != null) {
            this.fileWatchList.add(convertToFile);
            this.lastModifiedList.add(java.lang.Long.valueOf(convertToFile.lastModified()));
        }
    }

    public void addToWatchList(java.net.URL url) {
        addAsFileToWatch(url);
    }

    public boolean changeDetected() {
        int size = this.fileWatchList.size();
        for (int i = 0; i < size; i++) {
            if (((java.lang.Long) this.lastModifiedList.get(i)).longValue() != ((java.io.File) this.fileWatchList.get(i)).lastModified()) {
                return true;
            }
        }
        return false;
    }

    public void clear() {
        this.mainURL = null;
        this.lastModifiedList.clear();
        this.fileWatchList.clear();
    }

    /* access modifiers changed from: 0000 */
    public java.io.File convertToFile(java.net.URL url) {
        if (ch.qos.logback.core.joran.action.Action.FILE_ATTRIBUTE.equals(url.getProtocol())) {
            return new java.io.File(java.net.URLDecoder.decode(url.getFile()));
        }
        addInfo("URL [" + url + "] is not of type file");
        return null;
    }

    public java.util.List<java.io.File> getCopyOfFileWatchList() {
        return new java.util.ArrayList(this.fileWatchList);
    }

    public java.net.URL getMainURL() {
        return this.mainURL;
    }

    public void setMainURL(java.net.URL url) {
        this.mainURL = url;
        if (url != null) {
            addAsFileToWatch(url);
        }
    }
}
