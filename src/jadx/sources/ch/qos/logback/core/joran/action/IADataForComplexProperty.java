package ch.qos.logback.core.joran.action;

public class IADataForComplexProperty {
    final ch.qos.logback.core.util.AggregationType aggregationType;
    final java.lang.String complexPropertyName;
    boolean inError;
    private java.lang.Object nestedComplexProperty;
    final ch.qos.logback.core.joran.util.PropertySetter parentBean;

    public IADataForComplexProperty(ch.qos.logback.core.joran.util.PropertySetter propertySetter, ch.qos.logback.core.util.AggregationType aggregationType2, java.lang.String str) {
        this.parentBean = propertySetter;
        this.aggregationType = aggregationType2;
        this.complexPropertyName = str;
    }

    public ch.qos.logback.core.util.AggregationType getAggregationType() {
        return this.aggregationType;
    }

    public java.lang.String getComplexPropertyName() {
        return this.complexPropertyName;
    }

    public java.lang.Object getNestedComplexProperty() {
        return this.nestedComplexProperty;
    }

    public void setNestedComplexProperty(java.lang.Object obj) {
        this.nestedComplexProperty = obj;
    }
}
