package ch.qos.logback.core.joran;

public abstract class GenericConfigurator extends ch.qos.logback.core.spi.ContextAwareBase {
    protected ch.qos.logback.core.joran.spi.Interpreter interpreter;

    private final void doConfigure(org.xml.sax.InputSource inputSource) throws ch.qos.logback.core.joran.spi.JoranException {
        long currentTimeMillis = java.lang.System.currentTimeMillis();
        if (!ch.qos.logback.core.joran.util.ConfigurationWatchListUtil.wasConfigurationWatchListReset(this.context)) {
            informContextOfURLUsedForConfiguration(getContext(), null);
        }
        ch.qos.logback.core.joran.event.SaxEventRecorder saxEventRecorder = new ch.qos.logback.core.joran.event.SaxEventRecorder(this.context);
        saxEventRecorder.recordEvents(inputSource);
        doConfigure(saxEventRecorder.getSaxEventList());
        if (new ch.qos.logback.core.status.StatusUtil(this.context).noXMLParsingErrorsOccurred(currentTimeMillis)) {
            addInfo("Registering current configuration as safe fallback point");
            registerSafeConfiguration();
        }
    }

    public static void informContextOfURLUsedForConfiguration(ch.qos.logback.core.Context context, java.net.URL url) {
        ch.qos.logback.core.joran.util.ConfigurationWatchListUtil.setMainWatchURL(context, url);
    }

    /* access modifiers changed from: protected */
    public void addDefaultNestedComponentRegistryRules(ch.qos.logback.core.joran.spi.DefaultNestedComponentRegistry defaultNestedComponentRegistry) {
    }

    /* access modifiers changed from: protected */
    public abstract void addImplicitRules(ch.qos.logback.core.joran.spi.Interpreter interpreter2);

    /* access modifiers changed from: protected */
    public abstract void addInstanceRules(ch.qos.logback.core.joran.spi.RuleStore ruleStore);

    /* access modifiers changed from: protected */
    public void buildInterpreter() {
        ch.qos.logback.core.joran.spi.SimpleRuleStore simpleRuleStore = new ch.qos.logback.core.joran.spi.SimpleRuleStore(this.context);
        addInstanceRules(simpleRuleStore);
        this.interpreter = new ch.qos.logback.core.joran.spi.Interpreter(this.context, simpleRuleStore, initialElementPath());
        ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext = this.interpreter.getInterpretationContext();
        interpretationContext.setContext(this.context);
        addImplicitRules(this.interpreter);
        addDefaultNestedComponentRegistryRules(interpretationContext.getDefaultNestedComponentRegistry());
    }

    public final void doConfigure(java.io.File file) throws ch.qos.logback.core.joran.spi.JoranException {
        try {
            informContextOfURLUsedForConfiguration(getContext(), file.toURI().toURL());
            doConfigure((java.io.InputStream) new java.io.FileInputStream(file));
        } catch (java.io.IOException e) {
            java.lang.String str = "Could not open [" + file.getPath() + "].";
            addError(str, e);
            throw new ch.qos.logback.core.joran.spi.JoranException(str, e);
        }
    }

    public final void doConfigure(java.io.InputStream inputStream) throws ch.qos.logback.core.joran.spi.JoranException {
        try {
            doConfigure(new org.xml.sax.InputSource(inputStream));
            try {
            } catch (java.io.IOException e) {
                java.lang.String str = "Could not close the stream";
                addError(str, e);
                throw new ch.qos.logback.core.joran.spi.JoranException(str, e);
            }
        } finally {
            try {
                inputStream.close();
            } catch (java.io.IOException e2) {
                java.lang.String str2 = "Could not close the stream";
                addError(str2, e2);
                throw new ch.qos.logback.core.joran.spi.JoranException(str2, e2);
            }
        }
    }

    public final void doConfigure(java.lang.String str) throws ch.qos.logback.core.joran.spi.JoranException {
        doConfigure(new java.io.File(str));
    }

    public final void doConfigure(java.net.URL url) throws ch.qos.logback.core.joran.spi.JoranException {
        try {
            informContextOfURLUsedForConfiguration(getContext(), url);
            java.net.URLConnection openConnection = url.openConnection();
            openConnection.setUseCaches(false);
            doConfigure(openConnection.getInputStream());
        } catch (java.io.IOException e) {
            java.lang.String str = "Could not open URL [" + url + "].";
            addError(str, e);
            throw new ch.qos.logback.core.joran.spi.JoranException(str, e);
        }
    }

    public void doConfigure(java.util.List<ch.qos.logback.core.joran.event.SaxEvent> list) throws ch.qos.logback.core.joran.spi.JoranException {
        buildInterpreter();
        synchronized (this.context.getConfigurationLock()) {
            this.interpreter.getEventPlayer().play(list);
        }
    }

    /* access modifiers changed from: protected */
    public ch.qos.logback.core.joran.spi.ElementPath initialElementPath() {
        return new ch.qos.logback.core.joran.spi.ElementPath();
    }

    public java.util.List<ch.qos.logback.core.joran.event.SaxEvent> recallSafeConfiguration() {
        return (java.util.List) this.context.getObject(ch.qos.logback.core.CoreConstants.SAFE_JORAN_CONFIGURATION);
    }

    public void registerSafeConfiguration() {
        this.context.putObject(ch.qos.logback.core.CoreConstants.SAFE_JORAN_CONFIGURATION, this.interpreter.getEventPlayer().getCopyOfPlayerEventList());
    }
}
