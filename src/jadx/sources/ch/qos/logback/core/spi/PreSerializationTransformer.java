package ch.qos.logback.core.spi;

public interface PreSerializationTransformer<E> {
    java.io.Serializable transform(E e);
}
