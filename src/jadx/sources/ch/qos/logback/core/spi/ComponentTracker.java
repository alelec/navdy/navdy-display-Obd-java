package ch.qos.logback.core.spi;

public interface ComponentTracker<C> {
    public static final int DEFAULT_MAX_COMPONENTS = Integer.MAX_VALUE;
    public static final int DEFAULT_TIMEOUT = 1800000;

    java.util.Collection<C> allComponents();

    java.util.Set<java.lang.String> allKeys();

    void endOfLife(java.lang.String str);

    C find(java.lang.String str);

    int getComponentCount();

    C getOrCreate(java.lang.String str, long j);

    void removeStaleComponents(long j);
}
