package ch.qos.logback.core.spi;

public class ContextAwareBase implements ch.qos.logback.core.spi.ContextAware {
    /* access modifiers changed from: protected */
    public ch.qos.logback.core.Context context;
    final java.lang.Object declaredOrigin;
    private int noContextWarning;

    public ContextAwareBase() {
        this.noContextWarning = 0;
        this.declaredOrigin = this;
    }

    public ContextAwareBase(ch.qos.logback.core.spi.ContextAware contextAware) {
        this.noContextWarning = 0;
        this.declaredOrigin = contextAware;
    }

    public void addError(java.lang.String str) {
        addStatus(new ch.qos.logback.core.status.ErrorStatus(str, getDeclaredOrigin()));
    }

    public void addError(java.lang.String str, java.lang.Throwable th) {
        addStatus(new ch.qos.logback.core.status.ErrorStatus(str, getDeclaredOrigin(), th));
    }

    public void addInfo(java.lang.String str) {
        addStatus(new ch.qos.logback.core.status.InfoStatus(str, getDeclaredOrigin()));
    }

    public void addInfo(java.lang.String str, java.lang.Throwable th) {
        addStatus(new ch.qos.logback.core.status.InfoStatus(str, getDeclaredOrigin(), th));
    }

    public void addStatus(ch.qos.logback.core.status.Status status) {
        if (this.context == null) {
            int i = this.noContextWarning;
            this.noContextWarning = i + 1;
            if (i == 0) {
                java.lang.System.out.println("LOGBACK: No context given for " + this);
                return;
            }
            return;
        }
        ch.qos.logback.core.status.StatusManager statusManager = this.context.getStatusManager();
        if (statusManager != null) {
            statusManager.add(status);
        }
    }

    public void addWarn(java.lang.String str) {
        addStatus(new ch.qos.logback.core.status.WarnStatus(str, getDeclaredOrigin()));
    }

    public void addWarn(java.lang.String str, java.lang.Throwable th) {
        addStatus(new ch.qos.logback.core.status.WarnStatus(str, getDeclaredOrigin(), th));
    }

    public ch.qos.logback.core.Context getContext() {
        return this.context;
    }

    /* access modifiers changed from: protected */
    public java.lang.Object getDeclaredOrigin() {
        return this.declaredOrigin;
    }

    public ch.qos.logback.core.status.StatusManager getStatusManager() {
        if (this.context == null) {
            return null;
        }
        return this.context.getStatusManager();
    }

    public void setContext(ch.qos.logback.core.Context context2) {
        if (this.context == null) {
            this.context = context2;
        } else if (this.context != context2) {
            throw new java.lang.IllegalStateException("Context has been already set");
        }
    }
}
