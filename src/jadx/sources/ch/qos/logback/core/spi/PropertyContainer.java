package ch.qos.logback.core.spi;

public interface PropertyContainer {
    java.util.Map<java.lang.String, java.lang.String> getCopyOfPropertyMap();

    java.lang.String getProperty(java.lang.String str);
}
