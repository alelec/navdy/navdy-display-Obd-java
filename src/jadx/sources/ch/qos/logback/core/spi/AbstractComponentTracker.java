package ch.qos.logback.core.spi;

public abstract class AbstractComponentTracker<C> implements ch.qos.logback.core.spi.ComponentTracker<C> {
    private static final boolean ACCESS_ORDERED = true;
    public static final long LINGERING_TIMEOUT = 10000;
    public static final long WAIT_BETWEEN_SUCCESSIVE_REMOVAL_ITERATIONS = 1000;
    private ch.qos.logback.core.spi.AbstractComponentTracker.RemovalPredicator<C> byExcedent = new ch.qos.logback.core.spi.AbstractComponentTracker.RemovalPredicator<C>() {
        public boolean isSlatedForRemoval(ch.qos.logback.core.spi.AbstractComponentTracker.Entry<C> entry, long j) {
            if (ch.qos.logback.core.spi.AbstractComponentTracker.this.liveMap.size() > ch.qos.logback.core.spi.AbstractComponentTracker.this.maxComponents) {
                return ch.qos.logback.core.spi.AbstractComponentTracker.ACCESS_ORDERED;
            }
            return false;
        }
    };
    private ch.qos.logback.core.spi.AbstractComponentTracker.RemovalPredicator<C> byLingering = new ch.qos.logback.core.spi.AbstractComponentTracker.RemovalPredicator<C>() {
        public boolean isSlatedForRemoval(ch.qos.logback.core.spi.AbstractComponentTracker.Entry<C> entry, long j) {
            return ch.qos.logback.core.spi.AbstractComponentTracker.this.isEntryDoneLingering(entry, j);
        }
    };
    private ch.qos.logback.core.spi.AbstractComponentTracker.RemovalPredicator<C> byTimeout = new ch.qos.logback.core.spi.AbstractComponentTracker.RemovalPredicator<C>() {
        public boolean isSlatedForRemoval(ch.qos.logback.core.spi.AbstractComponentTracker.Entry<C> entry, long j) {
            return ch.qos.logback.core.spi.AbstractComponentTracker.this.isEntryStale(entry, j);
        }
    };
    long lastCheck = 0;
    java.util.LinkedHashMap<java.lang.String, ch.qos.logback.core.spi.AbstractComponentTracker.Entry<C>> lingerersMap = new java.util.LinkedHashMap<>(16, 0.75f, ACCESS_ORDERED);
    java.util.LinkedHashMap<java.lang.String, ch.qos.logback.core.spi.AbstractComponentTracker.Entry<C>> liveMap = new java.util.LinkedHashMap<>(32, 0.75f, ACCESS_ORDERED);
    protected int maxComponents = Integer.MAX_VALUE;
    protected long timeout = 1800000;

    private static class Entry<C> {
        C component;
        java.lang.String key;
        long timestamp;

        Entry(java.lang.String str, C c, long j) {
            this.key = str;
            this.component = c;
            this.timestamp = j;
        }

        public boolean equals(java.lang.Object obj) {
            if (this == obj) {
                return ch.qos.logback.core.spi.AbstractComponentTracker.ACCESS_ORDERED;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            ch.qos.logback.core.spi.AbstractComponentTracker.Entry entry = (ch.qos.logback.core.spi.AbstractComponentTracker.Entry) obj;
            if (this.key == null) {
                if (entry.key != null) {
                    return false;
                }
            } else if (!this.key.equals(entry.key)) {
                return false;
            }
            if (this.component == null) {
                if (entry.component != null) {
                    return false;
                }
                return ch.qos.logback.core.spi.AbstractComponentTracker.ACCESS_ORDERED;
            } else if (!this.component.equals(entry.component)) {
                return false;
            } else {
                return ch.qos.logback.core.spi.AbstractComponentTracker.ACCESS_ORDERED;
            }
        }

        public int hashCode() {
            return this.key.hashCode();
        }

        public void setTimestamp(long j) {
            this.timestamp = j;
        }

        public java.lang.String toString() {
            return "(" + this.key + ", " + this.component + ")";
        }
    }

    private interface RemovalPredicator<C> {
        boolean isSlatedForRemoval(ch.qos.logback.core.spi.AbstractComponentTracker.Entry<C> entry, long j);
    }

    private void genericStaleComponentRemover(java.util.LinkedHashMap<java.lang.String, ch.qos.logback.core.spi.AbstractComponentTracker.Entry<C>> linkedHashMap, long j, ch.qos.logback.core.spi.AbstractComponentTracker.RemovalPredicator<C> removalPredicator) {
        java.util.Iterator it = linkedHashMap.entrySet().iterator();
        while (it.hasNext()) {
            ch.qos.logback.core.spi.AbstractComponentTracker.Entry entry = (ch.qos.logback.core.spi.AbstractComponentTracker.Entry) ((java.util.Map.Entry) it.next()).getValue();
            if (removalPredicator.isSlatedForRemoval(entry, j)) {
                it.remove();
                processPriorToRemoval(entry.component);
            } else {
                return;
            }
        }
    }

    private ch.qos.logback.core.spi.AbstractComponentTracker.Entry<C> getFromEitherMap(java.lang.String str) {
        ch.qos.logback.core.spi.AbstractComponentTracker.Entry<C> entry = (ch.qos.logback.core.spi.AbstractComponentTracker.Entry) this.liveMap.get(str);
        return entry != null ? entry : (ch.qos.logback.core.spi.AbstractComponentTracker.Entry) this.lingerersMap.get(str);
    }

    /* access modifiers changed from: private */
    public boolean isEntryDoneLingering(ch.qos.logback.core.spi.AbstractComponentTracker.Entry entry, long j) {
        if (entry.timestamp + 10000 < j) {
            return ACCESS_ORDERED;
        }
        return false;
    }

    /* access modifiers changed from: private */
    public boolean isEntryStale(ch.qos.logback.core.spi.AbstractComponentTracker.Entry<C> entry, long j) {
        if (!isComponentStale(entry.component) && entry.timestamp + this.timeout >= j) {
            return false;
        }
        return ACCESS_ORDERED;
    }

    private boolean isTooSoonForRemovalIteration(long j) {
        if (this.lastCheck + 1000 > j) {
            return ACCESS_ORDERED;
        }
        this.lastCheck = j;
        return false;
    }

    private void removeExcedentComponents() {
        genericStaleComponentRemover(this.liveMap, 0, this.byExcedent);
    }

    private void removeStaleComponentsFromLingerersMap(long j) {
        genericStaleComponentRemover(this.lingerersMap, j, this.byLingering);
    }

    private void removeStaleComponentsFromMainMap(long j) {
        genericStaleComponentRemover(this.liveMap, j, this.byTimeout);
    }

    public java.util.Collection<C> allComponents() {
        java.util.ArrayList arrayList = new java.util.ArrayList();
        for (ch.qos.logback.core.spi.AbstractComponentTracker.Entry entry : this.liveMap.values()) {
            arrayList.add(entry.component);
        }
        for (ch.qos.logback.core.spi.AbstractComponentTracker.Entry entry2 : this.lingerersMap.values()) {
            arrayList.add(entry2.component);
        }
        return arrayList;
    }

    public java.util.Set<java.lang.String> allKeys() {
        java.util.HashSet hashSet = new java.util.HashSet(this.liveMap.keySet());
        hashSet.addAll(this.lingerersMap.keySet());
        return hashSet;
    }

    /* access modifiers changed from: protected */
    public abstract C buildComponent(java.lang.String str);

    public void endOfLife(java.lang.String str) {
        ch.qos.logback.core.spi.AbstractComponentTracker.Entry entry = (ch.qos.logback.core.spi.AbstractComponentTracker.Entry) this.liveMap.remove(str);
        if (entry != null) {
            this.lingerersMap.put(str, entry);
        }
    }

    public synchronized C find(java.lang.String str) {
        ch.qos.logback.core.spi.AbstractComponentTracker.Entry fromEitherMap;
        fromEitherMap = getFromEitherMap(str);
        return fromEitherMap == null ? null : fromEitherMap.component;
    }

    public int getComponentCount() {
        return this.liveMap.size() + this.lingerersMap.size();
    }

    public int getMaxComponents() {
        return this.maxComponents;
    }

    public synchronized C getOrCreate(java.lang.String str, long j) {
        ch.qos.logback.core.spi.AbstractComponentTracker.Entry fromEitherMap;
        fromEitherMap = getFromEitherMap(str);
        if (fromEitherMap == null) {
            fromEitherMap = new ch.qos.logback.core.spi.AbstractComponentTracker.Entry(str, buildComponent(str), j);
            this.liveMap.put(str, fromEitherMap);
        } else {
            fromEitherMap.setTimestamp(j);
        }
        return fromEitherMap.component;
    }

    public long getTimeout() {
        return this.timeout;
    }

    /* access modifiers changed from: protected */
    public abstract boolean isComponentStale(C c);

    /* access modifiers changed from: protected */
    public abstract void processPriorToRemoval(C c);

    public synchronized void removeStaleComponents(long j) {
        if (!isTooSoonForRemovalIteration(j)) {
            removeExcedentComponents();
            removeStaleComponentsFromMainMap(j);
            removeStaleComponentsFromLingerersMap(j);
        }
    }

    public void setMaxComponents(int i) {
        this.maxComponents = i;
    }

    public void setTimeout(long j) {
        this.timeout = j;
    }
}
