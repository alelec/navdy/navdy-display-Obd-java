package ch.qos.logback.core.spi;

public class ScanException extends java.lang.Exception {
    private static final long serialVersionUID = -3132040414328475658L;
    java.lang.Throwable cause;

    public ScanException(java.lang.String str) {
        super(str);
    }

    public ScanException(java.lang.String str, java.lang.Throwable th) {
        super(str);
        this.cause = th;
    }

    public java.lang.Throwable getCause() {
        return this.cause;
    }
}
