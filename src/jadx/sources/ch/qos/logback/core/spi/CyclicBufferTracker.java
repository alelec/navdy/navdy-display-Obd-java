package ch.qos.logback.core.spi;

public class CyclicBufferTracker<E> extends ch.qos.logback.core.spi.AbstractComponentTracker<ch.qos.logback.core.helpers.CyclicBuffer<E>> {
    static final int DEFAULT_BUFFER_SIZE = 256;
    static final int DEFAULT_NUMBER_OF_BUFFERS = 64;
    int bufferSize = 256;

    public CyclicBufferTracker() {
        setMaxComponents(64);
    }

    /* access modifiers changed from: protected */
    public ch.qos.logback.core.helpers.CyclicBuffer<E> buildComponent(java.lang.String str) {
        return new ch.qos.logback.core.helpers.CyclicBuffer<>(this.bufferSize);
    }

    public int getBufferSize() {
        return this.bufferSize;
    }

    /* access modifiers changed from: protected */
    public boolean isComponentStale(ch.qos.logback.core.helpers.CyclicBuffer<E> cyclicBuffer) {
        return false;
    }

    /* access modifiers changed from: 0000 */
    public java.util.List<java.lang.String> lingererKeysAsOrderedList() {
        return new java.util.ArrayList(this.lingerersMap.keySet());
    }

    /* access modifiers changed from: 0000 */
    public java.util.List<java.lang.String> liveKeysAsOrderedList() {
        return new java.util.ArrayList(this.liveMap.keySet());
    }

    /* access modifiers changed from: protected */
    public void processPriorToRemoval(ch.qos.logback.core.helpers.CyclicBuffer<E> cyclicBuffer) {
        cyclicBuffer.clear();
    }

    public void setBufferSize(int i) {
        this.bufferSize = i;
    }
}
