package ch.qos.logback.core.spi;

public class AppenderAttachableImpl<E> implements ch.qos.logback.core.spi.AppenderAttachable<E> {
    static final long START = java.lang.System.currentTimeMillis();
    private final java.util.concurrent.CopyOnWriteArrayList<ch.qos.logback.core.Appender<E>> appenderList = new java.util.concurrent.CopyOnWriteArrayList<>();

    public void addAppender(ch.qos.logback.core.Appender<E> appender) {
        if (appender == null) {
            throw new java.lang.IllegalArgumentException("Null argument disallowed");
        }
        this.appenderList.addIfAbsent(appender);
    }

    public int appendLoopOnAppenders(E e) {
        int i = 0;
        java.util.Iterator it = this.appenderList.iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return i2;
            }
            ((ch.qos.logback.core.Appender) it.next()).doAppend(e);
            i = i2 + 1;
        }
    }

    public void detachAndStopAllAppenders() {
        java.util.Iterator it = this.appenderList.iterator();
        while (it.hasNext()) {
            ((ch.qos.logback.core.Appender) it.next()).stop();
        }
        this.appenderList.clear();
    }

    public boolean detachAppender(ch.qos.logback.core.Appender<E> appender) {
        if (appender == null) {
            return false;
        }
        return this.appenderList.remove(appender);
    }

    public boolean detachAppender(java.lang.String str) {
        boolean z;
        if (str == null) {
            return false;
        }
        java.util.Iterator it = this.appenderList.iterator();
        while (true) {
            if (!it.hasNext()) {
                z = false;
                break;
            }
            ch.qos.logback.core.Appender appender = (ch.qos.logback.core.Appender) it.next();
            if (str.equals(appender.getName())) {
                z = this.appenderList.remove(appender);
                break;
            }
        }
        return z;
    }

    public ch.qos.logback.core.Appender<E> getAppender(java.lang.String str) {
        if (str == null) {
            return null;
        }
        java.util.Iterator it = this.appenderList.iterator();
        while (it.hasNext()) {
            ch.qos.logback.core.Appender<E> appender = (ch.qos.logback.core.Appender) it.next();
            if (str.equals(appender.getName())) {
                return appender;
            }
        }
        return null;
    }

    public boolean isAttached(ch.qos.logback.core.Appender<E> appender) {
        if (appender == null) {
            return false;
        }
        java.util.Iterator it = this.appenderList.iterator();
        while (it.hasNext()) {
            if (((ch.qos.logback.core.Appender) it.next()) == appender) {
                return true;
            }
        }
        return false;
    }

    public java.util.Iterator<ch.qos.logback.core.Appender<E>> iteratorForAppenders() {
        return this.appenderList.iterator();
    }
}
