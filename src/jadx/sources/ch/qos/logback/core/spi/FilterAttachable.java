package ch.qos.logback.core.spi;

public interface FilterAttachable<E> {
    void addFilter(ch.qos.logback.core.filter.Filter<E> filter);

    void clearAllFilters();

    java.util.List<ch.qos.logback.core.filter.Filter<E>> getCopyOfAttachedFiltersList();

    ch.qos.logback.core.spi.FilterReply getFilterChainDecision(E e);
}
