package ch.qos.logback.core.spi;

public interface ContextAware {
    void addError(java.lang.String str);

    void addError(java.lang.String str, java.lang.Throwable th);

    void addInfo(java.lang.String str);

    void addInfo(java.lang.String str, java.lang.Throwable th);

    void addStatus(ch.qos.logback.core.status.Status status);

    void addWarn(java.lang.String str);

    void addWarn(java.lang.String str, java.lang.Throwable th);

    ch.qos.logback.core.Context getContext();

    void setContext(ch.qos.logback.core.Context context);
}
