package ch.qos.logback.core.spi;

public interface AppenderAttachable<E> {
    void addAppender(ch.qos.logback.core.Appender<E> appender);

    void detachAndStopAllAppenders();

    boolean detachAppender(ch.qos.logback.core.Appender<E> appender);

    boolean detachAppender(java.lang.String str);

    ch.qos.logback.core.Appender<E> getAppender(java.lang.String str);

    boolean isAttached(ch.qos.logback.core.Appender<E> appender);

    java.util.Iterator<ch.qos.logback.core.Appender<E>> iteratorForAppenders();
}
