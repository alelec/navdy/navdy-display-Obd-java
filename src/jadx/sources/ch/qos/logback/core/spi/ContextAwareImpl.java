package ch.qos.logback.core.spi;

public class ContextAwareImpl implements ch.qos.logback.core.spi.ContextAware {
    protected ch.qos.logback.core.Context context;
    private int noContextWarning = 0;
    final java.lang.Object origin;

    public ContextAwareImpl(ch.qos.logback.core.Context context2, java.lang.Object obj) {
        this.context = context2;
        this.origin = obj;
    }

    public void addError(java.lang.String str) {
        addStatus(new ch.qos.logback.core.status.ErrorStatus(str, getOrigin()));
    }

    public void addError(java.lang.String str, java.lang.Throwable th) {
        addStatus(new ch.qos.logback.core.status.ErrorStatus(str, getOrigin(), th));
    }

    public void addInfo(java.lang.String str) {
        addStatus(new ch.qos.logback.core.status.InfoStatus(str, getOrigin()));
    }

    public void addInfo(java.lang.String str, java.lang.Throwable th) {
        addStatus(new ch.qos.logback.core.status.InfoStatus(str, getOrigin(), th));
    }

    public void addStatus(ch.qos.logback.core.status.Status status) {
        if (this.context == null) {
            int i = this.noContextWarning;
            this.noContextWarning = i + 1;
            if (i == 0) {
                java.lang.System.out.println("LOGBACK: No context given for " + this);
                return;
            }
            return;
        }
        ch.qos.logback.core.status.StatusManager statusManager = this.context.getStatusManager();
        if (statusManager != null) {
            statusManager.add(status);
        }
    }

    public void addWarn(java.lang.String str) {
        addStatus(new ch.qos.logback.core.status.WarnStatus(str, getOrigin()));
    }

    public void addWarn(java.lang.String str, java.lang.Throwable th) {
        addStatus(new ch.qos.logback.core.status.WarnStatus(str, getOrigin(), th));
    }

    public ch.qos.logback.core.Context getContext() {
        return this.context;
    }

    /* access modifiers changed from: protected */
    public java.lang.Object getOrigin() {
        return this.origin;
    }

    public ch.qos.logback.core.status.StatusManager getStatusManager() {
        if (this.context == null) {
            return null;
        }
        return this.context.getStatusManager();
    }

    public void setContext(ch.qos.logback.core.Context context2) {
        if (this.context == null) {
            this.context = context2;
        } else if (this.context != context2) {
            throw new java.lang.IllegalStateException("Context has been already set");
        }
    }
}
