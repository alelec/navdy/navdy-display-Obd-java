package ch.qos.logback.core.spi;

public final class FilterAttachableImpl<E> implements ch.qos.logback.core.spi.FilterAttachable<E> {
    java.util.concurrent.CopyOnWriteArrayList<ch.qos.logback.core.filter.Filter<E>> filterList = new java.util.concurrent.CopyOnWriteArrayList<>();

    public void addFilter(ch.qos.logback.core.filter.Filter<E> filter) {
        this.filterList.add(filter);
    }

    public void clearAllFilters() {
        this.filterList.clear();
    }

    public java.util.List<ch.qos.logback.core.filter.Filter<E>> getCopyOfAttachedFiltersList() {
        return new java.util.ArrayList(this.filterList);
    }

    public ch.qos.logback.core.spi.FilterReply getFilterChainDecision(E e) {
        java.util.Iterator it = this.filterList.iterator();
        while (it.hasNext()) {
            ch.qos.logback.core.spi.FilterReply decide = ((ch.qos.logback.core.filter.Filter) it.next()).decide(e);
            if (decide == ch.qos.logback.core.spi.FilterReply.DENY) {
                return decide;
            }
            if (decide == ch.qos.logback.core.spi.FilterReply.ACCEPT) {
                return decide;
            }
        }
        return ch.qos.logback.core.spi.FilterReply.NEUTRAL;
    }
}
