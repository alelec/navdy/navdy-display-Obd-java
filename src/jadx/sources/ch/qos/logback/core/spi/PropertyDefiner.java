package ch.qos.logback.core.spi;

public interface PropertyDefiner extends ch.qos.logback.core.spi.ContextAware {
    java.lang.String getPropertyValue();
}
