package ch.qos.logback.core;

public class LogbackException extends java.lang.RuntimeException {
    private static final long serialVersionUID = -799956346239073266L;

    public LogbackException(java.lang.String str) {
        super(str);
    }

    public LogbackException(java.lang.String str, java.lang.Throwable th) {
        super(str, th);
    }
}
