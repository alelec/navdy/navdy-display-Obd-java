package ch.qos.logback.core.boolex;

public interface EventEvaluator<E> extends ch.qos.logback.core.spi.ContextAware, ch.qos.logback.core.spi.LifeCycle {
    boolean evaluate(E e) throws java.lang.NullPointerException, ch.qos.logback.core.boolex.EvaluationException;

    java.lang.String getName();

    void setName(java.lang.String str);
}
