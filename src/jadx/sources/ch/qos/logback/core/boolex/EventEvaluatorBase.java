package ch.qos.logback.core.boolex;

public abstract class EventEvaluatorBase<E> extends ch.qos.logback.core.spi.ContextAwareBase implements ch.qos.logback.core.boolex.EventEvaluator<E> {
    java.lang.String name;
    boolean started;

    public java.lang.String getName() {
        return this.name;
    }

    public boolean isStarted() {
        return this.started;
    }

    public void setName(java.lang.String str) {
        if (this.name != null) {
            throw new java.lang.IllegalStateException("name has been already set");
        }
        this.name = str;
    }

    public void start() {
        this.started = true;
    }

    public void stop() {
        this.started = false;
    }
}
