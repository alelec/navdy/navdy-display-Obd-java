package ch.qos.logback.core.boolex;

public class EvaluationException extends java.lang.Exception {
    private static final long serialVersionUID = 1;

    public EvaluationException(java.lang.String str) {
        super(str);
    }

    public EvaluationException(java.lang.String str, java.lang.Throwable th) {
        super(str, th);
    }

    public EvaluationException(java.lang.Throwable th) {
        super(th);
    }
}
