package ch.qos.logback.core.property;

public class FileExistsPropertyDefiner extends ch.qos.logback.core.PropertyDefinerBase {
    java.lang.String path;

    public java.lang.String getPath() {
        return this.path;
    }

    public java.lang.String getPropertyValue() {
        if (!ch.qos.logback.core.util.OptionHelper.isEmpty(this.path)) {
            return booleanAsStr(new java.io.File(this.path).exists());
        }
        addError("The \"path\" property must be set.");
        return null;
    }

    public void setPath(java.lang.String str) {
        this.path = str;
    }
}
