package ch.qos.logback.core.property;

public class ResourceExistsPropertyDefiner extends ch.qos.logback.core.PropertyDefinerBase {
    java.lang.String resourceStr;

    public java.lang.String getPropertyValue() {
        if (ch.qos.logback.core.util.OptionHelper.isEmpty(this.resourceStr)) {
            addError("The \"resource\" property must be set.");
            return null;
        }
        return booleanAsStr(ch.qos.logback.core.util.Loader.getResourceBySelfClassLoader(this.resourceStr) != null);
    }

    public java.lang.String getResource() {
        return this.resourceStr;
    }

    public void setResource(java.lang.String str) {
        this.resourceStr = str;
    }
}
