package ch.qos.logback.core.android;

public class SystemPropertiesProxy {
    private static final ch.qos.logback.core.android.SystemPropertiesProxy SINGLETON = new ch.qos.logback.core.android.SystemPropertiesProxy(null);
    private java.lang.Class<?> SystemProperties;
    private java.lang.reflect.Method getBoolean;
    private java.lang.reflect.Method getString;

    private SystemPropertiesProxy(java.lang.ClassLoader classLoader) {
        try {
            setClassLoader(classLoader);
        } catch (java.lang.Exception e) {
        }
    }

    public static ch.qos.logback.core.android.SystemPropertiesProxy getInstance() {
        return SINGLETON;
    }

    public java.lang.String get(java.lang.String str, java.lang.String str2) throws java.lang.IllegalArgumentException {
        java.lang.String str3;
        if (this.SystemProperties == null || this.getString == null) {
            return null;
        }
        try {
            str3 = (java.lang.String) this.getString.invoke(this.SystemProperties, new java.lang.Object[]{str, str2});
        } catch (java.lang.IllegalArgumentException e) {
            throw e;
        } catch (java.lang.Exception e2) {
            str3 = null;
        }
        return (str3 == null || str3.length() == 0) ? str2 : str3;
    }

    public java.lang.Boolean getBoolean(java.lang.String str, boolean z) throws java.lang.IllegalArgumentException {
        if (this.SystemProperties == null || this.getBoolean == null) {
            return java.lang.Boolean.valueOf(z);
        }
        java.lang.Boolean valueOf = java.lang.Boolean.valueOf(z);
        try {
            return (java.lang.Boolean) this.getBoolean.invoke(this.SystemProperties, new java.lang.Object[]{str, java.lang.Boolean.valueOf(z)});
        } catch (java.lang.IllegalArgumentException e) {
            throw e;
        } catch (java.lang.Exception e2) {
            return valueOf;
        }
    }

    public void setClassLoader(java.lang.ClassLoader classLoader) throws java.lang.ClassNotFoundException, java.lang.SecurityException, java.lang.NoSuchMethodException {
        if (classLoader == null) {
            classLoader = getClass().getClassLoader();
        }
        this.SystemProperties = classLoader.loadClass("android.os.SystemProperties");
        this.getString = this.SystemProperties.getMethod("get", new java.lang.Class[]{java.lang.String.class, java.lang.String.class});
        this.getBoolean = this.SystemProperties.getMethod("getBoolean", new java.lang.Class[]{java.lang.String.class, java.lang.Boolean.TYPE});
    }
}
