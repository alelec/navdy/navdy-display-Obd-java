package ch.qos.logback.core.android;

public abstract class CommonPathUtil {
    private static final java.lang.String ASSETS_DIRECTORY = "assets";

    public static java.lang.String getAssetsDirectoryPath() {
        return ASSETS_DIRECTORY;
    }

    public static java.lang.String getDatabaseDirectoryPath(java.lang.String str) {
        return (ch.qos.logback.core.util.EnvUtil.isAndroidOS() ? android.os.Environment.getDataDirectory().getAbsolutePath() : "/data") + "/data/" + str + "/databases";
    }

    public static java.lang.String getExternalStorageDirectoryPath() {
        if (ch.qos.logback.core.util.EnvUtil.isAndroidOS()) {
            return android.os.Environment.getExternalStorageDirectory().getAbsolutePath();
        }
        java.lang.String env = ch.qos.logback.core.util.OptionHelper.getEnv("EXTERNAL_STORAGE");
        return env == null ? "/sdcard" : env;
    }

    public static java.lang.String getFilesDirectoryPath(java.lang.String str) {
        return (ch.qos.logback.core.util.EnvUtil.isAndroidOS() ? android.os.Environment.getDataDirectory().getAbsolutePath() : "/data") + "/data/" + str + "/files";
    }

    public static java.lang.String getMountedExternalStorageDirectoryPath() {
        if (!ch.qos.logback.core.util.EnvUtil.isAndroidOS()) {
            return "/mnt/sdcard";
        }
        java.lang.String externalStorageState = android.os.Environment.getExternalStorageState();
        if (externalStorageState.equals("mounted") || externalStorageState.equals("mounted_ro")) {
            return android.os.Environment.getExternalStorageDirectory().getAbsolutePath();
        }
        return null;
    }
}
