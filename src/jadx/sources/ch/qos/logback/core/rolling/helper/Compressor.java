package ch.qos.logback.core.rolling.helper;

public class Compressor extends ch.qos.logback.core.spi.ContextAwareBase {
    static final int BUFFER_SIZE = 8192;
    final ch.qos.logback.core.rolling.helper.CompressionMode compressionMode;

    public Compressor(ch.qos.logback.core.rolling.helper.CompressionMode compressionMode2) {
        this.compressionMode = compressionMode2;
    }

    public static java.lang.String computeFileNameStr_WCS(java.lang.String str, ch.qos.logback.core.rolling.helper.CompressionMode compressionMode2) {
        int length = str.length();
        switch (compressionMode2) {
            case GZ:
                return str.endsWith(".gz") ? str.substring(0, length - 3) : str;
            case ZIP:
                return str.endsWith(".zip") ? str.substring(0, length - 4) : str;
            case NONE:
                return str;
            default:
                throw new java.lang.IllegalStateException("Execution should not reach this point");
        }
    }

    /* JADX WARNING: type inference failed for: r2v0 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00ed A[SYNTHETIC, Splitter:B:25:0x00ed] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00f2 A[SYNTHETIC, Splitter:B:28:0x00f2] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x013c A[SYNTHETIC, Splitter:B:51:0x013c] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x0141 A[SYNTHETIC, Splitter:B:54:0x0141] */
    /* JADX WARNING: Removed duplicated region for block: B:75:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Unknown variable types count: 1 */
    private void gzCompress(java.lang.String str, java.lang.String str2) {
        java.util.zip.GZIPOutputStream gZIPOutputStream;
        java.io.BufferedInputStream bufferedInputStream;
        java.util.zip.GZIPOutputStream gZIPOutputStream2;
        java.io.BufferedInputStream bufferedInputStream2;
        java.util.zip.GZIPOutputStream gZIPOutputStream3;
        ? r2 = 0;
        java.io.File file = new java.io.File(str);
        if (!file.exists()) {
            addStatus(new ch.qos.logback.core.status.WarnStatus("The file to compress named [" + str + "] does not exist.", this));
            return;
        }
        if (!str2.endsWith(".gz")) {
            str2 = str2 + ".gz";
        }
        java.io.File file2 = new java.io.File(str2);
        if (file2.exists()) {
            addWarn("The target compressed file named [" + str2 + "] exist already. Aborting file compression.");
            return;
        }
        addInfo("GZ compressing [" + file + "] as [" + file2 + "]");
        createMissingTargetDirsIfNecessary(file2);
        try {
            java.io.BufferedInputStream bufferedInputStream3 = new java.io.BufferedInputStream(new java.io.FileInputStream(str));
            try {
                gZIPOutputStream3 = new java.util.zip.GZIPOutputStream(new java.io.FileOutputStream(str2));
            } catch (java.lang.Exception e) {
                e = e;
                gZIPOutputStream = 0;
                bufferedInputStream2 = bufferedInputStream3;
                try {
                    addStatus(new ch.qos.logback.core.status.ErrorStatus("Error occurred while compressing [" + str + "] into [" + str2 + "].", this, e));
                    if (bufferedInputStream2 != 0) {
                        try {
                            bufferedInputStream2.close();
                        } catch (java.io.IOException e2) {
                        }
                    }
                    if (gZIPOutputStream == 0) {
                        try {
                            gZIPOutputStream.close();
                            return;
                        } catch (java.io.IOException e3) {
                            return;
                        }
                    } else {
                        return;
                    }
                } catch (Throwable th) {
                    th = th;
                    bufferedInputStream = bufferedInputStream2;
                    gZIPOutputStream2 = gZIPOutputStream;
                    if (bufferedInputStream != 0) {
                        try {
                            bufferedInputStream.close();
                        } catch (java.io.IOException e4) {
                        }
                    }
                    if (gZIPOutputStream2 != 0) {
                        try {
                            gZIPOutputStream2.close();
                        } catch (java.io.IOException e5) {
                        }
                    }
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                gZIPOutputStream2 = r2;
                bufferedInputStream = bufferedInputStream3;
                if (bufferedInputStream != 0) {
                }
                if (gZIPOutputStream2 != 0) {
                }
                throw th;
            }
            try {
                byte[] bArr = new byte[8192];
                while (true) {
                    int read = bufferedInputStream3.read(bArr);
                    if (read == -1) {
                        break;
                    }
                    gZIPOutputStream3.write(bArr, 0, read);
                }
                bufferedInputStream3.close();
                java.io.BufferedInputStream bufferedInputStream4 = null;
                try {
                    gZIPOutputStream3.close();
                    java.util.zip.GZIPOutputStream gZIPOutputStream4 = null;
                    if (!file.delete()) {
                        addStatus(new ch.qos.logback.core.status.WarnStatus("Could not delete [" + str + "].", this));
                    }
                    if (0 != 0) {
                        try {
                            bufferedInputStream4.close();
                        } catch (java.io.IOException e6) {
                        }
                    }
                    if (0 != 0) {
                        try {
                            gZIPOutputStream4.close();
                        } catch (java.io.IOException e7) {
                        }
                    }
                } catch (java.lang.Exception e8) {
                    e = e8;
                    bufferedInputStream2 = r2;
                    gZIPOutputStream = gZIPOutputStream3;
                    addStatus(new ch.qos.logback.core.status.ErrorStatus("Error occurred while compressing [" + str + "] into [" + str2 + "].", this, e));
                    if (bufferedInputStream2 != 0) {
                    }
                    if (gZIPOutputStream == 0) {
                    }
                } catch (Throwable th3) {
                    th = th3;
                    bufferedInputStream = 0;
                    gZIPOutputStream2 = gZIPOutputStream3;
                    if (bufferedInputStream != 0) {
                    }
                    if (gZIPOutputStream2 != 0) {
                    }
                    throw th;
                }
            } catch (java.lang.Exception e9) {
                e = e9;
                bufferedInputStream2 = bufferedInputStream3;
                gZIPOutputStream = gZIPOutputStream3;
                addStatus(new ch.qos.logback.core.status.ErrorStatus("Error occurred while compressing [" + str + "] into [" + str2 + "].", this, e));
                if (bufferedInputStream2 != 0) {
                }
                if (gZIPOutputStream == 0) {
                }
            } catch (Throwable th4) {
                th = th4;
                gZIPOutputStream2 = gZIPOutputStream3;
                bufferedInputStream = bufferedInputStream3;
                if (bufferedInputStream != 0) {
                }
                if (gZIPOutputStream2 != 0) {
                }
                throw th;
            }
        } catch (java.lang.Exception e10) {
            e = e10;
            gZIPOutputStream = 0;
            bufferedInputStream2 = r2;
            addStatus(new ch.qos.logback.core.status.ErrorStatus("Error occurred while compressing [" + str + "] into [" + str2 + "].", this, e));
            if (bufferedInputStream2 != 0) {
            }
            if (gZIPOutputStream == 0) {
            }
        } catch (Throwable th5) {
            th = th5;
            bufferedInputStream = 0;
            gZIPOutputStream2 = r2;
            if (bufferedInputStream != 0) {
            }
            if (gZIPOutputStream2 != 0) {
            }
            throw th;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x0106 A[SYNTHETIC, Splitter:B:26:0x0106] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x010b A[SYNTHETIC, Splitter:B:29:0x010b] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0155 A[SYNTHETIC, Splitter:B:52:0x0155] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x015a A[SYNTHETIC, Splitter:B:55:0x015a] */
    /* JADX WARNING: Removed duplicated region for block: B:75:? A[RETURN, SYNTHETIC] */
    private void zipCompress(java.lang.String str, java.lang.String str2, java.lang.String str3) {
        java.util.zip.ZipOutputStream zipOutputStream;
        java.io.BufferedInputStream bufferedInputStream = null;
        java.io.File file = new java.io.File(str);
        if (!file.exists()) {
            addStatus(new ch.qos.logback.core.status.WarnStatus("The file to compress named [" + str + "] does not exist.", this));
        } else if (str3 == null) {
            addStatus(new ch.qos.logback.core.status.WarnStatus("The innerEntryName parameter cannot be null", this));
        } else {
            if (!str2.endsWith(".zip")) {
                str2 = str2 + ".zip";
            }
            java.io.File file2 = new java.io.File(str2);
            if (file2.exists()) {
                addStatus(new ch.qos.logback.core.status.WarnStatus("The target compressed file named [" + str2 + "] exist already.", this));
                return;
            }
            addInfo("ZIP compressing [" + file + "] as [" + file2 + "]");
            createMissingTargetDirsIfNecessary(file2);
            try {
                java.io.BufferedInputStream bufferedInputStream2 = new java.io.BufferedInputStream(new java.io.FileInputStream(str));
                try {
                    zipOutputStream = new java.util.zip.ZipOutputStream(new java.io.FileOutputStream(str2));
                } catch (java.lang.Exception e) {
                    e = e;
                    zipOutputStream = null;
                    bufferedInputStream = bufferedInputStream2;
                    try {
                        addStatus(new ch.qos.logback.core.status.ErrorStatus("Error occurred while compressing [" + str + "] into [" + str2 + "].", this, e));
                        if (bufferedInputStream != null) {
                        }
                        if (zipOutputStream == null) {
                        }
                    } catch (Throwable th) {
                        th = th;
                        if (bufferedInputStream != null) {
                            try {
                                bufferedInputStream.close();
                            } catch (java.io.IOException e2) {
                            }
                        }
                        if (zipOutputStream != null) {
                            try {
                                zipOutputStream.close();
                            } catch (java.io.IOException e3) {
                            }
                        }
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    zipOutputStream = null;
                    bufferedInputStream = bufferedInputStream2;
                    if (bufferedInputStream != null) {
                    }
                    if (zipOutputStream != null) {
                    }
                    throw th;
                }
                try {
                    zipOutputStream.putNextEntry(computeZipEntry(str3));
                    byte[] bArr = new byte[8192];
                    while (true) {
                        int read = bufferedInputStream2.read(bArr);
                        if (read == -1) {
                            break;
                        }
                        zipOutputStream.write(bArr, 0, read);
                    }
                    bufferedInputStream2.close();
                    java.io.BufferedInputStream bufferedInputStream3 = null;
                    try {
                        zipOutputStream.close();
                        java.util.zip.ZipOutputStream zipOutputStream2 = null;
                        if (!file.delete()) {
                            addStatus(new ch.qos.logback.core.status.WarnStatus("Could not delete [" + str + "].", this));
                        }
                        if (0 != 0) {
                            try {
                                bufferedInputStream3.close();
                            } catch (java.io.IOException e4) {
                            }
                        }
                        if (0 != 0) {
                            try {
                                zipOutputStream2.close();
                            } catch (java.io.IOException e5) {
                            }
                        }
                    } catch (java.lang.Exception e6) {
                        e = e6;
                        addStatus(new ch.qos.logback.core.status.ErrorStatus("Error occurred while compressing [" + str + "] into [" + str2 + "].", this, e));
                        if (bufferedInputStream != null) {
                            try {
                                bufferedInputStream.close();
                            } catch (java.io.IOException e7) {
                            }
                        }
                        if (zipOutputStream == null) {
                            try {
                                zipOutputStream.close();
                            } catch (java.io.IOException e8) {
                            }
                        }
                    }
                } catch (java.lang.Exception e9) {
                    e = e9;
                    bufferedInputStream = bufferedInputStream2;
                    addStatus(new ch.qos.logback.core.status.ErrorStatus("Error occurred while compressing [" + str + "] into [" + str2 + "].", this, e));
                    if (bufferedInputStream != null) {
                    }
                    if (zipOutputStream == null) {
                    }
                } catch (Throwable th3) {
                    th = th3;
                    bufferedInputStream = bufferedInputStream2;
                    if (bufferedInputStream != null) {
                    }
                    if (zipOutputStream != null) {
                    }
                    throw th;
                }
            } catch (java.lang.Exception e10) {
                e = e10;
                zipOutputStream = null;
                addStatus(new ch.qos.logback.core.status.ErrorStatus("Error occurred while compressing [" + str + "] into [" + str2 + "].", this, e));
                if (bufferedInputStream != null) {
                }
                if (zipOutputStream == null) {
                }
            } catch (Throwable th4) {
                th = th4;
                zipOutputStream = null;
                if (bufferedInputStream != null) {
                }
                if (zipOutputStream != null) {
                }
                throw th;
            }
        }
    }

    public void compress(java.lang.String str, java.lang.String str2, java.lang.String str3) {
        switch (this.compressionMode) {
            case GZ:
                gzCompress(str, str2);
                return;
            case ZIP:
                zipCompress(str, str2, str3);
                return;
            case NONE:
                throw new java.lang.UnsupportedOperationException("compress method called in NONE compression mode");
            default:
                return;
        }
    }

    /* access modifiers changed from: 0000 */
    public java.util.zip.ZipEntry computeZipEntry(java.io.File file) {
        return computeZipEntry(file.getName());
    }

    /* access modifiers changed from: 0000 */
    public java.util.zip.ZipEntry computeZipEntry(java.lang.String str) {
        return new java.util.zip.ZipEntry(computeFileNameStr_WCS(str, this.compressionMode));
    }

    /* access modifiers changed from: 0000 */
    public void createMissingTargetDirsIfNecessary(java.io.File file) {
        if (!ch.qos.logback.core.util.FileUtil.isParentDirectoryCreationRequired(file)) {
            return;
        }
        if (!ch.qos.logback.core.util.FileUtil.createMissingParentDirectories(file)) {
            addError("Failed to create parent directories for [" + file.getAbsolutePath() + "]");
        } else {
            addInfo("Created missing parent directories for [" + file.getAbsolutePath() + "]");
        }
    }

    public java.lang.String toString() {
        return getClass().getName();
    }
}
