package ch.qos.logback.core.rolling;

public class TimeBasedRollingPolicy<E> extends ch.qos.logback.core.rolling.RollingPolicyBase implements ch.qos.logback.core.rolling.TriggeringPolicy<E> {
    static final java.lang.String FNP_NOT_SET = "The FileNamePattern option must be set before using TimeBasedRollingPolicy. ";
    static final int INFINITE_HISTORY = 0;
    private ch.qos.logback.core.rolling.helper.ArchiveRemover archiveRemover;
    boolean cleanHistoryOnStart = false;
    private ch.qos.logback.core.rolling.helper.Compressor compressor;
    ch.qos.logback.core.rolling.helper.FileNamePattern fileNamePatternWCS;
    java.util.concurrent.Future<?> future;
    private int maxHistory = 0;
    private ch.qos.logback.core.rolling.helper.RenameUtil renameUtil = new ch.qos.logback.core.rolling.helper.RenameUtil();
    ch.qos.logback.core.rolling.TimeBasedFileNamingAndTriggeringPolicy<E> timeBasedFileNamingAndTriggeringPolicy;

    private java.lang.String transformFileNamePattern2ZipEntry(java.lang.String str) {
        return ch.qos.logback.core.rolling.helper.FileFilterUtil.afterLastSlash(ch.qos.logback.core.rolling.helper.FileFilterUtil.slashify(str));
    }

    private void waitForAsynchronousJobToStop() {
        if (this.future != null) {
            try {
                this.future.get(30, java.util.concurrent.TimeUnit.SECONDS);
            } catch (java.util.concurrent.TimeoutException e) {
                addError("Timeout while waiting for compression job to finish", e);
            } catch (java.lang.Exception e2) {
                addError("Unexpected exception while waiting for compression job to finish", e2);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public java.util.concurrent.Future asyncCompress(java.lang.String str, java.lang.String str2, java.lang.String str3) throws ch.qos.logback.core.rolling.RolloverFailure {
        return new ch.qos.logback.core.rolling.helper.AsynchronousCompressor(this.compressor).compressAsynchronously(str, str2, str3);
    }

    public java.lang.String getActiveFileName() {
        java.lang.String parentsRawFileProperty = getParentsRawFileProperty();
        return parentsRawFileProperty != null ? parentsRawFileProperty : this.timeBasedFileNamingAndTriggeringPolicy.getCurrentPeriodsFileNameWithoutCompressionSuffix();
    }

    public int getMaxHistory() {
        return this.maxHistory;
    }

    public ch.qos.logback.core.rolling.TimeBasedFileNamingAndTriggeringPolicy<E> getTimeBasedFileNamingAndTriggeringPolicy() {
        return this.timeBasedFileNamingAndTriggeringPolicy;
    }

    public boolean isCleanHistoryOnStart() {
        return this.cleanHistoryOnStart;
    }

    public boolean isTriggeringEvent(java.io.File file, E e) {
        return this.timeBasedFileNamingAndTriggeringPolicy.isTriggeringEvent(file, e);
    }

    /* access modifiers changed from: 0000 */
    public java.util.concurrent.Future renamedRawAndAsyncCompress(java.lang.String str, java.lang.String str2) throws ch.qos.logback.core.rolling.RolloverFailure {
        java.lang.String parentsRawFileProperty = getParentsRawFileProperty();
        java.lang.String str3 = parentsRawFileProperty + java.lang.System.nanoTime() + ".tmp";
        this.renameUtil.rename(parentsRawFileProperty, str3);
        return asyncCompress(str3, str, str2);
    }

    public void rollover() throws ch.qos.logback.core.rolling.RolloverFailure {
        java.lang.String elapsedPeriodsFileName = this.timeBasedFileNamingAndTriggeringPolicy.getElapsedPeriodsFileName();
        java.lang.String afterLastSlash = ch.qos.logback.core.rolling.helper.FileFilterUtil.afterLastSlash(elapsedPeriodsFileName);
        if (this.compressionMode == ch.qos.logback.core.rolling.helper.CompressionMode.NONE) {
            if (getParentsRawFileProperty() != null) {
                this.renameUtil.rename(getParentsRawFileProperty(), elapsedPeriodsFileName);
            }
        } else if (getParentsRawFileProperty() == null) {
            this.future = asyncCompress(elapsedPeriodsFileName, elapsedPeriodsFileName, afterLastSlash);
        } else {
            this.future = renamedRawAndAsyncCompress(elapsedPeriodsFileName, afterLastSlash);
        }
        if (this.archiveRemover != null) {
            this.archiveRemover.clean(new java.util.Date(this.timeBasedFileNamingAndTriggeringPolicy.getCurrentTime()));
        }
    }

    public void setCleanHistoryOnStart(boolean z) {
        this.cleanHistoryOnStart = z;
    }

    public void setMaxHistory(int i) {
        this.maxHistory = i;
    }

    public void setTimeBasedFileNamingAndTriggeringPolicy(ch.qos.logback.core.rolling.TimeBasedFileNamingAndTriggeringPolicy<E> timeBasedFileNamingAndTriggeringPolicy2) {
        this.timeBasedFileNamingAndTriggeringPolicy = timeBasedFileNamingAndTriggeringPolicy2;
    }

    public void start() {
        this.renameUtil.setContext(this.context);
        if (this.fileNamePatternStr != null) {
            this.fileNamePattern = new ch.qos.logback.core.rolling.helper.FileNamePattern(this.fileNamePatternStr, this.context);
            determineCompressionMode();
            this.compressor = new ch.qos.logback.core.rolling.helper.Compressor(this.compressionMode);
            this.compressor.setContext(this.context);
            this.fileNamePatternWCS = new ch.qos.logback.core.rolling.helper.FileNamePattern(ch.qos.logback.core.rolling.helper.Compressor.computeFileNameStr_WCS(this.fileNamePatternStr, this.compressionMode), this.context);
            addInfo("Will use the pattern " + this.fileNamePatternWCS + " for the active file");
            if (this.compressionMode == ch.qos.logback.core.rolling.helper.CompressionMode.ZIP) {
                this.zipEntryFileNamePattern = new ch.qos.logback.core.rolling.helper.FileNamePattern(transformFileNamePattern2ZipEntry(this.fileNamePatternStr), this.context);
            }
            if (this.timeBasedFileNamingAndTriggeringPolicy == null) {
                this.timeBasedFileNamingAndTriggeringPolicy = new ch.qos.logback.core.rolling.DefaultTimeBasedFileNamingAndTriggeringPolicy();
            }
            this.timeBasedFileNamingAndTriggeringPolicy.setContext(this.context);
            this.timeBasedFileNamingAndTriggeringPolicy.setTimeBasedRollingPolicy(this);
            this.timeBasedFileNamingAndTriggeringPolicy.start();
            if (this.maxHistory != 0) {
                this.archiveRemover = this.timeBasedFileNamingAndTriggeringPolicy.getArchiveRemover();
                this.archiveRemover.setMaxHistory(this.maxHistory);
                if (this.cleanHistoryOnStart) {
                    addInfo("Cleaning on start up");
                    this.archiveRemover.clean(new java.util.Date(this.timeBasedFileNamingAndTriggeringPolicy.getCurrentTime()));
                }
            }
            super.start();
            return;
        }
        addWarn(FNP_NOT_SET);
        addWarn(ch.qos.logback.core.CoreConstants.SEE_FNP_NOT_SET);
        throw new java.lang.IllegalStateException("The FileNamePattern option must be set before using TimeBasedRollingPolicy. See also http://logback.qos.ch/codes.html#tbr_fnp_not_set");
    }

    public void stop() {
        if (isStarted()) {
            waitForAsynchronousJobToStop();
            super.stop();
        }
    }

    public java.lang.String toString() {
        return "c.q.l.core.rolling.TimeBasedRollingPolicy";
    }
}
