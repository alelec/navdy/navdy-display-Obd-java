package ch.qos.logback.core.rolling;

public class SizeBasedTriggeringPolicy<E> extends ch.qos.logback.core.rolling.TriggeringPolicyBase<E> {
    public static final long DEFAULT_MAX_FILE_SIZE = 10485760;
    public static final java.lang.String SEE_SIZE_FORMAT = "http://logback.qos.ch/codes.html#sbtp_size_format";
    private ch.qos.logback.core.util.InvocationGate invocationGate = new ch.qos.logback.core.util.InvocationGate();
    ch.qos.logback.core.util.FileSize maxFileSize;
    java.lang.String maxFileSizeAsString = java.lang.Long.toString(DEFAULT_MAX_FILE_SIZE);

    public SizeBasedTriggeringPolicy() {
    }

    public SizeBasedTriggeringPolicy(java.lang.String str) {
        setMaxFileSize(str);
    }

    public java.lang.String getMaxFileSize() {
        return this.maxFileSizeAsString;
    }

    public boolean isTriggeringEvent(java.io.File file, E e) {
        if (this.invocationGate.skipFurtherWork()) {
            return false;
        }
        this.invocationGate.updateMaskIfNecessary(java.lang.System.currentTimeMillis());
        return file.length() >= this.maxFileSize.getSize();
    }

    public void setMaxFileSize(java.lang.String str) {
        this.maxFileSizeAsString = str;
        this.maxFileSize = ch.qos.logback.core.util.FileSize.valueOf(str);
    }

    /* access modifiers changed from: 0000 */
    public long toFileSize(java.lang.String str) {
        long j = DEFAULT_MAX_FILE_SIZE;
        if (str == null) {
            return j;
        }
        java.lang.String upperCase = str.trim().toUpperCase();
        long j2 = 1;
        int indexOf = upperCase.indexOf("KB");
        if (indexOf != -1) {
            j2 = android.support.v4.media.session.PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID;
            upperCase = upperCase.substring(0, indexOf);
        } else {
            int indexOf2 = upperCase.indexOf("MB");
            if (indexOf2 != -1) {
                j2 = 1048576;
                upperCase = upperCase.substring(0, indexOf2);
            } else {
                int indexOf3 = upperCase.indexOf("GB");
                if (indexOf3 != -1) {
                    j2 = 1073741824;
                    upperCase = upperCase.substring(0, indexOf3);
                }
            }
        }
        if (upperCase == null) {
            return j;
        }
        try {
            return java.lang.Long.valueOf(upperCase).longValue() * j2;
        } catch (java.lang.NumberFormatException e) {
            addError("[" + upperCase + "] is not in proper int format. Please refer to " + SEE_SIZE_FORMAT);
            addError("[" + str + "] not in expected format.", e);
            return j;
        }
    }
}
