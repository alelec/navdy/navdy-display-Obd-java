package ch.qos.logback.core.rolling;

public abstract class TimeBasedFileNamingAndTriggeringPolicyBase<E> extends ch.qos.logback.core.spi.ContextAwareBase implements ch.qos.logback.core.rolling.TimeBasedFileNamingAndTriggeringPolicy<E> {
    protected ch.qos.logback.core.rolling.helper.ArchiveRemover archiveRemover = null;
    protected long artificialCurrentTime = -1;
    protected java.util.Date dateInCurrentPeriod = null;
    protected java.lang.String elapsedPeriodsFileName;
    protected long nextCheck;
    protected ch.qos.logback.core.rolling.helper.RollingCalendar rc;
    protected boolean started = false;
    protected ch.qos.logback.core.rolling.TimeBasedRollingPolicy<E> tbrp;

    /* access modifiers changed from: protected */
    public void computeNextCheck() {
        this.nextCheck = this.rc.getNextTriggeringMillis(this.dateInCurrentPeriod);
    }

    public ch.qos.logback.core.rolling.helper.ArchiveRemover getArchiveRemover() {
        return this.archiveRemover;
    }

    public java.lang.String getCurrentPeriodsFileNameWithoutCompressionSuffix() {
        return this.tbrp.fileNamePatternWCS.convert(this.dateInCurrentPeriod);
    }

    public long getCurrentTime() {
        return this.artificialCurrentTime >= 0 ? this.artificialCurrentTime : java.lang.System.currentTimeMillis();
    }

    public java.lang.String getElapsedPeriodsFileName() {
        return this.elapsedPeriodsFileName;
    }

    public boolean isStarted() {
        return this.started;
    }

    public void setCurrentTime(long j) {
        this.artificialCurrentTime = j;
    }

    /* access modifiers changed from: protected */
    public void setDateInCurrentPeriod(long j) {
        this.dateInCurrentPeriod.setTime(j);
    }

    public void setDateInCurrentPeriod(java.util.Date date) {
        this.dateInCurrentPeriod = date;
    }

    public void setTimeBasedRollingPolicy(ch.qos.logback.core.rolling.TimeBasedRollingPolicy<E> timeBasedRollingPolicy) {
        this.tbrp = timeBasedRollingPolicy;
    }

    public void start() {
        ch.qos.logback.core.rolling.helper.DateTokenConverter primaryDateTokenConverter = this.tbrp.fileNamePattern.getPrimaryDateTokenConverter();
        if (primaryDateTokenConverter == null) {
            throw new java.lang.IllegalStateException("FileNamePattern [" + this.tbrp.fileNamePattern.getPattern() + "] does not contain a valid DateToken");
        }
        this.rc = new ch.qos.logback.core.rolling.helper.RollingCalendar();
        this.rc.init(primaryDateTokenConverter.getDatePattern());
        addInfo("The date pattern is '" + primaryDateTokenConverter.getDatePattern() + "' from file name pattern '" + this.tbrp.fileNamePattern.getPattern() + "'.");
        this.rc.printPeriodicity(this);
        setDateInCurrentPeriod(new java.util.Date(getCurrentTime()));
        if (this.tbrp.getParentsRawFileProperty() != null) {
            java.io.File file = new java.io.File(this.tbrp.getParentsRawFileProperty());
            if (file.exists() && file.canRead()) {
                setDateInCurrentPeriod(new java.util.Date(file.lastModified()));
            }
        }
        addInfo("Setting initial period to " + this.dateInCurrentPeriod);
        computeNextCheck();
    }

    public void stop() {
        this.started = false;
    }
}
