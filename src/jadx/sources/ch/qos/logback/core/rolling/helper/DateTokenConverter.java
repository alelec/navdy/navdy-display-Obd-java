package ch.qos.logback.core.rolling.helper;

public class DateTokenConverter<E> extends ch.qos.logback.core.pattern.DynamicConverter<E> implements ch.qos.logback.core.rolling.helper.MonoTypedConverter {
    public static final java.lang.String AUXILIARY_TOKEN = "AUX";
    public static final java.lang.String CONVERTER_KEY = "d";
    public static final java.lang.String DEFAULT_DATE_PATTERN = "yyyy-MM-dd";
    private ch.qos.logback.core.util.CachingDateFormatter cdf;
    private java.lang.String datePattern;
    private boolean primary = true;

    public java.lang.String convert(java.lang.Object obj) {
        if (obj == null) {
            throw new java.lang.IllegalArgumentException("Null argument forbidden");
        } else if (obj instanceof java.util.Date) {
            return convert((java.util.Date) obj);
        } else {
            throw new java.lang.IllegalArgumentException("Cannot convert " + obj + " of type" + obj.getClass().getName());
        }
    }

    public java.lang.String convert(java.util.Date date) {
        return this.cdf.format(date.getTime());
    }

    public java.lang.String getDatePattern() {
        return this.datePattern;
    }

    public boolean isApplicable(java.lang.Object obj) {
        return obj instanceof java.util.Date;
    }

    public boolean isPrimary() {
        return this.primary;
    }

    public void start() {
        this.datePattern = getFirstOption();
        if (this.datePattern == null) {
            this.datePattern = "yyyy-MM-dd";
        }
        java.util.List optionList = getOptionList();
        if (optionList != null && optionList.size() > 1) {
            if (AUXILIARY_TOKEN.equalsIgnoreCase((java.lang.String) optionList.get(1))) {
                this.primary = false;
            }
        }
        this.cdf = new ch.qos.logback.core.util.CachingDateFormatter(this.datePattern);
    }

    public java.lang.String toRegex() {
        return new ch.qos.logback.core.util.DatePatternToRegexUtil(this.datePattern).toRegex();
    }
}
