package ch.qos.logback.core.rolling.helper;

public class IntegerTokenConverter extends ch.qos.logback.core.pattern.DynamicConverter implements ch.qos.logback.core.rolling.helper.MonoTypedConverter {
    public static final java.lang.String CONVERTER_KEY = "i";

    public java.lang.String convert(int i) {
        return java.lang.Integer.toString(i);
    }

    public java.lang.String convert(java.lang.Object obj) {
        if (obj == null) {
            throw new java.lang.IllegalArgumentException("Null argument forbidden");
        } else if (obj instanceof java.lang.Integer) {
            return convert(((java.lang.Integer) obj).intValue());
        } else {
            throw new java.lang.IllegalArgumentException("Cannot convert " + obj + " of type" + obj.getClass().getName());
        }
    }

    public boolean isApplicable(java.lang.Object obj) {
        return obj instanceof java.lang.Integer;
    }
}
