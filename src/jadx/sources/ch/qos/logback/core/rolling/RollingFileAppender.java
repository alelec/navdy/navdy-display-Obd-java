package ch.qos.logback.core.rolling;

public class RollingFileAppender<E> extends ch.qos.logback.core.FileAppender<E> {
    private static java.lang.String COLLISION_URL = "http://logback.qos.ch/codes.html#rfa_collision";
    private static java.lang.String RFA_NO_RP_URL = "http://logback.qos.ch/codes.html#rfa_no_rp";
    private static java.lang.String RFA_NO_TP_URL = "http://logback.qos.ch/codes.html#rfa_no_tp";
    java.io.File currentlyActiveFile;
    ch.qos.logback.core.rolling.RollingPolicy rollingPolicy;
    ch.qos.logback.core.rolling.TriggeringPolicy<E> triggeringPolicy;

    private boolean fileAndPatternCollide() {
        if (this.triggeringPolicy instanceof ch.qos.logback.core.rolling.RollingPolicyBase) {
            ch.qos.logback.core.rolling.helper.FileNamePattern fileNamePattern = ((ch.qos.logback.core.rolling.RollingPolicyBase) this.triggeringPolicy).fileNamePattern;
            if (!(fileNamePattern == null || this.fileName == null)) {
                return this.fileName.matches(fileNamePattern.toRegex());
            }
        }
        return false;
    }

    public java.lang.String getFile() {
        return this.rollingPolicy.getActiveFileName();
    }

    public ch.qos.logback.core.rolling.RollingPolicy getRollingPolicy() {
        return this.rollingPolicy;
    }

    public ch.qos.logback.core.rolling.TriggeringPolicy<E> getTriggeringPolicy() {
        return this.triggeringPolicy;
    }

    public void rollover() {
        synchronized (this.lock) {
            closeOutputStream();
            try {
                this.rollingPolicy.rollover();
            } catch (ch.qos.logback.core.rolling.RolloverFailure e) {
                addWarn("RolloverFailure occurred. Deferring rollover");
                this.append = true;
            }
            java.lang.String activeFileName = this.rollingPolicy.getActiveFileName();
            try {
                this.currentlyActiveFile = new java.io.File(activeFileName);
                openFile(activeFileName);
            } catch (java.io.IOException e2) {
                addError("openFile(" + activeFileName + ") failed", e2);
            }
        }
        return;
    }

    public void setFile(java.lang.String str) {
        if (!(str == null || (this.triggeringPolicy == null && this.rollingPolicy == null))) {
            addError("File property must be set before any triggeringPolicy or rollingPolicy properties");
            addError("Visit http://logback.qos.ch/codes.html#rfa_file_after for more information");
        }
        super.setFile(str);
    }

    public void setRollingPolicy(ch.qos.logback.core.rolling.RollingPolicy rollingPolicy2) {
        this.rollingPolicy = rollingPolicy2;
        if (this.rollingPolicy instanceof ch.qos.logback.core.rolling.TriggeringPolicy) {
            this.triggeringPolicy = (ch.qos.logback.core.rolling.TriggeringPolicy) rollingPolicy2;
        }
    }

    public void setTriggeringPolicy(ch.qos.logback.core.rolling.TriggeringPolicy<E> triggeringPolicy2) {
        this.triggeringPolicy = triggeringPolicy2;
        if (triggeringPolicy2 instanceof ch.qos.logback.core.rolling.RollingPolicy) {
            this.rollingPolicy = (ch.qos.logback.core.rolling.RollingPolicy) triggeringPolicy2;
        }
    }

    public void start() {
        if (this.triggeringPolicy == null) {
            addWarn("No TriggeringPolicy was set for the RollingFileAppender named " + getName());
            addWarn("For more information, please visit " + RFA_NO_TP_URL);
            return;
        }
        if (!this.append) {
            addWarn("Append mode is mandatory for RollingFileAppender");
            this.append = true;
        }
        if (this.rollingPolicy == null) {
            addError("No RollingPolicy was set for the RollingFileAppender named " + getName());
            addError("For more information, please visit " + RFA_NO_RP_URL);
        } else if (fileAndPatternCollide()) {
            addError("File property collides with fileNamePattern. Aborting.");
            addError("For more information, please visit " + COLLISION_URL);
        } else {
            if (isPrudent()) {
                if (rawFileProperty() != null) {
                    addWarn("Setting \"File\" property to null on account of prudent mode");
                    setFile(null);
                }
                if (this.rollingPolicy.getCompressionMode() != ch.qos.logback.core.rolling.helper.CompressionMode.NONE) {
                    addError("Compression is not supported in prudent mode. Aborting");
                    return;
                }
            }
            this.currentlyActiveFile = new java.io.File(getFile());
            addInfo("Active log file name: " + getFile());
            super.start();
        }
    }

    public void stop() {
        if (this.rollingPolicy != null) {
            this.rollingPolicy.stop();
        }
        if (this.triggeringPolicy != null) {
            this.triggeringPolicy.stop();
        }
        super.stop();
    }

    /* access modifiers changed from: protected */
    public void subAppend(E e) {
        synchronized (this.triggeringPolicy) {
            if (this.triggeringPolicy.isTriggeringEvent(this.currentlyActiveFile, e)) {
                rollover();
            }
        }
        super.subAppend(e);
    }
}
