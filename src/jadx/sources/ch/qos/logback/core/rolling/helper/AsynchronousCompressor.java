package ch.qos.logback.core.rolling.helper;

public class AsynchronousCompressor {
    ch.qos.logback.core.rolling.helper.Compressor compressor;

    public AsynchronousCompressor(ch.qos.logback.core.rolling.helper.Compressor compressor2) {
        this.compressor = compressor2;
    }

    public java.util.concurrent.Future<?> compressAsynchronously(java.lang.String str, java.lang.String str2, java.lang.String str3) {
        java.util.concurrent.ScheduledExecutorService newScheduledThreadPool = java.util.concurrent.Executors.newScheduledThreadPool(1);
        java.util.concurrent.Future<?> submit = newScheduledThreadPool.submit(new ch.qos.logback.core.rolling.helper.CompressionRunnable(this.compressor, str, str2, str3));
        newScheduledThreadPool.shutdown();
        return submit;
    }
}
