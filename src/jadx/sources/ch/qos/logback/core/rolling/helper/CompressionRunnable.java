package ch.qos.logback.core.rolling.helper;

public class CompressionRunnable implements java.lang.Runnable {
    final ch.qos.logback.core.rolling.helper.Compressor compressor;
    final java.lang.String innerEntryName;
    final java.lang.String nameOfCompressedFile;
    final java.lang.String nameOfFile2Compress;

    public CompressionRunnable(ch.qos.logback.core.rolling.helper.Compressor compressor2, java.lang.String str, java.lang.String str2, java.lang.String str3) {
        this.compressor = compressor2;
        this.nameOfFile2Compress = str;
        this.nameOfCompressedFile = str2;
        this.innerEntryName = str3;
    }

    public void run() {
        this.compressor.compress(this.nameOfFile2Compress, this.nameOfCompressedFile, this.innerEntryName);
    }
}
