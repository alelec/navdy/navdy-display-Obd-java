package ch.qos.logback.core.rolling.helper;

public class RollingCalendar extends java.util.GregorianCalendar {
    static final java.util.TimeZone GMT_TIMEZONE = java.util.TimeZone.getTimeZone("GMT");
    private static final long serialVersionUID = -5937537740925066161L;
    ch.qos.logback.core.rolling.helper.PeriodicityType periodicityType = ch.qos.logback.core.rolling.helper.PeriodicityType.ERRONEOUS;

    public RollingCalendar() {
    }

    public RollingCalendar(java.util.TimeZone timeZone, java.util.Locale locale) {
        super(timeZone, locale);
    }

    public static int diffInMonths(long j, long j2) {
        if (j > j2) {
            throw new java.lang.IllegalArgumentException("startTime cannot be larger than endTime");
        }
        java.util.Calendar instance = java.util.Calendar.getInstance();
        instance.setTimeInMillis(j);
        java.util.Calendar instance2 = java.util.Calendar.getInstance();
        instance2.setTimeInMillis(j2);
        return (instance2.get(2) - instance.get(2)) + ((instance2.get(1) - instance.get(1)) * 12);
    }

    private void setPeriodicityType(ch.qos.logback.core.rolling.helper.PeriodicityType periodicityType2) {
        this.periodicityType = periodicityType2;
    }

    public ch.qos.logback.core.rolling.helper.PeriodicityType computePeriodicityType(java.lang.String str) {
        ch.qos.logback.core.rolling.helper.PeriodicityType[] periodicityTypeArr;
        ch.qos.logback.core.rolling.helper.RollingCalendar rollingCalendar = new ch.qos.logback.core.rolling.helper.RollingCalendar(GMT_TIMEZONE, java.util.Locale.getDefault());
        java.util.Date date = new java.util.Date(0);
        if (str != null) {
            for (ch.qos.logback.core.rolling.helper.PeriodicityType periodicityType2 : ch.qos.logback.core.rolling.helper.PeriodicityType.VALID_ORDERED_LIST) {
                java.text.SimpleDateFormat simpleDateFormat = new java.text.SimpleDateFormat(str);
                simpleDateFormat.setTimeZone(GMT_TIMEZONE);
                java.lang.String format = simpleDateFormat.format(date);
                rollingCalendar.setPeriodicityType(periodicityType2);
                java.lang.String format2 = simpleDateFormat.format(new java.util.Date(rollingCalendar.getNextTriggeringMillis(date)));
                if (format != null && format2 != null && !format.equals(format2)) {
                    return periodicityType2;
                }
            }
        }
        return ch.qos.logback.core.rolling.helper.PeriodicityType.ERRONEOUS;
    }

    public java.util.Date getNextTriggeringDate(java.util.Date date) {
        return getRelativeDate(date, 1);
    }

    public long getNextTriggeringMillis(java.util.Date date) {
        return getNextTriggeringDate(date).getTime();
    }

    public ch.qos.logback.core.rolling.helper.PeriodicityType getPeriodicityType() {
        return this.periodicityType;
    }

    public java.util.Date getRelativeDate(java.util.Date date, int i) {
        setTime(date);
        switch (this.periodicityType) {
            case TOP_OF_MILLISECOND:
                add(14, i);
                break;
            case TOP_OF_SECOND:
                set(14, 0);
                add(13, i);
                break;
            case TOP_OF_MINUTE:
                set(13, 0);
                set(14, 0);
                add(12, i);
                break;
            case TOP_OF_HOUR:
                set(12, 0);
                set(13, 0);
                set(14, 0);
                add(11, i);
                break;
            case TOP_OF_DAY:
                set(11, 0);
                set(12, 0);
                set(13, 0);
                set(14, 0);
                add(5, i);
                break;
            case TOP_OF_WEEK:
                set(7, getFirstDayOfWeek());
                set(11, 0);
                set(12, 0);
                set(13, 0);
                set(14, 0);
                add(3, i);
                break;
            case TOP_OF_MONTH:
                set(5, 1);
                set(11, 0);
                set(12, 0);
                set(13, 0);
                set(14, 0);
                add(2, i);
                break;
            default:
                throw new java.lang.IllegalStateException("Unknown periodicity type.");
        }
        return getTime();
    }

    public void init(java.lang.String str) {
        this.periodicityType = computePeriodicityType(str);
    }

    public long periodsElapsed(long j, long j2) {
        if (j > j2) {
            throw new java.lang.IllegalArgumentException("Start cannot come before end");
        }
        long j3 = j2 - j;
        switch (this.periodicityType) {
            case TOP_OF_MILLISECOND:
                return j3;
            case TOP_OF_SECOND:
                return j3 / 1000;
            case TOP_OF_MINUTE:
                return j3 / ch.qos.logback.classic.turbo.ReconfigureOnChangeFilter.DEFAULT_REFRESH_PERIOD;
            case TOP_OF_HOUR:
                return (long) (((int) j3) / ch.qos.logback.core.CoreConstants.MILLIS_IN_ONE_HOUR);
            case TOP_OF_DAY:
                return j3 / 86400000;
            case TOP_OF_WEEK:
                return j3 / 604800000;
            case TOP_OF_MONTH:
                return (long) diffInMonths(j, j2);
            default:
                throw new java.lang.IllegalStateException("Unknown periodicity type.");
        }
    }

    public void printPeriodicity(ch.qos.logback.core.spi.ContextAwareBase contextAwareBase) {
        switch (this.periodicityType) {
            case TOP_OF_MILLISECOND:
                contextAwareBase.addInfo("Roll-over every millisecond.");
                return;
            case TOP_OF_SECOND:
                contextAwareBase.addInfo("Roll-over every second.");
                return;
            case TOP_OF_MINUTE:
                contextAwareBase.addInfo("Roll-over every minute.");
                return;
            case TOP_OF_HOUR:
                contextAwareBase.addInfo("Roll-over at the top of every hour.");
                return;
            case HALF_DAY:
                contextAwareBase.addInfo("Roll-over at midday and midnight.");
                return;
            case TOP_OF_DAY:
                contextAwareBase.addInfo("Roll-over at midnight.");
                return;
            case TOP_OF_WEEK:
                contextAwareBase.addInfo("Rollover at the start of week.");
                return;
            case TOP_OF_MONTH:
                contextAwareBase.addInfo("Rollover at start of every month.");
                return;
            default:
                contextAwareBase.addInfo("Unknown periodicity.");
                return;
        }
    }
}
