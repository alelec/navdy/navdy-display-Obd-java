package ch.qos.logback.core.rolling;

public class ObdLogsRollingFileAppender<E> extends ch.qos.logback.core.rolling.RollingFileAppender<E> {
    java.lang.String loggerName;

    public void rollover() {
        super.rollover();
        java.lang.String fileName = ((ch.qos.logback.core.rolling.RollingPolicyBase) this.rollingPolicy).fileNamePattern.convertInt(1);
        com.navdy.obd.ObdService service = com.navdy.obd.ObdService.getObdService();
        if (service != null) {
            service.onLogFileRollOver(this.loggerName, fileName);
        }
    }

    /* access modifiers changed from: protected */
    public void append(E eventObject) {
        if (this.loggerName == null && (eventObject instanceof ch.qos.logback.classic.spi.ILoggingEvent)) {
            this.loggerName = ((ch.qos.logback.classic.spi.ILoggingEvent) eventObject).getLoggerName();
        }
        super.append(eventObject);
    }

    /* access modifiers changed from: protected */
    public void subAppend(E event) {
        com.navdy.obd.ObdService service = com.navdy.obd.ObdService.getObdService();
        if (service != null && service.monitoringCanBusLimitReached && com.navdy.obd.ObdService.CAN_BUS_LOGGER.equals(this.loggerName)) {
            synchronized (this.triggeringPolicy) {
                rollover();
            }
        }
        super.subAppend(event);
    }
}
