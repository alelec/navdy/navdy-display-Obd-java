package ch.qos.logback.core.rolling.helper;

public class FileNamePattern extends ch.qos.logback.core.spi.ContextAwareBase {
    static final java.util.Map<java.lang.String, java.lang.String> CONVERTER_MAP = new java.util.HashMap();
    ch.qos.logback.core.pattern.Converter<java.lang.Object> headTokenConverter;
    java.lang.String pattern;

    static {
        CONVERTER_MAP.put(ch.qos.logback.core.rolling.helper.IntegerTokenConverter.CONVERTER_KEY, ch.qos.logback.core.rolling.helper.IntegerTokenConverter.class.getName());
        CONVERTER_MAP.put(ch.qos.logback.core.rolling.helper.DateTokenConverter.CONVERTER_KEY, ch.qos.logback.core.rolling.helper.DateTokenConverter.class.getName());
    }

    public FileNamePattern(java.lang.String str, ch.qos.logback.core.Context context) {
        setPattern(ch.qos.logback.core.rolling.helper.FileFilterUtil.slashify(str));
        setContext(context);
        parse();
        ch.qos.logback.core.pattern.ConverterUtil.startConverters(this.headTokenConverter);
    }

    public java.lang.String convert(java.lang.Object obj) {
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        for (ch.qos.logback.core.pattern.Converter<java.lang.Object> converter = this.headTokenConverter; converter != null; converter = converter.getNext()) {
            sb.append(converter.convert(obj));
        }
        return sb.toString();
    }

    public java.lang.String convertInt(int i) {
        return convert(java.lang.Integer.valueOf(i));
    }

    public java.lang.String convertMultipleArguments(java.lang.Object... objArr) {
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        for (ch.qos.logback.core.pattern.Converter<java.lang.Object> converter = this.headTokenConverter; converter != null; converter = converter.getNext()) {
            if (converter instanceof ch.qos.logback.core.rolling.helper.MonoTypedConverter) {
                ch.qos.logback.core.rolling.helper.MonoTypedConverter monoTypedConverter = (ch.qos.logback.core.rolling.helper.MonoTypedConverter) converter;
                for (java.lang.Object obj : objArr) {
                    if (monoTypedConverter.isApplicable(obj)) {
                        sb.append(converter.convert(obj));
                    }
                }
            } else {
                sb.append(converter.convert(objArr));
            }
        }
        return sb.toString();
    }

    /* access modifiers changed from: 0000 */
    public java.lang.String escapeRightParantesis(java.lang.String str) {
        return this.pattern.replace(")", "\\)");
    }

    public ch.qos.logback.core.rolling.helper.IntegerTokenConverter getIntegerTokenConverter() {
        for (ch.qos.logback.core.pattern.Converter<java.lang.Object> converter = this.headTokenConverter; converter != null; converter = converter.getNext()) {
            if (converter instanceof ch.qos.logback.core.rolling.helper.IntegerTokenConverter) {
                return (ch.qos.logback.core.rolling.helper.IntegerTokenConverter) converter;
            }
        }
        return null;
    }

    public java.lang.String getPattern() {
        return this.pattern;
    }

    public ch.qos.logback.core.rolling.helper.DateTokenConverter getPrimaryDateTokenConverter() {
        for (ch.qos.logback.core.pattern.Converter<java.lang.Object> converter = this.headTokenConverter; converter != null; converter = converter.getNext()) {
            if (converter instanceof ch.qos.logback.core.rolling.helper.DateTokenConverter) {
                ch.qos.logback.core.rolling.helper.DateTokenConverter dateTokenConverter = (ch.qos.logback.core.rolling.helper.DateTokenConverter) converter;
                if (dateTokenConverter.isPrimary()) {
                    return dateTokenConverter;
                }
            }
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    public void parse() {
        try {
            ch.qos.logback.core.pattern.parser.Parser parser = new ch.qos.logback.core.pattern.parser.Parser(escapeRightParantesis(this.pattern), new ch.qos.logback.core.pattern.util.AlmostAsIsEscapeUtil());
            parser.setContext(this.context);
            this.headTokenConverter = parser.compile(parser.parse(), CONVERTER_MAP);
        } catch (ch.qos.logback.core.spi.ScanException e) {
            addError("Failed to parse pattern \"" + this.pattern + "\".", e);
        }
    }

    public void setPattern(java.lang.String str) {
        if (str != null) {
            this.pattern = str.trim();
        }
    }

    public java.lang.String toRegex() {
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        for (ch.qos.logback.core.pattern.Converter<java.lang.Object> converter = this.headTokenConverter; converter != null; converter = converter.getNext()) {
            if (converter instanceof ch.qos.logback.core.pattern.LiteralConverter) {
                sb.append(converter.convert(null));
            } else if (converter instanceof ch.qos.logback.core.rolling.helper.IntegerTokenConverter) {
                sb.append("\\d{1,2}");
            } else if (converter instanceof ch.qos.logback.core.rolling.helper.DateTokenConverter) {
                sb.append(((ch.qos.logback.core.rolling.helper.DateTokenConverter) converter).toRegex());
            }
        }
        return sb.toString();
    }

    public java.lang.String toRegexForFixedDate(java.util.Date date) {
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        for (ch.qos.logback.core.pattern.Converter<java.lang.Object> converter = this.headTokenConverter; converter != null; converter = converter.getNext()) {
            if (converter instanceof ch.qos.logback.core.pattern.LiteralConverter) {
                sb.append(converter.convert(null));
            } else if (converter instanceof ch.qos.logback.core.rolling.helper.IntegerTokenConverter) {
                sb.append("(\\d{1,3})");
            } else if (converter instanceof ch.qos.logback.core.rolling.helper.DateTokenConverter) {
                sb.append(converter.convert(date));
            }
        }
        return sb.toString();
    }

    public java.lang.String toString() {
        return this.pattern;
    }
}
