package ch.qos.logback.core.rolling;

public class DefaultTimeBasedFileNamingAndTriggeringPolicy<E> extends ch.qos.logback.core.rolling.TimeBasedFileNamingAndTriggeringPolicyBase<E> {
    public boolean isTriggeringEvent(java.io.File file, E e) {
        long currentTime = getCurrentTime();
        if (currentTime < this.nextCheck) {
            return false;
        }
        java.util.Date date = this.dateInCurrentPeriod;
        addInfo("Elapsed period: " + date);
        this.elapsedPeriodsFileName = this.tbrp.fileNamePatternWCS.convert(date);
        setDateInCurrentPeriod(currentTime);
        computeNextCheck();
        return true;
    }

    public void start() {
        super.start();
        this.archiveRemover = new ch.qos.logback.core.rolling.helper.TimeBasedArchiveRemover(this.tbrp.fileNamePattern, this.rc);
        this.archiveRemover.setContext(this.context);
        this.started = true;
    }

    public java.lang.String toString() {
        return "c.q.l.core.rolling.DefaultTimeBasedFileNamingAndTriggeringPolicy";
    }
}
