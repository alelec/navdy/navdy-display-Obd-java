package ch.qos.logback.core.rolling.helper;

public interface ArchiveRemover extends ch.qos.logback.core.spi.ContextAware {
    void clean(java.util.Date date);

    void setMaxHistory(int i);
}
