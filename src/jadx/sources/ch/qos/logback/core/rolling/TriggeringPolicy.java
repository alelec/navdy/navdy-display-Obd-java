package ch.qos.logback.core.rolling;

public interface TriggeringPolicy<E> extends ch.qos.logback.core.spi.LifeCycle {
    boolean isTriggeringEvent(java.io.File file, E e);
}
