package ch.qos.logback.core.rolling.helper;

public abstract class DefaultArchiveRemover extends ch.qos.logback.core.spi.ContextAwareBase implements ch.qos.logback.core.rolling.helper.ArchiveRemover {
    protected static final long INACTIVITY_TOLERANCE_IN_MILLIS = 5529600000L;
    static final int MAX_VALUE_FOR_INACTIVITY_PERIODS = 336;
    protected static final long UNINITIALIZED = -1;
    final ch.qos.logback.core.rolling.helper.FileNamePattern fileNamePattern;
    long lastHeartBeat = -1;
    final boolean parentClean;
    int periodOffsetForDeletionTarget;
    final ch.qos.logback.core.rolling.helper.RollingCalendar rc;

    public DefaultArchiveRemover(ch.qos.logback.core.rolling.helper.FileNamePattern fileNamePattern2, ch.qos.logback.core.rolling.helper.RollingCalendar rollingCalendar) {
        this.fileNamePattern = fileNamePattern2;
        this.rc = rollingCalendar;
        this.parentClean = computeParentCleaningFlag(fileNamePattern2);
    }

    private void removeFolderIfEmpty(java.io.File file, int i) {
        if (i < 3 && file.isDirectory() && ch.qos.logback.core.rolling.helper.FileFilterUtil.isEmptyDirectory(file)) {
            addInfo("deleting folder [" + file + "]");
            file.delete();
            removeFolderIfEmpty(file.getParentFile(), i + 1);
        }
    }

    public void clean(java.util.Date date) {
        long time = date.getTime();
        int computeElapsedPeriodsSinceLastClean = computeElapsedPeriodsSinceLastClean(time);
        this.lastHeartBeat = time;
        if (computeElapsedPeriodsSinceLastClean > 1) {
            addInfo("periodsElapsed = " + computeElapsedPeriodsSinceLastClean);
        }
        for (int i = 0; i < computeElapsedPeriodsSinceLastClean; i++) {
            cleanByPeriodOffset(date, this.periodOffsetForDeletionTarget - i);
        }
    }

    /* access modifiers changed from: 0000 */
    public abstract void cleanByPeriodOffset(java.util.Date date, int i);

    /* access modifiers changed from: 0000 */
    public int computeElapsedPeriodsSinceLastClean(long j) {
        long j2 = 336;
        if (this.lastHeartBeat == -1) {
            addInfo("first clean up after appender initialization");
            long periodsElapsed = this.rc.periodsElapsed(j, INACTIVITY_TOLERANCE_IN_MILLIS + j);
            if (periodsElapsed <= 336) {
                j2 = periodsElapsed;
            }
        } else {
            j2 = this.rc.periodsElapsed(this.lastHeartBeat, j);
            if (j2 < 1) {
                addWarn("Unexpected periodsElapsed value " + j2);
                j2 = 1;
            }
        }
        return (int) j2;
    }

    /* access modifiers changed from: 0000 */
    public boolean computeParentCleaningFlag(ch.qos.logback.core.rolling.helper.FileNamePattern fileNamePattern2) {
        if (fileNamePattern2.getPrimaryDateTokenConverter().getDatePattern().indexOf(47) != -1) {
            return true;
        }
        ch.qos.logback.core.pattern.Converter<java.lang.Object> converter = fileNamePattern2.headTokenConverter;
        while (converter != null && !(converter instanceof ch.qos.logback.core.rolling.helper.DateTokenConverter)) {
            converter = converter.getNext();
        }
        while (converter != null) {
            if ((converter instanceof ch.qos.logback.core.pattern.LiteralConverter) && converter.convert(null).indexOf(47) != -1) {
                return true;
            }
            converter = converter.getNext();
        }
        return false;
    }

    /* access modifiers changed from: 0000 */
    public void removeFolderIfEmpty(java.io.File file) {
        removeFolderIfEmpty(file, 0);
    }

    public void setMaxHistory(int i) {
        this.periodOffsetForDeletionTarget = (-i) - 1;
    }
}
