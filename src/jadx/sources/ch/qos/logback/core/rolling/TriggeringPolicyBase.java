package ch.qos.logback.core.rolling;

public abstract class TriggeringPolicyBase<E> extends ch.qos.logback.core.spi.ContextAwareBase implements ch.qos.logback.core.rolling.TriggeringPolicy<E> {
    private boolean start;

    public boolean isStarted() {
        return this.start;
    }

    public void start() {
        this.start = true;
    }

    public void stop() {
        this.start = false;
    }
}
