package ch.qos.logback.core.rolling.helper;

public class TokenConverter {
    static final int DATE = 1;
    static final int IDENTITY = 0;
    static final int INTEGER = 1;
    ch.qos.logback.core.rolling.helper.TokenConverter next;
    int type;

    protected TokenConverter(int i) {
        this.type = i;
    }

    public ch.qos.logback.core.rolling.helper.TokenConverter getNext() {
        return this.next;
    }

    public int getType() {
        return this.type;
    }

    public void setNext(ch.qos.logback.core.rolling.helper.TokenConverter tokenConverter) {
        this.next = tokenConverter;
    }

    public void setType(int i) {
        this.type = i;
    }
}
