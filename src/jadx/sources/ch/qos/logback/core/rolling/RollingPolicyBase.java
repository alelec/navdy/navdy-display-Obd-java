package ch.qos.logback.core.rolling;

public abstract class RollingPolicyBase extends ch.qos.logback.core.spi.ContextAwareBase implements ch.qos.logback.core.rolling.RollingPolicy {
    protected ch.qos.logback.core.rolling.helper.CompressionMode compressionMode = ch.qos.logback.core.rolling.helper.CompressionMode.NONE;
    ch.qos.logback.core.rolling.helper.FileNamePattern fileNamePattern;
    protected java.lang.String fileNamePatternStr;
    private ch.qos.logback.core.FileAppender parent;
    private boolean started;
    ch.qos.logback.core.rolling.helper.FileNamePattern zipEntryFileNamePattern;

    /* access modifiers changed from: protected */
    public void determineCompressionMode() {
        if (this.fileNamePatternStr.endsWith(".gz")) {
            addInfo("Will use gz compression");
            this.compressionMode = ch.qos.logback.core.rolling.helper.CompressionMode.GZ;
        } else if (this.fileNamePatternStr.endsWith(".zip")) {
            addInfo("Will use zip compression");
            this.compressionMode = ch.qos.logback.core.rolling.helper.CompressionMode.ZIP;
        } else {
            addInfo("No compression will be used");
            this.compressionMode = ch.qos.logback.core.rolling.helper.CompressionMode.NONE;
        }
    }

    public ch.qos.logback.core.rolling.helper.CompressionMode getCompressionMode() {
        return this.compressionMode;
    }

    public java.lang.String getFileNamePattern() {
        return this.fileNamePatternStr;
    }

    public java.lang.String getParentsRawFileProperty() {
        return this.parent.rawFileProperty();
    }

    public boolean isParentPrudent() {
        return this.parent.isPrudent();
    }

    public boolean isStarted() {
        return this.started;
    }

    public void setFileNamePattern(java.lang.String str) {
        this.fileNamePatternStr = str;
    }

    public void setParent(ch.qos.logback.core.FileAppender fileAppender) {
        this.parent = fileAppender;
    }

    public void start() {
        this.started = true;
    }

    public void stop() {
        this.started = false;
    }
}
