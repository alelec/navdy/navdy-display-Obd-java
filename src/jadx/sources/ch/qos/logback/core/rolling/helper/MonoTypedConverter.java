package ch.qos.logback.core.rolling.helper;

public interface MonoTypedConverter {
    boolean isApplicable(java.lang.Object obj);
}
