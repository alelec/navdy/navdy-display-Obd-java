package ch.qos.logback.core.rolling.helper;

public class FileStoreUtil {
    static final java.lang.String FILES_CLASS_STR = "java.nio.file.Files";
    static final java.lang.String PATH_CLASS_STR = "java.nio.file.Path";

    public static boolean areOnSameFileStore(java.io.File file, java.io.File file2) throws ch.qos.logback.core.rolling.RolloverFailure {
        if (!file.exists()) {
            throw new java.lang.IllegalArgumentException("File [" + file + "] does not exist.");
        } else if (!file2.exists()) {
            throw new java.lang.IllegalArgumentException("File [" + file2 + "] does not exist.");
        } else {
            try {
                java.lang.Class cls = java.lang.Class.forName(PATH_CLASS_STR);
                java.lang.Class cls2 = java.lang.Class.forName(FILES_CLASS_STR);
                java.lang.reflect.Method method = java.io.File.class.getMethod("toPath", new java.lang.Class[0]);
                java.lang.reflect.Method method2 = cls2.getMethod("getFileStore", new java.lang.Class[]{cls});
                java.lang.Object invoke = method.invoke(file, new java.lang.Object[0]);
                java.lang.Object invoke2 = method.invoke(file2, new java.lang.Object[0]);
                return method2.invoke(null, new java.lang.Object[]{invoke}).equals(method2.invoke(null, new java.lang.Object[]{invoke2}));
            } catch (java.lang.Exception e) {
                throw new ch.qos.logback.core.rolling.RolloverFailure("Failed to check file store equality for [" + file + "] and [" + file2 + "]", e);
            }
        }
    }
}
