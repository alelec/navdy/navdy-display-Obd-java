package ch.qos.logback.core.rolling;

public interface RollingPolicy extends ch.qos.logback.core.spi.LifeCycle {
    java.lang.String getActiveFileName();

    ch.qos.logback.core.rolling.helper.CompressionMode getCompressionMode();

    void rollover() throws ch.qos.logback.core.rolling.RolloverFailure;

    void setParent(ch.qos.logback.core.FileAppender fileAppender);
}
