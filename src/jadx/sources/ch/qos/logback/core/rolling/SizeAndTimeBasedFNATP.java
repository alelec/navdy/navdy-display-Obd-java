package ch.qos.logback.core.rolling;

@ch.qos.logback.core.joran.spi.NoAutoStart
public class SizeAndTimeBasedFNATP<E> extends ch.qos.logback.core.rolling.TimeBasedFileNamingAndTriggeringPolicyBase<E> {
    int currentPeriodsCounter = 0;
    private int invocationCounter;
    private int invocationMask = 1;
    ch.qos.logback.core.util.FileSize maxFileSize;
    java.lang.String maxFileSizeAsString;

    private java.lang.String getFileNameIncludingCompressionSuffix(java.util.Date date, int i) {
        return this.tbrp.fileNamePattern.convertMultipleArguments(this.dateInCurrentPeriod, java.lang.Integer.valueOf(i));
    }

    /* access modifiers changed from: 0000 */
    public void computeCurrentPeriodsHighestCounterValue(java.lang.String str) {
        java.io.File[] filesInFolderMatchingStemRegex = ch.qos.logback.core.rolling.helper.FileFilterUtil.filesInFolderMatchingStemRegex(new java.io.File(getCurrentPeriodsFileNameWithoutCompressionSuffix()).getParentFile(), str);
        if (filesInFolderMatchingStemRegex == null || filesInFolderMatchingStemRegex.length == 0) {
            this.currentPeriodsCounter = 0;
            return;
        }
        this.currentPeriodsCounter = ch.qos.logback.core.rolling.helper.FileFilterUtil.findHighestCounter(filesInFolderMatchingStemRegex, str);
        if (this.tbrp.getParentsRawFileProperty() != null || this.tbrp.compressionMode != ch.qos.logback.core.rolling.helper.CompressionMode.NONE) {
            this.currentPeriodsCounter++;
        }
    }

    /* access modifiers changed from: protected */
    public ch.qos.logback.core.rolling.helper.ArchiveRemover createArchiveRemover() {
        return new ch.qos.logback.core.rolling.helper.SizeAndTimeBasedArchiveRemover(this.tbrp.fileNamePattern, this.rc);
    }

    public java.lang.String getCurrentPeriodsFileNameWithoutCompressionSuffix() {
        return this.tbrp.fileNamePatternWCS.convertMultipleArguments(this.dateInCurrentPeriod, java.lang.Integer.valueOf(this.currentPeriodsCounter));
    }

    public java.lang.String getMaxFileSize() {
        return this.maxFileSizeAsString;
    }

    public boolean isTriggeringEvent(java.io.File file, E e) {
        long currentTime = getCurrentTime();
        if (currentTime >= this.nextCheck) {
            java.util.Date date = this.dateInCurrentPeriod;
            this.elapsedPeriodsFileName = this.tbrp.fileNamePatternWCS.convertMultipleArguments(date, java.lang.Integer.valueOf(this.currentPeriodsCounter));
            this.currentPeriodsCounter = 0;
            setDateInCurrentPeriod(currentTime);
            computeNextCheck();
            return true;
        }
        int i = this.invocationCounter + 1;
        this.invocationCounter = i;
        if ((i & this.invocationMask) != this.invocationMask) {
            return false;
        }
        if (this.invocationMask < 15) {
            this.invocationMask = (this.invocationMask << 1) + 1;
        }
        if (file.length() < this.maxFileSize.getSize()) {
            return false;
        }
        this.elapsedPeriodsFileName = this.tbrp.fileNamePatternWCS.convertMultipleArguments(this.dateInCurrentPeriod, java.lang.Integer.valueOf(this.currentPeriodsCounter));
        this.currentPeriodsCounter++;
        return true;
    }

    public void setMaxFileSize(java.lang.String str) {
        this.maxFileSizeAsString = str;
        this.maxFileSize = ch.qos.logback.core.util.FileSize.valueOf(str);
    }

    public void start() {
        super.start();
        this.archiveRemover = createArchiveRemover();
        this.archiveRemover.setContext(this.context);
        computeCurrentPeriodsHighestCounterValue(ch.qos.logback.core.rolling.helper.FileFilterUtil.afterLastSlash(this.tbrp.fileNamePattern.toRegexForFixedDate(this.dateInCurrentPeriod)));
        this.started = true;
    }
}
