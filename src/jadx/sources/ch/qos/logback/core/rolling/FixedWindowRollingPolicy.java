package ch.qos.logback.core.rolling;

public class FixedWindowRollingPolicy extends ch.qos.logback.core.rolling.RollingPolicyBase {
    static final java.lang.String FNP_NOT_SET = "The \"FileNamePattern\" property must be set before using FixedWindowRollingPolicy. ";
    private static int MAX_WINDOW_SIZE = 20;
    static final java.lang.String PRUDENT_MODE_UNSUPPORTED = "See also http://logback.qos.ch/codes.html#tbr_fnp_prudent_unsupported";
    static final java.lang.String SEE_PARENT_FN_NOT_SET = "Please refer to http://logback.qos.ch/codes.html#fwrp_parentFileName_not_set";
    public static final java.lang.String ZIP_ENTRY_DATE_PATTERN = "yyyy-MM-dd_HHmm";
    ch.qos.logback.core.rolling.helper.Compressor compressor;
    int maxIndex = 7;
    int minIndex = 1;

    /* renamed from: util reason: collision with root package name */
    ch.qos.logback.core.rolling.helper.RenameUtil f3util = new ch.qos.logback.core.rolling.helper.RenameUtil();

    private java.lang.String transformFileNamePatternFromInt2Date(java.lang.String str) {
        return ch.qos.logback.core.rolling.helper.FileFilterUtil.afterLastSlash(ch.qos.logback.core.rolling.helper.FileFilterUtil.slashify(str)).replace("%i", "%d{yyyy-MM-dd_HHmm}");
    }

    public java.lang.String getActiveFileName() {
        return getParentsRawFileProperty();
    }

    public int getMaxIndex() {
        return this.maxIndex;
    }

    /* access modifiers changed from: protected */
    public int getMaxWindowSize() {
        return MAX_WINDOW_SIZE;
    }

    public int getMinIndex() {
        return this.minIndex;
    }

    public void rollover() throws ch.qos.logback.core.rolling.RolloverFailure {
        if (this.maxIndex >= 0) {
            java.io.File file = new java.io.File(this.fileNamePattern.convertInt(this.maxIndex));
            if (file.exists()) {
                file.delete();
            }
            int i = this.maxIndex;
            while (true) {
                i--;
                if (i < this.minIndex) {
                    break;
                }
                java.lang.String convertInt = this.fileNamePattern.convertInt(i);
                if (new java.io.File(convertInt).exists()) {
                    this.f3util.rename(convertInt, this.fileNamePattern.convertInt(i + 1));
                } else {
                    addInfo("Skipping roll-over for inexistent file " + convertInt);
                }
            }
            switch (this.compressionMode) {
                case NONE:
                    this.f3util.rename(getActiveFileName(), this.fileNamePattern.convertInt(this.minIndex));
                    return;
                case GZ:
                    this.compressor.compress(getActiveFileName(), this.fileNamePattern.convertInt(this.minIndex), null);
                    return;
                case ZIP:
                    this.compressor.compress(getActiveFileName(), this.fileNamePattern.convertInt(this.minIndex), this.zipEntryFileNamePattern.convert(new java.util.Date()));
                    return;
                default:
                    return;
            }
        }
    }

    public void setMaxIndex(int i) {
        this.maxIndex = i;
    }

    public void setMinIndex(int i) {
        this.minIndex = i;
    }

    public void start() {
        this.f3util.setContext(this.context);
        if (this.fileNamePatternStr != null) {
            this.fileNamePattern = new ch.qos.logback.core.rolling.helper.FileNamePattern(this.fileNamePatternStr, this.context);
            determineCompressionMode();
            if (isParentPrudent()) {
                addError("Prudent mode is not supported with FixedWindowRollingPolicy.");
                addError(PRUDENT_MODE_UNSUPPORTED);
                throw new java.lang.IllegalStateException("Prudent mode is not supported.");
            } else if (getParentsRawFileProperty() == null) {
                addError("The File name property must be set before using this rolling policy.");
                addError(SEE_PARENT_FN_NOT_SET);
                throw new java.lang.IllegalStateException("The \"File\" option must be set.");
            } else {
                if (this.maxIndex < this.minIndex) {
                    addWarn("MaxIndex (" + this.maxIndex + ") cannot be smaller than MinIndex (" + this.minIndex + ").");
                    addWarn("Setting maxIndex to equal minIndex.");
                    this.maxIndex = this.minIndex;
                }
                int maxWindowSize = getMaxWindowSize();
                if (this.maxIndex - this.minIndex > maxWindowSize) {
                    addWarn("Large window sizes are not allowed.");
                    this.maxIndex = maxWindowSize + this.minIndex;
                    addWarn("MaxIndex reduced to " + this.maxIndex);
                }
                if (this.fileNamePattern.getIntegerTokenConverter() == null) {
                    throw new java.lang.IllegalStateException("FileNamePattern [" + this.fileNamePattern.getPattern() + "] does not contain a valid IntegerToken");
                }
                if (this.compressionMode == ch.qos.logback.core.rolling.helper.CompressionMode.ZIP) {
                    this.zipEntryFileNamePattern = new ch.qos.logback.core.rolling.helper.FileNamePattern(transformFileNamePatternFromInt2Date(this.fileNamePatternStr), this.context);
                }
                this.compressor = new ch.qos.logback.core.rolling.helper.Compressor(this.compressionMode);
                this.compressor.setContext(this.context);
                super.start();
            }
        } else {
            addError(FNP_NOT_SET);
            addError(ch.qos.logback.core.CoreConstants.SEE_FNP_NOT_SET);
            throw new java.lang.IllegalStateException("The \"FileNamePattern\" property must be set before using FixedWindowRollingPolicy. See also http://logback.qos.ch/codes.html#tbr_fnp_not_set");
        }
    }
}
