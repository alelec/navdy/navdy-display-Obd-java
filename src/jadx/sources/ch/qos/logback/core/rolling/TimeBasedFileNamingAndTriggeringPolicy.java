package ch.qos.logback.core.rolling;

public interface TimeBasedFileNamingAndTriggeringPolicy<E> extends ch.qos.logback.core.rolling.TriggeringPolicy<E>, ch.qos.logback.core.spi.ContextAware {
    ch.qos.logback.core.rolling.helper.ArchiveRemover getArchiveRemover();

    java.lang.String getCurrentPeriodsFileNameWithoutCompressionSuffix();

    long getCurrentTime();

    java.lang.String getElapsedPeriodsFileName();

    void setCurrentTime(long j);

    void setTimeBasedRollingPolicy(ch.qos.logback.core.rolling.TimeBasedRollingPolicy<E> timeBasedRollingPolicy);
}
