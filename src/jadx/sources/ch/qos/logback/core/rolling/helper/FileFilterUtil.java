package ch.qos.logback.core.rolling.helper;

public class FileFilterUtil {
    public static java.lang.String afterLastSlash(java.lang.String str) {
        int lastIndexOf = str.lastIndexOf(47);
        return lastIndexOf == -1 ? str : str.substring(lastIndexOf + 1);
    }

    public static int extractCounter(java.io.File file, java.lang.String str) {
        java.util.regex.Pattern compile = java.util.regex.Pattern.compile(str);
        java.lang.String name = file.getName();
        java.util.regex.Matcher matcher = compile.matcher(name);
        if (matcher.matches()) {
            return new java.lang.Integer(matcher.group(1)).intValue();
        }
        throw new java.lang.IllegalStateException("The regex [" + str + "] should match [" + name + "]");
    }

    public static java.io.File[] filesInFolderMatchingStemRegex(java.io.File file, final java.lang.String str) {
        return file == null ? new java.io.File[0] : (!file.exists() || !file.isDirectory()) ? new java.io.File[0] : file.listFiles(new java.io.FilenameFilter() {
            public boolean accept(java.io.File file, java.lang.String str) {
                return str.matches(str);
            }
        });
    }

    public static int findHighestCounter(java.io.File[] fileArr, java.lang.String str) {
        int i = Integer.MIN_VALUE;
        int length = fileArr.length;
        int i2 = 0;
        while (i2 < length) {
            int extractCounter = extractCounter(fileArr[i2], str);
            if (i >= extractCounter) {
                extractCounter = i;
            }
            i2++;
            i = extractCounter;
        }
        return i;
    }

    public static boolean isEmptyDirectory(java.io.File file) {
        if (!file.isDirectory()) {
            throw new java.lang.IllegalArgumentException("[" + file + "] must be a directory");
        }
        java.lang.String[] list = file.list();
        return list == null || list.length == 0;
    }

    public static void removeEmptyParentDirectories(java.io.File file, int i) {
        if (i < 3) {
            java.io.File parentFile = file.getParentFile();
            if (parentFile.isDirectory() && isEmptyDirectory(parentFile)) {
                parentFile.delete();
                removeEmptyParentDirectories(parentFile, i + 1);
            }
        }
    }

    public static void reverseSortFileArrayByName(java.io.File[] fileArr) {
        java.util.Arrays.sort(fileArr, new java.util.Comparator<java.io.File>() {
            public int compare(java.io.File file, java.io.File file2) {
                return file2.getName().compareTo(file.getName());
            }
        });
    }

    public static java.lang.String slashify(java.lang.String str) {
        return str.replace(ch.qos.logback.core.CoreConstants.ESCAPE_CHAR, '/');
    }

    public static void sortFileArrayByName(java.io.File[] fileArr) {
        java.util.Arrays.sort(fileArr, new java.util.Comparator<java.io.File>() {
            public int compare(java.io.File file, java.io.File file2) {
                return file.getName().compareTo(file2.getName());
            }
        });
    }
}
