package ch.qos.logback.core.rolling.helper;

public class TimeBasedArchiveRemover extends ch.qos.logback.core.rolling.helper.DefaultArchiveRemover {
    public TimeBasedArchiveRemover(ch.qos.logback.core.rolling.helper.FileNamePattern fileNamePattern, ch.qos.logback.core.rolling.helper.RollingCalendar rollingCalendar) {
        super(fileNamePattern, rollingCalendar);
    }

    /* access modifiers changed from: protected */
    public void cleanByPeriodOffset(java.util.Date date, int i) {
        java.io.File file = new java.io.File(this.fileNamePattern.convert(this.rc.getRelativeDate(date, i)));
        if (file.exists() && file.isFile()) {
            file.delete();
            addInfo("deleting " + file);
            if (this.parentClean) {
                removeFolderIfEmpty(file.getParentFile());
            }
        }
    }

    public java.lang.String toString() {
        return "c.q.l.core.rolling.helper.TimeBasedArchiveRemover";
    }
}
