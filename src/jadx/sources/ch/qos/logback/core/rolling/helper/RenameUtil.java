package ch.qos.logback.core.rolling.helper;

public class RenameUtil extends ch.qos.logback.core.spi.ContextAwareBase {
    static java.lang.String RENAMING_ERROR_URL = "http://logback.qos.ch/codes.html#renamingError";

    /* access modifiers changed from: 0000 */
    public boolean areOnDifferentVolumes(java.io.File file, java.io.File file2) throws ch.qos.logback.core.rolling.RolloverFailure {
        if (!ch.qos.logback.core.util.EnvUtil.isJDK7OrHigher()) {
            return false;
        }
        try {
            return !ch.qos.logback.core.rolling.helper.FileStoreUtil.areOnSameFileStore(file, file2.getParentFile());
        } catch (ch.qos.logback.core.rolling.RolloverFailure e) {
            addWarn("Error while checking file store equality", e);
            return false;
        }
    }

    /* access modifiers changed from: 0000 */
    public void createMissingTargetDirsIfNecessary(java.io.File file) throws ch.qos.logback.core.rolling.RolloverFailure {
        if (ch.qos.logback.core.util.FileUtil.isParentDirectoryCreationRequired(file) && !ch.qos.logback.core.util.FileUtil.createMissingParentDirectories(file)) {
            throw new ch.qos.logback.core.rolling.RolloverFailure("Failed to create parent directories for [" + file.getAbsolutePath() + "]");
        }
    }

    public void rename(java.lang.String str, java.lang.String str2) throws ch.qos.logback.core.rolling.RolloverFailure {
        if (str.equals(str2)) {
            addWarn("Source and target files are the same [" + str + "]. Skipping.");
            return;
        }
        java.io.File file = new java.io.File(str);
        if (file.exists()) {
            java.io.File file2 = new java.io.File(str2);
            createMissingTargetDirsIfNecessary(file2);
            addInfo("Renaming file [" + file + "] to [" + file2 + "]");
            if (!file.renameTo(file2)) {
                addWarn("Failed to rename file [" + file + "] as [" + file2 + "].");
                if (areOnDifferentVolumes(file, file2)) {
                    addWarn("Detected different file systems for source [" + str + "] and target [" + str2 + "]. Attempting rename by copying.");
                    renameByCopying(str, str2);
                    return;
                }
                addWarn("Please consider leaving the [file] option of " + ch.qos.logback.core.rolling.RollingFileAppender.class.getSimpleName() + " empty.");
                addWarn("See also " + RENAMING_ERROR_URL);
                return;
            }
            return;
        }
        throw new ch.qos.logback.core.rolling.RolloverFailure("File [" + str + "] does not exist.");
    }

    public void renameByCopying(java.lang.String str, java.lang.String str2) throws ch.qos.logback.core.rolling.RolloverFailure {
        new ch.qos.logback.core.util.FileUtil(getContext()).copy(str, str2);
        if (!new java.io.File(str).delete()) {
            addWarn("Could not delete " + str);
        }
    }

    public java.lang.String toString() {
        return "c.q.l.co.rolling.helper.RenameUtil";
    }
}
