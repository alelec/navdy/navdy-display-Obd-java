package ch.qos.logback.core.rolling;

public class RolloverFailure extends ch.qos.logback.core.LogbackException {
    private static final long serialVersionUID = -4407533730831239458L;

    public RolloverFailure(java.lang.String str) {
        super(str);
    }

    public RolloverFailure(java.lang.String str, java.lang.Throwable th) {
        super(str, th);
    }
}
