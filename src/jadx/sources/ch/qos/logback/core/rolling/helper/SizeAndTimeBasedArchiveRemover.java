package ch.qos.logback.core.rolling.helper;

public class SizeAndTimeBasedArchiveRemover extends ch.qos.logback.core.rolling.helper.DefaultArchiveRemover {
    public SizeAndTimeBasedArchiveRemover(ch.qos.logback.core.rolling.helper.FileNamePattern fileNamePattern, ch.qos.logback.core.rolling.helper.RollingCalendar rollingCalendar) {
        super(fileNamePattern, rollingCalendar);
    }

    public void cleanByPeriodOffset(java.util.Date date, int i) {
        java.util.Date relativeDate = this.rc.getRelativeDate(date, i);
        java.lang.String afterLastSlash = ch.qos.logback.core.rolling.helper.FileFilterUtil.afterLastSlash(this.fileNamePattern.toRegexForFixedDate(relativeDate));
        java.io.File parentFile = new java.io.File(this.fileNamePattern.convertMultipleArguments(relativeDate, java.lang.Integer.valueOf(0))).getAbsoluteFile().getAbsoluteFile().getParentFile();
        for (java.io.File delete : ch.qos.logback.core.rolling.helper.FileFilterUtil.filesInFolderMatchingStemRegex(parentFile, afterLastSlash)) {
            delete.delete();
        }
        if (this.parentClean) {
            removeFolderIfEmpty(parentFile);
        }
    }
}
