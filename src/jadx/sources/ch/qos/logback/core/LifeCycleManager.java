package ch.qos.logback.core;

public class LifeCycleManager {
    private final java.util.Set<ch.qos.logback.core.spi.LifeCycle> components = new java.util.HashSet();

    public void register(ch.qos.logback.core.spi.LifeCycle lifeCycle) {
        this.components.add(lifeCycle);
    }

    public void reset() {
        for (ch.qos.logback.core.spi.LifeCycle lifeCycle : this.components) {
            if (lifeCycle.isStarted()) {
                lifeCycle.stop();
            }
        }
        this.components.clear();
    }
}
