package ch.qos.logback.classic.encoder;

public class PatternLayoutEncoder extends ch.qos.logback.core.pattern.PatternLayoutEncoderBase<ch.qos.logback.classic.spi.ILoggingEvent> {
    public void start() {
        ch.qos.logback.classic.PatternLayout patternLayout = new ch.qos.logback.classic.PatternLayout();
        patternLayout.setContext(this.context);
        patternLayout.setPattern(getPattern());
        patternLayout.setOutputPatternAsHeader(this.outputPatternAsHeader);
        patternLayout.start();
        this.layout = patternLayout;
        super.start();
    }
}
