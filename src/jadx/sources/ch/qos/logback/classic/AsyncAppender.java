package ch.qos.logback.classic;

public class AsyncAppender extends ch.qos.logback.core.AsyncAppenderBase<ch.qos.logback.classic.spi.ILoggingEvent> {
    boolean includeCallerData = false;

    /* access modifiers changed from: protected */
    public boolean isDiscardable(ch.qos.logback.classic.spi.ILoggingEvent iLoggingEvent) {
        return iLoggingEvent.getLevel().toInt() <= 20000;
    }

    public boolean isIncludeCallerData() {
        return this.includeCallerData;
    }

    /* access modifiers changed from: protected */
    public void preprocess(ch.qos.logback.classic.spi.ILoggingEvent iLoggingEvent) {
        iLoggingEvent.prepareForDeferredProcessing();
        if (this.includeCallerData) {
            iLoggingEvent.getCallerData();
        }
    }

    public void setIncludeCallerData(boolean z) {
        this.includeCallerData = z;
    }
}
