package ch.qos.logback.classic.db.names;

public interface DBNameResolver {
    <N extends java.lang.Enum<?>> java.lang.String getColumnName(N n);

    <N extends java.lang.Enum<?>> java.lang.String getTableName(N n);
}
