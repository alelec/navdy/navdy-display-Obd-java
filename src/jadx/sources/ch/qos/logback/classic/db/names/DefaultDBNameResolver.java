package ch.qos.logback.classic.db.names;

public class DefaultDBNameResolver implements ch.qos.logback.classic.db.names.DBNameResolver {
    public <N extends java.lang.Enum<?>> java.lang.String getColumnName(N n) {
        return n.toString().toLowerCase();
    }

    public <N extends java.lang.Enum<?>> java.lang.String getTableName(N n) {
        return n.toString().toLowerCase();
    }
}
