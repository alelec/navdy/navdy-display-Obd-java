package ch.qos.logback.classic.db.names;

public class SimpleDBNameResolver implements ch.qos.logback.classic.db.names.DBNameResolver {
    private java.lang.String columnNamePrefix = "";
    private java.lang.String columnNameSuffix = "";
    private java.lang.String tableNamePrefix = "";
    private java.lang.String tableNameSuffix = "";

    public <N extends java.lang.Enum<?>> java.lang.String getColumnName(N n) {
        return this.columnNamePrefix + n.name().toLowerCase() + this.columnNameSuffix;
    }

    public <N extends java.lang.Enum<?>> java.lang.String getTableName(N n) {
        return this.tableNamePrefix + n.name().toLowerCase() + this.tableNameSuffix;
    }

    public void setColumnNamePrefix(java.lang.String str) {
        if (str == null) {
            str = "";
        }
        this.columnNamePrefix = str;
    }

    public void setColumnNameSuffix(java.lang.String str) {
        if (str == null) {
            str = "";
        }
        this.columnNameSuffix = str;
    }

    public void setTableNamePrefix(java.lang.String str) {
        if (str == null) {
            str = "";
        }
        this.tableNamePrefix = str;
    }

    public void setTableNameSuffix(java.lang.String str) {
        if (str == null) {
            str = "";
        }
        this.tableNameSuffix = str;
    }
}
