package ch.qos.logback.classic.db;

public class SQLBuilder {
    public static java.lang.String buildCreateExceptionTableSQL(ch.qos.logback.classic.db.names.DBNameResolver dBNameResolver) {
        java.lang.StringBuilder sb = new java.lang.StringBuilder("CREATE TABLE IF NOT EXISTS ");
        sb.append(dBNameResolver.getTableName(ch.qos.logback.classic.db.names.TableName.LOGGING_EVENT_EXCEPTION)).append(" (").append(dBNameResolver.getColumnName(ch.qos.logback.classic.db.names.ColumnName.EVENT_ID)).append(" BIGINT NOT NULL, ").append(dBNameResolver.getColumnName(ch.qos.logback.classic.db.names.ColumnName.I)).append(" SMALLINT NOT NULL, ").append(dBNameResolver.getColumnName(ch.qos.logback.classic.db.names.ColumnName.TRACE_LINE)).append(" VARCHAR(254) NOT NULL, ").append("PRIMARY KEY (").append(dBNameResolver.getColumnName(ch.qos.logback.classic.db.names.ColumnName.EVENT_ID)).append(", ").append(dBNameResolver.getColumnName(ch.qos.logback.classic.db.names.ColumnName.I)).append("), ").append("FOREIGN KEY (").append(dBNameResolver.getColumnName(ch.qos.logback.classic.db.names.ColumnName.EVENT_ID)).append(") ").append("REFERENCES ").append(dBNameResolver.getTableName(ch.qos.logback.classic.db.names.TableName.LOGGING_EVENT)).append(" (").append(dBNameResolver.getColumnName(ch.qos.logback.classic.db.names.ColumnName.EVENT_ID)).append(") ").append(")");
        return sb.toString();
    }

    public static java.lang.String buildCreateLoggingEventTableSQL(ch.qos.logback.classic.db.names.DBNameResolver dBNameResolver) {
        java.lang.StringBuilder sb = new java.lang.StringBuilder("CREATE TABLE IF NOT EXISTS ");
        sb.append(dBNameResolver.getTableName(ch.qos.logback.classic.db.names.TableName.LOGGING_EVENT)).append(" (").append(dBNameResolver.getColumnName(ch.qos.logback.classic.db.names.ColumnName.TIMESTMP)).append(" BIGINT NOT NULL, ").append(dBNameResolver.getColumnName(ch.qos.logback.classic.db.names.ColumnName.FORMATTED_MESSAGE)).append(" TEXT NOT NULL, ").append(dBNameResolver.getColumnName(ch.qos.logback.classic.db.names.ColumnName.LOGGER_NAME)).append(" VARCHAR(254) NOT NULL, ").append(dBNameResolver.getColumnName(ch.qos.logback.classic.db.names.ColumnName.LEVEL_STRING)).append(" VARCHAR(254) NOT NULL, ").append(dBNameResolver.getColumnName(ch.qos.logback.classic.db.names.ColumnName.THREAD_NAME)).append(" VARCHAR(254), ").append(dBNameResolver.getColumnName(ch.qos.logback.classic.db.names.ColumnName.REFERENCE_FLAG)).append(" SMALLINT, ").append(dBNameResolver.getColumnName(ch.qos.logback.classic.db.names.ColumnName.ARG0)).append(" VARCHAR(254), ").append(dBNameResolver.getColumnName(ch.qos.logback.classic.db.names.ColumnName.ARG1)).append(" VARCHAR(254), ").append(dBNameResolver.getColumnName(ch.qos.logback.classic.db.names.ColumnName.ARG2)).append(" VARCHAR(254), ").append(dBNameResolver.getColumnName(ch.qos.logback.classic.db.names.ColumnName.ARG3)).append(" VARCHAR(254), ").append(dBNameResolver.getColumnName(ch.qos.logback.classic.db.names.ColumnName.CALLER_FILENAME)).append(" VARCHAR(254), ").append(dBNameResolver.getColumnName(ch.qos.logback.classic.db.names.ColumnName.CALLER_CLASS)).append(" VARCHAR(254), ").append(dBNameResolver.getColumnName(ch.qos.logback.classic.db.names.ColumnName.CALLER_METHOD)).append(" VARCHAR(254), ").append(dBNameResolver.getColumnName(ch.qos.logback.classic.db.names.ColumnName.CALLER_LINE)).append(" CHAR(4), ").append(dBNameResolver.getColumnName(ch.qos.logback.classic.db.names.ColumnName.EVENT_ID)).append(" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT").append(")");
        return sb.toString();
    }

    public static java.lang.String buildCreatePropertyTableSQL(ch.qos.logback.classic.db.names.DBNameResolver dBNameResolver) {
        java.lang.StringBuilder sb = new java.lang.StringBuilder("CREATE TABLE IF NOT EXISTS ");
        sb.append(dBNameResolver.getTableName(ch.qos.logback.classic.db.names.TableName.LOGGING_EVENT_PROPERTY)).append(" (").append(dBNameResolver.getColumnName(ch.qos.logback.classic.db.names.ColumnName.EVENT_ID)).append(" BIGINT NOT NULL, ").append(dBNameResolver.getColumnName(ch.qos.logback.classic.db.names.ColumnName.MAPPED_KEY)).append(" VARCHAR(254) NOT NULL, ").append(dBNameResolver.getColumnName(ch.qos.logback.classic.db.names.ColumnName.MAPPED_VALUE)).append(" VARCHAR(254) NOT NULL, ").append("PRIMARY KEY (").append(dBNameResolver.getColumnName(ch.qos.logback.classic.db.names.ColumnName.EVENT_ID)).append(", ").append(dBNameResolver.getColumnName(ch.qos.logback.classic.db.names.ColumnName.MAPPED_KEY)).append("), ").append("FOREIGN KEY (").append(dBNameResolver.getColumnName(ch.qos.logback.classic.db.names.ColumnName.EVENT_ID)).append(") ").append("REFERENCES ").append(dBNameResolver.getTableName(ch.qos.logback.classic.db.names.TableName.LOGGING_EVENT)).append(" (").append(dBNameResolver.getColumnName(ch.qos.logback.classic.db.names.ColumnName.EVENT_ID)).append(") ").append(")");
        return sb.toString();
    }

    public static java.lang.String buildInsertExceptionSQL(ch.qos.logback.classic.db.names.DBNameResolver dBNameResolver) {
        java.lang.StringBuilder sb = new java.lang.StringBuilder("INSERT INTO ");
        sb.append(dBNameResolver.getTableName(ch.qos.logback.classic.db.names.TableName.LOGGING_EVENT_EXCEPTION)).append(" (").append(dBNameResolver.getColumnName(ch.qos.logback.classic.db.names.ColumnName.EVENT_ID)).append(", ").append(dBNameResolver.getColumnName(ch.qos.logback.classic.db.names.ColumnName.I)).append(", ").append(dBNameResolver.getColumnName(ch.qos.logback.classic.db.names.ColumnName.TRACE_LINE)).append(") ").append("VALUES (?, ?, ?)");
        return sb.toString();
    }

    public static java.lang.String buildInsertPropertiesSQL(ch.qos.logback.classic.db.names.DBNameResolver dBNameResolver) {
        java.lang.StringBuilder sb = new java.lang.StringBuilder("INSERT INTO ");
        sb.append(dBNameResolver.getTableName(ch.qos.logback.classic.db.names.TableName.LOGGING_EVENT_PROPERTY)).append(" (");
        sb.append(dBNameResolver.getColumnName(ch.qos.logback.classic.db.names.ColumnName.EVENT_ID)).append(", ");
        sb.append(dBNameResolver.getColumnName(ch.qos.logback.classic.db.names.ColumnName.MAPPED_KEY)).append(", ");
        sb.append(dBNameResolver.getColumnName(ch.qos.logback.classic.db.names.ColumnName.MAPPED_VALUE)).append(") ");
        sb.append("VALUES (?, ?, ?)");
        return sb.toString();
    }

    public static java.lang.String buildInsertSQL(ch.qos.logback.classic.db.names.DBNameResolver dBNameResolver) {
        java.lang.StringBuilder sb = new java.lang.StringBuilder("INSERT INTO ");
        sb.append(dBNameResolver.getTableName(ch.qos.logback.classic.db.names.TableName.LOGGING_EVENT)).append(" (").append(dBNameResolver.getColumnName(ch.qos.logback.classic.db.names.ColumnName.TIMESTMP)).append(", ").append(dBNameResolver.getColumnName(ch.qos.logback.classic.db.names.ColumnName.FORMATTED_MESSAGE)).append(", ").append(dBNameResolver.getColumnName(ch.qos.logback.classic.db.names.ColumnName.LOGGER_NAME)).append(", ").append(dBNameResolver.getColumnName(ch.qos.logback.classic.db.names.ColumnName.LEVEL_STRING)).append(", ").append(dBNameResolver.getColumnName(ch.qos.logback.classic.db.names.ColumnName.THREAD_NAME)).append(", ").append(dBNameResolver.getColumnName(ch.qos.logback.classic.db.names.ColumnName.REFERENCE_FLAG)).append(", ").append(dBNameResolver.getColumnName(ch.qos.logback.classic.db.names.ColumnName.ARG0)).append(", ").append(dBNameResolver.getColumnName(ch.qos.logback.classic.db.names.ColumnName.ARG1)).append(", ").append(dBNameResolver.getColumnName(ch.qos.logback.classic.db.names.ColumnName.ARG2)).append(", ").append(dBNameResolver.getColumnName(ch.qos.logback.classic.db.names.ColumnName.ARG3)).append(", ").append(dBNameResolver.getColumnName(ch.qos.logback.classic.db.names.ColumnName.CALLER_FILENAME)).append(", ").append(dBNameResolver.getColumnName(ch.qos.logback.classic.db.names.ColumnName.CALLER_CLASS)).append(", ").append(dBNameResolver.getColumnName(ch.qos.logback.classic.db.names.ColumnName.CALLER_METHOD)).append(", ").append(dBNameResolver.getColumnName(ch.qos.logback.classic.db.names.ColumnName.CALLER_LINE)).append(") ").append("VALUES (?, ?, ? ,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
        return sb.toString();
    }
}
