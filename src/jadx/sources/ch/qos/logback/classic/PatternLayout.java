package ch.qos.logback.classic;

public class PatternLayout extends ch.qos.logback.core.pattern.PatternLayoutBase<ch.qos.logback.classic.spi.ILoggingEvent> {
    public static final java.util.Map<java.lang.String, java.lang.String> defaultConverterMap = new java.util.HashMap();

    static {
        defaultConverterMap.putAll(ch.qos.logback.core.pattern.parser.Parser.DEFAULT_COMPOSITE_CONVERTER_MAP);
        defaultConverterMap.put(ch.qos.logback.core.rolling.helper.DateTokenConverter.CONVERTER_KEY, ch.qos.logback.classic.pattern.DateConverter.class.getName());
        defaultConverterMap.put("date", ch.qos.logback.classic.pattern.DateConverter.class.getName());
        defaultConverterMap.put("r", ch.qos.logback.classic.pattern.RelativeTimeConverter.class.getName());
        defaultConverterMap.put("relative", ch.qos.logback.classic.pattern.RelativeTimeConverter.class.getName());
        defaultConverterMap.put("level", ch.qos.logback.classic.pattern.LevelConverter.class.getName());
        defaultConverterMap.put("le", ch.qos.logback.classic.pattern.LevelConverter.class.getName());
        defaultConverterMap.put("p", ch.qos.logback.classic.pattern.LevelConverter.class.getName());
        defaultConverterMap.put("t", ch.qos.logback.classic.pattern.ThreadConverter.class.getName());
        defaultConverterMap.put("thread", ch.qos.logback.classic.pattern.ThreadConverter.class.getName());
        defaultConverterMap.put("lo", ch.qos.logback.classic.pattern.LoggerConverter.class.getName());
        defaultConverterMap.put("logger", ch.qos.logback.classic.pattern.LoggerConverter.class.getName());
        defaultConverterMap.put("c", ch.qos.logback.classic.pattern.LoggerConverter.class.getName());
        defaultConverterMap.put(ch.qos.logback.core.pattern.color.ANSIConstants.ESC_END, ch.qos.logback.classic.pattern.MessageConverter.class.getName());
        defaultConverterMap.put("msg", ch.qos.logback.classic.pattern.MessageConverter.class.getName());
        defaultConverterMap.put("message", ch.qos.logback.classic.pattern.MessageConverter.class.getName());
        defaultConverterMap.put("C", ch.qos.logback.classic.pattern.ClassOfCallerConverter.class.getName());
        defaultConverterMap.put(ch.qos.logback.core.joran.action.Action.CLASS_ATTRIBUTE, ch.qos.logback.classic.pattern.ClassOfCallerConverter.class.getName());
        defaultConverterMap.put("M", ch.qos.logback.classic.pattern.MethodOfCallerConverter.class.getName());
        defaultConverterMap.put("method", ch.qos.logback.classic.pattern.MethodOfCallerConverter.class.getName());
        defaultConverterMap.put("L", ch.qos.logback.classic.pattern.LineOfCallerConverter.class.getName());
        defaultConverterMap.put("line", ch.qos.logback.classic.pattern.LineOfCallerConverter.class.getName());
        defaultConverterMap.put("F", ch.qos.logback.classic.pattern.FileOfCallerConverter.class.getName());
        defaultConverterMap.put(ch.qos.logback.core.joran.action.Action.FILE_ATTRIBUTE, ch.qos.logback.classic.pattern.FileOfCallerConverter.class.getName());
        defaultConverterMap.put("X", ch.qos.logback.classic.pattern.MDCConverter.class.getName());
        defaultConverterMap.put("mdc", ch.qos.logback.classic.pattern.MDCConverter.class.getName());
        defaultConverterMap.put("ex", ch.qos.logback.classic.pattern.ThrowableProxyConverter.class.getName());
        defaultConverterMap.put("exception", ch.qos.logback.classic.pattern.ThrowableProxyConverter.class.getName());
        defaultConverterMap.put("rEx", ch.qos.logback.classic.pattern.RootCauseFirstThrowableProxyConverter.class.getName());
        defaultConverterMap.put("rootException", ch.qos.logback.classic.pattern.RootCauseFirstThrowableProxyConverter.class.getName());
        defaultConverterMap.put("throwable", ch.qos.logback.classic.pattern.ThrowableProxyConverter.class.getName());
        defaultConverterMap.put("xEx", ch.qos.logback.classic.pattern.ExtendedThrowableProxyConverter.class.getName());
        defaultConverterMap.put("xException", ch.qos.logback.classic.pattern.ExtendedThrowableProxyConverter.class.getName());
        defaultConverterMap.put("xThrowable", ch.qos.logback.classic.pattern.ExtendedThrowableProxyConverter.class.getName());
        defaultConverterMap.put("nopex", ch.qos.logback.classic.pattern.NopThrowableInformationConverter.class.getName());
        defaultConverterMap.put("nopexception", ch.qos.logback.classic.pattern.NopThrowableInformationConverter.class.getName());
        defaultConverterMap.put("cn", ch.qos.logback.classic.pattern.ContextNameConverter.class.getName());
        defaultConverterMap.put("contextName", ch.qos.logback.classic.pattern.ContextNameConverter.class.getName());
        defaultConverterMap.put("caller", ch.qos.logback.classic.pattern.CallerDataConverter.class.getName());
        defaultConverterMap.put("marker", ch.qos.logback.classic.pattern.MarkerConverter.class.getName());
        defaultConverterMap.put("property", ch.qos.logback.classic.pattern.PropertyConverter.class.getName());
        defaultConverterMap.put("n", ch.qos.logback.classic.pattern.LineSeparatorConverter.class.getName());
        defaultConverterMap.put("black", ch.qos.logback.core.pattern.color.BlackCompositeConverter.class.getName());
        defaultConverterMap.put("red", ch.qos.logback.core.pattern.color.RedCompositeConverter.class.getName());
        defaultConverterMap.put("green", ch.qos.logback.core.pattern.color.GreenCompositeConverter.class.getName());
        defaultConverterMap.put("yellow", ch.qos.logback.core.pattern.color.YellowCompositeConverter.class.getName());
        defaultConverterMap.put("blue", ch.qos.logback.core.pattern.color.BlueCompositeConverter.class.getName());
        defaultConverterMap.put("magenta", ch.qos.logback.core.pattern.color.MagentaCompositeConverter.class.getName());
        defaultConverterMap.put("cyan", ch.qos.logback.core.pattern.color.CyanCompositeConverter.class.getName());
        defaultConverterMap.put("white", ch.qos.logback.core.pattern.color.WhiteCompositeConverter.class.getName());
        defaultConverterMap.put("gray", ch.qos.logback.core.pattern.color.GrayCompositeConverter.class.getName());
        defaultConverterMap.put("boldRed", ch.qos.logback.core.pattern.color.BoldRedCompositeConverter.class.getName());
        defaultConverterMap.put("boldGreen", ch.qos.logback.core.pattern.color.BoldGreenCompositeConverter.class.getName());
        defaultConverterMap.put("boldYellow", ch.qos.logback.core.pattern.color.BoldYellowCompositeConverter.class.getName());
        defaultConverterMap.put("boldBlue", ch.qos.logback.core.pattern.color.BoldBlueCompositeConverter.class.getName());
        defaultConverterMap.put("boldMagenta", ch.qos.logback.core.pattern.color.BoldMagentaCompositeConverter.class.getName());
        defaultConverterMap.put("boldCyan", ch.qos.logback.core.pattern.color.BoldCyanCompositeConverter.class.getName());
        defaultConverterMap.put("boldWhite", ch.qos.logback.core.pattern.color.BoldWhiteCompositeConverter.class.getName());
        defaultConverterMap.put("highlight", ch.qos.logback.classic.pattern.color.HighlightingCompositeConverter.class.getName());
        defaultConverterMap.put("lsn", ch.qos.logback.classic.pattern.LocalSequenceNumberConverter.class.getName());
    }

    public PatternLayout() {
        this.postCompileProcessor = new ch.qos.logback.classic.pattern.EnsureExceptionHandling();
    }

    public java.lang.String doLayout(ch.qos.logback.classic.spi.ILoggingEvent iLoggingEvent) {
        return !isStarted() ? "" : writeLoopOnConverters(iLoggingEvent);
    }

    public java.util.Map<java.lang.String, java.lang.String> getDefaultConverterMap() {
        return defaultConverterMap;
    }
}
