package ch.qos.logback.classic.util;

public class ContextInitializer {
    private static final java.lang.String ASSETS_DIR = ch.qos.logback.core.android.CommonPathUtil.getAssetsDirectoryPath();
    public static final java.lang.String AUTOCONFIG_FILE = "logback.xml";
    public static final java.lang.String CONFIG_FILE_PROPERTY = "logback.configurationFile";
    public static final java.lang.String STATUS_LISTENER_CLASS = "logback.statusListenerClass";
    final java.lang.ClassLoader classLoader = ch.qos.logback.core.util.Loader.getClassLoaderOfObject(this);
    final ch.qos.logback.classic.LoggerContext loggerContext;

    public ContextInitializer(ch.qos.logback.classic.LoggerContext loggerContext2) {
        this.loggerContext = loggerContext2;
    }

    /* JADX WARNING: type inference failed for: r1v0, types: [java.net.URL] */
    /* JADX WARNING: type inference failed for: r0v1, types: [java.net.URL] */
    /* JADX WARNING: type inference failed for: r1v1, types: [java.lang.String] */
    /* JADX WARNING: type inference failed for: r1v2, types: [java.lang.String] */
    /* JADX WARNING: type inference failed for: r0v6 */
    /* JADX WARNING: type inference failed for: r0v7, types: [java.lang.String] */
    /* JADX WARNING: type inference failed for: r0v8, types: [java.lang.String] */
    /* JADX WARNING: type inference failed for: r1v3, types: [java.lang.String] */
    /* JADX WARNING: type inference failed for: r1v4, types: [java.lang.String] */
    /* JADX WARNING: type inference failed for: r1v5, types: [java.lang.String] */
    /* JADX WARNING: type inference failed for: r1v6, types: [java.lang.String] */
    /* JADX WARNING: type inference failed for: r1v7 */
    /* JADX WARNING: type inference failed for: r0v14 */
    /* JADX WARNING: type inference failed for: r1v8 */
    /* JADX WARNING: type inference failed for: r1v9 */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r1v0, types: [java.net.URL]
  assigns: [?[int, float, boolean, short, byte, char, OBJECT, ARRAY], java.lang.String]
  uses: [?[OBJECT, ARRAY], ?[int, boolean, OBJECT, ARRAY, byte, short, char], java.net.URL, java.lang.String]
  mth insns count: 52
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Unknown variable types count: 7 */
    private java.net.URL findConfigFileFromSystemProperties(boolean z) {
        java.net.URL url;
        ? r1 = 0;
        java.lang.String systemProperty = ch.qos.logback.core.util.OptionHelper.getSystemProperty(CONFIG_FILE_PROPERTY);
        if (systemProperty != null) {
            try {
                java.io.File file = new java.io.File(systemProperty);
                if (!file.exists() || !file.isFile()) {
                    url = new java.net.URL(systemProperty);
                } else {
                    if (z) {
                        statusOnResourceSearch(systemProperty, this.classLoader, systemProperty);
                    }
                    url = file.toURI().toURL();
                }
                if (!z) {
                    return url;
                }
                java.lang.ClassLoader classLoader2 = this.classLoader;
                if (url != null) {
                    r1 = url.toString();
                }
                statusOnResourceSearch(systemProperty, classLoader2, r1);
                return url;
            } catch (java.net.MalformedURLException e) {
                java.net.URL resource = ch.qos.logback.core.util.Loader.getResource(systemProperty, this.classLoader);
                if (resource != null) {
                    if (!z) {
                        return resource;
                    }
                    java.lang.ClassLoader classLoader3 = this.classLoader;
                    if (resource != null) {
                        r1 = resource.toString();
                    }
                    statusOnResourceSearch(systemProperty, classLoader3, r1);
                    return resource;
                } else if (z) {
                    statusOnResourceSearch(systemProperty, this.classLoader, resource != null ? resource.toString() : r1);
                }
            } catch (Throwable th) {
                if (z) {
                    java.lang.ClassLoader classLoader4 = this.classLoader;
                    if (r1 != 0) {
                        r1 = r1.toString();
                    }
                    statusOnResourceSearch(systemProperty, classLoader4, r1);
                }
                throw th;
            }
        }
        return r1;
    }

    private java.io.InputStream findConfigFileURLFromAssets(boolean z) {
        return getResource(ASSETS_DIR + "/" + AUTOCONFIG_FILE, this.classLoader, z);
    }

    private java.io.InputStream getResource(java.lang.String str, java.lang.ClassLoader classLoader2, boolean z) {
        java.io.InputStream resourceAsStream = classLoader2.getResourceAsStream(str);
        if (z) {
            java.lang.String str2 = null;
            if (resourceAsStream != null) {
                str2 = str;
            }
            statusOnResourceSearch(str, classLoader2, str2);
        }
        return resourceAsStream;
    }

    private void statusOnResourceSearch(java.lang.String str, java.lang.ClassLoader classLoader2, java.lang.String str2) {
        ch.qos.logback.core.status.StatusManager statusManager = this.loggerContext.getStatusManager();
        if (str2 == null) {
            statusManager.add((ch.qos.logback.core.status.Status) new ch.qos.logback.core.status.InfoStatus("Could NOT find resource [" + str + "]", this.loggerContext));
        } else {
            statusManager.add((ch.qos.logback.core.status.Status) new ch.qos.logback.core.status.InfoStatus("Found resource [" + str + "] at [" + str2 + "]", this.loggerContext));
        }
    }

    public void autoConfig() throws ch.qos.logback.core.joran.spi.JoranException {
        ch.qos.logback.classic.util.StatusListenerConfigHelper.installIfAsked(this.loggerContext);
        boolean z = false;
        ch.qos.logback.classic.joran.JoranConfigurator joranConfigurator = new ch.qos.logback.classic.joran.JoranConfigurator();
        joranConfigurator.setContext(this.loggerContext);
        java.net.URL findConfigFileFromSystemProperties = findConfigFileFromSystemProperties(true);
        if (findConfigFileFromSystemProperties != null) {
            joranConfigurator.doConfigure(findConfigFileFromSystemProperties);
            z = true;
        }
        if (!z) {
            java.io.InputStream findConfigFileURLFromAssets = findConfigFileURLFromAssets(true);
            if (findConfigFileURLFromAssets != null) {
                joranConfigurator.doConfigure(findConfigFileURLFromAssets);
            }
        }
    }
}
