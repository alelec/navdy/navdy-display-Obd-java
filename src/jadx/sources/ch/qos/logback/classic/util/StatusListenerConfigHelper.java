package ch.qos.logback.classic.util;

public class StatusListenerConfigHelper {
    private static void addStatusListener(ch.qos.logback.classic.LoggerContext loggerContext, java.lang.String str) {
        initListener(loggerContext, createListenerPerClassName(loggerContext, str));
    }

    private static ch.qos.logback.core.status.StatusListener createListenerPerClassName(ch.qos.logback.classic.LoggerContext loggerContext, java.lang.String str) {
        try {
            return (ch.qos.logback.core.status.StatusListener) ch.qos.logback.core.util.OptionHelper.instantiateByClassName(str, ch.qos.logback.core.status.StatusListener.class, (ch.qos.logback.core.Context) loggerContext);
        } catch (java.lang.Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static void initListener(ch.qos.logback.classic.LoggerContext loggerContext, ch.qos.logback.core.status.StatusListener statusListener) {
        if (statusListener != null) {
            if (statusListener instanceof ch.qos.logback.core.spi.ContextAware) {
                ((ch.qos.logback.core.spi.ContextAware) statusListener).setContext(loggerContext);
            }
            if (statusListener instanceof ch.qos.logback.core.spi.LifeCycle) {
                ((ch.qos.logback.core.spi.LifeCycle) statusListener).start();
            }
            loggerContext.getStatusManager().add(statusListener);
        }
    }

    static void installIfAsked(ch.qos.logback.classic.LoggerContext loggerContext) {
        java.lang.String systemProperty = ch.qos.logback.core.util.OptionHelper.getSystemProperty(ch.qos.logback.classic.util.ContextInitializer.STATUS_LISTENER_CLASS);
        if (!ch.qos.logback.core.util.OptionHelper.isEmpty(systemProperty)) {
            addStatusListener(loggerContext, systemProperty);
        }
    }
}
