package ch.qos.logback.classic.util;

public class ContextSelectorStaticBinder {
    static ch.qos.logback.classic.util.ContextSelectorStaticBinder singleton = new ch.qos.logback.classic.util.ContextSelectorStaticBinder();
    ch.qos.logback.classic.selector.ContextSelector contextSelector;
    java.lang.Object key;

    static ch.qos.logback.classic.selector.ContextSelector dynamicalContextSelector(ch.qos.logback.classic.LoggerContext loggerContext, java.lang.String str) throws java.lang.ClassNotFoundException, java.lang.SecurityException, java.lang.NoSuchMethodException, java.lang.IllegalArgumentException, java.lang.InstantiationException, java.lang.IllegalAccessException, java.lang.reflect.InvocationTargetException {
        return (ch.qos.logback.classic.selector.ContextSelector) ch.qos.logback.core.util.Loader.loadClass(str).getConstructor(new java.lang.Class[]{ch.qos.logback.classic.LoggerContext.class}).newInstance(new java.lang.Object[]{loggerContext});
    }

    public static ch.qos.logback.classic.util.ContextSelectorStaticBinder getSingleton() {
        return singleton;
    }

    public ch.qos.logback.classic.selector.ContextSelector getContextSelector() {
        return this.contextSelector;
    }

    public void init(ch.qos.logback.classic.LoggerContext loggerContext, java.lang.Object obj) throws java.lang.ClassNotFoundException, java.lang.NoSuchMethodException, java.lang.InstantiationException, java.lang.IllegalAccessException, java.lang.reflect.InvocationTargetException {
        if (this.key == null) {
            this.key = obj;
        } else if (this.key != obj) {
            throw new java.lang.IllegalAccessException("Only certain classes can access this method.");
        }
        java.lang.String systemProperty = ch.qos.logback.core.util.OptionHelper.getSystemProperty(ch.qos.logback.classic.ClassicConstants.LOGBACK_CONTEXT_SELECTOR);
        if (systemProperty == null) {
            this.contextSelector = new ch.qos.logback.classic.selector.DefaultContextSelector(loggerContext);
        } else if (systemProperty.equals("JNDI")) {
            throw new java.lang.RuntimeException("JNDI not supported");
        } else {
            this.contextSelector = dynamicalContextSelector(loggerContext, systemProperty);
        }
    }
}
