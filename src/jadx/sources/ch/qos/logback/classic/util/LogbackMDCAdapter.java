package ch.qos.logback.classic.util;

public final class LogbackMDCAdapter implements org.slf4j.spi.MDCAdapter {
    private static final int READ_OPERATION = 2;
    private static final int WRITE_OPERATION = 1;
    final java.lang.InheritableThreadLocal<java.util.Map<java.lang.String, java.lang.String>> copyOnInheritThreadLocal = new java.lang.InheritableThreadLocal<>();
    final java.lang.ThreadLocal<java.lang.Integer> lastOperation = new java.lang.ThreadLocal<>();

    private java.util.Map<java.lang.String, java.lang.String> duplicateAndInsertNewMap(java.util.Map<java.lang.String, java.lang.String> map) {
        java.util.Map<java.lang.String, java.lang.String> synchronizedMap = java.util.Collections.synchronizedMap(new java.util.HashMap());
        if (map != null) {
            synchronized (map) {
                synchronizedMap.putAll(map);
            }
        }
        this.copyOnInheritThreadLocal.set(synchronizedMap);
        return synchronizedMap;
    }

    private java.lang.Integer getAndSetLastOperation(int i) {
        java.lang.Integer num = (java.lang.Integer) this.lastOperation.get();
        this.lastOperation.set(java.lang.Integer.valueOf(i));
        return num;
    }

    private boolean wasLastOpReadOrNull(java.lang.Integer num) {
        return num == null || num.intValue() == 2;
    }

    public void clear() {
        this.lastOperation.set(java.lang.Integer.valueOf(1));
        this.copyOnInheritThreadLocal.remove();
    }

    public java.lang.String get(java.lang.String str) {
        java.util.Map propertyMap = getPropertyMap();
        if (propertyMap == null || str == null) {
            return null;
        }
        return (java.lang.String) propertyMap.get(str);
    }

    public java.util.Map getCopyOfContextMap() {
        this.lastOperation.set(java.lang.Integer.valueOf(2));
        java.util.Map map = (java.util.Map) this.copyOnInheritThreadLocal.get();
        if (map == null) {
            return null;
        }
        return new java.util.HashMap(map);
    }

    public java.util.Set<java.lang.String> getKeys() {
        java.util.Map propertyMap = getPropertyMap();
        if (propertyMap != null) {
            return propertyMap.keySet();
        }
        return null;
    }

    public java.util.Map<java.lang.String, java.lang.String> getPropertyMap() {
        this.lastOperation.set(java.lang.Integer.valueOf(2));
        return (java.util.Map) this.copyOnInheritThreadLocal.get();
    }

    public void put(java.lang.String str, java.lang.String str2) throws java.lang.IllegalArgumentException {
        if (str == null) {
            throw new java.lang.IllegalArgumentException("key cannot be null");
        }
        java.util.Map map = (java.util.Map) this.copyOnInheritThreadLocal.get();
        if (wasLastOpReadOrNull(getAndSetLastOperation(1)) || map == null) {
            duplicateAndInsertNewMap(map).put(str, str2);
        } else {
            map.put(str, str2);
        }
    }

    public void remove(java.lang.String str) {
        if (str != null) {
            java.util.Map map = (java.util.Map) this.copyOnInheritThreadLocal.get();
            if (map == null) {
                return;
            }
            if (wasLastOpReadOrNull(getAndSetLastOperation(1))) {
                duplicateAndInsertNewMap(map).remove(str);
            } else {
                map.remove(str);
            }
        }
    }

    public void setContextMap(java.util.Map map) {
        this.lastOperation.set(java.lang.Integer.valueOf(1));
        java.util.Map synchronizedMap = java.util.Collections.synchronizedMap(new java.util.HashMap());
        synchronizedMap.putAll(map);
        this.copyOnInheritThreadLocal.set(synchronizedMap);
    }
}
