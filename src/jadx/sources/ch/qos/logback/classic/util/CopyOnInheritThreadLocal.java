package ch.qos.logback.classic.util;

public class CopyOnInheritThreadLocal extends java.lang.InheritableThreadLocal<java.util.HashMap<java.lang.String, java.lang.String>> {
    /* access modifiers changed from: protected */
    public java.util.HashMap<java.lang.String, java.lang.String> childValue(java.util.HashMap<java.lang.String, java.lang.String> hashMap) {
        if (hashMap == null) {
            return null;
        }
        return new java.util.HashMap<>(hashMap);
    }
}
