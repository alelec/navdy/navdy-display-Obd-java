package ch.qos.logback.classic.util;

public class LevelToSyslogSeverity {
    public static int convert(ch.qos.logback.classic.spi.ILoggingEvent iLoggingEvent) {
        ch.qos.logback.classic.Level level = iLoggingEvent.getLevel();
        switch (level.levelInt) {
            case 5000:
            case 10000:
                return 7;
            case ch.qos.logback.classic.Level.INFO_INT /*20000*/:
                return 6;
            case 30000:
                return 4;
            case ch.qos.logback.classic.Level.ERROR_INT /*40000*/:
                return 3;
            default:
                throw new java.lang.IllegalArgumentException("Level " + level + " is not a valid level for a printing method");
        }
    }
}
