package ch.qos.logback.classic.util;

public class DefaultNestedComponentRules {
    public static void addDefaultNestedComponentRegistryRules(ch.qos.logback.core.joran.spi.DefaultNestedComponentRegistry defaultNestedComponentRegistry) {
        defaultNestedComponentRegistry.add(ch.qos.logback.core.AppenderBase.class, "layout", ch.qos.logback.classic.PatternLayout.class);
        defaultNestedComponentRegistry.add(ch.qos.logback.core.UnsynchronizedAppenderBase.class, "layout", ch.qos.logback.classic.PatternLayout.class);
        defaultNestedComponentRegistry.add(ch.qos.logback.core.AppenderBase.class, "encoder", ch.qos.logback.classic.encoder.PatternLayoutEncoder.class);
        defaultNestedComponentRegistry.add(ch.qos.logback.core.UnsynchronizedAppenderBase.class, "encoder", ch.qos.logback.classic.encoder.PatternLayoutEncoder.class);
        ch.qos.logback.core.net.ssl.SSLNestedComponentRegistryRules.addDefaultNestedComponentRegistryRules(defaultNestedComponentRegistry);
    }
}
