package ch.qos.logback.classic.spi;

public class EventArgUtil {
    public static java.lang.Object[] arrangeArguments(java.lang.Object[] objArr) {
        return objArr;
    }

    public static final java.lang.Throwable extractThrowable(java.lang.Object[] objArr) {
        if (objArr == null || objArr.length == 0) {
            return null;
        }
        java.lang.Throwable th = objArr[objArr.length - 1];
        if (th instanceof java.lang.Throwable) {
            return th;
        }
        return null;
    }

    public static boolean successfulExtraction(java.lang.Throwable th) {
        return th != null;
    }

    public static java.lang.Object[] trimmedCopy(java.lang.Object[] objArr) {
        if (objArr == null || objArr.length == 0) {
            throw new java.lang.IllegalStateException("non-sensical empty or null argument array");
        }
        int length = objArr.length - 1;
        java.lang.Object[] objArr2 = new java.lang.Object[length];
        java.lang.System.arraycopy(objArr, 0, objArr2, 0, length);
        return objArr2;
    }
}
