package ch.qos.logback.classic.spi;

public class LoggerRemoteView implements java.io.Serializable {
    static final /* synthetic */ boolean $assertionsDisabled = (!ch.qos.logback.classic.spi.LoggerRemoteView.class.desiredAssertionStatus());
    private static final long serialVersionUID = 5028223666108713696L;
    final ch.qos.logback.classic.spi.LoggerContextVO loggerContextView;
    final java.lang.String name;

    public LoggerRemoteView(java.lang.String str, ch.qos.logback.classic.LoggerContext loggerContext) {
        this.name = str;
        if ($assertionsDisabled || loggerContext.getLoggerContextRemoteView() != null) {
            this.loggerContextView = loggerContext.getLoggerContextRemoteView();
            return;
        }
        throw new java.lang.AssertionError();
    }

    public ch.qos.logback.classic.spi.LoggerContextVO getLoggerContextView() {
        return this.loggerContextView;
    }

    public java.lang.String getName() {
        return this.name;
    }
}
