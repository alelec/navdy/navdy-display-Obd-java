package ch.qos.logback.classic.spi;

public class ThrowableProxyUtil {
    private static final int BUILDER_CAPACITY = 2048;
    public static final int REGULAR_EXCEPTION_INDENT = 1;
    public static final int SUPPRESSED_EXCEPTION_INDENT = 1;

    public static java.lang.String asString(ch.qos.logback.classic.spi.IThrowableProxy iThrowableProxy) {
        java.lang.StringBuilder sb = new java.lang.StringBuilder(2048);
        recursiveAppend(sb, null, 1, iThrowableProxy);
        return sb.toString();
    }

    public static void build(ch.qos.logback.classic.spi.ThrowableProxy throwableProxy, java.lang.Throwable th, ch.qos.logback.classic.spi.ThrowableProxy throwableProxy2) {
        java.lang.StackTraceElement[] stackTrace = th.getStackTrace();
        int i = -1;
        if (throwableProxy2 != null) {
            i = findNumberOfCommonFrames(stackTrace, throwableProxy2.getStackTraceElementProxyArray());
        }
        throwableProxy.commonFrames = i;
        throwableProxy.stackTraceElementProxyArray = steArrayToStepArray(stackTrace);
    }

    static int findNumberOfCommonFrames(java.lang.StackTraceElement[] stackTraceElementArr, ch.qos.logback.classic.spi.StackTraceElementProxy[] stackTraceElementProxyArr) {
        int i = 0;
        if (!(stackTraceElementProxyArr == null || stackTraceElementArr == null)) {
            int length = stackTraceElementArr.length - 1;
            int length2 = stackTraceElementProxyArr.length - 1;
            while (length >= 0 && length2 >= 0 && stackTraceElementArr[length].equals(stackTraceElementProxyArr[length2].ste)) {
                i++;
                length--;
                length2--;
            }
        }
        return i;
    }

    public static void indent(java.lang.StringBuilder sb, int i) {
        for (int i2 = 0; i2 < i; i2++) {
            sb.append(9);
        }
    }

    private static void recursiveAppend(java.lang.StringBuilder sb, java.lang.String str, int i, ch.qos.logback.classic.spi.IThrowableProxy iThrowableProxy) {
        if (iThrowableProxy != null) {
            subjoinFirstLine(sb, str, i, iThrowableProxy);
            sb.append(ch.qos.logback.core.CoreConstants.LINE_SEPARATOR);
            subjoinSTEPArray(sb, i, iThrowableProxy);
            ch.qos.logback.classic.spi.IThrowableProxy[] suppressed = iThrowableProxy.getSuppressed();
            if (suppressed != null) {
                for (ch.qos.logback.classic.spi.IThrowableProxy recursiveAppend : suppressed) {
                    recursiveAppend(sb, ch.qos.logback.core.CoreConstants.SUPPRESSED, i + 1, recursiveAppend);
                }
            }
            recursiveAppend(sb, ch.qos.logback.core.CoreConstants.CAUSED_BY, i, iThrowableProxy.getCause());
        }
    }

    static ch.qos.logback.classic.spi.StackTraceElementProxy[] steArrayToStepArray(java.lang.StackTraceElement[] stackTraceElementArr) {
        if (stackTraceElementArr == null) {
            return new ch.qos.logback.classic.spi.StackTraceElementProxy[0];
        }
        ch.qos.logback.classic.spi.StackTraceElementProxy[] stackTraceElementProxyArr = new ch.qos.logback.classic.spi.StackTraceElementProxy[stackTraceElementArr.length];
        for (int i = 0; i < stackTraceElementProxyArr.length; i++) {
            stackTraceElementProxyArr[i] = new ch.qos.logback.classic.spi.StackTraceElementProxy(stackTraceElementArr[i]);
        }
        return stackTraceElementProxyArr;
    }

    private static void subjoinExceptionMessage(java.lang.StringBuilder sb, ch.qos.logback.classic.spi.IThrowableProxy iThrowableProxy) {
        sb.append(iThrowableProxy.getClassName()).append(": ").append(iThrowableProxy.getMessage());
    }

    public static void subjoinFirstLine(java.lang.StringBuilder sb, ch.qos.logback.classic.spi.IThrowableProxy iThrowableProxy) {
        if (iThrowableProxy.getCommonFrames() > 0) {
            sb.append(ch.qos.logback.core.CoreConstants.CAUSED_BY);
        }
        subjoinExceptionMessage(sb, iThrowableProxy);
    }

    private static void subjoinFirstLine(java.lang.StringBuilder sb, java.lang.String str, int i, ch.qos.logback.classic.spi.IThrowableProxy iThrowableProxy) {
        indent(sb, i - 1);
        if (str != null) {
            sb.append(str);
        }
        subjoinExceptionMessage(sb, iThrowableProxy);
    }

    public static void subjoinFirstLineRootCauseFirst(java.lang.StringBuilder sb, ch.qos.logback.classic.spi.IThrowableProxy iThrowableProxy) {
        if (iThrowableProxy.getCause() != null) {
            sb.append(ch.qos.logback.core.CoreConstants.WRAPPED_BY);
        }
        subjoinExceptionMessage(sb, iThrowableProxy);
    }

    public static void subjoinPackagingData(java.lang.StringBuilder sb, ch.qos.logback.classic.spi.StackTraceElementProxy stackTraceElementProxy) {
        if (stackTraceElementProxy != null) {
            ch.qos.logback.classic.spi.ClassPackagingData classPackagingData = stackTraceElementProxy.getClassPackagingData();
            if (classPackagingData != null) {
                if (!classPackagingData.isExact()) {
                    sb.append(" ~[");
                } else {
                    sb.append(" [");
                }
                sb.append(classPackagingData.getCodeLocation()).append(ch.qos.logback.core.CoreConstants.COLON_CHAR).append(classPackagingData.getVersion()).append(']');
            }
        }
    }

    public static void subjoinSTEP(java.lang.StringBuilder sb, ch.qos.logback.classic.spi.StackTraceElementProxy stackTraceElementProxy) {
        sb.append(stackTraceElementProxy.toString());
        subjoinPackagingData(sb, stackTraceElementProxy);
    }

    public static void subjoinSTEPArray(java.lang.StringBuilder sb, int i, ch.qos.logback.classic.spi.IThrowableProxy iThrowableProxy) {
        ch.qos.logback.classic.spi.StackTraceElementProxy[] stackTraceElementProxyArray = iThrowableProxy.getStackTraceElementProxyArray();
        int commonFrames = iThrowableProxy.getCommonFrames();
        for (int i2 = 0; i2 < stackTraceElementProxyArray.length - commonFrames; i2++) {
            ch.qos.logback.classic.spi.StackTraceElementProxy stackTraceElementProxy = stackTraceElementProxyArray[i2];
            indent(sb, i);
            subjoinSTEP(sb, stackTraceElementProxy);
            sb.append(ch.qos.logback.core.CoreConstants.LINE_SEPARATOR);
        }
        if (commonFrames > 0) {
            indent(sb, i);
            sb.append("... ").append(commonFrames).append(" common frames omitted").append(ch.qos.logback.core.CoreConstants.LINE_SEPARATOR);
        }
    }

    public static void subjoinSTEPArray(java.lang.StringBuilder sb, ch.qos.logback.classic.spi.IThrowableProxy iThrowableProxy) {
        subjoinSTEPArray(sb, 1, iThrowableProxy);
    }
}
