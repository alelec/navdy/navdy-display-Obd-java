package ch.qos.logback.classic.spi;

public interface IThrowableProxy {
    ch.qos.logback.classic.spi.IThrowableProxy getCause();

    java.lang.String getClassName();

    int getCommonFrames();

    java.lang.String getMessage();

    ch.qos.logback.classic.spi.StackTraceElementProxy[] getStackTraceElementProxyArray();

    ch.qos.logback.classic.spi.IThrowableProxy[] getSuppressed();
}
