package ch.qos.logback.classic.spi;

public class StackTraceElementProxy implements java.io.Serializable {
    private static final long serialVersionUID = -2374374378980555982L;
    private ch.qos.logback.classic.spi.ClassPackagingData cpd;
    final java.lang.StackTraceElement ste;
    private transient java.lang.String steAsString;

    public StackTraceElementProxy(java.lang.StackTraceElement stackTraceElement) {
        if (stackTraceElement == null) {
            throw new java.lang.IllegalArgumentException("ste cannot be null");
        }
        this.ste = stackTraceElement;
    }

    public boolean equals(java.lang.Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ch.qos.logback.classic.spi.StackTraceElementProxy stackTraceElementProxy = (ch.qos.logback.classic.spi.StackTraceElementProxy) obj;
        if (!this.ste.equals(stackTraceElementProxy.ste)) {
            return false;
        }
        return this.cpd == null ? stackTraceElementProxy.cpd == null : this.cpd.equals(stackTraceElementProxy.cpd);
    }

    public ch.qos.logback.classic.spi.ClassPackagingData getClassPackagingData() {
        return this.cpd;
    }

    public java.lang.String getSTEAsString() {
        if (this.steAsString == null) {
            this.steAsString = "at " + this.ste.toString();
        }
        return this.steAsString;
    }

    public java.lang.StackTraceElement getStackTraceElement() {
        return this.ste;
    }

    public int hashCode() {
        return this.ste.hashCode();
    }

    public void setClassPackagingData(ch.qos.logback.classic.spi.ClassPackagingData classPackagingData) {
        if (this.cpd != null) {
            throw new java.lang.IllegalStateException("Packaging data has been already set");
        }
        this.cpd = classPackagingData;
    }

    public java.lang.String toString() {
        return getSTEAsString();
    }
}
