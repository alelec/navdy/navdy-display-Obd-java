package ch.qos.logback.classic.spi;

public class ClassPackagingData implements java.io.Serializable {
    private static final long serialVersionUID = -804643281218337001L;
    final java.lang.String codeLocation;
    private final boolean exact;
    final java.lang.String version;

    public ClassPackagingData(java.lang.String str, java.lang.String str2) {
        this.codeLocation = str;
        this.version = str2;
        this.exact = true;
    }

    public ClassPackagingData(java.lang.String str, java.lang.String str2, boolean z) {
        this.codeLocation = str;
        this.version = str2;
        this.exact = z;
    }

    public boolean equals(java.lang.Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ch.qos.logback.classic.spi.ClassPackagingData classPackagingData = (ch.qos.logback.classic.spi.ClassPackagingData) obj;
        if (this.codeLocation == null) {
            if (classPackagingData.codeLocation != null) {
                return false;
            }
        } else if (!this.codeLocation.equals(classPackagingData.codeLocation)) {
            return false;
        }
        if (this.exact != classPackagingData.exact) {
            return false;
        }
        return this.version == null ? classPackagingData.version == null : this.version.equals(classPackagingData.version);
    }

    public java.lang.String getCodeLocation() {
        return this.codeLocation;
    }

    public java.lang.String getVersion() {
        return this.version;
    }

    public int hashCode() {
        return (this.codeLocation == null ? 0 : this.codeLocation.hashCode()) + 31;
    }

    public boolean isExact() {
        return this.exact;
    }
}
