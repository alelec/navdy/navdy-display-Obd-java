package ch.qos.logback.classic.spi;

public class ThrowableProxy implements ch.qos.logback.classic.spi.IThrowableProxy {
    private static final java.lang.reflect.Method GET_SUPPRESSED_METHOD;
    private static final ch.qos.logback.classic.spi.ThrowableProxy[] NO_SUPPRESSED = new ch.qos.logback.classic.spi.ThrowableProxy[0];
    private boolean calculatedPackageData = false;
    private ch.qos.logback.classic.spi.ThrowableProxy cause;
    private java.lang.String className;
    int commonFrames;
    private java.lang.String message;
    private transient ch.qos.logback.classic.spi.PackagingDataCalculator packagingDataCalculator;
    ch.qos.logback.classic.spi.StackTraceElementProxy[] stackTraceElementProxyArray;
    private ch.qos.logback.classic.spi.ThrowableProxy[] suppressed = NO_SUPPRESSED;
    private java.lang.Throwable throwable;

    static {
        java.lang.reflect.Method method = null;
        try {
            method = java.lang.Throwable.class.getMethod("getSuppressed", new java.lang.Class[0]);
        } catch (java.lang.NoSuchMethodException e) {
        }
        GET_SUPPRESSED_METHOD = method;
    }

    public ThrowableProxy(java.lang.Throwable th) {
        this.throwable = th;
        this.className = th.getClass().getName();
        this.message = th.getMessage();
        this.stackTraceElementProxyArray = ch.qos.logback.classic.spi.ThrowableProxyUtil.steArrayToStepArray(th.getStackTrace());
        java.lang.Throwable cause2 = th.getCause();
        if (cause2 != null) {
            this.cause = new ch.qos.logback.classic.spi.ThrowableProxy(cause2);
            this.cause.commonFrames = ch.qos.logback.classic.spi.ThrowableProxyUtil.findNumberOfCommonFrames(cause2.getStackTrace(), this.stackTraceElementProxyArray);
        }
        if (GET_SUPPRESSED_METHOD != null) {
            try {
                java.lang.Object invoke = GET_SUPPRESSED_METHOD.invoke(th, new java.lang.Object[0]);
                if (invoke instanceof java.lang.Throwable[]) {
                    java.lang.Throwable[] thArr = (java.lang.Throwable[]) invoke;
                    if (thArr.length > 0) {
                        this.suppressed = new ch.qos.logback.classic.spi.ThrowableProxy[thArr.length];
                        for (int i = 0; i < thArr.length; i++) {
                            this.suppressed[i] = new ch.qos.logback.classic.spi.ThrowableProxy(thArr[i]);
                            this.suppressed[i].commonFrames = ch.qos.logback.classic.spi.ThrowableProxyUtil.findNumberOfCommonFrames(thArr[i].getStackTrace(), this.stackTraceElementProxyArray);
                        }
                    }
                }
            } catch (java.lang.IllegalAccessException | java.lang.reflect.InvocationTargetException e) {
            }
        }
    }

    public void calculatePackagingData() {
        if (!this.calculatedPackageData) {
            ch.qos.logback.classic.spi.PackagingDataCalculator packagingDataCalculator2 = getPackagingDataCalculator();
            if (packagingDataCalculator2 != null) {
                this.calculatedPackageData = true;
                packagingDataCalculator2.calculate(this);
            }
        }
    }

    public void fullDump() {
        ch.qos.logback.classic.spi.StackTraceElementProxy[] stackTraceElementProxyArr;
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        for (ch.qos.logback.classic.spi.StackTraceElementProxy stackTraceElementProxy : this.stackTraceElementProxyArray) {
            sb.append(9).append(stackTraceElementProxy.toString());
            ch.qos.logback.classic.spi.ThrowableProxyUtil.subjoinPackagingData(sb, stackTraceElementProxy);
            sb.append(ch.qos.logback.core.CoreConstants.LINE_SEPARATOR);
        }
        java.lang.System.out.println(sb.toString());
    }

    public ch.qos.logback.classic.spi.IThrowableProxy getCause() {
        return this.cause;
    }

    public java.lang.String getClassName() {
        return this.className;
    }

    public int getCommonFrames() {
        return this.commonFrames;
    }

    public java.lang.String getMessage() {
        return this.message;
    }

    public ch.qos.logback.classic.spi.PackagingDataCalculator getPackagingDataCalculator() {
        if (this.throwable != null && this.packagingDataCalculator == null) {
            this.packagingDataCalculator = new ch.qos.logback.classic.spi.PackagingDataCalculator();
        }
        return this.packagingDataCalculator;
    }

    public ch.qos.logback.classic.spi.StackTraceElementProxy[] getStackTraceElementProxyArray() {
        return this.stackTraceElementProxyArray;
    }

    public ch.qos.logback.classic.spi.IThrowableProxy[] getSuppressed() {
        return this.suppressed;
    }

    public java.lang.Throwable getThrowable() {
        return this.throwable;
    }
}
