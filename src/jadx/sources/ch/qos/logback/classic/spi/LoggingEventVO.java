package ch.qos.logback.classic.spi;

public class LoggingEventVO implements ch.qos.logback.classic.spi.ILoggingEvent, java.io.Serializable {
    private static final int NULL_ARGUMENT_ARRAY = -1;
    private static final java.lang.String NULL_ARGUMENT_ARRAY_ELEMENT = "NULL_ARGUMENT_ARRAY_ELEMENT";
    private static final long serialVersionUID = 6553722650255690312L;
    private transient java.lang.Object[] argumentArray;
    private java.lang.StackTraceElement[] callerDataArray;
    private transient java.lang.String formattedMessage;
    private transient ch.qos.logback.classic.Level level;
    private ch.qos.logback.classic.spi.LoggerContextVO loggerContextVO;
    private java.lang.String loggerName;
    private org.slf4j.Marker marker;
    private java.util.Map<java.lang.String, java.lang.String> mdcPropertyMap;
    private java.lang.String message;
    private java.lang.String threadName;
    private ch.qos.logback.classic.spi.ThrowableProxyVO throwableProxy;
    private long timeStamp;

    public static ch.qos.logback.classic.spi.LoggingEventVO build(ch.qos.logback.classic.spi.ILoggingEvent iLoggingEvent) {
        ch.qos.logback.classic.spi.LoggingEventVO loggingEventVO = new ch.qos.logback.classic.spi.LoggingEventVO();
        loggingEventVO.loggerName = iLoggingEvent.getLoggerName();
        loggingEventVO.loggerContextVO = iLoggingEvent.getLoggerContextVO();
        loggingEventVO.threadName = iLoggingEvent.getThreadName();
        loggingEventVO.level = iLoggingEvent.getLevel();
        loggingEventVO.message = iLoggingEvent.getMessage();
        loggingEventVO.argumentArray = iLoggingEvent.getArgumentArray();
        loggingEventVO.marker = iLoggingEvent.getMarker();
        loggingEventVO.mdcPropertyMap = iLoggingEvent.getMDCPropertyMap();
        loggingEventVO.timeStamp = iLoggingEvent.getTimeStamp();
        loggingEventVO.throwableProxy = ch.qos.logback.classic.spi.ThrowableProxyVO.build(iLoggingEvent.getThrowableProxy());
        if (iLoggingEvent.hasCallerData()) {
            loggingEventVO.callerDataArray = iLoggingEvent.getCallerData();
        }
        return loggingEventVO;
    }

    private void readObject(java.io.ObjectInputStream objectInputStream) throws java.io.IOException, java.lang.ClassNotFoundException {
        objectInputStream.defaultReadObject();
        this.level = ch.qos.logback.classic.Level.toLevel(objectInputStream.readInt());
        int readInt = objectInputStream.readInt();
        if (readInt != -1) {
            this.argumentArray = new java.lang.String[readInt];
            for (int i = 0; i < readInt; i++) {
                java.lang.Object readObject = objectInputStream.readObject();
                if (!NULL_ARGUMENT_ARRAY_ELEMENT.equals(readObject)) {
                    this.argumentArray[i] = readObject;
                }
            }
        }
    }

    private void writeObject(java.io.ObjectOutputStream objectOutputStream) throws java.io.IOException {
        objectOutputStream.defaultWriteObject();
        objectOutputStream.writeInt(this.level.levelInt);
        if (this.argumentArray != null) {
            objectOutputStream.writeInt(this.argumentArray.length);
            for (int i = 0; i < this.argumentArray.length; i++) {
                if (this.argumentArray[i] != null) {
                    objectOutputStream.writeObject(this.argumentArray[i].toString());
                } else {
                    objectOutputStream.writeObject(NULL_ARGUMENT_ARRAY_ELEMENT);
                }
            }
            return;
        }
        objectOutputStream.writeInt(-1);
    }

    public boolean equals(java.lang.Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ch.qos.logback.classic.spi.LoggingEventVO loggingEventVO = (ch.qos.logback.classic.spi.LoggingEventVO) obj;
        if (this.message == null) {
            if (loggingEventVO.message != null) {
                return false;
            }
        } else if (!this.message.equals(loggingEventVO.message)) {
            return false;
        }
        if (this.loggerName == null) {
            if (loggingEventVO.loggerName != null) {
                return false;
            }
        } else if (!this.loggerName.equals(loggingEventVO.loggerName)) {
            return false;
        }
        if (this.threadName == null) {
            if (loggingEventVO.threadName != null) {
                return false;
            }
        } else if (!this.threadName.equals(loggingEventVO.threadName)) {
            return false;
        }
        if (this.timeStamp != loggingEventVO.timeStamp) {
            return false;
        }
        if (this.marker == null) {
            if (loggingEventVO.marker != null) {
                return false;
            }
        } else if (!this.marker.equals(loggingEventVO.marker)) {
            return false;
        }
        return this.mdcPropertyMap == null ? loggingEventVO.mdcPropertyMap == null : this.mdcPropertyMap.equals(loggingEventVO.mdcPropertyMap);
    }

    public java.lang.Object[] getArgumentArray() {
        return this.argumentArray;
    }

    public java.lang.StackTraceElement[] getCallerData() {
        return this.callerDataArray;
    }

    public long getContextBirthTime() {
        return this.loggerContextVO.getBirthTime();
    }

    public ch.qos.logback.classic.spi.LoggerContextVO getContextLoggerRemoteView() {
        return this.loggerContextVO;
    }

    public java.lang.String getFormattedMessage() {
        if (this.formattedMessage != null) {
            return this.formattedMessage;
        }
        if (this.argumentArray != null) {
            this.formattedMessage = org.slf4j.helpers.MessageFormatter.arrayFormat(this.message, this.argumentArray).getMessage();
        } else {
            this.formattedMessage = this.message;
        }
        return this.formattedMessage;
    }

    public ch.qos.logback.classic.Level getLevel() {
        return this.level;
    }

    public ch.qos.logback.classic.spi.LoggerContextVO getLoggerContextVO() {
        return this.loggerContextVO;
    }

    public java.lang.String getLoggerName() {
        return this.loggerName;
    }

    public java.util.Map<java.lang.String, java.lang.String> getMDCPropertyMap() {
        return this.mdcPropertyMap;
    }

    public org.slf4j.Marker getMarker() {
        return this.marker;
    }

    public java.util.Map<java.lang.String, java.lang.String> getMdc() {
        return this.mdcPropertyMap;
    }

    public java.lang.String getMessage() {
        return this.message;
    }

    public java.lang.String getThreadName() {
        return this.threadName;
    }

    public ch.qos.logback.classic.spi.IThrowableProxy getThrowableProxy() {
        return this.throwableProxy;
    }

    public long getTimeStamp() {
        return this.timeStamp;
    }

    public boolean hasCallerData() {
        return this.callerDataArray != null;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = ((this.message == null ? 0 : this.message.hashCode()) + 31) * 31;
        if (this.threadName != null) {
            i = this.threadName.hashCode();
        }
        return ((hashCode + i) * 31) + ((int) (this.timeStamp ^ (this.timeStamp >>> 32)));
    }

    public void prepareForDeferredProcessing() {
    }
}
