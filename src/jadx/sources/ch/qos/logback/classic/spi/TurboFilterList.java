package ch.qos.logback.classic.spi;

public final class TurboFilterList extends java.util.concurrent.CopyOnWriteArrayList<ch.qos.logback.classic.turbo.TurboFilter> {
    private static final long serialVersionUID = 1;

    public ch.qos.logback.core.spi.FilterReply getTurboFilterChainDecision(org.slf4j.Marker marker, ch.qos.logback.classic.Logger logger, ch.qos.logback.classic.Level level, java.lang.String str, java.lang.Object[] objArr, java.lang.Throwable th) {
        if (size() == 1) {
            try {
                return ((ch.qos.logback.classic.turbo.TurboFilter) get(0)).decide(marker, logger, level, str, objArr, th);
            } catch (java.lang.IndexOutOfBoundsException e) {
                return ch.qos.logback.core.spi.FilterReply.NEUTRAL;
            }
        } else {
            for (java.lang.Object obj : toArray()) {
                ch.qos.logback.core.spi.FilterReply decide = ((ch.qos.logback.classic.turbo.TurboFilter) obj).decide(marker, logger, level, str, objArr, th);
                if (decide == ch.qos.logback.core.spi.FilterReply.DENY || decide == ch.qos.logback.core.spi.FilterReply.ACCEPT) {
                    return decide;
                }
            }
            return ch.qos.logback.core.spi.FilterReply.NEUTRAL;
        }
    }
}
