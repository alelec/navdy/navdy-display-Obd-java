package ch.qos.logback.classic.spi;

public class CallerData {
    public static final java.lang.String CALLER_DATA_NA = ("?#?:?" + ch.qos.logback.core.CoreConstants.LINE_SEPARATOR);
    public static final java.lang.StackTraceElement[] EMPTY_CALLER_DATA_ARRAY = new java.lang.StackTraceElement[0];
    public static final int LINE_NA = -1;
    private static final java.lang.String LOG4J_CATEGORY = "org.apache.log4j.Category";
    public static final java.lang.String NA = "?";
    private static final java.lang.String SLF4J_BOUNDARY = "org.slf4j.Logger";

    public static java.lang.StackTraceElement[] extract(java.lang.Throwable th, java.lang.String str, int i, java.util.List<java.lang.String> list) {
        if (th == null) {
            return null;
        }
        java.lang.StackTraceElement[] stackTrace = th.getStackTrace();
        int i2 = -1;
        for (int i3 = 0; i3 < stackTrace.length; i3++) {
            if (isInFrameworkSpace(stackTrace[i3].getClassName(), str, list)) {
                i2 = i3 + 1;
            } else if (i2 != -1) {
                break;
            }
        }
        if (i2 == -1) {
            return EMPTY_CALLER_DATA_ARRAY;
        }
        int length = stackTrace.length - i2;
        if (i >= length) {
            i = length;
        }
        java.lang.StackTraceElement[] stackTraceElementArr = new java.lang.StackTraceElement[i];
        for (int i4 = 0; i4 < i; i4++) {
            stackTraceElementArr[i4] = stackTrace[i2 + i4];
        }
        return stackTraceElementArr;
    }

    static boolean isInFrameworkSpace(java.lang.String str, java.lang.String str2, java.util.List<java.lang.String> list) {
        return str.equals(str2) || str.equals(LOG4J_CATEGORY) || str.startsWith(SLF4J_BOUNDARY) || isInFrameworkSpaceList(str, list);
    }

    private static boolean isInFrameworkSpaceList(java.lang.String str, java.util.List<java.lang.String> list) {
        if (list == null) {
            return false;
        }
        for (java.lang.String startsWith : list) {
            if (str.startsWith(startsWith)) {
                return true;
            }
        }
        return false;
    }

    public static java.lang.StackTraceElement naInstance() {
        return new java.lang.StackTraceElement(NA, NA, NA, -1);
    }
}
