package ch.qos.logback.classic.spi;

public class ThrowableProxyVO implements ch.qos.logback.classic.spi.IThrowableProxy, java.io.Serializable {
    private static final long serialVersionUID = -773438177285807139L;
    private ch.qos.logback.classic.spi.IThrowableProxy cause;
    private java.lang.String className;
    private int commonFramesCount;
    private java.lang.String message;
    private ch.qos.logback.classic.spi.StackTraceElementProxy[] stackTraceElementProxyArray;
    private ch.qos.logback.classic.spi.IThrowableProxy[] suppressed;

    public static ch.qos.logback.classic.spi.ThrowableProxyVO build(ch.qos.logback.classic.spi.IThrowableProxy iThrowableProxy) {
        if (iThrowableProxy == null) {
            return null;
        }
        ch.qos.logback.classic.spi.ThrowableProxyVO throwableProxyVO = new ch.qos.logback.classic.spi.ThrowableProxyVO();
        throwableProxyVO.className = iThrowableProxy.getClassName();
        throwableProxyVO.message = iThrowableProxy.getMessage();
        throwableProxyVO.commonFramesCount = iThrowableProxy.getCommonFrames();
        throwableProxyVO.stackTraceElementProxyArray = iThrowableProxy.getStackTraceElementProxyArray();
        ch.qos.logback.classic.spi.IThrowableProxy cause2 = iThrowableProxy.getCause();
        if (cause2 != null) {
            throwableProxyVO.cause = build(cause2);
        }
        ch.qos.logback.classic.spi.IThrowableProxy[] suppressed2 = iThrowableProxy.getSuppressed();
        if (suppressed2 != null) {
            throwableProxyVO.suppressed = new ch.qos.logback.classic.spi.IThrowableProxy[suppressed2.length];
            for (int i = 0; i < suppressed2.length; i++) {
                throwableProxyVO.suppressed[i] = build(suppressed2[i]);
            }
        }
        return throwableProxyVO;
    }

    public boolean equals(java.lang.Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ch.qos.logback.classic.spi.ThrowableProxyVO throwableProxyVO = (ch.qos.logback.classic.spi.ThrowableProxyVO) obj;
        if (this.className == null) {
            if (throwableProxyVO.className != null) {
                return false;
            }
        } else if (!this.className.equals(throwableProxyVO.className)) {
            return false;
        }
        if (!java.util.Arrays.equals(this.stackTraceElementProxyArray, throwableProxyVO.stackTraceElementProxyArray)) {
            return false;
        }
        if (!java.util.Arrays.equals(this.suppressed, throwableProxyVO.suppressed)) {
            return false;
        }
        return this.cause == null ? throwableProxyVO.cause == null : this.cause.equals(throwableProxyVO.cause);
    }

    public ch.qos.logback.classic.spi.IThrowableProxy getCause() {
        return this.cause;
    }

    public java.lang.String getClassName() {
        return this.className;
    }

    public int getCommonFrames() {
        return this.commonFramesCount;
    }

    public java.lang.String getMessage() {
        return this.message;
    }

    public ch.qos.logback.classic.spi.StackTraceElementProxy[] getStackTraceElementProxyArray() {
        return this.stackTraceElementProxyArray;
    }

    public ch.qos.logback.classic.spi.IThrowableProxy[] getSuppressed() {
        return this.suppressed;
    }

    public int hashCode() {
        return (this.className == null ? 0 : this.className.hashCode()) + 31;
    }
}
