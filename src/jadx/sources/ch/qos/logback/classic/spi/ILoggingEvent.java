package ch.qos.logback.classic.spi;

public interface ILoggingEvent extends ch.qos.logback.core.spi.DeferredProcessingAware {
    java.lang.Object[] getArgumentArray();

    java.lang.StackTraceElement[] getCallerData();

    java.lang.String getFormattedMessage();

    ch.qos.logback.classic.Level getLevel();

    ch.qos.logback.classic.spi.LoggerContextVO getLoggerContextVO();

    java.lang.String getLoggerName();

    java.util.Map<java.lang.String, java.lang.String> getMDCPropertyMap();

    org.slf4j.Marker getMarker();

    java.util.Map<java.lang.String, java.lang.String> getMdc();

    java.lang.String getMessage();

    java.lang.String getThreadName();

    ch.qos.logback.classic.spi.IThrowableProxy getThrowableProxy();

    long getTimeStamp();

    boolean hasCallerData();

    void prepareForDeferredProcessing();
}
