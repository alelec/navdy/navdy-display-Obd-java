package ch.qos.logback.classic.spi;

public class LoggerContextAwareBase extends ch.qos.logback.core.spi.ContextAwareBase implements ch.qos.logback.classic.spi.LoggerContextAware {
    public ch.qos.logback.classic.LoggerContext getLoggerContext() {
        return (ch.qos.logback.classic.LoggerContext) this.context;
    }

    public void setContext(ch.qos.logback.core.Context context) {
        if ((context instanceof ch.qos.logback.classic.LoggerContext) || context == null) {
            super.setContext(context);
            return;
        }
        throw new java.lang.IllegalArgumentException("LoggerContextAwareBase only accepts contexts of type c.l.classic.LoggerContext");
    }

    public void setLoggerContext(ch.qos.logback.classic.LoggerContext loggerContext) {
        super.setContext(loggerContext);
    }
}
