package ch.qos.logback.classic.spi;

public interface LoggerContextAware extends ch.qos.logback.core.spi.ContextAware {
    void setLoggerContext(ch.qos.logback.classic.LoggerContext loggerContext);
}
