package ch.qos.logback.classic.spi;

public interface LoggerContextListener {
    boolean isResetResistant();

    void onLevelChange(ch.qos.logback.classic.Logger logger, ch.qos.logback.classic.Level level);

    void onReset(ch.qos.logback.classic.LoggerContext loggerContext);

    void onStart(ch.qos.logback.classic.LoggerContext loggerContext);

    void onStop(ch.qos.logback.classic.LoggerContext loggerContext);
}
