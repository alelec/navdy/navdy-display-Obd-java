package ch.qos.logback.classic.spi;

public class LoggerComparator implements java.util.Comparator<ch.qos.logback.classic.Logger>, java.io.Serializable {
    private static final long serialVersionUID = 1;

    public int compare(ch.qos.logback.classic.Logger logger, ch.qos.logback.classic.Logger logger2) {
        if (logger.getName().equals(logger2.getName())) {
            return 0;
        }
        if (logger.getName().equals(org.slf4j.Logger.ROOT_LOGGER_NAME)) {
            return -1;
        }
        if (logger2.getName().equals(org.slf4j.Logger.ROOT_LOGGER_NAME)) {
            return 1;
        }
        return logger.getName().compareTo(logger2.getName());
    }
}
