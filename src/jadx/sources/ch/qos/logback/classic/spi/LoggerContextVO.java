package ch.qos.logback.classic.spi;

public class LoggerContextVO implements java.io.Serializable {
    private static final long serialVersionUID = 5488023392483144387L;
    final long birthTime;
    final java.lang.String name;
    final java.util.Map<java.lang.String, java.lang.String> propertyMap;

    public LoggerContextVO(ch.qos.logback.classic.LoggerContext loggerContext) {
        this.name = loggerContext.getName();
        this.propertyMap = loggerContext.getCopyOfPropertyMap();
        this.birthTime = loggerContext.getBirthTime();
    }

    public LoggerContextVO(java.lang.String str, java.util.Map<java.lang.String, java.lang.String> map, long j) {
        this.name = str;
        this.propertyMap = map;
        this.birthTime = j;
    }

    public boolean equals(java.lang.Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ch.qos.logback.classic.spi.LoggerContextVO)) {
            return false;
        }
        ch.qos.logback.classic.spi.LoggerContextVO loggerContextVO = (ch.qos.logback.classic.spi.LoggerContextVO) obj;
        if (this.birthTime != loggerContextVO.birthTime) {
            return false;
        }
        if (this.name == null ? loggerContextVO.name != null : !this.name.equals(loggerContextVO.name)) {
            return false;
        }
        if (this.propertyMap != null) {
            if (this.propertyMap.equals(loggerContextVO.propertyMap)) {
                return true;
            }
        } else if (loggerContextVO.propertyMap == null) {
            return true;
        }
        return false;
    }

    public long getBirthTime() {
        return this.birthTime;
    }

    public java.lang.String getName() {
        return this.name;
    }

    public java.util.Map<java.lang.String, java.lang.String> getPropertyMap() {
        return this.propertyMap;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = (this.name != null ? this.name.hashCode() : 0) * 31;
        if (this.propertyMap != null) {
            i = this.propertyMap.hashCode();
        }
        return ((hashCode + i) * 31) + ((int) (this.birthTime ^ (this.birthTime >>> 32)));
    }

    public java.lang.String toString() {
        return "LoggerContextVO{name='" + this.name + ch.qos.logback.core.CoreConstants.SINGLE_QUOTE_CHAR + ", propertyMap=" + this.propertyMap + ", birthTime=" + this.birthTime + ch.qos.logback.core.CoreConstants.CURLY_RIGHT;
    }
}
