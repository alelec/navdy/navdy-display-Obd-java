package ch.qos.logback.classic.spi;

public class LoggingEvent implements ch.qos.logback.classic.spi.ILoggingEvent {
    private static final java.util.Map<java.lang.String, java.lang.String> CACHED_NULL_MAP = new java.util.HashMap();
    private transient java.lang.Object[] argumentArray;
    private java.lang.StackTraceElement[] callerDataArray;
    transient java.lang.String formattedMessage;
    transient java.lang.String fqnOfLoggerClass;
    private transient ch.qos.logback.classic.Level level;
    private ch.qos.logback.classic.LoggerContext loggerContext;
    private ch.qos.logback.classic.spi.LoggerContextVO loggerContextVO;
    private java.lang.String loggerName;
    private org.slf4j.Marker marker;
    private java.util.Map<java.lang.String, java.lang.String> mdcPropertyMap;
    private java.lang.String message;
    private java.lang.String threadName;
    private ch.qos.logback.classic.spi.ThrowableProxy throwableProxy;
    private long timeStamp;

    public LoggingEvent() {
    }

    public LoggingEvent(java.lang.String str, ch.qos.logback.classic.Logger logger, ch.qos.logback.classic.Level level2, java.lang.String str2, java.lang.Throwable th, java.lang.Object[] objArr) {
        this.fqnOfLoggerClass = str;
        this.loggerName = logger.getName();
        this.loggerContext = logger.getLoggerContext();
        this.loggerContextVO = this.loggerContext.getLoggerContextRemoteView();
        this.level = level2;
        this.message = str2;
        this.argumentArray = objArr;
        if (th == null) {
            th = extractThrowableAnRearrangeArguments(objArr);
        }
        if (th != null) {
            this.throwableProxy = new ch.qos.logback.classic.spi.ThrowableProxy(th);
            if (logger.getLoggerContext().isPackagingDataEnabled()) {
                this.throwableProxy.calculatePackagingData();
            }
        }
        this.timeStamp = java.lang.System.currentTimeMillis();
    }

    private java.lang.Throwable extractThrowableAnRearrangeArguments(java.lang.Object[] objArr) {
        java.lang.Throwable extractThrowable = ch.qos.logback.classic.spi.EventArgUtil.extractThrowable(objArr);
        if (ch.qos.logback.classic.spi.EventArgUtil.successfulExtraction(extractThrowable)) {
            this.argumentArray = ch.qos.logback.classic.spi.EventArgUtil.trimmedCopy(objArr);
        }
        return extractThrowable;
    }

    private void writeObject(java.io.ObjectOutputStream objectOutputStream) throws java.io.IOException {
        throw new java.lang.UnsupportedOperationException(getClass() + " does not support serialization. " + "Use LoggerEventVO instance instead. See also LoggerEventVO.build method.");
    }

    public java.lang.Object[] getArgumentArray() {
        return this.argumentArray;
    }

    public java.lang.StackTraceElement[] getCallerData() {
        if (this.callerDataArray == null) {
            this.callerDataArray = ch.qos.logback.classic.spi.CallerData.extract(new java.lang.Throwable(), this.fqnOfLoggerClass, this.loggerContext.getMaxCallerDataDepth(), this.loggerContext.getFrameworkPackages());
        }
        return this.callerDataArray;
    }

    public long getContextBirthTime() {
        return this.loggerContextVO.getBirthTime();
    }

    public java.lang.String getFormattedMessage() {
        if (this.formattedMessage != null) {
            return this.formattedMessage;
        }
        if (this.argumentArray != null) {
            this.formattedMessage = org.slf4j.helpers.MessageFormatter.arrayFormat(this.message, this.argumentArray).getMessage();
        } else {
            this.formattedMessage = this.message;
        }
        return this.formattedMessage;
    }

    public ch.qos.logback.classic.Level getLevel() {
        return this.level;
    }

    public ch.qos.logback.classic.spi.LoggerContextVO getLoggerContextVO() {
        return this.loggerContextVO;
    }

    public java.lang.String getLoggerName() {
        return this.loggerName;
    }

    public java.util.Map<java.lang.String, java.lang.String> getMDCPropertyMap() {
        if (this.mdcPropertyMap == null) {
            org.slf4j.spi.MDCAdapter mDCAdapter = org.slf4j.MDC.getMDCAdapter();
            if (mDCAdapter instanceof ch.qos.logback.classic.util.LogbackMDCAdapter) {
                this.mdcPropertyMap = ((ch.qos.logback.classic.util.LogbackMDCAdapter) mDCAdapter).getPropertyMap();
            } else {
                this.mdcPropertyMap = mDCAdapter.getCopyOfContextMap();
            }
        }
        if (this.mdcPropertyMap == null) {
            this.mdcPropertyMap = CACHED_NULL_MAP;
        }
        return this.mdcPropertyMap;
    }

    public org.slf4j.Marker getMarker() {
        return this.marker;
    }

    public java.util.Map<java.lang.String, java.lang.String> getMdc() {
        return getMDCPropertyMap();
    }

    public java.lang.String getMessage() {
        return this.message;
    }

    public java.lang.String getThreadName() {
        if (this.threadName == null) {
            this.threadName = java.lang.Thread.currentThread().getName();
        }
        return this.threadName;
    }

    public ch.qos.logback.classic.spi.IThrowableProxy getThrowableProxy() {
        return this.throwableProxy;
    }

    public long getTimeStamp() {
        return this.timeStamp;
    }

    public boolean hasCallerData() {
        return this.callerDataArray != null;
    }

    public void prepareForDeferredProcessing() {
        getFormattedMessage();
        getThreadName();
        getMDCPropertyMap();
    }

    public void setArgumentArray(java.lang.Object[] objArr) {
        if (this.argumentArray != null) {
            throw new java.lang.IllegalStateException("argArray has been already set");
        }
        this.argumentArray = objArr;
    }

    public void setCallerData(java.lang.StackTraceElement[] stackTraceElementArr) {
        this.callerDataArray = stackTraceElementArr;
    }

    public void setLevel(ch.qos.logback.classic.Level level2) {
        if (this.level != null) {
            throw new java.lang.IllegalStateException("The level has been already set for this event.");
        }
        this.level = level2;
    }

    public void setLoggerContextRemoteView(ch.qos.logback.classic.spi.LoggerContextVO loggerContextVO2) {
        this.loggerContextVO = loggerContextVO2;
    }

    public void setLoggerName(java.lang.String str) {
        this.loggerName = str;
    }

    public void setMDCPropertyMap(java.util.Map<java.lang.String, java.lang.String> map) {
        if (this.mdcPropertyMap != null) {
            throw new java.lang.IllegalStateException("The MDCPropertyMap has been already set for this event.");
        }
        this.mdcPropertyMap = map;
    }

    public void setMarker(org.slf4j.Marker marker2) {
        if (this.marker != null) {
            throw new java.lang.IllegalStateException("The marker has been already set for this event.");
        }
        this.marker = marker2;
    }

    public void setMessage(java.lang.String str) {
        if (this.message != null) {
            throw new java.lang.IllegalStateException("The message for this event has been set already.");
        }
        this.message = str;
    }

    public void setThreadName(java.lang.String str) throws java.lang.IllegalStateException {
        if (this.threadName != null) {
            throw new java.lang.IllegalStateException("threadName has been already set");
        }
        this.threadName = str;
    }

    public void setThrowableProxy(ch.qos.logback.classic.spi.ThrowableProxy throwableProxy2) {
        if (this.throwableProxy != null) {
            throw new java.lang.IllegalStateException("ThrowableProxy has been already set.");
        }
        this.throwableProxy = throwableProxy2;
    }

    public void setTimeStamp(long j) {
        this.timeStamp = j;
    }

    public java.lang.String toString() {
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        sb.append('[');
        sb.append(this.level).append("] ");
        sb.append(getFormattedMessage());
        return sb.toString();
    }
}
