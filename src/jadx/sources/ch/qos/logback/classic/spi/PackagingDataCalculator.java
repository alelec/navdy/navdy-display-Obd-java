package ch.qos.logback.classic.spi;

public class PackagingDataCalculator {
    static final ch.qos.logback.classic.spi.StackTraceElementProxy[] STEP_ARRAY_TEMPLATE = new ch.qos.logback.classic.spi.StackTraceElementProxy[0];
    java.util.HashMap<java.lang.String, ch.qos.logback.classic.spi.ClassPackagingData> cache = new java.util.HashMap<>();

    private java.lang.Class bestEffortLoadClass(java.lang.ClassLoader classLoader, java.lang.String str) {
        java.lang.Class loadClass = loadClass(classLoader, str);
        if (loadClass != null) {
            return loadClass;
        }
        java.lang.ClassLoader contextClassLoader = java.lang.Thread.currentThread().getContextClassLoader();
        if (contextClassLoader != classLoader) {
            loadClass = loadClass(contextClassLoader, str);
        }
        if (loadClass != null) {
            return loadClass;
        }
        try {
            return java.lang.Class.forName(str);
        } catch (java.lang.ClassNotFoundException e) {
            return null;
        } catch (java.lang.NoClassDefFoundError e2) {
            return null;
        } catch (java.lang.Exception e3) {
            e3.printStackTrace();
            return null;
        }
    }

    private ch.qos.logback.classic.spi.ClassPackagingData computeBySTEP(ch.qos.logback.classic.spi.StackTraceElementProxy stackTraceElementProxy, java.lang.ClassLoader classLoader) {
        java.lang.String className = stackTraceElementProxy.ste.getClassName();
        ch.qos.logback.classic.spi.ClassPackagingData classPackagingData = (ch.qos.logback.classic.spi.ClassPackagingData) this.cache.get(className);
        if (classPackagingData != null) {
            return classPackagingData;
        }
        java.lang.Class bestEffortLoadClass = bestEffortLoadClass(classLoader, className);
        ch.qos.logback.classic.spi.ClassPackagingData classPackagingData2 = new ch.qos.logback.classic.spi.ClassPackagingData(getCodeLocation(bestEffortLoadClass), getImplementationVersion(bestEffortLoadClass), false);
        this.cache.put(className, classPackagingData2);
        return classPackagingData2;
    }

    private java.lang.String getCodeLocation(java.lang.String str, char c) {
        int lastIndexOf = str.lastIndexOf(c);
        if (isFolder(lastIndexOf, str)) {
            return str.substring(str.lastIndexOf(c, lastIndexOf - 1) + 1);
        }
        if (lastIndexOf > 0) {
            return str.substring(lastIndexOf + 1);
        }
        return null;
    }

    private boolean isFolder(int i, java.lang.String str) {
        return i != -1 && i + 1 == str.length();
    }

    private java.lang.Class loadClass(java.lang.ClassLoader classLoader, java.lang.String str) {
        java.lang.Class cls = null;
        if (classLoader == null) {
            return cls;
        }
        try {
            return classLoader.loadClass(str);
        } catch (java.lang.ClassNotFoundException | java.lang.NoClassDefFoundError e) {
            return cls;
        } catch (java.lang.Exception e2) {
            e2.printStackTrace();
            return cls;
        }
    }

    public void calculate(ch.qos.logback.classic.spi.IThrowableProxy iThrowableProxy) {
        while (iThrowableProxy != null) {
            populateFrames(iThrowableProxy.getStackTraceElementProxyArray());
            ch.qos.logback.classic.spi.IThrowableProxy[] suppressed = iThrowableProxy.getSuppressed();
            if (suppressed != null) {
                for (ch.qos.logback.classic.spi.IThrowableProxy stackTraceElementProxyArray : suppressed) {
                    populateFrames(stackTraceElementProxyArray.getStackTraceElementProxyArray());
                }
            }
            iThrowableProxy = iThrowableProxy.getCause();
        }
    }

    /* access modifiers changed from: 0000 */
    public java.lang.String getCodeLocation(java.lang.Class cls) {
        if (cls != null) {
            try {
                java.security.CodeSource codeSource = cls.getProtectionDomain().getCodeSource();
                if (codeSource != null) {
                    java.net.URL location = codeSource.getLocation();
                    if (location != null) {
                        java.lang.String url = location.toString();
                        java.lang.String codeLocation = getCodeLocation(url, '/');
                        return codeLocation != null ? codeLocation : getCodeLocation(url, ch.qos.logback.core.CoreConstants.ESCAPE_CHAR);
                    }
                }
            } catch (java.lang.Exception e) {
            }
        }
        return "na";
    }

    /* access modifiers changed from: 0000 */
    public java.lang.String getImplementationVersion(java.lang.Class cls) {
        if (cls == null) {
            return "na";
        }
        java.lang.Package packageR = cls.getPackage();
        if (packageR == null) {
            return "na";
        }
        java.lang.String implementationVersion = packageR.getImplementationVersion();
        return implementationVersion == null ? "na" : implementationVersion;
    }

    /* access modifiers changed from: 0000 */
    public void populateFrames(ch.qos.logback.classic.spi.StackTraceElementProxy[] stackTraceElementProxyArr) {
        int findNumberOfCommonFrames = ch.qos.logback.classic.spi.STEUtil.findNumberOfCommonFrames(new java.lang.Throwable("local stack reference").getStackTrace(), stackTraceElementProxyArr);
        int length = stackTraceElementProxyArr.length - findNumberOfCommonFrames;
        for (int i = 0; i < findNumberOfCommonFrames; i++) {
            ch.qos.logback.classic.spi.StackTraceElementProxy stackTraceElementProxy = stackTraceElementProxyArr[length + i];
            stackTraceElementProxy.setClassPackagingData(computeBySTEP(stackTraceElementProxy, null));
        }
        populateUncommonFrames(findNumberOfCommonFrames, stackTraceElementProxyArr, null);
    }

    /* access modifiers changed from: 0000 */
    public void populateUncommonFrames(int i, ch.qos.logback.classic.spi.StackTraceElementProxy[] stackTraceElementProxyArr, java.lang.ClassLoader classLoader) {
        int length = stackTraceElementProxyArr.length - i;
        for (int i2 = 0; i2 < length; i2++) {
            ch.qos.logback.classic.spi.StackTraceElementProxy stackTraceElementProxy = stackTraceElementProxyArr[i2];
            stackTraceElementProxy.setClassPackagingData(computeBySTEP(stackTraceElementProxy, classLoader));
        }
    }
}
