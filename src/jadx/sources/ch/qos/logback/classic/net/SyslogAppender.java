package ch.qos.logback.classic.net;

public class SyslogAppender extends ch.qos.logback.core.net.SyslogAppenderBase<ch.qos.logback.classic.spi.ILoggingEvent> {
    public static final java.lang.String DEFAULT_STACKTRACE_PATTERN = "\t";
    public static final java.lang.String DEFAULT_SUFFIX_PATTERN = "[%thread] %logger %msg";
    ch.qos.logback.classic.PatternLayout stackTraceLayout = new ch.qos.logback.classic.PatternLayout();
    java.lang.String stackTracePattern = DEFAULT_STACKTRACE_PATTERN;
    boolean throwableExcluded = false;

    private void handleThrowableFirstLine(java.io.OutputStream outputStream, ch.qos.logback.classic.spi.IThrowableProxy iThrowableProxy, java.lang.String str, boolean z) throws java.io.IOException {
        java.lang.StringBuilder append = new java.lang.StringBuilder().append(str);
        if (!z) {
            append.append(ch.qos.logback.core.CoreConstants.CAUSED_BY);
        }
        append.append(iThrowableProxy.getClassName()).append(": ").append(iThrowableProxy.getMessage());
        outputStream.write(append.toString().getBytes());
        outputStream.flush();
    }

    private void setupStackTraceLayout() {
        this.stackTraceLayout.getInstanceConverterMap().put("syslogStart", ch.qos.logback.classic.pattern.SyslogStartConverter.class.getName());
        this.stackTraceLayout.setPattern(getPrefixPattern() + this.stackTracePattern);
        this.stackTraceLayout.setContext(getContext());
        this.stackTraceLayout.start();
    }

    public ch.qos.logback.core.Layout<ch.qos.logback.classic.spi.ILoggingEvent> buildLayout() {
        ch.qos.logback.classic.PatternLayout patternLayout = new ch.qos.logback.classic.PatternLayout();
        patternLayout.getInstanceConverterMap().put("syslogStart", ch.qos.logback.classic.pattern.SyslogStartConverter.class.getName());
        if (this.suffixPattern == null) {
            this.suffixPattern = DEFAULT_SUFFIX_PATTERN;
        }
        patternLayout.setPattern(getPrefixPattern() + this.suffixPattern);
        patternLayout.setContext(getContext());
        patternLayout.start();
        return patternLayout;
    }

    /* access modifiers changed from: 0000 */
    public java.lang.String getPrefixPattern() {
        return "%syslogStart{" + getFacility() + "}%nopex{}";
    }

    public int getSeverityForEvent(java.lang.Object obj) {
        return ch.qos.logback.classic.util.LevelToSyslogSeverity.convert((ch.qos.logback.classic.spi.ILoggingEvent) obj);
    }

    public java.lang.String getStackTracePattern() {
        return this.stackTracePattern;
    }

    public boolean isThrowableExcluded() {
        return this.throwableExcluded;
    }

    /* access modifiers changed from: protected */
    public void postProcess(java.lang.Object obj, java.io.OutputStream outputStream) {
        if (!this.throwableExcluded) {
            ch.qos.logback.classic.spi.ILoggingEvent iLoggingEvent = (ch.qos.logback.classic.spi.ILoggingEvent) obj;
            ch.qos.logback.classic.spi.IThrowableProxy throwableProxy = iLoggingEvent.getThrowableProxy();
            if (throwableProxy != null) {
                java.lang.String doLayout = this.stackTraceLayout.doLayout(iLoggingEvent);
                boolean z = true;
                while (throwableProxy != null) {
                    ch.qos.logback.classic.spi.StackTraceElementProxy[] stackTraceElementProxyArray = throwableProxy.getStackTraceElementProxyArray();
                    try {
                        handleThrowableFirstLine(outputStream, throwableProxy, doLayout, z);
                        for (ch.qos.logback.classic.spi.StackTraceElementProxy stackTraceElementProxy : stackTraceElementProxyArray) {
                            java.lang.StringBuilder sb = new java.lang.StringBuilder();
                            sb.append(doLayout).append(stackTraceElementProxy);
                            outputStream.write(sb.toString().getBytes());
                            outputStream.flush();
                        }
                        throwableProxy = throwableProxy.getCause();
                        z = false;
                    } catch (java.io.IOException e) {
                        return;
                    }
                }
            }
        }
    }

    public void setStackTracePattern(java.lang.String str) {
        this.stackTracePattern = str;
    }

    public void setThrowableExcluded(boolean z) {
        this.throwableExcluded = z;
    }

    /* access modifiers changed from: 0000 */
    public boolean stackTraceHeaderLine(java.lang.StringBuilder sb, boolean z) {
        return false;
    }

    public void start() {
        super.start();
        setupStackTraceLayout();
    }
}
