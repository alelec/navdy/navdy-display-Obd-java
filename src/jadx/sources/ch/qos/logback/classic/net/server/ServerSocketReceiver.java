package ch.qos.logback.classic.net.server;

public class ServerSocketReceiver extends ch.qos.logback.classic.net.ReceiverBase {
    public static final int DEFAULT_BACKLOG = 50;
    private java.lang.String address;
    private int backlog = 50;
    private int port = ch.qos.logback.core.net.AbstractSocketAppender.DEFAULT_PORT;
    private ch.qos.logback.core.net.server.ServerRunner runner;
    private java.net.ServerSocket serverSocket;

    /* access modifiers changed from: protected */
    public ch.qos.logback.core.net.server.ServerListener<ch.qos.logback.classic.net.server.RemoteAppenderClient> createServerListener(java.net.ServerSocket serverSocket2) {
        return new ch.qos.logback.classic.net.server.RemoteAppenderServerListener(serverSocket2);
    }

    /* access modifiers changed from: protected */
    public ch.qos.logback.core.net.server.ServerRunner createServerRunner(ch.qos.logback.core.net.server.ServerListener<ch.qos.logback.classic.net.server.RemoteAppenderClient> serverListener, java.util.concurrent.Executor executor) {
        return new ch.qos.logback.classic.net.server.RemoteAppenderServerRunner(serverListener, executor);
    }

    public java.lang.String getAddress() {
        return this.address;
    }

    public int getBacklog() {
        return this.backlog;
    }

    /* access modifiers changed from: protected */
    public java.net.InetAddress getInetAddress() throws java.net.UnknownHostException {
        if (getAddress() == null) {
            return null;
        }
        return java.net.InetAddress.getByName(getAddress());
    }

    public int getPort() {
        return this.port;
    }

    /* access modifiers changed from: protected */
    public java.lang.Runnable getRunnableTask() {
        return this.runner;
    }

    /* access modifiers changed from: protected */
    public javax.net.ServerSocketFactory getServerSocketFactory() throws java.lang.Exception {
        return javax.net.ServerSocketFactory.getDefault();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        try {
            if (this.runner != null) {
                this.runner.stop();
            }
        } catch (java.io.IOException e) {
            addError("server shutdown error: " + e, e);
        }
    }

    public void setAddress(java.lang.String str) {
        this.address = str;
    }

    public void setBacklog(int i) {
        this.backlog = i;
    }

    public void setPort(int i) {
        this.port = i;
    }

    /* access modifiers changed from: protected */
    public boolean shouldStart() {
        try {
            this.runner = createServerRunner(createServerListener(getServerSocketFactory().createServerSocket(getPort(), getBacklog(), getInetAddress())), getContext().getExecutorService());
            this.runner.setContext(getContext());
            return true;
        } catch (java.lang.Exception e) {
            addError("server startup error: " + e, e);
            ch.qos.logback.core.util.CloseUtil.closeQuietly(this.serverSocket);
            return false;
        }
    }
}
