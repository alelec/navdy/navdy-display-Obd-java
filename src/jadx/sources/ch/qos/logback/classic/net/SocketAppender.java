package ch.qos.logback.classic.net;

public class SocketAppender extends ch.qos.logback.core.net.AbstractSocketAppender<ch.qos.logback.classic.spi.ILoggingEvent> {
    private static final ch.qos.logback.core.spi.PreSerializationTransformer<ch.qos.logback.classic.spi.ILoggingEvent> pst = new ch.qos.logback.classic.net.LoggingEventPreSerializationTransformer();
    private boolean includeCallerData = false;

    public SocketAppender() {
    }

    @java.lang.Deprecated
    public SocketAppender(java.lang.String str, int i) {
        super(str, i);
    }

    @java.lang.Deprecated
    public SocketAppender(java.net.InetAddress inetAddress, int i) {
        super(inetAddress.getHostAddress(), i);
    }

    public ch.qos.logback.core.spi.PreSerializationTransformer<ch.qos.logback.classic.spi.ILoggingEvent> getPST() {
        return pst;
    }

    /* access modifiers changed from: protected */
    public void postProcessEvent(ch.qos.logback.classic.spi.ILoggingEvent iLoggingEvent) {
        if (this.includeCallerData) {
            iLoggingEvent.getCallerData();
        }
    }

    public void setIncludeCallerData(boolean z) {
        this.includeCallerData = z;
    }
}
