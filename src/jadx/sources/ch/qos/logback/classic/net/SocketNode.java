package ch.qos.logback.classic.net;

public class SocketNode implements java.lang.Runnable {
    boolean closed = false;
    ch.qos.logback.classic.LoggerContext context;
    ch.qos.logback.classic.Logger logger;
    java.io.ObjectInputStream ois;
    java.net.SocketAddress remoteSocketAddress;
    java.net.Socket socket;
    ch.qos.logback.classic.net.SimpleSocketServer socketServer;

    public SocketNode(ch.qos.logback.classic.net.SimpleSocketServer simpleSocketServer, java.net.Socket socket2, ch.qos.logback.classic.LoggerContext loggerContext) {
        this.socketServer = simpleSocketServer;
        this.socket = socket2;
        this.remoteSocketAddress = socket2.getRemoteSocketAddress();
        this.context = loggerContext;
        this.logger = loggerContext.getLogger(ch.qos.logback.classic.net.SocketNode.class);
    }

    /* access modifiers changed from: 0000 */
    public void close() {
        if (!this.closed) {
            this.closed = true;
            if (this.ois != null) {
                try {
                    this.ois.close();
                } catch (java.io.IOException e) {
                    this.logger.warn("Could not close connection.", (java.lang.Throwable) e);
                } finally {
                    this.ois = null;
                }
            }
        }
    }

    public void run() {
        try {
            this.ois = new java.io.ObjectInputStream(new java.io.BufferedInputStream(this.socket.getInputStream()));
        } catch (java.lang.Exception e) {
            this.logger.error("Could not open ObjectInputStream to " + this.socket, (java.lang.Throwable) e);
            this.closed = true;
        }
        while (!this.closed) {
            try {
                ch.qos.logback.classic.spi.ILoggingEvent iLoggingEvent = (ch.qos.logback.classic.spi.ILoggingEvent) this.ois.readObject();
                ch.qos.logback.classic.Logger logger2 = this.context.getLogger(iLoggingEvent.getLoggerName());
                if (logger2.isEnabledFor(iLoggingEvent.getLevel())) {
                    logger2.callAppenders(iLoggingEvent);
                }
            } catch (java.io.EOFException e2) {
                this.logger.info("Caught java.io.EOFException closing connection.");
            } catch (java.net.SocketException e3) {
                this.logger.info("Caught java.net.SocketException closing connection.");
            } catch (java.io.IOException e4) {
                this.logger.info("Caught java.io.IOException: " + e4);
                this.logger.info("Closing connection.");
            } catch (java.lang.Exception e5) {
                this.logger.error("Unexpected exception. Closing connection.", (java.lang.Throwable) e5);
            }
        }
        this.socketServer.socketNodeClosing(this);
        close();
    }

    public java.lang.String toString() {
        return getClass().getName() + this.remoteSocketAddress.toString();
    }
}
