package ch.qos.logback.classic.net;

public class SMTPAppender extends ch.qos.logback.core.net.SMTPAppenderBase<ch.qos.logback.classic.spi.ILoggingEvent> {
    static final java.lang.String DEFAULT_SUBJECT_PATTERN = "%logger{20} - %m";
    private boolean includeCallerData = false;

    public SMTPAppender() {
    }

    public SMTPAppender(ch.qos.logback.core.boolex.EventEvaluator<ch.qos.logback.classic.spi.ILoggingEvent> eventEvaluator) {
        this.eventEvaluator = eventEvaluator;
    }

    /* access modifiers changed from: protected */
    public boolean eventMarksEndOfLife(ch.qos.logback.classic.spi.ILoggingEvent iLoggingEvent) {
        org.slf4j.Marker marker = iLoggingEvent.getMarker();
        if (marker == null) {
            return false;
        }
        return marker.contains(ch.qos.logback.classic.ClassicConstants.FINALIZE_SESSION_MARKER);
    }

    /* access modifiers changed from: protected */
    public void fillBuffer(ch.qos.logback.core.helpers.CyclicBuffer<ch.qos.logback.classic.spi.ILoggingEvent> cyclicBuffer, java.lang.StringBuffer stringBuffer) {
        int length = cyclicBuffer.length();
        for (int i = 0; i < length; i++) {
            stringBuffer.append(this.layout.doLayout((ch.qos.logback.classic.spi.ILoggingEvent) cyclicBuffer.get()));
        }
    }

    public boolean isIncludeCallerData() {
        return this.includeCallerData;
    }

    /* access modifiers changed from: protected */
    public ch.qos.logback.classic.PatternLayout makeNewToPatternLayout(java.lang.String str) {
        ch.qos.logback.classic.PatternLayout patternLayout = new ch.qos.logback.classic.PatternLayout();
        patternLayout.setPattern(str + "%nopex");
        return patternLayout;
    }

    /* access modifiers changed from: protected */
    public ch.qos.logback.core.Layout<ch.qos.logback.classic.spi.ILoggingEvent> makeSubjectLayout(java.lang.String str) {
        if (str == null) {
            str = DEFAULT_SUBJECT_PATTERN;
        }
        ch.qos.logback.classic.PatternLayout patternLayout = new ch.qos.logback.classic.PatternLayout();
        patternLayout.setContext(getContext());
        patternLayout.setPattern(str);
        patternLayout.setPostCompileProcessor(null);
        patternLayout.start();
        return patternLayout;
    }

    public void setIncludeCallerData(boolean z) {
        this.includeCallerData = z;
    }

    public void start() {
        if (this.eventEvaluator == null) {
            ch.qos.logback.classic.boolex.OnErrorEvaluator onErrorEvaluator = new ch.qos.logback.classic.boolex.OnErrorEvaluator();
            onErrorEvaluator.setContext(getContext());
            onErrorEvaluator.setName("onError");
            onErrorEvaluator.start();
            this.eventEvaluator = onErrorEvaluator;
        }
        super.start();
    }

    /* access modifiers changed from: protected */
    public void subAppend(ch.qos.logback.core.helpers.CyclicBuffer<ch.qos.logback.classic.spi.ILoggingEvent> cyclicBuffer, ch.qos.logback.classic.spi.ILoggingEvent iLoggingEvent) {
        if (this.includeCallerData) {
            iLoggingEvent.getCallerData();
        }
        iLoggingEvent.prepareForDeferredProcessing();
        cyclicBuffer.add(iLoggingEvent);
    }
}
