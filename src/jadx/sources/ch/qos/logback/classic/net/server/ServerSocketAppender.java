package ch.qos.logback.classic.net.server;

public class ServerSocketAppender extends ch.qos.logback.core.net.server.AbstractServerSocketAppender<ch.qos.logback.classic.spi.ILoggingEvent> {
    private static final ch.qos.logback.core.spi.PreSerializationTransformer<ch.qos.logback.classic.spi.ILoggingEvent> pst = new ch.qos.logback.classic.net.LoggingEventPreSerializationTransformer();
    private boolean includeCallerData;

    /* access modifiers changed from: protected */
    public ch.qos.logback.core.spi.PreSerializationTransformer<ch.qos.logback.classic.spi.ILoggingEvent> getPST() {
        return pst;
    }

    public boolean isIncludeCallerData() {
        return this.includeCallerData;
    }

    /* access modifiers changed from: protected */
    public void postProcessEvent(ch.qos.logback.classic.spi.ILoggingEvent iLoggingEvent) {
        if (isIncludeCallerData()) {
            iLoggingEvent.getCallerData();
        }
    }

    public void setIncludeCallerData(boolean z) {
        this.includeCallerData = z;
    }
}
