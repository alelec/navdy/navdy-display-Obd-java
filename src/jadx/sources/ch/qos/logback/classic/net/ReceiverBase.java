package ch.qos.logback.classic.net;

public abstract class ReceiverBase extends ch.qos.logback.core.spi.ContextAwareBase implements ch.qos.logback.core.spi.LifeCycle {
    private boolean started;

    /* access modifiers changed from: protected */
    public abstract java.lang.Runnable getRunnableTask();

    public final boolean isStarted() {
        return this.started;
    }

    /* access modifiers changed from: protected */
    public abstract void onStop();

    /* access modifiers changed from: protected */
    public abstract boolean shouldStart();

    public final void start() {
        if (!isStarted()) {
            if (getContext() == null) {
                throw new java.lang.IllegalStateException("context not set");
            } else if (shouldStart()) {
                getContext().getExecutorService().execute(getRunnableTask());
                this.started = true;
            }
        }
    }

    public final void stop() {
        if (isStarted()) {
            try {
                onStop();
            } catch (java.lang.RuntimeException e) {
                addError("on stop: " + e, e);
            }
            this.started = false;
        }
    }
}
