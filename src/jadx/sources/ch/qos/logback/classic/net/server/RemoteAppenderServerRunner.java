package ch.qos.logback.classic.net.server;

class RemoteAppenderServerRunner extends ch.qos.logback.core.net.server.ConcurrentServerRunner<ch.qos.logback.classic.net.server.RemoteAppenderClient> {
    public RemoteAppenderServerRunner(ch.qos.logback.core.net.server.ServerListener<ch.qos.logback.classic.net.server.RemoteAppenderClient> serverListener, java.util.concurrent.Executor executor) {
        super(serverListener, executor);
    }

    /* access modifiers changed from: protected */
    public boolean configureClient(ch.qos.logback.classic.net.server.RemoteAppenderClient remoteAppenderClient) {
        remoteAppenderClient.setLoggerContext((ch.qos.logback.classic.LoggerContext) getContext());
        return true;
    }
}
