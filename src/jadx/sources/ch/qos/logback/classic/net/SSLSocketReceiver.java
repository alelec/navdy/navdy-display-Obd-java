package ch.qos.logback.classic.net;

public class SSLSocketReceiver extends ch.qos.logback.classic.net.SocketReceiver implements ch.qos.logback.core.net.ssl.SSLComponent {
    private javax.net.SocketFactory socketFactory;
    private ch.qos.logback.core.net.ssl.SSLConfiguration ssl;

    /* access modifiers changed from: protected */
    public javax.net.SocketFactory getSocketFactory() {
        return this.socketFactory;
    }

    public ch.qos.logback.core.net.ssl.SSLConfiguration getSsl() {
        if (this.ssl == null) {
            this.ssl = new ch.qos.logback.core.net.ssl.SSLConfiguration();
        }
        return this.ssl;
    }

    public void setSsl(ch.qos.logback.core.net.ssl.SSLConfiguration sSLConfiguration) {
        this.ssl = sSLConfiguration;
    }

    /* access modifiers changed from: protected */
    public boolean shouldStart() {
        try {
            javax.net.ssl.SSLContext createContext = getSsl().createContext(this);
            ch.qos.logback.core.net.ssl.SSLParametersConfiguration parameters = getSsl().getParameters();
            parameters.setContext(getContext());
            this.socketFactory = new ch.qos.logback.core.net.ssl.ConfigurableSSLSocketFactory(parameters, createContext.getSocketFactory());
            return super.shouldStart();
        } catch (java.lang.Exception e) {
            addError(e.getMessage(), e);
            return false;
        }
    }
}
