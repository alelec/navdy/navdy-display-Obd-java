package ch.qos.logback.classic.net;

public class SimpleSSLSocketServer extends ch.qos.logback.classic.net.SimpleSocketServer {
    private final javax.net.ServerSocketFactory socketFactory;

    public SimpleSSLSocketServer(ch.qos.logback.classic.LoggerContext loggerContext, int i) throws java.security.NoSuchAlgorithmException {
        this(loggerContext, i, javax.net.ssl.SSLContext.getDefault());
    }

    public SimpleSSLSocketServer(ch.qos.logback.classic.LoggerContext loggerContext, int i, javax.net.ssl.SSLContext sSLContext) {
        super(loggerContext, i);
        if (sSLContext == null) {
            throw new java.lang.NullPointerException("SSL context required");
        }
        ch.qos.logback.core.net.ssl.SSLParametersConfiguration sSLParametersConfiguration = new ch.qos.logback.core.net.ssl.SSLParametersConfiguration();
        sSLParametersConfiguration.setContext(loggerContext);
        this.socketFactory = new ch.qos.logback.core.net.ssl.ConfigurableSSLServerSocketFactory(sSLParametersConfiguration, sSLContext.getServerSocketFactory());
    }

    public static void main(java.lang.String[] strArr) throws java.lang.Exception {
        doMain(ch.qos.logback.classic.net.SimpleSSLSocketServer.class, strArr);
    }

    /* access modifiers changed from: protected */
    public javax.net.ServerSocketFactory getServerSocketFactory() {
        return this.socketFactory;
    }
}
