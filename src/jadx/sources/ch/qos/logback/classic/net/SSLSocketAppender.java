package ch.qos.logback.classic.net;

public class SSLSocketAppender extends ch.qos.logback.core.net.AbstractSSLSocketAppender<ch.qos.logback.classic.spi.ILoggingEvent> {
    private boolean includeCallerData;
    private final ch.qos.logback.core.spi.PreSerializationTransformer<ch.qos.logback.classic.spi.ILoggingEvent> pst = new ch.qos.logback.classic.net.LoggingEventPreSerializationTransformer();

    public SSLSocketAppender() {
    }

    @java.lang.Deprecated
    public SSLSocketAppender(java.lang.String str, int i) {
        super(str, i);
    }

    @java.lang.Deprecated
    public SSLSocketAppender(java.net.InetAddress inetAddress, int i) {
        super(inetAddress.getHostAddress(), i);
    }

    public ch.qos.logback.core.spi.PreSerializationTransformer<ch.qos.logback.classic.spi.ILoggingEvent> getPST() {
        return this.pst;
    }

    /* access modifiers changed from: protected */
    public void postProcessEvent(ch.qos.logback.classic.spi.ILoggingEvent iLoggingEvent) {
        if (this.includeCallerData) {
            iLoggingEvent.getCallerData();
        }
    }

    public void setIncludeCallerData(boolean z) {
        this.includeCallerData = z;
    }
}
