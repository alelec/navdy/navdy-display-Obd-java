package ch.qos.logback.classic.net.server;

class RemoteAppenderServerListener extends ch.qos.logback.core.net.server.ServerSocketListener<ch.qos.logback.classic.net.server.RemoteAppenderClient> {
    public RemoteAppenderServerListener(java.net.ServerSocket serverSocket) {
        super(serverSocket);
    }

    /* access modifiers changed from: protected */
    public ch.qos.logback.classic.net.server.RemoteAppenderClient createClient(java.lang.String str, java.net.Socket socket) throws java.io.IOException {
        return new ch.qos.logback.classic.net.server.RemoteAppenderStreamClient(str, socket);
    }
}
