package ch.qos.logback.classic.net;

public class SocketReceiver extends ch.qos.logback.classic.net.ReceiverBase implements java.lang.Runnable, ch.qos.logback.core.net.SocketConnector.ExceptionHandler {
    private static final int DEFAULT_ACCEPT_CONNECTION_DELAY = 5000;
    private int acceptConnectionTimeout = 5000;
    private java.net.InetAddress address;
    private java.util.concurrent.Future<java.net.Socket> connectorTask;
    private int port;
    private java.lang.String receiverId;
    private int reconnectionDelay;
    private java.lang.String remoteHost;
    private volatile java.net.Socket socket;

    private java.util.concurrent.Future<java.net.Socket> activateConnector(ch.qos.logback.core.net.SocketConnector socketConnector) {
        try {
            return getContext().getExecutorService().submit(socketConnector);
        } catch (java.util.concurrent.RejectedExecutionException e) {
            return null;
        }
    }

    private ch.qos.logback.core.net.SocketConnector createConnector(java.net.InetAddress inetAddress, int i, int i2, int i3) {
        ch.qos.logback.core.net.SocketConnector newConnector = newConnector(inetAddress, i, i2, i3);
        newConnector.setExceptionHandler(this);
        newConnector.setSocketFactory(getSocketFactory());
        return newConnector;
    }

    private void dispatchEvents(ch.qos.logback.classic.LoggerContext loggerContext) {
        try {
            this.socket.setSoTimeout(this.acceptConnectionTimeout);
            java.io.ObjectInputStream objectInputStream = new java.io.ObjectInputStream(this.socket.getInputStream());
            this.socket.setSoTimeout(0);
            addInfo(this.receiverId + "connection established");
            while (true) {
                ch.qos.logback.classic.spi.ILoggingEvent iLoggingEvent = (ch.qos.logback.classic.spi.ILoggingEvent) objectInputStream.readObject();
                ch.qos.logback.classic.Logger logger = loggerContext.getLogger(iLoggingEvent.getLoggerName());
                if (logger.isEnabledFor(iLoggingEvent.getLevel())) {
                    logger.callAppenders(iLoggingEvent);
                }
            }
        } catch (java.io.EOFException e) {
            addInfo(this.receiverId + "end-of-stream detected");
            ch.qos.logback.core.util.CloseUtil.closeQuietly(this.socket);
            this.socket = null;
            addInfo(this.receiverId + "connection closed");
        } catch (java.io.IOException e2) {
            addInfo(this.receiverId + "connection failed: " + e2);
            ch.qos.logback.core.util.CloseUtil.closeQuietly(this.socket);
            this.socket = null;
            addInfo(this.receiverId + "connection closed");
        } catch (java.lang.ClassNotFoundException e3) {
            addInfo(this.receiverId + "unknown event class: " + e3);
            ch.qos.logback.core.util.CloseUtil.closeQuietly(this.socket);
            this.socket = null;
            addInfo(this.receiverId + "connection closed");
        } catch (Throwable th) {
            ch.qos.logback.core.util.CloseUtil.closeQuietly(this.socket);
            this.socket = null;
            addInfo(this.receiverId + "connection closed");
            throw th;
        }
    }

    private java.net.Socket waitForConnectorToReturnASocket() throws java.lang.InterruptedException {
        try {
            java.net.Socket socket2 = (java.net.Socket) this.connectorTask.get();
            this.connectorTask = null;
            return socket2;
        } catch (java.util.concurrent.ExecutionException e) {
            return null;
        }
    }

    public void connectionFailed(ch.qos.logback.core.net.SocketConnector socketConnector, java.lang.Exception exc) {
        if (exc instanceof java.lang.InterruptedException) {
            addInfo("connector interrupted");
        } else if (exc instanceof java.net.ConnectException) {
            addInfo(this.receiverId + "connection refused");
        } else {
            addInfo(this.receiverId + exc);
        }
    }

    /* access modifiers changed from: protected */
    public java.lang.Runnable getRunnableTask() {
        return this;
    }

    /* access modifiers changed from: protected */
    public javax.net.SocketFactory getSocketFactory() {
        return javax.net.SocketFactory.getDefault();
    }

    /* access modifiers changed from: protected */
    public ch.qos.logback.core.net.SocketConnector newConnector(java.net.InetAddress inetAddress, int i, int i2, int i3) {
        return new ch.qos.logback.core.net.DefaultSocketConnector(inetAddress, i, (long) i2, (long) i3);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        if (this.socket != null) {
            ch.qos.logback.core.util.CloseUtil.closeQuietly(this.socket);
        }
    }

    public void run() {
        try {
            ch.qos.logback.classic.LoggerContext loggerContext = (ch.qos.logback.classic.LoggerContext) getContext();
            while (!java.lang.Thread.currentThread().isInterrupted()) {
                this.connectorTask = activateConnector(createConnector(this.address, this.port, 0, this.reconnectionDelay));
                if (this.connectorTask != null) {
                    this.socket = waitForConnectorToReturnASocket();
                    if (this.socket == null) {
                        break;
                    }
                    dispatchEvents(loggerContext);
                } else {
                    break;
                }
            }
        } catch (java.lang.InterruptedException e) {
        }
        addInfo("shutting down");
    }

    public void setAcceptConnectionTimeout(int i) {
        this.acceptConnectionTimeout = i;
    }

    public void setPort(int i) {
        this.port = i;
    }

    public void setReconnectionDelay(int i) {
        this.reconnectionDelay = i;
    }

    public void setRemoteHost(java.lang.String str) {
        this.remoteHost = str;
    }

    /* access modifiers changed from: protected */
    public boolean shouldStart() {
        int i;
        if (this.port == 0) {
            addError("No port was configured for receiver. For more information, please visit http://logback.qos.ch/codes.html#receiver_no_port");
            i = 1;
        } else {
            i = 0;
        }
        if (this.remoteHost == null) {
            i++;
            addError("No host name or address was configured for receiver. For more information, please visit http://logback.qos.ch/codes.html#receiver_no_host");
        }
        if (this.reconnectionDelay == 0) {
            this.reconnectionDelay = 30000;
        }
        if (i == 0) {
            try {
                this.address = java.net.InetAddress.getByName(this.remoteHost);
            } catch (java.net.UnknownHostException e) {
                addError("unknown host: " + this.remoteHost);
                i++;
            }
        }
        if (i == 0) {
            this.receiverId = "receiver " + this.remoteHost + ":" + this.port + ": ";
        }
        return i == 0;
    }
}
