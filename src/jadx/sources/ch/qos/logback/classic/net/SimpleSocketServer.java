package ch.qos.logback.classic.net;

public class SimpleSocketServer extends java.lang.Thread {
    private boolean closed = false;
    private java.util.concurrent.CountDownLatch latch;
    private final ch.qos.logback.classic.LoggerContext lc;
    org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ch.qos.logback.classic.net.SimpleSocketServer.class);
    private final int port;
    private java.net.ServerSocket serverSocket;
    private java.util.List<ch.qos.logback.classic.net.SocketNode> socketNodeList = new java.util.ArrayList();

    public SimpleSocketServer(ch.qos.logback.classic.LoggerContext loggerContext, int i) {
        this.lc = loggerContext;
        this.port = i;
    }

    public static void configureLC(ch.qos.logback.classic.LoggerContext loggerContext, java.lang.String str) throws ch.qos.logback.core.joran.spi.JoranException {
        ch.qos.logback.classic.joran.JoranConfigurator joranConfigurator = new ch.qos.logback.classic.joran.JoranConfigurator();
        loggerContext.reset();
        joranConfigurator.setContext(loggerContext);
        joranConfigurator.doConfigure(str);
    }

    protected static void doMain(java.lang.Class<? extends ch.qos.logback.classic.net.SimpleSocketServer> cls, java.lang.String[] strArr) throws java.lang.Exception {
        int i;
        if (strArr.length == 2) {
            i = parsePortNumber(strArr[0]);
        } else {
            usage("Wrong number of arguments.");
            i = -1;
        }
        ch.qos.logback.classic.LoggerContext loggerContext = (ch.qos.logback.classic.LoggerContext) org.slf4j.LoggerFactory.getILoggerFactory();
        configureLC(loggerContext, strArr[1]);
        new ch.qos.logback.classic.net.SimpleSocketServer(loggerContext, i).start();
    }

    public static void main(java.lang.String[] strArr) throws java.lang.Exception {
        doMain(ch.qos.logback.classic.net.SimpleSocketServer.class, strArr);
    }

    static int parsePortNumber(java.lang.String str) {
        try {
            return java.lang.Integer.parseInt(str);
        } catch (java.lang.NumberFormatException e) {
            e.printStackTrace();
            usage("Could not interpret port number [" + str + "].");
            return -1;
        }
    }

    static void usage(java.lang.String str) {
        java.lang.System.err.println(str);
        java.lang.System.err.println("Usage: java " + ch.qos.logback.classic.net.SimpleSocketServer.class.getName() + " port configFile");
        java.lang.System.exit(1);
    }

    public void close() {
        this.closed = true;
        if (this.serverSocket != null) {
            try {
                this.serverSocket.close();
            } catch (java.io.IOException e) {
                this.logger.error("Failed to close serverSocket", (java.lang.Throwable) e);
            } finally {
                this.serverSocket = null;
            }
        }
        this.logger.info("closing this server");
        synchronized (this.socketNodeList) {
            for (ch.qos.logback.classic.net.SocketNode close : this.socketNodeList) {
                close.close();
            }
        }
        if (this.socketNodeList.size() != 0) {
            this.logger.warn("Was expecting a 0-sized socketNodeList after server shutdown");
        }
    }

    /* access modifiers changed from: protected */
    public java.lang.String getClientThreadName(java.net.Socket socket) {
        return java.lang.String.format("Logback SocketNode (client: %s)", new java.lang.Object[]{socket.getRemoteSocketAddress()});
    }

    public java.util.concurrent.CountDownLatch getLatch() {
        return this.latch;
    }

    /* access modifiers changed from: protected */
    public javax.net.ServerSocketFactory getServerSocketFactory() {
        return javax.net.ServerSocketFactory.getDefault();
    }

    /* access modifiers changed from: protected */
    public java.lang.String getServerThreadName() {
        return java.lang.String.format("Logback %s (port %d)", new java.lang.Object[]{getClass().getSimpleName(), java.lang.Integer.valueOf(this.port)});
    }

    public boolean isClosed() {
        return this.closed;
    }

    public void run() {
        java.lang.String name = java.lang.Thread.currentThread().getName();
        try {
            java.lang.Thread.currentThread().setName(getServerThreadName());
            this.logger.info("Listening on port " + this.port);
            this.serverSocket = getServerSocketFactory().createServerSocket(this.port);
            while (!this.closed) {
                this.logger.info("Waiting to accept a new client.");
                signalAlmostReadiness();
                java.net.Socket accept = this.serverSocket.accept();
                this.logger.info("Connected to client at " + accept.getInetAddress());
                this.logger.info("Starting new socket node.");
                ch.qos.logback.classic.net.SocketNode socketNode = new ch.qos.logback.classic.net.SocketNode(this, accept, this.lc);
                synchronized (this.socketNodeList) {
                    this.socketNodeList.add(socketNode);
                }
                new java.lang.Thread(socketNode, getClientThreadName(accept)).start();
            }
            java.lang.Thread.currentThread().setName(name);
        } catch (java.lang.Exception e) {
            if (this.closed) {
                this.logger.info("Exception in run method for a closed server. This is normal.");
            } else {
                this.logger.error("Unexpected failure in run method", (java.lang.Throwable) e);
            }
            java.lang.Thread.currentThread().setName(name);
        } catch (Throwable th) {
            java.lang.Thread.currentThread().setName(name);
            throw th;
        }
    }

    /* access modifiers changed from: 0000 */
    public void setLatch(java.util.concurrent.CountDownLatch countDownLatch) {
        this.latch = countDownLatch;
    }

    /* access modifiers changed from: 0000 */
    public void signalAlmostReadiness() {
        if (this.latch != null && this.latch.getCount() != 0) {
            this.latch.countDown();
        }
    }

    public void socketNodeClosing(ch.qos.logback.classic.net.SocketNode socketNode) {
        this.logger.debug("Removing {}", (java.lang.Object) socketNode);
        synchronized (this.socketNodeList) {
            this.socketNodeList.remove(socketNode);
        }
    }
}
