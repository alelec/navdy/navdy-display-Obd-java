package ch.qos.logback.classic.net;

public class LoggingEventPreSerializationTransformer implements ch.qos.logback.core.spi.PreSerializationTransformer<ch.qos.logback.classic.spi.ILoggingEvent> {
    public java.io.Serializable transform(ch.qos.logback.classic.spi.ILoggingEvent iLoggingEvent) {
        if (iLoggingEvent == null) {
            return null;
        }
        if (iLoggingEvent instanceof ch.qos.logback.classic.spi.LoggingEvent) {
            return ch.qos.logback.classic.spi.LoggingEventVO.build(iLoggingEvent);
        }
        if (iLoggingEvent instanceof ch.qos.logback.classic.spi.LoggingEventVO) {
            return (ch.qos.logback.classic.spi.LoggingEventVO) iLoggingEvent;
        }
        throw new java.lang.IllegalArgumentException("Unsupported type " + iLoggingEvent.getClass().getName());
    }
}
