package ch.qos.logback.classic.net.server;

interface RemoteAppenderClient extends ch.qos.logback.core.net.server.Client {
    void setLoggerContext(ch.qos.logback.classic.LoggerContext loggerContext);
}
