package ch.qos.logback.classic.net.server;

class RemoteAppenderStreamClient implements ch.qos.logback.classic.net.server.RemoteAppenderClient {
    private final java.lang.String id;
    private final java.io.InputStream inputStream;
    private ch.qos.logback.classic.LoggerContext lc;
    private ch.qos.logback.classic.Logger logger;
    private final java.net.Socket socket;

    public RemoteAppenderStreamClient(java.lang.String str, java.io.InputStream inputStream2) {
        this.id = str;
        this.socket = null;
        this.inputStream = inputStream2;
    }

    public RemoteAppenderStreamClient(java.lang.String str, java.net.Socket socket2) {
        this.id = str;
        this.socket = socket2;
        this.inputStream = null;
    }

    private java.io.ObjectInputStream createObjectInputStream() throws java.io.IOException {
        return this.inputStream != null ? new java.io.ObjectInputStream(this.inputStream) : new java.io.ObjectInputStream(this.socket.getInputStream());
    }

    public void close() {
        if (this.socket != null) {
            ch.qos.logback.core.util.CloseUtil.closeQuietly(this.socket);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0080  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00bb  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00fc  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0122  */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:21:0x00a1=Splitter:B:21:0x00a1, B:14:0x0062=Splitter:B:14:0x0062, B:28:0x00de=Splitter:B:28:0x00de} */
    public void run() {
        java.io.ObjectInputStream objectInputStream;
        java.lang.Object e;
        java.lang.Object e2;
        this.logger.info(this + ": connected");
        java.io.ObjectInputStream objectInputStream2 = null;
        try {
            objectInputStream = createObjectInputStream();
            while (true) {
                try {
                    ch.qos.logback.classic.spi.ILoggingEvent iLoggingEvent = (ch.qos.logback.classic.spi.ILoggingEvent) objectInputStream.readObject();
                    ch.qos.logback.classic.Logger logger2 = this.lc.getLogger(iLoggingEvent.getLoggerName());
                    if (logger2.isEnabledFor(iLoggingEvent.getLevel())) {
                        logger2.callAppenders(iLoggingEvent);
                    }
                } catch (java.io.EOFException e3) {
                    objectInputStream2 = objectInputStream;
                    if (objectInputStream2 != null) {
                        ch.qos.logback.core.util.CloseUtil.closeQuietly((java.io.Closeable) objectInputStream2);
                    }
                    close();
                    this.logger.info(this + ": connection closed");
                    return;
                } catch (java.io.IOException e4) {
                    e2 = e4;
                    try {
                        this.logger.info(this + ": " + e2);
                        if (objectInputStream != null) {
                        }
                        close();
                        this.logger.info(this + ": connection closed");
                    } catch (Throwable th) {
                        th = th;
                        if (objectInputStream != null) {
                            ch.qos.logback.core.util.CloseUtil.closeQuietly((java.io.Closeable) objectInputStream);
                        }
                        close();
                        this.logger.info(this + ": connection closed");
                        throw th;
                    }
                } catch (java.lang.ClassNotFoundException e5) {
                    this.logger.error(this + ": unknown event class");
                    if (objectInputStream != null) {
                    }
                    close();
                    this.logger.info(this + ": connection closed");
                } catch (java.lang.RuntimeException e6) {
                    e = e6;
                    this.logger.error(this + ": " + e);
                    if (objectInputStream != null) {
                    }
                    close();
                    this.logger.info(this + ": connection closed");
                }
            }
        } catch (java.io.EOFException e7) {
        } catch (java.io.IOException e8) {
            java.lang.Object obj = e8;
            objectInputStream = null;
            e2 = obj;
            this.logger.info(this + ": " + e2);
            if (objectInputStream != null) {
                ch.qos.logback.core.util.CloseUtil.closeQuietly((java.io.Closeable) objectInputStream);
            }
            close();
            this.logger.info(this + ": connection closed");
        } catch (java.lang.ClassNotFoundException e9) {
            objectInputStream = null;
            this.logger.error(this + ": unknown event class");
            if (objectInputStream != null) {
                ch.qos.logback.core.util.CloseUtil.closeQuietly((java.io.Closeable) objectInputStream);
            }
            close();
            this.logger.info(this + ": connection closed");
        } catch (java.lang.RuntimeException e10) {
            java.lang.Object obj2 = e10;
            objectInputStream = null;
            e = obj2;
            this.logger.error(this + ": " + e);
            if (objectInputStream != null) {
                ch.qos.logback.core.util.CloseUtil.closeQuietly((java.io.Closeable) objectInputStream);
            }
            close();
            this.logger.info(this + ": connection closed");
        } catch (Throwable th2) {
            java.lang.Throwable th3 = th2;
            objectInputStream = null;
            th = th3;
            if (objectInputStream != null) {
            }
            close();
            this.logger.info(this + ": connection closed");
            throw th;
        }
    }

    public void setLoggerContext(ch.qos.logback.classic.LoggerContext loggerContext) {
        this.lc = loggerContext;
        this.logger = loggerContext.getLogger(getClass().getPackage().getName());
    }

    public java.lang.String toString() {
        return "client " + this.id;
    }
}
