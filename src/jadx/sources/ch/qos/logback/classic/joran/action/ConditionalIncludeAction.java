package ch.qos.logback.classic.joran.action;

public class ConditionalIncludeAction extends ch.qos.logback.core.joran.action.AbstractIncludeAction {

    class State {
        private java.net.URL url;

        State() {
        }

        /* access modifiers changed from: 0000 */
        public java.net.URL getUrl() {
            return this.url;
        }

        /* access modifiers changed from: 0000 */
        public void setUrl(java.net.URL url2) {
            this.url = url2;
        }
    }

    private java.net.URL peekPath(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext) {
        if (!interpretationContext.isEmpty()) {
            java.lang.Object peekObject = interpretationContext.peekObject();
            if (peekObject instanceof ch.qos.logback.classic.joran.action.ConditionalIncludeAction.State) {
                java.net.URL url = ((ch.qos.logback.classic.joran.action.ConditionalIncludeAction.State) peekObject).getUrl();
                if (url != null) {
                    return url;
                }
            }
        }
        return null;
    }

    private java.net.URL pushPath(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext, java.net.URL url) {
        ch.qos.logback.classic.joran.action.ConditionalIncludeAction.State state = new ch.qos.logback.classic.joran.action.ConditionalIncludeAction.State();
        state.setUrl(url);
        interpretationContext.pushObject(state);
        return url;
    }

    public void begin(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext, java.lang.String str, org.xml.sax.Attributes attributes) throws ch.qos.logback.core.joran.spi.ActionException {
        if (peekPath(interpretationContext) == null) {
            super.begin(interpretationContext, str, attributes);
        }
    }

    /* access modifiers changed from: protected */
    public void handleError(java.lang.String str, java.lang.Exception exc) {
        if (exc == null || (exc instanceof java.io.FileNotFoundException) || (exc instanceof java.net.UnknownHostException)) {
            addInfo(str);
        } else {
            addWarn(str, exc);
        }
    }

    /* access modifiers changed from: protected */
    public void processInclude(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext, java.net.URL url) throws ch.qos.logback.core.joran.spi.JoranException {
        pushPath(interpretationContext, url);
    }
}
