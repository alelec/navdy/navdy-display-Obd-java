package ch.qos.logback.classic.joran.action;

@java.lang.Deprecated
public class LevelAction extends ch.qos.logback.core.joran.action.Action {
    boolean inError = false;

    public void begin(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext, java.lang.String str, org.xml.sax.Attributes attributes) {
        java.lang.Object peekObject = interpretationContext.peekObject();
        if (!(peekObject instanceof ch.qos.logback.classic.Logger)) {
            this.inError = true;
            addError("For element <level>, could not find a logger at the top of execution stack.");
            return;
        }
        ch.qos.logback.classic.Logger logger = (ch.qos.logback.classic.Logger) peekObject;
        java.lang.String name = logger.getName();
        java.lang.String subst = interpretationContext.subst(attributes.getValue("value"));
        if (ch.qos.logback.core.joran.action.ActionConst.INHERITED.equalsIgnoreCase(subst) || ch.qos.logback.core.joran.action.ActionConst.NULL.equalsIgnoreCase(subst)) {
            logger.setLevel(null);
        } else {
            logger.setLevel(ch.qos.logback.classic.Level.toLevel(subst, ch.qos.logback.classic.Level.DEBUG));
        }
        addInfo(name + " level set to " + logger.getLevel());
    }

    public void end(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext, java.lang.String str) {
    }

    public void finish(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext) {
    }
}
