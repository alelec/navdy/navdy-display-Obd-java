package ch.qos.logback.classic.joran.action;

public class LoggerContextListenerAction extends ch.qos.logback.core.joran.action.Action {
    boolean inError = false;
    ch.qos.logback.classic.spi.LoggerContextListener lcl;

    public void begin(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext, java.lang.String str, org.xml.sax.Attributes attributes) throws ch.qos.logback.core.joran.spi.ActionException {
        this.inError = false;
        java.lang.String value = attributes.getValue(ch.qos.logback.core.joran.action.Action.CLASS_ATTRIBUTE);
        if (ch.qos.logback.core.util.OptionHelper.isEmpty(value)) {
            addError("Mandatory \"class\" attribute not set for <loggerContextListener> element");
            this.inError = true;
            return;
        }
        try {
            this.lcl = (ch.qos.logback.classic.spi.LoggerContextListener) ch.qos.logback.core.util.OptionHelper.instantiateByClassName(value, ch.qos.logback.classic.spi.LoggerContextListener.class, this.context);
            if (this.lcl instanceof ch.qos.logback.core.spi.ContextAware) {
                ((ch.qos.logback.core.spi.ContextAware) this.lcl).setContext(this.context);
            }
            interpretationContext.pushObject(this.lcl);
            addInfo("Adding LoggerContextListener of type [" + value + "] to the object stack");
        } catch (java.lang.Exception e) {
            this.inError = true;
            addError("Could not create LoggerContextListener of type " + value + "].", e);
        }
    }

    public void end(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext, java.lang.String str) throws ch.qos.logback.core.joran.spi.ActionException {
        if (!this.inError) {
            if (interpretationContext.peekObject() != this.lcl) {
                addWarn("The object on the top the of the stack is not the LoggerContextListener pushed earlier.");
                return;
            }
            if (this.lcl instanceof ch.qos.logback.core.spi.LifeCycle) {
                ((ch.qos.logback.core.spi.LifeCycle) this.lcl).start();
                addInfo("Starting LoggerContextListener");
            }
            ((ch.qos.logback.classic.LoggerContext) this.context).addListener(this.lcl);
            interpretationContext.popObject();
        }
    }
}
