package ch.qos.logback.classic.joran.action;

public class LoggerAction extends ch.qos.logback.core.joran.action.Action {
    public static final java.lang.String LEVEL_ATTRIBUTE = "level";
    boolean inError = false;
    ch.qos.logback.classic.Logger logger;

    public void begin(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext, java.lang.String str, org.xml.sax.Attributes attributes) {
        this.inError = false;
        this.logger = null;
        ch.qos.logback.classic.LoggerContext loggerContext = (ch.qos.logback.classic.LoggerContext) this.context;
        java.lang.String subst = interpretationContext.subst(attributes.getValue(ch.qos.logback.core.joran.action.Action.NAME_ATTRIBUTE));
        if (ch.qos.logback.core.util.OptionHelper.isEmpty(subst)) {
            this.inError = true;
            addError("No 'name' attribute in element " + str + ", around " + getLineColStr(interpretationContext));
            return;
        }
        this.logger = loggerContext.getLogger(subst);
        java.lang.String subst2 = interpretationContext.subst(attributes.getValue("level"));
        if (!ch.qos.logback.core.util.OptionHelper.isEmpty(subst2)) {
            if (ch.qos.logback.core.joran.action.ActionConst.INHERITED.equalsIgnoreCase(subst2) || ch.qos.logback.core.joran.action.ActionConst.NULL.equalsIgnoreCase(subst2)) {
                addInfo("Setting level of logger [" + subst + "] to null, i.e. INHERITED");
                this.logger.setLevel(null);
            } else {
                ch.qos.logback.classic.Level level = ch.qos.logback.classic.Level.toLevel(subst2);
                addInfo("Setting level of logger [" + subst + "] to " + level);
                this.logger.setLevel(level);
            }
        }
        java.lang.String subst3 = interpretationContext.subst(attributes.getValue(ch.qos.logback.core.joran.action.ActionConst.ADDITIVITY_ATTRIBUTE));
        if (!ch.qos.logback.core.util.OptionHelper.isEmpty(subst3)) {
            boolean booleanValue = java.lang.Boolean.valueOf(subst3).booleanValue();
            addInfo("Setting additivity of logger [" + subst + "] to " + booleanValue);
            this.logger.setAdditive(booleanValue);
        }
        interpretationContext.pushObject(this.logger);
    }

    public void end(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext, java.lang.String str) {
        if (!this.inError) {
            java.lang.Object peekObject = interpretationContext.peekObject();
            if (peekObject != this.logger) {
                addWarn("The object on the top the of the stack is not " + this.logger + " pushed earlier");
                addWarn("It is: " + peekObject);
                return;
            }
            interpretationContext.popObject();
        }
    }

    public void finish(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext) {
    }
}
