package ch.qos.logback.classic.joran.action;

public class ConfigurationAction extends ch.qos.logback.core.joran.action.Action {
    static final java.lang.String DEBUG_SYSTEM_PROPERTY_KEY = "logback.debug";
    static final java.lang.String INTERNAL_DEBUG_ATTR = "debug";
    static final java.lang.String SCAN_ATTR = "scan";
    static final java.lang.String SCAN_PERIOD_ATTR = "scanPeriod";

    public void begin(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext, java.lang.String str, org.xml.sax.Attributes attributes) {
        java.lang.String systemProperty = ch.qos.logback.core.util.OptionHelper.getSystemProperty(DEBUG_SYSTEM_PROPERTY_KEY);
        if (systemProperty == null) {
            systemProperty = interpretationContext.subst(attributes.getValue("debug"));
        }
        if (ch.qos.logback.core.util.OptionHelper.isEmpty(systemProperty) || systemProperty.equalsIgnoreCase("false") || systemProperty.equalsIgnoreCase("null")) {
            addInfo("debug attribute not set");
        } else {
            ch.qos.logback.core.status.OnConsoleStatusListener.addNewInstanceToContext(this.context);
        }
        processScanAttrib(interpretationContext, attributes);
        new ch.qos.logback.core.util.ContextUtil(this.context).addHostNameAsProperty();
        interpretationContext.pushObject(getContext());
    }

    public void end(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext, java.lang.String str) {
        addInfo("End of configuration.");
        interpretationContext.popObject();
    }

    /* access modifiers changed from: 0000 */
    public java.lang.String getSystemProperty(java.lang.String str) {
        try {
            return java.lang.System.getProperty(str);
        } catch (java.lang.SecurityException e) {
            return null;
        }
    }

    /* access modifiers changed from: 0000 */
    public void processScanAttrib(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext, org.xml.sax.Attributes attributes) {
        java.lang.String subst = interpretationContext.subst(attributes.getValue(SCAN_ATTR));
        if (!ch.qos.logback.core.util.OptionHelper.isEmpty(subst) && !"false".equalsIgnoreCase(subst)) {
            ch.qos.logback.classic.turbo.ReconfigureOnChangeFilter reconfigureOnChangeFilter = new ch.qos.logback.classic.turbo.ReconfigureOnChangeFilter();
            reconfigureOnChangeFilter.setContext(this.context);
            java.lang.String subst2 = interpretationContext.subst(attributes.getValue(SCAN_PERIOD_ATTR));
            if (!ch.qos.logback.core.util.OptionHelper.isEmpty(subst2)) {
                try {
                    ch.qos.logback.core.util.Duration valueOf = ch.qos.logback.core.util.Duration.valueOf(subst2);
                    reconfigureOnChangeFilter.setRefreshPeriod(valueOf.getMilliseconds());
                    addInfo("Setting ReconfigureOnChangeFilter scanning period to " + valueOf);
                } catch (java.lang.NumberFormatException e) {
                    addError("Error while converting [" + subst + "] to long", e);
                }
            }
            reconfigureOnChangeFilter.start();
            ch.qos.logback.classic.LoggerContext loggerContext = (ch.qos.logback.classic.LoggerContext) this.context;
            addInfo("Adding ReconfigureOnChangeFilter as a turbo filter");
            loggerContext.addTurboFilter(reconfigureOnChangeFilter);
        }
    }
}
