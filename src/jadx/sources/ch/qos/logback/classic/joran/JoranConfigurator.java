package ch.qos.logback.classic.joran;

public class JoranConfigurator extends ch.qos.logback.core.joran.JoranConfiguratorBase {
    /* access modifiers changed from: protected */
    public void addDefaultNestedComponentRegistryRules(ch.qos.logback.core.joran.spi.DefaultNestedComponentRegistry defaultNestedComponentRegistry) {
        ch.qos.logback.classic.util.DefaultNestedComponentRules.addDefaultNestedComponentRegistryRules(defaultNestedComponentRegistry);
    }

    public void addInstanceRules(ch.qos.logback.core.joran.spi.RuleStore ruleStore) {
        super.addInstanceRules(ruleStore);
        ruleStore.addRule(new ch.qos.logback.core.joran.spi.ElementSelector("configuration"), (ch.qos.logback.core.joran.action.Action) new ch.qos.logback.classic.joran.action.ConfigurationAction());
        ruleStore.addRule(new ch.qos.logback.core.joran.spi.ElementSelector("configuration/contextName"), (ch.qos.logback.core.joran.action.Action) new ch.qos.logback.classic.joran.action.ContextNameAction());
        ruleStore.addRule(new ch.qos.logback.core.joran.spi.ElementSelector("configuration/contextListener"), (ch.qos.logback.core.joran.action.Action) new ch.qos.logback.classic.joran.action.LoggerContextListenerAction());
        ruleStore.addRule(new ch.qos.logback.core.joran.spi.ElementSelector("configuration/appender/sift"), (ch.qos.logback.core.joran.action.Action) new ch.qos.logback.classic.sift.SiftAction());
        ruleStore.addRule(new ch.qos.logback.core.joran.spi.ElementSelector("configuration/appender/sift/*"), (ch.qos.logback.core.joran.action.Action) new ch.qos.logback.core.joran.action.NOPAction());
        ruleStore.addRule(new ch.qos.logback.core.joran.spi.ElementSelector("configuration/logger"), (ch.qos.logback.core.joran.action.Action) new ch.qos.logback.classic.joran.action.LoggerAction());
        ruleStore.addRule(new ch.qos.logback.core.joran.spi.ElementSelector("configuration/logger/level"), (ch.qos.logback.core.joran.action.Action) new ch.qos.logback.classic.joran.action.LevelAction());
        ruleStore.addRule(new ch.qos.logback.core.joran.spi.ElementSelector("configuration/root"), (ch.qos.logback.core.joran.action.Action) new ch.qos.logback.classic.joran.action.RootLoggerAction());
        ruleStore.addRule(new ch.qos.logback.core.joran.spi.ElementSelector("configuration/root/level"), (ch.qos.logback.core.joran.action.Action) new ch.qos.logback.classic.joran.action.LevelAction());
        ruleStore.addRule(new ch.qos.logback.core.joran.spi.ElementSelector("configuration/logger/appender-ref"), (ch.qos.logback.core.joran.action.Action) new ch.qos.logback.core.joran.action.AppenderRefAction());
        ruleStore.addRule(new ch.qos.logback.core.joran.spi.ElementSelector("configuration/root/appender-ref"), (ch.qos.logback.core.joran.action.Action) new ch.qos.logback.core.joran.action.AppenderRefAction());
        ruleStore.addRule(new ch.qos.logback.core.joran.spi.ElementSelector("configuration/include"), (ch.qos.logback.core.joran.action.Action) new ch.qos.logback.core.joran.action.IncludeAction());
        ruleStore.addRule(new ch.qos.logback.core.joran.spi.ElementSelector("configuration/includes"), (ch.qos.logback.core.joran.action.Action) new ch.qos.logback.classic.joran.action.FindIncludeAction());
        ruleStore.addRule(new ch.qos.logback.core.joran.spi.ElementSelector("configuration/includes/include"), (ch.qos.logback.core.joran.action.Action) new ch.qos.logback.classic.joran.action.ConditionalIncludeAction());
        ruleStore.addRule(new ch.qos.logback.core.joran.spi.ElementSelector("configuration/receiver"), (ch.qos.logback.core.joran.action.Action) new ch.qos.logback.classic.joran.action.ReceiverAction());
    }
}
