package ch.qos.logback.classic.joran.action;

public class ReceiverAction extends ch.qos.logback.core.joran.action.Action {
    private boolean inError;
    private ch.qos.logback.classic.net.ReceiverBase receiver;

    public void begin(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext, java.lang.String str, org.xml.sax.Attributes attributes) throws ch.qos.logback.core.joran.spi.ActionException {
        java.lang.String value = attributes.getValue(ch.qos.logback.core.joran.action.Action.CLASS_ATTRIBUTE);
        if (ch.qos.logback.core.util.OptionHelper.isEmpty(value)) {
            addError("Missing class name for receiver. Near [" + str + "] line " + getLineNumber(interpretationContext));
            this.inError = true;
            return;
        }
        try {
            addInfo("About to instantiate receiver of type [" + value + "]");
            this.receiver = (ch.qos.logback.classic.net.ReceiverBase) ch.qos.logback.core.util.OptionHelper.instantiateByClassName(value, ch.qos.logback.classic.net.ReceiverBase.class, this.context);
            this.receiver.setContext(this.context);
            interpretationContext.pushObject(this.receiver);
        } catch (java.lang.Exception e) {
            this.inError = true;
            addError("Could not create a receiver of type [" + value + "].", e);
            throw new ch.qos.logback.core.joran.spi.ActionException(e);
        }
    }

    public void end(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext, java.lang.String str) throws ch.qos.logback.core.joran.spi.ActionException {
        if (!this.inError) {
            interpretationContext.getContext().register(this.receiver);
            this.receiver.start();
            if (interpretationContext.peekObject() != this.receiver) {
                addWarn("The object at the of the stack is not the remote pushed earlier.");
            } else {
                interpretationContext.popObject();
            }
        }
    }
}
