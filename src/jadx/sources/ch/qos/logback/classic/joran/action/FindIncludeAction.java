package ch.qos.logback.classic.joran.action;

public class FindIncludeAction extends ch.qos.logback.core.joran.action.IncludeAction {
    private static final int EVENT_OFFSET = 1;

    public FindIncludeAction() {
        setEventOffset(1);
    }

    public void begin(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext, java.lang.String str, org.xml.sax.Attributes attributes) throws ch.qos.logback.core.joran.spi.ActionException {
    }

    /* access modifiers changed from: protected */
    public ch.qos.logback.core.joran.event.SaxEventRecorder createRecorder(java.io.InputStream inputStream, java.net.URL url) {
        if (!url.toString().endsWith("AndroidManifest.xml")) {
            return new ch.qos.logback.core.joran.event.SaxEventRecorder(getContext());
        }
        ch.qos.logback.classic.android.ASaxEventRecorder aSaxEventRecorder = new ch.qos.logback.classic.android.ASaxEventRecorder();
        aSaxEventRecorder.setFilter("logback");
        return aSaxEventRecorder;
    }

    public void end(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext, java.lang.String str) throws ch.qos.logback.core.joran.spi.ActionException {
        if (!interpretationContext.isEmpty() && (interpretationContext.peekObject() instanceof ch.qos.logback.classic.joran.action.ConditionalIncludeAction.State)) {
            java.net.URL url = ((ch.qos.logback.classic.joran.action.ConditionalIncludeAction.State) interpretationContext.popObject()).getUrl();
            if (url != null) {
                addInfo("Path found [" + url.toString() + "]");
                try {
                    processInclude(interpretationContext, url);
                } catch (ch.qos.logback.core.joran.spi.JoranException e) {
                    addError("Failed to process include [" + url.toString() + "]", e);
                }
            } else {
                addInfo("No paths found from includes");
            }
        }
    }
}
