package ch.qos.logback.classic.joran.action;

public class RootLoggerAction extends ch.qos.logback.core.joran.action.Action {
    boolean inError = false;
    ch.qos.logback.classic.Logger root;

    public void begin(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext, java.lang.String str, org.xml.sax.Attributes attributes) {
        this.inError = false;
        this.root = ((ch.qos.logback.classic.LoggerContext) this.context).getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME);
        java.lang.String subst = interpretationContext.subst(attributes.getValue("level"));
        if (!ch.qos.logback.core.util.OptionHelper.isEmpty(subst)) {
            ch.qos.logback.classic.Level level = ch.qos.logback.classic.Level.toLevel(subst);
            addInfo("Setting level of ROOT logger to " + level);
            this.root.setLevel(level);
        }
        interpretationContext.pushObject(this.root);
    }

    public void end(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext, java.lang.String str) {
        if (!this.inError) {
            java.lang.Object peekObject = interpretationContext.peekObject();
            if (peekObject != this.root) {
                addWarn("The object on the top the of the stack is not the root logger");
                addWarn("It is: " + peekObject);
                return;
            }
            interpretationContext.popObject();
        }
    }

    public void finish(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext) {
    }
}
