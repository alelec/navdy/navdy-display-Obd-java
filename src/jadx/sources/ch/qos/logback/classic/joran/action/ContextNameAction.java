package ch.qos.logback.classic.joran.action;

public class ContextNameAction extends ch.qos.logback.core.joran.action.Action {
    public void begin(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext, java.lang.String str, org.xml.sax.Attributes attributes) {
    }

    public void body(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext, java.lang.String str) {
        java.lang.String subst = interpretationContext.subst(str);
        addInfo("Setting logger context name as [" + subst + "]");
        try {
            this.context.setName(subst);
        } catch (java.lang.IllegalStateException e) {
            addError("Failed to rename context [" + this.context.getName() + "] as [" + subst + "]", e);
        }
    }

    public void end(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext, java.lang.String str) {
    }
}
