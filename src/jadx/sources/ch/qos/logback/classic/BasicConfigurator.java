package ch.qos.logback.classic;

@java.lang.Deprecated
public class BasicConfigurator {
    static final ch.qos.logback.classic.BasicConfigurator hiddenSingleton = new ch.qos.logback.classic.BasicConfigurator();

    private BasicConfigurator() {
    }

    public static void configure(ch.qos.logback.classic.LoggerContext loggerContext) {
        ch.qos.logback.core.status.StatusManager statusManager = loggerContext.getStatusManager();
        if (statusManager != null) {
            statusManager.add((ch.qos.logback.core.status.Status) new ch.qos.logback.core.status.InfoStatus("Setting up default configuration.", loggerContext));
        }
        ch.qos.logback.core.ConsoleAppender consoleAppender = new ch.qos.logback.core.ConsoleAppender();
        consoleAppender.setContext(loggerContext);
        consoleAppender.setName("console");
        ch.qos.logback.classic.encoder.PatternLayoutEncoder patternLayoutEncoder = new ch.qos.logback.classic.encoder.PatternLayoutEncoder();
        patternLayoutEncoder.setContext(loggerContext);
        patternLayoutEncoder.setPattern("%d{HH:mm:ss.SSS} [%thread] %-5level %logger{36} - %msg%n");
        patternLayoutEncoder.start();
        consoleAppender.setEncoder(patternLayoutEncoder);
        consoleAppender.start();
        loggerContext.getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME).addAppender(consoleAppender);
    }

    public static void configureDefaultContext() {
        configure((ch.qos.logback.classic.LoggerContext) org.slf4j.LoggerFactory.getILoggerFactory());
    }
}
