package ch.qos.logback.classic.android;

public class LogcatAppender extends ch.qos.logback.core.AppenderBase<ch.qos.logback.classic.spi.ILoggingEvent> {
    private static final int MAX_TAG_LENGTH = 23;
    private boolean checkLoggable = false;
    private ch.qos.logback.classic.encoder.PatternLayoutEncoder encoder = null;
    private ch.qos.logback.classic.encoder.PatternLayoutEncoder tagEncoder = null;

    public void append(ch.qos.logback.classic.spi.ILoggingEvent iLoggingEvent) {
        if (isStarted()) {
            java.lang.String tag = getTag(iLoggingEvent);
            switch (iLoggingEvent.getLevel().levelInt) {
                case Integer.MIN_VALUE:
                case 5000:
                    if (!this.checkLoggable || android.util.Log.isLoggable(tag, 2)) {
                        android.util.Log.v(tag, this.encoder.getLayout().doLayout(iLoggingEvent));
                        return;
                    }
                    return;
                case 10000:
                    if (!this.checkLoggable || android.util.Log.isLoggable(tag, 3)) {
                        android.util.Log.d(tag, this.encoder.getLayout().doLayout(iLoggingEvent));
                        return;
                    }
                    return;
                case ch.qos.logback.classic.Level.INFO_INT /*20000*/:
                    if (!this.checkLoggable || android.util.Log.isLoggable(tag, 4)) {
                        android.util.Log.i(tag, this.encoder.getLayout().doLayout(iLoggingEvent));
                        return;
                    }
                    return;
                case 30000:
                    if (!this.checkLoggable || android.util.Log.isLoggable(tag, 5)) {
                        android.util.Log.w(tag, this.encoder.getLayout().doLayout(iLoggingEvent));
                        return;
                    }
                    return;
                case ch.qos.logback.classic.Level.ERROR_INT /*40000*/:
                    if (!this.checkLoggable || android.util.Log.isLoggable(tag, 6)) {
                        android.util.Log.e(tag, this.encoder.getLayout().doLayout(iLoggingEvent));
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    public boolean getCheckLoggable() {
        return this.checkLoggable;
    }

    public ch.qos.logback.classic.encoder.PatternLayoutEncoder getEncoder() {
        return this.encoder;
    }

    /* access modifiers changed from: protected */
    public java.lang.String getTag(ch.qos.logback.classic.spi.ILoggingEvent iLoggingEvent) {
        java.lang.String loggerName = this.tagEncoder != null ? this.tagEncoder.getLayout().doLayout(iLoggingEvent) : iLoggingEvent.getLoggerName();
        return (!this.checkLoggable || loggerName.length() <= 23) ? loggerName : loggerName.substring(0, 22) + org.slf4j.Marker.ANY_MARKER;
    }

    public ch.qos.logback.classic.encoder.PatternLayoutEncoder getTagEncoder() {
        return this.tagEncoder;
    }

    public void setCheckLoggable(boolean z) {
        this.checkLoggable = z;
    }

    public void setEncoder(ch.qos.logback.classic.encoder.PatternLayoutEncoder patternLayoutEncoder) {
        this.encoder = patternLayoutEncoder;
    }

    public void setTagEncoder(ch.qos.logback.classic.encoder.PatternLayoutEncoder patternLayoutEncoder) {
        this.tagEncoder = patternLayoutEncoder;
    }

    public void start() {
        if (this.encoder == null || this.encoder.getLayout() == null) {
            addError("No layout set for the appender named [" + this.name + "].");
            return;
        }
        if (this.tagEncoder != null) {
            ch.qos.logback.core.Layout layout = this.tagEncoder.getLayout();
            if (layout == null) {
                addError("No tag layout set for the appender named [" + this.name + "].");
                return;
            } else if (layout instanceof ch.qos.logback.classic.PatternLayout) {
                java.lang.String pattern = this.tagEncoder.getPattern();
                if (!pattern.contains("%nopex")) {
                    this.tagEncoder.stop();
                    this.tagEncoder.setPattern(pattern + "%nopex");
                    this.tagEncoder.start();
                }
                ((ch.qos.logback.classic.PatternLayout) layout).setPostCompileProcessor(null);
            }
        }
        super.start();
    }
}
