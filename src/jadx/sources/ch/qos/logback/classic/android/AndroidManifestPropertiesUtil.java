package ch.qos.logback.classic.android;

public class AndroidManifestPropertiesUtil {
    public static void setAndroidProperties(ch.qos.logback.core.Context context) throws ch.qos.logback.core.joran.spi.JoranException {
        ch.qos.logback.classic.android.ASaxEventRecorder aSaxEventRecorder = new ch.qos.logback.classic.android.ASaxEventRecorder();
        aSaxEventRecorder.setFilter("-");
        aSaxEventRecorder.setAttributeWatch("manifest");
        ch.qos.logback.core.status.StatusManager statusManager = context.getStatusManager();
        java.io.InputStream resourceAsStream = ch.qos.logback.core.util.Loader.getClassLoaderOfObject(context).getResourceAsStream("AndroidManifest.xml");
        if (resourceAsStream == null) {
            statusManager.add((ch.qos.logback.core.status.Status) new ch.qos.logback.core.status.WarnStatus("Could not find AndroidManifest.xml", context));
            return;
        }
        try {
            aSaxEventRecorder.recordEvents(resourceAsStream);
            context.putProperty(ch.qos.logback.core.CoreConstants.EXT_DIR_KEY, ch.qos.logback.core.android.CommonPathUtil.getMountedExternalStorageDirectoryPath());
            java.util.Map attributeWatchValues = aSaxEventRecorder.getAttributeWatchValues();
            for (java.lang.String str : attributeWatchValues.keySet()) {
                if (str.equals("android:versionName")) {
                    context.putProperty(ch.qos.logback.core.CoreConstants.VERSION_NAME_KEY, (java.lang.String) attributeWatchValues.get(str));
                } else if (str.equals("android:versionCode")) {
                    context.putProperty(ch.qos.logback.core.CoreConstants.VERSION_CODE_KEY, (java.lang.String) attributeWatchValues.get(str));
                } else if (str.equals("package")) {
                    context.putProperty(ch.qos.logback.core.CoreConstants.PACKAGE_NAME_KEY, (java.lang.String) attributeWatchValues.get(str));
                }
            }
            java.lang.String str2 = (java.lang.String) attributeWatchValues.get("package");
            if (str2 == null || str2.length() <= 0) {
                statusManager.add((ch.qos.logback.core.status.Status) new ch.qos.logback.core.status.WarnStatus("Package name not found. Some properties cannot be set.", context));
            } else {
                context.putProperty(ch.qos.logback.core.CoreConstants.DATA_DIR_KEY, ch.qos.logback.core.android.CommonPathUtil.getFilesDirectoryPath(str2));
            }
        } finally {
            try {
                resourceAsStream.close();
            } catch (java.io.IOException e) {
            }
        }
    }
}
