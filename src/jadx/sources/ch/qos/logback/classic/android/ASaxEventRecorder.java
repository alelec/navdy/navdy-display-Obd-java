package ch.qos.logback.classic.android;

public class ASaxEventRecorder extends ch.qos.logback.core.joran.event.SaxEventRecorder {
    private java.util.Map<java.lang.String, java.lang.String> elemAttrs = null;
    private java.lang.String elemNameToWatch = null;
    private ch.qos.logback.classic.android.ASaxEventRecorder.StatePassFilter filter = new ch.qos.logback.classic.android.ASaxEventRecorder.StatePassFilter(new java.lang.String[0]);
    private int[] holderForStartAndLength = new int[2];

    static class StatePassFilter {
        private int _depth = 0;
        private final java.lang.String[] _states;

        public StatePassFilter(java.lang.String... strArr) {
            if (strArr == null) {
                strArr = new java.lang.String[0];
            }
            this._states = strArr;
        }

        public boolean checkEnd(java.lang.String str) {
            if (this._depth <= 0 || !str.equals(this._states[this._depth - 1])) {
                return this._depth == this._states.length;
            }
            this._depth--;
            return false;
        }

        public boolean checkStart(java.lang.String str) {
            if (this._depth == this._states.length) {
                return true;
            }
            if (str.equals(this._states[this._depth])) {
                this._depth++;
            }
            return false;
        }

        public int depth() {
            return this._depth;
        }

        public boolean passed() {
            return this._depth == this._states.length;
        }

        public void reset() {
            this._depth = 0;
        }

        public int size() {
            return this._states.length;
        }
    }

    private void characters(org.xmlpull.v1.XmlPullParser xmlPullParser) {
        if (this.filter.passed()) {
            super.characters(xmlPullParser.getTextCharacters(this.holderForStartAndLength), this.holderForStartAndLength[0], this.holderForStartAndLength[1]);
        }
    }

    private void checkForWatchedAttributes(org.xmlpull.v1.XmlPullParser xmlPullParser) {
        java.lang.String str;
        if (this.elemNameToWatch != null && this.elemAttrs == null && xmlPullParser.getName().equals(this.elemNameToWatch)) {
            java.util.HashMap hashMap = new java.util.HashMap();
            for (int i = 0; i < xmlPullParser.getAttributeCount(); i++) {
                java.lang.String str2 = "";
                java.lang.String attributeNamespace = xmlPullParser.getAttributeNamespace(i);
                if (attributeNamespace.length() > 0) {
                    int lastIndexOf = attributeNamespace.lastIndexOf("/");
                    if (lastIndexOf > -1 && lastIndexOf + 1 < attributeNamespace.length()) {
                        attributeNamespace = attributeNamespace.substring(lastIndexOf + 1);
                    }
                    str = attributeNamespace + ":";
                } else {
                    str = str2;
                }
                hashMap.put(str + xmlPullParser.getAttributeName(i), xmlPullParser.getAttributeValue(i));
            }
            this.elemAttrs = hashMap;
        }
    }

    private void endElement(org.xmlpull.v1.XmlPullParser xmlPullParser) {
        java.lang.String name = xmlPullParser.getName();
        if (this.filter.checkEnd(name)) {
            endElement(xmlPullParser.getNamespace(), name, name);
        }
    }

    private void startDocument(org.xmlpull.v1.XmlPullParser xmlPullParser) {
        super.startDocument();
        super.setDocumentLocator(new org.xml.sax.helpers.LocatorImpl());
    }

    private void startElement(org.xmlpull.v1.XmlPullParser xmlPullParser) {
        java.lang.String name = xmlPullParser.getName();
        if (this.filter.checkStart(name)) {
            org.xml.sax.helpers.AttributesImpl attributesImpl = new org.xml.sax.helpers.AttributesImpl();
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= xmlPullParser.getAttributeCount()) {
                    break;
                }
                attributesImpl.addAttribute(xmlPullParser.getAttributeNamespace(i2), xmlPullParser.getAttributeName(i2), xmlPullParser.getAttributeName(i2), xmlPullParser.getAttributeType(i2), xmlPullParser.getAttributeValue(i2));
                i = i2 + 1;
            }
            startElement(xmlPullParser.getNamespace(), name, name, attributesImpl);
        }
        checkForWatchedAttributes(xmlPullParser);
    }

    public java.util.Map<java.lang.String, java.lang.String> getAttributeWatchValues() {
        return this.elemAttrs;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        r3.filter.reset();
        endDocument();
     */
    public java.util.List<ch.qos.logback.core.joran.event.SaxEvent> recordEvents(org.xml.sax.InputSource inputSource) throws ch.qos.logback.core.joran.spi.JoranException {
        java.io.InputStream byteStream = inputSource.getByteStream();
        if (byteStream == null) {
            throw new java.lang.IllegalArgumentException("Input source must specify an input stream");
        }
        try {
            brut.androlib.res.decoder.AXmlResourceParser aXmlResourceParser = new brut.androlib.res.decoder.AXmlResourceParser(byteStream);
            this.elemAttrs = null;
            while (true) {
                int next = aXmlResourceParser.next();
                if (next <= -1) {
                    break;
                } else if (next == 0) {
                    this.filter.reset();
                    startDocument(aXmlResourceParser);
                } else if (1 == next) {
                    break;
                } else if (2 == next) {
                    startElement(aXmlResourceParser);
                } else if (3 == next) {
                    endElement(aXmlResourceParser);
                } else if (4 == next) {
                    characters(aXmlResourceParser);
                }
            }
            return getSaxEventList();
        } catch (java.lang.Exception e) {
            addError(e.getMessage(), e);
            throw new ch.qos.logback.core.joran.spi.JoranException("Can't parse Android XML resource", e);
        }
    }

    public void setAttributeWatch(java.lang.String str) {
        this.elemNameToWatch = str;
    }

    public void setFilter(java.lang.String... strArr) {
        this.filter = new ch.qos.logback.classic.android.ASaxEventRecorder.StatePassFilter(strArr);
    }
}
