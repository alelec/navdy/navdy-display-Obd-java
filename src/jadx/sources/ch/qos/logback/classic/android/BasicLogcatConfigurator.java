package ch.qos.logback.classic.android;

public class BasicLogcatConfigurator {
    private BasicLogcatConfigurator() {
    }

    public static void configure(ch.qos.logback.classic.LoggerContext loggerContext) {
        ch.qos.logback.core.status.StatusManager statusManager = loggerContext.getStatusManager();
        if (statusManager != null) {
            statusManager.add((ch.qos.logback.core.status.Status) new ch.qos.logback.core.status.InfoStatus("Setting up default configuration.", loggerContext));
        }
        ch.qos.logback.classic.android.LogcatAppender logcatAppender = new ch.qos.logback.classic.android.LogcatAppender();
        logcatAppender.setContext(loggerContext);
        logcatAppender.setName("logcat");
        ch.qos.logback.classic.encoder.PatternLayoutEncoder patternLayoutEncoder = new ch.qos.logback.classic.encoder.PatternLayoutEncoder();
        patternLayoutEncoder.setContext(loggerContext);
        patternLayoutEncoder.setPattern("%msg");
        patternLayoutEncoder.start();
        logcatAppender.setEncoder(patternLayoutEncoder);
        logcatAppender.start();
        loggerContext.getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME).addAppender(logcatAppender);
    }

    public static void configureDefaultContext() {
        configure((ch.qos.logback.classic.LoggerContext) org.slf4j.LoggerFactory.getILoggerFactory());
    }
}
