package ch.qos.logback.classic.android;

public class SQLiteAppender extends ch.qos.logback.core.UnsynchronizedAppenderBase<ch.qos.logback.classic.spi.ILoggingEvent> {
    private static final int ARG0_INDEX = 7;
    private static final int CALLER_CLASS_INDEX = 12;
    private static final int CALLER_FILENAME_INDEX = 11;
    private static final int CALLER_LINE_INDEX = 14;
    private static final int CALLER_METHOD_INDEX = 13;
    private static final short EXCEPTION_EXISTS = 2;
    private static final int FORMATTED_MESSAGE_INDEX = 2;
    private static final int LEVEL_STRING_INDEX = 4;
    private static final int LOGGER_NAME_INDEX = 3;
    private static final short PROPERTIES_EXIST = 1;
    private static final int REFERENCE_FLAG_INDEX = 6;
    private static final int THREAD_NAME_INDEX = 5;
    private static final int TIMESTMP_INDEX = 1;
    private android.database.sqlite.SQLiteDatabase db;
    private ch.qos.logback.classic.db.names.DBNameResolver dbNameResolver;
    private java.lang.String insertExceptionSQL;
    private java.lang.String insertPropertiesSQL;
    private java.lang.String insertSQL;

    private java.lang.String asStringTruncatedTo254(java.lang.Object obj) {
        java.lang.String str = null;
        if (obj != null) {
            str = obj.toString();
        }
        if (str != null && str.length() > 254) {
            str = str.substring(0, 254);
        }
        return str == null ? "" : str;
    }

    private void bindCallerData(android.database.sqlite.SQLiteStatement sQLiteStatement, java.lang.StackTraceElement[] stackTraceElementArr) throws java.sql.SQLException {
        if (stackTraceElementArr != null && stackTraceElementArr.length > 0) {
            java.lang.StackTraceElement stackTraceElement = stackTraceElementArr[0];
            if (stackTraceElement != null) {
                sQLiteStatement.bindString(11, stackTraceElement.getFileName());
                sQLiteStatement.bindString(12, stackTraceElement.getClassName());
                sQLiteStatement.bindString(13, stackTraceElement.getMethodName());
                sQLiteStatement.bindString(14, java.lang.Integer.toString(stackTraceElement.getLineNumber()));
            }
        }
    }

    private void bindLoggingEvent(android.database.sqlite.SQLiteStatement sQLiteStatement, ch.qos.logback.classic.spi.ILoggingEvent iLoggingEvent) throws java.sql.SQLException {
        sQLiteStatement.bindLong(1, iLoggingEvent.getTimeStamp());
        sQLiteStatement.bindString(2, iLoggingEvent.getFormattedMessage());
        sQLiteStatement.bindString(3, iLoggingEvent.getLoggerName());
        sQLiteStatement.bindString(4, iLoggingEvent.getLevel().toString());
        sQLiteStatement.bindString(5, iLoggingEvent.getThreadName());
        sQLiteStatement.bindLong(6, (long) computeReferenceMask(iLoggingEvent));
    }

    private void bindLoggingEventArguments(android.database.sqlite.SQLiteStatement sQLiteStatement, java.lang.Object[] objArr) throws java.sql.SQLException {
        int i = 0;
        int length = objArr != null ? objArr.length : 0;
        while (i < length && i < 4) {
            sQLiteStatement.bindString(i + 7, asStringTruncatedTo254(objArr[i]));
            i++;
        }
    }

    private static short computeReferenceMask(ch.qos.logback.classic.spi.ILoggingEvent iLoggingEvent) {
        short s = 0;
        int i = iLoggingEvent.getMDCPropertyMap() != null ? iLoggingEvent.getMDCPropertyMap().keySet().size() : 0;
        int i2 = iLoggingEvent.getLoggerContextVO().getPropertyMap() != null ? iLoggingEvent.getLoggerContextVO().getPropertyMap().size() : 0;
        if (i > 0 || i2 > 0) {
            s = PROPERTIES_EXIST;
        }
        return iLoggingEvent.getThrowableProxy() != null ? (short) (s | EXCEPTION_EXISTS) : s;
    }

    private void insertException(android.database.sqlite.SQLiteStatement sQLiteStatement, java.lang.String str, short s, long j) throws java.sql.SQLException {
        sQLiteStatement.bindLong(1, j);
        sQLiteStatement.bindLong(2, (long) s);
        sQLiteStatement.bindString(3, str);
        sQLiteStatement.executeInsert();
    }

    private void insertProperties(java.util.Map<java.lang.String, java.lang.String> map, long j) throws java.sql.SQLException {
        if (map.size() > 0) {
            android.database.sqlite.SQLiteStatement compileStatement = this.db.compileStatement(this.insertPropertiesSQL);
            try {
                for (java.util.Map.Entry entry : map.entrySet()) {
                    compileStatement.bindLong(1, j);
                    compileStatement.bindString(2, (java.lang.String) entry.getKey());
                    compileStatement.bindString(3, (java.lang.String) entry.getValue());
                    compileStatement.executeInsert();
                }
            } finally {
                compileStatement.close();
            }
        }
    }

    private void insertThrowable(ch.qos.logback.classic.spi.IThrowableProxy iThrowableProxy, long j) throws java.sql.SQLException {
        android.database.sqlite.SQLiteStatement compileStatement = this.db.compileStatement(this.insertExceptionSQL);
        short s = 0;
        while (iThrowableProxy != null) {
            try {
                java.lang.StringBuilder sb = new java.lang.StringBuilder();
                ch.qos.logback.classic.spi.ThrowableProxyUtil.subjoinFirstLine(sb, iThrowableProxy);
                short s2 = (short) (s + PROPERTIES_EXIST);
                insertException(compileStatement, sb.toString(), s, j);
                int commonFrames = iThrowableProxy.getCommonFrames();
                ch.qos.logback.classic.spi.StackTraceElementProxy[] stackTraceElementProxyArray = iThrowableProxy.getStackTraceElementProxyArray();
                s = s2;
                int i = 0;
                while (i < stackTraceElementProxyArray.length - commonFrames) {
                    java.lang.StringBuilder sb2 = new java.lang.StringBuilder();
                    sb2.append(9);
                    ch.qos.logback.classic.spi.ThrowableProxyUtil.subjoinSTEP(sb2, stackTraceElementProxyArray[i]);
                    short s3 = (short) (s + PROPERTIES_EXIST);
                    insertException(compileStatement, sb2.toString(), s, j);
                    i++;
                    s = s3;
                }
                if (commonFrames > 0) {
                    java.lang.StringBuilder sb3 = new java.lang.StringBuilder();
                    sb3.append(9).append("... ").append(commonFrames).append(" common frames omitted");
                    short s4 = (short) (s + PROPERTIES_EXIST);
                    insertException(compileStatement, sb3.toString(), s, j);
                    s = s4;
                }
                iThrowableProxy = iThrowableProxy.getCause();
            } catch (Throwable th) {
                compileStatement.close();
                throw th;
            }
        }
        compileStatement.close();
    }

    private java.util.Map<java.lang.String, java.lang.String> mergePropertyMaps(ch.qos.logback.classic.spi.ILoggingEvent iLoggingEvent) {
        java.util.HashMap hashMap = new java.util.HashMap();
        java.util.Map propertyMap = iLoggingEvent.getLoggerContextVO().getPropertyMap();
        if (propertyMap != null) {
            hashMap.putAll(propertyMap);
        }
        java.util.Map mDCPropertyMap = iLoggingEvent.getMDCPropertyMap();
        if (mDCPropertyMap != null) {
            hashMap.putAll(mDCPropertyMap);
        }
        return hashMap;
    }

    private void secondarySubAppend(ch.qos.logback.classic.spi.ILoggingEvent iLoggingEvent, long j) throws java.sql.SQLException {
        insertProperties(mergePropertyMaps(iLoggingEvent), j);
        if (iLoggingEvent.getThrowableProxy() != null) {
            insertThrowable(iLoggingEvent.getThrowableProxy(), j);
        }
    }

    private long subAppend(ch.qos.logback.classic.spi.ILoggingEvent iLoggingEvent, android.database.sqlite.SQLiteStatement sQLiteStatement) throws java.sql.SQLException {
        bindLoggingEvent(sQLiteStatement, iLoggingEvent);
        bindLoggingEventArguments(sQLiteStatement, iLoggingEvent.getArgumentArray());
        bindCallerData(sQLiteStatement, iLoggingEvent.getCallerData());
        long j = -1;
        try {
            return sQLiteStatement.executeInsert();
        } catch (android.database.sqlite.SQLiteException e) {
            addWarn("Failed to insert loggingEvent", e);
            return j;
        }
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    public void append(ch.qos.logback.classic.spi.ILoggingEvent iLoggingEvent) {
        android.database.sqlite.SQLiteStatement compileStatement;
        if (isStarted()) {
            try {
                compileStatement = this.db.compileStatement(this.insertSQL);
                this.db.beginTransaction();
                long subAppend = subAppend(iLoggingEvent, compileStatement);
                if (subAppend != -1) {
                    secondarySubAppend(iLoggingEvent, subAppend);
                    this.db.setTransactionSuccessful();
                }
                if (this.db.inTransaction()) {
                    this.db.endTransaction();
                }
                compileStatement.close();
            } catch (Throwable th) {
                addError("Cannot append event", th);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void finalize() throws java.lang.Throwable {
        this.db.close();
    }

    public void setDbNameResolver(ch.qos.logback.classic.db.names.DBNameResolver dBNameResolver) {
        this.dbNameResolver = dBNameResolver;
    }

    public void start() {
        java.lang.String str = null;
        boolean z = true;
        this.started = false;
        if (getContext() != null) {
            str = getContext().getProperty(ch.qos.logback.core.CoreConstants.PACKAGE_NAME_KEY);
        }
        if (str == null || str.length() == 0) {
            addError("Cannot create database without package name");
            return;
        }
        try {
            java.io.File file = new java.io.File(ch.qos.logback.core.android.CommonPathUtil.getDatabaseDirectoryPath(str), "logback.db");
            file.getParentFile().mkdirs();
            this.db = android.database.sqlite.SQLiteDatabase.openOrCreateDatabase(file.getPath(), null);
        } catch (android.database.sqlite.SQLiteException e) {
            addError("Cannot open database", e);
            z = false;
        }
        if (z) {
            if (this.dbNameResolver == null) {
                this.dbNameResolver = new ch.qos.logback.classic.db.names.DefaultDBNameResolver();
            }
            this.insertExceptionSQL = ch.qos.logback.classic.db.SQLBuilder.buildInsertExceptionSQL(this.dbNameResolver);
            this.insertPropertiesSQL = ch.qos.logback.classic.db.SQLBuilder.buildInsertPropertiesSQL(this.dbNameResolver);
            this.insertSQL = ch.qos.logback.classic.db.SQLBuilder.buildInsertSQL(this.dbNameResolver);
            try {
                this.db.execSQL(ch.qos.logback.classic.db.SQLBuilder.buildCreateLoggingEventTableSQL(this.dbNameResolver));
                this.db.execSQL(ch.qos.logback.classic.db.SQLBuilder.buildCreatePropertyTableSQL(this.dbNameResolver));
                this.db.execSQL(ch.qos.logback.classic.db.SQLBuilder.buildCreateExceptionTableSQL(this.dbNameResolver));
                super.start();
                this.started = true;
            } catch (android.database.sqlite.SQLiteException e2) {
                addError("Cannot create database tables", e2);
            }
        }
    }

    public void stop() {
        this.db.close();
    }
}
