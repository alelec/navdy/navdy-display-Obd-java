package ch.qos.logback.classic.html;

public class HTMLLayout extends ch.qos.logback.core.html.HTMLLayoutBase<ch.qos.logback.classic.spi.ILoggingEvent> {
    static final java.lang.String DEFAULT_CONVERSION_PATTERN = "%date%thread%level%logger%mdc%msg";
    ch.qos.logback.core.html.IThrowableRenderer<ch.qos.logback.classic.spi.ILoggingEvent> throwableRenderer;

    public HTMLLayout() {
        this.pattern = DEFAULT_CONVERSION_PATTERN;
        this.throwableRenderer = new ch.qos.logback.classic.html.DefaultThrowableRenderer();
        this.cssBuilder = new ch.qos.logback.classic.html.DefaultCssBuilder();
    }

    private void appendEventToBuffer(java.lang.StringBuilder sb, ch.qos.logback.core.pattern.Converter<ch.qos.logback.classic.spi.ILoggingEvent> converter, ch.qos.logback.classic.spi.ILoggingEvent iLoggingEvent) {
        sb.append("<td class=\"");
        sb.append(computeConverterName(converter));
        sb.append("\">");
        converter.write(sb, iLoggingEvent);
        sb.append("</td>");
        sb.append(ch.qos.logback.core.CoreConstants.LINE_SEPARATOR);
    }

    /* access modifiers changed from: protected */
    public java.lang.String computeConverterName(ch.qos.logback.core.pattern.Converter converter) {
        if (!(converter instanceof ch.qos.logback.classic.pattern.MDCConverter)) {
            return super.computeConverterName(converter);
        }
        java.lang.String firstOption = ((ch.qos.logback.classic.pattern.MDCConverter) converter).getFirstOption();
        return firstOption != null ? firstOption : "MDC";
    }

    public java.lang.String doLayout(ch.qos.logback.classic.spi.ILoggingEvent iLoggingEvent) {
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        startNewTableIfLimitReached(sb);
        boolean z = true;
        long j = this.counter;
        this.counter = j + 1;
        if ((j & 1) == 0) {
            z = false;
        }
        java.lang.String lowerCase = iLoggingEvent.getLevel().toString().toLowerCase();
        sb.append(ch.qos.logback.core.CoreConstants.LINE_SEPARATOR);
        sb.append("<tr class=\"");
        sb.append(lowerCase);
        if (z) {
            sb.append(" odd\">");
        } else {
            sb.append(" even\">");
        }
        sb.append(ch.qos.logback.core.CoreConstants.LINE_SEPARATOR);
        for (ch.qos.logback.core.pattern.Converter converter = this.head; converter != null; converter = converter.getNext()) {
            appendEventToBuffer(sb, converter, iLoggingEvent);
        }
        sb.append("</tr>");
        sb.append(ch.qos.logback.core.CoreConstants.LINE_SEPARATOR);
        if (iLoggingEvent.getThrowableProxy() != null) {
            this.throwableRenderer.render(sb, iLoggingEvent);
        }
        return sb.toString();
    }

    /* access modifiers changed from: protected */
    public java.util.Map<java.lang.String, java.lang.String> getDefaultConverterMap() {
        return ch.qos.logback.classic.PatternLayout.defaultConverterMap;
    }

    public ch.qos.logback.core.html.IThrowableRenderer getThrowableRenderer() {
        return this.throwableRenderer;
    }

    public void setThrowableRenderer(ch.qos.logback.core.html.IThrowableRenderer<ch.qos.logback.classic.spi.ILoggingEvent> iThrowableRenderer) {
        this.throwableRenderer = iThrowableRenderer;
    }

    public void start() {
        boolean z = false;
        if (this.throwableRenderer == null) {
            addError("ThrowableRender cannot be null.");
            z = true;
        }
        if (!z) {
            super.start();
        }
    }
}
