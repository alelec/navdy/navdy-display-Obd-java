package ch.qos.logback.classic.html;

public class UrlCssBuilder implements ch.qos.logback.core.html.CssBuilder {
    java.lang.String url = "http://logback.qos.ch/css/classic.css";

    public void addCss(java.lang.StringBuilder sb) {
        sb.append("<link REL=StyleSheet HREF=\"");
        sb.append(this.url);
        sb.append("\" TITLE=\"Basic\" />");
    }

    public java.lang.String getUrl() {
        return this.url;
    }

    public void setUrl(java.lang.String str) {
        this.url = str;
    }
}
