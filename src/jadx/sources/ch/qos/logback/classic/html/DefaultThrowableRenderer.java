package ch.qos.logback.classic.html;

public class DefaultThrowableRenderer implements ch.qos.logback.core.html.IThrowableRenderer<ch.qos.logback.classic.spi.ILoggingEvent> {
    static final java.lang.String TRACE_PREFIX = "<br />&nbsp;&nbsp;&nbsp;&nbsp;";

    public void printFirstLine(java.lang.StringBuilder sb, ch.qos.logback.classic.spi.IThrowableProxy iThrowableProxy) {
        if (iThrowableProxy.getCommonFrames() > 0) {
            sb.append("<br />").append(ch.qos.logback.core.CoreConstants.CAUSED_BY);
        }
        sb.append(iThrowableProxy.getClassName()).append(": ").append(ch.qos.logback.core.helpers.Transform.escapeTags(iThrowableProxy.getMessage()));
        sb.append(ch.qos.logback.core.CoreConstants.LINE_SEPARATOR);
    }

    public void render(java.lang.StringBuilder sb, ch.qos.logback.classic.spi.ILoggingEvent iLoggingEvent) {
        sb.append("<tr><td class=\"Exception\" colspan=\"6\">");
        for (ch.qos.logback.classic.spi.IThrowableProxy throwableProxy = iLoggingEvent.getThrowableProxy(); throwableProxy != null; throwableProxy = throwableProxy.getCause()) {
            render(sb, throwableProxy);
        }
        sb.append("</td></tr>");
    }

    /* access modifiers changed from: 0000 */
    public void render(java.lang.StringBuilder sb, ch.qos.logback.classic.spi.IThrowableProxy iThrowableProxy) {
        printFirstLine(sb, iThrowableProxy);
        int commonFrames = iThrowableProxy.getCommonFrames();
        ch.qos.logback.classic.spi.StackTraceElementProxy[] stackTraceElementProxyArray = iThrowableProxy.getStackTraceElementProxyArray();
        for (int i = 0; i < stackTraceElementProxyArray.length - commonFrames; i++) {
            ch.qos.logback.classic.spi.StackTraceElementProxy stackTraceElementProxy = stackTraceElementProxyArray[i];
            sb.append(TRACE_PREFIX);
            sb.append(ch.qos.logback.core.helpers.Transform.escapeTags(stackTraceElementProxy.toString()));
            sb.append(ch.qos.logback.core.CoreConstants.LINE_SEPARATOR);
        }
        if (commonFrames > 0) {
            sb.append(TRACE_PREFIX);
            sb.append("\t... ").append(commonFrames).append(" common frames omitted").append(ch.qos.logback.core.CoreConstants.LINE_SEPARATOR);
        }
    }
}
