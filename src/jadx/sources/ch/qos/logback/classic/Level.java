package ch.qos.logback.classic;

public final class Level implements java.io.Serializable {
    public static final ch.qos.logback.classic.Level ALL = new ch.qos.logback.classic.Level(Integer.MIN_VALUE, "ALL");
    public static final int ALL_INT = Integer.MIN_VALUE;
    public static final java.lang.Integer ALL_INTEGER = java.lang.Integer.valueOf(Integer.MIN_VALUE);
    public static final ch.qos.logback.classic.Level DEBUG = new ch.qos.logback.classic.Level(10000, "DEBUG");
    public static final int DEBUG_INT = 10000;
    public static final java.lang.Integer DEBUG_INTEGER = java.lang.Integer.valueOf(10000);
    public static final ch.qos.logback.classic.Level ERROR = new ch.qos.logback.classic.Level(ERROR_INT, "ERROR");
    public static final int ERROR_INT = 40000;
    public static final java.lang.Integer ERROR_INTEGER = java.lang.Integer.valueOf(ERROR_INT);
    public static final ch.qos.logback.classic.Level INFO = new ch.qos.logback.classic.Level(INFO_INT, "INFO");
    public static final int INFO_INT = 20000;
    public static final java.lang.Integer INFO_INTEGER = java.lang.Integer.valueOf(INFO_INT);
    public static final ch.qos.logback.classic.Level OFF = new ch.qos.logback.classic.Level(Integer.MAX_VALUE, "OFF");
    public static final int OFF_INT = Integer.MAX_VALUE;
    public static final java.lang.Integer OFF_INTEGER = java.lang.Integer.valueOf(Integer.MAX_VALUE);
    public static final ch.qos.logback.classic.Level TRACE = new ch.qos.logback.classic.Level(5000, "TRACE");
    public static final int TRACE_INT = 5000;
    public static final java.lang.Integer TRACE_INTEGER = java.lang.Integer.valueOf(5000);
    public static final ch.qos.logback.classic.Level WARN = new ch.qos.logback.classic.Level(30000, "WARN");
    public static final int WARN_INT = 30000;
    public static final java.lang.Integer WARN_INTEGER = java.lang.Integer.valueOf(30000);
    private static final long serialVersionUID = -814092767334282137L;
    public final int levelInt;
    public final java.lang.String levelStr;

    private Level(int i, java.lang.String str) {
        this.levelInt = i;
        this.levelStr = str;
    }

    public static ch.qos.logback.classic.Level fromLocationAwareLoggerInteger(int i) {
        switch (i) {
            case 0:
                return TRACE;
            case 10:
                return DEBUG;
            case 20:
                return INFO;
            case 30:
                return WARN;
            case 40:
                return ERROR;
            default:
                throw new java.lang.IllegalArgumentException(i + " not a valid level value");
        }
    }

    private java.lang.Object readResolve() {
        return toLevel(this.levelInt);
    }

    public static ch.qos.logback.classic.Level toLevel(int i) {
        return toLevel(i, DEBUG);
    }

    public static ch.qos.logback.classic.Level toLevel(int i, ch.qos.logback.classic.Level level) {
        switch (i) {
            case Integer.MIN_VALUE:
                return ALL;
            case 5000:
                return TRACE;
            case 10000:
                return DEBUG;
            case INFO_INT /*20000*/:
                return INFO;
            case 30000:
                return WARN;
            case ERROR_INT /*40000*/:
                return ERROR;
            case Integer.MAX_VALUE:
                return OFF;
            default:
                return level;
        }
    }

    public static ch.qos.logback.classic.Level toLevel(java.lang.String str) {
        return toLevel(str, DEBUG);
    }

    public static ch.qos.logback.classic.Level toLevel(java.lang.String str, ch.qos.logback.classic.Level level) {
        return str == null ? level : str.equalsIgnoreCase("ALL") ? ALL : str.equalsIgnoreCase("TRACE") ? TRACE : str.equalsIgnoreCase("DEBUG") ? DEBUG : str.equalsIgnoreCase("INFO") ? INFO : str.equalsIgnoreCase("WARN") ? WARN : str.equalsIgnoreCase("ERROR") ? ERROR : str.equalsIgnoreCase("OFF") ? OFF : level;
    }

    public static int toLocationAwareLoggerInteger(ch.qos.logback.classic.Level level) {
        if (level == null) {
            throw new java.lang.IllegalArgumentException("null level parameter is not admitted");
        }
        switch (level.toInt()) {
            case 5000:
                return 0;
            case 10000:
                return 10;
            case INFO_INT /*20000*/:
                return 20;
            case 30000:
                return 30;
            case ERROR_INT /*40000*/:
                return 40;
            default:
                throw new java.lang.IllegalArgumentException(level + " not a valid level value");
        }
    }

    public static ch.qos.logback.classic.Level valueOf(java.lang.String str) {
        return toLevel(str, DEBUG);
    }

    public boolean isGreaterOrEqual(ch.qos.logback.classic.Level level) {
        return this.levelInt >= level.levelInt;
    }

    public int toInt() {
        return this.levelInt;
    }

    public java.lang.Integer toInteger() {
        switch (this.levelInt) {
            case Integer.MIN_VALUE:
                return ALL_INTEGER;
            case 5000:
                return TRACE_INTEGER;
            case 10000:
                return DEBUG_INTEGER;
            case INFO_INT /*20000*/:
                return INFO_INTEGER;
            case 30000:
                return WARN_INTEGER;
            case ERROR_INT /*40000*/:
                return ERROR_INTEGER;
            case Integer.MAX_VALUE:
                return OFF_INTEGER;
            default:
                throw new java.lang.IllegalStateException("Level " + this.levelStr + ", " + this.levelInt + " is unknown.");
        }
    }

    public java.lang.String toString() {
        return this.levelStr;
    }
}
