package ch.qos.logback.classic.sift;

public class SiftingAppender extends ch.qos.logback.core.sift.SiftingAppenderBase<ch.qos.logback.classic.spi.ILoggingEvent> {
    /* access modifiers changed from: protected */
    public boolean eventMarksEndOfLife(ch.qos.logback.classic.spi.ILoggingEvent iLoggingEvent) {
        org.slf4j.Marker marker = iLoggingEvent.getMarker();
        if (marker == null) {
            return false;
        }
        return marker.contains(ch.qos.logback.classic.ClassicConstants.FINALIZE_SESSION_MARKER);
    }

    /* access modifiers changed from: protected */
    public long getTimestamp(ch.qos.logback.classic.spi.ILoggingEvent iLoggingEvent) {
        return iLoggingEvent.getTimeStamp();
    }

    @ch.qos.logback.core.joran.spi.DefaultClass(ch.qos.logback.classic.sift.MDCBasedDiscriminator.class)
    public void setDiscriminator(ch.qos.logback.core.sift.Discriminator<ch.qos.logback.classic.spi.ILoggingEvent> discriminator) {
        super.setDiscriminator(discriminator);
    }
}
