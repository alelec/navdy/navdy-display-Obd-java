package ch.qos.logback.classic.sift;

public class SiftingJoranConfigurator extends ch.qos.logback.core.sift.SiftingJoranConfiguratorBase<ch.qos.logback.classic.spi.ILoggingEvent> {
    SiftingJoranConfigurator(java.lang.String str, java.lang.String str2, java.util.Map<java.lang.String, java.lang.String> map) {
        super(str, str2, map);
    }

    /* access modifiers changed from: protected */
    public void addDefaultNestedComponentRegistryRules(ch.qos.logback.core.joran.spi.DefaultNestedComponentRegistry defaultNestedComponentRegistry) {
        ch.qos.logback.classic.util.DefaultNestedComponentRules.addDefaultNestedComponentRegistryRules(defaultNestedComponentRegistry);
    }

    /* access modifiers changed from: protected */
    public void addInstanceRules(ch.qos.logback.core.joran.spi.RuleStore ruleStore) {
        super.addInstanceRules(ruleStore);
        ruleStore.addRule(new ch.qos.logback.core.joran.spi.ElementSelector("configuration/appender"), (ch.qos.logback.core.joran.action.Action) new ch.qos.logback.core.joran.action.AppenderAction());
    }

    /* access modifiers changed from: protected */
    public void buildInterpreter() {
        super.buildInterpreter();
        java.util.Map objectMap = this.interpreter.getInterpretationContext().getObjectMap();
        objectMap.put(ch.qos.logback.core.joran.action.ActionConst.APPENDER_BAG, new java.util.HashMap());
        objectMap.put(ch.qos.logback.core.joran.action.ActionConst.FILTER_CHAIN_BAG, new java.util.HashMap());
        java.util.HashMap hashMap = new java.util.HashMap();
        hashMap.putAll(this.parentPropertyMap);
        hashMap.put(this.key, this.value);
        this.interpreter.setInterpretationContextPropertiesMap(hashMap);
    }

    public ch.qos.logback.core.Appender<ch.qos.logback.classic.spi.ILoggingEvent> getAppender() {
        java.util.HashMap hashMap = (java.util.HashMap) this.interpreter.getInterpretationContext().getObjectMap().get(ch.qos.logback.core.joran.action.ActionConst.APPENDER_BAG);
        oneAndOnlyOneCheck(hashMap);
        java.util.Collection values = hashMap.values();
        if (values.size() == 0) {
            return null;
        }
        return (ch.qos.logback.core.Appender) values.iterator().next();
    }

    /* access modifiers changed from: protected */
    public ch.qos.logback.core.joran.spi.ElementPath initialElementPath() {
        return new ch.qos.logback.core.joran.spi.ElementPath("configuration");
    }
}
