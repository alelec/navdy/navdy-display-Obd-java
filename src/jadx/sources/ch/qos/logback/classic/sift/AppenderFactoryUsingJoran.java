package ch.qos.logback.classic.sift;

public class AppenderFactoryUsingJoran extends ch.qos.logback.core.sift.AbstractAppenderFactoryUsingJoran<ch.qos.logback.classic.spi.ILoggingEvent> {
    AppenderFactoryUsingJoran(java.util.List<ch.qos.logback.core.joran.event.SaxEvent> list, java.lang.String str, java.util.Map<java.lang.String, java.lang.String> map) {
        super(list, str, map);
    }

    public ch.qos.logback.core.sift.SiftingJoranConfiguratorBase<ch.qos.logback.classic.spi.ILoggingEvent> getSiftingJoranConfigurator(java.lang.String str) {
        return new ch.qos.logback.classic.sift.SiftingJoranConfigurator(this.key, str, this.parentPropertyMap);
    }
}
