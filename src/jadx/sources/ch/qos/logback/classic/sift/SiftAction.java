package ch.qos.logback.classic.sift;

public class SiftAction extends ch.qos.logback.core.joran.action.Action implements ch.qos.logback.core.joran.event.InPlayListener {
    java.util.List<ch.qos.logback.core.joran.event.SaxEvent> seList;

    public void begin(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext, java.lang.String str, org.xml.sax.Attributes attributes) throws ch.qos.logback.core.joran.spi.ActionException {
        this.seList = new java.util.ArrayList();
        interpretationContext.addInPlayListener(this);
    }

    public void end(ch.qos.logback.core.joran.spi.InterpretationContext interpretationContext, java.lang.String str) throws ch.qos.logback.core.joran.spi.ActionException {
        interpretationContext.removeInPlayListener(this);
        java.lang.Object peekObject = interpretationContext.peekObject();
        if (peekObject instanceof ch.qos.logback.classic.sift.SiftingAppender) {
            ch.qos.logback.classic.sift.SiftingAppender siftingAppender = (ch.qos.logback.classic.sift.SiftingAppender) peekObject;
            siftingAppender.setAppenderFactory(new ch.qos.logback.classic.sift.AppenderFactoryUsingJoran(this.seList, siftingAppender.getDiscriminatorKey(), interpretationContext.getCopyOfPropertyMap()));
        }
    }

    public java.util.List<ch.qos.logback.core.joran.event.SaxEvent> getSeList() {
        return this.seList;
    }

    public void inPlay(ch.qos.logback.core.joran.event.SaxEvent saxEvent) {
        this.seList.add(saxEvent);
    }
}
