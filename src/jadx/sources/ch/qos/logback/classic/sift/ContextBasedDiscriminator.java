package ch.qos.logback.classic.sift;

public class ContextBasedDiscriminator extends ch.qos.logback.core.sift.AbstractDiscriminator<ch.qos.logback.classic.spi.ILoggingEvent> {
    private static final java.lang.String KEY = "contextName";
    private java.lang.String defaultValue;

    public java.lang.String getDefaultValue() {
        return this.defaultValue;
    }

    public java.lang.String getDiscriminatingValue(ch.qos.logback.classic.spi.ILoggingEvent iLoggingEvent) {
        java.lang.String name = iLoggingEvent.getLoggerContextVO().getName();
        return name == null ? this.defaultValue : name;
    }

    public java.lang.String getKey() {
        return KEY;
    }

    public void setDefaultValue(java.lang.String str) {
        this.defaultValue = str;
    }

    public void setKey(java.lang.String str) {
        throw new java.lang.UnsupportedOperationException("Key cannot be set. Using fixed key contextName");
    }
}
