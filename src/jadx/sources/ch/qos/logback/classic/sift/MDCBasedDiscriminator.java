package ch.qos.logback.classic.sift;

public class MDCBasedDiscriminator extends ch.qos.logback.core.sift.AbstractDiscriminator<ch.qos.logback.classic.spi.ILoggingEvent> {
    private java.lang.String defaultValue;
    private java.lang.String key;

    public java.lang.String getDefaultValue() {
        return this.defaultValue;
    }

    public java.lang.String getDiscriminatingValue(ch.qos.logback.classic.spi.ILoggingEvent iLoggingEvent) {
        java.util.Map mDCPropertyMap = iLoggingEvent.getMDCPropertyMap();
        if (mDCPropertyMap == null) {
            return this.defaultValue;
        }
        java.lang.String str = (java.lang.String) mDCPropertyMap.get(this.key);
        return str == null ? this.defaultValue : str;
    }

    public java.lang.String getKey() {
        return this.key;
    }

    public void setDefaultValue(java.lang.String str) {
        this.defaultValue = str;
    }

    public void setKey(java.lang.String str) {
        this.key = str;
    }

    public void start() {
        int i = 0;
        if (ch.qos.logback.core.util.OptionHelper.isEmpty(this.key)) {
            addError("The \"Key\" property must be set");
            i = 1;
        }
        if (ch.qos.logback.core.util.OptionHelper.isEmpty(this.defaultValue)) {
            i++;
            addError("The \"DefaultValue\" property must be set");
        }
        if (i == 0) {
            this.started = true;
        }
    }
}
