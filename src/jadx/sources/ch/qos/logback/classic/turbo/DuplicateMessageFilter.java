package ch.qos.logback.classic.turbo;

public class DuplicateMessageFilter extends ch.qos.logback.classic.turbo.TurboFilter {
    public static final int DEFAULT_ALLOWED_REPETITIONS = 5;
    public static final int DEFAULT_CACHE_SIZE = 100;
    public int allowedRepetitions = 5;
    public int cacheSize = 100;
    private ch.qos.logback.classic.turbo.LRUMessageCache msgCache;

    public ch.qos.logback.core.spi.FilterReply decide(org.slf4j.Marker marker, ch.qos.logback.classic.Logger logger, ch.qos.logback.classic.Level level, java.lang.String str, java.lang.Object[] objArr, java.lang.Throwable th) {
        return this.msgCache.getMessageCountAndThenIncrement(str) <= this.allowedRepetitions ? ch.qos.logback.core.spi.FilterReply.NEUTRAL : ch.qos.logback.core.spi.FilterReply.DENY;
    }

    public int getAllowedRepetitions() {
        return this.allowedRepetitions;
    }

    public int getCacheSize() {
        return this.cacheSize;
    }

    public void setAllowedRepetitions(int i) {
        this.allowedRepetitions = i;
    }

    public void setCacheSize(int i) {
        this.cacheSize = i;
    }

    public void start() {
        this.msgCache = new ch.qos.logback.classic.turbo.LRUMessageCache(this.cacheSize);
        super.start();
    }

    public void stop() {
        this.msgCache.clear();
        this.msgCache = null;
        super.stop();
    }
}
