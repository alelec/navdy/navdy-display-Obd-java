package ch.qos.logback.classic.turbo;

public class MDCFilter extends ch.qos.logback.classic.turbo.MatchingFilter {
    java.lang.String MDCKey;
    java.lang.String value;

    public ch.qos.logback.core.spi.FilterReply decide(org.slf4j.Marker marker, ch.qos.logback.classic.Logger logger, ch.qos.logback.classic.Level level, java.lang.String str, java.lang.Object[] objArr, java.lang.Throwable th) {
        if (this.MDCKey == null) {
            return ch.qos.logback.core.spi.FilterReply.NEUTRAL;
        }
        return this.value.equals(org.slf4j.MDC.get(this.MDCKey)) ? this.onMatch : this.onMismatch;
    }

    public void setMDCKey(java.lang.String str) {
        this.MDCKey = str;
    }

    public void setValue(java.lang.String str) {
        this.value = str;
    }
}
