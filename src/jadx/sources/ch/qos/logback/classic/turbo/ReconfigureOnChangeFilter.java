package ch.qos.logback.classic.turbo;

public class ReconfigureOnChangeFilter extends ch.qos.logback.classic.turbo.TurboFilter {
    public static final long DEFAULT_REFRESH_PERIOD = 60000;
    private static final long MASK_DECREASE_THRESHOLD = 800;
    private static final long MASK_INCREASE_THRESHOLD = 100;
    private static final int MAX_MASK = 65535;
    ch.qos.logback.core.joran.spi.ConfigurationWatchList configurationWatchList;
    private long invocationCounter = 0;
    private volatile long lastMaskCheck = java.lang.System.currentTimeMillis();
    java.net.URL mainConfigurationURL;
    private volatile long mask = 15;
    protected volatile long nextCheck;
    long refreshPeriod = DEFAULT_REFRESH_PERIOD;

    class ReconfiguringThread implements java.lang.Runnable {
        ReconfiguringThread() {
        }

        private void fallbackConfiguration(ch.qos.logback.classic.LoggerContext loggerContext, java.util.List<ch.qos.logback.core.joran.event.SaxEvent> list, java.net.URL url) {
            ch.qos.logback.classic.joran.JoranConfigurator joranConfigurator = new ch.qos.logback.classic.joran.JoranConfigurator();
            joranConfigurator.setContext(ch.qos.logback.classic.turbo.ReconfigureOnChangeFilter.this.context);
            if (list != null) {
                ch.qos.logback.classic.turbo.ReconfigureOnChangeFilter.this.addWarn("Falling back to previously registered safe configuration.");
                try {
                    loggerContext.reset();
                    ch.qos.logback.classic.joran.JoranConfigurator.informContextOfURLUsedForConfiguration(ch.qos.logback.classic.turbo.ReconfigureOnChangeFilter.this.context, url);
                    joranConfigurator.doConfigure((java.util.List) list);
                    ch.qos.logback.classic.turbo.ReconfigureOnChangeFilter.this.addInfo("Re-registering previous fallback configuration once more as a fallback configuration point");
                    joranConfigurator.registerSafeConfiguration();
                } catch (ch.qos.logback.core.joran.spi.JoranException e) {
                    ch.qos.logback.classic.turbo.ReconfigureOnChangeFilter.this.addError("Unexpected exception thrown by a configuration considered safe.", e);
                }
            } else {
                ch.qos.logback.classic.turbo.ReconfigureOnChangeFilter.this.addWarn("No previous configuration to fall back on.");
            }
        }

        private void performXMLConfiguration(ch.qos.logback.classic.LoggerContext loggerContext) {
            ch.qos.logback.classic.joran.JoranConfigurator joranConfigurator = new ch.qos.logback.classic.joran.JoranConfigurator();
            joranConfigurator.setContext(ch.qos.logback.classic.turbo.ReconfigureOnChangeFilter.this.context);
            ch.qos.logback.core.status.StatusUtil statusUtil = new ch.qos.logback.core.status.StatusUtil(ch.qos.logback.classic.turbo.ReconfigureOnChangeFilter.this.context);
            java.util.List recallSafeConfiguration = joranConfigurator.recallSafeConfiguration();
            java.net.URL mainWatchURL = ch.qos.logback.core.joran.util.ConfigurationWatchListUtil.getMainWatchURL(ch.qos.logback.classic.turbo.ReconfigureOnChangeFilter.this.context);
            loggerContext.reset();
            long currentTimeMillis = java.lang.System.currentTimeMillis();
            try {
                joranConfigurator.doConfigure(ch.qos.logback.classic.turbo.ReconfigureOnChangeFilter.this.mainConfigurationURL);
                if (statusUtil.hasXMLParsingErrors(currentTimeMillis)) {
                    fallbackConfiguration(loggerContext, recallSafeConfiguration, mainWatchURL);
                }
            } catch (ch.qos.logback.core.joran.spi.JoranException e) {
                fallbackConfiguration(loggerContext, recallSafeConfiguration, mainWatchURL);
            }
        }

        public void run() {
            if (ch.qos.logback.classic.turbo.ReconfigureOnChangeFilter.this.mainConfigurationURL == null) {
                ch.qos.logback.classic.turbo.ReconfigureOnChangeFilter.this.addInfo("Due to missing top level configuration file, skipping reconfiguration");
                return;
            }
            ch.qos.logback.classic.LoggerContext loggerContext = (ch.qos.logback.classic.LoggerContext) ch.qos.logback.classic.turbo.ReconfigureOnChangeFilter.this.context;
            ch.qos.logback.classic.turbo.ReconfigureOnChangeFilter.this.addInfo("Will reset and reconfigure context named [" + ch.qos.logback.classic.turbo.ReconfigureOnChangeFilter.this.context.getName() + "]");
            if (ch.qos.logback.classic.turbo.ReconfigureOnChangeFilter.this.mainConfigurationURL.toString().endsWith("xml")) {
                performXMLConfiguration(loggerContext);
            }
        }
    }

    private void updateMaskIfNecessary(long j) {
        long j2 = j - this.lastMaskCheck;
        this.lastMaskCheck = j;
        if (j2 < 100 && this.mask < 65535) {
            this.mask = (this.mask << 1) | 1;
        } else if (j2 > MASK_DECREASE_THRESHOLD) {
            this.mask >>>= 2;
        }
    }

    /* access modifiers changed from: protected */
    public boolean changeDetected(long j) {
        if (j < this.nextCheck) {
            return false;
        }
        updateNextCheck(j);
        return this.configurationWatchList.changeDetected();
    }

    public ch.qos.logback.core.spi.FilterReply decide(org.slf4j.Marker marker, ch.qos.logback.classic.Logger logger, ch.qos.logback.classic.Level level, java.lang.String str, java.lang.Object[] objArr, java.lang.Throwable th) {
        if (!isStarted()) {
            return ch.qos.logback.core.spi.FilterReply.NEUTRAL;
        }
        long j = this.invocationCounter;
        this.invocationCounter = 1 + j;
        if ((j & this.mask) != this.mask) {
            return ch.qos.logback.core.spi.FilterReply.NEUTRAL;
        }
        long currentTimeMillis = java.lang.System.currentTimeMillis();
        synchronized (this.configurationWatchList) {
            updateMaskIfNecessary(currentTimeMillis);
            if (changeDetected(currentTimeMillis)) {
                disableSubsequentReconfiguration();
                detachReconfigurationToNewThread();
            }
        }
        return ch.qos.logback.core.spi.FilterReply.NEUTRAL;
    }

    /* access modifiers changed from: 0000 */
    public void detachReconfigurationToNewThread() {
        addInfo("Detected change in [" + this.configurationWatchList.getCopyOfFileWatchList() + "]");
        this.context.getExecutorService().submit(new ch.qos.logback.classic.turbo.ReconfigureOnChangeFilter.ReconfiguringThread());
    }

    /* access modifiers changed from: 0000 */
    public void disableSubsequentReconfiguration() {
        this.nextCheck = kotlin.jvm.internal.LongCompanionObject.MAX_VALUE;
    }

    public long getRefreshPeriod() {
        return this.refreshPeriod;
    }

    public void setRefreshPeriod(long j) {
        this.refreshPeriod = j;
    }

    public void start() {
        this.configurationWatchList = ch.qos.logback.core.joran.util.ConfigurationWatchListUtil.getConfigurationWatchList(this.context);
        if (this.configurationWatchList != null) {
            this.mainConfigurationURL = this.configurationWatchList.getMainURL();
            if (this.mainConfigurationURL == null) {
                addWarn("Due to missing top level configuration file, automatic reconfiguration is impossible.");
                return;
            }
            long j = this.refreshPeriod / 1000;
            addInfo("Will scan for changes in [" + this.configurationWatchList.getCopyOfFileWatchList() + "] every " + j + " seconds. ");
            synchronized (this.configurationWatchList) {
                updateNextCheck(java.lang.System.currentTimeMillis());
            }
            super.start();
            return;
        }
        addWarn("Empty ConfigurationWatchList in context");
    }

    public java.lang.String toString() {
        return "ReconfigureOnChangeFilter{invocationCounter=" + this.invocationCounter + ch.qos.logback.core.CoreConstants.CURLY_RIGHT;
    }

    /* access modifiers changed from: 0000 */
    public void updateNextCheck(long j) {
        this.nextCheck = this.refreshPeriod + j;
    }
}
