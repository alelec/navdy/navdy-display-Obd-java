package ch.qos.logback.classic.turbo;

class LRUMessageCache extends java.util.LinkedHashMap<java.lang.String, java.lang.Integer> {
    private static final long serialVersionUID = 1;
    final int cacheSize;

    LRUMessageCache(int i) {
        super((int) (((float) i) * 1.3333334f), 0.75f, true);
        if (i < 1) {
            throw new java.lang.IllegalArgumentException("Cache size cannot be smaller than 1");
        }
        this.cacheSize = i;
    }

    public synchronized void clear() {
        super.clear();
    }

    /* access modifiers changed from: 0000 */
    public int getMessageCountAndThenIncrement(java.lang.String str) {
        java.lang.Integer valueOf;
        if (str == null) {
            return 0;
        }
        synchronized (this) {
            java.lang.Integer num = (java.lang.Integer) super.get(str);
            valueOf = num == null ? java.lang.Integer.valueOf(0) : java.lang.Integer.valueOf(num.intValue() + 1);
            super.put(str, valueOf);
        }
        return valueOf.intValue();
    }

    /* access modifiers changed from: protected */
    public boolean removeEldestEntry(java.util.Map.Entry entry) {
        return size() > this.cacheSize;
    }
}
