package ch.qos.logback.classic.turbo;

public class DynamicThresholdFilter extends ch.qos.logback.classic.turbo.TurboFilter {
    private ch.qos.logback.classic.Level defaultThreshold = ch.qos.logback.classic.Level.ERROR;
    private java.lang.String key;
    private ch.qos.logback.core.spi.FilterReply onHigherOrEqual = ch.qos.logback.core.spi.FilterReply.NEUTRAL;
    private ch.qos.logback.core.spi.FilterReply onLower = ch.qos.logback.core.spi.FilterReply.DENY;
    private java.util.Map<java.lang.String, ch.qos.logback.classic.Level> valueLevelMap = new java.util.HashMap();

    public void addMDCValueLevelPair(ch.qos.logback.classic.turbo.MDCValueLevelPair mDCValueLevelPair) {
        if (this.valueLevelMap.containsKey(mDCValueLevelPair.getValue())) {
            addError(mDCValueLevelPair.getValue() + " has been already set");
        } else {
            this.valueLevelMap.put(mDCValueLevelPair.getValue(), mDCValueLevelPair.getLevel());
        }
    }

    public ch.qos.logback.core.spi.FilterReply decide(org.slf4j.Marker marker, ch.qos.logback.classic.Logger logger, ch.qos.logback.classic.Level level, java.lang.String str, java.lang.Object[] objArr, java.lang.Throwable th) {
        java.lang.String str2 = org.slf4j.MDC.get(this.key);
        if (!isStarted()) {
            return ch.qos.logback.core.spi.FilterReply.NEUTRAL;
        }
        ch.qos.logback.classic.Level level2 = null;
        if (str2 != null) {
            level2 = (ch.qos.logback.classic.Level) this.valueLevelMap.get(str2);
        }
        if (level2 == null) {
            level2 = this.defaultThreshold;
        }
        return level.isGreaterOrEqual(level2) ? this.onHigherOrEqual : this.onLower;
    }

    public ch.qos.logback.classic.Level getDefaultThreshold() {
        return this.defaultThreshold;
    }

    public java.lang.String getKey() {
        return this.key;
    }

    public ch.qos.logback.core.spi.FilterReply getOnHigherOrEqual() {
        return this.onHigherOrEqual;
    }

    public ch.qos.logback.core.spi.FilterReply getOnLower() {
        return this.onLower;
    }

    public void setDefaultThreshold(ch.qos.logback.classic.Level level) {
        this.defaultThreshold = level;
    }

    public void setKey(java.lang.String str) {
        this.key = str;
    }

    public void setOnHigherOrEqual(ch.qos.logback.core.spi.FilterReply filterReply) {
        this.onHigherOrEqual = filterReply;
    }

    public void setOnLower(ch.qos.logback.core.spi.FilterReply filterReply) {
        this.onLower = filterReply;
    }

    public void start() {
        if (this.key == null) {
            addError("No key name was specified");
        }
        super.start();
    }
}
