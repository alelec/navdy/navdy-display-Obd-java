package ch.qos.logback.classic.turbo;

public class MarkerFilter extends ch.qos.logback.classic.turbo.MatchingFilter {
    org.slf4j.Marker markerToMatch;

    public ch.qos.logback.core.spi.FilterReply decide(org.slf4j.Marker marker, ch.qos.logback.classic.Logger logger, ch.qos.logback.classic.Level level, java.lang.String str, java.lang.Object[] objArr, java.lang.Throwable th) {
        return !isStarted() ? ch.qos.logback.core.spi.FilterReply.NEUTRAL : marker == null ? this.onMismatch : marker.contains(this.markerToMatch) ? this.onMatch : this.onMismatch;
    }

    public void setMarker(java.lang.String str) {
        if (str != null) {
            this.markerToMatch = org.slf4j.MarkerFactory.getMarker(str);
        }
    }

    public void start() {
        if (this.markerToMatch != null) {
            super.start();
        } else {
            addError("The marker property must be set for [" + getName() + "]");
        }
    }
}
