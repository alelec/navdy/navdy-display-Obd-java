package ch.qos.logback.classic.turbo;

public class MDCValueLevelPair {
    private ch.qos.logback.classic.Level level;
    private java.lang.String value;

    public ch.qos.logback.classic.Level getLevel() {
        return this.level;
    }

    public java.lang.String getValue() {
        return this.value;
    }

    public void setLevel(ch.qos.logback.classic.Level level2) {
        this.level = level2;
    }

    public void setValue(java.lang.String str) {
        this.value = str;
    }
}
