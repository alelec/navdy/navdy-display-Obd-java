package ch.qos.logback.classic.turbo;

public abstract class TurboFilter extends ch.qos.logback.core.spi.ContextAwareBase implements ch.qos.logback.core.spi.LifeCycle {
    private java.lang.String name;
    boolean start = false;

    public abstract ch.qos.logback.core.spi.FilterReply decide(org.slf4j.Marker marker, ch.qos.logback.classic.Logger logger, ch.qos.logback.classic.Level level, java.lang.String str, java.lang.Object[] objArr, java.lang.Throwable th);

    public java.lang.String getName() {
        return this.name;
    }

    public boolean isStarted() {
        return this.start;
    }

    public void setName(java.lang.String str) {
        this.name = str;
    }

    public void start() {
        this.start = true;
    }

    public void stop() {
        this.start = false;
    }
}
