package ch.qos.logback.classic.turbo;

public abstract class MatchingFilter extends ch.qos.logback.classic.turbo.TurboFilter {
    protected ch.qos.logback.core.spi.FilterReply onMatch = ch.qos.logback.core.spi.FilterReply.NEUTRAL;
    protected ch.qos.logback.core.spi.FilterReply onMismatch = ch.qos.logback.core.spi.FilterReply.NEUTRAL;

    public final void setOnMatch(java.lang.String str) {
        if ("NEUTRAL".equals(str)) {
            this.onMatch = ch.qos.logback.core.spi.FilterReply.NEUTRAL;
        } else if ("ACCEPT".equals(str)) {
            this.onMatch = ch.qos.logback.core.spi.FilterReply.ACCEPT;
        } else if ("DENY".equals(str)) {
            this.onMatch = ch.qos.logback.core.spi.FilterReply.DENY;
        }
    }

    public final void setOnMismatch(java.lang.String str) {
        if ("NEUTRAL".equals(str)) {
            this.onMismatch = ch.qos.logback.core.spi.FilterReply.NEUTRAL;
        } else if ("ACCEPT".equals(str)) {
            this.onMismatch = ch.qos.logback.core.spi.FilterReply.ACCEPT;
        } else if ("DENY".equals(str)) {
            this.onMismatch = ch.qos.logback.core.spi.FilterReply.DENY;
        }
    }
}
