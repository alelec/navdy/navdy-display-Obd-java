package ch.qos.logback.classic;

public final class Logger implements org.slf4j.Logger, org.slf4j.spi.LocationAwareLogger, ch.qos.logback.core.spi.AppenderAttachable<ch.qos.logback.classic.spi.ILoggingEvent>, java.io.Serializable {
    private static final int DEFAULT_CHILD_ARRAY_SIZE = 5;
    public static final java.lang.String FQCN = ch.qos.logback.classic.Logger.class.getName();
    private static final long serialVersionUID = 5454405123156820674L;
    private transient ch.qos.logback.core.spi.AppenderAttachableImpl<ch.qos.logback.classic.spi.ILoggingEvent> aai;
    private transient boolean additive = true;
    private transient java.util.List<ch.qos.logback.classic.Logger> childrenList;
    private transient int effectiveLevelInt;
    private transient ch.qos.logback.classic.Level level;
    final transient ch.qos.logback.classic.LoggerContext loggerContext;
    private java.lang.String name;
    private transient ch.qos.logback.classic.Logger parent;

    Logger(java.lang.String str, ch.qos.logback.classic.Logger logger, ch.qos.logback.classic.LoggerContext loggerContext2) {
        this.name = str;
        this.parent = logger;
        this.loggerContext = loggerContext2;
    }

    private int appendLoopOnAppenders(ch.qos.logback.classic.spi.ILoggingEvent iLoggingEvent) {
        if (this.aai != null) {
            return this.aai.appendLoopOnAppenders(iLoggingEvent);
        }
        return 0;
    }

    private void buildLoggingEventAndAppend(java.lang.String str, org.slf4j.Marker marker, ch.qos.logback.classic.Level level2, java.lang.String str2, java.lang.Object[] objArr, java.lang.Throwable th) {
        ch.qos.logback.classic.spi.LoggingEvent loggingEvent = new ch.qos.logback.classic.spi.LoggingEvent(str, this, level2, str2, th, objArr);
        loggingEvent.setMarker(marker);
        callAppenders(loggingEvent);
    }

    private ch.qos.logback.core.spi.FilterReply callTurboFilters(org.slf4j.Marker marker, ch.qos.logback.classic.Level level2) {
        return this.loggerContext.getTurboFilterChainDecision_0_3OrMore(marker, this, level2, null, null, null);
    }

    private void filterAndLog_0_Or3Plus(java.lang.String str, org.slf4j.Marker marker, ch.qos.logback.classic.Level level2, java.lang.String str2, java.lang.Object[] objArr, java.lang.Throwable th) {
        ch.qos.logback.core.spi.FilterReply turboFilterChainDecision_0_3OrMore = this.loggerContext.getTurboFilterChainDecision_0_3OrMore(marker, this, level2, str2, objArr, th);
        if (turboFilterChainDecision_0_3OrMore == ch.qos.logback.core.spi.FilterReply.NEUTRAL) {
            if (this.effectiveLevelInt > level2.levelInt) {
                return;
            }
        } else if (turboFilterChainDecision_0_3OrMore == ch.qos.logback.core.spi.FilterReply.DENY) {
            return;
        }
        buildLoggingEventAndAppend(str, marker, level2, str2, objArr, th);
    }

    private void filterAndLog_1(java.lang.String str, org.slf4j.Marker marker, ch.qos.logback.classic.Level level2, java.lang.String str2, java.lang.Object obj, java.lang.Throwable th) {
        ch.qos.logback.core.spi.FilterReply turboFilterChainDecision_1 = this.loggerContext.getTurboFilterChainDecision_1(marker, this, level2, str2, obj, th);
        if (turboFilterChainDecision_1 == ch.qos.logback.core.spi.FilterReply.NEUTRAL) {
            if (this.effectiveLevelInt > level2.levelInt) {
                return;
            }
        } else if (turboFilterChainDecision_1 == ch.qos.logback.core.spi.FilterReply.DENY) {
            return;
        }
        buildLoggingEventAndAppend(str, marker, level2, str2, new java.lang.Object[]{obj}, th);
    }

    private void filterAndLog_2(java.lang.String str, org.slf4j.Marker marker, ch.qos.logback.classic.Level level2, java.lang.String str2, java.lang.Object obj, java.lang.Object obj2, java.lang.Throwable th) {
        ch.qos.logback.core.spi.FilterReply turboFilterChainDecision_2 = this.loggerContext.getTurboFilterChainDecision_2(marker, this, level2, str2, obj, obj2, th);
        if (turboFilterChainDecision_2 == ch.qos.logback.core.spi.FilterReply.NEUTRAL) {
            if (this.effectiveLevelInt > level2.levelInt) {
                return;
            }
        } else if (turboFilterChainDecision_2 == ch.qos.logback.core.spi.FilterReply.DENY) {
            return;
        }
        buildLoggingEventAndAppend(str, marker, level2, str2, new java.lang.Object[]{obj, obj2}, th);
    }

    private synchronized void handleParentLevelChange(int i) {
        if (this.level == null) {
            this.effectiveLevelInt = i;
            if (this.childrenList != null) {
                int size = this.childrenList.size();
                for (int i2 = 0; i2 < size; i2++) {
                    ((ch.qos.logback.classic.Logger) this.childrenList.get(i2)).handleParentLevelChange(i);
                }
            }
        }
    }

    private boolean isRootLogger() {
        return this.parent == null;
    }

    private void localLevelReset() {
        this.effectiveLevelInt = 10000;
        if (isRootLogger()) {
            this.level = ch.qos.logback.classic.Level.DEBUG;
        } else {
            this.level = null;
        }
    }

    public synchronized void addAppender(ch.qos.logback.core.Appender<ch.qos.logback.classic.spi.ILoggingEvent> appender) {
        if (this.aai == null) {
            this.aai = new ch.qos.logback.core.spi.AppenderAttachableImpl<>();
        }
        this.aai.addAppender(appender);
    }

    public void callAppenders(ch.qos.logback.classic.spi.ILoggingEvent iLoggingEvent) {
        int i = 0;
        for (ch.qos.logback.classic.Logger logger = this; logger != null; logger = logger.parent) {
            i += logger.appendLoopOnAppenders(iLoggingEvent);
            if (!logger.additive) {
                break;
            }
        }
        if (i == 0) {
            this.loggerContext.noAppenderDefinedWarning(this);
        }
    }

    /* access modifiers changed from: 0000 */
    public ch.qos.logback.classic.Logger createChildByLastNamePart(java.lang.String str) {
        if (ch.qos.logback.classic.util.LoggerNameUtil.getFirstSeparatorIndexOf(str) != -1) {
            throw new java.lang.IllegalArgumentException("Child name [" + str + " passed as parameter, may not include [" + ch.qos.logback.core.CoreConstants.DOT + "]");
        }
        if (this.childrenList == null) {
            this.childrenList = new java.util.ArrayList();
        }
        ch.qos.logback.classic.Logger logger = isRootLogger() ? new ch.qos.logback.classic.Logger(str, this, this.loggerContext) : new ch.qos.logback.classic.Logger(this.name + ch.qos.logback.core.CoreConstants.DOT + str, this, this.loggerContext);
        this.childrenList.add(logger);
        logger.effectiveLevelInt = this.effectiveLevelInt;
        return logger;
    }

    /* access modifiers changed from: 0000 */
    public ch.qos.logback.classic.Logger createChildByName(java.lang.String str) {
        if (ch.qos.logback.classic.util.LoggerNameUtil.getSeparatorIndexOf(str, this.name.length() + 1) != -1) {
            throw new java.lang.IllegalArgumentException("For logger [" + this.name + "] child name [" + str + " passed as parameter, may not include '.' after index" + (this.name.length() + 1));
        }
        if (this.childrenList == null) {
            this.childrenList = new java.util.ArrayList(5);
        }
        ch.qos.logback.classic.Logger logger = new ch.qos.logback.classic.Logger(str, this, this.loggerContext);
        this.childrenList.add(logger);
        logger.effectiveLevelInt = this.effectiveLevelInt;
        return logger;
    }

    public void debug(java.lang.String str) {
        filterAndLog_0_Or3Plus(FQCN, null, ch.qos.logback.classic.Level.DEBUG, str, null, null);
    }

    public void debug(java.lang.String str, java.lang.Object obj) {
        filterAndLog_1(FQCN, null, ch.qos.logback.classic.Level.DEBUG, str, obj, null);
    }

    public void debug(java.lang.String str, java.lang.Object obj, java.lang.Object obj2) {
        filterAndLog_2(FQCN, null, ch.qos.logback.classic.Level.DEBUG, str, obj, obj2, null);
    }

    public void debug(java.lang.String str, java.lang.Throwable th) {
        filterAndLog_0_Or3Plus(FQCN, null, ch.qos.logback.classic.Level.DEBUG, str, null, th);
    }

    public void debug(java.lang.String str, java.lang.Object[] objArr) {
        filterAndLog_0_Or3Plus(FQCN, null, ch.qos.logback.classic.Level.DEBUG, str, objArr, null);
    }

    public void debug(org.slf4j.Marker marker, java.lang.String str) {
        filterAndLog_0_Or3Plus(FQCN, marker, ch.qos.logback.classic.Level.DEBUG, str, null, null);
    }

    public void debug(org.slf4j.Marker marker, java.lang.String str, java.lang.Object obj) {
        filterAndLog_1(FQCN, marker, ch.qos.logback.classic.Level.DEBUG, str, obj, null);
    }

    public void debug(org.slf4j.Marker marker, java.lang.String str, java.lang.Object obj, java.lang.Object obj2) {
        filterAndLog_2(FQCN, marker, ch.qos.logback.classic.Level.DEBUG, str, obj, obj2, null);
    }

    public void debug(org.slf4j.Marker marker, java.lang.String str, java.lang.Throwable th) {
        filterAndLog_0_Or3Plus(FQCN, marker, ch.qos.logback.classic.Level.DEBUG, str, null, th);
    }

    public void debug(org.slf4j.Marker marker, java.lang.String str, java.lang.Object[] objArr) {
        filterAndLog_0_Or3Plus(FQCN, marker, ch.qos.logback.classic.Level.DEBUG, str, objArr, null);
    }

    public void detachAndStopAllAppenders() {
        if (this.aai != null) {
            this.aai.detachAndStopAllAppenders();
        }
    }

    public boolean detachAppender(ch.qos.logback.core.Appender<ch.qos.logback.classic.spi.ILoggingEvent> appender) {
        if (this.aai == null) {
            return false;
        }
        return this.aai.detachAppender(appender);
    }

    public boolean detachAppender(java.lang.String str) {
        if (this.aai == null) {
            return false;
        }
        return this.aai.detachAppender(str);
    }

    public void error(java.lang.String str) {
        filterAndLog_0_Or3Plus(FQCN, null, ch.qos.logback.classic.Level.ERROR, str, null, null);
    }

    public void error(java.lang.String str, java.lang.Object obj) {
        filterAndLog_1(FQCN, null, ch.qos.logback.classic.Level.ERROR, str, obj, null);
    }

    public void error(java.lang.String str, java.lang.Object obj, java.lang.Object obj2) {
        filterAndLog_2(FQCN, null, ch.qos.logback.classic.Level.ERROR, str, obj, obj2, null);
    }

    public void error(java.lang.String str, java.lang.Throwable th) {
        filterAndLog_0_Or3Plus(FQCN, null, ch.qos.logback.classic.Level.ERROR, str, null, th);
    }

    public void error(java.lang.String str, java.lang.Object[] objArr) {
        filterAndLog_0_Or3Plus(FQCN, null, ch.qos.logback.classic.Level.ERROR, str, objArr, null);
    }

    public void error(org.slf4j.Marker marker, java.lang.String str) {
        filterAndLog_0_Or3Plus(FQCN, marker, ch.qos.logback.classic.Level.ERROR, str, null, null);
    }

    public void error(org.slf4j.Marker marker, java.lang.String str, java.lang.Object obj) {
        filterAndLog_1(FQCN, marker, ch.qos.logback.classic.Level.ERROR, str, obj, null);
    }

    public void error(org.slf4j.Marker marker, java.lang.String str, java.lang.Object obj, java.lang.Object obj2) {
        filterAndLog_2(FQCN, marker, ch.qos.logback.classic.Level.ERROR, str, obj, obj2, null);
    }

    public void error(org.slf4j.Marker marker, java.lang.String str, java.lang.Throwable th) {
        filterAndLog_0_Or3Plus(FQCN, marker, ch.qos.logback.classic.Level.ERROR, str, null, th);
    }

    public void error(org.slf4j.Marker marker, java.lang.String str, java.lang.Object[] objArr) {
        filterAndLog_0_Or3Plus(FQCN, marker, ch.qos.logback.classic.Level.ERROR, str, objArr, null);
    }

    public ch.qos.logback.core.Appender<ch.qos.logback.classic.spi.ILoggingEvent> getAppender(java.lang.String str) {
        if (this.aai == null) {
            return null;
        }
        return this.aai.getAppender(str);
    }

    /* access modifiers changed from: 0000 */
    public ch.qos.logback.classic.Logger getChildByName(java.lang.String str) {
        if (this.childrenList == null) {
            return null;
        }
        int size = this.childrenList.size();
        for (int i = 0; i < size; i++) {
            ch.qos.logback.classic.Logger logger = (ch.qos.logback.classic.Logger) this.childrenList.get(i);
            if (str.equals(logger.getName())) {
                return logger;
            }
        }
        return null;
    }

    public ch.qos.logback.classic.Level getEffectiveLevel() {
        return ch.qos.logback.classic.Level.toLevel(this.effectiveLevelInt);
    }

    /* access modifiers changed from: 0000 */
    public int getEffectiveLevelInt() {
        return this.effectiveLevelInt;
    }

    public ch.qos.logback.classic.Level getLevel() {
        return this.level;
    }

    public ch.qos.logback.classic.LoggerContext getLoggerContext() {
        return this.loggerContext;
    }

    public java.lang.String getName() {
        return this.name;
    }

    public void info(java.lang.String str) {
        filterAndLog_0_Or3Plus(FQCN, null, ch.qos.logback.classic.Level.INFO, str, null, null);
    }

    public void info(java.lang.String str, java.lang.Object obj) {
        filterAndLog_1(FQCN, null, ch.qos.logback.classic.Level.INFO, str, obj, null);
    }

    public void info(java.lang.String str, java.lang.Object obj, java.lang.Object obj2) {
        filterAndLog_2(FQCN, null, ch.qos.logback.classic.Level.INFO, str, obj, obj2, null);
    }

    public void info(java.lang.String str, java.lang.Throwable th) {
        filterAndLog_0_Or3Plus(FQCN, null, ch.qos.logback.classic.Level.INFO, str, null, th);
    }

    public void info(java.lang.String str, java.lang.Object[] objArr) {
        filterAndLog_0_Or3Plus(FQCN, null, ch.qos.logback.classic.Level.INFO, str, objArr, null);
    }

    public void info(org.slf4j.Marker marker, java.lang.String str) {
        filterAndLog_0_Or3Plus(FQCN, marker, ch.qos.logback.classic.Level.INFO, str, null, null);
    }

    public void info(org.slf4j.Marker marker, java.lang.String str, java.lang.Object obj) {
        filterAndLog_1(FQCN, marker, ch.qos.logback.classic.Level.INFO, str, obj, null);
    }

    public void info(org.slf4j.Marker marker, java.lang.String str, java.lang.Object obj, java.lang.Object obj2) {
        filterAndLog_2(FQCN, marker, ch.qos.logback.classic.Level.INFO, str, obj, obj2, null);
    }

    public void info(org.slf4j.Marker marker, java.lang.String str, java.lang.Throwable th) {
        filterAndLog_0_Or3Plus(FQCN, marker, ch.qos.logback.classic.Level.INFO, str, null, th);
    }

    public void info(org.slf4j.Marker marker, java.lang.String str, java.lang.Object[] objArr) {
        filterAndLog_0_Or3Plus(FQCN, marker, ch.qos.logback.classic.Level.INFO, str, objArr, null);
    }

    public boolean isAdditive() {
        return this.additive;
    }

    public boolean isAttached(ch.qos.logback.core.Appender<ch.qos.logback.classic.spi.ILoggingEvent> appender) {
        if (this.aai == null) {
            return false;
        }
        return this.aai.isAttached(appender);
    }

    public boolean isDebugEnabled() {
        return isDebugEnabled(null);
    }

    public boolean isDebugEnabled(org.slf4j.Marker marker) {
        ch.qos.logback.core.spi.FilterReply callTurboFilters = callTurboFilters(marker, ch.qos.logback.classic.Level.DEBUG);
        if (callTurboFilters == ch.qos.logback.core.spi.FilterReply.NEUTRAL) {
            return this.effectiveLevelInt <= 10000;
        }
        if (callTurboFilters == ch.qos.logback.core.spi.FilterReply.DENY) {
            return false;
        }
        if (callTurboFilters == ch.qos.logback.core.spi.FilterReply.ACCEPT) {
            return true;
        }
        throw new java.lang.IllegalStateException("Unknown FilterReply value: " + callTurboFilters);
    }

    public boolean isEnabledFor(ch.qos.logback.classic.Level level2) {
        return isEnabledFor(null, level2);
    }

    public boolean isEnabledFor(org.slf4j.Marker marker, ch.qos.logback.classic.Level level2) {
        ch.qos.logback.core.spi.FilterReply callTurboFilters = callTurboFilters(marker, level2);
        if (callTurboFilters == ch.qos.logback.core.spi.FilterReply.NEUTRAL) {
            return this.effectiveLevelInt <= level2.levelInt;
        }
        if (callTurboFilters == ch.qos.logback.core.spi.FilterReply.DENY) {
            return false;
        }
        if (callTurboFilters == ch.qos.logback.core.spi.FilterReply.ACCEPT) {
            return true;
        }
        throw new java.lang.IllegalStateException("Unknown FilterReply value: " + callTurboFilters);
    }

    public boolean isErrorEnabled() {
        return isErrorEnabled(null);
    }

    public boolean isErrorEnabled(org.slf4j.Marker marker) {
        ch.qos.logback.core.spi.FilterReply callTurboFilters = callTurboFilters(marker, ch.qos.logback.classic.Level.ERROR);
        if (callTurboFilters == ch.qos.logback.core.spi.FilterReply.NEUTRAL) {
            return this.effectiveLevelInt <= 40000;
        }
        if (callTurboFilters == ch.qos.logback.core.spi.FilterReply.DENY) {
            return false;
        }
        if (callTurboFilters == ch.qos.logback.core.spi.FilterReply.ACCEPT) {
            return true;
        }
        throw new java.lang.IllegalStateException("Unknown FilterReply value: " + callTurboFilters);
    }

    public boolean isInfoEnabled() {
        return isInfoEnabled(null);
    }

    public boolean isInfoEnabled(org.slf4j.Marker marker) {
        ch.qos.logback.core.spi.FilterReply callTurboFilters = callTurboFilters(marker, ch.qos.logback.classic.Level.INFO);
        if (callTurboFilters == ch.qos.logback.core.spi.FilterReply.NEUTRAL) {
            return this.effectiveLevelInt <= 20000;
        }
        if (callTurboFilters == ch.qos.logback.core.spi.FilterReply.DENY) {
            return false;
        }
        if (callTurboFilters == ch.qos.logback.core.spi.FilterReply.ACCEPT) {
            return true;
        }
        throw new java.lang.IllegalStateException("Unknown FilterReply value: " + callTurboFilters);
    }

    public boolean isTraceEnabled() {
        return isTraceEnabled(null);
    }

    public boolean isTraceEnabled(org.slf4j.Marker marker) {
        ch.qos.logback.core.spi.FilterReply callTurboFilters = callTurboFilters(marker, ch.qos.logback.classic.Level.TRACE);
        if (callTurboFilters == ch.qos.logback.core.spi.FilterReply.NEUTRAL) {
            return this.effectiveLevelInt <= 5000;
        }
        if (callTurboFilters == ch.qos.logback.core.spi.FilterReply.DENY) {
            return false;
        }
        if (callTurboFilters == ch.qos.logback.core.spi.FilterReply.ACCEPT) {
            return true;
        }
        throw new java.lang.IllegalStateException("Unknown FilterReply value: " + callTurboFilters);
    }

    public boolean isWarnEnabled() {
        return isWarnEnabled(null);
    }

    public boolean isWarnEnabled(org.slf4j.Marker marker) {
        ch.qos.logback.core.spi.FilterReply callTurboFilters = callTurboFilters(marker, ch.qos.logback.classic.Level.WARN);
        if (callTurboFilters == ch.qos.logback.core.spi.FilterReply.NEUTRAL) {
            return this.effectiveLevelInt <= 30000;
        }
        if (callTurboFilters == ch.qos.logback.core.spi.FilterReply.DENY) {
            return false;
        }
        if (callTurboFilters == ch.qos.logback.core.spi.FilterReply.ACCEPT) {
            return true;
        }
        throw new java.lang.IllegalStateException("Unknown FilterReply value: " + callTurboFilters);
    }

    public java.util.Iterator<ch.qos.logback.core.Appender<ch.qos.logback.classic.spi.ILoggingEvent>> iteratorForAppenders() {
        return this.aai == null ? java.util.Collections.EMPTY_LIST.iterator() : this.aai.iteratorForAppenders();
    }

    public void log(org.slf4j.Marker marker, java.lang.String str, int i, java.lang.String str2, java.lang.Object[] objArr, java.lang.Throwable th) {
        filterAndLog_0_Or3Plus(str, marker, ch.qos.logback.classic.Level.fromLocationAwareLoggerInteger(i), str2, objArr, th);
    }

    /* access modifiers changed from: protected */
    public java.lang.Object readResolve() throws java.io.ObjectStreamException {
        return org.slf4j.LoggerFactory.getLogger(getName());
    }

    /* access modifiers changed from: 0000 */
    public void recursiveReset() {
        detachAndStopAllAppenders();
        localLevelReset();
        this.additive = true;
        if (this.childrenList != null) {
            for (ch.qos.logback.classic.Logger recursiveReset : this.childrenList) {
                recursiveReset.recursiveReset();
            }
        }
    }

    public void setAdditive(boolean z) {
        this.additive = z;
    }

    public synchronized void setLevel(ch.qos.logback.classic.Level level2) {
        if (this.level != level2) {
            if (level2 == null) {
                if (isRootLogger()) {
                    throw new java.lang.IllegalArgumentException("The level of the root logger cannot be set to null");
                }
            }
            this.level = level2;
            if (level2 == null) {
                this.effectiveLevelInt = this.parent.effectiveLevelInt;
                level2 = this.parent.getEffectiveLevel();
            } else {
                this.effectiveLevelInt = level2.levelInt;
            }
            if (this.childrenList != null) {
                int size = this.childrenList.size();
                for (int i = 0; i < size; i++) {
                    ((ch.qos.logback.classic.Logger) this.childrenList.get(i)).handleParentLevelChange(this.effectiveLevelInt);
                }
            }
            this.loggerContext.fireOnLevelChange(this, level2);
        }
    }

    public java.lang.String toString() {
        return "Logger[" + this.name + "]";
    }

    public void trace(java.lang.String str) {
        filterAndLog_0_Or3Plus(FQCN, null, ch.qos.logback.classic.Level.TRACE, str, null, null);
    }

    public void trace(java.lang.String str, java.lang.Object obj) {
        filterAndLog_1(FQCN, null, ch.qos.logback.classic.Level.TRACE, str, obj, null);
    }

    public void trace(java.lang.String str, java.lang.Object obj, java.lang.Object obj2) {
        filterAndLog_2(FQCN, null, ch.qos.logback.classic.Level.TRACE, str, obj, obj2, null);
    }

    public void trace(java.lang.String str, java.lang.Throwable th) {
        filterAndLog_0_Or3Plus(FQCN, null, ch.qos.logback.classic.Level.TRACE, str, null, th);
    }

    public void trace(java.lang.String str, java.lang.Object[] objArr) {
        filterAndLog_0_Or3Plus(FQCN, null, ch.qos.logback.classic.Level.TRACE, str, objArr, null);
    }

    public void trace(org.slf4j.Marker marker, java.lang.String str) {
        filterAndLog_0_Or3Plus(FQCN, marker, ch.qos.logback.classic.Level.TRACE, str, null, null);
    }

    public void trace(org.slf4j.Marker marker, java.lang.String str, java.lang.Object obj) {
        filterAndLog_1(FQCN, marker, ch.qos.logback.classic.Level.TRACE, str, obj, null);
    }

    public void trace(org.slf4j.Marker marker, java.lang.String str, java.lang.Object obj, java.lang.Object obj2) {
        filterAndLog_2(FQCN, marker, ch.qos.logback.classic.Level.TRACE, str, obj, obj2, null);
    }

    public void trace(org.slf4j.Marker marker, java.lang.String str, java.lang.Throwable th) {
        filterAndLog_0_Or3Plus(FQCN, marker, ch.qos.logback.classic.Level.TRACE, str, null, th);
    }

    public void trace(org.slf4j.Marker marker, java.lang.String str, java.lang.Object[] objArr) {
        filterAndLog_0_Or3Plus(FQCN, marker, ch.qos.logback.classic.Level.TRACE, str, objArr, null);
    }

    public void warn(java.lang.String str) {
        filterAndLog_0_Or3Plus(FQCN, null, ch.qos.logback.classic.Level.WARN, str, null, null);
    }

    public void warn(java.lang.String str, java.lang.Object obj) {
        filterAndLog_1(FQCN, null, ch.qos.logback.classic.Level.WARN, str, obj, null);
    }

    public void warn(java.lang.String str, java.lang.Object obj, java.lang.Object obj2) {
        filterAndLog_2(FQCN, null, ch.qos.logback.classic.Level.WARN, str, obj, obj2, null);
    }

    public void warn(java.lang.String str, java.lang.Throwable th) {
        filterAndLog_0_Or3Plus(FQCN, null, ch.qos.logback.classic.Level.WARN, str, null, th);
    }

    public void warn(java.lang.String str, java.lang.Object[] objArr) {
        filterAndLog_0_Or3Plus(FQCN, null, ch.qos.logback.classic.Level.WARN, str, objArr, null);
    }

    public void warn(org.slf4j.Marker marker, java.lang.String str) {
        filterAndLog_0_Or3Plus(FQCN, marker, ch.qos.logback.classic.Level.WARN, str, null, null);
    }

    public void warn(org.slf4j.Marker marker, java.lang.String str, java.lang.Object obj) {
        filterAndLog_1(FQCN, marker, ch.qos.logback.classic.Level.WARN, str, obj, null);
    }

    public void warn(org.slf4j.Marker marker, java.lang.String str, java.lang.Object obj, java.lang.Object obj2) {
        filterAndLog_2(FQCN, marker, ch.qos.logback.classic.Level.WARN, str, obj, obj2, null);
    }

    public void warn(org.slf4j.Marker marker, java.lang.String str, java.lang.Throwable th) {
        filterAndLog_0_Or3Plus(FQCN, marker, ch.qos.logback.classic.Level.WARN, str, null, th);
    }

    public void warn(org.slf4j.Marker marker, java.lang.String str, java.lang.Object[] objArr) {
        filterAndLog_0_Or3Plus(FQCN, marker, ch.qos.logback.classic.Level.WARN, str, objArr, null);
    }
}
