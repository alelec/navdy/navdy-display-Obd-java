package ch.qos.logback.classic.boolex;

public class OnErrorEvaluator extends ch.qos.logback.core.boolex.EventEvaluatorBase<ch.qos.logback.classic.spi.ILoggingEvent> {
    public boolean evaluate(ch.qos.logback.classic.spi.ILoggingEvent iLoggingEvent) throws java.lang.NullPointerException, ch.qos.logback.core.boolex.EvaluationException {
        return iLoggingEvent.getLevel().levelInt >= 40000;
    }
}
