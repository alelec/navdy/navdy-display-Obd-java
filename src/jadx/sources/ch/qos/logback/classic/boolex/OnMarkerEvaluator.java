package ch.qos.logback.classic.boolex;

public class OnMarkerEvaluator extends ch.qos.logback.core.boolex.EventEvaluatorBase<ch.qos.logback.classic.spi.ILoggingEvent> {
    java.util.List<java.lang.String> markerList = new java.util.ArrayList();

    public void addMarker(java.lang.String str) {
        this.markerList.add(str);
    }

    public boolean evaluate(ch.qos.logback.classic.spi.ILoggingEvent iLoggingEvent) throws java.lang.NullPointerException, ch.qos.logback.core.boolex.EvaluationException {
        org.slf4j.Marker marker = iLoggingEvent.getMarker();
        if (marker == null) {
            return false;
        }
        for (java.lang.String contains : this.markerList) {
            if (marker.contains(contains)) {
                return true;
            }
        }
        return false;
    }
}
