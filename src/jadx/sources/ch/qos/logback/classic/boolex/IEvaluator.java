package ch.qos.logback.classic.boolex;

public interface IEvaluator {
    boolean doEvaluate(ch.qos.logback.classic.spi.ILoggingEvent iLoggingEvent);
}
