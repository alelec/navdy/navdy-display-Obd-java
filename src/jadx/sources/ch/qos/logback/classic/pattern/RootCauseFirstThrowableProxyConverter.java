package ch.qos.logback.classic.pattern;

public class RootCauseFirstThrowableProxyConverter extends ch.qos.logback.classic.pattern.ExtendedThrowableProxyConverter {
    /* access modifiers changed from: protected */
    public void recursiveAppendRootCauseFirst(java.lang.StringBuilder sb, java.lang.String str, int i, ch.qos.logback.classic.spi.IThrowableProxy iThrowableProxy) {
        if (iThrowableProxy.getCause() != null) {
            recursiveAppendRootCauseFirst(sb, str, i, iThrowableProxy.getCause());
            str = null;
        }
        ch.qos.logback.classic.spi.ThrowableProxyUtil.indent(sb, i - 1);
        if (str != null) {
            sb.append(str);
        }
        ch.qos.logback.classic.spi.ThrowableProxyUtil.subjoinFirstLineRootCauseFirst(sb, iThrowableProxy);
        sb.append(ch.qos.logback.core.CoreConstants.LINE_SEPARATOR);
        subjoinSTEPArray(sb, i, iThrowableProxy);
        ch.qos.logback.classic.spi.IThrowableProxy[] suppressed = iThrowableProxy.getSuppressed();
        if (suppressed != null) {
            for (ch.qos.logback.classic.spi.IThrowableProxy recursiveAppendRootCauseFirst : suppressed) {
                recursiveAppendRootCauseFirst(sb, ch.qos.logback.core.CoreConstants.SUPPRESSED, i + 1, recursiveAppendRootCauseFirst);
            }
        }
    }

    /* access modifiers changed from: protected */
    public java.lang.String throwableProxyToString(ch.qos.logback.classic.spi.IThrowableProxy iThrowableProxy) {
        java.lang.StringBuilder sb = new java.lang.StringBuilder(2048);
        recursiveAppendRootCauseFirst(sb, null, 1, iThrowableProxy);
        return sb.toString();
    }
}
