package ch.qos.logback.classic.pattern;

public class LevelConverter extends ch.qos.logback.classic.pattern.ClassicConverter {
    public java.lang.String convert(ch.qos.logback.classic.spi.ILoggingEvent iLoggingEvent) {
        return iLoggingEvent.getLevel().toString();
    }
}
