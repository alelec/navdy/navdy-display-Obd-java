package ch.qos.logback.classic.pattern;

public abstract class ThrowableHandlingConverter extends ch.qos.logback.classic.pattern.ClassicConverter {
    /* access modifiers changed from: 0000 */
    public boolean handlesThrowable() {
        return true;
    }
}
