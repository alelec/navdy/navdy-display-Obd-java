package ch.qos.logback.classic.pattern.color;

@java.lang.Deprecated
public class HighlightingCompositeConverter extends ch.qos.logback.core.pattern.color.ForegroundCompositeConverterBase<ch.qos.logback.classic.spi.ILoggingEvent> {
    /* access modifiers changed from: protected */
    public java.lang.String getForegroundColorCode(ch.qos.logback.classic.spi.ILoggingEvent iLoggingEvent) {
        switch (iLoggingEvent.getLevel().toInt()) {
            case ch.qos.logback.classic.Level.INFO_INT /*20000*/:
                return ch.qos.logback.core.pattern.color.ANSIConstants.BLUE_FG;
            case 30000:
                return ch.qos.logback.core.pattern.color.ANSIConstants.RED_FG;
            case ch.qos.logback.classic.Level.ERROR_INT /*40000*/:
                return "1;31";
            default:
                return ch.qos.logback.core.pattern.color.ANSIConstants.DEFAULT_FG;
        }
    }
}
