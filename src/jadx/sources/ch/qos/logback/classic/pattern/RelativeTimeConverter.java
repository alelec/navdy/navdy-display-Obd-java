package ch.qos.logback.classic.pattern;

public class RelativeTimeConverter extends ch.qos.logback.classic.pattern.ClassicConverter {
    long lastTimestamp = -1;
    java.lang.String timesmapCache = null;

    public java.lang.String convert(ch.qos.logback.classic.spi.ILoggingEvent iLoggingEvent) {
        java.lang.String str;
        long timeStamp = iLoggingEvent.getTimeStamp();
        synchronized (this) {
            if (timeStamp != this.lastTimestamp) {
                this.lastTimestamp = timeStamp;
                this.timesmapCache = java.lang.Long.toString(timeStamp - iLoggingEvent.getLoggerContextVO().getBirthTime());
            }
            str = this.timesmapCache;
        }
        return str;
    }
}
