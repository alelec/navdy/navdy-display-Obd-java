package ch.qos.logback.classic.pattern;

public class MDCConverter extends ch.qos.logback.classic.pattern.ClassicConverter {
    private java.lang.String defaultValue = "";
    private java.lang.String key;

    private java.lang.String outputMDCForAllKeys(java.util.Map<java.lang.String, java.lang.String> map) {
        boolean z;
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        boolean z2 = true;
        for (java.util.Map.Entry entry : map.entrySet()) {
            if (z2) {
                z = false;
            } else {
                sb.append(", ");
                z = z2;
            }
            sb.append((java.lang.String) entry.getKey()).append('=').append((java.lang.String) entry.getValue());
            z2 = z;
        }
        return sb.toString();
    }

    public java.lang.String convert(ch.qos.logback.classic.spi.ILoggingEvent iLoggingEvent) {
        java.util.Map mDCPropertyMap = iLoggingEvent.getMDCPropertyMap();
        if (mDCPropertyMap == null) {
            return this.defaultValue;
        }
        if (this.key == null) {
            return outputMDCForAllKeys(mDCPropertyMap);
        }
        java.lang.String str = (java.lang.String) iLoggingEvent.getMDCPropertyMap().get(this.key);
        return str == null ? this.defaultValue : str;
    }

    public void start() {
        java.lang.String[] extractDefaultReplacement = ch.qos.logback.core.util.OptionHelper.extractDefaultReplacement(getFirstOption());
        this.key = extractDefaultReplacement[0];
        if (extractDefaultReplacement[1] != null) {
            this.defaultValue = extractDefaultReplacement[1];
        }
        super.start();
    }

    public void stop() {
        this.key = null;
        super.stop();
    }
}
