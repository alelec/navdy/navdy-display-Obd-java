package ch.qos.logback.classic.pattern;

public class ThrowableProxyConverter extends ch.qos.logback.classic.pattern.ThrowableHandlingConverter {
    protected static final int BUILDER_CAPACITY = 2048;
    int errorCount = 0;
    java.util.List<ch.qos.logback.core.boolex.EventEvaluator<ch.qos.logback.classic.spi.ILoggingEvent>> evaluatorList = null;
    int lengthOption;

    private void addEvaluator(ch.qos.logback.core.boolex.EventEvaluator<ch.qos.logback.classic.spi.ILoggingEvent> eventEvaluator) {
        if (this.evaluatorList == null) {
            this.evaluatorList = new java.util.ArrayList();
        }
        this.evaluatorList.add(eventEvaluator);
    }

    private void recursiveAppend(java.lang.StringBuilder sb, java.lang.String str, int i, ch.qos.logback.classic.spi.IThrowableProxy iThrowableProxy) {
        if (iThrowableProxy != null) {
            subjoinFirstLine(sb, str, i, iThrowableProxy);
            sb.append(ch.qos.logback.core.CoreConstants.LINE_SEPARATOR);
            subjoinSTEPArray(sb, i, iThrowableProxy);
            ch.qos.logback.classic.spi.IThrowableProxy[] suppressed = iThrowableProxy.getSuppressed();
            if (suppressed != null) {
                for (ch.qos.logback.classic.spi.IThrowableProxy recursiveAppend : suppressed) {
                    recursiveAppend(sb, ch.qos.logback.core.CoreConstants.SUPPRESSED, i + 1, recursiveAppend);
                }
            }
            recursiveAppend(sb, ch.qos.logback.core.CoreConstants.CAUSED_BY, i, iThrowableProxy.getCause());
        }
    }

    private void subjoinExceptionMessage(java.lang.StringBuilder sb, ch.qos.logback.classic.spi.IThrowableProxy iThrowableProxy) {
        sb.append(iThrowableProxy.getClassName()).append(": ").append(iThrowableProxy.getMessage());
    }

    private void subjoinFirstLine(java.lang.StringBuilder sb, java.lang.String str, int i, ch.qos.logback.classic.spi.IThrowableProxy iThrowableProxy) {
        ch.qos.logback.classic.spi.ThrowableProxyUtil.indent(sb, i - 1);
        if (str != null) {
            sb.append(str);
        }
        subjoinExceptionMessage(sb, iThrowableProxy);
    }

    public java.lang.String convert(ch.qos.logback.classic.spi.ILoggingEvent iLoggingEvent) {
        boolean z = false;
        ch.qos.logback.classic.spi.IThrowableProxy throwableProxy = iLoggingEvent.getThrowableProxy();
        if (throwableProxy == null) {
            return "";
        }
        if (this.evaluatorList != null) {
            int i = 0;
            while (true) {
                if (i >= this.evaluatorList.size()) {
                    z = true;
                    break;
                }
                ch.qos.logback.core.boolex.EventEvaluator eventEvaluator = (ch.qos.logback.core.boolex.EventEvaluator) this.evaluatorList.get(i);
                try {
                    if (eventEvaluator.evaluate(iLoggingEvent)) {
                        break;
                    }
                    i++;
                } catch (ch.qos.logback.core.boolex.EvaluationException e) {
                    this.errorCount++;
                    if (this.errorCount < 4) {
                        addError("Exception thrown for evaluator named [" + eventEvaluator.getName() + "]", e);
                    } else if (this.errorCount == 4) {
                        ch.qos.logback.core.status.ErrorStatus errorStatus = new ch.qos.logback.core.status.ErrorStatus("Exception thrown for evaluator named [" + eventEvaluator.getName() + "].", this, e);
                        errorStatus.add(new ch.qos.logback.core.status.ErrorStatus("This was the last warning about this evaluator's errors.We don't want the StatusManager to get flooded.", this));
                        addStatus(errorStatus);
                    }
                }
            }
            if (!z) {
                return "";
            }
        }
        return throwableProxyToString(throwableProxy);
    }

    /* access modifiers changed from: protected */
    public void extraData(java.lang.StringBuilder sb, ch.qos.logback.classic.spi.StackTraceElementProxy stackTraceElementProxy) {
    }

    public void start() {
        java.lang.String firstOption = getFirstOption();
        if (firstOption == null) {
            this.lengthOption = Integer.MAX_VALUE;
        } else {
            java.lang.String lowerCase = firstOption.toLowerCase();
            if ("full".equals(lowerCase)) {
                this.lengthOption = Integer.MAX_VALUE;
            } else if ("short".equals(lowerCase)) {
                this.lengthOption = 1;
            } else {
                try {
                    this.lengthOption = java.lang.Integer.parseInt(lowerCase);
                } catch (java.lang.NumberFormatException e) {
                    addError("Could not parse [" + lowerCase + "] as an integer");
                    this.lengthOption = Integer.MAX_VALUE;
                }
            }
        }
        java.util.List optionList = getOptionList();
        if (optionList != null && optionList.size() > 1) {
            int size = optionList.size();
            for (int i = 1; i < size; i++) {
                addEvaluator((ch.qos.logback.core.boolex.EventEvaluator) ((java.util.Map) getContext().getObject(ch.qos.logback.core.CoreConstants.EVALUATOR_MAP)).get((java.lang.String) optionList.get(i)));
            }
        }
        super.start();
    }

    public void stop() {
        this.evaluatorList = null;
        super.stop();
    }

    /* access modifiers changed from: protected */
    public void subjoinSTEPArray(java.lang.StringBuilder sb, int i, ch.qos.logback.classic.spi.IThrowableProxy iThrowableProxy) {
        ch.qos.logback.classic.spi.StackTraceElementProxy[] stackTraceElementProxyArray = iThrowableProxy.getStackTraceElementProxyArray();
        int commonFrames = iThrowableProxy.getCommonFrames();
        boolean z = this.lengthOption > stackTraceElementProxyArray.length;
        int i2 = z ? stackTraceElementProxyArray.length : this.lengthOption;
        if (commonFrames > 0 && z) {
            i2 -= commonFrames;
        }
        for (int i3 = 0; i3 < i2; i3++) {
            ch.qos.logback.classic.spi.ThrowableProxyUtil.indent(sb, i);
            sb.append(stackTraceElementProxyArray[i3]);
            extraData(sb, stackTraceElementProxyArray[i3]);
            sb.append(ch.qos.logback.core.CoreConstants.LINE_SEPARATOR);
        }
        if (commonFrames > 0 && z) {
            ch.qos.logback.classic.spi.ThrowableProxyUtil.indent(sb, i);
            sb.append("... ").append(iThrowableProxy.getCommonFrames()).append(" common frames omitted").append(ch.qos.logback.core.CoreConstants.LINE_SEPARATOR);
        }
    }

    /* access modifiers changed from: protected */
    public java.lang.String throwableProxyToString(ch.qos.logback.classic.spi.IThrowableProxy iThrowableProxy) {
        java.lang.StringBuilder sb = new java.lang.StringBuilder(2048);
        recursiveAppend(sb, null, 1, iThrowableProxy);
        return sb.toString();
    }
}
