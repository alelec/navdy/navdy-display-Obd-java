package ch.qos.logback.classic.pattern;

public class DateConverter extends ch.qos.logback.classic.pattern.ClassicConverter {
    ch.qos.logback.core.util.CachingDateFormatter cachingDateFormatter = null;
    long lastTimestamp = -1;
    java.lang.String timestampStrCache = null;

    public java.lang.String convert(ch.qos.logback.classic.spi.ILoggingEvent iLoggingEvent) {
        return this.cachingDateFormatter.format(iLoggingEvent.getTimeStamp());
    }

    public void start() {
        java.lang.String firstOption = getFirstOption();
        if (firstOption == null) {
            firstOption = ch.qos.logback.core.CoreConstants.ISO8601_PATTERN;
        }
        java.lang.String str = firstOption.equals(ch.qos.logback.core.CoreConstants.ISO8601_STR) ? ch.qos.logback.core.CoreConstants.ISO8601_PATTERN : firstOption;
        try {
            this.cachingDateFormatter = new ch.qos.logback.core.util.CachingDateFormatter(str);
        } catch (java.lang.IllegalArgumentException e) {
            addWarn("Could not instantiate SimpleDateFormat with pattern " + str, e);
            this.cachingDateFormatter = new ch.qos.logback.core.util.CachingDateFormatter(ch.qos.logback.core.CoreConstants.ISO8601_PATTERN);
        }
        java.util.List optionList = getOptionList();
        if (optionList != null && optionList.size() > 1) {
            this.cachingDateFormatter.setTimeZone(java.util.TimeZone.getTimeZone((java.lang.String) optionList.get(1)));
        }
    }
}
