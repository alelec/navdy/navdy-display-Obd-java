package ch.qos.logback.classic.pattern;

public class EnsureExceptionHandling implements ch.qos.logback.core.pattern.PostCompileProcessor<ch.qos.logback.classic.spi.ILoggingEvent> {
    public boolean chainHandlesThrowable(ch.qos.logback.core.pattern.Converter converter) {
        while (converter != null) {
            if (converter instanceof ch.qos.logback.classic.pattern.ThrowableHandlingConverter) {
                return true;
            }
            converter = converter.getNext();
        }
        return false;
    }

    public void process(ch.qos.logback.core.pattern.Converter<ch.qos.logback.classic.spi.ILoggingEvent> converter) {
        if (converter == null) {
            throw new java.lang.IllegalArgumentException("cannot process empty chain");
        } else if (!chainHandlesThrowable(converter)) {
            ch.qos.logback.core.pattern.ConverterUtil.findTail(converter).setNext(new ch.qos.logback.classic.pattern.ExtendedThrowableProxyConverter());
        }
    }
}
