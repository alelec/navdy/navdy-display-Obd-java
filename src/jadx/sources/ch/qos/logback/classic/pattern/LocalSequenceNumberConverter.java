package ch.qos.logback.classic.pattern;

public class LocalSequenceNumberConverter extends ch.qos.logback.classic.pattern.ClassicConverter {
    java.util.concurrent.atomic.AtomicLong sequenceNumber = new java.util.concurrent.atomic.AtomicLong(java.lang.System.currentTimeMillis());

    public java.lang.String convert(ch.qos.logback.classic.spi.ILoggingEvent iLoggingEvent) {
        return java.lang.Long.toString(this.sequenceNumber.getAndIncrement());
    }
}
