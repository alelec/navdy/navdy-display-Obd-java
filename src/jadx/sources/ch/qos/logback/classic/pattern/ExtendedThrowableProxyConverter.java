package ch.qos.logback.classic.pattern;

public class ExtendedThrowableProxyConverter extends ch.qos.logback.classic.pattern.ThrowableProxyConverter {
    /* access modifiers changed from: protected */
    public void extraData(java.lang.StringBuilder sb, ch.qos.logback.classic.spi.StackTraceElementProxy stackTraceElementProxy) {
        ch.qos.logback.classic.spi.ThrowableProxyUtil.subjoinPackagingData(sb, stackTraceElementProxy);
    }

    /* access modifiers changed from: protected */
    public void prepareLoggingEvent(ch.qos.logback.classic.spi.ILoggingEvent iLoggingEvent) {
    }
}
