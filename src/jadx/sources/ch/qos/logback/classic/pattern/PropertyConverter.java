package ch.qos.logback.classic.pattern;

public final class PropertyConverter extends ch.qos.logback.classic.pattern.ClassicConverter {
    java.lang.String key;

    public java.lang.String convert(ch.qos.logback.classic.spi.ILoggingEvent iLoggingEvent) {
        if (this.key == null) {
            return "Property_HAS_NO_KEY";
        }
        java.lang.String str = (java.lang.String) iLoggingEvent.getLoggerContextVO().getPropertyMap().get(this.key);
        return str == null ? java.lang.System.getProperty(this.key) : str;
    }

    public void start() {
        java.lang.String firstOption = getFirstOption();
        if (firstOption != null) {
            this.key = firstOption;
            super.start();
        }
    }
}
