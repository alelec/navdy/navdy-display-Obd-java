package ch.qos.logback.classic.pattern;

public class SyslogStartConverter extends ch.qos.logback.classic.pattern.ClassicConverter {
    int facility;
    long lastTimestamp = -1;
    final java.lang.String localHostName = "localhost";
    java.text.SimpleDateFormat simpleFormat;
    java.lang.String timesmapStr = null;

    /* access modifiers changed from: 0000 */
    public java.lang.String computeTimeStampString(long j) {
        java.lang.String str;
        synchronized (this) {
            if (j != this.lastTimestamp) {
                this.lastTimestamp = j;
                this.timesmapStr = this.simpleFormat.format(new java.util.Date(j));
            }
            str = this.timesmapStr;
        }
        return str;
    }

    public java.lang.String convert(ch.qos.logback.classic.spi.ILoggingEvent iLoggingEvent) {
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        int convert = this.facility + ch.qos.logback.classic.util.LevelToSyslogSeverity.convert(iLoggingEvent);
        sb.append("<");
        sb.append(convert);
        sb.append(">");
        sb.append(computeTimeStampString(iLoggingEvent.getTimeStamp()));
        sb.append(' ');
        sb.append("localhost");
        sb.append(' ');
        return sb.toString();
    }

    public void start() {
        boolean z = false;
        java.lang.String firstOption = getFirstOption();
        if (firstOption == null) {
            addError("was expecting a facility string as an option");
            return;
        }
        this.facility = ch.qos.logback.core.net.SyslogAppenderBase.facilityStringToint(firstOption);
        try {
            this.simpleFormat = new java.text.SimpleDateFormat("MMM dd HH:mm:ss", new java.text.DateFormatSymbols(java.util.Locale.US));
        } catch (java.lang.IllegalArgumentException e) {
            addError("Could not instantiate SimpleDateFormat", e);
            z = true;
        }
        if (!z) {
            super.start();
        }
    }
}
