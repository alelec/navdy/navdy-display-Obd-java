package ch.qos.logback.classic.pattern;

public class MarkerConverter extends ch.qos.logback.classic.pattern.ClassicConverter {
    private static java.lang.String EMPTY = "";

    public java.lang.String convert(ch.qos.logback.classic.spi.ILoggingEvent iLoggingEvent) {
        org.slf4j.Marker marker = iLoggingEvent.getMarker();
        return marker == null ? EMPTY : marker.toString();
    }
}
