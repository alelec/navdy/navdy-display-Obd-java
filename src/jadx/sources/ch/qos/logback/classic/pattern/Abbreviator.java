package ch.qos.logback.classic.pattern;

public interface Abbreviator {
    java.lang.String abbreviate(java.lang.String str);
}
