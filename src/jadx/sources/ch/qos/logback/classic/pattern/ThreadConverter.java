package ch.qos.logback.classic.pattern;

public class ThreadConverter extends ch.qos.logback.classic.pattern.ClassicConverter {
    public java.lang.String convert(ch.qos.logback.classic.spi.ILoggingEvent iLoggingEvent) {
        return iLoggingEvent.getThreadName();
    }
}
