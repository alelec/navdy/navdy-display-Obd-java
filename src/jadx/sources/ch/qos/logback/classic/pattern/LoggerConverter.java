package ch.qos.logback.classic.pattern;

public class LoggerConverter extends ch.qos.logback.classic.pattern.NamedConverter {
    /* access modifiers changed from: protected */
    public java.lang.String getFullyQualifiedName(ch.qos.logback.classic.spi.ILoggingEvent iLoggingEvent) {
        return iLoggingEvent.getLoggerName();
    }
}
