package ch.qos.logback.classic.pattern;

public class CallerDataConverter extends ch.qos.logback.classic.pattern.ClassicConverter {
    public static final java.lang.String DEFAULT_CALLER_LINE_PREFIX = "Caller+";
    final int MAX_ERROR_COUNT = 4;
    int depth = 5;
    int errorCount = 0;
    java.util.List<ch.qos.logback.core.boolex.EventEvaluator<ch.qos.logback.classic.spi.ILoggingEvent>> evaluatorList = null;

    private void addEvaluator(ch.qos.logback.core.boolex.EventEvaluator<ch.qos.logback.classic.spi.ILoggingEvent> eventEvaluator) {
        if (this.evaluatorList == null) {
            this.evaluatorList = new java.util.ArrayList();
        }
        this.evaluatorList.add(eventEvaluator);
    }

    public java.lang.String convert(ch.qos.logback.classic.spi.ILoggingEvent iLoggingEvent) {
        boolean z;
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        if (this.evaluatorList != null) {
            int i = 0;
            while (true) {
                if (i >= this.evaluatorList.size()) {
                    z = false;
                    break;
                }
                ch.qos.logback.core.boolex.EventEvaluator eventEvaluator = (ch.qos.logback.core.boolex.EventEvaluator) this.evaluatorList.get(i);
                try {
                    if (eventEvaluator.evaluate(iLoggingEvent)) {
                        z = true;
                        break;
                    }
                    i++;
                } catch (ch.qos.logback.core.boolex.EvaluationException e) {
                    this.errorCount++;
                    if (this.errorCount < 4) {
                        addError("Exception thrown for evaluator named [" + eventEvaluator.getName() + "]", e);
                    } else if (this.errorCount == 4) {
                        ch.qos.logback.core.status.ErrorStatus errorStatus = new ch.qos.logback.core.status.ErrorStatus("Exception thrown for evaluator named [" + eventEvaluator.getName() + "].", this, e);
                        errorStatus.add(new ch.qos.logback.core.status.ErrorStatus("This was the last warning about this evaluator's errors.We don't want the StatusManager to get flooded.", this));
                        addStatus(errorStatus);
                    }
                }
            }
            if (!z) {
                return "";
            }
        }
        java.lang.StackTraceElement[] callerData = iLoggingEvent.getCallerData();
        if (callerData == null || callerData.length <= 0) {
            return ch.qos.logback.classic.spi.CallerData.CALLER_DATA_NA;
        }
        int length = this.depth < callerData.length ? this.depth : callerData.length;
        for (int i2 = 0; i2 < length; i2++) {
            sb.append(getCallerLinePrefix());
            sb.append(i2);
            sb.append("\t at ");
            sb.append(callerData[i2]);
            sb.append(ch.qos.logback.core.CoreConstants.LINE_SEPARATOR);
        }
        return sb.toString();
    }

    /* access modifiers changed from: protected */
    public java.lang.String getCallerLinePrefix() {
        return DEFAULT_CALLER_LINE_PREFIX;
    }

    public void start() {
        java.lang.String firstOption = getFirstOption();
        if (firstOption != null) {
            try {
                this.depth = java.lang.Integer.parseInt(firstOption);
            } catch (java.lang.NumberFormatException e) {
                addError("Failed to parse depth option [" + firstOption + "]", e);
            }
            java.util.List optionList = getOptionList();
            if (optionList != null && optionList.size() > 1) {
                int size = optionList.size();
                for (int i = 1; i < size; i++) {
                    java.lang.String str = (java.lang.String) optionList.get(i);
                    ch.qos.logback.core.Context context = getContext();
                    if (context != null) {
                        ch.qos.logback.core.boolex.EventEvaluator eventEvaluator = (ch.qos.logback.core.boolex.EventEvaluator) ((java.util.Map) context.getObject(ch.qos.logback.core.CoreConstants.EVALUATOR_MAP)).get(str);
                        if (eventEvaluator != null) {
                            addEvaluator(eventEvaluator);
                        }
                    }
                }
            }
        }
    }
}
