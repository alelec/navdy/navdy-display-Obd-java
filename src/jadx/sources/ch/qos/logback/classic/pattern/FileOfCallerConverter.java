package ch.qos.logback.classic.pattern;

public class FileOfCallerConverter extends ch.qos.logback.classic.pattern.ClassicConverter {
    public java.lang.String convert(ch.qos.logback.classic.spi.ILoggingEvent iLoggingEvent) {
        java.lang.StackTraceElement[] callerData = iLoggingEvent.getCallerData();
        return (callerData == null || callerData.length <= 0) ? ch.qos.logback.classic.spi.CallerData.NA : callerData[0].getFileName();
    }
}
