package ch.qos.logback.classic.pattern;

public class ClassOfCallerConverter extends ch.qos.logback.classic.pattern.NamedConverter {
    /* access modifiers changed from: protected */
    public java.lang.String getFullyQualifiedName(ch.qos.logback.classic.spi.ILoggingEvent iLoggingEvent) {
        java.lang.StackTraceElement[] callerData = iLoggingEvent.getCallerData();
        return (callerData == null || callerData.length <= 0) ? ch.qos.logback.classic.spi.CallerData.NA : callerData[0].getClassName();
    }
}
