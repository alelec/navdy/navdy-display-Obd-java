package ch.qos.logback.classic.pattern;

public class ClassNameOnlyAbbreviator implements ch.qos.logback.classic.pattern.Abbreviator {
    public java.lang.String abbreviate(java.lang.String str) {
        int lastIndexOf = str.lastIndexOf(46);
        return lastIndexOf != -1 ? str.substring(lastIndexOf + 1, str.length()) : str;
    }
}
