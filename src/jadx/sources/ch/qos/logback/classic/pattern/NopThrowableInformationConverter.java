package ch.qos.logback.classic.pattern;

public class NopThrowableInformationConverter extends ch.qos.logback.classic.pattern.ThrowableHandlingConverter {
    public java.lang.String convert(ch.qos.logback.classic.spi.ILoggingEvent iLoggingEvent) {
        return "";
    }
}
