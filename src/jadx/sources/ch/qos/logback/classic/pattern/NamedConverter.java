package ch.qos.logback.classic.pattern;

public abstract class NamedConverter extends ch.qos.logback.classic.pattern.ClassicConverter {
    ch.qos.logback.classic.pattern.Abbreviator abbreviator = null;

    public java.lang.String convert(ch.qos.logback.classic.spi.ILoggingEvent iLoggingEvent) {
        java.lang.String fullyQualifiedName = getFullyQualifiedName(iLoggingEvent);
        return this.abbreviator == null ? fullyQualifiedName : this.abbreviator.abbreviate(fullyQualifiedName);
    }

    /* access modifiers changed from: protected */
    public abstract java.lang.String getFullyQualifiedName(ch.qos.logback.classic.spi.ILoggingEvent iLoggingEvent);

    public void start() {
        java.lang.String firstOption = getFirstOption();
        if (firstOption != null) {
            try {
                int parseInt = java.lang.Integer.parseInt(firstOption);
                if (parseInt == 0) {
                    this.abbreviator = new ch.qos.logback.classic.pattern.ClassNameOnlyAbbreviator();
                } else if (parseInt > 0) {
                    this.abbreviator = new ch.qos.logback.classic.pattern.TargetLengthBasedClassNameAbbreviator(parseInt);
                }
            } catch (java.lang.NumberFormatException e) {
            }
        }
    }
}
