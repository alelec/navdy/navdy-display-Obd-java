package ch.qos.logback.classic.log4j;

public class XMLLayout extends ch.qos.logback.core.LayoutBase<ch.qos.logback.classic.spi.ILoggingEvent> {
    private static final int DEFAULT_SIZE = 256;
    private static final int UPPER_LIMIT = 2048;
    private java.lang.StringBuilder buf = new java.lang.StringBuilder(256);
    private boolean locationInfo = false;
    private boolean properties = false;

    public java.lang.String doLayout(ch.qos.logback.classic.spi.ILoggingEvent iLoggingEvent) {
        if (this.buf.capacity() > 2048) {
            this.buf = new java.lang.StringBuilder(256);
        } else {
            this.buf.setLength(0);
        }
        this.buf.append("<log4j:event logger=\"");
        this.buf.append(iLoggingEvent.getLoggerName());
        this.buf.append("\"\r\n");
        this.buf.append("             timestamp=\"");
        this.buf.append(iLoggingEvent.getTimeStamp());
        this.buf.append("\" level=\"");
        this.buf.append(iLoggingEvent.getLevel());
        this.buf.append("\" thread=\"");
        this.buf.append(iLoggingEvent.getThreadName());
        this.buf.append("\">\r\n");
        this.buf.append("  <log4j:message><![CDATA[");
        ch.qos.logback.core.helpers.Transform.appendEscapingCDATA(this.buf, iLoggingEvent.getFormattedMessage());
        this.buf.append("]]></log4j:message>\r\n");
        ch.qos.logback.classic.spi.IThrowableProxy throwableProxy = iLoggingEvent.getThrowableProxy();
        if (throwableProxy != null) {
            ch.qos.logback.classic.spi.StackTraceElementProxy[] stackTraceElementProxyArray = throwableProxy.getStackTraceElementProxyArray();
            this.buf.append("  <log4j:throwable><![CDATA[");
            for (ch.qos.logback.classic.spi.StackTraceElementProxy stackTraceElementProxy : stackTraceElementProxyArray) {
                this.buf.append(9);
                this.buf.append(stackTraceElementProxy.toString());
                this.buf.append("\r\n");
            }
            this.buf.append("]]></log4j:throwable>\r\n");
        }
        if (this.locationInfo) {
            java.lang.StackTraceElement[] callerData = iLoggingEvent.getCallerData();
            if (callerData != null && callerData.length > 0) {
                java.lang.StackTraceElement stackTraceElement = callerData[0];
                this.buf.append("  <log4j:locationInfo class=\"");
                this.buf.append(stackTraceElement.getClassName());
                this.buf.append("\"\r\n");
                this.buf.append("                      method=\"");
                this.buf.append(ch.qos.logback.core.helpers.Transform.escapeTags(stackTraceElement.getMethodName()));
                this.buf.append("\" file=\"");
                this.buf.append(stackTraceElement.getFileName());
                this.buf.append("\" line=\"");
                this.buf.append(stackTraceElement.getLineNumber());
                this.buf.append("\"/>\r\n");
            }
        }
        if (getProperties()) {
            java.util.Map mDCPropertyMap = iLoggingEvent.getMDCPropertyMap();
            if (!(mDCPropertyMap == null || mDCPropertyMap.size() == 0)) {
                java.util.Set<java.util.Map.Entry> entrySet = mDCPropertyMap.entrySet();
                this.buf.append("  <log4j:properties>");
                for (java.util.Map.Entry entry : entrySet) {
                    this.buf.append("\r\n    <log4j:data");
                    this.buf.append(" name='" + ch.qos.logback.core.helpers.Transform.escapeTags((java.lang.String) entry.getKey()) + "'");
                    this.buf.append(" value='" + ch.qos.logback.core.helpers.Transform.escapeTags((java.lang.String) entry.getValue()) + "'");
                    this.buf.append(" />");
                }
                this.buf.append("\r\n  </log4j:properties>");
            }
        }
        this.buf.append("\r\n</log4j:event>\r\n\r\n");
        return this.buf.toString();
    }

    public java.lang.String getContentType() {
        return "text/xml";
    }

    public boolean getLocationInfo() {
        return this.locationInfo;
    }

    public boolean getProperties() {
        return this.properties;
    }

    public void setLocationInfo(boolean z) {
        this.locationInfo = z;
    }

    public void setProperties(boolean z) {
        this.properties = z;
    }

    public void start() {
        super.start();
    }
}
