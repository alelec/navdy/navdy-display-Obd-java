package ch.qos.logback.classic.jul;

@java.lang.Deprecated
public class JULHelper {
    public static java.util.logging.Level asJULLevel(ch.qos.logback.classic.Level level) {
        if (level == null) {
            throw new java.lang.IllegalArgumentException("Unexpected level [null]");
        }
        switch (level.levelInt) {
            case Integer.MIN_VALUE:
                return java.util.logging.Level.ALL;
            case 5000:
                return java.util.logging.Level.FINEST;
            case 10000:
                return java.util.logging.Level.FINE;
            case ch.qos.logback.classic.Level.INFO_INT /*20000*/:
                return java.util.logging.Level.INFO;
            case 30000:
                return java.util.logging.Level.WARNING;
            case ch.qos.logback.classic.Level.ERROR_INT /*40000*/:
                return java.util.logging.Level.SEVERE;
            case Integer.MAX_VALUE:
                return java.util.logging.Level.OFF;
            default:
                throw new java.lang.IllegalArgumentException("Unexpected level [" + level + "]");
        }
    }

    public static java.util.logging.Logger asJULLogger(ch.qos.logback.classic.Logger logger) {
        return asJULLogger(logger.getName());
    }

    public static java.util.logging.Logger asJULLogger(java.lang.String str) {
        return java.util.logging.Logger.getLogger(asJULLoggerName(str));
    }

    public static java.lang.String asJULLoggerName(java.lang.String str) {
        return org.slf4j.Logger.ROOT_LOGGER_NAME.equals(str) ? "" : str;
    }

    public static final boolean isRegularNonRootLogger(java.util.logging.Logger logger) {
        return logger != null && !logger.getName().equals("");
    }

    public static final boolean isRoot(java.util.logging.Logger logger) {
        if (logger == null) {
            return false;
        }
        return logger.getName().equals("");
    }
}
