package ch.qos.logback.classic.jul;

@java.lang.Deprecated
public class LevelChangePropagator extends ch.qos.logback.core.spi.ContextAwareBase implements ch.qos.logback.classic.spi.LoggerContextListener, ch.qos.logback.core.spi.LifeCycle {
    boolean isStarted = false;
    private java.util.Set julLoggerSet = new java.util.HashSet();
    boolean resetJUL = false;

    private void propagate(ch.qos.logback.classic.Logger logger, ch.qos.logback.classic.Level level) {
        addInfo("Propagating " + level + " level on " + logger + " onto the JUL framework");
        java.util.logging.Logger asJULLogger = ch.qos.logback.classic.jul.JULHelper.asJULLogger(logger);
        this.julLoggerSet.add(asJULLogger);
        asJULLogger.setLevel(ch.qos.logback.classic.jul.JULHelper.asJULLevel(level));
    }

    private void propagateExistingLoggerLevels() {
        for (ch.qos.logback.classic.Logger logger : ((ch.qos.logback.classic.LoggerContext) this.context).getLoggerList()) {
            if (logger.getLevel() != null) {
                propagate(logger, logger.getLevel());
            }
        }
    }

    public boolean isResetResistant() {
        return false;
    }

    public boolean isStarted() {
        return this.isStarted;
    }

    public void onLevelChange(ch.qos.logback.classic.Logger logger, ch.qos.logback.classic.Level level) {
        propagate(logger, level);
    }

    public void onReset(ch.qos.logback.classic.LoggerContext loggerContext) {
    }

    public void onStart(ch.qos.logback.classic.LoggerContext loggerContext) {
    }

    public void onStop(ch.qos.logback.classic.LoggerContext loggerContext) {
    }

    public void resetJULLevels() {
        java.util.logging.LogManager logManager = java.util.logging.LogManager.getLogManager();
        java.util.Enumeration loggerNames = logManager.getLoggerNames();
        while (loggerNames.hasMoreElements()) {
            java.lang.String str = (java.lang.String) loggerNames.nextElement();
            java.util.logging.Logger logger = logManager.getLogger(str);
            if (ch.qos.logback.classic.jul.JULHelper.isRegularNonRootLogger(logger) && logger.getLevel() != null) {
                addInfo("Setting level of jul logger [" + str + "] to null");
                logger.setLevel(null);
            }
        }
    }

    public void setResetJUL(boolean z) {
        this.resetJUL = z;
    }

    public void start() {
        if (this.resetJUL) {
            resetJULLevels();
        }
        propagateExistingLoggerLevels();
        this.isStarted = true;
    }

    public void stop() {
        this.isStarted = false;
    }
}
