package ch.qos.logback.classic;

public class LoggerContext extends ch.qos.logback.core.ContextBase implements org.slf4j.ILoggerFactory, ch.qos.logback.core.spi.LifeCycle {
    private boolean androidPropsInitialized = false;
    private java.util.List<java.lang.String> frameworkPackages;
    private java.util.Map<java.lang.String, ch.qos.logback.classic.Logger> loggerCache = new java.util.concurrent.ConcurrentHashMap();
    private final java.util.List<ch.qos.logback.classic.spi.LoggerContextListener> loggerContextListenerList = new java.util.ArrayList();
    private ch.qos.logback.classic.spi.LoggerContextVO loggerContextRemoteView = new ch.qos.logback.classic.spi.LoggerContextVO(this);
    private int maxCallerDataDepth = 8;
    private int noAppenderWarning = 0;
    private boolean packagingDataEnabled = true;
    int resetCount = 0;
    final ch.qos.logback.classic.Logger root = new ch.qos.logback.classic.Logger(org.slf4j.Logger.ROOT_LOGGER_NAME, null, this);
    private int size;
    private final ch.qos.logback.classic.spi.TurboFilterList turboFilterList = new ch.qos.logback.classic.spi.TurboFilterList();

    public LoggerContext() {
        this.root.setLevel(ch.qos.logback.classic.Level.DEBUG);
        this.loggerCache.put(org.slf4j.Logger.ROOT_LOGGER_NAME, this.root);
        initEvaluatorMap();
        this.size = 1;
        this.frameworkPackages = new java.util.ArrayList();
    }

    private void fireOnReset() {
        for (ch.qos.logback.classic.spi.LoggerContextListener onReset : this.loggerContextListenerList) {
            onReset.onReset(this);
        }
    }

    private void fireOnStart() {
        for (ch.qos.logback.classic.spi.LoggerContextListener onStart : this.loggerContextListenerList) {
            onStart.onStart(this);
        }
    }

    private void fireOnStop() {
        for (ch.qos.logback.classic.spi.LoggerContextListener onStop : this.loggerContextListenerList) {
            onStop.onStop(this);
        }
    }

    private void incSize() {
        this.size++;
    }

    private boolean isSpecialKey(java.lang.String str) {
        return str.equals(ch.qos.logback.core.CoreConstants.PACKAGE_NAME_KEY) || str.equals(ch.qos.logback.core.CoreConstants.VERSION_NAME_KEY) || str.equals(ch.qos.logback.core.CoreConstants.VERSION_CODE_KEY) || str.equals(ch.qos.logback.core.CoreConstants.EXT_DIR_KEY) || str.equals(ch.qos.logback.core.CoreConstants.DATA_DIR_KEY);
    }

    private void resetAllListeners() {
        this.loggerContextListenerList.clear();
    }

    private void resetListenersExceptResetResistant() {
        java.util.ArrayList arrayList = new java.util.ArrayList();
        for (ch.qos.logback.classic.spi.LoggerContextListener loggerContextListener : this.loggerContextListenerList) {
            if (loggerContextListener.isResetResistant()) {
                arrayList.add(loggerContextListener);
            }
        }
        this.loggerContextListenerList.retainAll(arrayList);
    }

    private void resetStatusListeners() {
        ch.qos.logback.core.status.StatusManager statusManager = getStatusManager();
        for (ch.qos.logback.core.status.StatusListener remove : statusManager.getCopyOfStatusListenerList()) {
            statusManager.remove(remove);
        }
    }

    private void updateLoggerContextVO() {
        this.loggerContextRemoteView = new ch.qos.logback.classic.spi.LoggerContextVO(this);
    }

    public void addListener(ch.qos.logback.classic.spi.LoggerContextListener loggerContextListener) {
        this.loggerContextListenerList.add(loggerContextListener);
    }

    public void addTurboFilter(ch.qos.logback.classic.turbo.TurboFilter turboFilter) {
        this.turboFilterList.add(turboFilter);
    }

    public ch.qos.logback.classic.Logger exists(java.lang.String str) {
        return (ch.qos.logback.classic.Logger) this.loggerCache.get(str);
    }

    /* access modifiers changed from: 0000 */
    public void fireOnLevelChange(ch.qos.logback.classic.Logger logger, ch.qos.logback.classic.Level level) {
        for (ch.qos.logback.classic.spi.LoggerContextListener onLevelChange : this.loggerContextListenerList) {
            onLevelChange.onLevelChange(logger, level);
        }
    }

    public java.util.List<ch.qos.logback.classic.spi.LoggerContextListener> getCopyOfListenerList() {
        return new java.util.ArrayList(this.loggerContextListenerList);
    }

    public java.util.List<java.lang.String> getFrameworkPackages() {
        return this.frameworkPackages;
    }

    public final ch.qos.logback.classic.Logger getLogger(java.lang.Class cls) {
        return getLogger(cls.getName());
    }

    public final ch.qos.logback.classic.Logger getLogger(java.lang.String str) {
        ch.qos.logback.classic.Logger logger;
        if (str == null) {
            throw new java.lang.IllegalArgumentException("name argument cannot be null");
        } else if (org.slf4j.Logger.ROOT_LOGGER_NAME.equalsIgnoreCase(str)) {
            return this.root;
        } else {
            ch.qos.logback.classic.Logger logger2 = this.root;
            ch.qos.logback.classic.Logger logger3 = (ch.qos.logback.classic.Logger) this.loggerCache.get(str);
            if (logger3 != null) {
                return logger3;
            }
            ch.qos.logback.classic.Logger logger4 = logger2;
            int i = 0;
            while (true) {
                int separatorIndexOf = ch.qos.logback.classic.util.LoggerNameUtil.getSeparatorIndexOf(str, i);
                java.lang.String substring = separatorIndexOf == -1 ? str : str.substring(0, separatorIndexOf);
                int i2 = separatorIndexOf + 1;
                synchronized (logger4) {
                    ch.qos.logback.classic.Logger childByName = logger4.getChildByName(substring);
                    if (childByName == null) {
                        childByName = logger4.createChildByName(substring);
                        this.loggerCache.put(substring, childByName);
                        incSize();
                    }
                    logger = childByName;
                }
                if (separatorIndexOf == -1) {
                    return logger;
                }
                logger4 = logger;
                i = i2;
            }
            while (true) {
            }
        }
    }

    public ch.qos.logback.classic.spi.LoggerContextVO getLoggerContextRemoteView() {
        return this.loggerContextRemoteView;
    }

    public java.util.List<ch.qos.logback.classic.Logger> getLoggerList() {
        java.util.ArrayList arrayList = new java.util.ArrayList(this.loggerCache.values());
        java.util.Collections.sort(arrayList, new ch.qos.logback.classic.spi.LoggerComparator());
        return arrayList;
    }

    public int getMaxCallerDataDepth() {
        return this.maxCallerDataDepth;
    }

    public java.lang.String getProperty(java.lang.String str) {
        if (isSpecialKey(str)) {
            try {
                if (!this.androidPropsInitialized) {
                    this.androidPropsInitialized = true;
                    ch.qos.logback.classic.android.AndroidManifestPropertiesUtil.setAndroidProperties(this);
                }
            } catch (ch.qos.logback.core.joran.spi.JoranException e) {
                getStatusManager().add((ch.qos.logback.core.status.Status) new ch.qos.logback.core.status.WarnStatus("Can't set manifest properties", e));
                this.androidPropsInitialized = false;
            }
        }
        return super.getProperty(str);
    }

    /* access modifiers changed from: 0000 */
    public final ch.qos.logback.core.spi.FilterReply getTurboFilterChainDecision_0_3OrMore(org.slf4j.Marker marker, ch.qos.logback.classic.Logger logger, ch.qos.logback.classic.Level level, java.lang.String str, java.lang.Object[] objArr, java.lang.Throwable th) {
        return this.turboFilterList.size() == 0 ? ch.qos.logback.core.spi.FilterReply.NEUTRAL : this.turboFilterList.getTurboFilterChainDecision(marker, logger, level, str, objArr, th);
    }

    /* access modifiers changed from: 0000 */
    public final ch.qos.logback.core.spi.FilterReply getTurboFilterChainDecision_1(org.slf4j.Marker marker, ch.qos.logback.classic.Logger logger, ch.qos.logback.classic.Level level, java.lang.String str, java.lang.Object obj, java.lang.Throwable th) {
        if (this.turboFilterList.size() == 0) {
            return ch.qos.logback.core.spi.FilterReply.NEUTRAL;
        }
        return this.turboFilterList.getTurboFilterChainDecision(marker, logger, level, str, new java.lang.Object[]{obj}, th);
    }

    /* access modifiers changed from: 0000 */
    public final ch.qos.logback.core.spi.FilterReply getTurboFilterChainDecision_2(org.slf4j.Marker marker, ch.qos.logback.classic.Logger logger, ch.qos.logback.classic.Level level, java.lang.String str, java.lang.Object obj, java.lang.Object obj2, java.lang.Throwable th) {
        if (this.turboFilterList.size() == 0) {
            return ch.qos.logback.core.spi.FilterReply.NEUTRAL;
        }
        return this.turboFilterList.getTurboFilterChainDecision(marker, logger, level, str, new java.lang.Object[]{obj, obj2}, th);
    }

    public ch.qos.logback.classic.spi.TurboFilterList getTurboFilterList() {
        return this.turboFilterList;
    }

    /* access modifiers changed from: 0000 */
    public void initEvaluatorMap() {
        putObject(ch.qos.logback.core.CoreConstants.EVALUATOR_MAP, new java.util.HashMap());
    }

    public boolean isPackagingDataEnabled() {
        return this.packagingDataEnabled;
    }

    /* access modifiers changed from: 0000 */
    public final void noAppenderDefinedWarning(ch.qos.logback.classic.Logger logger) {
        int i = this.noAppenderWarning;
        this.noAppenderWarning = i + 1;
        if (i == 0) {
            getStatusManager().add((ch.qos.logback.core.status.Status) new ch.qos.logback.core.status.WarnStatus("No appenders present in context [" + getName() + "] for logger [" + logger.getName() + "].", logger));
        }
    }

    public void putProperty(java.lang.String str, java.lang.String str2) {
        super.putProperty(str, str2);
        updateLoggerContextVO();
    }

    public void removeListener(ch.qos.logback.classic.spi.LoggerContextListener loggerContextListener) {
        this.loggerContextListenerList.remove(loggerContextListener);
    }

    public void reset() {
        this.resetCount++;
        super.reset();
        initEvaluatorMap();
        this.root.recursiveReset();
        resetTurboFilterList();
        fireOnReset();
        resetListenersExceptResetResistant();
        resetStatusListeners();
    }

    public void resetTurboFilterList() {
        java.util.Iterator it = this.turboFilterList.iterator();
        while (it.hasNext()) {
            ((ch.qos.logback.classic.turbo.TurboFilter) it.next()).stop();
        }
        this.turboFilterList.clear();
    }

    public void setMaxCallerDataDepth(int i) {
        this.maxCallerDataDepth = i;
    }

    public void setName(java.lang.String str) {
        super.setName(str);
        updateLoggerContextVO();
    }

    public void setPackagingDataEnabled(boolean z) {
        this.packagingDataEnabled = z;
    }

    /* access modifiers changed from: 0000 */
    public int size() {
        return this.size;
    }

    public void start() {
        super.start();
        fireOnStart();
    }

    public void stop() {
        reset();
        fireOnStop();
        resetAllListeners();
        super.stop();
    }

    public java.lang.String toString() {
        return getClass().getName() + "[" + getName() + "]";
    }
}
