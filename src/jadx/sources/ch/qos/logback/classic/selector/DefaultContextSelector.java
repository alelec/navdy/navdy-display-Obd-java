package ch.qos.logback.classic.selector;

public class DefaultContextSelector implements ch.qos.logback.classic.selector.ContextSelector {
    private ch.qos.logback.classic.LoggerContext defaultLoggerContext;

    public DefaultContextSelector(ch.qos.logback.classic.LoggerContext loggerContext) {
        this.defaultLoggerContext = loggerContext;
    }

    public ch.qos.logback.classic.LoggerContext detachLoggerContext(java.lang.String str) {
        return this.defaultLoggerContext;
    }

    public java.util.List<java.lang.String> getContextNames() {
        return java.util.Arrays.asList(new java.lang.String[]{this.defaultLoggerContext.getName()});
    }

    public ch.qos.logback.classic.LoggerContext getDefaultLoggerContext() {
        return this.defaultLoggerContext;
    }

    public ch.qos.logback.classic.LoggerContext getLoggerContext() {
        return getDefaultLoggerContext();
    }

    public ch.qos.logback.classic.LoggerContext getLoggerContext(java.lang.String str) {
        if (this.defaultLoggerContext.getName().equals(str)) {
            return this.defaultLoggerContext;
        }
        return null;
    }
}
