package ch.qos.logback.classic.selector;

public interface ContextSelector {
    ch.qos.logback.classic.LoggerContext detachLoggerContext(java.lang.String str);

    java.util.List<java.lang.String> getContextNames();

    ch.qos.logback.classic.LoggerContext getDefaultLoggerContext();

    ch.qos.logback.classic.LoggerContext getLoggerContext();

    ch.qos.logback.classic.LoggerContext getLoggerContext(java.lang.String str);
}
