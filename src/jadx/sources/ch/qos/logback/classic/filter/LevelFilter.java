package ch.qos.logback.classic.filter;

public class LevelFilter extends ch.qos.logback.core.filter.AbstractMatcherFilter<ch.qos.logback.classic.spi.ILoggingEvent> {
    ch.qos.logback.classic.Level level;

    public ch.qos.logback.core.spi.FilterReply decide(ch.qos.logback.classic.spi.ILoggingEvent iLoggingEvent) {
        return !isStarted() ? ch.qos.logback.core.spi.FilterReply.NEUTRAL : iLoggingEvent.getLevel().equals(this.level) ? this.onMatch : this.onMismatch;
    }

    public void setLevel(ch.qos.logback.classic.Level level2) {
        this.level = level2;
    }

    public void start() {
        if (this.level != null) {
            super.start();
        }
    }
}
