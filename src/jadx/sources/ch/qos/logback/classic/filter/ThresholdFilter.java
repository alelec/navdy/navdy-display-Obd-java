package ch.qos.logback.classic.filter;

public class ThresholdFilter extends ch.qos.logback.core.filter.Filter<ch.qos.logback.classic.spi.ILoggingEvent> {
    ch.qos.logback.classic.Level level;

    public ch.qos.logback.core.spi.FilterReply decide(ch.qos.logback.classic.spi.ILoggingEvent iLoggingEvent) {
        return !isStarted() ? ch.qos.logback.core.spi.FilterReply.NEUTRAL : iLoggingEvent.getLevel().isGreaterOrEqual(this.level) ? ch.qos.logback.core.spi.FilterReply.NEUTRAL : ch.qos.logback.core.spi.FilterReply.DENY;
    }

    public void setLevel(java.lang.String str) {
        this.level = ch.qos.logback.classic.Level.toLevel(str);
    }

    public void start() {
        if (this.level != null) {
            super.start();
        }
    }
}
