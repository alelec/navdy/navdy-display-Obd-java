package android.support.mediacompat;

public final class BuildConfig {
    public static final java.lang.String APPLICATION_ID = "android.support.mediacompat";
    public static final java.lang.String BUILD_TYPE = "release";
    public static final boolean DEBUG = false;
    public static final java.lang.String FLAVOR = "";
    public static final int VERSION_CODE = -1;
    public static final java.lang.String VERSION_NAME = "";
}
