package android.support.v4.hardware.fingerprint;

@android.support.annotation.RequiresApi(23)
@android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
@android.annotation.TargetApi(23)
public final class FingerprintManagerCompatApi23 {

    public static abstract class AuthenticationCallback {
        public void onAuthenticationError(int errMsgId, java.lang.CharSequence errString) {
        }

        public void onAuthenticationHelp(int helpMsgId, java.lang.CharSequence helpString) {
        }

        public void onAuthenticationSucceeded(android.support.v4.hardware.fingerprint.FingerprintManagerCompatApi23.AuthenticationResultInternal result) {
        }

        public void onAuthenticationFailed() {
        }
    }

    public static final class AuthenticationResultInternal {
        private android.support.v4.hardware.fingerprint.FingerprintManagerCompatApi23.CryptoObject mCryptoObject;

        public AuthenticationResultInternal(android.support.v4.hardware.fingerprint.FingerprintManagerCompatApi23.CryptoObject crypto) {
            this.mCryptoObject = crypto;
        }

        public android.support.v4.hardware.fingerprint.FingerprintManagerCompatApi23.CryptoObject getCryptoObject() {
            return this.mCryptoObject;
        }
    }

    public static class CryptoObject {
        private final javax.crypto.Cipher mCipher;
        private final javax.crypto.Mac mMac;
        private final java.security.Signature mSignature;

        public CryptoObject(java.security.Signature signature) {
            this.mSignature = signature;
            this.mCipher = null;
            this.mMac = null;
        }

        public CryptoObject(javax.crypto.Cipher cipher) {
            this.mCipher = cipher;
            this.mSignature = null;
            this.mMac = null;
        }

        public CryptoObject(javax.crypto.Mac mac) {
            this.mMac = mac;
            this.mCipher = null;
            this.mSignature = null;
        }

        public java.security.Signature getSignature() {
            return this.mSignature;
        }

        public javax.crypto.Cipher getCipher() {
            return this.mCipher;
        }

        public javax.crypto.Mac getMac() {
            return this.mMac;
        }
    }

    private static android.hardware.fingerprint.FingerprintManager getFingerprintManagerOrNull(android.content.Context context) {
        if (context.getPackageManager().hasSystemFeature("android.hardware.fingerprint")) {
            return (android.hardware.fingerprint.FingerprintManager) context.getSystemService(android.hardware.fingerprint.FingerprintManager.class);
        }
        return null;
    }

    public static boolean hasEnrolledFingerprints(android.content.Context context) {
        android.hardware.fingerprint.FingerprintManager fp = getFingerprintManagerOrNull(context);
        return fp != null && fp.hasEnrolledFingerprints();
    }

    public static boolean isHardwareDetected(android.content.Context context) {
        android.hardware.fingerprint.FingerprintManager fp = getFingerprintManagerOrNull(context);
        return fp != null && fp.isHardwareDetected();
    }

    public static void authenticate(android.content.Context context, android.support.v4.hardware.fingerprint.FingerprintManagerCompatApi23.CryptoObject crypto, int flags, java.lang.Object cancel, android.support.v4.hardware.fingerprint.FingerprintManagerCompatApi23.AuthenticationCallback callback, android.os.Handler handler) {
        android.hardware.fingerprint.FingerprintManager fp = getFingerprintManagerOrNull(context);
        if (fp != null) {
            fp.authenticate(wrapCryptoObject(crypto), (android.os.CancellationSignal) cancel, flags, wrapCallback(callback), handler);
        }
    }

    private static android.hardware.fingerprint.FingerprintManager.CryptoObject wrapCryptoObject(android.support.v4.hardware.fingerprint.FingerprintManagerCompatApi23.CryptoObject cryptoObject) {
        if (cryptoObject == null) {
            return null;
        }
        if (cryptoObject.getCipher() != null) {
            return new android.hardware.fingerprint.FingerprintManager.CryptoObject(cryptoObject.getCipher());
        }
        if (cryptoObject.getSignature() != null) {
            return new android.hardware.fingerprint.FingerprintManager.CryptoObject(cryptoObject.getSignature());
        }
        if (cryptoObject.getMac() != null) {
            return new android.hardware.fingerprint.FingerprintManager.CryptoObject(cryptoObject.getMac());
        }
        return null;
    }

    /* access modifiers changed from: private */
    public static android.support.v4.hardware.fingerprint.FingerprintManagerCompatApi23.CryptoObject unwrapCryptoObject(android.hardware.fingerprint.FingerprintManager.CryptoObject cryptoObject) {
        if (cryptoObject == null) {
            return null;
        }
        if (cryptoObject.getCipher() != null) {
            return new android.support.v4.hardware.fingerprint.FingerprintManagerCompatApi23.CryptoObject(cryptoObject.getCipher());
        }
        if (cryptoObject.getSignature() != null) {
            return new android.support.v4.hardware.fingerprint.FingerprintManagerCompatApi23.CryptoObject(cryptoObject.getSignature());
        }
        if (cryptoObject.getMac() != null) {
            return new android.support.v4.hardware.fingerprint.FingerprintManagerCompatApi23.CryptoObject(cryptoObject.getMac());
        }
        return null;
    }

    private static android.hardware.fingerprint.FingerprintManager.AuthenticationCallback wrapCallback(final android.support.v4.hardware.fingerprint.FingerprintManagerCompatApi23.AuthenticationCallback callback) {
        return new android.hardware.fingerprint.FingerprintManager.AuthenticationCallback() {
            public void onAuthenticationError(int errMsgId, java.lang.CharSequence errString) {
                callback.onAuthenticationError(errMsgId, errString);
            }

            public void onAuthenticationHelp(int helpMsgId, java.lang.CharSequence helpString) {
                callback.onAuthenticationHelp(helpMsgId, helpString);
            }

            public void onAuthenticationSucceeded(android.hardware.fingerprint.FingerprintManager.AuthenticationResult result) {
                callback.onAuthenticationSucceeded(new android.support.v4.hardware.fingerprint.FingerprintManagerCompatApi23.AuthenticationResultInternal(android.support.v4.hardware.fingerprint.FingerprintManagerCompatApi23.unwrapCryptoObject(result.getCryptoObject())));
            }

            public void onAuthenticationFailed() {
                callback.onAuthenticationFailed();
            }
        };
    }
}
