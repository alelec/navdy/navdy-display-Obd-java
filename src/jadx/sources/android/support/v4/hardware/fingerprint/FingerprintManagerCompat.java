package android.support.v4.hardware.fingerprint;

public final class FingerprintManagerCompat {
    static final android.support.v4.hardware.fingerprint.FingerprintManagerCompat.FingerprintManagerCompatImpl IMPL;
    private android.content.Context mContext;

    private static class Api23FingerprintManagerCompatImpl implements android.support.v4.hardware.fingerprint.FingerprintManagerCompat.FingerprintManagerCompatImpl {
        public boolean hasEnrolledFingerprints(android.content.Context context) {
            return android.support.v4.hardware.fingerprint.FingerprintManagerCompatApi23.hasEnrolledFingerprints(context);
        }

        public boolean isHardwareDetected(android.content.Context context) {
            return android.support.v4.hardware.fingerprint.FingerprintManagerCompatApi23.isHardwareDetected(context);
        }

        public void authenticate(android.content.Context context, android.support.v4.hardware.fingerprint.FingerprintManagerCompat.CryptoObject crypto, int flags, android.support.v4.os.CancellationSignal cancel, android.support.v4.hardware.fingerprint.FingerprintManagerCompat.AuthenticationCallback callback, android.os.Handler handler) {
            android.support.v4.hardware.fingerprint.FingerprintManagerCompatApi23.authenticate(context, wrapCryptoObject(crypto), flags, cancel != null ? cancel.getCancellationSignalObject() : null, wrapCallback(callback), handler);
        }

        private static android.support.v4.hardware.fingerprint.FingerprintManagerCompatApi23.CryptoObject wrapCryptoObject(android.support.v4.hardware.fingerprint.FingerprintManagerCompat.CryptoObject cryptoObject) {
            if (cryptoObject == null) {
                return null;
            }
            if (cryptoObject.getCipher() != null) {
                return new android.support.v4.hardware.fingerprint.FingerprintManagerCompatApi23.CryptoObject(cryptoObject.getCipher());
            }
            if (cryptoObject.getSignature() != null) {
                return new android.support.v4.hardware.fingerprint.FingerprintManagerCompatApi23.CryptoObject(cryptoObject.getSignature());
            }
            if (cryptoObject.getMac() != null) {
                return new android.support.v4.hardware.fingerprint.FingerprintManagerCompatApi23.CryptoObject(cryptoObject.getMac());
            }
            return null;
        }

        static android.support.v4.hardware.fingerprint.FingerprintManagerCompat.CryptoObject unwrapCryptoObject(android.support.v4.hardware.fingerprint.FingerprintManagerCompatApi23.CryptoObject cryptoObject) {
            if (cryptoObject == null) {
                return null;
            }
            if (cryptoObject.getCipher() != null) {
                return new android.support.v4.hardware.fingerprint.FingerprintManagerCompat.CryptoObject(cryptoObject.getCipher());
            }
            if (cryptoObject.getSignature() != null) {
                return new android.support.v4.hardware.fingerprint.FingerprintManagerCompat.CryptoObject(cryptoObject.getSignature());
            }
            if (cryptoObject.getMac() != null) {
                return new android.support.v4.hardware.fingerprint.FingerprintManagerCompat.CryptoObject(cryptoObject.getMac());
            }
            return null;
        }

        private static android.support.v4.hardware.fingerprint.FingerprintManagerCompatApi23.AuthenticationCallback wrapCallback(final android.support.v4.hardware.fingerprint.FingerprintManagerCompat.AuthenticationCallback callback) {
            return new android.support.v4.hardware.fingerprint.FingerprintManagerCompatApi23.AuthenticationCallback() {
                public void onAuthenticationError(int errMsgId, java.lang.CharSequence errString) {
                    callback.onAuthenticationError(errMsgId, errString);
                }

                public void onAuthenticationHelp(int helpMsgId, java.lang.CharSequence helpString) {
                    callback.onAuthenticationHelp(helpMsgId, helpString);
                }

                public void onAuthenticationSucceeded(android.support.v4.hardware.fingerprint.FingerprintManagerCompatApi23.AuthenticationResultInternal result) {
                    callback.onAuthenticationSucceeded(new android.support.v4.hardware.fingerprint.FingerprintManagerCompat.AuthenticationResult(android.support.v4.hardware.fingerprint.FingerprintManagerCompat.Api23FingerprintManagerCompatImpl.unwrapCryptoObject(result.getCryptoObject())));
                }

                public void onAuthenticationFailed() {
                    callback.onAuthenticationFailed();
                }
            };
        }
    }

    public static abstract class AuthenticationCallback {
        public void onAuthenticationError(int errMsgId, java.lang.CharSequence errString) {
        }

        public void onAuthenticationHelp(int helpMsgId, java.lang.CharSequence helpString) {
        }

        public void onAuthenticationSucceeded(android.support.v4.hardware.fingerprint.FingerprintManagerCompat.AuthenticationResult result) {
        }

        public void onAuthenticationFailed() {
        }
    }

    public static final class AuthenticationResult {
        private android.support.v4.hardware.fingerprint.FingerprintManagerCompat.CryptoObject mCryptoObject;

        public AuthenticationResult(android.support.v4.hardware.fingerprint.FingerprintManagerCompat.CryptoObject crypto) {
            this.mCryptoObject = crypto;
        }

        public android.support.v4.hardware.fingerprint.FingerprintManagerCompat.CryptoObject getCryptoObject() {
            return this.mCryptoObject;
        }
    }

    public static class CryptoObject {
        private final javax.crypto.Cipher mCipher;
        private final javax.crypto.Mac mMac;
        private final java.security.Signature mSignature;

        public CryptoObject(java.security.Signature signature) {
            this.mSignature = signature;
            this.mCipher = null;
            this.mMac = null;
        }

        public CryptoObject(javax.crypto.Cipher cipher) {
            this.mCipher = cipher;
            this.mSignature = null;
            this.mMac = null;
        }

        public CryptoObject(javax.crypto.Mac mac) {
            this.mMac = mac;
            this.mCipher = null;
            this.mSignature = null;
        }

        public java.security.Signature getSignature() {
            return this.mSignature;
        }

        public javax.crypto.Cipher getCipher() {
            return this.mCipher;
        }

        public javax.crypto.Mac getMac() {
            return this.mMac;
        }
    }

    private interface FingerprintManagerCompatImpl {
        void authenticate(android.content.Context context, android.support.v4.hardware.fingerprint.FingerprintManagerCompat.CryptoObject cryptoObject, int i, android.support.v4.os.CancellationSignal cancellationSignal, android.support.v4.hardware.fingerprint.FingerprintManagerCompat.AuthenticationCallback authenticationCallback, android.os.Handler handler);

        boolean hasEnrolledFingerprints(android.content.Context context);

        boolean isHardwareDetected(android.content.Context context);
    }

    private static class LegacyFingerprintManagerCompatImpl implements android.support.v4.hardware.fingerprint.FingerprintManagerCompat.FingerprintManagerCompatImpl {
        public boolean hasEnrolledFingerprints(android.content.Context context) {
            return false;
        }

        public boolean isHardwareDetected(android.content.Context context) {
            return false;
        }

        public void authenticate(android.content.Context context, android.support.v4.hardware.fingerprint.FingerprintManagerCompat.CryptoObject crypto, int flags, android.support.v4.os.CancellationSignal cancel, android.support.v4.hardware.fingerprint.FingerprintManagerCompat.AuthenticationCallback callback, android.os.Handler handler) {
        }
    }

    public static android.support.v4.hardware.fingerprint.FingerprintManagerCompat from(android.content.Context context) {
        return new android.support.v4.hardware.fingerprint.FingerprintManagerCompat(context);
    }

    private FingerprintManagerCompat(android.content.Context context) {
        this.mContext = context;
    }

    static {
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            IMPL = new android.support.v4.hardware.fingerprint.FingerprintManagerCompat.Api23FingerprintManagerCompatImpl();
        } else {
            IMPL = new android.support.v4.hardware.fingerprint.FingerprintManagerCompat.LegacyFingerprintManagerCompatImpl();
        }
    }

    public boolean hasEnrolledFingerprints() {
        return IMPL.hasEnrolledFingerprints(this.mContext);
    }

    public boolean isHardwareDetected() {
        return IMPL.isHardwareDetected(this.mContext);
    }

    public void authenticate(@android.support.annotation.Nullable android.support.v4.hardware.fingerprint.FingerprintManagerCompat.CryptoObject crypto, int flags, @android.support.annotation.Nullable android.support.v4.os.CancellationSignal cancel, @android.support.annotation.NonNull android.support.v4.hardware.fingerprint.FingerprintManagerCompat.AuthenticationCallback callback, @android.support.annotation.Nullable android.os.Handler handler) {
        IMPL.authenticate(this.mContext, crypto, flags, cancel, callback, handler);
    }
}
