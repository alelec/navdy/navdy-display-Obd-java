package android.support.v4.text;

@android.annotation.TargetApi(17)
@android.support.annotation.RequiresApi(17)
class TextUtilsCompatJellybeanMr1 {
    TextUtilsCompatJellybeanMr1() {
    }

    @android.support.annotation.NonNull
    public static java.lang.String htmlEncode(@android.support.annotation.NonNull java.lang.String s) {
        return android.text.TextUtils.htmlEncode(s);
    }

    public static int getLayoutDirectionFromLocale(@android.support.annotation.Nullable java.util.Locale locale) {
        return android.text.TextUtils.getLayoutDirectionFromLocale(locale);
    }
}
