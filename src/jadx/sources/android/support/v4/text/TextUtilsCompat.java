package android.support.v4.text;

public final class TextUtilsCompat {
    static java.lang.String ARAB_SCRIPT_SUBTAG = "Arab";
    static java.lang.String HEBR_SCRIPT_SUBTAG = "Hebr";
    private static final android.support.v4.text.TextUtilsCompat.TextUtilsCompatImpl IMPL;
    public static final java.util.Locale ROOT = new java.util.Locale("", "");

    private static class TextUtilsCompatImpl {
        TextUtilsCompatImpl() {
        }

        @android.support.annotation.NonNull
        public java.lang.String htmlEncode(@android.support.annotation.NonNull java.lang.String s) {
            java.lang.StringBuilder sb = new java.lang.StringBuilder();
            for (int i = 0; i < s.length(); i++) {
                char c = s.charAt(i);
                switch (c) {
                    case android.support.v4.view.MotionEventCompat.AXIS_GENERIC_3 /*34*/:
                        sb.append("&quot;");
                        break;
                    case android.support.v4.view.MotionEventCompat.AXIS_GENERIC_7 /*38*/:
                        sb.append("&amp;");
                        break;
                    case android.support.v4.view.MotionEventCompat.AXIS_GENERIC_8 /*39*/:
                        sb.append("&#39;");
                        break;
                    case '<':
                        sb.append("&lt;");
                        break;
                    case '>':
                        sb.append("&gt;");
                        break;
                    default:
                        sb.append(c);
                        break;
                }
            }
            return sb.toString();
        }

        public int getLayoutDirectionFromLocale(@android.support.annotation.Nullable java.util.Locale locale) {
            if (locale != null && !locale.equals(android.support.v4.text.TextUtilsCompat.ROOT)) {
                java.lang.String scriptSubtag = android.support.v4.text.ICUCompat.maximizeAndGetScript(locale);
                if (scriptSubtag == null) {
                    return getLayoutDirectionFromFirstChar(locale);
                }
                if (scriptSubtag.equalsIgnoreCase(android.support.v4.text.TextUtilsCompat.ARAB_SCRIPT_SUBTAG) || scriptSubtag.equalsIgnoreCase(android.support.v4.text.TextUtilsCompat.HEBR_SCRIPT_SUBTAG)) {
                    return 1;
                }
            }
            return 0;
        }

        private static int getLayoutDirectionFromFirstChar(@android.support.annotation.NonNull java.util.Locale locale) {
            switch (java.lang.Character.getDirectionality(locale.getDisplayName(locale).charAt(0))) {
                case 1:
                case 2:
                    return 1;
                default:
                    return 0;
            }
        }
    }

    private static class TextUtilsCompatJellybeanMr1Impl extends android.support.v4.text.TextUtilsCompat.TextUtilsCompatImpl {
        TextUtilsCompatJellybeanMr1Impl() {
        }

        @android.support.annotation.NonNull
        public java.lang.String htmlEncode(@android.support.annotation.NonNull java.lang.String s) {
            return android.support.v4.text.TextUtilsCompatJellybeanMr1.htmlEncode(s);
        }

        public int getLayoutDirectionFromLocale(@android.support.annotation.Nullable java.util.Locale locale) {
            return android.support.v4.text.TextUtilsCompatJellybeanMr1.getLayoutDirectionFromLocale(locale);
        }
    }

    static {
        if (android.os.Build.VERSION.SDK_INT >= 17) {
            IMPL = new android.support.v4.text.TextUtilsCompat.TextUtilsCompatJellybeanMr1Impl();
        } else {
            IMPL = new android.support.v4.text.TextUtilsCompat.TextUtilsCompatImpl();
        }
    }

    @android.support.annotation.NonNull
    public static java.lang.String htmlEncode(@android.support.annotation.NonNull java.lang.String s) {
        return IMPL.htmlEncode(s);
    }

    public static int getLayoutDirectionFromLocale(@android.support.annotation.Nullable java.util.Locale locale) {
        return IMPL.getLayoutDirectionFromLocale(locale);
    }

    private TextUtilsCompat() {
    }
}
