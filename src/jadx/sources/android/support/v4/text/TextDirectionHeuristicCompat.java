package android.support.v4.text;

public interface TextDirectionHeuristicCompat {
    boolean isRtl(java.lang.CharSequence charSequence, int i, int i2);

    boolean isRtl(char[] cArr, int i, int i2);
}
