package android.support.v4.text.util;

public final class LinkifyCompat {
    private static final java.util.Comparator<android.support.v4.text.util.LinkifyCompat.LinkSpec> COMPARATOR = new java.util.Comparator<android.support.v4.text.util.LinkifyCompat.LinkSpec>() {
        public final int compare(android.support.v4.text.util.LinkifyCompat.LinkSpec a, android.support.v4.text.util.LinkifyCompat.LinkSpec b) {
            if (a.start < b.start) {
                return -1;
            }
            if (a.start > b.start) {
                return 1;
            }
            if (a.end < b.end) {
                return 1;
            }
            if (a.end <= b.end) {
                return 0;
            }
            return -1;
        }
    };
    private static final java.lang.String[] EMPTY_STRING = new java.lang.String[0];

    private static class LinkSpec {
        int end;
        android.text.style.URLSpan frameworkAddedSpan;
        int start;
        java.lang.String url;

        LinkSpec() {
        }
    }

    @java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.SOURCE)
    public @interface LinkifyMask {
    }

    public static final boolean addLinks(@android.support.annotation.NonNull android.text.Spannable text, int mask) {
        if (mask == 0) {
            return false;
        }
        android.text.style.URLSpan[] old = (android.text.style.URLSpan[]) text.getSpans(0, text.length(), android.text.style.URLSpan.class);
        for (int i = old.length - 1; i >= 0; i--) {
            text.removeSpan(old[i]);
        }
        if ((mask & 4) != 0) {
            boolean frameworkReturn = android.text.util.Linkify.addLinks(text, 4);
        }
        java.util.ArrayList<android.support.v4.text.util.LinkifyCompat.LinkSpec> links = new java.util.ArrayList<>();
        if ((mask & 1) != 0) {
            android.text.Spannable spannable = text;
            gatherLinks(links, spannable, android.support.v4.util.PatternsCompat.AUTOLINK_WEB_URL, new java.lang.String[]{"http://", "https://", "rtsp://"}, android.text.util.Linkify.sUrlMatchFilter, null);
        }
        if ((mask & 2) != 0) {
            gatherLinks(links, text, android.support.v4.util.PatternsCompat.AUTOLINK_EMAIL_ADDRESS, new java.lang.String[]{"mailto:"}, null, null);
        }
        if ((mask & 8) != 0) {
            gatherMapLinks(links, text);
        }
        pruneOverlaps(links, text);
        if (links.size() == 0) {
            return false;
        }
        java.util.Iterator it = links.iterator();
        while (it.hasNext()) {
            android.support.v4.text.util.LinkifyCompat.LinkSpec link = (android.support.v4.text.util.LinkifyCompat.LinkSpec) it.next();
            if (link.frameworkAddedSpan == null) {
                applyLink(link.url, link.start, link.end, text);
            }
        }
        return true;
    }

    public static final boolean addLinks(@android.support.annotation.NonNull android.widget.TextView text, int mask) {
        if (mask == 0) {
            return false;
        }
        java.lang.CharSequence t = text.getText();
        if (!(t instanceof android.text.Spannable)) {
            android.text.SpannableString s = android.text.SpannableString.valueOf(t);
            if (!addLinks((android.text.Spannable) s, mask)) {
                return false;
            }
            addLinkMovementMethod(text);
            text.setText(s);
            return true;
        } else if (!addLinks((android.text.Spannable) t, mask)) {
            return false;
        } else {
            addLinkMovementMethod(text);
            return true;
        }
    }

    public static final void addLinks(@android.support.annotation.NonNull android.widget.TextView text, @android.support.annotation.NonNull java.util.regex.Pattern pattern, @android.support.annotation.Nullable java.lang.String scheme) {
        addLinks(text, pattern, scheme, (java.lang.String[]) null, (android.text.util.Linkify.MatchFilter) null, (android.text.util.Linkify.TransformFilter) null);
    }

    public static final void addLinks(@android.support.annotation.NonNull android.widget.TextView text, @android.support.annotation.NonNull java.util.regex.Pattern pattern, @android.support.annotation.Nullable java.lang.String scheme, @android.support.annotation.Nullable android.text.util.Linkify.MatchFilter matchFilter, @android.support.annotation.Nullable android.text.util.Linkify.TransformFilter transformFilter) {
        addLinks(text, pattern, scheme, (java.lang.String[]) null, matchFilter, transformFilter);
    }

    public static final void addLinks(@android.support.annotation.NonNull android.widget.TextView text, @android.support.annotation.NonNull java.util.regex.Pattern pattern, @android.support.annotation.Nullable java.lang.String defaultScheme, @android.support.annotation.Nullable java.lang.String[] schemes, @android.support.annotation.Nullable android.text.util.Linkify.MatchFilter matchFilter, @android.support.annotation.Nullable android.text.util.Linkify.TransformFilter transformFilter) {
        android.text.SpannableString spannable = android.text.SpannableString.valueOf(text.getText());
        if (addLinks((android.text.Spannable) spannable, pattern, defaultScheme, schemes, matchFilter, transformFilter)) {
            text.setText(spannable);
            addLinkMovementMethod(text);
        }
    }

    public static final boolean addLinks(@android.support.annotation.NonNull android.text.Spannable text, @android.support.annotation.NonNull java.util.regex.Pattern pattern, @android.support.annotation.Nullable java.lang.String scheme) {
        return addLinks(text, pattern, scheme, (java.lang.String[]) null, (android.text.util.Linkify.MatchFilter) null, (android.text.util.Linkify.TransformFilter) null);
    }

    public static final boolean addLinks(@android.support.annotation.NonNull android.text.Spannable spannable, @android.support.annotation.NonNull java.util.regex.Pattern pattern, @android.support.annotation.Nullable java.lang.String scheme, @android.support.annotation.Nullable android.text.util.Linkify.MatchFilter matchFilter, @android.support.annotation.Nullable android.text.util.Linkify.TransformFilter transformFilter) {
        return addLinks(spannable, pattern, scheme, (java.lang.String[]) null, matchFilter, transformFilter);
    }

    public static final boolean addLinks(@android.support.annotation.NonNull android.text.Spannable spannable, @android.support.annotation.NonNull java.util.regex.Pattern pattern, @android.support.annotation.Nullable java.lang.String defaultScheme, @android.support.annotation.Nullable java.lang.String[] schemes, @android.support.annotation.Nullable android.text.util.Linkify.MatchFilter matchFilter, @android.support.annotation.Nullable android.text.util.Linkify.TransformFilter transformFilter) {
        if (defaultScheme == null) {
            defaultScheme = "";
        }
        if (schemes == null || schemes.length < 1) {
            schemes = EMPTY_STRING;
        }
        java.lang.String[] schemesCopy = new java.lang.String[(schemes.length + 1)];
        schemesCopy[0] = defaultScheme.toLowerCase(java.util.Locale.ROOT);
        for (int index = 0; index < schemes.length; index++) {
            java.lang.String scheme = schemes[index];
            schemesCopy[index + 1] = scheme == null ? "" : scheme.toLowerCase(java.util.Locale.ROOT);
        }
        boolean hasMatches = false;
        java.util.regex.Matcher m = pattern.matcher(spannable);
        while (m.find()) {
            int start = m.start();
            int end = m.end();
            boolean allowed = true;
            if (matchFilter != null) {
                allowed = matchFilter.acceptMatch(spannable, start, end);
            }
            if (allowed) {
                applyLink(makeUrl(m.group(0), schemesCopy, m, transformFilter), start, end, spannable);
                hasMatches = true;
            }
        }
        return hasMatches;
    }

    private static void addLinkMovementMethod(@android.support.annotation.NonNull android.widget.TextView t) {
        android.text.method.MovementMethod m = t.getMovementMethod();
        if ((m == null || !(m instanceof android.text.method.LinkMovementMethod)) && t.getLinksClickable()) {
            t.setMovementMethod(android.text.method.LinkMovementMethod.getInstance());
        }
    }

    private static java.lang.String makeUrl(@android.support.annotation.NonNull java.lang.String url, @android.support.annotation.NonNull java.lang.String[] prefixes, java.util.regex.Matcher matcher, @android.support.annotation.Nullable android.text.util.Linkify.TransformFilter filter) {
        if (filter != null) {
            url = filter.transformUrl(matcher, url);
        }
        boolean hasPrefix = false;
        int i = 0;
        while (true) {
            if (i >= prefixes.length) {
                break;
            }
            if (url.regionMatches(true, 0, prefixes[i], 0, prefixes[i].length())) {
                hasPrefix = true;
                if (!url.regionMatches(false, 0, prefixes[i], 0, prefixes[i].length())) {
                    url = prefixes[i] + url.substring(prefixes[i].length());
                }
            } else {
                i++;
            }
        }
        if (hasPrefix || prefixes.length <= 0) {
            return url;
        }
        return prefixes[0] + url;
    }

    private static void gatherLinks(java.util.ArrayList<android.support.v4.text.util.LinkifyCompat.LinkSpec> links, android.text.Spannable s, java.util.regex.Pattern pattern, java.lang.String[] schemes, android.text.util.Linkify.MatchFilter matchFilter, android.text.util.Linkify.TransformFilter transformFilter) {
        java.util.regex.Matcher m = pattern.matcher(s);
        while (m.find()) {
            int start = m.start();
            int end = m.end();
            if (matchFilter == null || matchFilter.acceptMatch(s, start, end)) {
                android.support.v4.text.util.LinkifyCompat.LinkSpec spec = new android.support.v4.text.util.LinkifyCompat.LinkSpec();
                spec.url = makeUrl(m.group(0), schemes, m, transformFilter);
                spec.start = start;
                spec.end = end;
                links.add(spec);
            }
        }
    }

    private static void applyLink(java.lang.String url, int start, int end, android.text.Spannable text) {
        text.setSpan(new android.text.style.URLSpan(url), start, end, 33);
    }

    private static final void gatherMapLinks(java.util.ArrayList<android.support.v4.text.util.LinkifyCompat.LinkSpec> links, android.text.Spannable s) {
        java.lang.String string = s.toString();
        int base = 0;
        while (true) {
            try {
                java.lang.String address = android.webkit.WebView.findAddress(string);
                if (address != null) {
                    int start = string.indexOf(address);
                    if (start >= 0) {
                        android.support.v4.text.util.LinkifyCompat.LinkSpec spec = new android.support.v4.text.util.LinkifyCompat.LinkSpec();
                        int end = start + address.length();
                        spec.start = base + start;
                        spec.end = base + end;
                        string = string.substring(end);
                        base += end;
                        try {
                            spec.url = "geo:0,0?q=" + java.net.URLEncoder.encode(address, util.Util.UTF_8);
                            links.add(spec);
                        } catch (java.io.UnsupportedEncodingException e) {
                        }
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            } catch (java.lang.UnsupportedOperationException e2) {
                return;
            }
        }
    }

    private static final void pruneOverlaps(java.util.ArrayList<android.support.v4.text.util.LinkifyCompat.LinkSpec> links, android.text.Spannable text) {
        android.text.style.URLSpan[] urlSpans = (android.text.style.URLSpan[]) text.getSpans(0, text.length(), android.text.style.URLSpan.class);
        for (int i = 0; i < urlSpans.length; i++) {
            android.support.v4.text.util.LinkifyCompat.LinkSpec spec = new android.support.v4.text.util.LinkifyCompat.LinkSpec();
            spec.frameworkAddedSpan = urlSpans[i];
            spec.start = text.getSpanStart(urlSpans[i]);
            spec.end = text.getSpanEnd(urlSpans[i]);
            links.add(spec);
        }
        java.util.Collections.sort(links, COMPARATOR);
        int len = links.size();
        int i2 = 0;
        while (i2 < len - 1) {
            android.support.v4.text.util.LinkifyCompat.LinkSpec a = (android.support.v4.text.util.LinkifyCompat.LinkSpec) links.get(i2);
            android.support.v4.text.util.LinkifyCompat.LinkSpec b = (android.support.v4.text.util.LinkifyCompat.LinkSpec) links.get(i2 + 1);
            int remove = -1;
            if (a.start <= b.start && a.end > b.start) {
                if (b.end <= a.end) {
                    remove = i2 + 1;
                } else if (a.end - a.start > b.end - b.start) {
                    remove = i2 + 1;
                } else if (a.end - a.start < b.end - b.start) {
                    remove = i2;
                }
                if (remove != -1) {
                    android.text.style.URLSpan span = ((android.support.v4.text.util.LinkifyCompat.LinkSpec) links.get(remove)).frameworkAddedSpan;
                    if (span != null) {
                        text.removeSpan(span);
                    }
                    links.remove(remove);
                    len--;
                }
            }
            i2++;
        }
    }

    private LinkifyCompat() {
    }
}
