package android.support.v4.text;

public final class ICUCompat {
    private static final android.support.v4.text.ICUCompat.ICUCompatImpl IMPL;

    interface ICUCompatImpl {
        java.lang.String maximizeAndGetScript(java.util.Locale locale);
    }

    static class ICUCompatImplBase implements android.support.v4.text.ICUCompat.ICUCompatImpl {
        ICUCompatImplBase() {
        }

        public java.lang.String maximizeAndGetScript(java.util.Locale locale) {
            return null;
        }
    }

    static class ICUCompatImplIcs implements android.support.v4.text.ICUCompat.ICUCompatImpl {
        ICUCompatImplIcs() {
        }

        public java.lang.String maximizeAndGetScript(java.util.Locale locale) {
            return android.support.v4.text.ICUCompatIcs.maximizeAndGetScript(locale);
        }
    }

    static class ICUCompatImplLollipop implements android.support.v4.text.ICUCompat.ICUCompatImpl {
        ICUCompatImplLollipop() {
        }

        public java.lang.String maximizeAndGetScript(java.util.Locale locale) {
            return android.support.v4.text.ICUCompatApi23.maximizeAndGetScript(locale);
        }
    }

    static {
        int version = android.os.Build.VERSION.SDK_INT;
        if (version >= 21) {
            IMPL = new android.support.v4.text.ICUCompat.ICUCompatImplLollipop();
        } else if (version >= 14) {
            IMPL = new android.support.v4.text.ICUCompat.ICUCompatImplIcs();
        } else {
            IMPL = new android.support.v4.text.ICUCompat.ICUCompatImplBase();
        }
    }

    public static java.lang.String maximizeAndGetScript(java.util.Locale locale) {
        return IMPL.maximizeAndGetScript(locale);
    }

    private ICUCompat() {
    }
}
