package android.support.v4.text;

@android.annotation.TargetApi(14)
@android.support.annotation.RequiresApi(14)
class ICUCompatIcs {
    private static final java.lang.String TAG = "ICUCompatIcs";
    private static java.lang.reflect.Method sAddLikelySubtagsMethod;
    private static java.lang.reflect.Method sGetScriptMethod;

    ICUCompatIcs() {
    }

    static {
        try {
            java.lang.Class<?> clazz = java.lang.Class.forName("libcore.icu.ICU");
            if (clazz != null) {
                sGetScriptMethod = clazz.getMethod("getScript", new java.lang.Class[]{java.lang.String.class});
                sAddLikelySubtagsMethod = clazz.getMethod("addLikelySubtags", new java.lang.Class[]{java.lang.String.class});
            }
        } catch (java.lang.Exception e) {
            sGetScriptMethod = null;
            sAddLikelySubtagsMethod = null;
            android.util.Log.w(TAG, e);
        }
    }

    public static java.lang.String maximizeAndGetScript(java.util.Locale locale) {
        java.lang.String localeWithSubtags = addLikelySubtags(locale);
        if (localeWithSubtags != null) {
            return getScript(localeWithSubtags);
        }
        return null;
    }

    private static java.lang.String getScript(java.lang.String localeStr) {
        try {
            if (sGetScriptMethod != null) {
                return (java.lang.String) sGetScriptMethod.invoke(null, new java.lang.Object[]{localeStr});
            }
        } catch (java.lang.IllegalAccessException e) {
            android.util.Log.w(TAG, e);
        } catch (java.lang.reflect.InvocationTargetException e2) {
            android.util.Log.w(TAG, e2);
        }
        return null;
    }

    private static java.lang.String addLikelySubtags(java.util.Locale locale) {
        java.lang.String localeStr = locale.toString();
        try {
            if (sAddLikelySubtagsMethod != null) {
                return (java.lang.String) sAddLikelySubtagsMethod.invoke(null, new java.lang.Object[]{localeStr});
            }
        } catch (java.lang.IllegalAccessException e) {
            android.util.Log.w(TAG, e);
        } catch (java.lang.reflect.InvocationTargetException e2) {
            android.util.Log.w(TAG, e2);
        }
        return localeStr;
    }
}
