package android.support.v4.text;

public final class TextDirectionHeuristicsCompat {
    public static final android.support.v4.text.TextDirectionHeuristicCompat ANYRTL_LTR = new android.support.v4.text.TextDirectionHeuristicsCompat.TextDirectionHeuristicInternal(android.support.v4.text.TextDirectionHeuristicsCompat.AnyStrong.INSTANCE_RTL, false);
    public static final android.support.v4.text.TextDirectionHeuristicCompat FIRSTSTRONG_LTR = new android.support.v4.text.TextDirectionHeuristicsCompat.TextDirectionHeuristicInternal(android.support.v4.text.TextDirectionHeuristicsCompat.FirstStrong.INSTANCE, false);
    public static final android.support.v4.text.TextDirectionHeuristicCompat FIRSTSTRONG_RTL = new android.support.v4.text.TextDirectionHeuristicsCompat.TextDirectionHeuristicInternal(android.support.v4.text.TextDirectionHeuristicsCompat.FirstStrong.INSTANCE, true);
    public static final android.support.v4.text.TextDirectionHeuristicCompat LOCALE = android.support.v4.text.TextDirectionHeuristicsCompat.TextDirectionHeuristicLocale.INSTANCE;
    public static final android.support.v4.text.TextDirectionHeuristicCompat LTR = new android.support.v4.text.TextDirectionHeuristicsCompat.TextDirectionHeuristicInternal(null, false);
    public static final android.support.v4.text.TextDirectionHeuristicCompat RTL = new android.support.v4.text.TextDirectionHeuristicsCompat.TextDirectionHeuristicInternal(null, true);
    private static final int STATE_FALSE = 1;
    private static final int STATE_TRUE = 0;
    private static final int STATE_UNKNOWN = 2;

    private static class AnyStrong implements android.support.v4.text.TextDirectionHeuristicsCompat.TextDirectionAlgorithm {
        public static final android.support.v4.text.TextDirectionHeuristicsCompat.AnyStrong INSTANCE_LTR = new android.support.v4.text.TextDirectionHeuristicsCompat.AnyStrong(false);
        public static final android.support.v4.text.TextDirectionHeuristicsCompat.AnyStrong INSTANCE_RTL = new android.support.v4.text.TextDirectionHeuristicsCompat.AnyStrong(true);
        private final boolean mLookForRtl;

        public int checkRtl(java.lang.CharSequence cs, int start, int count) {
            boolean haveUnlookedFor = false;
            int e = start + count;
            for (int i = start; i < e; i++) {
                switch (android.support.v4.text.TextDirectionHeuristicsCompat.isRtlText(java.lang.Character.getDirectionality(cs.charAt(i)))) {
                    case 0:
                        if (!this.mLookForRtl) {
                            haveUnlookedFor = true;
                            break;
                        } else {
                            return 0;
                        }
                    case 1:
                        if (this.mLookForRtl) {
                            haveUnlookedFor = true;
                            break;
                        } else {
                            return 1;
                        }
                }
            }
            if (!haveUnlookedFor) {
                return 2;
            }
            if (!this.mLookForRtl) {
                return 0;
            }
            return 1;
        }

        private AnyStrong(boolean lookForRtl) {
            this.mLookForRtl = lookForRtl;
        }
    }

    private static class FirstStrong implements android.support.v4.text.TextDirectionHeuristicsCompat.TextDirectionAlgorithm {
        public static final android.support.v4.text.TextDirectionHeuristicsCompat.FirstStrong INSTANCE = new android.support.v4.text.TextDirectionHeuristicsCompat.FirstStrong();

        public int checkRtl(java.lang.CharSequence cs, int start, int count) {
            int result = 2;
            int e = start + count;
            for (int i = start; i < e && result == 2; i++) {
                result = android.support.v4.text.TextDirectionHeuristicsCompat.isRtlTextOrFormat(java.lang.Character.getDirectionality(cs.charAt(i)));
            }
            return result;
        }

        private FirstStrong() {
        }
    }

    private interface TextDirectionAlgorithm {
        int checkRtl(java.lang.CharSequence charSequence, int i, int i2);
    }

    private static abstract class TextDirectionHeuristicImpl implements android.support.v4.text.TextDirectionHeuristicCompat {
        private final android.support.v4.text.TextDirectionHeuristicsCompat.TextDirectionAlgorithm mAlgorithm;

        /* access modifiers changed from: protected */
        public abstract boolean defaultIsRtl();

        public TextDirectionHeuristicImpl(android.support.v4.text.TextDirectionHeuristicsCompat.TextDirectionAlgorithm algorithm) {
            this.mAlgorithm = algorithm;
        }

        public boolean isRtl(char[] array, int start, int count) {
            return isRtl((java.lang.CharSequence) java.nio.CharBuffer.wrap(array), start, count);
        }

        public boolean isRtl(java.lang.CharSequence cs, int start, int count) {
            if (cs == null || start < 0 || count < 0 || cs.length() - count < start) {
                throw new java.lang.IllegalArgumentException();
            } else if (this.mAlgorithm == null) {
                return defaultIsRtl();
            } else {
                return doCheck(cs, start, count);
            }
        }

        private boolean doCheck(java.lang.CharSequence cs, int start, int count) {
            switch (this.mAlgorithm.checkRtl(cs, start, count)) {
                case 0:
                    return true;
                case 1:
                    return false;
                default:
                    return defaultIsRtl();
            }
        }
    }

    private static class TextDirectionHeuristicInternal extends android.support.v4.text.TextDirectionHeuristicsCompat.TextDirectionHeuristicImpl {
        private final boolean mDefaultIsRtl;

        TextDirectionHeuristicInternal(android.support.v4.text.TextDirectionHeuristicsCompat.TextDirectionAlgorithm algorithm, boolean defaultIsRtl) {
            super(algorithm);
            this.mDefaultIsRtl = defaultIsRtl;
        }

        /* access modifiers changed from: protected */
        public boolean defaultIsRtl() {
            return this.mDefaultIsRtl;
        }
    }

    private static class TextDirectionHeuristicLocale extends android.support.v4.text.TextDirectionHeuristicsCompat.TextDirectionHeuristicImpl {
        public static final android.support.v4.text.TextDirectionHeuristicsCompat.TextDirectionHeuristicLocale INSTANCE = new android.support.v4.text.TextDirectionHeuristicsCompat.TextDirectionHeuristicLocale();

        public TextDirectionHeuristicLocale() {
            super(null);
        }

        /* access modifiers changed from: protected */
        public boolean defaultIsRtl() {
            if (android.support.v4.text.TextUtilsCompat.getLayoutDirectionFromLocale(java.util.Locale.getDefault()) == 1) {
                return true;
            }
            return false;
        }
    }

    static int isRtlText(int directionality) {
        switch (directionality) {
            case 0:
                return 1;
            case 1:
            case 2:
                return 0;
            default:
                return 2;
        }
    }

    static int isRtlTextOrFormat(int directionality) {
        switch (directionality) {
            case 0:
            case android.support.v4.view.MotionEventCompat.AXIS_RZ /*14*/:
            case 15:
                return 1;
            case 1:
            case 2:
            case 16:
            case 17:
                return 0;
            default:
                return 2;
        }
    }

    private TextDirectionHeuristicsCompat() {
    }
}
