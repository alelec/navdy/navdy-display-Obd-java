package android.support.v4.text;

@android.annotation.TargetApi(23)
@android.support.annotation.RequiresApi(23)
class ICUCompatApi23 {
    private static final java.lang.String TAG = "ICUCompatIcs";
    private static java.lang.reflect.Method sAddLikelySubtagsMethod;

    ICUCompatApi23() {
    }

    static {
        try {
            sAddLikelySubtagsMethod = java.lang.Class.forName("libcore.icu.ICU").getMethod("addLikelySubtags", new java.lang.Class[]{java.util.Locale.class});
        } catch (java.lang.Exception e) {
            throw new java.lang.IllegalStateException(e);
        }
    }

    public static java.lang.String maximizeAndGetScript(java.util.Locale locale) {
        try {
            return ((java.util.Locale) sAddLikelySubtagsMethod.invoke(null, new java.lang.Object[]{locale})).getScript();
        } catch (java.lang.reflect.InvocationTargetException e) {
            android.util.Log.w(TAG, e);
        } catch (java.lang.IllegalAccessException e2) {
            android.util.Log.w(TAG, e2);
        }
        return locale.getScript();
    }
}
