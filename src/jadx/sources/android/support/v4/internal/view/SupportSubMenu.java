package android.support.v4.internal.view;

@android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
public interface SupportSubMenu extends android.support.v4.internal.view.SupportMenu, android.view.SubMenu {
}
