package android.support.v4.internal.view;

@android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
public interface SupportMenuItem extends android.view.MenuItem {
    public static final int SHOW_AS_ACTION_ALWAYS = 2;
    public static final int SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW = 8;
    public static final int SHOW_AS_ACTION_IF_ROOM = 1;
    public static final int SHOW_AS_ACTION_NEVER = 0;
    public static final int SHOW_AS_ACTION_WITH_TEXT = 4;

    boolean collapseActionView();

    boolean expandActionView();

    android.view.View getActionView();

    android.support.v4.view.ActionProvider getSupportActionProvider();

    boolean isActionViewExpanded();

    android.view.MenuItem setActionView(int i);

    android.view.MenuItem setActionView(android.view.View view);

    void setShowAsAction(int i);

    android.view.MenuItem setShowAsActionFlags(int i);

    android.support.v4.internal.view.SupportMenuItem setSupportActionProvider(android.support.v4.view.ActionProvider actionProvider);

    android.support.v4.internal.view.SupportMenuItem setSupportOnActionExpandListener(android.support.v4.view.MenuItemCompat.OnActionExpandListener onActionExpandListener);
}
