package android.support.v4.widget;

@android.annotation.TargetApi(21)
@android.support.annotation.RequiresApi(21)
class DrawerLayoutCompatApi21 {
    private static final int[] THEME_ATTRS = {16843828};

    static class InsetsListener implements android.view.View.OnApplyWindowInsetsListener {
        InsetsListener() {
        }

        public android.view.WindowInsets onApplyWindowInsets(android.view.View v, android.view.WindowInsets insets) {
            ((android.support.v4.widget.DrawerLayoutImpl) v).setChildInsets(insets, insets.getSystemWindowInsetTop() > 0);
            return insets.consumeSystemWindowInsets();
        }
    }

    DrawerLayoutCompatApi21() {
    }

    public static void configureApplyInsets(android.view.View drawerLayout) {
        if (drawerLayout instanceof android.support.v4.widget.DrawerLayoutImpl) {
            drawerLayout.setOnApplyWindowInsetsListener(new android.support.v4.widget.DrawerLayoutCompatApi21.InsetsListener());
            drawerLayout.setSystemUiVisibility(1280);
        }
    }

    public static void dispatchChildInsets(android.view.View child, java.lang.Object insets, int gravity) {
        android.view.WindowInsets wi = (android.view.WindowInsets) insets;
        if (gravity == 3) {
            wi = wi.replaceSystemWindowInsets(wi.getSystemWindowInsetLeft(), wi.getSystemWindowInsetTop(), 0, wi.getSystemWindowInsetBottom());
        } else if (gravity == 5) {
            wi = wi.replaceSystemWindowInsets(0, wi.getSystemWindowInsetTop(), wi.getSystemWindowInsetRight(), wi.getSystemWindowInsetBottom());
        }
        child.dispatchApplyWindowInsets(wi);
    }

    public static void applyMarginInsets(android.view.ViewGroup.MarginLayoutParams lp, java.lang.Object insets, int gravity) {
        android.view.WindowInsets wi = (android.view.WindowInsets) insets;
        if (gravity == 3) {
            wi = wi.replaceSystemWindowInsets(wi.getSystemWindowInsetLeft(), wi.getSystemWindowInsetTop(), 0, wi.getSystemWindowInsetBottom());
        } else if (gravity == 5) {
            wi = wi.replaceSystemWindowInsets(0, wi.getSystemWindowInsetTop(), wi.getSystemWindowInsetRight(), wi.getSystemWindowInsetBottom());
        }
        lp.leftMargin = wi.getSystemWindowInsetLeft();
        lp.topMargin = wi.getSystemWindowInsetTop();
        lp.rightMargin = wi.getSystemWindowInsetRight();
        lp.bottomMargin = wi.getSystemWindowInsetBottom();
    }

    public static int getTopInset(java.lang.Object insets) {
        if (insets != null) {
            return ((android.view.WindowInsets) insets).getSystemWindowInsetTop();
        }
        return 0;
    }

    public static android.graphics.drawable.Drawable getDefaultStatusBarBackground(android.content.Context context) {
        android.content.res.TypedArray a = context.obtainStyledAttributes(THEME_ATTRS);
        try {
            return a.getDrawable(0);
        } finally {
            a.recycle();
        }
    }
}
