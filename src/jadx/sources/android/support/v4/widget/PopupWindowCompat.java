package android.support.v4.widget;

public final class PopupWindowCompat {
    static final android.support.v4.widget.PopupWindowCompat.PopupWindowImpl IMPL;

    static class Api21PopupWindowImpl extends android.support.v4.widget.PopupWindowCompat.KitKatPopupWindowImpl {
        Api21PopupWindowImpl() {
        }

        public void setOverlapAnchor(android.widget.PopupWindow popupWindow, boolean overlapAnchor) {
            android.support.v4.widget.PopupWindowCompatApi21.setOverlapAnchor(popupWindow, overlapAnchor);
        }

        public boolean getOverlapAnchor(android.widget.PopupWindow popupWindow) {
            return android.support.v4.widget.PopupWindowCompatApi21.getOverlapAnchor(popupWindow);
        }
    }

    static class Api23PopupWindowImpl extends android.support.v4.widget.PopupWindowCompat.Api21PopupWindowImpl {
        Api23PopupWindowImpl() {
        }

        public void setOverlapAnchor(android.widget.PopupWindow popupWindow, boolean overlapAnchor) {
            android.support.v4.widget.PopupWindowCompatApi23.setOverlapAnchor(popupWindow, overlapAnchor);
        }

        public boolean getOverlapAnchor(android.widget.PopupWindow popupWindow) {
            return android.support.v4.widget.PopupWindowCompatApi23.getOverlapAnchor(popupWindow);
        }

        public void setWindowLayoutType(android.widget.PopupWindow popupWindow, int layoutType) {
            android.support.v4.widget.PopupWindowCompatApi23.setWindowLayoutType(popupWindow, layoutType);
        }

        public int getWindowLayoutType(android.widget.PopupWindow popupWindow) {
            return android.support.v4.widget.PopupWindowCompatApi23.getWindowLayoutType(popupWindow);
        }
    }

    static class BasePopupWindowImpl implements android.support.v4.widget.PopupWindowCompat.PopupWindowImpl {
        private static java.lang.reflect.Method sGetWindowLayoutTypeMethod;
        private static boolean sGetWindowLayoutTypeMethodAttempted;
        private static java.lang.reflect.Method sSetWindowLayoutTypeMethod;
        private static boolean sSetWindowLayoutTypeMethodAttempted;

        BasePopupWindowImpl() {
        }

        public void showAsDropDown(android.widget.PopupWindow popup, android.view.View anchor, int xoff, int yoff, int gravity) {
            if ((android.support.v4.view.GravityCompat.getAbsoluteGravity(gravity, android.support.v4.view.ViewCompat.getLayoutDirection(anchor)) & 7) == 5) {
                xoff -= popup.getWidth() - anchor.getWidth();
            }
            popup.showAsDropDown(anchor, xoff, yoff);
        }

        public void setOverlapAnchor(android.widget.PopupWindow popupWindow, boolean overlapAnchor) {
        }

        public boolean getOverlapAnchor(android.widget.PopupWindow popupWindow) {
            return false;
        }

        public void setWindowLayoutType(android.widget.PopupWindow popupWindow, int layoutType) {
            if (!sSetWindowLayoutTypeMethodAttempted) {
                try {
                    sSetWindowLayoutTypeMethod = android.widget.PopupWindow.class.getDeclaredMethod("setWindowLayoutType", new java.lang.Class[]{java.lang.Integer.TYPE});
                    sSetWindowLayoutTypeMethod.setAccessible(true);
                } catch (java.lang.Exception e) {
                }
                sSetWindowLayoutTypeMethodAttempted = true;
            }
            if (sSetWindowLayoutTypeMethod != null) {
                try {
                    sSetWindowLayoutTypeMethod.invoke(popupWindow, new java.lang.Object[]{java.lang.Integer.valueOf(layoutType)});
                } catch (java.lang.Exception e2) {
                }
            }
        }

        public int getWindowLayoutType(android.widget.PopupWindow popupWindow) {
            if (!sGetWindowLayoutTypeMethodAttempted) {
                try {
                    sGetWindowLayoutTypeMethod = android.widget.PopupWindow.class.getDeclaredMethod("getWindowLayoutType", new java.lang.Class[0]);
                    sGetWindowLayoutTypeMethod.setAccessible(true);
                } catch (java.lang.Exception e) {
                }
                sGetWindowLayoutTypeMethodAttempted = true;
            }
            if (sGetWindowLayoutTypeMethod != null) {
                try {
                    return ((java.lang.Integer) sGetWindowLayoutTypeMethod.invoke(popupWindow, new java.lang.Object[0])).intValue();
                } catch (java.lang.Exception e2) {
                }
            }
            return 0;
        }
    }

    static class KitKatPopupWindowImpl extends android.support.v4.widget.PopupWindowCompat.BasePopupWindowImpl {
        KitKatPopupWindowImpl() {
        }

        public void showAsDropDown(android.widget.PopupWindow popup, android.view.View anchor, int xoff, int yoff, int gravity) {
            android.support.v4.widget.PopupWindowCompatKitKat.showAsDropDown(popup, anchor, xoff, yoff, gravity);
        }
    }

    interface PopupWindowImpl {
        boolean getOverlapAnchor(android.widget.PopupWindow popupWindow);

        int getWindowLayoutType(android.widget.PopupWindow popupWindow);

        void setOverlapAnchor(android.widget.PopupWindow popupWindow, boolean z);

        void setWindowLayoutType(android.widget.PopupWindow popupWindow, int i);

        void showAsDropDown(android.widget.PopupWindow popupWindow, android.view.View view, int i, int i2, int i3);
    }

    static {
        int version = android.os.Build.VERSION.SDK_INT;
        if (version >= 23) {
            IMPL = new android.support.v4.widget.PopupWindowCompat.Api23PopupWindowImpl();
        } else if (version >= 21) {
            IMPL = new android.support.v4.widget.PopupWindowCompat.Api21PopupWindowImpl();
        } else if (version >= 19) {
            IMPL = new android.support.v4.widget.PopupWindowCompat.KitKatPopupWindowImpl();
        } else {
            IMPL = new android.support.v4.widget.PopupWindowCompat.BasePopupWindowImpl();
        }
    }

    private PopupWindowCompat() {
    }

    public static void showAsDropDown(android.widget.PopupWindow popup, android.view.View anchor, int xoff, int yoff, int gravity) {
        IMPL.showAsDropDown(popup, anchor, xoff, yoff, gravity);
    }

    public static void setOverlapAnchor(android.widget.PopupWindow popupWindow, boolean overlapAnchor) {
        IMPL.setOverlapAnchor(popupWindow, overlapAnchor);
    }

    public static boolean getOverlapAnchor(android.widget.PopupWindow popupWindow) {
        return IMPL.getOverlapAnchor(popupWindow);
    }

    public static void setWindowLayoutType(android.widget.PopupWindow popupWindow, int layoutType) {
        IMPL.setWindowLayoutType(popupWindow, layoutType);
    }

    public static int getWindowLayoutType(android.widget.PopupWindow popupWindow) {
        return IMPL.getWindowLayoutType(popupWindow);
    }
}
