package android.support.v4.widget;

public final class EdgeEffectCompat {
    private static final android.support.v4.widget.EdgeEffectCompat.EdgeEffectImpl IMPL;
    private java.lang.Object mEdgeEffect;

    static class BaseEdgeEffectImpl implements android.support.v4.widget.EdgeEffectCompat.EdgeEffectImpl {
        BaseEdgeEffectImpl() {
        }

        public java.lang.Object newEdgeEffect(android.content.Context context) {
            return null;
        }

        public void setSize(java.lang.Object edgeEffect, int width, int height) {
        }

        public boolean isFinished(java.lang.Object edgeEffect) {
            return true;
        }

        public void finish(java.lang.Object edgeEffect) {
        }

        public boolean onPull(java.lang.Object edgeEffect, float deltaDistance) {
            return false;
        }

        public boolean onRelease(java.lang.Object edgeEffect) {
            return false;
        }

        public boolean onAbsorb(java.lang.Object edgeEffect, int velocity) {
            return false;
        }

        public boolean draw(java.lang.Object edgeEffect, android.graphics.Canvas canvas) {
            return false;
        }

        public boolean onPull(java.lang.Object edgeEffect, float deltaDistance, float displacement) {
            return false;
        }
    }

    static class EdgeEffectIcsImpl implements android.support.v4.widget.EdgeEffectCompat.EdgeEffectImpl {
        EdgeEffectIcsImpl() {
        }

        public java.lang.Object newEdgeEffect(android.content.Context context) {
            return android.support.v4.widget.EdgeEffectCompatIcs.newEdgeEffect(context);
        }

        public void setSize(java.lang.Object edgeEffect, int width, int height) {
            android.support.v4.widget.EdgeEffectCompatIcs.setSize(edgeEffect, width, height);
        }

        public boolean isFinished(java.lang.Object edgeEffect) {
            return android.support.v4.widget.EdgeEffectCompatIcs.isFinished(edgeEffect);
        }

        public void finish(java.lang.Object edgeEffect) {
            android.support.v4.widget.EdgeEffectCompatIcs.finish(edgeEffect);
        }

        public boolean onPull(java.lang.Object edgeEffect, float deltaDistance) {
            return android.support.v4.widget.EdgeEffectCompatIcs.onPull(edgeEffect, deltaDistance);
        }

        public boolean onRelease(java.lang.Object edgeEffect) {
            return android.support.v4.widget.EdgeEffectCompatIcs.onRelease(edgeEffect);
        }

        public boolean onAbsorb(java.lang.Object edgeEffect, int velocity) {
            return android.support.v4.widget.EdgeEffectCompatIcs.onAbsorb(edgeEffect, velocity);
        }

        public boolean draw(java.lang.Object edgeEffect, android.graphics.Canvas canvas) {
            return android.support.v4.widget.EdgeEffectCompatIcs.draw(edgeEffect, canvas);
        }

        public boolean onPull(java.lang.Object edgeEffect, float deltaDistance, float displacement) {
            return android.support.v4.widget.EdgeEffectCompatIcs.onPull(edgeEffect, deltaDistance);
        }
    }

    interface EdgeEffectImpl {
        boolean draw(java.lang.Object obj, android.graphics.Canvas canvas);

        void finish(java.lang.Object obj);

        boolean isFinished(java.lang.Object obj);

        java.lang.Object newEdgeEffect(android.content.Context context);

        boolean onAbsorb(java.lang.Object obj, int i);

        boolean onPull(java.lang.Object obj, float f);

        boolean onPull(java.lang.Object obj, float f, float f2);

        boolean onRelease(java.lang.Object obj);

        void setSize(java.lang.Object obj, int i, int i2);
    }

    static class EdgeEffectLollipopImpl extends android.support.v4.widget.EdgeEffectCompat.EdgeEffectIcsImpl {
        EdgeEffectLollipopImpl() {
        }

        public boolean onPull(java.lang.Object edgeEffect, float deltaDistance, float displacement) {
            return android.support.v4.widget.EdgeEffectCompatLollipop.onPull(edgeEffect, deltaDistance, displacement);
        }
    }

    static {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            IMPL = new android.support.v4.widget.EdgeEffectCompat.EdgeEffectLollipopImpl();
        } else if (android.os.Build.VERSION.SDK_INT >= 14) {
            IMPL = new android.support.v4.widget.EdgeEffectCompat.EdgeEffectIcsImpl();
        } else {
            IMPL = new android.support.v4.widget.EdgeEffectCompat.BaseEdgeEffectImpl();
        }
    }

    public EdgeEffectCompat(android.content.Context context) {
        this.mEdgeEffect = IMPL.newEdgeEffect(context);
    }

    public void setSize(int width, int height) {
        IMPL.setSize(this.mEdgeEffect, width, height);
    }

    public boolean isFinished() {
        return IMPL.isFinished(this.mEdgeEffect);
    }

    public void finish() {
        IMPL.finish(this.mEdgeEffect);
    }

    @java.lang.Deprecated
    public boolean onPull(float deltaDistance) {
        return IMPL.onPull(this.mEdgeEffect, deltaDistance);
    }

    public boolean onPull(float deltaDistance, float displacement) {
        return IMPL.onPull(this.mEdgeEffect, deltaDistance, displacement);
    }

    public boolean onRelease() {
        return IMPL.onRelease(this.mEdgeEffect);
    }

    public boolean onAbsorb(int velocity) {
        return IMPL.onAbsorb(this.mEdgeEffect, velocity);
    }

    public boolean draw(android.graphics.Canvas canvas) {
        return IMPL.draw(this.mEdgeEffect, canvas);
    }
}
