package android.support.v4.widget;

public final class PopupMenuCompat {
    static final android.support.v4.widget.PopupMenuCompat.PopupMenuImpl IMPL;

    static class BasePopupMenuImpl implements android.support.v4.widget.PopupMenuCompat.PopupMenuImpl {
        BasePopupMenuImpl() {
        }

        public android.view.View.OnTouchListener getDragToOpenListener(java.lang.Object popupMenu) {
            return null;
        }
    }

    static class KitKatPopupMenuImpl extends android.support.v4.widget.PopupMenuCompat.BasePopupMenuImpl {
        KitKatPopupMenuImpl() {
        }

        public android.view.View.OnTouchListener getDragToOpenListener(java.lang.Object popupMenu) {
            return android.support.v4.widget.PopupMenuCompatKitKat.getDragToOpenListener(popupMenu);
        }
    }

    interface PopupMenuImpl {
        android.view.View.OnTouchListener getDragToOpenListener(java.lang.Object obj);
    }

    static {
        if (android.os.Build.VERSION.SDK_INT >= 19) {
            IMPL = new android.support.v4.widget.PopupMenuCompat.KitKatPopupMenuImpl();
        } else {
            IMPL = new android.support.v4.widget.PopupMenuCompat.BasePopupMenuImpl();
        }
    }

    private PopupMenuCompat() {
    }

    public static android.view.View.OnTouchListener getDragToOpenListener(java.lang.Object popupMenu) {
        return IMPL.getDragToOpenListener(popupMenu);
    }
}
