package android.support.v4.widget;

@android.annotation.TargetApi(17)
@android.support.annotation.RequiresApi(17)
class TextViewCompatJbMr1 {
    TextViewCompatJbMr1() {
    }

    public static void setCompoundDrawablesRelative(@android.support.annotation.NonNull android.widget.TextView textView, @android.support.annotation.Nullable android.graphics.drawable.Drawable start, @android.support.annotation.Nullable android.graphics.drawable.Drawable top, @android.support.annotation.Nullable android.graphics.drawable.Drawable end, @android.support.annotation.Nullable android.graphics.drawable.Drawable bottom) {
        android.graphics.drawable.Drawable drawable;
        boolean rtl = true;
        if (textView.getLayoutDirection() != 1) {
            rtl = false;
        }
        if (rtl) {
            drawable = end;
        } else {
            drawable = start;
        }
        if (!rtl) {
            start = end;
        }
        textView.setCompoundDrawables(drawable, top, start, bottom);
    }

    public static void setCompoundDrawablesRelativeWithIntrinsicBounds(@android.support.annotation.NonNull android.widget.TextView textView, @android.support.annotation.Nullable android.graphics.drawable.Drawable start, @android.support.annotation.Nullable android.graphics.drawable.Drawable top, @android.support.annotation.Nullable android.graphics.drawable.Drawable end, @android.support.annotation.Nullable android.graphics.drawable.Drawable bottom) {
        android.graphics.drawable.Drawable drawable;
        boolean rtl = true;
        if (textView.getLayoutDirection() != 1) {
            rtl = false;
        }
        if (rtl) {
            drawable = end;
        } else {
            drawable = start;
        }
        if (!rtl) {
            start = end;
        }
        textView.setCompoundDrawablesWithIntrinsicBounds(drawable, top, start, bottom);
    }

    public static void setCompoundDrawablesRelativeWithIntrinsicBounds(@android.support.annotation.NonNull android.widget.TextView textView, int start, int top, int end, int bottom) {
        int i;
        boolean rtl = true;
        if (textView.getLayoutDirection() != 1) {
            rtl = false;
        }
        if (rtl) {
            i = end;
        } else {
            i = start;
        }
        if (!rtl) {
            start = end;
        }
        textView.setCompoundDrawablesWithIntrinsicBounds(i, top, start, bottom);
    }

    public static android.graphics.drawable.Drawable[] getCompoundDrawablesRelative(@android.support.annotation.NonNull android.widget.TextView textView) {
        boolean rtl = true;
        if (textView.getLayoutDirection() != 1) {
            rtl = false;
        }
        android.graphics.drawable.Drawable[] compounds = textView.getCompoundDrawables();
        if (rtl) {
            android.graphics.drawable.Drawable start = compounds[2];
            android.graphics.drawable.Drawable end = compounds[0];
            compounds[0] = start;
            compounds[2] = end;
        }
        return compounds;
    }
}
