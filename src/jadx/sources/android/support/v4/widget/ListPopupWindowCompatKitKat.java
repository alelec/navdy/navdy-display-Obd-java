package android.support.v4.widget;

@android.annotation.TargetApi(19)
@android.support.annotation.RequiresApi(19)
class ListPopupWindowCompatKitKat {
    ListPopupWindowCompatKitKat() {
    }

    public static android.view.View.OnTouchListener createDragToOpenListener(java.lang.Object listPopupWindow, android.view.View src) {
        return ((android.widget.ListPopupWindow) listPopupWindow).createDragToOpenListener(src);
    }
}
