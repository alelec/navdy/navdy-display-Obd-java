package android.support.v4.widget;

public class SlidingPaneLayout extends android.view.ViewGroup {
    private static final int DEFAULT_FADE_COLOR = -858993460;
    private static final int DEFAULT_OVERHANG_SIZE = 32;
    static final android.support.v4.widget.SlidingPaneLayout.SlidingPanelLayoutImpl IMPL;
    private static final int MIN_FLING_VELOCITY = 400;
    private static final java.lang.String TAG = "SlidingPaneLayout";
    private boolean mCanSlide;
    private int mCoveredFadeColor;
    final android.support.v4.widget.ViewDragHelper mDragHelper;
    private boolean mFirstLayout;
    private float mInitialMotionX;
    private float mInitialMotionY;
    boolean mIsUnableToDrag;
    private final int mOverhangSize;
    private android.support.v4.widget.SlidingPaneLayout.PanelSlideListener mPanelSlideListener;
    private int mParallaxBy;
    private float mParallaxOffset;
    final java.util.ArrayList<android.support.v4.widget.SlidingPaneLayout.DisableLayerRunnable> mPostedRunnables;
    boolean mPreservedOpenState;
    private android.graphics.drawable.Drawable mShadowDrawableLeft;
    private android.graphics.drawable.Drawable mShadowDrawableRight;
    float mSlideOffset;
    int mSlideRange;
    android.view.View mSlideableView;
    private int mSliderFadeColor;
    private final android.graphics.Rect mTmpRect;

    class AccessibilityDelegate extends android.support.v4.view.AccessibilityDelegateCompat {
        private final android.graphics.Rect mTmpRect = new android.graphics.Rect();

        AccessibilityDelegate() {
        }

        public void onInitializeAccessibilityNodeInfo(android.view.View host, android.support.v4.view.accessibility.AccessibilityNodeInfoCompat info) {
            android.support.v4.view.accessibility.AccessibilityNodeInfoCompat superNode = android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.obtain(info);
            super.onInitializeAccessibilityNodeInfo(host, superNode);
            copyNodeInfoNoChildren(info, superNode);
            superNode.recycle();
            info.setClassName(android.support.v4.widget.SlidingPaneLayout.class.getName());
            info.setSource(host);
            android.view.ViewParent parent = android.support.v4.view.ViewCompat.getParentForAccessibility(host);
            if (parent instanceof android.view.View) {
                info.setParent((android.view.View) parent);
            }
            int childCount = android.support.v4.widget.SlidingPaneLayout.this.getChildCount();
            for (int i = 0; i < childCount; i++) {
                android.view.View child = android.support.v4.widget.SlidingPaneLayout.this.getChildAt(i);
                if (!filter(child) && child.getVisibility() == 0) {
                    android.support.v4.view.ViewCompat.setImportantForAccessibility(child, 1);
                    info.addChild(child);
                }
            }
        }

        public void onInitializeAccessibilityEvent(android.view.View host, android.view.accessibility.AccessibilityEvent event) {
            super.onInitializeAccessibilityEvent(host, event);
            event.setClassName(android.support.v4.widget.SlidingPaneLayout.class.getName());
        }

        public boolean onRequestSendAccessibilityEvent(android.view.ViewGroup host, android.view.View child, android.view.accessibility.AccessibilityEvent event) {
            if (!filter(child)) {
                return super.onRequestSendAccessibilityEvent(host, child, event);
            }
            return false;
        }

        public boolean filter(android.view.View child) {
            return android.support.v4.widget.SlidingPaneLayout.this.isDimmed(child);
        }

        private void copyNodeInfoNoChildren(android.support.v4.view.accessibility.AccessibilityNodeInfoCompat dest, android.support.v4.view.accessibility.AccessibilityNodeInfoCompat src) {
            android.graphics.Rect rect = this.mTmpRect;
            src.getBoundsInParent(rect);
            dest.setBoundsInParent(rect);
            src.getBoundsInScreen(rect);
            dest.setBoundsInScreen(rect);
            dest.setVisibleToUser(src.isVisibleToUser());
            dest.setPackageName(src.getPackageName());
            dest.setClassName(src.getClassName());
            dest.setContentDescription(src.getContentDescription());
            dest.setEnabled(src.isEnabled());
            dest.setClickable(src.isClickable());
            dest.setFocusable(src.isFocusable());
            dest.setFocused(src.isFocused());
            dest.setAccessibilityFocused(src.isAccessibilityFocused());
            dest.setSelected(src.isSelected());
            dest.setLongClickable(src.isLongClickable());
            dest.addAction(src.getActions());
            dest.setMovementGranularities(src.getMovementGranularities());
        }
    }

    private class DisableLayerRunnable implements java.lang.Runnable {
        final android.view.View mChildView;

        DisableLayerRunnable(android.view.View childView) {
            this.mChildView = childView;
        }

        public void run() {
            if (this.mChildView.getParent() == android.support.v4.widget.SlidingPaneLayout.this) {
                android.support.v4.view.ViewCompat.setLayerType(this.mChildView, 0, null);
                android.support.v4.widget.SlidingPaneLayout.this.invalidateChildRegion(this.mChildView);
            }
            android.support.v4.widget.SlidingPaneLayout.this.mPostedRunnables.remove(this);
        }
    }

    private class DragHelperCallback extends android.support.v4.widget.ViewDragHelper.Callback {
        DragHelperCallback() {
        }

        public boolean tryCaptureView(android.view.View child, int pointerId) {
            if (android.support.v4.widget.SlidingPaneLayout.this.mIsUnableToDrag) {
                return false;
            }
            return ((android.support.v4.widget.SlidingPaneLayout.LayoutParams) child.getLayoutParams()).slideable;
        }

        public void onViewDragStateChanged(int state) {
            if (android.support.v4.widget.SlidingPaneLayout.this.mDragHelper.getViewDragState() != 0) {
                return;
            }
            if (android.support.v4.widget.SlidingPaneLayout.this.mSlideOffset == 0.0f) {
                android.support.v4.widget.SlidingPaneLayout.this.updateObscuredViewsVisibility(android.support.v4.widget.SlidingPaneLayout.this.mSlideableView);
                android.support.v4.widget.SlidingPaneLayout.this.dispatchOnPanelClosed(android.support.v4.widget.SlidingPaneLayout.this.mSlideableView);
                android.support.v4.widget.SlidingPaneLayout.this.mPreservedOpenState = false;
                return;
            }
            android.support.v4.widget.SlidingPaneLayout.this.dispatchOnPanelOpened(android.support.v4.widget.SlidingPaneLayout.this.mSlideableView);
            android.support.v4.widget.SlidingPaneLayout.this.mPreservedOpenState = true;
        }

        public void onViewCaptured(android.view.View capturedChild, int activePointerId) {
            android.support.v4.widget.SlidingPaneLayout.this.setAllChildrenVisible();
        }

        public void onViewPositionChanged(android.view.View changedView, int left, int top, int dx, int dy) {
            android.support.v4.widget.SlidingPaneLayout.this.onPanelDragged(left);
            android.support.v4.widget.SlidingPaneLayout.this.invalidate();
        }

        public void onViewReleased(android.view.View releasedChild, float xvel, float yvel) {
            int left;
            android.support.v4.widget.SlidingPaneLayout.LayoutParams lp = (android.support.v4.widget.SlidingPaneLayout.LayoutParams) releasedChild.getLayoutParams();
            if (android.support.v4.widget.SlidingPaneLayout.this.isLayoutRtlSupport()) {
                int startToRight = android.support.v4.widget.SlidingPaneLayout.this.getPaddingRight() + lp.rightMargin;
                if (xvel < 0.0f || (xvel == 0.0f && android.support.v4.widget.SlidingPaneLayout.this.mSlideOffset > 0.5f)) {
                    startToRight += android.support.v4.widget.SlidingPaneLayout.this.mSlideRange;
                }
                left = (android.support.v4.widget.SlidingPaneLayout.this.getWidth() - startToRight) - android.support.v4.widget.SlidingPaneLayout.this.mSlideableView.getWidth();
            } else {
                left = android.support.v4.widget.SlidingPaneLayout.this.getPaddingLeft() + lp.leftMargin;
                if (xvel > 0.0f || (xvel == 0.0f && android.support.v4.widget.SlidingPaneLayout.this.mSlideOffset > 0.5f)) {
                    left += android.support.v4.widget.SlidingPaneLayout.this.mSlideRange;
                }
            }
            android.support.v4.widget.SlidingPaneLayout.this.mDragHelper.settleCapturedViewAt(left, releasedChild.getTop());
            android.support.v4.widget.SlidingPaneLayout.this.invalidate();
        }

        public int getViewHorizontalDragRange(android.view.View child) {
            return android.support.v4.widget.SlidingPaneLayout.this.mSlideRange;
        }

        public int clampViewPositionHorizontal(android.view.View child, int left, int dx) {
            android.support.v4.widget.SlidingPaneLayout.LayoutParams lp = (android.support.v4.widget.SlidingPaneLayout.LayoutParams) android.support.v4.widget.SlidingPaneLayout.this.mSlideableView.getLayoutParams();
            if (android.support.v4.widget.SlidingPaneLayout.this.isLayoutRtlSupport()) {
                int startBound = android.support.v4.widget.SlidingPaneLayout.this.getWidth() - ((android.support.v4.widget.SlidingPaneLayout.this.getPaddingRight() + lp.rightMargin) + android.support.v4.widget.SlidingPaneLayout.this.mSlideableView.getWidth());
                return java.lang.Math.max(java.lang.Math.min(left, startBound), startBound - android.support.v4.widget.SlidingPaneLayout.this.mSlideRange);
            }
            int startBound2 = android.support.v4.widget.SlidingPaneLayout.this.getPaddingLeft() + lp.leftMargin;
            return java.lang.Math.min(java.lang.Math.max(left, startBound2), startBound2 + android.support.v4.widget.SlidingPaneLayout.this.mSlideRange);
        }

        public int clampViewPositionVertical(android.view.View child, int top, int dy) {
            return child.getTop();
        }

        public void onEdgeDragStarted(int edgeFlags, int pointerId) {
            android.support.v4.widget.SlidingPaneLayout.this.mDragHelper.captureChildView(android.support.v4.widget.SlidingPaneLayout.this.mSlideableView, pointerId);
        }
    }

    public static class LayoutParams extends android.view.ViewGroup.MarginLayoutParams {
        private static final int[] ATTRS = {16843137};
        android.graphics.Paint dimPaint;
        boolean dimWhenOffset;
        boolean slideable;
        public float weight = 0.0f;

        public LayoutParams() {
            super(-1, -1);
        }

        public LayoutParams(int width, int height) {
            super(width, height);
        }

        public LayoutParams(android.view.ViewGroup.LayoutParams source) {
            super(source);
        }

        public LayoutParams(android.view.ViewGroup.MarginLayoutParams source) {
            super(source);
        }

        public LayoutParams(android.support.v4.widget.SlidingPaneLayout.LayoutParams source) {
            super(source);
            this.weight = source.weight;
        }

        public LayoutParams(android.content.Context c, android.util.AttributeSet attrs) {
            super(c, attrs);
            android.content.res.TypedArray a = c.obtainStyledAttributes(attrs, ATTRS);
            this.weight = a.getFloat(0, 0.0f);
            a.recycle();
        }
    }

    public interface PanelSlideListener {
        void onPanelClosed(android.view.View view);

        void onPanelOpened(android.view.View view);

        void onPanelSlide(android.view.View view, float f);
    }

    static class SavedState extends android.support.v4.view.AbsSavedState {
        public static final android.os.Parcelable.Creator<android.support.v4.widget.SlidingPaneLayout.SavedState> CREATOR = android.support.v4.os.ParcelableCompat.newCreator(new android.support.v4.os.ParcelableCompatCreatorCallbacks<android.support.v4.widget.SlidingPaneLayout.SavedState>() {
            public android.support.v4.widget.SlidingPaneLayout.SavedState createFromParcel(android.os.Parcel in, java.lang.ClassLoader loader) {
                return new android.support.v4.widget.SlidingPaneLayout.SavedState(in, loader);
            }

            public android.support.v4.widget.SlidingPaneLayout.SavedState[] newArray(int size) {
                return new android.support.v4.widget.SlidingPaneLayout.SavedState[size];
            }
        });
        boolean isOpen;

        SavedState(android.os.Parcelable superState) {
            super(superState);
        }

        SavedState(android.os.Parcel in, java.lang.ClassLoader loader) {
            super(in, loader);
            this.isOpen = in.readInt() != 0;
        }

        public void writeToParcel(android.os.Parcel out, int flags) {
            super.writeToParcel(out, flags);
            out.writeInt(this.isOpen ? 1 : 0);
        }
    }

    public static class SimplePanelSlideListener implements android.support.v4.widget.SlidingPaneLayout.PanelSlideListener {
        public void onPanelSlide(android.view.View panel, float slideOffset) {
        }

        public void onPanelOpened(android.view.View panel) {
        }

        public void onPanelClosed(android.view.View panel) {
        }
    }

    interface SlidingPanelLayoutImpl {
        void invalidateChildRegion(android.support.v4.widget.SlidingPaneLayout slidingPaneLayout, android.view.View view);
    }

    static class SlidingPanelLayoutImplBase implements android.support.v4.widget.SlidingPaneLayout.SlidingPanelLayoutImpl {
        SlidingPanelLayoutImplBase() {
        }

        public void invalidateChildRegion(android.support.v4.widget.SlidingPaneLayout parent, android.view.View child) {
            android.support.v4.view.ViewCompat.postInvalidateOnAnimation(parent, child.getLeft(), child.getTop(), child.getRight(), child.getBottom());
        }
    }

    static class SlidingPanelLayoutImplJB extends android.support.v4.widget.SlidingPaneLayout.SlidingPanelLayoutImplBase {
        private java.lang.reflect.Method mGetDisplayList;
        private java.lang.reflect.Field mRecreateDisplayList;

        SlidingPanelLayoutImplJB() {
            try {
                this.mGetDisplayList = android.view.View.class.getDeclaredMethod("getDisplayList", null);
            } catch (java.lang.NoSuchMethodException e) {
                android.util.Log.e(android.support.v4.widget.SlidingPaneLayout.TAG, "Couldn't fetch getDisplayList method; dimming won't work right.", e);
            }
            try {
                this.mRecreateDisplayList = android.view.View.class.getDeclaredField("mRecreateDisplayList");
                this.mRecreateDisplayList.setAccessible(true);
            } catch (java.lang.NoSuchFieldException e2) {
                android.util.Log.e(android.support.v4.widget.SlidingPaneLayout.TAG, "Couldn't fetch mRecreateDisplayList field; dimming will be slow.", e2);
            }
        }

        public void invalidateChildRegion(android.support.v4.widget.SlidingPaneLayout parent, android.view.View child) {
            if (this.mGetDisplayList == null || this.mRecreateDisplayList == null) {
                child.invalidate();
                return;
            }
            try {
                this.mRecreateDisplayList.setBoolean(child, true);
                this.mGetDisplayList.invoke(child, null);
            } catch (java.lang.Exception e) {
                android.util.Log.e(android.support.v4.widget.SlidingPaneLayout.TAG, "Error refreshing display list state", e);
            }
            super.invalidateChildRegion(parent, child);
        }
    }

    static class SlidingPanelLayoutImplJBMR1 extends android.support.v4.widget.SlidingPaneLayout.SlidingPanelLayoutImplBase {
        SlidingPanelLayoutImplJBMR1() {
        }

        public void invalidateChildRegion(android.support.v4.widget.SlidingPaneLayout parent, android.view.View child) {
            android.support.v4.view.ViewCompat.setLayerPaint(child, ((android.support.v4.widget.SlidingPaneLayout.LayoutParams) child.getLayoutParams()).dimPaint);
        }
    }

    static {
        int deviceVersion = android.os.Build.VERSION.SDK_INT;
        if (deviceVersion >= 17) {
            IMPL = new android.support.v4.widget.SlidingPaneLayout.SlidingPanelLayoutImplJBMR1();
        } else if (deviceVersion >= 16) {
            IMPL = new android.support.v4.widget.SlidingPaneLayout.SlidingPanelLayoutImplJB();
        } else {
            IMPL = new android.support.v4.widget.SlidingPaneLayout.SlidingPanelLayoutImplBase();
        }
    }

    public SlidingPaneLayout(android.content.Context context) {
        this(context, null);
    }

    public SlidingPaneLayout(android.content.Context context, android.util.AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SlidingPaneLayout(android.content.Context context, android.util.AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.mSliderFadeColor = DEFAULT_FADE_COLOR;
        this.mFirstLayout = true;
        this.mTmpRect = new android.graphics.Rect();
        this.mPostedRunnables = new java.util.ArrayList<>();
        float density = context.getResources().getDisplayMetrics().density;
        this.mOverhangSize = (int) ((32.0f * density) + 0.5f);
        android.view.ViewConfiguration viewConfiguration = android.view.ViewConfiguration.get(context);
        setWillNotDraw(false);
        android.support.v4.view.ViewCompat.setAccessibilityDelegate(this, new android.support.v4.widget.SlidingPaneLayout.AccessibilityDelegate());
        android.support.v4.view.ViewCompat.setImportantForAccessibility(this, 1);
        this.mDragHelper = android.support.v4.widget.ViewDragHelper.create(this, 0.5f, new android.support.v4.widget.SlidingPaneLayout.DragHelperCallback());
        this.mDragHelper.setMinVelocity(400.0f * density);
    }

    public void setParallaxDistance(int parallaxBy) {
        this.mParallaxBy = parallaxBy;
        requestLayout();
    }

    public int getParallaxDistance() {
        return this.mParallaxBy;
    }

    public void setSliderFadeColor(@android.support.annotation.ColorInt int color) {
        this.mSliderFadeColor = color;
    }

    @android.support.annotation.ColorInt
    public int getSliderFadeColor() {
        return this.mSliderFadeColor;
    }

    public void setCoveredFadeColor(@android.support.annotation.ColorInt int color) {
        this.mCoveredFadeColor = color;
    }

    @android.support.annotation.ColorInt
    public int getCoveredFadeColor() {
        return this.mCoveredFadeColor;
    }

    public void setPanelSlideListener(android.support.v4.widget.SlidingPaneLayout.PanelSlideListener listener) {
        this.mPanelSlideListener = listener;
    }

    /* access modifiers changed from: 0000 */
    public void dispatchOnPanelSlide(android.view.View panel) {
        if (this.mPanelSlideListener != null) {
            this.mPanelSlideListener.onPanelSlide(panel, this.mSlideOffset);
        }
    }

    /* access modifiers changed from: 0000 */
    public void dispatchOnPanelOpened(android.view.View panel) {
        if (this.mPanelSlideListener != null) {
            this.mPanelSlideListener.onPanelOpened(panel);
        }
        sendAccessibilityEvent(32);
    }

    /* access modifiers changed from: 0000 */
    public void dispatchOnPanelClosed(android.view.View panel) {
        if (this.mPanelSlideListener != null) {
            this.mPanelSlideListener.onPanelClosed(panel);
        }
        sendAccessibilityEvent(32);
    }

    /* access modifiers changed from: 0000 */
    public void updateObscuredViewsVisibility(android.view.View panel) {
        int endBound;
        int bottom;
        int top;
        int right;
        int left;
        int i;
        int i2;
        int vis;
        boolean isLayoutRtl = isLayoutRtlSupport();
        int startBound = isLayoutRtl ? getWidth() - getPaddingRight() : getPaddingLeft();
        if (isLayoutRtl) {
            endBound = getPaddingLeft();
        } else {
            endBound = getWidth() - getPaddingRight();
        }
        int topBound = getPaddingTop();
        int bottomBound = getHeight() - getPaddingBottom();
        if (panel == null || !viewIsOpaque(panel)) {
            bottom = 0;
            top = 0;
            right = 0;
            left = 0;
        } else {
            left = panel.getLeft();
            right = panel.getRight();
            top = panel.getTop();
            bottom = panel.getBottom();
        }
        int i3 = 0;
        int childCount = getChildCount();
        while (i3 < childCount) {
            android.view.View child = getChildAt(i3);
            if (child != panel) {
                if (child.getVisibility() != 8) {
                    if (isLayoutRtl) {
                        i = endBound;
                    } else {
                        i = startBound;
                    }
                    int clampedChildLeft = java.lang.Math.max(i, child.getLeft());
                    int clampedChildTop = java.lang.Math.max(topBound, child.getTop());
                    if (isLayoutRtl) {
                        i2 = startBound;
                    } else {
                        i2 = endBound;
                    }
                    int clampedChildRight = java.lang.Math.min(i2, child.getRight());
                    int clampedChildBottom = java.lang.Math.min(bottomBound, child.getBottom());
                    if (clampedChildLeft < left || clampedChildTop < top || clampedChildRight > right || clampedChildBottom > bottom) {
                        vis = 0;
                    } else {
                        vis = 4;
                    }
                    child.setVisibility(vis);
                }
                i3++;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void setAllChildrenVisible() {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            android.view.View child = getChildAt(i);
            if (child.getVisibility() == 4) {
                child.setVisibility(0);
            }
        }
    }

    private static boolean viewIsOpaque(android.view.View v) {
        if (v.isOpaque()) {
            return true;
        }
        if (android.os.Build.VERSION.SDK_INT >= 18) {
            return false;
        }
        android.graphics.drawable.Drawable bg = v.getBackground();
        if (bg == null) {
            return false;
        }
        if (bg.getOpacity() != -1) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.mFirstLayout = true;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.mFirstLayout = true;
        int count = this.mPostedRunnables.size();
        for (int i = 0; i < count; i++) {
            ((android.support.v4.widget.SlidingPaneLayout.DisableLayerRunnable) this.mPostedRunnables.get(i)).run();
        }
        this.mPostedRunnables.clear();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int childHeightSpec;
        int childHeightSpec2;
        int childWidthSpec;
        int childHeightSpec3;
        int widthMode = android.view.View.MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = android.view.View.MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = android.view.View.MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = android.view.View.MeasureSpec.getSize(heightMeasureSpec);
        if (widthMode != 1073741824) {
            if (!isInEditMode()) {
                throw new java.lang.IllegalStateException("Width must have an exact value or MATCH_PARENT");
            } else if (widthMode != Integer.MIN_VALUE) {
                if (widthMode == 0) {
                    widthSize = 300;
                }
            }
        } else if (heightMode == 0) {
            if (!isInEditMode()) {
                throw new java.lang.IllegalStateException("Height must not be UNSPECIFIED");
            } else if (heightMode == 0) {
                heightMode = Integer.MIN_VALUE;
                heightSize = 300;
            }
        }
        int layoutHeight = 0;
        int maxLayoutHeight = -1;
        switch (heightMode) {
            case Integer.MIN_VALUE:
                maxLayoutHeight = (heightSize - getPaddingTop()) - getPaddingBottom();
                break;
            case 1073741824:
                maxLayoutHeight = (heightSize - getPaddingTop()) - getPaddingBottom();
                layoutHeight = maxLayoutHeight;
                break;
        }
        float weightSum = 0.0f;
        boolean canSlide = false;
        int widthAvailable = (widthSize - getPaddingLeft()) - getPaddingRight();
        int widthRemaining = widthAvailable;
        int childCount = getChildCount();
        if (childCount > 2) {
            android.util.Log.e(TAG, "onMeasure: More than two child views are not supported.");
        }
        this.mSlideableView = null;
        for (int i = 0; i < childCount; i++) {
            android.view.View child = getChildAt(i);
            android.support.v4.widget.SlidingPaneLayout.LayoutParams lp = (android.support.v4.widget.SlidingPaneLayout.LayoutParams) child.getLayoutParams();
            if (child.getVisibility() == 8) {
                lp.dimWhenOffset = false;
            } else {
                if (lp.weight > 0.0f) {
                    weightSum += lp.weight;
                    if (lp.width == 0) {
                    }
                }
                int horizontalMargin = lp.leftMargin + lp.rightMargin;
                if (lp.width == -2) {
                    childWidthSpec = android.view.View.MeasureSpec.makeMeasureSpec(widthAvailable - horizontalMargin, Integer.MIN_VALUE);
                } else if (lp.width == -1) {
                    childWidthSpec = android.view.View.MeasureSpec.makeMeasureSpec(widthAvailable - horizontalMargin, 1073741824);
                } else {
                    childWidthSpec = android.view.View.MeasureSpec.makeMeasureSpec(lp.width, 1073741824);
                }
                if (lp.height == -2) {
                    childHeightSpec3 = android.view.View.MeasureSpec.makeMeasureSpec(maxLayoutHeight, Integer.MIN_VALUE);
                } else if (lp.height == -1) {
                    childHeightSpec3 = android.view.View.MeasureSpec.makeMeasureSpec(maxLayoutHeight, 1073741824);
                } else {
                    childHeightSpec3 = android.view.View.MeasureSpec.makeMeasureSpec(lp.height, 1073741824);
                }
                child.measure(childWidthSpec, childHeightSpec3);
                int childWidth = child.getMeasuredWidth();
                int childHeight = child.getMeasuredHeight();
                if (heightMode == Integer.MIN_VALUE && childHeight > layoutHeight) {
                    layoutHeight = java.lang.Math.min(childHeight, maxLayoutHeight);
                }
                widthRemaining -= childWidth;
                boolean z = widthRemaining < 0;
                lp.slideable = z;
                canSlide |= z;
                if (lp.slideable) {
                    this.mSlideableView = child;
                }
            }
        }
        if (canSlide || weightSum > 0.0f) {
            int fixedPanelWidthLimit = widthAvailable - this.mOverhangSize;
            for (int i2 = 0; i2 < childCount; i2++) {
                android.view.View child2 = getChildAt(i2);
                if (child2.getVisibility() != 8) {
                    android.support.v4.widget.SlidingPaneLayout.LayoutParams lp2 = (android.support.v4.widget.SlidingPaneLayout.LayoutParams) child2.getLayoutParams();
                    if (child2.getVisibility() != 8) {
                        boolean skippedFirstPass = lp2.width == 0 && lp2.weight > 0.0f;
                        int measuredWidth = skippedFirstPass ? 0 : child2.getMeasuredWidth();
                        if (!canSlide || child2 == this.mSlideableView) {
                            if (lp2.weight > 0.0f) {
                                if (lp2.width != 0) {
                                    childHeightSpec = android.view.View.MeasureSpec.makeMeasureSpec(child2.getMeasuredHeight(), 1073741824);
                                } else if (lp2.height == -2) {
                                    childHeightSpec = android.view.View.MeasureSpec.makeMeasureSpec(maxLayoutHeight, Integer.MIN_VALUE);
                                } else if (lp2.height == -1) {
                                    childHeightSpec = android.view.View.MeasureSpec.makeMeasureSpec(maxLayoutHeight, 1073741824);
                                } else {
                                    childHeightSpec = android.view.View.MeasureSpec.makeMeasureSpec(lp2.height, 1073741824);
                                }
                                if (canSlide) {
                                    int newWidth = widthAvailable - (lp2.leftMargin + lp2.rightMargin);
                                    int childWidthSpec2 = android.view.View.MeasureSpec.makeMeasureSpec(newWidth, 1073741824);
                                    if (measuredWidth != newWidth) {
                                        child2.measure(childWidthSpec2, childHeightSpec);
                                    }
                                } else {
                                    child2.measure(android.view.View.MeasureSpec.makeMeasureSpec(measuredWidth + ((int) ((lp2.weight * ((float) java.lang.Math.max(0, widthRemaining))) / weightSum)), 1073741824), childHeightSpec);
                                }
                            }
                        } else if (lp2.width < 0 && (measuredWidth > fixedPanelWidthLimit || lp2.weight > 0.0f)) {
                            if (!skippedFirstPass) {
                                childHeightSpec2 = android.view.View.MeasureSpec.makeMeasureSpec(child2.getMeasuredHeight(), 1073741824);
                            } else if (lp2.height == -2) {
                                childHeightSpec2 = android.view.View.MeasureSpec.makeMeasureSpec(maxLayoutHeight, Integer.MIN_VALUE);
                            } else if (lp2.height == -1) {
                                childHeightSpec2 = android.view.View.MeasureSpec.makeMeasureSpec(maxLayoutHeight, 1073741824);
                            } else {
                                childHeightSpec2 = android.view.View.MeasureSpec.makeMeasureSpec(lp2.height, 1073741824);
                            }
                            child2.measure(android.view.View.MeasureSpec.makeMeasureSpec(fixedPanelWidthLimit, 1073741824), childHeightSpec2);
                        }
                    }
                }
            }
        }
        setMeasuredDimension(widthSize, getPaddingTop() + layoutHeight + getPaddingBottom());
        this.mCanSlide = canSlide;
        if (this.mDragHelper.getViewDragState() != 0 && !canSlide) {
            this.mDragHelper.abort();
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean changed, int l, int t, int r, int b) {
        int childLeft;
        int childRight;
        boolean isLayoutRtl = isLayoutRtlSupport();
        if (isLayoutRtl) {
            this.mDragHelper.setEdgeTrackingEnabled(2);
        } else {
            this.mDragHelper.setEdgeTrackingEnabled(1);
        }
        int width = r - l;
        int paddingStart = isLayoutRtl ? getPaddingRight() : getPaddingLeft();
        int paddingEnd = isLayoutRtl ? getPaddingLeft() : getPaddingRight();
        int paddingTop = getPaddingTop();
        int childCount = getChildCount();
        int xStart = paddingStart;
        int nextXStart = xStart;
        if (this.mFirstLayout) {
            this.mSlideOffset = (!this.mCanSlide || !this.mPreservedOpenState) ? 0.0f : 1.0f;
        }
        for (int i = 0; i < childCount; i++) {
            android.view.View child = getChildAt(i);
            if (child.getVisibility() != 8) {
                android.support.v4.widget.SlidingPaneLayout.LayoutParams lp = (android.support.v4.widget.SlidingPaneLayout.LayoutParams) child.getLayoutParams();
                int childWidth = child.getMeasuredWidth();
                int offset = 0;
                if (lp.slideable) {
                    int range = (java.lang.Math.min(nextXStart, (width - paddingEnd) - this.mOverhangSize) - xStart) - (lp.leftMargin + lp.rightMargin);
                    this.mSlideRange = range;
                    int lpMargin = isLayoutRtl ? lp.rightMargin : lp.leftMargin;
                    lp.dimWhenOffset = ((xStart + lpMargin) + range) + (childWidth / 2) > width - paddingEnd;
                    int pos = (int) (((float) range) * this.mSlideOffset);
                    xStart += pos + lpMargin;
                    this.mSlideOffset = ((float) pos) / ((float) this.mSlideRange);
                } else if (!this.mCanSlide || this.mParallaxBy == 0) {
                    xStart = nextXStart;
                } else {
                    offset = (int) ((1.0f - this.mSlideOffset) * ((float) this.mParallaxBy));
                    xStart = nextXStart;
                }
                if (isLayoutRtl) {
                    childRight = (width - xStart) + offset;
                    childLeft = childRight - childWidth;
                } else {
                    childLeft = xStart - offset;
                    childRight = childLeft + childWidth;
                }
                child.layout(childLeft, paddingTop, childRight, paddingTop + child.getMeasuredHeight());
                nextXStart += child.getWidth();
            }
        }
        if (this.mFirstLayout) {
            if (this.mCanSlide) {
                if (this.mParallaxBy != 0) {
                    parallaxOtherViews(this.mSlideOffset);
                }
                if (((android.support.v4.widget.SlidingPaneLayout.LayoutParams) this.mSlideableView.getLayoutParams()).dimWhenOffset) {
                    dimChildView(this.mSlideableView, this.mSlideOffset, this.mSliderFadeColor);
                }
            } else {
                for (int i2 = 0; i2 < childCount; i2++) {
                    dimChildView(getChildAt(i2), 0.0f, this.mSliderFadeColor);
                }
            }
            updateObscuredViewsVisibility(this.mSlideableView);
        }
        this.mFirstLayout = false;
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        if (w != oldw) {
            this.mFirstLayout = true;
        }
    }

    public void requestChildFocus(android.view.View child, android.view.View focused) {
        super.requestChildFocus(child, focused);
        if (!isInTouchMode() && !this.mCanSlide) {
            this.mPreservedOpenState = child == this.mSlideableView;
        }
    }

    public boolean onInterceptTouchEvent(android.view.MotionEvent ev) {
        int action = android.support.v4.view.MotionEventCompat.getActionMasked(ev);
        if (!this.mCanSlide && action == 0 && getChildCount() > 1) {
            android.view.View secondChild = getChildAt(1);
            if (secondChild != null) {
                this.mPreservedOpenState = !this.mDragHelper.isViewUnder(secondChild, (int) ev.getX(), (int) ev.getY());
            }
        }
        if (!this.mCanSlide || (this.mIsUnableToDrag && action != 0)) {
            this.mDragHelper.cancel();
            return super.onInterceptTouchEvent(ev);
        } else if (action == 3 || action == 1) {
            this.mDragHelper.cancel();
            return false;
        } else {
            boolean interceptTap = false;
            switch (action) {
                case 0:
                    this.mIsUnableToDrag = false;
                    float x = ev.getX();
                    float y = ev.getY();
                    this.mInitialMotionX = x;
                    this.mInitialMotionY = y;
                    if (this.mDragHelper.isViewUnder(this.mSlideableView, (int) x, (int) y) && isDimmed(this.mSlideableView)) {
                        interceptTap = true;
                        break;
                    }
                case 2:
                    float x2 = ev.getX();
                    float y2 = ev.getY();
                    float adx = java.lang.Math.abs(x2 - this.mInitialMotionX);
                    float ady = java.lang.Math.abs(y2 - this.mInitialMotionY);
                    if (adx > ((float) this.mDragHelper.getTouchSlop()) && ady > adx) {
                        this.mDragHelper.cancel();
                        this.mIsUnableToDrag = true;
                        return false;
                    }
            }
            if (this.mDragHelper.shouldInterceptTouchEvent(ev) || interceptTap) {
                return true;
            }
            return false;
        }
    }

    public boolean onTouchEvent(android.view.MotionEvent ev) {
        if (!this.mCanSlide) {
            return super.onTouchEvent(ev);
        }
        this.mDragHelper.processTouchEvent(ev);
        switch (ev.getAction() & 255) {
            case 0:
                float x = ev.getX();
                float y = ev.getY();
                this.mInitialMotionX = x;
                this.mInitialMotionY = y;
                return true;
            case 1:
                if (!isDimmed(this.mSlideableView)) {
                    return true;
                }
                float x2 = ev.getX();
                float y2 = ev.getY();
                float dx = x2 - this.mInitialMotionX;
                float dy = y2 - this.mInitialMotionY;
                int slop = this.mDragHelper.getTouchSlop();
                if ((dx * dx) + (dy * dy) >= ((float) (slop * slop)) || !this.mDragHelper.isViewUnder(this.mSlideableView, (int) x2, (int) y2)) {
                    return true;
                }
                closePane(this.mSlideableView, 0);
                return true;
            default:
                return true;
        }
    }

    private boolean closePane(android.view.View pane, int initialVelocity) {
        if (!this.mFirstLayout && !smoothSlideTo(0.0f, initialVelocity)) {
            return false;
        }
        this.mPreservedOpenState = false;
        return true;
    }

    private boolean openPane(android.view.View pane, int initialVelocity) {
        if (!this.mFirstLayout && !smoothSlideTo(1.0f, initialVelocity)) {
            return false;
        }
        this.mPreservedOpenState = true;
        return true;
    }

    @java.lang.Deprecated
    public void smoothSlideOpen() {
        openPane();
    }

    public boolean openPane() {
        return openPane(this.mSlideableView, 0);
    }

    @java.lang.Deprecated
    public void smoothSlideClosed() {
        closePane();
    }

    public boolean closePane() {
        return closePane(this.mSlideableView, 0);
    }

    public boolean isOpen() {
        return !this.mCanSlide || this.mSlideOffset == 1.0f;
    }

    @java.lang.Deprecated
    public boolean canSlide() {
        return this.mCanSlide;
    }

    public boolean isSlideable() {
        return this.mCanSlide;
    }

    /* access modifiers changed from: 0000 */
    public void onPanelDragged(int newLeft) {
        int newStart;
        if (this.mSlideableView == null) {
            this.mSlideOffset = 0.0f;
            return;
        }
        boolean isLayoutRtl = isLayoutRtlSupport();
        android.support.v4.widget.SlidingPaneLayout.LayoutParams lp = (android.support.v4.widget.SlidingPaneLayout.LayoutParams) this.mSlideableView.getLayoutParams();
        int childWidth = this.mSlideableView.getWidth();
        if (isLayoutRtl) {
            newStart = (getWidth() - newLeft) - childWidth;
        } else {
            newStart = newLeft;
        }
        this.mSlideOffset = ((float) (newStart - ((isLayoutRtl ? getPaddingRight() : getPaddingLeft()) + (isLayoutRtl ? lp.rightMargin : lp.leftMargin)))) / ((float) this.mSlideRange);
        if (this.mParallaxBy != 0) {
            parallaxOtherViews(this.mSlideOffset);
        }
        if (lp.dimWhenOffset) {
            dimChildView(this.mSlideableView, this.mSlideOffset, this.mSliderFadeColor);
        }
        dispatchOnPanelSlide(this.mSlideableView);
    }

    private void dimChildView(android.view.View v, float mag, int fadeColor) {
        android.support.v4.widget.SlidingPaneLayout.LayoutParams lp = (android.support.v4.widget.SlidingPaneLayout.LayoutParams) v.getLayoutParams();
        if (mag > 0.0f && fadeColor != 0) {
            int color = (((int) (((float) ((-16777216 & fadeColor) >>> 24)) * mag)) << 24) | (16777215 & fadeColor);
            if (lp.dimPaint == null) {
                lp.dimPaint = new android.graphics.Paint();
            }
            lp.dimPaint.setColorFilter(new android.graphics.PorterDuffColorFilter(color, android.graphics.PorterDuff.Mode.SRC_OVER));
            if (android.support.v4.view.ViewCompat.getLayerType(v) != 2) {
                android.support.v4.view.ViewCompat.setLayerType(v, 2, lp.dimPaint);
            }
            invalidateChildRegion(v);
        } else if (android.support.v4.view.ViewCompat.getLayerType(v) != 0) {
            if (lp.dimPaint != null) {
                lp.dimPaint.setColorFilter(null);
            }
            android.support.v4.widget.SlidingPaneLayout.DisableLayerRunnable dlr = new android.support.v4.widget.SlidingPaneLayout.DisableLayerRunnable(v);
            this.mPostedRunnables.add(dlr);
            android.support.v4.view.ViewCompat.postOnAnimation(this, dlr);
        }
    }

    /* access modifiers changed from: protected */
    public boolean drawChild(android.graphics.Canvas canvas, android.view.View child, long drawingTime) {
        boolean result;
        android.support.v4.widget.SlidingPaneLayout.LayoutParams lp = (android.support.v4.widget.SlidingPaneLayout.LayoutParams) child.getLayoutParams();
        int save = canvas.save(2);
        if (this.mCanSlide && !lp.slideable && this.mSlideableView != null) {
            canvas.getClipBounds(this.mTmpRect);
            if (isLayoutRtlSupport()) {
                this.mTmpRect.left = java.lang.Math.max(this.mTmpRect.left, this.mSlideableView.getRight());
            } else {
                this.mTmpRect.right = java.lang.Math.min(this.mTmpRect.right, this.mSlideableView.getLeft());
            }
            canvas.clipRect(this.mTmpRect);
        }
        if (android.os.Build.VERSION.SDK_INT >= 11) {
            result = super.drawChild(canvas, child, drawingTime);
        } else if (!lp.dimWhenOffset || this.mSlideOffset <= 0.0f) {
            if (child.isDrawingCacheEnabled()) {
                child.setDrawingCacheEnabled(false);
            }
            result = super.drawChild(canvas, child, drawingTime);
        } else {
            if (!child.isDrawingCacheEnabled()) {
                child.setDrawingCacheEnabled(true);
            }
            android.graphics.Bitmap cache = child.getDrawingCache();
            if (cache != null) {
                canvas.drawBitmap(cache, (float) child.getLeft(), (float) child.getTop(), lp.dimPaint);
                result = false;
            } else {
                android.util.Log.e(TAG, "drawChild: child view " + child + " returned null drawing cache");
                result = super.drawChild(canvas, child, drawingTime);
            }
        }
        canvas.restoreToCount(save);
        return result;
    }

    /* access modifiers changed from: 0000 */
    public void invalidateChildRegion(android.view.View v) {
        IMPL.invalidateChildRegion(this, v);
    }

    /* access modifiers changed from: 0000 */
    public boolean smoothSlideTo(float slideOffset, int velocity) {
        int x;
        if (!this.mCanSlide) {
            return false;
        }
        android.support.v4.widget.SlidingPaneLayout.LayoutParams lp = (android.support.v4.widget.SlidingPaneLayout.LayoutParams) this.mSlideableView.getLayoutParams();
        if (isLayoutRtlSupport()) {
            x = (int) (((float) getWidth()) - ((((float) (getPaddingRight() + lp.rightMargin)) + (((float) this.mSlideRange) * slideOffset)) + ((float) this.mSlideableView.getWidth())));
        } else {
            x = (int) (((float) (getPaddingLeft() + lp.leftMargin)) + (((float) this.mSlideRange) * slideOffset));
        }
        if (!this.mDragHelper.smoothSlideViewTo(this.mSlideableView, x, this.mSlideableView.getTop())) {
            return false;
        }
        setAllChildrenVisible();
        android.support.v4.view.ViewCompat.postInvalidateOnAnimation(this);
        return true;
    }

    public void computeScroll() {
        if (!this.mDragHelper.continueSettling(true)) {
            return;
        }
        if (!this.mCanSlide) {
            this.mDragHelper.abort();
        } else {
            android.support.v4.view.ViewCompat.postInvalidateOnAnimation(this);
        }
    }

    @java.lang.Deprecated
    public void setShadowDrawable(android.graphics.drawable.Drawable d) {
        setShadowDrawableLeft(d);
    }

    public void setShadowDrawableLeft(android.graphics.drawable.Drawable d) {
        this.mShadowDrawableLeft = d;
    }

    public void setShadowDrawableRight(android.graphics.drawable.Drawable d) {
        this.mShadowDrawableRight = d;
    }

    @java.lang.Deprecated
    public void setShadowResource(@android.support.annotation.DrawableRes int resId) {
        setShadowDrawable(getResources().getDrawable(resId));
    }

    public void setShadowResourceLeft(int resId) {
        setShadowDrawableLeft(android.support.v4.content.ContextCompat.getDrawable(getContext(), resId));
    }

    public void setShadowResourceRight(int resId) {
        setShadowDrawableRight(android.support.v4.content.ContextCompat.getDrawable(getContext(), resId));
    }

    public void draw(android.graphics.Canvas c) {
        android.graphics.drawable.Drawable shadowDrawable;
        int right;
        int left;
        super.draw(c);
        if (isLayoutRtlSupport()) {
            shadowDrawable = this.mShadowDrawableRight;
        } else {
            shadowDrawable = this.mShadowDrawableLeft;
        }
        android.view.View shadowView = getChildCount() > 1 ? getChildAt(1) : null;
        if (shadowView != null && shadowDrawable != null) {
            int top = shadowView.getTop();
            int bottom = shadowView.getBottom();
            int shadowWidth = shadowDrawable.getIntrinsicWidth();
            if (isLayoutRtlSupport()) {
                left = shadowView.getRight();
                right = left + shadowWidth;
            } else {
                right = shadowView.getLeft();
                left = right - shadowWidth;
            }
            shadowDrawable.setBounds(left, top, right, bottom);
            shadowDrawable.draw(c);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:8:0x0020  */
    private void parallaxOtherViews(float slideOffset) {
        boolean dimViews;
        int childCount;
        int i;
        boolean isLayoutRtl = isLayoutRtlSupport();
        android.support.v4.widget.SlidingPaneLayout.LayoutParams slideLp = (android.support.v4.widget.SlidingPaneLayout.LayoutParams) this.mSlideableView.getLayoutParams();
        if (slideLp.dimWhenOffset) {
            if ((isLayoutRtl ? slideLp.rightMargin : slideLp.leftMargin) <= 0) {
                dimViews = true;
                childCount = getChildCount();
                for (i = 0; i < childCount; i++) {
                    android.view.View v = getChildAt(i);
                    if (v != this.mSlideableView) {
                        int oldOffset = (int) ((1.0f - this.mParallaxOffset) * ((float) this.mParallaxBy));
                        this.mParallaxOffset = slideOffset;
                        int dx = oldOffset - ((int) ((1.0f - slideOffset) * ((float) this.mParallaxBy)));
                        if (isLayoutRtl) {
                            dx = -dx;
                        }
                        v.offsetLeftAndRight(dx);
                        if (dimViews) {
                            dimChildView(v, isLayoutRtl ? this.mParallaxOffset - 1.0f : 1.0f - this.mParallaxOffset, this.mCoveredFadeColor);
                        }
                    }
                }
            }
        }
        dimViews = false;
        childCount = getChildCount();
        while (i < childCount) {
        }
    }

    /* access modifiers changed from: protected */
    public boolean canScroll(android.view.View v, boolean checkV, int dx, int x, int y) {
        if (v instanceof android.view.ViewGroup) {
            android.view.ViewGroup group = (android.view.ViewGroup) v;
            int scrollX = v.getScrollX();
            int scrollY = v.getScrollY();
            for (int i = group.getChildCount() - 1; i >= 0; i--) {
                android.view.View child = group.getChildAt(i);
                if (x + scrollX >= child.getLeft() && x + scrollX < child.getRight() && y + scrollY >= child.getTop() && y + scrollY < child.getBottom()) {
                    if (canScroll(child, true, dx, (x + scrollX) - child.getLeft(), (y + scrollY) - child.getTop())) {
                        return true;
                    }
                }
            }
        }
        if (checkV) {
            if (!isLayoutRtlSupport()) {
                dx = -dx;
            }
            if (android.support.v4.view.ViewCompat.canScrollHorizontally(v, dx)) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: 0000 */
    public boolean isDimmed(android.view.View child) {
        if (child == null) {
            return false;
        }
        android.support.v4.widget.SlidingPaneLayout.LayoutParams lp = (android.support.v4.widget.SlidingPaneLayout.LayoutParams) child.getLayoutParams();
        if (!this.mCanSlide || !lp.dimWhenOffset || this.mSlideOffset <= 0.0f) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public android.view.ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new android.support.v4.widget.SlidingPaneLayout.LayoutParams();
    }

    /* access modifiers changed from: protected */
    public android.view.ViewGroup.LayoutParams generateLayoutParams(android.view.ViewGroup.LayoutParams p) {
        return p instanceof android.view.ViewGroup.MarginLayoutParams ? new android.support.v4.widget.SlidingPaneLayout.LayoutParams((android.view.ViewGroup.MarginLayoutParams) p) : new android.support.v4.widget.SlidingPaneLayout.LayoutParams(p);
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(android.view.ViewGroup.LayoutParams p) {
        return (p instanceof android.support.v4.widget.SlidingPaneLayout.LayoutParams) && super.checkLayoutParams(p);
    }

    public android.view.ViewGroup.LayoutParams generateLayoutParams(android.util.AttributeSet attrs) {
        return new android.support.v4.widget.SlidingPaneLayout.LayoutParams(getContext(), attrs);
    }

    /* access modifiers changed from: protected */
    public android.os.Parcelable onSaveInstanceState() {
        android.support.v4.widget.SlidingPaneLayout.SavedState ss = new android.support.v4.widget.SlidingPaneLayout.SavedState(super.onSaveInstanceState());
        ss.isOpen = isSlideable() ? isOpen() : this.mPreservedOpenState;
        return ss;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(android.os.Parcelable state) {
        if (!(state instanceof android.support.v4.widget.SlidingPaneLayout.SavedState)) {
            super.onRestoreInstanceState(state);
            return;
        }
        android.support.v4.widget.SlidingPaneLayout.SavedState ss = (android.support.v4.widget.SlidingPaneLayout.SavedState) state;
        super.onRestoreInstanceState(ss.getSuperState());
        if (ss.isOpen) {
            openPane();
        } else {
            closePane();
        }
        this.mPreservedOpenState = ss.isOpen;
    }

    /* access modifiers changed from: 0000 */
    public boolean isLayoutRtlSupport() {
        return android.support.v4.view.ViewCompat.getLayoutDirection(this) == 1;
    }
}
