package android.support.v4.widget;

@android.annotation.TargetApi(19)
@android.support.annotation.RequiresApi(19)
class PopupWindowCompatKitKat {
    PopupWindowCompatKitKat() {
    }

    public static void showAsDropDown(android.widget.PopupWindow popup, android.view.View anchor, int xoff, int yoff, int gravity) {
        popup.showAsDropDown(anchor, xoff, yoff, gravity);
    }
}
