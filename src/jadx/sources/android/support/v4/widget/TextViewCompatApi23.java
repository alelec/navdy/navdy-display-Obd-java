package android.support.v4.widget;

@android.annotation.TargetApi(23)
@android.support.annotation.RequiresApi(23)
class TextViewCompatApi23 {
    TextViewCompatApi23() {
    }

    public static void setTextAppearance(@android.support.annotation.NonNull android.widget.TextView textView, @android.support.annotation.StyleRes int resId) {
        textView.setTextAppearance(resId);
    }
}
