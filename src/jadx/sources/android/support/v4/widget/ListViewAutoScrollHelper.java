package android.support.v4.widget;

public class ListViewAutoScrollHelper extends android.support.v4.widget.AutoScrollHelper {
    private final android.widget.ListView mTarget;

    public ListViewAutoScrollHelper(android.widget.ListView target) {
        super(target);
        this.mTarget = target;
    }

    public void scrollTargetBy(int deltaX, int deltaY) {
        android.support.v4.widget.ListViewCompat.scrollListBy(this.mTarget, deltaY);
    }

    public boolean canTargetScrollHorizontally(int direction) {
        return false;
    }

    public boolean canTargetScrollVertically(int direction) {
        android.widget.ListView target = this.mTarget;
        int itemCount = target.getCount();
        if (itemCount == 0) {
            return false;
        }
        int childCount = target.getChildCount();
        int firstPosition = target.getFirstVisiblePosition();
        int lastPosition = firstPosition + childCount;
        if (direction > 0) {
            if (lastPosition >= itemCount && target.getChildAt(childCount - 1).getBottom() <= target.getHeight()) {
                return false;
            }
        } else if (direction >= 0) {
            return false;
        } else {
            if (firstPosition <= 0 && target.getChildAt(0).getTop() >= 0) {
                return false;
            }
        }
        return true;
    }
}
