package android.support.v4.widget;

public final class SearchViewCompat {
    private static final android.support.v4.widget.SearchViewCompat.SearchViewCompatImpl IMPL;

    public interface OnCloseListener {
        boolean onClose();
    }

    @java.lang.Deprecated
    public static abstract class OnCloseListenerCompat implements android.support.v4.widget.SearchViewCompat.OnCloseListener {
        public boolean onClose() {
            return false;
        }
    }

    public interface OnQueryTextListener {
        boolean onQueryTextChange(java.lang.String str);

        boolean onQueryTextSubmit(java.lang.String str);
    }

    @java.lang.Deprecated
    public static abstract class OnQueryTextListenerCompat implements android.support.v4.widget.SearchViewCompat.OnQueryTextListener {
        public boolean onQueryTextSubmit(java.lang.String query) {
            return false;
        }

        public boolean onQueryTextChange(java.lang.String newText) {
            return false;
        }
    }

    static class SearchViewCompatHoneycombImpl extends android.support.v4.widget.SearchViewCompat.SearchViewCompatStubImpl {
        SearchViewCompatHoneycombImpl() {
        }

        public android.view.View newSearchView(android.content.Context context) {
            return android.support.v4.widget.SearchViewCompatHoneycomb.newSearchView(context);
        }

        public void setSearchableInfo(android.view.View searchView, android.content.ComponentName searchableComponent) {
            checkIfLegalArg(searchView);
            android.support.v4.widget.SearchViewCompatHoneycomb.setSearchableInfo(searchView, searchableComponent);
        }

        public java.lang.Object newOnQueryTextListener(final android.support.v4.widget.SearchViewCompat.OnQueryTextListener listener) {
            return android.support.v4.widget.SearchViewCompatHoneycomb.newOnQueryTextListener(new android.support.v4.widget.SearchViewCompatHoneycomb.OnQueryTextListenerCompatBridge() {
                public boolean onQueryTextSubmit(java.lang.String query) {
                    return listener.onQueryTextSubmit(query);
                }

                public boolean onQueryTextChange(java.lang.String newText) {
                    return listener.onQueryTextChange(newText);
                }
            });
        }

        public void setOnQueryTextListener(android.view.View searchView, android.support.v4.widget.SearchViewCompat.OnQueryTextListener listener) {
            checkIfLegalArg(searchView);
            android.support.v4.widget.SearchViewCompatHoneycomb.setOnQueryTextListener(searchView, newOnQueryTextListener(listener));
        }

        public java.lang.Object newOnCloseListener(final android.support.v4.widget.SearchViewCompat.OnCloseListener listener) {
            return android.support.v4.widget.SearchViewCompatHoneycomb.newOnCloseListener(new android.support.v4.widget.SearchViewCompatHoneycomb.OnCloseListenerCompatBridge() {
                public boolean onClose() {
                    return listener.onClose();
                }
            });
        }

        public void setOnCloseListener(android.view.View searchView, android.support.v4.widget.SearchViewCompat.OnCloseListener listener) {
            checkIfLegalArg(searchView);
            android.support.v4.widget.SearchViewCompatHoneycomb.setOnCloseListener(searchView, newOnCloseListener(listener));
        }

        public java.lang.CharSequence getQuery(android.view.View searchView) {
            checkIfLegalArg(searchView);
            return android.support.v4.widget.SearchViewCompatHoneycomb.getQuery(searchView);
        }

        public void setQuery(android.view.View searchView, java.lang.CharSequence query, boolean submit) {
            checkIfLegalArg(searchView);
            android.support.v4.widget.SearchViewCompatHoneycomb.setQuery(searchView, query, submit);
        }

        public void setQueryHint(android.view.View searchView, java.lang.CharSequence hint) {
            checkIfLegalArg(searchView);
            android.support.v4.widget.SearchViewCompatHoneycomb.setQueryHint(searchView, hint);
        }

        public void setIconified(android.view.View searchView, boolean iconify) {
            checkIfLegalArg(searchView);
            android.support.v4.widget.SearchViewCompatHoneycomb.setIconified(searchView, iconify);
        }

        public boolean isIconified(android.view.View searchView) {
            checkIfLegalArg(searchView);
            return android.support.v4.widget.SearchViewCompatHoneycomb.isIconified(searchView);
        }

        public void setSubmitButtonEnabled(android.view.View searchView, boolean enabled) {
            checkIfLegalArg(searchView);
            android.support.v4.widget.SearchViewCompatHoneycomb.setSubmitButtonEnabled(searchView, enabled);
        }

        public boolean isSubmitButtonEnabled(android.view.View searchView) {
            checkIfLegalArg(searchView);
            return android.support.v4.widget.SearchViewCompatHoneycomb.isSubmitButtonEnabled(searchView);
        }

        public void setQueryRefinementEnabled(android.view.View searchView, boolean enable) {
            checkIfLegalArg(searchView);
            android.support.v4.widget.SearchViewCompatHoneycomb.setQueryRefinementEnabled(searchView, enable);
        }

        public boolean isQueryRefinementEnabled(android.view.View searchView) {
            checkIfLegalArg(searchView);
            return android.support.v4.widget.SearchViewCompatHoneycomb.isQueryRefinementEnabled(searchView);
        }

        public void setMaxWidth(android.view.View searchView, int maxpixels) {
            checkIfLegalArg(searchView);
            android.support.v4.widget.SearchViewCompatHoneycomb.setMaxWidth(searchView, maxpixels);
        }

        /* access modifiers changed from: protected */
        public void checkIfLegalArg(android.view.View searchView) {
            android.support.v4.widget.SearchViewCompatHoneycomb.checkIfLegalArg(searchView);
        }
    }

    static class SearchViewCompatIcsImpl extends android.support.v4.widget.SearchViewCompat.SearchViewCompatHoneycombImpl {
        SearchViewCompatIcsImpl() {
        }

        public android.view.View newSearchView(android.content.Context context) {
            return android.support.v4.widget.SearchViewCompatIcs.newSearchView(context);
        }

        public void setImeOptions(android.view.View searchView, int imeOptions) {
            checkIfLegalArg(searchView);
            android.support.v4.widget.SearchViewCompatIcs.setImeOptions(searchView, imeOptions);
        }

        public void setInputType(android.view.View searchView, int inputType) {
            checkIfLegalArg(searchView);
            android.support.v4.widget.SearchViewCompatIcs.setInputType(searchView, inputType);
        }
    }

    interface SearchViewCompatImpl {
        java.lang.CharSequence getQuery(android.view.View view);

        boolean isIconified(android.view.View view);

        boolean isQueryRefinementEnabled(android.view.View view);

        boolean isSubmitButtonEnabled(android.view.View view);

        java.lang.Object newOnCloseListener(android.support.v4.widget.SearchViewCompat.OnCloseListener onCloseListener);

        java.lang.Object newOnQueryTextListener(android.support.v4.widget.SearchViewCompat.OnQueryTextListener onQueryTextListener);

        android.view.View newSearchView(android.content.Context context);

        void setIconified(android.view.View view, boolean z);

        void setImeOptions(android.view.View view, int i);

        void setInputType(android.view.View view, int i);

        void setMaxWidth(android.view.View view, int i);

        void setOnCloseListener(android.view.View view, android.support.v4.widget.SearchViewCompat.OnCloseListener onCloseListener);

        void setOnQueryTextListener(android.view.View view, android.support.v4.widget.SearchViewCompat.OnQueryTextListener onQueryTextListener);

        void setQuery(android.view.View view, java.lang.CharSequence charSequence, boolean z);

        void setQueryHint(android.view.View view, java.lang.CharSequence charSequence);

        void setQueryRefinementEnabled(android.view.View view, boolean z);

        void setSearchableInfo(android.view.View view, android.content.ComponentName componentName);

        void setSubmitButtonEnabled(android.view.View view, boolean z);
    }

    static class SearchViewCompatStubImpl implements android.support.v4.widget.SearchViewCompat.SearchViewCompatImpl {
        SearchViewCompatStubImpl() {
        }

        public android.view.View newSearchView(android.content.Context context) {
            return null;
        }

        public void setSearchableInfo(android.view.View searchView, android.content.ComponentName searchableComponent) {
        }

        public void setImeOptions(android.view.View searchView, int imeOptions) {
        }

        public void setInputType(android.view.View searchView, int inputType) {
        }

        public java.lang.Object newOnQueryTextListener(android.support.v4.widget.SearchViewCompat.OnQueryTextListener listener) {
            return null;
        }

        public void setOnQueryTextListener(android.view.View searchView, android.support.v4.widget.SearchViewCompat.OnQueryTextListener listener) {
        }

        public java.lang.Object newOnCloseListener(android.support.v4.widget.SearchViewCompat.OnCloseListener listener) {
            return null;
        }

        public void setOnCloseListener(android.view.View searchView, android.support.v4.widget.SearchViewCompat.OnCloseListener listener) {
        }

        public java.lang.CharSequence getQuery(android.view.View searchView) {
            return null;
        }

        public void setQuery(android.view.View searchView, java.lang.CharSequence query, boolean submit) {
        }

        public void setQueryHint(android.view.View searchView, java.lang.CharSequence hint) {
        }

        public void setIconified(android.view.View searchView, boolean iconify) {
        }

        public boolean isIconified(android.view.View searchView) {
            return true;
        }

        public void setSubmitButtonEnabled(android.view.View searchView, boolean enabled) {
        }

        public boolean isSubmitButtonEnabled(android.view.View searchView) {
            return false;
        }

        public void setQueryRefinementEnabled(android.view.View searchView, boolean enable) {
        }

        public boolean isQueryRefinementEnabled(android.view.View searchView) {
            return false;
        }

        public void setMaxWidth(android.view.View searchView, int maxpixels) {
        }
    }

    static {
        if (android.os.Build.VERSION.SDK_INT >= 14) {
            IMPL = new android.support.v4.widget.SearchViewCompat.SearchViewCompatIcsImpl();
        } else if (android.os.Build.VERSION.SDK_INT >= 11) {
            IMPL = new android.support.v4.widget.SearchViewCompat.SearchViewCompatHoneycombImpl();
        } else {
            IMPL = new android.support.v4.widget.SearchViewCompat.SearchViewCompatStubImpl();
        }
    }

    private SearchViewCompat(android.content.Context context) {
    }

    public static android.view.View newSearchView(android.content.Context context) {
        return IMPL.newSearchView(context);
    }

    public static void setSearchableInfo(android.view.View searchView, android.content.ComponentName searchableComponent) {
        IMPL.setSearchableInfo(searchView, searchableComponent);
    }

    public static void setImeOptions(android.view.View searchView, int imeOptions) {
        IMPL.setImeOptions(searchView, imeOptions);
    }

    public static void setInputType(android.view.View searchView, int inputType) {
        IMPL.setInputType(searchView, inputType);
    }

    public static void setOnQueryTextListener(android.view.View searchView, android.support.v4.widget.SearchViewCompat.OnQueryTextListener listener) {
        IMPL.setOnQueryTextListener(searchView, listener);
    }

    public static void setOnCloseListener(android.view.View searchView, android.support.v4.widget.SearchViewCompat.OnCloseListener listener) {
        IMPL.setOnCloseListener(searchView, listener);
    }

    public static java.lang.CharSequence getQuery(android.view.View searchView) {
        return IMPL.getQuery(searchView);
    }

    public static void setQuery(android.view.View searchView, java.lang.CharSequence query, boolean submit) {
        IMPL.setQuery(searchView, query, submit);
    }

    public static void setQueryHint(android.view.View searchView, java.lang.CharSequence hint) {
        IMPL.setQueryHint(searchView, hint);
    }

    public static void setIconified(android.view.View searchView, boolean iconify) {
        IMPL.setIconified(searchView, iconify);
    }

    public static boolean isIconified(android.view.View searchView) {
        return IMPL.isIconified(searchView);
    }

    public static void setSubmitButtonEnabled(android.view.View searchView, boolean enabled) {
        IMPL.setSubmitButtonEnabled(searchView, enabled);
    }

    public static boolean isSubmitButtonEnabled(android.view.View searchView) {
        return IMPL.isSubmitButtonEnabled(searchView);
    }

    public static void setQueryRefinementEnabled(android.view.View searchView, boolean enable) {
        IMPL.setQueryRefinementEnabled(searchView, enable);
    }

    public static boolean isQueryRefinementEnabled(android.view.View searchView) {
        return IMPL.isQueryRefinementEnabled(searchView);
    }

    public static void setMaxWidth(android.view.View searchView, int maxpixels) {
        IMPL.setMaxWidth(searchView, maxpixels);
    }
}
