package android.support.v4.widget;

@android.annotation.TargetApi(18)
@android.support.annotation.RequiresApi(18)
class TextViewCompatJbMr2 {
    TextViewCompatJbMr2() {
    }

    public static void setCompoundDrawablesRelative(@android.support.annotation.NonNull android.widget.TextView textView, @android.support.annotation.Nullable android.graphics.drawable.Drawable start, @android.support.annotation.Nullable android.graphics.drawable.Drawable top, @android.support.annotation.Nullable android.graphics.drawable.Drawable end, @android.support.annotation.Nullable android.graphics.drawable.Drawable bottom) {
        textView.setCompoundDrawablesRelative(start, top, end, bottom);
    }

    public static void setCompoundDrawablesRelativeWithIntrinsicBounds(@android.support.annotation.NonNull android.widget.TextView textView, @android.support.annotation.Nullable android.graphics.drawable.Drawable start, @android.support.annotation.Nullable android.graphics.drawable.Drawable top, @android.support.annotation.Nullable android.graphics.drawable.Drawable end, @android.support.annotation.Nullable android.graphics.drawable.Drawable bottom) {
        textView.setCompoundDrawablesRelativeWithIntrinsicBounds(start, top, end, bottom);
    }

    public static void setCompoundDrawablesRelativeWithIntrinsicBounds(@android.support.annotation.NonNull android.widget.TextView textView, @android.support.annotation.DrawableRes int start, @android.support.annotation.DrawableRes int top, @android.support.annotation.DrawableRes int end, @android.support.annotation.DrawableRes int bottom) {
        textView.setCompoundDrawablesRelativeWithIntrinsicBounds(start, top, end, bottom);
    }

    public static android.graphics.drawable.Drawable[] getCompoundDrawablesRelative(@android.support.annotation.NonNull android.widget.TextView textView) {
        return textView.getCompoundDrawablesRelative();
    }
}
