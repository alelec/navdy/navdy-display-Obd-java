package android.support.v4.widget;

@android.annotation.TargetApi(23)
@android.support.annotation.RequiresApi(23)
class PopupWindowCompatApi23 {
    PopupWindowCompatApi23() {
    }

    static void setOverlapAnchor(android.widget.PopupWindow popupWindow, boolean overlapAnchor) {
        popupWindow.setOverlapAnchor(overlapAnchor);
    }

    static boolean getOverlapAnchor(android.widget.PopupWindow popupWindow) {
        return popupWindow.getOverlapAnchor();
    }

    static void setWindowLayoutType(android.widget.PopupWindow popupWindow, int layoutType) {
        popupWindow.setWindowLayoutType(layoutType);
    }

    static int getWindowLayoutType(android.widget.PopupWindow popupWindow) {
        return popupWindow.getWindowLayoutType();
    }
}
