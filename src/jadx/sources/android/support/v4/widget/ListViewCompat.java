package android.support.v4.widget;

public final class ListViewCompat {
    public static void scrollListBy(@android.support.annotation.NonNull android.widget.ListView listView, int y) {
        if (android.os.Build.VERSION.SDK_INT >= 19) {
            android.support.v4.widget.ListViewCompatKitKat.scrollListBy(listView, y);
        } else {
            android.support.v4.widget.ListViewCompatGingerbread.scrollListBy(listView, y);
        }
    }

    private ListViewCompat() {
    }
}
