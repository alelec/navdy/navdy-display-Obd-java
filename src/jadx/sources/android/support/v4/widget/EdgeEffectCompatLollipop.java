package android.support.v4.widget;

@android.annotation.TargetApi(21)
@android.support.annotation.RequiresApi(21)
class EdgeEffectCompatLollipop {
    EdgeEffectCompatLollipop() {
    }

    public static boolean onPull(java.lang.Object edgeEffect, float deltaDistance, float displacement) {
        ((android.widget.EdgeEffect) edgeEffect).onPull(deltaDistance, displacement);
        return true;
    }
}
