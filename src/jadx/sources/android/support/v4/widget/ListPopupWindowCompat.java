package android.support.v4.widget;

public final class ListPopupWindowCompat {
    static final android.support.v4.widget.ListPopupWindowCompat.ListPopupWindowImpl IMPL;

    static class BaseListPopupWindowImpl implements android.support.v4.widget.ListPopupWindowCompat.ListPopupWindowImpl {
        BaseListPopupWindowImpl() {
        }

        public android.view.View.OnTouchListener createDragToOpenListener(java.lang.Object listPopupWindow, android.view.View src) {
            return null;
        }
    }

    static class KitKatListPopupWindowImpl extends android.support.v4.widget.ListPopupWindowCompat.BaseListPopupWindowImpl {
        KitKatListPopupWindowImpl() {
        }

        public android.view.View.OnTouchListener createDragToOpenListener(java.lang.Object listPopupWindow, android.view.View src) {
            return android.support.v4.widget.ListPopupWindowCompatKitKat.createDragToOpenListener(listPopupWindow, src);
        }
    }

    interface ListPopupWindowImpl {
        android.view.View.OnTouchListener createDragToOpenListener(java.lang.Object obj, android.view.View view);
    }

    static {
        if (android.os.Build.VERSION.SDK_INT >= 19) {
            IMPL = new android.support.v4.widget.ListPopupWindowCompat.KitKatListPopupWindowImpl();
        } else {
            IMPL = new android.support.v4.widget.ListPopupWindowCompat.BaseListPopupWindowImpl();
        }
    }

    private ListPopupWindowCompat() {
    }

    public static android.view.View.OnTouchListener createDragToOpenListener(java.lang.Object listPopupWindow, android.view.View src) {
        return IMPL.createDragToOpenListener(listPopupWindow, src);
    }
}
