package android.support.v4.widget;

public abstract class CursorAdapter extends android.widget.BaseAdapter implements android.widget.Filterable, android.support.v4.widget.CursorFilter.CursorFilterClient {
    @java.lang.Deprecated
    public static final int FLAG_AUTO_REQUERY = 1;
    public static final int FLAG_REGISTER_CONTENT_OBSERVER = 2;
    @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
    protected boolean mAutoRequery;
    @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
    protected android.support.v4.widget.CursorAdapter.ChangeObserver mChangeObserver;
    @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
    protected android.content.Context mContext;
    @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
    protected android.database.Cursor mCursor;
    @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
    protected android.support.v4.widget.CursorFilter mCursorFilter;
    @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
    protected android.database.DataSetObserver mDataSetObserver;
    @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
    protected boolean mDataValid;
    @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
    protected android.widget.FilterQueryProvider mFilterQueryProvider;
    @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
    protected int mRowIDColumn;

    private class ChangeObserver extends android.database.ContentObserver {
        ChangeObserver() {
            super(new android.os.Handler());
        }

        public boolean deliverSelfNotifications() {
            return true;
        }

        public void onChange(boolean selfChange) {
            android.support.v4.widget.CursorAdapter.this.onContentChanged();
        }
    }

    private class MyDataSetObserver extends android.database.DataSetObserver {
        MyDataSetObserver() {
        }

        public void onChanged() {
            android.support.v4.widget.CursorAdapter.this.mDataValid = true;
            android.support.v4.widget.CursorAdapter.this.notifyDataSetChanged();
        }

        public void onInvalidated() {
            android.support.v4.widget.CursorAdapter.this.mDataValid = false;
            android.support.v4.widget.CursorAdapter.this.notifyDataSetInvalidated();
        }
    }

    public abstract void bindView(android.view.View view, android.content.Context context, android.database.Cursor cursor);

    public abstract android.view.View newView(android.content.Context context, android.database.Cursor cursor, android.view.ViewGroup viewGroup);

    @java.lang.Deprecated
    public CursorAdapter(android.content.Context context, android.database.Cursor c) {
        init(context, c, 1);
    }

    public CursorAdapter(android.content.Context context, android.database.Cursor c, boolean autoRequery) {
        init(context, c, autoRequery ? 1 : 2);
    }

    public CursorAdapter(android.content.Context context, android.database.Cursor c, int flags) {
        init(context, c, flags);
    }

    /* access modifiers changed from: protected */
    @java.lang.Deprecated
    public void init(android.content.Context context, android.database.Cursor c, boolean autoRequery) {
        init(context, c, autoRequery ? 1 : 2);
    }

    /* access modifiers changed from: 0000 */
    public void init(android.content.Context context, android.database.Cursor c, int flags) {
        boolean cursorPresent = true;
        if ((flags & 1) == 1) {
            flags |= 2;
            this.mAutoRequery = true;
        } else {
            this.mAutoRequery = false;
        }
        if (c == null) {
            cursorPresent = false;
        }
        this.mCursor = c;
        this.mDataValid = cursorPresent;
        this.mContext = context;
        this.mRowIDColumn = cursorPresent ? c.getColumnIndexOrThrow("_id") : -1;
        if ((flags & 2) == 2) {
            this.mChangeObserver = new android.support.v4.widget.CursorAdapter.ChangeObserver();
            this.mDataSetObserver = new android.support.v4.widget.CursorAdapter.MyDataSetObserver();
        } else {
            this.mChangeObserver = null;
            this.mDataSetObserver = null;
        }
        if (cursorPresent) {
            if (this.mChangeObserver != null) {
                c.registerContentObserver(this.mChangeObserver);
            }
            if (this.mDataSetObserver != null) {
                c.registerDataSetObserver(this.mDataSetObserver);
            }
        }
    }

    public android.database.Cursor getCursor() {
        return this.mCursor;
    }

    public int getCount() {
        if (!this.mDataValid || this.mCursor == null) {
            return 0;
        }
        return this.mCursor.getCount();
    }

    public java.lang.Object getItem(int position) {
        if (!this.mDataValid || this.mCursor == null) {
            return null;
        }
        this.mCursor.moveToPosition(position);
        return this.mCursor;
    }

    public long getItemId(int position) {
        if (!this.mDataValid || this.mCursor == null || !this.mCursor.moveToPosition(position)) {
            return 0;
        }
        return this.mCursor.getLong(this.mRowIDColumn);
    }

    public boolean hasStableIds() {
        return true;
    }

    public android.view.View getView(int position, android.view.View convertView, android.view.ViewGroup parent) {
        android.view.View v;
        if (!this.mDataValid) {
            throw new java.lang.IllegalStateException("this should only be called when the cursor is valid");
        } else if (!this.mCursor.moveToPosition(position)) {
            throw new java.lang.IllegalStateException("couldn't move cursor to position " + position);
        } else {
            if (convertView == null) {
                v = newView(this.mContext, this.mCursor, parent);
            } else {
                v = convertView;
            }
            bindView(v, this.mContext, this.mCursor);
            return v;
        }
    }

    public android.view.View getDropDownView(int position, android.view.View convertView, android.view.ViewGroup parent) {
        android.view.View v;
        if (!this.mDataValid) {
            return null;
        }
        this.mCursor.moveToPosition(position);
        if (convertView == null) {
            v = newDropDownView(this.mContext, this.mCursor, parent);
        } else {
            v = convertView;
        }
        bindView(v, this.mContext, this.mCursor);
        return v;
    }

    public android.view.View newDropDownView(android.content.Context context, android.database.Cursor cursor, android.view.ViewGroup parent) {
        return newView(context, cursor, parent);
    }

    public void changeCursor(android.database.Cursor cursor) {
        android.database.Cursor old = swapCursor(cursor);
        if (old != null) {
            old.close();
        }
    }

    public android.database.Cursor swapCursor(android.database.Cursor newCursor) {
        if (newCursor == this.mCursor) {
            return null;
        }
        android.database.Cursor oldCursor = this.mCursor;
        if (oldCursor != null) {
            if (this.mChangeObserver != null) {
                oldCursor.unregisterContentObserver(this.mChangeObserver);
            }
            if (this.mDataSetObserver != null) {
                oldCursor.unregisterDataSetObserver(this.mDataSetObserver);
            }
        }
        this.mCursor = newCursor;
        if (newCursor != null) {
            if (this.mChangeObserver != null) {
                newCursor.registerContentObserver(this.mChangeObserver);
            }
            if (this.mDataSetObserver != null) {
                newCursor.registerDataSetObserver(this.mDataSetObserver);
            }
            this.mRowIDColumn = newCursor.getColumnIndexOrThrow("_id");
            this.mDataValid = true;
            notifyDataSetChanged();
            return oldCursor;
        }
        this.mRowIDColumn = -1;
        this.mDataValid = false;
        notifyDataSetInvalidated();
        return oldCursor;
    }

    public java.lang.CharSequence convertToString(android.database.Cursor cursor) {
        return cursor == null ? "" : cursor.toString();
    }

    public android.database.Cursor runQueryOnBackgroundThread(java.lang.CharSequence constraint) {
        if (this.mFilterQueryProvider != null) {
            return this.mFilterQueryProvider.runQuery(constraint);
        }
        return this.mCursor;
    }

    public android.widget.Filter getFilter() {
        if (this.mCursorFilter == null) {
            this.mCursorFilter = new android.support.v4.widget.CursorFilter(this);
        }
        return this.mCursorFilter;
    }

    public android.widget.FilterQueryProvider getFilterQueryProvider() {
        return this.mFilterQueryProvider;
    }

    public void setFilterQueryProvider(android.widget.FilterQueryProvider filterQueryProvider) {
        this.mFilterQueryProvider = filterQueryProvider;
    }

    /* access modifiers changed from: protected */
    public void onContentChanged() {
        if (this.mAutoRequery && this.mCursor != null && !this.mCursor.isClosed()) {
            this.mDataValid = this.mCursor.requery();
        }
    }
}
