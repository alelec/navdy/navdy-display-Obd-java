package android.support.v4.widget;

@android.annotation.TargetApi(14)
@android.support.annotation.RequiresApi(14)
class ScrollerCompatIcs {
    ScrollerCompatIcs() {
    }

    public static float getCurrVelocity(java.lang.Object scroller) {
        return ((android.widget.OverScroller) scroller).getCurrVelocity();
    }
}
