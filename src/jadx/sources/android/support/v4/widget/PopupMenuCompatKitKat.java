package android.support.v4.widget;

@android.annotation.TargetApi(19)
@android.support.annotation.RequiresApi(19)
class PopupMenuCompatKitKat {
    PopupMenuCompatKitKat() {
    }

    public static android.view.View.OnTouchListener getDragToOpenListener(java.lang.Object popupMenu) {
        return ((android.widget.PopupMenu) popupMenu).getDragToOpenListener();
    }
}
