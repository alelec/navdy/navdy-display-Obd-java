package android.support.v4.widget;

public final class CompoundButtonCompat {
    private static final android.support.v4.widget.CompoundButtonCompat.CompoundButtonCompatImpl IMPL;

    static class Api23CompoundButtonImpl extends android.support.v4.widget.CompoundButtonCompat.LollipopCompoundButtonImpl {
        Api23CompoundButtonImpl() {
        }

        public android.graphics.drawable.Drawable getButtonDrawable(android.widget.CompoundButton button) {
            return android.support.v4.widget.CompoundButtonCompatApi23.getButtonDrawable(button);
        }
    }

    static class BaseCompoundButtonCompat implements android.support.v4.widget.CompoundButtonCompat.CompoundButtonCompatImpl {
        BaseCompoundButtonCompat() {
        }

        public void setButtonTintList(android.widget.CompoundButton button, android.content.res.ColorStateList tint) {
            android.support.v4.widget.CompoundButtonCompatGingerbread.setButtonTintList(button, tint);
        }

        public android.content.res.ColorStateList getButtonTintList(android.widget.CompoundButton button) {
            return android.support.v4.widget.CompoundButtonCompatGingerbread.getButtonTintList(button);
        }

        public void setButtonTintMode(android.widget.CompoundButton button, android.graphics.PorterDuff.Mode tintMode) {
            android.support.v4.widget.CompoundButtonCompatGingerbread.setButtonTintMode(button, tintMode);
        }

        public android.graphics.PorterDuff.Mode getButtonTintMode(android.widget.CompoundButton button) {
            return android.support.v4.widget.CompoundButtonCompatGingerbread.getButtonTintMode(button);
        }

        public android.graphics.drawable.Drawable getButtonDrawable(android.widget.CompoundButton button) {
            return android.support.v4.widget.CompoundButtonCompatGingerbread.getButtonDrawable(button);
        }
    }

    interface CompoundButtonCompatImpl {
        android.graphics.drawable.Drawable getButtonDrawable(android.widget.CompoundButton compoundButton);

        android.content.res.ColorStateList getButtonTintList(android.widget.CompoundButton compoundButton);

        android.graphics.PorterDuff.Mode getButtonTintMode(android.widget.CompoundButton compoundButton);

        void setButtonTintList(android.widget.CompoundButton compoundButton, android.content.res.ColorStateList colorStateList);

        void setButtonTintMode(android.widget.CompoundButton compoundButton, android.graphics.PorterDuff.Mode mode);
    }

    static class LollipopCompoundButtonImpl extends android.support.v4.widget.CompoundButtonCompat.BaseCompoundButtonCompat {
        LollipopCompoundButtonImpl() {
        }

        public void setButtonTintList(android.widget.CompoundButton button, android.content.res.ColorStateList tint) {
            android.support.v4.widget.CompoundButtonCompatLollipop.setButtonTintList(button, tint);
        }

        public android.content.res.ColorStateList getButtonTintList(android.widget.CompoundButton button) {
            return android.support.v4.widget.CompoundButtonCompatLollipop.getButtonTintList(button);
        }

        public void setButtonTintMode(android.widget.CompoundButton button, android.graphics.PorterDuff.Mode tintMode) {
            android.support.v4.widget.CompoundButtonCompatLollipop.setButtonTintMode(button, tintMode);
        }

        public android.graphics.PorterDuff.Mode getButtonTintMode(android.widget.CompoundButton button) {
            return android.support.v4.widget.CompoundButtonCompatLollipop.getButtonTintMode(button);
        }
    }

    static {
        int sdk = android.os.Build.VERSION.SDK_INT;
        if (sdk >= 23) {
            IMPL = new android.support.v4.widget.CompoundButtonCompat.Api23CompoundButtonImpl();
        } else if (sdk >= 21) {
            IMPL = new android.support.v4.widget.CompoundButtonCompat.LollipopCompoundButtonImpl();
        } else {
            IMPL = new android.support.v4.widget.CompoundButtonCompat.BaseCompoundButtonCompat();
        }
    }

    private CompoundButtonCompat() {
    }

    public static void setButtonTintList(@android.support.annotation.NonNull android.widget.CompoundButton button, @android.support.annotation.Nullable android.content.res.ColorStateList tint) {
        IMPL.setButtonTintList(button, tint);
    }

    @android.support.annotation.Nullable
    public static android.content.res.ColorStateList getButtonTintList(@android.support.annotation.NonNull android.widget.CompoundButton button) {
        return IMPL.getButtonTintList(button);
    }

    public static void setButtonTintMode(@android.support.annotation.NonNull android.widget.CompoundButton button, @android.support.annotation.Nullable android.graphics.PorterDuff.Mode tintMode) {
        IMPL.setButtonTintMode(button, tintMode);
    }

    @android.support.annotation.Nullable
    public static android.graphics.PorterDuff.Mode getButtonTintMode(@android.support.annotation.NonNull android.widget.CompoundButton button) {
        return IMPL.getButtonTintMode(button);
    }

    @android.support.annotation.Nullable
    public static android.graphics.drawable.Drawable getButtonDrawable(@android.support.annotation.NonNull android.widget.CompoundButton button) {
        return IMPL.getButtonDrawable(button);
    }
}
