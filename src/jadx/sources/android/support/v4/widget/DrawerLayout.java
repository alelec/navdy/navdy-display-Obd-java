package android.support.v4.widget;

public class DrawerLayout extends android.view.ViewGroup implements android.support.v4.widget.DrawerLayoutImpl {
    private static final boolean ALLOW_EDGE_LOCK = false;
    static final boolean CAN_HIDE_DESCENDANTS;
    private static final boolean CHILDREN_DISALLOW_INTERCEPT = true;
    private static final int DEFAULT_SCRIM_COLOR = -1728053248;
    private static final int DRAWER_ELEVATION = 10;
    static final android.support.v4.widget.DrawerLayout.DrawerLayoutCompatImpl IMPL;
    static final int[] LAYOUT_ATTRS = {16842931};
    public static final int LOCK_MODE_LOCKED_CLOSED = 1;
    public static final int LOCK_MODE_LOCKED_OPEN = 2;
    public static final int LOCK_MODE_UNDEFINED = 3;
    public static final int LOCK_MODE_UNLOCKED = 0;
    private static final int MIN_DRAWER_MARGIN = 64;
    private static final int MIN_FLING_VELOCITY = 400;
    private static final int PEEK_DELAY = 160;
    private static final boolean SET_DRAWER_SHADOW_FROM_ELEVATION;
    public static final int STATE_DRAGGING = 1;
    public static final int STATE_IDLE = 0;
    public static final int STATE_SETTLING = 2;
    private static final java.lang.String TAG = "DrawerLayout";
    private static final float TOUCH_SLOP_SENSITIVITY = 1.0f;
    private final android.support.v4.widget.DrawerLayout.ChildAccessibilityDelegate mChildAccessibilityDelegate;
    private boolean mChildrenCanceledTouch;
    private boolean mDisallowInterceptRequested;
    private boolean mDrawStatusBarBackground;
    private float mDrawerElevation;
    private int mDrawerState;
    private boolean mFirstLayout;
    private boolean mInLayout;
    private float mInitialMotionX;
    private float mInitialMotionY;
    private java.lang.Object mLastInsets;
    private final android.support.v4.widget.DrawerLayout.ViewDragCallback mLeftCallback;
    private final android.support.v4.widget.ViewDragHelper mLeftDragger;
    @android.support.annotation.Nullable
    private android.support.v4.widget.DrawerLayout.DrawerListener mListener;
    private java.util.List<android.support.v4.widget.DrawerLayout.DrawerListener> mListeners;
    private int mLockModeEnd;
    private int mLockModeLeft;
    private int mLockModeRight;
    private int mLockModeStart;
    private int mMinDrawerMargin;
    private final java.util.ArrayList<android.view.View> mNonDrawerViews;
    private final android.support.v4.widget.DrawerLayout.ViewDragCallback mRightCallback;
    private final android.support.v4.widget.ViewDragHelper mRightDragger;
    private int mScrimColor;
    private float mScrimOpacity;
    private android.graphics.Paint mScrimPaint;
    private android.graphics.drawable.Drawable mShadowEnd;
    private android.graphics.drawable.Drawable mShadowLeft;
    private android.graphics.drawable.Drawable mShadowLeftResolved;
    private android.graphics.drawable.Drawable mShadowRight;
    private android.graphics.drawable.Drawable mShadowRightResolved;
    private android.graphics.drawable.Drawable mShadowStart;
    private android.graphics.drawable.Drawable mStatusBarBackground;
    private java.lang.CharSequence mTitleLeft;
    private java.lang.CharSequence mTitleRight;

    class AccessibilityDelegate extends android.support.v4.view.AccessibilityDelegateCompat {
        private final android.graphics.Rect mTmpRect = new android.graphics.Rect();

        AccessibilityDelegate() {
        }

        public void onInitializeAccessibilityNodeInfo(android.view.View host, android.support.v4.view.accessibility.AccessibilityNodeInfoCompat info) {
            if (android.support.v4.widget.DrawerLayout.CAN_HIDE_DESCENDANTS) {
                super.onInitializeAccessibilityNodeInfo(host, info);
            } else {
                android.support.v4.view.accessibility.AccessibilityNodeInfoCompat superNode = android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.obtain(info);
                super.onInitializeAccessibilityNodeInfo(host, superNode);
                info.setSource(host);
                android.view.ViewParent parent = android.support.v4.view.ViewCompat.getParentForAccessibility(host);
                if (parent instanceof android.view.View) {
                    info.setParent((android.view.View) parent);
                }
                copyNodeInfoNoChildren(info, superNode);
                superNode.recycle();
                addChildrenForAccessibility(info, (android.view.ViewGroup) host);
            }
            info.setClassName(android.support.v4.widget.DrawerLayout.class.getName());
            info.setFocusable(false);
            info.setFocused(false);
            info.removeAction(android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat.ACTION_FOCUS);
            info.removeAction(android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat.ACTION_CLEAR_FOCUS);
        }

        public void onInitializeAccessibilityEvent(android.view.View host, android.view.accessibility.AccessibilityEvent event) {
            super.onInitializeAccessibilityEvent(host, event);
            event.setClassName(android.support.v4.widget.DrawerLayout.class.getName());
        }

        public boolean dispatchPopulateAccessibilityEvent(android.view.View host, android.view.accessibility.AccessibilityEvent event) {
            if (event.getEventType() != 32) {
                return super.dispatchPopulateAccessibilityEvent(host, event);
            }
            java.util.List<java.lang.CharSequence> eventText = event.getText();
            android.view.View visibleDrawer = android.support.v4.widget.DrawerLayout.this.findVisibleDrawer();
            if (visibleDrawer != null) {
                java.lang.CharSequence title = android.support.v4.widget.DrawerLayout.this.getDrawerTitle(android.support.v4.widget.DrawerLayout.this.getDrawerViewAbsoluteGravity(visibleDrawer));
                if (title != null) {
                    eventText.add(title);
                }
            }
            return android.support.v4.widget.DrawerLayout.CHILDREN_DISALLOW_INTERCEPT;
        }

        public boolean onRequestSendAccessibilityEvent(android.view.ViewGroup host, android.view.View child, android.view.accessibility.AccessibilityEvent event) {
            if (android.support.v4.widget.DrawerLayout.CAN_HIDE_DESCENDANTS || android.support.v4.widget.DrawerLayout.includeChildForAccessibility(child)) {
                return super.onRequestSendAccessibilityEvent(host, child, event);
            }
            return false;
        }

        private void addChildrenForAccessibility(android.support.v4.view.accessibility.AccessibilityNodeInfoCompat info, android.view.ViewGroup v) {
            int childCount = v.getChildCount();
            for (int i = 0; i < childCount; i++) {
                android.view.View child = v.getChildAt(i);
                if (android.support.v4.widget.DrawerLayout.includeChildForAccessibility(child)) {
                    info.addChild(child);
                }
            }
        }

        private void copyNodeInfoNoChildren(android.support.v4.view.accessibility.AccessibilityNodeInfoCompat dest, android.support.v4.view.accessibility.AccessibilityNodeInfoCompat src) {
            android.graphics.Rect rect = this.mTmpRect;
            src.getBoundsInParent(rect);
            dest.setBoundsInParent(rect);
            src.getBoundsInScreen(rect);
            dest.setBoundsInScreen(rect);
            dest.setVisibleToUser(src.isVisibleToUser());
            dest.setPackageName(src.getPackageName());
            dest.setClassName(src.getClassName());
            dest.setContentDescription(src.getContentDescription());
            dest.setEnabled(src.isEnabled());
            dest.setClickable(src.isClickable());
            dest.setFocusable(src.isFocusable());
            dest.setFocused(src.isFocused());
            dest.setAccessibilityFocused(src.isAccessibilityFocused());
            dest.setSelected(src.isSelected());
            dest.setLongClickable(src.isLongClickable());
            dest.addAction(src.getActions());
        }
    }

    final class ChildAccessibilityDelegate extends android.support.v4.view.AccessibilityDelegateCompat {
        ChildAccessibilityDelegate() {
        }

        public void onInitializeAccessibilityNodeInfo(android.view.View child, android.support.v4.view.accessibility.AccessibilityNodeInfoCompat info) {
            super.onInitializeAccessibilityNodeInfo(child, info);
            if (!android.support.v4.widget.DrawerLayout.includeChildForAccessibility(child)) {
                info.setParent(null);
            }
        }
    }

    interface DrawerLayoutCompatImpl {
        void applyMarginInsets(android.view.ViewGroup.MarginLayoutParams marginLayoutParams, java.lang.Object obj, int i);

        void configureApplyInsets(android.view.View view);

        void dispatchChildInsets(android.view.View view, java.lang.Object obj, int i);

        android.graphics.drawable.Drawable getDefaultStatusBarBackground(android.content.Context context);

        int getTopInset(java.lang.Object obj);
    }

    static class DrawerLayoutCompatImplApi21 implements android.support.v4.widget.DrawerLayout.DrawerLayoutCompatImpl {
        DrawerLayoutCompatImplApi21() {
        }

        public void configureApplyInsets(android.view.View drawerLayout) {
            android.support.v4.widget.DrawerLayoutCompatApi21.configureApplyInsets(drawerLayout);
        }

        public void dispatchChildInsets(android.view.View child, java.lang.Object insets, int drawerGravity) {
            android.support.v4.widget.DrawerLayoutCompatApi21.dispatchChildInsets(child, insets, drawerGravity);
        }

        public void applyMarginInsets(android.view.ViewGroup.MarginLayoutParams lp, java.lang.Object insets, int drawerGravity) {
            android.support.v4.widget.DrawerLayoutCompatApi21.applyMarginInsets(lp, insets, drawerGravity);
        }

        public int getTopInset(java.lang.Object insets) {
            return android.support.v4.widget.DrawerLayoutCompatApi21.getTopInset(insets);
        }

        public android.graphics.drawable.Drawable getDefaultStatusBarBackground(android.content.Context context) {
            return android.support.v4.widget.DrawerLayoutCompatApi21.getDefaultStatusBarBackground(context);
        }
    }

    static class DrawerLayoutCompatImplBase implements android.support.v4.widget.DrawerLayout.DrawerLayoutCompatImpl {
        DrawerLayoutCompatImplBase() {
        }

        public void configureApplyInsets(android.view.View drawerLayout) {
        }

        public void dispatchChildInsets(android.view.View child, java.lang.Object insets, int drawerGravity) {
        }

        public void applyMarginInsets(android.view.ViewGroup.MarginLayoutParams lp, java.lang.Object insets, int drawerGravity) {
        }

        public int getTopInset(java.lang.Object insets) {
            return 0;
        }

        public android.graphics.drawable.Drawable getDefaultStatusBarBackground(android.content.Context context) {
            return null;
        }
    }

    public interface DrawerListener {
        void onDrawerClosed(android.view.View view);

        void onDrawerOpened(android.view.View view);

        void onDrawerSlide(android.view.View view, float f);

        void onDrawerStateChanged(int i);
    }

    public static class LayoutParams extends android.view.ViewGroup.MarginLayoutParams {
        private static final int FLAG_IS_CLOSING = 4;
        private static final int FLAG_IS_OPENED = 1;
        private static final int FLAG_IS_OPENING = 2;
        public int gravity;
        boolean isPeeking;
        float onScreen;
        int openState;

        public LayoutParams(android.content.Context c, android.util.AttributeSet attrs) {
            super(c, attrs);
            this.gravity = 0;
            android.content.res.TypedArray a = c.obtainStyledAttributes(attrs, android.support.v4.widget.DrawerLayout.LAYOUT_ATTRS);
            this.gravity = a.getInt(0, 0);
            a.recycle();
        }

        public LayoutParams(int width, int height) {
            super(width, height);
            this.gravity = 0;
        }

        public LayoutParams(int width, int height, int gravity2) {
            this(width, height);
            this.gravity = gravity2;
        }

        public LayoutParams(android.support.v4.widget.DrawerLayout.LayoutParams source) {
            super(source);
            this.gravity = 0;
            this.gravity = source.gravity;
        }

        public LayoutParams(android.view.ViewGroup.LayoutParams source) {
            super(source);
            this.gravity = 0;
        }

        public LayoutParams(android.view.ViewGroup.MarginLayoutParams source) {
            super(source);
            this.gravity = 0;
        }
    }

    protected static class SavedState extends android.support.v4.view.AbsSavedState {
        public static final android.os.Parcelable.Creator<android.support.v4.widget.DrawerLayout.SavedState> CREATOR = android.support.v4.os.ParcelableCompat.newCreator(new android.support.v4.os.ParcelableCompatCreatorCallbacks<android.support.v4.widget.DrawerLayout.SavedState>() {
            public android.support.v4.widget.DrawerLayout.SavedState createFromParcel(android.os.Parcel in, java.lang.ClassLoader loader) {
                return new android.support.v4.widget.DrawerLayout.SavedState(in, loader);
            }

            public android.support.v4.widget.DrawerLayout.SavedState[] newArray(int size) {
                return new android.support.v4.widget.DrawerLayout.SavedState[size];
            }
        });
        int lockModeEnd;
        int lockModeLeft;
        int lockModeRight;
        int lockModeStart;
        int openDrawerGravity = 0;

        public SavedState(android.os.Parcel in, java.lang.ClassLoader loader) {
            super(in, loader);
            this.openDrawerGravity = in.readInt();
            this.lockModeLeft = in.readInt();
            this.lockModeRight = in.readInt();
            this.lockModeStart = in.readInt();
            this.lockModeEnd = in.readInt();
        }

        public SavedState(android.os.Parcelable superState) {
            super(superState);
        }

        public void writeToParcel(android.os.Parcel dest, int flags) {
            super.writeToParcel(dest, flags);
            dest.writeInt(this.openDrawerGravity);
            dest.writeInt(this.lockModeLeft);
            dest.writeInt(this.lockModeRight);
            dest.writeInt(this.lockModeStart);
            dest.writeInt(this.lockModeEnd);
        }
    }

    public static abstract class SimpleDrawerListener implements android.support.v4.widget.DrawerLayout.DrawerListener {
        public void onDrawerSlide(android.view.View drawerView, float slideOffset) {
        }

        public void onDrawerOpened(android.view.View drawerView) {
        }

        public void onDrawerClosed(android.view.View drawerView) {
        }

        public void onDrawerStateChanged(int newState) {
        }
    }

    private class ViewDragCallback extends android.support.v4.widget.ViewDragHelper.Callback {
        private final int mAbsGravity;
        private android.support.v4.widget.ViewDragHelper mDragger;
        private final java.lang.Runnable mPeekRunnable = new java.lang.Runnable() {
            public void run() {
                android.support.v4.widget.DrawerLayout.ViewDragCallback.this.peekDrawer();
            }
        };

        ViewDragCallback(int gravity) {
            this.mAbsGravity = gravity;
        }

        public void setDragger(android.support.v4.widget.ViewDragHelper dragger) {
            this.mDragger = dragger;
        }

        public void removeCallbacks() {
            android.support.v4.widget.DrawerLayout.this.removeCallbacks(this.mPeekRunnable);
        }

        public boolean tryCaptureView(android.view.View child, int pointerId) {
            if (!android.support.v4.widget.DrawerLayout.this.isDrawerView(child) || !android.support.v4.widget.DrawerLayout.this.checkDrawerViewAbsoluteGravity(child, this.mAbsGravity) || android.support.v4.widget.DrawerLayout.this.getDrawerLockMode(child) != 0) {
                return false;
            }
            return android.support.v4.widget.DrawerLayout.CHILDREN_DISALLOW_INTERCEPT;
        }

        public void onViewDragStateChanged(int state) {
            android.support.v4.widget.DrawerLayout.this.updateDrawerState(this.mAbsGravity, state, this.mDragger.getCapturedView());
        }

        public void onViewPositionChanged(android.view.View changedView, int left, int top, int dx, int dy) {
            float offset;
            int childWidth = changedView.getWidth();
            if (android.support.v4.widget.DrawerLayout.this.checkDrawerViewAbsoluteGravity(changedView, 3)) {
                offset = ((float) (childWidth + left)) / ((float) childWidth);
            } else {
                offset = ((float) (android.support.v4.widget.DrawerLayout.this.getWidth() - left)) / ((float) childWidth);
            }
            android.support.v4.widget.DrawerLayout.this.setDrawerViewOffset(changedView, offset);
            changedView.setVisibility(offset == 0.0f ? 4 : 0);
            android.support.v4.widget.DrawerLayout.this.invalidate();
        }

        public void onViewCaptured(android.view.View capturedChild, int activePointerId) {
            ((android.support.v4.widget.DrawerLayout.LayoutParams) capturedChild.getLayoutParams()).isPeeking = false;
            closeOtherDrawer();
        }

        private void closeOtherDrawer() {
            int otherGrav = 3;
            if (this.mAbsGravity == 3) {
                otherGrav = 5;
            }
            android.view.View toClose = android.support.v4.widget.DrawerLayout.this.findDrawerWithGravity(otherGrav);
            if (toClose != null) {
                android.support.v4.widget.DrawerLayout.this.closeDrawer(toClose);
            }
        }

        public void onViewReleased(android.view.View releasedChild, float xvel, float yvel) {
            int left;
            float offset = android.support.v4.widget.DrawerLayout.this.getDrawerViewOffset(releasedChild);
            int childWidth = releasedChild.getWidth();
            if (android.support.v4.widget.DrawerLayout.this.checkDrawerViewAbsoluteGravity(releasedChild, 3)) {
                left = (xvel > 0.0f || (xvel == 0.0f && offset > 0.5f)) ? 0 : -childWidth;
            } else {
                int width = android.support.v4.widget.DrawerLayout.this.getWidth();
                left = (xvel < 0.0f || (xvel == 0.0f && offset > 0.5f)) ? width - childWidth : width;
            }
            this.mDragger.settleCapturedViewAt(left, releasedChild.getTop());
            android.support.v4.widget.DrawerLayout.this.invalidate();
        }

        public void onEdgeTouched(int edgeFlags, int pointerId) {
            android.support.v4.widget.DrawerLayout.this.postDelayed(this.mPeekRunnable, 160);
        }

        /* access modifiers changed from: 0000 */
        public void peekDrawer() {
            boolean leftEdge;
            android.view.View toCapture;
            int childLeft;
            int i = 0;
            int peekDistance = this.mDragger.getEdgeSize();
            if (this.mAbsGravity == 3) {
                leftEdge = true;
            } else {
                leftEdge = false;
            }
            if (leftEdge) {
                toCapture = android.support.v4.widget.DrawerLayout.this.findDrawerWithGravity(3);
                if (toCapture != null) {
                    i = -toCapture.getWidth();
                }
                childLeft = i + peekDistance;
            } else {
                toCapture = android.support.v4.widget.DrawerLayout.this.findDrawerWithGravity(5);
                childLeft = android.support.v4.widget.DrawerLayout.this.getWidth() - peekDistance;
            }
            if (toCapture == null) {
                return;
            }
            if (((leftEdge && toCapture.getLeft() < childLeft) || (!leftEdge && toCapture.getLeft() > childLeft)) && android.support.v4.widget.DrawerLayout.this.getDrawerLockMode(toCapture) == 0) {
                android.support.v4.widget.DrawerLayout.LayoutParams lp = (android.support.v4.widget.DrawerLayout.LayoutParams) toCapture.getLayoutParams();
                this.mDragger.smoothSlideViewTo(toCapture, childLeft, toCapture.getTop());
                lp.isPeeking = android.support.v4.widget.DrawerLayout.CHILDREN_DISALLOW_INTERCEPT;
                android.support.v4.widget.DrawerLayout.this.invalidate();
                closeOtherDrawer();
                android.support.v4.widget.DrawerLayout.this.cancelChildViewTouch();
            }
        }

        public boolean onEdgeLock(int edgeFlags) {
            return false;
        }

        public void onEdgeDragStarted(int edgeFlags, int pointerId) {
            android.view.View toCapture;
            if ((edgeFlags & 1) == 1) {
                toCapture = android.support.v4.widget.DrawerLayout.this.findDrawerWithGravity(3);
            } else {
                toCapture = android.support.v4.widget.DrawerLayout.this.findDrawerWithGravity(5);
            }
            if (toCapture != null && android.support.v4.widget.DrawerLayout.this.getDrawerLockMode(toCapture) == 0) {
                this.mDragger.captureChildView(toCapture, pointerId);
            }
        }

        public int getViewHorizontalDragRange(android.view.View child) {
            if (android.support.v4.widget.DrawerLayout.this.isDrawerView(child)) {
                return child.getWidth();
            }
            return 0;
        }

        public int clampViewPositionHorizontal(android.view.View child, int left, int dx) {
            if (android.support.v4.widget.DrawerLayout.this.checkDrawerViewAbsoluteGravity(child, 3)) {
                return java.lang.Math.max(-child.getWidth(), java.lang.Math.min(left, 0));
            }
            int width = android.support.v4.widget.DrawerLayout.this.getWidth();
            return java.lang.Math.max(width - child.getWidth(), java.lang.Math.min(left, width));
        }

        public int clampViewPositionVertical(android.view.View child, int top, int dy) {
            return child.getTop();
        }
    }

    static {
        boolean z;
        boolean z2 = CHILDREN_DISALLOW_INTERCEPT;
        if (android.os.Build.VERSION.SDK_INT >= 19) {
            z = true;
        } else {
            z = false;
        }
        CAN_HIDE_DESCENDANTS = z;
        if (android.os.Build.VERSION.SDK_INT < 21) {
            z2 = false;
        }
        SET_DRAWER_SHADOW_FROM_ELEVATION = z2;
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            IMPL = new android.support.v4.widget.DrawerLayout.DrawerLayoutCompatImplApi21();
        } else {
            IMPL = new android.support.v4.widget.DrawerLayout.DrawerLayoutCompatImplBase();
        }
    }

    public DrawerLayout(android.content.Context context) {
        this(context, null);
    }

    public DrawerLayout(android.content.Context context, android.util.AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DrawerLayout(android.content.Context context, android.util.AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.mChildAccessibilityDelegate = new android.support.v4.widget.DrawerLayout.ChildAccessibilityDelegate();
        this.mScrimColor = DEFAULT_SCRIM_COLOR;
        this.mScrimPaint = new android.graphics.Paint();
        this.mFirstLayout = CHILDREN_DISALLOW_INTERCEPT;
        this.mLockModeLeft = 3;
        this.mLockModeRight = 3;
        this.mLockModeStart = 3;
        this.mLockModeEnd = 3;
        this.mShadowStart = null;
        this.mShadowEnd = null;
        this.mShadowLeft = null;
        this.mShadowRight = null;
        setDescendantFocusability(262144);
        float density = getResources().getDisplayMetrics().density;
        this.mMinDrawerMargin = (int) ((64.0f * density) + 0.5f);
        float minVel = 400.0f * density;
        this.mLeftCallback = new android.support.v4.widget.DrawerLayout.ViewDragCallback(3);
        this.mRightCallback = new android.support.v4.widget.DrawerLayout.ViewDragCallback(5);
        this.mLeftDragger = android.support.v4.widget.ViewDragHelper.create(this, TOUCH_SLOP_SENSITIVITY, this.mLeftCallback);
        this.mLeftDragger.setEdgeTrackingEnabled(1);
        this.mLeftDragger.setMinVelocity(minVel);
        this.mLeftCallback.setDragger(this.mLeftDragger);
        this.mRightDragger = android.support.v4.widget.ViewDragHelper.create(this, TOUCH_SLOP_SENSITIVITY, this.mRightCallback);
        this.mRightDragger.setEdgeTrackingEnabled(2);
        this.mRightDragger.setMinVelocity(minVel);
        this.mRightCallback.setDragger(this.mRightDragger);
        setFocusableInTouchMode(CHILDREN_DISALLOW_INTERCEPT);
        android.support.v4.view.ViewCompat.setImportantForAccessibility(this, 1);
        android.support.v4.view.ViewCompat.setAccessibilityDelegate(this, new android.support.v4.widget.DrawerLayout.AccessibilityDelegate());
        android.support.v4.view.ViewGroupCompat.setMotionEventSplittingEnabled(this, false);
        if (android.support.v4.view.ViewCompat.getFitsSystemWindows(this)) {
            IMPL.configureApplyInsets(this);
            this.mStatusBarBackground = IMPL.getDefaultStatusBarBackground(context);
        }
        this.mDrawerElevation = 10.0f * density;
        this.mNonDrawerViews = new java.util.ArrayList<>();
    }

    public void setDrawerElevation(float elevation) {
        this.mDrawerElevation = elevation;
        for (int i = 0; i < getChildCount(); i++) {
            android.view.View child = getChildAt(i);
            if (isDrawerView(child)) {
                android.support.v4.view.ViewCompat.setElevation(child, this.mDrawerElevation);
            }
        }
    }

    public float getDrawerElevation() {
        if (SET_DRAWER_SHADOW_FROM_ELEVATION) {
            return this.mDrawerElevation;
        }
        return 0.0f;
    }

    public void setChildInsets(java.lang.Object insets, boolean draw) {
        this.mLastInsets = insets;
        this.mDrawStatusBarBackground = draw;
        setWillNotDraw((draw || getBackground() != null) ? false : CHILDREN_DISALLOW_INTERCEPT);
        requestLayout();
    }

    public void setDrawerShadow(android.graphics.drawable.Drawable shadowDrawable, int gravity) {
        if (!SET_DRAWER_SHADOW_FROM_ELEVATION) {
            if ((gravity & android.support.v4.view.GravityCompat.START) == 8388611) {
                this.mShadowStart = shadowDrawable;
            } else if ((gravity & android.support.v4.view.GravityCompat.END) == 8388613) {
                this.mShadowEnd = shadowDrawable;
            } else if ((gravity & 3) == 3) {
                this.mShadowLeft = shadowDrawable;
            } else if ((gravity & 5) == 5) {
                this.mShadowRight = shadowDrawable;
            } else {
                return;
            }
            resolveShadowDrawables();
            invalidate();
        }
    }

    public void setDrawerShadow(@android.support.annotation.DrawableRes int resId, int gravity) {
        setDrawerShadow(android.support.v4.content.ContextCompat.getDrawable(getContext(), resId), gravity);
    }

    public void setScrimColor(@android.support.annotation.ColorInt int color) {
        this.mScrimColor = color;
        invalidate();
    }

    @java.lang.Deprecated
    public void setDrawerListener(android.support.v4.widget.DrawerLayout.DrawerListener listener) {
        if (this.mListener != null) {
            removeDrawerListener(this.mListener);
        }
        if (listener != null) {
            addDrawerListener(listener);
        }
        this.mListener = listener;
    }

    public void addDrawerListener(@android.support.annotation.NonNull android.support.v4.widget.DrawerLayout.DrawerListener listener) {
        if (listener != null) {
            if (this.mListeners == null) {
                this.mListeners = new java.util.ArrayList();
            }
            this.mListeners.add(listener);
        }
    }

    public void removeDrawerListener(@android.support.annotation.NonNull android.support.v4.widget.DrawerLayout.DrawerListener listener) {
        if (listener != null && this.mListeners != null) {
            this.mListeners.remove(listener);
        }
    }

    public void setDrawerLockMode(int lockMode) {
        setDrawerLockMode(lockMode, 3);
        setDrawerLockMode(lockMode, 5);
    }

    public void setDrawerLockMode(int lockMode, int edgeGravity) {
        int absGravity = android.support.v4.view.GravityCompat.getAbsoluteGravity(edgeGravity, android.support.v4.view.ViewCompat.getLayoutDirection(this));
        switch (edgeGravity) {
            case 3:
                this.mLockModeLeft = lockMode;
                break;
            case 5:
                this.mLockModeRight = lockMode;
                break;
            case android.support.v4.view.GravityCompat.START /*8388611*/:
                this.mLockModeStart = lockMode;
                break;
            case android.support.v4.view.GravityCompat.END /*8388613*/:
                this.mLockModeEnd = lockMode;
                break;
        }
        if (lockMode != 0) {
            (absGravity == 3 ? this.mLeftDragger : this.mRightDragger).cancel();
        }
        switch (lockMode) {
            case 1:
                android.view.View toClose = findDrawerWithGravity(absGravity);
                if (toClose != null) {
                    closeDrawer(toClose);
                    return;
                }
                return;
            case 2:
                android.view.View toOpen = findDrawerWithGravity(absGravity);
                if (toOpen != null) {
                    openDrawer(toOpen);
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void setDrawerLockMode(int lockMode, android.view.View drawerView) {
        if (!isDrawerView(drawerView)) {
            throw new java.lang.IllegalArgumentException("View " + drawerView + " is not a " + "drawer with appropriate layout_gravity");
        }
        setDrawerLockMode(lockMode, ((android.support.v4.widget.DrawerLayout.LayoutParams) drawerView.getLayoutParams()).gravity);
    }

    public int getDrawerLockMode(int edgeGravity) {
        int layoutDirection = android.support.v4.view.ViewCompat.getLayoutDirection(this);
        switch (edgeGravity) {
            case 3:
                if (this.mLockModeLeft != 3) {
                    return this.mLockModeLeft;
                }
                int leftLockMode = layoutDirection == 0 ? this.mLockModeStart : this.mLockModeEnd;
                if (leftLockMode != 3) {
                    return leftLockMode;
                }
                break;
            case 5:
                if (this.mLockModeRight != 3) {
                    return this.mLockModeRight;
                }
                int rightLockMode = layoutDirection == 0 ? this.mLockModeEnd : this.mLockModeStart;
                if (rightLockMode != 3) {
                    return rightLockMode;
                }
                break;
            case android.support.v4.view.GravityCompat.START /*8388611*/:
                if (this.mLockModeStart != 3) {
                    return this.mLockModeStart;
                }
                int startLockMode = layoutDirection == 0 ? this.mLockModeLeft : this.mLockModeRight;
                if (startLockMode != 3) {
                    return startLockMode;
                }
                break;
            case android.support.v4.view.GravityCompat.END /*8388613*/:
                if (this.mLockModeEnd != 3) {
                    return this.mLockModeEnd;
                }
                int endLockMode = layoutDirection == 0 ? this.mLockModeRight : this.mLockModeLeft;
                if (endLockMode != 3) {
                    return endLockMode;
                }
                break;
        }
        return 0;
    }

    public int getDrawerLockMode(android.view.View drawerView) {
        if (isDrawerView(drawerView)) {
            return getDrawerLockMode(((android.support.v4.widget.DrawerLayout.LayoutParams) drawerView.getLayoutParams()).gravity);
        }
        throw new java.lang.IllegalArgumentException("View " + drawerView + " is not a drawer");
    }

    public void setDrawerTitle(int edgeGravity, java.lang.CharSequence title) {
        int absGravity = android.support.v4.view.GravityCompat.getAbsoluteGravity(edgeGravity, android.support.v4.view.ViewCompat.getLayoutDirection(this));
        if (absGravity == 3) {
            this.mTitleLeft = title;
        } else if (absGravity == 5) {
            this.mTitleRight = title;
        }
    }

    @android.support.annotation.Nullable
    public java.lang.CharSequence getDrawerTitle(int edgeGravity) {
        int absGravity = android.support.v4.view.GravityCompat.getAbsoluteGravity(edgeGravity, android.support.v4.view.ViewCompat.getLayoutDirection(this));
        if (absGravity == 3) {
            return this.mTitleLeft;
        }
        if (absGravity == 5) {
            return this.mTitleRight;
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    public void updateDrawerState(int forGravity, int activeState, android.view.View activeDrawer) {
        int state;
        int leftState = this.mLeftDragger.getViewDragState();
        int rightState = this.mRightDragger.getViewDragState();
        if (leftState == 1 || rightState == 1) {
            state = 1;
        } else if (leftState == 2 || rightState == 2) {
            state = 2;
        } else {
            state = 0;
        }
        if (activeDrawer != null && activeState == 0) {
            android.support.v4.widget.DrawerLayout.LayoutParams lp = (android.support.v4.widget.DrawerLayout.LayoutParams) activeDrawer.getLayoutParams();
            if (lp.onScreen == 0.0f) {
                dispatchOnDrawerClosed(activeDrawer);
            } else if (lp.onScreen == TOUCH_SLOP_SENSITIVITY) {
                dispatchOnDrawerOpened(activeDrawer);
            }
        }
        if (state != this.mDrawerState) {
            this.mDrawerState = state;
            if (this.mListeners != null) {
                for (int i = this.mListeners.size() - 1; i >= 0; i--) {
                    ((android.support.v4.widget.DrawerLayout.DrawerListener) this.mListeners.get(i)).onDrawerStateChanged(state);
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void dispatchOnDrawerClosed(android.view.View drawerView) {
        android.support.v4.widget.DrawerLayout.LayoutParams lp = (android.support.v4.widget.DrawerLayout.LayoutParams) drawerView.getLayoutParams();
        if ((lp.openState & 1) == 1) {
            lp.openState = 0;
            if (this.mListeners != null) {
                for (int i = this.mListeners.size() - 1; i >= 0; i--) {
                    ((android.support.v4.widget.DrawerLayout.DrawerListener) this.mListeners.get(i)).onDrawerClosed(drawerView);
                }
            }
            updateChildrenImportantForAccessibility(drawerView, false);
            if (hasWindowFocus()) {
                android.view.View rootView = getRootView();
                if (rootView != null) {
                    rootView.sendAccessibilityEvent(32);
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void dispatchOnDrawerOpened(android.view.View drawerView) {
        android.support.v4.widget.DrawerLayout.LayoutParams lp = (android.support.v4.widget.DrawerLayout.LayoutParams) drawerView.getLayoutParams();
        if ((lp.openState & 1) == 0) {
            lp.openState = 1;
            if (this.mListeners != null) {
                for (int i = this.mListeners.size() - 1; i >= 0; i--) {
                    ((android.support.v4.widget.DrawerLayout.DrawerListener) this.mListeners.get(i)).onDrawerOpened(drawerView);
                }
            }
            updateChildrenImportantForAccessibility(drawerView, CHILDREN_DISALLOW_INTERCEPT);
            if (hasWindowFocus()) {
                sendAccessibilityEvent(32);
            }
        }
    }

    private void updateChildrenImportantForAccessibility(android.view.View drawerView, boolean isDrawerOpen) {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            android.view.View child = getChildAt(i);
            if ((isDrawerOpen || isDrawerView(child)) && (!isDrawerOpen || child != drawerView)) {
                android.support.v4.view.ViewCompat.setImportantForAccessibility(child, 4);
            } else {
                android.support.v4.view.ViewCompat.setImportantForAccessibility(child, 1);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void dispatchOnDrawerSlide(android.view.View drawerView, float slideOffset) {
        if (this.mListeners != null) {
            for (int i = this.mListeners.size() - 1; i >= 0; i--) {
                ((android.support.v4.widget.DrawerLayout.DrawerListener) this.mListeners.get(i)).onDrawerSlide(drawerView, slideOffset);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void setDrawerViewOffset(android.view.View drawerView, float slideOffset) {
        android.support.v4.widget.DrawerLayout.LayoutParams lp = (android.support.v4.widget.DrawerLayout.LayoutParams) drawerView.getLayoutParams();
        if (slideOffset != lp.onScreen) {
            lp.onScreen = slideOffset;
            dispatchOnDrawerSlide(drawerView, slideOffset);
        }
    }

    /* access modifiers changed from: 0000 */
    public float getDrawerViewOffset(android.view.View drawerView) {
        return ((android.support.v4.widget.DrawerLayout.LayoutParams) drawerView.getLayoutParams()).onScreen;
    }

    /* access modifiers changed from: 0000 */
    public int getDrawerViewAbsoluteGravity(android.view.View drawerView) {
        return android.support.v4.view.GravityCompat.getAbsoluteGravity(((android.support.v4.widget.DrawerLayout.LayoutParams) drawerView.getLayoutParams()).gravity, android.support.v4.view.ViewCompat.getLayoutDirection(this));
    }

    /* access modifiers changed from: 0000 */
    public boolean checkDrawerViewAbsoluteGravity(android.view.View drawerView, int checkFor) {
        if ((getDrawerViewAbsoluteGravity(drawerView) & checkFor) == checkFor) {
            return CHILDREN_DISALLOW_INTERCEPT;
        }
        return false;
    }

    /* access modifiers changed from: 0000 */
    public android.view.View findOpenDrawer() {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            android.view.View child = getChildAt(i);
            if ((((android.support.v4.widget.DrawerLayout.LayoutParams) child.getLayoutParams()).openState & 1) == 1) {
                return child;
            }
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    public void moveDrawerToOffset(android.view.View drawerView, float slideOffset) {
        float oldOffset = getDrawerViewOffset(drawerView);
        int width = drawerView.getWidth();
        int dx = ((int) (((float) width) * slideOffset)) - ((int) (((float) width) * oldOffset));
        if (!checkDrawerViewAbsoluteGravity(drawerView, 3)) {
            dx = -dx;
        }
        drawerView.offsetLeftAndRight(dx);
        setDrawerViewOffset(drawerView, slideOffset);
    }

    /* access modifiers changed from: 0000 */
    public android.view.View findDrawerWithGravity(int gravity) {
        int absHorizGravity = android.support.v4.view.GravityCompat.getAbsoluteGravity(gravity, android.support.v4.view.ViewCompat.getLayoutDirection(this)) & 7;
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            android.view.View child = getChildAt(i);
            if ((getDrawerViewAbsoluteGravity(child) & 7) == absHorizGravity) {
                return child;
            }
        }
        return null;
    }

    static java.lang.String gravityToString(int gravity) {
        if ((gravity & 3) == 3) {
            return "LEFT";
        }
        if ((gravity & 5) == 5) {
            return "RIGHT";
        }
        return java.lang.Integer.toHexString(gravity);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.mFirstLayout = CHILDREN_DISALLOW_INTERCEPT;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.mFirstLayout = CHILDREN_DISALLOW_INTERCEPT;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int widthMode = android.view.View.MeasureSpec.getMode(widthMeasureSpec);
        int heightMode = android.view.View.MeasureSpec.getMode(heightMeasureSpec);
        int widthSize = android.view.View.MeasureSpec.getSize(widthMeasureSpec);
        int heightSize = android.view.View.MeasureSpec.getSize(heightMeasureSpec);
        if (!(widthMode == 1073741824 && heightMode == 1073741824)) {
            if (isInEditMode()) {
                if (widthMode != Integer.MIN_VALUE) {
                    if (widthMode == 0) {
                        widthSize = 300;
                    }
                }
                if (heightMode != Integer.MIN_VALUE) {
                    if (heightMode == 0) {
                        heightSize = 300;
                    }
                }
            } else {
                throw new java.lang.IllegalArgumentException("DrawerLayout must be measured with MeasureSpec.EXACTLY.");
            }
        }
        setMeasuredDimension(widthSize, heightSize);
        boolean applyInsets = (this.mLastInsets == null || !android.support.v4.view.ViewCompat.getFitsSystemWindows(this)) ? false : CHILDREN_DISALLOW_INTERCEPT;
        int layoutDirection = android.support.v4.view.ViewCompat.getLayoutDirection(this);
        boolean hasDrawerOnLeftEdge = false;
        boolean hasDrawerOnRightEdge = false;
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            android.view.View child = getChildAt(i);
            if (child.getVisibility() != 8) {
                android.support.v4.widget.DrawerLayout.LayoutParams lp = (android.support.v4.widget.DrawerLayout.LayoutParams) child.getLayoutParams();
                if (applyInsets) {
                    int cgrav = android.support.v4.view.GravityCompat.getAbsoluteGravity(lp.gravity, layoutDirection);
                    if (android.support.v4.view.ViewCompat.getFitsSystemWindows(child)) {
                        IMPL.dispatchChildInsets(child, this.mLastInsets, cgrav);
                    } else {
                        IMPL.applyMarginInsets(lp, this.mLastInsets, cgrav);
                    }
                }
                if (isContentView(child)) {
                    child.measure(android.view.View.MeasureSpec.makeMeasureSpec((widthSize - lp.leftMargin) - lp.rightMargin, 1073741824), android.view.View.MeasureSpec.makeMeasureSpec((heightSize - lp.topMargin) - lp.bottomMargin, 1073741824));
                } else if (isDrawerView(child)) {
                    if (SET_DRAWER_SHADOW_FROM_ELEVATION && android.support.v4.view.ViewCompat.getElevation(child) != this.mDrawerElevation) {
                        android.support.v4.view.ViewCompat.setElevation(child, this.mDrawerElevation);
                    }
                    int childGravity = getDrawerViewAbsoluteGravity(child) & 7;
                    boolean isLeftEdgeDrawer = childGravity == 3 ? CHILDREN_DISALLOW_INTERCEPT : false;
                    if ((!isLeftEdgeDrawer || !hasDrawerOnLeftEdge) && (isLeftEdgeDrawer || !hasDrawerOnRightEdge)) {
                        if (isLeftEdgeDrawer) {
                            hasDrawerOnLeftEdge = CHILDREN_DISALLOW_INTERCEPT;
                        } else {
                            hasDrawerOnRightEdge = CHILDREN_DISALLOW_INTERCEPT;
                        }
                        child.measure(getChildMeasureSpec(widthMeasureSpec, this.mMinDrawerMargin + lp.leftMargin + lp.rightMargin, lp.width), getChildMeasureSpec(heightMeasureSpec, lp.topMargin + lp.bottomMargin, lp.height));
                    } else {
                        throw new java.lang.IllegalStateException("Child drawer has absolute gravity " + gravityToString(childGravity) + " but this " + TAG + " already has a " + "drawer view along that edge");
                    }
                } else {
                    throw new java.lang.IllegalStateException("Child " + child + " at index " + i + " does not have a valid layout_gravity - must be Gravity.LEFT, " + "Gravity.RIGHT or Gravity.NO_GRAVITY");
                }
            }
        }
    }

    private void resolveShadowDrawables() {
        if (!SET_DRAWER_SHADOW_FROM_ELEVATION) {
            this.mShadowLeftResolved = resolveLeftShadow();
            this.mShadowRightResolved = resolveRightShadow();
        }
    }

    private android.graphics.drawable.Drawable resolveLeftShadow() {
        int layoutDirection = android.support.v4.view.ViewCompat.getLayoutDirection(this);
        if (layoutDirection == 0) {
            if (this.mShadowStart != null) {
                mirror(this.mShadowStart, layoutDirection);
                return this.mShadowStart;
            }
        } else if (this.mShadowEnd != null) {
            mirror(this.mShadowEnd, layoutDirection);
            return this.mShadowEnd;
        }
        return this.mShadowLeft;
    }

    private android.graphics.drawable.Drawable resolveRightShadow() {
        int layoutDirection = android.support.v4.view.ViewCompat.getLayoutDirection(this);
        if (layoutDirection == 0) {
            if (this.mShadowEnd != null) {
                mirror(this.mShadowEnd, layoutDirection);
                return this.mShadowEnd;
            }
        } else if (this.mShadowStart != null) {
            mirror(this.mShadowStart, layoutDirection);
            return this.mShadowStart;
        }
        return this.mShadowRight;
    }

    private boolean mirror(android.graphics.drawable.Drawable drawable, int layoutDirection) {
        if (drawable == null || !android.support.v4.graphics.drawable.DrawableCompat.isAutoMirrored(drawable)) {
            return false;
        }
        android.support.v4.graphics.drawable.DrawableCompat.setLayoutDirection(drawable, layoutDirection);
        return CHILDREN_DISALLOW_INTERCEPT;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean changed, int l, int t, int r, int b) {
        int childLeft;
        float newOffset;
        this.mInLayout = CHILDREN_DISALLOW_INTERCEPT;
        int width = r - l;
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            android.view.View child = getChildAt(i);
            if (child.getVisibility() != 8) {
                android.support.v4.widget.DrawerLayout.LayoutParams lp = (android.support.v4.widget.DrawerLayout.LayoutParams) child.getLayoutParams();
                if (isContentView(child)) {
                    child.layout(lp.leftMargin, lp.topMargin, lp.leftMargin + child.getMeasuredWidth(), lp.topMargin + child.getMeasuredHeight());
                } else {
                    int childWidth = child.getMeasuredWidth();
                    int childHeight = child.getMeasuredHeight();
                    if (checkDrawerViewAbsoluteGravity(child, 3)) {
                        childLeft = (-childWidth) + ((int) (((float) childWidth) * lp.onScreen));
                        newOffset = ((float) (childWidth + childLeft)) / ((float) childWidth);
                    } else {
                        childLeft = width - ((int) (((float) childWidth) * lp.onScreen));
                        newOffset = ((float) (width - childLeft)) / ((float) childWidth);
                    }
                    boolean changeOffset = newOffset != lp.onScreen ? CHILDREN_DISALLOW_INTERCEPT : false;
                    switch (lp.gravity & ch.qos.logback.core.net.SyslogConstants.LOG_ALERT) {
                        case 16:
                            int height = b - t;
                            int childTop = (height - childHeight) / 2;
                            if (childTop < lp.topMargin) {
                                childTop = lp.topMargin;
                            } else {
                                if (childTop + childHeight > height - lp.bottomMargin) {
                                    childTop = (height - lp.bottomMargin) - childHeight;
                                }
                            }
                            child.layout(childLeft, childTop, childLeft + childWidth, childTop + childHeight);
                            break;
                        case ch.qos.logback.core.net.SyslogConstants.LOG_AUTHPRIV /*80*/:
                            int height2 = b - t;
                            child.layout(childLeft, (height2 - lp.bottomMargin) - child.getMeasuredHeight(), childLeft + childWidth, height2 - lp.bottomMargin);
                            break;
                        default:
                            child.layout(childLeft, lp.topMargin, childLeft + childWidth, lp.topMargin + childHeight);
                            break;
                    }
                    if (changeOffset) {
                        setDrawerViewOffset(child, newOffset);
                    }
                    int newVisibility = lp.onScreen > 0.0f ? 0 : 4;
                    if (child.getVisibility() != newVisibility) {
                        child.setVisibility(newVisibility);
                    }
                }
            }
        }
        this.mInLayout = false;
        this.mFirstLayout = false;
    }

    public void requestLayout() {
        if (!this.mInLayout) {
            super.requestLayout();
        }
    }

    public void computeScroll() {
        int childCount = getChildCount();
        float scrimOpacity = 0.0f;
        for (int i = 0; i < childCount; i++) {
            scrimOpacity = java.lang.Math.max(scrimOpacity, ((android.support.v4.widget.DrawerLayout.LayoutParams) getChildAt(i).getLayoutParams()).onScreen);
        }
        this.mScrimOpacity = scrimOpacity;
        if (this.mLeftDragger.continueSettling(CHILDREN_DISALLOW_INTERCEPT) || this.mRightDragger.continueSettling(CHILDREN_DISALLOW_INTERCEPT)) {
            android.support.v4.view.ViewCompat.postInvalidateOnAnimation(this);
        }
    }

    private static boolean hasOpaqueBackground(android.view.View v) {
        android.graphics.drawable.Drawable bg = v.getBackground();
        if (bg == null || bg.getOpacity() != -1) {
            return false;
        }
        return CHILDREN_DISALLOW_INTERCEPT;
    }

    public void setStatusBarBackground(android.graphics.drawable.Drawable bg) {
        this.mStatusBarBackground = bg;
        invalidate();
    }

    public android.graphics.drawable.Drawable getStatusBarBackgroundDrawable() {
        return this.mStatusBarBackground;
    }

    public void setStatusBarBackground(int resId) {
        this.mStatusBarBackground = resId != 0 ? android.support.v4.content.ContextCompat.getDrawable(getContext(), resId) : null;
        invalidate();
    }

    public void setStatusBarBackgroundColor(@android.support.annotation.ColorInt int color) {
        this.mStatusBarBackground = new android.graphics.drawable.ColorDrawable(color);
        invalidate();
    }

    public void onRtlPropertiesChanged(int layoutDirection) {
        resolveShadowDrawables();
    }

    public void onDraw(android.graphics.Canvas c) {
        super.onDraw(c);
        if (this.mDrawStatusBarBackground && this.mStatusBarBackground != null) {
            int inset = IMPL.getTopInset(this.mLastInsets);
            if (inset > 0) {
                this.mStatusBarBackground.setBounds(0, 0, getWidth(), inset);
                this.mStatusBarBackground.draw(c);
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean drawChild(android.graphics.Canvas canvas, android.view.View child, long drawingTime) {
        int height = getHeight();
        boolean drawingContent = isContentView(child);
        int clipLeft = 0;
        int clipRight = getWidth();
        int restoreCount = canvas.save();
        if (drawingContent) {
            int childCount = getChildCount();
            for (int i = 0; i < childCount; i++) {
                android.view.View v = getChildAt(i);
                if (v != child && v.getVisibility() == 0 && hasOpaqueBackground(v) && isDrawerView(v) && v.getHeight() >= height) {
                    if (checkDrawerViewAbsoluteGravity(v, 3)) {
                        int vright = v.getRight();
                        if (vright > clipLeft) {
                            clipLeft = vright;
                        }
                    } else {
                        int vleft = v.getLeft();
                        if (vleft < clipRight) {
                            clipRight = vleft;
                        }
                    }
                }
            }
            canvas.clipRect(clipLeft, 0, clipRight, getHeight());
        }
        boolean result = super.drawChild(canvas, child, drawingTime);
        canvas.restoreToCount(restoreCount);
        if (this.mScrimOpacity > 0.0f && drawingContent) {
            this.mScrimPaint.setColor((((int) (((float) ((this.mScrimColor & android.support.v4.view.ViewCompat.MEASURED_STATE_MASK) >>> 24)) * this.mScrimOpacity)) << 24) | (this.mScrimColor & android.support.v4.view.ViewCompat.MEASURED_SIZE_MASK));
            canvas.drawRect((float) clipLeft, 0.0f, (float) clipRight, (float) getHeight(), this.mScrimPaint);
        } else if (this.mShadowLeftResolved != null && checkDrawerViewAbsoluteGravity(child, 3)) {
            int shadowWidth = this.mShadowLeftResolved.getIntrinsicWidth();
            int childRight = child.getRight();
            float alpha = java.lang.Math.max(0.0f, java.lang.Math.min(((float) childRight) / ((float) this.mLeftDragger.getEdgeSize()), TOUCH_SLOP_SENSITIVITY));
            this.mShadowLeftResolved.setBounds(childRight, child.getTop(), childRight + shadowWidth, child.getBottom());
            this.mShadowLeftResolved.setAlpha((int) (255.0f * alpha));
            this.mShadowLeftResolved.draw(canvas);
        } else if (this.mShadowRightResolved != null && checkDrawerViewAbsoluteGravity(child, 5)) {
            int shadowWidth2 = this.mShadowRightResolved.getIntrinsicWidth();
            int childLeft = child.getLeft();
            float alpha2 = java.lang.Math.max(0.0f, java.lang.Math.min(((float) (getWidth() - childLeft)) / ((float) this.mRightDragger.getEdgeSize()), TOUCH_SLOP_SENSITIVITY));
            this.mShadowRightResolved.setBounds(childLeft - shadowWidth2, child.getTop(), childLeft, child.getBottom());
            this.mShadowRightResolved.setAlpha((int) (255.0f * alpha2));
            this.mShadowRightResolved.draw(canvas);
        }
        return result;
    }

    /* access modifiers changed from: 0000 */
    public boolean isContentView(android.view.View child) {
        if (((android.support.v4.widget.DrawerLayout.LayoutParams) child.getLayoutParams()).gravity == 0) {
            return CHILDREN_DISALLOW_INTERCEPT;
        }
        return false;
    }

    /* access modifiers changed from: 0000 */
    public boolean isDrawerView(android.view.View child) {
        int absGravity = android.support.v4.view.GravityCompat.getAbsoluteGravity(((android.support.v4.widget.DrawerLayout.LayoutParams) child.getLayoutParams()).gravity, android.support.v4.view.ViewCompat.getLayoutDirection(child));
        if ((absGravity & 3) != 0) {
            return CHILDREN_DISALLOW_INTERCEPT;
        }
        if ((absGravity & 5) != 0) {
            return CHILDREN_DISALLOW_INTERCEPT;
        }
        return false;
    }

    public boolean onInterceptTouchEvent(android.view.MotionEvent ev) {
        boolean interceptForDrag = this.mLeftDragger.shouldInterceptTouchEvent(ev) | this.mRightDragger.shouldInterceptTouchEvent(ev);
        boolean interceptForTap = false;
        switch (android.support.v4.view.MotionEventCompat.getActionMasked(ev)) {
            case 0:
                float x = ev.getX();
                float y = ev.getY();
                this.mInitialMotionX = x;
                this.mInitialMotionY = y;
                if (this.mScrimOpacity > 0.0f) {
                    android.view.View child = this.mLeftDragger.findTopChildUnder((int) x, (int) y);
                    if (child != null && isContentView(child)) {
                        interceptForTap = CHILDREN_DISALLOW_INTERCEPT;
                    }
                }
                this.mDisallowInterceptRequested = false;
                this.mChildrenCanceledTouch = false;
                break;
            case 1:
            case 3:
                closeDrawers(CHILDREN_DISALLOW_INTERCEPT);
                this.mDisallowInterceptRequested = false;
                this.mChildrenCanceledTouch = false;
                break;
            case 2:
                if (this.mLeftDragger.checkTouchSlop(3)) {
                    this.mLeftCallback.removeCallbacks();
                    this.mRightCallback.removeCallbacks();
                    break;
                }
                break;
        }
        if (interceptForDrag || interceptForTap || hasPeekingDrawer() || this.mChildrenCanceledTouch) {
            return CHILDREN_DISALLOW_INTERCEPT;
        }
        return false;
    }

    public boolean onTouchEvent(android.view.MotionEvent ev) {
        this.mLeftDragger.processTouchEvent(ev);
        this.mRightDragger.processTouchEvent(ev);
        switch (ev.getAction() & 255) {
            case 0:
                float x = ev.getX();
                float y = ev.getY();
                this.mInitialMotionX = x;
                this.mInitialMotionY = y;
                this.mDisallowInterceptRequested = false;
                this.mChildrenCanceledTouch = false;
                break;
            case 1:
                float x2 = ev.getX();
                float y2 = ev.getY();
                boolean peekingOnly = CHILDREN_DISALLOW_INTERCEPT;
                android.view.View touchedView = this.mLeftDragger.findTopChildUnder((int) x2, (int) y2);
                if (touchedView != null && isContentView(touchedView)) {
                    float dx = x2 - this.mInitialMotionX;
                    float dy = y2 - this.mInitialMotionY;
                    int slop = this.mLeftDragger.getTouchSlop();
                    if ((dx * dx) + (dy * dy) < ((float) (slop * slop))) {
                        android.view.View openDrawer = findOpenDrawer();
                        if (openDrawer != null) {
                            peekingOnly = getDrawerLockMode(openDrawer) == 2 ? CHILDREN_DISALLOW_INTERCEPT : false;
                        }
                    }
                }
                closeDrawers(peekingOnly);
                this.mDisallowInterceptRequested = false;
                break;
            case 3:
                closeDrawers(CHILDREN_DISALLOW_INTERCEPT);
                this.mDisallowInterceptRequested = false;
                this.mChildrenCanceledTouch = false;
                break;
        }
        return CHILDREN_DISALLOW_INTERCEPT;
    }

    public void requestDisallowInterceptTouchEvent(boolean disallowIntercept) {
        super.requestDisallowInterceptTouchEvent(disallowIntercept);
        this.mDisallowInterceptRequested = disallowIntercept;
        if (disallowIntercept) {
            closeDrawers(CHILDREN_DISALLOW_INTERCEPT);
        }
    }

    public void closeDrawers() {
        closeDrawers(false);
    }

    /* access modifiers changed from: 0000 */
    public void closeDrawers(boolean peekingOnly) {
        boolean needsInvalidate = false;
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            android.view.View child = getChildAt(i);
            android.support.v4.widget.DrawerLayout.LayoutParams lp = (android.support.v4.widget.DrawerLayout.LayoutParams) child.getLayoutParams();
            if (isDrawerView(child) && (!peekingOnly || lp.isPeeking)) {
                int childWidth = child.getWidth();
                if (checkDrawerViewAbsoluteGravity(child, 3)) {
                    needsInvalidate |= this.mLeftDragger.smoothSlideViewTo(child, -childWidth, child.getTop());
                } else {
                    needsInvalidate |= this.mRightDragger.smoothSlideViewTo(child, getWidth(), child.getTop());
                }
                lp.isPeeking = false;
            }
        }
        this.mLeftCallback.removeCallbacks();
        this.mRightCallback.removeCallbacks();
        if (needsInvalidate) {
            invalidate();
        }
    }

    public void openDrawer(android.view.View drawerView) {
        openDrawer(drawerView, (boolean) CHILDREN_DISALLOW_INTERCEPT);
    }

    public void openDrawer(android.view.View drawerView, boolean animate) {
        if (!isDrawerView(drawerView)) {
            throw new java.lang.IllegalArgumentException("View " + drawerView + " is not a sliding drawer");
        }
        android.support.v4.widget.DrawerLayout.LayoutParams lp = (android.support.v4.widget.DrawerLayout.LayoutParams) drawerView.getLayoutParams();
        if (this.mFirstLayout) {
            lp.onScreen = TOUCH_SLOP_SENSITIVITY;
            lp.openState = 1;
            updateChildrenImportantForAccessibility(drawerView, CHILDREN_DISALLOW_INTERCEPT);
        } else if (animate) {
            lp.openState |= 2;
            if (checkDrawerViewAbsoluteGravity(drawerView, 3)) {
                this.mLeftDragger.smoothSlideViewTo(drawerView, 0, drawerView.getTop());
            } else {
                this.mRightDragger.smoothSlideViewTo(drawerView, getWidth() - drawerView.getWidth(), drawerView.getTop());
            }
        } else {
            moveDrawerToOffset(drawerView, TOUCH_SLOP_SENSITIVITY);
            updateDrawerState(lp.gravity, 0, drawerView);
            drawerView.setVisibility(0);
        }
        invalidate();
    }

    public void openDrawer(int gravity) {
        openDrawer(gravity, (boolean) CHILDREN_DISALLOW_INTERCEPT);
    }

    public void openDrawer(int gravity, boolean animate) {
        android.view.View drawerView = findDrawerWithGravity(gravity);
        if (drawerView == null) {
            throw new java.lang.IllegalArgumentException("No drawer view found with gravity " + gravityToString(gravity));
        }
        openDrawer(drawerView, animate);
    }

    public void closeDrawer(android.view.View drawerView) {
        closeDrawer(drawerView, (boolean) CHILDREN_DISALLOW_INTERCEPT);
    }

    public void closeDrawer(android.view.View drawerView, boolean animate) {
        if (!isDrawerView(drawerView)) {
            throw new java.lang.IllegalArgumentException("View " + drawerView + " is not a sliding drawer");
        }
        android.support.v4.widget.DrawerLayout.LayoutParams lp = (android.support.v4.widget.DrawerLayout.LayoutParams) drawerView.getLayoutParams();
        if (this.mFirstLayout) {
            lp.onScreen = 0.0f;
            lp.openState = 0;
        } else if (animate) {
            lp.openState |= 4;
            if (checkDrawerViewAbsoluteGravity(drawerView, 3)) {
                this.mLeftDragger.smoothSlideViewTo(drawerView, -drawerView.getWidth(), drawerView.getTop());
            } else {
                this.mRightDragger.smoothSlideViewTo(drawerView, getWidth(), drawerView.getTop());
            }
        } else {
            moveDrawerToOffset(drawerView, 0.0f);
            updateDrawerState(lp.gravity, 0, drawerView);
            drawerView.setVisibility(4);
        }
        invalidate();
    }

    public void closeDrawer(int gravity) {
        closeDrawer(gravity, (boolean) CHILDREN_DISALLOW_INTERCEPT);
    }

    public void closeDrawer(int gravity, boolean animate) {
        android.view.View drawerView = findDrawerWithGravity(gravity);
        if (drawerView == null) {
            throw new java.lang.IllegalArgumentException("No drawer view found with gravity " + gravityToString(gravity));
        }
        closeDrawer(drawerView, animate);
    }

    public boolean isDrawerOpen(android.view.View drawer) {
        if (!isDrawerView(drawer)) {
            throw new java.lang.IllegalArgumentException("View " + drawer + " is not a drawer");
        } else if ((((android.support.v4.widget.DrawerLayout.LayoutParams) drawer.getLayoutParams()).openState & 1) == 1) {
            return CHILDREN_DISALLOW_INTERCEPT;
        } else {
            return false;
        }
    }

    public boolean isDrawerOpen(int drawerGravity) {
        android.view.View drawerView = findDrawerWithGravity(drawerGravity);
        if (drawerView != null) {
            return isDrawerOpen(drawerView);
        }
        return false;
    }

    public boolean isDrawerVisible(android.view.View drawer) {
        if (!isDrawerView(drawer)) {
            throw new java.lang.IllegalArgumentException("View " + drawer + " is not a drawer");
        } else if (((android.support.v4.widget.DrawerLayout.LayoutParams) drawer.getLayoutParams()).onScreen > 0.0f) {
            return CHILDREN_DISALLOW_INTERCEPT;
        } else {
            return false;
        }
    }

    public boolean isDrawerVisible(int drawerGravity) {
        android.view.View drawerView = findDrawerWithGravity(drawerGravity);
        if (drawerView != null) {
            return isDrawerVisible(drawerView);
        }
        return false;
    }

    private boolean hasPeekingDrawer() {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            if (((android.support.v4.widget.DrawerLayout.LayoutParams) getChildAt(i).getLayoutParams()).isPeeking) {
                return CHILDREN_DISALLOW_INTERCEPT;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public android.view.ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new android.support.v4.widget.DrawerLayout.LayoutParams(-1, -1);
    }

    /* access modifiers changed from: protected */
    public android.view.ViewGroup.LayoutParams generateLayoutParams(android.view.ViewGroup.LayoutParams p) {
        if (p instanceof android.support.v4.widget.DrawerLayout.LayoutParams) {
            return new android.support.v4.widget.DrawerLayout.LayoutParams((android.support.v4.widget.DrawerLayout.LayoutParams) p);
        }
        return p instanceof android.view.ViewGroup.MarginLayoutParams ? new android.support.v4.widget.DrawerLayout.LayoutParams((android.view.ViewGroup.MarginLayoutParams) p) : new android.support.v4.widget.DrawerLayout.LayoutParams(p);
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(android.view.ViewGroup.LayoutParams p) {
        if (!(p instanceof android.support.v4.widget.DrawerLayout.LayoutParams) || !super.checkLayoutParams(p)) {
            return false;
        }
        return CHILDREN_DISALLOW_INTERCEPT;
    }

    public android.view.ViewGroup.LayoutParams generateLayoutParams(android.util.AttributeSet attrs) {
        return new android.support.v4.widget.DrawerLayout.LayoutParams(getContext(), attrs);
    }

    public void addFocusables(java.util.ArrayList<android.view.View> views, int direction, int focusableMode) {
        if (getDescendantFocusability() != 393216) {
            int childCount = getChildCount();
            boolean isDrawerOpen = false;
            for (int i = 0; i < childCount; i++) {
                android.view.View child = getChildAt(i);
                if (!isDrawerView(child)) {
                    this.mNonDrawerViews.add(child);
                } else if (isDrawerOpen(child)) {
                    isDrawerOpen = CHILDREN_DISALLOW_INTERCEPT;
                    child.addFocusables(views, direction, focusableMode);
                }
            }
            if (!isDrawerOpen) {
                int nonDrawerViewsCount = this.mNonDrawerViews.size();
                for (int i2 = 0; i2 < nonDrawerViewsCount; i2++) {
                    android.view.View child2 = (android.view.View) this.mNonDrawerViews.get(i2);
                    if (child2.getVisibility() == 0) {
                        child2.addFocusables(views, direction, focusableMode);
                    }
                }
            }
            this.mNonDrawerViews.clear();
        }
    }

    private boolean hasVisibleDrawer() {
        if (findVisibleDrawer() != null) {
            return CHILDREN_DISALLOW_INTERCEPT;
        }
        return false;
    }

    /* access modifiers changed from: 0000 */
    public android.view.View findVisibleDrawer() {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            android.view.View child = getChildAt(i);
            if (isDrawerView(child) && isDrawerVisible(child)) {
                return child;
            }
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    public void cancelChildViewTouch() {
        if (!this.mChildrenCanceledTouch) {
            long now = android.os.SystemClock.uptimeMillis();
            android.view.MotionEvent cancelEvent = android.view.MotionEvent.obtain(now, now, 3, 0.0f, 0.0f, 0);
            int childCount = getChildCount();
            for (int i = 0; i < childCount; i++) {
                getChildAt(i).dispatchTouchEvent(cancelEvent);
            }
            cancelEvent.recycle();
            this.mChildrenCanceledTouch = CHILDREN_DISALLOW_INTERCEPT;
        }
    }

    public boolean onKeyDown(int keyCode, android.view.KeyEvent event) {
        if (keyCode != 4 || !hasVisibleDrawer()) {
            return super.onKeyDown(keyCode, event);
        }
        event.startTracking();
        return CHILDREN_DISALLOW_INTERCEPT;
    }

    public boolean onKeyUp(int keyCode, android.view.KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyUp(keyCode, event);
        }
        android.view.View visibleDrawer = findVisibleDrawer();
        if (visibleDrawer != null && getDrawerLockMode(visibleDrawer) == 0) {
            closeDrawers();
        }
        if (visibleDrawer != null) {
            return CHILDREN_DISALLOW_INTERCEPT;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(android.os.Parcelable state) {
        if (!(state instanceof android.support.v4.widget.DrawerLayout.SavedState)) {
            super.onRestoreInstanceState(state);
            return;
        }
        android.support.v4.widget.DrawerLayout.SavedState ss = (android.support.v4.widget.DrawerLayout.SavedState) state;
        super.onRestoreInstanceState(ss.getSuperState());
        if (ss.openDrawerGravity != 0) {
            android.view.View toOpen = findDrawerWithGravity(ss.openDrawerGravity);
            if (toOpen != null) {
                openDrawer(toOpen);
            }
        }
        if (ss.lockModeLeft != 3) {
            setDrawerLockMode(ss.lockModeLeft, 3);
        }
        if (ss.lockModeRight != 3) {
            setDrawerLockMode(ss.lockModeRight, 5);
        }
        if (ss.lockModeStart != 3) {
            setDrawerLockMode(ss.lockModeStart, (int) android.support.v4.view.GravityCompat.START);
        }
        if (ss.lockModeEnd != 3) {
            setDrawerLockMode(ss.lockModeEnd, (int) android.support.v4.view.GravityCompat.END);
        }
    }

    /* access modifiers changed from: protected */
    public android.os.Parcelable onSaveInstanceState() {
        android.support.v4.widget.DrawerLayout.LayoutParams lp;
        boolean isOpenedAndNotClosing;
        boolean isClosedAndOpening;
        android.support.v4.widget.DrawerLayout.SavedState ss = new android.support.v4.widget.DrawerLayout.SavedState(super.onSaveInstanceState());
        int childCount = getChildCount();
        int i = 0;
        while (true) {
            if (i >= childCount) {
                break;
            }
            lp = (android.support.v4.widget.DrawerLayout.LayoutParams) getChildAt(i).getLayoutParams();
            if (lp.openState == 1) {
                isOpenedAndNotClosing = true;
            } else {
                isOpenedAndNotClosing = false;
            }
            if (lp.openState == 2) {
                isClosedAndOpening = true;
            } else {
                isClosedAndOpening = false;
            }
            if (isOpenedAndNotClosing || isClosedAndOpening) {
                ss.openDrawerGravity = lp.gravity;
            } else {
                i++;
            }
        }
        ss.openDrawerGravity = lp.gravity;
        ss.lockModeLeft = this.mLockModeLeft;
        ss.lockModeRight = this.mLockModeRight;
        ss.lockModeStart = this.mLockModeStart;
        ss.lockModeEnd = this.mLockModeEnd;
        return ss;
    }

    public void addView(android.view.View child, int index, android.view.ViewGroup.LayoutParams params) {
        super.addView(child, index, params);
        if (findOpenDrawer() != null || isDrawerView(child)) {
            android.support.v4.view.ViewCompat.setImportantForAccessibility(child, 4);
        } else {
            android.support.v4.view.ViewCompat.setImportantForAccessibility(child, 1);
        }
        if (!CAN_HIDE_DESCENDANTS) {
            android.support.v4.view.ViewCompat.setAccessibilityDelegate(child, this.mChildAccessibilityDelegate);
        }
    }

    static boolean includeChildForAccessibility(android.view.View child) {
        if (android.support.v4.view.ViewCompat.getImportantForAccessibility(child) == 4 || android.support.v4.view.ViewCompat.getImportantForAccessibility(child) == 2) {
            return false;
        }
        return CHILDREN_DISALLOW_INTERCEPT;
    }
}
