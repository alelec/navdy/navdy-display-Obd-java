package android.support.v4.widget;

@android.annotation.TargetApi(16)
@android.support.annotation.RequiresApi(16)
class TextViewCompatJb {
    TextViewCompatJb() {
    }

    static int getMaxLines(android.widget.TextView textView) {
        return textView.getMaxLines();
    }

    static int getMinLines(android.widget.TextView textView) {
        return textView.getMinLines();
    }
}
