package android.support.v4.widget;

@android.annotation.TargetApi(19)
@android.support.annotation.RequiresApi(19)
class ListViewCompatKitKat {
    ListViewCompatKitKat() {
    }

    static void scrollListBy(android.widget.ListView listView, int y) {
        listView.scrollListBy(y);
    }
}
