package android.support.v4.widget;

@android.annotation.TargetApi(9)
@android.support.annotation.RequiresApi(9)
class CompoundButtonCompatGingerbread {
    private static final java.lang.String TAG = "CompoundButtonCompatGingerbread";
    private static java.lang.reflect.Field sButtonDrawableField;
    private static boolean sButtonDrawableFieldFetched;

    CompoundButtonCompatGingerbread() {
    }

    static void setButtonTintList(android.widget.CompoundButton button, android.content.res.ColorStateList tint) {
        if (button instanceof android.support.v4.widget.TintableCompoundButton) {
            ((android.support.v4.widget.TintableCompoundButton) button).setSupportButtonTintList(tint);
        }
    }

    static android.content.res.ColorStateList getButtonTintList(android.widget.CompoundButton button) {
        if (button instanceof android.support.v4.widget.TintableCompoundButton) {
            return ((android.support.v4.widget.TintableCompoundButton) button).getSupportButtonTintList();
        }
        return null;
    }

    static void setButtonTintMode(android.widget.CompoundButton button, android.graphics.PorterDuff.Mode tintMode) {
        if (button instanceof android.support.v4.widget.TintableCompoundButton) {
            ((android.support.v4.widget.TintableCompoundButton) button).setSupportButtonTintMode(tintMode);
        }
    }

    static android.graphics.PorterDuff.Mode getButtonTintMode(android.widget.CompoundButton button) {
        if (button instanceof android.support.v4.widget.TintableCompoundButton) {
            return ((android.support.v4.widget.TintableCompoundButton) button).getSupportButtonTintMode();
        }
        return null;
    }

    static android.graphics.drawable.Drawable getButtonDrawable(android.widget.CompoundButton button) {
        if (!sButtonDrawableFieldFetched) {
            try {
                sButtonDrawableField = android.widget.CompoundButton.class.getDeclaredField("mButtonDrawable");
                sButtonDrawableField.setAccessible(true);
            } catch (java.lang.NoSuchFieldException e) {
                android.util.Log.i(TAG, "Failed to retrieve mButtonDrawable field", e);
            }
            sButtonDrawableFieldFetched = true;
        }
        if (sButtonDrawableField != null) {
            try {
                return (android.graphics.drawable.Drawable) sButtonDrawableField.get(button);
            } catch (java.lang.IllegalAccessException e2) {
                android.util.Log.i(TAG, "Failed to get button drawable via reflection", e2);
                sButtonDrawableField = null;
            }
        }
        return null;
    }
}
