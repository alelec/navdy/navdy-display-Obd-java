package android.support.v4.widget;

@android.annotation.TargetApi(21)
@android.support.annotation.RequiresApi(21)
class PopupWindowCompatApi21 {
    private static final java.lang.String TAG = "PopupWindowCompatApi21";
    private static java.lang.reflect.Field sOverlapAnchorField;

    PopupWindowCompatApi21() {
    }

    static {
        try {
            sOverlapAnchorField = android.widget.PopupWindow.class.getDeclaredField("mOverlapAnchor");
            sOverlapAnchorField.setAccessible(true);
        } catch (java.lang.NoSuchFieldException e) {
            android.util.Log.i(TAG, "Could not fetch mOverlapAnchor field from PopupWindow", e);
        }
    }

    static void setOverlapAnchor(android.widget.PopupWindow popupWindow, boolean overlapAnchor) {
        if (sOverlapAnchorField != null) {
            try {
                sOverlapAnchorField.set(popupWindow, java.lang.Boolean.valueOf(overlapAnchor));
            } catch (java.lang.IllegalAccessException e) {
                android.util.Log.i(TAG, "Could not set overlap anchor field in PopupWindow", e);
            }
        }
    }

    static boolean getOverlapAnchor(android.widget.PopupWindow popupWindow) {
        if (sOverlapAnchorField != null) {
            try {
                return ((java.lang.Boolean) sOverlapAnchorField.get(popupWindow)).booleanValue();
            } catch (java.lang.IllegalAccessException e) {
                android.util.Log.i(TAG, "Could not get overlap anchor field in PopupWindow", e);
            }
        }
        return false;
    }
}
