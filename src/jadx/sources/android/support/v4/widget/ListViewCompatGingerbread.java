package android.support.v4.widget;

@android.annotation.TargetApi(9)
@android.support.annotation.RequiresApi(9)
class ListViewCompatGingerbread {
    ListViewCompatGingerbread() {
    }

    static void scrollListBy(android.widget.ListView listView, int y) {
        int firstPosition = listView.getFirstVisiblePosition();
        if (firstPosition != -1) {
            android.view.View firstView = listView.getChildAt(0);
            if (firstView != null) {
                listView.setSelectionFromTop(firstPosition, firstView.getTop() - y);
            }
        }
    }
}
