package android.support.v4.widget;

public final class TextViewCompat {
    static final android.support.v4.widget.TextViewCompat.TextViewCompatImpl IMPL;

    static class Api23TextViewCompatImpl extends android.support.v4.widget.TextViewCompat.JbMr2TextViewCompatImpl {
        Api23TextViewCompatImpl() {
        }

        public void setTextAppearance(@android.support.annotation.NonNull android.widget.TextView textView, @android.support.annotation.StyleRes int resId) {
            android.support.v4.widget.TextViewCompatApi23.setTextAppearance(textView, resId);
        }
    }

    static class BaseTextViewCompatImpl implements android.support.v4.widget.TextViewCompat.TextViewCompatImpl {
        BaseTextViewCompatImpl() {
        }

        public void setCompoundDrawablesRelative(@android.support.annotation.NonNull android.widget.TextView textView, @android.support.annotation.Nullable android.graphics.drawable.Drawable start, @android.support.annotation.Nullable android.graphics.drawable.Drawable top, @android.support.annotation.Nullable android.graphics.drawable.Drawable end, @android.support.annotation.Nullable android.graphics.drawable.Drawable bottom) {
            textView.setCompoundDrawables(start, top, end, bottom);
        }

        public void setCompoundDrawablesRelativeWithIntrinsicBounds(@android.support.annotation.NonNull android.widget.TextView textView, @android.support.annotation.Nullable android.graphics.drawable.Drawable start, @android.support.annotation.Nullable android.graphics.drawable.Drawable top, @android.support.annotation.Nullable android.graphics.drawable.Drawable end, @android.support.annotation.Nullable android.graphics.drawable.Drawable bottom) {
            textView.setCompoundDrawablesWithIntrinsicBounds(start, top, end, bottom);
        }

        public void setCompoundDrawablesRelativeWithIntrinsicBounds(@android.support.annotation.NonNull android.widget.TextView textView, @android.support.annotation.DrawableRes int start, @android.support.annotation.DrawableRes int top, @android.support.annotation.DrawableRes int end, @android.support.annotation.DrawableRes int bottom) {
            textView.setCompoundDrawablesWithIntrinsicBounds(start, top, end, bottom);
        }

        public int getMaxLines(android.widget.TextView textView) {
            return android.support.v4.widget.TextViewCompatGingerbread.getMaxLines(textView);
        }

        public int getMinLines(android.widget.TextView textView) {
            return android.support.v4.widget.TextViewCompatGingerbread.getMinLines(textView);
        }

        public void setTextAppearance(android.widget.TextView textView, @android.support.annotation.StyleRes int resId) {
            android.support.v4.widget.TextViewCompatGingerbread.setTextAppearance(textView, resId);
        }

        public android.graphics.drawable.Drawable[] getCompoundDrawablesRelative(@android.support.annotation.NonNull android.widget.TextView textView) {
            return android.support.v4.widget.TextViewCompatGingerbread.getCompoundDrawablesRelative(textView);
        }
    }

    static class JbMr1TextViewCompatImpl extends android.support.v4.widget.TextViewCompat.JbTextViewCompatImpl {
        JbMr1TextViewCompatImpl() {
        }

        public void setCompoundDrawablesRelative(@android.support.annotation.NonNull android.widget.TextView textView, @android.support.annotation.Nullable android.graphics.drawable.Drawable start, @android.support.annotation.Nullable android.graphics.drawable.Drawable top, @android.support.annotation.Nullable android.graphics.drawable.Drawable end, @android.support.annotation.Nullable android.graphics.drawable.Drawable bottom) {
            android.support.v4.widget.TextViewCompatJbMr1.setCompoundDrawablesRelative(textView, start, top, end, bottom);
        }

        public void setCompoundDrawablesRelativeWithIntrinsicBounds(@android.support.annotation.NonNull android.widget.TextView textView, @android.support.annotation.Nullable android.graphics.drawable.Drawable start, @android.support.annotation.Nullable android.graphics.drawable.Drawable top, @android.support.annotation.Nullable android.graphics.drawable.Drawable end, @android.support.annotation.Nullable android.graphics.drawable.Drawable bottom) {
            android.support.v4.widget.TextViewCompatJbMr1.setCompoundDrawablesRelativeWithIntrinsicBounds(textView, start, top, end, bottom);
        }

        public void setCompoundDrawablesRelativeWithIntrinsicBounds(@android.support.annotation.NonNull android.widget.TextView textView, @android.support.annotation.DrawableRes int start, @android.support.annotation.DrawableRes int top, @android.support.annotation.DrawableRes int end, @android.support.annotation.DrawableRes int bottom) {
            android.support.v4.widget.TextViewCompatJbMr1.setCompoundDrawablesRelativeWithIntrinsicBounds(textView, start, top, end, bottom);
        }

        public android.graphics.drawable.Drawable[] getCompoundDrawablesRelative(@android.support.annotation.NonNull android.widget.TextView textView) {
            return android.support.v4.widget.TextViewCompatJbMr1.getCompoundDrawablesRelative(textView);
        }
    }

    static class JbMr2TextViewCompatImpl extends android.support.v4.widget.TextViewCompat.JbMr1TextViewCompatImpl {
        JbMr2TextViewCompatImpl() {
        }

        public void setCompoundDrawablesRelative(@android.support.annotation.NonNull android.widget.TextView textView, @android.support.annotation.Nullable android.graphics.drawable.Drawable start, @android.support.annotation.Nullable android.graphics.drawable.Drawable top, @android.support.annotation.Nullable android.graphics.drawable.Drawable end, @android.support.annotation.Nullable android.graphics.drawable.Drawable bottom) {
            android.support.v4.widget.TextViewCompatJbMr2.setCompoundDrawablesRelative(textView, start, top, end, bottom);
        }

        public void setCompoundDrawablesRelativeWithIntrinsicBounds(@android.support.annotation.NonNull android.widget.TextView textView, @android.support.annotation.Nullable android.graphics.drawable.Drawable start, @android.support.annotation.Nullable android.graphics.drawable.Drawable top, @android.support.annotation.Nullable android.graphics.drawable.Drawable end, @android.support.annotation.Nullable android.graphics.drawable.Drawable bottom) {
            android.support.v4.widget.TextViewCompatJbMr2.setCompoundDrawablesRelativeWithIntrinsicBounds(textView, start, top, end, bottom);
        }

        public void setCompoundDrawablesRelativeWithIntrinsicBounds(@android.support.annotation.NonNull android.widget.TextView textView, @android.support.annotation.DrawableRes int start, @android.support.annotation.DrawableRes int top, @android.support.annotation.DrawableRes int end, @android.support.annotation.DrawableRes int bottom) {
            android.support.v4.widget.TextViewCompatJbMr2.setCompoundDrawablesRelativeWithIntrinsicBounds(textView, start, top, end, bottom);
        }

        public android.graphics.drawable.Drawable[] getCompoundDrawablesRelative(@android.support.annotation.NonNull android.widget.TextView textView) {
            return android.support.v4.widget.TextViewCompatJbMr2.getCompoundDrawablesRelative(textView);
        }
    }

    static class JbTextViewCompatImpl extends android.support.v4.widget.TextViewCompat.BaseTextViewCompatImpl {
        JbTextViewCompatImpl() {
        }

        public int getMaxLines(android.widget.TextView textView) {
            return android.support.v4.widget.TextViewCompatJb.getMaxLines(textView);
        }

        public int getMinLines(android.widget.TextView textView) {
            return android.support.v4.widget.TextViewCompatJb.getMinLines(textView);
        }
    }

    interface TextViewCompatImpl {
        android.graphics.drawable.Drawable[] getCompoundDrawablesRelative(@android.support.annotation.NonNull android.widget.TextView textView);

        int getMaxLines(android.widget.TextView textView);

        int getMinLines(android.widget.TextView textView);

        void setCompoundDrawablesRelative(@android.support.annotation.NonNull android.widget.TextView textView, @android.support.annotation.Nullable android.graphics.drawable.Drawable drawable, @android.support.annotation.Nullable android.graphics.drawable.Drawable drawable2, @android.support.annotation.Nullable android.graphics.drawable.Drawable drawable3, @android.support.annotation.Nullable android.graphics.drawable.Drawable drawable4);

        void setCompoundDrawablesRelativeWithIntrinsicBounds(@android.support.annotation.NonNull android.widget.TextView textView, @android.support.annotation.DrawableRes int i, @android.support.annotation.DrawableRes int i2, @android.support.annotation.DrawableRes int i3, @android.support.annotation.DrawableRes int i4);

        void setCompoundDrawablesRelativeWithIntrinsicBounds(@android.support.annotation.NonNull android.widget.TextView textView, @android.support.annotation.Nullable android.graphics.drawable.Drawable drawable, @android.support.annotation.Nullable android.graphics.drawable.Drawable drawable2, @android.support.annotation.Nullable android.graphics.drawable.Drawable drawable3, @android.support.annotation.Nullable android.graphics.drawable.Drawable drawable4);

        void setTextAppearance(@android.support.annotation.NonNull android.widget.TextView textView, @android.support.annotation.StyleRes int i);
    }

    private TextViewCompat() {
    }

    static {
        int version = android.os.Build.VERSION.SDK_INT;
        if (version >= 23) {
            IMPL = new android.support.v4.widget.TextViewCompat.Api23TextViewCompatImpl();
        } else if (version >= 18) {
            IMPL = new android.support.v4.widget.TextViewCompat.JbMr2TextViewCompatImpl();
        } else if (version >= 17) {
            IMPL = new android.support.v4.widget.TextViewCompat.JbMr1TextViewCompatImpl();
        } else if (version >= 16) {
            IMPL = new android.support.v4.widget.TextViewCompat.JbTextViewCompatImpl();
        } else {
            IMPL = new android.support.v4.widget.TextViewCompat.BaseTextViewCompatImpl();
        }
    }

    public static void setCompoundDrawablesRelative(@android.support.annotation.NonNull android.widget.TextView textView, @android.support.annotation.Nullable android.graphics.drawable.Drawable start, @android.support.annotation.Nullable android.graphics.drawable.Drawable top, @android.support.annotation.Nullable android.graphics.drawable.Drawable end, @android.support.annotation.Nullable android.graphics.drawable.Drawable bottom) {
        IMPL.setCompoundDrawablesRelative(textView, start, top, end, bottom);
    }

    public static void setCompoundDrawablesRelativeWithIntrinsicBounds(@android.support.annotation.NonNull android.widget.TextView textView, @android.support.annotation.Nullable android.graphics.drawable.Drawable start, @android.support.annotation.Nullable android.graphics.drawable.Drawable top, @android.support.annotation.Nullable android.graphics.drawable.Drawable end, @android.support.annotation.Nullable android.graphics.drawable.Drawable bottom) {
        IMPL.setCompoundDrawablesRelativeWithIntrinsicBounds(textView, start, top, end, bottom);
    }

    public static void setCompoundDrawablesRelativeWithIntrinsicBounds(@android.support.annotation.NonNull android.widget.TextView textView, @android.support.annotation.DrawableRes int start, @android.support.annotation.DrawableRes int top, @android.support.annotation.DrawableRes int end, @android.support.annotation.DrawableRes int bottom) {
        IMPL.setCompoundDrawablesRelativeWithIntrinsicBounds(textView, start, top, end, bottom);
    }

    public static int getMaxLines(@android.support.annotation.NonNull android.widget.TextView textView) {
        return IMPL.getMaxLines(textView);
    }

    public static int getMinLines(@android.support.annotation.NonNull android.widget.TextView textView) {
        return IMPL.getMinLines(textView);
    }

    public static void setTextAppearance(@android.support.annotation.NonNull android.widget.TextView textView, @android.support.annotation.StyleRes int resId) {
        IMPL.setTextAppearance(textView, resId);
    }

    public static android.graphics.drawable.Drawable[] getCompoundDrawablesRelative(@android.support.annotation.NonNull android.widget.TextView textView) {
        return IMPL.getCompoundDrawablesRelative(textView);
    }
}
