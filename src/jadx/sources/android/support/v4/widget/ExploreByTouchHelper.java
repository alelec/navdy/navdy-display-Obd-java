package android.support.v4.widget;

public abstract class ExploreByTouchHelper extends android.support.v4.view.AccessibilityDelegateCompat {
    private static final java.lang.String DEFAULT_CLASS_NAME = "android.view.View";
    public static final int HOST_ID = -1;
    public static final int INVALID_ID = Integer.MIN_VALUE;
    private static final android.graphics.Rect INVALID_PARENT_BOUNDS = new android.graphics.Rect(Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE);
    private static final android.support.v4.widget.FocusStrategy.BoundsAdapter<android.support.v4.view.accessibility.AccessibilityNodeInfoCompat> NODE_ADAPTER = new android.support.v4.widget.FocusStrategy.BoundsAdapter<android.support.v4.view.accessibility.AccessibilityNodeInfoCompat>() {
        public void obtainBounds(android.support.v4.view.accessibility.AccessibilityNodeInfoCompat node, android.graphics.Rect outBounds) {
            node.getBoundsInParent(outBounds);
        }
    };
    private static final android.support.v4.widget.FocusStrategy.CollectionAdapter<android.support.v4.util.SparseArrayCompat<android.support.v4.view.accessibility.AccessibilityNodeInfoCompat>, android.support.v4.view.accessibility.AccessibilityNodeInfoCompat> SPARSE_VALUES_ADAPTER = new android.support.v4.widget.FocusStrategy.CollectionAdapter<android.support.v4.util.SparseArrayCompat<android.support.v4.view.accessibility.AccessibilityNodeInfoCompat>, android.support.v4.view.accessibility.AccessibilityNodeInfoCompat>() {
        public android.support.v4.view.accessibility.AccessibilityNodeInfoCompat get(android.support.v4.util.SparseArrayCompat<android.support.v4.view.accessibility.AccessibilityNodeInfoCompat> collection, int index) {
            return (android.support.v4.view.accessibility.AccessibilityNodeInfoCompat) collection.valueAt(index);
        }

        public int size(android.support.v4.util.SparseArrayCompat<android.support.v4.view.accessibility.AccessibilityNodeInfoCompat> collection) {
            return collection.size();
        }
    };
    /* access modifiers changed from: private */
    public int mAccessibilityFocusedVirtualViewId = Integer.MIN_VALUE;
    private final android.view.View mHost;
    private int mHoveredVirtualViewId = Integer.MIN_VALUE;
    /* access modifiers changed from: private */
    public int mKeyboardFocusedVirtualViewId = Integer.MIN_VALUE;
    private final android.view.accessibility.AccessibilityManager mManager;
    private android.support.v4.widget.ExploreByTouchHelper.MyNodeProvider mNodeProvider;
    private final int[] mTempGlobalRect = new int[2];
    private final android.graphics.Rect mTempParentRect = new android.graphics.Rect();
    private final android.graphics.Rect mTempScreenRect = new android.graphics.Rect();
    private final android.graphics.Rect mTempVisibleRect = new android.graphics.Rect();

    private class MyNodeProvider extends android.support.v4.view.accessibility.AccessibilityNodeProviderCompat {
        MyNodeProvider() {
        }

        public android.support.v4.view.accessibility.AccessibilityNodeInfoCompat createAccessibilityNodeInfo(int virtualViewId) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.obtain(android.support.v4.widget.ExploreByTouchHelper.this.obtainAccessibilityNodeInfo(virtualViewId));
        }

        public boolean performAction(int virtualViewId, int action, android.os.Bundle arguments) {
            return android.support.v4.widget.ExploreByTouchHelper.this.performAction(virtualViewId, action, arguments);
        }

        public android.support.v4.view.accessibility.AccessibilityNodeInfoCompat findFocus(int focusType) {
            int focusedId = focusType == 2 ? android.support.v4.widget.ExploreByTouchHelper.this.mAccessibilityFocusedVirtualViewId : android.support.v4.widget.ExploreByTouchHelper.this.mKeyboardFocusedVirtualViewId;
            if (focusedId == Integer.MIN_VALUE) {
                return null;
            }
            return createAccessibilityNodeInfo(focusedId);
        }
    }

    /* access modifiers changed from: protected */
    public abstract int getVirtualViewAt(float f, float f2);

    /* access modifiers changed from: protected */
    public abstract void getVisibleVirtualViews(java.util.List<java.lang.Integer> list);

    /* access modifiers changed from: protected */
    public abstract boolean onPerformActionForVirtualView(int i, int i2, android.os.Bundle bundle);

    /* access modifiers changed from: protected */
    public abstract void onPopulateNodeForVirtualView(int i, android.support.v4.view.accessibility.AccessibilityNodeInfoCompat accessibilityNodeInfoCompat);

    public ExploreByTouchHelper(android.view.View host) {
        if (host == null) {
            throw new java.lang.IllegalArgumentException("View may not be null");
        }
        this.mHost = host;
        this.mManager = (android.view.accessibility.AccessibilityManager) host.getContext().getSystemService("accessibility");
        host.setFocusable(true);
        if (android.support.v4.view.ViewCompat.getImportantForAccessibility(host) == 0) {
            android.support.v4.view.ViewCompat.setImportantForAccessibility(host, 1);
        }
    }

    public android.support.v4.view.accessibility.AccessibilityNodeProviderCompat getAccessibilityNodeProvider(android.view.View host) {
        if (this.mNodeProvider == null) {
            this.mNodeProvider = new android.support.v4.widget.ExploreByTouchHelper.MyNodeProvider();
        }
        return this.mNodeProvider;
    }

    public final boolean dispatchHoverEvent(@android.support.annotation.NonNull android.view.MotionEvent event) {
        boolean z = true;
        if (!this.mManager.isEnabled() || !android.support.v4.view.accessibility.AccessibilityManagerCompat.isTouchExplorationEnabled(this.mManager)) {
            return false;
        }
        switch (event.getAction()) {
            case 7:
            case 9:
                int virtualViewId = getVirtualViewAt(event.getX(), event.getY());
                updateHoveredVirtualView(virtualViewId);
                if (virtualViewId == Integer.MIN_VALUE) {
                    z = false;
                }
                return z;
            case 10:
                if (this.mAccessibilityFocusedVirtualViewId == Integer.MIN_VALUE) {
                    return false;
                }
                updateHoveredVirtualView(Integer.MIN_VALUE);
                return true;
            default:
                return false;
        }
    }

    public final boolean dispatchKeyEvent(@android.support.annotation.NonNull android.view.KeyEvent event) {
        boolean handled = false;
        if (event.getAction() == 1) {
            return false;
        }
        int keyCode = event.getKeyCode();
        switch (keyCode) {
            case 19:
            case 20:
            case android.support.v4.view.MotionEventCompat.AXIS_WHEEL /*21*/:
            case android.support.v4.view.MotionEventCompat.AXIS_GAS /*22*/:
                if (!android.support.v4.view.KeyEventCompat.hasNoModifiers(event)) {
                    return false;
                }
                int direction = keyToDirection(keyCode);
                int count = event.getRepeatCount() + 1;
                for (int i = 0; i < count && moveFocus(direction, null); i++) {
                    handled = true;
                }
                return handled;
            case android.support.v4.view.MotionEventCompat.AXIS_BRAKE /*23*/:
            case 66:
                if (!android.support.v4.view.KeyEventCompat.hasNoModifiers(event) || event.getRepeatCount() != 0) {
                    return false;
                }
                clickKeyboardFocusedVirtualView();
                return true;
            case 61:
                if (android.support.v4.view.KeyEventCompat.hasNoModifiers(event)) {
                    return moveFocus(2, null);
                }
                if (android.support.v4.view.KeyEventCompat.hasModifiers(event, 1)) {
                    return moveFocus(1, null);
                }
                return false;
            default:
                return false;
        }
    }

    public final void onFocusChanged(boolean gainFocus, int direction, @android.support.annotation.Nullable android.graphics.Rect previouslyFocusedRect) {
        if (this.mKeyboardFocusedVirtualViewId != Integer.MIN_VALUE) {
            clearKeyboardFocusForVirtualView(this.mKeyboardFocusedVirtualViewId);
        }
        if (gainFocus) {
            moveFocus(direction, previouslyFocusedRect);
        }
    }

    public final int getAccessibilityFocusedVirtualViewId() {
        return this.mAccessibilityFocusedVirtualViewId;
    }

    public final int getKeyboardFocusedVirtualViewId() {
        return this.mKeyboardFocusedVirtualViewId;
    }

    private static int keyToDirection(int keyCode) {
        switch (keyCode) {
            case 19:
                return 33;
            case android.support.v4.view.MotionEventCompat.AXIS_WHEEL /*21*/:
                return 17;
            case android.support.v4.view.MotionEventCompat.AXIS_GAS /*22*/:
                return 66;
            default:
                return android.support.v4.media.TransportMediator.KEYCODE_MEDIA_RECORD;
        }
    }

    private void getBoundsInParent(int virtualViewId, android.graphics.Rect outBounds) {
        obtainAccessibilityNodeInfo(virtualViewId).getBoundsInParent(outBounds);
    }

    private boolean moveFocus(int direction, @android.support.annotation.Nullable android.graphics.Rect previouslyFocusedRect) {
        android.support.v4.view.accessibility.AccessibilityNodeInfoCompat focusedNode;
        android.support.v4.view.accessibility.AccessibilityNodeInfoCompat nextFocusedNode;
        int nextFocusedNodeId;
        android.support.v4.util.SparseArrayCompat<android.support.v4.view.accessibility.AccessibilityNodeInfoCompat> allNodes = getAllNodes();
        int focusedNodeId = this.mKeyboardFocusedVirtualViewId;
        if (focusedNodeId == Integer.MIN_VALUE) {
            focusedNode = null;
        } else {
            focusedNode = (android.support.v4.view.accessibility.AccessibilityNodeInfoCompat) allNodes.get(focusedNodeId);
        }
        switch (direction) {
            case 1:
            case 2:
                nextFocusedNode = (android.support.v4.view.accessibility.AccessibilityNodeInfoCompat) android.support.v4.widget.FocusStrategy.findNextFocusInRelativeDirection(allNodes, SPARSE_VALUES_ADAPTER, NODE_ADAPTER, focusedNode, direction, android.support.v4.view.ViewCompat.getLayoutDirection(this.mHost) == 1, false);
                break;
            case 17:
            case android.support.v4.view.MotionEventCompat.AXIS_GENERIC_2 /*33*/:
            case 66:
            case android.support.v4.media.TransportMediator.KEYCODE_MEDIA_RECORD /*130*/:
                android.graphics.Rect selectedRect = new android.graphics.Rect();
                if (this.mKeyboardFocusedVirtualViewId != Integer.MIN_VALUE) {
                    getBoundsInParent(this.mKeyboardFocusedVirtualViewId, selectedRect);
                } else if (previouslyFocusedRect != null) {
                    selectedRect.set(previouslyFocusedRect);
                } else {
                    guessPreviouslyFocusedRect(this.mHost, direction, selectedRect);
                }
                nextFocusedNode = (android.support.v4.view.accessibility.AccessibilityNodeInfoCompat) android.support.v4.widget.FocusStrategy.findNextFocusInAbsoluteDirection(allNodes, SPARSE_VALUES_ADAPTER, NODE_ADAPTER, focusedNode, selectedRect, direction);
                break;
            default:
                throw new java.lang.IllegalArgumentException("direction must be one of {FOCUS_FORWARD, FOCUS_BACKWARD, FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}.");
        }
        if (nextFocusedNode == null) {
            nextFocusedNodeId = Integer.MIN_VALUE;
        } else {
            nextFocusedNodeId = allNodes.keyAt(allNodes.indexOfValue(nextFocusedNode));
        }
        return requestKeyboardFocusForVirtualView(nextFocusedNodeId);
    }

    private android.support.v4.util.SparseArrayCompat<android.support.v4.view.accessibility.AccessibilityNodeInfoCompat> getAllNodes() {
        java.util.List<java.lang.Integer> virtualViewIds = new java.util.ArrayList<>();
        getVisibleVirtualViews(virtualViewIds);
        android.support.v4.util.SparseArrayCompat<android.support.v4.view.accessibility.AccessibilityNodeInfoCompat> allNodes = new android.support.v4.util.SparseArrayCompat<>();
        for (int virtualViewId = 0; virtualViewId < virtualViewIds.size(); virtualViewId++) {
            allNodes.put(virtualViewId, createNodeForChild(virtualViewId));
        }
        return allNodes;
    }

    private static android.graphics.Rect guessPreviouslyFocusedRect(@android.support.annotation.NonNull android.view.View host, int direction, @android.support.annotation.NonNull android.graphics.Rect outBounds) {
        int w = host.getWidth();
        int h = host.getHeight();
        switch (direction) {
            case 17:
                outBounds.set(w, 0, w, h);
                break;
            case android.support.v4.view.MotionEventCompat.AXIS_GENERIC_2 /*33*/:
                outBounds.set(0, h, w, h);
                break;
            case 66:
                outBounds.set(-1, 0, -1, h);
                break;
            case android.support.v4.media.TransportMediator.KEYCODE_MEDIA_RECORD /*130*/:
                outBounds.set(0, -1, w, -1);
                break;
            default:
                throw new java.lang.IllegalArgumentException("direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}.");
        }
        return outBounds;
    }

    private boolean clickKeyboardFocusedVirtualView() {
        return this.mKeyboardFocusedVirtualViewId != Integer.MIN_VALUE && onPerformActionForVirtualView(this.mKeyboardFocusedVirtualViewId, 16, null);
    }

    public final boolean sendEventForVirtualView(int virtualViewId, int eventType) {
        if (virtualViewId == Integer.MIN_VALUE || !this.mManager.isEnabled()) {
            return false;
        }
        android.view.ViewParent parent = this.mHost.getParent();
        if (parent == null) {
            return false;
        }
        return android.support.v4.view.ViewParentCompat.requestSendAccessibilityEvent(parent, this.mHost, createEvent(virtualViewId, eventType));
    }

    public final void invalidateRoot() {
        invalidateVirtualView(-1, 1);
    }

    public final void invalidateVirtualView(int virtualViewId) {
        invalidateVirtualView(virtualViewId, 0);
    }

    public final void invalidateVirtualView(int virtualViewId, int changeTypes) {
        if (virtualViewId != Integer.MIN_VALUE && this.mManager.isEnabled()) {
            android.view.ViewParent parent = this.mHost.getParent();
            if (parent != null) {
                android.view.accessibility.AccessibilityEvent event = createEvent(virtualViewId, 2048);
                android.support.v4.view.accessibility.AccessibilityEventCompat.setContentChangeTypes(event, changeTypes);
                android.support.v4.view.ViewParentCompat.requestSendAccessibilityEvent(parent, this.mHost, event);
            }
        }
    }

    @java.lang.Deprecated
    public int getFocusedVirtualView() {
        return getAccessibilityFocusedVirtualViewId();
    }

    /* access modifiers changed from: protected */
    public void onVirtualViewKeyboardFocusChanged(int virtualViewId, boolean hasFocus) {
    }

    private void updateHoveredVirtualView(int virtualViewId) {
        if (this.mHoveredVirtualViewId != virtualViewId) {
            int previousVirtualViewId = this.mHoveredVirtualViewId;
            this.mHoveredVirtualViewId = virtualViewId;
            sendEventForVirtualView(virtualViewId, 128);
            sendEventForVirtualView(previousVirtualViewId, 256);
        }
    }

    private android.view.accessibility.AccessibilityEvent createEvent(int virtualViewId, int eventType) {
        switch (virtualViewId) {
            case -1:
                return createEventForHost(eventType);
            default:
                return createEventForChild(virtualViewId, eventType);
        }
    }

    private android.view.accessibility.AccessibilityEvent createEventForHost(int eventType) {
        android.view.accessibility.AccessibilityEvent event = android.view.accessibility.AccessibilityEvent.obtain(eventType);
        android.support.v4.view.ViewCompat.onInitializeAccessibilityEvent(this.mHost, event);
        return event;
    }

    public void onInitializeAccessibilityEvent(android.view.View host, android.view.accessibility.AccessibilityEvent event) {
        super.onInitializeAccessibilityEvent(host, event);
        onPopulateEventForHost(event);
    }

    private android.view.accessibility.AccessibilityEvent createEventForChild(int virtualViewId, int eventType) {
        android.view.accessibility.AccessibilityEvent event = android.view.accessibility.AccessibilityEvent.obtain(eventType);
        android.support.v4.view.accessibility.AccessibilityRecordCompat record = android.support.v4.view.accessibility.AccessibilityEventCompat.asRecord(event);
        android.support.v4.view.accessibility.AccessibilityNodeInfoCompat node = obtainAccessibilityNodeInfo(virtualViewId);
        record.getText().add(node.getText());
        record.setContentDescription(node.getContentDescription());
        record.setScrollable(node.isScrollable());
        record.setPassword(node.isPassword());
        record.setEnabled(node.isEnabled());
        record.setChecked(node.isChecked());
        onPopulateEventForVirtualView(virtualViewId, event);
        if (!event.getText().isEmpty() || event.getContentDescription() != null) {
            record.setClassName(node.getClassName());
            record.setSource(this.mHost, virtualViewId);
            event.setPackageName(this.mHost.getContext().getPackageName());
            return event;
        }
        throw new java.lang.RuntimeException("Callbacks must add text or a content description in populateEventForVirtualViewId()");
    }

    /* access modifiers changed from: 0000 */
    @android.support.annotation.NonNull
    public android.support.v4.view.accessibility.AccessibilityNodeInfoCompat obtainAccessibilityNodeInfo(int virtualViewId) {
        if (virtualViewId == -1) {
            return createNodeForHost();
        }
        return createNodeForChild(virtualViewId);
    }

    @android.support.annotation.NonNull
    private android.support.v4.view.accessibility.AccessibilityNodeInfoCompat createNodeForHost() {
        android.support.v4.view.accessibility.AccessibilityNodeInfoCompat info = android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.obtain(this.mHost);
        android.support.v4.view.ViewCompat.onInitializeAccessibilityNodeInfo(this.mHost, info);
        java.util.ArrayList<java.lang.Integer> virtualViewIds = new java.util.ArrayList<>();
        getVisibleVirtualViews(virtualViewIds);
        if (info.getChildCount() <= 0 || virtualViewIds.size() <= 0) {
            int count = virtualViewIds.size();
            for (int i = 0; i < count; i++) {
                info.addChild(this.mHost, ((java.lang.Integer) virtualViewIds.get(i)).intValue());
            }
            return info;
        }
        throw new java.lang.RuntimeException("Views cannot have both real and virtual children");
    }

    public void onInitializeAccessibilityNodeInfo(android.view.View host, android.support.v4.view.accessibility.AccessibilityNodeInfoCompat info) {
        super.onInitializeAccessibilityNodeInfo(host, info);
        onPopulateNodeForHost(info);
    }

    @android.support.annotation.NonNull
    private android.support.v4.view.accessibility.AccessibilityNodeInfoCompat createNodeForChild(int virtualViewId) {
        boolean isFocused;
        android.support.v4.view.accessibility.AccessibilityNodeInfoCompat node = android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.obtain();
        node.setEnabled(true);
        node.setFocusable(true);
        node.setClassName(DEFAULT_CLASS_NAME);
        node.setBoundsInParent(INVALID_PARENT_BOUNDS);
        node.setBoundsInScreen(INVALID_PARENT_BOUNDS);
        node.setParent(this.mHost);
        onPopulateNodeForVirtualView(virtualViewId, node);
        if (node.getText() == null && node.getContentDescription() == null) {
            throw new java.lang.RuntimeException("Callbacks must add text or a content description in populateNodeForVirtualViewId()");
        }
        node.getBoundsInParent(this.mTempParentRect);
        if (this.mTempParentRect.equals(INVALID_PARENT_BOUNDS)) {
            throw new java.lang.RuntimeException("Callbacks must set parent bounds in populateNodeForVirtualViewId()");
        }
        int actions = node.getActions();
        if ((actions & 64) != 0) {
            throw new java.lang.RuntimeException("Callbacks must not add ACTION_ACCESSIBILITY_FOCUS in populateNodeForVirtualViewId()");
        } else if ((actions & 128) != 0) {
            throw new java.lang.RuntimeException("Callbacks must not add ACTION_CLEAR_ACCESSIBILITY_FOCUS in populateNodeForVirtualViewId()");
        } else {
            node.setPackageName(this.mHost.getContext().getPackageName());
            node.setSource(this.mHost, virtualViewId);
            if (this.mAccessibilityFocusedVirtualViewId == virtualViewId) {
                node.setAccessibilityFocused(true);
                node.addAction(128);
            } else {
                node.setAccessibilityFocused(false);
                node.addAction(64);
            }
            if (this.mKeyboardFocusedVirtualViewId == virtualViewId) {
                isFocused = true;
            } else {
                isFocused = false;
            }
            if (isFocused) {
                node.addAction(2);
            } else if (node.isFocusable()) {
                node.addAction(1);
            }
            node.setFocused(isFocused);
            this.mHost.getLocationOnScreen(this.mTempGlobalRect);
            node.getBoundsInScreen(this.mTempScreenRect);
            if (this.mTempScreenRect.equals(INVALID_PARENT_BOUNDS)) {
                node.getBoundsInParent(this.mTempScreenRect);
                if (node.mParentVirtualDescendantId != -1) {
                    android.support.v4.view.accessibility.AccessibilityNodeInfoCompat parentNode = android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.obtain();
                    for (int virtualDescendantId = node.mParentVirtualDescendantId; virtualDescendantId != -1; virtualDescendantId = parentNode.mParentVirtualDescendantId) {
                        parentNode.setParent(this.mHost, -1);
                        parentNode.setBoundsInParent(INVALID_PARENT_BOUNDS);
                        onPopulateNodeForVirtualView(virtualDescendantId, parentNode);
                        parentNode.getBoundsInParent(this.mTempParentRect);
                        this.mTempScreenRect.offset(this.mTempParentRect.left, this.mTempParentRect.top);
                    }
                    parentNode.recycle();
                }
                this.mTempScreenRect.offset(this.mTempGlobalRect[0] - this.mHost.getScrollX(), this.mTempGlobalRect[1] - this.mHost.getScrollY());
            }
            if (this.mHost.getLocalVisibleRect(this.mTempVisibleRect)) {
                this.mTempVisibleRect.offset(this.mTempGlobalRect[0] - this.mHost.getScrollX(), this.mTempGlobalRect[1] - this.mHost.getScrollY());
                this.mTempScreenRect.intersect(this.mTempVisibleRect);
                node.setBoundsInScreen(this.mTempScreenRect);
                if (isVisibleToUser(this.mTempScreenRect)) {
                    node.setVisibleToUser(true);
                }
            }
            return node;
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean performAction(int virtualViewId, int action, android.os.Bundle arguments) {
        switch (virtualViewId) {
            case -1:
                return performActionForHost(action, arguments);
            default:
                return performActionForChild(virtualViewId, action, arguments);
        }
    }

    private boolean performActionForHost(int action, android.os.Bundle arguments) {
        return android.support.v4.view.ViewCompat.performAccessibilityAction(this.mHost, action, arguments);
    }

    private boolean performActionForChild(int virtualViewId, int action, android.os.Bundle arguments) {
        switch (action) {
            case 1:
                return requestKeyboardFocusForVirtualView(virtualViewId);
            case 2:
                return clearKeyboardFocusForVirtualView(virtualViewId);
            case 64:
                return requestAccessibilityFocus(virtualViewId);
            case 128:
                return clearAccessibilityFocus(virtualViewId);
            default:
                return onPerformActionForVirtualView(virtualViewId, action, arguments);
        }
    }

    private boolean isVisibleToUser(android.graphics.Rect localRect) {
        if (localRect == null || localRect.isEmpty() || this.mHost.getWindowVisibility() != 0) {
            return false;
        }
        android.view.ViewParent viewParent = this.mHost.getParent();
        while (viewParent instanceof android.view.View) {
            android.view.View view = (android.view.View) viewParent;
            if (android.support.v4.view.ViewCompat.getAlpha(view) <= 0.0f || view.getVisibility() != 0) {
                return false;
            }
            viewParent = view.getParent();
        }
        if (viewParent != null) {
            return true;
        }
        return false;
    }

    private boolean requestAccessibilityFocus(int virtualViewId) {
        if (!this.mManager.isEnabled() || !android.support.v4.view.accessibility.AccessibilityManagerCompat.isTouchExplorationEnabled(this.mManager) || this.mAccessibilityFocusedVirtualViewId == virtualViewId) {
            return false;
        }
        if (this.mAccessibilityFocusedVirtualViewId != Integer.MIN_VALUE) {
            clearAccessibilityFocus(this.mAccessibilityFocusedVirtualViewId);
        }
        this.mAccessibilityFocusedVirtualViewId = virtualViewId;
        this.mHost.invalidate();
        sendEventForVirtualView(virtualViewId, 32768);
        return true;
    }

    private boolean clearAccessibilityFocus(int virtualViewId) {
        if (this.mAccessibilityFocusedVirtualViewId != virtualViewId) {
            return false;
        }
        this.mAccessibilityFocusedVirtualViewId = Integer.MIN_VALUE;
        this.mHost.invalidate();
        sendEventForVirtualView(virtualViewId, 65536);
        return true;
    }

    public final boolean requestKeyboardFocusForVirtualView(int virtualViewId) {
        if ((!this.mHost.isFocused() && !this.mHost.requestFocus()) || this.mKeyboardFocusedVirtualViewId == virtualViewId) {
            return false;
        }
        if (this.mKeyboardFocusedVirtualViewId != Integer.MIN_VALUE) {
            clearKeyboardFocusForVirtualView(this.mKeyboardFocusedVirtualViewId);
        }
        this.mKeyboardFocusedVirtualViewId = virtualViewId;
        onVirtualViewKeyboardFocusChanged(virtualViewId, true);
        sendEventForVirtualView(virtualViewId, 8);
        return true;
    }

    public final boolean clearKeyboardFocusForVirtualView(int virtualViewId) {
        if (this.mKeyboardFocusedVirtualViewId != virtualViewId) {
            return false;
        }
        this.mKeyboardFocusedVirtualViewId = Integer.MIN_VALUE;
        onVirtualViewKeyboardFocusChanged(virtualViewId, false);
        sendEventForVirtualView(virtualViewId, 8);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPopulateEventForVirtualView(int virtualViewId, android.view.accessibility.AccessibilityEvent event) {
    }

    /* access modifiers changed from: protected */
    public void onPopulateEventForHost(android.view.accessibility.AccessibilityEvent event) {
    }

    /* access modifiers changed from: protected */
    public void onPopulateNodeForHost(android.support.v4.view.accessibility.AccessibilityNodeInfoCompat node) {
    }
}
