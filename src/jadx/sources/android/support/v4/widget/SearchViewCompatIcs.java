package android.support.v4.widget;

@android.annotation.TargetApi(14)
@android.support.annotation.RequiresApi(14)
class SearchViewCompatIcs {

    public static class MySearchView extends android.widget.SearchView {
        public MySearchView(android.content.Context context) {
            super(context);
        }

        public void onActionViewCollapsed() {
            setQuery("", false);
            super.onActionViewCollapsed();
        }
    }

    SearchViewCompatIcs() {
    }

    public static android.view.View newSearchView(android.content.Context context) {
        return new android.support.v4.widget.SearchViewCompatIcs.MySearchView(context);
    }

    public static void setImeOptions(android.view.View searchView, int imeOptions) {
        ((android.widget.SearchView) searchView).setImeOptions(imeOptions);
    }

    public static void setInputType(android.view.View searchView, int inputType) {
        ((android.widget.SearchView) searchView).setInputType(inputType);
    }
}
