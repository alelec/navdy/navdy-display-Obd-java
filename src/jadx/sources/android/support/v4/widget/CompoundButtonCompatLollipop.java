package android.support.v4.widget;

@android.annotation.TargetApi(21)
@android.support.annotation.RequiresApi(21)
class CompoundButtonCompatLollipop {
    CompoundButtonCompatLollipop() {
    }

    static void setButtonTintList(android.widget.CompoundButton button, android.content.res.ColorStateList tint) {
        button.setButtonTintList(tint);
    }

    static android.content.res.ColorStateList getButtonTintList(android.widget.CompoundButton button) {
        return button.getButtonTintList();
    }

    static void setButtonTintMode(android.widget.CompoundButton button, android.graphics.PorterDuff.Mode tintMode) {
        button.setButtonTintMode(tintMode);
    }

    static android.graphics.PorterDuff.Mode getButtonTintMode(android.widget.CompoundButton button) {
        return button.getButtonTintMode();
    }
}
