package android.support.v4.widget;

@android.annotation.TargetApi(23)
@android.support.annotation.RequiresApi(23)
class CompoundButtonCompatApi23 {
    CompoundButtonCompatApi23() {
    }

    static android.graphics.drawable.Drawable getButtonDrawable(android.widget.CompoundButton button) {
        return button.getButtonDrawable();
    }
}
