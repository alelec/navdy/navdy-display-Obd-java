package android.support.v4.widget;

public class ContentLoadingProgressBar extends android.widget.ProgressBar {
    private static final int MIN_DELAY = 500;
    private static final int MIN_SHOW_TIME = 500;
    private final java.lang.Runnable mDelayedHide;
    private final java.lang.Runnable mDelayedShow;
    boolean mDismissed;
    boolean mPostedHide;
    boolean mPostedShow;
    long mStartTime;

    public ContentLoadingProgressBar(android.content.Context context) {
        this(context, null);
    }

    public ContentLoadingProgressBar(android.content.Context context, android.util.AttributeSet attrs) {
        super(context, attrs, 0);
        this.mStartTime = -1;
        this.mPostedHide = false;
        this.mPostedShow = false;
        this.mDismissed = false;
        this.mDelayedHide = new java.lang.Runnable() {
            public void run() {
                android.support.v4.widget.ContentLoadingProgressBar.this.mPostedHide = false;
                android.support.v4.widget.ContentLoadingProgressBar.this.mStartTime = -1;
                android.support.v4.widget.ContentLoadingProgressBar.this.setVisibility(8);
            }
        };
        this.mDelayedShow = new java.lang.Runnable() {
            public void run() {
                android.support.v4.widget.ContentLoadingProgressBar.this.mPostedShow = false;
                if (!android.support.v4.widget.ContentLoadingProgressBar.this.mDismissed) {
                    android.support.v4.widget.ContentLoadingProgressBar.this.mStartTime = java.lang.System.currentTimeMillis();
                    android.support.v4.widget.ContentLoadingProgressBar.this.setVisibility(0);
                }
            }
        };
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        removeCallbacks();
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        removeCallbacks();
    }

    private void removeCallbacks() {
        removeCallbacks(this.mDelayedHide);
        removeCallbacks(this.mDelayedShow);
    }

    public void hide() {
        this.mDismissed = true;
        removeCallbacks(this.mDelayedShow);
        long diff = java.lang.System.currentTimeMillis() - this.mStartTime;
        if (diff >= 500 || this.mStartTime == -1) {
            setVisibility(8);
        } else if (!this.mPostedHide) {
            postDelayed(this.mDelayedHide, 500 - diff);
            this.mPostedHide = true;
        }
    }

    public void show() {
        this.mStartTime = -1;
        this.mDismissed = false;
        removeCallbacks(this.mDelayedHide);
        if (!this.mPostedShow) {
            postDelayed(this.mDelayedShow, 500);
            this.mPostedShow = true;
        }
    }
}
