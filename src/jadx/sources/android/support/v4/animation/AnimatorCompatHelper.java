package android.support.v4.animation;

@android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
public final class AnimatorCompatHelper {
    private static final android.support.v4.animation.AnimatorProvider IMPL;

    static {
        if (android.os.Build.VERSION.SDK_INT >= 12) {
            IMPL = new android.support.v4.animation.HoneycombMr1AnimatorCompatProvider();
        } else {
            IMPL = new android.support.v4.animation.GingerbreadAnimatorCompatProvider();
        }
    }

    public static android.support.v4.animation.ValueAnimatorCompat emptyValueAnimator() {
        return IMPL.emptyValueAnimator();
    }

    private AnimatorCompatHelper() {
    }

    public static void clearInterpolator(android.view.View view) {
        IMPL.clearInterpolator(view);
    }
}
