package android.support.v4.animation;

interface AnimatorProvider {
    void clearInterpolator(android.view.View view);

    android.support.v4.animation.ValueAnimatorCompat emptyValueAnimator();
}
