package android.support.v4.animation;

@android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
public interface AnimatorListenerCompat {
    void onAnimationCancel(android.support.v4.animation.ValueAnimatorCompat valueAnimatorCompat);

    void onAnimationEnd(android.support.v4.animation.ValueAnimatorCompat valueAnimatorCompat);

    void onAnimationRepeat(android.support.v4.animation.ValueAnimatorCompat valueAnimatorCompat);

    void onAnimationStart(android.support.v4.animation.ValueAnimatorCompat valueAnimatorCompat);
}
