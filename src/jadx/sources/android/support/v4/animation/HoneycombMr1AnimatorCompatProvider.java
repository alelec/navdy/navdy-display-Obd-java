package android.support.v4.animation;

@android.annotation.TargetApi(12)
@android.support.annotation.RequiresApi(12)
class HoneycombMr1AnimatorCompatProvider implements android.support.v4.animation.AnimatorProvider {
    private android.animation.TimeInterpolator mDefaultInterpolator;

    static class AnimatorListenerCompatWrapper implements android.animation.Animator.AnimatorListener {
        final android.support.v4.animation.ValueAnimatorCompat mValueAnimatorCompat;
        final android.support.v4.animation.AnimatorListenerCompat mWrapped;

        public AnimatorListenerCompatWrapper(android.support.v4.animation.AnimatorListenerCompat wrapped, android.support.v4.animation.ValueAnimatorCompat valueAnimatorCompat) {
            this.mWrapped = wrapped;
            this.mValueAnimatorCompat = valueAnimatorCompat;
        }

        public void onAnimationStart(android.animation.Animator animation) {
            this.mWrapped.onAnimationStart(this.mValueAnimatorCompat);
        }

        public void onAnimationEnd(android.animation.Animator animation) {
            this.mWrapped.onAnimationEnd(this.mValueAnimatorCompat);
        }

        public void onAnimationCancel(android.animation.Animator animation) {
            this.mWrapped.onAnimationCancel(this.mValueAnimatorCompat);
        }

        public void onAnimationRepeat(android.animation.Animator animation) {
            this.mWrapped.onAnimationRepeat(this.mValueAnimatorCompat);
        }
    }

    static class HoneycombValueAnimatorCompat implements android.support.v4.animation.ValueAnimatorCompat {
        final android.animation.Animator mWrapped;

        public HoneycombValueAnimatorCompat(android.animation.Animator wrapped) {
            this.mWrapped = wrapped;
        }

        public void setTarget(android.view.View view) {
            this.mWrapped.setTarget(view);
        }

        public void addListener(android.support.v4.animation.AnimatorListenerCompat listener) {
            this.mWrapped.addListener(new android.support.v4.animation.HoneycombMr1AnimatorCompatProvider.AnimatorListenerCompatWrapper(listener, this));
        }

        public void setDuration(long duration) {
            this.mWrapped.setDuration(duration);
        }

        public void start() {
            this.mWrapped.start();
        }

        public void cancel() {
            this.mWrapped.cancel();
        }

        public void addUpdateListener(final android.support.v4.animation.AnimatorUpdateListenerCompat animatorUpdateListener) {
            if (this.mWrapped instanceof android.animation.ValueAnimator) {
                ((android.animation.ValueAnimator) this.mWrapped).addUpdateListener(new android.animation.ValueAnimator.AnimatorUpdateListener() {
                    public void onAnimationUpdate(android.animation.ValueAnimator animation) {
                        animatorUpdateListener.onAnimationUpdate(android.support.v4.animation.HoneycombMr1AnimatorCompatProvider.HoneycombValueAnimatorCompat.this);
                    }
                });
            }
        }

        public float getAnimatedFraction() {
            return ((android.animation.ValueAnimator) this.mWrapped).getAnimatedFraction();
        }
    }

    HoneycombMr1AnimatorCompatProvider() {
    }

    public android.support.v4.animation.ValueAnimatorCompat emptyValueAnimator() {
        return new android.support.v4.animation.HoneycombMr1AnimatorCompatProvider.HoneycombValueAnimatorCompat(android.animation.ValueAnimator.ofFloat(new float[]{0.0f, 1.0f}));
    }

    public void clearInterpolator(android.view.View view) {
        if (this.mDefaultInterpolator == null) {
            this.mDefaultInterpolator = new android.animation.ValueAnimator().getInterpolator();
        }
        view.animate().setInterpolator(this.mDefaultInterpolator);
    }
}
