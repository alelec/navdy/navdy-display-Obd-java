package android.support.v4.animation;

@android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
public interface ValueAnimatorCompat {
    void addListener(android.support.v4.animation.AnimatorListenerCompat animatorListenerCompat);

    void addUpdateListener(android.support.v4.animation.AnimatorUpdateListenerCompat animatorUpdateListenerCompat);

    void cancel();

    float getAnimatedFraction();

    void setDuration(long j);

    void setTarget(android.view.View view);

    void start();
}
