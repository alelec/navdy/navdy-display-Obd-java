package android.support.v4.animation;

@android.annotation.TargetApi(9)
@android.support.annotation.RequiresApi(9)
class GingerbreadAnimatorCompatProvider implements android.support.v4.animation.AnimatorProvider {

    private static class GingerbreadFloatValueAnimator implements android.support.v4.animation.ValueAnimatorCompat {
        /* access modifiers changed from: private */
        public long mDuration = 200;
        private boolean mEnded = false;
        /* access modifiers changed from: private */
        public float mFraction = 0.0f;
        java.util.List<android.support.v4.animation.AnimatorListenerCompat> mListeners = new java.util.ArrayList();
        /* access modifiers changed from: private */
        public java.lang.Runnable mLoopRunnable = new java.lang.Runnable() {
            public void run() {
                float fraction = (((float) (android.support.v4.animation.GingerbreadAnimatorCompatProvider.GingerbreadFloatValueAnimator.this.getTime() - android.support.v4.animation.GingerbreadAnimatorCompatProvider.GingerbreadFloatValueAnimator.this.mStartTime)) * 1.0f) / ((float) android.support.v4.animation.GingerbreadAnimatorCompatProvider.GingerbreadFloatValueAnimator.this.mDuration);
                if (fraction > 1.0f || android.support.v4.animation.GingerbreadAnimatorCompatProvider.GingerbreadFloatValueAnimator.this.mTarget.getParent() == null) {
                    fraction = 1.0f;
                }
                android.support.v4.animation.GingerbreadAnimatorCompatProvider.GingerbreadFloatValueAnimator.this.mFraction = fraction;
                android.support.v4.animation.GingerbreadAnimatorCompatProvider.GingerbreadFloatValueAnimator.this.notifyUpdateListeners();
                if (android.support.v4.animation.GingerbreadAnimatorCompatProvider.GingerbreadFloatValueAnimator.this.mFraction >= 1.0f) {
                    android.support.v4.animation.GingerbreadAnimatorCompatProvider.GingerbreadFloatValueAnimator.this.dispatchEnd();
                } else {
                    android.support.v4.animation.GingerbreadAnimatorCompatProvider.GingerbreadFloatValueAnimator.this.mTarget.postDelayed(android.support.v4.animation.GingerbreadAnimatorCompatProvider.GingerbreadFloatValueAnimator.this.mLoopRunnable, 16);
                }
            }
        };
        /* access modifiers changed from: private */
        public long mStartTime;
        private boolean mStarted = false;
        android.view.View mTarget;
        java.util.List<android.support.v4.animation.AnimatorUpdateListenerCompat> mUpdateListeners = new java.util.ArrayList();

        /* access modifiers changed from: private */
        public void notifyUpdateListeners() {
            for (int i = this.mUpdateListeners.size() - 1; i >= 0; i--) {
                ((android.support.v4.animation.AnimatorUpdateListenerCompat) this.mUpdateListeners.get(i)).onAnimationUpdate(this);
            }
        }

        public void setTarget(android.view.View view) {
            this.mTarget = view;
        }

        public void addListener(android.support.v4.animation.AnimatorListenerCompat listener) {
            this.mListeners.add(listener);
        }

        public void setDuration(long duration) {
            if (!this.mStarted) {
                this.mDuration = duration;
            }
        }

        public void start() {
            if (!this.mStarted) {
                this.mStarted = true;
                dispatchStart();
                this.mFraction = 0.0f;
                this.mStartTime = getTime();
                this.mTarget.postDelayed(this.mLoopRunnable, 16);
            }
        }

        /* access modifiers changed from: private */
        public long getTime() {
            return this.mTarget.getDrawingTime();
        }

        private void dispatchStart() {
            for (int i = this.mListeners.size() - 1; i >= 0; i--) {
                ((android.support.v4.animation.AnimatorListenerCompat) this.mListeners.get(i)).onAnimationStart(this);
            }
        }

        /* access modifiers changed from: private */
        public void dispatchEnd() {
            for (int i = this.mListeners.size() - 1; i >= 0; i--) {
                ((android.support.v4.animation.AnimatorListenerCompat) this.mListeners.get(i)).onAnimationEnd(this);
            }
        }

        private void dispatchCancel() {
            for (int i = this.mListeners.size() - 1; i >= 0; i--) {
                ((android.support.v4.animation.AnimatorListenerCompat) this.mListeners.get(i)).onAnimationCancel(this);
            }
        }

        public void cancel() {
            if (!this.mEnded) {
                this.mEnded = true;
                if (this.mStarted) {
                    dispatchCancel();
                }
                dispatchEnd();
            }
        }

        public void addUpdateListener(android.support.v4.animation.AnimatorUpdateListenerCompat animatorUpdateListener) {
            this.mUpdateListeners.add(animatorUpdateListener);
        }

        public float getAnimatedFraction() {
            return this.mFraction;
        }
    }

    GingerbreadAnimatorCompatProvider() {
    }

    public android.support.v4.animation.ValueAnimatorCompat emptyValueAnimator() {
        return new android.support.v4.animation.GingerbreadAnimatorCompatProvider.GingerbreadFloatValueAnimator();
    }

    public void clearInterpolator(android.view.View view) {
    }
}
