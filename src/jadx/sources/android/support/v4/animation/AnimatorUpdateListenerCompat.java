package android.support.v4.animation;

@android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
public interface AnimatorUpdateListenerCompat {
    void onAnimationUpdate(android.support.v4.animation.ValueAnimatorCompat valueAnimatorCompat);
}
