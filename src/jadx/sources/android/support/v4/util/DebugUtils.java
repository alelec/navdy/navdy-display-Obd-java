package android.support.v4.util;

@android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
public class DebugUtils {
    public static void buildShortClassTag(java.lang.Object cls, java.lang.StringBuilder out) {
        if (cls == null) {
            out.append("null");
            return;
        }
        java.lang.String simpleName = cls.getClass().getSimpleName();
        if (simpleName == null || simpleName.length() <= 0) {
            simpleName = cls.getClass().getName();
            int end = simpleName.lastIndexOf(46);
            if (end > 0) {
                simpleName = simpleName.substring(end + 1);
            }
        }
        out.append(simpleName);
        out.append(ch.qos.logback.core.CoreConstants.CURLY_LEFT);
        out.append(java.lang.Integer.toHexString(java.lang.System.identityHashCode(cls)));
    }
}
