package android.support.v4.util;

public final class ArraySet<E> implements java.util.Collection<E>, java.util.Set<E> {
    private static final int BASE_SIZE = 4;
    private static final int CACHE_SIZE = 10;
    private static final boolean DEBUG = false;
    private static final int[] INT = new int[0];
    private static final java.lang.Object[] OBJECT = new java.lang.Object[0];
    private static final java.lang.String TAG = "ArraySet";
    static java.lang.Object[] sBaseCache;
    static int sBaseCacheSize;
    static java.lang.Object[] sTwiceBaseCache;
    static int sTwiceBaseCacheSize;
    java.lang.Object[] mArray;
    android.support.v4.util.MapCollections<E, E> mCollections;
    int[] mHashes;
    final boolean mIdentityHashCode;
    int mSize;

    private int indexOf(java.lang.Object key, int hash) {
        int N = this.mSize;
        if (N == 0) {
            return -1;
        }
        int index = android.support.v4.util.ContainerHelpers.binarySearch(this.mHashes, N, hash);
        if (index < 0 || key.equals(this.mArray[index])) {
            return index;
        }
        int end = index + 1;
        while (end < N && this.mHashes[end] == hash) {
            if (key.equals(this.mArray[end])) {
                return end;
            }
            end++;
        }
        int i = index - 1;
        while (i >= 0 && this.mHashes[i] == hash) {
            if (key.equals(this.mArray[i])) {
                return i;
            }
            i--;
        }
        return end ^ -1;
    }

    private int indexOfNull() {
        int N = this.mSize;
        if (N == 0) {
            return -1;
        }
        int index = android.support.v4.util.ContainerHelpers.binarySearch(this.mHashes, N, 0);
        if (index < 0 || this.mArray[index] == null) {
            return index;
        }
        int end = index + 1;
        while (end < N && this.mHashes[end] == 0) {
            if (this.mArray[end] == null) {
                return end;
            }
            end++;
        }
        int i = index - 1;
        while (i >= 0 && this.mHashes[i] == 0) {
            if (this.mArray[i] == null) {
                return i;
            }
            i--;
        }
        return end ^ -1;
    }

    private void allocArrays(int size) {
        if (size == 8) {
            synchronized (android.support.v4.util.ArraySet.class) {
                if (sTwiceBaseCache != null) {
                    java.lang.Object[] array = sTwiceBaseCache;
                    this.mArray = array;
                    sTwiceBaseCache = (java.lang.Object[]) array[0];
                    this.mHashes = (int[]) array[1];
                    array[1] = null;
                    array[0] = null;
                    sTwiceBaseCacheSize--;
                    return;
                }
            }
        } else if (size == 4) {
            synchronized (android.support.v4.util.ArraySet.class) {
                if (sBaseCache != null) {
                    java.lang.Object[] array2 = sBaseCache;
                    this.mArray = array2;
                    sBaseCache = (java.lang.Object[]) array2[0];
                    this.mHashes = (int[]) array2[1];
                    array2[1] = null;
                    array2[0] = null;
                    sBaseCacheSize--;
                    return;
                }
            }
        }
        this.mHashes = new int[size];
        this.mArray = new java.lang.Object[size];
    }

    private static void freeArrays(int[] hashes, java.lang.Object[] array, int size) {
        if (hashes.length == 8) {
            synchronized (android.support.v4.util.ArraySet.class) {
                if (sTwiceBaseCacheSize < 10) {
                    array[0] = sTwiceBaseCache;
                    array[1] = hashes;
                    for (int i = size - 1; i >= 2; i--) {
                        array[i] = null;
                    }
                    sTwiceBaseCache = array;
                    sTwiceBaseCacheSize++;
                }
            }
        } else if (hashes.length == 4) {
            synchronized (android.support.v4.util.ArraySet.class) {
                if (sBaseCacheSize < 10) {
                    array[0] = sBaseCache;
                    array[1] = hashes;
                    for (int i2 = size - 1; i2 >= 2; i2--) {
                        array[i2] = null;
                    }
                    sBaseCache = array;
                    sBaseCacheSize++;
                }
            }
        }
    }

    public ArraySet() {
        this(0, false);
    }

    public ArraySet(int capacity) {
        this(capacity, false);
    }

    public ArraySet(int capacity, boolean identityHashCode) {
        this.mIdentityHashCode = identityHashCode;
        if (capacity == 0) {
            this.mHashes = INT;
            this.mArray = OBJECT;
        } else {
            allocArrays(capacity);
        }
        this.mSize = 0;
    }

    public ArraySet(android.support.v4.util.ArraySet<E> set) {
        this();
        if (set != null) {
            addAll(set);
        }
    }

    public ArraySet(java.util.Collection<E> set) {
        this();
        if (set != null) {
            addAll(set);
        }
    }

    public void clear() {
        if (this.mSize != 0) {
            freeArrays(this.mHashes, this.mArray, this.mSize);
            this.mHashes = INT;
            this.mArray = OBJECT;
            this.mSize = 0;
        }
    }

    public void ensureCapacity(int minimumCapacity) {
        if (this.mHashes.length < minimumCapacity) {
            int[] ohashes = this.mHashes;
            java.lang.Object[] oarray = this.mArray;
            allocArrays(minimumCapacity);
            if (this.mSize > 0) {
                java.lang.System.arraycopy(ohashes, 0, this.mHashes, 0, this.mSize);
                java.lang.System.arraycopy(oarray, 0, this.mArray, 0, this.mSize);
            }
            freeArrays(ohashes, oarray, this.mSize);
        }
    }

    public boolean contains(java.lang.Object key) {
        return indexOf(key) >= 0;
    }

    public int indexOf(java.lang.Object key) {
        if (key == null) {
            return indexOfNull();
        }
        return indexOf(key, this.mIdentityHashCode ? java.lang.System.identityHashCode(key) : key.hashCode());
    }

    public E valueAt(int index) {
        return this.mArray[index];
    }

    public boolean isEmpty() {
        return this.mSize <= 0;
    }

    public boolean add(E value) {
        int hash;
        int index;
        int n = 8;
        if (value == null) {
            hash = 0;
            index = indexOfNull();
        } else {
            hash = this.mIdentityHashCode ? java.lang.System.identityHashCode(value) : value.hashCode();
            index = indexOf(value, hash);
        }
        if (index >= 0) {
            return false;
        }
        int index2 = index ^ -1;
        if (this.mSize >= this.mHashes.length) {
            if (this.mSize >= 8) {
                n = this.mSize + (this.mSize >> 1);
            } else if (this.mSize < 4) {
                n = 4;
            }
            int[] ohashes = this.mHashes;
            java.lang.Object[] oarray = this.mArray;
            allocArrays(n);
            if (this.mHashes.length > 0) {
                java.lang.System.arraycopy(ohashes, 0, this.mHashes, 0, ohashes.length);
                java.lang.System.arraycopy(oarray, 0, this.mArray, 0, oarray.length);
            }
            freeArrays(ohashes, oarray, this.mSize);
        }
        if (index2 < this.mSize) {
            java.lang.System.arraycopy(this.mHashes, index2, this.mHashes, index2 + 1, this.mSize - index2);
            java.lang.System.arraycopy(this.mArray, index2, this.mArray, index2 + 1, this.mSize - index2);
        }
        this.mHashes[index2] = hash;
        this.mArray[index2] = value;
        this.mSize++;
        return true;
    }

    public void append(E value) {
        int index = this.mSize;
        int hash = value == null ? 0 : this.mIdentityHashCode ? java.lang.System.identityHashCode(value) : value.hashCode();
        if (index >= this.mHashes.length) {
            throw new java.lang.IllegalStateException("Array is full");
        } else if (index <= 0 || this.mHashes[index - 1] <= hash) {
            this.mSize = index + 1;
            this.mHashes[index] = hash;
            this.mArray[index] = value;
        } else {
            add(value);
        }
    }

    public void addAll(android.support.v4.util.ArraySet<? extends E> array) {
        int N = array.mSize;
        ensureCapacity(this.mSize + N);
        if (this.mSize != 0) {
            for (int i = 0; i < N; i++) {
                add(array.valueAt(i));
            }
        } else if (N > 0) {
            java.lang.System.arraycopy(array.mHashes, 0, this.mHashes, 0, N);
            java.lang.System.arraycopy(array.mArray, 0, this.mArray, 0, N);
            this.mSize = N;
        }
    }

    public boolean remove(java.lang.Object object) {
        int index = indexOf(object);
        if (index < 0) {
            return false;
        }
        removeAt(index);
        return true;
    }

    public E removeAt(int index) {
        int n = 8;
        java.lang.Object old = this.mArray[index];
        if (this.mSize <= 1) {
            freeArrays(this.mHashes, this.mArray, this.mSize);
            this.mHashes = INT;
            this.mArray = OBJECT;
            this.mSize = 0;
        } else if (this.mHashes.length <= 8 || this.mSize >= this.mHashes.length / 3) {
            this.mSize--;
            if (index < this.mSize) {
                java.lang.System.arraycopy(this.mHashes, index + 1, this.mHashes, index, this.mSize - index);
                java.lang.System.arraycopy(this.mArray, index + 1, this.mArray, index, this.mSize - index);
            }
            this.mArray[this.mSize] = null;
        } else {
            if (this.mSize > 8) {
                n = this.mSize + (this.mSize >> 1);
            }
            int[] ohashes = this.mHashes;
            java.lang.Object[] oarray = this.mArray;
            allocArrays(n);
            this.mSize--;
            if (index > 0) {
                java.lang.System.arraycopy(ohashes, 0, this.mHashes, 0, index);
                java.lang.System.arraycopy(oarray, 0, this.mArray, 0, index);
            }
            if (index < this.mSize) {
                java.lang.System.arraycopy(ohashes, index + 1, this.mHashes, index, this.mSize - index);
                java.lang.System.arraycopy(oarray, index + 1, this.mArray, index, this.mSize - index);
            }
        }
        return old;
    }

    public boolean removeAll(android.support.v4.util.ArraySet<? extends E> array) {
        int N = array.mSize;
        int originalSize = this.mSize;
        for (int i = 0; i < N; i++) {
            remove(array.valueAt(i));
        }
        return originalSize != this.mSize;
    }

    public int size() {
        return this.mSize;
    }

    public java.lang.Object[] toArray() {
        java.lang.Object[] result = new java.lang.Object[this.mSize];
        java.lang.System.arraycopy(this.mArray, 0, result, 0, this.mSize);
        return result;
    }

    public <T> T[] toArray(T[] array) {
        if (array.length < this.mSize) {
            array = (java.lang.Object[]) ((java.lang.Object[]) java.lang.reflect.Array.newInstance(array.getClass().getComponentType(), this.mSize));
        }
        java.lang.System.arraycopy(this.mArray, 0, array, 0, this.mSize);
        if (array.length > this.mSize) {
            array[this.mSize] = null;
        }
        return array;
    }

    public boolean equals(java.lang.Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof java.util.Set)) {
            return false;
        }
        java.util.Set<?> set = (java.util.Set) object;
        if (size() != set.size()) {
            return false;
        }
        int i = 0;
        while (i < this.mSize) {
            try {
                if (!set.contains(valueAt(i))) {
                    return false;
                }
                i++;
            } catch (java.lang.NullPointerException e) {
                return false;
            } catch (java.lang.ClassCastException e2) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        int[] hashes = this.mHashes;
        int result = 0;
        for (int i = 0; i < this.mSize; i++) {
            result += hashes[i];
        }
        return result;
    }

    public java.lang.String toString() {
        if (isEmpty()) {
            return "{}";
        }
        java.lang.StringBuilder buffer = new java.lang.StringBuilder(this.mSize * 14);
        buffer.append(ch.qos.logback.core.CoreConstants.CURLY_LEFT);
        for (int i = 0; i < this.mSize; i++) {
            if (i > 0) {
                buffer.append(", ");
            }
            java.lang.Object value = valueAt(i);
            if (value != this) {
                buffer.append(value);
            } else {
                buffer.append("(this Set)");
            }
        }
        buffer.append(ch.qos.logback.core.CoreConstants.CURLY_RIGHT);
        return buffer.toString();
    }

    private android.support.v4.util.MapCollections<E, E> getCollection() {
        if (this.mCollections == null) {
            this.mCollections = new android.support.v4.util.MapCollections<E, E>() {
                /* access modifiers changed from: protected */
                public int colGetSize() {
                    return android.support.v4.util.ArraySet.this.mSize;
                }

                /* access modifiers changed from: protected */
                public java.lang.Object colGetEntry(int index, int offset) {
                    return android.support.v4.util.ArraySet.this.mArray[index];
                }

                /* access modifiers changed from: protected */
                public int colIndexOfKey(java.lang.Object key) {
                    return android.support.v4.util.ArraySet.this.indexOf(key);
                }

                /* access modifiers changed from: protected */
                public int colIndexOfValue(java.lang.Object value) {
                    return android.support.v4.util.ArraySet.this.indexOf(value);
                }

                /* access modifiers changed from: protected */
                public java.util.Map<E, E> colGetMap() {
                    throw new java.lang.UnsupportedOperationException("not a map");
                }

                /* access modifiers changed from: protected */
                public void colPut(E key, E e) {
                    android.support.v4.util.ArraySet.this.add(key);
                }

                /* access modifiers changed from: protected */
                public E colSetValue(int index, E e) {
                    throw new java.lang.UnsupportedOperationException("not a map");
                }

                /* access modifiers changed from: protected */
                public void colRemoveAt(int index) {
                    android.support.v4.util.ArraySet.this.removeAt(index);
                }

                /* access modifiers changed from: protected */
                public void colClear() {
                    android.support.v4.util.ArraySet.this.clear();
                }
            };
        }
        return this.mCollections;
    }

    public java.util.Iterator<E> iterator() {
        return getCollection().getKeySet().iterator();
    }

    public boolean containsAll(java.util.Collection<?> collection) {
        for (java.lang.Object contains : collection) {
            if (!contains(contains)) {
                return false;
            }
        }
        return true;
    }

    public boolean addAll(java.util.Collection<? extends E> collection) {
        ensureCapacity(this.mSize + collection.size());
        boolean added = false;
        for (E value : collection) {
            added |= add(value);
        }
        return added;
    }

    public boolean removeAll(java.util.Collection<?> collection) {
        boolean removed = false;
        for (java.lang.Object value : collection) {
            removed |= remove(value);
        }
        return removed;
    }

    public boolean retainAll(java.util.Collection<?> collection) {
        boolean removed = false;
        for (int i = this.mSize - 1; i >= 0; i--) {
            if (!collection.contains(this.mArray[i])) {
                removeAt(i);
                removed = true;
            }
        }
        return removed;
    }
}
