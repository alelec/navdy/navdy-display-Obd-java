package android.support.v4.util;

abstract class MapCollections<K, V> {
    android.support.v4.util.MapCollections.EntrySet mEntrySet;
    android.support.v4.util.MapCollections.KeySet mKeySet;
    android.support.v4.util.MapCollections.ValuesCollection mValues;

    final class ArrayIterator<T> implements java.util.Iterator<T> {
        boolean mCanRemove = false;
        int mIndex;
        final int mOffset;
        int mSize;

        ArrayIterator(int offset) {
            this.mOffset = offset;
            this.mSize = android.support.v4.util.MapCollections.this.colGetSize();
        }

        public boolean hasNext() {
            return this.mIndex < this.mSize;
        }

        public T next() {
            java.lang.Object res = android.support.v4.util.MapCollections.this.colGetEntry(this.mIndex, this.mOffset);
            this.mIndex++;
            this.mCanRemove = true;
            return res;
        }

        public void remove() {
            if (!this.mCanRemove) {
                throw new java.lang.IllegalStateException();
            }
            this.mIndex--;
            this.mSize--;
            this.mCanRemove = false;
            android.support.v4.util.MapCollections.this.colRemoveAt(this.mIndex);
        }
    }

    final class EntrySet implements java.util.Set<java.util.Map.Entry<K, V>> {
        EntrySet() {
        }

        public boolean add(java.util.Map.Entry<K, V> entry) {
            throw new java.lang.UnsupportedOperationException();
        }

        public boolean addAll(java.util.Collection<? extends java.util.Map.Entry<K, V>> collection) {
            int oldSize = android.support.v4.util.MapCollections.this.colGetSize();
            for (java.util.Map.Entry<K, V> entry : collection) {
                android.support.v4.util.MapCollections.this.colPut(entry.getKey(), entry.getValue());
            }
            return oldSize != android.support.v4.util.MapCollections.this.colGetSize();
        }

        public void clear() {
            android.support.v4.util.MapCollections.this.colClear();
        }

        public boolean contains(java.lang.Object o) {
            if (!(o instanceof java.util.Map.Entry)) {
                return false;
            }
            java.util.Map.Entry<?, ?> e = (java.util.Map.Entry) o;
            int index = android.support.v4.util.MapCollections.this.colIndexOfKey(e.getKey());
            if (index >= 0) {
                return android.support.v4.util.ContainerHelpers.equal(android.support.v4.util.MapCollections.this.colGetEntry(index, 1), e.getValue());
            }
            return false;
        }

        public boolean containsAll(java.util.Collection<?> collection) {
            for (java.lang.Object contains : collection) {
                if (!contains(contains)) {
                    return false;
                }
            }
            return true;
        }

        public boolean isEmpty() {
            return android.support.v4.util.MapCollections.this.colGetSize() == 0;
        }

        public java.util.Iterator<java.util.Map.Entry<K, V>> iterator() {
            return new android.support.v4.util.MapCollections.MapIterator();
        }

        public boolean remove(java.lang.Object object) {
            throw new java.lang.UnsupportedOperationException();
        }

        public boolean removeAll(java.util.Collection<?> collection) {
            throw new java.lang.UnsupportedOperationException();
        }

        public boolean retainAll(java.util.Collection<?> collection) {
            throw new java.lang.UnsupportedOperationException();
        }

        public int size() {
            return android.support.v4.util.MapCollections.this.colGetSize();
        }

        public java.lang.Object[] toArray() {
            throw new java.lang.UnsupportedOperationException();
        }

        public <T> T[] toArray(T[] tArr) {
            throw new java.lang.UnsupportedOperationException();
        }

        public boolean equals(java.lang.Object object) {
            return android.support.v4.util.MapCollections.equalsSetHelper(this, object);
        }

        public int hashCode() {
            int hashCode;
            int result = 0;
            for (int i = android.support.v4.util.MapCollections.this.colGetSize() - 1; i >= 0; i--) {
                java.lang.Object key = android.support.v4.util.MapCollections.this.colGetEntry(i, 0);
                java.lang.Object value = android.support.v4.util.MapCollections.this.colGetEntry(i, 1);
                int hashCode2 = key == null ? 0 : key.hashCode();
                if (value == null) {
                    hashCode = 0;
                } else {
                    hashCode = value.hashCode();
                }
                result += hashCode ^ hashCode2;
            }
            return result;
        }
    }

    final class KeySet implements java.util.Set<K> {
        KeySet() {
        }

        public boolean add(K k) {
            throw new java.lang.UnsupportedOperationException();
        }

        public boolean addAll(java.util.Collection<? extends K> collection) {
            throw new java.lang.UnsupportedOperationException();
        }

        public void clear() {
            android.support.v4.util.MapCollections.this.colClear();
        }

        public boolean contains(java.lang.Object object) {
            return android.support.v4.util.MapCollections.this.colIndexOfKey(object) >= 0;
        }

        public boolean containsAll(java.util.Collection<?> collection) {
            return android.support.v4.util.MapCollections.containsAllHelper(android.support.v4.util.MapCollections.this.colGetMap(), collection);
        }

        public boolean isEmpty() {
            return android.support.v4.util.MapCollections.this.colGetSize() == 0;
        }

        public java.util.Iterator<K> iterator() {
            return new android.support.v4.util.MapCollections.ArrayIterator(0);
        }

        public boolean remove(java.lang.Object object) {
            int index = android.support.v4.util.MapCollections.this.colIndexOfKey(object);
            if (index < 0) {
                return false;
            }
            android.support.v4.util.MapCollections.this.colRemoveAt(index);
            return true;
        }

        public boolean removeAll(java.util.Collection<?> collection) {
            return android.support.v4.util.MapCollections.removeAllHelper(android.support.v4.util.MapCollections.this.colGetMap(), collection);
        }

        public boolean retainAll(java.util.Collection<?> collection) {
            return android.support.v4.util.MapCollections.retainAllHelper(android.support.v4.util.MapCollections.this.colGetMap(), collection);
        }

        public int size() {
            return android.support.v4.util.MapCollections.this.colGetSize();
        }

        public java.lang.Object[] toArray() {
            return android.support.v4.util.MapCollections.this.toArrayHelper(0);
        }

        public <T> T[] toArray(T[] array) {
            return android.support.v4.util.MapCollections.this.toArrayHelper(array, 0);
        }

        public boolean equals(java.lang.Object object) {
            return android.support.v4.util.MapCollections.equalsSetHelper(this, object);
        }

        public int hashCode() {
            int result = 0;
            for (int i = android.support.v4.util.MapCollections.this.colGetSize() - 1; i >= 0; i--) {
                java.lang.Object obj = android.support.v4.util.MapCollections.this.colGetEntry(i, 0);
                result += obj == null ? 0 : obj.hashCode();
            }
            return result;
        }
    }

    final class MapIterator implements java.util.Iterator<java.util.Map.Entry<K, V>>, java.util.Map.Entry<K, V> {
        int mEnd;
        boolean mEntryValid = false;
        int mIndex;

        MapIterator() {
            this.mEnd = android.support.v4.util.MapCollections.this.colGetSize() - 1;
            this.mIndex = -1;
        }

        public boolean hasNext() {
            return this.mIndex < this.mEnd;
        }

        public java.util.Map.Entry<K, V> next() {
            this.mIndex++;
            this.mEntryValid = true;
            return this;
        }

        public void remove() {
            if (!this.mEntryValid) {
                throw new java.lang.IllegalStateException();
            }
            android.support.v4.util.MapCollections.this.colRemoveAt(this.mIndex);
            this.mIndex--;
            this.mEnd--;
            this.mEntryValid = false;
        }

        public K getKey() {
            if (this.mEntryValid) {
                return android.support.v4.util.MapCollections.this.colGetEntry(this.mIndex, 0);
            }
            throw new java.lang.IllegalStateException("This container does not support retaining Map.Entry objects");
        }

        public V getValue() {
            if (this.mEntryValid) {
                return android.support.v4.util.MapCollections.this.colGetEntry(this.mIndex, 1);
            }
            throw new java.lang.IllegalStateException("This container does not support retaining Map.Entry objects");
        }

        public V setValue(V object) {
            if (this.mEntryValid) {
                return android.support.v4.util.MapCollections.this.colSetValue(this.mIndex, object);
            }
            throw new java.lang.IllegalStateException("This container does not support retaining Map.Entry objects");
        }

        public final boolean equals(java.lang.Object o) {
            boolean z = true;
            if (!this.mEntryValid) {
                throw new java.lang.IllegalStateException("This container does not support retaining Map.Entry objects");
            } else if (!(o instanceof java.util.Map.Entry)) {
                return false;
            } else {
                java.util.Map.Entry<?, ?> e = (java.util.Map.Entry) o;
                if (!android.support.v4.util.ContainerHelpers.equal(e.getKey(), android.support.v4.util.MapCollections.this.colGetEntry(this.mIndex, 0)) || !android.support.v4.util.ContainerHelpers.equal(e.getValue(), android.support.v4.util.MapCollections.this.colGetEntry(this.mIndex, 1))) {
                    z = false;
                }
                return z;
            }
        }

        public final int hashCode() {
            int i = 0;
            if (!this.mEntryValid) {
                throw new java.lang.IllegalStateException("This container does not support retaining Map.Entry objects");
            }
            java.lang.Object key = android.support.v4.util.MapCollections.this.colGetEntry(this.mIndex, 0);
            java.lang.Object value = android.support.v4.util.MapCollections.this.colGetEntry(this.mIndex, 1);
            int hashCode = key == null ? 0 : key.hashCode();
            if (value != null) {
                i = value.hashCode();
            }
            return i ^ hashCode;
        }

        public final java.lang.String toString() {
            return getKey() + "=" + getValue();
        }
    }

    final class ValuesCollection implements java.util.Collection<V> {
        ValuesCollection() {
        }

        public boolean add(V v) {
            throw new java.lang.UnsupportedOperationException();
        }

        public boolean addAll(java.util.Collection<? extends V> collection) {
            throw new java.lang.UnsupportedOperationException();
        }

        public void clear() {
            android.support.v4.util.MapCollections.this.colClear();
        }

        public boolean contains(java.lang.Object object) {
            return android.support.v4.util.MapCollections.this.colIndexOfValue(object) >= 0;
        }

        public boolean containsAll(java.util.Collection<?> collection) {
            for (java.lang.Object contains : collection) {
                if (!contains(contains)) {
                    return false;
                }
            }
            return true;
        }

        public boolean isEmpty() {
            return android.support.v4.util.MapCollections.this.colGetSize() == 0;
        }

        public java.util.Iterator<V> iterator() {
            return new android.support.v4.util.MapCollections.ArrayIterator(1);
        }

        public boolean remove(java.lang.Object object) {
            int index = android.support.v4.util.MapCollections.this.colIndexOfValue(object);
            if (index < 0) {
                return false;
            }
            android.support.v4.util.MapCollections.this.colRemoveAt(index);
            return true;
        }

        public boolean removeAll(java.util.Collection<?> collection) {
            int N = android.support.v4.util.MapCollections.this.colGetSize();
            boolean changed = false;
            int i = 0;
            while (i < N) {
                if (collection.contains(android.support.v4.util.MapCollections.this.colGetEntry(i, 1))) {
                    android.support.v4.util.MapCollections.this.colRemoveAt(i);
                    i--;
                    N--;
                    changed = true;
                }
                i++;
            }
            return changed;
        }

        public boolean retainAll(java.util.Collection<?> collection) {
            int N = android.support.v4.util.MapCollections.this.colGetSize();
            boolean changed = false;
            int i = 0;
            while (i < N) {
                if (!collection.contains(android.support.v4.util.MapCollections.this.colGetEntry(i, 1))) {
                    android.support.v4.util.MapCollections.this.colRemoveAt(i);
                    i--;
                    N--;
                    changed = true;
                }
                i++;
            }
            return changed;
        }

        public int size() {
            return android.support.v4.util.MapCollections.this.colGetSize();
        }

        public java.lang.Object[] toArray() {
            return android.support.v4.util.MapCollections.this.toArrayHelper(1);
        }

        public <T> T[] toArray(T[] array) {
            return android.support.v4.util.MapCollections.this.toArrayHelper(array, 1);
        }
    }

    /* access modifiers changed from: protected */
    public abstract void colClear();

    /* access modifiers changed from: protected */
    public abstract java.lang.Object colGetEntry(int i, int i2);

    /* access modifiers changed from: protected */
    public abstract java.util.Map<K, V> colGetMap();

    /* access modifiers changed from: protected */
    public abstract int colGetSize();

    /* access modifiers changed from: protected */
    public abstract int colIndexOfKey(java.lang.Object obj);

    /* access modifiers changed from: protected */
    public abstract int colIndexOfValue(java.lang.Object obj);

    /* access modifiers changed from: protected */
    public abstract void colPut(K k, V v);

    /* access modifiers changed from: protected */
    public abstract void colRemoveAt(int i);

    /* access modifiers changed from: protected */
    public abstract V colSetValue(int i, V v);

    MapCollections() {
    }

    public static <K, V> boolean containsAllHelper(java.util.Map<K, V> map, java.util.Collection<?> collection) {
        for (java.lang.Object containsKey : collection) {
            if (!map.containsKey(containsKey)) {
                return false;
            }
        }
        return true;
    }

    public static <K, V> boolean removeAllHelper(java.util.Map<K, V> map, java.util.Collection<?> collection) {
        int oldSize = map.size();
        for (java.lang.Object remove : collection) {
            map.remove(remove);
        }
        return oldSize != map.size();
    }

    public static <K, V> boolean retainAllHelper(java.util.Map<K, V> map, java.util.Collection<?> collection) {
        int oldSize = map.size();
        java.util.Iterator<K> it = map.keySet().iterator();
        while (it.hasNext()) {
            if (!collection.contains(it.next())) {
                it.remove();
            }
        }
        return oldSize != map.size();
    }

    public java.lang.Object[] toArrayHelper(int offset) {
        int N = colGetSize();
        java.lang.Object[] result = new java.lang.Object[N];
        for (int i = 0; i < N; i++) {
            result[i] = colGetEntry(i, offset);
        }
        return result;
    }

    public <T> T[] toArrayHelper(T[] array, int offset) {
        int N = colGetSize();
        if (array.length < N) {
            array = (java.lang.Object[]) ((java.lang.Object[]) java.lang.reflect.Array.newInstance(array.getClass().getComponentType(), N));
        }
        for (int i = 0; i < N; i++) {
            array[i] = colGetEntry(i, offset);
        }
        if (array.length > N) {
            array[N] = null;
        }
        return array;
    }

    public static <T> boolean equalsSetHelper(java.util.Set<T> set, java.lang.Object object) {
        boolean z = true;
        if (set == object) {
            return true;
        }
        if (!(object instanceof java.util.Set)) {
            return false;
        }
        java.util.Set<?> s = (java.util.Set) object;
        try {
            if (set.size() != s.size() || !set.containsAll(s)) {
                z = false;
            }
            return z;
        } catch (java.lang.ClassCastException | java.lang.NullPointerException e) {
            return false;
        }
    }

    public java.util.Set<java.util.Map.Entry<K, V>> getEntrySet() {
        if (this.mEntrySet == null) {
            this.mEntrySet = new android.support.v4.util.MapCollections.EntrySet<>();
        }
        return this.mEntrySet;
    }

    public java.util.Set<K> getKeySet() {
        if (this.mKeySet == null) {
            this.mKeySet = new android.support.v4.util.MapCollections.KeySet<>();
        }
        return this.mKeySet;
    }

    public java.util.Collection<V> getValues() {
        if (this.mValues == null) {
            this.mValues = new android.support.v4.util.MapCollections.ValuesCollection<>();
        }
        return this.mValues;
    }
}
