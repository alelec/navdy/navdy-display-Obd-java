package android.support.v4.util;

public class Pair<F, S> {
    public final F first;
    public final S second;

    public Pair(F first2, S second2) {
        this.first = first2;
        this.second = second2;
    }

    public boolean equals(java.lang.Object o) {
        if (!(o instanceof android.support.v4.util.Pair)) {
            return false;
        }
        android.support.v4.util.Pair<?, ?> p = (android.support.v4.util.Pair) o;
        if (!objectsEqual(p.first, this.first) || !objectsEqual(p.second, this.second)) {
            return false;
        }
        return true;
    }

    private static boolean objectsEqual(java.lang.Object a, java.lang.Object b) {
        return a == b || (a != null && a.equals(b));
    }

    public int hashCode() {
        int i = 0;
        int hashCode = this.first == null ? 0 : this.first.hashCode();
        if (this.second != null) {
            i = this.second.hashCode();
        }
        return hashCode ^ i;
    }

    public java.lang.String toString() {
        return "Pair{" + java.lang.String.valueOf(this.first) + " " + java.lang.String.valueOf(this.second) + "}";
    }

    public static <A, B> android.support.v4.util.Pair<A, B> create(A a, B b) {
        return new android.support.v4.util.Pair<>(a, b);
    }
}
