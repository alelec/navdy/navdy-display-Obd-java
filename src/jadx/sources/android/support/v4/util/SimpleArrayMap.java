package android.support.v4.util;

public class SimpleArrayMap<K, V> {
    private static final int BASE_SIZE = 4;
    private static final int CACHE_SIZE = 10;
    private static final boolean DEBUG = false;
    private static final java.lang.String TAG = "ArrayMap";
    static java.lang.Object[] mBaseCache;
    static int mBaseCacheSize;
    static java.lang.Object[] mTwiceBaseCache;
    static int mTwiceBaseCacheSize;
    java.lang.Object[] mArray;
    int[] mHashes;
    int mSize;

    /* access modifiers changed from: 0000 */
    public int indexOf(java.lang.Object key, int hash) {
        int N = this.mSize;
        if (N == 0) {
            return -1;
        }
        int index = android.support.v4.util.ContainerHelpers.binarySearch(this.mHashes, N, hash);
        if (index < 0 || key.equals(this.mArray[index << 1])) {
            return index;
        }
        int end = index + 1;
        while (end < N && this.mHashes[end] == hash) {
            if (key.equals(this.mArray[end << 1])) {
                return end;
            }
            end++;
        }
        int i = index - 1;
        while (i >= 0 && this.mHashes[i] == hash) {
            if (key.equals(this.mArray[i << 1])) {
                return i;
            }
            i--;
        }
        return end ^ -1;
    }

    /* access modifiers changed from: 0000 */
    public int indexOfNull() {
        int N = this.mSize;
        if (N == 0) {
            return -1;
        }
        int index = android.support.v4.util.ContainerHelpers.binarySearch(this.mHashes, N, 0);
        if (index < 0 || this.mArray[index << 1] == null) {
            return index;
        }
        int end = index + 1;
        while (end < N && this.mHashes[end] == 0) {
            if (this.mArray[end << 1] == null) {
                return end;
            }
            end++;
        }
        int i = index - 1;
        while (i >= 0 && this.mHashes[i] == 0) {
            if (this.mArray[i << 1] == null) {
                return i;
            }
            i--;
        }
        return end ^ -1;
    }

    private void allocArrays(int size) {
        if (size == 8) {
            synchronized (android.support.v4.util.ArrayMap.class) {
                if (mTwiceBaseCache != null) {
                    java.lang.Object[] array = mTwiceBaseCache;
                    this.mArray = array;
                    mTwiceBaseCache = (java.lang.Object[]) array[0];
                    this.mHashes = (int[]) array[1];
                    array[1] = null;
                    array[0] = null;
                    mTwiceBaseCacheSize--;
                    return;
                }
            }
        } else if (size == 4) {
            synchronized (android.support.v4.util.ArrayMap.class) {
                if (mBaseCache != null) {
                    java.lang.Object[] array2 = mBaseCache;
                    this.mArray = array2;
                    mBaseCache = (java.lang.Object[]) array2[0];
                    this.mHashes = (int[]) array2[1];
                    array2[1] = null;
                    array2[0] = null;
                    mBaseCacheSize--;
                    return;
                }
            }
        }
        this.mHashes = new int[size];
        this.mArray = new java.lang.Object[(size << 1)];
    }

    private static void freeArrays(int[] hashes, java.lang.Object[] array, int size) {
        if (hashes.length == 8) {
            synchronized (android.support.v4.util.ArrayMap.class) {
                if (mTwiceBaseCacheSize < 10) {
                    array[0] = mTwiceBaseCache;
                    array[1] = hashes;
                    for (int i = (size << 1) - 1; i >= 2; i--) {
                        array[i] = null;
                    }
                    mTwiceBaseCache = array;
                    mTwiceBaseCacheSize++;
                }
            }
        } else if (hashes.length == 4) {
            synchronized (android.support.v4.util.ArrayMap.class) {
                if (mBaseCacheSize < 10) {
                    array[0] = mBaseCache;
                    array[1] = hashes;
                    for (int i2 = (size << 1) - 1; i2 >= 2; i2--) {
                        array[i2] = null;
                    }
                    mBaseCache = array;
                    mBaseCacheSize++;
                }
            }
        }
    }

    public SimpleArrayMap() {
        this.mHashes = android.support.v4.util.ContainerHelpers.EMPTY_INTS;
        this.mArray = android.support.v4.util.ContainerHelpers.EMPTY_OBJECTS;
        this.mSize = 0;
    }

    public SimpleArrayMap(int capacity) {
        if (capacity == 0) {
            this.mHashes = android.support.v4.util.ContainerHelpers.EMPTY_INTS;
            this.mArray = android.support.v4.util.ContainerHelpers.EMPTY_OBJECTS;
        } else {
            allocArrays(capacity);
        }
        this.mSize = 0;
    }

    public SimpleArrayMap(android.support.v4.util.SimpleArrayMap map) {
        this();
        if (map != null) {
            putAll(map);
        }
    }

    public void clear() {
        if (this.mSize != 0) {
            freeArrays(this.mHashes, this.mArray, this.mSize);
            this.mHashes = android.support.v4.util.ContainerHelpers.EMPTY_INTS;
            this.mArray = android.support.v4.util.ContainerHelpers.EMPTY_OBJECTS;
            this.mSize = 0;
        }
    }

    public void ensureCapacity(int minimumCapacity) {
        if (this.mHashes.length < minimumCapacity) {
            int[] ohashes = this.mHashes;
            java.lang.Object[] oarray = this.mArray;
            allocArrays(minimumCapacity);
            if (this.mSize > 0) {
                java.lang.System.arraycopy(ohashes, 0, this.mHashes, 0, this.mSize);
                java.lang.System.arraycopy(oarray, 0, this.mArray, 0, this.mSize << 1);
            }
            freeArrays(ohashes, oarray, this.mSize);
        }
    }

    public boolean containsKey(java.lang.Object key) {
        return indexOfKey(key) >= 0;
    }

    public int indexOfKey(java.lang.Object key) {
        return key == null ? indexOfNull() : indexOf(key, key.hashCode());
    }

    /* access modifiers changed from: 0000 */
    public int indexOfValue(java.lang.Object value) {
        int N = this.mSize * 2;
        java.lang.Object[] array = this.mArray;
        if (value == null) {
            for (int i = 1; i < N; i += 2) {
                if (array[i] == null) {
                    return i >> 1;
                }
            }
        } else {
            for (int i2 = 1; i2 < N; i2 += 2) {
                if (value.equals(array[i2])) {
                    return i2 >> 1;
                }
            }
        }
        return -1;
    }

    public boolean containsValue(java.lang.Object value) {
        return indexOfValue(value) >= 0;
    }

    public V get(java.lang.Object key) {
        int index = indexOfKey(key);
        if (index >= 0) {
            return this.mArray[(index << 1) + 1];
        }
        return null;
    }

    public K keyAt(int index) {
        return this.mArray[index << 1];
    }

    public V valueAt(int index) {
        return this.mArray[(index << 1) + 1];
    }

    public V setValueAt(int index, V value) {
        int index2 = (index << 1) + 1;
        V old = this.mArray[index2];
        this.mArray[index2] = value;
        return old;
    }

    public boolean isEmpty() {
        return this.mSize <= 0;
    }

    public V put(K key, V value) {
        int hash;
        int index;
        int n = 8;
        if (key == null) {
            hash = 0;
            index = indexOfNull();
        } else {
            hash = key.hashCode();
            index = indexOf(key, hash);
        }
        if (index >= 0) {
            int index2 = (index << 1) + 1;
            V old = this.mArray[index2];
            this.mArray[index2] = value;
            return old;
        }
        int index3 = index ^ -1;
        if (this.mSize >= this.mHashes.length) {
            if (this.mSize >= 8) {
                n = this.mSize + (this.mSize >> 1);
            } else if (this.mSize < 4) {
                n = 4;
            }
            int[] ohashes = this.mHashes;
            java.lang.Object[] oarray = this.mArray;
            allocArrays(n);
            if (this.mHashes.length > 0) {
                java.lang.System.arraycopy(ohashes, 0, this.mHashes, 0, ohashes.length);
                java.lang.System.arraycopy(oarray, 0, this.mArray, 0, oarray.length);
            }
            freeArrays(ohashes, oarray, this.mSize);
        }
        if (index3 < this.mSize) {
            java.lang.System.arraycopy(this.mHashes, index3, this.mHashes, index3 + 1, this.mSize - index3);
            java.lang.System.arraycopy(this.mArray, index3 << 1, this.mArray, (index3 + 1) << 1, (this.mSize - index3) << 1);
        }
        this.mHashes[index3] = hash;
        this.mArray[index3 << 1] = key;
        this.mArray[(index3 << 1) + 1] = value;
        this.mSize++;
        return null;
    }

    public void putAll(android.support.v4.util.SimpleArrayMap<? extends K, ? extends V> array) {
        int N = array.mSize;
        ensureCapacity(this.mSize + N);
        if (this.mSize != 0) {
            for (int i = 0; i < N; i++) {
                put(array.keyAt(i), array.valueAt(i));
            }
        } else if (N > 0) {
            java.lang.System.arraycopy(array.mHashes, 0, this.mHashes, 0, N);
            java.lang.System.arraycopy(array.mArray, 0, this.mArray, 0, N << 1);
            this.mSize = N;
        }
    }

    public V remove(java.lang.Object key) {
        int index = indexOfKey(key);
        if (index >= 0) {
            return removeAt(index);
        }
        return null;
    }

    public V removeAt(int index) {
        int n = 8;
        java.lang.Object old = this.mArray[(index << 1) + 1];
        if (this.mSize <= 1) {
            freeArrays(this.mHashes, this.mArray, this.mSize);
            this.mHashes = android.support.v4.util.ContainerHelpers.EMPTY_INTS;
            this.mArray = android.support.v4.util.ContainerHelpers.EMPTY_OBJECTS;
            this.mSize = 0;
        } else if (this.mHashes.length <= 8 || this.mSize >= this.mHashes.length / 3) {
            this.mSize--;
            if (index < this.mSize) {
                java.lang.System.arraycopy(this.mHashes, index + 1, this.mHashes, index, this.mSize - index);
                java.lang.System.arraycopy(this.mArray, (index + 1) << 1, this.mArray, index << 1, (this.mSize - index) << 1);
            }
            this.mArray[this.mSize << 1] = null;
            this.mArray[(this.mSize << 1) + 1] = null;
        } else {
            if (this.mSize > 8) {
                n = this.mSize + (this.mSize >> 1);
            }
            int[] ohashes = this.mHashes;
            java.lang.Object[] oarray = this.mArray;
            allocArrays(n);
            this.mSize--;
            if (index > 0) {
                java.lang.System.arraycopy(ohashes, 0, this.mHashes, 0, index);
                java.lang.System.arraycopy(oarray, 0, this.mArray, 0, index << 1);
            }
            if (index < this.mSize) {
                java.lang.System.arraycopy(ohashes, index + 1, this.mHashes, index, this.mSize - index);
                java.lang.System.arraycopy(oarray, (index + 1) << 1, this.mArray, index << 1, (this.mSize - index) << 1);
            }
        }
        return old;
    }

    public int size() {
        return this.mSize;
    }

    public boolean equals(java.lang.Object object) {
        if (this == object) {
            return true;
        }
        if (object instanceof android.support.v4.util.SimpleArrayMap) {
            android.support.v4.util.SimpleArrayMap<?, ?> map = (android.support.v4.util.SimpleArrayMap) object;
            if (size() != map.size()) {
                return false;
            }
            int i = 0;
            while (i < this.mSize) {
                try {
                    K key = keyAt(i);
                    V mine = valueAt(i);
                    java.lang.Object theirs = map.get(key);
                    if (mine == null) {
                        if (theirs != null || !map.containsKey(key)) {
                            return false;
                        }
                    } else if (!mine.equals(theirs)) {
                        return false;
                    }
                    i++;
                } catch (java.lang.NullPointerException e) {
                    return false;
                } catch (java.lang.ClassCastException e2) {
                    return false;
                }
            }
            return true;
        } else if (!(object instanceof java.util.Map)) {
            return false;
        } else {
            java.util.Map<?, ?> map2 = (java.util.Map) object;
            if (size() != map2.size()) {
                return false;
            }
            int i2 = 0;
            while (i2 < this.mSize) {
                try {
                    K key2 = keyAt(i2);
                    V mine2 = valueAt(i2);
                    java.lang.Object theirs2 = map2.get(key2);
                    if (mine2 == null) {
                        if (theirs2 != null || !map2.containsKey(key2)) {
                            return false;
                        }
                    } else if (!mine2.equals(theirs2)) {
                        return false;
                    }
                    i2++;
                } catch (java.lang.NullPointerException e3) {
                    return false;
                } catch (java.lang.ClassCastException e4) {
                    return false;
                }
            }
            return true;
        }
    }

    public int hashCode() {
        int[] hashes = this.mHashes;
        java.lang.Object[] array = this.mArray;
        int result = 0;
        int i = 0;
        int v = 1;
        int s = this.mSize;
        while (i < s) {
            java.lang.Object value = array[v];
            result += (value == null ? 0 : value.hashCode()) ^ hashes[i];
            i++;
            v += 2;
        }
        return result;
    }

    public java.lang.String toString() {
        if (isEmpty()) {
            return "{}";
        }
        java.lang.StringBuilder buffer = new java.lang.StringBuilder(this.mSize * 28);
        buffer.append(ch.qos.logback.core.CoreConstants.CURLY_LEFT);
        for (int i = 0; i < this.mSize; i++) {
            if (i > 0) {
                buffer.append(", ");
            }
            java.lang.Object key = keyAt(i);
            if (key != this) {
                buffer.append(key);
            } else {
                buffer.append("(this Map)");
            }
            buffer.append('=');
            java.lang.Object value = valueAt(i);
            if (value != this) {
                buffer.append(value);
            } else {
                buffer.append("(this Map)");
            }
        }
        buffer.append(ch.qos.logback.core.CoreConstants.CURLY_RIGHT);
        return buffer.toString();
    }
}
