package android.support.v4.util;

public class ArrayMap<K, V> extends android.support.v4.util.SimpleArrayMap<K, V> implements java.util.Map<K, V> {
    android.support.v4.util.MapCollections<K, V> mCollections;

    public ArrayMap() {
    }

    public ArrayMap(int capacity) {
        super(capacity);
    }

    public ArrayMap(android.support.v4.util.SimpleArrayMap map) {
        super(map);
    }

    private android.support.v4.util.MapCollections<K, V> getCollection() {
        if (this.mCollections == null) {
            this.mCollections = new android.support.v4.util.MapCollections<K, V>() {
                /* access modifiers changed from: protected */
                public int colGetSize() {
                    return android.support.v4.util.ArrayMap.this.mSize;
                }

                /* access modifiers changed from: protected */
                public java.lang.Object colGetEntry(int index, int offset) {
                    return android.support.v4.util.ArrayMap.this.mArray[(index << 1) + offset];
                }

                /* access modifiers changed from: protected */
                public int colIndexOfKey(java.lang.Object key) {
                    return android.support.v4.util.ArrayMap.this.indexOfKey(key);
                }

                /* access modifiers changed from: protected */
                public int colIndexOfValue(java.lang.Object value) {
                    return android.support.v4.util.ArrayMap.this.indexOfValue(value);
                }

                /* access modifiers changed from: protected */
                public java.util.Map<K, V> colGetMap() {
                    return android.support.v4.util.ArrayMap.this;
                }

                /* access modifiers changed from: protected */
                public void colPut(K key, V value) {
                    android.support.v4.util.ArrayMap.this.put(key, value);
                }

                /* access modifiers changed from: protected */
                public V colSetValue(int index, V value) {
                    return android.support.v4.util.ArrayMap.this.setValueAt(index, value);
                }

                /* access modifiers changed from: protected */
                public void colRemoveAt(int index) {
                    android.support.v4.util.ArrayMap.this.removeAt(index);
                }

                /* access modifiers changed from: protected */
                public void colClear() {
                    android.support.v4.util.ArrayMap.this.clear();
                }
            };
        }
        return this.mCollections;
    }

    public boolean containsAll(java.util.Collection<?> collection) {
        return android.support.v4.util.MapCollections.containsAllHelper(this, collection);
    }

    public void putAll(java.util.Map<? extends K, ? extends V> map) {
        ensureCapacity(this.mSize + map.size());
        for (java.util.Map.Entry<? extends K, ? extends V> entry : map.entrySet()) {
            put(entry.getKey(), entry.getValue());
        }
    }

    public boolean removeAll(java.util.Collection<?> collection) {
        return android.support.v4.util.MapCollections.removeAllHelper(this, collection);
    }

    public boolean retainAll(java.util.Collection<?> collection) {
        return android.support.v4.util.MapCollections.retainAllHelper(this, collection);
    }

    public java.util.Set<java.util.Map.Entry<K, V>> entrySet() {
        return getCollection().getEntrySet();
    }

    public java.util.Set<K> keySet() {
        return getCollection().getKeySet();
    }

    public java.util.Collection<V> values() {
        return getCollection().getValues();
    }
}
