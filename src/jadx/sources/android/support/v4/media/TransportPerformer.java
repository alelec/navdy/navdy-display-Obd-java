package android.support.v4.media;

@java.lang.Deprecated
public abstract class TransportPerformer {
    static final int AUDIOFOCUS_GAIN = 1;
    static final int AUDIOFOCUS_GAIN_TRANSIENT = 2;
    static final int AUDIOFOCUS_GAIN_TRANSIENT_MAY_DUCK = 3;
    static final int AUDIOFOCUS_LOSS = -1;
    static final int AUDIOFOCUS_LOSS_TRANSIENT = -2;
    static final int AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK = -3;

    @java.lang.Deprecated
    public abstract long onGetCurrentPosition();

    @java.lang.Deprecated
    public abstract long onGetDuration();

    @java.lang.Deprecated
    public abstract boolean onIsPlaying();

    @java.lang.Deprecated
    public abstract void onPause();

    @java.lang.Deprecated
    public abstract void onSeekTo(long j);

    @java.lang.Deprecated
    public abstract void onStart();

    @java.lang.Deprecated
    public abstract void onStop();

    @java.lang.Deprecated
    public int onGetBufferPercentage() {
        return 100;
    }

    @java.lang.Deprecated
    public int onGetTransportControlFlags() {
        return 60;
    }

    @java.lang.Deprecated
    public boolean onMediaButtonDown(int keyCode, android.view.KeyEvent event) {
        switch (keyCode) {
            case 79:
            case 85:
                if (!onIsPlaying()) {
                    onStart();
                    break;
                } else {
                    onPause();
                    break;
                }
            case 86:
                onStop();
                break;
            case android.support.v4.media.TransportMediator.KEYCODE_MEDIA_PLAY /*126*/:
                onStart();
                break;
            case android.support.v4.media.TransportMediator.KEYCODE_MEDIA_PAUSE /*127*/:
                onPause();
                break;
        }
        return true;
    }

    @java.lang.Deprecated
    public boolean onMediaButtonUp(int keyCode, android.view.KeyEvent event) {
        return true;
    }

    @java.lang.Deprecated
    public void onAudioFocusChange(int focusChange) {
        int keyCode = 0;
        switch (focusChange) {
            case -1:
                keyCode = android.support.v4.media.TransportMediator.KEYCODE_MEDIA_PAUSE;
                break;
        }
        if (keyCode != 0) {
            long now = android.os.SystemClock.uptimeMillis();
            onMediaButtonDown(keyCode, new android.view.KeyEvent(now, now, 0, keyCode, 0));
            onMediaButtonUp(keyCode, new android.view.KeyEvent(now, now, 1, keyCode, 0));
        }
    }
}
