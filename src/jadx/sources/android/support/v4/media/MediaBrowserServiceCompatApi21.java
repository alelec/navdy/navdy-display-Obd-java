package android.support.v4.media;

@android.annotation.TargetApi(21)
@android.support.annotation.RequiresApi(21)
class MediaBrowserServiceCompatApi21 {

    static class BrowserRoot {
        final android.os.Bundle mExtras;
        final java.lang.String mRootId;

        BrowserRoot(java.lang.String rootId, android.os.Bundle extras) {
            this.mRootId = rootId;
            this.mExtras = extras;
        }
    }

    static class MediaBrowserServiceAdaptor extends android.service.media.MediaBrowserService {
        final android.support.v4.media.MediaBrowserServiceCompatApi21.ServiceCompatProxy mServiceProxy;

        MediaBrowserServiceAdaptor(android.content.Context context, android.support.v4.media.MediaBrowserServiceCompatApi21.ServiceCompatProxy serviceWrapper) {
            attachBaseContext(context);
            this.mServiceProxy = serviceWrapper;
        }

        public android.service.media.MediaBrowserService.BrowserRoot onGetRoot(java.lang.String clientPackageName, int clientUid, android.os.Bundle rootHints) {
            android.support.v4.media.MediaBrowserServiceCompatApi21.BrowserRoot browserRoot = this.mServiceProxy.onGetRoot(clientPackageName, clientUid, rootHints);
            if (browserRoot == null) {
                return null;
            }
            return new android.service.media.MediaBrowserService.BrowserRoot(browserRoot.mRootId, browserRoot.mExtras);
        }

        public void onLoadChildren(java.lang.String parentId, android.service.media.MediaBrowserService.Result<java.util.List<android.media.browse.MediaBrowser.MediaItem>> result) {
            this.mServiceProxy.onLoadChildren(parentId, new android.support.v4.media.MediaBrowserServiceCompatApi21.ResultWrapper(result));
        }
    }

    static class ResultWrapper<T> {
        android.service.media.MediaBrowserService.Result mResultObj;

        ResultWrapper(android.service.media.MediaBrowserService.Result result) {
            this.mResultObj = result;
        }

        public void sendResult(T result) {
            if (result instanceof java.util.List) {
                this.mResultObj.sendResult(parcelListToItemList((java.util.List) result));
            } else if (result instanceof android.os.Parcel) {
                android.os.Parcel parcel = (android.os.Parcel) result;
                parcel.setDataPosition(0);
                this.mResultObj.sendResult(android.media.browse.MediaBrowser.MediaItem.CREATOR.createFromParcel(parcel));
                parcel.recycle();
            } else {
                this.mResultObj.sendResult(null);
            }
        }

        public void detach() {
            this.mResultObj.detach();
        }

        /* access modifiers changed from: 0000 */
        public java.util.List<android.media.browse.MediaBrowser.MediaItem> parcelListToItemList(java.util.List<android.os.Parcel> parcelList) {
            if (parcelList == null) {
                return null;
            }
            java.util.List<android.media.browse.MediaBrowser.MediaItem> items = new java.util.ArrayList<>();
            for (android.os.Parcel parcel : parcelList) {
                parcel.setDataPosition(0);
                items.add(android.media.browse.MediaBrowser.MediaItem.CREATOR.createFromParcel(parcel));
                parcel.recycle();
            }
            return items;
        }
    }

    public interface ServiceCompatProxy {
        android.support.v4.media.MediaBrowserServiceCompatApi21.BrowserRoot onGetRoot(java.lang.String str, int i, android.os.Bundle bundle);

        void onLoadChildren(java.lang.String str, android.support.v4.media.MediaBrowserServiceCompatApi21.ResultWrapper<java.util.List<android.os.Parcel>> resultWrapper);
    }

    MediaBrowserServiceCompatApi21() {
    }

    public static java.lang.Object createService(android.content.Context context, android.support.v4.media.MediaBrowserServiceCompatApi21.ServiceCompatProxy serviceProxy) {
        return new android.support.v4.media.MediaBrowserServiceCompatApi21.MediaBrowserServiceAdaptor(context, serviceProxy);
    }

    public static void onCreate(java.lang.Object serviceObj) {
        ((android.service.media.MediaBrowserService) serviceObj).onCreate();
    }

    public static android.os.IBinder onBind(java.lang.Object serviceObj, android.content.Intent intent) {
        return ((android.service.media.MediaBrowserService) serviceObj).onBind(intent);
    }

    public static void setSessionToken(java.lang.Object serviceObj, java.lang.Object token) {
        ((android.service.media.MediaBrowserService) serviceObj).setSessionToken((android.media.session.MediaSession.Token) token);
    }

    public static void notifyChildrenChanged(java.lang.Object serviceObj, java.lang.String parentId) {
        ((android.service.media.MediaBrowserService) serviceObj).notifyChildrenChanged(parentId);
    }
}
