package android.support.v4.media;

@android.annotation.TargetApi(21)
@android.support.annotation.RequiresApi(21)
class MediaMetadataCompatApi21 {

    public static class Builder {
        public static java.lang.Object newInstance() {
            return new android.media.MediaMetadata.Builder();
        }

        public static void putBitmap(java.lang.Object builderObj, java.lang.String key, android.graphics.Bitmap value) {
            ((android.media.MediaMetadata.Builder) builderObj).putBitmap(key, value);
        }

        public static void putLong(java.lang.Object builderObj, java.lang.String key, long value) {
            ((android.media.MediaMetadata.Builder) builderObj).putLong(key, value);
        }

        public static void putRating(java.lang.Object builderObj, java.lang.String key, java.lang.Object ratingObj) {
            ((android.media.MediaMetadata.Builder) builderObj).putRating(key, (android.media.Rating) ratingObj);
        }

        public static void putText(java.lang.Object builderObj, java.lang.String key, java.lang.CharSequence value) {
            ((android.media.MediaMetadata.Builder) builderObj).putText(key, value);
        }

        public static void putString(java.lang.Object builderObj, java.lang.String key, java.lang.String value) {
            ((android.media.MediaMetadata.Builder) builderObj).putString(key, value);
        }

        public static java.lang.Object build(java.lang.Object builderObj) {
            return ((android.media.MediaMetadata.Builder) builderObj).build();
        }
    }

    MediaMetadataCompatApi21() {
    }

    public static java.util.Set<java.lang.String> keySet(java.lang.Object metadataObj) {
        return ((android.media.MediaMetadata) metadataObj).keySet();
    }

    public static android.graphics.Bitmap getBitmap(java.lang.Object metadataObj, java.lang.String key) {
        return ((android.media.MediaMetadata) metadataObj).getBitmap(key);
    }

    public static long getLong(java.lang.Object metadataObj, java.lang.String key) {
        return ((android.media.MediaMetadata) metadataObj).getLong(key);
    }

    public static java.lang.Object getRating(java.lang.Object metadataObj, java.lang.String key) {
        return ((android.media.MediaMetadata) metadataObj).getRating(key);
    }

    public static java.lang.CharSequence getText(java.lang.Object metadataObj, java.lang.String key) {
        return ((android.media.MediaMetadata) metadataObj).getText(key);
    }

    public static void writeToParcel(java.lang.Object metadataObj, android.os.Parcel dest, int flags) {
        ((android.media.MediaMetadata) metadataObj).writeToParcel(dest, flags);
    }

    public static java.lang.Object createFromParcel(android.os.Parcel in) {
        return android.media.MediaMetadata.CREATOR.createFromParcel(in);
    }
}
