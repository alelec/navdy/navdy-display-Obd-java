package android.support.v4.media;

@android.annotation.TargetApi(24)
@android.support.annotation.RequiresApi(24)
class MediaBrowserServiceCompatApi24 {
    private static final java.lang.String TAG = "MBSCompatApi24";
    /* access modifiers changed from: private */
    public static java.lang.reflect.Field sResultFlags;

    static class MediaBrowserServiceAdaptor extends android.support.v4.media.MediaBrowserServiceCompatApi23.MediaBrowserServiceAdaptor {
        MediaBrowserServiceAdaptor(android.content.Context context, android.support.v4.media.MediaBrowserServiceCompatApi24.ServiceCompatProxy serviceWrapper) {
            super(context, serviceWrapper);
        }

        public void onLoadChildren(java.lang.String parentId, android.service.media.MediaBrowserService.Result<java.util.List<android.media.browse.MediaBrowser.MediaItem>> result, android.os.Bundle options) {
            ((android.support.v4.media.MediaBrowserServiceCompatApi24.ServiceCompatProxy) this.mServiceProxy).onLoadChildren(parentId, new android.support.v4.media.MediaBrowserServiceCompatApi24.ResultWrapper(result), options);
        }
    }

    static class ResultWrapper {
        android.service.media.MediaBrowserService.Result mResultObj;

        ResultWrapper(android.service.media.MediaBrowserService.Result result) {
            this.mResultObj = result;
        }

        public void sendResult(java.util.List<android.os.Parcel> result, int flags) {
            try {
                android.support.v4.media.MediaBrowserServiceCompatApi24.sResultFlags.setInt(this.mResultObj, flags);
            } catch (java.lang.IllegalAccessException e) {
                android.util.Log.w(android.support.v4.media.MediaBrowserServiceCompatApi24.TAG, e);
            }
            this.mResultObj.sendResult(parcelListToItemList(result));
        }

        public void detach() {
            this.mResultObj.detach();
        }

        /* access modifiers changed from: 0000 */
        public java.util.List<android.media.browse.MediaBrowser.MediaItem> parcelListToItemList(java.util.List<android.os.Parcel> parcelList) {
            if (parcelList == null) {
                return null;
            }
            java.util.List<android.media.browse.MediaBrowser.MediaItem> items = new java.util.ArrayList<>();
            for (android.os.Parcel parcel : parcelList) {
                parcel.setDataPosition(0);
                items.add(android.media.browse.MediaBrowser.MediaItem.CREATOR.createFromParcel(parcel));
                parcel.recycle();
            }
            return items;
        }
    }

    public interface ServiceCompatProxy extends android.support.v4.media.MediaBrowserServiceCompatApi23.ServiceCompatProxy {
        void onLoadChildren(java.lang.String str, android.support.v4.media.MediaBrowserServiceCompatApi24.ResultWrapper resultWrapper, android.os.Bundle bundle);
    }

    MediaBrowserServiceCompatApi24() {
    }

    static {
        try {
            sResultFlags = android.service.media.MediaBrowserService.Result.class.getDeclaredField("mFlags");
            sResultFlags.setAccessible(true);
        } catch (java.lang.NoSuchFieldException e) {
            android.util.Log.w(TAG, e);
        }
    }

    public static java.lang.Object createService(android.content.Context context, android.support.v4.media.MediaBrowserServiceCompatApi24.ServiceCompatProxy serviceProxy) {
        return new android.support.v4.media.MediaBrowserServiceCompatApi24.MediaBrowserServiceAdaptor(context, serviceProxy);
    }

    public static void notifyChildrenChanged(java.lang.Object serviceObj, java.lang.String parentId, android.os.Bundle options) {
        ((android.service.media.MediaBrowserService) serviceObj).notifyChildrenChanged(parentId, options);
    }

    public static android.os.Bundle getBrowserRootHints(java.lang.Object serviceObj) {
        return ((android.service.media.MediaBrowserService) serviceObj).getBrowserRootHints();
    }
}
