package android.support.v4.media;

public abstract class MediaBrowserServiceCompat extends android.app.Service {
    static final boolean DEBUG = android.util.Log.isLoggable(TAG, 3);
    @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
    public static final java.lang.String KEY_MEDIA_ITEM = "media_item";
    @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
    public static final java.lang.String KEY_SEARCH_RESULTS = "search_results";
    static final int RESULT_ERROR = -1;
    static final int RESULT_FLAG_ON_LOAD_ITEM_NOT_IMPLEMENTED = 2;
    static final int RESULT_FLAG_ON_SEARCH_NOT_IMPLEMENTED = 4;
    static final int RESULT_FLAG_OPTION_NOT_HANDLED = 1;
    static final int RESULT_OK = 0;
    public static final java.lang.String SERVICE_INTERFACE = "android.media.browse.MediaBrowserService";
    static final java.lang.String TAG = "MBServiceCompat";
    final android.support.v4.util.ArrayMap<android.os.IBinder, android.support.v4.media.MediaBrowserServiceCompat.ConnectionRecord> mConnections = new android.support.v4.util.ArrayMap<>();
    android.support.v4.media.MediaBrowserServiceCompat.ConnectionRecord mCurConnection;
    final android.support.v4.media.MediaBrowserServiceCompat.ServiceHandler mHandler = new android.support.v4.media.MediaBrowserServiceCompat.ServiceHandler();
    private android.support.v4.media.MediaBrowserServiceCompat.MediaBrowserServiceImpl mImpl;
    android.support.v4.media.session.MediaSessionCompat.Token mSession;

    public static final class BrowserRoot {
        public static final java.lang.String EXTRA_OFFLINE = "android.service.media.extra.OFFLINE";
        public static final java.lang.String EXTRA_RECENT = "android.service.media.extra.RECENT";
        public static final java.lang.String EXTRA_SUGGESTED = "android.service.media.extra.SUGGESTED";
        @java.lang.Deprecated
        public static final java.lang.String EXTRA_SUGGESTION_KEYWORDS = "android.service.media.extra.SUGGESTION_KEYWORDS";
        private final android.os.Bundle mExtras;
        private final java.lang.String mRootId;

        public BrowserRoot(@android.support.annotation.NonNull java.lang.String rootId, @android.support.annotation.Nullable android.os.Bundle extras) {
            if (rootId == null) {
                throw new java.lang.IllegalArgumentException("The root id in BrowserRoot cannot be null. Use null for BrowserRoot instead.");
            }
            this.mRootId = rootId;
            this.mExtras = extras;
        }

        public java.lang.String getRootId() {
            return this.mRootId;
        }

        public android.os.Bundle getExtras() {
            return this.mExtras;
        }
    }

    private class ConnectionRecord {
        android.support.v4.media.MediaBrowserServiceCompat.ServiceCallbacks callbacks;
        java.lang.String pkg;
        android.support.v4.media.MediaBrowserServiceCompat.BrowserRoot root;
        android.os.Bundle rootHints;
        java.util.HashMap<java.lang.String, java.util.List<android.support.v4.util.Pair<android.os.IBinder, android.os.Bundle>>> subscriptions = new java.util.HashMap<>();

        ConnectionRecord() {
        }
    }

    interface MediaBrowserServiceImpl {
        android.os.Bundle getBrowserRootHints();

        void notifyChildrenChanged(java.lang.String str, android.os.Bundle bundle);

        android.os.IBinder onBind(android.content.Intent intent);

        void onCreate();

        void setSessionToken(android.support.v4.media.session.MediaSessionCompat.Token token);
    }

    class MediaBrowserServiceImplApi21 implements android.support.v4.media.MediaBrowserServiceCompat.MediaBrowserServiceImpl, android.support.v4.media.MediaBrowserServiceCompatApi21.ServiceCompatProxy {
        android.os.Messenger mMessenger;
        java.lang.Object mServiceObj;

        MediaBrowserServiceImplApi21() {
        }

        public void onCreate() {
            this.mServiceObj = android.support.v4.media.MediaBrowserServiceCompatApi21.createService(android.support.v4.media.MediaBrowserServiceCompat.this, this);
            android.support.v4.media.MediaBrowserServiceCompatApi21.onCreate(this.mServiceObj);
        }

        public android.os.IBinder onBind(android.content.Intent intent) {
            return android.support.v4.media.MediaBrowserServiceCompatApi21.onBind(this.mServiceObj, intent);
        }

        public void setSessionToken(android.support.v4.media.session.MediaSessionCompat.Token token) {
            android.support.v4.media.MediaBrowserServiceCompatApi21.setSessionToken(this.mServiceObj, token.getToken());
        }

        public void notifyChildrenChanged(final java.lang.String parentId, final android.os.Bundle options) {
            if (this.mMessenger == null) {
                android.support.v4.media.MediaBrowserServiceCompatApi21.notifyChildrenChanged(this.mServiceObj, parentId);
            } else {
                android.support.v4.media.MediaBrowserServiceCompat.this.mHandler.post(new java.lang.Runnable() {
                    public void run() {
                        for (android.os.IBinder binder : android.support.v4.media.MediaBrowserServiceCompat.this.mConnections.keySet()) {
                            android.support.v4.media.MediaBrowserServiceCompat.ConnectionRecord connection = (android.support.v4.media.MediaBrowserServiceCompat.ConnectionRecord) android.support.v4.media.MediaBrowserServiceCompat.this.mConnections.get(binder);
                            java.util.List<android.support.v4.util.Pair<android.os.IBinder, android.os.Bundle>> callbackList = (java.util.List) connection.subscriptions.get(parentId);
                            if (callbackList != null) {
                                for (android.support.v4.util.Pair<android.os.IBinder, android.os.Bundle> callback : callbackList) {
                                    if (android.support.v4.media.MediaBrowserCompatUtils.hasDuplicatedItems(options, (android.os.Bundle) callback.second)) {
                                        android.support.v4.media.MediaBrowserServiceCompat.this.performLoadChildren(parentId, connection, (android.os.Bundle) callback.second);
                                    }
                                }
                            }
                        }
                    }
                });
            }
        }

        public android.os.Bundle getBrowserRootHints() {
            if (this.mMessenger == null) {
                return null;
            }
            if (android.support.v4.media.MediaBrowserServiceCompat.this.mCurConnection == null) {
                throw new java.lang.IllegalStateException("This should be called inside of onLoadChildren or onLoadItem methods");
            } else if (android.support.v4.media.MediaBrowserServiceCompat.this.mCurConnection.rootHints != null) {
                return new android.os.Bundle(android.support.v4.media.MediaBrowserServiceCompat.this.mCurConnection.rootHints);
            } else {
                return null;
            }
        }

        public android.support.v4.media.MediaBrowserServiceCompatApi21.BrowserRoot onGetRoot(java.lang.String clientPackageName, int clientUid, android.os.Bundle rootHints) {
            android.os.Bundle rootExtras = null;
            if (!(rootHints == null || rootHints.getInt(android.support.v4.media.MediaBrowserProtocol.EXTRA_CLIENT_VERSION, 0) == 0)) {
                rootHints.remove(android.support.v4.media.MediaBrowserProtocol.EXTRA_CLIENT_VERSION);
                this.mMessenger = new android.os.Messenger(android.support.v4.media.MediaBrowserServiceCompat.this.mHandler);
                rootExtras = new android.os.Bundle();
                rootExtras.putInt(android.support.v4.media.MediaBrowserProtocol.EXTRA_SERVICE_VERSION, 1);
                android.support.v4.app.BundleCompat.putBinder(rootExtras, android.support.v4.media.MediaBrowserProtocol.EXTRA_MESSENGER_BINDER, this.mMessenger.getBinder());
            }
            android.support.v4.media.MediaBrowserServiceCompat.BrowserRoot root = android.support.v4.media.MediaBrowserServiceCompat.this.onGetRoot(clientPackageName, clientUid, rootHints);
            if (root == null) {
                return null;
            }
            if (rootExtras == null) {
                rootExtras = root.getExtras();
            } else if (root.getExtras() != null) {
                rootExtras.putAll(root.getExtras());
            }
            return new android.support.v4.media.MediaBrowserServiceCompatApi21.BrowserRoot(root.getRootId(), rootExtras);
        }

        public void onLoadChildren(java.lang.String parentId, final android.support.v4.media.MediaBrowserServiceCompatApi21.ResultWrapper<java.util.List<android.os.Parcel>> resultWrapper) {
            android.support.v4.media.MediaBrowserServiceCompat.this.onLoadChildren(parentId, new android.support.v4.media.MediaBrowserServiceCompat.Result<java.util.List<android.support.v4.media.MediaBrowserCompat.MediaItem>>(parentId) {
                /* access modifiers changed from: 0000 */
                public void onResultSent(java.util.List<android.support.v4.media.MediaBrowserCompat.MediaItem> list, int flags) {
                    java.util.List<android.os.Parcel> parcelList = null;
                    if (list != null) {
                        parcelList = new java.util.ArrayList<>();
                        for (android.support.v4.media.MediaBrowserCompat.MediaItem item : list) {
                            android.os.Parcel parcel = android.os.Parcel.obtain();
                            item.writeToParcel(parcel, 0);
                            parcelList.add(parcel);
                        }
                    }
                    resultWrapper.sendResult(parcelList);
                }

                public void detach() {
                    resultWrapper.detach();
                }
            });
        }
    }

    class MediaBrowserServiceImplApi23 extends android.support.v4.media.MediaBrowserServiceCompat.MediaBrowserServiceImplApi21 implements android.support.v4.media.MediaBrowserServiceCompatApi23.ServiceCompatProxy {
        MediaBrowserServiceImplApi23() {
            super();
        }

        public void onCreate() {
            this.mServiceObj = android.support.v4.media.MediaBrowserServiceCompatApi23.createService(android.support.v4.media.MediaBrowserServiceCompat.this, this);
            android.support.v4.media.MediaBrowserServiceCompatApi21.onCreate(this.mServiceObj);
        }

        public void onLoadItem(java.lang.String itemId, final android.support.v4.media.MediaBrowserServiceCompatApi21.ResultWrapper<android.os.Parcel> resultWrapper) {
            android.support.v4.media.MediaBrowserServiceCompat.this.onLoadItem(itemId, new android.support.v4.media.MediaBrowserServiceCompat.Result<android.support.v4.media.MediaBrowserCompat.MediaItem>(itemId) {
                /* access modifiers changed from: 0000 */
                public void onResultSent(android.support.v4.media.MediaBrowserCompat.MediaItem item, int flags) {
                    if (item == null) {
                        resultWrapper.sendResult(null);
                        return;
                    }
                    android.os.Parcel parcelItem = android.os.Parcel.obtain();
                    item.writeToParcel(parcelItem, 0);
                    resultWrapper.sendResult(parcelItem);
                }

                public void detach() {
                    resultWrapper.detach();
                }
            });
        }
    }

    class MediaBrowserServiceImplApi24 extends android.support.v4.media.MediaBrowserServiceCompat.MediaBrowserServiceImplApi23 implements android.support.v4.media.MediaBrowserServiceCompatApi24.ServiceCompatProxy {
        MediaBrowserServiceImplApi24() {
            super();
        }

        public void onCreate() {
            this.mServiceObj = android.support.v4.media.MediaBrowserServiceCompatApi24.createService(android.support.v4.media.MediaBrowserServiceCompat.this, this);
            android.support.v4.media.MediaBrowserServiceCompatApi21.onCreate(this.mServiceObj);
        }

        public void notifyChildrenChanged(java.lang.String parentId, android.os.Bundle options) {
            if (options == null) {
                android.support.v4.media.MediaBrowserServiceCompatApi21.notifyChildrenChanged(this.mServiceObj, parentId);
            } else {
                android.support.v4.media.MediaBrowserServiceCompatApi24.notifyChildrenChanged(this.mServiceObj, parentId, options);
            }
        }

        public void onLoadChildren(java.lang.String parentId, final android.support.v4.media.MediaBrowserServiceCompatApi24.ResultWrapper resultWrapper, android.os.Bundle options) {
            android.support.v4.media.MediaBrowserServiceCompat.this.onLoadChildren(parentId, new android.support.v4.media.MediaBrowserServiceCompat.Result<java.util.List<android.support.v4.media.MediaBrowserCompat.MediaItem>>(parentId) {
                /* access modifiers changed from: 0000 */
                public void onResultSent(java.util.List<android.support.v4.media.MediaBrowserCompat.MediaItem> list, int flags) {
                    java.util.List<android.os.Parcel> parcelList = null;
                    if (list != null) {
                        parcelList = new java.util.ArrayList<>();
                        for (android.support.v4.media.MediaBrowserCompat.MediaItem item : list) {
                            android.os.Parcel parcel = android.os.Parcel.obtain();
                            item.writeToParcel(parcel, 0);
                            parcelList.add(parcel);
                        }
                    }
                    resultWrapper.sendResult(parcelList, flags);
                }

                public void detach() {
                    resultWrapper.detach();
                }
            }, options);
        }

        public android.os.Bundle getBrowserRootHints() {
            if (android.support.v4.media.MediaBrowserServiceCompat.this.mCurConnection == null) {
                return android.support.v4.media.MediaBrowserServiceCompatApi24.getBrowserRootHints(this.mServiceObj);
            }
            if (android.support.v4.media.MediaBrowserServiceCompat.this.mCurConnection.rootHints == null) {
                return null;
            }
            return new android.os.Bundle(android.support.v4.media.MediaBrowserServiceCompat.this.mCurConnection.rootHints);
        }
    }

    class MediaBrowserServiceImplBase implements android.support.v4.media.MediaBrowserServiceCompat.MediaBrowserServiceImpl {
        private android.os.Messenger mMessenger;

        MediaBrowserServiceImplBase() {
        }

        public void onCreate() {
            this.mMessenger = new android.os.Messenger(android.support.v4.media.MediaBrowserServiceCompat.this.mHandler);
        }

        public android.os.IBinder onBind(android.content.Intent intent) {
            if (android.support.v4.media.MediaBrowserServiceCompat.SERVICE_INTERFACE.equals(intent.getAction())) {
                return this.mMessenger.getBinder();
            }
            return null;
        }

        public void setSessionToken(final android.support.v4.media.session.MediaSessionCompat.Token token) {
            android.support.v4.media.MediaBrowserServiceCompat.this.mHandler.post(new java.lang.Runnable() {
                public void run() {
                    java.util.Iterator<android.support.v4.media.MediaBrowserServiceCompat.ConnectionRecord> iter = android.support.v4.media.MediaBrowserServiceCompat.this.mConnections.values().iterator();
                    while (iter.hasNext()) {
                        android.support.v4.media.MediaBrowserServiceCompat.ConnectionRecord connection = (android.support.v4.media.MediaBrowserServiceCompat.ConnectionRecord) iter.next();
                        try {
                            connection.callbacks.onConnect(connection.root.getRootId(), token, connection.root.getExtras());
                        } catch (android.os.RemoteException e) {
                            android.util.Log.w(android.support.v4.media.MediaBrowserServiceCompat.TAG, "Connection for " + connection.pkg + " is no longer valid.");
                            iter.remove();
                        }
                    }
                }
            });
        }

        public void notifyChildrenChanged(@android.support.annotation.NonNull final java.lang.String parentId, final android.os.Bundle options) {
            android.support.v4.media.MediaBrowserServiceCompat.this.mHandler.post(new java.lang.Runnable() {
                public void run() {
                    for (android.os.IBinder binder : android.support.v4.media.MediaBrowserServiceCompat.this.mConnections.keySet()) {
                        android.support.v4.media.MediaBrowserServiceCompat.ConnectionRecord connection = (android.support.v4.media.MediaBrowserServiceCompat.ConnectionRecord) android.support.v4.media.MediaBrowserServiceCompat.this.mConnections.get(binder);
                        java.util.List<android.support.v4.util.Pair<android.os.IBinder, android.os.Bundle>> callbackList = (java.util.List) connection.subscriptions.get(parentId);
                        if (callbackList != null) {
                            for (android.support.v4.util.Pair<android.os.IBinder, android.os.Bundle> callback : callbackList) {
                                if (android.support.v4.media.MediaBrowserCompatUtils.hasDuplicatedItems(options, (android.os.Bundle) callback.second)) {
                                    android.support.v4.media.MediaBrowserServiceCompat.this.performLoadChildren(parentId, connection, (android.os.Bundle) callback.second);
                                }
                            }
                        }
                    }
                }
            });
        }

        public android.os.Bundle getBrowserRootHints() {
            if (android.support.v4.media.MediaBrowserServiceCompat.this.mCurConnection == null) {
                throw new java.lang.IllegalStateException("This should be called inside of onLoadChildren or onLoadItem methods");
            } else if (android.support.v4.media.MediaBrowserServiceCompat.this.mCurConnection.rootHints == null) {
                return null;
            } else {
                return new android.os.Bundle(android.support.v4.media.MediaBrowserServiceCompat.this.mCurConnection.rootHints);
            }
        }
    }

    public static class Result<T> {
        private java.lang.Object mDebug;
        private boolean mDetachCalled;
        private int mFlags;
        private boolean mSendResultCalled;

        Result(java.lang.Object debug) {
            this.mDebug = debug;
        }

        public void sendResult(T result) {
            if (this.mSendResultCalled) {
                throw new java.lang.IllegalStateException("sendResult() called twice for: " + this.mDebug);
            }
            this.mSendResultCalled = true;
            onResultSent(result, this.mFlags);
        }

        public void detach() {
            if (this.mDetachCalled) {
                throw new java.lang.IllegalStateException("detach() called when detach() had already been called for: " + this.mDebug);
            } else if (this.mSendResultCalled) {
                throw new java.lang.IllegalStateException("detach() called when sendResult() had already been called for: " + this.mDebug);
            } else {
                this.mDetachCalled = true;
            }
        }

        /* access modifiers changed from: 0000 */
        public boolean isDone() {
            return this.mDetachCalled || this.mSendResultCalled;
        }

        /* access modifiers changed from: 0000 */
        public void setFlags(int flags) {
            this.mFlags = flags;
        }

        /* access modifiers changed from: 0000 */
        public void onResultSent(T t, int flags) {
        }
    }

    private class ServiceBinderImpl {
        ServiceBinderImpl() {
        }

        public void connect(java.lang.String pkg, int uid, android.os.Bundle rootHints, android.support.v4.media.MediaBrowserServiceCompat.ServiceCallbacks callbacks) {
            if (!android.support.v4.media.MediaBrowserServiceCompat.this.isValidPackage(pkg, uid)) {
                throw new java.lang.IllegalArgumentException("Package/uid mismatch: uid=" + uid + " package=" + pkg);
            }
            final android.support.v4.media.MediaBrowserServiceCompat.ServiceCallbacks serviceCallbacks = callbacks;
            final java.lang.String str = pkg;
            final android.os.Bundle bundle = rootHints;
            final int i = uid;
            android.support.v4.media.MediaBrowserServiceCompat.this.mHandler.postOrRun(new java.lang.Runnable() {
                public void run() {
                    android.os.IBinder b = serviceCallbacks.asBinder();
                    android.support.v4.media.MediaBrowserServiceCompat.this.mConnections.remove(b);
                    android.support.v4.media.MediaBrowserServiceCompat.ConnectionRecord connection = new android.support.v4.media.MediaBrowserServiceCompat.ConnectionRecord();
                    connection.pkg = str;
                    connection.rootHints = bundle;
                    connection.callbacks = serviceCallbacks;
                    connection.root = android.support.v4.media.MediaBrowserServiceCompat.this.onGetRoot(str, i, bundle);
                    if (connection.root == null) {
                        android.util.Log.i(android.support.v4.media.MediaBrowserServiceCompat.TAG, "No root for client " + str + " from service " + getClass().getName());
                        try {
                            serviceCallbacks.onConnectFailed();
                        } catch (android.os.RemoteException e) {
                            android.util.Log.w(android.support.v4.media.MediaBrowserServiceCompat.TAG, "Calling onConnectFailed() failed. Ignoring. pkg=" + str);
                        }
                    } else {
                        try {
                            android.support.v4.media.MediaBrowserServiceCompat.this.mConnections.put(b, connection);
                            if (android.support.v4.media.MediaBrowserServiceCompat.this.mSession != null) {
                                serviceCallbacks.onConnect(connection.root.getRootId(), android.support.v4.media.MediaBrowserServiceCompat.this.mSession, connection.root.getExtras());
                            }
                        } catch (android.os.RemoteException e2) {
                            android.util.Log.w(android.support.v4.media.MediaBrowserServiceCompat.TAG, "Calling onConnect() failed. Dropping client. pkg=" + str);
                            android.support.v4.media.MediaBrowserServiceCompat.this.mConnections.remove(b);
                        }
                    }
                }
            });
        }

        public void disconnect(final android.support.v4.media.MediaBrowserServiceCompat.ServiceCallbacks callbacks) {
            android.support.v4.media.MediaBrowserServiceCompat.this.mHandler.postOrRun(new java.lang.Runnable() {
                public void run() {
                    if (((android.support.v4.media.MediaBrowserServiceCompat.ConnectionRecord) android.support.v4.media.MediaBrowserServiceCompat.this.mConnections.remove(callbacks.asBinder())) != null) {
                    }
                }
            });
        }

        public void addSubscription(java.lang.String id, android.os.IBinder token, android.os.Bundle options, android.support.v4.media.MediaBrowserServiceCompat.ServiceCallbacks callbacks) {
            final android.support.v4.media.MediaBrowserServiceCompat.ServiceCallbacks serviceCallbacks = callbacks;
            final java.lang.String str = id;
            final android.os.IBinder iBinder = token;
            final android.os.Bundle bundle = options;
            android.support.v4.media.MediaBrowserServiceCompat.this.mHandler.postOrRun(new java.lang.Runnable() {
                public void run() {
                    android.support.v4.media.MediaBrowserServiceCompat.ConnectionRecord connection = (android.support.v4.media.MediaBrowserServiceCompat.ConnectionRecord) android.support.v4.media.MediaBrowserServiceCompat.this.mConnections.get(serviceCallbacks.asBinder());
                    if (connection == null) {
                        android.util.Log.w(android.support.v4.media.MediaBrowserServiceCompat.TAG, "addSubscription for callback that isn't registered id=" + str);
                    } else {
                        android.support.v4.media.MediaBrowserServiceCompat.this.addSubscription(str, connection, iBinder, bundle);
                    }
                }
            });
        }

        public void removeSubscription(final java.lang.String id, final android.os.IBinder token, final android.support.v4.media.MediaBrowserServiceCompat.ServiceCallbacks callbacks) {
            android.support.v4.media.MediaBrowserServiceCompat.this.mHandler.postOrRun(new java.lang.Runnable() {
                public void run() {
                    android.support.v4.media.MediaBrowserServiceCompat.ConnectionRecord connection = (android.support.v4.media.MediaBrowserServiceCompat.ConnectionRecord) android.support.v4.media.MediaBrowserServiceCompat.this.mConnections.get(callbacks.asBinder());
                    if (connection == null) {
                        android.util.Log.w(android.support.v4.media.MediaBrowserServiceCompat.TAG, "removeSubscription for callback that isn't registered id=" + id);
                    } else if (!android.support.v4.media.MediaBrowserServiceCompat.this.removeSubscription(id, connection, token)) {
                        android.util.Log.w(android.support.v4.media.MediaBrowserServiceCompat.TAG, "removeSubscription called for " + id + " which is not subscribed");
                    }
                }
            });
        }

        public void getMediaItem(final java.lang.String mediaId, final android.support.v4.os.ResultReceiver receiver, final android.support.v4.media.MediaBrowserServiceCompat.ServiceCallbacks callbacks) {
            if (!android.text.TextUtils.isEmpty(mediaId) && receiver != null) {
                android.support.v4.media.MediaBrowserServiceCompat.this.mHandler.postOrRun(new java.lang.Runnable() {
                    public void run() {
                        android.support.v4.media.MediaBrowserServiceCompat.ConnectionRecord connection = (android.support.v4.media.MediaBrowserServiceCompat.ConnectionRecord) android.support.v4.media.MediaBrowserServiceCompat.this.mConnections.get(callbacks.asBinder());
                        if (connection == null) {
                            android.util.Log.w(android.support.v4.media.MediaBrowserServiceCompat.TAG, "getMediaItem for callback that isn't registered id=" + mediaId);
                        } else {
                            android.support.v4.media.MediaBrowserServiceCompat.this.performLoadItem(mediaId, connection, receiver);
                        }
                    }
                });
            }
        }

        public void registerCallbacks(final android.support.v4.media.MediaBrowserServiceCompat.ServiceCallbacks callbacks, final android.os.Bundle rootHints) {
            android.support.v4.media.MediaBrowserServiceCompat.this.mHandler.postOrRun(new java.lang.Runnable() {
                public void run() {
                    android.os.IBinder b = callbacks.asBinder();
                    android.support.v4.media.MediaBrowserServiceCompat.this.mConnections.remove(b);
                    android.support.v4.media.MediaBrowserServiceCompat.ConnectionRecord connection = new android.support.v4.media.MediaBrowserServiceCompat.ConnectionRecord();
                    connection.callbacks = callbacks;
                    connection.rootHints = rootHints;
                    android.support.v4.media.MediaBrowserServiceCompat.this.mConnections.put(b, connection);
                }
            });
        }

        public void unregisterCallbacks(final android.support.v4.media.MediaBrowserServiceCompat.ServiceCallbacks callbacks) {
            android.support.v4.media.MediaBrowserServiceCompat.this.mHandler.postOrRun(new java.lang.Runnable() {
                public void run() {
                    android.support.v4.media.MediaBrowserServiceCompat.this.mConnections.remove(callbacks.asBinder());
                }
            });
        }

        public void search(java.lang.String query, android.os.Bundle extras, android.support.v4.os.ResultReceiver receiver, android.support.v4.media.MediaBrowserServiceCompat.ServiceCallbacks callbacks) {
            if (!android.text.TextUtils.isEmpty(query) && receiver != null) {
                final android.support.v4.media.MediaBrowserServiceCompat.ServiceCallbacks serviceCallbacks = callbacks;
                final java.lang.String str = query;
                final android.os.Bundle bundle = extras;
                final android.support.v4.os.ResultReceiver resultReceiver = receiver;
                android.support.v4.media.MediaBrowserServiceCompat.this.mHandler.postOrRun(new java.lang.Runnable() {
                    public void run() {
                        android.support.v4.media.MediaBrowserServiceCompat.ConnectionRecord connection = (android.support.v4.media.MediaBrowserServiceCompat.ConnectionRecord) android.support.v4.media.MediaBrowserServiceCompat.this.mConnections.get(serviceCallbacks.asBinder());
                        if (connection == null) {
                            android.util.Log.w(android.support.v4.media.MediaBrowserServiceCompat.TAG, "search for callback that isn't registered query=" + str);
                        } else {
                            android.support.v4.media.MediaBrowserServiceCompat.this.performSearch(str, bundle, connection, resultReceiver);
                        }
                    }
                });
            }
        }
    }

    private interface ServiceCallbacks {
        android.os.IBinder asBinder();

        void onConnect(java.lang.String str, android.support.v4.media.session.MediaSessionCompat.Token token, android.os.Bundle bundle) throws android.os.RemoteException;

        void onConnectFailed() throws android.os.RemoteException;

        void onLoadChildren(java.lang.String str, java.util.List<android.support.v4.media.MediaBrowserCompat.MediaItem> list, android.os.Bundle bundle) throws android.os.RemoteException;
    }

    private class ServiceCallbacksCompat implements android.support.v4.media.MediaBrowserServiceCompat.ServiceCallbacks {
        final android.os.Messenger mCallbacks;

        ServiceCallbacksCompat(android.os.Messenger callbacks) {
            this.mCallbacks = callbacks;
        }

        public android.os.IBinder asBinder() {
            return this.mCallbacks.getBinder();
        }

        public void onConnect(java.lang.String root, android.support.v4.media.session.MediaSessionCompat.Token session, android.os.Bundle extras) throws android.os.RemoteException {
            if (extras == null) {
                extras = new android.os.Bundle();
            }
            extras.putInt(android.support.v4.media.MediaBrowserProtocol.EXTRA_SERVICE_VERSION, 1);
            android.os.Bundle data = new android.os.Bundle();
            data.putString(android.support.v4.media.MediaBrowserProtocol.DATA_MEDIA_ITEM_ID, root);
            data.putParcelable(android.support.v4.media.MediaBrowserProtocol.DATA_MEDIA_SESSION_TOKEN, session);
            data.putBundle(android.support.v4.media.MediaBrowserProtocol.DATA_ROOT_HINTS, extras);
            sendRequest(1, data);
        }

        public void onConnectFailed() throws android.os.RemoteException {
            sendRequest(2, null);
        }

        public void onLoadChildren(java.lang.String mediaId, java.util.List<android.support.v4.media.MediaBrowserCompat.MediaItem> list, android.os.Bundle options) throws android.os.RemoteException {
            android.os.Bundle data = new android.os.Bundle();
            data.putString(android.support.v4.media.MediaBrowserProtocol.DATA_MEDIA_ITEM_ID, mediaId);
            data.putBundle(android.support.v4.media.MediaBrowserProtocol.DATA_OPTIONS, options);
            if (list != null) {
                data.putParcelableArrayList(android.support.v4.media.MediaBrowserProtocol.DATA_MEDIA_ITEM_LIST, list instanceof java.util.ArrayList ? (java.util.ArrayList) list : new java.util.ArrayList(list));
            }
            sendRequest(3, data);
        }

        private void sendRequest(int what, android.os.Bundle data) throws android.os.RemoteException {
            android.os.Message msg = android.os.Message.obtain();
            msg.what = what;
            msg.arg1 = 1;
            msg.setData(data);
            this.mCallbacks.send(msg);
        }
    }

    private final class ServiceHandler extends android.os.Handler {
        private final android.support.v4.media.MediaBrowserServiceCompat.ServiceBinderImpl mServiceBinderImpl = new android.support.v4.media.MediaBrowserServiceCompat.ServiceBinderImpl();

        ServiceHandler() {
        }

        public void handleMessage(android.os.Message msg) {
            android.os.Bundle data = msg.getData();
            switch (msg.what) {
                case 1:
                    this.mServiceBinderImpl.connect(data.getString(android.support.v4.media.MediaBrowserProtocol.DATA_PACKAGE_NAME), data.getInt(android.support.v4.media.MediaBrowserProtocol.DATA_CALLING_UID), data.getBundle(android.support.v4.media.MediaBrowserProtocol.DATA_ROOT_HINTS), new android.support.v4.media.MediaBrowserServiceCompat.ServiceCallbacksCompat(msg.replyTo));
                    return;
                case 2:
                    this.mServiceBinderImpl.disconnect(new android.support.v4.media.MediaBrowserServiceCompat.ServiceCallbacksCompat(msg.replyTo));
                    return;
                case 3:
                    this.mServiceBinderImpl.addSubscription(data.getString(android.support.v4.media.MediaBrowserProtocol.DATA_MEDIA_ITEM_ID), android.support.v4.app.BundleCompat.getBinder(data, android.support.v4.media.MediaBrowserProtocol.DATA_CALLBACK_TOKEN), data.getBundle(android.support.v4.media.MediaBrowserProtocol.DATA_OPTIONS), new android.support.v4.media.MediaBrowserServiceCompat.ServiceCallbacksCompat(msg.replyTo));
                    return;
                case 4:
                    this.mServiceBinderImpl.removeSubscription(data.getString(android.support.v4.media.MediaBrowserProtocol.DATA_MEDIA_ITEM_ID), android.support.v4.app.BundleCompat.getBinder(data, android.support.v4.media.MediaBrowserProtocol.DATA_CALLBACK_TOKEN), new android.support.v4.media.MediaBrowserServiceCompat.ServiceCallbacksCompat(msg.replyTo));
                    return;
                case 5:
                    this.mServiceBinderImpl.getMediaItem(data.getString(android.support.v4.media.MediaBrowserProtocol.DATA_MEDIA_ITEM_ID), (android.support.v4.os.ResultReceiver) data.getParcelable(android.support.v4.media.MediaBrowserProtocol.DATA_RESULT_RECEIVER), new android.support.v4.media.MediaBrowserServiceCompat.ServiceCallbacksCompat(msg.replyTo));
                    return;
                case 6:
                    this.mServiceBinderImpl.registerCallbacks(new android.support.v4.media.MediaBrowserServiceCompat.ServiceCallbacksCompat(msg.replyTo), data.getBundle(android.support.v4.media.MediaBrowserProtocol.DATA_ROOT_HINTS));
                    return;
                case 7:
                    this.mServiceBinderImpl.unregisterCallbacks(new android.support.v4.media.MediaBrowserServiceCompat.ServiceCallbacksCompat(msg.replyTo));
                    return;
                case 8:
                    this.mServiceBinderImpl.search(data.getString(android.support.v4.media.MediaBrowserProtocol.DATA_SEARCH_QUERY), data.getBundle(android.support.v4.media.MediaBrowserProtocol.DATA_SEARCH_EXTRAS), (android.support.v4.os.ResultReceiver) data.getParcelable(android.support.v4.media.MediaBrowserProtocol.DATA_RESULT_RECEIVER), new android.support.v4.media.MediaBrowserServiceCompat.ServiceCallbacksCompat(msg.replyTo));
                    return;
                default:
                    android.util.Log.w(android.support.v4.media.MediaBrowserServiceCompat.TAG, "Unhandled message: " + msg + "\n  Service version: " + 1 + "\n  Client version: " + msg.arg1);
                    return;
            }
        }

        public boolean sendMessageAtTime(android.os.Message msg, long uptimeMillis) {
            android.os.Bundle data = msg.getData();
            data.setClassLoader(android.support.v4.media.MediaBrowserCompat.class.getClassLoader());
            data.putInt(android.support.v4.media.MediaBrowserProtocol.DATA_CALLING_UID, android.os.Binder.getCallingUid());
            return super.sendMessageAtTime(msg, uptimeMillis);
        }

        public void postOrRun(java.lang.Runnable r) {
            if (java.lang.Thread.currentThread() == getLooper().getThread()) {
                r.run();
            } else {
                post(r);
            }
        }
    }

    @android.support.annotation.Nullable
    public abstract android.support.v4.media.MediaBrowserServiceCompat.BrowserRoot onGetRoot(@android.support.annotation.NonNull java.lang.String str, int i, @android.support.annotation.Nullable android.os.Bundle bundle);

    public abstract void onLoadChildren(@android.support.annotation.NonNull java.lang.String str, @android.support.annotation.NonNull android.support.v4.media.MediaBrowserServiceCompat.Result<java.util.List<android.support.v4.media.MediaBrowserCompat.MediaItem>> result);

    public void onCreate() {
        super.onCreate();
        if (android.os.Build.VERSION.SDK_INT >= 26 || android.support.v4.os.BuildCompat.isAtLeastO()) {
            this.mImpl = new android.support.v4.media.MediaBrowserServiceCompat.MediaBrowserServiceImplApi24();
        } else if (android.os.Build.VERSION.SDK_INT >= 23) {
            this.mImpl = new android.support.v4.media.MediaBrowserServiceCompat.MediaBrowserServiceImplApi23();
        } else if (android.os.Build.VERSION.SDK_INT >= 21) {
            this.mImpl = new android.support.v4.media.MediaBrowserServiceCompat.MediaBrowserServiceImplApi21();
        } else {
            this.mImpl = new android.support.v4.media.MediaBrowserServiceCompat.MediaBrowserServiceImplBase();
        }
        this.mImpl.onCreate();
    }

    public android.os.IBinder onBind(android.content.Intent intent) {
        return this.mImpl.onBind(intent);
    }

    public void dump(java.io.FileDescriptor fd, java.io.PrintWriter writer, java.lang.String[] args) {
    }

    public void onLoadChildren(@android.support.annotation.NonNull java.lang.String parentId, @android.support.annotation.NonNull android.support.v4.media.MediaBrowserServiceCompat.Result<java.util.List<android.support.v4.media.MediaBrowserCompat.MediaItem>> result, @android.support.annotation.NonNull android.os.Bundle options) {
        result.setFlags(1);
        onLoadChildren(parentId, result);
    }

    public void onLoadItem(java.lang.String itemId, @android.support.annotation.NonNull android.support.v4.media.MediaBrowserServiceCompat.Result<android.support.v4.media.MediaBrowserCompat.MediaItem> result) {
        result.setFlags(2);
        result.sendResult(null);
    }

    public void onSearch(@android.support.annotation.NonNull java.lang.String query, android.os.Bundle extras, @android.support.annotation.NonNull android.support.v4.media.MediaBrowserServiceCompat.Result<java.util.List<android.support.v4.media.MediaBrowserCompat.MediaItem>> result) {
        result.setFlags(4);
        result.sendResult(null);
    }

    public void setSessionToken(android.support.v4.media.session.MediaSessionCompat.Token token) {
        if (token == null) {
            throw new java.lang.IllegalArgumentException("Session token may not be null.");
        } else if (this.mSession != null) {
            throw new java.lang.IllegalStateException("The session token has already been set.");
        } else {
            this.mSession = token;
            this.mImpl.setSessionToken(token);
        }
    }

    @android.support.annotation.Nullable
    public android.support.v4.media.session.MediaSessionCompat.Token getSessionToken() {
        return this.mSession;
    }

    public final android.os.Bundle getBrowserRootHints() {
        return this.mImpl.getBrowserRootHints();
    }

    public void notifyChildrenChanged(@android.support.annotation.NonNull java.lang.String parentId) {
        if (parentId == null) {
            throw new java.lang.IllegalArgumentException("parentId cannot be null in notifyChildrenChanged");
        }
        this.mImpl.notifyChildrenChanged(parentId, null);
    }

    public void notifyChildrenChanged(@android.support.annotation.NonNull java.lang.String parentId, @android.support.annotation.NonNull android.os.Bundle options) {
        if (parentId == null) {
            throw new java.lang.IllegalArgumentException("parentId cannot be null in notifyChildrenChanged");
        } else if (options == null) {
            throw new java.lang.IllegalArgumentException("options cannot be null in notifyChildrenChanged");
        } else {
            this.mImpl.notifyChildrenChanged(parentId, options);
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean isValidPackage(java.lang.String pkg, int uid) {
        if (pkg == null) {
            return false;
        }
        for (java.lang.String equals : getPackageManager().getPackagesForUid(uid)) {
            if (equals.equals(pkg)) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: 0000 */
    public void addSubscription(java.lang.String id, android.support.v4.media.MediaBrowserServiceCompat.ConnectionRecord connection, android.os.IBinder token, android.os.Bundle options) {
        java.util.List<android.support.v4.util.Pair<android.os.IBinder, android.os.Bundle>> callbackList = (java.util.List) connection.subscriptions.get(id);
        if (callbackList == null) {
            callbackList = new java.util.ArrayList<>();
        }
        for (android.support.v4.util.Pair<android.os.IBinder, android.os.Bundle> callback : callbackList) {
            if (token == callback.first && android.support.v4.media.MediaBrowserCompatUtils.areSameOptions(options, (android.os.Bundle) callback.second)) {
                return;
            }
        }
        callbackList.add(new android.support.v4.util.Pair(token, options));
        connection.subscriptions.put(id, callbackList);
        performLoadChildren(id, connection, options);
    }

    /* access modifiers changed from: 0000 */
    public boolean removeSubscription(java.lang.String id, android.support.v4.media.MediaBrowserServiceCompat.ConnectionRecord connection, android.os.IBinder token) {
        if (token != null) {
            boolean removed = false;
            java.util.List<android.support.v4.util.Pair<android.os.IBinder, android.os.Bundle>> callbackList = (java.util.List) connection.subscriptions.get(id);
            if (callbackList != null) {
                java.util.Iterator<android.support.v4.util.Pair<android.os.IBinder, android.os.Bundle>> iter = callbackList.iterator();
                while (iter.hasNext()) {
                    if (token == ((android.support.v4.util.Pair) iter.next()).first) {
                        removed = true;
                        iter.remove();
                    }
                }
                if (callbackList.size() == 0) {
                    connection.subscriptions.remove(id);
                }
            }
            return removed;
        } else if (connection.subscriptions.remove(id) != null) {
            return true;
        } else {
            return false;
        }
    }

    /* access modifiers changed from: 0000 */
    public void performLoadChildren(java.lang.String parentId, android.support.v4.media.MediaBrowserServiceCompat.ConnectionRecord connection, android.os.Bundle options) {
        final android.support.v4.media.MediaBrowserServiceCompat.ConnectionRecord connectionRecord = connection;
        final java.lang.String str = parentId;
        final android.os.Bundle bundle = options;
        android.support.v4.media.MediaBrowserServiceCompat.Result<java.util.List<android.support.v4.media.MediaBrowserCompat.MediaItem>> result = new android.support.v4.media.MediaBrowserServiceCompat.Result<java.util.List<android.support.v4.media.MediaBrowserCompat.MediaItem>>(parentId) {
            /* access modifiers changed from: 0000 */
            public void onResultSent(java.util.List<android.support.v4.media.MediaBrowserCompat.MediaItem> list, int flags) {
                java.util.List<android.support.v4.media.MediaBrowserCompat.MediaItem> filteredList;
                if (android.support.v4.media.MediaBrowserServiceCompat.this.mConnections.get(connectionRecord.callbacks.asBinder()) == connectionRecord) {
                    if ((flags & 1) != 0) {
                        filteredList = android.support.v4.media.MediaBrowserServiceCompat.this.applyOptions(list, bundle);
                    } else {
                        filteredList = list;
                    }
                    try {
                        connectionRecord.callbacks.onLoadChildren(str, filteredList, bundle);
                    } catch (android.os.RemoteException e) {
                        android.util.Log.w(android.support.v4.media.MediaBrowserServiceCompat.TAG, "Calling onLoadChildren() failed for id=" + str + " package=" + connectionRecord.pkg);
                    }
                } else if (android.support.v4.media.MediaBrowserServiceCompat.DEBUG) {
                    android.util.Log.d(android.support.v4.media.MediaBrowserServiceCompat.TAG, "Not sending onLoadChildren result for connection that has been disconnected. pkg=" + connectionRecord.pkg + " id=" + str);
                }
            }
        };
        this.mCurConnection = connection;
        if (options == null) {
            onLoadChildren(parentId, result);
        } else {
            onLoadChildren(parentId, result, options);
        }
        this.mCurConnection = null;
        if (!result.isDone()) {
            throw new java.lang.IllegalStateException("onLoadChildren must call detach() or sendResult() before returning for package=" + connection.pkg + " id=" + parentId);
        }
    }

    /* access modifiers changed from: 0000 */
    public java.util.List<android.support.v4.media.MediaBrowserCompat.MediaItem> applyOptions(java.util.List<android.support.v4.media.MediaBrowserCompat.MediaItem> list, android.os.Bundle options) {
        if (list == null) {
            return null;
        }
        int page = options.getInt(android.support.v4.media.MediaBrowserCompat.EXTRA_PAGE, -1);
        int pageSize = options.getInt(android.support.v4.media.MediaBrowserCompat.EXTRA_PAGE_SIZE, -1);
        if (page == -1 && pageSize == -1) {
            return list;
        }
        int fromIndex = pageSize * page;
        int toIndex = fromIndex + pageSize;
        if (page < 0 || pageSize < 1 || fromIndex >= list.size()) {
            return java.util.Collections.EMPTY_LIST;
        }
        if (toIndex > list.size()) {
            toIndex = list.size();
        }
        return list.subList(fromIndex, toIndex);
    }

    /* access modifiers changed from: 0000 */
    public void performLoadItem(java.lang.String itemId, android.support.v4.media.MediaBrowserServiceCompat.ConnectionRecord connection, final android.support.v4.os.ResultReceiver receiver) {
        android.support.v4.media.MediaBrowserServiceCompat.Result<android.support.v4.media.MediaBrowserCompat.MediaItem> result = new android.support.v4.media.MediaBrowserServiceCompat.Result<android.support.v4.media.MediaBrowserCompat.MediaItem>(itemId) {
            /* access modifiers changed from: 0000 */
            public void onResultSent(android.support.v4.media.MediaBrowserCompat.MediaItem item, int flags) {
                if ((flags & 2) != 0) {
                    receiver.send(-1, null);
                    return;
                }
                android.os.Bundle bundle = new android.os.Bundle();
                bundle.putParcelable(android.support.v4.media.MediaBrowserServiceCompat.KEY_MEDIA_ITEM, item);
                receiver.send(0, bundle);
            }
        };
        this.mCurConnection = connection;
        onLoadItem(itemId, result);
        this.mCurConnection = null;
        if (!result.isDone()) {
            throw new java.lang.IllegalStateException("onLoadItem must call detach() or sendResult() before returning for id=" + itemId);
        }
    }

    /* access modifiers changed from: 0000 */
    public void performSearch(java.lang.String query, android.os.Bundle extras, android.support.v4.media.MediaBrowserServiceCompat.ConnectionRecord connection, final android.support.v4.os.ResultReceiver receiver) {
        android.support.v4.media.MediaBrowserServiceCompat.Result<java.util.List<android.support.v4.media.MediaBrowserCompat.MediaItem>> result = new android.support.v4.media.MediaBrowserServiceCompat.Result<java.util.List<android.support.v4.media.MediaBrowserCompat.MediaItem>>(query) {
            /* access modifiers changed from: 0000 */
            public void onResultSent(java.util.List<android.support.v4.media.MediaBrowserCompat.MediaItem> items, int flag) {
                if ((flag & 4) != 0 || items == null) {
                    receiver.send(-1, null);
                    return;
                }
                android.os.Bundle bundle = new android.os.Bundle();
                bundle.putParcelableArray(android.support.v4.media.MediaBrowserServiceCompat.KEY_SEARCH_RESULTS, (android.os.Parcelable[]) items.toArray(new android.support.v4.media.MediaBrowserCompat.MediaItem[0]));
                receiver.send(0, bundle);
            }
        };
        this.mCurConnection = connection;
        onSearch(query, extras, result);
        this.mCurConnection = null;
        if (!result.isDone()) {
            throw new java.lang.IllegalStateException("onSearch must call detach() or sendResult() before returning for query=" + query);
        }
    }
}
