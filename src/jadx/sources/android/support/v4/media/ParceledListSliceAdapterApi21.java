package android.support.v4.media;

@android.annotation.TargetApi(21)
@android.support.annotation.RequiresApi(21)
class ParceledListSliceAdapterApi21 {
    private static java.lang.reflect.Constructor sConstructor;

    ParceledListSliceAdapterApi21() {
    }

    static {
        try {
            sConstructor = java.lang.Class.forName("android.content.pm.ParceledListSlice").getConstructor(new java.lang.Class[]{java.util.List.class});
        } catch (java.lang.ClassNotFoundException | java.lang.NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    static java.lang.Object newInstance(java.util.List<android.media.browse.MediaBrowser.MediaItem> itemList) {
        java.lang.Object result = null;
        try {
            return sConstructor.newInstance(new java.lang.Object[]{itemList});
        } catch (java.lang.IllegalAccessException | java.lang.InstantiationException | java.lang.reflect.InvocationTargetException e) {
            e.printStackTrace();
            return result;
        }
    }
}
