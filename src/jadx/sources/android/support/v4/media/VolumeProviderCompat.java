package android.support.v4.media;

public abstract class VolumeProviderCompat {
    public static final int VOLUME_CONTROL_ABSOLUTE = 2;
    public static final int VOLUME_CONTROL_FIXED = 0;
    public static final int VOLUME_CONTROL_RELATIVE = 1;
    private android.support.v4.media.VolumeProviderCompat.Callback mCallback;
    private final int mControlType;
    private int mCurrentVolume;
    private final int mMaxVolume;
    private java.lang.Object mVolumeProviderObj;

    public static abstract class Callback {
        public abstract void onVolumeChanged(android.support.v4.media.VolumeProviderCompat volumeProviderCompat);
    }

    @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
    @java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.SOURCE)
    public @interface ControlType {
    }

    public VolumeProviderCompat(int volumeControl, int maxVolume, int currentVolume) {
        this.mControlType = volumeControl;
        this.mMaxVolume = maxVolume;
        this.mCurrentVolume = currentVolume;
    }

    public final int getCurrentVolume() {
        return this.mCurrentVolume;
    }

    public final int getVolumeControl() {
        return this.mControlType;
    }

    public final int getMaxVolume() {
        return this.mMaxVolume;
    }

    public final void setCurrentVolume(int currentVolume) {
        this.mCurrentVolume = currentVolume;
        java.lang.Object volumeProviderObj = getVolumeProvider();
        if (volumeProviderObj != null) {
            android.support.v4.media.VolumeProviderCompatApi21.setCurrentVolume(volumeProviderObj, currentVolume);
        }
        if (this.mCallback != null) {
            this.mCallback.onVolumeChanged(this);
        }
    }

    public void onSetVolumeTo(int volume) {
    }

    public void onAdjustVolume(int direction) {
    }

    public void setCallback(android.support.v4.media.VolumeProviderCompat.Callback callback) {
        this.mCallback = callback;
    }

    public java.lang.Object getVolumeProvider() {
        if (this.mVolumeProviderObj != null || android.os.Build.VERSION.SDK_INT < 21) {
            return this.mVolumeProviderObj;
        }
        this.mVolumeProviderObj = android.support.v4.media.VolumeProviderCompatApi21.createVolumeProvider(this.mControlType, this.mMaxVolume, this.mCurrentVolume, new android.support.v4.media.VolumeProviderCompatApi21.Delegate() {
            public void onSetVolumeTo(int volume) {
                android.support.v4.media.VolumeProviderCompat.this.onSetVolumeTo(volume);
            }

            public void onAdjustVolume(int direction) {
                android.support.v4.media.VolumeProviderCompat.this.onAdjustVolume(direction);
            }
        });
        return this.mVolumeProviderObj;
    }
}
