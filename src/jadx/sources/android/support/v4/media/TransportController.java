package android.support.v4.media;

@java.lang.Deprecated
public abstract class TransportController {
    @java.lang.Deprecated
    public abstract int getBufferPercentage();

    @java.lang.Deprecated
    public abstract long getCurrentPosition();

    @java.lang.Deprecated
    public abstract long getDuration();

    @java.lang.Deprecated
    public abstract int getTransportControlFlags();

    @java.lang.Deprecated
    public abstract boolean isPlaying();

    @java.lang.Deprecated
    public abstract void pausePlaying();

    @java.lang.Deprecated
    public abstract void registerStateListener(android.support.v4.media.TransportStateListener transportStateListener);

    @java.lang.Deprecated
    public abstract void seekTo(long j);

    @java.lang.Deprecated
    public abstract void startPlaying();

    @java.lang.Deprecated
    public abstract void stopPlaying();

    @java.lang.Deprecated
    public abstract void unregisterStateListener(android.support.v4.media.TransportStateListener transportStateListener);
}
