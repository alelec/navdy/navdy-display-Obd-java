package android.support.v4.media;

@android.annotation.TargetApi(18)
@android.support.annotation.RequiresApi(18)
class TransportMediatorJellybeanMR2 {
    android.media.AudioManager.OnAudioFocusChangeListener mAudioFocusChangeListener = new android.media.AudioManager.OnAudioFocusChangeListener() {
        public void onAudioFocusChange(int focusChange) {
            android.support.v4.media.TransportMediatorJellybeanMR2.this.mTransportCallback.handleAudioFocusChange(focusChange);
        }
    };
    boolean mAudioFocused;
    final android.media.AudioManager mAudioManager;
    final android.content.Context mContext;
    boolean mFocused;
    final android.media.RemoteControlClient.OnGetPlaybackPositionListener mGetPlaybackPositionListener = new android.media.RemoteControlClient.OnGetPlaybackPositionListener() {
        public long onGetPlaybackPosition() {
            return android.support.v4.media.TransportMediatorJellybeanMR2.this.mTransportCallback.getPlaybackPosition();
        }
    };
    final android.content.Intent mIntent;
    final android.content.BroadcastReceiver mMediaButtonReceiver = new android.content.BroadcastReceiver() {
        public void onReceive(android.content.Context context, android.content.Intent intent) {
            try {
                android.support.v4.media.TransportMediatorJellybeanMR2.this.mTransportCallback.handleKey((android.view.KeyEvent) intent.getParcelableExtra("android.intent.extra.KEY_EVENT"));
            } catch (java.lang.ClassCastException e) {
                android.util.Log.w("TransportController", e);
            }
        }
    };
    android.app.PendingIntent mPendingIntent;
    int mPlayState = 0;
    final android.media.RemoteControlClient.OnPlaybackPositionUpdateListener mPlaybackPositionUpdateListener = new android.media.RemoteControlClient.OnPlaybackPositionUpdateListener() {
        public void onPlaybackPositionUpdate(long newPositionMs) {
            android.support.v4.media.TransportMediatorJellybeanMR2.this.mTransportCallback.playbackPositionUpdate(newPositionMs);
        }
    };
    final java.lang.String mReceiverAction;
    final android.content.IntentFilter mReceiverFilter;
    android.media.RemoteControlClient mRemoteControl;
    final android.view.View mTargetView;
    final android.support.v4.media.TransportMediatorCallback mTransportCallback;
    final android.view.ViewTreeObserver.OnWindowAttachListener mWindowAttachListener = new android.view.ViewTreeObserver.OnWindowAttachListener() {
        public void onWindowAttached() {
            android.support.v4.media.TransportMediatorJellybeanMR2.this.windowAttached();
        }

        public void onWindowDetached() {
            android.support.v4.media.TransportMediatorJellybeanMR2.this.windowDetached();
        }
    };
    final android.view.ViewTreeObserver.OnWindowFocusChangeListener mWindowFocusListener = new android.view.ViewTreeObserver.OnWindowFocusChangeListener() {
        public void onWindowFocusChanged(boolean hasFocus) {
            if (hasFocus) {
                android.support.v4.media.TransportMediatorJellybeanMR2.this.gainFocus();
            } else {
                android.support.v4.media.TransportMediatorJellybeanMR2.this.loseFocus();
            }
        }
    };

    public TransportMediatorJellybeanMR2(android.content.Context context, android.media.AudioManager audioManager, android.view.View view, android.support.v4.media.TransportMediatorCallback transportCallback) {
        this.mContext = context;
        this.mAudioManager = audioManager;
        this.mTargetView = view;
        this.mTransportCallback = transportCallback;
        this.mReceiverAction = context.getPackageName() + ":transport:" + java.lang.System.identityHashCode(this);
        this.mIntent = new android.content.Intent(this.mReceiverAction);
        this.mIntent.setPackage(context.getPackageName());
        this.mReceiverFilter = new android.content.IntentFilter();
        this.mReceiverFilter.addAction(this.mReceiverAction);
        this.mTargetView.getViewTreeObserver().addOnWindowAttachListener(this.mWindowAttachListener);
        this.mTargetView.getViewTreeObserver().addOnWindowFocusChangeListener(this.mWindowFocusListener);
    }

    public java.lang.Object getRemoteControlClient() {
        return this.mRemoteControl;
    }

    public void destroy() {
        windowDetached();
        this.mTargetView.getViewTreeObserver().removeOnWindowAttachListener(this.mWindowAttachListener);
        this.mTargetView.getViewTreeObserver().removeOnWindowFocusChangeListener(this.mWindowFocusListener);
    }

    /* access modifiers changed from: 0000 */
    public void windowAttached() {
        this.mContext.registerReceiver(this.mMediaButtonReceiver, this.mReceiverFilter);
        this.mPendingIntent = android.app.PendingIntent.getBroadcast(this.mContext, 0, this.mIntent, 268435456);
        this.mRemoteControl = new android.media.RemoteControlClient(this.mPendingIntent);
        this.mRemoteControl.setOnGetPlaybackPositionListener(this.mGetPlaybackPositionListener);
        this.mRemoteControl.setPlaybackPositionUpdateListener(this.mPlaybackPositionUpdateListener);
    }

    /* access modifiers changed from: 0000 */
    public void gainFocus() {
        if (!this.mFocused) {
            this.mFocused = true;
            this.mAudioManager.registerMediaButtonEventReceiver(this.mPendingIntent);
            this.mAudioManager.registerRemoteControlClient(this.mRemoteControl);
            if (this.mPlayState == 3) {
                takeAudioFocus();
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void takeAudioFocus() {
        if (!this.mAudioFocused) {
            this.mAudioFocused = true;
            this.mAudioManager.requestAudioFocus(this.mAudioFocusChangeListener, 3, 1);
        }
    }

    public void startPlaying() {
        if (this.mPlayState != 3) {
            this.mPlayState = 3;
            this.mRemoteControl.setPlaybackState(3);
        }
        if (this.mFocused) {
            takeAudioFocus();
        }
    }

    public void refreshState(boolean playing, long position, int transportControls) {
        if (this.mRemoteControl != null) {
            this.mRemoteControl.setPlaybackState(playing ? 3 : 1, position, playing ? 1.0f : 0.0f);
            this.mRemoteControl.setTransportControlFlags(transportControls);
        }
    }

    public void pausePlaying() {
        if (this.mPlayState == 3) {
            this.mPlayState = 2;
            this.mRemoteControl.setPlaybackState(2);
        }
        dropAudioFocus();
    }

    public void stopPlaying() {
        if (this.mPlayState != 1) {
            this.mPlayState = 1;
            this.mRemoteControl.setPlaybackState(1);
        }
        dropAudioFocus();
    }

    /* access modifiers changed from: 0000 */
    public void dropAudioFocus() {
        if (this.mAudioFocused) {
            this.mAudioFocused = false;
            this.mAudioManager.abandonAudioFocus(this.mAudioFocusChangeListener);
        }
    }

    /* access modifiers changed from: 0000 */
    public void loseFocus() {
        dropAudioFocus();
        if (this.mFocused) {
            this.mFocused = false;
            this.mAudioManager.unregisterRemoteControlClient(this.mRemoteControl);
            this.mAudioManager.unregisterMediaButtonEventReceiver(this.mPendingIntent);
        }
    }

    /* access modifiers changed from: 0000 */
    public void windowDetached() {
        loseFocus();
        if (this.mPendingIntent != null) {
            this.mContext.unregisterReceiver(this.mMediaButtonReceiver);
            this.mPendingIntent.cancel();
            this.mPendingIntent = null;
            this.mRemoteControl = null;
        }
    }
}
