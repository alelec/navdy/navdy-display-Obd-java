package android.support.v4.media;

public final class MediaMetadataCompat implements android.os.Parcelable {
    public static final android.os.Parcelable.Creator<android.support.v4.media.MediaMetadataCompat> CREATOR = new android.os.Parcelable.Creator<android.support.v4.media.MediaMetadataCompat>() {
        public android.support.v4.media.MediaMetadataCompat createFromParcel(android.os.Parcel in) {
            return new android.support.v4.media.MediaMetadataCompat(in);
        }

        public android.support.v4.media.MediaMetadataCompat[] newArray(int size) {
            return new android.support.v4.media.MediaMetadataCompat[size];
        }
    };
    static final android.support.v4.util.ArrayMap<java.lang.String, java.lang.Integer> METADATA_KEYS_TYPE = new android.support.v4.util.ArrayMap<>();
    public static final java.lang.String METADATA_KEY_ADVERTISEMENT = "android.media.metadata.ADVERTISEMENT";
    public static final java.lang.String METADATA_KEY_ALBUM = "android.media.metadata.ALBUM";
    public static final java.lang.String METADATA_KEY_ALBUM_ART = "android.media.metadata.ALBUM_ART";
    public static final java.lang.String METADATA_KEY_ALBUM_ARTIST = "android.media.metadata.ALBUM_ARTIST";
    public static final java.lang.String METADATA_KEY_ALBUM_ART_URI = "android.media.metadata.ALBUM_ART_URI";
    public static final java.lang.String METADATA_KEY_ART = "android.media.metadata.ART";
    public static final java.lang.String METADATA_KEY_ARTIST = "android.media.metadata.ARTIST";
    public static final java.lang.String METADATA_KEY_ART_URI = "android.media.metadata.ART_URI";
    public static final java.lang.String METADATA_KEY_AUTHOR = "android.media.metadata.AUTHOR";
    public static final java.lang.String METADATA_KEY_BT_FOLDER_TYPE = "android.media.metadata.BT_FOLDER_TYPE";
    public static final java.lang.String METADATA_KEY_COMPILATION = "android.media.metadata.COMPILATION";
    public static final java.lang.String METADATA_KEY_COMPOSER = "android.media.metadata.COMPOSER";
    public static final java.lang.String METADATA_KEY_DATE = "android.media.metadata.DATE";
    public static final java.lang.String METADATA_KEY_DISC_NUMBER = "android.media.metadata.DISC_NUMBER";
    public static final java.lang.String METADATA_KEY_DISPLAY_DESCRIPTION = "android.media.metadata.DISPLAY_DESCRIPTION";
    public static final java.lang.String METADATA_KEY_DISPLAY_ICON = "android.media.metadata.DISPLAY_ICON";
    public static final java.lang.String METADATA_KEY_DISPLAY_ICON_URI = "android.media.metadata.DISPLAY_ICON_URI";
    public static final java.lang.String METADATA_KEY_DISPLAY_SUBTITLE = "android.media.metadata.DISPLAY_SUBTITLE";
    public static final java.lang.String METADATA_KEY_DISPLAY_TITLE = "android.media.metadata.DISPLAY_TITLE";
    public static final java.lang.String METADATA_KEY_DURATION = "android.media.metadata.DURATION";
    public static final java.lang.String METADATA_KEY_GENRE = "android.media.metadata.GENRE";
    public static final java.lang.String METADATA_KEY_MEDIA_ID = "android.media.metadata.MEDIA_ID";
    public static final java.lang.String METADATA_KEY_MEDIA_URI = "android.media.metadata.MEDIA_URI";
    public static final java.lang.String METADATA_KEY_NUM_TRACKS = "android.media.metadata.NUM_TRACKS";
    public static final java.lang.String METADATA_KEY_RATING = "android.media.metadata.RATING";
    public static final java.lang.String METADATA_KEY_TITLE = "android.media.metadata.TITLE";
    public static final java.lang.String METADATA_KEY_TRACK_NUMBER = "android.media.metadata.TRACK_NUMBER";
    public static final java.lang.String METADATA_KEY_USER_RATING = "android.media.metadata.USER_RATING";
    public static final java.lang.String METADATA_KEY_WRITER = "android.media.metadata.WRITER";
    public static final java.lang.String METADATA_KEY_YEAR = "android.media.metadata.YEAR";
    static final int METADATA_TYPE_BITMAP = 2;
    static final int METADATA_TYPE_LONG = 0;
    static final int METADATA_TYPE_RATING = 3;
    static final int METADATA_TYPE_TEXT = 1;
    private static final java.lang.String[] PREFERRED_BITMAP_ORDER = {METADATA_KEY_DISPLAY_ICON, METADATA_KEY_ART, METADATA_KEY_ALBUM_ART};
    private static final java.lang.String[] PREFERRED_DESCRIPTION_ORDER = {METADATA_KEY_TITLE, METADATA_KEY_ARTIST, METADATA_KEY_ALBUM, METADATA_KEY_ALBUM_ARTIST, METADATA_KEY_WRITER, METADATA_KEY_AUTHOR, METADATA_KEY_COMPOSER};
    private static final java.lang.String[] PREFERRED_URI_ORDER = {METADATA_KEY_DISPLAY_ICON_URI, METADATA_KEY_ART_URI, METADATA_KEY_ALBUM_ART_URI};
    private static final java.lang.String TAG = "MediaMetadata";
    final android.os.Bundle mBundle;
    private android.support.v4.media.MediaDescriptionCompat mDescription;
    private java.lang.Object mMetadataObj;

    @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
    @java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.SOURCE)
    public @interface BitmapKey {
    }

    public static final class Builder {
        private final android.os.Bundle mBundle;

        public Builder() {
            this.mBundle = new android.os.Bundle();
        }

        public Builder(android.support.v4.media.MediaMetadataCompat source) {
            this.mBundle = new android.os.Bundle(source.mBundle);
        }

        @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
        public Builder(android.support.v4.media.MediaMetadataCompat source, int maxBitmapSize) {
            this(source);
            for (java.lang.String key : this.mBundle.keySet()) {
                java.lang.Object value = this.mBundle.get(key);
                if (value != null && (value instanceof android.graphics.Bitmap)) {
                    android.graphics.Bitmap bmp = (android.graphics.Bitmap) value;
                    if (bmp.getHeight() > maxBitmapSize || bmp.getWidth() > maxBitmapSize) {
                        putBitmap(key, scaleBitmap(bmp, maxBitmapSize));
                    } else if (android.os.Build.VERSION.SDK_INT >= 14 && (key.equals(android.support.v4.media.MediaMetadataCompat.METADATA_KEY_ART) || key.equals(android.support.v4.media.MediaMetadataCompat.METADATA_KEY_ALBUM_ART))) {
                        putBitmap(key, bmp.copy(bmp.getConfig(), false));
                    }
                }
            }
        }

        public android.support.v4.media.MediaMetadataCompat.Builder putText(java.lang.String key, java.lang.CharSequence value) {
            if (!android.support.v4.media.MediaMetadataCompat.METADATA_KEYS_TYPE.containsKey(key) || ((java.lang.Integer) android.support.v4.media.MediaMetadataCompat.METADATA_KEYS_TYPE.get(key)).intValue() == 1) {
                this.mBundle.putCharSequence(key, value);
                return this;
            }
            throw new java.lang.IllegalArgumentException("The " + key + " key cannot be used to put a CharSequence");
        }

        public android.support.v4.media.MediaMetadataCompat.Builder putString(java.lang.String key, java.lang.String value) {
            if (!android.support.v4.media.MediaMetadataCompat.METADATA_KEYS_TYPE.containsKey(key) || ((java.lang.Integer) android.support.v4.media.MediaMetadataCompat.METADATA_KEYS_TYPE.get(key)).intValue() == 1) {
                this.mBundle.putCharSequence(key, value);
                return this;
            }
            throw new java.lang.IllegalArgumentException("The " + key + " key cannot be used to put a String");
        }

        public android.support.v4.media.MediaMetadataCompat.Builder putLong(java.lang.String key, long value) {
            if (!android.support.v4.media.MediaMetadataCompat.METADATA_KEYS_TYPE.containsKey(key) || ((java.lang.Integer) android.support.v4.media.MediaMetadataCompat.METADATA_KEYS_TYPE.get(key)).intValue() == 0) {
                this.mBundle.putLong(key, value);
                return this;
            }
            throw new java.lang.IllegalArgumentException("The " + key + " key cannot be used to put a long");
        }

        public android.support.v4.media.MediaMetadataCompat.Builder putRating(java.lang.String key, android.support.v4.media.RatingCompat value) {
            if (!android.support.v4.media.MediaMetadataCompat.METADATA_KEYS_TYPE.containsKey(key) || ((java.lang.Integer) android.support.v4.media.MediaMetadataCompat.METADATA_KEYS_TYPE.get(key)).intValue() == 3) {
                if (android.os.Build.VERSION.SDK_INT >= 19) {
                    this.mBundle.putParcelable(key, (android.os.Parcelable) value.getRating());
                } else {
                    this.mBundle.putParcelable(key, value);
                }
                return this;
            }
            throw new java.lang.IllegalArgumentException("The " + key + " key cannot be used to put a Rating");
        }

        public android.support.v4.media.MediaMetadataCompat.Builder putBitmap(java.lang.String key, android.graphics.Bitmap value) {
            if (!android.support.v4.media.MediaMetadataCompat.METADATA_KEYS_TYPE.containsKey(key) || ((java.lang.Integer) android.support.v4.media.MediaMetadataCompat.METADATA_KEYS_TYPE.get(key)).intValue() == 2) {
                this.mBundle.putParcelable(key, value);
                return this;
            }
            throw new java.lang.IllegalArgumentException("The " + key + " key cannot be used to put a Bitmap");
        }

        public android.support.v4.media.MediaMetadataCompat build() {
            return new android.support.v4.media.MediaMetadataCompat(this.mBundle);
        }

        private android.graphics.Bitmap scaleBitmap(android.graphics.Bitmap bmp, int maxSize) {
            float maxSizeF = (float) maxSize;
            float scale = java.lang.Math.min(maxSizeF / ((float) bmp.getWidth()), maxSizeF / ((float) bmp.getHeight()));
            return android.graphics.Bitmap.createScaledBitmap(bmp, (int) (((float) bmp.getWidth()) * scale), (int) (((float) bmp.getHeight()) * scale), true);
        }
    }

    @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
    @java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.SOURCE)
    public @interface LongKey {
    }

    @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
    @java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.SOURCE)
    public @interface RatingKey {
    }

    @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
    @java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.SOURCE)
    public @interface TextKey {
    }

    static {
        METADATA_KEYS_TYPE.put(METADATA_KEY_TITLE, java.lang.Integer.valueOf(1));
        METADATA_KEYS_TYPE.put(METADATA_KEY_ARTIST, java.lang.Integer.valueOf(1));
        METADATA_KEYS_TYPE.put(METADATA_KEY_DURATION, java.lang.Integer.valueOf(0));
        METADATA_KEYS_TYPE.put(METADATA_KEY_ALBUM, java.lang.Integer.valueOf(1));
        METADATA_KEYS_TYPE.put(METADATA_KEY_AUTHOR, java.lang.Integer.valueOf(1));
        METADATA_KEYS_TYPE.put(METADATA_KEY_WRITER, java.lang.Integer.valueOf(1));
        METADATA_KEYS_TYPE.put(METADATA_KEY_COMPOSER, java.lang.Integer.valueOf(1));
        METADATA_KEYS_TYPE.put(METADATA_KEY_COMPILATION, java.lang.Integer.valueOf(1));
        METADATA_KEYS_TYPE.put(METADATA_KEY_DATE, java.lang.Integer.valueOf(1));
        METADATA_KEYS_TYPE.put(METADATA_KEY_YEAR, java.lang.Integer.valueOf(0));
        METADATA_KEYS_TYPE.put(METADATA_KEY_GENRE, java.lang.Integer.valueOf(1));
        METADATA_KEYS_TYPE.put(METADATA_KEY_TRACK_NUMBER, java.lang.Integer.valueOf(0));
        METADATA_KEYS_TYPE.put(METADATA_KEY_NUM_TRACKS, java.lang.Integer.valueOf(0));
        METADATA_KEYS_TYPE.put(METADATA_KEY_DISC_NUMBER, java.lang.Integer.valueOf(0));
        METADATA_KEYS_TYPE.put(METADATA_KEY_ALBUM_ARTIST, java.lang.Integer.valueOf(1));
        METADATA_KEYS_TYPE.put(METADATA_KEY_ART, java.lang.Integer.valueOf(2));
        METADATA_KEYS_TYPE.put(METADATA_KEY_ART_URI, java.lang.Integer.valueOf(1));
        METADATA_KEYS_TYPE.put(METADATA_KEY_ALBUM_ART, java.lang.Integer.valueOf(2));
        METADATA_KEYS_TYPE.put(METADATA_KEY_ALBUM_ART_URI, java.lang.Integer.valueOf(1));
        METADATA_KEYS_TYPE.put(METADATA_KEY_USER_RATING, java.lang.Integer.valueOf(3));
        METADATA_KEYS_TYPE.put(METADATA_KEY_RATING, java.lang.Integer.valueOf(3));
        METADATA_KEYS_TYPE.put(METADATA_KEY_DISPLAY_TITLE, java.lang.Integer.valueOf(1));
        METADATA_KEYS_TYPE.put(METADATA_KEY_DISPLAY_SUBTITLE, java.lang.Integer.valueOf(1));
        METADATA_KEYS_TYPE.put(METADATA_KEY_DISPLAY_DESCRIPTION, java.lang.Integer.valueOf(1));
        METADATA_KEYS_TYPE.put(METADATA_KEY_DISPLAY_ICON, java.lang.Integer.valueOf(2));
        METADATA_KEYS_TYPE.put(METADATA_KEY_DISPLAY_ICON_URI, java.lang.Integer.valueOf(1));
        METADATA_KEYS_TYPE.put(METADATA_KEY_MEDIA_ID, java.lang.Integer.valueOf(1));
        METADATA_KEYS_TYPE.put(METADATA_KEY_BT_FOLDER_TYPE, java.lang.Integer.valueOf(0));
        METADATA_KEYS_TYPE.put(METADATA_KEY_MEDIA_URI, java.lang.Integer.valueOf(1));
        METADATA_KEYS_TYPE.put(METADATA_KEY_ADVERTISEMENT, java.lang.Integer.valueOf(0));
    }

    MediaMetadataCompat(android.os.Bundle bundle) {
        this.mBundle = new android.os.Bundle(bundle);
    }

    MediaMetadataCompat(android.os.Parcel in) {
        this.mBundle = in.readBundle();
    }

    public boolean containsKey(java.lang.String key) {
        return this.mBundle.containsKey(key);
    }

    public java.lang.CharSequence getText(java.lang.String key) {
        return this.mBundle.getCharSequence(key);
    }

    public java.lang.String getString(java.lang.String key) {
        java.lang.CharSequence text = this.mBundle.getCharSequence(key);
        if (text != null) {
            return text.toString();
        }
        return null;
    }

    public long getLong(java.lang.String key) {
        return this.mBundle.getLong(key, 0);
    }

    public android.support.v4.media.RatingCompat getRating(java.lang.String key) {
        try {
            if (android.os.Build.VERSION.SDK_INT >= 19) {
                return android.support.v4.media.RatingCompat.fromRating(this.mBundle.getParcelable(key));
            }
            return (android.support.v4.media.RatingCompat) this.mBundle.getParcelable(key);
        } catch (java.lang.Exception e) {
            android.util.Log.w(TAG, "Failed to retrieve a key as Rating.", e);
            return null;
        }
    }

    public android.graphics.Bitmap getBitmap(java.lang.String key) {
        android.graphics.Bitmap bmp = null;
        try {
            return (android.graphics.Bitmap) this.mBundle.getParcelable(key);
        } catch (java.lang.Exception e) {
            android.util.Log.w(TAG, "Failed to retrieve a key as Bitmap.", e);
            return bmp;
        }
    }

    public android.support.v4.media.MediaDescriptionCompat getDescription() {
        if (this.mDescription != null) {
            return this.mDescription;
        }
        java.lang.String mediaId = getString(METADATA_KEY_MEDIA_ID);
        java.lang.CharSequence[] text = new java.lang.CharSequence[3];
        android.graphics.Bitmap icon = null;
        android.net.Uri iconUri = null;
        java.lang.CharSequence displayText = getText(METADATA_KEY_DISPLAY_TITLE);
        if (!android.text.TextUtils.isEmpty(displayText)) {
            text[0] = displayText;
            text[1] = getText(METADATA_KEY_DISPLAY_SUBTITLE);
            text[2] = getText(METADATA_KEY_DISPLAY_DESCRIPTION);
        } else {
            int textIndex = 0;
            int keyIndex = 0;
            while (textIndex < text.length && keyIndex < PREFERRED_DESCRIPTION_ORDER.length) {
                int keyIndex2 = keyIndex + 1;
                java.lang.CharSequence next = getText(PREFERRED_DESCRIPTION_ORDER[keyIndex]);
                if (!android.text.TextUtils.isEmpty(next)) {
                    int textIndex2 = textIndex + 1;
                    text[textIndex] = next;
                    textIndex = textIndex2;
                }
                keyIndex = keyIndex2;
            }
        }
        int i = 0;
        while (true) {
            if (i >= PREFERRED_BITMAP_ORDER.length) {
                break;
            }
            android.graphics.Bitmap next2 = getBitmap(PREFERRED_BITMAP_ORDER[i]);
            if (next2 != null) {
                icon = next2;
                break;
            }
            i++;
        }
        int i2 = 0;
        while (true) {
            if (i2 >= PREFERRED_URI_ORDER.length) {
                break;
            }
            java.lang.String next3 = getString(PREFERRED_URI_ORDER[i2]);
            if (!android.text.TextUtils.isEmpty(next3)) {
                iconUri = android.net.Uri.parse(next3);
                break;
            }
            i2++;
        }
        android.net.Uri mediaUri = null;
        java.lang.String mediaUriStr = getString(METADATA_KEY_MEDIA_URI);
        if (!android.text.TextUtils.isEmpty(mediaUriStr)) {
            mediaUri = android.net.Uri.parse(mediaUriStr);
        }
        android.support.v4.media.MediaDescriptionCompat.Builder bob = new android.support.v4.media.MediaDescriptionCompat.Builder();
        bob.setMediaId(mediaId);
        bob.setTitle(text[0]);
        bob.setSubtitle(text[1]);
        bob.setDescription(text[2]);
        bob.setIconBitmap(icon);
        bob.setIconUri(iconUri);
        bob.setMediaUri(mediaUri);
        if (this.mBundle.containsKey(METADATA_KEY_BT_FOLDER_TYPE)) {
            android.os.Bundle bundle = new android.os.Bundle();
            bundle.putLong(android.support.v4.media.MediaDescriptionCompat.EXTRA_BT_FOLDER_TYPE, getLong(METADATA_KEY_BT_FOLDER_TYPE));
            bob.setExtras(bundle);
        }
        this.mDescription = bob.build();
        return this.mDescription;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeBundle(this.mBundle);
    }

    public int size() {
        return this.mBundle.size();
    }

    public java.util.Set<java.lang.String> keySet() {
        return this.mBundle.keySet();
    }

    public android.os.Bundle getBundle() {
        return this.mBundle;
    }

    public static android.support.v4.media.MediaMetadataCompat fromMediaMetadata(java.lang.Object metadataObj) {
        if (metadataObj == null || android.os.Build.VERSION.SDK_INT < 21) {
            return null;
        }
        android.os.Parcel p = android.os.Parcel.obtain();
        android.support.v4.media.MediaMetadataCompatApi21.writeToParcel(metadataObj, p, 0);
        p.setDataPosition(0);
        android.support.v4.media.MediaMetadataCompat metadata = (android.support.v4.media.MediaMetadataCompat) CREATOR.createFromParcel(p);
        p.recycle();
        metadata.mMetadataObj = metadataObj;
        return metadata;
    }

    public java.lang.Object getMediaMetadata() {
        if (this.mMetadataObj != null || android.os.Build.VERSION.SDK_INT < 21) {
            return this.mMetadataObj;
        }
        android.os.Parcel p = android.os.Parcel.obtain();
        writeToParcel(p, 0);
        p.setDataPosition(0);
        this.mMetadataObj = android.support.v4.media.MediaMetadataCompatApi21.createFromParcel(p);
        p.recycle();
        return this.mMetadataObj;
    }
}
