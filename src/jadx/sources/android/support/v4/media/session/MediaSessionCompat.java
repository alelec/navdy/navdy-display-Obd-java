package android.support.v4.media.session;

public class MediaSessionCompat {
    static final java.lang.String ACTION_ARGUMENT_EXTRAS = "android.support.v4.media.session.action.ARGUMENT_EXTRAS";
    static final java.lang.String ACTION_ARGUMENT_MEDIA_ID = "android.support.v4.media.session.action.ARGUMENT_MEDIA_ID";
    static final java.lang.String ACTION_ARGUMENT_QUERY = "android.support.v4.media.session.action.ARGUMENT_QUERY";
    static final java.lang.String ACTION_ARGUMENT_REPEAT_MODE = "android.support.v4.media.session.action.ARGUMENT_REPEAT_MODE";
    static final java.lang.String ACTION_ARGUMENT_SHUFFLE_MODE_ENABLED = "android.support.v4.media.session.action.ARGUMENT_SHUFFLE_MODE_ENABLED";
    static final java.lang.String ACTION_ARGUMENT_URI = "android.support.v4.media.session.action.ARGUMENT_URI";
    static final java.lang.String ACTION_PLAY_FROM_URI = "android.support.v4.media.session.action.PLAY_FROM_URI";
    static final java.lang.String ACTION_PREPARE = "android.support.v4.media.session.action.PREPARE";
    static final java.lang.String ACTION_PREPARE_FROM_MEDIA_ID = "android.support.v4.media.session.action.PREPARE_FROM_MEDIA_ID";
    static final java.lang.String ACTION_PREPARE_FROM_SEARCH = "android.support.v4.media.session.action.PREPARE_FROM_SEARCH";
    static final java.lang.String ACTION_PREPARE_FROM_URI = "android.support.v4.media.session.action.PREPARE_FROM_URI";
    static final java.lang.String ACTION_SET_REPEAT_MODE = "android.support.v4.media.session.action.SET_REPEAT_MODE";
    static final java.lang.String ACTION_SET_SHUFFLE_MODE_ENABLED = "android.support.v4.media.session.action.SET_SHUFFLE_MODE_ENABLED";
    static final java.lang.String EXTRA_BINDER = "android.support.v4.media.session.EXTRA_BINDER";
    public static final int FLAG_HANDLES_MEDIA_BUTTONS = 1;
    public static final int FLAG_HANDLES_QUEUE_COMMANDS = 4;
    public static final int FLAG_HANDLES_TRANSPORT_CONTROLS = 2;
    private static final int MAX_BITMAP_SIZE_IN_DP = 320;
    static final java.lang.String TAG = "MediaSessionCompat";
    static int sMaxBitmapSize;
    private final java.util.ArrayList<android.support.v4.media.session.MediaSessionCompat.OnActiveChangeListener> mActiveListeners;
    private final android.support.v4.media.session.MediaControllerCompat mController;
    private final android.support.v4.media.session.MediaSessionCompat.MediaSessionImpl mImpl;

    public static abstract class Callback {
        final java.lang.Object mCallbackObj;
        java.lang.ref.WeakReference<android.support.v4.media.session.MediaSessionCompat.MediaSessionImpl> mSessionImpl;

        private class StubApi21 implements android.support.v4.media.session.MediaSessionCompatApi21.Callback {
            StubApi21() {
            }

            public void onCommand(java.lang.String command, android.os.Bundle extras, android.os.ResultReceiver cb) {
                if (command.equals("android.support.v4.media.session.command.GET_EXTRA_BINDER")) {
                    android.support.v4.media.session.MediaSessionCompat.MediaSessionImplApi21 impl = (android.support.v4.media.session.MediaSessionCompat.MediaSessionImplApi21) android.support.v4.media.session.MediaSessionCompat.Callback.this.mSessionImpl.get();
                    if (impl != null) {
                        android.os.Bundle result = new android.os.Bundle();
                        android.support.v4.app.BundleCompat.putBinder(result, android.support.v4.media.session.MediaSessionCompat.EXTRA_BINDER, impl.getExtraSessionBinder());
                        cb.send(0, result);
                    }
                } else if (command.equals("android.support.v4.media.session.command.ADD_QUEUE_ITEM")) {
                    extras.setClassLoader(android.support.v4.media.MediaDescriptionCompat.class.getClassLoader());
                    android.support.v4.media.session.MediaSessionCompat.Callback.this.onAddQueueItem((android.support.v4.media.MediaDescriptionCompat) extras.getParcelable("android.support.v4.media.session.command.ARGUMENT_MEDIA_DESCRIPTION"));
                } else if (command.equals("android.support.v4.media.session.command.ADD_QUEUE_ITEM_AT")) {
                    extras.setClassLoader(android.support.v4.media.MediaDescriptionCompat.class.getClassLoader());
                    android.support.v4.media.session.MediaSessionCompat.Callback.this.onAddQueueItem((android.support.v4.media.MediaDescriptionCompat) extras.getParcelable("android.support.v4.media.session.command.ARGUMENT_MEDIA_DESCRIPTION"), extras.getInt("android.support.v4.media.session.command.ARGUMENT_INDEX"));
                } else if (command.equals("android.support.v4.media.session.command.REMOVE_QUEUE_ITEM")) {
                    extras.setClassLoader(android.support.v4.media.MediaDescriptionCompat.class.getClassLoader());
                    android.support.v4.media.session.MediaSessionCompat.Callback.this.onRemoveQueueItem((android.support.v4.media.MediaDescriptionCompat) extras.getParcelable("android.support.v4.media.session.command.ARGUMENT_MEDIA_DESCRIPTION"));
                } else if (command.equals("android.support.v4.media.session.command.REMOVE_QUEUE_ITEM_AT")) {
                    android.support.v4.media.session.MediaSessionCompat.Callback.this.onRemoveQueueItemAt(extras.getInt("android.support.v4.media.session.command.ARGUMENT_INDEX"));
                } else {
                    android.support.v4.media.session.MediaSessionCompat.Callback.this.onCommand(command, extras, cb);
                }
            }

            public boolean onMediaButtonEvent(android.content.Intent mediaButtonIntent) {
                return android.support.v4.media.session.MediaSessionCompat.Callback.this.onMediaButtonEvent(mediaButtonIntent);
            }

            public void onPlay() {
                android.support.v4.media.session.MediaSessionCompat.Callback.this.onPlay();
            }

            public void onPlayFromMediaId(java.lang.String mediaId, android.os.Bundle extras) {
                android.support.v4.media.session.MediaSessionCompat.Callback.this.onPlayFromMediaId(mediaId, extras);
            }

            public void onPlayFromSearch(java.lang.String search, android.os.Bundle extras) {
                android.support.v4.media.session.MediaSessionCompat.Callback.this.onPlayFromSearch(search, extras);
            }

            public void onSkipToQueueItem(long id) {
                android.support.v4.media.session.MediaSessionCompat.Callback.this.onSkipToQueueItem(id);
            }

            public void onPause() {
                android.support.v4.media.session.MediaSessionCompat.Callback.this.onPause();
            }

            public void onSkipToNext() {
                android.support.v4.media.session.MediaSessionCompat.Callback.this.onSkipToNext();
            }

            public void onSkipToPrevious() {
                android.support.v4.media.session.MediaSessionCompat.Callback.this.onSkipToPrevious();
            }

            public void onFastForward() {
                android.support.v4.media.session.MediaSessionCompat.Callback.this.onFastForward();
            }

            public void onRewind() {
                android.support.v4.media.session.MediaSessionCompat.Callback.this.onRewind();
            }

            public void onStop() {
                android.support.v4.media.session.MediaSessionCompat.Callback.this.onStop();
            }

            public void onSeekTo(long pos) {
                android.support.v4.media.session.MediaSessionCompat.Callback.this.onSeekTo(pos);
            }

            public void onSetRating(java.lang.Object ratingObj) {
                android.support.v4.media.session.MediaSessionCompat.Callback.this.onSetRating(android.support.v4.media.RatingCompat.fromRating(ratingObj));
            }

            public void onCustomAction(java.lang.String action, android.os.Bundle extras) {
                if (action.equals(android.support.v4.media.session.MediaSessionCompat.ACTION_PLAY_FROM_URI)) {
                    android.support.v4.media.session.MediaSessionCompat.Callback.this.onPlayFromUri((android.net.Uri) extras.getParcelable(android.support.v4.media.session.MediaSessionCompat.ACTION_ARGUMENT_URI), (android.os.Bundle) extras.getParcelable(android.support.v4.media.session.MediaSessionCompat.ACTION_ARGUMENT_EXTRAS));
                } else if (action.equals(android.support.v4.media.session.MediaSessionCompat.ACTION_PREPARE)) {
                    android.support.v4.media.session.MediaSessionCompat.Callback.this.onPrepare();
                } else if (action.equals(android.support.v4.media.session.MediaSessionCompat.ACTION_PREPARE_FROM_MEDIA_ID)) {
                    android.support.v4.media.session.MediaSessionCompat.Callback.this.onPrepareFromMediaId(extras.getString(android.support.v4.media.session.MediaSessionCompat.ACTION_ARGUMENT_MEDIA_ID), extras.getBundle(android.support.v4.media.session.MediaSessionCompat.ACTION_ARGUMENT_EXTRAS));
                } else if (action.equals(android.support.v4.media.session.MediaSessionCompat.ACTION_PREPARE_FROM_SEARCH)) {
                    android.support.v4.media.session.MediaSessionCompat.Callback.this.onPrepareFromSearch(extras.getString(android.support.v4.media.session.MediaSessionCompat.ACTION_ARGUMENT_QUERY), extras.getBundle(android.support.v4.media.session.MediaSessionCompat.ACTION_ARGUMENT_EXTRAS));
                } else if (action.equals(android.support.v4.media.session.MediaSessionCompat.ACTION_PREPARE_FROM_URI)) {
                    android.support.v4.media.session.MediaSessionCompat.Callback.this.onPrepareFromUri((android.net.Uri) extras.getParcelable(android.support.v4.media.session.MediaSessionCompat.ACTION_ARGUMENT_URI), extras.getBundle(android.support.v4.media.session.MediaSessionCompat.ACTION_ARGUMENT_EXTRAS));
                } else if (action.equals(android.support.v4.media.session.MediaSessionCompat.ACTION_SET_REPEAT_MODE)) {
                    android.support.v4.media.session.MediaSessionCompat.Callback.this.onSetRepeatMode(extras.getInt(android.support.v4.media.session.MediaSessionCompat.ACTION_ARGUMENT_REPEAT_MODE));
                } else if (action.equals(android.support.v4.media.session.MediaSessionCompat.ACTION_SET_SHUFFLE_MODE_ENABLED)) {
                    android.support.v4.media.session.MediaSessionCompat.Callback.this.onSetShuffleModeEnabled(extras.getBoolean(android.support.v4.media.session.MediaSessionCompat.ACTION_ARGUMENT_SHUFFLE_MODE_ENABLED));
                } else {
                    android.support.v4.media.session.MediaSessionCompat.Callback.this.onCustomAction(action, extras);
                }
            }
        }

        private class StubApi23 extends android.support.v4.media.session.MediaSessionCompat.Callback.StubApi21 implements android.support.v4.media.session.MediaSessionCompatApi23.Callback {
            StubApi23() {
                super();
            }

            public void onPlayFromUri(android.net.Uri uri, android.os.Bundle extras) {
                android.support.v4.media.session.MediaSessionCompat.Callback.this.onPlayFromUri(uri, extras);
            }
        }

        private class StubApi24 extends android.support.v4.media.session.MediaSessionCompat.Callback.StubApi23 implements android.support.v4.media.session.MediaSessionCompatApi24.Callback {
            StubApi24() {
                super();
            }

            public void onPrepare() {
                android.support.v4.media.session.MediaSessionCompat.Callback.this.onPrepare();
            }

            public void onPrepareFromMediaId(java.lang.String mediaId, android.os.Bundle extras) {
                android.support.v4.media.session.MediaSessionCompat.Callback.this.onPrepareFromMediaId(mediaId, extras);
            }

            public void onPrepareFromSearch(java.lang.String query, android.os.Bundle extras) {
                android.support.v4.media.session.MediaSessionCompat.Callback.this.onPrepareFromSearch(query, extras);
            }

            public void onPrepareFromUri(android.net.Uri uri, android.os.Bundle extras) {
                android.support.v4.media.session.MediaSessionCompat.Callback.this.onPrepareFromUri(uri, extras);
            }
        }

        public Callback() {
            if (android.os.Build.VERSION.SDK_INT >= 24) {
                this.mCallbackObj = android.support.v4.media.session.MediaSessionCompatApi24.createCallback(new android.support.v4.media.session.MediaSessionCompat.Callback.StubApi24());
            } else if (android.os.Build.VERSION.SDK_INT >= 23) {
                this.mCallbackObj = android.support.v4.media.session.MediaSessionCompatApi23.createCallback(new android.support.v4.media.session.MediaSessionCompat.Callback.StubApi23());
            } else if (android.os.Build.VERSION.SDK_INT >= 21) {
                this.mCallbackObj = android.support.v4.media.session.MediaSessionCompatApi21.createCallback(new android.support.v4.media.session.MediaSessionCompat.Callback.StubApi21());
            } else {
                this.mCallbackObj = null;
            }
        }

        public void onCommand(java.lang.String command, android.os.Bundle extras, android.os.ResultReceiver cb) {
        }

        public boolean onMediaButtonEvent(android.content.Intent mediaButtonEvent) {
            return false;
        }

        public void onPrepare() {
        }

        public void onPrepareFromMediaId(java.lang.String mediaId, android.os.Bundle extras) {
        }

        public void onPrepareFromSearch(java.lang.String query, android.os.Bundle extras) {
        }

        public void onPrepareFromUri(android.net.Uri uri, android.os.Bundle extras) {
        }

        public void onPlay() {
        }

        public void onPlayFromMediaId(java.lang.String mediaId, android.os.Bundle extras) {
        }

        public void onPlayFromSearch(java.lang.String query, android.os.Bundle extras) {
        }

        public void onPlayFromUri(android.net.Uri uri, android.os.Bundle extras) {
        }

        public void onSkipToQueueItem(long id) {
        }

        public void onPause() {
        }

        public void onSkipToNext() {
        }

        public void onSkipToPrevious() {
        }

        public void onFastForward() {
        }

        public void onRewind() {
        }

        public void onStop() {
        }

        public void onSeekTo(long pos) {
        }

        public void onSetRating(android.support.v4.media.RatingCompat rating) {
        }

        public void onSetRepeatMode(int repeatMode) {
        }

        public void onSetShuffleModeEnabled(boolean enabled) {
        }

        public void onCustomAction(java.lang.String action, android.os.Bundle extras) {
        }

        public void onAddQueueItem(android.support.v4.media.MediaDescriptionCompat description) {
        }

        public void onAddQueueItem(android.support.v4.media.MediaDescriptionCompat description, int index) {
        }

        public void onRemoveQueueItem(android.support.v4.media.MediaDescriptionCompat description) {
        }

        public void onRemoveQueueItemAt(int index) {
        }
    }

    interface MediaSessionImpl {
        java.lang.String getCallingPackage();

        java.lang.Object getMediaSession();

        java.lang.Object getRemoteControlClient();

        android.support.v4.media.session.MediaSessionCompat.Token getSessionToken();

        boolean isActive();

        void release();

        void sendSessionEvent(java.lang.String str, android.os.Bundle bundle);

        void setActive(boolean z);

        void setCallback(android.support.v4.media.session.MediaSessionCompat.Callback callback, android.os.Handler handler);

        void setExtras(android.os.Bundle bundle);

        void setFlags(int i);

        void setMediaButtonReceiver(android.app.PendingIntent pendingIntent);

        void setMetadata(android.support.v4.media.MediaMetadataCompat mediaMetadataCompat);

        void setPlaybackState(android.support.v4.media.session.PlaybackStateCompat playbackStateCompat);

        void setPlaybackToLocal(int i);

        void setPlaybackToRemote(android.support.v4.media.VolumeProviderCompat volumeProviderCompat);

        void setQueue(java.util.List<android.support.v4.media.session.MediaSessionCompat.QueueItem> list);

        void setQueueTitle(java.lang.CharSequence charSequence);

        void setRatingType(int i);

        void setRepeatMode(int i);

        void setSessionActivity(android.app.PendingIntent pendingIntent);

        void setShuffleModeEnabled(boolean z);
    }

    static class MediaSessionImplApi21 implements android.support.v4.media.session.MediaSessionCompat.MediaSessionImpl {
        /* access modifiers changed from: private */
        public boolean mDestroyed = false;
        /* access modifiers changed from: private */
        public final android.os.RemoteCallbackList<android.support.v4.media.session.IMediaControllerCallback> mExtraControllerCallbacks = new android.os.RemoteCallbackList<>();
        private android.support.v4.media.session.MediaSessionCompat.MediaSessionImplApi21.ExtraSession mExtraSessionBinder;
        /* access modifiers changed from: private */
        public android.support.v4.media.session.PlaybackStateCompat mPlaybackState;
        int mRatingType;
        int mRepeatMode;
        private final java.lang.Object mSessionObj;
        boolean mShuffleModeEnabled;
        private final android.support.v4.media.session.MediaSessionCompat.Token mToken;

        class ExtraSession extends android.support.v4.media.session.IMediaSession.Stub {
            ExtraSession() {
            }

            public void sendCommand(java.lang.String command, android.os.Bundle args, android.support.v4.media.session.MediaSessionCompat.ResultReceiverWrapper cb) {
                throw new java.lang.AssertionError();
            }

            public boolean sendMediaButton(android.view.KeyEvent mediaButton) {
                throw new java.lang.AssertionError();
            }

            public void registerCallbackListener(android.support.v4.media.session.IMediaControllerCallback cb) {
                if (!android.support.v4.media.session.MediaSessionCompat.MediaSessionImplApi21.this.mDestroyed) {
                    android.support.v4.media.session.MediaSessionCompat.MediaSessionImplApi21.this.mExtraControllerCallbacks.register(cb);
                }
            }

            public void unregisterCallbackListener(android.support.v4.media.session.IMediaControllerCallback cb) {
                android.support.v4.media.session.MediaSessionCompat.MediaSessionImplApi21.this.mExtraControllerCallbacks.unregister(cb);
            }

            public java.lang.String getPackageName() {
                throw new java.lang.AssertionError();
            }

            public java.lang.String getTag() {
                throw new java.lang.AssertionError();
            }

            public android.app.PendingIntent getLaunchPendingIntent() {
                throw new java.lang.AssertionError();
            }

            public long getFlags() {
                throw new java.lang.AssertionError();
            }

            public android.support.v4.media.session.ParcelableVolumeInfo getVolumeAttributes() {
                throw new java.lang.AssertionError();
            }

            public void adjustVolume(int direction, int flags, java.lang.String packageName) {
                throw new java.lang.AssertionError();
            }

            public void setVolumeTo(int value, int flags, java.lang.String packageName) {
                throw new java.lang.AssertionError();
            }

            public void prepare() throws android.os.RemoteException {
                throw new java.lang.AssertionError();
            }

            public void prepareFromMediaId(java.lang.String mediaId, android.os.Bundle extras) throws android.os.RemoteException {
                throw new java.lang.AssertionError();
            }

            public void prepareFromSearch(java.lang.String query, android.os.Bundle extras) throws android.os.RemoteException {
                throw new java.lang.AssertionError();
            }

            public void prepareFromUri(android.net.Uri uri, android.os.Bundle extras) throws android.os.RemoteException {
                throw new java.lang.AssertionError();
            }

            public void play() throws android.os.RemoteException {
                throw new java.lang.AssertionError();
            }

            public void playFromMediaId(java.lang.String mediaId, android.os.Bundle extras) throws android.os.RemoteException {
                throw new java.lang.AssertionError();
            }

            public void playFromSearch(java.lang.String query, android.os.Bundle extras) throws android.os.RemoteException {
                throw new java.lang.AssertionError();
            }

            public void playFromUri(android.net.Uri uri, android.os.Bundle extras) throws android.os.RemoteException {
                throw new java.lang.AssertionError();
            }

            public void skipToQueueItem(long id) {
                throw new java.lang.AssertionError();
            }

            public void pause() throws android.os.RemoteException {
                throw new java.lang.AssertionError();
            }

            public void stop() throws android.os.RemoteException {
                throw new java.lang.AssertionError();
            }

            public void next() throws android.os.RemoteException {
                throw new java.lang.AssertionError();
            }

            public void previous() throws android.os.RemoteException {
                throw new java.lang.AssertionError();
            }

            public void fastForward() throws android.os.RemoteException {
                throw new java.lang.AssertionError();
            }

            public void rewind() throws android.os.RemoteException {
                throw new java.lang.AssertionError();
            }

            public void seekTo(long pos) throws android.os.RemoteException {
                throw new java.lang.AssertionError();
            }

            public void rate(android.support.v4.media.RatingCompat rating) throws android.os.RemoteException {
                throw new java.lang.AssertionError();
            }

            public void setRepeatMode(int repeatMode) throws android.os.RemoteException {
                throw new java.lang.AssertionError();
            }

            public void setShuffleModeEnabled(boolean enabled) throws android.os.RemoteException {
                throw new java.lang.AssertionError();
            }

            public void sendCustomAction(java.lang.String action, android.os.Bundle args) throws android.os.RemoteException {
                throw new java.lang.AssertionError();
            }

            public android.support.v4.media.MediaMetadataCompat getMetadata() {
                throw new java.lang.AssertionError();
            }

            public android.support.v4.media.session.PlaybackStateCompat getPlaybackState() {
                return android.support.v4.media.session.MediaSessionCompat.MediaSessionImplApi21.this.mPlaybackState;
            }

            public java.util.List<android.support.v4.media.session.MediaSessionCompat.QueueItem> getQueue() {
                return null;
            }

            public void addQueueItem(android.support.v4.media.MediaDescriptionCompat descriptionCompat) {
                throw new java.lang.AssertionError();
            }

            public void addQueueItemAt(android.support.v4.media.MediaDescriptionCompat descriptionCompat, int index) {
                throw new java.lang.AssertionError();
            }

            public void removeQueueItem(android.support.v4.media.MediaDescriptionCompat description) {
                throw new java.lang.AssertionError();
            }

            public void removeQueueItemAt(int index) {
                throw new java.lang.AssertionError();
            }

            public java.lang.CharSequence getQueueTitle() {
                throw new java.lang.AssertionError();
            }

            public android.os.Bundle getExtras() {
                throw new java.lang.AssertionError();
            }

            public int getRatingType() {
                return android.support.v4.media.session.MediaSessionCompat.MediaSessionImplApi21.this.mRatingType;
            }

            public int getRepeatMode() {
                return android.support.v4.media.session.MediaSessionCompat.MediaSessionImplApi21.this.mRepeatMode;
            }

            public boolean isShuffleModeEnabled() {
                return android.support.v4.media.session.MediaSessionCompat.MediaSessionImplApi21.this.mShuffleModeEnabled;
            }

            public boolean isTransportControlEnabled() {
                throw new java.lang.AssertionError();
            }
        }

        public MediaSessionImplApi21(android.content.Context context, java.lang.String tag) {
            this.mSessionObj = android.support.v4.media.session.MediaSessionCompatApi21.createSession(context, tag);
            this.mToken = new android.support.v4.media.session.MediaSessionCompat.Token(android.support.v4.media.session.MediaSessionCompatApi21.getSessionToken(this.mSessionObj));
        }

        public MediaSessionImplApi21(java.lang.Object mediaSession) {
            this.mSessionObj = android.support.v4.media.session.MediaSessionCompatApi21.verifySession(mediaSession);
            this.mToken = new android.support.v4.media.session.MediaSessionCompat.Token(android.support.v4.media.session.MediaSessionCompatApi21.getSessionToken(this.mSessionObj));
        }

        public void setCallback(android.support.v4.media.session.MediaSessionCompat.Callback callback, android.os.Handler handler) {
            android.support.v4.media.session.MediaSessionCompatApi21.setCallback(this.mSessionObj, callback == null ? null : callback.mCallbackObj, handler);
            if (callback != null) {
                callback.mSessionImpl = new java.lang.ref.WeakReference<>(this);
            }
        }

        public void setFlags(int flags) {
            android.support.v4.media.session.MediaSessionCompatApi21.setFlags(this.mSessionObj, flags);
        }

        public void setPlaybackToLocal(int stream) {
            android.support.v4.media.session.MediaSessionCompatApi21.setPlaybackToLocal(this.mSessionObj, stream);
        }

        public void setPlaybackToRemote(android.support.v4.media.VolumeProviderCompat volumeProvider) {
            android.support.v4.media.session.MediaSessionCompatApi21.setPlaybackToRemote(this.mSessionObj, volumeProvider.getVolumeProvider());
        }

        public void setActive(boolean active) {
            android.support.v4.media.session.MediaSessionCompatApi21.setActive(this.mSessionObj, active);
        }

        public boolean isActive() {
            return android.support.v4.media.session.MediaSessionCompatApi21.isActive(this.mSessionObj);
        }

        public void sendSessionEvent(java.lang.String event, android.os.Bundle extras) {
            if (android.os.Build.VERSION.SDK_INT < 23) {
                for (int i = this.mExtraControllerCallbacks.beginBroadcast() - 1; i >= 0; i--) {
                    try {
                        ((android.support.v4.media.session.IMediaControllerCallback) this.mExtraControllerCallbacks.getBroadcastItem(i)).onEvent(event, extras);
                    } catch (android.os.RemoteException e) {
                    }
                }
                this.mExtraControllerCallbacks.finishBroadcast();
            }
            android.support.v4.media.session.MediaSessionCompatApi21.sendSessionEvent(this.mSessionObj, event, extras);
        }

        public void release() {
            this.mDestroyed = true;
            android.support.v4.media.session.MediaSessionCompatApi21.release(this.mSessionObj);
        }

        public android.support.v4.media.session.MediaSessionCompat.Token getSessionToken() {
            return this.mToken;
        }

        public void setPlaybackState(android.support.v4.media.session.PlaybackStateCompat state) {
            java.lang.Object playbackState;
            this.mPlaybackState = state;
            for (int i = this.mExtraControllerCallbacks.beginBroadcast() - 1; i >= 0; i--) {
                try {
                    ((android.support.v4.media.session.IMediaControllerCallback) this.mExtraControllerCallbacks.getBroadcastItem(i)).onPlaybackStateChanged(state);
                } catch (android.os.RemoteException e) {
                }
            }
            this.mExtraControllerCallbacks.finishBroadcast();
            java.lang.Object obj = this.mSessionObj;
            if (state == null) {
                playbackState = null;
            } else {
                playbackState = state.getPlaybackState();
            }
            android.support.v4.media.session.MediaSessionCompatApi21.setPlaybackState(obj, playbackState);
        }

        public void setMetadata(android.support.v4.media.MediaMetadataCompat metadata) {
            java.lang.Object mediaMetadata;
            java.lang.Object obj = this.mSessionObj;
            if (metadata == null) {
                mediaMetadata = null;
            } else {
                mediaMetadata = metadata.getMediaMetadata();
            }
            android.support.v4.media.session.MediaSessionCompatApi21.setMetadata(obj, mediaMetadata);
        }

        public void setSessionActivity(android.app.PendingIntent pi) {
            android.support.v4.media.session.MediaSessionCompatApi21.setSessionActivity(this.mSessionObj, pi);
        }

        public void setMediaButtonReceiver(android.app.PendingIntent mbr) {
            android.support.v4.media.session.MediaSessionCompatApi21.setMediaButtonReceiver(this.mSessionObj, mbr);
        }

        public void setQueue(java.util.List<android.support.v4.media.session.MediaSessionCompat.QueueItem> queue) {
            java.util.List<java.lang.Object> queueObjs = null;
            if (queue != null) {
                queueObjs = new java.util.ArrayList<>();
                for (android.support.v4.media.session.MediaSessionCompat.QueueItem item : queue) {
                    queueObjs.add(item.getQueueItem());
                }
            }
            android.support.v4.media.session.MediaSessionCompatApi21.setQueue(this.mSessionObj, queueObjs);
        }

        public void setQueueTitle(java.lang.CharSequence title) {
            android.support.v4.media.session.MediaSessionCompatApi21.setQueueTitle(this.mSessionObj, title);
        }

        public void setRatingType(int type) {
            if (android.os.Build.VERSION.SDK_INT < 22) {
                this.mRatingType = type;
            } else {
                android.support.v4.media.session.MediaSessionCompatApi22.setRatingType(this.mSessionObj, type);
            }
        }

        public void setRepeatMode(int repeatMode) {
            if (this.mRepeatMode != repeatMode) {
                this.mRepeatMode = repeatMode;
                for (int i = this.mExtraControllerCallbacks.beginBroadcast() - 1; i >= 0; i--) {
                    try {
                        ((android.support.v4.media.session.IMediaControllerCallback) this.mExtraControllerCallbacks.getBroadcastItem(i)).onRepeatModeChanged(repeatMode);
                    } catch (android.os.RemoteException e) {
                    }
                }
                this.mExtraControllerCallbacks.finishBroadcast();
            }
        }

        public void setShuffleModeEnabled(boolean enabled) {
            if (this.mShuffleModeEnabled != enabled) {
                this.mShuffleModeEnabled = enabled;
                for (int i = this.mExtraControllerCallbacks.beginBroadcast() - 1; i >= 0; i--) {
                    try {
                        ((android.support.v4.media.session.IMediaControllerCallback) this.mExtraControllerCallbacks.getBroadcastItem(i)).onShuffleModeChanged(enabled);
                    } catch (android.os.RemoteException e) {
                    }
                }
                this.mExtraControllerCallbacks.finishBroadcast();
            }
        }

        public void setExtras(android.os.Bundle extras) {
            android.support.v4.media.session.MediaSessionCompatApi21.setExtras(this.mSessionObj, extras);
        }

        public java.lang.Object getMediaSession() {
            return this.mSessionObj;
        }

        public java.lang.Object getRemoteControlClient() {
            return null;
        }

        public java.lang.String getCallingPackage() {
            if (android.os.Build.VERSION.SDK_INT < 24) {
                return null;
            }
            return android.support.v4.media.session.MediaSessionCompatApi24.getCallingPackage(this.mSessionObj);
        }

        /* access modifiers changed from: 0000 */
        public android.support.v4.media.session.MediaSessionCompat.MediaSessionImplApi21.ExtraSession getExtraSessionBinder() {
            if (this.mExtraSessionBinder == null) {
                this.mExtraSessionBinder = new android.support.v4.media.session.MediaSessionCompat.MediaSessionImplApi21.ExtraSession();
            }
            return this.mExtraSessionBinder;
        }
    }

    static class MediaSessionImplBase implements android.support.v4.media.session.MediaSessionCompat.MediaSessionImpl {
        final android.media.AudioManager mAudioManager;
        volatile android.support.v4.media.session.MediaSessionCompat.Callback mCallback;
        private final android.content.Context mContext;
        final android.os.RemoteCallbackList<android.support.v4.media.session.IMediaControllerCallback> mControllerCallbacks = new android.os.RemoteCallbackList<>();
        boolean mDestroyed = false;
        android.os.Bundle mExtras;
        int mFlags;
        private android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.MessageHandler mHandler;
        private boolean mIsActive = false;
        private boolean mIsMbrRegistered = false;
        private boolean mIsRccRegistered = false;
        int mLocalStream;
        final java.lang.Object mLock = new java.lang.Object();
        private final android.content.ComponentName mMediaButtonReceiverComponentName;
        private final android.app.PendingIntent mMediaButtonReceiverIntent;
        android.support.v4.media.MediaMetadataCompat mMetadata;
        final java.lang.String mPackageName;
        java.util.List<android.support.v4.media.session.MediaSessionCompat.QueueItem> mQueue;
        java.lang.CharSequence mQueueTitle;
        int mRatingType;
        private final java.lang.Object mRccObj;
        int mRepeatMode;
        android.app.PendingIntent mSessionActivity;
        boolean mShuffleModeEnabled;
        android.support.v4.media.session.PlaybackStateCompat mState;
        private final android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.MediaSessionStub mStub;
        final java.lang.String mTag;
        private final android.support.v4.media.session.MediaSessionCompat.Token mToken;
        private android.support.v4.media.VolumeProviderCompat.Callback mVolumeCallback = new android.support.v4.media.VolumeProviderCompat.Callback() {
            public void onVolumeChanged(android.support.v4.media.VolumeProviderCompat volumeProvider) {
                if (android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.mVolumeProvider == volumeProvider) {
                    android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.sendVolumeInfoChanged(new android.support.v4.media.session.ParcelableVolumeInfo(android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.mVolumeType, android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.mLocalStream, volumeProvider.getVolumeControl(), volumeProvider.getMaxVolume(), volumeProvider.getCurrentVolume()));
                }
            }
        };
        android.support.v4.media.VolumeProviderCompat mVolumeProvider;
        int mVolumeType;

        private static final class Command {
            public final java.lang.String command;
            public final android.os.Bundle extras;
            public final android.os.ResultReceiver stub;

            public Command(java.lang.String command2, android.os.Bundle extras2, android.os.ResultReceiver stub2) {
                this.command = command2;
                this.extras = extras2;
                this.stub = stub2;
            }
        }

        class MediaSessionStub extends android.support.v4.media.session.IMediaSession.Stub {
            MediaSessionStub() {
            }

            public void sendCommand(java.lang.String command, android.os.Bundle args, android.support.v4.media.session.MediaSessionCompat.ResultReceiverWrapper cb) {
                android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.postToHandler(1, (java.lang.Object) new android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.Command(command, args, cb.mResultReceiver));
            }

            public boolean sendMediaButton(android.view.KeyEvent mediaButton) {
                boolean handlesMediaButtons = (android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.mFlags & 1) != 0;
                if (handlesMediaButtons) {
                    android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.postToHandler(21, (java.lang.Object) mediaButton);
                }
                return handlesMediaButtons;
            }

            public void registerCallbackListener(android.support.v4.media.session.IMediaControllerCallback cb) {
                if (android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.mDestroyed) {
                    try {
                        cb.onSessionDestroyed();
                    } catch (java.lang.Exception e) {
                    }
                } else {
                    android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.mControllerCallbacks.register(cb);
                }
            }

            public void unregisterCallbackListener(android.support.v4.media.session.IMediaControllerCallback cb) {
                android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.mControllerCallbacks.unregister(cb);
            }

            public java.lang.String getPackageName() {
                return android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.mPackageName;
            }

            public java.lang.String getTag() {
                return android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.mTag;
            }

            public android.app.PendingIntent getLaunchPendingIntent() {
                android.app.PendingIntent pendingIntent;
                synchronized (android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.mLock) {
                    pendingIntent = android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.mSessionActivity;
                }
                return pendingIntent;
            }

            public long getFlags() {
                long j;
                synchronized (android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.mLock) {
                    j = (long) android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.mFlags;
                }
                return j;
            }

            public android.support.v4.media.session.ParcelableVolumeInfo getVolumeAttributes() {
                int volumeType;
                int stream;
                int controlType;
                int max;
                int current;
                synchronized (android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.mLock) {
                    volumeType = android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.mVolumeType;
                    stream = android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.mLocalStream;
                    android.support.v4.media.VolumeProviderCompat vp = android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.mVolumeProvider;
                    if (volumeType == 2) {
                        controlType = vp.getVolumeControl();
                        max = vp.getMaxVolume();
                        current = vp.getCurrentVolume();
                    } else {
                        controlType = 2;
                        max = android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.mAudioManager.getStreamMaxVolume(stream);
                        current = android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.mAudioManager.getStreamVolume(stream);
                    }
                }
                return new android.support.v4.media.session.ParcelableVolumeInfo(volumeType, stream, controlType, max, current);
            }

            public void adjustVolume(int direction, int flags, java.lang.String packageName) {
                android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.adjustVolume(direction, flags);
            }

            public void setVolumeTo(int value, int flags, java.lang.String packageName) {
                android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.setVolumeTo(value, flags);
            }

            public void prepare() throws android.os.RemoteException {
                android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.postToHandler(3);
            }

            public void prepareFromMediaId(java.lang.String mediaId, android.os.Bundle extras) throws android.os.RemoteException {
                android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.postToHandler(4, (java.lang.Object) mediaId, extras);
            }

            public void prepareFromSearch(java.lang.String query, android.os.Bundle extras) throws android.os.RemoteException {
                android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.postToHandler(5, (java.lang.Object) query, extras);
            }

            public void prepareFromUri(android.net.Uri uri, android.os.Bundle extras) throws android.os.RemoteException {
                android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.postToHandler(6, (java.lang.Object) uri, extras);
            }

            public void play() throws android.os.RemoteException {
                android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.postToHandler(7);
            }

            public void playFromMediaId(java.lang.String mediaId, android.os.Bundle extras) throws android.os.RemoteException {
                android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.postToHandler(8, (java.lang.Object) mediaId, extras);
            }

            public void playFromSearch(java.lang.String query, android.os.Bundle extras) throws android.os.RemoteException {
                android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.postToHandler(9, (java.lang.Object) query, extras);
            }

            public void playFromUri(android.net.Uri uri, android.os.Bundle extras) throws android.os.RemoteException {
                android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.postToHandler(10, (java.lang.Object) uri, extras);
            }

            public void skipToQueueItem(long id) {
                android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.postToHandler(11, (java.lang.Object) java.lang.Long.valueOf(id));
            }

            public void pause() throws android.os.RemoteException {
                android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.postToHandler(12);
            }

            public void stop() throws android.os.RemoteException {
                android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.postToHandler(13);
            }

            public void next() throws android.os.RemoteException {
                android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.postToHandler(14);
            }

            public void previous() throws android.os.RemoteException {
                android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.postToHandler(15);
            }

            public void fastForward() throws android.os.RemoteException {
                android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.postToHandler(16);
            }

            public void rewind() throws android.os.RemoteException {
                android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.postToHandler(17);
            }

            public void seekTo(long pos) throws android.os.RemoteException {
                android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.postToHandler(18, (java.lang.Object) java.lang.Long.valueOf(pos));
            }

            public void rate(android.support.v4.media.RatingCompat rating) throws android.os.RemoteException {
                android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.postToHandler(19, (java.lang.Object) rating);
            }

            public void setRepeatMode(int repeatMode) throws android.os.RemoteException {
                android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.postToHandler(23, repeatMode);
            }

            public void setShuffleModeEnabled(boolean enabled) throws android.os.RemoteException {
                android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.postToHandler(24, (java.lang.Object) java.lang.Boolean.valueOf(enabled));
            }

            public void sendCustomAction(java.lang.String action, android.os.Bundle args) throws android.os.RemoteException {
                android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.postToHandler(20, (java.lang.Object) action, args);
            }

            public android.support.v4.media.MediaMetadataCompat getMetadata() {
                return android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.mMetadata;
            }

            public android.support.v4.media.session.PlaybackStateCompat getPlaybackState() {
                return android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.getStateWithUpdatedPosition();
            }

            public java.util.List<android.support.v4.media.session.MediaSessionCompat.QueueItem> getQueue() {
                java.util.List<android.support.v4.media.session.MediaSessionCompat.QueueItem> list;
                synchronized (android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.mLock) {
                    list = android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.mQueue;
                }
                return list;
            }

            public void addQueueItem(android.support.v4.media.MediaDescriptionCompat description) {
                android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.postToHandler(25, (java.lang.Object) description);
            }

            public void addQueueItemAt(android.support.v4.media.MediaDescriptionCompat description, int index) {
                android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.postToHandler(26, (java.lang.Object) description, index);
            }

            public void removeQueueItem(android.support.v4.media.MediaDescriptionCompat description) {
                android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.postToHandler(27, (java.lang.Object) description);
            }

            public void removeQueueItemAt(int index) {
                android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.postToHandler(28, index);
            }

            public java.lang.CharSequence getQueueTitle() {
                return android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.mQueueTitle;
            }

            public android.os.Bundle getExtras() {
                android.os.Bundle bundle;
                synchronized (android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.mLock) {
                    bundle = android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.mExtras;
                }
                return bundle;
            }

            public int getRatingType() {
                return android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.mRatingType;
            }

            public int getRepeatMode() {
                return android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.mRepeatMode;
            }

            public boolean isShuffleModeEnabled() {
                return android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.mShuffleModeEnabled;
            }

            public boolean isTransportControlEnabled() {
                return (android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.mFlags & 2) != 0;
            }
        }

        private class MessageHandler extends android.os.Handler {
            private static final int KEYCODE_MEDIA_PAUSE = 127;
            private static final int KEYCODE_MEDIA_PLAY = 126;
            private static final int MSG_ADD_QUEUE_ITEM = 25;
            private static final int MSG_ADD_QUEUE_ITEM_AT = 26;
            private static final int MSG_ADJUST_VOLUME = 2;
            private static final int MSG_COMMAND = 1;
            private static final int MSG_CUSTOM_ACTION = 20;
            private static final int MSG_FAST_FORWARD = 16;
            private static final int MSG_MEDIA_BUTTON = 21;
            private static final int MSG_NEXT = 14;
            private static final int MSG_PAUSE = 12;
            private static final int MSG_PLAY = 7;
            private static final int MSG_PLAY_MEDIA_ID = 8;
            private static final int MSG_PLAY_SEARCH = 9;
            private static final int MSG_PLAY_URI = 10;
            private static final int MSG_PREPARE = 3;
            private static final int MSG_PREPARE_MEDIA_ID = 4;
            private static final int MSG_PREPARE_SEARCH = 5;
            private static final int MSG_PREPARE_URI = 6;
            private static final int MSG_PREVIOUS = 15;
            private static final int MSG_RATE = 19;
            private static final int MSG_REMOVE_QUEUE_ITEM = 27;
            private static final int MSG_REMOVE_QUEUE_ITEM_AT = 28;
            private static final int MSG_REWIND = 17;
            private static final int MSG_SEEK_TO = 18;
            private static final int MSG_SET_REPEAT_MODE = 23;
            private static final int MSG_SET_SHUFFLE_MODE_ENABLED = 24;
            private static final int MSG_SET_VOLUME = 22;
            private static final int MSG_SKIP_TO_ITEM = 11;
            private static final int MSG_STOP = 13;

            public MessageHandler(android.os.Looper looper) {
                super(looper);
            }

            public void post(int what, java.lang.Object obj, android.os.Bundle bundle) {
                android.os.Message msg = obtainMessage(what, obj);
                msg.setData(bundle);
                msg.sendToTarget();
            }

            public void post(int what, java.lang.Object obj) {
                obtainMessage(what, obj).sendToTarget();
            }

            public void post(int what) {
                post(what, null);
            }

            public void post(int what, java.lang.Object obj, int arg1) {
                obtainMessage(what, arg1, 0, obj).sendToTarget();
            }

            public void handleMessage(android.os.Message msg) {
                android.support.v4.media.session.MediaSessionCompat.Callback cb = android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.mCallback;
                if (cb != null) {
                    switch (msg.what) {
                        case 1:
                            android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.Command cmd = (android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.Command) msg.obj;
                            cb.onCommand(cmd.command, cmd.extras, cmd.stub);
                            return;
                        case 2:
                            android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.adjustVolume(msg.arg1, 0);
                            return;
                        case 3:
                            cb.onPrepare();
                            return;
                        case 4:
                            cb.onPrepareFromMediaId((java.lang.String) msg.obj, msg.getData());
                            return;
                        case 5:
                            cb.onPrepareFromSearch((java.lang.String) msg.obj, msg.getData());
                            return;
                        case 6:
                            cb.onPrepareFromUri((android.net.Uri) msg.obj, msg.getData());
                            return;
                        case 7:
                            cb.onPlay();
                            return;
                        case 8:
                            cb.onPlayFromMediaId((java.lang.String) msg.obj, msg.getData());
                            return;
                        case 9:
                            cb.onPlayFromSearch((java.lang.String) msg.obj, msg.getData());
                            return;
                        case 10:
                            cb.onPlayFromUri((android.net.Uri) msg.obj, msg.getData());
                            return;
                        case 11:
                            cb.onSkipToQueueItem(((java.lang.Long) msg.obj).longValue());
                            return;
                        case 12:
                            cb.onPause();
                            return;
                        case 13:
                            cb.onStop();
                            return;
                        case 14:
                            cb.onSkipToNext();
                            return;
                        case 15:
                            cb.onSkipToPrevious();
                            return;
                        case 16:
                            cb.onFastForward();
                            return;
                        case 17:
                            cb.onRewind();
                            return;
                        case 18:
                            cb.onSeekTo(((java.lang.Long) msg.obj).longValue());
                            return;
                        case 19:
                            cb.onSetRating((android.support.v4.media.RatingCompat) msg.obj);
                            return;
                        case 20:
                            cb.onCustomAction((java.lang.String) msg.obj, msg.getData());
                            return;
                        case 21:
                            android.view.KeyEvent keyEvent = (android.view.KeyEvent) msg.obj;
                            android.content.Intent intent = new android.content.Intent("android.intent.action.MEDIA_BUTTON");
                            intent.putExtra("android.intent.extra.KEY_EVENT", keyEvent);
                            if (!cb.onMediaButtonEvent(intent)) {
                                onMediaButtonEvent(keyEvent, cb);
                                return;
                            }
                            return;
                        case 22:
                            android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.setVolumeTo(msg.arg1, 0);
                            return;
                        case 23:
                            cb.onSetRepeatMode(msg.arg1);
                            return;
                        case 24:
                            cb.onSetShuffleModeEnabled(((java.lang.Boolean) msg.obj).booleanValue());
                            return;
                        case 25:
                            cb.onAddQueueItem((android.support.v4.media.MediaDescriptionCompat) msg.obj);
                            return;
                        case MSG_ADD_QUEUE_ITEM_AT /*26*/:
                            cb.onAddQueueItem((android.support.v4.media.MediaDescriptionCompat) msg.obj, msg.arg1);
                            return;
                        case 27:
                            cb.onRemoveQueueItem((android.support.v4.media.MediaDescriptionCompat) msg.obj);
                            return;
                        case 28:
                            cb.onRemoveQueueItemAt(msg.arg1);
                            return;
                        default:
                            return;
                    }
                }
            }

            private void onMediaButtonEvent(android.view.KeyEvent ke, android.support.v4.media.session.MediaSessionCompat.Callback cb) {
                boolean isPlaying;
                boolean canPlay;
                boolean canPause = true;
                if (ke != null && ke.getAction() == 0) {
                    long validActions = android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.mState == null ? 0 : android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.mState.getActions();
                    switch (ke.getKeyCode()) {
                        case 79:
                        case 85:
                            if (android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.mState == null || android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.mState.getState() != 3) {
                                isPlaying = false;
                            } else {
                                isPlaying = true;
                            }
                            if ((516 & validActions) != 0) {
                                canPlay = true;
                            } else {
                                canPlay = false;
                            }
                            if ((514 & validActions) == 0) {
                                canPause = false;
                            }
                            if (isPlaying && canPause) {
                                cb.onPause();
                                return;
                            } else if (!isPlaying && canPlay) {
                                cb.onPlay();
                                return;
                            } else {
                                return;
                            }
                        case 86:
                            if ((1 & validActions) != 0) {
                                cb.onStop();
                                return;
                            }
                            return;
                        case 87:
                            if ((32 & validActions) != 0) {
                                cb.onSkipToNext();
                                return;
                            }
                            return;
                        case ch.qos.logback.core.net.SyslogConstants.LOG_FTP /*88*/:
                            if ((16 & validActions) != 0) {
                                cb.onSkipToPrevious();
                                return;
                            }
                            return;
                        case 89:
                            if ((8 & validActions) != 0) {
                                cb.onRewind();
                                return;
                            }
                            return;
                        case 90:
                            if ((64 & validActions) != 0) {
                                cb.onFastForward();
                                return;
                            }
                            return;
                        case 126:
                            if ((4 & validActions) != 0) {
                                cb.onPlay();
                                return;
                            }
                            return;
                        case 127:
                            if ((2 & validActions) != 0) {
                                cb.onPause();
                                return;
                            }
                            return;
                        default:
                            return;
                    }
                }
            }
        }

        public MediaSessionImplBase(android.content.Context context, java.lang.String tag, android.content.ComponentName mbrComponent, android.app.PendingIntent mbrIntent) {
            if (mbrComponent == null) {
                throw new java.lang.IllegalArgumentException("MediaButtonReceiver component may not be null.");
            }
            this.mContext = context;
            this.mPackageName = context.getPackageName();
            this.mAudioManager = (android.media.AudioManager) context.getSystemService("audio");
            this.mTag = tag;
            this.mMediaButtonReceiverComponentName = mbrComponent;
            this.mMediaButtonReceiverIntent = mbrIntent;
            this.mStub = new android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.MediaSessionStub();
            this.mToken = new android.support.v4.media.session.MediaSessionCompat.Token(this.mStub);
            this.mRatingType = 0;
            this.mVolumeType = 1;
            this.mLocalStream = 3;
            if (android.os.Build.VERSION.SDK_INT >= 14) {
                this.mRccObj = android.support.v4.media.session.MediaSessionCompatApi14.createRemoteControlClient(mbrIntent);
            } else {
                this.mRccObj = null;
            }
        }

        public void setCallback(android.support.v4.media.session.MediaSessionCompat.Callback callback, android.os.Handler handler) {
            this.mCallback = callback;
            if (callback == null) {
                if (android.os.Build.VERSION.SDK_INT >= 18) {
                    android.support.v4.media.session.MediaSessionCompatApi18.setOnPlaybackPositionUpdateListener(this.mRccObj, null);
                }
                if (android.os.Build.VERSION.SDK_INT >= 19) {
                    android.support.v4.media.session.MediaSessionCompatApi19.setOnMetadataUpdateListener(this.mRccObj, null);
                    return;
                }
                return;
            }
            if (handler == null) {
                handler = new android.os.Handler();
            }
            synchronized (this.mLock) {
                this.mHandler = new android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.MessageHandler(handler.getLooper());
            }
            android.support.v4.media.session.MediaSessionCompatApi19.Callback cb19 = new android.support.v4.media.session.MediaSessionCompatApi19.Callback() {
                public void onSetRating(java.lang.Object ratingObj) {
                    android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.postToHandler(19, (java.lang.Object) android.support.v4.media.RatingCompat.fromRating(ratingObj));
                }

                public void onSeekTo(long pos) {
                    android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase.this.postToHandler(18, (java.lang.Object) java.lang.Long.valueOf(pos));
                }
            };
            if (android.os.Build.VERSION.SDK_INT >= 18) {
                android.support.v4.media.session.MediaSessionCompatApi18.setOnPlaybackPositionUpdateListener(this.mRccObj, android.support.v4.media.session.MediaSessionCompatApi18.createPlaybackPositionUpdateListener(cb19));
            }
            if (android.os.Build.VERSION.SDK_INT >= 19) {
                android.support.v4.media.session.MediaSessionCompatApi19.setOnMetadataUpdateListener(this.mRccObj, android.support.v4.media.session.MediaSessionCompatApi19.createMetadataUpdateListener(cb19));
            }
        }

        /* access modifiers changed from: 0000 */
        public void postToHandler(int what) {
            postToHandler(what, (java.lang.Object) null);
        }

        /* access modifiers changed from: 0000 */
        public void postToHandler(int what, int arg1) {
            postToHandler(what, (java.lang.Object) null, arg1);
        }

        /* access modifiers changed from: 0000 */
        public void postToHandler(int what, java.lang.Object obj) {
            postToHandler(what, obj, (android.os.Bundle) null);
        }

        /* access modifiers changed from: 0000 */
        public void postToHandler(int what, java.lang.Object obj, int arg1) {
            synchronized (this.mLock) {
                if (this.mHandler != null) {
                    this.mHandler.post(what, obj, arg1);
                }
            }
        }

        /* access modifiers changed from: 0000 */
        public void postToHandler(int what, java.lang.Object obj, android.os.Bundle extras) {
            synchronized (this.mLock) {
                if (this.mHandler != null) {
                    this.mHandler.post(what, obj, extras);
                }
            }
        }

        public void setFlags(int flags) {
            synchronized (this.mLock) {
                this.mFlags = flags;
            }
            update();
        }

        public void setPlaybackToLocal(int stream) {
            if (this.mVolumeProvider != null) {
                this.mVolumeProvider.setCallback(null);
            }
            this.mVolumeType = 1;
            sendVolumeInfoChanged(new android.support.v4.media.session.ParcelableVolumeInfo(this.mVolumeType, this.mLocalStream, 2, this.mAudioManager.getStreamMaxVolume(this.mLocalStream), this.mAudioManager.getStreamVolume(this.mLocalStream)));
        }

        public void setPlaybackToRemote(android.support.v4.media.VolumeProviderCompat volumeProvider) {
            if (volumeProvider == null) {
                throw new java.lang.IllegalArgumentException("volumeProvider may not be null");
            }
            if (this.mVolumeProvider != null) {
                this.mVolumeProvider.setCallback(null);
            }
            this.mVolumeType = 2;
            this.mVolumeProvider = volumeProvider;
            sendVolumeInfoChanged(new android.support.v4.media.session.ParcelableVolumeInfo(this.mVolumeType, this.mLocalStream, this.mVolumeProvider.getVolumeControl(), this.mVolumeProvider.getMaxVolume(), this.mVolumeProvider.getCurrentVolume()));
            volumeProvider.setCallback(this.mVolumeCallback);
        }

        public void setActive(boolean active) {
            if (active != this.mIsActive) {
                this.mIsActive = active;
                if (update()) {
                    setMetadata(this.mMetadata);
                    setPlaybackState(this.mState);
                }
            }
        }

        public boolean isActive() {
            return this.mIsActive;
        }

        public void sendSessionEvent(java.lang.String event, android.os.Bundle extras) {
            sendEvent(event, extras);
        }

        public void release() {
            this.mIsActive = false;
            this.mDestroyed = true;
            update();
            sendSessionDestroyed();
        }

        public android.support.v4.media.session.MediaSessionCompat.Token getSessionToken() {
            return this.mToken;
        }

        public void setPlaybackState(android.support.v4.media.session.PlaybackStateCompat state) {
            synchronized (this.mLock) {
                this.mState = state;
            }
            sendState(state);
            if (this.mIsActive) {
                if (state != null) {
                    if (android.os.Build.VERSION.SDK_INT >= 18) {
                        android.support.v4.media.session.MediaSessionCompatApi18.setState(this.mRccObj, state.getState(), state.getPosition(), state.getPlaybackSpeed(), state.getLastPositionUpdateTime());
                    } else if (android.os.Build.VERSION.SDK_INT >= 14) {
                        android.support.v4.media.session.MediaSessionCompatApi14.setState(this.mRccObj, state.getState());
                    }
                    if (android.os.Build.VERSION.SDK_INT >= 19) {
                        android.support.v4.media.session.MediaSessionCompatApi19.setTransportControlFlags(this.mRccObj, state.getActions());
                    } else if (android.os.Build.VERSION.SDK_INT >= 18) {
                        android.support.v4.media.session.MediaSessionCompatApi18.setTransportControlFlags(this.mRccObj, state.getActions());
                    } else if (android.os.Build.VERSION.SDK_INT >= 14) {
                        android.support.v4.media.session.MediaSessionCompatApi14.setTransportControlFlags(this.mRccObj, state.getActions());
                    }
                } else if (android.os.Build.VERSION.SDK_INT >= 14) {
                    android.support.v4.media.session.MediaSessionCompatApi14.setState(this.mRccObj, 0);
                    android.support.v4.media.session.MediaSessionCompatApi14.setTransportControlFlags(this.mRccObj, 0);
                }
            }
        }

        public void setMetadata(android.support.v4.media.MediaMetadataCompat metadata) {
            long actions;
            android.os.Bundle bundle = null;
            if (metadata != null) {
                metadata = new android.support.v4.media.MediaMetadataCompat.Builder(metadata, android.support.v4.media.session.MediaSessionCompat.sMaxBitmapSize).build();
            }
            synchronized (this.mLock) {
                this.mMetadata = metadata;
            }
            sendMetadata(metadata);
            if (this.mIsActive) {
                if (android.os.Build.VERSION.SDK_INT >= 19) {
                    java.lang.Object obj = this.mRccObj;
                    if (metadata != null) {
                        bundle = metadata.getBundle();
                    }
                    if (this.mState == null) {
                        actions = 0;
                    } else {
                        actions = this.mState.getActions();
                    }
                    android.support.v4.media.session.MediaSessionCompatApi19.setMetadata(obj, bundle, actions);
                } else if (android.os.Build.VERSION.SDK_INT >= 14) {
                    java.lang.Object obj2 = this.mRccObj;
                    if (metadata != null) {
                        bundle = metadata.getBundle();
                    }
                    android.support.v4.media.session.MediaSessionCompatApi14.setMetadata(obj2, bundle);
                }
            }
        }

        public void setSessionActivity(android.app.PendingIntent pi) {
            synchronized (this.mLock) {
                this.mSessionActivity = pi;
            }
        }

        public void setMediaButtonReceiver(android.app.PendingIntent mbr) {
        }

        public void setQueue(java.util.List<android.support.v4.media.session.MediaSessionCompat.QueueItem> queue) {
            this.mQueue = queue;
            sendQueue(queue);
        }

        public void setQueueTitle(java.lang.CharSequence title) {
            this.mQueueTitle = title;
            sendQueueTitle(title);
        }

        public java.lang.Object getMediaSession() {
            return null;
        }

        public java.lang.Object getRemoteControlClient() {
            return this.mRccObj;
        }

        public java.lang.String getCallingPackage() {
            return null;
        }

        public void setRatingType(int type) {
            this.mRatingType = type;
        }

        public void setRepeatMode(int repeatMode) {
            if (this.mRepeatMode != repeatMode) {
                this.mRepeatMode = repeatMode;
                sendRepeatMode(repeatMode);
            }
        }

        public void setShuffleModeEnabled(boolean enabled) {
            if (this.mShuffleModeEnabled != enabled) {
                this.mShuffleModeEnabled = enabled;
                sendShuffleModeEnabled(enabled);
            }
        }

        public void setExtras(android.os.Bundle extras) {
            this.mExtras = extras;
            sendExtras(extras);
        }

        private boolean update() {
            if (this.mIsActive) {
                if (!this.mIsMbrRegistered && (this.mFlags & 1) != 0) {
                    if (android.os.Build.VERSION.SDK_INT >= 18) {
                        android.support.v4.media.session.MediaSessionCompatApi18.registerMediaButtonEventReceiver(this.mContext, this.mMediaButtonReceiverIntent, this.mMediaButtonReceiverComponentName);
                    } else {
                        ((android.media.AudioManager) this.mContext.getSystemService("audio")).registerMediaButtonEventReceiver(this.mMediaButtonReceiverComponentName);
                    }
                    this.mIsMbrRegistered = true;
                } else if (this.mIsMbrRegistered && (this.mFlags & 1) == 0) {
                    if (android.os.Build.VERSION.SDK_INT >= 18) {
                        android.support.v4.media.session.MediaSessionCompatApi18.unregisterMediaButtonEventReceiver(this.mContext, this.mMediaButtonReceiverIntent, this.mMediaButtonReceiverComponentName);
                    } else {
                        ((android.media.AudioManager) this.mContext.getSystemService("audio")).unregisterMediaButtonEventReceiver(this.mMediaButtonReceiverComponentName);
                    }
                    this.mIsMbrRegistered = false;
                }
                if (android.os.Build.VERSION.SDK_INT < 14) {
                    return false;
                }
                if (!this.mIsRccRegistered && (this.mFlags & 2) != 0) {
                    android.support.v4.media.session.MediaSessionCompatApi14.registerRemoteControlClient(this.mContext, this.mRccObj);
                    this.mIsRccRegistered = true;
                    return true;
                } else if (!this.mIsRccRegistered || (this.mFlags & 2) != 0) {
                    return false;
                } else {
                    android.support.v4.media.session.MediaSessionCompatApi14.setState(this.mRccObj, 0);
                    android.support.v4.media.session.MediaSessionCompatApi14.unregisterRemoteControlClient(this.mContext, this.mRccObj);
                    this.mIsRccRegistered = false;
                    return false;
                }
            } else {
                if (this.mIsMbrRegistered) {
                    if (android.os.Build.VERSION.SDK_INT >= 18) {
                        android.support.v4.media.session.MediaSessionCompatApi18.unregisterMediaButtonEventReceiver(this.mContext, this.mMediaButtonReceiverIntent, this.mMediaButtonReceiverComponentName);
                    } else {
                        ((android.media.AudioManager) this.mContext.getSystemService("audio")).unregisterMediaButtonEventReceiver(this.mMediaButtonReceiverComponentName);
                    }
                    this.mIsMbrRegistered = false;
                }
                if (!this.mIsRccRegistered) {
                    return false;
                }
                android.support.v4.media.session.MediaSessionCompatApi14.setState(this.mRccObj, 0);
                android.support.v4.media.session.MediaSessionCompatApi14.unregisterRemoteControlClient(this.mContext, this.mRccObj);
                this.mIsRccRegistered = false;
                return false;
            }
        }

        /* access modifiers changed from: 0000 */
        public void adjustVolume(int direction, int flags) {
            if (this.mVolumeType != 2) {
                this.mAudioManager.adjustStreamVolume(this.mLocalStream, direction, flags);
            } else if (this.mVolumeProvider != null) {
                this.mVolumeProvider.onAdjustVolume(direction);
            }
        }

        /* access modifiers changed from: 0000 */
        public void setVolumeTo(int value, int flags) {
            if (this.mVolumeType != 2) {
                this.mAudioManager.setStreamVolume(this.mLocalStream, value, flags);
            } else if (this.mVolumeProvider != null) {
                this.mVolumeProvider.onSetVolumeTo(value);
            }
        }

        /* access modifiers changed from: 0000 */
        public android.support.v4.media.session.PlaybackStateCompat getStateWithUpdatedPosition() {
            android.support.v4.media.session.PlaybackStateCompat state;
            long duration = -1;
            synchronized (this.mLock) {
                state = this.mState;
                if (this.mMetadata != null && this.mMetadata.containsKey(android.support.v4.media.MediaMetadataCompat.METADATA_KEY_DURATION)) {
                    duration = this.mMetadata.getLong(android.support.v4.media.MediaMetadataCompat.METADATA_KEY_DURATION);
                }
            }
            android.support.v4.media.session.PlaybackStateCompat result = null;
            if (state != null && (state.getState() == 3 || state.getState() == 4 || state.getState() == 5)) {
                long updateTime = state.getLastPositionUpdateTime();
                long currentTime = android.os.SystemClock.elapsedRealtime();
                if (updateTime > 0) {
                    long position = ((long) (state.getPlaybackSpeed() * ((float) (currentTime - updateTime)))) + state.getPosition();
                    if (duration >= 0 && position > duration) {
                        position = duration;
                    } else if (position < 0) {
                        position = 0;
                    }
                    android.support.v4.media.session.PlaybackStateCompat.Builder builder = new android.support.v4.media.session.PlaybackStateCompat.Builder(state);
                    builder.setState(state.getState(), position, state.getPlaybackSpeed(), currentTime);
                    result = builder.build();
                }
            }
            if (result == null) {
                return state;
            }
            return result;
        }

        /* access modifiers changed from: 0000 */
        public void sendVolumeInfoChanged(android.support.v4.media.session.ParcelableVolumeInfo info) {
            for (int i = this.mControllerCallbacks.beginBroadcast() - 1; i >= 0; i--) {
                try {
                    ((android.support.v4.media.session.IMediaControllerCallback) this.mControllerCallbacks.getBroadcastItem(i)).onVolumeInfoChanged(info);
                } catch (android.os.RemoteException e) {
                }
            }
            this.mControllerCallbacks.finishBroadcast();
        }

        private void sendSessionDestroyed() {
            for (int i = this.mControllerCallbacks.beginBroadcast() - 1; i >= 0; i--) {
                try {
                    ((android.support.v4.media.session.IMediaControllerCallback) this.mControllerCallbacks.getBroadcastItem(i)).onSessionDestroyed();
                } catch (android.os.RemoteException e) {
                }
            }
            this.mControllerCallbacks.finishBroadcast();
            this.mControllerCallbacks.kill();
        }

        private void sendEvent(java.lang.String event, android.os.Bundle extras) {
            for (int i = this.mControllerCallbacks.beginBroadcast() - 1; i >= 0; i--) {
                try {
                    ((android.support.v4.media.session.IMediaControllerCallback) this.mControllerCallbacks.getBroadcastItem(i)).onEvent(event, extras);
                } catch (android.os.RemoteException e) {
                }
            }
            this.mControllerCallbacks.finishBroadcast();
        }

        private void sendState(android.support.v4.media.session.PlaybackStateCompat state) {
            for (int i = this.mControllerCallbacks.beginBroadcast() - 1; i >= 0; i--) {
                try {
                    ((android.support.v4.media.session.IMediaControllerCallback) this.mControllerCallbacks.getBroadcastItem(i)).onPlaybackStateChanged(state);
                } catch (android.os.RemoteException e) {
                }
            }
            this.mControllerCallbacks.finishBroadcast();
        }

        private void sendMetadata(android.support.v4.media.MediaMetadataCompat metadata) {
            for (int i = this.mControllerCallbacks.beginBroadcast() - 1; i >= 0; i--) {
                try {
                    ((android.support.v4.media.session.IMediaControllerCallback) this.mControllerCallbacks.getBroadcastItem(i)).onMetadataChanged(metadata);
                } catch (android.os.RemoteException e) {
                }
            }
            this.mControllerCallbacks.finishBroadcast();
        }

        private void sendQueue(java.util.List<android.support.v4.media.session.MediaSessionCompat.QueueItem> queue) {
            for (int i = this.mControllerCallbacks.beginBroadcast() - 1; i >= 0; i--) {
                try {
                    ((android.support.v4.media.session.IMediaControllerCallback) this.mControllerCallbacks.getBroadcastItem(i)).onQueueChanged(queue);
                } catch (android.os.RemoteException e) {
                }
            }
            this.mControllerCallbacks.finishBroadcast();
        }

        private void sendQueueTitle(java.lang.CharSequence queueTitle) {
            for (int i = this.mControllerCallbacks.beginBroadcast() - 1; i >= 0; i--) {
                try {
                    ((android.support.v4.media.session.IMediaControllerCallback) this.mControllerCallbacks.getBroadcastItem(i)).onQueueTitleChanged(queueTitle);
                } catch (android.os.RemoteException e) {
                }
            }
            this.mControllerCallbacks.finishBroadcast();
        }

        private void sendRepeatMode(int repeatMode) {
            for (int i = this.mControllerCallbacks.beginBroadcast() - 1; i >= 0; i--) {
                try {
                    ((android.support.v4.media.session.IMediaControllerCallback) this.mControllerCallbacks.getBroadcastItem(i)).onRepeatModeChanged(repeatMode);
                } catch (android.os.RemoteException e) {
                }
            }
            this.mControllerCallbacks.finishBroadcast();
        }

        private void sendShuffleModeEnabled(boolean enabled) {
            for (int i = this.mControllerCallbacks.beginBroadcast() - 1; i >= 0; i--) {
                try {
                    ((android.support.v4.media.session.IMediaControllerCallback) this.mControllerCallbacks.getBroadcastItem(i)).onShuffleModeChanged(enabled);
                } catch (android.os.RemoteException e) {
                }
            }
            this.mControllerCallbacks.finishBroadcast();
        }

        private void sendExtras(android.os.Bundle extras) {
            for (int i = this.mControllerCallbacks.beginBroadcast() - 1; i >= 0; i--) {
                try {
                    ((android.support.v4.media.session.IMediaControllerCallback) this.mControllerCallbacks.getBroadcastItem(i)).onExtrasChanged(extras);
                } catch (android.os.RemoteException e) {
                }
            }
            this.mControllerCallbacks.finishBroadcast();
        }
    }

    public interface OnActiveChangeListener {
        void onActiveChanged();
    }

    public static final class QueueItem implements android.os.Parcelable {
        public static final android.os.Parcelable.Creator<android.support.v4.media.session.MediaSessionCompat.QueueItem> CREATOR = new android.os.Parcelable.Creator<android.support.v4.media.session.MediaSessionCompat.QueueItem>() {
            public android.support.v4.media.session.MediaSessionCompat.QueueItem createFromParcel(android.os.Parcel p) {
                return new android.support.v4.media.session.MediaSessionCompat.QueueItem(p);
            }

            public android.support.v4.media.session.MediaSessionCompat.QueueItem[] newArray(int size) {
                return new android.support.v4.media.session.MediaSessionCompat.QueueItem[size];
            }
        };
        public static final int UNKNOWN_ID = -1;
        private final android.support.v4.media.MediaDescriptionCompat mDescription;
        private final long mId;
        private java.lang.Object mItem;

        public QueueItem(android.support.v4.media.MediaDescriptionCompat description, long id) {
            this(null, description, id);
        }

        private QueueItem(java.lang.Object queueItem, android.support.v4.media.MediaDescriptionCompat description, long id) {
            if (description == null) {
                throw new java.lang.IllegalArgumentException("Description cannot be null.");
            } else if (id == -1) {
                throw new java.lang.IllegalArgumentException("Id cannot be QueueItem.UNKNOWN_ID");
            } else {
                this.mDescription = description;
                this.mId = id;
                this.mItem = queueItem;
            }
        }

        QueueItem(android.os.Parcel in) {
            this.mDescription = (android.support.v4.media.MediaDescriptionCompat) android.support.v4.media.MediaDescriptionCompat.CREATOR.createFromParcel(in);
            this.mId = in.readLong();
        }

        public android.support.v4.media.MediaDescriptionCompat getDescription() {
            return this.mDescription;
        }

        public long getQueueId() {
            return this.mId;
        }

        public void writeToParcel(android.os.Parcel dest, int flags) {
            this.mDescription.writeToParcel(dest, flags);
            dest.writeLong(this.mId);
        }

        public int describeContents() {
            return 0;
        }

        public java.lang.Object getQueueItem() {
            if (this.mItem != null || android.os.Build.VERSION.SDK_INT < 21) {
                return this.mItem;
            }
            this.mItem = android.support.v4.media.session.MediaSessionCompatApi21.QueueItem.createItem(this.mDescription.getMediaDescription(), this.mId);
            return this.mItem;
        }

        @java.lang.Deprecated
        public static android.support.v4.media.session.MediaSessionCompat.QueueItem obtain(java.lang.Object queueItem) {
            return fromQueueItem(queueItem);
        }

        public static android.support.v4.media.session.MediaSessionCompat.QueueItem fromQueueItem(java.lang.Object queueItem) {
            if (queueItem == null || android.os.Build.VERSION.SDK_INT < 21) {
                return null;
            }
            return new android.support.v4.media.session.MediaSessionCompat.QueueItem(queueItem, android.support.v4.media.MediaDescriptionCompat.fromMediaDescription(android.support.v4.media.session.MediaSessionCompatApi21.QueueItem.getDescription(queueItem)), android.support.v4.media.session.MediaSessionCompatApi21.QueueItem.getQueueId(queueItem));
        }

        public static java.util.List<android.support.v4.media.session.MediaSessionCompat.QueueItem> fromQueueItemList(java.util.List<?> itemList) {
            if (itemList == null || android.os.Build.VERSION.SDK_INT < 21) {
                return null;
            }
            java.util.List<android.support.v4.media.session.MediaSessionCompat.QueueItem> items = new java.util.ArrayList<>();
            for (java.lang.Object itemObj : itemList) {
                items.add(fromQueueItem(itemObj));
            }
            return items;
        }

        public java.lang.String toString() {
            return "MediaSession.QueueItem {Description=" + this.mDescription + ", Id=" + this.mId + " }";
        }
    }

    static final class ResultReceiverWrapper implements android.os.Parcelable {
        public static final android.os.Parcelable.Creator<android.support.v4.media.session.MediaSessionCompat.ResultReceiverWrapper> CREATOR = new android.os.Parcelable.Creator<android.support.v4.media.session.MediaSessionCompat.ResultReceiverWrapper>() {
            public android.support.v4.media.session.MediaSessionCompat.ResultReceiverWrapper createFromParcel(android.os.Parcel p) {
                return new android.support.v4.media.session.MediaSessionCompat.ResultReceiverWrapper(p);
            }

            public android.support.v4.media.session.MediaSessionCompat.ResultReceiverWrapper[] newArray(int size) {
                return new android.support.v4.media.session.MediaSessionCompat.ResultReceiverWrapper[size];
            }
        };
        /* access modifiers changed from: private */
        public android.os.ResultReceiver mResultReceiver;

        public ResultReceiverWrapper(android.os.ResultReceiver resultReceiver) {
            this.mResultReceiver = resultReceiver;
        }

        ResultReceiverWrapper(android.os.Parcel in) {
            this.mResultReceiver = (android.os.ResultReceiver) android.os.ResultReceiver.CREATOR.createFromParcel(in);
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(android.os.Parcel dest, int flags) {
            this.mResultReceiver.writeToParcel(dest, flags);
        }
    }

    @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
    @java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.SOURCE)
    public @interface SessionFlags {
    }

    public static final class Token implements android.os.Parcelable {
        public static final android.os.Parcelable.Creator<android.support.v4.media.session.MediaSessionCompat.Token> CREATOR = new android.os.Parcelable.Creator<android.support.v4.media.session.MediaSessionCompat.Token>() {
            public android.support.v4.media.session.MediaSessionCompat.Token createFromParcel(android.os.Parcel in) {
                java.lang.Object readStrongBinder;
                if (android.os.Build.VERSION.SDK_INT >= 21) {
                    readStrongBinder = in.readParcelable(null);
                } else {
                    readStrongBinder = in.readStrongBinder();
                }
                return new android.support.v4.media.session.MediaSessionCompat.Token(readStrongBinder);
            }

            public android.support.v4.media.session.MediaSessionCompat.Token[] newArray(int size) {
                return new android.support.v4.media.session.MediaSessionCompat.Token[size];
            }
        };
        private final java.lang.Object mInner;

        Token(java.lang.Object inner) {
            this.mInner = inner;
        }

        public static android.support.v4.media.session.MediaSessionCompat.Token fromToken(java.lang.Object token) {
            if (token == null || android.os.Build.VERSION.SDK_INT < 21) {
                return null;
            }
            return new android.support.v4.media.session.MediaSessionCompat.Token(android.support.v4.media.session.MediaSessionCompatApi21.verifyToken(token));
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(android.os.Parcel dest, int flags) {
            if (android.os.Build.VERSION.SDK_INT >= 21) {
                dest.writeParcelable((android.os.Parcelable) this.mInner, flags);
            } else {
                dest.writeStrongBinder((android.os.IBinder) this.mInner);
            }
        }

        public int hashCode() {
            if (this.mInner == null) {
                return 0;
            }
            return this.mInner.hashCode();
        }

        public boolean equals(java.lang.Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof android.support.v4.media.session.MediaSessionCompat.Token)) {
                return false;
            }
            android.support.v4.media.session.MediaSessionCompat.Token other = (android.support.v4.media.session.MediaSessionCompat.Token) obj;
            if (this.mInner == null) {
                if (other.mInner != null) {
                    return false;
                }
                return true;
            } else if (other.mInner == null) {
                return false;
            } else {
                return this.mInner.equals(other.mInner);
            }
        }

        public java.lang.Object getToken() {
            return this.mInner;
        }
    }

    public MediaSessionCompat(android.content.Context context, java.lang.String tag) {
        this(context, tag, null, null);
    }

    public MediaSessionCompat(android.content.Context context, java.lang.String tag, android.content.ComponentName mbrComponent, android.app.PendingIntent mbrIntent) {
        this.mActiveListeners = new java.util.ArrayList<>();
        if (context == null) {
            throw new java.lang.IllegalArgumentException("context must not be null");
        } else if (android.text.TextUtils.isEmpty(tag)) {
            throw new java.lang.IllegalArgumentException("tag must not be null or empty");
        } else {
            if (mbrComponent == null) {
                mbrComponent = android.support.v4.media.session.MediaButtonReceiver.getMediaButtonReceiverComponent(context);
                if (mbrComponent == null) {
                    android.util.Log.w(TAG, "Couldn't find a unique registered media button receiver in the given context.");
                }
            }
            if (mbrComponent != null && mbrIntent == null) {
                android.content.Intent mediaButtonIntent = new android.content.Intent("android.intent.action.MEDIA_BUTTON");
                mediaButtonIntent.setComponent(mbrComponent);
                mbrIntent = android.app.PendingIntent.getBroadcast(context, 0, mediaButtonIntent, 0);
            }
            if (android.os.Build.VERSION.SDK_INT >= 21) {
                this.mImpl = new android.support.v4.media.session.MediaSessionCompat.MediaSessionImplApi21(context, tag);
                this.mImpl.setMediaButtonReceiver(mbrIntent);
                setCallback(new android.support.v4.media.session.MediaSessionCompat.Callback() {
                });
            } else {
                this.mImpl = new android.support.v4.media.session.MediaSessionCompat.MediaSessionImplBase(context, tag, mbrComponent, mbrIntent);
            }
            this.mController = new android.support.v4.media.session.MediaControllerCompat(context, this);
            if (sMaxBitmapSize == 0) {
                sMaxBitmapSize = (int) android.util.TypedValue.applyDimension(1, 320.0f, context.getResources().getDisplayMetrics());
            }
        }
    }

    private MediaSessionCompat(android.content.Context context, android.support.v4.media.session.MediaSessionCompat.MediaSessionImpl impl) {
        this.mActiveListeners = new java.util.ArrayList<>();
        this.mImpl = impl;
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            setCallback(new android.support.v4.media.session.MediaSessionCompat.Callback() {
            });
        }
        this.mController = new android.support.v4.media.session.MediaControllerCompat(context, this);
    }

    public void setCallback(android.support.v4.media.session.MediaSessionCompat.Callback callback) {
        setCallback(callback, null);
    }

    public void setCallback(android.support.v4.media.session.MediaSessionCompat.Callback callback, android.os.Handler handler) {
        android.support.v4.media.session.MediaSessionCompat.MediaSessionImpl mediaSessionImpl = this.mImpl;
        if (handler == null) {
            handler = new android.os.Handler();
        }
        mediaSessionImpl.setCallback(callback, handler);
    }

    public void setSessionActivity(android.app.PendingIntent pi) {
        this.mImpl.setSessionActivity(pi);
    }

    public void setMediaButtonReceiver(android.app.PendingIntent mbr) {
        this.mImpl.setMediaButtonReceiver(mbr);
    }

    public void setFlags(int flags) {
        this.mImpl.setFlags(flags);
    }

    public void setPlaybackToLocal(int stream) {
        this.mImpl.setPlaybackToLocal(stream);
    }

    public void setPlaybackToRemote(android.support.v4.media.VolumeProviderCompat volumeProvider) {
        if (volumeProvider == null) {
            throw new java.lang.IllegalArgumentException("volumeProvider may not be null!");
        }
        this.mImpl.setPlaybackToRemote(volumeProvider);
    }

    public void setActive(boolean active) {
        this.mImpl.setActive(active);
        java.util.Iterator it = this.mActiveListeners.iterator();
        while (it.hasNext()) {
            ((android.support.v4.media.session.MediaSessionCompat.OnActiveChangeListener) it.next()).onActiveChanged();
        }
    }

    public boolean isActive() {
        return this.mImpl.isActive();
    }

    public void sendSessionEvent(java.lang.String event, android.os.Bundle extras) {
        if (android.text.TextUtils.isEmpty(event)) {
            throw new java.lang.IllegalArgumentException("event cannot be null or empty");
        }
        this.mImpl.sendSessionEvent(event, extras);
    }

    public void release() {
        this.mImpl.release();
    }

    public android.support.v4.media.session.MediaSessionCompat.Token getSessionToken() {
        return this.mImpl.getSessionToken();
    }

    public android.support.v4.media.session.MediaControllerCompat getController() {
        return this.mController;
    }

    public void setPlaybackState(android.support.v4.media.session.PlaybackStateCompat state) {
        this.mImpl.setPlaybackState(state);
    }

    public void setMetadata(android.support.v4.media.MediaMetadataCompat metadata) {
        this.mImpl.setMetadata(metadata);
    }

    public void setQueue(java.util.List<android.support.v4.media.session.MediaSessionCompat.QueueItem> queue) {
        this.mImpl.setQueue(queue);
    }

    public void setQueueTitle(java.lang.CharSequence title) {
        this.mImpl.setQueueTitle(title);
    }

    public void setRatingType(int type) {
        this.mImpl.setRatingType(type);
    }

    public void setRepeatMode(int repeatMode) {
        this.mImpl.setRepeatMode(repeatMode);
    }

    public void setShuffleModeEnabled(boolean enabled) {
        this.mImpl.setShuffleModeEnabled(enabled);
    }

    public void setExtras(android.os.Bundle extras) {
        this.mImpl.setExtras(extras);
    }

    public java.lang.Object getMediaSession() {
        return this.mImpl.getMediaSession();
    }

    public java.lang.Object getRemoteControlClient() {
        return this.mImpl.getRemoteControlClient();
    }

    @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
    public java.lang.String getCallingPackage() {
        return this.mImpl.getCallingPackage();
    }

    public void addOnActiveChangeListener(android.support.v4.media.session.MediaSessionCompat.OnActiveChangeListener listener) {
        if (listener == null) {
            throw new java.lang.IllegalArgumentException("Listener may not be null");
        }
        this.mActiveListeners.add(listener);
    }

    public void removeOnActiveChangeListener(android.support.v4.media.session.MediaSessionCompat.OnActiveChangeListener listener) {
        if (listener == null) {
            throw new java.lang.IllegalArgumentException("Listener may not be null");
        }
        this.mActiveListeners.remove(listener);
    }

    @java.lang.Deprecated
    public static android.support.v4.media.session.MediaSessionCompat obtain(android.content.Context context, java.lang.Object mediaSession) {
        return fromMediaSession(context, mediaSession);
    }

    public static android.support.v4.media.session.MediaSessionCompat fromMediaSession(android.content.Context context, java.lang.Object mediaSession) {
        if (context == null || mediaSession == null || android.os.Build.VERSION.SDK_INT < 21) {
            return null;
        }
        return new android.support.v4.media.session.MediaSessionCompat(context, (android.support.v4.media.session.MediaSessionCompat.MediaSessionImpl) new android.support.v4.media.session.MediaSessionCompat.MediaSessionImplApi21(mediaSession));
    }
}
