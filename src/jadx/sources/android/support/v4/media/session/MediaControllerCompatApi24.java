package android.support.v4.media.session;

@android.annotation.TargetApi(24)
@android.support.annotation.RequiresApi(24)
class MediaControllerCompatApi24 {

    public static class TransportControls extends android.support.v4.media.session.MediaControllerCompatApi23.TransportControls {
        public static void prepare(java.lang.Object controlsObj) {
            ((android.media.session.MediaController.TransportControls) controlsObj).prepare();
        }

        public static void prepareFromMediaId(java.lang.Object controlsObj, java.lang.String mediaId, android.os.Bundle extras) {
            ((android.media.session.MediaController.TransportControls) controlsObj).prepareFromMediaId(mediaId, extras);
        }

        public static void prepareFromSearch(java.lang.Object controlsObj, java.lang.String query, android.os.Bundle extras) {
            ((android.media.session.MediaController.TransportControls) controlsObj).prepareFromSearch(query, extras);
        }

        public static void prepareFromUri(java.lang.Object controlsObj, android.net.Uri uri, android.os.Bundle extras) {
            ((android.media.session.MediaController.TransportControls) controlsObj).prepareFromUri(uri, extras);
        }
    }

    MediaControllerCompatApi24() {
    }
}
