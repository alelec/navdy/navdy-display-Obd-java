package android.support.v4.media.session;

@android.annotation.TargetApi(21)
@android.support.annotation.RequiresApi(21)
class MediaSessionCompatApi21 {

    interface Callback extends android.support.v4.media.session.MediaSessionCompatApi19.Callback {
        void onCommand(java.lang.String str, android.os.Bundle bundle, android.os.ResultReceiver resultReceiver);

        void onCustomAction(java.lang.String str, android.os.Bundle bundle);

        void onFastForward();

        boolean onMediaButtonEvent(android.content.Intent intent);

        void onPause();

        void onPlay();

        void onPlayFromMediaId(java.lang.String str, android.os.Bundle bundle);

        void onPlayFromSearch(java.lang.String str, android.os.Bundle bundle);

        void onRewind();

        void onSkipToNext();

        void onSkipToPrevious();

        void onSkipToQueueItem(long j);

        void onStop();
    }

    static class CallbackProxy<T extends android.support.v4.media.session.MediaSessionCompatApi21.Callback> extends android.media.session.MediaSession.Callback {
        protected final T mCallback;

        public CallbackProxy(T callback) {
            this.mCallback = callback;
        }

        public void onCommand(java.lang.String command, android.os.Bundle args, android.os.ResultReceiver cb) {
            this.mCallback.onCommand(command, args, cb);
        }

        public boolean onMediaButtonEvent(android.content.Intent mediaButtonIntent) {
            return this.mCallback.onMediaButtonEvent(mediaButtonIntent) || super.onMediaButtonEvent(mediaButtonIntent);
        }

        public void onPlay() {
            this.mCallback.onPlay();
        }

        public void onPlayFromMediaId(java.lang.String mediaId, android.os.Bundle extras) {
            this.mCallback.onPlayFromMediaId(mediaId, extras);
        }

        public void onPlayFromSearch(java.lang.String search, android.os.Bundle extras) {
            this.mCallback.onPlayFromSearch(search, extras);
        }

        public void onSkipToQueueItem(long id) {
            this.mCallback.onSkipToQueueItem(id);
        }

        public void onPause() {
            this.mCallback.onPause();
        }

        public void onSkipToNext() {
            this.mCallback.onSkipToNext();
        }

        public void onSkipToPrevious() {
            this.mCallback.onSkipToPrevious();
        }

        public void onFastForward() {
            this.mCallback.onFastForward();
        }

        public void onRewind() {
            this.mCallback.onRewind();
        }

        public void onStop() {
            this.mCallback.onStop();
        }

        public void onSeekTo(long pos) {
            this.mCallback.onSeekTo(pos);
        }

        public void onSetRating(android.media.Rating rating) {
            this.mCallback.onSetRating(rating);
        }

        public void onCustomAction(java.lang.String action, android.os.Bundle extras) {
            this.mCallback.onCustomAction(action, extras);
        }
    }

    static class QueueItem {
        QueueItem() {
        }

        public static java.lang.Object createItem(java.lang.Object mediaDescription, long id) {
            return new android.media.session.MediaSession.QueueItem((android.media.MediaDescription) mediaDescription, id);
        }

        public static java.lang.Object getDescription(java.lang.Object queueItem) {
            return ((android.media.session.MediaSession.QueueItem) queueItem).getDescription();
        }

        public static long getQueueId(java.lang.Object queueItem) {
            return ((android.media.session.MediaSession.QueueItem) queueItem).getQueueId();
        }
    }

    MediaSessionCompatApi21() {
    }

    public static java.lang.Object createSession(android.content.Context context, java.lang.String tag) {
        return new android.media.session.MediaSession(context, tag);
    }

    public static java.lang.Object verifySession(java.lang.Object mediaSession) {
        if (mediaSession instanceof android.media.session.MediaSession) {
            return mediaSession;
        }
        throw new java.lang.IllegalArgumentException("mediaSession is not a valid MediaSession object");
    }

    public static java.lang.Object verifyToken(java.lang.Object token) {
        if (token instanceof android.media.session.MediaSession.Token) {
            return token;
        }
        throw new java.lang.IllegalArgumentException("token is not a valid MediaSession.Token object");
    }

    public static java.lang.Object createCallback(android.support.v4.media.session.MediaSessionCompatApi21.Callback callback) {
        return new android.support.v4.media.session.MediaSessionCompatApi21.CallbackProxy(callback);
    }

    public static void setCallback(java.lang.Object sessionObj, java.lang.Object callbackObj, android.os.Handler handler) {
        ((android.media.session.MediaSession) sessionObj).setCallback((android.media.session.MediaSession.Callback) callbackObj, handler);
    }

    public static void setFlags(java.lang.Object sessionObj, int flags) {
        ((android.media.session.MediaSession) sessionObj).setFlags(flags);
    }

    public static void setPlaybackToLocal(java.lang.Object sessionObj, int stream) {
        android.media.AudioAttributes.Builder bob = new android.media.AudioAttributes.Builder();
        bob.setLegacyStreamType(stream);
        ((android.media.session.MediaSession) sessionObj).setPlaybackToLocal(bob.build());
    }

    public static void setPlaybackToRemote(java.lang.Object sessionObj, java.lang.Object volumeProviderObj) {
        ((android.media.session.MediaSession) sessionObj).setPlaybackToRemote((android.media.VolumeProvider) volumeProviderObj);
    }

    public static void setActive(java.lang.Object sessionObj, boolean active) {
        ((android.media.session.MediaSession) sessionObj).setActive(active);
    }

    public static boolean isActive(java.lang.Object sessionObj) {
        return ((android.media.session.MediaSession) sessionObj).isActive();
    }

    public static void sendSessionEvent(java.lang.Object sessionObj, java.lang.String event, android.os.Bundle extras) {
        ((android.media.session.MediaSession) sessionObj).sendSessionEvent(event, extras);
    }

    public static void release(java.lang.Object sessionObj) {
        ((android.media.session.MediaSession) sessionObj).release();
    }

    public static android.os.Parcelable getSessionToken(java.lang.Object sessionObj) {
        return ((android.media.session.MediaSession) sessionObj).getSessionToken();
    }

    public static void setPlaybackState(java.lang.Object sessionObj, java.lang.Object stateObj) {
        ((android.media.session.MediaSession) sessionObj).setPlaybackState((android.media.session.PlaybackState) stateObj);
    }

    public static void setMetadata(java.lang.Object sessionObj, java.lang.Object metadataObj) {
        ((android.media.session.MediaSession) sessionObj).setMetadata((android.media.MediaMetadata) metadataObj);
    }

    public static void setSessionActivity(java.lang.Object sessionObj, android.app.PendingIntent pi) {
        ((android.media.session.MediaSession) sessionObj).setSessionActivity(pi);
    }

    public static void setMediaButtonReceiver(java.lang.Object sessionObj, android.app.PendingIntent pi) {
        ((android.media.session.MediaSession) sessionObj).setMediaButtonReceiver(pi);
    }

    public static void setQueue(java.lang.Object sessionObj, java.util.List<java.lang.Object> queueObjs) {
        if (queueObjs == null) {
            ((android.media.session.MediaSession) sessionObj).setQueue(null);
            return;
        }
        java.util.ArrayList<android.media.session.MediaSession.QueueItem> queue = new java.util.ArrayList<>();
        for (java.lang.Object itemObj : queueObjs) {
            queue.add(itemObj);
        }
        ((android.media.session.MediaSession) sessionObj).setQueue(queue);
    }

    public static void setQueueTitle(java.lang.Object sessionObj, java.lang.CharSequence title) {
        ((android.media.session.MediaSession) sessionObj).setQueueTitle(title);
    }

    public static void setExtras(java.lang.Object sessionObj, android.os.Bundle extras) {
        ((android.media.session.MediaSession) sessionObj).setExtras(extras);
    }
}
