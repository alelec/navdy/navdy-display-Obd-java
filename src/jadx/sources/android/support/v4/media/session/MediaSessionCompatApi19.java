package android.support.v4.media.session;

@android.annotation.TargetApi(19)
@android.support.annotation.RequiresApi(19)
class MediaSessionCompatApi19 {
    private static final long ACTION_SET_RATING = 128;
    private static final java.lang.String METADATA_KEY_RATING = "android.media.metadata.RATING";
    private static final java.lang.String METADATA_KEY_USER_RATING = "android.media.metadata.USER_RATING";
    private static final java.lang.String METADATA_KEY_YEAR = "android.media.metadata.YEAR";

    interface Callback extends android.support.v4.media.session.MediaSessionCompatApi18.Callback {
        void onSetRating(java.lang.Object obj);
    }

    static class OnMetadataUpdateListener<T extends android.support.v4.media.session.MediaSessionCompatApi19.Callback> implements android.media.RemoteControlClient.OnMetadataUpdateListener {
        protected final T mCallback;

        public OnMetadataUpdateListener(T callback) {
            this.mCallback = callback;
        }

        public void onMetadataUpdate(int key, java.lang.Object newValue) {
            if (key == 268435457 && (newValue instanceof android.media.Rating)) {
                this.mCallback.onSetRating(newValue);
            }
        }
    }

    MediaSessionCompatApi19() {
    }

    public static void setTransportControlFlags(java.lang.Object rccObj, long actions) {
        ((android.media.RemoteControlClient) rccObj).setTransportControlFlags(getRccTransportControlFlagsFromActions(actions));
    }

    public static java.lang.Object createMetadataUpdateListener(android.support.v4.media.session.MediaSessionCompatApi19.Callback callback) {
        return new android.support.v4.media.session.MediaSessionCompatApi19.OnMetadataUpdateListener(callback);
    }

    public static void setMetadata(java.lang.Object rccObj, android.os.Bundle metadata, long actions) {
        android.media.RemoteControlClient.MetadataEditor editor = ((android.media.RemoteControlClient) rccObj).editMetadata(true);
        android.support.v4.media.session.MediaSessionCompatApi14.buildOldMetadata(metadata, editor);
        addNewMetadata(metadata, editor);
        if ((128 & actions) != 0) {
            editor.addEditableKey(268435457);
        }
        editor.apply();
    }

    public static void setOnMetadataUpdateListener(java.lang.Object rccObj, java.lang.Object onMetadataUpdateObj) {
        ((android.media.RemoteControlClient) rccObj).setMetadataUpdateListener((android.media.RemoteControlClient.OnMetadataUpdateListener) onMetadataUpdateObj);
    }

    static int getRccTransportControlFlagsFromActions(long actions) {
        int transportControlFlags = android.support.v4.media.session.MediaSessionCompatApi18.getRccTransportControlFlagsFromActions(actions);
        if ((128 & actions) != 0) {
            return transportControlFlags | 512;
        }
        return transportControlFlags;
    }

    static void addNewMetadata(android.os.Bundle metadata, android.media.RemoteControlClient.MetadataEditor editor) {
        if (metadata != null) {
            if (metadata.containsKey("android.media.metadata.YEAR")) {
                editor.putLong(8, metadata.getLong("android.media.metadata.YEAR"));
            }
            if (metadata.containsKey("android.media.metadata.RATING")) {
                editor.putObject(101, metadata.getParcelable("android.media.metadata.RATING"));
            }
            if (metadata.containsKey("android.media.metadata.USER_RATING")) {
                editor.putObject(268435457, metadata.getParcelable("android.media.metadata.USER_RATING"));
            }
        }
    }
}
