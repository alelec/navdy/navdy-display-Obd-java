package android.support.v4.media.session;

@android.annotation.TargetApi(22)
@android.support.annotation.RequiresApi(22)
class PlaybackStateCompatApi22 {
    PlaybackStateCompatApi22() {
    }

    public static android.os.Bundle getExtras(java.lang.Object stateObj) {
        return ((android.media.session.PlaybackState) stateObj).getExtras();
    }

    public static java.lang.Object newInstance(int state, long position, long bufferedPosition, float speed, long actions, java.lang.CharSequence errorMessage, long updateTime, java.util.List<java.lang.Object> customActions, long activeItemId, android.os.Bundle extras) {
        android.media.session.PlaybackState.Builder stateObj = new android.media.session.PlaybackState.Builder();
        stateObj.setState(state, position, speed, updateTime);
        stateObj.setBufferedPosition(bufferedPosition);
        stateObj.setActions(actions);
        stateObj.setErrorMessage(errorMessage);
        java.util.Iterator it = customActions.iterator();
        while (it.hasNext()) {
            stateObj.addCustomAction((android.media.session.PlaybackState.CustomAction) it.next());
        }
        stateObj.setActiveQueueItemId(activeItemId);
        stateObj.setExtras(extras);
        return stateObj.build();
    }
}
