package android.support.v4.media.session;

@android.annotation.TargetApi(24)
@android.support.annotation.RequiresApi(24)
class MediaSessionCompatApi24 {
    private static final java.lang.String TAG = "MediaSessionCompatApi24";

    public interface Callback extends android.support.v4.media.session.MediaSessionCompatApi23.Callback {
        void onPrepare();

        void onPrepareFromMediaId(java.lang.String str, android.os.Bundle bundle);

        void onPrepareFromSearch(java.lang.String str, android.os.Bundle bundle);

        void onPrepareFromUri(android.net.Uri uri, android.os.Bundle bundle);
    }

    static class CallbackProxy<T extends android.support.v4.media.session.MediaSessionCompatApi24.Callback> extends android.support.v4.media.session.MediaSessionCompatApi23.CallbackProxy<T> {
        public CallbackProxy(T callback) {
            super(callback);
        }

        public void onPrepare() {
            ((android.support.v4.media.session.MediaSessionCompatApi24.Callback) this.mCallback).onPrepare();
        }

        public void onPrepareFromMediaId(java.lang.String mediaId, android.os.Bundle extras) {
            ((android.support.v4.media.session.MediaSessionCompatApi24.Callback) this.mCallback).onPrepareFromMediaId(mediaId, extras);
        }

        public void onPrepareFromSearch(java.lang.String query, android.os.Bundle extras) {
            ((android.support.v4.media.session.MediaSessionCompatApi24.Callback) this.mCallback).onPrepareFromSearch(query, extras);
        }

        public void onPrepareFromUri(android.net.Uri uri, android.os.Bundle extras) {
            ((android.support.v4.media.session.MediaSessionCompatApi24.Callback) this.mCallback).onPrepareFromUri(uri, extras);
        }
    }

    MediaSessionCompatApi24() {
    }

    public static java.lang.Object createCallback(android.support.v4.media.session.MediaSessionCompatApi24.Callback callback) {
        return new android.support.v4.media.session.MediaSessionCompatApi24.CallbackProxy(callback);
    }

    public static java.lang.String getCallingPackage(java.lang.Object sessionObj) {
        android.media.session.MediaSession session = (android.media.session.MediaSession) sessionObj;
        try {
            return (java.lang.String) session.getClass().getMethod("getCallingPackage", new java.lang.Class[0]).invoke(session, new java.lang.Object[0]);
        } catch (java.lang.IllegalAccessException | java.lang.NoSuchMethodException | java.lang.reflect.InvocationTargetException e) {
            android.util.Log.e(TAG, "Cannot execute MediaSession.getCallingPackage()", e);
            return null;
        }
    }
}
