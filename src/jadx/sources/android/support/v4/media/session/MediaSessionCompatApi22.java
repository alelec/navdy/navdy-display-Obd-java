package android.support.v4.media.session;

@android.annotation.TargetApi(22)
@android.support.annotation.RequiresApi(22)
class MediaSessionCompatApi22 {
    MediaSessionCompatApi22() {
    }

    public static void setRatingType(java.lang.Object sessionObj, int type) {
        ((android.media.session.MediaSession) sessionObj).setRatingType(type);
    }
}
