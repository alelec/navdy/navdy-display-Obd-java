package android.support.v4.media.session;

@android.annotation.TargetApi(21)
@android.support.annotation.RequiresApi(21)
class MediaControllerCompatApi21 {

    public interface Callback {
        void onAudioInfoChanged(int i, int i2, int i3, int i4, int i5);

        void onExtrasChanged(android.os.Bundle bundle);

        void onMetadataChanged(java.lang.Object obj);

        void onPlaybackStateChanged(java.lang.Object obj);

        void onQueueChanged(java.util.List<?> list);

        void onQueueTitleChanged(java.lang.CharSequence charSequence);

        void onSessionDestroyed();

        void onSessionEvent(java.lang.String str, android.os.Bundle bundle);
    }

    static class CallbackProxy<T extends android.support.v4.media.session.MediaControllerCompatApi21.Callback> extends android.media.session.MediaController.Callback {
        protected final T mCallback;

        public CallbackProxy(T callback) {
            this.mCallback = callback;
        }

        public void onSessionDestroyed() {
            this.mCallback.onSessionDestroyed();
        }

        public void onSessionEvent(java.lang.String event, android.os.Bundle extras) {
            this.mCallback.onSessionEvent(event, extras);
        }

        public void onPlaybackStateChanged(android.media.session.PlaybackState state) {
            this.mCallback.onPlaybackStateChanged(state);
        }

        public void onMetadataChanged(android.media.MediaMetadata metadata) {
            this.mCallback.onMetadataChanged(metadata);
        }

        public void onQueueChanged(java.util.List<android.media.session.MediaSession.QueueItem> queue) {
            this.mCallback.onQueueChanged(queue);
        }

        public void onQueueTitleChanged(java.lang.CharSequence title) {
            this.mCallback.onQueueTitleChanged(title);
        }

        public void onExtrasChanged(android.os.Bundle extras) {
            this.mCallback.onExtrasChanged(extras);
        }

        public void onAudioInfoChanged(android.media.session.MediaController.PlaybackInfo info) {
            this.mCallback.onAudioInfoChanged(info.getPlaybackType(), android.support.v4.media.session.MediaControllerCompatApi21.PlaybackInfo.getLegacyAudioStream(info), info.getVolumeControl(), info.getMaxVolume(), info.getCurrentVolume());
        }
    }

    public static class PlaybackInfo {
        private static final int FLAG_SCO = 4;
        private static final int STREAM_BLUETOOTH_SCO = 6;
        private static final int STREAM_SYSTEM_ENFORCED = 7;

        public static int getPlaybackType(java.lang.Object volumeInfoObj) {
            return ((android.media.session.MediaController.PlaybackInfo) volumeInfoObj).getPlaybackType();
        }

        public static android.media.AudioAttributes getAudioAttributes(java.lang.Object volumeInfoObj) {
            return ((android.media.session.MediaController.PlaybackInfo) volumeInfoObj).getAudioAttributes();
        }

        public static int getLegacyAudioStream(java.lang.Object volumeInfoObj) {
            return toLegacyStreamType(getAudioAttributes(volumeInfoObj));
        }

        public static int getVolumeControl(java.lang.Object volumeInfoObj) {
            return ((android.media.session.MediaController.PlaybackInfo) volumeInfoObj).getVolumeControl();
        }

        public static int getMaxVolume(java.lang.Object volumeInfoObj) {
            return ((android.media.session.MediaController.PlaybackInfo) volumeInfoObj).getMaxVolume();
        }

        public static int getCurrentVolume(java.lang.Object volumeInfoObj) {
            return ((android.media.session.MediaController.PlaybackInfo) volumeInfoObj).getCurrentVolume();
        }

        private static int toLegacyStreamType(android.media.AudioAttributes aa) {
            if ((aa.getFlags() & 1) == 1) {
                return 7;
            }
            if ((aa.getFlags() & 4) == 4) {
                return 6;
            }
            switch (aa.getUsage()) {
                case 2:
                    return 0;
                case 3:
                    return 8;
                case 4:
                    return 4;
                case 5:
                case 7:
                case 8:
                case 9:
                case 10:
                    return 5;
                case 6:
                    return 2;
                case 13:
                    return 1;
                default:
                    return 3;
            }
        }
    }

    public static class TransportControls {
        public static void play(java.lang.Object controlsObj) {
            ((android.media.session.MediaController.TransportControls) controlsObj).play();
        }

        public static void pause(java.lang.Object controlsObj) {
            ((android.media.session.MediaController.TransportControls) controlsObj).pause();
        }

        public static void stop(java.lang.Object controlsObj) {
            ((android.media.session.MediaController.TransportControls) controlsObj).stop();
        }

        public static void seekTo(java.lang.Object controlsObj, long pos) {
            ((android.media.session.MediaController.TransportControls) controlsObj).seekTo(pos);
        }

        public static void fastForward(java.lang.Object controlsObj) {
            ((android.media.session.MediaController.TransportControls) controlsObj).fastForward();
        }

        public static void rewind(java.lang.Object controlsObj) {
            ((android.media.session.MediaController.TransportControls) controlsObj).rewind();
        }

        public static void skipToNext(java.lang.Object controlsObj) {
            ((android.media.session.MediaController.TransportControls) controlsObj).skipToNext();
        }

        public static void skipToPrevious(java.lang.Object controlsObj) {
            ((android.media.session.MediaController.TransportControls) controlsObj).skipToPrevious();
        }

        public static void setRating(java.lang.Object controlsObj, java.lang.Object ratingObj) {
            ((android.media.session.MediaController.TransportControls) controlsObj).setRating((android.media.Rating) ratingObj);
        }

        public static void playFromMediaId(java.lang.Object controlsObj, java.lang.String mediaId, android.os.Bundle extras) {
            ((android.media.session.MediaController.TransportControls) controlsObj).playFromMediaId(mediaId, extras);
        }

        public static void playFromSearch(java.lang.Object controlsObj, java.lang.String query, android.os.Bundle extras) {
            ((android.media.session.MediaController.TransportControls) controlsObj).playFromSearch(query, extras);
        }

        public static void skipToQueueItem(java.lang.Object controlsObj, long id) {
            ((android.media.session.MediaController.TransportControls) controlsObj).skipToQueueItem(id);
        }

        public static void sendCustomAction(java.lang.Object controlsObj, java.lang.String action, android.os.Bundle args) {
            ((android.media.session.MediaController.TransportControls) controlsObj).sendCustomAction(action, args);
        }
    }

    MediaControllerCompatApi21() {
    }

    public static java.lang.Object fromToken(android.content.Context context, java.lang.Object sessionToken) {
        return new android.media.session.MediaController(context, (android.media.session.MediaSession.Token) sessionToken);
    }

    public static java.lang.Object createCallback(android.support.v4.media.session.MediaControllerCompatApi21.Callback callback) {
        return new android.support.v4.media.session.MediaControllerCompatApi21.CallbackProxy(callback);
    }

    public static void registerCallback(java.lang.Object controllerObj, java.lang.Object callbackObj, android.os.Handler handler) {
        ((android.media.session.MediaController) controllerObj).registerCallback((android.media.session.MediaController.Callback) callbackObj, handler);
    }

    public static void unregisterCallback(java.lang.Object controllerObj, java.lang.Object callbackObj) {
        ((android.media.session.MediaController) controllerObj).unregisterCallback((android.media.session.MediaController.Callback) callbackObj);
    }

    public static void setMediaController(android.app.Activity activity, java.lang.Object controllerObj) {
        activity.setMediaController((android.media.session.MediaController) controllerObj);
    }

    public static java.lang.Object getMediaController(android.app.Activity activity) {
        return activity.getMediaController();
    }

    public static java.lang.Object getSessionToken(java.lang.Object controllerObj) {
        return ((android.media.session.MediaController) controllerObj).getSessionToken();
    }

    public static java.lang.Object getTransportControls(java.lang.Object controllerObj) {
        return ((android.media.session.MediaController) controllerObj).getTransportControls();
    }

    public static java.lang.Object getPlaybackState(java.lang.Object controllerObj) {
        return ((android.media.session.MediaController) controllerObj).getPlaybackState();
    }

    public static java.lang.Object getMetadata(java.lang.Object controllerObj) {
        return ((android.media.session.MediaController) controllerObj).getMetadata();
    }

    public static java.util.List<java.lang.Object> getQueue(java.lang.Object controllerObj) {
        java.util.List<android.media.session.MediaSession.QueueItem> queue = ((android.media.session.MediaController) controllerObj).getQueue();
        if (queue == null) {
            return null;
        }
        return new java.util.ArrayList(queue);
    }

    public static java.lang.CharSequence getQueueTitle(java.lang.Object controllerObj) {
        return ((android.media.session.MediaController) controllerObj).getQueueTitle();
    }

    public static android.os.Bundle getExtras(java.lang.Object controllerObj) {
        return ((android.media.session.MediaController) controllerObj).getExtras();
    }

    public static int getRatingType(java.lang.Object controllerObj) {
        return ((android.media.session.MediaController) controllerObj).getRatingType();
    }

    public static long getFlags(java.lang.Object controllerObj) {
        return ((android.media.session.MediaController) controllerObj).getFlags();
    }

    public static java.lang.Object getPlaybackInfo(java.lang.Object controllerObj) {
        return ((android.media.session.MediaController) controllerObj).getPlaybackInfo();
    }

    public static android.app.PendingIntent getSessionActivity(java.lang.Object controllerObj) {
        return ((android.media.session.MediaController) controllerObj).getSessionActivity();
    }

    public static boolean dispatchMediaButtonEvent(java.lang.Object controllerObj, android.view.KeyEvent event) {
        return ((android.media.session.MediaController) controllerObj).dispatchMediaButtonEvent(event);
    }

    public static void setVolumeTo(java.lang.Object controllerObj, int value, int flags) {
        ((android.media.session.MediaController) controllerObj).setVolumeTo(value, flags);
    }

    public static void adjustVolume(java.lang.Object controllerObj, int direction, int flags) {
        ((android.media.session.MediaController) controllerObj).adjustVolume(direction, flags);
    }

    public static void sendCommand(java.lang.Object controllerObj, java.lang.String command, android.os.Bundle params, android.os.ResultReceiver cb) {
        ((android.media.session.MediaController) controllerObj).sendCommand(command, params, cb);
    }

    public static java.lang.String getPackageName(java.lang.Object controllerObj) {
        return ((android.media.session.MediaController) controllerObj).getPackageName();
    }
}
