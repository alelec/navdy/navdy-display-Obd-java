package android.support.v4.media.session;

public class MediaButtonReceiver extends android.content.BroadcastReceiver {
    private static final java.lang.String TAG = "MediaButtonReceiver";

    public void onReceive(android.content.Context context, android.content.Intent intent) {
        android.content.Intent queryIntent = new android.content.Intent("android.intent.action.MEDIA_BUTTON");
        queryIntent.setPackage(context.getPackageName());
        android.content.pm.PackageManager pm = context.getPackageManager();
        java.util.List<android.content.pm.ResolveInfo> resolveInfos = pm.queryIntentServices(queryIntent, 0);
        if (resolveInfos.isEmpty()) {
            queryIntent.setAction(android.support.v4.media.MediaBrowserServiceCompat.SERVICE_INTERFACE);
            resolveInfos = pm.queryIntentServices(queryIntent, 0);
        }
        if (resolveInfos.isEmpty()) {
            throw new java.lang.IllegalStateException("Could not find any Service that handles android.intent.action.MEDIA_BUTTON or a media browser service implementation");
        } else if (resolveInfos.size() != 1) {
            throw new java.lang.IllegalStateException("Expected 1 Service that handles " + queryIntent.getAction() + ", found " + resolveInfos.size());
        } else {
            android.content.pm.ResolveInfo resolveInfo = (android.content.pm.ResolveInfo) resolveInfos.get(0);
            intent.setComponent(new android.content.ComponentName(resolveInfo.serviceInfo.packageName, resolveInfo.serviceInfo.name));
            context.startService(intent);
        }
    }

    public static android.view.KeyEvent handleIntent(android.support.v4.media.session.MediaSessionCompat mediaSessionCompat, android.content.Intent intent) {
        if (mediaSessionCompat == null || intent == null || !"android.intent.action.MEDIA_BUTTON".equals(intent.getAction()) || !intent.hasExtra("android.intent.extra.KEY_EVENT")) {
            return null;
        }
        android.view.KeyEvent ke = (android.view.KeyEvent) intent.getParcelableExtra("android.intent.extra.KEY_EVENT");
        mediaSessionCompat.getController().dispatchMediaButtonEvent(ke);
        return ke;
    }

    public static android.app.PendingIntent buildMediaButtonPendingIntent(android.content.Context context, long action) {
        android.content.ComponentName mbrComponent = getMediaButtonReceiverComponent(context);
        if (mbrComponent != null) {
            return buildMediaButtonPendingIntent(context, mbrComponent, action);
        }
        android.util.Log.w(TAG, "A unique media button receiver could not be found in the given context, so couldn't build a pending intent.");
        return null;
    }

    public static android.app.PendingIntent buildMediaButtonPendingIntent(android.content.Context context, android.content.ComponentName mbrComponent, long action) {
        if (mbrComponent == null) {
            android.util.Log.w(TAG, "The component name of media button receiver should be provided.");
            return null;
        }
        int keyCode = android.support.v4.media.session.PlaybackStateCompat.toKeyCode(action);
        if (keyCode == 0) {
            android.util.Log.w(TAG, "Cannot build a media button pending intent with the given action: " + action);
            return null;
        }
        android.content.Intent intent = new android.content.Intent("android.intent.action.MEDIA_BUTTON");
        intent.setComponent(mbrComponent);
        intent.putExtra("android.intent.extra.KEY_EVENT", new android.view.KeyEvent(0, keyCode));
        return android.app.PendingIntent.getBroadcast(context, keyCode, intent, 0);
    }

    static android.content.ComponentName getMediaButtonReceiverComponent(android.content.Context context) {
        android.content.Intent queryIntent = new android.content.Intent("android.intent.action.MEDIA_BUTTON");
        queryIntent.setPackage(context.getPackageName());
        java.util.List<android.content.pm.ResolveInfo> resolveInfos = context.getPackageManager().queryBroadcastReceivers(queryIntent, 0);
        if (resolveInfos.size() == 1) {
            android.content.pm.ResolveInfo resolveInfo = (android.content.pm.ResolveInfo) resolveInfos.get(0);
            return new android.content.ComponentName(resolveInfo.activityInfo.packageName, resolveInfo.activityInfo.name);
        }
        if (resolveInfos.size() > 1) {
            android.util.Log.w(TAG, "More than one BroadcastReceiver that handles android.intent.action.MEDIA_BUTTON was found, returning null.");
        }
        return null;
    }
}
