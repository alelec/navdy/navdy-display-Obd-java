package android.support.v4.media.session;

@android.annotation.TargetApi(18)
@android.support.annotation.RequiresApi(18)
class MediaSessionCompatApi18 {
    private static final long ACTION_SEEK_TO = 256;
    private static final java.lang.String TAG = "MediaSessionCompatApi18";
    private static boolean sIsMbrPendingIntentSupported = true;

    interface Callback {
        void onSeekTo(long j);
    }

    static class OnPlaybackPositionUpdateListener<T extends android.support.v4.media.session.MediaSessionCompatApi18.Callback> implements android.media.RemoteControlClient.OnPlaybackPositionUpdateListener {
        protected final T mCallback;

        public OnPlaybackPositionUpdateListener(T callback) {
            this.mCallback = callback;
        }

        public void onPlaybackPositionUpdate(long newPositionMs) {
            this.mCallback.onSeekTo(newPositionMs);
        }
    }

    MediaSessionCompatApi18() {
    }

    public static java.lang.Object createPlaybackPositionUpdateListener(android.support.v4.media.session.MediaSessionCompatApi18.Callback callback) {
        return new android.support.v4.media.session.MediaSessionCompatApi18.OnPlaybackPositionUpdateListener(callback);
    }

    public static void registerMediaButtonEventReceiver(android.content.Context context, android.app.PendingIntent pi, android.content.ComponentName cn) {
        android.media.AudioManager am = (android.media.AudioManager) context.getSystemService("audio");
        if (sIsMbrPendingIntentSupported) {
            try {
                am.registerMediaButtonEventReceiver(pi);
            } catch (java.lang.NullPointerException e) {
                android.util.Log.w(TAG, "Unable to register media button event receiver with PendingIntent, falling back to ComponentName.");
                sIsMbrPendingIntentSupported = false;
            }
        }
        if (!sIsMbrPendingIntentSupported) {
            am.registerMediaButtonEventReceiver(cn);
        }
    }

    public static void unregisterMediaButtonEventReceiver(android.content.Context context, android.app.PendingIntent pi, android.content.ComponentName cn) {
        android.media.AudioManager am = (android.media.AudioManager) context.getSystemService("audio");
        if (sIsMbrPendingIntentSupported) {
            am.unregisterMediaButtonEventReceiver(pi);
        } else {
            am.unregisterMediaButtonEventReceiver(cn);
        }
    }

    public static void setState(java.lang.Object rccObj, int state, long position, float speed, long updateTime) {
        long currTime = android.os.SystemClock.elapsedRealtime();
        if (state == 3 && position > 0) {
            long diff = 0;
            if (updateTime > 0) {
                diff = currTime - updateTime;
                if (speed > 0.0f && speed != 1.0f) {
                    diff = (long) (((float) diff) * speed);
                }
            }
            position += diff;
        }
        ((android.media.RemoteControlClient) rccObj).setPlaybackState(android.support.v4.media.session.MediaSessionCompatApi14.getRccStateFromState(state), position, speed);
    }

    public static void setTransportControlFlags(java.lang.Object rccObj, long actions) {
        ((android.media.RemoteControlClient) rccObj).setTransportControlFlags(getRccTransportControlFlagsFromActions(actions));
    }

    public static void setOnPlaybackPositionUpdateListener(java.lang.Object rccObj, java.lang.Object onPositionUpdateObj) {
        ((android.media.RemoteControlClient) rccObj).setPlaybackPositionUpdateListener((android.media.RemoteControlClient.OnPlaybackPositionUpdateListener) onPositionUpdateObj);
    }

    static int getRccTransportControlFlagsFromActions(long actions) {
        int transportControlFlags = android.support.v4.media.session.MediaSessionCompatApi14.getRccTransportControlFlagsFromActions(actions);
        if ((256 & actions) != 0) {
            return transportControlFlags | 256;
        }
        return transportControlFlags;
    }
}
