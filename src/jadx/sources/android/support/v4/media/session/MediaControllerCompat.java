package android.support.v4.media.session;

public final class MediaControllerCompat {
    static final java.lang.String COMMAND_ADD_QUEUE_ITEM = "android.support.v4.media.session.command.ADD_QUEUE_ITEM";
    static final java.lang.String COMMAND_ADD_QUEUE_ITEM_AT = "android.support.v4.media.session.command.ADD_QUEUE_ITEM_AT";
    static final java.lang.String COMMAND_ARGUMENT_INDEX = "android.support.v4.media.session.command.ARGUMENT_INDEX";
    static final java.lang.String COMMAND_ARGUMENT_MEDIA_DESCRIPTION = "android.support.v4.media.session.command.ARGUMENT_MEDIA_DESCRIPTION";
    static final java.lang.String COMMAND_GET_EXTRA_BINDER = "android.support.v4.media.session.command.GET_EXTRA_BINDER";
    static final java.lang.String COMMAND_REMOVE_QUEUE_ITEM = "android.support.v4.media.session.command.REMOVE_QUEUE_ITEM";
    static final java.lang.String COMMAND_REMOVE_QUEUE_ITEM_AT = "android.support.v4.media.session.command.REMOVE_QUEUE_ITEM_AT";
    static final java.lang.String TAG = "MediaControllerCompat";
    private final android.support.v4.media.session.MediaControllerCompat.MediaControllerImpl mImpl;
    private final android.support.v4.media.session.MediaSessionCompat.Token mToken;

    public static abstract class Callback implements android.os.IBinder.DeathRecipient {
        /* access modifiers changed from: private */
        public final java.lang.Object mCallbackObj;
        android.support.v4.media.session.MediaControllerCompat.Callback.MessageHandler mHandler;
        boolean mHasExtraCallback;
        boolean mRegistered = false;

        private class MessageHandler extends android.os.Handler {
            private static final int MSG_DESTROYED = 8;
            private static final int MSG_EVENT = 1;
            private static final int MSG_UPDATE_EXTRAS = 7;
            private static final int MSG_UPDATE_METADATA = 3;
            private static final int MSG_UPDATE_PLAYBACK_STATE = 2;
            private static final int MSG_UPDATE_QUEUE = 5;
            private static final int MSG_UPDATE_QUEUE_TITLE = 6;
            private static final int MSG_UPDATE_REPEAT_MODE = 9;
            private static final int MSG_UPDATE_SHUFFLE_MODE = 10;
            private static final int MSG_UPDATE_VOLUME = 4;

            public MessageHandler(android.os.Looper looper) {
                super(looper);
            }

            public void handleMessage(android.os.Message msg) {
                if (android.support.v4.media.session.MediaControllerCompat.Callback.this.mRegistered) {
                    switch (msg.what) {
                        case 1:
                            android.support.v4.media.session.MediaControllerCompat.Callback.this.onSessionEvent((java.lang.String) msg.obj, msg.getData());
                            return;
                        case 2:
                            android.support.v4.media.session.MediaControllerCompat.Callback.this.onPlaybackStateChanged((android.support.v4.media.session.PlaybackStateCompat) msg.obj);
                            return;
                        case 3:
                            android.support.v4.media.session.MediaControllerCompat.Callback.this.onMetadataChanged((android.support.v4.media.MediaMetadataCompat) msg.obj);
                            return;
                        case 4:
                            android.support.v4.media.session.MediaControllerCompat.Callback.this.onAudioInfoChanged((android.support.v4.media.session.MediaControllerCompat.PlaybackInfo) msg.obj);
                            return;
                        case 5:
                            android.support.v4.media.session.MediaControllerCompat.Callback.this.onQueueChanged((java.util.List) msg.obj);
                            return;
                        case 6:
                            android.support.v4.media.session.MediaControllerCompat.Callback.this.onQueueTitleChanged((java.lang.CharSequence) msg.obj);
                            return;
                        case 7:
                            android.support.v4.media.session.MediaControllerCompat.Callback.this.onExtrasChanged((android.os.Bundle) msg.obj);
                            return;
                        case 8:
                            android.support.v4.media.session.MediaControllerCompat.Callback.this.onSessionDestroyed();
                            return;
                        case 9:
                            android.support.v4.media.session.MediaControllerCompat.Callback.this.onRepeatModeChanged(((java.lang.Integer) msg.obj).intValue());
                            return;
                        case 10:
                            android.support.v4.media.session.MediaControllerCompat.Callback.this.onShuffleModeChanged(((java.lang.Boolean) msg.obj).booleanValue());
                            return;
                        default:
                            return;
                    }
                }
            }

            public void post(int what, java.lang.Object obj, android.os.Bundle data) {
                android.os.Message msg = obtainMessage(what, obj);
                msg.setData(data);
                msg.sendToTarget();
            }
        }

        private class StubApi21 implements android.support.v4.media.session.MediaControllerCompatApi21.Callback {
            StubApi21() {
            }

            public void onSessionDestroyed() {
                android.support.v4.media.session.MediaControllerCompat.Callback.this.onSessionDestroyed();
            }

            public void onSessionEvent(java.lang.String event, android.os.Bundle extras) {
                if (!android.support.v4.media.session.MediaControllerCompat.Callback.this.mHasExtraCallback || android.os.Build.VERSION.SDK_INT >= 23) {
                    android.support.v4.media.session.MediaControllerCompat.Callback.this.onSessionEvent(event, extras);
                }
            }

            public void onPlaybackStateChanged(java.lang.Object stateObj) {
                if (!android.support.v4.media.session.MediaControllerCompat.Callback.this.mHasExtraCallback) {
                    android.support.v4.media.session.MediaControllerCompat.Callback.this.onPlaybackStateChanged(android.support.v4.media.session.PlaybackStateCompat.fromPlaybackState(stateObj));
                }
            }

            public void onMetadataChanged(java.lang.Object metadataObj) {
                android.support.v4.media.session.MediaControllerCompat.Callback.this.onMetadataChanged(android.support.v4.media.MediaMetadataCompat.fromMediaMetadata(metadataObj));
            }

            public void onQueueChanged(java.util.List<?> queue) {
                android.support.v4.media.session.MediaControllerCompat.Callback.this.onQueueChanged(android.support.v4.media.session.MediaSessionCompat.QueueItem.fromQueueItemList(queue));
            }

            public void onQueueTitleChanged(java.lang.CharSequence title) {
                android.support.v4.media.session.MediaControllerCompat.Callback.this.onQueueTitleChanged(title);
            }

            public void onExtrasChanged(android.os.Bundle extras) {
                android.support.v4.media.session.MediaControllerCompat.Callback.this.onExtrasChanged(extras);
            }

            public void onAudioInfoChanged(int type, int stream, int control, int max, int current) {
                android.support.v4.media.session.MediaControllerCompat.Callback.this.onAudioInfoChanged(new android.support.v4.media.session.MediaControllerCompat.PlaybackInfo(type, stream, control, max, current));
            }
        }

        private class StubCompat extends android.support.v4.media.session.IMediaControllerCallback.Stub {
            StubCompat() {
            }

            public void onEvent(java.lang.String event, android.os.Bundle extras) throws android.os.RemoteException {
                android.support.v4.media.session.MediaControllerCompat.Callback.this.mHandler.post(1, event, extras);
            }

            public void onSessionDestroyed() throws android.os.RemoteException {
                android.support.v4.media.session.MediaControllerCompat.Callback.this.mHandler.post(8, null, null);
            }

            public void onPlaybackStateChanged(android.support.v4.media.session.PlaybackStateCompat state) throws android.os.RemoteException {
                android.support.v4.media.session.MediaControllerCompat.Callback.this.mHandler.post(2, state, null);
            }

            public void onMetadataChanged(android.support.v4.media.MediaMetadataCompat metadata) throws android.os.RemoteException {
                android.support.v4.media.session.MediaControllerCompat.Callback.this.mHandler.post(3, metadata, null);
            }

            public void onQueueChanged(java.util.List<android.support.v4.media.session.MediaSessionCompat.QueueItem> queue) throws android.os.RemoteException {
                android.support.v4.media.session.MediaControllerCompat.Callback.this.mHandler.post(5, queue, null);
            }

            public void onQueueTitleChanged(java.lang.CharSequence title) throws android.os.RemoteException {
                android.support.v4.media.session.MediaControllerCompat.Callback.this.mHandler.post(6, title, null);
            }

            public void onRepeatModeChanged(int repeatMode) throws android.os.RemoteException {
                android.support.v4.media.session.MediaControllerCompat.Callback.this.mHandler.post(9, java.lang.Integer.valueOf(repeatMode), null);
            }

            public void onShuffleModeChanged(boolean enabled) throws android.os.RemoteException {
                android.support.v4.media.session.MediaControllerCompat.Callback.this.mHandler.post(10, java.lang.Boolean.valueOf(enabled), null);
            }

            public void onExtrasChanged(android.os.Bundle extras) throws android.os.RemoteException {
                android.support.v4.media.session.MediaControllerCompat.Callback.this.mHandler.post(7, extras, null);
            }

            public void onVolumeInfoChanged(android.support.v4.media.session.ParcelableVolumeInfo info) throws android.os.RemoteException {
                android.support.v4.media.session.MediaControllerCompat.PlaybackInfo pi = null;
                if (info != null) {
                    pi = new android.support.v4.media.session.MediaControllerCompat.PlaybackInfo(info.volumeType, info.audioStream, info.controlType, info.maxVolume, info.currentVolume);
                }
                android.support.v4.media.session.MediaControllerCompat.Callback.this.mHandler.post(4, pi, null);
            }
        }

        public Callback() {
            if (android.os.Build.VERSION.SDK_INT >= 21) {
                this.mCallbackObj = android.support.v4.media.session.MediaControllerCompatApi21.createCallback(new android.support.v4.media.session.MediaControllerCompat.Callback.StubApi21());
            } else {
                this.mCallbackObj = new android.support.v4.media.session.MediaControllerCompat.Callback.StubCompat();
            }
        }

        public void onSessionDestroyed() {
        }

        public void onSessionEvent(java.lang.String event, android.os.Bundle extras) {
        }

        public void onPlaybackStateChanged(android.support.v4.media.session.PlaybackStateCompat state) {
        }

        public void onMetadataChanged(android.support.v4.media.MediaMetadataCompat metadata) {
        }

        public void onQueueChanged(java.util.List<android.support.v4.media.session.MediaSessionCompat.QueueItem> list) {
        }

        public void onQueueTitleChanged(java.lang.CharSequence title) {
        }

        public void onExtrasChanged(android.os.Bundle extras) {
        }

        public void onAudioInfoChanged(android.support.v4.media.session.MediaControllerCompat.PlaybackInfo info) {
        }

        public void onRepeatModeChanged(int repeatMode) {
        }

        public void onShuffleModeChanged(boolean enabled) {
        }

        public void binderDied() {
            onSessionDestroyed();
        }

        /* access modifiers changed from: private */
        public void setHandler(android.os.Handler handler) {
            this.mHandler = new android.support.v4.media.session.MediaControllerCompat.Callback.MessageHandler(handler.getLooper());
        }
    }

    private static class MediaControllerExtraData extends android.support.v4.app.SupportActivity.ExtraData {
        private final android.support.v4.media.session.MediaControllerCompat mMediaController;

        MediaControllerExtraData(android.support.v4.media.session.MediaControllerCompat mediaController) {
            this.mMediaController = mediaController;
        }

        /* access modifiers changed from: 0000 */
        public android.support.v4.media.session.MediaControllerCompat getMediaController() {
            return this.mMediaController;
        }
    }

    interface MediaControllerImpl {
        void addQueueItem(android.support.v4.media.MediaDescriptionCompat mediaDescriptionCompat);

        void addQueueItem(android.support.v4.media.MediaDescriptionCompat mediaDescriptionCompat, int i);

        void adjustVolume(int i, int i2);

        boolean dispatchMediaButtonEvent(android.view.KeyEvent keyEvent);

        android.os.Bundle getExtras();

        long getFlags();

        java.lang.Object getMediaController();

        android.support.v4.media.MediaMetadataCompat getMetadata();

        java.lang.String getPackageName();

        android.support.v4.media.session.MediaControllerCompat.PlaybackInfo getPlaybackInfo();

        android.support.v4.media.session.PlaybackStateCompat getPlaybackState();

        java.util.List<android.support.v4.media.session.MediaSessionCompat.QueueItem> getQueue();

        java.lang.CharSequence getQueueTitle();

        int getRatingType();

        int getRepeatMode();

        android.app.PendingIntent getSessionActivity();

        android.support.v4.media.session.MediaControllerCompat.TransportControls getTransportControls();

        boolean isShuffleModeEnabled();

        void registerCallback(android.support.v4.media.session.MediaControllerCompat.Callback callback, android.os.Handler handler);

        void removeQueueItem(android.support.v4.media.MediaDescriptionCompat mediaDescriptionCompat);

        void removeQueueItemAt(int i);

        void sendCommand(java.lang.String str, android.os.Bundle bundle, android.os.ResultReceiver resultReceiver);

        void setVolumeTo(int i, int i2);

        void unregisterCallback(android.support.v4.media.session.MediaControllerCompat.Callback callback);
    }

    static class MediaControllerImplApi21 implements android.support.v4.media.session.MediaControllerCompat.MediaControllerImpl {
        private java.util.HashMap<android.support.v4.media.session.MediaControllerCompat.Callback, android.support.v4.media.session.MediaControllerCompat.MediaControllerImplApi21.ExtraCallback> mCallbackMap = new java.util.HashMap<>();
        protected final java.lang.Object mControllerObj;
        /* access modifiers changed from: private */
        public android.support.v4.media.session.IMediaSession mExtraBinder;
        private java.util.List<android.support.v4.media.session.MediaControllerCompat.Callback> mPendingCallbacks = new java.util.ArrayList();

        private static class ExtraBinderRequestResultReceiver extends android.os.ResultReceiver {
            private java.lang.ref.WeakReference<android.support.v4.media.session.MediaControllerCompat.MediaControllerImplApi21> mMediaControllerImpl;

            public ExtraBinderRequestResultReceiver(android.support.v4.media.session.MediaControllerCompat.MediaControllerImplApi21 mediaControllerImpl, android.os.Handler handler) {
                super(handler);
                this.mMediaControllerImpl = new java.lang.ref.WeakReference<>(mediaControllerImpl);
            }

            /* access modifiers changed from: protected */
            public void onReceiveResult(int resultCode, android.os.Bundle resultData) {
                android.support.v4.media.session.MediaControllerCompat.MediaControllerImplApi21 mediaControllerImpl = (android.support.v4.media.session.MediaControllerCompat.MediaControllerImplApi21) this.mMediaControllerImpl.get();
                if (mediaControllerImpl != null && resultData != null) {
                    mediaControllerImpl.mExtraBinder = android.support.v4.media.session.IMediaSession.Stub.asInterface(android.support.v4.app.BundleCompat.getBinder(resultData, "android.support.v4.media.session.EXTRA_BINDER"));
                    mediaControllerImpl.processPendingCallbacks();
                }
            }
        }

        private class ExtraCallback extends android.support.v4.media.session.IMediaControllerCallback.Stub {
            /* access modifiers changed from: private */
            public android.support.v4.media.session.MediaControllerCompat.Callback mCallback;

            ExtraCallback(android.support.v4.media.session.MediaControllerCompat.Callback callback) {
                this.mCallback = callback;
            }

            public void onEvent(final java.lang.String event, final android.os.Bundle extras) throws android.os.RemoteException {
                this.mCallback.mHandler.post(new java.lang.Runnable() {
                    public void run() {
                        android.support.v4.media.session.MediaControllerCompat.MediaControllerImplApi21.ExtraCallback.this.mCallback.onSessionEvent(event, extras);
                    }
                });
            }

            public void onSessionDestroyed() throws android.os.RemoteException {
                throw new java.lang.AssertionError();
            }

            public void onPlaybackStateChanged(final android.support.v4.media.session.PlaybackStateCompat state) throws android.os.RemoteException {
                this.mCallback.mHandler.post(new java.lang.Runnable() {
                    public void run() {
                        android.support.v4.media.session.MediaControllerCompat.MediaControllerImplApi21.ExtraCallback.this.mCallback.onPlaybackStateChanged(state);
                    }
                });
            }

            public void onMetadataChanged(android.support.v4.media.MediaMetadataCompat metadata) throws android.os.RemoteException {
                throw new java.lang.AssertionError();
            }

            public void onQueueChanged(java.util.List<android.support.v4.media.session.MediaSessionCompat.QueueItem> list) throws android.os.RemoteException {
                throw new java.lang.AssertionError();
            }

            public void onQueueTitleChanged(java.lang.CharSequence title) throws android.os.RemoteException {
                throw new java.lang.AssertionError();
            }

            public void onRepeatModeChanged(final int repeatMode) throws android.os.RemoteException {
                this.mCallback.mHandler.post(new java.lang.Runnable() {
                    public void run() {
                        android.support.v4.media.session.MediaControllerCompat.MediaControllerImplApi21.ExtraCallback.this.mCallback.onRepeatModeChanged(repeatMode);
                    }
                });
            }

            public void onShuffleModeChanged(final boolean enabled) throws android.os.RemoteException {
                this.mCallback.mHandler.post(new java.lang.Runnable() {
                    public void run() {
                        android.support.v4.media.session.MediaControllerCompat.MediaControllerImplApi21.ExtraCallback.this.mCallback.onShuffleModeChanged(enabled);
                    }
                });
            }

            public void onExtrasChanged(android.os.Bundle extras) throws android.os.RemoteException {
                throw new java.lang.AssertionError();
            }

            public void onVolumeInfoChanged(android.support.v4.media.session.ParcelableVolumeInfo info) throws android.os.RemoteException {
                throw new java.lang.AssertionError();
            }
        }

        public MediaControllerImplApi21(android.content.Context context, android.support.v4.media.session.MediaSessionCompat session) {
            this.mControllerObj = android.support.v4.media.session.MediaControllerCompatApi21.fromToken(context, session.getSessionToken().getToken());
            requestExtraBinder();
        }

        public MediaControllerImplApi21(android.content.Context context, android.support.v4.media.session.MediaSessionCompat.Token sessionToken) throws android.os.RemoteException {
            this.mControllerObj = android.support.v4.media.session.MediaControllerCompatApi21.fromToken(context, sessionToken.getToken());
            if (this.mControllerObj == null) {
                throw new android.os.RemoteException();
            }
            requestExtraBinder();
        }

        public final void registerCallback(android.support.v4.media.session.MediaControllerCompat.Callback callback, android.os.Handler handler) {
            android.support.v4.media.session.MediaControllerCompatApi21.registerCallback(this.mControllerObj, callback.mCallbackObj, handler);
            if (this.mExtraBinder != null) {
                callback.setHandler(handler);
                android.support.v4.media.session.MediaControllerCompat.MediaControllerImplApi21.ExtraCallback extraCallback = new android.support.v4.media.session.MediaControllerCompat.MediaControllerImplApi21.ExtraCallback(callback);
                this.mCallbackMap.put(callback, extraCallback);
                callback.mHasExtraCallback = true;
                try {
                    this.mExtraBinder.registerCallbackListener(extraCallback);
                } catch (android.os.RemoteException e) {
                    android.util.Log.e(android.support.v4.media.session.MediaControllerCompat.TAG, "Dead object in registerCallback.", e);
                }
            } else {
                callback.setHandler(handler);
                synchronized (this.mPendingCallbacks) {
                    this.mPendingCallbacks.add(callback);
                }
            }
        }

        public final void unregisterCallback(android.support.v4.media.session.MediaControllerCompat.Callback callback) {
            android.support.v4.media.session.MediaControllerCompatApi21.unregisterCallback(this.mControllerObj, callback.mCallbackObj);
            if (this.mExtraBinder != null) {
                try {
                    android.support.v4.media.session.MediaControllerCompat.MediaControllerImplApi21.ExtraCallback extraCallback = (android.support.v4.media.session.MediaControllerCompat.MediaControllerImplApi21.ExtraCallback) this.mCallbackMap.remove(callback);
                    if (extraCallback != null) {
                        this.mExtraBinder.unregisterCallbackListener(extraCallback);
                    }
                } catch (android.os.RemoteException e) {
                    android.util.Log.e(android.support.v4.media.session.MediaControllerCompat.TAG, "Dead object in unregisterCallback.", e);
                }
            } else {
                synchronized (this.mPendingCallbacks) {
                    this.mPendingCallbacks.remove(callback);
                }
            }
        }

        public boolean dispatchMediaButtonEvent(android.view.KeyEvent event) {
            return android.support.v4.media.session.MediaControllerCompatApi21.dispatchMediaButtonEvent(this.mControllerObj, event);
        }

        public android.support.v4.media.session.MediaControllerCompat.TransportControls getTransportControls() {
            java.lang.Object controlsObj = android.support.v4.media.session.MediaControllerCompatApi21.getTransportControls(this.mControllerObj);
            if (controlsObj != null) {
                return new android.support.v4.media.session.MediaControllerCompat.TransportControlsApi21(controlsObj);
            }
            return null;
        }

        public android.support.v4.media.session.PlaybackStateCompat getPlaybackState() {
            if (this.mExtraBinder != null) {
                try {
                    return this.mExtraBinder.getPlaybackState();
                } catch (android.os.RemoteException e) {
                    android.util.Log.e(android.support.v4.media.session.MediaControllerCompat.TAG, "Dead object in getPlaybackState.", e);
                }
            }
            java.lang.Object stateObj = android.support.v4.media.session.MediaControllerCompatApi21.getPlaybackState(this.mControllerObj);
            if (stateObj != null) {
                return android.support.v4.media.session.PlaybackStateCompat.fromPlaybackState(stateObj);
            }
            return null;
        }

        public android.support.v4.media.MediaMetadataCompat getMetadata() {
            java.lang.Object metadataObj = android.support.v4.media.session.MediaControllerCompatApi21.getMetadata(this.mControllerObj);
            if (metadataObj != null) {
                return android.support.v4.media.MediaMetadataCompat.fromMediaMetadata(metadataObj);
            }
            return null;
        }

        public java.util.List<android.support.v4.media.session.MediaSessionCompat.QueueItem> getQueue() {
            java.util.List<java.lang.Object> queueObjs = android.support.v4.media.session.MediaControllerCompatApi21.getQueue(this.mControllerObj);
            if (queueObjs != null) {
                return android.support.v4.media.session.MediaSessionCompat.QueueItem.fromQueueItemList(queueObjs);
            }
            return null;
        }

        public void addQueueItem(android.support.v4.media.MediaDescriptionCompat description) {
            if ((4 & getFlags()) == 0) {
                throw new java.lang.UnsupportedOperationException("This session doesn't support queue management operations");
            }
            android.os.Bundle params = new android.os.Bundle();
            params.putParcelable(android.support.v4.media.session.MediaControllerCompat.COMMAND_ARGUMENT_MEDIA_DESCRIPTION, description);
            sendCommand(android.support.v4.media.session.MediaControllerCompat.COMMAND_ADD_QUEUE_ITEM, params, null);
        }

        public void addQueueItem(android.support.v4.media.MediaDescriptionCompat description, int index) {
            if ((4 & getFlags()) == 0) {
                throw new java.lang.UnsupportedOperationException("This session doesn't support queue management operations");
            }
            android.os.Bundle params = new android.os.Bundle();
            params.putParcelable(android.support.v4.media.session.MediaControllerCompat.COMMAND_ARGUMENT_MEDIA_DESCRIPTION, description);
            params.putInt(android.support.v4.media.session.MediaControllerCompat.COMMAND_ARGUMENT_INDEX, index);
            sendCommand(android.support.v4.media.session.MediaControllerCompat.COMMAND_ADD_QUEUE_ITEM_AT, params, null);
        }

        public void removeQueueItem(android.support.v4.media.MediaDescriptionCompat description) {
            if ((4 & getFlags()) == 0) {
                throw new java.lang.UnsupportedOperationException("This session doesn't support queue management operations");
            }
            android.os.Bundle params = new android.os.Bundle();
            params.putParcelable(android.support.v4.media.session.MediaControllerCompat.COMMAND_ARGUMENT_MEDIA_DESCRIPTION, description);
            sendCommand(android.support.v4.media.session.MediaControllerCompat.COMMAND_REMOVE_QUEUE_ITEM, params, null);
        }

        public void removeQueueItemAt(int index) {
            if ((4 & getFlags()) == 0) {
                throw new java.lang.UnsupportedOperationException("This session doesn't support queue management operations");
            }
            android.os.Bundle params = new android.os.Bundle();
            params.putInt(android.support.v4.media.session.MediaControllerCompat.COMMAND_ARGUMENT_INDEX, index);
            sendCommand(android.support.v4.media.session.MediaControllerCompat.COMMAND_REMOVE_QUEUE_ITEM_AT, params, null);
        }

        public java.lang.CharSequence getQueueTitle() {
            return android.support.v4.media.session.MediaControllerCompatApi21.getQueueTitle(this.mControllerObj);
        }

        public android.os.Bundle getExtras() {
            return android.support.v4.media.session.MediaControllerCompatApi21.getExtras(this.mControllerObj);
        }

        public int getRatingType() {
            if (android.os.Build.VERSION.SDK_INT < 22 && this.mExtraBinder != null) {
                try {
                    return this.mExtraBinder.getRatingType();
                } catch (android.os.RemoteException e) {
                    android.util.Log.e(android.support.v4.media.session.MediaControllerCompat.TAG, "Dead object in getRatingType.", e);
                }
            }
            return android.support.v4.media.session.MediaControllerCompatApi21.getRatingType(this.mControllerObj);
        }

        public int getRepeatMode() {
            if (this.mExtraBinder != null) {
                try {
                    return this.mExtraBinder.getRepeatMode();
                } catch (android.os.RemoteException e) {
                    android.util.Log.e(android.support.v4.media.session.MediaControllerCompat.TAG, "Dead object in getRepeatMode.", e);
                }
            }
            return 0;
        }

        public boolean isShuffleModeEnabled() {
            if (this.mExtraBinder != null) {
                try {
                    return this.mExtraBinder.isShuffleModeEnabled();
                } catch (android.os.RemoteException e) {
                    android.util.Log.e(android.support.v4.media.session.MediaControllerCompat.TAG, "Dead object in isShuffleModeEnabled.", e);
                }
            }
            return false;
        }

        public long getFlags() {
            return android.support.v4.media.session.MediaControllerCompatApi21.getFlags(this.mControllerObj);
        }

        public android.support.v4.media.session.MediaControllerCompat.PlaybackInfo getPlaybackInfo() {
            java.lang.Object volumeInfoObj = android.support.v4.media.session.MediaControllerCompatApi21.getPlaybackInfo(this.mControllerObj);
            if (volumeInfoObj != null) {
                return new android.support.v4.media.session.MediaControllerCompat.PlaybackInfo(android.support.v4.media.session.MediaControllerCompatApi21.PlaybackInfo.getPlaybackType(volumeInfoObj), android.support.v4.media.session.MediaControllerCompatApi21.PlaybackInfo.getLegacyAudioStream(volumeInfoObj), android.support.v4.media.session.MediaControllerCompatApi21.PlaybackInfo.getVolumeControl(volumeInfoObj), android.support.v4.media.session.MediaControllerCompatApi21.PlaybackInfo.getMaxVolume(volumeInfoObj), android.support.v4.media.session.MediaControllerCompatApi21.PlaybackInfo.getCurrentVolume(volumeInfoObj));
            }
            return null;
        }

        public android.app.PendingIntent getSessionActivity() {
            return android.support.v4.media.session.MediaControllerCompatApi21.getSessionActivity(this.mControllerObj);
        }

        public void setVolumeTo(int value, int flags) {
            android.support.v4.media.session.MediaControllerCompatApi21.setVolumeTo(this.mControllerObj, value, flags);
        }

        public void adjustVolume(int direction, int flags) {
            android.support.v4.media.session.MediaControllerCompatApi21.adjustVolume(this.mControllerObj, direction, flags);
        }

        public void sendCommand(java.lang.String command, android.os.Bundle params, android.os.ResultReceiver cb) {
            android.support.v4.media.session.MediaControllerCompatApi21.sendCommand(this.mControllerObj, command, params, cb);
        }

        public java.lang.String getPackageName() {
            return android.support.v4.media.session.MediaControllerCompatApi21.getPackageName(this.mControllerObj);
        }

        public java.lang.Object getMediaController() {
            return this.mControllerObj;
        }

        private void requestExtraBinder() {
            sendCommand(android.support.v4.media.session.MediaControllerCompat.COMMAND_GET_EXTRA_BINDER, null, new android.support.v4.media.session.MediaControllerCompat.MediaControllerImplApi21.ExtraBinderRequestResultReceiver(this, new android.os.Handler()));
        }

        /* access modifiers changed from: private */
        public void processPendingCallbacks() {
            if (this.mExtraBinder != null) {
                synchronized (this.mPendingCallbacks) {
                    for (android.support.v4.media.session.MediaControllerCompat.Callback callback : this.mPendingCallbacks) {
                        android.support.v4.media.session.MediaControllerCompat.MediaControllerImplApi21.ExtraCallback extraCallback = new android.support.v4.media.session.MediaControllerCompat.MediaControllerImplApi21.ExtraCallback(callback);
                        this.mCallbackMap.put(callback, extraCallback);
                        callback.mHasExtraCallback = true;
                        try {
                            this.mExtraBinder.registerCallbackListener(extraCallback);
                        } catch (android.os.RemoteException e) {
                            android.util.Log.e(android.support.v4.media.session.MediaControllerCompat.TAG, "Dead object in registerCallback.", e);
                        }
                    }
                    this.mPendingCallbacks.clear();
                }
            }
        }
    }

    static class MediaControllerImplApi23 extends android.support.v4.media.session.MediaControllerCompat.MediaControllerImplApi21 {
        public MediaControllerImplApi23(android.content.Context context, android.support.v4.media.session.MediaSessionCompat session) {
            super(context, session);
        }

        public MediaControllerImplApi23(android.content.Context context, android.support.v4.media.session.MediaSessionCompat.Token sessionToken) throws android.os.RemoteException {
            super(context, sessionToken);
        }

        public android.support.v4.media.session.MediaControllerCompat.TransportControls getTransportControls() {
            java.lang.Object controlsObj = android.support.v4.media.session.MediaControllerCompatApi21.getTransportControls(this.mControllerObj);
            if (controlsObj != null) {
                return new android.support.v4.media.session.MediaControllerCompat.TransportControlsApi23(controlsObj);
            }
            return null;
        }
    }

    static class MediaControllerImplApi24 extends android.support.v4.media.session.MediaControllerCompat.MediaControllerImplApi23 {
        public MediaControllerImplApi24(android.content.Context context, android.support.v4.media.session.MediaSessionCompat session) {
            super(context, session);
        }

        public MediaControllerImplApi24(android.content.Context context, android.support.v4.media.session.MediaSessionCompat.Token sessionToken) throws android.os.RemoteException {
            super(context, sessionToken);
        }

        public android.support.v4.media.session.MediaControllerCompat.TransportControls getTransportControls() {
            java.lang.Object controlsObj = android.support.v4.media.session.MediaControllerCompatApi21.getTransportControls(this.mControllerObj);
            if (controlsObj != null) {
                return new android.support.v4.media.session.MediaControllerCompat.TransportControlsApi24(controlsObj);
            }
            return null;
        }
    }

    static class MediaControllerImplBase implements android.support.v4.media.session.MediaControllerCompat.MediaControllerImpl {
        private android.support.v4.media.session.IMediaSession mBinder;
        private android.support.v4.media.session.MediaSessionCompat.Token mToken;
        private android.support.v4.media.session.MediaControllerCompat.TransportControls mTransportControls;

        public MediaControllerImplBase(android.support.v4.media.session.MediaSessionCompat.Token token) {
            this.mToken = token;
            this.mBinder = android.support.v4.media.session.IMediaSession.Stub.asInterface((android.os.IBinder) token.getToken());
        }

        public void registerCallback(android.support.v4.media.session.MediaControllerCompat.Callback callback, android.os.Handler handler) {
            if (callback == null) {
                throw new java.lang.IllegalArgumentException("callback may not be null.");
            }
            try {
                this.mBinder.asBinder().linkToDeath(callback, 0);
                this.mBinder.registerCallbackListener((android.support.v4.media.session.IMediaControllerCallback) callback.mCallbackObj);
                callback.setHandler(handler);
                callback.mRegistered = true;
            } catch (android.os.RemoteException e) {
                android.util.Log.e(android.support.v4.media.session.MediaControllerCompat.TAG, "Dead object in registerCallback.", e);
                callback.onSessionDestroyed();
            }
        }

        public void unregisterCallback(android.support.v4.media.session.MediaControllerCompat.Callback callback) {
            if (callback == null) {
                throw new java.lang.IllegalArgumentException("callback may not be null.");
            }
            try {
                this.mBinder.unregisterCallbackListener((android.support.v4.media.session.IMediaControllerCallback) callback.mCallbackObj);
                this.mBinder.asBinder().unlinkToDeath(callback, 0);
                callback.mRegistered = false;
            } catch (android.os.RemoteException e) {
                android.util.Log.e(android.support.v4.media.session.MediaControllerCompat.TAG, "Dead object in unregisterCallback.", e);
            }
        }

        public boolean dispatchMediaButtonEvent(android.view.KeyEvent event) {
            if (event == null) {
                throw new java.lang.IllegalArgumentException("event may not be null.");
            }
            try {
                this.mBinder.sendMediaButton(event);
            } catch (android.os.RemoteException e) {
                android.util.Log.e(android.support.v4.media.session.MediaControllerCompat.TAG, "Dead object in dispatchMediaButtonEvent.", e);
            }
            return false;
        }

        public android.support.v4.media.session.MediaControllerCompat.TransportControls getTransportControls() {
            if (this.mTransportControls == null) {
                this.mTransportControls = new android.support.v4.media.session.MediaControllerCompat.TransportControlsBase(this.mBinder);
            }
            return this.mTransportControls;
        }

        public android.support.v4.media.session.PlaybackStateCompat getPlaybackState() {
            try {
                return this.mBinder.getPlaybackState();
            } catch (android.os.RemoteException e) {
                android.util.Log.e(android.support.v4.media.session.MediaControllerCompat.TAG, "Dead object in getPlaybackState.", e);
                return null;
            }
        }

        public android.support.v4.media.MediaMetadataCompat getMetadata() {
            try {
                return this.mBinder.getMetadata();
            } catch (android.os.RemoteException e) {
                android.util.Log.e(android.support.v4.media.session.MediaControllerCompat.TAG, "Dead object in getMetadata.", e);
                return null;
            }
        }

        public java.util.List<android.support.v4.media.session.MediaSessionCompat.QueueItem> getQueue() {
            try {
                return this.mBinder.getQueue();
            } catch (android.os.RemoteException e) {
                android.util.Log.e(android.support.v4.media.session.MediaControllerCompat.TAG, "Dead object in getQueue.", e);
                return null;
            }
        }

        public void addQueueItem(android.support.v4.media.MediaDescriptionCompat description) {
            try {
                if ((4 & this.mBinder.getFlags()) == 0) {
                    throw new java.lang.UnsupportedOperationException("This session doesn't support queue management operations");
                }
                this.mBinder.addQueueItem(description);
            } catch (android.os.RemoteException e) {
                android.util.Log.e(android.support.v4.media.session.MediaControllerCompat.TAG, "Dead object in addQueueItem.", e);
            }
        }

        public void addQueueItem(android.support.v4.media.MediaDescriptionCompat description, int index) {
            try {
                if ((4 & this.mBinder.getFlags()) == 0) {
                    throw new java.lang.UnsupportedOperationException("This session doesn't support queue management operations");
                }
                this.mBinder.addQueueItemAt(description, index);
            } catch (android.os.RemoteException e) {
                android.util.Log.e(android.support.v4.media.session.MediaControllerCompat.TAG, "Dead object in addQueueItemAt.", e);
            }
        }

        public void removeQueueItem(android.support.v4.media.MediaDescriptionCompat description) {
            try {
                if ((4 & this.mBinder.getFlags()) == 0) {
                    throw new java.lang.UnsupportedOperationException("This session doesn't support queue management operations");
                }
                this.mBinder.removeQueueItem(description);
            } catch (android.os.RemoteException e) {
                android.util.Log.e(android.support.v4.media.session.MediaControllerCompat.TAG, "Dead object in removeQueueItem.", e);
            }
        }

        public void removeQueueItemAt(int index) {
            try {
                if ((4 & this.mBinder.getFlags()) == 0) {
                    throw new java.lang.UnsupportedOperationException("This session doesn't support queue management operations");
                }
                this.mBinder.removeQueueItemAt(index);
            } catch (android.os.RemoteException e) {
                android.util.Log.e(android.support.v4.media.session.MediaControllerCompat.TAG, "Dead object in removeQueueItemAt.", e);
            }
        }

        public java.lang.CharSequence getQueueTitle() {
            try {
                return this.mBinder.getQueueTitle();
            } catch (android.os.RemoteException e) {
                android.util.Log.e(android.support.v4.media.session.MediaControllerCompat.TAG, "Dead object in getQueueTitle.", e);
                return null;
            }
        }

        public android.os.Bundle getExtras() {
            try {
                return this.mBinder.getExtras();
            } catch (android.os.RemoteException e) {
                android.util.Log.e(android.support.v4.media.session.MediaControllerCompat.TAG, "Dead object in getExtras.", e);
                return null;
            }
        }

        public int getRatingType() {
            try {
                return this.mBinder.getRatingType();
            } catch (android.os.RemoteException e) {
                android.util.Log.e(android.support.v4.media.session.MediaControllerCompat.TAG, "Dead object in getRatingType.", e);
                return 0;
            }
        }

        public int getRepeatMode() {
            try {
                return this.mBinder.getRepeatMode();
            } catch (android.os.RemoteException e) {
                android.util.Log.e(android.support.v4.media.session.MediaControllerCompat.TAG, "Dead object in getRepeatMode.", e);
                return 0;
            }
        }

        public boolean isShuffleModeEnabled() {
            try {
                return this.mBinder.isShuffleModeEnabled();
            } catch (android.os.RemoteException e) {
                android.util.Log.e(android.support.v4.media.session.MediaControllerCompat.TAG, "Dead object in isShuffleModeEnabled.", e);
                return false;
            }
        }

        public long getFlags() {
            try {
                return this.mBinder.getFlags();
            } catch (android.os.RemoteException e) {
                android.util.Log.e(android.support.v4.media.session.MediaControllerCompat.TAG, "Dead object in getFlags.", e);
                return 0;
            }
        }

        public android.support.v4.media.session.MediaControllerCompat.PlaybackInfo getPlaybackInfo() {
            try {
                android.support.v4.media.session.ParcelableVolumeInfo info = this.mBinder.getVolumeAttributes();
                return new android.support.v4.media.session.MediaControllerCompat.PlaybackInfo(info.volumeType, info.audioStream, info.controlType, info.maxVolume, info.currentVolume);
            } catch (android.os.RemoteException e) {
                android.util.Log.e(android.support.v4.media.session.MediaControllerCompat.TAG, "Dead object in getPlaybackInfo.", e);
                return null;
            }
        }

        public android.app.PendingIntent getSessionActivity() {
            try {
                return this.mBinder.getLaunchPendingIntent();
            } catch (android.os.RemoteException e) {
                android.util.Log.e(android.support.v4.media.session.MediaControllerCompat.TAG, "Dead object in getSessionActivity.", e);
                return null;
            }
        }

        public void setVolumeTo(int value, int flags) {
            try {
                this.mBinder.setVolumeTo(value, flags, null);
            } catch (android.os.RemoteException e) {
                android.util.Log.e(android.support.v4.media.session.MediaControllerCompat.TAG, "Dead object in setVolumeTo.", e);
            }
        }

        public void adjustVolume(int direction, int flags) {
            try {
                this.mBinder.adjustVolume(direction, flags, null);
            } catch (android.os.RemoteException e) {
                android.util.Log.e(android.support.v4.media.session.MediaControllerCompat.TAG, "Dead object in adjustVolume.", e);
            }
        }

        public void sendCommand(java.lang.String command, android.os.Bundle params, android.os.ResultReceiver cb) {
            try {
                this.mBinder.sendCommand(command, params, new android.support.v4.media.session.MediaSessionCompat.ResultReceiverWrapper(cb));
            } catch (android.os.RemoteException e) {
                android.util.Log.e(android.support.v4.media.session.MediaControllerCompat.TAG, "Dead object in sendCommand.", e);
            }
        }

        public java.lang.String getPackageName() {
            try {
                return this.mBinder.getPackageName();
            } catch (android.os.RemoteException e) {
                android.util.Log.e(android.support.v4.media.session.MediaControllerCompat.TAG, "Dead object in getPackageName.", e);
                return null;
            }
        }

        public java.lang.Object getMediaController() {
            return null;
        }
    }

    public static final class PlaybackInfo {
        public static final int PLAYBACK_TYPE_LOCAL = 1;
        public static final int PLAYBACK_TYPE_REMOTE = 2;
        private final int mAudioStream;
        private final int mCurrentVolume;
        private final int mMaxVolume;
        private final int mPlaybackType;
        private final int mVolumeControl;

        PlaybackInfo(int type, int stream, int control, int max, int current) {
            this.mPlaybackType = type;
            this.mAudioStream = stream;
            this.mVolumeControl = control;
            this.mMaxVolume = max;
            this.mCurrentVolume = current;
        }

        public int getPlaybackType() {
            return this.mPlaybackType;
        }

        public int getAudioStream() {
            return this.mAudioStream;
        }

        public int getVolumeControl() {
            return this.mVolumeControl;
        }

        public int getMaxVolume() {
            return this.mMaxVolume;
        }

        public int getCurrentVolume() {
            return this.mCurrentVolume;
        }
    }

    public static abstract class TransportControls {
        public abstract void fastForward();

        public abstract void pause();

        public abstract void play();

        public abstract void playFromMediaId(java.lang.String str, android.os.Bundle bundle);

        public abstract void playFromSearch(java.lang.String str, android.os.Bundle bundle);

        public abstract void playFromUri(android.net.Uri uri, android.os.Bundle bundle);

        public abstract void prepare();

        public abstract void prepareFromMediaId(java.lang.String str, android.os.Bundle bundle);

        public abstract void prepareFromSearch(java.lang.String str, android.os.Bundle bundle);

        public abstract void prepareFromUri(android.net.Uri uri, android.os.Bundle bundle);

        public abstract void rewind();

        public abstract void seekTo(long j);

        public abstract void sendCustomAction(android.support.v4.media.session.PlaybackStateCompat.CustomAction customAction, android.os.Bundle bundle);

        public abstract void sendCustomAction(java.lang.String str, android.os.Bundle bundle);

        public abstract void setRating(android.support.v4.media.RatingCompat ratingCompat);

        public abstract void setRepeatMode(int i);

        public abstract void setShuffleModeEnabled(boolean z);

        public abstract void skipToNext();

        public abstract void skipToPrevious();

        public abstract void skipToQueueItem(long j);

        public abstract void stop();

        TransportControls() {
        }
    }

    static class TransportControlsApi21 extends android.support.v4.media.session.MediaControllerCompat.TransportControls {
        protected final java.lang.Object mControlsObj;

        public TransportControlsApi21(java.lang.Object controlsObj) {
            this.mControlsObj = controlsObj;
        }

        public void prepare() {
            sendCustomAction("android.support.v4.media.session.action.PREPARE", (android.os.Bundle) null);
        }

        public void prepareFromMediaId(java.lang.String mediaId, android.os.Bundle extras) {
            android.os.Bundle bundle = new android.os.Bundle();
            bundle.putString("android.support.v4.media.session.action.ARGUMENT_MEDIA_ID", mediaId);
            bundle.putBundle("android.support.v4.media.session.action.ARGUMENT_EXTRAS", extras);
            sendCustomAction("android.support.v4.media.session.action.PREPARE_FROM_MEDIA_ID", bundle);
        }

        public void prepareFromSearch(java.lang.String query, android.os.Bundle extras) {
            android.os.Bundle bundle = new android.os.Bundle();
            bundle.putString("android.support.v4.media.session.action.ARGUMENT_QUERY", query);
            bundle.putBundle("android.support.v4.media.session.action.ARGUMENT_EXTRAS", extras);
            sendCustomAction("android.support.v4.media.session.action.PREPARE_FROM_SEARCH", bundle);
        }

        public void prepareFromUri(android.net.Uri uri, android.os.Bundle extras) {
            android.os.Bundle bundle = new android.os.Bundle();
            bundle.putParcelable("android.support.v4.media.session.action.ARGUMENT_URI", uri);
            bundle.putBundle("android.support.v4.media.session.action.ARGUMENT_EXTRAS", extras);
            sendCustomAction("android.support.v4.media.session.action.PREPARE_FROM_URI", bundle);
        }

        public void play() {
            android.support.v4.media.session.MediaControllerCompatApi21.TransportControls.play(this.mControlsObj);
        }

        public void pause() {
            android.support.v4.media.session.MediaControllerCompatApi21.TransportControls.pause(this.mControlsObj);
        }

        public void stop() {
            android.support.v4.media.session.MediaControllerCompatApi21.TransportControls.stop(this.mControlsObj);
        }

        public void seekTo(long pos) {
            android.support.v4.media.session.MediaControllerCompatApi21.TransportControls.seekTo(this.mControlsObj, pos);
        }

        public void fastForward() {
            android.support.v4.media.session.MediaControllerCompatApi21.TransportControls.fastForward(this.mControlsObj);
        }

        public void rewind() {
            android.support.v4.media.session.MediaControllerCompatApi21.TransportControls.rewind(this.mControlsObj);
        }

        public void skipToNext() {
            android.support.v4.media.session.MediaControllerCompatApi21.TransportControls.skipToNext(this.mControlsObj);
        }

        public void skipToPrevious() {
            android.support.v4.media.session.MediaControllerCompatApi21.TransportControls.skipToPrevious(this.mControlsObj);
        }

        public void setRating(android.support.v4.media.RatingCompat rating) {
            android.support.v4.media.session.MediaControllerCompatApi21.TransportControls.setRating(this.mControlsObj, rating != null ? rating.getRating() : null);
        }

        public void setRepeatMode(int repeatMode) {
            android.os.Bundle bundle = new android.os.Bundle();
            bundle.putInt("android.support.v4.media.session.action.ARGUMENT_REPEAT_MODE", repeatMode);
            sendCustomAction("android.support.v4.media.session.action.SET_REPEAT_MODE", bundle);
        }

        public void setShuffleModeEnabled(boolean enabled) {
            android.os.Bundle bundle = new android.os.Bundle();
            bundle.putBoolean("android.support.v4.media.session.action.ARGUMENT_SHUFFLE_MODE_ENABLED", enabled);
            sendCustomAction("android.support.v4.media.session.action.SET_SHUFFLE_MODE_ENABLED", bundle);
        }

        public void playFromMediaId(java.lang.String mediaId, android.os.Bundle extras) {
            android.support.v4.media.session.MediaControllerCompatApi21.TransportControls.playFromMediaId(this.mControlsObj, mediaId, extras);
        }

        public void playFromSearch(java.lang.String query, android.os.Bundle extras) {
            android.support.v4.media.session.MediaControllerCompatApi21.TransportControls.playFromSearch(this.mControlsObj, query, extras);
        }

        public void playFromUri(android.net.Uri uri, android.os.Bundle extras) {
            if (uri == null || android.net.Uri.EMPTY.equals(uri)) {
                throw new java.lang.IllegalArgumentException("You must specify a non-empty Uri for playFromUri.");
            }
            android.os.Bundle bundle = new android.os.Bundle();
            bundle.putParcelable("android.support.v4.media.session.action.ARGUMENT_URI", uri);
            bundle.putParcelable("android.support.v4.media.session.action.ARGUMENT_EXTRAS", extras);
            sendCustomAction("android.support.v4.media.session.action.PLAY_FROM_URI", bundle);
        }

        public void skipToQueueItem(long id) {
            android.support.v4.media.session.MediaControllerCompatApi21.TransportControls.skipToQueueItem(this.mControlsObj, id);
        }

        public void sendCustomAction(android.support.v4.media.session.PlaybackStateCompat.CustomAction customAction, android.os.Bundle args) {
            android.support.v4.media.session.MediaControllerCompatApi21.TransportControls.sendCustomAction(this.mControlsObj, customAction.getAction(), args);
        }

        public void sendCustomAction(java.lang.String action, android.os.Bundle args) {
            android.support.v4.media.session.MediaControllerCompatApi21.TransportControls.sendCustomAction(this.mControlsObj, action, args);
        }
    }

    static class TransportControlsApi23 extends android.support.v4.media.session.MediaControllerCompat.TransportControlsApi21 {
        public TransportControlsApi23(java.lang.Object controlsObj) {
            super(controlsObj);
        }

        public void playFromUri(android.net.Uri uri, android.os.Bundle extras) {
            android.support.v4.media.session.MediaControllerCompatApi23.TransportControls.playFromUri(this.mControlsObj, uri, extras);
        }
    }

    static class TransportControlsApi24 extends android.support.v4.media.session.MediaControllerCompat.TransportControlsApi23 {
        public TransportControlsApi24(java.lang.Object controlsObj) {
            super(controlsObj);
        }

        public void prepare() {
            android.support.v4.media.session.MediaControllerCompatApi24.TransportControls.prepare(this.mControlsObj);
        }

        public void prepareFromMediaId(java.lang.String mediaId, android.os.Bundle extras) {
            android.support.v4.media.session.MediaControllerCompatApi24.TransportControls.prepareFromMediaId(this.mControlsObj, mediaId, extras);
        }

        public void prepareFromSearch(java.lang.String query, android.os.Bundle extras) {
            android.support.v4.media.session.MediaControllerCompatApi24.TransportControls.prepareFromSearch(this.mControlsObj, query, extras);
        }

        public void prepareFromUri(android.net.Uri uri, android.os.Bundle extras) {
            android.support.v4.media.session.MediaControllerCompatApi24.TransportControls.prepareFromUri(this.mControlsObj, uri, extras);
        }
    }

    static class TransportControlsBase extends android.support.v4.media.session.MediaControllerCompat.TransportControls {
        private android.support.v4.media.session.IMediaSession mBinder;

        public TransportControlsBase(android.support.v4.media.session.IMediaSession binder) {
            this.mBinder = binder;
        }

        public void prepare() {
            try {
                this.mBinder.prepare();
            } catch (android.os.RemoteException e) {
                android.util.Log.e(android.support.v4.media.session.MediaControllerCompat.TAG, "Dead object in prepare.", e);
            }
        }

        public void prepareFromMediaId(java.lang.String mediaId, android.os.Bundle extras) {
            try {
                this.mBinder.prepareFromMediaId(mediaId, extras);
            } catch (android.os.RemoteException e) {
                android.util.Log.e(android.support.v4.media.session.MediaControllerCompat.TAG, "Dead object in prepareFromMediaId.", e);
            }
        }

        public void prepareFromSearch(java.lang.String query, android.os.Bundle extras) {
            try {
                this.mBinder.prepareFromSearch(query, extras);
            } catch (android.os.RemoteException e) {
                android.util.Log.e(android.support.v4.media.session.MediaControllerCompat.TAG, "Dead object in prepareFromSearch.", e);
            }
        }

        public void prepareFromUri(android.net.Uri uri, android.os.Bundle extras) {
            try {
                this.mBinder.prepareFromUri(uri, extras);
            } catch (android.os.RemoteException e) {
                android.util.Log.e(android.support.v4.media.session.MediaControllerCompat.TAG, "Dead object in prepareFromUri.", e);
            }
        }

        public void play() {
            try {
                this.mBinder.play();
            } catch (android.os.RemoteException e) {
                android.util.Log.e(android.support.v4.media.session.MediaControllerCompat.TAG, "Dead object in play.", e);
            }
        }

        public void playFromMediaId(java.lang.String mediaId, android.os.Bundle extras) {
            try {
                this.mBinder.playFromMediaId(mediaId, extras);
            } catch (android.os.RemoteException e) {
                android.util.Log.e(android.support.v4.media.session.MediaControllerCompat.TAG, "Dead object in playFromMediaId.", e);
            }
        }

        public void playFromSearch(java.lang.String query, android.os.Bundle extras) {
            try {
                this.mBinder.playFromSearch(query, extras);
            } catch (android.os.RemoteException e) {
                android.util.Log.e(android.support.v4.media.session.MediaControllerCompat.TAG, "Dead object in playFromSearch.", e);
            }
        }

        public void playFromUri(android.net.Uri uri, android.os.Bundle extras) {
            try {
                this.mBinder.playFromUri(uri, extras);
            } catch (android.os.RemoteException e) {
                android.util.Log.e(android.support.v4.media.session.MediaControllerCompat.TAG, "Dead object in playFromUri.", e);
            }
        }

        public void skipToQueueItem(long id) {
            try {
                this.mBinder.skipToQueueItem(id);
            } catch (android.os.RemoteException e) {
                android.util.Log.e(android.support.v4.media.session.MediaControllerCompat.TAG, "Dead object in skipToQueueItem.", e);
            }
        }

        public void pause() {
            try {
                this.mBinder.pause();
            } catch (android.os.RemoteException e) {
                android.util.Log.e(android.support.v4.media.session.MediaControllerCompat.TAG, "Dead object in pause.", e);
            }
        }

        public void stop() {
            try {
                this.mBinder.stop();
            } catch (android.os.RemoteException e) {
                android.util.Log.e(android.support.v4.media.session.MediaControllerCompat.TAG, "Dead object in stop.", e);
            }
        }

        public void seekTo(long pos) {
            try {
                this.mBinder.seekTo(pos);
            } catch (android.os.RemoteException e) {
                android.util.Log.e(android.support.v4.media.session.MediaControllerCompat.TAG, "Dead object in seekTo.", e);
            }
        }

        public void fastForward() {
            try {
                this.mBinder.fastForward();
            } catch (android.os.RemoteException e) {
                android.util.Log.e(android.support.v4.media.session.MediaControllerCompat.TAG, "Dead object in fastForward.", e);
            }
        }

        public void skipToNext() {
            try {
                this.mBinder.next();
            } catch (android.os.RemoteException e) {
                android.util.Log.e(android.support.v4.media.session.MediaControllerCompat.TAG, "Dead object in skipToNext.", e);
            }
        }

        public void rewind() {
            try {
                this.mBinder.rewind();
            } catch (android.os.RemoteException e) {
                android.util.Log.e(android.support.v4.media.session.MediaControllerCompat.TAG, "Dead object in rewind.", e);
            }
        }

        public void skipToPrevious() {
            try {
                this.mBinder.previous();
            } catch (android.os.RemoteException e) {
                android.util.Log.e(android.support.v4.media.session.MediaControllerCompat.TAG, "Dead object in skipToPrevious.", e);
            }
        }

        public void setRating(android.support.v4.media.RatingCompat rating) {
            try {
                this.mBinder.rate(rating);
            } catch (android.os.RemoteException e) {
                android.util.Log.e(android.support.v4.media.session.MediaControllerCompat.TAG, "Dead object in setRating.", e);
            }
        }

        public void setRepeatMode(int repeatMode) {
            try {
                this.mBinder.setRepeatMode(repeatMode);
            } catch (android.os.RemoteException e) {
                android.util.Log.e(android.support.v4.media.session.MediaControllerCompat.TAG, "Dead object in setRepeatMode.", e);
            }
        }

        public void setShuffleModeEnabled(boolean enabled) {
            try {
                this.mBinder.setShuffleModeEnabled(enabled);
            } catch (android.os.RemoteException e) {
                android.util.Log.e(android.support.v4.media.session.MediaControllerCompat.TAG, "Dead object in setShuffleModeEnabled.", e);
            }
        }

        public void sendCustomAction(android.support.v4.media.session.PlaybackStateCompat.CustomAction customAction, android.os.Bundle args) {
            sendCustomAction(customAction.getAction(), args);
        }

        public void sendCustomAction(java.lang.String action, android.os.Bundle args) {
            try {
                this.mBinder.sendCustomAction(action, args);
            } catch (android.os.RemoteException e) {
                android.util.Log.e(android.support.v4.media.session.MediaControllerCompat.TAG, "Dead object in sendCustomAction.", e);
            }
        }
    }

    public static void setMediaController(android.app.Activity activity, android.support.v4.media.session.MediaControllerCompat mediaController) {
        if (activity instanceof android.support.v4.app.SupportActivity) {
            ((android.support.v4.app.SupportActivity) activity).putExtraData(new android.support.v4.media.session.MediaControllerCompat.MediaControllerExtraData(mediaController));
        }
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            java.lang.Object controllerObj = null;
            if (mediaController != null) {
                controllerObj = android.support.v4.media.session.MediaControllerCompatApi21.fromToken(activity, mediaController.getSessionToken().getToken());
            }
            android.support.v4.media.session.MediaControllerCompatApi21.setMediaController(activity, controllerObj);
        }
    }

    public static android.support.v4.media.session.MediaControllerCompat getMediaController(android.app.Activity activity) {
        if (activity instanceof android.support.v4.app.SupportActivity) {
            android.support.v4.media.session.MediaControllerCompat.MediaControllerExtraData extraData = (android.support.v4.media.session.MediaControllerCompat.MediaControllerExtraData) ((android.support.v4.app.SupportActivity) activity).getExtraData(android.support.v4.media.session.MediaControllerCompat.MediaControllerExtraData.class);
            if (extraData != null) {
                return extraData.getMediaController();
            }
            return null;
        } else if (android.os.Build.VERSION.SDK_INT < 21) {
            return null;
        } else {
            java.lang.Object controllerObj = android.support.v4.media.session.MediaControllerCompatApi21.getMediaController(activity);
            if (controllerObj == null) {
                return null;
            }
            try {
                return new android.support.v4.media.session.MediaControllerCompat((android.content.Context) activity, android.support.v4.media.session.MediaSessionCompat.Token.fromToken(android.support.v4.media.session.MediaControllerCompatApi21.getSessionToken(controllerObj)));
            } catch (android.os.RemoteException e) {
                android.util.Log.e(TAG, "Dead object in getMediaController.", e);
                return null;
            }
        }
    }

    public MediaControllerCompat(android.content.Context context, android.support.v4.media.session.MediaSessionCompat session) {
        if (session == null) {
            throw new java.lang.IllegalArgumentException("session must not be null");
        }
        this.mToken = session.getSessionToken();
        if (android.os.Build.VERSION.SDK_INT >= 24) {
            this.mImpl = new android.support.v4.media.session.MediaControllerCompat.MediaControllerImplApi24(context, session);
        } else if (android.os.Build.VERSION.SDK_INT >= 23) {
            this.mImpl = new android.support.v4.media.session.MediaControllerCompat.MediaControllerImplApi23(context, session);
        } else if (android.os.Build.VERSION.SDK_INT >= 21) {
            this.mImpl = new android.support.v4.media.session.MediaControllerCompat.MediaControllerImplApi21(context, session);
        } else {
            this.mImpl = new android.support.v4.media.session.MediaControllerCompat.MediaControllerImplBase(this.mToken);
        }
    }

    public MediaControllerCompat(android.content.Context context, android.support.v4.media.session.MediaSessionCompat.Token sessionToken) throws android.os.RemoteException {
        if (sessionToken == null) {
            throw new java.lang.IllegalArgumentException("sessionToken must not be null");
        }
        this.mToken = sessionToken;
        if (android.os.Build.VERSION.SDK_INT >= 24) {
            this.mImpl = new android.support.v4.media.session.MediaControllerCompat.MediaControllerImplApi24(context, sessionToken);
        } else if (android.os.Build.VERSION.SDK_INT >= 23) {
            this.mImpl = new android.support.v4.media.session.MediaControllerCompat.MediaControllerImplApi23(context, sessionToken);
        } else if (android.os.Build.VERSION.SDK_INT >= 21) {
            this.mImpl = new android.support.v4.media.session.MediaControllerCompat.MediaControllerImplApi21(context, sessionToken);
        } else {
            this.mImpl = new android.support.v4.media.session.MediaControllerCompat.MediaControllerImplBase(this.mToken);
        }
    }

    public android.support.v4.media.session.MediaControllerCompat.TransportControls getTransportControls() {
        return this.mImpl.getTransportControls();
    }

    public boolean dispatchMediaButtonEvent(android.view.KeyEvent keyEvent) {
        if (keyEvent != null) {
            return this.mImpl.dispatchMediaButtonEvent(keyEvent);
        }
        throw new java.lang.IllegalArgumentException("KeyEvent may not be null");
    }

    public android.support.v4.media.session.PlaybackStateCompat getPlaybackState() {
        return this.mImpl.getPlaybackState();
    }

    public android.support.v4.media.MediaMetadataCompat getMetadata() {
        return this.mImpl.getMetadata();
    }

    public java.util.List<android.support.v4.media.session.MediaSessionCompat.QueueItem> getQueue() {
        return this.mImpl.getQueue();
    }

    public void addQueueItem(android.support.v4.media.MediaDescriptionCompat description) {
        this.mImpl.addQueueItem(description);
    }

    public void addQueueItem(android.support.v4.media.MediaDescriptionCompat description, int index) {
        this.mImpl.addQueueItem(description, index);
    }

    public void removeQueueItem(android.support.v4.media.MediaDescriptionCompat description) {
        this.mImpl.removeQueueItem(description);
    }

    public void removeQueueItemAt(int index) {
        this.mImpl.removeQueueItemAt(index);
    }

    public java.lang.CharSequence getQueueTitle() {
        return this.mImpl.getQueueTitle();
    }

    public android.os.Bundle getExtras() {
        return this.mImpl.getExtras();
    }

    public int getRatingType() {
        return this.mImpl.getRatingType();
    }

    public int getRepeatMode() {
        return this.mImpl.getRepeatMode();
    }

    public boolean isShuffleModeEnabled() {
        return this.mImpl.isShuffleModeEnabled();
    }

    public long getFlags() {
        return this.mImpl.getFlags();
    }

    public android.support.v4.media.session.MediaControllerCompat.PlaybackInfo getPlaybackInfo() {
        return this.mImpl.getPlaybackInfo();
    }

    public android.app.PendingIntent getSessionActivity() {
        return this.mImpl.getSessionActivity();
    }

    public android.support.v4.media.session.MediaSessionCompat.Token getSessionToken() {
        return this.mToken;
    }

    public void setVolumeTo(int value, int flags) {
        this.mImpl.setVolumeTo(value, flags);
    }

    public void adjustVolume(int direction, int flags) {
        this.mImpl.adjustVolume(direction, flags);
    }

    public void registerCallback(android.support.v4.media.session.MediaControllerCompat.Callback callback) {
        registerCallback(callback, null);
    }

    public void registerCallback(android.support.v4.media.session.MediaControllerCompat.Callback callback, android.os.Handler handler) {
        if (callback == null) {
            throw new java.lang.IllegalArgumentException("callback cannot be null");
        }
        if (handler == null) {
            handler = new android.os.Handler();
        }
        this.mImpl.registerCallback(callback, handler);
    }

    public void unregisterCallback(android.support.v4.media.session.MediaControllerCompat.Callback callback) {
        if (callback == null) {
            throw new java.lang.IllegalArgumentException("callback cannot be null");
        }
        this.mImpl.unregisterCallback(callback);
    }

    public void sendCommand(java.lang.String command, android.os.Bundle params, android.os.ResultReceiver cb) {
        if (android.text.TextUtils.isEmpty(command)) {
            throw new java.lang.IllegalArgumentException("command cannot be null or empty");
        }
        this.mImpl.sendCommand(command, params, cb);
    }

    public java.lang.String getPackageName() {
        return this.mImpl.getPackageName();
    }

    /* access modifiers changed from: 0000 */
    @android.support.annotation.VisibleForTesting
    public boolean isExtraBinderReady() {
        if (!(this.mImpl instanceof android.support.v4.media.session.MediaControllerCompat.MediaControllerImplApi21)) {
            return false;
        }
        if (((android.support.v4.media.session.MediaControllerCompat.MediaControllerImplApi21) this.mImpl).mExtraBinder != null) {
            return true;
        }
        return false;
    }

    public java.lang.Object getMediaController() {
        return this.mImpl.getMediaController();
    }
}
