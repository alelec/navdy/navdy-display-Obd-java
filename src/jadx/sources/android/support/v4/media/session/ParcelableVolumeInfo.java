package android.support.v4.media.session;

public class ParcelableVolumeInfo implements android.os.Parcelable {
    public static final android.os.Parcelable.Creator<android.support.v4.media.session.ParcelableVolumeInfo> CREATOR = new android.os.Parcelable.Creator<android.support.v4.media.session.ParcelableVolumeInfo>() {
        public android.support.v4.media.session.ParcelableVolumeInfo createFromParcel(android.os.Parcel in) {
            return new android.support.v4.media.session.ParcelableVolumeInfo(in);
        }

        public android.support.v4.media.session.ParcelableVolumeInfo[] newArray(int size) {
            return new android.support.v4.media.session.ParcelableVolumeInfo[size];
        }
    };
    public int audioStream;
    public int controlType;
    public int currentVolume;
    public int maxVolume;
    public int volumeType;

    public ParcelableVolumeInfo(int volumeType2, int audioStream2, int controlType2, int maxVolume2, int currentVolume2) {
        this.volumeType = volumeType2;
        this.audioStream = audioStream2;
        this.controlType = controlType2;
        this.maxVolume = maxVolume2;
        this.currentVolume = currentVolume2;
    }

    public ParcelableVolumeInfo(android.os.Parcel from) {
        this.volumeType = from.readInt();
        this.controlType = from.readInt();
        this.maxVolume = from.readInt();
        this.currentVolume = from.readInt();
        this.audioStream = from.readInt();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeInt(this.volumeType);
        dest.writeInt(this.controlType);
        dest.writeInt(this.maxVolume);
        dest.writeInt(this.currentVolume);
        dest.writeInt(this.audioStream);
    }
}
