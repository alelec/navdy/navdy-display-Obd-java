package android.support.v4.media.session;

@android.annotation.TargetApi(23)
@android.support.annotation.RequiresApi(23)
class MediaControllerCompatApi23 {

    public static class TransportControls extends android.support.v4.media.session.MediaControllerCompatApi21.TransportControls {
        public static void playFromUri(java.lang.Object controlsObj, android.net.Uri uri, android.os.Bundle extras) {
            ((android.media.session.MediaController.TransportControls) controlsObj).playFromUri(uri, extras);
        }
    }

    MediaControllerCompatApi23() {
    }
}
