package android.support.v4.media.session;

public final class PlaybackStateCompat implements android.os.Parcelable {
    public static final long ACTION_FAST_FORWARD = 64;
    public static final long ACTION_PAUSE = 2;
    public static final long ACTION_PLAY = 4;
    public static final long ACTION_PLAY_FROM_MEDIA_ID = 1024;
    public static final long ACTION_PLAY_FROM_SEARCH = 2048;
    public static final long ACTION_PLAY_FROM_URI = 8192;
    public static final long ACTION_PLAY_PAUSE = 512;
    public static final long ACTION_PREPARE = 16384;
    public static final long ACTION_PREPARE_FROM_MEDIA_ID = 32768;
    public static final long ACTION_PREPARE_FROM_SEARCH = 65536;
    public static final long ACTION_PREPARE_FROM_URI = 131072;
    public static final long ACTION_REWIND = 8;
    public static final long ACTION_SEEK_TO = 256;
    public static final long ACTION_SET_RATING = 128;
    public static final long ACTION_SET_REPEAT_MODE = 262144;
    public static final long ACTION_SET_SHUFFLE_MODE_ENABLED = 524288;
    public static final long ACTION_SKIP_TO_NEXT = 32;
    public static final long ACTION_SKIP_TO_PREVIOUS = 16;
    public static final long ACTION_SKIP_TO_QUEUE_ITEM = 4096;
    public static final long ACTION_STOP = 1;
    public static final android.os.Parcelable.Creator<android.support.v4.media.session.PlaybackStateCompat> CREATOR = new android.os.Parcelable.Creator<android.support.v4.media.session.PlaybackStateCompat>() {
        public android.support.v4.media.session.PlaybackStateCompat createFromParcel(android.os.Parcel in) {
            return new android.support.v4.media.session.PlaybackStateCompat(in);
        }

        public android.support.v4.media.session.PlaybackStateCompat[] newArray(int size) {
            return new android.support.v4.media.session.PlaybackStateCompat[size];
        }
    };
    public static final int ERROR_CODE_ACTION_ABORTED = 10;
    public static final int ERROR_CODE_APP_ERROR = 1;
    public static final int ERROR_CODE_AUTHENTICATION_EXPIRED = 3;
    public static final int ERROR_CODE_CONCURRENT_STREAM_LIMIT = 5;
    public static final int ERROR_CODE_CONTENT_ALREADY_PLAYING = 8;
    public static final int ERROR_CODE_END_OF_QUEUE = 11;
    public static final int ERROR_CODE_NOT_AVAILABLE_IN_REGION = 7;
    public static final int ERROR_CODE_NOT_SUPPORTED = 2;
    public static final int ERROR_CODE_PARENTAL_CONTROL_RESTRICTED = 6;
    public static final int ERROR_CODE_PREMIUM_ACCOUNT_REQUIRED = 4;
    public static final int ERROR_CODE_SKIP_LIMIT_REACHED = 9;
    public static final int ERROR_CODE_UNKNOWN_ERROR = 0;
    private static final int KEYCODE_MEDIA_PAUSE = 127;
    private static final int KEYCODE_MEDIA_PLAY = 126;
    public static final long PLAYBACK_POSITION_UNKNOWN = -1;
    public static final int REPEAT_MODE_ALL = 2;
    public static final int REPEAT_MODE_NONE = 0;
    public static final int REPEAT_MODE_ONE = 1;
    public static final int STATE_BUFFERING = 6;
    public static final int STATE_CONNECTING = 8;
    public static final int STATE_ERROR = 7;
    public static final int STATE_FAST_FORWARDING = 4;
    public static final int STATE_NONE = 0;
    public static final int STATE_PAUSED = 2;
    public static final int STATE_PLAYING = 3;
    public static final int STATE_REWINDING = 5;
    public static final int STATE_SKIPPING_TO_NEXT = 10;
    public static final int STATE_SKIPPING_TO_PREVIOUS = 9;
    public static final int STATE_SKIPPING_TO_QUEUE_ITEM = 11;
    public static final int STATE_STOPPED = 1;
    final long mActions;
    final long mActiveItemId;
    final long mBufferedPosition;
    java.util.List<android.support.v4.media.session.PlaybackStateCompat.CustomAction> mCustomActions;
    final int mErrorCode;
    final java.lang.CharSequence mErrorMessage;
    final android.os.Bundle mExtras;
    final long mPosition;
    final float mSpeed;
    final int mState;
    private java.lang.Object mStateObj;
    final long mUpdateTime;

    @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
    @java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.SOURCE)
    public @interface Actions {
    }

    public static final class Builder {
        private long mActions;
        private long mActiveItemId = -1;
        private long mBufferedPosition;
        private final java.util.List<android.support.v4.media.session.PlaybackStateCompat.CustomAction> mCustomActions = new java.util.ArrayList();
        private int mErrorCode;
        private java.lang.CharSequence mErrorMessage;
        private android.os.Bundle mExtras;
        private long mPosition;
        private float mRate;
        private int mState;
        private long mUpdateTime;

        public Builder() {
        }

        public Builder(android.support.v4.media.session.PlaybackStateCompat source) {
            this.mState = source.mState;
            this.mPosition = source.mPosition;
            this.mRate = source.mSpeed;
            this.mUpdateTime = source.mUpdateTime;
            this.mBufferedPosition = source.mBufferedPosition;
            this.mActions = source.mActions;
            this.mErrorCode = source.mErrorCode;
            this.mErrorMessage = source.mErrorMessage;
            if (source.mCustomActions != null) {
                this.mCustomActions.addAll(source.mCustomActions);
            }
            this.mActiveItemId = source.mActiveItemId;
            this.mExtras = source.mExtras;
        }

        public android.support.v4.media.session.PlaybackStateCompat.Builder setState(int state, long position, float playbackSpeed) {
            return setState(state, position, playbackSpeed, android.os.SystemClock.elapsedRealtime());
        }

        public android.support.v4.media.session.PlaybackStateCompat.Builder setState(int state, long position, float playbackSpeed, long updateTime) {
            this.mState = state;
            this.mPosition = position;
            this.mUpdateTime = updateTime;
            this.mRate = playbackSpeed;
            return this;
        }

        public android.support.v4.media.session.PlaybackStateCompat.Builder setBufferedPosition(long bufferPosition) {
            this.mBufferedPosition = bufferPosition;
            return this;
        }

        public android.support.v4.media.session.PlaybackStateCompat.Builder setActions(long capabilities) {
            this.mActions = capabilities;
            return this;
        }

        public android.support.v4.media.session.PlaybackStateCompat.Builder addCustomAction(java.lang.String action, java.lang.String name, int icon) {
            return addCustomAction(new android.support.v4.media.session.PlaybackStateCompat.CustomAction(action, name, icon, null));
        }

        public android.support.v4.media.session.PlaybackStateCompat.Builder addCustomAction(android.support.v4.media.session.PlaybackStateCompat.CustomAction customAction) {
            if (customAction == null) {
                throw new java.lang.IllegalArgumentException("You may not add a null CustomAction to PlaybackStateCompat.");
            }
            this.mCustomActions.add(customAction);
            return this;
        }

        public android.support.v4.media.session.PlaybackStateCompat.Builder setActiveQueueItemId(long id) {
            this.mActiveItemId = id;
            return this;
        }

        public android.support.v4.media.session.PlaybackStateCompat.Builder setErrorMessage(java.lang.CharSequence errorMessage) {
            this.mErrorMessage = errorMessage;
            return this;
        }

        public android.support.v4.media.session.PlaybackStateCompat.Builder setErrorMessage(int errorCode, java.lang.CharSequence errorMessage) {
            this.mErrorCode = errorCode;
            this.mErrorMessage = errorMessage;
            return this;
        }

        public android.support.v4.media.session.PlaybackStateCompat.Builder setExtras(android.os.Bundle extras) {
            this.mExtras = extras;
            return this;
        }

        public android.support.v4.media.session.PlaybackStateCompat build() {
            return new android.support.v4.media.session.PlaybackStateCompat(this.mState, this.mPosition, this.mBufferedPosition, this.mRate, this.mActions, this.mErrorCode, this.mErrorMessage, this.mUpdateTime, this.mCustomActions, this.mActiveItemId, this.mExtras);
        }
    }

    public static final class CustomAction implements android.os.Parcelable {
        public static final android.os.Parcelable.Creator<android.support.v4.media.session.PlaybackStateCompat.CustomAction> CREATOR = new android.os.Parcelable.Creator<android.support.v4.media.session.PlaybackStateCompat.CustomAction>() {
            public android.support.v4.media.session.PlaybackStateCompat.CustomAction createFromParcel(android.os.Parcel p) {
                return new android.support.v4.media.session.PlaybackStateCompat.CustomAction(p);
            }

            public android.support.v4.media.session.PlaybackStateCompat.CustomAction[] newArray(int size) {
                return new android.support.v4.media.session.PlaybackStateCompat.CustomAction[size];
            }
        };
        private final java.lang.String mAction;
        private java.lang.Object mCustomActionObj;
        private final android.os.Bundle mExtras;
        private final int mIcon;
        private final java.lang.CharSequence mName;

        public static final class Builder {
            private final java.lang.String mAction;
            private android.os.Bundle mExtras;
            private final int mIcon;
            private final java.lang.CharSequence mName;

            public Builder(java.lang.String action, java.lang.CharSequence name, int icon) {
                if (android.text.TextUtils.isEmpty(action)) {
                    throw new java.lang.IllegalArgumentException("You must specify an action to build a CustomAction.");
                } else if (android.text.TextUtils.isEmpty(name)) {
                    throw new java.lang.IllegalArgumentException("You must specify a name to build a CustomAction.");
                } else if (icon == 0) {
                    throw new java.lang.IllegalArgumentException("You must specify an icon resource id to build a CustomAction.");
                } else {
                    this.mAction = action;
                    this.mName = name;
                    this.mIcon = icon;
                }
            }

            public android.support.v4.media.session.PlaybackStateCompat.CustomAction.Builder setExtras(android.os.Bundle extras) {
                this.mExtras = extras;
                return this;
            }

            public android.support.v4.media.session.PlaybackStateCompat.CustomAction build() {
                return new android.support.v4.media.session.PlaybackStateCompat.CustomAction(this.mAction, this.mName, this.mIcon, this.mExtras);
            }
        }

        CustomAction(java.lang.String action, java.lang.CharSequence name, int icon, android.os.Bundle extras) {
            this.mAction = action;
            this.mName = name;
            this.mIcon = icon;
            this.mExtras = extras;
        }

        CustomAction(android.os.Parcel in) {
            this.mAction = in.readString();
            this.mName = (java.lang.CharSequence) android.text.TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(in);
            this.mIcon = in.readInt();
            this.mExtras = in.readBundle();
        }

        public void writeToParcel(android.os.Parcel dest, int flags) {
            dest.writeString(this.mAction);
            android.text.TextUtils.writeToParcel(this.mName, dest, flags);
            dest.writeInt(this.mIcon);
            dest.writeBundle(this.mExtras);
        }

        public int describeContents() {
            return 0;
        }

        public static android.support.v4.media.session.PlaybackStateCompat.CustomAction fromCustomAction(java.lang.Object customActionObj) {
            if (customActionObj == null || android.os.Build.VERSION.SDK_INT < 21) {
                return null;
            }
            android.support.v4.media.session.PlaybackStateCompat.CustomAction customAction = new android.support.v4.media.session.PlaybackStateCompat.CustomAction(android.support.v4.media.session.PlaybackStateCompatApi21.CustomAction.getAction(customActionObj), android.support.v4.media.session.PlaybackStateCompatApi21.CustomAction.getName(customActionObj), android.support.v4.media.session.PlaybackStateCompatApi21.CustomAction.getIcon(customActionObj), android.support.v4.media.session.PlaybackStateCompatApi21.CustomAction.getExtras(customActionObj));
            customAction.mCustomActionObj = customActionObj;
            return customAction;
        }

        public java.lang.Object getCustomAction() {
            if (this.mCustomActionObj != null || android.os.Build.VERSION.SDK_INT < 21) {
                return this.mCustomActionObj;
            }
            this.mCustomActionObj = android.support.v4.media.session.PlaybackStateCompatApi21.CustomAction.newInstance(this.mAction, this.mName, this.mIcon, this.mExtras);
            return this.mCustomActionObj;
        }

        public java.lang.String getAction() {
            return this.mAction;
        }

        public java.lang.CharSequence getName() {
            return this.mName;
        }

        public int getIcon() {
            return this.mIcon;
        }

        public android.os.Bundle getExtras() {
            return this.mExtras;
        }

        public java.lang.String toString() {
            return "Action:mName='" + this.mName + ", mIcon=" + this.mIcon + ", mExtras=" + this.mExtras;
        }
    }

    @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
    @java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.SOURCE)
    public @interface ErrorCode {
    }

    @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
    @java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.SOURCE)
    public @interface MediaKeyAction {
    }

    @java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.SOURCE)
    public @interface RepeatMode {
    }

    @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
    @java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.SOURCE)
    public @interface State {
    }

    public static int toKeyCode(long action) {
        if (action == 4) {
            return 126;
        }
        if (action == 2) {
            return 127;
        }
        if (action == 32) {
            return 87;
        }
        if (action == 16) {
            return 88;
        }
        if (action == 1) {
            return 86;
        }
        if (action == 64) {
            return 90;
        }
        if (action == 8) {
            return 89;
        }
        if (action == 512) {
            return 85;
        }
        return 0;
    }

    PlaybackStateCompat(int state, long position, long bufferedPosition, float rate, long actions, int errorCode, java.lang.CharSequence errorMessage, long updateTime, java.util.List<android.support.v4.media.session.PlaybackStateCompat.CustomAction> customActions, long activeItemId, android.os.Bundle extras) {
        this.mState = state;
        this.mPosition = position;
        this.mBufferedPosition = bufferedPosition;
        this.mSpeed = rate;
        this.mActions = actions;
        this.mErrorCode = errorCode;
        this.mErrorMessage = errorMessage;
        this.mUpdateTime = updateTime;
        this.mCustomActions = new java.util.ArrayList(customActions);
        this.mActiveItemId = activeItemId;
        this.mExtras = extras;
    }

    PlaybackStateCompat(android.os.Parcel in) {
        this.mState = in.readInt();
        this.mPosition = in.readLong();
        this.mSpeed = in.readFloat();
        this.mUpdateTime = in.readLong();
        this.mBufferedPosition = in.readLong();
        this.mActions = in.readLong();
        this.mErrorMessage = (java.lang.CharSequence) android.text.TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(in);
        this.mCustomActions = in.createTypedArrayList(android.support.v4.media.session.PlaybackStateCompat.CustomAction.CREATOR);
        this.mActiveItemId = in.readLong();
        this.mExtras = in.readBundle();
        this.mErrorCode = in.readInt();
    }

    public java.lang.String toString() {
        java.lang.StringBuilder bob = new java.lang.StringBuilder("PlaybackState {");
        bob.append("state=").append(this.mState);
        bob.append(", position=").append(this.mPosition);
        bob.append(", buffered position=").append(this.mBufferedPosition);
        bob.append(", speed=").append(this.mSpeed);
        bob.append(", updated=").append(this.mUpdateTime);
        bob.append(", actions=").append(this.mActions);
        bob.append(", error code=").append(this.mErrorCode);
        bob.append(", error message=").append(this.mErrorMessage);
        bob.append(", custom actions=").append(this.mCustomActions);
        bob.append(", active item id=").append(this.mActiveItemId);
        bob.append("}");
        return bob.toString();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeInt(this.mState);
        dest.writeLong(this.mPosition);
        dest.writeFloat(this.mSpeed);
        dest.writeLong(this.mUpdateTime);
        dest.writeLong(this.mBufferedPosition);
        dest.writeLong(this.mActions);
        android.text.TextUtils.writeToParcel(this.mErrorMessage, dest, flags);
        dest.writeTypedList(this.mCustomActions);
        dest.writeLong(this.mActiveItemId);
        dest.writeBundle(this.mExtras);
        dest.writeInt(this.mErrorCode);
    }

    public int getState() {
        return this.mState;
    }

    public long getPosition() {
        return this.mPosition;
    }

    public long getBufferedPosition() {
        return this.mBufferedPosition;
    }

    public float getPlaybackSpeed() {
        return this.mSpeed;
    }

    public long getActions() {
        return this.mActions;
    }

    public java.util.List<android.support.v4.media.session.PlaybackStateCompat.CustomAction> getCustomActions() {
        return this.mCustomActions;
    }

    public int getErrorCode() {
        return this.mErrorCode;
    }

    public java.lang.CharSequence getErrorMessage() {
        return this.mErrorMessage;
    }

    public long getLastPositionUpdateTime() {
        return this.mUpdateTime;
    }

    public long getActiveQueueItemId() {
        return this.mActiveItemId;
    }

    @android.support.annotation.Nullable
    public android.os.Bundle getExtras() {
        return this.mExtras;
    }

    public static android.support.v4.media.session.PlaybackStateCompat fromPlaybackState(java.lang.Object stateObj) {
        if (stateObj == null || android.os.Build.VERSION.SDK_INT < 21) {
            return null;
        }
        java.util.List<java.lang.Object> customActionObjs = android.support.v4.media.session.PlaybackStateCompatApi21.getCustomActions(stateObj);
        java.util.List<android.support.v4.media.session.PlaybackStateCompat.CustomAction> customActions = null;
        if (customActionObjs != null) {
            customActions = new java.util.ArrayList<>(customActionObjs.size());
            for (java.lang.Object customActionObj : customActionObjs) {
                customActions.add(android.support.v4.media.session.PlaybackStateCompat.CustomAction.fromCustomAction(customActionObj));
            }
        }
        android.support.v4.media.session.PlaybackStateCompat state = new android.support.v4.media.session.PlaybackStateCompat(android.support.v4.media.session.PlaybackStateCompatApi21.getState(stateObj), android.support.v4.media.session.PlaybackStateCompatApi21.getPosition(stateObj), android.support.v4.media.session.PlaybackStateCompatApi21.getBufferedPosition(stateObj), android.support.v4.media.session.PlaybackStateCompatApi21.getPlaybackSpeed(stateObj), android.support.v4.media.session.PlaybackStateCompatApi21.getActions(stateObj), 0, android.support.v4.media.session.PlaybackStateCompatApi21.getErrorMessage(stateObj), android.support.v4.media.session.PlaybackStateCompatApi21.getLastPositionUpdateTime(stateObj), customActions, android.support.v4.media.session.PlaybackStateCompatApi21.getActiveQueueItemId(stateObj), android.os.Build.VERSION.SDK_INT >= 22 ? android.support.v4.media.session.PlaybackStateCompatApi22.getExtras(stateObj) : null);
        state.mStateObj = stateObj;
        return state;
    }

    public java.lang.Object getPlaybackState() {
        if (this.mStateObj != null || android.os.Build.VERSION.SDK_INT < 21) {
            return this.mStateObj;
        }
        java.util.List<java.lang.Object> customActions = null;
        if (this.mCustomActions != null) {
            customActions = new java.util.ArrayList<>(this.mCustomActions.size());
            for (android.support.v4.media.session.PlaybackStateCompat.CustomAction customAction : this.mCustomActions) {
                customActions.add(customAction.getCustomAction());
            }
        }
        if (android.os.Build.VERSION.SDK_INT >= 22) {
            this.mStateObj = android.support.v4.media.session.PlaybackStateCompatApi22.newInstance(this.mState, this.mPosition, this.mBufferedPosition, this.mSpeed, this.mActions, this.mErrorMessage, this.mUpdateTime, customActions, this.mActiveItemId, this.mExtras);
        } else {
            this.mStateObj = android.support.v4.media.session.PlaybackStateCompatApi21.newInstance(this.mState, this.mPosition, this.mBufferedPosition, this.mSpeed, this.mActions, this.mErrorMessage, this.mUpdateTime, customActions, this.mActiveItemId);
        }
        return this.mStateObj;
    }
}
