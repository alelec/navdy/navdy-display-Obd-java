package android.support.v4.media;

public final class MediaDescriptionCompat implements android.os.Parcelable {
    public static final long BT_FOLDER_TYPE_ALBUMS = 2;
    public static final long BT_FOLDER_TYPE_ARTISTS = 3;
    public static final long BT_FOLDER_TYPE_GENRES = 4;
    public static final long BT_FOLDER_TYPE_MIXED = 0;
    public static final long BT_FOLDER_TYPE_PLAYLISTS = 5;
    public static final long BT_FOLDER_TYPE_TITLES = 1;
    public static final long BT_FOLDER_TYPE_YEARS = 6;
    public static final android.os.Parcelable.Creator<android.support.v4.media.MediaDescriptionCompat> CREATOR = new android.os.Parcelable.Creator<android.support.v4.media.MediaDescriptionCompat>() {
        public android.support.v4.media.MediaDescriptionCompat createFromParcel(android.os.Parcel in) {
            if (android.os.Build.VERSION.SDK_INT < 21) {
                return new android.support.v4.media.MediaDescriptionCompat(in);
            }
            return android.support.v4.media.MediaDescriptionCompat.fromMediaDescription(android.support.v4.media.MediaDescriptionCompatApi21.fromParcel(in));
        }

        public android.support.v4.media.MediaDescriptionCompat[] newArray(int size) {
            return new android.support.v4.media.MediaDescriptionCompat[size];
        }
    };
    @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
    public static final java.lang.String DESCRIPTION_KEY_MEDIA_URI = "android.support.v4.media.description.MEDIA_URI";
    @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
    public static final java.lang.String DESCRIPTION_KEY_NULL_BUNDLE_FLAG = "android.support.v4.media.description.NULL_BUNDLE_FLAG";
    public static final java.lang.String EXTRA_BT_FOLDER_TYPE = "android.media.extra.BT_FOLDER_TYPE";
    private final java.lang.CharSequence mDescription;
    private java.lang.Object mDescriptionObj;
    private final android.os.Bundle mExtras;
    private final android.graphics.Bitmap mIcon;
    private final android.net.Uri mIconUri;
    private final java.lang.String mMediaId;
    private final android.net.Uri mMediaUri;
    private final java.lang.CharSequence mSubtitle;
    private final java.lang.CharSequence mTitle;

    public static final class Builder {
        private java.lang.CharSequence mDescription;
        private android.os.Bundle mExtras;
        private android.graphics.Bitmap mIcon;
        private android.net.Uri mIconUri;
        private java.lang.String mMediaId;
        private android.net.Uri mMediaUri;
        private java.lang.CharSequence mSubtitle;
        private java.lang.CharSequence mTitle;

        public android.support.v4.media.MediaDescriptionCompat.Builder setMediaId(@android.support.annotation.Nullable java.lang.String mediaId) {
            this.mMediaId = mediaId;
            return this;
        }

        public android.support.v4.media.MediaDescriptionCompat.Builder setTitle(@android.support.annotation.Nullable java.lang.CharSequence title) {
            this.mTitle = title;
            return this;
        }

        public android.support.v4.media.MediaDescriptionCompat.Builder setSubtitle(@android.support.annotation.Nullable java.lang.CharSequence subtitle) {
            this.mSubtitle = subtitle;
            return this;
        }

        public android.support.v4.media.MediaDescriptionCompat.Builder setDescription(@android.support.annotation.Nullable java.lang.CharSequence description) {
            this.mDescription = description;
            return this;
        }

        public android.support.v4.media.MediaDescriptionCompat.Builder setIconBitmap(@android.support.annotation.Nullable android.graphics.Bitmap icon) {
            this.mIcon = icon;
            return this;
        }

        public android.support.v4.media.MediaDescriptionCompat.Builder setIconUri(@android.support.annotation.Nullable android.net.Uri iconUri) {
            this.mIconUri = iconUri;
            return this;
        }

        public android.support.v4.media.MediaDescriptionCompat.Builder setExtras(@android.support.annotation.Nullable android.os.Bundle extras) {
            this.mExtras = extras;
            return this;
        }

        public android.support.v4.media.MediaDescriptionCompat.Builder setMediaUri(@android.support.annotation.Nullable android.net.Uri mediaUri) {
            this.mMediaUri = mediaUri;
            return this;
        }

        public android.support.v4.media.MediaDescriptionCompat build() {
            return new android.support.v4.media.MediaDescriptionCompat(this.mMediaId, this.mTitle, this.mSubtitle, this.mDescription, this.mIcon, this.mIconUri, this.mExtras, this.mMediaUri);
        }
    }

    MediaDescriptionCompat(java.lang.String mediaId, java.lang.CharSequence title, java.lang.CharSequence subtitle, java.lang.CharSequence description, android.graphics.Bitmap icon, android.net.Uri iconUri, android.os.Bundle extras, android.net.Uri mediaUri) {
        this.mMediaId = mediaId;
        this.mTitle = title;
        this.mSubtitle = subtitle;
        this.mDescription = description;
        this.mIcon = icon;
        this.mIconUri = iconUri;
        this.mExtras = extras;
        this.mMediaUri = mediaUri;
    }

    MediaDescriptionCompat(android.os.Parcel in) {
        this.mMediaId = in.readString();
        this.mTitle = (java.lang.CharSequence) android.text.TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(in);
        this.mSubtitle = (java.lang.CharSequence) android.text.TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(in);
        this.mDescription = (java.lang.CharSequence) android.text.TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(in);
        this.mIcon = (android.graphics.Bitmap) in.readParcelable(null);
        this.mIconUri = (android.net.Uri) in.readParcelable(null);
        this.mExtras = in.readBundle();
        this.mMediaUri = (android.net.Uri) in.readParcelable(null);
    }

    @android.support.annotation.Nullable
    public java.lang.String getMediaId() {
        return this.mMediaId;
    }

    @android.support.annotation.Nullable
    public java.lang.CharSequence getTitle() {
        return this.mTitle;
    }

    @android.support.annotation.Nullable
    public java.lang.CharSequence getSubtitle() {
        return this.mSubtitle;
    }

    @android.support.annotation.Nullable
    public java.lang.CharSequence getDescription() {
        return this.mDescription;
    }

    @android.support.annotation.Nullable
    public android.graphics.Bitmap getIconBitmap() {
        return this.mIcon;
    }

    @android.support.annotation.Nullable
    public android.net.Uri getIconUri() {
        return this.mIconUri;
    }

    @android.support.annotation.Nullable
    public android.os.Bundle getExtras() {
        return this.mExtras;
    }

    @android.support.annotation.Nullable
    public android.net.Uri getMediaUri() {
        return this.mMediaUri;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        if (android.os.Build.VERSION.SDK_INT < 21) {
            dest.writeString(this.mMediaId);
            android.text.TextUtils.writeToParcel(this.mTitle, dest, flags);
            android.text.TextUtils.writeToParcel(this.mSubtitle, dest, flags);
            android.text.TextUtils.writeToParcel(this.mDescription, dest, flags);
            dest.writeParcelable(this.mIcon, flags);
            dest.writeParcelable(this.mIconUri, flags);
            dest.writeBundle(this.mExtras);
            dest.writeParcelable(this.mMediaUri, flags);
            return;
        }
        android.support.v4.media.MediaDescriptionCompatApi21.writeToParcel(getMediaDescription(), dest, flags);
    }

    public java.lang.String toString() {
        return this.mTitle + ", " + this.mSubtitle + ", " + this.mDescription;
    }

    public java.lang.Object getMediaDescription() {
        if (this.mDescriptionObj != null || android.os.Build.VERSION.SDK_INT < 21) {
            return this.mDescriptionObj;
        }
        java.lang.Object bob = android.support.v4.media.MediaDescriptionCompatApi21.Builder.newInstance();
        android.support.v4.media.MediaDescriptionCompatApi21.Builder.setMediaId(bob, this.mMediaId);
        android.support.v4.media.MediaDescriptionCompatApi21.Builder.setTitle(bob, this.mTitle);
        android.support.v4.media.MediaDescriptionCompatApi21.Builder.setSubtitle(bob, this.mSubtitle);
        android.support.v4.media.MediaDescriptionCompatApi21.Builder.setDescription(bob, this.mDescription);
        android.support.v4.media.MediaDescriptionCompatApi21.Builder.setIconBitmap(bob, this.mIcon);
        android.support.v4.media.MediaDescriptionCompatApi21.Builder.setIconUri(bob, this.mIconUri);
        android.os.Bundle extras = this.mExtras;
        if (android.os.Build.VERSION.SDK_INT < 23 && this.mMediaUri != null) {
            if (extras == null) {
                extras = new android.os.Bundle();
                extras.putBoolean(DESCRIPTION_KEY_NULL_BUNDLE_FLAG, true);
            }
            extras.putParcelable(DESCRIPTION_KEY_MEDIA_URI, this.mMediaUri);
        }
        android.support.v4.media.MediaDescriptionCompatApi21.Builder.setExtras(bob, extras);
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            android.support.v4.media.MediaDescriptionCompatApi23.Builder.setMediaUri(bob, this.mMediaUri);
        }
        this.mDescriptionObj = android.support.v4.media.MediaDescriptionCompatApi21.Builder.build(bob);
        return this.mDescriptionObj;
    }

    public static android.support.v4.media.MediaDescriptionCompat fromMediaDescription(java.lang.Object descriptionObj) {
        android.net.Uri mediaUri;
        if (descriptionObj == null || android.os.Build.VERSION.SDK_INT < 21) {
            return null;
        }
        android.support.v4.media.MediaDescriptionCompat.Builder bob = new android.support.v4.media.MediaDescriptionCompat.Builder();
        bob.setMediaId(android.support.v4.media.MediaDescriptionCompatApi21.getMediaId(descriptionObj));
        bob.setTitle(android.support.v4.media.MediaDescriptionCompatApi21.getTitle(descriptionObj));
        bob.setSubtitle(android.support.v4.media.MediaDescriptionCompatApi21.getSubtitle(descriptionObj));
        bob.setDescription(android.support.v4.media.MediaDescriptionCompatApi21.getDescription(descriptionObj));
        bob.setIconBitmap(android.support.v4.media.MediaDescriptionCompatApi21.getIconBitmap(descriptionObj));
        bob.setIconUri(android.support.v4.media.MediaDescriptionCompatApi21.getIconUri(descriptionObj));
        android.os.Bundle extras = android.support.v4.media.MediaDescriptionCompatApi21.getExtras(descriptionObj);
        if (extras == null) {
            mediaUri = null;
        } else {
            mediaUri = (android.net.Uri) extras.getParcelable(DESCRIPTION_KEY_MEDIA_URI);
        }
        if (mediaUri != null) {
            if (!extras.containsKey(DESCRIPTION_KEY_NULL_BUNDLE_FLAG) || extras.size() != 2) {
                extras.remove(DESCRIPTION_KEY_MEDIA_URI);
                extras.remove(DESCRIPTION_KEY_NULL_BUNDLE_FLAG);
            } else {
                extras = null;
            }
        }
        bob.setExtras(extras);
        if (mediaUri != null) {
            bob.setMediaUri(mediaUri);
        } else if (android.os.Build.VERSION.SDK_INT >= 23) {
            bob.setMediaUri(android.support.v4.media.MediaDescriptionCompatApi23.getMediaUri(descriptionObj));
        }
        android.support.v4.media.MediaDescriptionCompat descriptionCompat = bob.build();
        descriptionCompat.mDescriptionObj = descriptionObj;
        return descriptionCompat;
    }
}
