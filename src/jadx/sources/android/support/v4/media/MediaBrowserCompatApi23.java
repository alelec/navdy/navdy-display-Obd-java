package android.support.v4.media;

@android.annotation.TargetApi(23)
@android.support.annotation.RequiresApi(23)
class MediaBrowserCompatApi23 {

    interface ItemCallback {
        void onError(@android.support.annotation.NonNull java.lang.String str);

        void onItemLoaded(android.os.Parcel parcel);
    }

    static class ItemCallbackProxy<T extends android.support.v4.media.MediaBrowserCompatApi23.ItemCallback> extends android.media.browse.MediaBrowser.ItemCallback {
        protected final T mItemCallback;

        public ItemCallbackProxy(T callback) {
            this.mItemCallback = callback;
        }

        public void onItemLoaded(android.media.browse.MediaBrowser.MediaItem item) {
            if (item == null) {
                this.mItemCallback.onItemLoaded(null);
                return;
            }
            android.os.Parcel parcel = android.os.Parcel.obtain();
            item.writeToParcel(parcel, 0);
            this.mItemCallback.onItemLoaded(parcel);
        }

        public void onError(@android.support.annotation.NonNull java.lang.String itemId) {
            this.mItemCallback.onError(itemId);
        }
    }

    MediaBrowserCompatApi23() {
    }

    public static java.lang.Object createItemCallback(android.support.v4.media.MediaBrowserCompatApi23.ItemCallback callback) {
        return new android.support.v4.media.MediaBrowserCompatApi23.ItemCallbackProxy(callback);
    }

    public static void getItem(java.lang.Object browserObj, java.lang.String mediaId, java.lang.Object itemCallbackObj) {
        ((android.media.browse.MediaBrowser) browserObj).getItem(mediaId, (android.media.browse.MediaBrowser.ItemCallback) itemCallbackObj);
    }
}
