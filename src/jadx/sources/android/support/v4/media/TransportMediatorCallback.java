package android.support.v4.media;

@android.annotation.TargetApi(18)
@android.support.annotation.RequiresApi(18)
interface TransportMediatorCallback {
    long getPlaybackPosition();

    void handleAudioFocusChange(int i);

    void handleKey(android.view.KeyEvent keyEvent);

    void playbackPositionUpdate(long j);
}
