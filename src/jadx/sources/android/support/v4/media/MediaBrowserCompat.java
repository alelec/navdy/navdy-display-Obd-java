package android.support.v4.media;

public final class MediaBrowserCompat {
    static final boolean DEBUG = android.util.Log.isLoggable(TAG, 3);
    public static final java.lang.String EXTRA_PAGE = "android.media.browse.extra.PAGE";
    public static final java.lang.String EXTRA_PAGE_SIZE = "android.media.browse.extra.PAGE_SIZE";
    static final java.lang.String TAG = "MediaBrowserCompat";
    private final android.support.v4.media.MediaBrowserCompat.MediaBrowserImpl mImpl;

    private static class CallbackHandler extends android.os.Handler {
        private final java.lang.ref.WeakReference<android.support.v4.media.MediaBrowserCompat.MediaBrowserServiceCallbackImpl> mCallbackImplRef;
        private java.lang.ref.WeakReference<android.os.Messenger> mCallbacksMessengerRef;

        CallbackHandler(android.support.v4.media.MediaBrowserCompat.MediaBrowserServiceCallbackImpl callbackImpl) {
            this.mCallbackImplRef = new java.lang.ref.WeakReference<>(callbackImpl);
        }

        public void handleMessage(android.os.Message msg) {
            if (this.mCallbacksMessengerRef != null && this.mCallbacksMessengerRef.get() != null && this.mCallbackImplRef.get() != null) {
                android.os.Bundle data = msg.getData();
                data.setClassLoader(android.support.v4.media.session.MediaSessionCompat.class.getClassLoader());
                switch (msg.what) {
                    case 1:
                        ((android.support.v4.media.MediaBrowserCompat.MediaBrowserServiceCallbackImpl) this.mCallbackImplRef.get()).onServiceConnected((android.os.Messenger) this.mCallbacksMessengerRef.get(), data.getString(android.support.v4.media.MediaBrowserProtocol.DATA_MEDIA_ITEM_ID), (android.support.v4.media.session.MediaSessionCompat.Token) data.getParcelable(android.support.v4.media.MediaBrowserProtocol.DATA_MEDIA_SESSION_TOKEN), data.getBundle(android.support.v4.media.MediaBrowserProtocol.DATA_ROOT_HINTS));
                        return;
                    case 2:
                        ((android.support.v4.media.MediaBrowserCompat.MediaBrowserServiceCallbackImpl) this.mCallbackImplRef.get()).onConnectionFailed((android.os.Messenger) this.mCallbacksMessengerRef.get());
                        return;
                    case 3:
                        ((android.support.v4.media.MediaBrowserCompat.MediaBrowserServiceCallbackImpl) this.mCallbackImplRef.get()).onLoadChildren((android.os.Messenger) this.mCallbacksMessengerRef.get(), data.getString(android.support.v4.media.MediaBrowserProtocol.DATA_MEDIA_ITEM_ID), data.getParcelableArrayList(android.support.v4.media.MediaBrowserProtocol.DATA_MEDIA_ITEM_LIST), data.getBundle(android.support.v4.media.MediaBrowserProtocol.DATA_OPTIONS));
                        return;
                    default:
                        android.util.Log.w(android.support.v4.media.MediaBrowserCompat.TAG, "Unhandled message: " + msg + "\n  Client version: " + 1 + "\n  Service version: " + msg.arg1);
                        return;
                }
            }
        }

        /* access modifiers changed from: 0000 */
        public void setCallbacksMessenger(android.os.Messenger callbacksMessenger) {
            this.mCallbacksMessengerRef = new java.lang.ref.WeakReference<>(callbacksMessenger);
        }
    }

    public static class ConnectionCallback {
        android.support.v4.media.MediaBrowserCompat.ConnectionCallback.ConnectionCallbackInternal mConnectionCallbackInternal;
        final java.lang.Object mConnectionCallbackObj;

        interface ConnectionCallbackInternal {
            void onConnected();

            void onConnectionFailed();

            void onConnectionSuspended();
        }

        private class StubApi21 implements android.support.v4.media.MediaBrowserCompatApi21.ConnectionCallback {
            StubApi21() {
            }

            public void onConnected() {
                if (android.support.v4.media.MediaBrowserCompat.ConnectionCallback.this.mConnectionCallbackInternal != null) {
                    android.support.v4.media.MediaBrowserCompat.ConnectionCallback.this.mConnectionCallbackInternal.onConnected();
                }
                android.support.v4.media.MediaBrowserCompat.ConnectionCallback.this.onConnected();
            }

            public void onConnectionSuspended() {
                if (android.support.v4.media.MediaBrowserCompat.ConnectionCallback.this.mConnectionCallbackInternal != null) {
                    android.support.v4.media.MediaBrowserCompat.ConnectionCallback.this.mConnectionCallbackInternal.onConnectionSuspended();
                }
                android.support.v4.media.MediaBrowserCompat.ConnectionCallback.this.onConnectionSuspended();
            }

            public void onConnectionFailed() {
                if (android.support.v4.media.MediaBrowserCompat.ConnectionCallback.this.mConnectionCallbackInternal != null) {
                    android.support.v4.media.MediaBrowserCompat.ConnectionCallback.this.mConnectionCallbackInternal.onConnectionFailed();
                }
                android.support.v4.media.MediaBrowserCompat.ConnectionCallback.this.onConnectionFailed();
            }
        }

        public ConnectionCallback() {
            if (android.os.Build.VERSION.SDK_INT >= 21) {
                this.mConnectionCallbackObj = android.support.v4.media.MediaBrowserCompatApi21.createConnectionCallback(new android.support.v4.media.MediaBrowserCompat.ConnectionCallback.StubApi21());
            } else {
                this.mConnectionCallbackObj = null;
            }
        }

        public void onConnected() {
        }

        public void onConnectionSuspended() {
        }

        public void onConnectionFailed() {
        }

        /* access modifiers changed from: 0000 */
        public void setInternalConnectionCallback(android.support.v4.media.MediaBrowserCompat.ConnectionCallback.ConnectionCallbackInternal connectionCallbackInternal) {
            this.mConnectionCallbackInternal = connectionCallbackInternal;
        }
    }

    public static abstract class ItemCallback {
        final java.lang.Object mItemCallbackObj;

        private class StubApi23 implements android.support.v4.media.MediaBrowserCompatApi23.ItemCallback {
            StubApi23() {
            }

            public void onItemLoaded(android.os.Parcel itemParcel) {
                if (itemParcel == null) {
                    android.support.v4.media.MediaBrowserCompat.ItemCallback.this.onItemLoaded(null);
                    return;
                }
                itemParcel.setDataPosition(0);
                android.support.v4.media.MediaBrowserCompat.MediaItem item = (android.support.v4.media.MediaBrowserCompat.MediaItem) android.support.v4.media.MediaBrowserCompat.MediaItem.CREATOR.createFromParcel(itemParcel);
                itemParcel.recycle();
                android.support.v4.media.MediaBrowserCompat.ItemCallback.this.onItemLoaded(item);
            }

            public void onError(@android.support.annotation.NonNull java.lang.String itemId) {
                android.support.v4.media.MediaBrowserCompat.ItemCallback.this.onError(itemId);
            }
        }

        public ItemCallback() {
            if (android.os.Build.VERSION.SDK_INT >= 23) {
                this.mItemCallbackObj = android.support.v4.media.MediaBrowserCompatApi23.createItemCallback(new android.support.v4.media.MediaBrowserCompat.ItemCallback.StubApi23());
            } else {
                this.mItemCallbackObj = null;
            }
        }

        public void onItemLoaded(android.support.v4.media.MediaBrowserCompat.MediaItem item) {
        }

        public void onError(@android.support.annotation.NonNull java.lang.String itemId) {
        }
    }

    private static class ItemReceiver extends android.support.v4.os.ResultReceiver {
        private final android.support.v4.media.MediaBrowserCompat.ItemCallback mCallback;
        private final java.lang.String mMediaId;

        ItemReceiver(java.lang.String mediaId, android.support.v4.media.MediaBrowserCompat.ItemCallback callback, android.os.Handler handler) {
            super(handler);
            this.mMediaId = mediaId;
            this.mCallback = callback;
        }

        /* access modifiers changed from: protected */
        public void onReceiveResult(int resultCode, android.os.Bundle resultData) {
            if (resultData != null) {
                resultData.setClassLoader(android.support.v4.media.MediaBrowserCompat.class.getClassLoader());
            }
            if (resultCode != 0 || resultData == null || !resultData.containsKey(android.support.v4.media.MediaBrowserServiceCompat.KEY_MEDIA_ITEM)) {
                this.mCallback.onError(this.mMediaId);
                return;
            }
            android.os.Parcelable item = resultData.getParcelable(android.support.v4.media.MediaBrowserServiceCompat.KEY_MEDIA_ITEM);
            if (item == null || (item instanceof android.support.v4.media.MediaBrowserCompat.MediaItem)) {
                this.mCallback.onItemLoaded((android.support.v4.media.MediaBrowserCompat.MediaItem) item);
            } else {
                this.mCallback.onError(this.mMediaId);
            }
        }
    }

    interface MediaBrowserImpl {
        void connect();

        void disconnect();

        @android.support.annotation.Nullable
        android.os.Bundle getExtras();

        void getItem(@android.support.annotation.NonNull java.lang.String str, @android.support.annotation.NonNull android.support.v4.media.MediaBrowserCompat.ItemCallback itemCallback);

        @android.support.annotation.NonNull
        java.lang.String getRoot();

        android.content.ComponentName getServiceComponent();

        @android.support.annotation.NonNull
        android.support.v4.media.session.MediaSessionCompat.Token getSessionToken();

        boolean isConnected();

        void search(@android.support.annotation.NonNull java.lang.String str, android.os.Bundle bundle, @android.support.annotation.NonNull android.support.v4.media.MediaBrowserCompat.SearchCallback searchCallback);

        void subscribe(@android.support.annotation.NonNull java.lang.String str, android.os.Bundle bundle, @android.support.annotation.NonNull android.support.v4.media.MediaBrowserCompat.SubscriptionCallback subscriptionCallback);

        void unsubscribe(@android.support.annotation.NonNull java.lang.String str, android.support.v4.media.MediaBrowserCompat.SubscriptionCallback subscriptionCallback);
    }

    static class MediaBrowserImplApi21 implements android.support.v4.media.MediaBrowserCompat.MediaBrowserImpl, android.support.v4.media.MediaBrowserCompat.MediaBrowserServiceCallbackImpl, android.support.v4.media.MediaBrowserCompat.ConnectionCallback.ConnectionCallbackInternal {
        protected final java.lang.Object mBrowserObj;
        protected android.os.Messenger mCallbacksMessenger;
        protected final android.support.v4.media.MediaBrowserCompat.CallbackHandler mHandler = new android.support.v4.media.MediaBrowserCompat.CallbackHandler(this);
        protected final android.os.Bundle mRootHints;
        protected android.support.v4.media.MediaBrowserCompat.ServiceBinderWrapper mServiceBinderWrapper;
        private final android.support.v4.util.ArrayMap<java.lang.String, android.support.v4.media.MediaBrowserCompat.Subscription> mSubscriptions = new android.support.v4.util.ArrayMap<>();

        public MediaBrowserImplApi21(android.content.Context context, android.content.ComponentName serviceComponent, android.support.v4.media.MediaBrowserCompat.ConnectionCallback callback, android.os.Bundle rootHints) {
            if (android.os.Build.VERSION.SDK_INT <= 25) {
                if (rootHints == null) {
                    rootHints = new android.os.Bundle();
                }
                rootHints.putInt(android.support.v4.media.MediaBrowserProtocol.EXTRA_CLIENT_VERSION, 1);
                this.mRootHints = new android.os.Bundle(rootHints);
            } else {
                this.mRootHints = rootHints == null ? null : new android.os.Bundle(rootHints);
            }
            callback.setInternalConnectionCallback(this);
            this.mBrowserObj = android.support.v4.media.MediaBrowserCompatApi21.createBrowser(context, serviceComponent, callback.mConnectionCallbackObj, this.mRootHints);
        }

        public void connect() {
            android.support.v4.media.MediaBrowserCompatApi21.connect(this.mBrowserObj);
        }

        public void disconnect() {
            if (!(this.mServiceBinderWrapper == null || this.mCallbacksMessenger == null)) {
                try {
                    this.mServiceBinderWrapper.unregisterCallbackMessenger(this.mCallbacksMessenger);
                } catch (android.os.RemoteException e) {
                    android.util.Log.i(android.support.v4.media.MediaBrowserCompat.TAG, "Remote error unregistering client messenger.");
                }
            }
            android.support.v4.media.MediaBrowserCompatApi21.disconnect(this.mBrowserObj);
        }

        public boolean isConnected() {
            return android.support.v4.media.MediaBrowserCompatApi21.isConnected(this.mBrowserObj);
        }

        public android.content.ComponentName getServiceComponent() {
            return android.support.v4.media.MediaBrowserCompatApi21.getServiceComponent(this.mBrowserObj);
        }

        @android.support.annotation.NonNull
        public java.lang.String getRoot() {
            return android.support.v4.media.MediaBrowserCompatApi21.getRoot(this.mBrowserObj);
        }

        @android.support.annotation.Nullable
        public android.os.Bundle getExtras() {
            return android.support.v4.media.MediaBrowserCompatApi21.getExtras(this.mBrowserObj);
        }

        @android.support.annotation.NonNull
        public android.support.v4.media.session.MediaSessionCompat.Token getSessionToken() {
            return android.support.v4.media.session.MediaSessionCompat.Token.fromToken(android.support.v4.media.MediaBrowserCompatApi21.getSessionToken(this.mBrowserObj));
        }

        public void subscribe(@android.support.annotation.NonNull java.lang.String parentId, android.os.Bundle options, @android.support.annotation.NonNull android.support.v4.media.MediaBrowserCompat.SubscriptionCallback callback) {
            android.support.v4.media.MediaBrowserCompat.Subscription sub = (android.support.v4.media.MediaBrowserCompat.Subscription) this.mSubscriptions.get(parentId);
            if (sub == null) {
                sub = new android.support.v4.media.MediaBrowserCompat.Subscription();
                this.mSubscriptions.put(parentId, sub);
            }
            callback.setSubscription(sub);
            android.os.Bundle copiedOptions = options == null ? null : new android.os.Bundle(options);
            sub.putCallback(copiedOptions, callback);
            if (this.mServiceBinderWrapper == null) {
                android.support.v4.media.MediaBrowserCompatApi21.subscribe(this.mBrowserObj, parentId, callback.mSubscriptionCallbackObj);
                return;
            }
            try {
                this.mServiceBinderWrapper.addSubscription(parentId, callback.mToken, copiedOptions, this.mCallbacksMessenger);
            } catch (android.os.RemoteException e) {
                android.util.Log.i(android.support.v4.media.MediaBrowserCompat.TAG, "Remote error subscribing media item: " + parentId);
            }
        }

        public void unsubscribe(@android.support.annotation.NonNull java.lang.String parentId, android.support.v4.media.MediaBrowserCompat.SubscriptionCallback callback) {
            android.support.v4.media.MediaBrowserCompat.Subscription sub = (android.support.v4.media.MediaBrowserCompat.Subscription) this.mSubscriptions.get(parentId);
            if (sub != null) {
                if (this.mServiceBinderWrapper == null) {
                    if (callback == null) {
                        android.support.v4.media.MediaBrowserCompatApi21.unsubscribe(this.mBrowserObj, parentId);
                    } else {
                        java.util.List<android.support.v4.media.MediaBrowserCompat.SubscriptionCallback> callbacks = sub.getCallbacks();
                        java.util.List<android.os.Bundle> optionsList = sub.getOptionsList();
                        for (int i = callbacks.size() - 1; i >= 0; i--) {
                            if (callbacks.get(i) == callback) {
                                callbacks.remove(i);
                                optionsList.remove(i);
                            }
                        }
                        if (callbacks.size() == 0) {
                            android.support.v4.media.MediaBrowserCompatApi21.unsubscribe(this.mBrowserObj, parentId);
                        }
                    }
                } else if (callback == null) {
                    try {
                        this.mServiceBinderWrapper.removeSubscription(parentId, null, this.mCallbacksMessenger);
                    } catch (android.os.RemoteException e) {
                        android.util.Log.d(android.support.v4.media.MediaBrowserCompat.TAG, "removeSubscription failed with RemoteException parentId=" + parentId);
                    }
                } else {
                    java.util.List<android.support.v4.media.MediaBrowserCompat.SubscriptionCallback> callbacks2 = sub.getCallbacks();
                    java.util.List<android.os.Bundle> optionsList2 = sub.getOptionsList();
                    for (int i2 = callbacks2.size() - 1; i2 >= 0; i2--) {
                        if (callbacks2.get(i2) == callback) {
                            this.mServiceBinderWrapper.removeSubscription(parentId, callback.mToken, this.mCallbacksMessenger);
                            callbacks2.remove(i2);
                            optionsList2.remove(i2);
                        }
                    }
                }
                if (sub.isEmpty() || callback == null) {
                    this.mSubscriptions.remove(parentId);
                }
            }
        }

        public void getItem(@android.support.annotation.NonNull final java.lang.String mediaId, @android.support.annotation.NonNull final android.support.v4.media.MediaBrowserCompat.ItemCallback cb) {
            if (android.text.TextUtils.isEmpty(mediaId)) {
                throw new java.lang.IllegalArgumentException("mediaId is empty");
            } else if (cb == null) {
                throw new java.lang.IllegalArgumentException("cb is null");
            } else if (!android.support.v4.media.MediaBrowserCompatApi21.isConnected(this.mBrowserObj)) {
                android.util.Log.i(android.support.v4.media.MediaBrowserCompat.TAG, "Not connected, unable to retrieve the MediaItem.");
                this.mHandler.post(new java.lang.Runnable() {
                    public void run() {
                        cb.onError(mediaId);
                    }
                });
            } else if (this.mServiceBinderWrapper == null) {
                this.mHandler.post(new java.lang.Runnable() {
                    public void run() {
                        cb.onError(mediaId);
                    }
                });
            } else {
                try {
                    this.mServiceBinderWrapper.getMediaItem(mediaId, new android.support.v4.media.MediaBrowserCompat.ItemReceiver(mediaId, cb, this.mHandler), this.mCallbacksMessenger);
                } catch (android.os.RemoteException e) {
                    android.util.Log.i(android.support.v4.media.MediaBrowserCompat.TAG, "Remote error getting media item: " + mediaId);
                    this.mHandler.post(new java.lang.Runnable() {
                        public void run() {
                            cb.onError(mediaId);
                        }
                    });
                }
            }
        }

        public void search(@android.support.annotation.NonNull final java.lang.String query, final android.os.Bundle extras, @android.support.annotation.NonNull final android.support.v4.media.MediaBrowserCompat.SearchCallback callback) {
            if (!isConnected()) {
                android.util.Log.i(android.support.v4.media.MediaBrowserCompat.TAG, "Not connected, unable to search.");
                this.mHandler.post(new java.lang.Runnable() {
                    public void run() {
                        callback.onError(query, extras);
                    }
                });
            } else if (this.mServiceBinderWrapper == null) {
                android.util.Log.i(android.support.v4.media.MediaBrowserCompat.TAG, "The connected service doesn't support search.");
                this.mHandler.post(new java.lang.Runnable() {
                    public void run() {
                        callback.onError(query, extras);
                    }
                });
            } else {
                try {
                    this.mServiceBinderWrapper.search(query, extras, new android.support.v4.media.MediaBrowserCompat.SearchResultReceiver(query, extras, callback, this.mHandler), this.mCallbacksMessenger);
                } catch (android.os.RemoteException e) {
                    android.util.Log.i(android.support.v4.media.MediaBrowserCompat.TAG, "Remote error searching items with query: " + query, e);
                    this.mHandler.post(new java.lang.Runnable() {
                        public void run() {
                            callback.onError(query, extras);
                        }
                    });
                }
            }
        }

        public void onConnected() {
            android.os.Bundle extras = android.support.v4.media.MediaBrowserCompatApi21.getExtras(this.mBrowserObj);
            if (extras != null) {
                android.os.IBinder serviceBinder = android.support.v4.app.BundleCompat.getBinder(extras, android.support.v4.media.MediaBrowserProtocol.EXTRA_MESSENGER_BINDER);
                if (serviceBinder != null) {
                    this.mServiceBinderWrapper = new android.support.v4.media.MediaBrowserCompat.ServiceBinderWrapper(serviceBinder, this.mRootHints);
                    this.mCallbacksMessenger = new android.os.Messenger(this.mHandler);
                    this.mHandler.setCallbacksMessenger(this.mCallbacksMessenger);
                    try {
                        this.mServiceBinderWrapper.registerCallbackMessenger(this.mCallbacksMessenger);
                    } catch (android.os.RemoteException e) {
                        android.util.Log.i(android.support.v4.media.MediaBrowserCompat.TAG, "Remote error registering client messenger.");
                    }
                }
            }
        }

        public void onConnectionSuspended() {
            this.mServiceBinderWrapper = null;
            this.mCallbacksMessenger = null;
            this.mHandler.setCallbacksMessenger(null);
        }

        public void onConnectionFailed() {
        }

        public void onServiceConnected(android.os.Messenger callback, java.lang.String root, android.support.v4.media.session.MediaSessionCompat.Token session, android.os.Bundle extra) {
        }

        public void onConnectionFailed(android.os.Messenger callback) {
        }

        public void onLoadChildren(android.os.Messenger callback, java.lang.String parentId, java.util.List list, android.os.Bundle options) {
            if (this.mCallbacksMessenger == callback) {
                android.support.v4.media.MediaBrowserCompat.Subscription subscription = (android.support.v4.media.MediaBrowserCompat.Subscription) this.mSubscriptions.get(parentId);
                if (subscription != null) {
                    android.support.v4.media.MediaBrowserCompat.SubscriptionCallback subscriptionCallback = subscription.getCallback(options);
                    if (subscriptionCallback == null) {
                        return;
                    }
                    if (options == null) {
                        if (list == null) {
                            subscriptionCallback.onError(parentId);
                        } else {
                            subscriptionCallback.onChildrenLoaded(parentId, list);
                        }
                    } else if (list == null) {
                        subscriptionCallback.onError(parentId, options);
                    } else {
                        subscriptionCallback.onChildrenLoaded(parentId, list, options);
                    }
                } else if (android.support.v4.media.MediaBrowserCompat.DEBUG) {
                    android.util.Log.d(android.support.v4.media.MediaBrowserCompat.TAG, "onLoadChildren for id that isn't subscribed id=" + parentId);
                }
            }
        }
    }

    static class MediaBrowserImplApi23 extends android.support.v4.media.MediaBrowserCompat.MediaBrowserImplApi21 {
        public MediaBrowserImplApi23(android.content.Context context, android.content.ComponentName serviceComponent, android.support.v4.media.MediaBrowserCompat.ConnectionCallback callback, android.os.Bundle rootHints) {
            super(context, serviceComponent, callback, rootHints);
        }

        public void getItem(@android.support.annotation.NonNull java.lang.String mediaId, @android.support.annotation.NonNull android.support.v4.media.MediaBrowserCompat.ItemCallback cb) {
            if (this.mServiceBinderWrapper == null) {
                android.support.v4.media.MediaBrowserCompatApi23.getItem(this.mBrowserObj, mediaId, cb.mItemCallbackObj);
            } else {
                super.getItem(mediaId, cb);
            }
        }
    }

    static class MediaBrowserImplApi24 extends android.support.v4.media.MediaBrowserCompat.MediaBrowserImplApi23 {
        public MediaBrowserImplApi24(android.content.Context context, android.content.ComponentName serviceComponent, android.support.v4.media.MediaBrowserCompat.ConnectionCallback callback, android.os.Bundle rootHints) {
            super(context, serviceComponent, callback, rootHints);
        }

        public void subscribe(@android.support.annotation.NonNull java.lang.String parentId, @android.support.annotation.NonNull android.os.Bundle options, @android.support.annotation.NonNull android.support.v4.media.MediaBrowserCompat.SubscriptionCallback callback) {
            if (options == null) {
                android.support.v4.media.MediaBrowserCompatApi21.subscribe(this.mBrowserObj, parentId, callback.mSubscriptionCallbackObj);
            } else {
                android.support.v4.media.MediaBrowserCompatApi24.subscribe(this.mBrowserObj, parentId, options, callback.mSubscriptionCallbackObj);
            }
        }

        public void unsubscribe(@android.support.annotation.NonNull java.lang.String parentId, android.support.v4.media.MediaBrowserCompat.SubscriptionCallback callback) {
            if (callback == null) {
                android.support.v4.media.MediaBrowserCompatApi21.unsubscribe(this.mBrowserObj, parentId);
            } else {
                android.support.v4.media.MediaBrowserCompatApi24.unsubscribe(this.mBrowserObj, parentId, callback.mSubscriptionCallbackObj);
            }
        }
    }

    static class MediaBrowserImplBase implements android.support.v4.media.MediaBrowserCompat.MediaBrowserImpl, android.support.v4.media.MediaBrowserCompat.MediaBrowserServiceCallbackImpl {
        private static final int CONNECT_STATE_CONNECTED = 2;
        static final int CONNECT_STATE_CONNECTING = 1;
        static final int CONNECT_STATE_DISCONNECTED = 0;
        static final int CONNECT_STATE_SUSPENDED = 3;
        final android.support.v4.media.MediaBrowserCompat.ConnectionCallback mCallback;
        android.os.Messenger mCallbacksMessenger;
        final android.content.Context mContext;
        private android.os.Bundle mExtras;
        final android.support.v4.media.MediaBrowserCompat.CallbackHandler mHandler = new android.support.v4.media.MediaBrowserCompat.CallbackHandler(this);
        private android.support.v4.media.session.MediaSessionCompat.Token mMediaSessionToken;
        final android.os.Bundle mRootHints;
        private java.lang.String mRootId;
        android.support.v4.media.MediaBrowserCompat.ServiceBinderWrapper mServiceBinderWrapper;
        final android.content.ComponentName mServiceComponent;
        android.support.v4.media.MediaBrowserCompat.MediaBrowserImplBase.MediaServiceConnection mServiceConnection;
        int mState = 0;
        private final android.support.v4.util.ArrayMap<java.lang.String, android.support.v4.media.MediaBrowserCompat.Subscription> mSubscriptions = new android.support.v4.util.ArrayMap<>();

        private class MediaServiceConnection implements android.content.ServiceConnection {
            MediaServiceConnection() {
            }

            public void onServiceConnected(final android.content.ComponentName name, final android.os.IBinder binder) {
                postOrRun(new java.lang.Runnable() {
                    public void run() {
                        if (android.support.v4.media.MediaBrowserCompat.DEBUG) {
                            android.util.Log.d(android.support.v4.media.MediaBrowserCompat.TAG, "MediaServiceConnection.onServiceConnected name=" + name + " binder=" + binder);
                            android.support.v4.media.MediaBrowserCompat.MediaBrowserImplBase.this.dump();
                        }
                        if (android.support.v4.media.MediaBrowserCompat.MediaBrowserImplBase.MediaServiceConnection.this.isCurrent("onServiceConnected")) {
                            android.support.v4.media.MediaBrowserCompat.MediaBrowserImplBase.this.mServiceBinderWrapper = new android.support.v4.media.MediaBrowserCompat.ServiceBinderWrapper(binder, android.support.v4.media.MediaBrowserCompat.MediaBrowserImplBase.this.mRootHints);
                            android.support.v4.media.MediaBrowserCompat.MediaBrowserImplBase.this.mCallbacksMessenger = new android.os.Messenger(android.support.v4.media.MediaBrowserCompat.MediaBrowserImplBase.this.mHandler);
                            android.support.v4.media.MediaBrowserCompat.MediaBrowserImplBase.this.mHandler.setCallbacksMessenger(android.support.v4.media.MediaBrowserCompat.MediaBrowserImplBase.this.mCallbacksMessenger);
                            android.support.v4.media.MediaBrowserCompat.MediaBrowserImplBase.this.mState = 1;
                            try {
                                if (android.support.v4.media.MediaBrowserCompat.DEBUG) {
                                    android.util.Log.d(android.support.v4.media.MediaBrowserCompat.TAG, "ServiceCallbacks.onConnect...");
                                    android.support.v4.media.MediaBrowserCompat.MediaBrowserImplBase.this.dump();
                                }
                                android.support.v4.media.MediaBrowserCompat.MediaBrowserImplBase.this.mServiceBinderWrapper.connect(android.support.v4.media.MediaBrowserCompat.MediaBrowserImplBase.this.mContext, android.support.v4.media.MediaBrowserCompat.MediaBrowserImplBase.this.mCallbacksMessenger);
                            } catch (android.os.RemoteException e) {
                                android.util.Log.w(android.support.v4.media.MediaBrowserCompat.TAG, "RemoteException during connect for " + android.support.v4.media.MediaBrowserCompat.MediaBrowserImplBase.this.mServiceComponent);
                                if (android.support.v4.media.MediaBrowserCompat.DEBUG) {
                                    android.util.Log.d(android.support.v4.media.MediaBrowserCompat.TAG, "ServiceCallbacks.onConnect...");
                                    android.support.v4.media.MediaBrowserCompat.MediaBrowserImplBase.this.dump();
                                }
                            }
                        }
                    }
                });
            }

            public void onServiceDisconnected(final android.content.ComponentName name) {
                postOrRun(new java.lang.Runnable() {
                    public void run() {
                        if (android.support.v4.media.MediaBrowserCompat.DEBUG) {
                            android.util.Log.d(android.support.v4.media.MediaBrowserCompat.TAG, "MediaServiceConnection.onServiceDisconnected name=" + name + " this=" + this + " mServiceConnection=" + android.support.v4.media.MediaBrowserCompat.MediaBrowserImplBase.this.mServiceConnection);
                            android.support.v4.media.MediaBrowserCompat.MediaBrowserImplBase.this.dump();
                        }
                        if (android.support.v4.media.MediaBrowserCompat.MediaBrowserImplBase.MediaServiceConnection.this.isCurrent("onServiceDisconnected")) {
                            android.support.v4.media.MediaBrowserCompat.MediaBrowserImplBase.this.mServiceBinderWrapper = null;
                            android.support.v4.media.MediaBrowserCompat.MediaBrowserImplBase.this.mCallbacksMessenger = null;
                            android.support.v4.media.MediaBrowserCompat.MediaBrowserImplBase.this.mHandler.setCallbacksMessenger(null);
                            android.support.v4.media.MediaBrowserCompat.MediaBrowserImplBase.this.mState = 3;
                            android.support.v4.media.MediaBrowserCompat.MediaBrowserImplBase.this.mCallback.onConnectionSuspended();
                        }
                    }
                });
            }

            private void postOrRun(java.lang.Runnable r) {
                if (java.lang.Thread.currentThread() == android.support.v4.media.MediaBrowserCompat.MediaBrowserImplBase.this.mHandler.getLooper().getThread()) {
                    r.run();
                } else {
                    android.support.v4.media.MediaBrowserCompat.MediaBrowserImplBase.this.mHandler.post(r);
                }
            }

            /* access modifiers changed from: 0000 */
            public boolean isCurrent(java.lang.String funcName) {
                if (android.support.v4.media.MediaBrowserCompat.MediaBrowserImplBase.this.mServiceConnection == this) {
                    return true;
                }
                if (android.support.v4.media.MediaBrowserCompat.MediaBrowserImplBase.this.mState != 0) {
                    android.util.Log.i(android.support.v4.media.MediaBrowserCompat.TAG, funcName + " for " + android.support.v4.media.MediaBrowserCompat.MediaBrowserImplBase.this.mServiceComponent + " with mServiceConnection=" + android.support.v4.media.MediaBrowserCompat.MediaBrowserImplBase.this.mServiceConnection + " this=" + this);
                }
                return false;
            }
        }

        public MediaBrowserImplBase(android.content.Context context, android.content.ComponentName serviceComponent, android.support.v4.media.MediaBrowserCompat.ConnectionCallback callback, android.os.Bundle rootHints) {
            if (context == null) {
                throw new java.lang.IllegalArgumentException("context must not be null");
            } else if (serviceComponent == null) {
                throw new java.lang.IllegalArgumentException("service component must not be null");
            } else if (callback == null) {
                throw new java.lang.IllegalArgumentException("connection callback must not be null");
            } else {
                this.mContext = context;
                this.mServiceComponent = serviceComponent;
                this.mCallback = callback;
                this.mRootHints = rootHints == null ? null : new android.os.Bundle(rootHints);
            }
        }

        public void connect() {
            if (this.mState != 0) {
                throw new java.lang.IllegalStateException("connect() called while not disconnected (state=" + getStateLabel(this.mState) + ")");
            } else if (android.support.v4.media.MediaBrowserCompat.DEBUG && this.mServiceConnection != null) {
                throw new java.lang.RuntimeException("mServiceConnection should be null. Instead it is " + this.mServiceConnection);
            } else if (this.mServiceBinderWrapper != null) {
                throw new java.lang.RuntimeException("mServiceBinderWrapper should be null. Instead it is " + this.mServiceBinderWrapper);
            } else if (this.mCallbacksMessenger != null) {
                throw new java.lang.RuntimeException("mCallbacksMessenger should be null. Instead it is " + this.mCallbacksMessenger);
            } else {
                this.mState = 1;
                android.content.Intent intent = new android.content.Intent(android.support.v4.media.MediaBrowserServiceCompat.SERVICE_INTERFACE);
                intent.setComponent(this.mServiceComponent);
                final android.support.v4.media.MediaBrowserCompat.MediaBrowserImplBase.MediaServiceConnection mediaServiceConnection = new android.support.v4.media.MediaBrowserCompat.MediaBrowserImplBase.MediaServiceConnection();
                this.mServiceConnection = mediaServiceConnection;
                boolean bound = false;
                try {
                    bound = this.mContext.bindService(intent, this.mServiceConnection, 1);
                } catch (java.lang.Exception e) {
                    android.util.Log.e(android.support.v4.media.MediaBrowserCompat.TAG, "Failed binding to service " + this.mServiceComponent);
                }
                if (!bound) {
                    this.mHandler.post(new java.lang.Runnable() {
                        public void run() {
                            if (mediaServiceConnection == android.support.v4.media.MediaBrowserCompat.MediaBrowserImplBase.this.mServiceConnection) {
                                android.support.v4.media.MediaBrowserCompat.MediaBrowserImplBase.this.forceCloseConnection();
                                android.support.v4.media.MediaBrowserCompat.MediaBrowserImplBase.this.mCallback.onConnectionFailed();
                            }
                        }
                    });
                }
                if (android.support.v4.media.MediaBrowserCompat.DEBUG) {
                    android.util.Log.d(android.support.v4.media.MediaBrowserCompat.TAG, "connect...");
                    dump();
                }
            }
        }

        public void disconnect() {
            if (this.mCallbacksMessenger != null) {
                try {
                    this.mServiceBinderWrapper.disconnect(this.mCallbacksMessenger);
                } catch (android.os.RemoteException e) {
                    android.util.Log.w(android.support.v4.media.MediaBrowserCompat.TAG, "RemoteException during connect for " + this.mServiceComponent);
                }
            }
            forceCloseConnection();
            if (android.support.v4.media.MediaBrowserCompat.DEBUG) {
                android.util.Log.d(android.support.v4.media.MediaBrowserCompat.TAG, "disconnect...");
                dump();
            }
        }

        /* access modifiers changed from: 0000 */
        public void forceCloseConnection() {
            if (this.mServiceConnection != null) {
                this.mContext.unbindService(this.mServiceConnection);
            }
            this.mState = 0;
            this.mServiceConnection = null;
            this.mServiceBinderWrapper = null;
            this.mCallbacksMessenger = null;
            this.mHandler.setCallbacksMessenger(null);
            this.mRootId = null;
            this.mMediaSessionToken = null;
        }

        public boolean isConnected() {
            return this.mState == 2;
        }

        @android.support.annotation.NonNull
        public android.content.ComponentName getServiceComponent() {
            if (isConnected()) {
                return this.mServiceComponent;
            }
            throw new java.lang.IllegalStateException("getServiceComponent() called while not connected (state=" + this.mState + ")");
        }

        @android.support.annotation.NonNull
        public java.lang.String getRoot() {
            if (isConnected()) {
                return this.mRootId;
            }
            throw new java.lang.IllegalStateException("getRoot() called while not connected(state=" + getStateLabel(this.mState) + ")");
        }

        @android.support.annotation.Nullable
        public android.os.Bundle getExtras() {
            if (isConnected()) {
                return this.mExtras;
            }
            throw new java.lang.IllegalStateException("getExtras() called while not connected (state=" + getStateLabel(this.mState) + ")");
        }

        @android.support.annotation.NonNull
        public android.support.v4.media.session.MediaSessionCompat.Token getSessionToken() {
            if (isConnected()) {
                return this.mMediaSessionToken;
            }
            throw new java.lang.IllegalStateException("getSessionToken() called while not connected(state=" + this.mState + ")");
        }

        public void subscribe(@android.support.annotation.NonNull java.lang.String parentId, android.os.Bundle options, @android.support.annotation.NonNull android.support.v4.media.MediaBrowserCompat.SubscriptionCallback callback) {
            android.support.v4.media.MediaBrowserCompat.Subscription sub = (android.support.v4.media.MediaBrowserCompat.Subscription) this.mSubscriptions.get(parentId);
            if (sub == null) {
                sub = new android.support.v4.media.MediaBrowserCompat.Subscription();
                this.mSubscriptions.put(parentId, sub);
            }
            android.os.Bundle copiedOptions = options == null ? null : new android.os.Bundle(options);
            sub.putCallback(copiedOptions, callback);
            if (this.mState == 2) {
                try {
                    this.mServiceBinderWrapper.addSubscription(parentId, callback.mToken, copiedOptions, this.mCallbacksMessenger);
                } catch (android.os.RemoteException e) {
                    android.util.Log.d(android.support.v4.media.MediaBrowserCompat.TAG, "addSubscription failed with RemoteException parentId=" + parentId);
                }
            }
        }

        public void unsubscribe(@android.support.annotation.NonNull java.lang.String parentId, android.support.v4.media.MediaBrowserCompat.SubscriptionCallback callback) {
            android.support.v4.media.MediaBrowserCompat.Subscription sub = (android.support.v4.media.MediaBrowserCompat.Subscription) this.mSubscriptions.get(parentId);
            if (sub != null) {
                if (callback == null) {
                    try {
                        if (this.mState == 2) {
                            this.mServiceBinderWrapper.removeSubscription(parentId, null, this.mCallbacksMessenger);
                        }
                    } catch (android.os.RemoteException e) {
                        android.util.Log.d(android.support.v4.media.MediaBrowserCompat.TAG, "removeSubscription failed with RemoteException parentId=" + parentId);
                    }
                } else {
                    java.util.List<android.support.v4.media.MediaBrowserCompat.SubscriptionCallback> callbacks = sub.getCallbacks();
                    java.util.List<android.os.Bundle> optionsList = sub.getOptionsList();
                    for (int i = callbacks.size() - 1; i >= 0; i--) {
                        if (callbacks.get(i) == callback) {
                            if (this.mState == 2) {
                                this.mServiceBinderWrapper.removeSubscription(parentId, callback.mToken, this.mCallbacksMessenger);
                            }
                            callbacks.remove(i);
                            optionsList.remove(i);
                        }
                    }
                }
                if (sub.isEmpty() || callback == null) {
                    this.mSubscriptions.remove(parentId);
                }
            }
        }

        public void getItem(@android.support.annotation.NonNull final java.lang.String mediaId, @android.support.annotation.NonNull final android.support.v4.media.MediaBrowserCompat.ItemCallback cb) {
            if (android.text.TextUtils.isEmpty(mediaId)) {
                throw new java.lang.IllegalArgumentException("mediaId is empty");
            } else if (cb == null) {
                throw new java.lang.IllegalArgumentException("cb is null");
            } else if (this.mState != 2) {
                android.util.Log.i(android.support.v4.media.MediaBrowserCompat.TAG, "Not connected, unable to retrieve the MediaItem.");
                this.mHandler.post(new java.lang.Runnable() {
                    public void run() {
                        cb.onError(mediaId);
                    }
                });
            } else {
                try {
                    this.mServiceBinderWrapper.getMediaItem(mediaId, new android.support.v4.media.MediaBrowserCompat.ItemReceiver(mediaId, cb, this.mHandler), this.mCallbacksMessenger);
                } catch (android.os.RemoteException e) {
                    android.util.Log.i(android.support.v4.media.MediaBrowserCompat.TAG, "Remote error getting media item.");
                    this.mHandler.post(new java.lang.Runnable() {
                        public void run() {
                            cb.onError(mediaId);
                        }
                    });
                }
            }
        }

        public void search(@android.support.annotation.NonNull final java.lang.String query, final android.os.Bundle extras, @android.support.annotation.NonNull final android.support.v4.media.MediaBrowserCompat.SearchCallback callback) {
            if (!isConnected()) {
                android.util.Log.i(android.support.v4.media.MediaBrowserCompat.TAG, "Not connected, unable to search.");
                this.mHandler.post(new java.lang.Runnable() {
                    public void run() {
                        callback.onError(query, extras);
                    }
                });
                return;
            }
            try {
                this.mServiceBinderWrapper.search(query, extras, new android.support.v4.media.MediaBrowserCompat.SearchResultReceiver(query, extras, callback, this.mHandler), this.mCallbacksMessenger);
            } catch (android.os.RemoteException e) {
                android.util.Log.i(android.support.v4.media.MediaBrowserCompat.TAG, "Remote error searching items with query: " + query, e);
                this.mHandler.post(new java.lang.Runnable() {
                    public void run() {
                        callback.onError(query, extras);
                    }
                });
            }
        }

        public void onServiceConnected(android.os.Messenger callback, java.lang.String root, android.support.v4.media.session.MediaSessionCompat.Token session, android.os.Bundle extra) {
            if (isCurrent(callback, "onConnect")) {
                if (this.mState != 1) {
                    android.util.Log.w(android.support.v4.media.MediaBrowserCompat.TAG, "onConnect from service while mState=" + getStateLabel(this.mState) + "... ignoring");
                    return;
                }
                this.mRootId = root;
                this.mMediaSessionToken = session;
                this.mExtras = extra;
                this.mState = 2;
                if (android.support.v4.media.MediaBrowserCompat.DEBUG) {
                    android.util.Log.d(android.support.v4.media.MediaBrowserCompat.TAG, "ServiceCallbacks.onConnect...");
                    dump();
                }
                this.mCallback.onConnected();
                try {
                    for (java.util.Map.Entry<java.lang.String, android.support.v4.media.MediaBrowserCompat.Subscription> subscriptionEntry : this.mSubscriptions.entrySet()) {
                        java.lang.String id = (java.lang.String) subscriptionEntry.getKey();
                        android.support.v4.media.MediaBrowserCompat.Subscription sub = (android.support.v4.media.MediaBrowserCompat.Subscription) subscriptionEntry.getValue();
                        java.util.List<android.support.v4.media.MediaBrowserCompat.SubscriptionCallback> callbackList = sub.getCallbacks();
                        java.util.List<android.os.Bundle> optionsList = sub.getOptionsList();
                        for (int i = 0; i < callbackList.size(); i++) {
                            this.mServiceBinderWrapper.addSubscription(id, ((android.support.v4.media.MediaBrowserCompat.SubscriptionCallback) callbackList.get(i)).mToken, (android.os.Bundle) optionsList.get(i), this.mCallbacksMessenger);
                        }
                    }
                } catch (android.os.RemoteException e) {
                    android.util.Log.d(android.support.v4.media.MediaBrowserCompat.TAG, "addSubscription failed with RemoteException.");
                }
            }
        }

        public void onConnectionFailed(android.os.Messenger callback) {
            android.util.Log.e(android.support.v4.media.MediaBrowserCompat.TAG, "onConnectFailed for " + this.mServiceComponent);
            if (isCurrent(callback, "onConnectFailed")) {
                if (this.mState != 1) {
                    android.util.Log.w(android.support.v4.media.MediaBrowserCompat.TAG, "onConnect from service while mState=" + getStateLabel(this.mState) + "... ignoring");
                    return;
                }
                forceCloseConnection();
                this.mCallback.onConnectionFailed();
            }
        }

        public void onLoadChildren(android.os.Messenger callback, java.lang.String parentId, java.util.List list, android.os.Bundle options) {
            if (isCurrent(callback, "onLoadChildren")) {
                if (android.support.v4.media.MediaBrowserCompat.DEBUG) {
                    android.util.Log.d(android.support.v4.media.MediaBrowserCompat.TAG, "onLoadChildren for " + this.mServiceComponent + " id=" + parentId);
                }
                android.support.v4.media.MediaBrowserCompat.Subscription subscription = (android.support.v4.media.MediaBrowserCompat.Subscription) this.mSubscriptions.get(parentId);
                if (subscription != null) {
                    android.support.v4.media.MediaBrowserCompat.SubscriptionCallback subscriptionCallback = subscription.getCallback(options);
                    if (subscriptionCallback == null) {
                        return;
                    }
                    if (options == null) {
                        if (list == null) {
                            subscriptionCallback.onError(parentId);
                        } else {
                            subscriptionCallback.onChildrenLoaded(parentId, list);
                        }
                    } else if (list == null) {
                        subscriptionCallback.onError(parentId, options);
                    } else {
                        subscriptionCallback.onChildrenLoaded(parentId, list, options);
                    }
                } else if (android.support.v4.media.MediaBrowserCompat.DEBUG) {
                    android.util.Log.d(android.support.v4.media.MediaBrowserCompat.TAG, "onLoadChildren for id that isn't subscribed id=" + parentId);
                }
            }
        }

        private static java.lang.String getStateLabel(int state) {
            switch (state) {
                case 0:
                    return "CONNECT_STATE_DISCONNECTED";
                case 1:
                    return "CONNECT_STATE_CONNECTING";
                case 2:
                    return "CONNECT_STATE_CONNECTED";
                case 3:
                    return "CONNECT_STATE_SUSPENDED";
                default:
                    return "UNKNOWN/" + state;
            }
        }

        private boolean isCurrent(android.os.Messenger callback, java.lang.String funcName) {
            if (this.mCallbacksMessenger == callback) {
                return true;
            }
            if (this.mState != 0) {
                android.util.Log.i(android.support.v4.media.MediaBrowserCompat.TAG, funcName + " for " + this.mServiceComponent + " with mCallbacksMessenger=" + this.mCallbacksMessenger + " this=" + this);
            }
            return false;
        }

        /* access modifiers changed from: 0000 */
        public void dump() {
            android.util.Log.d(android.support.v4.media.MediaBrowserCompat.TAG, "MediaBrowserCompat...");
            android.util.Log.d(android.support.v4.media.MediaBrowserCompat.TAG, "  mServiceComponent=" + this.mServiceComponent);
            android.util.Log.d(android.support.v4.media.MediaBrowserCompat.TAG, "  mCallback=" + this.mCallback);
            android.util.Log.d(android.support.v4.media.MediaBrowserCompat.TAG, "  mRootHints=" + this.mRootHints);
            android.util.Log.d(android.support.v4.media.MediaBrowserCompat.TAG, "  mState=" + getStateLabel(this.mState));
            android.util.Log.d(android.support.v4.media.MediaBrowserCompat.TAG, "  mServiceConnection=" + this.mServiceConnection);
            android.util.Log.d(android.support.v4.media.MediaBrowserCompat.TAG, "  mServiceBinderWrapper=" + this.mServiceBinderWrapper);
            android.util.Log.d(android.support.v4.media.MediaBrowserCompat.TAG, "  mCallbacksMessenger=" + this.mCallbacksMessenger);
            android.util.Log.d(android.support.v4.media.MediaBrowserCompat.TAG, "  mRootId=" + this.mRootId);
            android.util.Log.d(android.support.v4.media.MediaBrowserCompat.TAG, "  mMediaSessionToken=" + this.mMediaSessionToken);
        }
    }

    interface MediaBrowserServiceCallbackImpl {
        void onConnectionFailed(android.os.Messenger messenger);

        void onLoadChildren(android.os.Messenger messenger, java.lang.String str, java.util.List list, android.os.Bundle bundle);

        void onServiceConnected(android.os.Messenger messenger, java.lang.String str, android.support.v4.media.session.MediaSessionCompat.Token token, android.os.Bundle bundle);
    }

    public static class MediaItem implements android.os.Parcelable {
        public static final android.os.Parcelable.Creator<android.support.v4.media.MediaBrowserCompat.MediaItem> CREATOR = new android.os.Parcelable.Creator<android.support.v4.media.MediaBrowserCompat.MediaItem>() {
            public android.support.v4.media.MediaBrowserCompat.MediaItem createFromParcel(android.os.Parcel in) {
                return new android.support.v4.media.MediaBrowserCompat.MediaItem(in);
            }

            public android.support.v4.media.MediaBrowserCompat.MediaItem[] newArray(int size) {
                return new android.support.v4.media.MediaBrowserCompat.MediaItem[size];
            }
        };
        public static final int FLAG_BROWSABLE = 1;
        public static final int FLAG_PLAYABLE = 2;
        private final android.support.v4.media.MediaDescriptionCompat mDescription;
        private final int mFlags;

        @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
        @java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.SOURCE)
        public @interface Flags {
        }

        public static android.support.v4.media.MediaBrowserCompat.MediaItem fromMediaItem(java.lang.Object itemObj) {
            if (itemObj == null || android.os.Build.VERSION.SDK_INT < 21) {
                return null;
            }
            return new android.support.v4.media.MediaBrowserCompat.MediaItem(android.support.v4.media.MediaDescriptionCompat.fromMediaDescription(android.support.v4.media.MediaBrowserCompatApi21.MediaItem.getDescription(itemObj)), android.support.v4.media.MediaBrowserCompatApi21.MediaItem.getFlags(itemObj));
        }

        public static java.util.List<android.support.v4.media.MediaBrowserCompat.MediaItem> fromMediaItemList(java.util.List<?> itemList) {
            if (itemList == null || android.os.Build.VERSION.SDK_INT < 21) {
                return null;
            }
            java.util.List<android.support.v4.media.MediaBrowserCompat.MediaItem> items = new java.util.ArrayList<>(itemList.size());
            for (java.lang.Object itemObj : itemList) {
                items.add(fromMediaItem(itemObj));
            }
            return items;
        }

        public MediaItem(@android.support.annotation.NonNull android.support.v4.media.MediaDescriptionCompat description, int flags) {
            if (description == null) {
                throw new java.lang.IllegalArgumentException("description cannot be null");
            } else if (android.text.TextUtils.isEmpty(description.getMediaId())) {
                throw new java.lang.IllegalArgumentException("description must have a non-empty media id");
            } else {
                this.mFlags = flags;
                this.mDescription = description;
            }
        }

        MediaItem(android.os.Parcel in) {
            this.mFlags = in.readInt();
            this.mDescription = (android.support.v4.media.MediaDescriptionCompat) android.support.v4.media.MediaDescriptionCompat.CREATOR.createFromParcel(in);
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(android.os.Parcel out, int flags) {
            out.writeInt(this.mFlags);
            this.mDescription.writeToParcel(out, flags);
        }

        public java.lang.String toString() {
            java.lang.StringBuilder sb = new java.lang.StringBuilder("MediaItem{");
            sb.append("mFlags=").append(this.mFlags);
            sb.append(", mDescription=").append(this.mDescription);
            sb.append(ch.qos.logback.core.CoreConstants.CURLY_RIGHT);
            return sb.toString();
        }

        public int getFlags() {
            return this.mFlags;
        }

        public boolean isBrowsable() {
            return (this.mFlags & 1) != 0;
        }

        public boolean isPlayable() {
            return (this.mFlags & 2) != 0;
        }

        @android.support.annotation.NonNull
        public android.support.v4.media.MediaDescriptionCompat getDescription() {
            return this.mDescription;
        }

        @android.support.annotation.Nullable
        public java.lang.String getMediaId() {
            return this.mDescription.getMediaId();
        }
    }

    public static abstract class SearchCallback {
        public void onSearchResult(@android.support.annotation.NonNull java.lang.String query, android.os.Bundle extras, @android.support.annotation.NonNull java.util.List<android.support.v4.media.MediaBrowserCompat.MediaItem> list) {
        }

        public void onError(@android.support.annotation.NonNull java.lang.String query, android.os.Bundle extras) {
        }
    }

    private static class SearchResultReceiver extends android.support.v4.os.ResultReceiver {
        private final android.support.v4.media.MediaBrowserCompat.SearchCallback mCallback;
        private final android.os.Bundle mExtras;
        private final java.lang.String mQuery;

        SearchResultReceiver(java.lang.String query, android.os.Bundle extras, android.support.v4.media.MediaBrowserCompat.SearchCallback callback, android.os.Handler handler) {
            super(handler);
            this.mQuery = query;
            this.mExtras = extras;
            this.mCallback = callback;
        }

        /* access modifiers changed from: protected */
        public void onReceiveResult(int resultCode, android.os.Bundle resultData) {
            if (resultCode != 0 || resultData == null || !resultData.containsKey(android.support.v4.media.MediaBrowserServiceCompat.KEY_SEARCH_RESULTS)) {
                this.mCallback.onError(this.mQuery, this.mExtras);
                return;
            }
            android.os.Parcelable[] items = resultData.getParcelableArray(android.support.v4.media.MediaBrowserServiceCompat.KEY_SEARCH_RESULTS);
            java.util.List<android.support.v4.media.MediaBrowserCompat.MediaItem> results = null;
            if (items != null) {
                results = new java.util.ArrayList<>();
                for (android.os.Parcelable item : items) {
                    results.add((android.support.v4.media.MediaBrowserCompat.MediaItem) item);
                }
            }
            this.mCallback.onSearchResult(this.mQuery, this.mExtras, results);
        }
    }

    private static class ServiceBinderWrapper {
        private android.os.Messenger mMessenger;
        private android.os.Bundle mRootHints;

        public ServiceBinderWrapper(android.os.IBinder target, android.os.Bundle rootHints) {
            this.mMessenger = new android.os.Messenger(target);
            this.mRootHints = rootHints;
        }

        /* access modifiers changed from: 0000 */
        public void connect(android.content.Context context, android.os.Messenger callbacksMessenger) throws android.os.RemoteException {
            android.os.Bundle data = new android.os.Bundle();
            data.putString(android.support.v4.media.MediaBrowserProtocol.DATA_PACKAGE_NAME, context.getPackageName());
            data.putBundle(android.support.v4.media.MediaBrowserProtocol.DATA_ROOT_HINTS, this.mRootHints);
            sendRequest(1, data, callbacksMessenger);
        }

        /* access modifiers changed from: 0000 */
        public void disconnect(android.os.Messenger callbacksMessenger) throws android.os.RemoteException {
            sendRequest(2, null, callbacksMessenger);
        }

        /* access modifiers changed from: 0000 */
        public void addSubscription(java.lang.String parentId, android.os.IBinder callbackToken, android.os.Bundle options, android.os.Messenger callbacksMessenger) throws android.os.RemoteException {
            android.os.Bundle data = new android.os.Bundle();
            data.putString(android.support.v4.media.MediaBrowserProtocol.DATA_MEDIA_ITEM_ID, parentId);
            android.support.v4.app.BundleCompat.putBinder(data, android.support.v4.media.MediaBrowserProtocol.DATA_CALLBACK_TOKEN, callbackToken);
            data.putBundle(android.support.v4.media.MediaBrowserProtocol.DATA_OPTIONS, options);
            sendRequest(3, data, callbacksMessenger);
        }

        /* access modifiers changed from: 0000 */
        public void removeSubscription(java.lang.String parentId, android.os.IBinder callbackToken, android.os.Messenger callbacksMessenger) throws android.os.RemoteException {
            android.os.Bundle data = new android.os.Bundle();
            data.putString(android.support.v4.media.MediaBrowserProtocol.DATA_MEDIA_ITEM_ID, parentId);
            android.support.v4.app.BundleCompat.putBinder(data, android.support.v4.media.MediaBrowserProtocol.DATA_CALLBACK_TOKEN, callbackToken);
            sendRequest(4, data, callbacksMessenger);
        }

        /* access modifiers changed from: 0000 */
        public void getMediaItem(java.lang.String mediaId, android.support.v4.os.ResultReceiver receiver, android.os.Messenger callbacksMessenger) throws android.os.RemoteException {
            android.os.Bundle data = new android.os.Bundle();
            data.putString(android.support.v4.media.MediaBrowserProtocol.DATA_MEDIA_ITEM_ID, mediaId);
            data.putParcelable(android.support.v4.media.MediaBrowserProtocol.DATA_RESULT_RECEIVER, receiver);
            sendRequest(5, data, callbacksMessenger);
        }

        /* access modifiers changed from: 0000 */
        public void registerCallbackMessenger(android.os.Messenger callbackMessenger) throws android.os.RemoteException {
            android.os.Bundle data = new android.os.Bundle();
            data.putBundle(android.support.v4.media.MediaBrowserProtocol.DATA_ROOT_HINTS, this.mRootHints);
            sendRequest(6, data, callbackMessenger);
        }

        /* access modifiers changed from: 0000 */
        public void unregisterCallbackMessenger(android.os.Messenger callbackMessenger) throws android.os.RemoteException {
            sendRequest(7, null, callbackMessenger);
        }

        /* access modifiers changed from: 0000 */
        public void search(java.lang.String query, android.os.Bundle extras, android.support.v4.os.ResultReceiver receiver, android.os.Messenger callbacksMessenger) throws android.os.RemoteException {
            android.os.Bundle data = new android.os.Bundle();
            data.putString(android.support.v4.media.MediaBrowserProtocol.DATA_SEARCH_QUERY, query);
            data.putBundle(android.support.v4.media.MediaBrowserProtocol.DATA_SEARCH_EXTRAS, extras);
            data.putParcelable(android.support.v4.media.MediaBrowserProtocol.DATA_RESULT_RECEIVER, receiver);
            sendRequest(8, data, callbacksMessenger);
        }

        private void sendRequest(int what, android.os.Bundle data, android.os.Messenger cbMessenger) throws android.os.RemoteException {
            android.os.Message msg = android.os.Message.obtain();
            msg.what = what;
            msg.arg1 = 1;
            msg.setData(data);
            msg.replyTo = cbMessenger;
            this.mMessenger.send(msg);
        }
    }

    private static class Subscription {
        private final java.util.List<android.support.v4.media.MediaBrowserCompat.SubscriptionCallback> mCallbacks = new java.util.ArrayList();
        private final java.util.List<android.os.Bundle> mOptionsList = new java.util.ArrayList();

        public boolean isEmpty() {
            return this.mCallbacks.isEmpty();
        }

        public java.util.List<android.os.Bundle> getOptionsList() {
            return this.mOptionsList;
        }

        public java.util.List<android.support.v4.media.MediaBrowserCompat.SubscriptionCallback> getCallbacks() {
            return this.mCallbacks;
        }

        public android.support.v4.media.MediaBrowserCompat.SubscriptionCallback getCallback(android.os.Bundle options) {
            for (int i = 0; i < this.mOptionsList.size(); i++) {
                if (android.support.v4.media.MediaBrowserCompatUtils.areSameOptions((android.os.Bundle) this.mOptionsList.get(i), options)) {
                    return (android.support.v4.media.MediaBrowserCompat.SubscriptionCallback) this.mCallbacks.get(i);
                }
            }
            return null;
        }

        public void putCallback(android.os.Bundle options, android.support.v4.media.MediaBrowserCompat.SubscriptionCallback callback) {
            for (int i = 0; i < this.mOptionsList.size(); i++) {
                if (android.support.v4.media.MediaBrowserCompatUtils.areSameOptions((android.os.Bundle) this.mOptionsList.get(i), options)) {
                    this.mCallbacks.set(i, callback);
                    return;
                }
            }
            this.mCallbacks.add(callback);
            this.mOptionsList.add(options);
        }
    }

    public static abstract class SubscriptionCallback {
        /* access modifiers changed from: private */
        public final java.lang.Object mSubscriptionCallbackObj;
        java.lang.ref.WeakReference<android.support.v4.media.MediaBrowserCompat.Subscription> mSubscriptionRef;
        /* access modifiers changed from: private */
        public final android.os.IBinder mToken;

        private class StubApi21 implements android.support.v4.media.MediaBrowserCompatApi21.SubscriptionCallback {
            StubApi21() {
            }

            public void onChildrenLoaded(@android.support.annotation.NonNull java.lang.String parentId, java.util.List<?> children) {
                android.support.v4.media.MediaBrowserCompat.Subscription sub = android.support.v4.media.MediaBrowserCompat.SubscriptionCallback.this.mSubscriptionRef == null ? null : (android.support.v4.media.MediaBrowserCompat.Subscription) android.support.v4.media.MediaBrowserCompat.SubscriptionCallback.this.mSubscriptionRef.get();
                if (sub == null) {
                    android.support.v4.media.MediaBrowserCompat.SubscriptionCallback.this.onChildrenLoaded(parentId, android.support.v4.media.MediaBrowserCompat.MediaItem.fromMediaItemList(children));
                    return;
                }
                java.util.List<android.support.v4.media.MediaBrowserCompat.MediaItem> itemList = android.support.v4.media.MediaBrowserCompat.MediaItem.fromMediaItemList(children);
                java.util.List<android.support.v4.media.MediaBrowserCompat.SubscriptionCallback> callbacks = sub.getCallbacks();
                java.util.List<android.os.Bundle> optionsList = sub.getOptionsList();
                for (int i = 0; i < callbacks.size(); i++) {
                    android.os.Bundle options = (android.os.Bundle) optionsList.get(i);
                    if (options == null) {
                        android.support.v4.media.MediaBrowserCompat.SubscriptionCallback.this.onChildrenLoaded(parentId, itemList);
                    } else {
                        android.support.v4.media.MediaBrowserCompat.SubscriptionCallback.this.onChildrenLoaded(parentId, applyOptions(itemList, options), options);
                    }
                }
            }

            public void onError(@android.support.annotation.NonNull java.lang.String parentId) {
                android.support.v4.media.MediaBrowserCompat.SubscriptionCallback.this.onError(parentId);
            }

            /* access modifiers changed from: 0000 */
            public java.util.List<android.support.v4.media.MediaBrowserCompat.MediaItem> applyOptions(java.util.List<android.support.v4.media.MediaBrowserCompat.MediaItem> list, android.os.Bundle options) {
                if (list == null) {
                    return null;
                }
                int page = options.getInt(android.support.v4.media.MediaBrowserCompat.EXTRA_PAGE, -1);
                int pageSize = options.getInt(android.support.v4.media.MediaBrowserCompat.EXTRA_PAGE_SIZE, -1);
                if (page == -1 && pageSize == -1) {
                    return list;
                }
                int fromIndex = pageSize * page;
                int toIndex = fromIndex + pageSize;
                if (page < 0 || pageSize < 1 || fromIndex >= list.size()) {
                    return java.util.Collections.EMPTY_LIST;
                }
                if (toIndex > list.size()) {
                    toIndex = list.size();
                }
                return list.subList(fromIndex, toIndex);
            }
        }

        private class StubApi24 extends android.support.v4.media.MediaBrowserCompat.SubscriptionCallback.StubApi21 implements android.support.v4.media.MediaBrowserCompatApi24.SubscriptionCallback {
            StubApi24() {
                super();
            }

            public void onChildrenLoaded(@android.support.annotation.NonNull java.lang.String parentId, java.util.List<?> children, @android.support.annotation.NonNull android.os.Bundle options) {
                android.support.v4.media.MediaBrowserCompat.SubscriptionCallback.this.onChildrenLoaded(parentId, android.support.v4.media.MediaBrowserCompat.MediaItem.fromMediaItemList(children), options);
            }

            public void onError(@android.support.annotation.NonNull java.lang.String parentId, @android.support.annotation.NonNull android.os.Bundle options) {
                android.support.v4.media.MediaBrowserCompat.SubscriptionCallback.this.onError(parentId, options);
            }
        }

        public SubscriptionCallback() {
            if (android.os.Build.VERSION.SDK_INT >= 26 || android.support.v4.os.BuildCompat.isAtLeastO()) {
                this.mSubscriptionCallbackObj = android.support.v4.media.MediaBrowserCompatApi24.createSubscriptionCallback(new android.support.v4.media.MediaBrowserCompat.SubscriptionCallback.StubApi24());
                this.mToken = null;
            } else if (android.os.Build.VERSION.SDK_INT >= 21) {
                this.mSubscriptionCallbackObj = android.support.v4.media.MediaBrowserCompatApi21.createSubscriptionCallback(new android.support.v4.media.MediaBrowserCompat.SubscriptionCallback.StubApi21());
                this.mToken = new android.os.Binder();
            } else {
                this.mSubscriptionCallbackObj = null;
                this.mToken = new android.os.Binder();
            }
        }

        public void onChildrenLoaded(@android.support.annotation.NonNull java.lang.String parentId, @android.support.annotation.NonNull java.util.List<android.support.v4.media.MediaBrowserCompat.MediaItem> list) {
        }

        public void onChildrenLoaded(@android.support.annotation.NonNull java.lang.String parentId, @android.support.annotation.NonNull java.util.List<android.support.v4.media.MediaBrowserCompat.MediaItem> list, @android.support.annotation.NonNull android.os.Bundle options) {
        }

        public void onError(@android.support.annotation.NonNull java.lang.String parentId) {
        }

        public void onError(@android.support.annotation.NonNull java.lang.String parentId, @android.support.annotation.NonNull android.os.Bundle options) {
        }

        /* access modifiers changed from: private */
        public void setSubscription(android.support.v4.media.MediaBrowserCompat.Subscription subscription) {
            this.mSubscriptionRef = new java.lang.ref.WeakReference<>(subscription);
        }
    }

    public MediaBrowserCompat(android.content.Context context, android.content.ComponentName serviceComponent, android.support.v4.media.MediaBrowserCompat.ConnectionCallback callback, android.os.Bundle rootHints) {
        if (android.os.Build.VERSION.SDK_INT >= 26 || android.support.v4.os.BuildCompat.isAtLeastO()) {
            this.mImpl = new android.support.v4.media.MediaBrowserCompat.MediaBrowserImplApi24(context, serviceComponent, callback, rootHints);
        } else if (android.os.Build.VERSION.SDK_INT >= 23) {
            this.mImpl = new android.support.v4.media.MediaBrowserCompat.MediaBrowserImplApi23(context, serviceComponent, callback, rootHints);
        } else if (android.os.Build.VERSION.SDK_INT >= 21) {
            this.mImpl = new android.support.v4.media.MediaBrowserCompat.MediaBrowserImplApi21(context, serviceComponent, callback, rootHints);
        } else {
            this.mImpl = new android.support.v4.media.MediaBrowserCompat.MediaBrowserImplBase(context, serviceComponent, callback, rootHints);
        }
    }

    public void connect() {
        this.mImpl.connect();
    }

    public void disconnect() {
        this.mImpl.disconnect();
    }

    public boolean isConnected() {
        return this.mImpl.isConnected();
    }

    @android.support.annotation.NonNull
    public android.content.ComponentName getServiceComponent() {
        return this.mImpl.getServiceComponent();
    }

    @android.support.annotation.NonNull
    public java.lang.String getRoot() {
        return this.mImpl.getRoot();
    }

    @android.support.annotation.Nullable
    public android.os.Bundle getExtras() {
        return this.mImpl.getExtras();
    }

    @android.support.annotation.NonNull
    public android.support.v4.media.session.MediaSessionCompat.Token getSessionToken() {
        return this.mImpl.getSessionToken();
    }

    public void subscribe(@android.support.annotation.NonNull java.lang.String parentId, @android.support.annotation.NonNull android.support.v4.media.MediaBrowserCompat.SubscriptionCallback callback) {
        if (android.text.TextUtils.isEmpty(parentId)) {
            throw new java.lang.IllegalArgumentException("parentId is empty");
        } else if (callback == null) {
            throw new java.lang.IllegalArgumentException("callback is null");
        } else {
            this.mImpl.subscribe(parentId, null, callback);
        }
    }

    public void subscribe(@android.support.annotation.NonNull java.lang.String parentId, @android.support.annotation.NonNull android.os.Bundle options, @android.support.annotation.NonNull android.support.v4.media.MediaBrowserCompat.SubscriptionCallback callback) {
        if (android.text.TextUtils.isEmpty(parentId)) {
            throw new java.lang.IllegalArgumentException("parentId is empty");
        } else if (callback == null) {
            throw new java.lang.IllegalArgumentException("callback is null");
        } else if (options == null) {
            throw new java.lang.IllegalArgumentException("options are null");
        } else {
            this.mImpl.subscribe(parentId, options, callback);
        }
    }

    public void unsubscribe(@android.support.annotation.NonNull java.lang.String parentId) {
        if (android.text.TextUtils.isEmpty(parentId)) {
            throw new java.lang.IllegalArgumentException("parentId is empty");
        }
        this.mImpl.unsubscribe(parentId, null);
    }

    public void unsubscribe(@android.support.annotation.NonNull java.lang.String parentId, @android.support.annotation.NonNull android.support.v4.media.MediaBrowserCompat.SubscriptionCallback callback) {
        if (android.text.TextUtils.isEmpty(parentId)) {
            throw new java.lang.IllegalArgumentException("parentId is empty");
        } else if (callback == null) {
            throw new java.lang.IllegalArgumentException("callback is null");
        } else {
            this.mImpl.unsubscribe(parentId, callback);
        }
    }

    public void getItem(@android.support.annotation.NonNull java.lang.String mediaId, @android.support.annotation.NonNull android.support.v4.media.MediaBrowserCompat.ItemCallback cb) {
        this.mImpl.getItem(mediaId, cb);
    }

    public void search(@android.support.annotation.NonNull java.lang.String query, android.os.Bundle extras, @android.support.annotation.NonNull android.support.v4.media.MediaBrowserCompat.SearchCallback callback) {
        if (android.text.TextUtils.isEmpty(query)) {
            throw new java.lang.IllegalArgumentException("query cannot be empty");
        } else if (callback == null) {
            throw new java.lang.IllegalArgumentException("callback cannot be null");
        } else {
            this.mImpl.search(query, extras, callback);
        }
    }
}
