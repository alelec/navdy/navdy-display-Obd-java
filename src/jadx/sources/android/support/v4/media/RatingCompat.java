package android.support.v4.media;

public final class RatingCompat implements android.os.Parcelable {
    public static final android.os.Parcelable.Creator<android.support.v4.media.RatingCompat> CREATOR = new android.os.Parcelable.Creator<android.support.v4.media.RatingCompat>() {
        public android.support.v4.media.RatingCompat createFromParcel(android.os.Parcel p) {
            return new android.support.v4.media.RatingCompat(p.readInt(), p.readFloat());
        }

        public android.support.v4.media.RatingCompat[] newArray(int size) {
            return new android.support.v4.media.RatingCompat[size];
        }
    };
    public static final int RATING_3_STARS = 3;
    public static final int RATING_4_STARS = 4;
    public static final int RATING_5_STARS = 5;
    public static final int RATING_HEART = 1;
    public static final int RATING_NONE = 0;
    private static final float RATING_NOT_RATED = -1.0f;
    public static final int RATING_PERCENTAGE = 6;
    public static final int RATING_THUMB_UP_DOWN = 2;
    private static final java.lang.String TAG = "Rating";
    private java.lang.Object mRatingObj;
    private final int mRatingStyle;
    private final float mRatingValue;

    @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
    @java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.SOURCE)
    public @interface StarStyle {
    }

    @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
    @java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.SOURCE)
    public @interface Style {
    }

    RatingCompat(int ratingStyle, float rating) {
        this.mRatingStyle = ratingStyle;
        this.mRatingValue = rating;
    }

    public java.lang.String toString() {
        java.lang.String valueOf;
        java.lang.StringBuilder append = new java.lang.StringBuilder().append("Rating:style=").append(this.mRatingStyle).append(" rating=");
        if (this.mRatingValue < 0.0f) {
            valueOf = "unrated";
        } else {
            valueOf = java.lang.String.valueOf(this.mRatingValue);
        }
        return append.append(valueOf).toString();
    }

    public int describeContents() {
        return this.mRatingStyle;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeInt(this.mRatingStyle);
        dest.writeFloat(this.mRatingValue);
    }

    public static android.support.v4.media.RatingCompat newUnratedRating(int ratingStyle) {
        switch (ratingStyle) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
                return new android.support.v4.media.RatingCompat(ratingStyle, RATING_NOT_RATED);
            default:
                return null;
        }
    }

    public static android.support.v4.media.RatingCompat newHeartRating(boolean hasHeart) {
        return new android.support.v4.media.RatingCompat(1, hasHeart ? 1.0f : 0.0f);
    }

    public static android.support.v4.media.RatingCompat newThumbRating(boolean thumbIsUp) {
        return new android.support.v4.media.RatingCompat(2, thumbIsUp ? 1.0f : 0.0f);
    }

    public static android.support.v4.media.RatingCompat newStarRating(int starRatingStyle, float starRating) {
        float maxRating;
        switch (starRatingStyle) {
            case 3:
                maxRating = 3.0f;
                break;
            case 4:
                maxRating = 4.0f;
                break;
            case 5:
                maxRating = 5.0f;
                break;
            default:
                android.util.Log.e(TAG, "Invalid rating style (" + starRatingStyle + ") for a star rating");
                return null;
        }
        if (starRating >= 0.0f && starRating <= maxRating) {
            return new android.support.v4.media.RatingCompat(starRatingStyle, starRating);
        }
        android.util.Log.e(TAG, "Trying to set out of range star-based rating");
        return null;
    }

    public static android.support.v4.media.RatingCompat newPercentageRating(float percent) {
        if (percent >= 0.0f && percent <= 100.0f) {
            return new android.support.v4.media.RatingCompat(6, percent);
        }
        android.util.Log.e(TAG, "Invalid percentage-based rating value");
        return null;
    }

    public boolean isRated() {
        return this.mRatingValue >= 0.0f;
    }

    public int getRatingStyle() {
        return this.mRatingStyle;
    }

    public boolean hasHeart() {
        boolean z = true;
        if (this.mRatingStyle != 1) {
            return false;
        }
        if (this.mRatingValue != 1.0f) {
            z = false;
        }
        return z;
    }

    public boolean isThumbUp() {
        if (this.mRatingStyle == 2 && this.mRatingValue == 1.0f) {
            return true;
        }
        return false;
    }

    public float getStarRating() {
        switch (this.mRatingStyle) {
            case 3:
            case 4:
            case 5:
                if (isRated()) {
                    return this.mRatingValue;
                }
                break;
        }
        return RATING_NOT_RATED;
    }

    public float getPercentRating() {
        if (this.mRatingStyle != 6 || !isRated()) {
            return RATING_NOT_RATED;
        }
        return this.mRatingValue;
    }

    public static android.support.v4.media.RatingCompat fromRating(java.lang.Object ratingObj) {
        android.support.v4.media.RatingCompat rating = null;
        if (ratingObj != null && android.os.Build.VERSION.SDK_INT >= 19) {
            int ratingStyle = android.support.v4.media.RatingCompatKitkat.getRatingStyle(ratingObj);
            if (android.support.v4.media.RatingCompatKitkat.isRated(ratingObj)) {
                switch (ratingStyle) {
                    case 1:
                        rating = newHeartRating(android.support.v4.media.RatingCompatKitkat.hasHeart(ratingObj));
                        break;
                    case 2:
                        rating = newThumbRating(android.support.v4.media.RatingCompatKitkat.isThumbUp(ratingObj));
                        break;
                    case 3:
                    case 4:
                    case 5:
                        rating = newStarRating(ratingStyle, android.support.v4.media.RatingCompatKitkat.getStarRating(ratingObj));
                        break;
                    case 6:
                        rating = newPercentageRating(android.support.v4.media.RatingCompatKitkat.getPercentRating(ratingObj));
                        break;
                }
            } else {
                rating = newUnratedRating(ratingStyle);
            }
            rating.mRatingObj = ratingObj;
        }
        return rating;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        return null;
     */
    public java.lang.Object getRating() {
        if (this.mRatingObj != null || android.os.Build.VERSION.SDK_INT < 19) {
            return this.mRatingObj;
        }
        if (isRated()) {
            switch (this.mRatingStyle) {
                case 1:
                    this.mRatingObj = android.support.v4.media.RatingCompatKitkat.newHeartRating(hasHeart());
                    break;
                case 2:
                    this.mRatingObj = android.support.v4.media.RatingCompatKitkat.newThumbRating(isThumbUp());
                    break;
                case 3:
                case 4:
                case 5:
                    this.mRatingObj = android.support.v4.media.RatingCompatKitkat.newStarRating(this.mRatingStyle, getStarRating());
                    break;
                case 6:
                    this.mRatingObj = android.support.v4.media.RatingCompatKitkat.newPercentageRating(getPercentRating());
                    break;
            }
        } else {
            this.mRatingObj = android.support.v4.media.RatingCompatKitkat.newUnratedRating(this.mRatingStyle);
        }
        return this.mRatingObj;
    }
}
