package android.support.v4.media;

@android.annotation.TargetApi(21)
@android.support.annotation.RequiresApi(21)
class MediaBrowserCompatApi21 {
    static final java.lang.String NULL_MEDIA_ITEM_ID = "android.support.v4.media.MediaBrowserCompat.NULL_MEDIA_ITEM";

    interface ConnectionCallback {
        void onConnected();

        void onConnectionFailed();

        void onConnectionSuspended();
    }

    static class ConnectionCallbackProxy<T extends android.support.v4.media.MediaBrowserCompatApi21.ConnectionCallback> extends android.media.browse.MediaBrowser.ConnectionCallback {
        protected final T mConnectionCallback;

        public ConnectionCallbackProxy(T connectionCallback) {
            this.mConnectionCallback = connectionCallback;
        }

        public void onConnected() {
            this.mConnectionCallback.onConnected();
        }

        public void onConnectionSuspended() {
            this.mConnectionCallback.onConnectionSuspended();
        }

        public void onConnectionFailed() {
            this.mConnectionCallback.onConnectionFailed();
        }
    }

    static class MediaItem {
        MediaItem() {
        }

        public static int getFlags(java.lang.Object itemObj) {
            return ((android.media.browse.MediaBrowser.MediaItem) itemObj).getFlags();
        }

        public static java.lang.Object getDescription(java.lang.Object itemObj) {
            return ((android.media.browse.MediaBrowser.MediaItem) itemObj).getDescription();
        }
    }

    interface SubscriptionCallback {
        void onChildrenLoaded(@android.support.annotation.NonNull java.lang.String str, java.util.List<?> list);

        void onError(@android.support.annotation.NonNull java.lang.String str);
    }

    static class SubscriptionCallbackProxy<T extends android.support.v4.media.MediaBrowserCompatApi21.SubscriptionCallback> extends android.media.browse.MediaBrowser.SubscriptionCallback {
        protected final T mSubscriptionCallback;

        public SubscriptionCallbackProxy(T callback) {
            this.mSubscriptionCallback = callback;
        }

        public void onChildrenLoaded(@android.support.annotation.NonNull java.lang.String parentId, java.util.List<android.media.browse.MediaBrowser.MediaItem> children) {
            this.mSubscriptionCallback.onChildrenLoaded(parentId, children);
        }

        public void onError(@android.support.annotation.NonNull java.lang.String parentId) {
            this.mSubscriptionCallback.onError(parentId);
        }
    }

    MediaBrowserCompatApi21() {
    }

    public static java.lang.Object createConnectionCallback(android.support.v4.media.MediaBrowserCompatApi21.ConnectionCallback callback) {
        return new android.support.v4.media.MediaBrowserCompatApi21.ConnectionCallbackProxy(callback);
    }

    public static java.lang.Object createBrowser(android.content.Context context, android.content.ComponentName serviceComponent, java.lang.Object callback, android.os.Bundle rootHints) {
        return new android.media.browse.MediaBrowser(context, serviceComponent, (android.media.browse.MediaBrowser.ConnectionCallback) callback, rootHints);
    }

    public static void connect(java.lang.Object browserObj) {
        ((android.media.browse.MediaBrowser) browserObj).connect();
    }

    public static void disconnect(java.lang.Object browserObj) {
        ((android.media.browse.MediaBrowser) browserObj).disconnect();
    }

    public static boolean isConnected(java.lang.Object browserObj) {
        return ((android.media.browse.MediaBrowser) browserObj).isConnected();
    }

    public static android.content.ComponentName getServiceComponent(java.lang.Object browserObj) {
        return ((android.media.browse.MediaBrowser) browserObj).getServiceComponent();
    }

    public static java.lang.String getRoot(java.lang.Object browserObj) {
        return ((android.media.browse.MediaBrowser) browserObj).getRoot();
    }

    public static android.os.Bundle getExtras(java.lang.Object browserObj) {
        return ((android.media.browse.MediaBrowser) browserObj).getExtras();
    }

    public static java.lang.Object getSessionToken(java.lang.Object browserObj) {
        return ((android.media.browse.MediaBrowser) browserObj).getSessionToken();
    }

    public static java.lang.Object createSubscriptionCallback(android.support.v4.media.MediaBrowserCompatApi21.SubscriptionCallback callback) {
        return new android.support.v4.media.MediaBrowserCompatApi21.SubscriptionCallbackProxy(callback);
    }

    public static void subscribe(java.lang.Object browserObj, java.lang.String parentId, java.lang.Object subscriptionCallbackObj) {
        ((android.media.browse.MediaBrowser) browserObj).subscribe(parentId, (android.media.browse.MediaBrowser.SubscriptionCallback) subscriptionCallbackObj);
    }

    public static void unsubscribe(java.lang.Object browserObj, java.lang.String parentId) {
        ((android.media.browse.MediaBrowser) browserObj).unsubscribe(parentId);
    }
}
