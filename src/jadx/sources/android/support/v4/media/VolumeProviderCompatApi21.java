package android.support.v4.media;

@android.annotation.TargetApi(21)
@android.support.annotation.RequiresApi(21)
class VolumeProviderCompatApi21 {

    public interface Delegate {
        void onAdjustVolume(int i);

        void onSetVolumeTo(int i);
    }

    VolumeProviderCompatApi21() {
    }

    public static java.lang.Object createVolumeProvider(int volumeControl, int maxVolume, int currentVolume, final android.support.v4.media.VolumeProviderCompatApi21.Delegate delegate) {
        return new android.media.VolumeProvider(volumeControl, maxVolume, currentVolume) {
            public void onSetVolumeTo(int volume) {
                delegate.onSetVolumeTo(volume);
            }

            public void onAdjustVolume(int direction) {
                delegate.onAdjustVolume(direction);
            }
        };
    }

    public static void setCurrentVolume(java.lang.Object volumeProviderObj, int currentVolume) {
        ((android.media.VolumeProvider) volumeProviderObj).setCurrentVolume(currentVolume);
    }
}
