package android.support.v4.media;

@android.annotation.TargetApi(23)
@android.support.annotation.RequiresApi(23)
class MediaDescriptionCompatApi23 extends android.support.v4.media.MediaDescriptionCompatApi21 {

    static class Builder extends android.support.v4.media.MediaDescriptionCompatApi21.Builder {
        Builder() {
        }

        public static void setMediaUri(java.lang.Object builderObj, android.net.Uri mediaUri) {
            ((android.media.MediaDescription.Builder) builderObj).setMediaUri(mediaUri);
        }
    }

    MediaDescriptionCompatApi23() {
    }

    public static android.net.Uri getMediaUri(java.lang.Object descriptionObj) {
        return ((android.media.MediaDescription) descriptionObj).getMediaUri();
    }
}
