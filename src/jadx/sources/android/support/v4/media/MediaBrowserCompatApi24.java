package android.support.v4.media;

@android.annotation.TargetApi(24)
@android.support.annotation.RequiresApi(24)
class MediaBrowserCompatApi24 {

    interface SubscriptionCallback extends android.support.v4.media.MediaBrowserCompatApi21.SubscriptionCallback {
        void onChildrenLoaded(@android.support.annotation.NonNull java.lang.String str, java.util.List<?> list, @android.support.annotation.NonNull android.os.Bundle bundle);

        void onError(@android.support.annotation.NonNull java.lang.String str, @android.support.annotation.NonNull android.os.Bundle bundle);
    }

    static class SubscriptionCallbackProxy<T extends android.support.v4.media.MediaBrowserCompatApi24.SubscriptionCallback> extends android.support.v4.media.MediaBrowserCompatApi21.SubscriptionCallbackProxy<T> {
        public SubscriptionCallbackProxy(T callback) {
            super(callback);
        }

        public void onChildrenLoaded(@android.support.annotation.NonNull java.lang.String parentId, java.util.List<android.media.browse.MediaBrowser.MediaItem> children, @android.support.annotation.NonNull android.os.Bundle options) {
            ((android.support.v4.media.MediaBrowserCompatApi24.SubscriptionCallback) this.mSubscriptionCallback).onChildrenLoaded(parentId, children, options);
        }

        public void onError(@android.support.annotation.NonNull java.lang.String parentId, @android.support.annotation.NonNull android.os.Bundle options) {
            ((android.support.v4.media.MediaBrowserCompatApi24.SubscriptionCallback) this.mSubscriptionCallback).onError(parentId, options);
        }
    }

    MediaBrowserCompatApi24() {
    }

    public static java.lang.Object createSubscriptionCallback(android.support.v4.media.MediaBrowserCompatApi24.SubscriptionCallback callback) {
        return new android.support.v4.media.MediaBrowserCompatApi24.SubscriptionCallbackProxy(callback);
    }

    public static void subscribe(java.lang.Object browserObj, java.lang.String parentId, android.os.Bundle options, java.lang.Object subscriptionCallbackObj) {
        ((android.media.browse.MediaBrowser) browserObj).subscribe(parentId, options, (android.media.browse.MediaBrowser.SubscriptionCallback) subscriptionCallbackObj);
    }

    public static void unsubscribe(java.lang.Object browserObj, java.lang.String parentId, java.lang.Object subscriptionCallbackObj) {
        ((android.media.browse.MediaBrowser) browserObj).unsubscribe(parentId, (android.media.browse.MediaBrowser.SubscriptionCallback) subscriptionCallbackObj);
    }
}
