package android.support.v4.os;

public class BuildCompat {
    private BuildCompat() {
    }

    public static boolean isAtLeastN() {
        return android.os.Build.VERSION.SDK_INT >= 24;
    }

    public static boolean isAtLeastNMR1() {
        return android.os.Build.VERSION.SDK_INT >= 25;
    }

    public static boolean isAtLeastO() {
        return !"REL".equals(android.os.Build.VERSION.CODENAME) && ("O".equals(android.os.Build.VERSION.CODENAME) || android.os.Build.VERSION.CODENAME.startsWith("OMR"));
    }
}
