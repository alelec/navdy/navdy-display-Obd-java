package android.support.v4.os;

public final class EnvironmentCompat {
    public static final java.lang.String MEDIA_UNKNOWN = "unknown";
    private static final java.lang.String TAG = "EnvironmentCompat";

    public static java.lang.String getStorageState(java.io.File path) {
        if (android.os.Build.VERSION.SDK_INT >= 19) {
            return android.support.v4.os.EnvironmentCompatKitKat.getStorageState(path);
        }
        try {
            if (path.getCanonicalPath().startsWith(android.os.Environment.getExternalStorageDirectory().getCanonicalPath())) {
                return android.os.Environment.getExternalStorageState();
            }
        } catch (java.io.IOException e) {
            android.util.Log.w(TAG, "Failed to resolve canonical path: " + e);
        }
        return MEDIA_UNKNOWN;
    }

    private EnvironmentCompat() {
    }
}
