package android.support.v4.os;

@android.annotation.TargetApi(24)
@android.support.annotation.RequiresApi(24)
@android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
public class UserManagerCompatApi24 {
    public static boolean isUserUnlocked(android.content.Context context) {
        return ((android.os.UserManager) context.getSystemService(android.os.UserManager.class)).isUserUnlocked();
    }
}
