package android.support.v4.os;

@android.annotation.TargetApi(19)
@android.support.annotation.RequiresApi(19)
class EnvironmentCompatKitKat {
    EnvironmentCompatKitKat() {
    }

    public static java.lang.String getStorageState(java.io.File path) {
        return android.os.Environment.getStorageState(path);
    }
}
