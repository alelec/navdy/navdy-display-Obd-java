package android.support.v4.os;

@android.annotation.TargetApi(13)
@android.support.annotation.RequiresApi(13)
/* compiled from: ParcelableCompatHoneycombMR2 */
class ParcelableCompatCreatorHoneycombMR2Stub {
    ParcelableCompatCreatorHoneycombMR2Stub() {
    }

    static <T> android.os.Parcelable.Creator<T> instantiate(android.support.v4.os.ParcelableCompatCreatorCallbacks<T> callbacks) {
        return new android.support.v4.os.ParcelableCompatCreatorHoneycombMR2(callbacks);
    }
}
