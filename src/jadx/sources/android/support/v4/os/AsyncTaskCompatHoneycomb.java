package android.support.v4.os;

@android.annotation.TargetApi(11)
@android.support.annotation.RequiresApi(11)
class AsyncTaskCompatHoneycomb {
    AsyncTaskCompatHoneycomb() {
    }

    static <Params, Progress, Result> void executeParallel(android.os.AsyncTask<Params, Progress, Result> task, Params... params) {
        task.executeOnExecutor(android.os.AsyncTask.THREAD_POOL_EXECUTOR, params);
    }
}
