package android.support.v4.os;

public final class TraceCompat {
    public static void beginSection(java.lang.String sectionName) {
        if (android.os.Build.VERSION.SDK_INT >= 18) {
            android.support.v4.os.TraceJellybeanMR2.beginSection(sectionName);
        }
    }

    public static void endSection() {
        if (android.os.Build.VERSION.SDK_INT >= 18) {
            android.support.v4.os.TraceJellybeanMR2.endSection();
        }
    }

    private TraceCompat() {
    }
}
