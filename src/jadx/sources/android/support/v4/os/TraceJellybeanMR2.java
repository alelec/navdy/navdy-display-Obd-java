package android.support.v4.os;

@android.annotation.TargetApi(18)
@android.support.annotation.RequiresApi(18)
class TraceJellybeanMR2 {
    TraceJellybeanMR2() {
    }

    public static void beginSection(java.lang.String section) {
        android.os.Trace.beginSection(section);
    }

    public static void endSection() {
        android.os.Trace.endSection();
    }
}
