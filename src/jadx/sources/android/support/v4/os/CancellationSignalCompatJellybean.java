package android.support.v4.os;

@android.annotation.TargetApi(16)
@android.support.annotation.RequiresApi(16)
class CancellationSignalCompatJellybean {
    CancellationSignalCompatJellybean() {
    }

    public static java.lang.Object create() {
        return new android.os.CancellationSignal();
    }

    public static void cancel(java.lang.Object cancellationSignalObj) {
        ((android.os.CancellationSignal) cancellationSignalObj).cancel();
    }
}
