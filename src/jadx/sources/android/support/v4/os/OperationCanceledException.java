package android.support.v4.os;

public class OperationCanceledException extends java.lang.RuntimeException {
    public OperationCanceledException() {
        this(null);
    }

    public OperationCanceledException(java.lang.String message) {
        if (message == null) {
            message = "The operation has been canceled.";
        }
        super(message);
    }
}
