package android.support.v4.os;

@android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
public class ResultReceiver implements android.os.Parcelable {
    public static final android.os.Parcelable.Creator<android.support.v4.os.ResultReceiver> CREATOR = new android.os.Parcelable.Creator<android.support.v4.os.ResultReceiver>() {
        public android.support.v4.os.ResultReceiver createFromParcel(android.os.Parcel in) {
            return new android.support.v4.os.ResultReceiver(in);
        }

        public android.support.v4.os.ResultReceiver[] newArray(int size) {
            return new android.support.v4.os.ResultReceiver[size];
        }
    };
    final android.os.Handler mHandler;
    final boolean mLocal;
    android.support.v4.os.IResultReceiver mReceiver;

    class MyResultReceiver extends android.support.v4.os.IResultReceiver.Stub {
        MyResultReceiver() {
        }

        public void send(int resultCode, android.os.Bundle resultData) {
            if (android.support.v4.os.ResultReceiver.this.mHandler != null) {
                android.support.v4.os.ResultReceiver.this.mHandler.post(new android.support.v4.os.ResultReceiver.MyRunnable(resultCode, resultData));
            } else {
                android.support.v4.os.ResultReceiver.this.onReceiveResult(resultCode, resultData);
            }
        }
    }

    class MyRunnable implements java.lang.Runnable {
        final int mResultCode;
        final android.os.Bundle mResultData;

        MyRunnable(int resultCode, android.os.Bundle resultData) {
            this.mResultCode = resultCode;
            this.mResultData = resultData;
        }

        public void run() {
            android.support.v4.os.ResultReceiver.this.onReceiveResult(this.mResultCode, this.mResultData);
        }
    }

    public ResultReceiver(android.os.Handler handler) {
        this.mLocal = true;
        this.mHandler = handler;
    }

    public void send(int resultCode, android.os.Bundle resultData) {
        if (this.mLocal) {
            if (this.mHandler != null) {
                this.mHandler.post(new android.support.v4.os.ResultReceiver.MyRunnable(resultCode, resultData));
            } else {
                onReceiveResult(resultCode, resultData);
            }
        } else if (this.mReceiver != null) {
            try {
                this.mReceiver.send(resultCode, resultData);
            } catch (android.os.RemoteException e) {
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onReceiveResult(int resultCode, android.os.Bundle resultData) {
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(android.os.Parcel out, int flags) {
        synchronized (this) {
            if (this.mReceiver == null) {
                this.mReceiver = new android.support.v4.os.ResultReceiver.MyResultReceiver();
            }
            out.writeStrongBinder(this.mReceiver.asBinder());
        }
    }

    ResultReceiver(android.os.Parcel in) {
        this.mLocal = false;
        this.mHandler = null;
        this.mReceiver = android.support.v4.os.IResultReceiver.Stub.asInterface(in.readStrongBinder());
    }
}
