package android.support.v4.os;

public class UserManagerCompat {
    private UserManagerCompat() {
    }

    public static boolean isUserUnlocked(android.content.Context context) {
        if (android.support.v4.os.BuildCompat.isAtLeastN()) {
            return android.support.v4.os.UserManagerCompatApi24.isUserUnlocked(context);
        }
        return true;
    }
}
