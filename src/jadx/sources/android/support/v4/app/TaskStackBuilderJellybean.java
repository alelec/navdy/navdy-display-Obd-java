package android.support.v4.app;

@android.annotation.TargetApi(16)
@android.support.annotation.RequiresApi(16)
class TaskStackBuilderJellybean {
    TaskStackBuilderJellybean() {
    }

    public static android.app.PendingIntent getActivitiesPendingIntent(android.content.Context context, int requestCode, android.content.Intent[] intents, int flags, android.os.Bundle options) {
        return android.app.PendingIntent.getActivities(context, requestCode, intents, flags, options);
    }
}
