package android.support.v4.app;

@android.annotation.TargetApi(21)
@android.support.annotation.RequiresApi(21)
class ActivityCompatApi21 {

    public static abstract class SharedElementCallback21 {
        public abstract android.os.Parcelable onCaptureSharedElementSnapshot(android.view.View view, android.graphics.Matrix matrix, android.graphics.RectF rectF);

        public abstract android.view.View onCreateSnapshotView(android.content.Context context, android.os.Parcelable parcelable);

        public abstract void onMapSharedElements(java.util.List<java.lang.String> list, java.util.Map<java.lang.String, android.view.View> map);

        public abstract void onRejectSharedElements(java.util.List<android.view.View> list);

        public abstract void onSharedElementEnd(java.util.List<java.lang.String> list, java.util.List<android.view.View> list2, java.util.List<android.view.View> list3);

        public abstract void onSharedElementStart(java.util.List<java.lang.String> list, java.util.List<android.view.View> list2, java.util.List<android.view.View> list3);
    }

    private static class SharedElementCallbackImpl extends android.app.SharedElementCallback {
        private android.support.v4.app.ActivityCompatApi21.SharedElementCallback21 mCallback;

        public SharedElementCallbackImpl(android.support.v4.app.ActivityCompatApi21.SharedElementCallback21 callback) {
            this.mCallback = callback;
        }

        public void onSharedElementStart(java.util.List<java.lang.String> sharedElementNames, java.util.List<android.view.View> sharedElements, java.util.List<android.view.View> sharedElementSnapshots) {
            this.mCallback.onSharedElementStart(sharedElementNames, sharedElements, sharedElementSnapshots);
        }

        public void onSharedElementEnd(java.util.List<java.lang.String> sharedElementNames, java.util.List<android.view.View> sharedElements, java.util.List<android.view.View> sharedElementSnapshots) {
            this.mCallback.onSharedElementEnd(sharedElementNames, sharedElements, sharedElementSnapshots);
        }

        public void onRejectSharedElements(java.util.List<android.view.View> rejectedSharedElements) {
            this.mCallback.onRejectSharedElements(rejectedSharedElements);
        }

        public void onMapSharedElements(java.util.List<java.lang.String> names, java.util.Map<java.lang.String, android.view.View> sharedElements) {
            this.mCallback.onMapSharedElements(names, sharedElements);
        }

        public android.os.Parcelable onCaptureSharedElementSnapshot(android.view.View sharedElement, android.graphics.Matrix viewToGlobalMatrix, android.graphics.RectF screenBounds) {
            return this.mCallback.onCaptureSharedElementSnapshot(sharedElement, viewToGlobalMatrix, screenBounds);
        }

        public android.view.View onCreateSnapshotView(android.content.Context context, android.os.Parcelable snapshot) {
            return this.mCallback.onCreateSnapshotView(context, snapshot);
        }
    }

    ActivityCompatApi21() {
    }

    public static void finishAfterTransition(android.app.Activity activity) {
        activity.finishAfterTransition();
    }

    public static void setEnterSharedElementCallback(android.app.Activity activity, android.support.v4.app.ActivityCompatApi21.SharedElementCallback21 callback) {
        activity.setEnterSharedElementCallback(createCallback(callback));
    }

    public static void setExitSharedElementCallback(android.app.Activity activity, android.support.v4.app.ActivityCompatApi21.SharedElementCallback21 callback) {
        activity.setExitSharedElementCallback(createCallback(callback));
    }

    public static void postponeEnterTransition(android.app.Activity activity) {
        activity.postponeEnterTransition();
    }

    public static void startPostponedEnterTransition(android.app.Activity activity) {
        activity.startPostponedEnterTransition();
    }

    private static android.app.SharedElementCallback createCallback(android.support.v4.app.ActivityCompatApi21.SharedElementCallback21 callback) {
        if (callback != null) {
            return new android.support.v4.app.ActivityCompatApi21.SharedElementCallbackImpl(callback);
        }
        return null;
    }
}
