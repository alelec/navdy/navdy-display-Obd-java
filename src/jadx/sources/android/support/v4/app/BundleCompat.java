package android.support.v4.app;

public final class BundleCompat {
    private BundleCompat() {
    }

    public static android.os.IBinder getBinder(android.os.Bundle bundle, java.lang.String key) {
        if (android.os.Build.VERSION.SDK_INT >= 18) {
            return android.support.v4.app.BundleCompatJellybeanMR2.getBinder(bundle, key);
        }
        return android.support.v4.app.BundleCompatGingerbread.getBinder(bundle, key);
    }

    public static void putBinder(android.os.Bundle bundle, java.lang.String key, android.os.IBinder binder) {
        if (android.os.Build.VERSION.SDK_INT >= 18) {
            android.support.v4.app.BundleCompatJellybeanMR2.putBinder(bundle, key, binder);
        } else {
            android.support.v4.app.BundleCompatGingerbread.putBinder(bundle, key, binder);
        }
    }
}
