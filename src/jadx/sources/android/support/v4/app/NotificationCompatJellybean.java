package android.support.v4.app;

@android.annotation.TargetApi(16)
@android.support.annotation.RequiresApi(16)
class NotificationCompatJellybean {
    static final java.lang.String EXTRA_ACTION_EXTRAS = "android.support.actionExtras";
    static final java.lang.String EXTRA_ALLOW_GENERATED_REPLIES = "android.support.allowGeneratedReplies";
    static final java.lang.String EXTRA_GROUP_KEY = "android.support.groupKey";
    static final java.lang.String EXTRA_GROUP_SUMMARY = "android.support.isGroupSummary";
    static final java.lang.String EXTRA_LOCAL_ONLY = "android.support.localOnly";
    static final java.lang.String EXTRA_REMOTE_INPUTS = "android.support.remoteInputs";
    static final java.lang.String EXTRA_SORT_KEY = "android.support.sortKey";
    static final java.lang.String EXTRA_USE_SIDE_CHANNEL = "android.support.useSideChannel";
    private static final java.lang.String KEY_ACTION_INTENT = "actionIntent";
    private static final java.lang.String KEY_ALLOW_GENERATED_REPLIES = "allowGeneratedReplies";
    private static final java.lang.String KEY_EXTRAS = "extras";
    private static final java.lang.String KEY_ICON = "icon";
    private static final java.lang.String KEY_REMOTE_INPUTS = "remoteInputs";
    private static final java.lang.String KEY_TITLE = "title";
    public static final java.lang.String TAG = "NotificationCompat";
    private static java.lang.Class<?> sActionClass;
    private static java.lang.reflect.Field sActionIconField;
    private static java.lang.reflect.Field sActionIntentField;
    private static java.lang.reflect.Field sActionTitleField;
    private static boolean sActionsAccessFailed;
    private static java.lang.reflect.Field sActionsField;
    private static final java.lang.Object sActionsLock = new java.lang.Object();
    private static java.lang.reflect.Field sExtrasField;
    private static boolean sExtrasFieldAccessFailed;
    private static final java.lang.Object sExtrasLock = new java.lang.Object();

    public static class Builder implements android.support.v4.app.NotificationBuilderWithBuilderAccessor, android.support.v4.app.NotificationBuilderWithActions {
        private android.app.Notification.Builder b;
        private java.util.List<android.os.Bundle> mActionExtrasList = new java.util.ArrayList();
        private android.widget.RemoteViews mBigContentView;
        private android.widget.RemoteViews mContentView;
        private final android.os.Bundle mExtras;

        public Builder(android.content.Context context, android.app.Notification n, java.lang.CharSequence contentTitle, java.lang.CharSequence contentText, java.lang.CharSequence contentInfo, android.widget.RemoteViews tickerView, int number, android.app.PendingIntent contentIntent, android.app.PendingIntent fullScreenIntent, android.graphics.Bitmap largeIcon, int progressMax, int progress, boolean progressIndeterminate, boolean useChronometer, int priority, java.lang.CharSequence subText, boolean localOnly, android.os.Bundle extras, java.lang.String groupKey, boolean groupSummary, java.lang.String sortKey, android.widget.RemoteViews contentView, android.widget.RemoteViews bigContentView) {
            this.b = new android.app.Notification.Builder(context).setWhen(n.when).setSmallIcon(n.icon, n.iconLevel).setContent(n.contentView).setTicker(n.tickerText, tickerView).setSound(n.sound, n.audioStreamType).setVibrate(n.vibrate).setLights(n.ledARGB, n.ledOnMS, n.ledOffMS).setOngoing((n.flags & 2) != 0).setOnlyAlertOnce((n.flags & 8) != 0).setAutoCancel((n.flags & 16) != 0).setDefaults(n.defaults).setContentTitle(contentTitle).setContentText(contentText).setSubText(subText).setContentInfo(contentInfo).setContentIntent(contentIntent).setDeleteIntent(n.deleteIntent).setFullScreenIntent(fullScreenIntent, (n.flags & 128) != 0).setLargeIcon(largeIcon).setNumber(number).setUsesChronometer(useChronometer).setPriority(priority).setProgress(progressMax, progress, progressIndeterminate);
            this.mExtras = new android.os.Bundle();
            if (extras != null) {
                this.mExtras.putAll(extras);
            }
            if (localOnly) {
                this.mExtras.putBoolean("android.support.localOnly", true);
            }
            if (groupKey != null) {
                this.mExtras.putString("android.support.groupKey", groupKey);
                if (groupSummary) {
                    this.mExtras.putBoolean("android.support.isGroupSummary", true);
                } else {
                    this.mExtras.putBoolean("android.support.useSideChannel", true);
                }
            }
            if (sortKey != null) {
                this.mExtras.putString("android.support.sortKey", sortKey);
            }
            this.mContentView = contentView;
            this.mBigContentView = bigContentView;
        }

        public void addAction(android.support.v4.app.NotificationCompatBase.Action action) {
            this.mActionExtrasList.add(android.support.v4.app.NotificationCompatJellybean.writeActionAndGetExtras(this.b, action));
        }

        public android.app.Notification.Builder getBuilder() {
            return this.b;
        }

        public android.app.Notification build() {
            android.app.Notification notif = this.b.build();
            android.os.Bundle extras = android.support.v4.app.NotificationCompatJellybean.getExtras(notif);
            android.os.Bundle mergeBundle = new android.os.Bundle(this.mExtras);
            for (java.lang.String key : this.mExtras.keySet()) {
                if (extras.containsKey(key)) {
                    mergeBundle.remove(key);
                }
            }
            extras.putAll(mergeBundle);
            android.util.SparseArray<android.os.Bundle> actionExtrasMap = android.support.v4.app.NotificationCompatJellybean.buildActionExtrasMap(this.mActionExtrasList);
            if (actionExtrasMap != null) {
                android.support.v4.app.NotificationCompatJellybean.getExtras(notif).putSparseParcelableArray("android.support.actionExtras", actionExtrasMap);
            }
            if (this.mContentView != null) {
                notif.contentView = this.mContentView;
            }
            if (this.mBigContentView != null) {
                notif.bigContentView = this.mBigContentView;
            }
            return notif;
        }
    }

    NotificationCompatJellybean() {
    }

    public static void addBigTextStyle(android.support.v4.app.NotificationBuilderWithBuilderAccessor b, java.lang.CharSequence bigContentTitle, boolean useSummary, java.lang.CharSequence summaryText, java.lang.CharSequence bigText) {
        android.app.Notification.BigTextStyle style = new android.app.Notification.BigTextStyle(b.getBuilder()).setBigContentTitle(bigContentTitle).bigText(bigText);
        if (useSummary) {
            style.setSummaryText(summaryText);
        }
    }

    public static void addBigPictureStyle(android.support.v4.app.NotificationBuilderWithBuilderAccessor b, java.lang.CharSequence bigContentTitle, boolean useSummary, java.lang.CharSequence summaryText, android.graphics.Bitmap bigPicture, android.graphics.Bitmap bigLargeIcon, boolean bigLargeIconSet) {
        android.app.Notification.BigPictureStyle style = new android.app.Notification.BigPictureStyle(b.getBuilder()).setBigContentTitle(bigContentTitle).bigPicture(bigPicture);
        if (bigLargeIconSet) {
            style.bigLargeIcon(bigLargeIcon);
        }
        if (useSummary) {
            style.setSummaryText(summaryText);
        }
    }

    public static void addInboxStyle(android.support.v4.app.NotificationBuilderWithBuilderAccessor b, java.lang.CharSequence bigContentTitle, boolean useSummary, java.lang.CharSequence summaryText, java.util.ArrayList<java.lang.CharSequence> texts) {
        android.app.Notification.InboxStyle style = new android.app.Notification.InboxStyle(b.getBuilder()).setBigContentTitle(bigContentTitle);
        if (useSummary) {
            style.setSummaryText(summaryText);
        }
        java.util.Iterator it = texts.iterator();
        while (it.hasNext()) {
            style.addLine((java.lang.CharSequence) it.next());
        }
    }

    public static android.util.SparseArray<android.os.Bundle> buildActionExtrasMap(java.util.List<android.os.Bundle> actionExtrasList) {
        android.util.SparseArray<android.os.Bundle> actionExtrasMap = null;
        int count = actionExtrasList.size();
        for (int i = 0; i < count; i++) {
            android.os.Bundle actionExtras = (android.os.Bundle) actionExtrasList.get(i);
            if (actionExtras != null) {
                if (actionExtrasMap == null) {
                    actionExtrasMap = new android.util.SparseArray<>();
                }
                actionExtrasMap.put(i, actionExtras);
            }
        }
        return actionExtrasMap;
    }

    public static android.os.Bundle getExtras(android.app.Notification notif) {
        synchronized (sExtrasLock) {
            if (sExtrasFieldAccessFailed) {
                return null;
            }
            try {
                if (sExtrasField == null) {
                    java.lang.reflect.Field extrasField = android.app.Notification.class.getDeclaredField(KEY_EXTRAS);
                    if (!android.os.Bundle.class.isAssignableFrom(extrasField.getType())) {
                        android.util.Log.e(TAG, "Notification.extras field is not of type Bundle");
                        sExtrasFieldAccessFailed = true;
                        return null;
                    }
                    extrasField.setAccessible(true);
                    sExtrasField = extrasField;
                }
                android.os.Bundle extras = (android.os.Bundle) sExtrasField.get(notif);
                if (extras == null) {
                    extras = new android.os.Bundle();
                    sExtrasField.set(notif, extras);
                }
                return extras;
            } catch (java.lang.IllegalAccessException e) {
                android.util.Log.e(TAG, "Unable to access notification extras", e);
                sExtrasFieldAccessFailed = true;
                return null;
            } catch (java.lang.NoSuchFieldException e2) {
                android.util.Log.e(TAG, "Unable to access notification extras", e2);
                sExtrasFieldAccessFailed = true;
                return null;
            }
        }
    }

    public static android.support.v4.app.NotificationCompatBase.Action readAction(android.support.v4.app.NotificationCompatBase.Action.Factory factory, android.support.v4.app.RemoteInputCompatBase.RemoteInput.Factory remoteInputFactory, int icon, java.lang.CharSequence title, android.app.PendingIntent actionIntent, android.os.Bundle extras) {
        android.support.v4.app.RemoteInputCompatBase.RemoteInput[] remoteInputs = null;
        boolean allowGeneratedReplies = false;
        if (extras != null) {
            remoteInputs = android.support.v4.app.RemoteInputCompatJellybean.fromBundleArray(android.support.v4.app.BundleUtil.getBundleArrayFromBundle(extras, "android.support.remoteInputs"), remoteInputFactory);
            allowGeneratedReplies = extras.getBoolean(EXTRA_ALLOW_GENERATED_REPLIES);
        }
        return factory.build(icon, title, actionIntent, extras, remoteInputs, allowGeneratedReplies);
    }

    public static android.os.Bundle writeActionAndGetExtras(android.app.Notification.Builder builder, android.support.v4.app.NotificationCompatBase.Action action) {
        builder.addAction(action.getIcon(), action.getTitle(), action.getActionIntent());
        android.os.Bundle actionExtras = new android.os.Bundle(action.getExtras());
        if (action.getRemoteInputs() != null) {
            actionExtras.putParcelableArray("android.support.remoteInputs", android.support.v4.app.RemoteInputCompatJellybean.toBundleArray(action.getRemoteInputs()));
        }
        actionExtras.putBoolean(EXTRA_ALLOW_GENERATED_REPLIES, action.getAllowGeneratedReplies());
        return actionExtras;
    }

    public static int getActionCount(android.app.Notification notif) {
        int i;
        synchronized (sActionsLock) {
            java.lang.Object[] actionObjects = getActionObjectsLocked(notif);
            i = actionObjects != null ? actionObjects.length : 0;
        }
        return i;
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    public static android.support.v4.app.NotificationCompatBase.Action getAction(android.app.Notification notif, int actionIndex, android.support.v4.app.NotificationCompatBase.Action.Factory factory, android.support.v4.app.RemoteInputCompatBase.RemoteInput.Factory remoteInputFactory) {
        synchronized (sActionsLock) {
            try {
                java.lang.Object[] actionObjects = getActionObjectsLocked(notif);
                if (actionObjects != null) {
                    java.lang.Object actionObject = actionObjects[actionIndex];
                    android.os.Bundle actionExtras = null;
                    android.os.Bundle extras = getExtras(notif);
                    if (extras != null) {
                        android.util.SparseArray<android.os.Bundle> actionExtrasMap = extras.getSparseParcelableArray("android.support.actionExtras");
                        if (actionExtrasMap != null) {
                            actionExtras = (android.os.Bundle) actionExtrasMap.get(actionIndex);
                        }
                    }
                    android.support.v4.app.NotificationCompatBase.Action readAction = readAction(factory, remoteInputFactory, sActionIconField.getInt(actionObject), (java.lang.CharSequence) sActionTitleField.get(actionObject), (android.app.PendingIntent) sActionIntentField.get(actionObject), actionExtras);
                    return readAction;
                }
            } catch (java.lang.IllegalAccessException e) {
                android.util.Log.e(TAG, "Unable to access notification actions", e);
                sActionsAccessFailed = true;
            }
        }
        return null;
    }

    private static java.lang.Object[] getActionObjectsLocked(android.app.Notification notif) {
        synchronized (sActionsLock) {
            if (!ensureActionReflectionReadyLocked()) {
                return null;
            }
            try {
                java.lang.Object[] objArr = (java.lang.Object[]) sActionsField.get(notif);
                return objArr;
            } catch (java.lang.IllegalAccessException e) {
                android.util.Log.e(TAG, "Unable to access notification actions", e);
                sActionsAccessFailed = true;
                return null;
            }
        }
    }

    private static boolean ensureActionReflectionReadyLocked() {
        boolean z = true;
        if (sActionsAccessFailed) {
            return false;
        }
        try {
            if (sActionsField == null) {
                sActionClass = java.lang.Class.forName("android.app.Notification$Action");
                sActionIconField = sActionClass.getDeclaredField(KEY_ICON);
                sActionTitleField = sActionClass.getDeclaredField(KEY_TITLE);
                sActionIntentField = sActionClass.getDeclaredField(KEY_ACTION_INTENT);
                sActionsField = android.app.Notification.class.getDeclaredField("actions");
                sActionsField.setAccessible(true);
            }
        } catch (java.lang.ClassNotFoundException e) {
            android.util.Log.e(TAG, "Unable to access notification actions", e);
            sActionsAccessFailed = true;
        } catch (java.lang.NoSuchFieldException e2) {
            android.util.Log.e(TAG, "Unable to access notification actions", e2);
            sActionsAccessFailed = true;
        }
        if (sActionsAccessFailed) {
            z = false;
        }
        return z;
    }

    public static android.support.v4.app.NotificationCompatBase.Action[] getActionsFromParcelableArrayList(java.util.ArrayList<android.os.Parcelable> parcelables, android.support.v4.app.NotificationCompatBase.Action.Factory actionFactory, android.support.v4.app.RemoteInputCompatBase.RemoteInput.Factory remoteInputFactory) {
        if (parcelables == null) {
            return null;
        }
        android.support.v4.app.NotificationCompatBase.Action[] actions = actionFactory.newArray(parcelables.size());
        for (int i = 0; i < actions.length; i++) {
            actions[i] = getActionFromBundle((android.os.Bundle) parcelables.get(i), actionFactory, remoteInputFactory);
        }
        return actions;
    }

    private static android.support.v4.app.NotificationCompatBase.Action getActionFromBundle(android.os.Bundle bundle, android.support.v4.app.NotificationCompatBase.Action.Factory actionFactory, android.support.v4.app.RemoteInputCompatBase.RemoteInput.Factory remoteInputFactory) {
        android.os.Bundle extras = bundle.getBundle(KEY_EXTRAS);
        boolean allowGeneratedReplies = false;
        if (extras != null) {
            allowGeneratedReplies = extras.getBoolean(EXTRA_ALLOW_GENERATED_REPLIES, false);
        }
        return actionFactory.build(bundle.getInt(KEY_ICON), bundle.getCharSequence(KEY_TITLE), (android.app.PendingIntent) bundle.getParcelable(KEY_ACTION_INTENT), bundle.getBundle(KEY_EXTRAS), android.support.v4.app.RemoteInputCompatJellybean.fromBundleArray(android.support.v4.app.BundleUtil.getBundleArrayFromBundle(bundle, KEY_REMOTE_INPUTS), remoteInputFactory), allowGeneratedReplies);
    }

    public static java.util.ArrayList<android.os.Parcelable> getParcelableArrayListForActions(android.support.v4.app.NotificationCompatBase.Action[] actions) {
        if (actions == null) {
            return null;
        }
        java.util.ArrayList<android.os.Parcelable> parcelables = new java.util.ArrayList<>(actions.length);
        for (android.support.v4.app.NotificationCompatBase.Action action : actions) {
            parcelables.add(getBundleForAction(action));
        }
        return parcelables;
    }

    private static android.os.Bundle getBundleForAction(android.support.v4.app.NotificationCompatBase.Action action) {
        android.os.Bundle actionExtras;
        android.os.Bundle bundle = new android.os.Bundle();
        bundle.putInt(KEY_ICON, action.getIcon());
        bundle.putCharSequence(KEY_TITLE, action.getTitle());
        bundle.putParcelable(KEY_ACTION_INTENT, action.getActionIntent());
        if (action.getExtras() != null) {
            actionExtras = new android.os.Bundle(action.getExtras());
        } else {
            actionExtras = new android.os.Bundle();
        }
        actionExtras.putBoolean(EXTRA_ALLOW_GENERATED_REPLIES, action.getAllowGeneratedReplies());
        bundle.putBundle(KEY_EXTRAS, actionExtras);
        bundle.putParcelableArray(KEY_REMOTE_INPUTS, android.support.v4.app.RemoteInputCompatJellybean.toBundleArray(action.getRemoteInputs()));
        return bundle;
    }

    public static boolean getLocalOnly(android.app.Notification notif) {
        return getExtras(notif).getBoolean("android.support.localOnly");
    }

    public static java.lang.String getGroup(android.app.Notification n) {
        return getExtras(n).getString("android.support.groupKey");
    }

    public static boolean isGroupSummary(android.app.Notification n) {
        return getExtras(n).getBoolean("android.support.isGroupSummary");
    }

    public static java.lang.String getSortKey(android.app.Notification n) {
        return getExtras(n).getString("android.support.sortKey");
    }
}
