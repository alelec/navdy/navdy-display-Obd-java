package android.support.v4.app;

final class BackStackRecord extends android.support.v4.app.FragmentTransaction implements android.support.v4.app.FragmentManager.BackStackEntry, android.support.v4.app.FragmentManagerImpl.OpGenerator {
    static final int OP_ADD = 1;
    static final int OP_ATTACH = 7;
    static final int OP_DETACH = 6;
    static final int OP_HIDE = 4;
    static final int OP_NULL = 0;
    static final int OP_REMOVE = 3;
    static final int OP_REPLACE = 2;
    static final int OP_SHOW = 5;
    static final boolean SUPPORTS_TRANSITIONS = (android.os.Build.VERSION.SDK_INT >= 21);
    static final java.lang.String TAG = "FragmentManager";
    boolean mAddToBackStack;
    boolean mAllowAddToBackStack = true;
    boolean mAllowOptimization = false;
    int mBreadCrumbShortTitleRes;
    java.lang.CharSequence mBreadCrumbShortTitleText;
    int mBreadCrumbTitleRes;
    java.lang.CharSequence mBreadCrumbTitleText;
    boolean mCommitted;
    int mEnterAnim;
    int mExitAnim;
    int mIndex = -1;
    final android.support.v4.app.FragmentManagerImpl mManager;
    java.lang.String mName;
    java.util.ArrayList<android.support.v4.app.BackStackRecord.Op> mOps = new java.util.ArrayList<>();
    int mPopEnterAnim;
    int mPopExitAnim;
    java.util.ArrayList<java.lang.String> mSharedElementSourceNames;
    java.util.ArrayList<java.lang.String> mSharedElementTargetNames;
    int mTransition;
    int mTransitionStyle;

    static final class Op {
        int cmd;
        int enterAnim;
        int exitAnim;
        android.support.v4.app.Fragment fragment;
        int popEnterAnim;
        int popExitAnim;

        Op() {
        }
    }

    public java.lang.String toString() {
        java.lang.StringBuilder sb = new java.lang.StringBuilder(128);
        sb.append("BackStackEntry{");
        sb.append(java.lang.Integer.toHexString(java.lang.System.identityHashCode(this)));
        if (this.mIndex >= 0) {
            sb.append(" #");
            sb.append(this.mIndex);
        }
        if (this.mName != null) {
            sb.append(" ");
            sb.append(this.mName);
        }
        sb.append("}");
        return sb.toString();
    }

    public void dump(java.lang.String prefix, java.io.FileDescriptor fd, java.io.PrintWriter writer, java.lang.String[] args) {
        dump(prefix, writer, true);
    }

    public void dump(java.lang.String prefix, java.io.PrintWriter writer, boolean full) {
        java.lang.String cmdStr;
        if (full) {
            writer.print(prefix);
            writer.print("mName=");
            writer.print(this.mName);
            writer.print(" mIndex=");
            writer.print(this.mIndex);
            writer.print(" mCommitted=");
            writer.println(this.mCommitted);
            if (this.mTransition != 0) {
                writer.print(prefix);
                writer.print("mTransition=#");
                writer.print(java.lang.Integer.toHexString(this.mTransition));
                writer.print(" mTransitionStyle=#");
                writer.println(java.lang.Integer.toHexString(this.mTransitionStyle));
            }
            if (!(this.mEnterAnim == 0 && this.mExitAnim == 0)) {
                writer.print(prefix);
                writer.print("mEnterAnim=#");
                writer.print(java.lang.Integer.toHexString(this.mEnterAnim));
                writer.print(" mExitAnim=#");
                writer.println(java.lang.Integer.toHexString(this.mExitAnim));
            }
            if (!(this.mPopEnterAnim == 0 && this.mPopExitAnim == 0)) {
                writer.print(prefix);
                writer.print("mPopEnterAnim=#");
                writer.print(java.lang.Integer.toHexString(this.mPopEnterAnim));
                writer.print(" mPopExitAnim=#");
                writer.println(java.lang.Integer.toHexString(this.mPopExitAnim));
            }
            if (!(this.mBreadCrumbTitleRes == 0 && this.mBreadCrumbTitleText == null)) {
                writer.print(prefix);
                writer.print("mBreadCrumbTitleRes=#");
                writer.print(java.lang.Integer.toHexString(this.mBreadCrumbTitleRes));
                writer.print(" mBreadCrumbTitleText=");
                writer.println(this.mBreadCrumbTitleText);
            }
            if (!(this.mBreadCrumbShortTitleRes == 0 && this.mBreadCrumbShortTitleText == null)) {
                writer.print(prefix);
                writer.print("mBreadCrumbShortTitleRes=#");
                writer.print(java.lang.Integer.toHexString(this.mBreadCrumbShortTitleRes));
                writer.print(" mBreadCrumbShortTitleText=");
                writer.println(this.mBreadCrumbShortTitleText);
            }
        }
        if (!this.mOps.isEmpty()) {
            writer.print(prefix);
            writer.println("Operations:");
            java.lang.String str = prefix + "    ";
            int numOps = this.mOps.size();
            for (int opNum = 0; opNum < numOps; opNum++) {
                android.support.v4.app.BackStackRecord.Op op = (android.support.v4.app.BackStackRecord.Op) this.mOps.get(opNum);
                switch (op.cmd) {
                    case 0:
                        cmdStr = ch.qos.logback.core.joran.action.ActionConst.NULL;
                        break;
                    case 1:
                        cmdStr = "ADD";
                        break;
                    case 2:
                        cmdStr = "REPLACE";
                        break;
                    case 3:
                        cmdStr = "REMOVE";
                        break;
                    case 4:
                        cmdStr = "HIDE";
                        break;
                    case 5:
                        cmdStr = "SHOW";
                        break;
                    case 6:
                        cmdStr = "DETACH";
                        break;
                    case 7:
                        cmdStr = "ATTACH";
                        break;
                    default:
                        cmdStr = "cmd=" + op.cmd;
                        break;
                }
                writer.print(prefix);
                writer.print("  Op #");
                writer.print(opNum);
                writer.print(": ");
                writer.print(cmdStr);
                writer.print(" ");
                writer.println(op.fragment);
                if (full) {
                    if (!(op.enterAnim == 0 && op.exitAnim == 0)) {
                        writer.print(prefix);
                        writer.print("enterAnim=#");
                        writer.print(java.lang.Integer.toHexString(op.enterAnim));
                        writer.print(" exitAnim=#");
                        writer.println(java.lang.Integer.toHexString(op.exitAnim));
                    }
                    if (op.popEnterAnim != 0 || op.popExitAnim != 0) {
                        writer.print(prefix);
                        writer.print("popEnterAnim=#");
                        writer.print(java.lang.Integer.toHexString(op.popEnterAnim));
                        writer.print(" popExitAnim=#");
                        writer.println(java.lang.Integer.toHexString(op.popExitAnim));
                    }
                }
            }
        }
    }

    public BackStackRecord(android.support.v4.app.FragmentManagerImpl manager) {
        this.mManager = manager;
    }

    public int getId() {
        return this.mIndex;
    }

    public int getBreadCrumbTitleRes() {
        return this.mBreadCrumbTitleRes;
    }

    public int getBreadCrumbShortTitleRes() {
        return this.mBreadCrumbShortTitleRes;
    }

    public java.lang.CharSequence getBreadCrumbTitle() {
        if (this.mBreadCrumbTitleRes != 0) {
            return this.mManager.mHost.getContext().getText(this.mBreadCrumbTitleRes);
        }
        return this.mBreadCrumbTitleText;
    }

    public java.lang.CharSequence getBreadCrumbShortTitle() {
        if (this.mBreadCrumbShortTitleRes != 0) {
            return this.mManager.mHost.getContext().getText(this.mBreadCrumbShortTitleRes);
        }
        return this.mBreadCrumbShortTitleText;
    }

    /* access modifiers changed from: 0000 */
    public void addOp(android.support.v4.app.BackStackRecord.Op op) {
        this.mOps.add(op);
        op.enterAnim = this.mEnterAnim;
        op.exitAnim = this.mExitAnim;
        op.popEnterAnim = this.mPopEnterAnim;
        op.popExitAnim = this.mPopExitAnim;
    }

    public android.support.v4.app.FragmentTransaction add(android.support.v4.app.Fragment fragment, java.lang.String tag) {
        doAddOp(0, fragment, tag, 1);
        return this;
    }

    public android.support.v4.app.FragmentTransaction add(int containerViewId, android.support.v4.app.Fragment fragment) {
        doAddOp(containerViewId, fragment, null, 1);
        return this;
    }

    public android.support.v4.app.FragmentTransaction add(int containerViewId, android.support.v4.app.Fragment fragment, java.lang.String tag) {
        doAddOp(containerViewId, fragment, tag, 1);
        return this;
    }

    private void doAddOp(int containerViewId, android.support.v4.app.Fragment fragment, java.lang.String tag, int opcmd) {
        java.lang.Class fragmentClass = fragment.getClass();
        int modifiers = fragmentClass.getModifiers();
        if (fragmentClass.isAnonymousClass() || !java.lang.reflect.Modifier.isPublic(modifiers) || (fragmentClass.isMemberClass() && !java.lang.reflect.Modifier.isStatic(modifiers))) {
            throw new java.lang.IllegalStateException("Fragment " + fragmentClass.getCanonicalName() + " must be a public static class to be  properly recreated from" + " instance state.");
        }
        fragment.mFragmentManager = this.mManager;
        if (tag != null) {
            if (fragment.mTag == null || tag.equals(fragment.mTag)) {
                fragment.mTag = tag;
            } else {
                throw new java.lang.IllegalStateException("Can't change tag of fragment " + fragment + ": was " + fragment.mTag + " now " + tag);
            }
        }
        if (containerViewId != 0) {
            if (containerViewId == -1) {
                throw new java.lang.IllegalArgumentException("Can't add fragment " + fragment + " with tag " + tag + " to container view with no id");
            } else if (fragment.mFragmentId == 0 || fragment.mFragmentId == containerViewId) {
                fragment.mFragmentId = containerViewId;
                fragment.mContainerId = containerViewId;
            } else {
                throw new java.lang.IllegalStateException("Can't change container ID of fragment " + fragment + ": was " + fragment.mFragmentId + " now " + containerViewId);
            }
        }
        android.support.v4.app.BackStackRecord.Op op = new android.support.v4.app.BackStackRecord.Op();
        op.cmd = opcmd;
        op.fragment = fragment;
        addOp(op);
    }

    public android.support.v4.app.FragmentTransaction replace(int containerViewId, android.support.v4.app.Fragment fragment) {
        return replace(containerViewId, fragment, null);
    }

    public android.support.v4.app.FragmentTransaction replace(int containerViewId, android.support.v4.app.Fragment fragment, java.lang.String tag) {
        if (containerViewId == 0) {
            throw new java.lang.IllegalArgumentException("Must use non-zero containerViewId");
        }
        doAddOp(containerViewId, fragment, tag, 2);
        return this;
    }

    public android.support.v4.app.FragmentTransaction remove(android.support.v4.app.Fragment fragment) {
        android.support.v4.app.BackStackRecord.Op op = new android.support.v4.app.BackStackRecord.Op();
        op.cmd = 3;
        op.fragment = fragment;
        addOp(op);
        return this;
    }

    public android.support.v4.app.FragmentTransaction hide(android.support.v4.app.Fragment fragment) {
        android.support.v4.app.BackStackRecord.Op op = new android.support.v4.app.BackStackRecord.Op();
        op.cmd = 4;
        op.fragment = fragment;
        addOp(op);
        return this;
    }

    public android.support.v4.app.FragmentTransaction show(android.support.v4.app.Fragment fragment) {
        android.support.v4.app.BackStackRecord.Op op = new android.support.v4.app.BackStackRecord.Op();
        op.cmd = 5;
        op.fragment = fragment;
        addOp(op);
        return this;
    }

    public android.support.v4.app.FragmentTransaction detach(android.support.v4.app.Fragment fragment) {
        android.support.v4.app.BackStackRecord.Op op = new android.support.v4.app.BackStackRecord.Op();
        op.cmd = 6;
        op.fragment = fragment;
        addOp(op);
        return this;
    }

    public android.support.v4.app.FragmentTransaction attach(android.support.v4.app.Fragment fragment) {
        android.support.v4.app.BackStackRecord.Op op = new android.support.v4.app.BackStackRecord.Op();
        op.cmd = 7;
        op.fragment = fragment;
        addOp(op);
        return this;
    }

    public android.support.v4.app.FragmentTransaction setCustomAnimations(int enter, int exit) {
        return setCustomAnimations(enter, exit, 0, 0);
    }

    public android.support.v4.app.FragmentTransaction setCustomAnimations(int enter, int exit, int popEnter, int popExit) {
        this.mEnterAnim = enter;
        this.mExitAnim = exit;
        this.mPopEnterAnim = popEnter;
        this.mPopExitAnim = popExit;
        return this;
    }

    public android.support.v4.app.FragmentTransaction setTransition(int transition) {
        this.mTransition = transition;
        return this;
    }

    public android.support.v4.app.FragmentTransaction addSharedElement(android.view.View sharedElement, java.lang.String name) {
        if (SUPPORTS_TRANSITIONS) {
            java.lang.String transitionName = android.support.v4.view.ViewCompat.getTransitionName(sharedElement);
            if (transitionName == null) {
                throw new java.lang.IllegalArgumentException("Unique transitionNames are required for all sharedElements");
            }
            if (this.mSharedElementSourceNames == null) {
                this.mSharedElementSourceNames = new java.util.ArrayList<>();
                this.mSharedElementTargetNames = new java.util.ArrayList<>();
            } else if (this.mSharedElementTargetNames.contains(name)) {
                throw new java.lang.IllegalArgumentException("A shared element with the target name '" + name + "' has already been added to the transaction.");
            } else if (this.mSharedElementSourceNames.contains(transitionName)) {
                throw new java.lang.IllegalArgumentException("A shared element with the source name '" + transitionName + " has already been added to the transaction.");
            }
            this.mSharedElementSourceNames.add(transitionName);
            this.mSharedElementTargetNames.add(name);
        }
        return this;
    }

    public android.support.v4.app.FragmentTransaction setTransitionStyle(int styleRes) {
        this.mTransitionStyle = styleRes;
        return this;
    }

    public android.support.v4.app.FragmentTransaction addToBackStack(java.lang.String name) {
        if (!this.mAllowAddToBackStack) {
            throw new java.lang.IllegalStateException("This FragmentTransaction is not allowed to be added to the back stack.");
        }
        this.mAddToBackStack = true;
        this.mName = name;
        return this;
    }

    public boolean isAddToBackStackAllowed() {
        return this.mAllowAddToBackStack;
    }

    public android.support.v4.app.FragmentTransaction disallowAddToBackStack() {
        if (this.mAddToBackStack) {
            throw new java.lang.IllegalStateException("This transaction is already being added to the back stack");
        }
        this.mAllowAddToBackStack = false;
        return this;
    }

    public android.support.v4.app.FragmentTransaction setBreadCrumbTitle(int res) {
        this.mBreadCrumbTitleRes = res;
        this.mBreadCrumbTitleText = null;
        return this;
    }

    public android.support.v4.app.FragmentTransaction setBreadCrumbTitle(java.lang.CharSequence text) {
        this.mBreadCrumbTitleRes = 0;
        this.mBreadCrumbTitleText = text;
        return this;
    }

    public android.support.v4.app.FragmentTransaction setBreadCrumbShortTitle(int res) {
        this.mBreadCrumbShortTitleRes = res;
        this.mBreadCrumbShortTitleText = null;
        return this;
    }

    public android.support.v4.app.FragmentTransaction setBreadCrumbShortTitle(java.lang.CharSequence text) {
        this.mBreadCrumbShortTitleRes = 0;
        this.mBreadCrumbShortTitleText = text;
        return this;
    }

    /* access modifiers changed from: 0000 */
    public void bumpBackStackNesting(int amt) {
        if (this.mAddToBackStack) {
            if (android.support.v4.app.FragmentManagerImpl.DEBUG) {
                android.util.Log.v(TAG, "Bump nesting in " + this + " by " + amt);
            }
            int numOps = this.mOps.size();
            for (int opNum = 0; opNum < numOps; opNum++) {
                android.support.v4.app.BackStackRecord.Op op = (android.support.v4.app.BackStackRecord.Op) this.mOps.get(opNum);
                if (op.fragment != null) {
                    op.fragment.mBackStackNesting += amt;
                    if (android.support.v4.app.FragmentManagerImpl.DEBUG) {
                        android.util.Log.v(TAG, "Bump nesting of " + op.fragment + " to " + op.fragment.mBackStackNesting);
                    }
                }
            }
        }
    }

    public int commit() {
        return commitInternal(false);
    }

    public int commitAllowingStateLoss() {
        return commitInternal(true);
    }

    public void commitNow() {
        disallowAddToBackStack();
        this.mManager.execSingleAction(this, false);
    }

    public void commitNowAllowingStateLoss() {
        disallowAddToBackStack();
        this.mManager.execSingleAction(this, true);
    }

    public android.support.v4.app.FragmentTransaction setAllowOptimization(boolean allowOptimization) {
        this.mAllowOptimization = allowOptimization;
        return this;
    }

    /* access modifiers changed from: 0000 */
    public int commitInternal(boolean allowStateLoss) {
        if (this.mCommitted) {
            throw new java.lang.IllegalStateException("commit already called");
        }
        if (android.support.v4.app.FragmentManagerImpl.DEBUG) {
            android.util.Log.v(TAG, "Commit: " + this);
            java.io.PrintWriter pw = new java.io.PrintWriter(new android.support.v4.util.LogWriter(TAG));
            dump("  ", null, pw, null);
            pw.close();
        }
        this.mCommitted = true;
        if (this.mAddToBackStack) {
            this.mIndex = this.mManager.allocBackStackIndex(this);
        } else {
            this.mIndex = -1;
        }
        this.mManager.enqueueAction(this, allowStateLoss);
        return this.mIndex;
    }

    public boolean generateOps(java.util.ArrayList<android.support.v4.app.BackStackRecord> records, java.util.ArrayList<java.lang.Boolean> isRecordPop) {
        if (android.support.v4.app.FragmentManagerImpl.DEBUG) {
            android.util.Log.v(TAG, "Run: " + this);
        }
        records.add(this);
        isRecordPop.add(java.lang.Boolean.valueOf(false));
        if (this.mAddToBackStack) {
            this.mManager.addBackStackState(this);
        }
        return true;
    }

    /* access modifiers changed from: 0000 */
    public boolean interactsWith(int containerId) {
        int numOps = this.mOps.size();
        for (int opNum = 0; opNum < numOps; opNum++) {
            if (((android.support.v4.app.BackStackRecord.Op) this.mOps.get(opNum)).fragment.mContainerId == containerId) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: 0000 */
    public boolean interactsWith(java.util.ArrayList<android.support.v4.app.BackStackRecord> records, int startIndex, int endIndex) {
        if (endIndex == startIndex) {
            return false;
        }
        int numOps = this.mOps.size();
        int lastContainer = -1;
        for (int opNum = 0; opNum < numOps; opNum++) {
            int container = ((android.support.v4.app.BackStackRecord.Op) this.mOps.get(opNum)).fragment.mContainerId;
            if (!(container == 0 || container == lastContainer)) {
                lastContainer = container;
                for (int i = startIndex; i < endIndex; i++) {
                    android.support.v4.app.BackStackRecord record = (android.support.v4.app.BackStackRecord) records.get(i);
                    int numThoseOps = record.mOps.size();
                    for (int thoseOpIndex = 0; thoseOpIndex < numThoseOps; thoseOpIndex++) {
                        if (((android.support.v4.app.BackStackRecord.Op) record.mOps.get(thoseOpIndex)).fragment.mContainerId == container) {
                            return true;
                        }
                    }
                }
                continue;
            }
        }
        return false;
    }

    /* access modifiers changed from: 0000 */
    public void executeOps() {
        int numOps = this.mOps.size();
        for (int opNum = 0; opNum < numOps; opNum++) {
            android.support.v4.app.BackStackRecord.Op op = (android.support.v4.app.BackStackRecord.Op) this.mOps.get(opNum);
            android.support.v4.app.Fragment f = op.fragment;
            f.setNextTransition(this.mTransition, this.mTransitionStyle);
            switch (op.cmd) {
                case 1:
                    f.setNextAnim(op.enterAnim);
                    this.mManager.addFragment(f, false);
                    break;
                case 3:
                    f.setNextAnim(op.exitAnim);
                    this.mManager.removeFragment(f);
                    break;
                case 4:
                    f.setNextAnim(op.exitAnim);
                    this.mManager.hideFragment(f);
                    break;
                case 5:
                    f.setNextAnim(op.enterAnim);
                    this.mManager.showFragment(f);
                    break;
                case 6:
                    f.setNextAnim(op.exitAnim);
                    this.mManager.detachFragment(f);
                    break;
                case 7:
                    f.setNextAnim(op.enterAnim);
                    this.mManager.attachFragment(f);
                    break;
                default:
                    throw new java.lang.IllegalArgumentException("Unknown cmd: " + op.cmd);
            }
            if (!this.mAllowOptimization && op.cmd != 1) {
                this.mManager.moveFragmentToExpectedState(f);
            }
        }
        if (!this.mAllowOptimization) {
            this.mManager.moveToState(this.mManager.mCurState, true);
        }
    }

    /* access modifiers changed from: 0000 */
    public void executePopOps(boolean moveToState) {
        for (int opNum = this.mOps.size() - 1; opNum >= 0; opNum--) {
            android.support.v4.app.BackStackRecord.Op op = (android.support.v4.app.BackStackRecord.Op) this.mOps.get(opNum);
            android.support.v4.app.Fragment f = op.fragment;
            f.setNextTransition(android.support.v4.app.FragmentManagerImpl.reverseTransit(this.mTransition), this.mTransitionStyle);
            switch (op.cmd) {
                case 1:
                    f.setNextAnim(op.popExitAnim);
                    this.mManager.removeFragment(f);
                    break;
                case 3:
                    f.setNextAnim(op.popEnterAnim);
                    this.mManager.addFragment(f, false);
                    break;
                case 4:
                    f.setNextAnim(op.popEnterAnim);
                    this.mManager.showFragment(f);
                    break;
                case 5:
                    f.setNextAnim(op.popExitAnim);
                    this.mManager.hideFragment(f);
                    break;
                case 6:
                    f.setNextAnim(op.popEnterAnim);
                    this.mManager.attachFragment(f);
                    break;
                case 7:
                    f.setNextAnim(op.popExitAnim);
                    this.mManager.detachFragment(f);
                    break;
                default:
                    throw new java.lang.IllegalArgumentException("Unknown cmd: " + op.cmd);
            }
            if (!this.mAllowOptimization && op.cmd != 3) {
                this.mManager.moveFragmentToExpectedState(f);
            }
        }
        if (!this.mAllowOptimization && moveToState) {
            this.mManager.moveToState(this.mManager.mCurState, true);
        }
    }

    /* access modifiers changed from: 0000 */
    public void expandReplaceOps(java.util.ArrayList<android.support.v4.app.Fragment> added) {
        int opNum = 0;
        while (opNum < this.mOps.size()) {
            android.support.v4.app.BackStackRecord.Op op = (android.support.v4.app.BackStackRecord.Op) this.mOps.get(opNum);
            switch (op.cmd) {
                case 1:
                case 7:
                    added.add(op.fragment);
                    break;
                case 2:
                    android.support.v4.app.Fragment f = op.fragment;
                    int containerId = f.mContainerId;
                    boolean alreadyAdded = false;
                    for (int i = added.size() - 1; i >= 0; i--) {
                        android.support.v4.app.Fragment old = (android.support.v4.app.Fragment) added.get(i);
                        if (old.mContainerId == containerId) {
                            if (old == f) {
                                alreadyAdded = true;
                            } else {
                                android.support.v4.app.BackStackRecord.Op removeOp = new android.support.v4.app.BackStackRecord.Op();
                                removeOp.cmd = 3;
                                removeOp.fragment = old;
                                removeOp.enterAnim = op.enterAnim;
                                removeOp.popEnterAnim = op.popEnterAnim;
                                removeOp.exitAnim = op.exitAnim;
                                removeOp.popExitAnim = op.popExitAnim;
                                this.mOps.add(opNum, removeOp);
                                added.remove(old);
                                opNum++;
                            }
                        }
                    }
                    if (!alreadyAdded) {
                        op.cmd = 1;
                        added.add(f);
                        break;
                    } else {
                        this.mOps.remove(opNum);
                        opNum--;
                        break;
                    }
                case 3:
                case 6:
                    added.remove(op.fragment);
                    break;
            }
            opNum++;
        }
    }

    /* access modifiers changed from: 0000 */
    public void trackAddedFragmentsInPop(java.util.ArrayList<android.support.v4.app.Fragment> added) {
        for (int opNum = 0; opNum < this.mOps.size(); opNum++) {
            android.support.v4.app.BackStackRecord.Op op = (android.support.v4.app.BackStackRecord.Op) this.mOps.get(opNum);
            switch (op.cmd) {
                case 1:
                case 7:
                    added.remove(op.fragment);
                    break;
                case 3:
                case 6:
                    added.add(op.fragment);
                    break;
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean isPostponed() {
        for (int opNum = 0; opNum < this.mOps.size(); opNum++) {
            if (isFragmentPostponed((android.support.v4.app.BackStackRecord.Op) this.mOps.get(opNum))) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: 0000 */
    public void setOnStartPostponedListener(android.support.v4.app.Fragment.OnStartEnterTransitionListener listener) {
        for (int opNum = 0; opNum < this.mOps.size(); opNum++) {
            android.support.v4.app.BackStackRecord.Op op = (android.support.v4.app.BackStackRecord.Op) this.mOps.get(opNum);
            if (isFragmentPostponed(op)) {
                op.fragment.setOnStartEnterTransitionListener(listener);
            }
        }
    }

    private static boolean isFragmentPostponed(android.support.v4.app.BackStackRecord.Op op) {
        android.support.v4.app.Fragment fragment = op.fragment;
        return fragment.mAdded && fragment.mView != null && !fragment.mDetached && !fragment.mHidden && fragment.isPostponed();
    }

    public java.lang.String getName() {
        return this.mName;
    }

    public int getTransition() {
        return this.mTransition;
    }

    public int getTransitionStyle() {
        return this.mTransitionStyle;
    }

    public boolean isEmpty() {
        return this.mOps.isEmpty();
    }
}
