package android.support.v4.app;

@android.annotation.TargetApi(22)
@android.support.annotation.RequiresApi(22)
class ActivityCompatApi22 {
    ActivityCompatApi22() {
    }

    public static android.net.Uri getReferrer(android.app.Activity activity) {
        return activity.getReferrer();
    }
}
