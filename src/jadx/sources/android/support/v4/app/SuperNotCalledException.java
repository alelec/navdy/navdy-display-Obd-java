package android.support.v4.app;

final class SuperNotCalledException extends android.util.AndroidRuntimeException {
    public SuperNotCalledException(java.lang.String msg) {
        super(msg);
    }
}
