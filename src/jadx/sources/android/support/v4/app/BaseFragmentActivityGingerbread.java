package android.support.v4.app;

@android.annotation.TargetApi(9)
@android.support.annotation.RequiresApi(9)
abstract class BaseFragmentActivityGingerbread extends android.support.v4.app.SupportActivity {
    boolean mStartedIntentSenderFromFragment;

    /* access modifiers changed from: 0000 */
    public abstract android.view.View dispatchFragmentsOnCreateView(android.view.View view, java.lang.String str, android.content.Context context, android.util.AttributeSet attributeSet);

    BaseFragmentActivityGingerbread() {
    }

    /* access modifiers changed from: protected */
    public void onCreate(android.os.Bundle savedInstanceState) {
        if (android.os.Build.VERSION.SDK_INT < 11 && getLayoutInflater().getFactory() == null) {
            getLayoutInflater().setFactory(this);
        }
        super.onCreate(savedInstanceState);
    }

    public android.view.View onCreateView(java.lang.String name, android.content.Context context, android.util.AttributeSet attrs) {
        android.view.View v = dispatchFragmentsOnCreateView(null, name, context, attrs);
        if (v == null) {
            return super.onCreateView(name, context, attrs);
        }
        return v;
    }

    public void startIntentSenderForResult(android.content.IntentSender intent, int requestCode, @android.support.annotation.Nullable android.content.Intent fillInIntent, int flagsMask, int flagsValues, int extraFlags) throws android.content.IntentSender.SendIntentException {
        if (!this.mStartedIntentSenderFromFragment && requestCode != -1) {
            checkForValidRequestCode(requestCode);
        }
        super.startIntentSenderForResult(intent, requestCode, fillInIntent, flagsMask, flagsValues, extraFlags);
    }

    static void checkForValidRequestCode(int requestCode) {
        if ((-65536 & requestCode) != 0) {
            throw new java.lang.IllegalArgumentException("Can only use lower 16 bits for requestCode");
        }
    }
}
