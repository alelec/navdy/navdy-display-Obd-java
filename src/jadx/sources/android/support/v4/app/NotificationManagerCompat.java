package android.support.v4.app;

public final class NotificationManagerCompat {
    public static final java.lang.String ACTION_BIND_SIDE_CHANNEL = "android.support.BIND_NOTIFICATION_SIDE_CHANNEL";
    public static final java.lang.String EXTRA_USE_SIDE_CHANNEL = "android.support.useSideChannel";
    private static final android.support.v4.app.NotificationManagerCompat.Impl IMPL;
    public static final int IMPORTANCE_DEFAULT = 3;
    public static final int IMPORTANCE_HIGH = 4;
    public static final int IMPORTANCE_LOW = 2;
    public static final int IMPORTANCE_MAX = 5;
    public static final int IMPORTANCE_MIN = 1;
    public static final int IMPORTANCE_NONE = 0;
    public static final int IMPORTANCE_UNSPECIFIED = -1000;
    static final int MAX_SIDE_CHANNEL_SDK_VERSION = 19;
    private static final java.lang.String SETTING_ENABLED_NOTIFICATION_LISTENERS = "enabled_notification_listeners";
    static final int SIDE_CHANNEL_BIND_FLAGS = IMPL.getSideChannelBindFlags();
    private static final int SIDE_CHANNEL_RETRY_BASE_INTERVAL_MS = 1000;
    private static final int SIDE_CHANNEL_RETRY_MAX_COUNT = 6;
    private static final java.lang.String TAG = "NotifManCompat";
    private static java.util.Set<java.lang.String> sEnabledNotificationListenerPackages = new java.util.HashSet();
    private static java.lang.String sEnabledNotificationListeners;
    private static final java.lang.Object sEnabledNotificationListenersLock = new java.lang.Object();
    private static final java.lang.Object sLock = new java.lang.Object();
    private static android.support.v4.app.NotificationManagerCompat.SideChannelManager sSideChannelManager;
    private final android.content.Context mContext;
    private final android.app.NotificationManager mNotificationManager = ((android.app.NotificationManager) this.mContext.getSystemService("notification"));

    private static class CancelTask implements android.support.v4.app.NotificationManagerCompat.Task {
        final boolean all;
        final int id;
        final java.lang.String packageName;
        final java.lang.String tag;

        public CancelTask(java.lang.String packageName2) {
            this.packageName = packageName2;
            this.id = 0;
            this.tag = null;
            this.all = true;
        }

        public CancelTask(java.lang.String packageName2, int id2, java.lang.String tag2) {
            this.packageName = packageName2;
            this.id = id2;
            this.tag = tag2;
            this.all = false;
        }

        public void send(android.support.v4.app.INotificationSideChannel service) throws android.os.RemoteException {
            if (this.all) {
                service.cancelAll(this.packageName);
            } else {
                service.cancel(this.packageName, this.id, this.tag);
            }
        }

        public java.lang.String toString() {
            java.lang.StringBuilder sb = new java.lang.StringBuilder("CancelTask[");
            sb.append("packageName:").append(this.packageName);
            sb.append(", id:").append(this.id);
            sb.append(", tag:").append(this.tag);
            sb.append(", all:").append(this.all);
            sb.append("]");
            return sb.toString();
        }
    }

    interface Impl {
        boolean areNotificationsEnabled(android.content.Context context, android.app.NotificationManager notificationManager);

        void cancelNotification(android.app.NotificationManager notificationManager, java.lang.String str, int i);

        int getImportance(android.app.NotificationManager notificationManager);

        int getSideChannelBindFlags();

        void postNotification(android.app.NotificationManager notificationManager, java.lang.String str, int i, android.app.Notification notification);
    }

    static class ImplApi24 extends android.support.v4.app.NotificationManagerCompat.ImplKitKat {
        ImplApi24() {
        }

        public boolean areNotificationsEnabled(android.content.Context context, android.app.NotificationManager notificationManager) {
            return android.support.v4.app.NotificationManagerCompatApi24.areNotificationsEnabled(notificationManager);
        }

        public int getImportance(android.app.NotificationManager notificationManager) {
            return android.support.v4.app.NotificationManagerCompatApi24.getImportance(notificationManager);
        }
    }

    static class ImplBase implements android.support.v4.app.NotificationManagerCompat.Impl {
        ImplBase() {
        }

        public void cancelNotification(android.app.NotificationManager notificationManager, java.lang.String tag, int id) {
            notificationManager.cancel(tag, id);
        }

        public void postNotification(android.app.NotificationManager notificationManager, java.lang.String tag, int id, android.app.Notification notification) {
            notificationManager.notify(tag, id, notification);
        }

        public int getSideChannelBindFlags() {
            return 1;
        }

        public boolean areNotificationsEnabled(android.content.Context context, android.app.NotificationManager notificationManager) {
            return true;
        }

        public int getImportance(android.app.NotificationManager notificationManager) {
            return android.support.v4.app.NotificationManagerCompat.IMPORTANCE_UNSPECIFIED;
        }
    }

    static class ImplIceCreamSandwich extends android.support.v4.app.NotificationManagerCompat.ImplBase {
        ImplIceCreamSandwich() {
        }

        public int getSideChannelBindFlags() {
            return 33;
        }
    }

    static class ImplKitKat extends android.support.v4.app.NotificationManagerCompat.ImplIceCreamSandwich {
        ImplKitKat() {
        }

        public boolean areNotificationsEnabled(android.content.Context context, android.app.NotificationManager notificationManager) {
            return android.support.v4.app.NotificationManagerCompatKitKat.areNotificationsEnabled(context);
        }
    }

    private static class NotifyTask implements android.support.v4.app.NotificationManagerCompat.Task {
        final int id;
        final android.app.Notification notif;
        final java.lang.String packageName;
        final java.lang.String tag;

        public NotifyTask(java.lang.String packageName2, int id2, java.lang.String tag2, android.app.Notification notif2) {
            this.packageName = packageName2;
            this.id = id2;
            this.tag = tag2;
            this.notif = notif2;
        }

        public void send(android.support.v4.app.INotificationSideChannel service) throws android.os.RemoteException {
            service.notify(this.packageName, this.id, this.tag, this.notif);
        }

        public java.lang.String toString() {
            java.lang.StringBuilder sb = new java.lang.StringBuilder("NotifyTask[");
            sb.append("packageName:").append(this.packageName);
            sb.append(", id:").append(this.id);
            sb.append(", tag:").append(this.tag);
            sb.append("]");
            return sb.toString();
        }
    }

    private static class ServiceConnectedEvent {
        final android.content.ComponentName componentName;
        final android.os.IBinder iBinder;

        public ServiceConnectedEvent(android.content.ComponentName componentName2, android.os.IBinder iBinder2) {
            this.componentName = componentName2;
            this.iBinder = iBinder2;
        }
    }

    private static class SideChannelManager implements android.os.Handler.Callback, android.content.ServiceConnection {
        private static final java.lang.String KEY_BINDER = "binder";
        private static final int MSG_QUEUE_TASK = 0;
        private static final int MSG_RETRY_LISTENER_QUEUE = 3;
        private static final int MSG_SERVICE_CONNECTED = 1;
        private static final int MSG_SERVICE_DISCONNECTED = 2;
        private java.util.Set<java.lang.String> mCachedEnabledPackages = new java.util.HashSet();
        private final android.content.Context mContext;
        private final android.os.Handler mHandler;
        private final android.os.HandlerThread mHandlerThread;
        private final java.util.Map<android.content.ComponentName, android.support.v4.app.NotificationManagerCompat.SideChannelManager.ListenerRecord> mRecordMap = new java.util.HashMap();

        private static class ListenerRecord {
            public boolean bound = false;
            public final android.content.ComponentName componentName;
            public int retryCount = 0;
            public android.support.v4.app.INotificationSideChannel service;
            public java.util.LinkedList<android.support.v4.app.NotificationManagerCompat.Task> taskQueue = new java.util.LinkedList<>();

            public ListenerRecord(android.content.ComponentName componentName2) {
                this.componentName = componentName2;
            }
        }

        public SideChannelManager(android.content.Context context) {
            this.mContext = context;
            this.mHandlerThread = new android.os.HandlerThread("NotificationManagerCompat");
            this.mHandlerThread.start();
            this.mHandler = new android.os.Handler(this.mHandlerThread.getLooper(), this);
        }

        public void queueTask(android.support.v4.app.NotificationManagerCompat.Task task) {
            this.mHandler.obtainMessage(0, task).sendToTarget();
        }

        public boolean handleMessage(android.os.Message msg) {
            switch (msg.what) {
                case 0:
                    handleQueueTask((android.support.v4.app.NotificationManagerCompat.Task) msg.obj);
                    return true;
                case 1:
                    android.support.v4.app.NotificationManagerCompat.ServiceConnectedEvent event = (android.support.v4.app.NotificationManagerCompat.ServiceConnectedEvent) msg.obj;
                    handleServiceConnected(event.componentName, event.iBinder);
                    return true;
                case 2:
                    handleServiceDisconnected((android.content.ComponentName) msg.obj);
                    return true;
                case 3:
                    handleRetryListenerQueue((android.content.ComponentName) msg.obj);
                    return true;
                default:
                    return false;
            }
        }

        private void handleQueueTask(android.support.v4.app.NotificationManagerCompat.Task task) {
            updateListenerMap();
            for (android.support.v4.app.NotificationManagerCompat.SideChannelManager.ListenerRecord record : this.mRecordMap.values()) {
                record.taskQueue.add(task);
                processListenerQueue(record);
            }
        }

        private void handleServiceConnected(android.content.ComponentName componentName, android.os.IBinder iBinder) {
            android.support.v4.app.NotificationManagerCompat.SideChannelManager.ListenerRecord record = (android.support.v4.app.NotificationManagerCompat.SideChannelManager.ListenerRecord) this.mRecordMap.get(componentName);
            if (record != null) {
                record.service = android.support.v4.app.INotificationSideChannel.Stub.asInterface(iBinder);
                record.retryCount = 0;
                processListenerQueue(record);
            }
        }

        private void handleServiceDisconnected(android.content.ComponentName componentName) {
            android.support.v4.app.NotificationManagerCompat.SideChannelManager.ListenerRecord record = (android.support.v4.app.NotificationManagerCompat.SideChannelManager.ListenerRecord) this.mRecordMap.get(componentName);
            if (record != null) {
                ensureServiceUnbound(record);
            }
        }

        private void handleRetryListenerQueue(android.content.ComponentName componentName) {
            android.support.v4.app.NotificationManagerCompat.SideChannelManager.ListenerRecord record = (android.support.v4.app.NotificationManagerCompat.SideChannelManager.ListenerRecord) this.mRecordMap.get(componentName);
            if (record != null) {
                processListenerQueue(record);
            }
        }

        public void onServiceConnected(android.content.ComponentName componentName, android.os.IBinder iBinder) {
            if (android.util.Log.isLoggable(android.support.v4.app.NotificationManagerCompat.TAG, 3)) {
                android.util.Log.d(android.support.v4.app.NotificationManagerCompat.TAG, "Connected to service " + componentName);
            }
            this.mHandler.obtainMessage(1, new android.support.v4.app.NotificationManagerCompat.ServiceConnectedEvent(componentName, iBinder)).sendToTarget();
        }

        public void onServiceDisconnected(android.content.ComponentName componentName) {
            if (android.util.Log.isLoggable(android.support.v4.app.NotificationManagerCompat.TAG, 3)) {
                android.util.Log.d(android.support.v4.app.NotificationManagerCompat.TAG, "Disconnected from service " + componentName);
            }
            this.mHandler.obtainMessage(2, componentName).sendToTarget();
        }

        private void updateListenerMap() {
            java.util.Set<java.lang.String> enabledPackages = android.support.v4.app.NotificationManagerCompat.getEnabledListenerPackages(this.mContext);
            if (!enabledPackages.equals(this.mCachedEnabledPackages)) {
                this.mCachedEnabledPackages = enabledPackages;
                java.util.List<android.content.pm.ResolveInfo> resolveInfos = this.mContext.getPackageManager().queryIntentServices(new android.content.Intent().setAction(android.support.v4.app.NotificationManagerCompat.ACTION_BIND_SIDE_CHANNEL), 4);
                java.util.Set<android.content.ComponentName> enabledComponents = new java.util.HashSet<>();
                for (android.content.pm.ResolveInfo resolveInfo : resolveInfos) {
                    if (enabledPackages.contains(resolveInfo.serviceInfo.packageName)) {
                        android.content.ComponentName componentName = new android.content.ComponentName(resolveInfo.serviceInfo.packageName, resolveInfo.serviceInfo.name);
                        if (resolveInfo.serviceInfo.permission != null) {
                            android.util.Log.w(android.support.v4.app.NotificationManagerCompat.TAG, "Permission present on component " + componentName + ", not adding listener record.");
                        } else {
                            enabledComponents.add(componentName);
                        }
                    }
                }
                for (android.content.ComponentName componentName2 : enabledComponents) {
                    if (!this.mRecordMap.containsKey(componentName2)) {
                        if (android.util.Log.isLoggable(android.support.v4.app.NotificationManagerCompat.TAG, 3)) {
                            android.util.Log.d(android.support.v4.app.NotificationManagerCompat.TAG, "Adding listener record for " + componentName2);
                        }
                        this.mRecordMap.put(componentName2, new android.support.v4.app.NotificationManagerCompat.SideChannelManager.ListenerRecord(componentName2));
                    }
                }
                java.util.Iterator<java.util.Map.Entry<android.content.ComponentName, android.support.v4.app.NotificationManagerCompat.SideChannelManager.ListenerRecord>> it = this.mRecordMap.entrySet().iterator();
                while (it.hasNext()) {
                    java.util.Map.Entry<android.content.ComponentName, android.support.v4.app.NotificationManagerCompat.SideChannelManager.ListenerRecord> entry = (java.util.Map.Entry) it.next();
                    if (!enabledComponents.contains(entry.getKey())) {
                        if (android.util.Log.isLoggable(android.support.v4.app.NotificationManagerCompat.TAG, 3)) {
                            android.util.Log.d(android.support.v4.app.NotificationManagerCompat.TAG, "Removing listener record for " + entry.getKey());
                        }
                        ensureServiceUnbound((android.support.v4.app.NotificationManagerCompat.SideChannelManager.ListenerRecord) entry.getValue());
                        it.remove();
                    }
                }
            }
        }

        private boolean ensureServiceBound(android.support.v4.app.NotificationManagerCompat.SideChannelManager.ListenerRecord record) {
            if (record.bound) {
                return true;
            }
            record.bound = this.mContext.bindService(new android.content.Intent(android.support.v4.app.NotificationManagerCompat.ACTION_BIND_SIDE_CHANNEL).setComponent(record.componentName), this, android.support.v4.app.NotificationManagerCompat.SIDE_CHANNEL_BIND_FLAGS);
            if (record.bound) {
                record.retryCount = 0;
            } else {
                android.util.Log.w(android.support.v4.app.NotificationManagerCompat.TAG, "Unable to bind to listener " + record.componentName);
                this.mContext.unbindService(this);
            }
            return record.bound;
        }

        private void ensureServiceUnbound(android.support.v4.app.NotificationManagerCompat.SideChannelManager.ListenerRecord record) {
            if (record.bound) {
                this.mContext.unbindService(this);
                record.bound = false;
            }
            record.service = null;
        }

        private void scheduleListenerRetry(android.support.v4.app.NotificationManagerCompat.SideChannelManager.ListenerRecord record) {
            if (!this.mHandler.hasMessages(3, record.componentName)) {
                record.retryCount++;
                if (record.retryCount > 6) {
                    android.util.Log.w(android.support.v4.app.NotificationManagerCompat.TAG, "Giving up on delivering " + record.taskQueue.size() + " tasks to " + record.componentName + " after " + record.retryCount + " retries");
                    record.taskQueue.clear();
                    return;
                }
                int delayMs = (1 << (record.retryCount - 1)) * 1000;
                if (android.util.Log.isLoggable(android.support.v4.app.NotificationManagerCompat.TAG, 3)) {
                    android.util.Log.d(android.support.v4.app.NotificationManagerCompat.TAG, "Scheduling retry for " + delayMs + " ms");
                }
                this.mHandler.sendMessageDelayed(this.mHandler.obtainMessage(3, record.componentName), (long) delayMs);
            }
        }

        private void processListenerQueue(android.support.v4.app.NotificationManagerCompat.SideChannelManager.ListenerRecord record) {
            if (android.util.Log.isLoggable(android.support.v4.app.NotificationManagerCompat.TAG, 3)) {
                android.util.Log.d(android.support.v4.app.NotificationManagerCompat.TAG, "Processing component " + record.componentName + ", " + record.taskQueue.size() + " queued tasks");
            }
            if (!record.taskQueue.isEmpty()) {
                if (!ensureServiceBound(record) || record.service == null) {
                    scheduleListenerRetry(record);
                    return;
                }
                while (true) {
                    android.support.v4.app.NotificationManagerCompat.Task task = (android.support.v4.app.NotificationManagerCompat.Task) record.taskQueue.peek();
                    if (task == null) {
                        break;
                    }
                    try {
                        if (android.util.Log.isLoggable(android.support.v4.app.NotificationManagerCompat.TAG, 3)) {
                            android.util.Log.d(android.support.v4.app.NotificationManagerCompat.TAG, "Sending task " + task);
                        }
                        task.send(record.service);
                        record.taskQueue.remove();
                    } catch (android.os.DeadObjectException e) {
                        if (android.util.Log.isLoggable(android.support.v4.app.NotificationManagerCompat.TAG, 3)) {
                            android.util.Log.d(android.support.v4.app.NotificationManagerCompat.TAG, "Remote service has died: " + record.componentName);
                        }
                    } catch (android.os.RemoteException e2) {
                        android.util.Log.w(android.support.v4.app.NotificationManagerCompat.TAG, "RemoteException communicating with " + record.componentName, e2);
                    }
                }
                if (!record.taskQueue.isEmpty()) {
                    scheduleListenerRetry(record);
                }
            }
        }
    }

    private interface Task {
        void send(android.support.v4.app.INotificationSideChannel iNotificationSideChannel) throws android.os.RemoteException;
    }

    static {
        if (android.support.v4.os.BuildCompat.isAtLeastN()) {
            IMPL = new android.support.v4.app.NotificationManagerCompat.ImplApi24();
        } else if (android.os.Build.VERSION.SDK_INT >= 19) {
            IMPL = new android.support.v4.app.NotificationManagerCompat.ImplKitKat();
        } else if (android.os.Build.VERSION.SDK_INT >= 14) {
            IMPL = new android.support.v4.app.NotificationManagerCompat.ImplIceCreamSandwich();
        } else {
            IMPL = new android.support.v4.app.NotificationManagerCompat.ImplBase();
        }
    }

    public static android.support.v4.app.NotificationManagerCompat from(android.content.Context context) {
        return new android.support.v4.app.NotificationManagerCompat(context);
    }

    private NotificationManagerCompat(android.content.Context context) {
        this.mContext = context;
    }

    public void cancel(int id) {
        cancel(null, id);
    }

    public void cancel(java.lang.String tag, int id) {
        IMPL.cancelNotification(this.mNotificationManager, tag, id);
        if (android.os.Build.VERSION.SDK_INT <= 19) {
            pushSideChannelQueue(new android.support.v4.app.NotificationManagerCompat.CancelTask(this.mContext.getPackageName(), id, tag));
        }
    }

    public void cancelAll() {
        this.mNotificationManager.cancelAll();
        if (android.os.Build.VERSION.SDK_INT <= 19) {
            pushSideChannelQueue(new android.support.v4.app.NotificationManagerCompat.CancelTask(this.mContext.getPackageName()));
        }
    }

    public void notify(int id, android.app.Notification notification) {
        notify(null, id, notification);
    }

    public void notify(java.lang.String tag, int id, android.app.Notification notification) {
        if (useSideChannelForNotification(notification)) {
            pushSideChannelQueue(new android.support.v4.app.NotificationManagerCompat.NotifyTask(this.mContext.getPackageName(), id, tag, notification));
            IMPL.cancelNotification(this.mNotificationManager, tag, id);
            return;
        }
        IMPL.postNotification(this.mNotificationManager, tag, id, notification);
    }

    public boolean areNotificationsEnabled() {
        return IMPL.areNotificationsEnabled(this.mContext, this.mNotificationManager);
    }

    public int getImportance() {
        return IMPL.getImportance(this.mNotificationManager);
    }

    public static java.util.Set<java.lang.String> getEnabledListenerPackages(android.content.Context context) {
        java.util.Set<java.lang.String> set;
        java.lang.String enabledNotificationListeners = android.provider.Settings.Secure.getString(context.getContentResolver(), SETTING_ENABLED_NOTIFICATION_LISTENERS);
        synchronized (sEnabledNotificationListenersLock) {
            if (enabledNotificationListeners != null) {
                if (!enabledNotificationListeners.equals(sEnabledNotificationListeners)) {
                    java.lang.String[] components = enabledNotificationListeners.split(":");
                    java.util.Set<java.lang.String> packageNames = new java.util.HashSet<>(components.length);
                    for (java.lang.String component : components) {
                        android.content.ComponentName componentName = android.content.ComponentName.unflattenFromString(component);
                        if (componentName != null) {
                            packageNames.add(componentName.getPackageName());
                        }
                    }
                    sEnabledNotificationListenerPackages = packageNames;
                    sEnabledNotificationListeners = enabledNotificationListeners;
                }
            }
            set = sEnabledNotificationListenerPackages;
        }
        return set;
    }

    private static boolean useSideChannelForNotification(android.app.Notification notification) {
        android.os.Bundle extras = android.support.v4.app.NotificationCompat.getExtras(notification);
        return extras != null && extras.getBoolean(EXTRA_USE_SIDE_CHANNEL);
    }

    private void pushSideChannelQueue(android.support.v4.app.NotificationManagerCompat.Task task) {
        synchronized (sLock) {
            if (sSideChannelManager == null) {
                sSideChannelManager = new android.support.v4.app.NotificationManagerCompat.SideChannelManager(this.mContext.getApplicationContext());
            }
            sSideChannelManager.queueTask(task);
        }
    }
}
