package android.support.v4.app;

class BundleUtil {
    BundleUtil() {
    }

    public static android.os.Bundle[] getBundleArrayFromBundle(android.os.Bundle bundle, java.lang.String key) {
        android.os.Parcelable[] array = bundle.getParcelableArray(key);
        if ((array instanceof android.os.Bundle[]) || array == null) {
            return (android.os.Bundle[]) array;
        }
        android.os.Bundle[] typedArray = (android.os.Bundle[]) java.util.Arrays.copyOf(array, array.length, android.os.Bundle[].class);
        bundle.putParcelableArray(key, typedArray);
        return typedArray;
    }
}
