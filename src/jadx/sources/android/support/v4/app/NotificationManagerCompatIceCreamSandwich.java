package android.support.v4.app;

@android.annotation.TargetApi(14)
@android.support.annotation.RequiresApi(14)
class NotificationManagerCompatIceCreamSandwich {
    static final int SIDE_CHANNEL_BIND_FLAGS = 33;

    NotificationManagerCompatIceCreamSandwich() {
    }
}
