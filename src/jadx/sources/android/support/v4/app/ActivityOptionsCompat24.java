package android.support.v4.app;

@android.annotation.TargetApi(24)
@android.support.annotation.RequiresApi(24)
class ActivityOptionsCompat24 {
    private final android.app.ActivityOptions mActivityOptions;

    public static android.support.v4.app.ActivityOptionsCompat24 makeCustomAnimation(android.content.Context context, int enterResId, int exitResId) {
        return new android.support.v4.app.ActivityOptionsCompat24(android.app.ActivityOptions.makeCustomAnimation(context, enterResId, exitResId));
    }

    public static android.support.v4.app.ActivityOptionsCompat24 makeScaleUpAnimation(android.view.View source, int startX, int startY, int startWidth, int startHeight) {
        return new android.support.v4.app.ActivityOptionsCompat24(android.app.ActivityOptions.makeScaleUpAnimation(source, startX, startY, startWidth, startHeight));
    }

    public static android.support.v4.app.ActivityOptionsCompat24 makeThumbnailScaleUpAnimation(android.view.View source, android.graphics.Bitmap thumbnail, int startX, int startY) {
        return new android.support.v4.app.ActivityOptionsCompat24(android.app.ActivityOptions.makeThumbnailScaleUpAnimation(source, thumbnail, startX, startY));
    }

    public static android.support.v4.app.ActivityOptionsCompat24 makeSceneTransitionAnimation(android.app.Activity activity, android.view.View sharedElement, java.lang.String sharedElementName) {
        return new android.support.v4.app.ActivityOptionsCompat24(android.app.ActivityOptions.makeSceneTransitionAnimation(activity, sharedElement, sharedElementName));
    }

    public static android.support.v4.app.ActivityOptionsCompat24 makeSceneTransitionAnimation(android.app.Activity activity, android.view.View[] sharedElements, java.lang.String[] sharedElementNames) {
        android.util.Pair[] pairs = null;
        if (sharedElements != null) {
            pairs = new android.util.Pair[sharedElements.length];
            for (int i = 0; i < pairs.length; i++) {
                pairs[i] = android.util.Pair.create(sharedElements[i], sharedElementNames[i]);
            }
        }
        return new android.support.v4.app.ActivityOptionsCompat24(android.app.ActivityOptions.makeSceneTransitionAnimation(activity, pairs));
    }

    public static android.support.v4.app.ActivityOptionsCompat24 makeClipRevealAnimation(android.view.View source, int startX, int startY, int width, int height) {
        return new android.support.v4.app.ActivityOptionsCompat24(android.app.ActivityOptions.makeClipRevealAnimation(source, startX, startY, width, height));
    }

    public static android.support.v4.app.ActivityOptionsCompat24 makeTaskLaunchBehind() {
        return new android.support.v4.app.ActivityOptionsCompat24(android.app.ActivityOptions.makeTaskLaunchBehind());
    }

    public static android.support.v4.app.ActivityOptionsCompat24 makeBasic() {
        return new android.support.v4.app.ActivityOptionsCompat24(android.app.ActivityOptions.makeBasic());
    }

    private ActivityOptionsCompat24(android.app.ActivityOptions activityOptions) {
        this.mActivityOptions = activityOptions;
    }

    public android.support.v4.app.ActivityOptionsCompat24 setLaunchBounds(@android.support.annotation.Nullable android.graphics.Rect screenSpacePixelRect) {
        return new android.support.v4.app.ActivityOptionsCompat24(this.mActivityOptions.setLaunchBounds(screenSpacePixelRect));
    }

    public android.graphics.Rect getLaunchBounds() {
        return this.mActivityOptions.getLaunchBounds();
    }

    public android.os.Bundle toBundle() {
        return this.mActivityOptions.toBundle();
    }

    public void update(android.support.v4.app.ActivityOptionsCompat24 otherOptions) {
        this.mActivityOptions.update(otherOptions.mActivityOptions);
    }

    public void requestUsageTimeReport(android.app.PendingIntent receiver) {
        this.mActivityOptions.requestUsageTimeReport(receiver);
    }
}
