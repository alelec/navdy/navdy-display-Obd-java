package android.support.v4.app;

public abstract class FragmentStatePagerAdapter extends android.support.v4.view.PagerAdapter {
    private static final boolean DEBUG = false;
    private static final java.lang.String TAG = "FragmentStatePagerAdapter";
    private android.support.v4.app.FragmentTransaction mCurTransaction = null;
    private android.support.v4.app.Fragment mCurrentPrimaryItem = null;
    private final android.support.v4.app.FragmentManager mFragmentManager;
    private java.util.ArrayList<android.support.v4.app.Fragment> mFragments = new java.util.ArrayList<>();
    private java.util.ArrayList<android.support.v4.app.Fragment.SavedState> mSavedState = new java.util.ArrayList<>();

    public abstract android.support.v4.app.Fragment getItem(int i);

    public FragmentStatePagerAdapter(android.support.v4.app.FragmentManager fm) {
        this.mFragmentManager = fm;
    }

    public void startUpdate(android.view.ViewGroup container) {
        if (container.getId() == -1) {
            throw new java.lang.IllegalStateException("ViewPager with adapter " + this + " requires a view id");
        }
    }

    public java.lang.Object instantiateItem(android.view.ViewGroup container, int position) {
        if (this.mFragments.size() > position) {
            android.support.v4.app.Fragment f = (android.support.v4.app.Fragment) this.mFragments.get(position);
            if (f != null) {
                return f;
            }
        }
        if (this.mCurTransaction == null) {
            this.mCurTransaction = this.mFragmentManager.beginTransaction();
        }
        android.support.v4.app.Fragment fragment = getItem(position);
        if (this.mSavedState.size() > position) {
            android.support.v4.app.Fragment.SavedState fss = (android.support.v4.app.Fragment.SavedState) this.mSavedState.get(position);
            if (fss != null) {
                fragment.setInitialSavedState(fss);
            }
        }
        while (this.mFragments.size() <= position) {
            this.mFragments.add(null);
        }
        fragment.setMenuVisibility(false);
        fragment.setUserVisibleHint(false);
        this.mFragments.set(position, fragment);
        this.mCurTransaction.add(container.getId(), fragment);
        return fragment;
    }

    public void destroyItem(android.view.ViewGroup container, int position, java.lang.Object object) {
        android.support.v4.app.Fragment.SavedState savedState;
        android.support.v4.app.Fragment fragment = (android.support.v4.app.Fragment) object;
        if (this.mCurTransaction == null) {
            this.mCurTransaction = this.mFragmentManager.beginTransaction();
        }
        while (this.mSavedState.size() <= position) {
            this.mSavedState.add(null);
        }
        java.util.ArrayList<android.support.v4.app.Fragment.SavedState> arrayList = this.mSavedState;
        if (fragment.isAdded()) {
            savedState = this.mFragmentManager.saveFragmentInstanceState(fragment);
        } else {
            savedState = null;
        }
        arrayList.set(position, savedState);
        this.mFragments.set(position, null);
        this.mCurTransaction.remove(fragment);
    }

    public void setPrimaryItem(android.view.ViewGroup container, int position, java.lang.Object object) {
        android.support.v4.app.Fragment fragment = (android.support.v4.app.Fragment) object;
        if (fragment != this.mCurrentPrimaryItem) {
            if (this.mCurrentPrimaryItem != null) {
                this.mCurrentPrimaryItem.setMenuVisibility(false);
                this.mCurrentPrimaryItem.setUserVisibleHint(false);
            }
            if (fragment != null) {
                fragment.setMenuVisibility(true);
                fragment.setUserVisibleHint(true);
            }
            this.mCurrentPrimaryItem = fragment;
        }
    }

    public void finishUpdate(android.view.ViewGroup container) {
        if (this.mCurTransaction != null) {
            this.mCurTransaction.commitNowAllowingStateLoss();
            this.mCurTransaction = null;
        }
    }

    public boolean isViewFromObject(android.view.View view, java.lang.Object object) {
        return ((android.support.v4.app.Fragment) object).getView() == view;
    }

    public android.os.Parcelable saveState() {
        android.os.Bundle state = null;
        if (this.mSavedState.size() > 0) {
            state = new android.os.Bundle();
            android.support.v4.app.Fragment.SavedState[] fss = new android.support.v4.app.Fragment.SavedState[this.mSavedState.size()];
            this.mSavedState.toArray(fss);
            state.putParcelableArray("states", fss);
        }
        for (int i = 0; i < this.mFragments.size(); i++) {
            android.support.v4.app.Fragment f = (android.support.v4.app.Fragment) this.mFragments.get(i);
            if (f != null && f.isAdded()) {
                if (state == null) {
                    state = new android.os.Bundle();
                }
                this.mFragmentManager.putFragment(state, "f" + i, f);
            }
        }
        return state;
    }

    public void restoreState(android.os.Parcelable state, java.lang.ClassLoader loader) {
        if (state != null) {
            android.os.Bundle bundle = (android.os.Bundle) state;
            bundle.setClassLoader(loader);
            android.os.Parcelable[] fss = bundle.getParcelableArray("states");
            this.mSavedState.clear();
            this.mFragments.clear();
            if (fss != null) {
                for (android.os.Parcelable parcelable : fss) {
                    this.mSavedState.add((android.support.v4.app.Fragment.SavedState) parcelable);
                }
            }
            for (java.lang.String key : bundle.keySet()) {
                if (key.startsWith("f")) {
                    int index = java.lang.Integer.parseInt(key.substring(1));
                    android.support.v4.app.Fragment f = this.mFragmentManager.getFragment(bundle, key);
                    if (f != null) {
                        while (this.mFragments.size() <= index) {
                            this.mFragments.add(null);
                        }
                        f.setMenuVisibility(false);
                        this.mFragments.set(index, f);
                    } else {
                        android.util.Log.w(TAG, "Bad fragment at key " + key);
                    }
                }
            }
        }
    }
}
