package android.support.v4.app;

@android.annotation.TargetApi(24)
@android.support.annotation.RequiresApi(24)
class ServiceCompatApi24 {
    ServiceCompatApi24() {
    }

    public static void stopForeground(android.app.Service service, int flags) {
        service.stopForeground(flags);
    }
}
