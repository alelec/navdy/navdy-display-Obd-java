package android.support.v4.app;

public abstract class SharedElementCallback {
    private static final java.lang.String BUNDLE_SNAPSHOT_BITMAP = "sharedElement:snapshot:bitmap";
    private static final java.lang.String BUNDLE_SNAPSHOT_IMAGE_MATRIX = "sharedElement:snapshot:imageMatrix";
    private static final java.lang.String BUNDLE_SNAPSHOT_IMAGE_SCALETYPE = "sharedElement:snapshot:imageScaleType";
    private static int MAX_IMAGE_SIZE = 1048576;
    private android.graphics.Matrix mTempMatrix;

    public interface OnSharedElementsReadyListener {
        void onSharedElementsReady();
    }

    public void onSharedElementStart(java.util.List<java.lang.String> list, java.util.List<android.view.View> list2, java.util.List<android.view.View> list3) {
    }

    public void onSharedElementEnd(java.util.List<java.lang.String> list, java.util.List<android.view.View> list2, java.util.List<android.view.View> list3) {
    }

    public void onRejectSharedElements(java.util.List<android.view.View> list) {
    }

    public void onMapSharedElements(java.util.List<java.lang.String> list, java.util.Map<java.lang.String, android.view.View> map) {
    }

    public android.os.Parcelable onCaptureSharedElementSnapshot(android.view.View sharedElement, android.graphics.Matrix viewToGlobalMatrix, android.graphics.RectF screenBounds) {
        if (sharedElement instanceof android.widget.ImageView) {
            android.widget.ImageView imageView = (android.widget.ImageView) sharedElement;
            android.graphics.drawable.Drawable d = imageView.getDrawable();
            android.graphics.drawable.Drawable bg = imageView.getBackground();
            if (d != null && bg == null) {
                android.graphics.Bitmap bitmap = createDrawableBitmap(d);
                if (bitmap != null) {
                    android.os.Bundle bundle = new android.os.Bundle();
                    bundle.putParcelable(BUNDLE_SNAPSHOT_BITMAP, bitmap);
                    bundle.putString(BUNDLE_SNAPSHOT_IMAGE_SCALETYPE, imageView.getScaleType().toString());
                    if (imageView.getScaleType() != android.widget.ImageView.ScaleType.MATRIX) {
                        return bundle;
                    }
                    float[] values = new float[9];
                    imageView.getImageMatrix().getValues(values);
                    bundle.putFloatArray(BUNDLE_SNAPSHOT_IMAGE_MATRIX, values);
                    return bundle;
                }
            }
        }
        int bitmapWidth = java.lang.Math.round(screenBounds.width());
        int bitmapHeight = java.lang.Math.round(screenBounds.height());
        android.graphics.Bitmap bitmap2 = null;
        if (bitmapWidth > 0 && bitmapHeight > 0) {
            float scale = java.lang.Math.min(1.0f, ((float) MAX_IMAGE_SIZE) / ((float) (bitmapWidth * bitmapHeight)));
            int bitmapWidth2 = (int) (((float) bitmapWidth) * scale);
            int bitmapHeight2 = (int) (((float) bitmapHeight) * scale);
            if (this.mTempMatrix == null) {
                this.mTempMatrix = new android.graphics.Matrix();
            }
            this.mTempMatrix.set(viewToGlobalMatrix);
            this.mTempMatrix.postTranslate(-screenBounds.left, -screenBounds.top);
            this.mTempMatrix.postScale(scale, scale);
            bitmap2 = android.graphics.Bitmap.createBitmap(bitmapWidth2, bitmapHeight2, android.graphics.Bitmap.Config.ARGB_8888);
            android.graphics.Canvas canvas = new android.graphics.Canvas(bitmap2);
            canvas.concat(this.mTempMatrix);
            sharedElement.draw(canvas);
        }
        return bitmap2;
    }

    private static android.graphics.Bitmap createDrawableBitmap(android.graphics.drawable.Drawable drawable) {
        int width = drawable.getIntrinsicWidth();
        int height = drawable.getIntrinsicHeight();
        if (width <= 0 || height <= 0) {
            return null;
        }
        float scale = java.lang.Math.min(1.0f, ((float) MAX_IMAGE_SIZE) / ((float) (width * height)));
        if ((drawable instanceof android.graphics.drawable.BitmapDrawable) && scale == 1.0f) {
            return ((android.graphics.drawable.BitmapDrawable) drawable).getBitmap();
        }
        int bitmapWidth = (int) (((float) width) * scale);
        int bitmapHeight = (int) (((float) height) * scale);
        android.graphics.Bitmap bitmap = android.graphics.Bitmap.createBitmap(bitmapWidth, bitmapHeight, android.graphics.Bitmap.Config.ARGB_8888);
        android.graphics.Canvas canvas = new android.graphics.Canvas(bitmap);
        android.graphics.Rect existingBounds = drawable.getBounds();
        int left = existingBounds.left;
        int top = existingBounds.top;
        int right = existingBounds.right;
        int bottom = existingBounds.bottom;
        drawable.setBounds(0, 0, bitmapWidth, bitmapHeight);
        drawable.draw(canvas);
        drawable.setBounds(left, top, right, bottom);
        return bitmap;
    }

    public android.view.View onCreateSnapshotView(android.content.Context context, android.os.Parcelable snapshot) {
        android.widget.ImageView view = null;
        if (snapshot instanceof android.os.Bundle) {
            android.os.Bundle bundle = (android.os.Bundle) snapshot;
            android.graphics.Bitmap bitmap = (android.graphics.Bitmap) bundle.getParcelable(BUNDLE_SNAPSHOT_BITMAP);
            if (bitmap == null) {
                return null;
            }
            android.widget.ImageView imageView = new android.widget.ImageView(context);
            view = imageView;
            imageView.setImageBitmap(bitmap);
            imageView.setScaleType(android.widget.ImageView.ScaleType.valueOf(bundle.getString(BUNDLE_SNAPSHOT_IMAGE_SCALETYPE)));
            if (imageView.getScaleType() == android.widget.ImageView.ScaleType.MATRIX) {
                float[] values = bundle.getFloatArray(BUNDLE_SNAPSHOT_IMAGE_MATRIX);
                android.graphics.Matrix matrix = new android.graphics.Matrix();
                matrix.setValues(values);
                imageView.setImageMatrix(matrix);
            }
        } else if (snapshot instanceof android.graphics.Bitmap) {
            android.graphics.Bitmap bitmap2 = (android.graphics.Bitmap) snapshot;
            view = new android.widget.ImageView(context);
            view.setImageBitmap(bitmap2);
        }
        return view;
    }

    public void onSharedElementsArrived(java.util.List<java.lang.String> list, java.util.List<android.view.View> list2, android.support.v4.app.SharedElementCallback.OnSharedElementsReadyListener listener) {
        listener.onSharedElementsReady();
    }
}
