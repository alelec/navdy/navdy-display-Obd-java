package android.support.v4.app;

public final class ActivityManagerCompat {
    private ActivityManagerCompat() {
    }

    public static boolean isLowRamDevice(@android.support.annotation.NonNull android.app.ActivityManager am) {
        if (android.os.Build.VERSION.SDK_INT >= 19) {
            return android.support.v4.app.ActivityManagerCompatKitKat.isLowRamDevice(am);
        }
        return false;
    }
}
