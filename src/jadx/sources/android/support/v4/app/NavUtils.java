package android.support.v4.app;

public final class NavUtils {
    private static final android.support.v4.app.NavUtils.NavUtilsImpl IMPL;
    public static final java.lang.String PARENT_ACTIVITY = "android.support.PARENT_ACTIVITY";
    private static final java.lang.String TAG = "NavUtils";

    interface NavUtilsImpl {
        android.content.Intent getParentActivityIntent(android.app.Activity activity);

        java.lang.String getParentActivityName(android.content.Context context, android.content.pm.ActivityInfo activityInfo);

        void navigateUpTo(android.app.Activity activity, android.content.Intent intent);

        boolean shouldUpRecreateTask(android.app.Activity activity, android.content.Intent intent);
    }

    static class NavUtilsImplBase implements android.support.v4.app.NavUtils.NavUtilsImpl {
        NavUtilsImplBase() {
        }

        public android.content.Intent getParentActivityIntent(android.app.Activity activity) {
            android.content.Intent parentIntent = null;
            java.lang.String parentName = android.support.v4.app.NavUtils.getParentActivityName(activity);
            if (parentName != null) {
                android.content.ComponentName target = new android.content.ComponentName(activity, parentName);
                try {
                    if (android.support.v4.app.NavUtils.getParentActivityName(activity, target) == null) {
                        parentIntent = android.support.v4.content.IntentCompat.makeMainActivity(target);
                    } else {
                        parentIntent = new android.content.Intent().setComponent(target);
                    }
                } catch (android.content.pm.PackageManager.NameNotFoundException e) {
                    android.util.Log.e(android.support.v4.app.NavUtils.TAG, "getParentActivityIntent: bad parentActivityName '" + parentName + "' in manifest");
                }
            }
            return parentIntent;
        }

        public boolean shouldUpRecreateTask(android.app.Activity activity, android.content.Intent targetIntent) {
            java.lang.String action = activity.getIntent().getAction();
            return action != null && !action.equals("android.intent.action.MAIN");
        }

        public void navigateUpTo(android.app.Activity activity, android.content.Intent upIntent) {
            upIntent.addFlags(67108864);
            activity.startActivity(upIntent);
            activity.finish();
        }

        public java.lang.String getParentActivityName(android.content.Context context, android.content.pm.ActivityInfo info) {
            if (info.metaData == null) {
                return null;
            }
            java.lang.String parentActivity = info.metaData.getString(android.support.v4.app.NavUtils.PARENT_ACTIVITY);
            if (parentActivity == null) {
                return null;
            }
            if (parentActivity.charAt(0) == '.') {
                return context.getPackageName() + parentActivity;
            }
            return parentActivity;
        }
    }

    static class NavUtilsImplJB extends android.support.v4.app.NavUtils.NavUtilsImplBase {
        NavUtilsImplJB() {
        }

        public android.content.Intent getParentActivityIntent(android.app.Activity activity) {
            android.content.Intent result = android.support.v4.app.NavUtilsJB.getParentActivityIntent(activity);
            if (result == null) {
                return superGetParentActivityIntent(activity);
            }
            return result;
        }

        /* access modifiers changed from: 0000 */
        public android.content.Intent superGetParentActivityIntent(android.app.Activity activity) {
            return super.getParentActivityIntent(activity);
        }

        public boolean shouldUpRecreateTask(android.app.Activity activity, android.content.Intent targetIntent) {
            return android.support.v4.app.NavUtilsJB.shouldUpRecreateTask(activity, targetIntent);
        }

        public void navigateUpTo(android.app.Activity activity, android.content.Intent upIntent) {
            android.support.v4.app.NavUtilsJB.navigateUpTo(activity, upIntent);
        }

        public java.lang.String getParentActivityName(android.content.Context context, android.content.pm.ActivityInfo info) {
            java.lang.String result = android.support.v4.app.NavUtilsJB.getParentActivityName(info);
            if (result == null) {
                return super.getParentActivityName(context, info);
            }
            return result;
        }
    }

    static {
        if (android.os.Build.VERSION.SDK_INT >= 16) {
            IMPL = new android.support.v4.app.NavUtils.NavUtilsImplJB();
        } else {
            IMPL = new android.support.v4.app.NavUtils.NavUtilsImplBase();
        }
    }

    public static boolean shouldUpRecreateTask(android.app.Activity sourceActivity, android.content.Intent targetIntent) {
        return IMPL.shouldUpRecreateTask(sourceActivity, targetIntent);
    }

    public static void navigateUpFromSameTask(android.app.Activity sourceActivity) {
        android.content.Intent upIntent = getParentActivityIntent(sourceActivity);
        if (upIntent == null) {
            throw new java.lang.IllegalArgumentException("Activity " + sourceActivity.getClass().getSimpleName() + " does not have a parent activity name specified." + " (Did you forget to add the android.support.PARENT_ACTIVITY <meta-data> " + " element in your manifest?)");
        }
        navigateUpTo(sourceActivity, upIntent);
    }

    public static void navigateUpTo(android.app.Activity sourceActivity, android.content.Intent upIntent) {
        IMPL.navigateUpTo(sourceActivity, upIntent);
    }

    public static android.content.Intent getParentActivityIntent(android.app.Activity sourceActivity) {
        return IMPL.getParentActivityIntent(sourceActivity);
    }

    public static android.content.Intent getParentActivityIntent(android.content.Context context, java.lang.Class<?> sourceActivityClass) throws android.content.pm.PackageManager.NameNotFoundException {
        java.lang.String parentActivity = getParentActivityName(context, new android.content.ComponentName(context, sourceActivityClass));
        if (parentActivity == null) {
            return null;
        }
        android.content.ComponentName target = new android.content.ComponentName(context, parentActivity);
        if (getParentActivityName(context, target) == null) {
            return android.support.v4.content.IntentCompat.makeMainActivity(target);
        }
        return new android.content.Intent().setComponent(target);
    }

    public static android.content.Intent getParentActivityIntent(android.content.Context context, android.content.ComponentName componentName) throws android.content.pm.PackageManager.NameNotFoundException {
        java.lang.String parentActivity = getParentActivityName(context, componentName);
        if (parentActivity == null) {
            return null;
        }
        android.content.ComponentName target = new android.content.ComponentName(componentName.getPackageName(), parentActivity);
        if (getParentActivityName(context, target) == null) {
            return android.support.v4.content.IntentCompat.makeMainActivity(target);
        }
        return new android.content.Intent().setComponent(target);
    }

    @android.support.annotation.Nullable
    public static java.lang.String getParentActivityName(android.app.Activity sourceActivity) {
        try {
            return getParentActivityName(sourceActivity, sourceActivity.getComponentName());
        } catch (android.content.pm.PackageManager.NameNotFoundException e) {
            throw new java.lang.IllegalArgumentException(e);
        }
    }

    @android.support.annotation.Nullable
    public static java.lang.String getParentActivityName(android.content.Context context, android.content.ComponentName componentName) throws android.content.pm.PackageManager.NameNotFoundException {
        return IMPL.getParentActivityName(context, context.getPackageManager().getActivityInfo(componentName, 128));
    }

    private NavUtils() {
    }
}
