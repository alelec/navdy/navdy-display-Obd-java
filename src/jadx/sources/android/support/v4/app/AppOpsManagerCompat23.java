package android.support.v4.app;

@android.annotation.TargetApi(23)
@android.support.annotation.RequiresApi(23)
class AppOpsManagerCompat23 {
    AppOpsManagerCompat23() {
    }

    public static java.lang.String permissionToOp(java.lang.String permission) {
        return android.app.AppOpsManager.permissionToOp(permission);
    }

    public static int noteOp(android.content.Context context, java.lang.String op, int uid, java.lang.String packageName) {
        return ((android.app.AppOpsManager) context.getSystemService(android.app.AppOpsManager.class)).noteOp(op, uid, packageName);
    }

    public static int noteProxyOp(android.content.Context context, java.lang.String op, java.lang.String proxiedPackageName) {
        return ((android.app.AppOpsManager) context.getSystemService(android.app.AppOpsManager.class)).noteProxyOp(op, proxiedPackageName);
    }
}
