package android.support.v4.app;

@android.annotation.TargetApi(21)
@android.support.annotation.RequiresApi(21)
class NotificationCompatApi21 {
    public static final java.lang.String CATEGORY_ALARM = "alarm";
    public static final java.lang.String CATEGORY_CALL = "call";
    public static final java.lang.String CATEGORY_EMAIL = "email";
    public static final java.lang.String CATEGORY_ERROR = "err";
    public static final java.lang.String CATEGORY_EVENT = "event";
    public static final java.lang.String CATEGORY_MESSAGE = "msg";
    public static final java.lang.String CATEGORY_PROGRESS = "progress";
    public static final java.lang.String CATEGORY_PROMO = "promo";
    public static final java.lang.String CATEGORY_RECOMMENDATION = "recommendation";
    public static final java.lang.String CATEGORY_SERVICE = "service";
    public static final java.lang.String CATEGORY_SOCIAL = "social";
    public static final java.lang.String CATEGORY_STATUS = "status";
    public static final java.lang.String CATEGORY_SYSTEM = "sys";
    public static final java.lang.String CATEGORY_TRANSPORT = "transport";
    private static final java.lang.String KEY_AUTHOR = "author";
    private static final java.lang.String KEY_MESSAGES = "messages";
    private static final java.lang.String KEY_ON_READ = "on_read";
    private static final java.lang.String KEY_ON_REPLY = "on_reply";
    private static final java.lang.String KEY_PARTICIPANTS = "participants";
    private static final java.lang.String KEY_REMOTE_INPUT = "remote_input";
    private static final java.lang.String KEY_TEXT = "text";
    private static final java.lang.String KEY_TIMESTAMP = "timestamp";

    public static class Builder implements android.support.v4.app.NotificationBuilderWithBuilderAccessor, android.support.v4.app.NotificationBuilderWithActions {
        private android.app.Notification.Builder b;
        private android.widget.RemoteViews mBigContentView;
        private android.widget.RemoteViews mContentView;
        private android.os.Bundle mExtras;
        private android.widget.RemoteViews mHeadsUpContentView;

        public Builder(android.content.Context context, android.app.Notification n, java.lang.CharSequence contentTitle, java.lang.CharSequence contentText, java.lang.CharSequence contentInfo, android.widget.RemoteViews tickerView, int number, android.app.PendingIntent contentIntent, android.app.PendingIntent fullScreenIntent, android.graphics.Bitmap largeIcon, int progressMax, int progress, boolean progressIndeterminate, boolean showWhen, boolean useChronometer, int priority, java.lang.CharSequence subText, boolean localOnly, java.lang.String category, java.util.ArrayList<java.lang.String> people, android.os.Bundle extras, int color, int visibility, android.app.Notification publicVersion, java.lang.String groupKey, boolean groupSummary, java.lang.String sortKey, android.widget.RemoteViews contentView, android.widget.RemoteViews bigContentView, android.widget.RemoteViews headsUpContentView) {
            this.b = new android.app.Notification.Builder(context).setWhen(n.when).setShowWhen(showWhen).setSmallIcon(n.icon, n.iconLevel).setContent(n.contentView).setTicker(n.tickerText, tickerView).setSound(n.sound, n.audioStreamType).setVibrate(n.vibrate).setLights(n.ledARGB, n.ledOnMS, n.ledOffMS).setOngoing((n.flags & 2) != 0).setOnlyAlertOnce((n.flags & 8) != 0).setAutoCancel((n.flags & 16) != 0).setDefaults(n.defaults).setContentTitle(contentTitle).setContentText(contentText).setSubText(subText).setContentInfo(contentInfo).setContentIntent(contentIntent).setDeleteIntent(n.deleteIntent).setFullScreenIntent(fullScreenIntent, (n.flags & 128) != 0).setLargeIcon(largeIcon).setNumber(number).setUsesChronometer(useChronometer).setPriority(priority).setProgress(progressMax, progress, progressIndeterminate).setLocalOnly(localOnly).setGroup(groupKey).setGroupSummary(groupSummary).setSortKey(sortKey).setCategory(category).setColor(color).setVisibility(visibility).setPublicVersion(publicVersion);
            this.mExtras = new android.os.Bundle();
            if (extras != null) {
                this.mExtras.putAll(extras);
            }
            java.util.Iterator it = people.iterator();
            while (it.hasNext()) {
                this.b.addPerson((java.lang.String) it.next());
            }
            this.mContentView = contentView;
            this.mBigContentView = bigContentView;
            this.mHeadsUpContentView = headsUpContentView;
        }

        public void addAction(android.support.v4.app.NotificationCompatBase.Action action) {
            android.support.v4.app.NotificationCompatApi20.addAction(this.b, action);
        }

        public android.app.Notification.Builder getBuilder() {
            return this.b;
        }

        public android.app.Notification build() {
            this.b.setExtras(this.mExtras);
            android.app.Notification notification = this.b.build();
            if (this.mContentView != null) {
                notification.contentView = this.mContentView;
            }
            if (this.mBigContentView != null) {
                notification.bigContentView = this.mBigContentView;
            }
            if (this.mHeadsUpContentView != null) {
                notification.headsUpContentView = this.mHeadsUpContentView;
            }
            return notification;
        }
    }

    NotificationCompatApi21() {
    }

    public static java.lang.String getCategory(android.app.Notification notif) {
        return notif.category;
    }

    static android.os.Bundle getBundleForUnreadConversation(android.support.v4.app.NotificationCompatBase.UnreadConversation uc) {
        if (uc == null) {
            return null;
        }
        android.os.Bundle b = new android.os.Bundle();
        java.lang.String author = null;
        if (uc.getParticipants() != null && uc.getParticipants().length > 1) {
            author = uc.getParticipants()[0];
        }
        android.os.Parcelable[] messages = new android.os.Parcelable[uc.getMessages().length];
        for (int i = 0; i < messages.length; i++) {
            android.os.Bundle m = new android.os.Bundle();
            m.putString(KEY_TEXT, uc.getMessages()[i]);
            m.putString(KEY_AUTHOR, author);
            messages[i] = m;
        }
        b.putParcelableArray(KEY_MESSAGES, messages);
        android.support.v4.app.RemoteInputCompatBase.RemoteInput remoteInput = uc.getRemoteInput();
        if (remoteInput != null) {
            b.putParcelable(KEY_REMOTE_INPUT, fromCompatRemoteInput(remoteInput));
        }
        b.putParcelable(KEY_ON_REPLY, uc.getReplyPendingIntent());
        b.putParcelable(KEY_ON_READ, uc.getReadPendingIntent());
        b.putStringArray(KEY_PARTICIPANTS, uc.getParticipants());
        b.putLong(KEY_TIMESTAMP, uc.getLatestTimestamp());
        return b;
    }

    static android.support.v4.app.NotificationCompatBase.UnreadConversation getUnreadConversationFromBundle(android.os.Bundle b, android.support.v4.app.NotificationCompatBase.UnreadConversation.Factory factory, android.support.v4.app.RemoteInputCompatBase.RemoteInput.Factory remoteInputFactory) {
        android.support.v4.app.RemoteInputCompatBase.RemoteInput remoteInput = null;
        if (b == null) {
            return null;
        }
        android.os.Parcelable[] parcelableMessages = b.getParcelableArray(KEY_MESSAGES);
        java.lang.String[] messages = null;
        if (parcelableMessages != null) {
            java.lang.String[] tmp = new java.lang.String[parcelableMessages.length];
            boolean success = true;
            int i = 0;
            while (true) {
                if (i >= tmp.length) {
                    break;
                } else if (!(parcelableMessages[i] instanceof android.os.Bundle)) {
                    success = false;
                    break;
                } else {
                    tmp[i] = ((android.os.Bundle) parcelableMessages[i]).getString(KEY_TEXT);
                    if (tmp[i] == null) {
                        success = false;
                        break;
                    }
                    i++;
                }
            }
            if (!success) {
                return null;
            }
            messages = tmp;
        }
        android.app.PendingIntent onRead = (android.app.PendingIntent) b.getParcelable(KEY_ON_READ);
        android.app.PendingIntent onReply = (android.app.PendingIntent) b.getParcelable(KEY_ON_REPLY);
        android.app.RemoteInput remoteInput2 = (android.app.RemoteInput) b.getParcelable(KEY_REMOTE_INPUT);
        java.lang.String[] participants = b.getStringArray(KEY_PARTICIPANTS);
        if (participants == null || participants.length != 1) {
            return null;
        }
        if (remoteInput2 != null) {
            remoteInput = toCompatRemoteInput(remoteInput2, remoteInputFactory);
        }
        return factory.build(messages, remoteInput, onReply, onRead, participants, b.getLong(KEY_TIMESTAMP));
    }

    private static android.app.RemoteInput fromCompatRemoteInput(android.support.v4.app.RemoteInputCompatBase.RemoteInput src) {
        return new android.app.RemoteInput.Builder(src.getResultKey()).setLabel(src.getLabel()).setChoices(src.getChoices()).setAllowFreeFormInput(src.getAllowFreeFormInput()).addExtras(src.getExtras()).build();
    }

    private static android.support.v4.app.RemoteInputCompatBase.RemoteInput toCompatRemoteInput(android.app.RemoteInput remoteInput, android.support.v4.app.RemoteInputCompatBase.RemoteInput.Factory factory) {
        return factory.build(remoteInput.getResultKey(), remoteInput.getLabel(), remoteInput.getChoices(), remoteInput.getAllowFreeFormInput(), remoteInput.getExtras());
    }
}
