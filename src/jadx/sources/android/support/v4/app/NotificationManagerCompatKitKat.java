package android.support.v4.app;

@android.annotation.TargetApi(19)
@android.support.annotation.RequiresApi(19)
class NotificationManagerCompatKitKat {
    private static final java.lang.String CHECK_OP_NO_THROW = "checkOpNoThrow";
    private static final java.lang.String OP_POST_NOTIFICATION = "OP_POST_NOTIFICATION";

    NotificationManagerCompatKitKat() {
    }

    public static boolean areNotificationsEnabled(android.content.Context context) {
        android.app.AppOpsManager appOps = (android.app.AppOpsManager) context.getSystemService("appops");
        android.content.pm.ApplicationInfo appInfo = context.getApplicationInfo();
        java.lang.String pkg = context.getApplicationContext().getPackageName();
        int uid = appInfo.uid;
        try {
            java.lang.Class<?> appOpsClass = java.lang.Class.forName(android.app.AppOpsManager.class.getName());
            if (((java.lang.Integer) appOpsClass.getMethod(CHECK_OP_NO_THROW, new java.lang.Class[]{java.lang.Integer.TYPE, java.lang.Integer.TYPE, java.lang.String.class}).invoke(appOps, new java.lang.Object[]{java.lang.Integer.valueOf(((java.lang.Integer) appOpsClass.getDeclaredField(OP_POST_NOTIFICATION).get(java.lang.Integer.class)).intValue()), java.lang.Integer.valueOf(uid), pkg})).intValue() == 0) {
                return true;
            }
            return false;
        } catch (java.lang.ClassNotFoundException | java.lang.IllegalAccessException | java.lang.NoSuchFieldException | java.lang.NoSuchMethodException | java.lang.RuntimeException | java.lang.reflect.InvocationTargetException e) {
            return true;
        }
    }
}
