package android.support.v4.app;

public class ActivityOptionsCompat {
    public static final java.lang.String EXTRA_USAGE_TIME_REPORT = "android.activity.usage_time";
    public static final java.lang.String EXTRA_USAGE_TIME_REPORT_PACKAGES = "android.usage_time_packages";

    @android.support.annotation.RequiresApi(21)
    @android.annotation.TargetApi(21)
    private static class ActivityOptionsImpl21 extends android.support.v4.app.ActivityOptionsCompat {
        private final android.support.v4.app.ActivityOptionsCompat21 mImpl;

        ActivityOptionsImpl21(android.support.v4.app.ActivityOptionsCompat21 impl) {
            this.mImpl = impl;
        }

        public android.os.Bundle toBundle() {
            return this.mImpl.toBundle();
        }

        public void update(android.support.v4.app.ActivityOptionsCompat otherOptions) {
            if (otherOptions instanceof android.support.v4.app.ActivityOptionsCompat.ActivityOptionsImpl21) {
                this.mImpl.update(((android.support.v4.app.ActivityOptionsCompat.ActivityOptionsImpl21) otherOptions).mImpl);
            }
        }
    }

    @android.support.annotation.RequiresApi(23)
    @android.annotation.TargetApi(23)
    private static class ActivityOptionsImpl23 extends android.support.v4.app.ActivityOptionsCompat {
        private final android.support.v4.app.ActivityOptionsCompat23 mImpl;

        ActivityOptionsImpl23(android.support.v4.app.ActivityOptionsCompat23 impl) {
            this.mImpl = impl;
        }

        public android.os.Bundle toBundle() {
            return this.mImpl.toBundle();
        }

        public void update(android.support.v4.app.ActivityOptionsCompat otherOptions) {
            if (otherOptions instanceof android.support.v4.app.ActivityOptionsCompat.ActivityOptionsImpl23) {
                this.mImpl.update(((android.support.v4.app.ActivityOptionsCompat.ActivityOptionsImpl23) otherOptions).mImpl);
            }
        }

        public void requestUsageTimeReport(android.app.PendingIntent receiver) {
            this.mImpl.requestUsageTimeReport(receiver);
        }
    }

    @android.support.annotation.RequiresApi(24)
    @android.annotation.TargetApi(24)
    private static class ActivityOptionsImpl24 extends android.support.v4.app.ActivityOptionsCompat {
        private final android.support.v4.app.ActivityOptionsCompat24 mImpl;

        ActivityOptionsImpl24(android.support.v4.app.ActivityOptionsCompat24 impl) {
            this.mImpl = impl;
        }

        public android.os.Bundle toBundle() {
            return this.mImpl.toBundle();
        }

        public void update(android.support.v4.app.ActivityOptionsCompat otherOptions) {
            if (otherOptions instanceof android.support.v4.app.ActivityOptionsCompat.ActivityOptionsImpl24) {
                this.mImpl.update(((android.support.v4.app.ActivityOptionsCompat.ActivityOptionsImpl24) otherOptions).mImpl);
            }
        }

        public android.support.v4.app.ActivityOptionsCompat setLaunchBounds(@android.support.annotation.Nullable android.graphics.Rect screenSpacePixelRect) {
            return new android.support.v4.app.ActivityOptionsCompat.ActivityOptionsImpl24(this.mImpl.setLaunchBounds(screenSpacePixelRect));
        }

        public android.graphics.Rect getLaunchBounds() {
            return this.mImpl.getLaunchBounds();
        }

        public void requestUsageTimeReport(android.app.PendingIntent receiver) {
            this.mImpl.requestUsageTimeReport(receiver);
        }
    }

    @android.support.annotation.RequiresApi(16)
    @android.annotation.TargetApi(16)
    private static class ActivityOptionsImplJB extends android.support.v4.app.ActivityOptionsCompat {
        private final android.support.v4.app.ActivityOptionsCompatJB mImpl;

        ActivityOptionsImplJB(android.support.v4.app.ActivityOptionsCompatJB impl) {
            this.mImpl = impl;
        }

        public android.os.Bundle toBundle() {
            return this.mImpl.toBundle();
        }

        public void update(android.support.v4.app.ActivityOptionsCompat otherOptions) {
            if (otherOptions instanceof android.support.v4.app.ActivityOptionsCompat.ActivityOptionsImplJB) {
                this.mImpl.update(((android.support.v4.app.ActivityOptionsCompat.ActivityOptionsImplJB) otherOptions).mImpl);
            }
        }
    }

    public static android.support.v4.app.ActivityOptionsCompat makeCustomAnimation(android.content.Context context, int enterResId, int exitResId) {
        if (android.os.Build.VERSION.SDK_INT >= 24) {
            return new android.support.v4.app.ActivityOptionsCompat.ActivityOptionsImpl24(android.support.v4.app.ActivityOptionsCompat24.makeCustomAnimation(context, enterResId, exitResId));
        }
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            return new android.support.v4.app.ActivityOptionsCompat.ActivityOptionsImpl23(android.support.v4.app.ActivityOptionsCompat23.makeCustomAnimation(context, enterResId, exitResId));
        }
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            return new android.support.v4.app.ActivityOptionsCompat.ActivityOptionsImpl21(android.support.v4.app.ActivityOptionsCompat21.makeCustomAnimation(context, enterResId, exitResId));
        }
        if (android.os.Build.VERSION.SDK_INT >= 16) {
            return new android.support.v4.app.ActivityOptionsCompat.ActivityOptionsImplJB(android.support.v4.app.ActivityOptionsCompatJB.makeCustomAnimation(context, enterResId, exitResId));
        }
        return new android.support.v4.app.ActivityOptionsCompat();
    }

    public static android.support.v4.app.ActivityOptionsCompat makeScaleUpAnimation(android.view.View source, int startX, int startY, int startWidth, int startHeight) {
        if (android.os.Build.VERSION.SDK_INT >= 24) {
            return new android.support.v4.app.ActivityOptionsCompat.ActivityOptionsImpl24(android.support.v4.app.ActivityOptionsCompat24.makeScaleUpAnimation(source, startX, startY, startWidth, startHeight));
        }
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            return new android.support.v4.app.ActivityOptionsCompat.ActivityOptionsImpl23(android.support.v4.app.ActivityOptionsCompat23.makeScaleUpAnimation(source, startX, startY, startWidth, startHeight));
        }
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            return new android.support.v4.app.ActivityOptionsCompat.ActivityOptionsImpl21(android.support.v4.app.ActivityOptionsCompat21.makeScaleUpAnimation(source, startX, startY, startWidth, startHeight));
        }
        if (android.os.Build.VERSION.SDK_INT >= 16) {
            return new android.support.v4.app.ActivityOptionsCompat.ActivityOptionsImplJB(android.support.v4.app.ActivityOptionsCompatJB.makeScaleUpAnimation(source, startX, startY, startWidth, startHeight));
        }
        return new android.support.v4.app.ActivityOptionsCompat();
    }

    public static android.support.v4.app.ActivityOptionsCompat makeClipRevealAnimation(android.view.View source, int startX, int startY, int width, int height) {
        if (android.os.Build.VERSION.SDK_INT >= 24) {
            return new android.support.v4.app.ActivityOptionsCompat.ActivityOptionsImpl24(android.support.v4.app.ActivityOptionsCompat24.makeClipRevealAnimation(source, startX, startY, width, height));
        }
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            return new android.support.v4.app.ActivityOptionsCompat.ActivityOptionsImpl23(android.support.v4.app.ActivityOptionsCompat23.makeClipRevealAnimation(source, startX, startY, width, height));
        }
        return new android.support.v4.app.ActivityOptionsCompat();
    }

    public static android.support.v4.app.ActivityOptionsCompat makeThumbnailScaleUpAnimation(android.view.View source, android.graphics.Bitmap thumbnail, int startX, int startY) {
        if (android.os.Build.VERSION.SDK_INT >= 24) {
            return new android.support.v4.app.ActivityOptionsCompat.ActivityOptionsImpl24(android.support.v4.app.ActivityOptionsCompat24.makeThumbnailScaleUpAnimation(source, thumbnail, startX, startY));
        }
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            return new android.support.v4.app.ActivityOptionsCompat.ActivityOptionsImpl23(android.support.v4.app.ActivityOptionsCompat23.makeThumbnailScaleUpAnimation(source, thumbnail, startX, startY));
        }
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            return new android.support.v4.app.ActivityOptionsCompat.ActivityOptionsImpl21(android.support.v4.app.ActivityOptionsCompat21.makeThumbnailScaleUpAnimation(source, thumbnail, startX, startY));
        }
        if (android.os.Build.VERSION.SDK_INT >= 16) {
            return new android.support.v4.app.ActivityOptionsCompat.ActivityOptionsImplJB(android.support.v4.app.ActivityOptionsCompatJB.makeThumbnailScaleUpAnimation(source, thumbnail, startX, startY));
        }
        return new android.support.v4.app.ActivityOptionsCompat();
    }

    public static android.support.v4.app.ActivityOptionsCompat makeSceneTransitionAnimation(android.app.Activity activity, android.view.View sharedElement, java.lang.String sharedElementName) {
        if (android.os.Build.VERSION.SDK_INT >= 24) {
            return new android.support.v4.app.ActivityOptionsCompat.ActivityOptionsImpl24(android.support.v4.app.ActivityOptionsCompat24.makeSceneTransitionAnimation(activity, sharedElement, sharedElementName));
        }
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            return new android.support.v4.app.ActivityOptionsCompat.ActivityOptionsImpl23(android.support.v4.app.ActivityOptionsCompat23.makeSceneTransitionAnimation(activity, sharedElement, sharedElementName));
        }
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            return new android.support.v4.app.ActivityOptionsCompat.ActivityOptionsImpl21(android.support.v4.app.ActivityOptionsCompat21.makeSceneTransitionAnimation(activity, sharedElement, sharedElementName));
        }
        return new android.support.v4.app.ActivityOptionsCompat();
    }

    public static android.support.v4.app.ActivityOptionsCompat makeSceneTransitionAnimation(android.app.Activity activity, android.support.v4.util.Pair<android.view.View, java.lang.String>... sharedElements) {
        if (android.os.Build.VERSION.SDK_INT < 21) {
            return new android.support.v4.app.ActivityOptionsCompat();
        }
        android.view.View[] views = null;
        java.lang.String[] names = null;
        if (sharedElements != null) {
            views = new android.view.View[sharedElements.length];
            names = new java.lang.String[sharedElements.length];
            for (int i = 0; i < sharedElements.length; i++) {
                views[i] = (android.view.View) sharedElements[i].first;
                names[i] = (java.lang.String) sharedElements[i].second;
            }
        }
        if (android.os.Build.VERSION.SDK_INT >= 24) {
            return new android.support.v4.app.ActivityOptionsCompat.ActivityOptionsImpl24(android.support.v4.app.ActivityOptionsCompat24.makeSceneTransitionAnimation(activity, views, names));
        }
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            return new android.support.v4.app.ActivityOptionsCompat.ActivityOptionsImpl23(android.support.v4.app.ActivityOptionsCompat23.makeSceneTransitionAnimation(activity, views, names));
        }
        return new android.support.v4.app.ActivityOptionsCompat.ActivityOptionsImpl21(android.support.v4.app.ActivityOptionsCompat21.makeSceneTransitionAnimation(activity, views, names));
    }

    public static android.support.v4.app.ActivityOptionsCompat makeTaskLaunchBehind() {
        if (android.os.Build.VERSION.SDK_INT >= 24) {
            return new android.support.v4.app.ActivityOptionsCompat.ActivityOptionsImpl24(android.support.v4.app.ActivityOptionsCompat24.makeTaskLaunchBehind());
        }
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            return new android.support.v4.app.ActivityOptionsCompat.ActivityOptionsImpl23(android.support.v4.app.ActivityOptionsCompat23.makeTaskLaunchBehind());
        }
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            return new android.support.v4.app.ActivityOptionsCompat.ActivityOptionsImpl21(android.support.v4.app.ActivityOptionsCompat21.makeTaskLaunchBehind());
        }
        return new android.support.v4.app.ActivityOptionsCompat();
    }

    public static android.support.v4.app.ActivityOptionsCompat makeBasic() {
        if (android.os.Build.VERSION.SDK_INT >= 24) {
            return new android.support.v4.app.ActivityOptionsCompat.ActivityOptionsImpl24(android.support.v4.app.ActivityOptionsCompat24.makeBasic());
        }
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            return new android.support.v4.app.ActivityOptionsCompat.ActivityOptionsImpl23(android.support.v4.app.ActivityOptionsCompat23.makeBasic());
        }
        return new android.support.v4.app.ActivityOptionsCompat();
    }

    protected ActivityOptionsCompat() {
    }

    public android.support.v4.app.ActivityOptionsCompat setLaunchBounds(@android.support.annotation.Nullable android.graphics.Rect screenSpacePixelRect) {
        return null;
    }

    @android.support.annotation.Nullable
    public android.graphics.Rect getLaunchBounds() {
        return null;
    }

    public android.os.Bundle toBundle() {
        return null;
    }

    public void update(android.support.v4.app.ActivityOptionsCompat otherOptions) {
    }

    public void requestUsageTimeReport(android.app.PendingIntent receiver) {
    }
}
