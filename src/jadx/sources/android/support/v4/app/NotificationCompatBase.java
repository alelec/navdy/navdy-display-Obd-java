package android.support.v4.app;

@android.support.annotation.RequiresApi(9)
@android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
@android.annotation.TargetApi(9)
public class NotificationCompatBase {
    private static java.lang.reflect.Method sSetLatestEventInfo;

    public static abstract class Action {

        public interface Factory {
            android.support.v4.app.NotificationCompatBase.Action build(int i, java.lang.CharSequence charSequence, android.app.PendingIntent pendingIntent, android.os.Bundle bundle, android.support.v4.app.RemoteInputCompatBase.RemoteInput[] remoteInputArr, boolean z);

            android.support.v4.app.NotificationCompatBase.Action[] newArray(int i);
        }

        public abstract android.app.PendingIntent getActionIntent();

        public abstract boolean getAllowGeneratedReplies();

        public abstract android.os.Bundle getExtras();

        public abstract int getIcon();

        public abstract android.support.v4.app.RemoteInputCompatBase.RemoteInput[] getRemoteInputs();

        public abstract java.lang.CharSequence getTitle();
    }

    public static abstract class UnreadConversation {

        public interface Factory {
            android.support.v4.app.NotificationCompatBase.UnreadConversation build(java.lang.String[] strArr, android.support.v4.app.RemoteInputCompatBase.RemoteInput remoteInput, android.app.PendingIntent pendingIntent, android.app.PendingIntent pendingIntent2, java.lang.String[] strArr2, long j);
        }

        /* access modifiers changed from: 0000 */
        public abstract long getLatestTimestamp();

        /* access modifiers changed from: 0000 */
        public abstract java.lang.String[] getMessages();

        /* access modifiers changed from: 0000 */
        public abstract java.lang.String getParticipant();

        /* access modifiers changed from: 0000 */
        public abstract java.lang.String[] getParticipants();

        /* access modifiers changed from: 0000 */
        public abstract android.app.PendingIntent getReadPendingIntent();

        /* access modifiers changed from: 0000 */
        public abstract android.support.v4.app.RemoteInputCompatBase.RemoteInput getRemoteInput();

        /* access modifiers changed from: 0000 */
        public abstract android.app.PendingIntent getReplyPendingIntent();
    }

    public static android.app.Notification add(android.app.Notification notification, android.content.Context context, java.lang.CharSequence contentTitle, java.lang.CharSequence contentText, android.app.PendingIntent contentIntent, android.app.PendingIntent fullScreenIntent) {
        if (sSetLatestEventInfo == null) {
            try {
                sSetLatestEventInfo = android.app.Notification.class.getMethod("setLatestEventInfo", new java.lang.Class[]{android.content.Context.class, java.lang.CharSequence.class, java.lang.CharSequence.class, android.app.PendingIntent.class});
            } catch (java.lang.NoSuchMethodException e) {
                throw new java.lang.RuntimeException(e);
            }
        }
        try {
            sSetLatestEventInfo.invoke(notification, new java.lang.Object[]{context, contentTitle, contentText, contentIntent});
            notification.fullScreenIntent = fullScreenIntent;
            return notification;
        } catch (java.lang.IllegalAccessException | java.lang.reflect.InvocationTargetException e2) {
            throw new java.lang.RuntimeException(e2);
        }
    }
}
