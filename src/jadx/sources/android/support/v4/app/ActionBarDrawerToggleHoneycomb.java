package android.support.v4.app;

@android.annotation.TargetApi(11)
@android.support.annotation.RequiresApi(11)
class ActionBarDrawerToggleHoneycomb {
    private static final java.lang.String TAG = "ActionBarDrawerToggleHoneycomb";
    private static final int[] THEME_ATTRS = {16843531};

    private static class SetIndicatorInfo {
        public java.lang.reflect.Method setHomeActionContentDescription;
        public java.lang.reflect.Method setHomeAsUpIndicator;
        public android.widget.ImageView upIndicatorView;

        SetIndicatorInfo(android.app.Activity activity) {
            android.view.View up;
            try {
                this.setHomeAsUpIndicator = android.app.ActionBar.class.getDeclaredMethod("setHomeAsUpIndicator", new java.lang.Class[]{android.graphics.drawable.Drawable.class});
                this.setHomeActionContentDescription = android.app.ActionBar.class.getDeclaredMethod("setHomeActionContentDescription", new java.lang.Class[]{java.lang.Integer.TYPE});
            } catch (java.lang.NoSuchMethodException e) {
                android.view.View home = activity.findViewById(16908332);
                if (home != null) {
                    android.view.ViewGroup parent = (android.view.ViewGroup) home.getParent();
                    if (parent.getChildCount() == 2) {
                        android.view.View first = parent.getChildAt(0);
                        android.view.View second = parent.getChildAt(1);
                        if (first.getId() == 16908332) {
                            up = second;
                        } else {
                            up = first;
                        }
                        if (up instanceof android.widget.ImageView) {
                            this.upIndicatorView = (android.widget.ImageView) up;
                        }
                    }
                }
            }
        }
    }

    ActionBarDrawerToggleHoneycomb() {
    }

    public static java.lang.Object setActionBarUpIndicator(java.lang.Object info, android.app.Activity activity, android.graphics.drawable.Drawable drawable, int contentDescRes) {
        if (info == null) {
            info = new android.support.v4.app.ActionBarDrawerToggleHoneycomb.SetIndicatorInfo(activity);
        }
        android.support.v4.app.ActionBarDrawerToggleHoneycomb.SetIndicatorInfo sii = (android.support.v4.app.ActionBarDrawerToggleHoneycomb.SetIndicatorInfo) info;
        if (sii.setHomeAsUpIndicator != null) {
            try {
                android.app.ActionBar actionBar = activity.getActionBar();
                sii.setHomeAsUpIndicator.invoke(actionBar, new java.lang.Object[]{drawable});
                sii.setHomeActionContentDescription.invoke(actionBar, new java.lang.Object[]{java.lang.Integer.valueOf(contentDescRes)});
            } catch (java.lang.Exception e) {
                android.util.Log.w(TAG, "Couldn't set home-as-up indicator via JB-MR2 API", e);
            }
        } else if (sii.upIndicatorView != null) {
            sii.upIndicatorView.setImageDrawable(drawable);
        } else {
            android.util.Log.w(TAG, "Couldn't set home-as-up indicator");
        }
        return info;
    }

    public static java.lang.Object setActionBarDescription(java.lang.Object info, android.app.Activity activity, int contentDescRes) {
        if (info == null) {
            info = new android.support.v4.app.ActionBarDrawerToggleHoneycomb.SetIndicatorInfo(activity);
        }
        android.support.v4.app.ActionBarDrawerToggleHoneycomb.SetIndicatorInfo sii = (android.support.v4.app.ActionBarDrawerToggleHoneycomb.SetIndicatorInfo) info;
        if (sii.setHomeAsUpIndicator != null) {
            try {
                android.app.ActionBar actionBar = activity.getActionBar();
                sii.setHomeActionContentDescription.invoke(actionBar, new java.lang.Object[]{java.lang.Integer.valueOf(contentDescRes)});
                if (android.os.Build.VERSION.SDK_INT <= 19) {
                    actionBar.setSubtitle(actionBar.getSubtitle());
                }
            } catch (java.lang.Exception e) {
                android.util.Log.w(TAG, "Couldn't set content description via JB-MR2 API", e);
            }
        }
        return info;
    }

    public static android.graphics.drawable.Drawable getThemeUpIndicator(android.app.Activity activity) {
        android.content.res.TypedArray a = activity.obtainStyledAttributes(THEME_ATTRS);
        android.graphics.drawable.Drawable result = a.getDrawable(0);
        a.recycle();
        return result;
    }
}
