package android.support.v4.app;

public final class AppOpsManagerCompat {
    private static final android.support.v4.app.AppOpsManagerCompat.AppOpsManagerImpl IMPL;
    public static final int MODE_ALLOWED = 0;
    public static final int MODE_DEFAULT = 3;
    public static final int MODE_IGNORED = 1;

    private static class AppOpsManager23 extends android.support.v4.app.AppOpsManagerCompat.AppOpsManagerImpl {
        AppOpsManager23() {
        }

        public java.lang.String permissionToOp(java.lang.String permission) {
            return android.support.v4.app.AppOpsManagerCompat23.permissionToOp(permission);
        }

        public int noteOp(android.content.Context context, java.lang.String op, int uid, java.lang.String packageName) {
            return android.support.v4.app.AppOpsManagerCompat23.noteOp(context, op, uid, packageName);
        }

        public int noteProxyOp(android.content.Context context, java.lang.String op, java.lang.String proxiedPackageName) {
            return android.support.v4.app.AppOpsManagerCompat23.noteProxyOp(context, op, proxiedPackageName);
        }
    }

    private static class AppOpsManagerImpl {
        AppOpsManagerImpl() {
        }

        public java.lang.String permissionToOp(java.lang.String permission) {
            return null;
        }

        public int noteOp(android.content.Context context, java.lang.String op, int uid, java.lang.String packageName) {
            return 1;
        }

        public int noteProxyOp(android.content.Context context, java.lang.String op, java.lang.String proxiedPackageName) {
            return 1;
        }
    }

    static {
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            IMPL = new android.support.v4.app.AppOpsManagerCompat.AppOpsManager23();
        } else {
            IMPL = new android.support.v4.app.AppOpsManagerCompat.AppOpsManagerImpl();
        }
    }

    private AppOpsManagerCompat() {
    }

    public static java.lang.String permissionToOp(@android.support.annotation.NonNull java.lang.String permission) {
        return IMPL.permissionToOp(permission);
    }

    public static int noteOp(@android.support.annotation.NonNull android.content.Context context, @android.support.annotation.NonNull java.lang.String op, int uid, @android.support.annotation.NonNull java.lang.String packageName) {
        return IMPL.noteOp(context, op, uid, packageName);
    }

    public static int noteProxyOp(@android.support.annotation.NonNull android.content.Context context, @android.support.annotation.NonNull java.lang.String op, @android.support.annotation.NonNull java.lang.String proxiedPackageName) {
        return IMPL.noteProxyOp(context, op, proxiedPackageName);
    }
}
