package android.support.v4.app;

class NoSaveStateFrameLayout extends android.widget.FrameLayout {
    static android.view.ViewGroup wrap(android.view.View child) {
        android.support.v4.app.NoSaveStateFrameLayout wrapper = new android.support.v4.app.NoSaveStateFrameLayout(child.getContext());
        android.view.ViewGroup.LayoutParams childParams = child.getLayoutParams();
        if (childParams != null) {
            wrapper.setLayoutParams(childParams);
        }
        child.setLayoutParams(new android.widget.FrameLayout.LayoutParams(-1, -1));
        wrapper.addView(child);
        return wrapper;
    }

    public NoSaveStateFrameLayout(android.content.Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public void dispatchSaveInstanceState(android.util.SparseArray<android.os.Parcelable> container) {
        dispatchFreezeSelfOnly(container);
    }

    /* access modifiers changed from: protected */
    public void dispatchRestoreInstanceState(android.util.SparseArray<android.os.Parcelable> container) {
        dispatchThawSelfOnly(container);
    }
}
