package android.support.v4.app;

@android.annotation.TargetApi(23)
@android.support.annotation.RequiresApi(23)
class ActivityOptionsCompat23 {
    private final android.app.ActivityOptions mActivityOptions;

    public static android.support.v4.app.ActivityOptionsCompat23 makeCustomAnimation(android.content.Context context, int enterResId, int exitResId) {
        return new android.support.v4.app.ActivityOptionsCompat23(android.app.ActivityOptions.makeCustomAnimation(context, enterResId, exitResId));
    }

    public static android.support.v4.app.ActivityOptionsCompat23 makeScaleUpAnimation(android.view.View source, int startX, int startY, int startWidth, int startHeight) {
        return new android.support.v4.app.ActivityOptionsCompat23(android.app.ActivityOptions.makeScaleUpAnimation(source, startX, startY, startWidth, startHeight));
    }

    public static android.support.v4.app.ActivityOptionsCompat23 makeThumbnailScaleUpAnimation(android.view.View source, android.graphics.Bitmap thumbnail, int startX, int startY) {
        return new android.support.v4.app.ActivityOptionsCompat23(android.app.ActivityOptions.makeThumbnailScaleUpAnimation(source, thumbnail, startX, startY));
    }

    public static android.support.v4.app.ActivityOptionsCompat23 makeSceneTransitionAnimation(android.app.Activity activity, android.view.View sharedElement, java.lang.String sharedElementName) {
        return new android.support.v4.app.ActivityOptionsCompat23(android.app.ActivityOptions.makeSceneTransitionAnimation(activity, sharedElement, sharedElementName));
    }

    public static android.support.v4.app.ActivityOptionsCompat23 makeSceneTransitionAnimation(android.app.Activity activity, android.view.View[] sharedElements, java.lang.String[] sharedElementNames) {
        android.util.Pair[] pairs = null;
        if (sharedElements != null) {
            pairs = new android.util.Pair[sharedElements.length];
            for (int i = 0; i < pairs.length; i++) {
                pairs[i] = android.util.Pair.create(sharedElements[i], sharedElementNames[i]);
            }
        }
        return new android.support.v4.app.ActivityOptionsCompat23(android.app.ActivityOptions.makeSceneTransitionAnimation(activity, pairs));
    }

    public static android.support.v4.app.ActivityOptionsCompat23 makeClipRevealAnimation(android.view.View source, int startX, int startY, int width, int height) {
        return new android.support.v4.app.ActivityOptionsCompat23(android.app.ActivityOptions.makeClipRevealAnimation(source, startX, startY, width, height));
    }

    public static android.support.v4.app.ActivityOptionsCompat23 makeTaskLaunchBehind() {
        return new android.support.v4.app.ActivityOptionsCompat23(android.app.ActivityOptions.makeTaskLaunchBehind());
    }

    public static android.support.v4.app.ActivityOptionsCompat23 makeBasic() {
        return new android.support.v4.app.ActivityOptionsCompat23(android.app.ActivityOptions.makeBasic());
    }

    private ActivityOptionsCompat23(android.app.ActivityOptions activityOptions) {
        this.mActivityOptions = activityOptions;
    }

    public android.os.Bundle toBundle() {
        return this.mActivityOptions.toBundle();
    }

    public void update(android.support.v4.app.ActivityOptionsCompat23 otherOptions) {
        this.mActivityOptions.update(otherOptions.mActivityOptions);
    }

    public void requestUsageTimeReport(android.app.PendingIntent receiver) {
        this.mActivityOptions.requestUsageTimeReport(receiver);
    }
}
