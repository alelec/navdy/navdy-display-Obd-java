package android.support.v4.app;

public final class ServiceCompat {
    static final android.support.v4.app.ServiceCompat.ServiceCompatImpl IMPL;
    public static final int START_STICKY = 1;
    public static final int STOP_FOREGROUND_DETACH = 2;
    public static final int STOP_FOREGROUND_REMOVE = 1;

    static class Api24ServiceCompatImpl implements android.support.v4.app.ServiceCompat.ServiceCompatImpl {
        Api24ServiceCompatImpl() {
        }

        public void stopForeground(android.app.Service service, int flags) {
            android.support.v4.app.ServiceCompatApi24.stopForeground(service, flags);
        }
    }

    static class BaseServiceCompatImpl implements android.support.v4.app.ServiceCompat.ServiceCompatImpl {
        BaseServiceCompatImpl() {
        }

        public void stopForeground(android.app.Service service, int flags) {
            service.stopForeground((flags & 1) != 0);
        }
    }

    interface ServiceCompatImpl {
        void stopForeground(android.app.Service service, int i);
    }

    @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
    @java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.SOURCE)
    public @interface StopForegroundFlags {
    }

    private ServiceCompat() {
    }

    static {
        if (android.support.v4.os.BuildCompat.isAtLeastN()) {
            IMPL = new android.support.v4.app.ServiceCompat.Api24ServiceCompatImpl();
        } else {
            IMPL = new android.support.v4.app.ServiceCompat.BaseServiceCompatImpl();
        }
    }

    public static void stopForeground(android.app.Service service, int flags) {
        IMPL.stopForeground(service, flags);
    }
}
