package android.support.v4.app;

@java.lang.Deprecated
public class ActionBarDrawerToggle implements android.support.v4.widget.DrawerLayout.DrawerListener {
    private static final int ID_HOME = 16908332;
    private static final android.support.v4.app.ActionBarDrawerToggle.ActionBarDrawerToggleImpl IMPL;
    private static final float TOGGLE_DRAWABLE_OFFSET = 0.33333334f;
    final android.app.Activity mActivity;
    private final android.support.v4.app.ActionBarDrawerToggle.Delegate mActivityImpl;
    private final int mCloseDrawerContentDescRes;
    private android.graphics.drawable.Drawable mDrawerImage;
    private final int mDrawerImageResource;
    private boolean mDrawerIndicatorEnabled;
    private final android.support.v4.widget.DrawerLayout mDrawerLayout;
    private boolean mHasCustomUpIndicator;
    private android.graphics.drawable.Drawable mHomeAsUpIndicator;
    private final int mOpenDrawerContentDescRes;
    private java.lang.Object mSetIndicatorInfo;
    private android.support.v4.app.ActionBarDrawerToggle.SlideDrawable mSlider;

    private interface ActionBarDrawerToggleImpl {
        android.graphics.drawable.Drawable getThemeUpIndicator(android.app.Activity activity);

        java.lang.Object setActionBarDescription(java.lang.Object obj, android.app.Activity activity, int i);

        java.lang.Object setActionBarUpIndicator(java.lang.Object obj, android.app.Activity activity, android.graphics.drawable.Drawable drawable, int i);
    }

    private static class ActionBarDrawerToggleImplBase implements android.support.v4.app.ActionBarDrawerToggle.ActionBarDrawerToggleImpl {
        ActionBarDrawerToggleImplBase() {
        }

        public android.graphics.drawable.Drawable getThemeUpIndicator(android.app.Activity activity) {
            return null;
        }

        public java.lang.Object setActionBarUpIndicator(java.lang.Object info, android.app.Activity activity, android.graphics.drawable.Drawable themeImage, int contentDescRes) {
            return info;
        }

        public java.lang.Object setActionBarDescription(java.lang.Object info, android.app.Activity activity, int contentDescRes) {
            return info;
        }
    }

    @android.support.annotation.RequiresApi(11)
    @android.annotation.TargetApi(11)
    private static class ActionBarDrawerToggleImplHC implements android.support.v4.app.ActionBarDrawerToggle.ActionBarDrawerToggleImpl {
        ActionBarDrawerToggleImplHC() {
        }

        public android.graphics.drawable.Drawable getThemeUpIndicator(android.app.Activity activity) {
            return android.support.v4.app.ActionBarDrawerToggleHoneycomb.getThemeUpIndicator(activity);
        }

        public java.lang.Object setActionBarUpIndicator(java.lang.Object info, android.app.Activity activity, android.graphics.drawable.Drawable themeImage, int contentDescRes) {
            return android.support.v4.app.ActionBarDrawerToggleHoneycomb.setActionBarUpIndicator(info, activity, themeImage, contentDescRes);
        }

        public java.lang.Object setActionBarDescription(java.lang.Object info, android.app.Activity activity, int contentDescRes) {
            return android.support.v4.app.ActionBarDrawerToggleHoneycomb.setActionBarDescription(info, activity, contentDescRes);
        }
    }

    @android.support.annotation.RequiresApi(18)
    @android.annotation.TargetApi(18)
    private static class ActionBarDrawerToggleImplJellybeanMR2 implements android.support.v4.app.ActionBarDrawerToggle.ActionBarDrawerToggleImpl {
        ActionBarDrawerToggleImplJellybeanMR2() {
        }

        public android.graphics.drawable.Drawable getThemeUpIndicator(android.app.Activity activity) {
            return android.support.v4.app.ActionBarDrawerToggleJellybeanMR2.getThemeUpIndicator(activity);
        }

        public java.lang.Object setActionBarUpIndicator(java.lang.Object info, android.app.Activity activity, android.graphics.drawable.Drawable themeImage, int contentDescRes) {
            return android.support.v4.app.ActionBarDrawerToggleJellybeanMR2.setActionBarUpIndicator(info, activity, themeImage, contentDescRes);
        }

        public java.lang.Object setActionBarDescription(java.lang.Object info, android.app.Activity activity, int contentDescRes) {
            return android.support.v4.app.ActionBarDrawerToggleJellybeanMR2.setActionBarDescription(info, activity, contentDescRes);
        }
    }

    public interface Delegate {
        @android.support.annotation.Nullable
        android.graphics.drawable.Drawable getThemeUpIndicator();

        void setActionBarDescription(@android.support.annotation.StringRes int i);

        void setActionBarUpIndicator(android.graphics.drawable.Drawable drawable, @android.support.annotation.StringRes int i);
    }

    public interface DelegateProvider {
        @android.support.annotation.Nullable
        android.support.v4.app.ActionBarDrawerToggle.Delegate getDrawerToggleDelegate();
    }

    private class SlideDrawable extends android.graphics.drawable.InsetDrawable implements android.graphics.drawable.Drawable.Callback {
        private final boolean mHasMirroring;
        private float mOffset;
        private float mPosition;
        private final android.graphics.Rect mTmpRect;
        final /* synthetic */ android.support.v4.app.ActionBarDrawerToggle this$0;

        SlideDrawable(android.support.v4.app.ActionBarDrawerToggle actionBarDrawerToggle, android.graphics.drawable.Drawable wrapped) {
            boolean z = false;
            this.this$0 = actionBarDrawerToggle;
            super(wrapped, 0);
            if (android.os.Build.VERSION.SDK_INT > 18) {
                z = true;
            }
            this.mHasMirroring = z;
            this.mTmpRect = new android.graphics.Rect();
        }

        public void setPosition(float position) {
            this.mPosition = position;
            invalidateSelf();
        }

        public float getPosition() {
            return this.mPosition;
        }

        public void setOffset(float offset) {
            this.mOffset = offset;
            invalidateSelf();
        }

        public void draw(android.graphics.Canvas canvas) {
            int flipRtl = 1;
            copyBounds(this.mTmpRect);
            canvas.save();
            boolean isLayoutRTL = android.support.v4.view.ViewCompat.getLayoutDirection(this.this$0.mActivity.getWindow().getDecorView()) == 1;
            if (isLayoutRTL) {
                flipRtl = -1;
            }
            int width = this.mTmpRect.width();
            canvas.translate((-this.mOffset) * ((float) width) * this.mPosition * ((float) flipRtl), 0.0f);
            if (isLayoutRTL && !this.mHasMirroring) {
                canvas.translate((float) width, 0.0f);
                canvas.scale(-1.0f, 1.0f);
            }
            super.draw(canvas);
            canvas.restore();
        }
    }

    static {
        int version = android.os.Build.VERSION.SDK_INT;
        if (version >= 18) {
            IMPL = new android.support.v4.app.ActionBarDrawerToggle.ActionBarDrawerToggleImplJellybeanMR2();
        } else if (version >= 11) {
            IMPL = new android.support.v4.app.ActionBarDrawerToggle.ActionBarDrawerToggleImplHC();
        } else {
            IMPL = new android.support.v4.app.ActionBarDrawerToggle.ActionBarDrawerToggleImplBase();
        }
    }

    public ActionBarDrawerToggle(android.app.Activity activity, android.support.v4.widget.DrawerLayout drawerLayout, @android.support.annotation.DrawableRes int drawerImageRes, @android.support.annotation.StringRes int openDrawerContentDescRes, @android.support.annotation.StringRes int closeDrawerContentDescRes) {
        this(activity, drawerLayout, !assumeMaterial(activity), drawerImageRes, openDrawerContentDescRes, closeDrawerContentDescRes);
    }

    private static boolean assumeMaterial(android.content.Context context) {
        return context.getApplicationInfo().targetSdkVersion >= 21 && android.os.Build.VERSION.SDK_INT >= 21;
    }

    public ActionBarDrawerToggle(android.app.Activity activity, android.support.v4.widget.DrawerLayout drawerLayout, boolean animate, @android.support.annotation.DrawableRes int drawerImageRes, @android.support.annotation.StringRes int openDrawerContentDescRes, @android.support.annotation.StringRes int closeDrawerContentDescRes) {
        this.mDrawerIndicatorEnabled = true;
        this.mActivity = activity;
        if (activity instanceof android.support.v4.app.ActionBarDrawerToggle.DelegateProvider) {
            this.mActivityImpl = ((android.support.v4.app.ActionBarDrawerToggle.DelegateProvider) activity).getDrawerToggleDelegate();
        } else {
            this.mActivityImpl = null;
        }
        this.mDrawerLayout = drawerLayout;
        this.mDrawerImageResource = drawerImageRes;
        this.mOpenDrawerContentDescRes = openDrawerContentDescRes;
        this.mCloseDrawerContentDescRes = closeDrawerContentDescRes;
        this.mHomeAsUpIndicator = getThemeUpIndicator();
        this.mDrawerImage = android.support.v4.content.ContextCompat.getDrawable(activity, drawerImageRes);
        this.mSlider = new android.support.v4.app.ActionBarDrawerToggle.SlideDrawable(this, this.mDrawerImage);
        this.mSlider.setOffset(animate ? TOGGLE_DRAWABLE_OFFSET : 0.0f);
    }

    public void syncState() {
        if (this.mDrawerLayout.isDrawerOpen((int) android.support.v4.view.GravityCompat.START)) {
            this.mSlider.setPosition(1.0f);
        } else {
            this.mSlider.setPosition(0.0f);
        }
        if (this.mDrawerIndicatorEnabled) {
            setActionBarUpIndicator(this.mSlider, this.mDrawerLayout.isDrawerOpen((int) android.support.v4.view.GravityCompat.START) ? this.mCloseDrawerContentDescRes : this.mOpenDrawerContentDescRes);
        }
    }

    public void setHomeAsUpIndicator(android.graphics.drawable.Drawable indicator) {
        if (indicator == null) {
            this.mHomeAsUpIndicator = getThemeUpIndicator();
            this.mHasCustomUpIndicator = false;
        } else {
            this.mHomeAsUpIndicator = indicator;
            this.mHasCustomUpIndicator = true;
        }
        if (!this.mDrawerIndicatorEnabled) {
            setActionBarUpIndicator(this.mHomeAsUpIndicator, 0);
        }
    }

    public void setHomeAsUpIndicator(int resId) {
        android.graphics.drawable.Drawable indicator = null;
        if (resId != 0) {
            indicator = android.support.v4.content.ContextCompat.getDrawable(this.mActivity, resId);
        }
        setHomeAsUpIndicator(indicator);
    }

    public void setDrawerIndicatorEnabled(boolean enable) {
        if (enable != this.mDrawerIndicatorEnabled) {
            if (enable) {
                setActionBarUpIndicator(this.mSlider, this.mDrawerLayout.isDrawerOpen((int) android.support.v4.view.GravityCompat.START) ? this.mCloseDrawerContentDescRes : this.mOpenDrawerContentDescRes);
            } else {
                setActionBarUpIndicator(this.mHomeAsUpIndicator, 0);
            }
            this.mDrawerIndicatorEnabled = enable;
        }
    }

    public boolean isDrawerIndicatorEnabled() {
        return this.mDrawerIndicatorEnabled;
    }

    public void onConfigurationChanged(android.content.res.Configuration newConfig) {
        if (!this.mHasCustomUpIndicator) {
            this.mHomeAsUpIndicator = getThemeUpIndicator();
        }
        this.mDrawerImage = android.support.v4.content.ContextCompat.getDrawable(this.mActivity, this.mDrawerImageResource);
        syncState();
    }

    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        if (item == null || item.getItemId() != ID_HOME || !this.mDrawerIndicatorEnabled) {
            return false;
        }
        if (this.mDrawerLayout.isDrawerVisible((int) android.support.v4.view.GravityCompat.START)) {
            this.mDrawerLayout.closeDrawer((int) android.support.v4.view.GravityCompat.START);
        } else {
            this.mDrawerLayout.openDrawer((int) android.support.v4.view.GravityCompat.START);
        }
        return true;
    }

    public void onDrawerSlide(android.view.View drawerView, float slideOffset) {
        float glyphOffset;
        float glyphOffset2 = this.mSlider.getPosition();
        if (slideOffset > 0.5f) {
            glyphOffset = java.lang.Math.max(glyphOffset2, java.lang.Math.max(0.0f, slideOffset - 0.5f) * 2.0f);
        } else {
            glyphOffset = java.lang.Math.min(glyphOffset2, slideOffset * 2.0f);
        }
        this.mSlider.setPosition(glyphOffset);
    }

    public void onDrawerOpened(android.view.View drawerView) {
        this.mSlider.setPosition(1.0f);
        if (this.mDrawerIndicatorEnabled) {
            setActionBarDescription(this.mCloseDrawerContentDescRes);
        }
    }

    public void onDrawerClosed(android.view.View drawerView) {
        this.mSlider.setPosition(0.0f);
        if (this.mDrawerIndicatorEnabled) {
            setActionBarDescription(this.mOpenDrawerContentDescRes);
        }
    }

    public void onDrawerStateChanged(int newState) {
    }

    /* access modifiers changed from: 0000 */
    public android.graphics.drawable.Drawable getThemeUpIndicator() {
        if (this.mActivityImpl != null) {
            return this.mActivityImpl.getThemeUpIndicator();
        }
        return IMPL.getThemeUpIndicator(this.mActivity);
    }

    /* access modifiers changed from: 0000 */
    public void setActionBarUpIndicator(android.graphics.drawable.Drawable upDrawable, int contentDescRes) {
        if (this.mActivityImpl != null) {
            this.mActivityImpl.setActionBarUpIndicator(upDrawable, contentDescRes);
        } else {
            this.mSetIndicatorInfo = IMPL.setActionBarUpIndicator(this.mSetIndicatorInfo, this.mActivity, upDrawable, contentDescRes);
        }
    }

    /* access modifiers changed from: 0000 */
    public void setActionBarDescription(int contentDescRes) {
        if (this.mActivityImpl != null) {
            this.mActivityImpl.setActionBarDescription(contentDescRes);
        } else {
            this.mSetIndicatorInfo = IMPL.setActionBarDescription(this.mSetIndicatorInfo, this.mActivity, contentDescRes);
        }
    }
}
