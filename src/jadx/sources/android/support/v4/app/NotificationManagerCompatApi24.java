package android.support.v4.app;

@android.annotation.TargetApi(24)
@android.support.annotation.RequiresApi(24)
class NotificationManagerCompatApi24 {
    NotificationManagerCompatApi24() {
    }

    public static boolean areNotificationsEnabled(android.app.NotificationManager notificationManager) {
        return notificationManager.areNotificationsEnabled();
    }

    public static int getImportance(android.app.NotificationManager notificationManager) {
        return notificationManager.getImportance();
    }
}
