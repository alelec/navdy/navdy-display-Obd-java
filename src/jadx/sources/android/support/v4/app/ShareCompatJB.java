package android.support.v4.app;

@android.annotation.TargetApi(16)
@android.support.annotation.RequiresApi(16)
class ShareCompatJB {
    ShareCompatJB() {
    }

    public static java.lang.String escapeHtml(java.lang.CharSequence html) {
        return android.text.Html.escapeHtml(html);
    }
}
