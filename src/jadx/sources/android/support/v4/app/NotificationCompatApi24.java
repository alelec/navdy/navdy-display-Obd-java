package android.support.v4.app;

@android.annotation.TargetApi(24)
@android.support.annotation.RequiresApi(24)
class NotificationCompatApi24 {
    public static final java.lang.String CATEGORY_ALARM = "alarm";
    public static final java.lang.String CATEGORY_CALL = "call";
    public static final java.lang.String CATEGORY_EMAIL = "email";
    public static final java.lang.String CATEGORY_ERROR = "err";
    public static final java.lang.String CATEGORY_EVENT = "event";
    public static final java.lang.String CATEGORY_MESSAGE = "msg";
    public static final java.lang.String CATEGORY_PROGRESS = "progress";
    public static final java.lang.String CATEGORY_PROMO = "promo";
    public static final java.lang.String CATEGORY_RECOMMENDATION = "recommendation";
    public static final java.lang.String CATEGORY_SERVICE = "service";
    public static final java.lang.String CATEGORY_SOCIAL = "social";
    public static final java.lang.String CATEGORY_STATUS = "status";
    public static final java.lang.String CATEGORY_SYSTEM = "sys";
    public static final java.lang.String CATEGORY_TRANSPORT = "transport";

    public static class Builder implements android.support.v4.app.NotificationBuilderWithBuilderAccessor, android.support.v4.app.NotificationBuilderWithActions {
        private android.app.Notification.Builder b;

        public Builder(android.content.Context context, android.app.Notification n, java.lang.CharSequence contentTitle, java.lang.CharSequence contentText, java.lang.CharSequence contentInfo, android.widget.RemoteViews tickerView, int number, android.app.PendingIntent contentIntent, android.app.PendingIntent fullScreenIntent, android.graphics.Bitmap largeIcon, int progressMax, int progress, boolean progressIndeterminate, boolean showWhen, boolean useChronometer, int priority, java.lang.CharSequence subText, boolean localOnly, java.lang.String category, java.util.ArrayList<java.lang.String> people, android.os.Bundle extras, int color, int visibility, android.app.Notification publicVersion, java.lang.String groupKey, boolean groupSummary, java.lang.String sortKey, java.lang.CharSequence[] remoteInputHistory, android.widget.RemoteViews contentView, android.widget.RemoteViews bigContentView, android.widget.RemoteViews headsUpContentView) {
            this.b = new android.app.Notification.Builder(context).setWhen(n.when).setShowWhen(showWhen).setSmallIcon(n.icon, n.iconLevel).setContent(n.contentView).setTicker(n.tickerText, tickerView).setSound(n.sound, n.audioStreamType).setVibrate(n.vibrate).setLights(n.ledARGB, n.ledOnMS, n.ledOffMS).setOngoing((n.flags & 2) != 0).setOnlyAlertOnce((n.flags & 8) != 0).setAutoCancel((n.flags & 16) != 0).setDefaults(n.defaults).setContentTitle(contentTitle).setContentText(contentText).setSubText(subText).setContentInfo(contentInfo).setContentIntent(contentIntent).setDeleteIntent(n.deleteIntent).setFullScreenIntent(fullScreenIntent, (n.flags & 128) != 0).setLargeIcon(largeIcon).setNumber(number).setUsesChronometer(useChronometer).setPriority(priority).setProgress(progressMax, progress, progressIndeterminate).setLocalOnly(localOnly).setExtras(extras).setGroup(groupKey).setGroupSummary(groupSummary).setSortKey(sortKey).setCategory(category).setColor(color).setVisibility(visibility).setPublicVersion(publicVersion).setRemoteInputHistory(remoteInputHistory);
            if (contentView != null) {
                this.b.setCustomContentView(contentView);
            }
            if (bigContentView != null) {
                this.b.setCustomBigContentView(bigContentView);
            }
            if (headsUpContentView != null) {
                this.b.setCustomHeadsUpContentView(headsUpContentView);
            }
            java.util.Iterator it = people.iterator();
            while (it.hasNext()) {
                this.b.addPerson((java.lang.String) it.next());
            }
        }

        public void addAction(android.support.v4.app.NotificationCompatBase.Action action) {
            android.os.Bundle actionExtras;
            android.app.Notification.Action.Builder actionBuilder = new android.app.Notification.Action.Builder(action.getIcon(), action.getTitle(), action.getActionIntent());
            if (action.getRemoteInputs() != null) {
                for (android.app.RemoteInput remoteInput : android.support.v4.app.RemoteInputCompatApi20.fromCompat(action.getRemoteInputs())) {
                    actionBuilder.addRemoteInput(remoteInput);
                }
            }
            if (action.getExtras() != null) {
                actionExtras = new android.os.Bundle(action.getExtras());
            } else {
                actionExtras = new android.os.Bundle();
            }
            actionExtras.putBoolean("android.support.allowGeneratedReplies", action.getAllowGeneratedReplies());
            actionBuilder.addExtras(actionExtras);
            actionBuilder.setAllowGeneratedReplies(action.getAllowGeneratedReplies());
            this.b.addAction(actionBuilder.build());
        }

        public android.app.Notification.Builder getBuilder() {
            return this.b;
        }

        public android.app.Notification build() {
            return this.b.build();
        }
    }

    NotificationCompatApi24() {
    }

    public static void addMessagingStyle(android.support.v4.app.NotificationBuilderWithBuilderAccessor b, java.lang.CharSequence userDisplayName, java.lang.CharSequence conversationTitle, java.util.List<java.lang.CharSequence> texts, java.util.List<java.lang.Long> timestamps, java.util.List<java.lang.CharSequence> senders, java.util.List<java.lang.String> dataMimeTypes, java.util.List<android.net.Uri> dataUris) {
        android.app.Notification.MessagingStyle style = new android.app.Notification.MessagingStyle(userDisplayName).setConversationTitle(conversationTitle);
        for (int i = 0; i < texts.size(); i++) {
            android.app.Notification.MessagingStyle.Message message = new android.app.Notification.MessagingStyle.Message((java.lang.CharSequence) texts.get(i), ((java.lang.Long) timestamps.get(i)).longValue(), (java.lang.CharSequence) senders.get(i));
            if (dataMimeTypes.get(i) != null) {
                message.setData((java.lang.String) dataMimeTypes.get(i), (android.net.Uri) dataUris.get(i));
            }
            style.addMessage(message);
        }
        style.setBuilder(b.getBuilder());
    }
}
