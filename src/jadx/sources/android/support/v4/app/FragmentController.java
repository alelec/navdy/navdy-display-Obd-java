package android.support.v4.app;

public class FragmentController {
    private final android.support.v4.app.FragmentHostCallback<?> mHost;

    public static final android.support.v4.app.FragmentController createController(android.support.v4.app.FragmentHostCallback<?> callbacks) {
        return new android.support.v4.app.FragmentController(callbacks);
    }

    private FragmentController(android.support.v4.app.FragmentHostCallback<?> callbacks) {
        this.mHost = callbacks;
    }

    public android.support.v4.app.FragmentManager getSupportFragmentManager() {
        return this.mHost.getFragmentManagerImpl();
    }

    public android.support.v4.app.LoaderManager getSupportLoaderManager() {
        return this.mHost.getLoaderManagerImpl();
    }

    @android.support.annotation.Nullable
    public android.support.v4.app.Fragment findFragmentByWho(java.lang.String who) {
        return this.mHost.mFragmentManager.findFragmentByWho(who);
    }

    public int getActiveFragmentsCount() {
        java.util.List<android.support.v4.app.Fragment> actives = this.mHost.mFragmentManager.mActive;
        if (actives == null) {
            return 0;
        }
        return actives.size();
    }

    public java.util.List<android.support.v4.app.Fragment> getActiveFragments(java.util.List<android.support.v4.app.Fragment> actives) {
        if (this.mHost.mFragmentManager.mActive == null) {
            return null;
        }
        if (actives == null) {
            actives = new java.util.ArrayList<>(getActiveFragmentsCount());
        }
        actives.addAll(this.mHost.mFragmentManager.mActive);
        return actives;
    }

    public void attachHost(android.support.v4.app.Fragment parent) {
        this.mHost.mFragmentManager.attachController(this.mHost, this.mHost, parent);
    }

    public android.view.View onCreateView(android.view.View parent, java.lang.String name, android.content.Context context, android.util.AttributeSet attrs) {
        return this.mHost.mFragmentManager.onCreateView(parent, name, context, attrs);
    }

    public void noteStateNotSaved() {
        this.mHost.mFragmentManager.noteStateNotSaved();
    }

    public android.os.Parcelable saveAllState() {
        return this.mHost.mFragmentManager.saveAllState();
    }

    @java.lang.Deprecated
    public void restoreAllState(android.os.Parcelable state, java.util.List<android.support.v4.app.Fragment> nonConfigList) {
        this.mHost.mFragmentManager.restoreAllState(state, new android.support.v4.app.FragmentManagerNonConfig(nonConfigList, null));
    }

    public void restoreAllState(android.os.Parcelable state, android.support.v4.app.FragmentManagerNonConfig nonConfig) {
        this.mHost.mFragmentManager.restoreAllState(state, nonConfig);
    }

    @java.lang.Deprecated
    public java.util.List<android.support.v4.app.Fragment> retainNonConfig() {
        android.support.v4.app.FragmentManagerNonConfig nonconf = this.mHost.mFragmentManager.retainNonConfig();
        if (nonconf != null) {
            return nonconf.getFragments();
        }
        return null;
    }

    public android.support.v4.app.FragmentManagerNonConfig retainNestedNonConfig() {
        return this.mHost.mFragmentManager.retainNonConfig();
    }

    public void dispatchCreate() {
        this.mHost.mFragmentManager.dispatchCreate();
    }

    public void dispatchActivityCreated() {
        this.mHost.mFragmentManager.dispatchActivityCreated();
    }

    public void dispatchStart() {
        this.mHost.mFragmentManager.dispatchStart();
    }

    public void dispatchResume() {
        this.mHost.mFragmentManager.dispatchResume();
    }

    public void dispatchPause() {
        this.mHost.mFragmentManager.dispatchPause();
    }

    public void dispatchStop() {
        this.mHost.mFragmentManager.dispatchStop();
    }

    public void dispatchReallyStop() {
        this.mHost.mFragmentManager.dispatchReallyStop();
    }

    public void dispatchDestroyView() {
        this.mHost.mFragmentManager.dispatchDestroyView();
    }

    public void dispatchDestroy() {
        this.mHost.mFragmentManager.dispatchDestroy();
    }

    public void dispatchMultiWindowModeChanged(boolean isInMultiWindowMode) {
        this.mHost.mFragmentManager.dispatchMultiWindowModeChanged(isInMultiWindowMode);
    }

    public void dispatchPictureInPictureModeChanged(boolean isInPictureInPictureMode) {
        this.mHost.mFragmentManager.dispatchPictureInPictureModeChanged(isInPictureInPictureMode);
    }

    public void dispatchConfigurationChanged(android.content.res.Configuration newConfig) {
        this.mHost.mFragmentManager.dispatchConfigurationChanged(newConfig);
    }

    public void dispatchLowMemory() {
        this.mHost.mFragmentManager.dispatchLowMemory();
    }

    public boolean dispatchCreateOptionsMenu(android.view.Menu menu, android.view.MenuInflater inflater) {
        return this.mHost.mFragmentManager.dispatchCreateOptionsMenu(menu, inflater);
    }

    public boolean dispatchPrepareOptionsMenu(android.view.Menu menu) {
        return this.mHost.mFragmentManager.dispatchPrepareOptionsMenu(menu);
    }

    public boolean dispatchOptionsItemSelected(android.view.MenuItem item) {
        return this.mHost.mFragmentManager.dispatchOptionsItemSelected(item);
    }

    public boolean dispatchContextItemSelected(android.view.MenuItem item) {
        return this.mHost.mFragmentManager.dispatchContextItemSelected(item);
    }

    public void dispatchOptionsMenuClosed(android.view.Menu menu) {
        this.mHost.mFragmentManager.dispatchOptionsMenuClosed(menu);
    }

    public boolean execPendingActions() {
        return this.mHost.mFragmentManager.execPendingActions();
    }

    public void doLoaderStart() {
        this.mHost.doLoaderStart();
    }

    public void doLoaderStop(boolean retain) {
        this.mHost.doLoaderStop(retain);
    }

    public void doLoaderRetain() {
        this.mHost.doLoaderRetain();
    }

    public void doLoaderDestroy() {
        this.mHost.doLoaderDestroy();
    }

    public void reportLoaderStart() {
        this.mHost.reportLoaderStart();
    }

    public android.support.v4.util.SimpleArrayMap<java.lang.String, android.support.v4.app.LoaderManager> retainLoaderNonConfig() {
        return this.mHost.retainLoaderNonConfig();
    }

    public void restoreLoaderNonConfig(android.support.v4.util.SimpleArrayMap<java.lang.String, android.support.v4.app.LoaderManager> loaderManagers) {
        this.mHost.restoreLoaderNonConfig(loaderManagers);
    }

    public void dumpLoaders(java.lang.String prefix, java.io.FileDescriptor fd, java.io.PrintWriter writer, java.lang.String[] args) {
        this.mHost.dumpLoaders(prefix, fd, writer, args);
    }
}
