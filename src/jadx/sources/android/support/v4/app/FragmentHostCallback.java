package android.support.v4.app;

public abstract class FragmentHostCallback<E> extends android.support.v4.app.FragmentContainer {
    private final android.app.Activity mActivity;
    private android.support.v4.util.SimpleArrayMap<java.lang.String, android.support.v4.app.LoaderManager> mAllLoaderManagers;
    private boolean mCheckedForLoaderManager;
    final android.content.Context mContext;
    final android.support.v4.app.FragmentManagerImpl mFragmentManager;
    private final android.os.Handler mHandler;
    private android.support.v4.app.LoaderManagerImpl mLoaderManager;
    private boolean mLoadersStarted;
    private boolean mRetainLoaders;
    final int mWindowAnimations;

    @android.support.annotation.Nullable
    public abstract E onGetHost();

    public FragmentHostCallback(android.content.Context context, android.os.Handler handler, int windowAnimations) {
        this(context instanceof android.app.Activity ? (android.app.Activity) context : null, context, handler, windowAnimations);
    }

    FragmentHostCallback(android.support.v4.app.FragmentActivity activity) {
        this(activity, activity, activity.mHandler, 0);
    }

    FragmentHostCallback(android.app.Activity activity, android.content.Context context, android.os.Handler handler, int windowAnimations) {
        this.mFragmentManager = new android.support.v4.app.FragmentManagerImpl();
        this.mActivity = activity;
        this.mContext = context;
        this.mHandler = handler;
        this.mWindowAnimations = windowAnimations;
    }

    public void onDump(java.lang.String prefix, java.io.FileDescriptor fd, java.io.PrintWriter writer, java.lang.String[] args) {
    }

    public boolean onShouldSaveFragmentState(android.support.v4.app.Fragment fragment) {
        return true;
    }

    public android.view.LayoutInflater onGetLayoutInflater() {
        return (android.view.LayoutInflater) this.mContext.getSystemService("layout_inflater");
    }

    public void onSupportInvalidateOptionsMenu() {
    }

    public void onStartActivityFromFragment(android.support.v4.app.Fragment fragment, android.content.Intent intent, int requestCode) {
        onStartActivityFromFragment(fragment, intent, requestCode, null);
    }

    public void onStartActivityFromFragment(android.support.v4.app.Fragment fragment, android.content.Intent intent, int requestCode, @android.support.annotation.Nullable android.os.Bundle options) {
        if (requestCode != -1) {
            throw new java.lang.IllegalStateException("Starting activity with a requestCode requires a FragmentActivity host");
        }
        this.mContext.startActivity(intent);
    }

    public void onStartIntentSenderFromFragment(android.support.v4.app.Fragment fragment, android.content.IntentSender intent, int requestCode, @android.support.annotation.Nullable android.content.Intent fillInIntent, int flagsMask, int flagsValues, int extraFlags, android.os.Bundle options) throws android.content.IntentSender.SendIntentException {
        if (requestCode != -1) {
            throw new java.lang.IllegalStateException("Starting intent sender with a requestCode requires a FragmentActivity host");
        }
        android.support.v4.app.ActivityCompat.startIntentSenderForResult(this.mActivity, intent, requestCode, fillInIntent, flagsMask, flagsValues, extraFlags, options);
    }

    public void onRequestPermissionsFromFragment(@android.support.annotation.NonNull android.support.v4.app.Fragment fragment, @android.support.annotation.NonNull java.lang.String[] permissions, int requestCode) {
    }

    public boolean onShouldShowRequestPermissionRationale(@android.support.annotation.NonNull java.lang.String permission) {
        return false;
    }

    public boolean onHasWindowAnimations() {
        return true;
    }

    public int onGetWindowAnimations() {
        return this.mWindowAnimations;
    }

    @android.support.annotation.Nullable
    public android.view.View onFindViewById(int id) {
        return null;
    }

    public boolean onHasView() {
        return true;
    }

    /* access modifiers changed from: 0000 */
    public android.app.Activity getActivity() {
        return this.mActivity;
    }

    /* access modifiers changed from: 0000 */
    public android.content.Context getContext() {
        return this.mContext;
    }

    /* access modifiers changed from: 0000 */
    public android.os.Handler getHandler() {
        return this.mHandler;
    }

    /* access modifiers changed from: 0000 */
    public android.support.v4.app.FragmentManagerImpl getFragmentManagerImpl() {
        return this.mFragmentManager;
    }

    /* access modifiers changed from: 0000 */
    public android.support.v4.app.LoaderManagerImpl getLoaderManagerImpl() {
        if (this.mLoaderManager != null) {
            return this.mLoaderManager;
        }
        this.mCheckedForLoaderManager = true;
        this.mLoaderManager = getLoaderManager("(root)", this.mLoadersStarted, true);
        return this.mLoaderManager;
    }

    /* access modifiers changed from: 0000 */
    public void inactivateFragment(java.lang.String who) {
        if (this.mAllLoaderManagers != null) {
            android.support.v4.app.LoaderManagerImpl lm = (android.support.v4.app.LoaderManagerImpl) this.mAllLoaderManagers.get(who);
            if (lm != null && !lm.mRetaining) {
                lm.doDestroy();
                this.mAllLoaderManagers.remove(who);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void onAttachFragment(android.support.v4.app.Fragment fragment) {
    }

    /* access modifiers changed from: 0000 */
    public boolean getRetainLoaders() {
        return this.mRetainLoaders;
    }

    /* access modifiers changed from: 0000 */
    public void doLoaderStart() {
        if (!this.mLoadersStarted) {
            this.mLoadersStarted = true;
            if (this.mLoaderManager != null) {
                this.mLoaderManager.doStart();
            } else if (!this.mCheckedForLoaderManager) {
                this.mLoaderManager = getLoaderManager("(root)", this.mLoadersStarted, false);
                if (this.mLoaderManager != null && !this.mLoaderManager.mStarted) {
                    this.mLoaderManager.doStart();
                }
            }
            this.mCheckedForLoaderManager = true;
        }
    }

    /* access modifiers changed from: 0000 */
    public void doLoaderStop(boolean retain) {
        this.mRetainLoaders = retain;
        if (this.mLoaderManager != null && this.mLoadersStarted) {
            this.mLoadersStarted = false;
            if (retain) {
                this.mLoaderManager.doRetain();
            } else {
                this.mLoaderManager.doStop();
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void doLoaderRetain() {
        if (this.mLoaderManager != null) {
            this.mLoaderManager.doRetain();
        }
    }

    /* access modifiers changed from: 0000 */
    public void doLoaderDestroy() {
        if (this.mLoaderManager != null) {
            this.mLoaderManager.doDestroy();
        }
    }

    /* access modifiers changed from: 0000 */
    public void reportLoaderStart() {
        if (this.mAllLoaderManagers != null) {
            int N = this.mAllLoaderManagers.size();
            android.support.v4.app.LoaderManagerImpl[] loaders = new android.support.v4.app.LoaderManagerImpl[N];
            for (int i = N - 1; i >= 0; i--) {
                loaders[i] = (android.support.v4.app.LoaderManagerImpl) this.mAllLoaderManagers.valueAt(i);
            }
            for (int i2 = 0; i2 < N; i2++) {
                android.support.v4.app.LoaderManagerImpl lm = loaders[i2];
                lm.finishRetain();
                lm.doReportStart();
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public android.support.v4.app.LoaderManagerImpl getLoaderManager(java.lang.String who, boolean started, boolean create) {
        if (this.mAllLoaderManagers == null) {
            this.mAllLoaderManagers = new android.support.v4.util.SimpleArrayMap<>();
        }
        android.support.v4.app.LoaderManagerImpl lm = (android.support.v4.app.LoaderManagerImpl) this.mAllLoaderManagers.get(who);
        if (lm == null && create) {
            android.support.v4.app.LoaderManagerImpl lm2 = new android.support.v4.app.LoaderManagerImpl(who, this, started);
            this.mAllLoaderManagers.put(who, lm2);
            return lm2;
        } else if (!started || lm == null || lm.mStarted) {
            return lm;
        } else {
            lm.doStart();
            return lm;
        }
    }

    /* access modifiers changed from: 0000 */
    public android.support.v4.util.SimpleArrayMap<java.lang.String, android.support.v4.app.LoaderManager> retainLoaderNonConfig() {
        boolean retainLoaders = false;
        if (this.mAllLoaderManagers != null) {
            int N = this.mAllLoaderManagers.size();
            android.support.v4.app.LoaderManagerImpl[] loaders = new android.support.v4.app.LoaderManagerImpl[N];
            for (int i = N - 1; i >= 0; i--) {
                loaders[i] = (android.support.v4.app.LoaderManagerImpl) this.mAllLoaderManagers.valueAt(i);
            }
            boolean doRetainLoaders = getRetainLoaders();
            for (int i2 = 0; i2 < N; i2++) {
                android.support.v4.app.LoaderManagerImpl lm = loaders[i2];
                if (!lm.mRetaining && doRetainLoaders) {
                    if (!lm.mStarted) {
                        lm.doStart();
                    }
                    lm.doRetain();
                }
                if (lm.mRetaining) {
                    retainLoaders = true;
                } else {
                    lm.doDestroy();
                    this.mAllLoaderManagers.remove(lm.mWho);
                }
            }
        }
        if (retainLoaders) {
            return this.mAllLoaderManagers;
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    public void restoreLoaderNonConfig(android.support.v4.util.SimpleArrayMap<java.lang.String, android.support.v4.app.LoaderManager> loaderManagers) {
        if (loaderManagers != null) {
            int numLoaderManagers = loaderManagers.size();
            for (int i = 0; i < numLoaderManagers; i++) {
                ((android.support.v4.app.LoaderManagerImpl) loaderManagers.valueAt(i)).updateHostController(this);
            }
        }
        this.mAllLoaderManagers = loaderManagers;
    }

    /* access modifiers changed from: 0000 */
    public void dumpLoaders(java.lang.String prefix, java.io.FileDescriptor fd, java.io.PrintWriter writer, java.lang.String[] args) {
        writer.print(prefix);
        writer.print("mLoadersStarted=");
        writer.println(this.mLoadersStarted);
        if (this.mLoaderManager != null) {
            writer.print(prefix);
            writer.print("Loader Manager ");
            writer.print(java.lang.Integer.toHexString(java.lang.System.identityHashCode(this.mLoaderManager)));
            writer.println(":");
            this.mLoaderManager.dump(prefix + "  ", fd, writer, args);
        }
    }
}
