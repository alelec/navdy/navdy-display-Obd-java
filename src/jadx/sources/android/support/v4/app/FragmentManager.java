package android.support.v4.app;

public abstract class FragmentManager {
    public static final int POP_BACK_STACK_INCLUSIVE = 1;

    public interface BackStackEntry {
        java.lang.CharSequence getBreadCrumbShortTitle();

        @android.support.annotation.StringRes
        int getBreadCrumbShortTitleRes();

        java.lang.CharSequence getBreadCrumbTitle();

        @android.support.annotation.StringRes
        int getBreadCrumbTitleRes();

        int getId();

        java.lang.String getName();
    }

    public static abstract class FragmentLifecycleCallbacks {
        public void onFragmentPreAttached(android.support.v4.app.FragmentManager fm, android.support.v4.app.Fragment f, android.content.Context context) {
        }

        public void onFragmentAttached(android.support.v4.app.FragmentManager fm, android.support.v4.app.Fragment f, android.content.Context context) {
        }

        public void onFragmentCreated(android.support.v4.app.FragmentManager fm, android.support.v4.app.Fragment f, android.os.Bundle savedInstanceState) {
        }

        public void onFragmentActivityCreated(android.support.v4.app.FragmentManager fm, android.support.v4.app.Fragment f, android.os.Bundle savedInstanceState) {
        }

        public void onFragmentViewCreated(android.support.v4.app.FragmentManager fm, android.support.v4.app.Fragment f, android.view.View v, android.os.Bundle savedInstanceState) {
        }

        public void onFragmentStarted(android.support.v4.app.FragmentManager fm, android.support.v4.app.Fragment f) {
        }

        public void onFragmentResumed(android.support.v4.app.FragmentManager fm, android.support.v4.app.Fragment f) {
        }

        public void onFragmentPaused(android.support.v4.app.FragmentManager fm, android.support.v4.app.Fragment f) {
        }

        public void onFragmentStopped(android.support.v4.app.FragmentManager fm, android.support.v4.app.Fragment f) {
        }

        public void onFragmentSaveInstanceState(android.support.v4.app.FragmentManager fm, android.support.v4.app.Fragment f, android.os.Bundle outState) {
        }

        public void onFragmentViewDestroyed(android.support.v4.app.FragmentManager fm, android.support.v4.app.Fragment f) {
        }

        public void onFragmentDestroyed(android.support.v4.app.FragmentManager fm, android.support.v4.app.Fragment f) {
        }

        public void onFragmentDetached(android.support.v4.app.FragmentManager fm, android.support.v4.app.Fragment f) {
        }
    }

    public interface OnBackStackChangedListener {
        void onBackStackChanged();
    }

    public abstract void addOnBackStackChangedListener(android.support.v4.app.FragmentManager.OnBackStackChangedListener onBackStackChangedListener);

    public abstract android.support.v4.app.FragmentTransaction beginTransaction();

    public abstract void dump(java.lang.String str, java.io.FileDescriptor fileDescriptor, java.io.PrintWriter printWriter, java.lang.String[] strArr);

    public abstract boolean executePendingTransactions();

    public abstract android.support.v4.app.Fragment findFragmentById(@android.support.annotation.IdRes int i);

    public abstract android.support.v4.app.Fragment findFragmentByTag(java.lang.String str);

    public abstract android.support.v4.app.FragmentManager.BackStackEntry getBackStackEntryAt(int i);

    public abstract int getBackStackEntryCount();

    public abstract android.support.v4.app.Fragment getFragment(android.os.Bundle bundle, java.lang.String str);

    @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
    public abstract java.util.List<android.support.v4.app.Fragment> getFragments();

    public abstract boolean isDestroyed();

    public abstract void popBackStack();

    public abstract void popBackStack(int i, int i2);

    public abstract void popBackStack(java.lang.String str, int i);

    public abstract boolean popBackStackImmediate();

    public abstract boolean popBackStackImmediate(int i, int i2);

    public abstract boolean popBackStackImmediate(java.lang.String str, int i);

    public abstract void putFragment(android.os.Bundle bundle, java.lang.String str, android.support.v4.app.Fragment fragment);

    public abstract void registerFragmentLifecycleCallbacks(android.support.v4.app.FragmentManager.FragmentLifecycleCallbacks fragmentLifecycleCallbacks, boolean z);

    public abstract void removeOnBackStackChangedListener(android.support.v4.app.FragmentManager.OnBackStackChangedListener onBackStackChangedListener);

    public abstract android.support.v4.app.Fragment.SavedState saveFragmentInstanceState(android.support.v4.app.Fragment fragment);

    public abstract void unregisterFragmentLifecycleCallbacks(android.support.v4.app.FragmentManager.FragmentLifecycleCallbacks fragmentLifecycleCallbacks);

    @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
    @java.lang.Deprecated
    public android.support.v4.app.FragmentTransaction openTransaction() {
        return beginTransaction();
    }

    public static void enableDebugLogging(boolean enabled) {
        android.support.v4.app.FragmentManagerImpl.DEBUG = enabled;
    }
}
