package android.support.v4.app;

@android.annotation.TargetApi(16)
@android.support.annotation.RequiresApi(16)
class RemoteInputCompatJellybean {
    public static final java.lang.String EXTRA_RESULTS_DATA = "android.remoteinput.resultsData";
    private static final java.lang.String KEY_ALLOW_FREE_FORM_INPUT = "allowFreeFormInput";
    private static final java.lang.String KEY_CHOICES = "choices";
    private static final java.lang.String KEY_EXTRAS = "extras";
    private static final java.lang.String KEY_LABEL = "label";
    private static final java.lang.String KEY_RESULT_KEY = "resultKey";
    public static final java.lang.String RESULTS_CLIP_LABEL = "android.remoteinput.results";

    RemoteInputCompatJellybean() {
    }

    static android.support.v4.app.RemoteInputCompatBase.RemoteInput fromBundle(android.os.Bundle data, android.support.v4.app.RemoteInputCompatBase.RemoteInput.Factory factory) {
        return factory.build(data.getString(KEY_RESULT_KEY), data.getCharSequence(KEY_LABEL), data.getCharSequenceArray(KEY_CHOICES), data.getBoolean(KEY_ALLOW_FREE_FORM_INPUT), data.getBundle(KEY_EXTRAS));
    }

    static android.os.Bundle toBundle(android.support.v4.app.RemoteInputCompatBase.RemoteInput remoteInput) {
        android.os.Bundle data = new android.os.Bundle();
        data.putString(KEY_RESULT_KEY, remoteInput.getResultKey());
        data.putCharSequence(KEY_LABEL, remoteInput.getLabel());
        data.putCharSequenceArray(KEY_CHOICES, remoteInput.getChoices());
        data.putBoolean(KEY_ALLOW_FREE_FORM_INPUT, remoteInput.getAllowFreeFormInput());
        data.putBundle(KEY_EXTRAS, remoteInput.getExtras());
        return data;
    }

    static android.support.v4.app.RemoteInputCompatBase.RemoteInput[] fromBundleArray(android.os.Bundle[] bundles, android.support.v4.app.RemoteInputCompatBase.RemoteInput.Factory factory) {
        if (bundles == null) {
            return null;
        }
        android.support.v4.app.RemoteInputCompatBase.RemoteInput[] remoteInputs = factory.newArray(bundles.length);
        for (int i = 0; i < bundles.length; i++) {
            remoteInputs[i] = fromBundle(bundles[i], factory);
        }
        return remoteInputs;
    }

    static android.os.Bundle[] toBundleArray(android.support.v4.app.RemoteInputCompatBase.RemoteInput[] remoteInputs) {
        if (remoteInputs == null) {
            return null;
        }
        android.os.Bundle[] bundles = new android.os.Bundle[remoteInputs.length];
        for (int i = 0; i < remoteInputs.length; i++) {
            bundles[i] = toBundle(remoteInputs[i]);
        }
        return bundles;
    }

    static android.os.Bundle getResultsFromIntent(android.content.Intent intent) {
        android.content.ClipData clipData = intent.getClipData();
        if (clipData == null) {
            return null;
        }
        android.content.ClipDescription clipDescription = clipData.getDescription();
        if (!clipDescription.hasMimeType("text/vnd.android.intent") || !clipDescription.getLabel().equals("android.remoteinput.results")) {
            return null;
        }
        return (android.os.Bundle) clipData.getItemAt(0).getIntent().getExtras().getParcelable("android.remoteinput.resultsData");
    }

    static void addResultsToIntent(android.support.v4.app.RemoteInputCompatBase.RemoteInput[] remoteInputs, android.content.Intent intent, android.os.Bundle results) {
        android.os.Bundle resultsBundle = new android.os.Bundle();
        for (android.support.v4.app.RemoteInputCompatBase.RemoteInput remoteInput : remoteInputs) {
            java.lang.Object result = results.get(remoteInput.getResultKey());
            if (result instanceof java.lang.CharSequence) {
                resultsBundle.putCharSequence(remoteInput.getResultKey(), (java.lang.CharSequence) result);
            }
        }
        android.content.Intent clipIntent = new android.content.Intent();
        clipIntent.putExtra("android.remoteinput.resultsData", resultsBundle);
        intent.setClipData(android.content.ClipData.newIntent("android.remoteinput.results", clipIntent));
    }
}
