package android.support.v4.app;

public abstract class FragmentContainer {
    @android.support.annotation.Nullable
    public abstract android.view.View onFindViewById(@android.support.annotation.IdRes int i);

    public abstract boolean onHasView();
}
