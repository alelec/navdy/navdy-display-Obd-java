package android.support.v4.app;

public class ActivityCompat extends android.support.v4.content.ContextCompat {

    public interface OnRequestPermissionsResultCallback {
        void onRequestPermissionsResult(int i, @android.support.annotation.NonNull java.lang.String[] strArr, @android.support.annotation.NonNull int[] iArr);
    }

    private static class SharedElementCallback21Impl extends android.support.v4.app.ActivityCompatApi21.SharedElementCallback21 {
        private android.support.v4.app.SharedElementCallback mCallback;

        public SharedElementCallback21Impl(android.support.v4.app.SharedElementCallback callback) {
            this.mCallback = callback;
        }

        public void onSharedElementStart(java.util.List<java.lang.String> sharedElementNames, java.util.List<android.view.View> sharedElements, java.util.List<android.view.View> sharedElementSnapshots) {
            this.mCallback.onSharedElementStart(sharedElementNames, sharedElements, sharedElementSnapshots);
        }

        public void onSharedElementEnd(java.util.List<java.lang.String> sharedElementNames, java.util.List<android.view.View> sharedElements, java.util.List<android.view.View> sharedElementSnapshots) {
            this.mCallback.onSharedElementEnd(sharedElementNames, sharedElements, sharedElementSnapshots);
        }

        public void onRejectSharedElements(java.util.List<android.view.View> rejectedSharedElements) {
            this.mCallback.onRejectSharedElements(rejectedSharedElements);
        }

        public void onMapSharedElements(java.util.List<java.lang.String> names, java.util.Map<java.lang.String, android.view.View> sharedElements) {
            this.mCallback.onMapSharedElements(names, sharedElements);
        }

        public android.os.Parcelable onCaptureSharedElementSnapshot(android.view.View sharedElement, android.graphics.Matrix viewToGlobalMatrix, android.graphics.RectF screenBounds) {
            return this.mCallback.onCaptureSharedElementSnapshot(sharedElement, viewToGlobalMatrix, screenBounds);
        }

        public android.view.View onCreateSnapshotView(android.content.Context context, android.os.Parcelable snapshot) {
            return this.mCallback.onCreateSnapshotView(context, snapshot);
        }
    }

    private static class SharedElementCallback23Impl extends android.support.v4.app.ActivityCompatApi23.SharedElementCallback23 {
        private android.support.v4.app.SharedElementCallback mCallback;

        public SharedElementCallback23Impl(android.support.v4.app.SharedElementCallback callback) {
            this.mCallback = callback;
        }

        public void onSharedElementStart(java.util.List<java.lang.String> sharedElementNames, java.util.List<android.view.View> sharedElements, java.util.List<android.view.View> sharedElementSnapshots) {
            this.mCallback.onSharedElementStart(sharedElementNames, sharedElements, sharedElementSnapshots);
        }

        public void onSharedElementEnd(java.util.List<java.lang.String> sharedElementNames, java.util.List<android.view.View> sharedElements, java.util.List<android.view.View> sharedElementSnapshots) {
            this.mCallback.onSharedElementEnd(sharedElementNames, sharedElements, sharedElementSnapshots);
        }

        public void onRejectSharedElements(java.util.List<android.view.View> rejectedSharedElements) {
            this.mCallback.onRejectSharedElements(rejectedSharedElements);
        }

        public void onMapSharedElements(java.util.List<java.lang.String> names, java.util.Map<java.lang.String, android.view.View> sharedElements) {
            this.mCallback.onMapSharedElements(names, sharedElements);
        }

        public android.os.Parcelable onCaptureSharedElementSnapshot(android.view.View sharedElement, android.graphics.Matrix viewToGlobalMatrix, android.graphics.RectF screenBounds) {
            return this.mCallback.onCaptureSharedElementSnapshot(sharedElement, viewToGlobalMatrix, screenBounds);
        }

        public android.view.View onCreateSnapshotView(android.content.Context context, android.os.Parcelable snapshot) {
            return this.mCallback.onCreateSnapshotView(context, snapshot);
        }

        public void onSharedElementsArrived(java.util.List<java.lang.String> sharedElementNames, java.util.List<android.view.View> sharedElements, final android.support.v4.app.ActivityCompatApi23.OnSharedElementsReadyListenerBridge listener) {
            this.mCallback.onSharedElementsArrived(sharedElementNames, sharedElements, new android.support.v4.app.SharedElementCallback.OnSharedElementsReadyListener() {
                public void onSharedElementsReady() {
                    listener.onSharedElementsReady();
                }
            });
        }
    }

    protected ActivityCompat() {
    }

    public static boolean invalidateOptionsMenu(android.app.Activity activity) {
        if (android.os.Build.VERSION.SDK_INT < 11) {
            return false;
        }
        android.support.v4.app.ActivityCompatHoneycomb.invalidateOptionsMenu(activity);
        return true;
    }

    public static void startActivityForResult(android.app.Activity activity, android.content.Intent intent, int requestCode, @android.support.annotation.Nullable android.os.Bundle options) {
        if (android.os.Build.VERSION.SDK_INT >= 16) {
            android.support.v4.app.ActivityCompatJB.startActivityForResult(activity, intent, requestCode, options);
        } else {
            activity.startActivityForResult(intent, requestCode);
        }
    }

    public static void startIntentSenderForResult(android.app.Activity activity, android.content.IntentSender intent, int requestCode, android.content.Intent fillInIntent, int flagsMask, int flagsValues, int extraFlags, @android.support.annotation.Nullable android.os.Bundle options) throws android.content.IntentSender.SendIntentException {
        if (android.os.Build.VERSION.SDK_INT >= 16) {
            android.support.v4.app.ActivityCompatJB.startIntentSenderForResult(activity, intent, requestCode, fillInIntent, flagsMask, flagsValues, extraFlags, options);
        } else {
            activity.startIntentSenderForResult(intent, requestCode, fillInIntent, flagsMask, flagsValues, extraFlags);
        }
    }

    public static void finishAffinity(android.app.Activity activity) {
        if (android.os.Build.VERSION.SDK_INT >= 16) {
            android.support.v4.app.ActivityCompatJB.finishAffinity(activity);
        } else {
            activity.finish();
        }
    }

    public static void finishAfterTransition(android.app.Activity activity) {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            android.support.v4.app.ActivityCompatApi21.finishAfterTransition(activity);
        } else {
            activity.finish();
        }
    }

    @android.support.annotation.Nullable
    public static android.net.Uri getReferrer(android.app.Activity activity) {
        if (android.os.Build.VERSION.SDK_INT >= 22) {
            return android.support.v4.app.ActivityCompatApi22.getReferrer(activity);
        }
        android.content.Intent intent = activity.getIntent();
        android.net.Uri referrer = (android.net.Uri) intent.getParcelableExtra("android.intent.extra.REFERRER");
        if (referrer != null) {
            return referrer;
        }
        java.lang.String referrerName = intent.getStringExtra("android.intent.extra.REFERRER_NAME");
        if (referrerName != null) {
            return android.net.Uri.parse(referrerName);
        }
        return null;
    }

    public static void setEnterSharedElementCallback(android.app.Activity activity, android.support.v4.app.SharedElementCallback callback) {
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            android.support.v4.app.ActivityCompatApi23.setEnterSharedElementCallback(activity, createCallback23(callback));
        } else if (android.os.Build.VERSION.SDK_INT >= 21) {
            android.support.v4.app.ActivityCompatApi21.setEnterSharedElementCallback(activity, createCallback(callback));
        }
    }

    public static void setExitSharedElementCallback(android.app.Activity activity, android.support.v4.app.SharedElementCallback callback) {
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            android.support.v4.app.ActivityCompatApi23.setExitSharedElementCallback(activity, createCallback23(callback));
        } else if (android.os.Build.VERSION.SDK_INT >= 21) {
            android.support.v4.app.ActivityCompatApi21.setExitSharedElementCallback(activity, createCallback(callback));
        }
    }

    public static void postponeEnterTransition(android.app.Activity activity) {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            android.support.v4.app.ActivityCompatApi21.postponeEnterTransition(activity);
        }
    }

    public static void startPostponedEnterTransition(android.app.Activity activity) {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            android.support.v4.app.ActivityCompatApi21.startPostponedEnterTransition(activity);
        }
    }

    public static void requestPermissions(@android.support.annotation.NonNull final android.app.Activity activity, @android.support.annotation.NonNull final java.lang.String[] permissions, @android.support.annotation.IntRange(from = 0) final int requestCode) {
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            android.support.v4.app.ActivityCompatApi23.requestPermissions(activity, permissions, requestCode);
        } else if (activity instanceof android.support.v4.app.ActivityCompat.OnRequestPermissionsResultCallback) {
            new android.os.Handler(android.os.Looper.getMainLooper()).post(new java.lang.Runnable() {
                public void run() {
                    int[] grantResults = new int[permissions.length];
                    android.content.pm.PackageManager packageManager = activity.getPackageManager();
                    java.lang.String packageName = activity.getPackageName();
                    int permissionCount = permissions.length;
                    for (int i = 0; i < permissionCount; i++) {
                        grantResults[i] = packageManager.checkPermission(permissions[i], packageName);
                    }
                    ((android.support.v4.app.ActivityCompat.OnRequestPermissionsResultCallback) activity).onRequestPermissionsResult(requestCode, permissions, grantResults);
                }
            });
        }
    }

    public static boolean shouldShowRequestPermissionRationale(@android.support.annotation.NonNull android.app.Activity activity, @android.support.annotation.NonNull java.lang.String permission) {
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            return android.support.v4.app.ActivityCompatApi23.shouldShowRequestPermissionRationale(activity, permission);
        }
        return false;
    }

    private static android.support.v4.app.ActivityCompatApi21.SharedElementCallback21 createCallback(android.support.v4.app.SharedElementCallback callback) {
        if (callback != null) {
            return new android.support.v4.app.ActivityCompat.SharedElementCallback21Impl(callback);
        }
        return null;
    }

    private static android.support.v4.app.ActivityCompatApi23.SharedElementCallback23 createCallback23(android.support.v4.app.SharedElementCallback callback) {
        if (callback != null) {
            return new android.support.v4.app.ActivityCompat.SharedElementCallback23Impl(callback);
        }
        return null;
    }
}
