package android.support.v4.app;

@android.annotation.TargetApi(18)
@android.support.annotation.RequiresApi(18)
class BundleCompatJellybeanMR2 {
    BundleCompatJellybeanMR2() {
    }

    public static android.os.IBinder getBinder(android.os.Bundle bundle, java.lang.String key) {
        return bundle.getBinder(key);
    }

    public static void putBinder(android.os.Bundle bundle, java.lang.String key, android.os.IBinder binder) {
        bundle.putBinder(key, binder);
    }
}
