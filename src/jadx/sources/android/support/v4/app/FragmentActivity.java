package android.support.v4.app;

public class FragmentActivity extends android.support.v4.app.BaseFragmentActivityJB implements android.support.v4.app.ActivityCompat.OnRequestPermissionsResultCallback, android.support.v4.app.ActivityCompatApi23.RequestPermissionsRequestCodeValidator {
    static final java.lang.String ALLOCATED_REQUEST_INDICIES_TAG = "android:support:request_indicies";
    static final java.lang.String FRAGMENTS_TAG = "android:support:fragments";
    static final int MAX_NUM_PENDING_FRAGMENT_ACTIVITY_RESULTS = 65534;
    static final int MSG_REALLY_STOPPED = 1;
    static final int MSG_RESUME_PENDING = 2;
    static final java.lang.String NEXT_CANDIDATE_REQUEST_INDEX_TAG = "android:support:next_request_index";
    static final java.lang.String REQUEST_FRAGMENT_WHO_TAG = "android:support:request_fragment_who";
    private static final java.lang.String TAG = "FragmentActivity";
    boolean mCreated;
    final android.support.v4.app.FragmentController mFragments = android.support.v4.app.FragmentController.createController(new android.support.v4.app.FragmentActivity.HostCallbacks());
    final android.os.Handler mHandler = new android.os.Handler() {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                case 1:
                    if (android.support.v4.app.FragmentActivity.this.mStopped) {
                        android.support.v4.app.FragmentActivity.this.doReallyStop(false);
                        return;
                    }
                    return;
                case 2:
                    android.support.v4.app.FragmentActivity.this.onResumeFragments();
                    android.support.v4.app.FragmentActivity.this.mFragments.execPendingActions();
                    return;
                default:
                    super.handleMessage(msg);
                    return;
            }
        }
    };
    int mNextCandidateRequestIndex;
    boolean mOptionsMenuInvalidated;
    android.support.v4.util.SparseArrayCompat<java.lang.String> mPendingFragmentActivityResults;
    boolean mReallyStopped = true;
    boolean mRequestedPermissionsFromFragment;
    boolean mResumed;
    boolean mRetaining;
    boolean mStopped = true;

    class HostCallbacks extends android.support.v4.app.FragmentHostCallback<android.support.v4.app.FragmentActivity> {
        public HostCallbacks() {
            super(android.support.v4.app.FragmentActivity.this);
        }

        @android.annotation.SuppressLint({"NewApi"})
        public void onDump(java.lang.String prefix, java.io.FileDescriptor fd, java.io.PrintWriter writer, java.lang.String[] args) {
            android.support.v4.app.FragmentActivity.this.dump(prefix, fd, writer, args);
        }

        public boolean onShouldSaveFragmentState(android.support.v4.app.Fragment fragment) {
            return !android.support.v4.app.FragmentActivity.this.isFinishing();
        }

        public android.view.LayoutInflater onGetLayoutInflater() {
            return android.support.v4.app.FragmentActivity.this.getLayoutInflater().cloneInContext(android.support.v4.app.FragmentActivity.this);
        }

        public android.support.v4.app.FragmentActivity onGetHost() {
            return android.support.v4.app.FragmentActivity.this;
        }

        public void onSupportInvalidateOptionsMenu() {
            android.support.v4.app.FragmentActivity.this.supportInvalidateOptionsMenu();
        }

        public void onStartActivityFromFragment(android.support.v4.app.Fragment fragment, android.content.Intent intent, int requestCode) {
            android.support.v4.app.FragmentActivity.this.startActivityFromFragment(fragment, intent, requestCode);
        }

        public void onStartActivityFromFragment(android.support.v4.app.Fragment fragment, android.content.Intent intent, int requestCode, @android.support.annotation.Nullable android.os.Bundle options) {
            android.support.v4.app.FragmentActivity.this.startActivityFromFragment(fragment, intent, requestCode, options);
        }

        public void onStartIntentSenderFromFragment(android.support.v4.app.Fragment fragment, android.content.IntentSender intent, int requestCode, @android.support.annotation.Nullable android.content.Intent fillInIntent, int flagsMask, int flagsValues, int extraFlags, android.os.Bundle options) throws android.content.IntentSender.SendIntentException {
            android.support.v4.app.FragmentActivity.this.startIntentSenderFromFragment(fragment, intent, requestCode, fillInIntent, flagsMask, flagsValues, extraFlags, options);
        }

        public void onRequestPermissionsFromFragment(@android.support.annotation.NonNull android.support.v4.app.Fragment fragment, @android.support.annotation.NonNull java.lang.String[] permissions, int requestCode) {
            android.support.v4.app.FragmentActivity.this.requestPermissionsFromFragment(fragment, permissions, requestCode);
        }

        public boolean onShouldShowRequestPermissionRationale(@android.support.annotation.NonNull java.lang.String permission) {
            return android.support.v4.app.ActivityCompat.shouldShowRequestPermissionRationale(android.support.v4.app.FragmentActivity.this, permission);
        }

        public boolean onHasWindowAnimations() {
            return android.support.v4.app.FragmentActivity.this.getWindow() != null;
        }

        public int onGetWindowAnimations() {
            android.view.Window w = android.support.v4.app.FragmentActivity.this.getWindow();
            if (w == null) {
                return 0;
            }
            return w.getAttributes().windowAnimations;
        }

        public void onAttachFragment(android.support.v4.app.Fragment fragment) {
            android.support.v4.app.FragmentActivity.this.onAttachFragment(fragment);
        }

        @android.support.annotation.Nullable
        public android.view.View onFindViewById(int id) {
            return android.support.v4.app.FragmentActivity.this.findViewById(id);
        }

        public boolean onHasView() {
            android.view.Window w = android.support.v4.app.FragmentActivity.this.getWindow();
            return (w == null || w.peekDecorView() == null) ? false : true;
        }
    }

    static final class NonConfigurationInstances {
        java.lang.Object custom;
        android.support.v4.app.FragmentManagerNonConfig fragments;
        android.support.v4.util.SimpleArrayMap<java.lang.String, android.support.v4.app.LoaderManager> loaders;

        NonConfigurationInstances() {
        }
    }

    public /* bridge */ /* synthetic */ android.view.View onCreateView(android.view.View view, java.lang.String str, android.content.Context context, android.util.AttributeSet attributeSet) {
        return super.onCreateView(view, str, context, attributeSet);
    }

    public /* bridge */ /* synthetic */ android.view.View onCreateView(java.lang.String str, android.content.Context context, android.util.AttributeSet attributeSet) {
        return super.onCreateView(str, context, attributeSet);
    }

    @android.support.annotation.RequiresApi(16)
    public /* bridge */ /* synthetic */ void startActivityForResult(android.content.Intent intent, int i, @android.support.annotation.Nullable android.os.Bundle bundle) {
        super.startActivityForResult(intent, i, bundle);
    }

    public /* bridge */ /* synthetic */ void startIntentSenderForResult(android.content.IntentSender intentSender, int i, @android.support.annotation.Nullable android.content.Intent intent, int i2, int i3, int i4) throws android.content.IntentSender.SendIntentException {
        super.startIntentSenderForResult(intentSender, i, intent, i2, i3, i4);
    }

    @android.support.annotation.RequiresApi(16)
    public /* bridge */ /* synthetic */ void startIntentSenderForResult(android.content.IntentSender intentSender, int i, @android.support.annotation.Nullable android.content.Intent intent, int i2, int i3, int i4, android.os.Bundle bundle) throws android.content.IntentSender.SendIntentException {
        super.startIntentSenderForResult(intentSender, i, intent, i2, i3, i4, bundle);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, android.content.Intent data) {
        this.mFragments.noteStateNotSaved();
        int requestIndex = requestCode >> 16;
        if (requestIndex != 0) {
            int requestIndex2 = requestIndex - 1;
            java.lang.String who = (java.lang.String) this.mPendingFragmentActivityResults.get(requestIndex2);
            this.mPendingFragmentActivityResults.remove(requestIndex2);
            if (who == null) {
                android.util.Log.w(TAG, "Activity result delivered for unknown Fragment.");
                return;
            }
            android.support.v4.app.Fragment targetFragment = this.mFragments.findFragmentByWho(who);
            if (targetFragment == null) {
                android.util.Log.w(TAG, "Activity result no fragment exists for who: " + who);
            } else {
                targetFragment.onActivityResult(65535 & requestCode, resultCode, data);
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void onBackPressed() {
        if (!this.mFragments.getSupportFragmentManager().popBackStackImmediate()) {
            super.onBackPressed();
        }
    }

    @java.lang.Deprecated
    public final void setSupportMediaController(android.support.v4.media.session.MediaControllerCompat mediaController) {
        android.support.v4.media.session.MediaControllerCompat.setMediaController(this, mediaController);
    }

    @java.lang.Deprecated
    public final android.support.v4.media.session.MediaControllerCompat getSupportMediaController() {
        return android.support.v4.media.session.MediaControllerCompat.getMediaController(this);
    }

    public void supportFinishAfterTransition() {
        android.support.v4.app.ActivityCompat.finishAfterTransition(this);
    }

    public void setEnterSharedElementCallback(android.support.v4.app.SharedElementCallback callback) {
        android.support.v4.app.ActivityCompat.setEnterSharedElementCallback(this, callback);
    }

    public void setExitSharedElementCallback(android.support.v4.app.SharedElementCallback listener) {
        android.support.v4.app.ActivityCompat.setExitSharedElementCallback(this, listener);
    }

    public void supportPostponeEnterTransition() {
        android.support.v4.app.ActivityCompat.postponeEnterTransition(this);
    }

    public void supportStartPostponedEnterTransition() {
        android.support.v4.app.ActivityCompat.startPostponedEnterTransition(this);
    }

    @android.support.annotation.CallSuper
    public void onMultiWindowModeChanged(boolean isInMultiWindowMode) {
        this.mFragments.dispatchMultiWindowModeChanged(isInMultiWindowMode);
    }

    @android.support.annotation.CallSuper
    public void onPictureInPictureModeChanged(boolean isInPictureInPictureMode) {
        this.mFragments.dispatchPictureInPictureModeChanged(isInPictureInPictureMode);
    }

    public void onConfigurationChanged(android.content.res.Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        this.mFragments.dispatchConfigurationChanged(newConfig);
    }

    /* access modifiers changed from: protected */
    public void onCreate(@android.support.annotation.Nullable android.os.Bundle savedInstanceState) {
        android.support.v4.app.FragmentManagerNonConfig fragmentManagerNonConfig = null;
        this.mFragments.attachHost(null);
        super.onCreate(savedInstanceState);
        android.support.v4.app.FragmentActivity.NonConfigurationInstances nc = (android.support.v4.app.FragmentActivity.NonConfigurationInstances) getLastNonConfigurationInstance();
        if (nc != null) {
            this.mFragments.restoreLoaderNonConfig(nc.loaders);
        }
        if (savedInstanceState != null) {
            android.os.Parcelable p = savedInstanceState.getParcelable(FRAGMENTS_TAG);
            android.support.v4.app.FragmentController fragmentController = this.mFragments;
            if (nc != null) {
                fragmentManagerNonConfig = nc.fragments;
            }
            fragmentController.restoreAllState(p, fragmentManagerNonConfig);
            if (savedInstanceState.containsKey(NEXT_CANDIDATE_REQUEST_INDEX_TAG)) {
                this.mNextCandidateRequestIndex = savedInstanceState.getInt(NEXT_CANDIDATE_REQUEST_INDEX_TAG);
                int[] requestCodes = savedInstanceState.getIntArray(ALLOCATED_REQUEST_INDICIES_TAG);
                java.lang.String[] fragmentWhos = savedInstanceState.getStringArray(REQUEST_FRAGMENT_WHO_TAG);
                if (requestCodes == null || fragmentWhos == null || requestCodes.length != fragmentWhos.length) {
                    android.util.Log.w(TAG, "Invalid requestCode mapping in savedInstanceState.");
                } else {
                    this.mPendingFragmentActivityResults = new android.support.v4.util.SparseArrayCompat<>(requestCodes.length);
                    for (int i = 0; i < requestCodes.length; i++) {
                        this.mPendingFragmentActivityResults.put(requestCodes[i], fragmentWhos[i]);
                    }
                }
            }
        }
        if (this.mPendingFragmentActivityResults == null) {
            this.mPendingFragmentActivityResults = new android.support.v4.util.SparseArrayCompat<>();
            this.mNextCandidateRequestIndex = 0;
        }
        this.mFragments.dispatchCreate();
    }

    public boolean onCreatePanelMenu(int featureId, android.view.Menu menu) {
        if (featureId != 0) {
            return super.onCreatePanelMenu(featureId, menu);
        }
        boolean show = super.onCreatePanelMenu(featureId, menu) | this.mFragments.dispatchCreateOptionsMenu(menu, getMenuInflater());
        if (android.os.Build.VERSION.SDK_INT >= 11) {
            return show;
        }
        return true;
    }

    /* access modifiers changed from: 0000 */
    public final android.view.View dispatchFragmentsOnCreateView(android.view.View parent, java.lang.String name, android.content.Context context, android.util.AttributeSet attrs) {
        return this.mFragments.onCreateView(parent, name, context, attrs);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        doReallyStop(false);
        this.mFragments.dispatchDestroy();
        this.mFragments.doLoaderDestroy();
    }

    public void onLowMemory() {
        super.onLowMemory();
        this.mFragments.dispatchLowMemory();
    }

    public boolean onMenuItemSelected(int featureId, android.view.MenuItem item) {
        if (super.onMenuItemSelected(featureId, item)) {
            return true;
        }
        switch (featureId) {
            case 0:
                return this.mFragments.dispatchOptionsItemSelected(item);
            case 6:
                return this.mFragments.dispatchContextItemSelected(item);
            default:
                return false;
        }
    }

    public void onPanelClosed(int featureId, android.view.Menu menu) {
        switch (featureId) {
            case 0:
                this.mFragments.dispatchOptionsMenuClosed(menu);
                break;
        }
        super.onPanelClosed(featureId, menu);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.mResumed = false;
        if (this.mHandler.hasMessages(2)) {
            this.mHandler.removeMessages(2);
            onResumeFragments();
        }
        this.mFragments.dispatchPause();
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(android.content.Intent intent) {
        super.onNewIntent(intent);
        this.mFragments.noteStateNotSaved();
    }

    public void onStateNotSaved() {
        this.mFragments.noteStateNotSaved();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.mHandler.sendEmptyMessage(2);
        this.mResumed = true;
        this.mFragments.execPendingActions();
    }

    /* access modifiers changed from: protected */
    public void onPostResume() {
        super.onPostResume();
        this.mHandler.removeMessages(2);
        onResumeFragments();
        this.mFragments.execPendingActions();
    }

    /* access modifiers changed from: protected */
    public void onResumeFragments() {
        this.mFragments.dispatchResume();
    }

    public boolean onPreparePanel(int featureId, android.view.View view, android.view.Menu menu) {
        if (featureId != 0 || menu == null) {
            return super.onPreparePanel(featureId, view, menu);
        }
        if (this.mOptionsMenuInvalidated) {
            this.mOptionsMenuInvalidated = false;
            menu.clear();
            onCreatePanelMenu(featureId, menu);
        }
        return onPrepareOptionsPanel(view, menu) | this.mFragments.dispatchPrepareOptionsMenu(menu);
    }

    /* access modifiers changed from: protected */
    @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
    public boolean onPrepareOptionsPanel(android.view.View view, android.view.Menu menu) {
        return super.onPreparePanel(0, view, menu);
    }

    public final java.lang.Object onRetainNonConfigurationInstance() {
        if (this.mStopped) {
            doReallyStop(true);
        }
        java.lang.Object custom = onRetainCustomNonConfigurationInstance();
        android.support.v4.app.FragmentManagerNonConfig fragments = this.mFragments.retainNestedNonConfig();
        android.support.v4.util.SimpleArrayMap<java.lang.String, android.support.v4.app.LoaderManager> loaders = this.mFragments.retainLoaderNonConfig();
        if (fragments == null && loaders == null && custom == null) {
            return null;
        }
        android.support.v4.app.FragmentActivity.NonConfigurationInstances nci = new android.support.v4.app.FragmentActivity.NonConfigurationInstances();
        nci.custom = custom;
        nci.fragments = fragments;
        nci.loaders = loaders;
        return nci;
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(android.os.Bundle outState) {
        super.onSaveInstanceState(outState);
        android.os.Parcelable p = this.mFragments.saveAllState();
        if (p != null) {
            outState.putParcelable(FRAGMENTS_TAG, p);
        }
        if (this.mPendingFragmentActivityResults.size() > 0) {
            outState.putInt(NEXT_CANDIDATE_REQUEST_INDEX_TAG, this.mNextCandidateRequestIndex);
            int[] requestCodes = new int[this.mPendingFragmentActivityResults.size()];
            java.lang.String[] fragmentWhos = new java.lang.String[this.mPendingFragmentActivityResults.size()];
            for (int i = 0; i < this.mPendingFragmentActivityResults.size(); i++) {
                requestCodes[i] = this.mPendingFragmentActivityResults.keyAt(i);
                fragmentWhos[i] = (java.lang.String) this.mPendingFragmentActivityResults.valueAt(i);
            }
            outState.putIntArray(ALLOCATED_REQUEST_INDICIES_TAG, requestCodes);
            outState.putStringArray(REQUEST_FRAGMENT_WHO_TAG, fragmentWhos);
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        this.mStopped = false;
        this.mReallyStopped = false;
        this.mHandler.removeMessages(1);
        if (!this.mCreated) {
            this.mCreated = true;
            this.mFragments.dispatchActivityCreated();
        }
        this.mFragments.noteStateNotSaved();
        this.mFragments.execPendingActions();
        this.mFragments.doLoaderStart();
        this.mFragments.dispatchStart();
        this.mFragments.reportLoaderStart();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        this.mStopped = true;
        this.mHandler.sendEmptyMessage(1);
        this.mFragments.dispatchStop();
    }

    public java.lang.Object onRetainCustomNonConfigurationInstance() {
        return null;
    }

    public java.lang.Object getLastCustomNonConfigurationInstance() {
        android.support.v4.app.FragmentActivity.NonConfigurationInstances nc = (android.support.v4.app.FragmentActivity.NonConfigurationInstances) getLastNonConfigurationInstance();
        if (nc != null) {
            return nc.custom;
        }
        return null;
    }

    public void supportInvalidateOptionsMenu() {
        if (android.os.Build.VERSION.SDK_INT >= 11) {
            android.support.v4.app.ActivityCompatHoneycomb.invalidateOptionsMenu(this);
        } else {
            this.mOptionsMenuInvalidated = true;
        }
    }

    public void dump(java.lang.String prefix, java.io.FileDescriptor fd, java.io.PrintWriter writer, java.lang.String[] args) {
        if (android.os.Build.VERSION.SDK_INT >= 11) {
        }
        writer.print(prefix);
        writer.print("Local FragmentActivity ");
        writer.print(java.lang.Integer.toHexString(java.lang.System.identityHashCode(this)));
        writer.println(" State:");
        java.lang.String innerPrefix = prefix + "  ";
        writer.print(innerPrefix);
        writer.print("mCreated=");
        writer.print(this.mCreated);
        writer.print("mResumed=");
        writer.print(this.mResumed);
        writer.print(" mStopped=");
        writer.print(this.mStopped);
        writer.print(" mReallyStopped=");
        writer.println(this.mReallyStopped);
        this.mFragments.dumpLoaders(innerPrefix, fd, writer, args);
        this.mFragments.getSupportFragmentManager().dump(prefix, fd, writer, args);
        writer.print(prefix);
        writer.println("View Hierarchy:");
        dumpViewHierarchy(prefix + "  ", writer, getWindow().getDecorView());
    }

    private static java.lang.String viewToString(android.view.View view) {
        char c;
        char c2;
        char c3;
        char c4;
        char c5;
        char c6;
        char c7;
        java.lang.String pkgname;
        char c8 = 'F';
        char c9 = ch.qos.logback.core.CoreConstants.DOT;
        java.lang.StringBuilder out = new java.lang.StringBuilder(128);
        out.append(view.getClass().getName());
        out.append(ch.qos.logback.core.CoreConstants.CURLY_LEFT);
        out.append(java.lang.Integer.toHexString(java.lang.System.identityHashCode(view)));
        out.append(' ');
        switch (view.getVisibility()) {
            case 0:
                out.append('V');
                break;
            case 4:
                out.append('I');
                break;
            case 8:
                out.append('G');
                break;
            default:
                out.append(ch.qos.logback.core.CoreConstants.DOT);
                break;
        }
        if (view.isFocusable()) {
            c = 'F';
        } else {
            c = '.';
        }
        out.append(c);
        if (view.isEnabled()) {
            c2 = 'E';
        } else {
            c2 = '.';
        }
        out.append(c2);
        out.append(view.willNotDraw() ? '.' : 'D');
        if (view.isHorizontalScrollBarEnabled()) {
            c3 = 'H';
        } else {
            c3 = '.';
        }
        out.append(c3);
        if (view.isVerticalScrollBarEnabled()) {
            c4 = 'V';
        } else {
            c4 = '.';
        }
        out.append(c4);
        if (view.isClickable()) {
            c5 = 'C';
        } else {
            c5 = '.';
        }
        out.append(c5);
        if (view.isLongClickable()) {
            c6 = 'L';
        } else {
            c6 = '.';
        }
        out.append(c6);
        out.append(' ');
        if (!view.isFocused()) {
            c8 = '.';
        }
        out.append(c8);
        if (view.isSelected()) {
            c7 = 'S';
        } else {
            c7 = '.';
        }
        out.append(c7);
        if (view.isPressed()) {
            c9 = 'P';
        }
        out.append(c9);
        out.append(' ');
        out.append(view.getLeft());
        out.append(ch.qos.logback.core.CoreConstants.COMMA_CHAR);
        out.append(view.getTop());
        out.append(ch.qos.logback.core.CoreConstants.DASH_CHAR);
        out.append(view.getRight());
        out.append(ch.qos.logback.core.CoreConstants.COMMA_CHAR);
        out.append(view.getBottom());
        int id = view.getId();
        if (id != -1) {
            out.append(" #");
            out.append(java.lang.Integer.toHexString(id));
            android.content.res.Resources r = view.getResources();
            if (!(id == 0 || r == null)) {
                switch (-16777216 & id) {
                    case 16777216:
                        pkgname = "android";
                        break;
                    case 2130706432:
                        pkgname = "app";
                        break;
                    default:
                        try {
                            pkgname = r.getResourcePackageName(id);
                            break;
                        } catch (android.content.res.Resources.NotFoundException e) {
                            break;
                        }
                }
                java.lang.String typename = r.getResourceTypeName(id);
                java.lang.String entryname = r.getResourceEntryName(id);
                out.append(" ");
                out.append(pkgname);
                out.append(":");
                out.append(typename);
                out.append("/");
                out.append(entryname);
            }
        }
        out.append("}");
        return out.toString();
    }

    private void dumpViewHierarchy(java.lang.String prefix, java.io.PrintWriter writer, android.view.View view) {
        writer.print(prefix);
        if (view == null) {
            writer.println("null");
            return;
        }
        writer.println(viewToString(view));
        if (view instanceof android.view.ViewGroup) {
            android.view.ViewGroup grp = (android.view.ViewGroup) view;
            int N = grp.getChildCount();
            if (N > 0) {
                java.lang.String prefix2 = prefix + "  ";
                for (int i = 0; i < N; i++) {
                    dumpViewHierarchy(prefix2, writer, grp.getChildAt(i));
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void doReallyStop(boolean retaining) {
        if (!this.mReallyStopped) {
            this.mReallyStopped = true;
            this.mRetaining = retaining;
            this.mHandler.removeMessages(1);
            onReallyStop();
        } else if (retaining) {
            this.mFragments.doLoaderStart();
            this.mFragments.doLoaderStop(true);
        }
    }

    /* access modifiers changed from: 0000 */
    public void onReallyStop() {
        this.mFragments.doLoaderStop(this.mRetaining);
        this.mFragments.dispatchReallyStop();
    }

    public void onAttachFragment(android.support.v4.app.Fragment fragment) {
    }

    public android.support.v4.app.FragmentManager getSupportFragmentManager() {
        return this.mFragments.getSupportFragmentManager();
    }

    public android.support.v4.app.LoaderManager getSupportLoaderManager() {
        return this.mFragments.getSupportLoaderManager();
    }

    public void startActivityForResult(android.content.Intent intent, int requestCode) {
        if (!this.mStartedActivityFromFragment && requestCode != -1) {
            checkForValidRequestCode(requestCode);
        }
        super.startActivityForResult(intent, requestCode);
    }

    public final void validateRequestPermissionsRequestCode(int requestCode) {
        if (!this.mRequestedPermissionsFromFragment && requestCode != -1) {
            checkForValidRequestCode(requestCode);
        }
    }

    public void onRequestPermissionsResult(int requestCode, @android.support.annotation.NonNull java.lang.String[] permissions, @android.support.annotation.NonNull int[] grantResults) {
        int index = (requestCode >> 16) & android.support.v4.internal.view.SupportMenu.USER_MASK;
        if (index != 0) {
            int index2 = index - 1;
            java.lang.String who = (java.lang.String) this.mPendingFragmentActivityResults.get(index2);
            this.mPendingFragmentActivityResults.remove(index2);
            if (who == null) {
                android.util.Log.w(TAG, "Activity result delivered for unknown Fragment.");
                return;
            }
            android.support.v4.app.Fragment frag = this.mFragments.findFragmentByWho(who);
            if (frag == null) {
                android.util.Log.w(TAG, "Activity result no fragment exists for who: " + who);
            } else {
                frag.onRequestPermissionsResult(requestCode & android.support.v4.internal.view.SupportMenu.USER_MASK, permissions, grantResults);
            }
        }
    }

    public void startActivityFromFragment(android.support.v4.app.Fragment fragment, android.content.Intent intent, int requestCode) {
        startActivityFromFragment(fragment, intent, requestCode, null);
    }

    public void startActivityFromFragment(android.support.v4.app.Fragment fragment, android.content.Intent intent, int requestCode, @android.support.annotation.Nullable android.os.Bundle options) {
        this.mStartedActivityFromFragment = true;
        if (requestCode == -1) {
            try {
                android.support.v4.app.ActivityCompat.startActivityForResult(this, intent, -1, options);
            } finally {
                this.mStartedActivityFromFragment = false;
            }
        } else {
            checkForValidRequestCode(requestCode);
            android.support.v4.app.ActivityCompat.startActivityForResult(this, intent, ((allocateRequestIndex(fragment) + 1) << 16) + (65535 & requestCode), options);
            this.mStartedActivityFromFragment = false;
        }
    }

    public void startIntentSenderFromFragment(android.support.v4.app.Fragment fragment, android.content.IntentSender intent, int requestCode, @android.support.annotation.Nullable android.content.Intent fillInIntent, int flagsMask, int flagsValues, int extraFlags, android.os.Bundle options) throws android.content.IntentSender.SendIntentException {
        this.mStartedIntentSenderFromFragment = true;
        if (requestCode == -1) {
            try {
                android.support.v4.app.ActivityCompat.startIntentSenderForResult(this, intent, requestCode, fillInIntent, flagsMask, flagsValues, extraFlags, options);
            } finally {
                this.mStartedIntentSenderFromFragment = false;
            }
        } else {
            checkForValidRequestCode(requestCode);
            android.support.v4.app.ActivityCompat.startIntentSenderForResult(this, intent, ((allocateRequestIndex(fragment) + 1) << 16) + (65535 & requestCode), fillInIntent, flagsMask, flagsValues, extraFlags, options);
            this.mStartedIntentSenderFromFragment = false;
        }
    }

    private int allocateRequestIndex(android.support.v4.app.Fragment fragment) {
        if (this.mPendingFragmentActivityResults.size() >= MAX_NUM_PENDING_FRAGMENT_ACTIVITY_RESULTS) {
            throw new java.lang.IllegalStateException("Too many pending Fragment activity results.");
        }
        while (this.mPendingFragmentActivityResults.indexOfKey(this.mNextCandidateRequestIndex) >= 0) {
            this.mNextCandidateRequestIndex = (this.mNextCandidateRequestIndex + 1) % MAX_NUM_PENDING_FRAGMENT_ACTIVITY_RESULTS;
        }
        int requestIndex = this.mNextCandidateRequestIndex;
        this.mPendingFragmentActivityResults.put(requestIndex, fragment.mWho);
        this.mNextCandidateRequestIndex = (this.mNextCandidateRequestIndex + 1) % MAX_NUM_PENDING_FRAGMENT_ACTIVITY_RESULTS;
        return requestIndex;
    }

    /* access modifiers changed from: 0000 */
    public void requestPermissionsFromFragment(android.support.v4.app.Fragment fragment, java.lang.String[] permissions, int requestCode) {
        if (requestCode == -1) {
            android.support.v4.app.ActivityCompat.requestPermissions(this, permissions, requestCode);
            return;
        }
        checkForValidRequestCode(requestCode);
        try {
            this.mRequestedPermissionsFromFragment = true;
            android.support.v4.app.ActivityCompat.requestPermissions(this, permissions, ((allocateRequestIndex(fragment) + 1) << 16) + (65535 & requestCode));
        } finally {
            this.mRequestedPermissionsFromFragment = false;
        }
    }
}
