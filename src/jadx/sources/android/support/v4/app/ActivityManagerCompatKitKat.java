package android.support.v4.app;

@android.annotation.TargetApi(19)
@android.support.annotation.RequiresApi(19)
class ActivityManagerCompatKitKat {
    ActivityManagerCompatKitKat() {
    }

    public static boolean isLowRamDevice(android.app.ActivityManager am) {
        return am.isLowRamDevice();
    }
}
