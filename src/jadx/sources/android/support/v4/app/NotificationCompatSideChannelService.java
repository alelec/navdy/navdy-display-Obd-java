package android.support.v4.app;

public abstract class NotificationCompatSideChannelService extends android.app.Service {

    private class NotificationSideChannelStub extends android.support.v4.app.INotificationSideChannel.Stub {
        NotificationSideChannelStub() {
        }

        public void notify(java.lang.String packageName, int id, java.lang.String tag, android.app.Notification notification) throws android.os.RemoteException {
            android.support.v4.app.NotificationCompatSideChannelService.this.checkPermission(getCallingUid(), packageName);
            long idToken = clearCallingIdentity();
            try {
                android.support.v4.app.NotificationCompatSideChannelService.this.notify(packageName, id, tag, notification);
            } finally {
                restoreCallingIdentity(idToken);
            }
        }

        public void cancel(java.lang.String packageName, int id, java.lang.String tag) throws android.os.RemoteException {
            android.support.v4.app.NotificationCompatSideChannelService.this.checkPermission(getCallingUid(), packageName);
            long idToken = clearCallingIdentity();
            try {
                android.support.v4.app.NotificationCompatSideChannelService.this.cancel(packageName, id, tag);
            } finally {
                restoreCallingIdentity(idToken);
            }
        }

        public void cancelAll(java.lang.String packageName) {
            android.support.v4.app.NotificationCompatSideChannelService.this.checkPermission(getCallingUid(), packageName);
            long idToken = clearCallingIdentity();
            try {
                android.support.v4.app.NotificationCompatSideChannelService.this.cancelAll(packageName);
            } finally {
                restoreCallingIdentity(idToken);
            }
        }
    }

    public abstract void cancel(java.lang.String str, int i, java.lang.String str2);

    public abstract void cancelAll(java.lang.String str);

    public abstract void notify(java.lang.String str, int i, java.lang.String str2, android.app.Notification notification);

    public android.os.IBinder onBind(android.content.Intent intent) {
        if (!intent.getAction().equals(android.support.v4.app.NotificationManagerCompat.ACTION_BIND_SIDE_CHANNEL) || android.os.Build.VERSION.SDK_INT > 19) {
            return null;
        }
        return new android.support.v4.app.NotificationCompatSideChannelService.NotificationSideChannelStub();
    }

    /* access modifiers changed from: 0000 */
    public void checkPermission(int callingUid, java.lang.String packageName) {
        java.lang.String[] packagesForUid = getPackageManager().getPackagesForUid(callingUid);
        int length = packagesForUid.length;
        int i = 0;
        while (i < length) {
            if (!packagesForUid[i].equals(packageName)) {
                i++;
            } else {
                return;
            }
        }
        throw new java.lang.SecurityException("NotificationSideChannelService: Uid " + callingUid + " is not authorized for package " + packageName);
    }
}
