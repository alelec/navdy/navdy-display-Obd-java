package android.support.v4.app;

public final class RemoteInput extends android.support.v4.app.RemoteInputCompatBase.RemoteInput {
    public static final java.lang.String EXTRA_RESULTS_DATA = "android.remoteinput.resultsData";
    @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
    public static final android.support.v4.app.RemoteInputCompatBase.RemoteInput.Factory FACTORY = new android.support.v4.app.RemoteInputCompatBase.RemoteInput.Factory() {
        public android.support.v4.app.RemoteInput build(java.lang.String resultKey, java.lang.CharSequence label, java.lang.CharSequence[] choices, boolean allowFreeFormInput, android.os.Bundle extras) {
            return new android.support.v4.app.RemoteInput(resultKey, label, choices, allowFreeFormInput, extras);
        }

        public android.support.v4.app.RemoteInput[] newArray(int size) {
            return new android.support.v4.app.RemoteInput[size];
        }
    };
    private static final android.support.v4.app.RemoteInput.Impl IMPL;
    public static final java.lang.String RESULTS_CLIP_LABEL = "android.remoteinput.results";
    private static final java.lang.String TAG = "RemoteInput";
    private final boolean mAllowFreeFormInput;
    private final java.lang.CharSequence[] mChoices;
    private final android.os.Bundle mExtras;
    private final java.lang.CharSequence mLabel;
    private final java.lang.String mResultKey;

    public static final class Builder {
        private boolean mAllowFreeFormInput = true;
        private java.lang.CharSequence[] mChoices;
        private android.os.Bundle mExtras = new android.os.Bundle();
        private java.lang.CharSequence mLabel;
        private final java.lang.String mResultKey;

        public Builder(java.lang.String resultKey) {
            if (resultKey == null) {
                throw new java.lang.IllegalArgumentException("Result key can't be null");
            }
            this.mResultKey = resultKey;
        }

        public android.support.v4.app.RemoteInput.Builder setLabel(java.lang.CharSequence label) {
            this.mLabel = label;
            return this;
        }

        public android.support.v4.app.RemoteInput.Builder setChoices(java.lang.CharSequence[] choices) {
            this.mChoices = choices;
            return this;
        }

        public android.support.v4.app.RemoteInput.Builder setAllowFreeFormInput(boolean allowFreeFormInput) {
            this.mAllowFreeFormInput = allowFreeFormInput;
            return this;
        }

        public android.support.v4.app.RemoteInput.Builder addExtras(android.os.Bundle extras) {
            if (extras != null) {
                this.mExtras.putAll(extras);
            }
            return this;
        }

        public android.os.Bundle getExtras() {
            return this.mExtras;
        }

        public android.support.v4.app.RemoteInput build() {
            return new android.support.v4.app.RemoteInput(this.mResultKey, this.mLabel, this.mChoices, this.mAllowFreeFormInput, this.mExtras);
        }
    }

    interface Impl {
        void addResultsToIntent(android.support.v4.app.RemoteInput[] remoteInputArr, android.content.Intent intent, android.os.Bundle bundle);

        android.os.Bundle getResultsFromIntent(android.content.Intent intent);
    }

    static class ImplApi20 implements android.support.v4.app.RemoteInput.Impl {
        ImplApi20() {
        }

        public android.os.Bundle getResultsFromIntent(android.content.Intent intent) {
            return android.support.v4.app.RemoteInputCompatApi20.getResultsFromIntent(intent);
        }

        public void addResultsToIntent(android.support.v4.app.RemoteInput[] remoteInputs, android.content.Intent intent, android.os.Bundle results) {
            android.support.v4.app.RemoteInputCompatApi20.addResultsToIntent(remoteInputs, intent, results);
        }
    }

    static class ImplBase implements android.support.v4.app.RemoteInput.Impl {
        ImplBase() {
        }

        public android.os.Bundle getResultsFromIntent(android.content.Intent intent) {
            android.util.Log.w(android.support.v4.app.RemoteInput.TAG, "RemoteInput is only supported from API Level 16");
            return null;
        }

        public void addResultsToIntent(android.support.v4.app.RemoteInput[] remoteInputs, android.content.Intent intent, android.os.Bundle results) {
            android.util.Log.w(android.support.v4.app.RemoteInput.TAG, "RemoteInput is only supported from API Level 16");
        }
    }

    static class ImplJellybean implements android.support.v4.app.RemoteInput.Impl {
        ImplJellybean() {
        }

        public android.os.Bundle getResultsFromIntent(android.content.Intent intent) {
            return android.support.v4.app.RemoteInputCompatJellybean.getResultsFromIntent(intent);
        }

        public void addResultsToIntent(android.support.v4.app.RemoteInput[] remoteInputs, android.content.Intent intent, android.os.Bundle results) {
            android.support.v4.app.RemoteInputCompatJellybean.addResultsToIntent(remoteInputs, intent, results);
        }
    }

    RemoteInput(java.lang.String resultKey, java.lang.CharSequence label, java.lang.CharSequence[] choices, boolean allowFreeFormInput, android.os.Bundle extras) {
        this.mResultKey = resultKey;
        this.mLabel = label;
        this.mChoices = choices;
        this.mAllowFreeFormInput = allowFreeFormInput;
        this.mExtras = extras;
    }

    public java.lang.String getResultKey() {
        return this.mResultKey;
    }

    public java.lang.CharSequence getLabel() {
        return this.mLabel;
    }

    public java.lang.CharSequence[] getChoices() {
        return this.mChoices;
    }

    public boolean getAllowFreeFormInput() {
        return this.mAllowFreeFormInput;
    }

    public android.os.Bundle getExtras() {
        return this.mExtras;
    }

    public static android.os.Bundle getResultsFromIntent(android.content.Intent intent) {
        return IMPL.getResultsFromIntent(intent);
    }

    public static void addResultsToIntent(android.support.v4.app.RemoteInput[] remoteInputs, android.content.Intent intent, android.os.Bundle results) {
        IMPL.addResultsToIntent(remoteInputs, intent, results);
    }

    static {
        if (android.os.Build.VERSION.SDK_INT >= 20) {
            IMPL = new android.support.v4.app.RemoteInput.ImplApi20();
        } else if (android.os.Build.VERSION.SDK_INT >= 16) {
            IMPL = new android.support.v4.app.RemoteInput.ImplJellybean();
        } else {
            IMPL = new android.support.v4.app.RemoteInput.ImplBase();
        }
    }
}
