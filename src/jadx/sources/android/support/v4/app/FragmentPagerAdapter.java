package android.support.v4.app;

public abstract class FragmentPagerAdapter extends android.support.v4.view.PagerAdapter {
    private static final boolean DEBUG = false;
    private static final java.lang.String TAG = "FragmentPagerAdapter";
    private android.support.v4.app.FragmentTransaction mCurTransaction = null;
    private android.support.v4.app.Fragment mCurrentPrimaryItem = null;
    private final android.support.v4.app.FragmentManager mFragmentManager;

    public abstract android.support.v4.app.Fragment getItem(int i);

    public FragmentPagerAdapter(android.support.v4.app.FragmentManager fm) {
        this.mFragmentManager = fm;
    }

    public void startUpdate(android.view.ViewGroup container) {
        if (container.getId() == -1) {
            throw new java.lang.IllegalStateException("ViewPager with adapter " + this + " requires a view id");
        }
    }

    public java.lang.Object instantiateItem(android.view.ViewGroup container, int position) {
        if (this.mCurTransaction == null) {
            this.mCurTransaction = this.mFragmentManager.beginTransaction();
        }
        long itemId = getItemId(position);
        android.support.v4.app.Fragment fragment = this.mFragmentManager.findFragmentByTag(makeFragmentName(container.getId(), itemId));
        if (fragment != null) {
            this.mCurTransaction.attach(fragment);
        } else {
            fragment = getItem(position);
            this.mCurTransaction.add(container.getId(), fragment, makeFragmentName(container.getId(), itemId));
        }
        if (fragment != this.mCurrentPrimaryItem) {
            fragment.setMenuVisibility(false);
            fragment.setUserVisibleHint(false);
        }
        return fragment;
    }

    public void destroyItem(android.view.ViewGroup container, int position, java.lang.Object object) {
        if (this.mCurTransaction == null) {
            this.mCurTransaction = this.mFragmentManager.beginTransaction();
        }
        this.mCurTransaction.detach((android.support.v4.app.Fragment) object);
    }

    public void setPrimaryItem(android.view.ViewGroup container, int position, java.lang.Object object) {
        android.support.v4.app.Fragment fragment = (android.support.v4.app.Fragment) object;
        if (fragment != this.mCurrentPrimaryItem) {
            if (this.mCurrentPrimaryItem != null) {
                this.mCurrentPrimaryItem.setMenuVisibility(false);
                this.mCurrentPrimaryItem.setUserVisibleHint(false);
            }
            if (fragment != null) {
                fragment.setMenuVisibility(true);
                fragment.setUserVisibleHint(true);
            }
            this.mCurrentPrimaryItem = fragment;
        }
    }

    public void finishUpdate(android.view.ViewGroup container) {
        if (this.mCurTransaction != null) {
            this.mCurTransaction.commitNowAllowingStateLoss();
            this.mCurTransaction = null;
        }
    }

    public boolean isViewFromObject(android.view.View view, java.lang.Object object) {
        return ((android.support.v4.app.Fragment) object).getView() == view;
    }

    public android.os.Parcelable saveState() {
        return null;
    }

    public void restoreState(android.os.Parcelable state, java.lang.ClassLoader loader) {
    }

    public long getItemId(int position) {
        return (long) position;
    }

    private static java.lang.String makeFragmentName(int viewId, long id) {
        return "android:switcher:" + viewId + ":" + id;
    }
}
