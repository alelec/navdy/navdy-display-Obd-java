package android.support.v4.app;

@android.annotation.TargetApi(9)
@android.support.annotation.RequiresApi(9)
class RemoteInputCompatBase {

    public static abstract class RemoteInput {

        public interface Factory {
            android.support.v4.app.RemoteInputCompatBase.RemoteInput build(java.lang.String str, java.lang.CharSequence charSequence, java.lang.CharSequence[] charSequenceArr, boolean z, android.os.Bundle bundle);

            android.support.v4.app.RemoteInputCompatBase.RemoteInput[] newArray(int i);
        }

        /* access modifiers changed from: protected */
        public abstract boolean getAllowFreeFormInput();

        /* access modifiers changed from: protected */
        public abstract java.lang.CharSequence[] getChoices();

        /* access modifiers changed from: protected */
        public abstract android.os.Bundle getExtras();

        /* access modifiers changed from: protected */
        public abstract java.lang.CharSequence getLabel();

        /* access modifiers changed from: protected */
        public abstract java.lang.String getResultKey();
    }

    RemoteInputCompatBase() {
    }
}
