package android.support.v4.app;

@android.annotation.TargetApi(19)
@android.support.annotation.RequiresApi(19)
class NotificationCompatKitKat {

    public static class Builder implements android.support.v4.app.NotificationBuilderWithBuilderAccessor, android.support.v4.app.NotificationBuilderWithActions {
        private android.app.Notification.Builder b;
        private java.util.List<android.os.Bundle> mActionExtrasList = new java.util.ArrayList();
        private android.widget.RemoteViews mBigContentView;
        private android.widget.RemoteViews mContentView;
        private android.os.Bundle mExtras;

        public Builder(android.content.Context context, android.app.Notification n, java.lang.CharSequence contentTitle, java.lang.CharSequence contentText, java.lang.CharSequence contentInfo, android.widget.RemoteViews tickerView, int number, android.app.PendingIntent contentIntent, android.app.PendingIntent fullScreenIntent, android.graphics.Bitmap largeIcon, int progressMax, int progress, boolean progressIndeterminate, boolean showWhen, boolean useChronometer, int priority, java.lang.CharSequence subText, boolean localOnly, java.util.ArrayList<java.lang.String> people, android.os.Bundle extras, java.lang.String groupKey, boolean groupSummary, java.lang.String sortKey, android.widget.RemoteViews contentView, android.widget.RemoteViews bigContentView) {
            this.b = new android.app.Notification.Builder(context).setWhen(n.when).setShowWhen(showWhen).setSmallIcon(n.icon, n.iconLevel).setContent(n.contentView).setTicker(n.tickerText, tickerView).setSound(n.sound, n.audioStreamType).setVibrate(n.vibrate).setLights(n.ledARGB, n.ledOnMS, n.ledOffMS).setOngoing((n.flags & 2) != 0).setOnlyAlertOnce((n.flags & 8) != 0).setAutoCancel((n.flags & 16) != 0).setDefaults(n.defaults).setContentTitle(contentTitle).setContentText(contentText).setSubText(subText).setContentInfo(contentInfo).setContentIntent(contentIntent).setDeleteIntent(n.deleteIntent).setFullScreenIntent(fullScreenIntent, (n.flags & 128) != 0).setLargeIcon(largeIcon).setNumber(number).setUsesChronometer(useChronometer).setPriority(priority).setProgress(progressMax, progress, progressIndeterminate);
            this.mExtras = new android.os.Bundle();
            if (extras != null) {
                this.mExtras.putAll(extras);
            }
            if (people != null && !people.isEmpty()) {
                this.mExtras.putStringArray(android.support.v4.app.NotificationCompat.EXTRA_PEOPLE, (java.lang.String[]) people.toArray(new java.lang.String[people.size()]));
            }
            if (localOnly) {
                this.mExtras.putBoolean(android.support.v4.app.NotificationCompatExtras.EXTRA_LOCAL_ONLY, true);
            }
            if (groupKey != null) {
                this.mExtras.putString(android.support.v4.app.NotificationCompatExtras.EXTRA_GROUP_KEY, groupKey);
                if (groupSummary) {
                    this.mExtras.putBoolean(android.support.v4.app.NotificationCompatExtras.EXTRA_GROUP_SUMMARY, true);
                } else {
                    this.mExtras.putBoolean(android.support.v4.app.NotificationManagerCompat.EXTRA_USE_SIDE_CHANNEL, true);
                }
            }
            if (sortKey != null) {
                this.mExtras.putString(android.support.v4.app.NotificationCompatExtras.EXTRA_SORT_KEY, sortKey);
            }
            this.mContentView = contentView;
            this.mBigContentView = bigContentView;
        }

        public void addAction(android.support.v4.app.NotificationCompatBase.Action action) {
            this.mActionExtrasList.add(android.support.v4.app.NotificationCompatJellybean.writeActionAndGetExtras(this.b, action));
        }

        public android.app.Notification.Builder getBuilder() {
            return this.b;
        }

        public android.app.Notification build() {
            android.util.SparseArray<android.os.Bundle> actionExtrasMap = android.support.v4.app.NotificationCompatJellybean.buildActionExtrasMap(this.mActionExtrasList);
            if (actionExtrasMap != null) {
                this.mExtras.putSparseParcelableArray(android.support.v4.app.NotificationCompatExtras.EXTRA_ACTION_EXTRAS, actionExtrasMap);
            }
            this.b.setExtras(this.mExtras);
            android.app.Notification notification = this.b.build();
            if (this.mContentView != null) {
                notification.contentView = this.mContentView;
            }
            if (this.mBigContentView != null) {
                notification.bigContentView = this.mBigContentView;
            }
            return notification;
        }
    }

    NotificationCompatKitKat() {
    }

    public static android.os.Bundle getExtras(android.app.Notification notif) {
        return notif.extras;
    }

    public static int getActionCount(android.app.Notification notif) {
        if (notif.actions != null) {
            return notif.actions.length;
        }
        return 0;
    }

    public static android.support.v4.app.NotificationCompatBase.Action getAction(android.app.Notification notif, int actionIndex, android.support.v4.app.NotificationCompatBase.Action.Factory factory, android.support.v4.app.RemoteInputCompatBase.RemoteInput.Factory remoteInputFactory) {
        android.app.Notification.Action action = notif.actions[actionIndex];
        android.os.Bundle actionExtras = null;
        android.util.SparseArray<android.os.Bundle> actionExtrasMap = notif.extras.getSparseParcelableArray(android.support.v4.app.NotificationCompatExtras.EXTRA_ACTION_EXTRAS);
        if (actionExtrasMap != null) {
            actionExtras = (android.os.Bundle) actionExtrasMap.get(actionIndex);
        }
        return android.support.v4.app.NotificationCompatJellybean.readAction(factory, remoteInputFactory, action.icon, action.title, action.actionIntent, actionExtras);
    }

    public static boolean getLocalOnly(android.app.Notification notif) {
        return notif.extras.getBoolean(android.support.v4.app.NotificationCompatExtras.EXTRA_LOCAL_ONLY);
    }

    public static java.lang.String getGroup(android.app.Notification notif) {
        return notif.extras.getString(android.support.v4.app.NotificationCompatExtras.EXTRA_GROUP_KEY);
    }

    public static boolean isGroupSummary(android.app.Notification notif) {
        return notif.extras.getBoolean(android.support.v4.app.NotificationCompatExtras.EXTRA_GROUP_SUMMARY);
    }

    public static java.lang.String getSortKey(android.app.Notification notif) {
        return notif.extras.getString(android.support.v4.app.NotificationCompatExtras.EXTRA_SORT_KEY);
    }
}
