package android.support.v4.app;

public class NotificationCompat {
    public static final java.lang.String CATEGORY_ALARM = "alarm";
    public static final java.lang.String CATEGORY_CALL = "call";
    public static final java.lang.String CATEGORY_EMAIL = "email";
    public static final java.lang.String CATEGORY_ERROR = "err";
    public static final java.lang.String CATEGORY_EVENT = "event";
    public static final java.lang.String CATEGORY_MESSAGE = "msg";
    public static final java.lang.String CATEGORY_PROGRESS = "progress";
    public static final java.lang.String CATEGORY_PROMO = "promo";
    public static final java.lang.String CATEGORY_RECOMMENDATION = "recommendation";
    public static final java.lang.String CATEGORY_REMINDER = "reminder";
    public static final java.lang.String CATEGORY_SERVICE = "service";
    public static final java.lang.String CATEGORY_SOCIAL = "social";
    public static final java.lang.String CATEGORY_STATUS = "status";
    public static final java.lang.String CATEGORY_SYSTEM = "sys";
    public static final java.lang.String CATEGORY_TRANSPORT = "transport";
    @android.support.annotation.ColorInt
    public static final int COLOR_DEFAULT = 0;
    public static final int DEFAULT_ALL = -1;
    public static final int DEFAULT_LIGHTS = 4;
    public static final int DEFAULT_SOUND = 1;
    public static final int DEFAULT_VIBRATE = 2;
    public static final java.lang.String EXTRA_BACKGROUND_IMAGE_URI = "android.backgroundImageUri";
    public static final java.lang.String EXTRA_BIG_TEXT = "android.bigText";
    public static final java.lang.String EXTRA_COMPACT_ACTIONS = "android.compactActions";
    public static final java.lang.String EXTRA_CONVERSATION_TITLE = "android.conversationTitle";
    public static final java.lang.String EXTRA_INFO_TEXT = "android.infoText";
    public static final java.lang.String EXTRA_LARGE_ICON = "android.largeIcon";
    public static final java.lang.String EXTRA_LARGE_ICON_BIG = "android.largeIcon.big";
    public static final java.lang.String EXTRA_MEDIA_SESSION = "android.mediaSession";
    public static final java.lang.String EXTRA_MESSAGES = "android.messages";
    public static final java.lang.String EXTRA_PEOPLE = "android.people";
    public static final java.lang.String EXTRA_PICTURE = "android.picture";
    public static final java.lang.String EXTRA_PROGRESS = "android.progress";
    public static final java.lang.String EXTRA_PROGRESS_INDETERMINATE = "android.progressIndeterminate";
    public static final java.lang.String EXTRA_PROGRESS_MAX = "android.progressMax";
    public static final java.lang.String EXTRA_REMOTE_INPUT_HISTORY = "android.remoteInputHistory";
    public static final java.lang.String EXTRA_SELF_DISPLAY_NAME = "android.selfDisplayName";
    public static final java.lang.String EXTRA_SHOW_CHRONOMETER = "android.showChronometer";
    public static final java.lang.String EXTRA_SHOW_WHEN = "android.showWhen";
    public static final java.lang.String EXTRA_SMALL_ICON = "android.icon";
    public static final java.lang.String EXTRA_SUB_TEXT = "android.subText";
    public static final java.lang.String EXTRA_SUMMARY_TEXT = "android.summaryText";
    public static final java.lang.String EXTRA_TEMPLATE = "android.template";
    public static final java.lang.String EXTRA_TEXT = "android.text";
    public static final java.lang.String EXTRA_TEXT_LINES = "android.textLines";
    public static final java.lang.String EXTRA_TITLE = "android.title";
    public static final java.lang.String EXTRA_TITLE_BIG = "android.title.big";
    public static final int FLAG_AUTO_CANCEL = 16;
    public static final int FLAG_FOREGROUND_SERVICE = 64;
    public static final int FLAG_GROUP_SUMMARY = 512;
    @java.lang.Deprecated
    public static final int FLAG_HIGH_PRIORITY = 128;
    public static final int FLAG_INSISTENT = 4;
    public static final int FLAG_LOCAL_ONLY = 256;
    public static final int FLAG_NO_CLEAR = 32;
    public static final int FLAG_ONGOING_EVENT = 2;
    public static final int FLAG_ONLY_ALERT_ONCE = 8;
    public static final int FLAG_SHOW_LIGHTS = 1;
    static final android.support.v4.app.NotificationCompat.NotificationCompatImpl IMPL;
    public static final int PRIORITY_DEFAULT = 0;
    public static final int PRIORITY_HIGH = 1;
    public static final int PRIORITY_LOW = -1;
    public static final int PRIORITY_MAX = 2;
    public static final int PRIORITY_MIN = -2;
    public static final int STREAM_DEFAULT = -1;
    public static final int VISIBILITY_PRIVATE = 0;
    public static final int VISIBILITY_PUBLIC = 1;
    public static final int VISIBILITY_SECRET = -1;

    public static class Action extends android.support.v4.app.NotificationCompatBase.Action {
        @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
        public static final android.support.v4.app.NotificationCompatBase.Action.Factory FACTORY = new android.support.v4.app.NotificationCompatBase.Action.Factory() {
            public android.support.v4.app.NotificationCompatBase.Action build(int icon, java.lang.CharSequence title, android.app.PendingIntent actionIntent, android.os.Bundle extras, android.support.v4.app.RemoteInputCompatBase.RemoteInput[] remoteInputs, boolean allowGeneratedReplies) {
                return new android.support.v4.app.NotificationCompat.Action(icon, title, actionIntent, extras, (android.support.v4.app.RemoteInput[]) remoteInputs, allowGeneratedReplies);
            }

            public android.support.v4.app.NotificationCompat.Action[] newArray(int length) {
                return new android.support.v4.app.NotificationCompat.Action[length];
            }
        };
        public android.app.PendingIntent actionIntent;
        public int icon;
        private boolean mAllowGeneratedReplies;
        final android.os.Bundle mExtras;
        private final android.support.v4.app.RemoteInput[] mRemoteInputs;
        public java.lang.CharSequence title;

        public static final class Builder {
            private boolean mAllowGeneratedReplies;
            private final android.os.Bundle mExtras;
            private final int mIcon;
            private final android.app.PendingIntent mIntent;
            private java.util.ArrayList<android.support.v4.app.RemoteInput> mRemoteInputs;
            private final java.lang.CharSequence mTitle;

            public Builder(int icon, java.lang.CharSequence title, android.app.PendingIntent intent) {
                this(icon, title, intent, new android.os.Bundle(), null, true);
            }

            public Builder(android.support.v4.app.NotificationCompat.Action action) {
                this(action.icon, action.title, action.actionIntent, new android.os.Bundle(action.mExtras), action.getRemoteInputs(), action.getAllowGeneratedReplies());
            }

            private Builder(int icon, java.lang.CharSequence title, android.app.PendingIntent intent, android.os.Bundle extras, android.support.v4.app.RemoteInput[] remoteInputs, boolean allowGeneratedReplies) {
                java.util.ArrayList<android.support.v4.app.RemoteInput> arrayList;
                this.mAllowGeneratedReplies = true;
                this.mIcon = icon;
                this.mTitle = android.support.v4.app.NotificationCompat.Builder.limitCharSequenceLength(title);
                this.mIntent = intent;
                this.mExtras = extras;
                if (remoteInputs == null) {
                    arrayList = null;
                } else {
                    arrayList = new java.util.ArrayList<>(java.util.Arrays.asList(remoteInputs));
                }
                this.mRemoteInputs = arrayList;
                this.mAllowGeneratedReplies = allowGeneratedReplies;
            }

            public android.support.v4.app.NotificationCompat.Action.Builder addExtras(android.os.Bundle extras) {
                if (extras != null) {
                    this.mExtras.putAll(extras);
                }
                return this;
            }

            public android.os.Bundle getExtras() {
                return this.mExtras;
            }

            public android.support.v4.app.NotificationCompat.Action.Builder addRemoteInput(android.support.v4.app.RemoteInput remoteInput) {
                if (this.mRemoteInputs == null) {
                    this.mRemoteInputs = new java.util.ArrayList<>();
                }
                this.mRemoteInputs.add(remoteInput);
                return this;
            }

            public android.support.v4.app.NotificationCompat.Action.Builder setAllowGeneratedReplies(boolean allowGeneratedReplies) {
                this.mAllowGeneratedReplies = allowGeneratedReplies;
                return this;
            }

            public android.support.v4.app.NotificationCompat.Action.Builder extend(android.support.v4.app.NotificationCompat.Action.Extender extender) {
                extender.extend(this);
                return this;
            }

            public android.support.v4.app.NotificationCompat.Action build() {
                return new android.support.v4.app.NotificationCompat.Action(this.mIcon, this.mTitle, this.mIntent, this.mExtras, this.mRemoteInputs != null ? (android.support.v4.app.RemoteInput[]) this.mRemoteInputs.toArray(new android.support.v4.app.RemoteInput[this.mRemoteInputs.size()]) : null, this.mAllowGeneratedReplies);
            }
        }

        public interface Extender {
            android.support.v4.app.NotificationCompat.Action.Builder extend(android.support.v4.app.NotificationCompat.Action.Builder builder);
        }

        public static final class WearableExtender implements android.support.v4.app.NotificationCompat.Action.Extender {
            private static final int DEFAULT_FLAGS = 1;
            private static final java.lang.String EXTRA_WEARABLE_EXTENSIONS = "android.wearable.EXTENSIONS";
            private static final int FLAG_AVAILABLE_OFFLINE = 1;
            private static final int FLAG_HINT_DISPLAY_INLINE = 4;
            private static final int FLAG_HINT_LAUNCHES_ACTIVITY = 2;
            private static final java.lang.String KEY_CANCEL_LABEL = "cancelLabel";
            private static final java.lang.String KEY_CONFIRM_LABEL = "confirmLabel";
            private static final java.lang.String KEY_FLAGS = "flags";
            private static final java.lang.String KEY_IN_PROGRESS_LABEL = "inProgressLabel";
            private java.lang.CharSequence mCancelLabel;
            private java.lang.CharSequence mConfirmLabel;
            private int mFlags = 1;
            private java.lang.CharSequence mInProgressLabel;

            public WearableExtender() {
            }

            public WearableExtender(android.support.v4.app.NotificationCompat.Action action) {
                android.os.Bundle wearableBundle = action.getExtras().getBundle(EXTRA_WEARABLE_EXTENSIONS);
                if (wearableBundle != null) {
                    this.mFlags = wearableBundle.getInt(KEY_FLAGS, 1);
                    this.mInProgressLabel = wearableBundle.getCharSequence(KEY_IN_PROGRESS_LABEL);
                    this.mConfirmLabel = wearableBundle.getCharSequence(KEY_CONFIRM_LABEL);
                    this.mCancelLabel = wearableBundle.getCharSequence(KEY_CANCEL_LABEL);
                }
            }

            public android.support.v4.app.NotificationCompat.Action.Builder extend(android.support.v4.app.NotificationCompat.Action.Builder builder) {
                android.os.Bundle wearableBundle = new android.os.Bundle();
                if (this.mFlags != 1) {
                    wearableBundle.putInt(KEY_FLAGS, this.mFlags);
                }
                if (this.mInProgressLabel != null) {
                    wearableBundle.putCharSequence(KEY_IN_PROGRESS_LABEL, this.mInProgressLabel);
                }
                if (this.mConfirmLabel != null) {
                    wearableBundle.putCharSequence(KEY_CONFIRM_LABEL, this.mConfirmLabel);
                }
                if (this.mCancelLabel != null) {
                    wearableBundle.putCharSequence(KEY_CANCEL_LABEL, this.mCancelLabel);
                }
                builder.getExtras().putBundle(EXTRA_WEARABLE_EXTENSIONS, wearableBundle);
                return builder;
            }

            public android.support.v4.app.NotificationCompat.Action.WearableExtender clone() {
                android.support.v4.app.NotificationCompat.Action.WearableExtender that = new android.support.v4.app.NotificationCompat.Action.WearableExtender();
                that.mFlags = this.mFlags;
                that.mInProgressLabel = this.mInProgressLabel;
                that.mConfirmLabel = this.mConfirmLabel;
                that.mCancelLabel = this.mCancelLabel;
                return that;
            }

            public android.support.v4.app.NotificationCompat.Action.WearableExtender setAvailableOffline(boolean availableOffline) {
                setFlag(1, availableOffline);
                return this;
            }

            public boolean isAvailableOffline() {
                return (this.mFlags & 1) != 0;
            }

            private void setFlag(int mask, boolean value) {
                if (value) {
                    this.mFlags |= mask;
                } else {
                    this.mFlags &= mask ^ -1;
                }
            }

            public android.support.v4.app.NotificationCompat.Action.WearableExtender setInProgressLabel(java.lang.CharSequence label) {
                this.mInProgressLabel = label;
                return this;
            }

            public java.lang.CharSequence getInProgressLabel() {
                return this.mInProgressLabel;
            }

            public android.support.v4.app.NotificationCompat.Action.WearableExtender setConfirmLabel(java.lang.CharSequence label) {
                this.mConfirmLabel = label;
                return this;
            }

            public java.lang.CharSequence getConfirmLabel() {
                return this.mConfirmLabel;
            }

            public android.support.v4.app.NotificationCompat.Action.WearableExtender setCancelLabel(java.lang.CharSequence label) {
                this.mCancelLabel = label;
                return this;
            }

            public java.lang.CharSequence getCancelLabel() {
                return this.mCancelLabel;
            }

            public android.support.v4.app.NotificationCompat.Action.WearableExtender setHintLaunchesActivity(boolean hintLaunchesActivity) {
                setFlag(2, hintLaunchesActivity);
                return this;
            }

            public boolean getHintLaunchesActivity() {
                return (this.mFlags & 2) != 0;
            }

            public android.support.v4.app.NotificationCompat.Action.WearableExtender setHintDisplayActionInline(boolean hintDisplayInline) {
                setFlag(4, hintDisplayInline);
                return this;
            }

            public boolean getHintDisplayActionInline() {
                return (this.mFlags & 4) != 0;
            }
        }

        public Action(int icon2, java.lang.CharSequence title2, android.app.PendingIntent intent) {
            this(icon2, title2, intent, new android.os.Bundle(), null, true);
        }

        Action(int icon2, java.lang.CharSequence title2, android.app.PendingIntent intent, android.os.Bundle extras, android.support.v4.app.RemoteInput[] remoteInputs, boolean allowGeneratedReplies) {
            this.icon = icon2;
            this.title = android.support.v4.app.NotificationCompat.Builder.limitCharSequenceLength(title2);
            this.actionIntent = intent;
            if (extras == null) {
                extras = new android.os.Bundle();
            }
            this.mExtras = extras;
            this.mRemoteInputs = remoteInputs;
            this.mAllowGeneratedReplies = allowGeneratedReplies;
        }

        public int getIcon() {
            return this.icon;
        }

        public java.lang.CharSequence getTitle() {
            return this.title;
        }

        public android.app.PendingIntent getActionIntent() {
            return this.actionIntent;
        }

        public android.os.Bundle getExtras() {
            return this.mExtras;
        }

        public boolean getAllowGeneratedReplies() {
            return this.mAllowGeneratedReplies;
        }

        public android.support.v4.app.RemoteInput[] getRemoteInputs() {
            return this.mRemoteInputs;
        }
    }

    public static class BigPictureStyle extends android.support.v4.app.NotificationCompat.Style {
        android.graphics.Bitmap mBigLargeIcon;
        boolean mBigLargeIconSet;
        android.graphics.Bitmap mPicture;

        public BigPictureStyle() {
        }

        public BigPictureStyle(android.support.v4.app.NotificationCompat.Builder builder) {
            setBuilder(builder);
        }

        public android.support.v4.app.NotificationCompat.BigPictureStyle setBigContentTitle(java.lang.CharSequence title) {
            this.mBigContentTitle = android.support.v4.app.NotificationCompat.Builder.limitCharSequenceLength(title);
            return this;
        }

        public android.support.v4.app.NotificationCompat.BigPictureStyle setSummaryText(java.lang.CharSequence cs) {
            this.mSummaryText = android.support.v4.app.NotificationCompat.Builder.limitCharSequenceLength(cs);
            this.mSummaryTextSet = true;
            return this;
        }

        public android.support.v4.app.NotificationCompat.BigPictureStyle bigPicture(android.graphics.Bitmap b) {
            this.mPicture = b;
            return this;
        }

        public android.support.v4.app.NotificationCompat.BigPictureStyle bigLargeIcon(android.graphics.Bitmap b) {
            this.mBigLargeIcon = b;
            this.mBigLargeIconSet = true;
            return this;
        }
    }

    public static class BigTextStyle extends android.support.v4.app.NotificationCompat.Style {
        java.lang.CharSequence mBigText;

        public BigTextStyle() {
        }

        public BigTextStyle(android.support.v4.app.NotificationCompat.Builder builder) {
            setBuilder(builder);
        }

        public android.support.v4.app.NotificationCompat.BigTextStyle setBigContentTitle(java.lang.CharSequence title) {
            this.mBigContentTitle = android.support.v4.app.NotificationCompat.Builder.limitCharSequenceLength(title);
            return this;
        }

        public android.support.v4.app.NotificationCompat.BigTextStyle setSummaryText(java.lang.CharSequence cs) {
            this.mSummaryText = android.support.v4.app.NotificationCompat.Builder.limitCharSequenceLength(cs);
            this.mSummaryTextSet = true;
            return this;
        }

        public android.support.v4.app.NotificationCompat.BigTextStyle bigText(java.lang.CharSequence cs) {
            this.mBigText = android.support.v4.app.NotificationCompat.Builder.limitCharSequenceLength(cs);
            return this;
        }
    }

    public static class Builder {
        private static final int MAX_CHARSEQUENCE_LENGTH = 5120;
        @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
        public java.util.ArrayList<android.support.v4.app.NotificationCompat.Action> mActions = new java.util.ArrayList<>();
        android.widget.RemoteViews mBigContentView;
        java.lang.String mCategory;
        int mColor = 0;
        @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
        public java.lang.CharSequence mContentInfo;
        android.app.PendingIntent mContentIntent;
        @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
        public java.lang.CharSequence mContentText;
        @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
        public java.lang.CharSequence mContentTitle;
        android.widget.RemoteViews mContentView;
        @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
        public android.content.Context mContext;
        android.os.Bundle mExtras;
        android.app.PendingIntent mFullScreenIntent;
        java.lang.String mGroupKey;
        boolean mGroupSummary;
        android.widget.RemoteViews mHeadsUpContentView;
        @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
        public android.graphics.Bitmap mLargeIcon;
        boolean mLocalOnly = false;
        @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
        public android.app.Notification mNotification = new android.app.Notification();
        @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
        public int mNumber;
        public java.util.ArrayList<java.lang.String> mPeople;
        int mPriority;
        int mProgress;
        boolean mProgressIndeterminate;
        int mProgressMax;
        android.app.Notification mPublicVersion;
        @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
        public java.lang.CharSequence[] mRemoteInputHistory;
        boolean mShowWhen = true;
        java.lang.String mSortKey;
        @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
        public android.support.v4.app.NotificationCompat.Style mStyle;
        @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
        public java.lang.CharSequence mSubText;
        android.widget.RemoteViews mTickerView;
        @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
        public boolean mUseChronometer;
        int mVisibility = 0;

        public Builder(android.content.Context context) {
            this.mContext = context;
            this.mNotification.when = java.lang.System.currentTimeMillis();
            this.mNotification.audioStreamType = -1;
            this.mPriority = 0;
            this.mPeople = new java.util.ArrayList<>();
        }

        public android.support.v4.app.NotificationCompat.Builder setWhen(long when) {
            this.mNotification.when = when;
            return this;
        }

        public android.support.v4.app.NotificationCompat.Builder setShowWhen(boolean show) {
            this.mShowWhen = show;
            return this;
        }

        public android.support.v4.app.NotificationCompat.Builder setUsesChronometer(boolean b) {
            this.mUseChronometer = b;
            return this;
        }

        public android.support.v4.app.NotificationCompat.Builder setSmallIcon(int icon) {
            this.mNotification.icon = icon;
            return this;
        }

        public android.support.v4.app.NotificationCompat.Builder setSmallIcon(int icon, int level) {
            this.mNotification.icon = icon;
            this.mNotification.iconLevel = level;
            return this;
        }

        public android.support.v4.app.NotificationCompat.Builder setContentTitle(java.lang.CharSequence title) {
            this.mContentTitle = limitCharSequenceLength(title);
            return this;
        }

        public android.support.v4.app.NotificationCompat.Builder setContentText(java.lang.CharSequence text) {
            this.mContentText = limitCharSequenceLength(text);
            return this;
        }

        public android.support.v4.app.NotificationCompat.Builder setSubText(java.lang.CharSequence text) {
            this.mSubText = limitCharSequenceLength(text);
            return this;
        }

        public android.support.v4.app.NotificationCompat.Builder setRemoteInputHistory(java.lang.CharSequence[] text) {
            this.mRemoteInputHistory = text;
            return this;
        }

        public android.support.v4.app.NotificationCompat.Builder setNumber(int number) {
            this.mNumber = number;
            return this;
        }

        public android.support.v4.app.NotificationCompat.Builder setContentInfo(java.lang.CharSequence info) {
            this.mContentInfo = limitCharSequenceLength(info);
            return this;
        }

        public android.support.v4.app.NotificationCompat.Builder setProgress(int max, int progress, boolean indeterminate) {
            this.mProgressMax = max;
            this.mProgress = progress;
            this.mProgressIndeterminate = indeterminate;
            return this;
        }

        public android.support.v4.app.NotificationCompat.Builder setContent(android.widget.RemoteViews views) {
            this.mNotification.contentView = views;
            return this;
        }

        public android.support.v4.app.NotificationCompat.Builder setContentIntent(android.app.PendingIntent intent) {
            this.mContentIntent = intent;
            return this;
        }

        public android.support.v4.app.NotificationCompat.Builder setDeleteIntent(android.app.PendingIntent intent) {
            this.mNotification.deleteIntent = intent;
            return this;
        }

        public android.support.v4.app.NotificationCompat.Builder setFullScreenIntent(android.app.PendingIntent intent, boolean highPriority) {
            this.mFullScreenIntent = intent;
            setFlag(128, highPriority);
            return this;
        }

        public android.support.v4.app.NotificationCompat.Builder setTicker(java.lang.CharSequence tickerText) {
            this.mNotification.tickerText = limitCharSequenceLength(tickerText);
            return this;
        }

        public android.support.v4.app.NotificationCompat.Builder setTicker(java.lang.CharSequence tickerText, android.widget.RemoteViews views) {
            this.mNotification.tickerText = limitCharSequenceLength(tickerText);
            this.mTickerView = views;
            return this;
        }

        public android.support.v4.app.NotificationCompat.Builder setLargeIcon(android.graphics.Bitmap icon) {
            this.mLargeIcon = icon;
            return this;
        }

        public android.support.v4.app.NotificationCompat.Builder setSound(android.net.Uri sound) {
            this.mNotification.sound = sound;
            this.mNotification.audioStreamType = -1;
            return this;
        }

        public android.support.v4.app.NotificationCompat.Builder setSound(android.net.Uri sound, int streamType) {
            this.mNotification.sound = sound;
            this.mNotification.audioStreamType = streamType;
            return this;
        }

        public android.support.v4.app.NotificationCompat.Builder setVibrate(long[] pattern) {
            this.mNotification.vibrate = pattern;
            return this;
        }

        public android.support.v4.app.NotificationCompat.Builder setLights(@android.support.annotation.ColorInt int argb, int onMs, int offMs) {
            boolean showLights;
            int i = 1;
            this.mNotification.ledARGB = argb;
            this.mNotification.ledOnMS = onMs;
            this.mNotification.ledOffMS = offMs;
            if (this.mNotification.ledOnMS == 0 || this.mNotification.ledOffMS == 0) {
                showLights = false;
            } else {
                showLights = true;
            }
            android.app.Notification notification = this.mNotification;
            int i2 = this.mNotification.flags & -2;
            if (!showLights) {
                i = 0;
            }
            notification.flags = i | i2;
            return this;
        }

        public android.support.v4.app.NotificationCompat.Builder setOngoing(boolean ongoing) {
            setFlag(2, ongoing);
            return this;
        }

        public android.support.v4.app.NotificationCompat.Builder setOnlyAlertOnce(boolean onlyAlertOnce) {
            setFlag(8, onlyAlertOnce);
            return this;
        }

        public android.support.v4.app.NotificationCompat.Builder setAutoCancel(boolean autoCancel) {
            setFlag(16, autoCancel);
            return this;
        }

        public android.support.v4.app.NotificationCompat.Builder setLocalOnly(boolean b) {
            this.mLocalOnly = b;
            return this;
        }

        public android.support.v4.app.NotificationCompat.Builder setCategory(java.lang.String category) {
            this.mCategory = category;
            return this;
        }

        public android.support.v4.app.NotificationCompat.Builder setDefaults(int defaults) {
            this.mNotification.defaults = defaults;
            if ((defaults & 4) != 0) {
                this.mNotification.flags |= 1;
            }
            return this;
        }

        private void setFlag(int mask, boolean value) {
            if (value) {
                this.mNotification.flags |= mask;
                return;
            }
            this.mNotification.flags &= mask ^ -1;
        }

        public android.support.v4.app.NotificationCompat.Builder setPriority(int pri) {
            this.mPriority = pri;
            return this;
        }

        public android.support.v4.app.NotificationCompat.Builder addPerson(java.lang.String uri) {
            this.mPeople.add(uri);
            return this;
        }

        public android.support.v4.app.NotificationCompat.Builder setGroup(java.lang.String groupKey) {
            this.mGroupKey = groupKey;
            return this;
        }

        public android.support.v4.app.NotificationCompat.Builder setGroupSummary(boolean isGroupSummary) {
            this.mGroupSummary = isGroupSummary;
            return this;
        }

        public android.support.v4.app.NotificationCompat.Builder setSortKey(java.lang.String sortKey) {
            this.mSortKey = sortKey;
            return this;
        }

        public android.support.v4.app.NotificationCompat.Builder addExtras(android.os.Bundle extras) {
            if (extras != null) {
                if (this.mExtras == null) {
                    this.mExtras = new android.os.Bundle(extras);
                } else {
                    this.mExtras.putAll(extras);
                }
            }
            return this;
        }

        public android.support.v4.app.NotificationCompat.Builder setExtras(android.os.Bundle extras) {
            this.mExtras = extras;
            return this;
        }

        public android.os.Bundle getExtras() {
            if (this.mExtras == null) {
                this.mExtras = new android.os.Bundle();
            }
            return this.mExtras;
        }

        public android.support.v4.app.NotificationCompat.Builder addAction(int icon, java.lang.CharSequence title, android.app.PendingIntent intent) {
            this.mActions.add(new android.support.v4.app.NotificationCompat.Action(icon, title, intent));
            return this;
        }

        public android.support.v4.app.NotificationCompat.Builder addAction(android.support.v4.app.NotificationCompat.Action action) {
            this.mActions.add(action);
            return this;
        }

        public android.support.v4.app.NotificationCompat.Builder setStyle(android.support.v4.app.NotificationCompat.Style style) {
            if (this.mStyle != style) {
                this.mStyle = style;
                if (this.mStyle != null) {
                    this.mStyle.setBuilder(this);
                }
            }
            return this;
        }

        public android.support.v4.app.NotificationCompat.Builder setColor(@android.support.annotation.ColorInt int argb) {
            this.mColor = argb;
            return this;
        }

        public android.support.v4.app.NotificationCompat.Builder setVisibility(int visibility) {
            this.mVisibility = visibility;
            return this;
        }

        public android.support.v4.app.NotificationCompat.Builder setPublicVersion(android.app.Notification n) {
            this.mPublicVersion = n;
            return this;
        }

        public android.support.v4.app.NotificationCompat.Builder setCustomContentView(android.widget.RemoteViews contentView) {
            this.mContentView = contentView;
            return this;
        }

        public android.support.v4.app.NotificationCompat.Builder setCustomBigContentView(android.widget.RemoteViews contentView) {
            this.mBigContentView = contentView;
            return this;
        }

        public android.support.v4.app.NotificationCompat.Builder setCustomHeadsUpContentView(android.widget.RemoteViews contentView) {
            this.mHeadsUpContentView = contentView;
            return this;
        }

        public android.support.v4.app.NotificationCompat.Builder extend(android.support.v4.app.NotificationCompat.Extender extender) {
            extender.extend(this);
            return this;
        }

        @java.lang.Deprecated
        public android.app.Notification getNotification() {
            return build();
        }

        public android.app.Notification build() {
            return android.support.v4.app.NotificationCompat.IMPL.build(this, getExtender());
        }

        /* access modifiers changed from: protected */
        @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
        public android.support.v4.app.NotificationCompat.BuilderExtender getExtender() {
            return new android.support.v4.app.NotificationCompat.BuilderExtender();
        }

        protected static java.lang.CharSequence limitCharSequenceLength(java.lang.CharSequence cs) {
            if (cs != null && cs.length() > 5120) {
                return cs.subSequence(0, 5120);
            }
            return cs;
        }

        @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
        public android.widget.RemoteViews getContentView() {
            return this.mContentView;
        }

        @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
        public android.widget.RemoteViews getBigContentView() {
            return this.mBigContentView;
        }

        @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
        public android.widget.RemoteViews getHeadsUpContentView() {
            return this.mHeadsUpContentView;
        }

        @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
        public long getWhenIfShowing() {
            if (this.mShowWhen) {
                return this.mNotification.when;
            }
            return 0;
        }

        @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
        public int getPriority() {
            return this.mPriority;
        }

        @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
        public int getColor() {
            return this.mColor;
        }

        /* access modifiers changed from: protected */
        @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
        public java.lang.CharSequence resolveText() {
            return this.mContentText;
        }

        /* access modifiers changed from: protected */
        @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
        public java.lang.CharSequence resolveTitle() {
            return this.mContentTitle;
        }
    }

    @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
    protected static class BuilderExtender {
        protected BuilderExtender() {
        }

        public android.app.Notification build(android.support.v4.app.NotificationCompat.Builder b, android.support.v4.app.NotificationBuilderWithBuilderAccessor builder) {
            android.app.Notification n = builder.build();
            if (b.mContentView != null) {
                n.contentView = b.mContentView;
            }
            return n;
        }
    }

    public static final class CarExtender implements android.support.v4.app.NotificationCompat.Extender {
        private static final java.lang.String EXTRA_CAR_EXTENDER = "android.car.EXTENSIONS";
        private static final java.lang.String EXTRA_COLOR = "app_color";
        private static final java.lang.String EXTRA_CONVERSATION = "car_conversation";
        private static final java.lang.String EXTRA_LARGE_ICON = "large_icon";
        private static final java.lang.String TAG = "CarExtender";
        private int mColor = 0;
        private android.graphics.Bitmap mLargeIcon;
        private android.support.v4.app.NotificationCompat.CarExtender.UnreadConversation mUnreadConversation;

        public static class UnreadConversation extends android.support.v4.app.NotificationCompatBase.UnreadConversation {
            static final android.support.v4.app.NotificationCompatBase.UnreadConversation.Factory FACTORY = new android.support.v4.app.NotificationCompatBase.UnreadConversation.Factory() {
                public android.support.v4.app.NotificationCompat.CarExtender.UnreadConversation build(java.lang.String[] messages, android.support.v4.app.RemoteInputCompatBase.RemoteInput remoteInput, android.app.PendingIntent replyPendingIntent, android.app.PendingIntent readPendingIntent, java.lang.String[] participants, long latestTimestamp) {
                    return new android.support.v4.app.NotificationCompat.CarExtender.UnreadConversation(messages, (android.support.v4.app.RemoteInput) remoteInput, replyPendingIntent, readPendingIntent, participants, latestTimestamp);
                }
            };
            private final long mLatestTimestamp;
            private final java.lang.String[] mMessages;
            private final java.lang.String[] mParticipants;
            private final android.app.PendingIntent mReadPendingIntent;
            private final android.support.v4.app.RemoteInput mRemoteInput;
            private final android.app.PendingIntent mReplyPendingIntent;

            public static class Builder {
                private long mLatestTimestamp;
                private final java.util.List<java.lang.String> mMessages = new java.util.ArrayList();
                private final java.lang.String mParticipant;
                private android.app.PendingIntent mReadPendingIntent;
                private android.support.v4.app.RemoteInput mRemoteInput;
                private android.app.PendingIntent mReplyPendingIntent;

                public Builder(java.lang.String name) {
                    this.mParticipant = name;
                }

                public android.support.v4.app.NotificationCompat.CarExtender.UnreadConversation.Builder addMessage(java.lang.String message) {
                    this.mMessages.add(message);
                    return this;
                }

                public android.support.v4.app.NotificationCompat.CarExtender.UnreadConversation.Builder setReplyAction(android.app.PendingIntent pendingIntent, android.support.v4.app.RemoteInput remoteInput) {
                    this.mRemoteInput = remoteInput;
                    this.mReplyPendingIntent = pendingIntent;
                    return this;
                }

                public android.support.v4.app.NotificationCompat.CarExtender.UnreadConversation.Builder setReadPendingIntent(android.app.PendingIntent pendingIntent) {
                    this.mReadPendingIntent = pendingIntent;
                    return this;
                }

                public android.support.v4.app.NotificationCompat.CarExtender.UnreadConversation.Builder setLatestTimestamp(long timestamp) {
                    this.mLatestTimestamp = timestamp;
                    return this;
                }

                public android.support.v4.app.NotificationCompat.CarExtender.UnreadConversation build() {
                    return new android.support.v4.app.NotificationCompat.CarExtender.UnreadConversation((java.lang.String[]) this.mMessages.toArray(new java.lang.String[this.mMessages.size()]), this.mRemoteInput, this.mReplyPendingIntent, this.mReadPendingIntent, new java.lang.String[]{this.mParticipant}, this.mLatestTimestamp);
                }
            }

            UnreadConversation(java.lang.String[] messages, android.support.v4.app.RemoteInput remoteInput, android.app.PendingIntent replyPendingIntent, android.app.PendingIntent readPendingIntent, java.lang.String[] participants, long latestTimestamp) {
                this.mMessages = messages;
                this.mRemoteInput = remoteInput;
                this.mReadPendingIntent = readPendingIntent;
                this.mReplyPendingIntent = replyPendingIntent;
                this.mParticipants = participants;
                this.mLatestTimestamp = latestTimestamp;
            }

            public java.lang.String[] getMessages() {
                return this.mMessages;
            }

            public android.support.v4.app.RemoteInput getRemoteInput() {
                return this.mRemoteInput;
            }

            public android.app.PendingIntent getReplyPendingIntent() {
                return this.mReplyPendingIntent;
            }

            public android.app.PendingIntent getReadPendingIntent() {
                return this.mReadPendingIntent;
            }

            public java.lang.String[] getParticipants() {
                return this.mParticipants;
            }

            public java.lang.String getParticipant() {
                if (this.mParticipants.length > 0) {
                    return this.mParticipants[0];
                }
                return null;
            }

            public long getLatestTimestamp() {
                return this.mLatestTimestamp;
            }
        }

        public CarExtender() {
        }

        public CarExtender(android.app.Notification notif) {
            android.os.Bundle carBundle;
            if (android.os.Build.VERSION.SDK_INT >= 21) {
                if (android.support.v4.app.NotificationCompat.getExtras(notif) == null) {
                    carBundle = null;
                } else {
                    carBundle = android.support.v4.app.NotificationCompat.getExtras(notif).getBundle(EXTRA_CAR_EXTENDER);
                }
                if (carBundle != null) {
                    this.mLargeIcon = (android.graphics.Bitmap) carBundle.getParcelable(EXTRA_LARGE_ICON);
                    this.mColor = carBundle.getInt(EXTRA_COLOR, 0);
                    this.mUnreadConversation = (android.support.v4.app.NotificationCompat.CarExtender.UnreadConversation) android.support.v4.app.NotificationCompat.IMPL.getUnreadConversationFromBundle(carBundle.getBundle(EXTRA_CONVERSATION), android.support.v4.app.NotificationCompat.CarExtender.UnreadConversation.FACTORY, android.support.v4.app.RemoteInput.FACTORY);
                }
            }
        }

        public android.support.v4.app.NotificationCompat.Builder extend(android.support.v4.app.NotificationCompat.Builder builder) {
            if (android.os.Build.VERSION.SDK_INT >= 21) {
                android.os.Bundle carExtensions = new android.os.Bundle();
                if (this.mLargeIcon != null) {
                    carExtensions.putParcelable(EXTRA_LARGE_ICON, this.mLargeIcon);
                }
                if (this.mColor != 0) {
                    carExtensions.putInt(EXTRA_COLOR, this.mColor);
                }
                if (this.mUnreadConversation != null) {
                    carExtensions.putBundle(EXTRA_CONVERSATION, android.support.v4.app.NotificationCompat.IMPL.getBundleForUnreadConversation(this.mUnreadConversation));
                }
                builder.getExtras().putBundle(EXTRA_CAR_EXTENDER, carExtensions);
            }
            return builder;
        }

        public android.support.v4.app.NotificationCompat.CarExtender setColor(@android.support.annotation.ColorInt int color) {
            this.mColor = color;
            return this;
        }

        @android.support.annotation.ColorInt
        public int getColor() {
            return this.mColor;
        }

        public android.support.v4.app.NotificationCompat.CarExtender setLargeIcon(android.graphics.Bitmap largeIcon) {
            this.mLargeIcon = largeIcon;
            return this;
        }

        public android.graphics.Bitmap getLargeIcon() {
            return this.mLargeIcon;
        }

        public android.support.v4.app.NotificationCompat.CarExtender setUnreadConversation(android.support.v4.app.NotificationCompat.CarExtender.UnreadConversation unreadConversation) {
            this.mUnreadConversation = unreadConversation;
            return this;
        }

        public android.support.v4.app.NotificationCompat.CarExtender.UnreadConversation getUnreadConversation() {
            return this.mUnreadConversation;
        }
    }

    public interface Extender {
        android.support.v4.app.NotificationCompat.Builder extend(android.support.v4.app.NotificationCompat.Builder builder);
    }

    public static class InboxStyle extends android.support.v4.app.NotificationCompat.Style {
        java.util.ArrayList<java.lang.CharSequence> mTexts = new java.util.ArrayList<>();

        public InboxStyle() {
        }

        public InboxStyle(android.support.v4.app.NotificationCompat.Builder builder) {
            setBuilder(builder);
        }

        public android.support.v4.app.NotificationCompat.InboxStyle setBigContentTitle(java.lang.CharSequence title) {
            this.mBigContentTitle = android.support.v4.app.NotificationCompat.Builder.limitCharSequenceLength(title);
            return this;
        }

        public android.support.v4.app.NotificationCompat.InboxStyle setSummaryText(java.lang.CharSequence cs) {
            this.mSummaryText = android.support.v4.app.NotificationCompat.Builder.limitCharSequenceLength(cs);
            this.mSummaryTextSet = true;
            return this;
        }

        public android.support.v4.app.NotificationCompat.InboxStyle addLine(java.lang.CharSequence cs) {
            this.mTexts.add(android.support.v4.app.NotificationCompat.Builder.limitCharSequenceLength(cs));
            return this;
        }
    }

    public static class MessagingStyle extends android.support.v4.app.NotificationCompat.Style {
        public static final int MAXIMUM_RETAINED_MESSAGES = 25;
        java.lang.CharSequence mConversationTitle;
        java.util.List<android.support.v4.app.NotificationCompat.MessagingStyle.Message> mMessages = new java.util.ArrayList();
        java.lang.CharSequence mUserDisplayName;

        public static final class Message {
            static final java.lang.String KEY_DATA_MIME_TYPE = "type";
            static final java.lang.String KEY_DATA_URI = "uri";
            static final java.lang.String KEY_SENDER = "sender";
            static final java.lang.String KEY_TEXT = "text";
            static final java.lang.String KEY_TIMESTAMP = "time";
            private java.lang.String mDataMimeType;
            private android.net.Uri mDataUri;
            private final java.lang.CharSequence mSender;
            private final java.lang.CharSequence mText;
            private final long mTimestamp;

            public Message(java.lang.CharSequence text, long timestamp, java.lang.CharSequence sender) {
                this.mText = text;
                this.mTimestamp = timestamp;
                this.mSender = sender;
            }

            public android.support.v4.app.NotificationCompat.MessagingStyle.Message setData(java.lang.String dataMimeType, android.net.Uri dataUri) {
                this.mDataMimeType = dataMimeType;
                this.mDataUri = dataUri;
                return this;
            }

            public java.lang.CharSequence getText() {
                return this.mText;
            }

            public long getTimestamp() {
                return this.mTimestamp;
            }

            public java.lang.CharSequence getSender() {
                return this.mSender;
            }

            public java.lang.String getDataMimeType() {
                return this.mDataMimeType;
            }

            public android.net.Uri getDataUri() {
                return this.mDataUri;
            }

            private android.os.Bundle toBundle() {
                android.os.Bundle bundle = new android.os.Bundle();
                if (this.mText != null) {
                    bundle.putCharSequence(KEY_TEXT, this.mText);
                }
                bundle.putLong(KEY_TIMESTAMP, this.mTimestamp);
                if (this.mSender != null) {
                    bundle.putCharSequence(KEY_SENDER, this.mSender);
                }
                if (this.mDataMimeType != null) {
                    bundle.putString(KEY_DATA_MIME_TYPE, this.mDataMimeType);
                }
                if (this.mDataUri != null) {
                    bundle.putParcelable(KEY_DATA_URI, this.mDataUri);
                }
                return bundle;
            }

            static android.os.Bundle[] getBundleArrayForMessages(java.util.List<android.support.v4.app.NotificationCompat.MessagingStyle.Message> messages) {
                android.os.Bundle[] bundles = new android.os.Bundle[messages.size()];
                int N = messages.size();
                for (int i = 0; i < N; i++) {
                    bundles[i] = ((android.support.v4.app.NotificationCompat.MessagingStyle.Message) messages.get(i)).toBundle();
                }
                return bundles;
            }

            static java.util.List<android.support.v4.app.NotificationCompat.MessagingStyle.Message> getMessagesFromBundleArray(android.os.Parcelable[] bundles) {
                java.util.List<android.support.v4.app.NotificationCompat.MessagingStyle.Message> messages = new java.util.ArrayList<>(bundles.length);
                for (int i = 0; i < bundles.length; i++) {
                    if (bundles[i] instanceof android.os.Bundle) {
                        android.support.v4.app.NotificationCompat.MessagingStyle.Message message = getMessageFromBundle(bundles[i]);
                        if (message != null) {
                            messages.add(message);
                        }
                    }
                }
                return messages;
            }

            static android.support.v4.app.NotificationCompat.MessagingStyle.Message getMessageFromBundle(android.os.Bundle bundle) {
                try {
                    if (!bundle.containsKey(KEY_TEXT) || !bundle.containsKey(KEY_TIMESTAMP)) {
                        return null;
                    }
                    android.support.v4.app.NotificationCompat.MessagingStyle.Message message = new android.support.v4.app.NotificationCompat.MessagingStyle.Message(bundle.getCharSequence(KEY_TEXT), bundle.getLong(KEY_TIMESTAMP), bundle.getCharSequence(KEY_SENDER));
                    if (!bundle.containsKey(KEY_DATA_MIME_TYPE) || !bundle.containsKey(KEY_DATA_URI)) {
                        return message;
                    }
                    message.setData(bundle.getString(KEY_DATA_MIME_TYPE), (android.net.Uri) bundle.getParcelable(KEY_DATA_URI));
                    return message;
                } catch (java.lang.ClassCastException e) {
                    return null;
                }
            }
        }

        MessagingStyle() {
        }

        public MessagingStyle(@android.support.annotation.NonNull java.lang.CharSequence userDisplayName) {
            this.mUserDisplayName = userDisplayName;
        }

        public java.lang.CharSequence getUserDisplayName() {
            return this.mUserDisplayName;
        }

        public android.support.v4.app.NotificationCompat.MessagingStyle setConversationTitle(java.lang.CharSequence conversationTitle) {
            this.mConversationTitle = conversationTitle;
            return this;
        }

        public java.lang.CharSequence getConversationTitle() {
            return this.mConversationTitle;
        }

        public android.support.v4.app.NotificationCompat.MessagingStyle addMessage(java.lang.CharSequence text, long timestamp, java.lang.CharSequence sender) {
            this.mMessages.add(new android.support.v4.app.NotificationCompat.MessagingStyle.Message(text, timestamp, sender));
            if (this.mMessages.size() > 25) {
                this.mMessages.remove(0);
            }
            return this;
        }

        public android.support.v4.app.NotificationCompat.MessagingStyle addMessage(android.support.v4.app.NotificationCompat.MessagingStyle.Message message) {
            this.mMessages.add(message);
            if (this.mMessages.size() > 25) {
                this.mMessages.remove(0);
            }
            return this;
        }

        public java.util.List<android.support.v4.app.NotificationCompat.MessagingStyle.Message> getMessages() {
            return this.mMessages;
        }

        public static android.support.v4.app.NotificationCompat.MessagingStyle extractMessagingStyleFromNotification(android.app.Notification notif) {
            android.os.Bundle extras = android.support.v4.app.NotificationCompat.IMPL.getExtras(notif);
            if (extras != null && !extras.containsKey(android.support.v4.app.NotificationCompat.EXTRA_SELF_DISPLAY_NAME)) {
                return null;
            }
            try {
                android.support.v4.app.NotificationCompat.MessagingStyle style = new android.support.v4.app.NotificationCompat.MessagingStyle();
                style.restoreFromCompatExtras(extras);
                return style;
            } catch (java.lang.ClassCastException e) {
                return null;
            }
        }

        public void addCompatExtras(android.os.Bundle extras) {
            super.addCompatExtras(extras);
            if (this.mUserDisplayName != null) {
                extras.putCharSequence(android.support.v4.app.NotificationCompat.EXTRA_SELF_DISPLAY_NAME, this.mUserDisplayName);
            }
            if (this.mConversationTitle != null) {
                extras.putCharSequence(android.support.v4.app.NotificationCompat.EXTRA_CONVERSATION_TITLE, this.mConversationTitle);
            }
            if (!this.mMessages.isEmpty()) {
                extras.putParcelableArray(android.support.v4.app.NotificationCompat.EXTRA_MESSAGES, android.support.v4.app.NotificationCompat.MessagingStyle.Message.getBundleArrayForMessages(this.mMessages));
            }
        }

        /* access modifiers changed from: protected */
        @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
        public void restoreFromCompatExtras(android.os.Bundle extras) {
            this.mMessages.clear();
            this.mUserDisplayName = extras.getString(android.support.v4.app.NotificationCompat.EXTRA_SELF_DISPLAY_NAME);
            this.mConversationTitle = extras.getString(android.support.v4.app.NotificationCompat.EXTRA_CONVERSATION_TITLE);
            android.os.Parcelable[] parcelables = extras.getParcelableArray(android.support.v4.app.NotificationCompat.EXTRA_MESSAGES);
            if (parcelables != null) {
                this.mMessages = android.support.v4.app.NotificationCompat.MessagingStyle.Message.getMessagesFromBundleArray(parcelables);
            }
        }
    }

    interface NotificationCompatImpl {
        android.app.Notification build(android.support.v4.app.NotificationCompat.Builder builder, android.support.v4.app.NotificationCompat.BuilderExtender builderExtender);

        android.support.v4.app.NotificationCompat.Action getAction(android.app.Notification notification, int i);

        int getActionCount(android.app.Notification notification);

        android.support.v4.app.NotificationCompat.Action[] getActionsFromParcelableArrayList(java.util.ArrayList<android.os.Parcelable> arrayList);

        android.os.Bundle getBundleForUnreadConversation(android.support.v4.app.NotificationCompatBase.UnreadConversation unreadConversation);

        java.lang.String getCategory(android.app.Notification notification);

        android.os.Bundle getExtras(android.app.Notification notification);

        java.lang.String getGroup(android.app.Notification notification);

        boolean getLocalOnly(android.app.Notification notification);

        java.util.ArrayList<android.os.Parcelable> getParcelableArrayListForActions(android.support.v4.app.NotificationCompat.Action[] actionArr);

        java.lang.String getSortKey(android.app.Notification notification);

        android.support.v4.app.NotificationCompatBase.UnreadConversation getUnreadConversationFromBundle(android.os.Bundle bundle, android.support.v4.app.NotificationCompatBase.UnreadConversation.Factory factory, android.support.v4.app.RemoteInputCompatBase.RemoteInput.Factory factory2);

        boolean isGroupSummary(android.app.Notification notification);
    }

    static class NotificationCompatImplApi20 extends android.support.v4.app.NotificationCompat.NotificationCompatImplKitKat {
        NotificationCompatImplApi20() {
        }

        public android.app.Notification build(android.support.v4.app.NotificationCompat.Builder b, android.support.v4.app.NotificationCompat.BuilderExtender extender) {
            android.support.v4.app.NotificationCompatApi20.Builder builder = new android.support.v4.app.NotificationCompatApi20.Builder(b.mContext, b.mNotification, b.resolveTitle(), b.resolveText(), b.mContentInfo, b.mTickerView, b.mNumber, b.mContentIntent, b.mFullScreenIntent, b.mLargeIcon, b.mProgressMax, b.mProgress, b.mProgressIndeterminate, b.mShowWhen, b.mUseChronometer, b.mPriority, b.mSubText, b.mLocalOnly, b.mPeople, b.mExtras, b.mGroupKey, b.mGroupSummary, b.mSortKey, b.mContentView, b.mBigContentView);
            android.support.v4.app.NotificationCompat.addActionsToBuilder(builder, b.mActions);
            android.support.v4.app.NotificationCompat.addStyleToBuilderJellybean(builder, b.mStyle);
            android.app.Notification notification = extender.build(b, builder);
            if (b.mStyle != null) {
                b.mStyle.addCompatExtras(getExtras(notification));
            }
            return notification;
        }

        public android.support.v4.app.NotificationCompat.Action getAction(android.app.Notification n, int actionIndex) {
            return (android.support.v4.app.NotificationCompat.Action) android.support.v4.app.NotificationCompatApi20.getAction(n, actionIndex, android.support.v4.app.NotificationCompat.Action.FACTORY, android.support.v4.app.RemoteInput.FACTORY);
        }

        public android.support.v4.app.NotificationCompat.Action[] getActionsFromParcelableArrayList(java.util.ArrayList<android.os.Parcelable> parcelables) {
            return (android.support.v4.app.NotificationCompat.Action[]) android.support.v4.app.NotificationCompatApi20.getActionsFromParcelableArrayList(parcelables, android.support.v4.app.NotificationCompat.Action.FACTORY, android.support.v4.app.RemoteInput.FACTORY);
        }

        public java.util.ArrayList<android.os.Parcelable> getParcelableArrayListForActions(android.support.v4.app.NotificationCompat.Action[] actions) {
            return android.support.v4.app.NotificationCompatApi20.getParcelableArrayListForActions(actions);
        }

        public boolean getLocalOnly(android.app.Notification n) {
            return android.support.v4.app.NotificationCompatApi20.getLocalOnly(n);
        }

        public java.lang.String getGroup(android.app.Notification n) {
            return android.support.v4.app.NotificationCompatApi20.getGroup(n);
        }

        public boolean isGroupSummary(android.app.Notification n) {
            return android.support.v4.app.NotificationCompatApi20.isGroupSummary(n);
        }

        public java.lang.String getSortKey(android.app.Notification n) {
            return android.support.v4.app.NotificationCompatApi20.getSortKey(n);
        }
    }

    static class NotificationCompatImplApi21 extends android.support.v4.app.NotificationCompat.NotificationCompatImplApi20 {
        NotificationCompatImplApi21() {
        }

        public android.app.Notification build(android.support.v4.app.NotificationCompat.Builder b, android.support.v4.app.NotificationCompat.BuilderExtender extender) {
            android.support.v4.app.NotificationCompatApi21.Builder builder = new android.support.v4.app.NotificationCompatApi21.Builder(b.mContext, b.mNotification, b.resolveTitle(), b.resolveText(), b.mContentInfo, b.mTickerView, b.mNumber, b.mContentIntent, b.mFullScreenIntent, b.mLargeIcon, b.mProgressMax, b.mProgress, b.mProgressIndeterminate, b.mShowWhen, b.mUseChronometer, b.mPriority, b.mSubText, b.mLocalOnly, b.mCategory, b.mPeople, b.mExtras, b.mColor, b.mVisibility, b.mPublicVersion, b.mGroupKey, b.mGroupSummary, b.mSortKey, b.mContentView, b.mBigContentView, b.mHeadsUpContentView);
            android.support.v4.app.NotificationCompat.addActionsToBuilder(builder, b.mActions);
            android.support.v4.app.NotificationCompat.addStyleToBuilderJellybean(builder, b.mStyle);
            android.app.Notification notification = extender.build(b, builder);
            if (b.mStyle != null) {
                b.mStyle.addCompatExtras(getExtras(notification));
            }
            return notification;
        }

        public java.lang.String getCategory(android.app.Notification notif) {
            return android.support.v4.app.NotificationCompatApi21.getCategory(notif);
        }

        public android.os.Bundle getBundleForUnreadConversation(android.support.v4.app.NotificationCompatBase.UnreadConversation uc) {
            return android.support.v4.app.NotificationCompatApi21.getBundleForUnreadConversation(uc);
        }

        public android.support.v4.app.NotificationCompatBase.UnreadConversation getUnreadConversationFromBundle(android.os.Bundle b, android.support.v4.app.NotificationCompatBase.UnreadConversation.Factory factory, android.support.v4.app.RemoteInputCompatBase.RemoteInput.Factory remoteInputFactory) {
            return android.support.v4.app.NotificationCompatApi21.getUnreadConversationFromBundle(b, factory, remoteInputFactory);
        }
    }

    static class NotificationCompatImplApi24 extends android.support.v4.app.NotificationCompat.NotificationCompatImplApi21 {
        NotificationCompatImplApi24() {
        }

        public android.app.Notification build(android.support.v4.app.NotificationCompat.Builder b, android.support.v4.app.NotificationCompat.BuilderExtender extender) {
            android.support.v4.app.NotificationCompatApi24.Builder builder = new android.support.v4.app.NotificationCompatApi24.Builder(b.mContext, b.mNotification, b.mContentTitle, b.mContentText, b.mContentInfo, b.mTickerView, b.mNumber, b.mContentIntent, b.mFullScreenIntent, b.mLargeIcon, b.mProgressMax, b.mProgress, b.mProgressIndeterminate, b.mShowWhen, b.mUseChronometer, b.mPriority, b.mSubText, b.mLocalOnly, b.mCategory, b.mPeople, b.mExtras, b.mColor, b.mVisibility, b.mPublicVersion, b.mGroupKey, b.mGroupSummary, b.mSortKey, b.mRemoteInputHistory, b.mContentView, b.mBigContentView, b.mHeadsUpContentView);
            android.support.v4.app.NotificationCompat.addActionsToBuilder(builder, b.mActions);
            android.support.v4.app.NotificationCompat.addStyleToBuilderApi24(builder, b.mStyle);
            android.app.Notification notification = extender.build(b, builder);
            if (b.mStyle != null) {
                b.mStyle.addCompatExtras(getExtras(notification));
            }
            return notification;
        }
    }

    static class NotificationCompatImplBase implements android.support.v4.app.NotificationCompat.NotificationCompatImpl {
        NotificationCompatImplBase() {
        }

        public android.app.Notification build(android.support.v4.app.NotificationCompat.Builder b, android.support.v4.app.NotificationCompat.BuilderExtender extender) {
            android.app.Notification result = android.support.v4.app.NotificationCompatBase.add(b.mNotification, b.mContext, b.resolveTitle(), b.resolveText(), b.mContentIntent, b.mFullScreenIntent);
            if (b.mPriority > 0) {
                result.flags |= 128;
            }
            if (b.mContentView != null) {
                result.contentView = b.mContentView;
            }
            return result;
        }

        public android.os.Bundle getExtras(android.app.Notification n) {
            return null;
        }

        public int getActionCount(android.app.Notification n) {
            return 0;
        }

        public android.support.v4.app.NotificationCompat.Action getAction(android.app.Notification n, int actionIndex) {
            return null;
        }

        public android.support.v4.app.NotificationCompat.Action[] getActionsFromParcelableArrayList(java.util.ArrayList<android.os.Parcelable> arrayList) {
            return null;
        }

        public java.util.ArrayList<android.os.Parcelable> getParcelableArrayListForActions(android.support.v4.app.NotificationCompat.Action[] actions) {
            return null;
        }

        public java.lang.String getCategory(android.app.Notification n) {
            return null;
        }

        public boolean getLocalOnly(android.app.Notification n) {
            return false;
        }

        public java.lang.String getGroup(android.app.Notification n) {
            return null;
        }

        public boolean isGroupSummary(android.app.Notification n) {
            return false;
        }

        public java.lang.String getSortKey(android.app.Notification n) {
            return null;
        }

        public android.os.Bundle getBundleForUnreadConversation(android.support.v4.app.NotificationCompatBase.UnreadConversation uc) {
            return null;
        }

        public android.support.v4.app.NotificationCompatBase.UnreadConversation getUnreadConversationFromBundle(android.os.Bundle b, android.support.v4.app.NotificationCompatBase.UnreadConversation.Factory factory, android.support.v4.app.RemoteInputCompatBase.RemoteInput.Factory remoteInputFactory) {
            return null;
        }
    }

    static class NotificationCompatImplHoneycomb extends android.support.v4.app.NotificationCompat.NotificationCompatImplBase {
        NotificationCompatImplHoneycomb() {
        }

        public android.app.Notification build(android.support.v4.app.NotificationCompat.Builder b, android.support.v4.app.NotificationCompat.BuilderExtender extender) {
            android.app.Notification notification = android.support.v4.app.NotificationCompatHoneycomb.add(b.mContext, b.mNotification, b.resolveTitle(), b.resolveText(), b.mContentInfo, b.mTickerView, b.mNumber, b.mContentIntent, b.mFullScreenIntent, b.mLargeIcon);
            if (b.mContentView != null) {
                notification.contentView = b.mContentView;
            }
            return notification;
        }
    }

    static class NotificationCompatImplIceCreamSandwich extends android.support.v4.app.NotificationCompat.NotificationCompatImplBase {
        NotificationCompatImplIceCreamSandwich() {
        }

        public android.app.Notification build(android.support.v4.app.NotificationCompat.Builder b, android.support.v4.app.NotificationCompat.BuilderExtender extender) {
            return extender.build(b, new android.support.v4.app.NotificationCompatIceCreamSandwich.Builder(b.mContext, b.mNotification, b.resolveTitle(), b.resolveText(), b.mContentInfo, b.mTickerView, b.mNumber, b.mContentIntent, b.mFullScreenIntent, b.mLargeIcon, b.mProgressMax, b.mProgress, b.mProgressIndeterminate));
        }
    }

    static class NotificationCompatImplJellybean extends android.support.v4.app.NotificationCompat.NotificationCompatImplBase {
        NotificationCompatImplJellybean() {
        }

        public android.app.Notification build(android.support.v4.app.NotificationCompat.Builder b, android.support.v4.app.NotificationCompat.BuilderExtender extender) {
            android.support.v4.app.NotificationCompatJellybean.Builder builder = new android.support.v4.app.NotificationCompatJellybean.Builder(b.mContext, b.mNotification, b.resolveTitle(), b.resolveText(), b.mContentInfo, b.mTickerView, b.mNumber, b.mContentIntent, b.mFullScreenIntent, b.mLargeIcon, b.mProgressMax, b.mProgress, b.mProgressIndeterminate, b.mUseChronometer, b.mPriority, b.mSubText, b.mLocalOnly, b.mExtras, b.mGroupKey, b.mGroupSummary, b.mSortKey, b.mContentView, b.mBigContentView);
            android.support.v4.app.NotificationCompat.addActionsToBuilder(builder, b.mActions);
            android.support.v4.app.NotificationCompat.addStyleToBuilderJellybean(builder, b.mStyle);
            android.app.Notification notification = extender.build(b, builder);
            if (b.mStyle != null) {
                android.os.Bundle extras = getExtras(notification);
                if (extras != null) {
                    b.mStyle.addCompatExtras(extras);
                }
            }
            return notification;
        }

        public android.os.Bundle getExtras(android.app.Notification n) {
            return android.support.v4.app.NotificationCompatJellybean.getExtras(n);
        }

        public int getActionCount(android.app.Notification n) {
            return android.support.v4.app.NotificationCompatJellybean.getActionCount(n);
        }

        public android.support.v4.app.NotificationCompat.Action getAction(android.app.Notification n, int actionIndex) {
            return (android.support.v4.app.NotificationCompat.Action) android.support.v4.app.NotificationCompatJellybean.getAction(n, actionIndex, android.support.v4.app.NotificationCompat.Action.FACTORY, android.support.v4.app.RemoteInput.FACTORY);
        }

        public android.support.v4.app.NotificationCompat.Action[] getActionsFromParcelableArrayList(java.util.ArrayList<android.os.Parcelable> parcelables) {
            return (android.support.v4.app.NotificationCompat.Action[]) android.support.v4.app.NotificationCompatJellybean.getActionsFromParcelableArrayList(parcelables, android.support.v4.app.NotificationCompat.Action.FACTORY, android.support.v4.app.RemoteInput.FACTORY);
        }

        public java.util.ArrayList<android.os.Parcelable> getParcelableArrayListForActions(android.support.v4.app.NotificationCompat.Action[] actions) {
            return android.support.v4.app.NotificationCompatJellybean.getParcelableArrayListForActions(actions);
        }

        public boolean getLocalOnly(android.app.Notification n) {
            return android.support.v4.app.NotificationCompatJellybean.getLocalOnly(n);
        }

        public java.lang.String getGroup(android.app.Notification n) {
            return android.support.v4.app.NotificationCompatJellybean.getGroup(n);
        }

        public boolean isGroupSummary(android.app.Notification n) {
            return android.support.v4.app.NotificationCompatJellybean.isGroupSummary(n);
        }

        public java.lang.String getSortKey(android.app.Notification n) {
            return android.support.v4.app.NotificationCompatJellybean.getSortKey(n);
        }
    }

    static class NotificationCompatImplKitKat extends android.support.v4.app.NotificationCompat.NotificationCompatImplJellybean {
        NotificationCompatImplKitKat() {
        }

        public android.app.Notification build(android.support.v4.app.NotificationCompat.Builder b, android.support.v4.app.NotificationCompat.BuilderExtender extender) {
            android.support.v4.app.NotificationCompatKitKat.Builder builder = new android.support.v4.app.NotificationCompatKitKat.Builder(b.mContext, b.mNotification, b.resolveTitle(), b.resolveText(), b.mContentInfo, b.mTickerView, b.mNumber, b.mContentIntent, b.mFullScreenIntent, b.mLargeIcon, b.mProgressMax, b.mProgress, b.mProgressIndeterminate, b.mShowWhen, b.mUseChronometer, b.mPriority, b.mSubText, b.mLocalOnly, b.mPeople, b.mExtras, b.mGroupKey, b.mGroupSummary, b.mSortKey, b.mContentView, b.mBigContentView);
            android.support.v4.app.NotificationCompat.addActionsToBuilder(builder, b.mActions);
            android.support.v4.app.NotificationCompat.addStyleToBuilderJellybean(builder, b.mStyle);
            return extender.build(b, builder);
        }

        public android.os.Bundle getExtras(android.app.Notification n) {
            return android.support.v4.app.NotificationCompatKitKat.getExtras(n);
        }

        public int getActionCount(android.app.Notification n) {
            return android.support.v4.app.NotificationCompatKitKat.getActionCount(n);
        }

        public android.support.v4.app.NotificationCompat.Action getAction(android.app.Notification n, int actionIndex) {
            return (android.support.v4.app.NotificationCompat.Action) android.support.v4.app.NotificationCompatKitKat.getAction(n, actionIndex, android.support.v4.app.NotificationCompat.Action.FACTORY, android.support.v4.app.RemoteInput.FACTORY);
        }

        public boolean getLocalOnly(android.app.Notification n) {
            return android.support.v4.app.NotificationCompatKitKat.getLocalOnly(n);
        }

        public java.lang.String getGroup(android.app.Notification n) {
            return android.support.v4.app.NotificationCompatKitKat.getGroup(n);
        }

        public boolean isGroupSummary(android.app.Notification n) {
            return android.support.v4.app.NotificationCompatKitKat.isGroupSummary(n);
        }

        public java.lang.String getSortKey(android.app.Notification n) {
            return android.support.v4.app.NotificationCompatKitKat.getSortKey(n);
        }
    }

    @java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.SOURCE)
    public @interface NotificationVisibility {
    }

    public static abstract class Style {
        java.lang.CharSequence mBigContentTitle;
        android.support.v4.app.NotificationCompat.Builder mBuilder;
        java.lang.CharSequence mSummaryText;
        boolean mSummaryTextSet = false;

        public void setBuilder(android.support.v4.app.NotificationCompat.Builder builder) {
            if (this.mBuilder != builder) {
                this.mBuilder = builder;
                if (this.mBuilder != null) {
                    this.mBuilder.setStyle(this);
                }
            }
        }

        public android.app.Notification build() {
            if (this.mBuilder != null) {
                return this.mBuilder.build();
            }
            return null;
        }

        @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
        public void addCompatExtras(android.os.Bundle extras) {
        }

        /* access modifiers changed from: protected */
        @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
        public void restoreFromCompatExtras(android.os.Bundle extras) {
        }
    }

    public static final class WearableExtender implements android.support.v4.app.NotificationCompat.Extender {
        private static final int DEFAULT_CONTENT_ICON_GRAVITY = 8388613;
        private static final int DEFAULT_FLAGS = 1;
        private static final int DEFAULT_GRAVITY = 80;
        private static final java.lang.String EXTRA_WEARABLE_EXTENSIONS = "android.wearable.EXTENSIONS";
        private static final int FLAG_BIG_PICTURE_AMBIENT = 32;
        private static final int FLAG_CONTENT_INTENT_AVAILABLE_OFFLINE = 1;
        private static final int FLAG_HINT_AVOID_BACKGROUND_CLIPPING = 16;
        private static final int FLAG_HINT_CONTENT_INTENT_LAUNCHES_ACTIVITY = 64;
        private static final int FLAG_HINT_HIDE_ICON = 2;
        private static final int FLAG_HINT_SHOW_BACKGROUND_ONLY = 4;
        private static final int FLAG_START_SCROLL_BOTTOM = 8;
        private static final java.lang.String KEY_ACTIONS = "actions";
        private static final java.lang.String KEY_BACKGROUND = "background";
        private static final java.lang.String KEY_BRIDGE_TAG = "bridgeTag";
        private static final java.lang.String KEY_CONTENT_ACTION_INDEX = "contentActionIndex";
        private static final java.lang.String KEY_CONTENT_ICON = "contentIcon";
        private static final java.lang.String KEY_CONTENT_ICON_GRAVITY = "contentIconGravity";
        private static final java.lang.String KEY_CUSTOM_CONTENT_HEIGHT = "customContentHeight";
        private static final java.lang.String KEY_CUSTOM_SIZE_PRESET = "customSizePreset";
        private static final java.lang.String KEY_DISMISSAL_ID = "dismissalId";
        private static final java.lang.String KEY_DISPLAY_INTENT = "displayIntent";
        private static final java.lang.String KEY_FLAGS = "flags";
        private static final java.lang.String KEY_GRAVITY = "gravity";
        private static final java.lang.String KEY_HINT_SCREEN_TIMEOUT = "hintScreenTimeout";
        private static final java.lang.String KEY_PAGES = "pages";
        public static final int SCREEN_TIMEOUT_LONG = -1;
        public static final int SCREEN_TIMEOUT_SHORT = 0;
        public static final int SIZE_DEFAULT = 0;
        public static final int SIZE_FULL_SCREEN = 5;
        public static final int SIZE_LARGE = 4;
        public static final int SIZE_MEDIUM = 3;
        public static final int SIZE_SMALL = 2;
        public static final int SIZE_XSMALL = 1;
        public static final int UNSET_ACTION_INDEX = -1;
        private java.util.ArrayList<android.support.v4.app.NotificationCompat.Action> mActions = new java.util.ArrayList<>();
        private android.graphics.Bitmap mBackground;
        private java.lang.String mBridgeTag;
        private int mContentActionIndex = -1;
        private int mContentIcon;
        private int mContentIconGravity = 8388613;
        private int mCustomContentHeight;
        private int mCustomSizePreset = 0;
        private java.lang.String mDismissalId;
        private android.app.PendingIntent mDisplayIntent;
        private int mFlags = 1;
        private int mGravity = 80;
        private int mHintScreenTimeout;
        private java.util.ArrayList<android.app.Notification> mPages = new java.util.ArrayList<>();

        public WearableExtender() {
        }

        public WearableExtender(android.app.Notification notif) {
            android.os.Bundle extras = android.support.v4.app.NotificationCompat.getExtras(notif);
            android.os.Bundle wearableBundle = extras != null ? extras.getBundle(EXTRA_WEARABLE_EXTENSIONS) : null;
            if (wearableBundle != null) {
                android.support.v4.app.NotificationCompat.Action[] actions = android.support.v4.app.NotificationCompat.IMPL.getActionsFromParcelableArrayList(wearableBundle.getParcelableArrayList(KEY_ACTIONS));
                if (actions != null) {
                    java.util.Collections.addAll(this.mActions, actions);
                }
                this.mFlags = wearableBundle.getInt(KEY_FLAGS, 1);
                this.mDisplayIntent = (android.app.PendingIntent) wearableBundle.getParcelable(KEY_DISPLAY_INTENT);
                android.app.Notification[] pages = android.support.v4.app.NotificationCompat.getNotificationArrayFromBundle(wearableBundle, KEY_PAGES);
                if (pages != null) {
                    java.util.Collections.addAll(this.mPages, pages);
                }
                this.mBackground = (android.graphics.Bitmap) wearableBundle.getParcelable(KEY_BACKGROUND);
                this.mContentIcon = wearableBundle.getInt(KEY_CONTENT_ICON);
                this.mContentIconGravity = wearableBundle.getInt(KEY_CONTENT_ICON_GRAVITY, 8388613);
                this.mContentActionIndex = wearableBundle.getInt(KEY_CONTENT_ACTION_INDEX, -1);
                this.mCustomSizePreset = wearableBundle.getInt(KEY_CUSTOM_SIZE_PRESET, 0);
                this.mCustomContentHeight = wearableBundle.getInt(KEY_CUSTOM_CONTENT_HEIGHT);
                this.mGravity = wearableBundle.getInt(KEY_GRAVITY, 80);
                this.mHintScreenTimeout = wearableBundle.getInt(KEY_HINT_SCREEN_TIMEOUT);
                this.mDismissalId = wearableBundle.getString(KEY_DISMISSAL_ID);
                this.mBridgeTag = wearableBundle.getString(KEY_BRIDGE_TAG);
            }
        }

        public android.support.v4.app.NotificationCompat.Builder extend(android.support.v4.app.NotificationCompat.Builder builder) {
            android.os.Bundle wearableBundle = new android.os.Bundle();
            if (!this.mActions.isEmpty()) {
                wearableBundle.putParcelableArrayList(KEY_ACTIONS, android.support.v4.app.NotificationCompat.IMPL.getParcelableArrayListForActions((android.support.v4.app.NotificationCompat.Action[]) this.mActions.toArray(new android.support.v4.app.NotificationCompat.Action[this.mActions.size()])));
            }
            if (this.mFlags != 1) {
                wearableBundle.putInt(KEY_FLAGS, this.mFlags);
            }
            if (this.mDisplayIntent != null) {
                wearableBundle.putParcelable(KEY_DISPLAY_INTENT, this.mDisplayIntent);
            }
            if (!this.mPages.isEmpty()) {
                wearableBundle.putParcelableArray(KEY_PAGES, (android.os.Parcelable[]) this.mPages.toArray(new android.app.Notification[this.mPages.size()]));
            }
            if (this.mBackground != null) {
                wearableBundle.putParcelable(KEY_BACKGROUND, this.mBackground);
            }
            if (this.mContentIcon != 0) {
                wearableBundle.putInt(KEY_CONTENT_ICON, this.mContentIcon);
            }
            if (this.mContentIconGravity != 8388613) {
                wearableBundle.putInt(KEY_CONTENT_ICON_GRAVITY, this.mContentIconGravity);
            }
            if (this.mContentActionIndex != -1) {
                wearableBundle.putInt(KEY_CONTENT_ACTION_INDEX, this.mContentActionIndex);
            }
            if (this.mCustomSizePreset != 0) {
                wearableBundle.putInt(KEY_CUSTOM_SIZE_PRESET, this.mCustomSizePreset);
            }
            if (this.mCustomContentHeight != 0) {
                wearableBundle.putInt(KEY_CUSTOM_CONTENT_HEIGHT, this.mCustomContentHeight);
            }
            if (this.mGravity != 80) {
                wearableBundle.putInt(KEY_GRAVITY, this.mGravity);
            }
            if (this.mHintScreenTimeout != 0) {
                wearableBundle.putInt(KEY_HINT_SCREEN_TIMEOUT, this.mHintScreenTimeout);
            }
            if (this.mDismissalId != null) {
                wearableBundle.putString(KEY_DISMISSAL_ID, this.mDismissalId);
            }
            if (this.mBridgeTag != null) {
                wearableBundle.putString(KEY_BRIDGE_TAG, this.mBridgeTag);
            }
            builder.getExtras().putBundle(EXTRA_WEARABLE_EXTENSIONS, wearableBundle);
            return builder;
        }

        public android.support.v4.app.NotificationCompat.WearableExtender clone() {
            android.support.v4.app.NotificationCompat.WearableExtender that = new android.support.v4.app.NotificationCompat.WearableExtender();
            that.mActions = new java.util.ArrayList<>(this.mActions);
            that.mFlags = this.mFlags;
            that.mDisplayIntent = this.mDisplayIntent;
            that.mPages = new java.util.ArrayList<>(this.mPages);
            that.mBackground = this.mBackground;
            that.mContentIcon = this.mContentIcon;
            that.mContentIconGravity = this.mContentIconGravity;
            that.mContentActionIndex = this.mContentActionIndex;
            that.mCustomSizePreset = this.mCustomSizePreset;
            that.mCustomContentHeight = this.mCustomContentHeight;
            that.mGravity = this.mGravity;
            that.mHintScreenTimeout = this.mHintScreenTimeout;
            that.mDismissalId = this.mDismissalId;
            that.mBridgeTag = this.mBridgeTag;
            return that;
        }

        public android.support.v4.app.NotificationCompat.WearableExtender addAction(android.support.v4.app.NotificationCompat.Action action) {
            this.mActions.add(action);
            return this;
        }

        public android.support.v4.app.NotificationCompat.WearableExtender addActions(java.util.List<android.support.v4.app.NotificationCompat.Action> actions) {
            this.mActions.addAll(actions);
            return this;
        }

        public android.support.v4.app.NotificationCompat.WearableExtender clearActions() {
            this.mActions.clear();
            return this;
        }

        public java.util.List<android.support.v4.app.NotificationCompat.Action> getActions() {
            return this.mActions;
        }

        public android.support.v4.app.NotificationCompat.WearableExtender setDisplayIntent(android.app.PendingIntent intent) {
            this.mDisplayIntent = intent;
            return this;
        }

        public android.app.PendingIntent getDisplayIntent() {
            return this.mDisplayIntent;
        }

        public android.support.v4.app.NotificationCompat.WearableExtender addPage(android.app.Notification page) {
            this.mPages.add(page);
            return this;
        }

        public android.support.v4.app.NotificationCompat.WearableExtender addPages(java.util.List<android.app.Notification> pages) {
            this.mPages.addAll(pages);
            return this;
        }

        public android.support.v4.app.NotificationCompat.WearableExtender clearPages() {
            this.mPages.clear();
            return this;
        }

        public java.util.List<android.app.Notification> getPages() {
            return this.mPages;
        }

        public android.support.v4.app.NotificationCompat.WearableExtender setBackground(android.graphics.Bitmap background) {
            this.mBackground = background;
            return this;
        }

        public android.graphics.Bitmap getBackground() {
            return this.mBackground;
        }

        public android.support.v4.app.NotificationCompat.WearableExtender setContentIcon(int icon) {
            this.mContentIcon = icon;
            return this;
        }

        public int getContentIcon() {
            return this.mContentIcon;
        }

        public android.support.v4.app.NotificationCompat.WearableExtender setContentIconGravity(int contentIconGravity) {
            this.mContentIconGravity = contentIconGravity;
            return this;
        }

        public int getContentIconGravity() {
            return this.mContentIconGravity;
        }

        public android.support.v4.app.NotificationCompat.WearableExtender setContentAction(int actionIndex) {
            this.mContentActionIndex = actionIndex;
            return this;
        }

        public int getContentAction() {
            return this.mContentActionIndex;
        }

        public android.support.v4.app.NotificationCompat.WearableExtender setGravity(int gravity) {
            this.mGravity = gravity;
            return this;
        }

        public int getGravity() {
            return this.mGravity;
        }

        public android.support.v4.app.NotificationCompat.WearableExtender setCustomSizePreset(int sizePreset) {
            this.mCustomSizePreset = sizePreset;
            return this;
        }

        public int getCustomSizePreset() {
            return this.mCustomSizePreset;
        }

        public android.support.v4.app.NotificationCompat.WearableExtender setCustomContentHeight(int height) {
            this.mCustomContentHeight = height;
            return this;
        }

        public int getCustomContentHeight() {
            return this.mCustomContentHeight;
        }

        public android.support.v4.app.NotificationCompat.WearableExtender setStartScrollBottom(boolean startScrollBottom) {
            setFlag(8, startScrollBottom);
            return this;
        }

        public boolean getStartScrollBottom() {
            return (this.mFlags & 8) != 0;
        }

        public android.support.v4.app.NotificationCompat.WearableExtender setContentIntentAvailableOffline(boolean contentIntentAvailableOffline) {
            setFlag(1, contentIntentAvailableOffline);
            return this;
        }

        public boolean getContentIntentAvailableOffline() {
            return (this.mFlags & 1) != 0;
        }

        public android.support.v4.app.NotificationCompat.WearableExtender setHintHideIcon(boolean hintHideIcon) {
            setFlag(2, hintHideIcon);
            return this;
        }

        public boolean getHintHideIcon() {
            return (this.mFlags & 2) != 0;
        }

        public android.support.v4.app.NotificationCompat.WearableExtender setHintShowBackgroundOnly(boolean hintShowBackgroundOnly) {
            setFlag(4, hintShowBackgroundOnly);
            return this;
        }

        public boolean getHintShowBackgroundOnly() {
            return (this.mFlags & 4) != 0;
        }

        public android.support.v4.app.NotificationCompat.WearableExtender setHintAvoidBackgroundClipping(boolean hintAvoidBackgroundClipping) {
            setFlag(16, hintAvoidBackgroundClipping);
            return this;
        }

        public boolean getHintAvoidBackgroundClipping() {
            return (this.mFlags & 16) != 0;
        }

        public android.support.v4.app.NotificationCompat.WearableExtender setHintScreenTimeout(int timeout) {
            this.mHintScreenTimeout = timeout;
            return this;
        }

        public int getHintScreenTimeout() {
            return this.mHintScreenTimeout;
        }

        public android.support.v4.app.NotificationCompat.WearableExtender setHintAmbientBigPicture(boolean hintAmbientBigPicture) {
            setFlag(32, hintAmbientBigPicture);
            return this;
        }

        public boolean getHintAmbientBigPicture() {
            return (this.mFlags & 32) != 0;
        }

        public android.support.v4.app.NotificationCompat.WearableExtender setHintContentIntentLaunchesActivity(boolean hintContentIntentLaunchesActivity) {
            setFlag(64, hintContentIntentLaunchesActivity);
            return this;
        }

        public boolean getHintContentIntentLaunchesActivity() {
            return (this.mFlags & 64) != 0;
        }

        public android.support.v4.app.NotificationCompat.WearableExtender setDismissalId(java.lang.String dismissalId) {
            this.mDismissalId = dismissalId;
            return this;
        }

        public java.lang.String getDismissalId() {
            return this.mDismissalId;
        }

        public android.support.v4.app.NotificationCompat.WearableExtender setBridgeTag(java.lang.String bridgeTag) {
            this.mBridgeTag = bridgeTag;
            return this;
        }

        public java.lang.String getBridgeTag() {
            return this.mBridgeTag;
        }

        private void setFlag(int mask, boolean value) {
            if (value) {
                this.mFlags |= mask;
            } else {
                this.mFlags &= mask ^ -1;
            }
        }
    }

    static void addActionsToBuilder(android.support.v4.app.NotificationBuilderWithActions builder, java.util.ArrayList<android.support.v4.app.NotificationCompat.Action> actions) {
        java.util.Iterator it = actions.iterator();
        while (it.hasNext()) {
            builder.addAction((android.support.v4.app.NotificationCompat.Action) it.next());
        }
    }

    static void addStyleToBuilderJellybean(android.support.v4.app.NotificationBuilderWithBuilderAccessor builder, android.support.v4.app.NotificationCompat.Style style) {
        if (style == null) {
            return;
        }
        if (style instanceof android.support.v4.app.NotificationCompat.BigTextStyle) {
            android.support.v4.app.NotificationCompat.BigTextStyle bigTextStyle = (android.support.v4.app.NotificationCompat.BigTextStyle) style;
            android.support.v4.app.NotificationCompatJellybean.addBigTextStyle(builder, bigTextStyle.mBigContentTitle, bigTextStyle.mSummaryTextSet, bigTextStyle.mSummaryText, bigTextStyle.mBigText);
        } else if (style instanceof android.support.v4.app.NotificationCompat.InboxStyle) {
            android.support.v4.app.NotificationCompat.InboxStyle inboxStyle = (android.support.v4.app.NotificationCompat.InboxStyle) style;
            android.support.v4.app.NotificationCompatJellybean.addInboxStyle(builder, inboxStyle.mBigContentTitle, inboxStyle.mSummaryTextSet, inboxStyle.mSummaryText, inboxStyle.mTexts);
        } else if (style instanceof android.support.v4.app.NotificationCompat.BigPictureStyle) {
            android.support.v4.app.NotificationCompat.BigPictureStyle bigPictureStyle = (android.support.v4.app.NotificationCompat.BigPictureStyle) style;
            android.support.v4.app.NotificationCompatJellybean.addBigPictureStyle(builder, bigPictureStyle.mBigContentTitle, bigPictureStyle.mSummaryTextSet, bigPictureStyle.mSummaryText, bigPictureStyle.mPicture, bigPictureStyle.mBigLargeIcon, bigPictureStyle.mBigLargeIconSet);
        }
    }

    static void addStyleToBuilderApi24(android.support.v4.app.NotificationBuilderWithBuilderAccessor builder, android.support.v4.app.NotificationCompat.Style style) {
        if (style == null) {
            return;
        }
        if (style instanceof android.support.v4.app.NotificationCompat.MessagingStyle) {
            android.support.v4.app.NotificationCompat.MessagingStyle messagingStyle = (android.support.v4.app.NotificationCompat.MessagingStyle) style;
            java.util.List<java.lang.CharSequence> texts = new java.util.ArrayList<>();
            java.util.List<java.lang.Long> timestamps = new java.util.ArrayList<>();
            java.util.List<java.lang.CharSequence> senders = new java.util.ArrayList<>();
            java.util.List<java.lang.String> dataMimeTypes = new java.util.ArrayList<>();
            java.util.List<android.net.Uri> dataUris = new java.util.ArrayList<>();
            for (android.support.v4.app.NotificationCompat.MessagingStyle.Message message : messagingStyle.mMessages) {
                texts.add(message.getText());
                timestamps.add(java.lang.Long.valueOf(message.getTimestamp()));
                senders.add(message.getSender());
                dataMimeTypes.add(message.getDataMimeType());
                dataUris.add(message.getDataUri());
            }
            android.support.v4.app.NotificationCompatApi24.addMessagingStyle(builder, messagingStyle.mUserDisplayName, messagingStyle.mConversationTitle, texts, timestamps, senders, dataMimeTypes, dataUris);
            return;
        }
        addStyleToBuilderJellybean(builder, style);
    }

    static {
        if (android.support.v4.os.BuildCompat.isAtLeastN()) {
            IMPL = new android.support.v4.app.NotificationCompat.NotificationCompatImplApi24();
        } else if (android.os.Build.VERSION.SDK_INT >= 21) {
            IMPL = new android.support.v4.app.NotificationCompat.NotificationCompatImplApi21();
        } else if (android.os.Build.VERSION.SDK_INT >= 20) {
            IMPL = new android.support.v4.app.NotificationCompat.NotificationCompatImplApi20();
        } else if (android.os.Build.VERSION.SDK_INT >= 19) {
            IMPL = new android.support.v4.app.NotificationCompat.NotificationCompatImplKitKat();
        } else if (android.os.Build.VERSION.SDK_INT >= 16) {
            IMPL = new android.support.v4.app.NotificationCompat.NotificationCompatImplJellybean();
        } else if (android.os.Build.VERSION.SDK_INT >= 14) {
            IMPL = new android.support.v4.app.NotificationCompat.NotificationCompatImplIceCreamSandwich();
        } else if (android.os.Build.VERSION.SDK_INT >= 11) {
            IMPL = new android.support.v4.app.NotificationCompat.NotificationCompatImplHoneycomb();
        } else {
            IMPL = new android.support.v4.app.NotificationCompat.NotificationCompatImplBase();
        }
    }

    static android.app.Notification[] getNotificationArrayFromBundle(android.os.Bundle bundle, java.lang.String key) {
        android.os.Parcelable[] array = bundle.getParcelableArray(key);
        if ((array instanceof android.app.Notification[]) || array == null) {
            return (android.app.Notification[]) array;
        }
        android.app.Notification[] typedArray = new android.app.Notification[array.length];
        for (int i = 0; i < array.length; i++) {
            typedArray[i] = (android.app.Notification) array[i];
        }
        bundle.putParcelableArray(key, typedArray);
        return typedArray;
    }

    public static android.os.Bundle getExtras(android.app.Notification notif) {
        return IMPL.getExtras(notif);
    }

    public static int getActionCount(android.app.Notification notif) {
        return IMPL.getActionCount(notif);
    }

    public static android.support.v4.app.NotificationCompat.Action getAction(android.app.Notification notif, int actionIndex) {
        return IMPL.getAction(notif, actionIndex);
    }

    public static java.lang.String getCategory(android.app.Notification notif) {
        return IMPL.getCategory(notif);
    }

    public static boolean getLocalOnly(android.app.Notification notif) {
        return IMPL.getLocalOnly(notif);
    }

    public static java.lang.String getGroup(android.app.Notification notif) {
        return IMPL.getGroup(notif);
    }

    public static boolean isGroupSummary(android.app.Notification notif) {
        return IMPL.isGroupSummary(notif);
    }

    public static java.lang.String getSortKey(android.app.Notification notif) {
        return IMPL.getSortKey(notif);
    }
}
