package android.support.v4.app;

/* compiled from: FragmentManager */
final class FragmentManagerState implements android.os.Parcelable {
    public static final android.os.Parcelable.Creator<android.support.v4.app.FragmentManagerState> CREATOR = new android.os.Parcelable.Creator<android.support.v4.app.FragmentManagerState>() {
        public android.support.v4.app.FragmentManagerState createFromParcel(android.os.Parcel in) {
            return new android.support.v4.app.FragmentManagerState(in);
        }

        public android.support.v4.app.FragmentManagerState[] newArray(int size) {
            return new android.support.v4.app.FragmentManagerState[size];
        }
    };
    android.support.v4.app.FragmentState[] mActive;
    int[] mAdded;
    android.support.v4.app.BackStackState[] mBackStack;

    public FragmentManagerState() {
    }

    public FragmentManagerState(android.os.Parcel in) {
        this.mActive = (android.support.v4.app.FragmentState[]) in.createTypedArray(android.support.v4.app.FragmentState.CREATOR);
        this.mAdded = in.createIntArray();
        this.mBackStack = (android.support.v4.app.BackStackState[]) in.createTypedArray(android.support.v4.app.BackStackState.CREATOR);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeTypedArray(this.mActive, flags);
        dest.writeIntArray(this.mAdded);
        dest.writeTypedArray(this.mBackStack, flags);
    }
}
