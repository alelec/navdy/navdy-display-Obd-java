package android.support.v4.app;

@android.annotation.TargetApi(11)
@android.support.annotation.RequiresApi(11)
@android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
public interface NotificationBuilderWithBuilderAccessor {
    android.app.Notification build();

    android.app.Notification.Builder getBuilder();
}
