package android.support.v4.app;

@android.annotation.TargetApi(23)
@android.support.annotation.RequiresApi(23)
class NotificationCompatApi23 {
    public static final java.lang.String CATEGORY_REMINDER = "reminder";

    NotificationCompatApi23() {
    }
}
