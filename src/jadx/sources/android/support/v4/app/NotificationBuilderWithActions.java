package android.support.v4.app;

interface NotificationBuilderWithActions {
    void addAction(android.support.v4.app.NotificationCompatBase.Action action);
}
