package android.support.v4.app;

@android.annotation.TargetApi(20)
@android.support.annotation.RequiresApi(20)
class RemoteInputCompatApi20 {
    RemoteInputCompatApi20() {
    }

    static android.support.v4.app.RemoteInputCompatBase.RemoteInput[] toCompat(android.app.RemoteInput[] srcArray, android.support.v4.app.RemoteInputCompatBase.RemoteInput.Factory factory) {
        if (srcArray == null) {
            return null;
        }
        android.support.v4.app.RemoteInputCompatBase.RemoteInput[] result = factory.newArray(srcArray.length);
        for (int i = 0; i < srcArray.length; i++) {
            android.app.RemoteInput src = srcArray[i];
            result[i] = factory.build(src.getResultKey(), src.getLabel(), src.getChoices(), src.getAllowFreeFormInput(), src.getExtras());
        }
        return result;
    }

    static android.app.RemoteInput[] fromCompat(android.support.v4.app.RemoteInputCompatBase.RemoteInput[] srcArray) {
        if (srcArray == null) {
            return null;
        }
        android.app.RemoteInput[] result = new android.app.RemoteInput[srcArray.length];
        for (int i = 0; i < srcArray.length; i++) {
            android.support.v4.app.RemoteInputCompatBase.RemoteInput src = srcArray[i];
            result[i] = new android.app.RemoteInput.Builder(src.getResultKey()).setLabel(src.getLabel()).setChoices(src.getChoices()).setAllowFreeFormInput(src.getAllowFreeFormInput()).addExtras(src.getExtras()).build();
        }
        return result;
    }

    static android.os.Bundle getResultsFromIntent(android.content.Intent intent) {
        return android.app.RemoteInput.getResultsFromIntent(intent);
    }

    static void addResultsToIntent(android.support.v4.app.RemoteInputCompatBase.RemoteInput[] remoteInputs, android.content.Intent intent, android.os.Bundle results) {
        android.app.RemoteInput.addResultsToIntent(fromCompat(remoteInputs), intent, results);
    }
}
