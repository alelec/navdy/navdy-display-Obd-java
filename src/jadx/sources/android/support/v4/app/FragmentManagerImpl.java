package android.support.v4.app;

/* compiled from: FragmentManager */
final class FragmentManagerImpl extends android.support.v4.app.FragmentManager implements android.support.v4.view.LayoutInflaterFactory {
    static final android.view.animation.Interpolator ACCELERATE_CUBIC = new android.view.animation.AccelerateInterpolator(1.5f);
    static final android.view.animation.Interpolator ACCELERATE_QUINT = new android.view.animation.AccelerateInterpolator(2.5f);
    static final int ANIM_DUR = 220;
    public static final int ANIM_STYLE_CLOSE_ENTER = 3;
    public static final int ANIM_STYLE_CLOSE_EXIT = 4;
    public static final int ANIM_STYLE_FADE_ENTER = 5;
    public static final int ANIM_STYLE_FADE_EXIT = 6;
    public static final int ANIM_STYLE_OPEN_ENTER = 1;
    public static final int ANIM_STYLE_OPEN_EXIT = 2;
    static boolean DEBUG = false;
    static final android.view.animation.Interpolator DECELERATE_CUBIC = new android.view.animation.DecelerateInterpolator(1.5f);
    static final android.view.animation.Interpolator DECELERATE_QUINT = new android.view.animation.DecelerateInterpolator(2.5f);
    static final boolean HONEYCOMB;
    static final java.lang.String TAG = "FragmentManager";
    static final java.lang.String TARGET_REQUEST_CODE_STATE_TAG = "android:target_req_state";
    static final java.lang.String TARGET_STATE_TAG = "android:target_state";
    static final java.lang.String USER_VISIBLE_HINT_TAG = "android:user_visible_hint";
    static final java.lang.String VIEW_STATE_TAG = "android:view_state";
    static java.lang.reflect.Field sAnimationListenerField = null;
    java.util.ArrayList<android.support.v4.app.Fragment> mActive;
    java.util.ArrayList<android.support.v4.app.Fragment> mAdded;
    java.util.ArrayList<java.lang.Integer> mAvailBackStackIndices;
    java.util.ArrayList<java.lang.Integer> mAvailIndices;
    java.util.ArrayList<android.support.v4.app.BackStackRecord> mBackStack;
    java.util.ArrayList<android.support.v4.app.FragmentManager.OnBackStackChangedListener> mBackStackChangeListeners;
    java.util.ArrayList<android.support.v4.app.BackStackRecord> mBackStackIndices;
    android.support.v4.app.FragmentContainer mContainer;
    java.util.ArrayList<android.support.v4.app.Fragment> mCreatedMenus;
    int mCurState = 0;
    boolean mDestroyed;
    java.lang.Runnable mExecCommit = new java.lang.Runnable() {
        public void run() {
            android.support.v4.app.FragmentManagerImpl.this.execPendingActions();
        }
    };
    boolean mExecutingActions;
    boolean mHavePendingDeferredStart;
    android.support.v4.app.FragmentHostCallback mHost;
    private java.util.concurrent.CopyOnWriteArrayList<android.support.v4.util.Pair<android.support.v4.app.FragmentManager.FragmentLifecycleCallbacks, java.lang.Boolean>> mLifecycleCallbacks;
    boolean mNeedMenuInvalidate;
    java.lang.String mNoTransactionsBecause;
    android.support.v4.app.Fragment mParent;
    java.util.ArrayList<android.support.v4.app.FragmentManagerImpl.OpGenerator> mPendingActions;
    java.util.ArrayList<android.support.v4.app.FragmentManagerImpl.StartEnterTransitionListener> mPostponedTransactions;
    android.util.SparseArray<android.os.Parcelable> mStateArray = null;
    android.os.Bundle mStateBundle = null;
    boolean mStateSaved;
    java.lang.Runnable[] mTmpActions;
    java.util.ArrayList<android.support.v4.app.Fragment> mTmpAddedFragments;
    java.util.ArrayList<java.lang.Boolean> mTmpIsPop;
    java.util.ArrayList<android.support.v4.app.BackStackRecord> mTmpRecords;

    /* compiled from: FragmentManager */
    static class AnimateOnHWLayerIfNeededListener implements android.view.animation.Animation.AnimationListener {
        private android.view.animation.Animation.AnimationListener mOriginalListener;
        private boolean mShouldRunOnHWLayer;
        android.view.View mView;

        public AnimateOnHWLayerIfNeededListener(android.view.View v, android.view.animation.Animation anim) {
            if (v != null && anim != null) {
                this.mView = v;
            }
        }

        public AnimateOnHWLayerIfNeededListener(android.view.View v, android.view.animation.Animation anim, android.view.animation.Animation.AnimationListener listener) {
            if (v != null && anim != null) {
                this.mOriginalListener = listener;
                this.mView = v;
                this.mShouldRunOnHWLayer = true;
            }
        }

        @android.support.annotation.CallSuper
        public void onAnimationStart(android.view.animation.Animation animation) {
            if (this.mOriginalListener != null) {
                this.mOriginalListener.onAnimationStart(animation);
            }
        }

        @android.support.annotation.CallSuper
        public void onAnimationEnd(android.view.animation.Animation animation) {
            if (this.mView != null && this.mShouldRunOnHWLayer) {
                if (android.support.v4.view.ViewCompat.isAttachedToWindow(this.mView) || android.support.v4.os.BuildCompat.isAtLeastN()) {
                    this.mView.post(new java.lang.Runnable() {
                        public void run() {
                            android.support.v4.view.ViewCompat.setLayerType(android.support.v4.app.FragmentManagerImpl.AnimateOnHWLayerIfNeededListener.this.mView, 0, null);
                        }
                    });
                } else {
                    android.support.v4.view.ViewCompat.setLayerType(this.mView, 0, null);
                }
            }
            if (this.mOriginalListener != null) {
                this.mOriginalListener.onAnimationEnd(animation);
            }
        }

        public void onAnimationRepeat(android.view.animation.Animation animation) {
            if (this.mOriginalListener != null) {
                this.mOriginalListener.onAnimationRepeat(animation);
            }
        }
    }

    /* compiled from: FragmentManager */
    static class FragmentTag {
        public static final int[] Fragment = {16842755, 16842960, 16842961};
        public static final int Fragment_id = 1;
        public static final int Fragment_name = 0;
        public static final int Fragment_tag = 2;

        FragmentTag() {
        }
    }

    /* compiled from: FragmentManager */
    interface OpGenerator {
        boolean generateOps(java.util.ArrayList<android.support.v4.app.BackStackRecord> arrayList, java.util.ArrayList<java.lang.Boolean> arrayList2);
    }

    /* compiled from: FragmentManager */
    private class PopBackStackState implements android.support.v4.app.FragmentManagerImpl.OpGenerator {
        final int mFlags;
        final int mId;
        final java.lang.String mName;

        PopBackStackState(java.lang.String name, int id, int flags) {
            this.mName = name;
            this.mId = id;
            this.mFlags = flags;
        }

        public boolean generateOps(java.util.ArrayList<android.support.v4.app.BackStackRecord> records, java.util.ArrayList<java.lang.Boolean> isRecordPop) {
            return android.support.v4.app.FragmentManagerImpl.this.popBackStackState(records, isRecordPop, this.mName, this.mId, this.mFlags);
        }
    }

    /* compiled from: FragmentManager */
    static class StartEnterTransitionListener implements android.support.v4.app.Fragment.OnStartEnterTransitionListener {
        /* access modifiers changed from: private */
        public final boolean mIsBack;
        private int mNumPostponed;
        /* access modifiers changed from: private */
        public final android.support.v4.app.BackStackRecord mRecord;

        StartEnterTransitionListener(android.support.v4.app.BackStackRecord record, boolean isBack) {
            this.mIsBack = isBack;
            this.mRecord = record;
        }

        public void onStartEnterTransition() {
            this.mNumPostponed--;
            if (this.mNumPostponed == 0) {
                this.mRecord.mManager.scheduleCommit();
            }
        }

        public void startListening() {
            this.mNumPostponed++;
        }

        public boolean isReady() {
            return this.mNumPostponed == 0;
        }

        public void completeTransaction() {
            boolean canceled;
            boolean z = false;
            if (this.mNumPostponed > 0) {
                canceled = true;
            } else {
                canceled = false;
            }
            android.support.v4.app.FragmentManagerImpl manager = this.mRecord.mManager;
            int numAdded = manager.mAdded.size();
            for (int i = 0; i < numAdded; i++) {
                android.support.v4.app.Fragment fragment = (android.support.v4.app.Fragment) manager.mAdded.get(i);
                fragment.setOnStartEnterTransitionListener(null);
                if (canceled && fragment.isPostponed()) {
                    fragment.startPostponedEnterTransition();
                }
            }
            android.support.v4.app.FragmentManagerImpl fragmentManagerImpl = this.mRecord.mManager;
            android.support.v4.app.BackStackRecord backStackRecord = this.mRecord;
            boolean z2 = this.mIsBack;
            if (!canceled) {
                z = true;
            }
            fragmentManagerImpl.completeExecute(backStackRecord, z2, z, true);
        }

        public void cancelTransaction() {
            this.mRecord.mManager.completeExecute(this.mRecord, this.mIsBack, false, false);
        }
    }

    FragmentManagerImpl() {
    }

    static {
        boolean z = false;
        if (android.os.Build.VERSION.SDK_INT >= 11) {
            z = true;
        }
        HONEYCOMB = z;
    }

    static boolean modifiesAlpha(android.view.animation.Animation anim) {
        if (anim instanceof android.view.animation.AlphaAnimation) {
            return true;
        }
        if (anim instanceof android.view.animation.AnimationSet) {
            java.util.List<android.view.animation.Animation> anims = ((android.view.animation.AnimationSet) anim).getAnimations();
            for (int i = 0; i < anims.size(); i++) {
                if (anims.get(i) instanceof android.view.animation.AlphaAnimation) {
                    return true;
                }
            }
        }
        return false;
    }

    static boolean shouldRunOnHWLayer(android.view.View v, android.view.animation.Animation anim) {
        return android.os.Build.VERSION.SDK_INT >= 19 && android.support.v4.view.ViewCompat.getLayerType(v) == 0 && android.support.v4.view.ViewCompat.hasOverlappingRendering(v) && modifiesAlpha(anim);
    }

    private void throwException(java.lang.RuntimeException ex) {
        android.util.Log.e(TAG, ex.getMessage());
        android.util.Log.e(TAG, "Activity state:");
        java.io.PrintWriter pw = new java.io.PrintWriter(new android.support.v4.util.LogWriter(TAG));
        if (this.mHost != null) {
            try {
                this.mHost.onDump("  ", null, pw, new java.lang.String[0]);
            } catch (java.lang.Exception e) {
                android.util.Log.e(TAG, "Failed dumping state", e);
            }
        } else {
            try {
                dump("  ", null, pw, new java.lang.String[0]);
            } catch (java.lang.Exception e2) {
                android.util.Log.e(TAG, "Failed dumping state", e2);
            }
        }
        throw ex;
    }

    public android.support.v4.app.FragmentTransaction beginTransaction() {
        return new android.support.v4.app.BackStackRecord(this);
    }

    public boolean executePendingTransactions() {
        boolean updates = execPendingActions();
        forcePostponedTransactions();
        return updates;
    }

    public void popBackStack() {
        enqueueAction(new android.support.v4.app.FragmentManagerImpl.PopBackStackState(null, -1, 0), false);
    }

    public boolean popBackStackImmediate() {
        checkStateLoss();
        return popBackStackImmediate(null, -1, 0);
    }

    public void popBackStack(java.lang.String name, int flags) {
        enqueueAction(new android.support.v4.app.FragmentManagerImpl.PopBackStackState(name, -1, flags), false);
    }

    public boolean popBackStackImmediate(java.lang.String name, int flags) {
        checkStateLoss();
        return popBackStackImmediate(name, -1, flags);
    }

    public void popBackStack(int id, int flags) {
        if (id < 0) {
            throw new java.lang.IllegalArgumentException("Bad id: " + id);
        }
        enqueueAction(new android.support.v4.app.FragmentManagerImpl.PopBackStackState(null, id, flags), false);
    }

    public boolean popBackStackImmediate(int id, int flags) {
        checkStateLoss();
        execPendingActions();
        if (id >= 0) {
            return popBackStackImmediate(null, id, flags);
        }
        throw new java.lang.IllegalArgumentException("Bad id: " + id);
    }

    private boolean popBackStackImmediate(java.lang.String name, int id, int flags) {
        execPendingActions();
        ensureExecReady(true);
        boolean executePop = popBackStackState(this.mTmpRecords, this.mTmpIsPop, name, id, flags);
        if (executePop) {
            this.mExecutingActions = true;
            try {
                optimizeAndExecuteOps(this.mTmpRecords, this.mTmpIsPop);
            } finally {
                cleanupExec();
            }
        }
        doPendingDeferredStart();
        return executePop;
    }

    public int getBackStackEntryCount() {
        if (this.mBackStack != null) {
            return this.mBackStack.size();
        }
        return 0;
    }

    public android.support.v4.app.FragmentManager.BackStackEntry getBackStackEntryAt(int index) {
        return (android.support.v4.app.FragmentManager.BackStackEntry) this.mBackStack.get(index);
    }

    public void addOnBackStackChangedListener(android.support.v4.app.FragmentManager.OnBackStackChangedListener listener) {
        if (this.mBackStackChangeListeners == null) {
            this.mBackStackChangeListeners = new java.util.ArrayList<>();
        }
        this.mBackStackChangeListeners.add(listener);
    }

    public void removeOnBackStackChangedListener(android.support.v4.app.FragmentManager.OnBackStackChangedListener listener) {
        if (this.mBackStackChangeListeners != null) {
            this.mBackStackChangeListeners.remove(listener);
        }
    }

    public void putFragment(android.os.Bundle bundle, java.lang.String key, android.support.v4.app.Fragment fragment) {
        if (fragment.mIndex < 0) {
            throwException(new java.lang.IllegalStateException("Fragment " + fragment + " is not currently in the FragmentManager"));
        }
        bundle.putInt(key, fragment.mIndex);
    }

    public android.support.v4.app.Fragment getFragment(android.os.Bundle bundle, java.lang.String key) {
        int index = bundle.getInt(key, -1);
        if (index == -1) {
            return null;
        }
        if (index >= this.mActive.size()) {
            throwException(new java.lang.IllegalStateException("Fragment no longer exists for key " + key + ": index " + index));
        }
        android.support.v4.app.Fragment f = (android.support.v4.app.Fragment) this.mActive.get(index);
        if (f != null) {
            return f;
        }
        throwException(new java.lang.IllegalStateException("Fragment no longer exists for key " + key + ": index " + index));
        return f;
    }

    public java.util.List<android.support.v4.app.Fragment> getFragments() {
        return this.mActive;
    }

    public android.support.v4.app.Fragment.SavedState saveFragmentInstanceState(android.support.v4.app.Fragment fragment) {
        if (fragment.mIndex < 0) {
            throwException(new java.lang.IllegalStateException("Fragment " + fragment + " is not currently in the FragmentManager"));
        }
        if (fragment.mState <= 0) {
            return null;
        }
        android.os.Bundle result = saveFragmentBasicState(fragment);
        if (result != null) {
            return new android.support.v4.app.Fragment.SavedState(result);
        }
        return null;
    }

    public boolean isDestroyed() {
        return this.mDestroyed;
    }

    public java.lang.String toString() {
        java.lang.StringBuilder sb = new java.lang.StringBuilder(128);
        sb.append("FragmentManager{");
        sb.append(java.lang.Integer.toHexString(java.lang.System.identityHashCode(this)));
        sb.append(" in ");
        if (this.mParent != null) {
            android.support.v4.util.DebugUtils.buildShortClassTag(this.mParent, sb);
        } else {
            android.support.v4.util.DebugUtils.buildShortClassTag(this.mHost, sb);
        }
        sb.append("}}");
        return sb.toString();
    }

    public void dump(java.lang.String prefix, java.io.FileDescriptor fd, java.io.PrintWriter writer, java.lang.String[] args) {
        java.lang.String innerPrefix = prefix + "    ";
        if (this.mActive != null) {
            int N = this.mActive.size();
            if (N > 0) {
                writer.print(prefix);
                writer.print("Active Fragments in ");
                writer.print(java.lang.Integer.toHexString(java.lang.System.identityHashCode(this)));
                writer.println(":");
                for (int i = 0; i < N; i++) {
                    android.support.v4.app.Fragment f = (android.support.v4.app.Fragment) this.mActive.get(i);
                    writer.print(prefix);
                    writer.print("  #");
                    writer.print(i);
                    writer.print(": ");
                    writer.println(f);
                    if (f != null) {
                        f.dump(innerPrefix, fd, writer, args);
                    }
                }
            }
        }
        if (this.mAdded != null) {
            int N2 = this.mAdded.size();
            if (N2 > 0) {
                writer.print(prefix);
                writer.println("Added Fragments:");
                for (int i2 = 0; i2 < N2; i2++) {
                    android.support.v4.app.Fragment f2 = (android.support.v4.app.Fragment) this.mAdded.get(i2);
                    writer.print(prefix);
                    writer.print("  #");
                    writer.print(i2);
                    writer.print(": ");
                    writer.println(f2.toString());
                }
            }
        }
        if (this.mCreatedMenus != null) {
            int N3 = this.mCreatedMenus.size();
            if (N3 > 0) {
                writer.print(prefix);
                writer.println("Fragments Created Menus:");
                for (int i3 = 0; i3 < N3; i3++) {
                    android.support.v4.app.Fragment f3 = (android.support.v4.app.Fragment) this.mCreatedMenus.get(i3);
                    writer.print(prefix);
                    writer.print("  #");
                    writer.print(i3);
                    writer.print(": ");
                    writer.println(f3.toString());
                }
            }
        }
        if (this.mBackStack != null) {
            int N4 = this.mBackStack.size();
            if (N4 > 0) {
                writer.print(prefix);
                writer.println("Back Stack:");
                for (int i4 = 0; i4 < N4; i4++) {
                    android.support.v4.app.BackStackRecord bs = (android.support.v4.app.BackStackRecord) this.mBackStack.get(i4);
                    writer.print(prefix);
                    writer.print("  #");
                    writer.print(i4);
                    writer.print(": ");
                    writer.println(bs.toString());
                    bs.dump(innerPrefix, fd, writer, args);
                }
            }
        }
        synchronized (this) {
            if (this.mBackStackIndices != null) {
                int N5 = this.mBackStackIndices.size();
                if (N5 > 0) {
                    writer.print(prefix);
                    writer.println("Back Stack Indices:");
                    for (int i5 = 0; i5 < N5; i5++) {
                        android.support.v4.app.BackStackRecord bs2 = (android.support.v4.app.BackStackRecord) this.mBackStackIndices.get(i5);
                        writer.print(prefix);
                        writer.print("  #");
                        writer.print(i5);
                        writer.print(": ");
                        writer.println(bs2);
                    }
                }
            }
            if (this.mAvailBackStackIndices != null && this.mAvailBackStackIndices.size() > 0) {
                writer.print(prefix);
                writer.print("mAvailBackStackIndices: ");
                writer.println(java.util.Arrays.toString(this.mAvailBackStackIndices.toArray()));
            }
        }
        if (this.mPendingActions != null) {
            int N6 = this.mPendingActions.size();
            if (N6 > 0) {
                writer.print(prefix);
                writer.println("Pending Actions:");
                for (int i6 = 0; i6 < N6; i6++) {
                    android.support.v4.app.FragmentManagerImpl.OpGenerator r = (android.support.v4.app.FragmentManagerImpl.OpGenerator) this.mPendingActions.get(i6);
                    writer.print(prefix);
                    writer.print("  #");
                    writer.print(i6);
                    writer.print(": ");
                    writer.println(r);
                }
            }
        }
        writer.print(prefix);
        writer.println("FragmentManager misc state:");
        writer.print(prefix);
        writer.print("  mHost=");
        writer.println(this.mHost);
        writer.print(prefix);
        writer.print("  mContainer=");
        writer.println(this.mContainer);
        if (this.mParent != null) {
            writer.print(prefix);
            writer.print("  mParent=");
            writer.println(this.mParent);
        }
        writer.print(prefix);
        writer.print("  mCurState=");
        writer.print(this.mCurState);
        writer.print(" mStateSaved=");
        writer.print(this.mStateSaved);
        writer.print(" mDestroyed=");
        writer.println(this.mDestroyed);
        if (this.mNeedMenuInvalidate) {
            writer.print(prefix);
            writer.print("  mNeedMenuInvalidate=");
            writer.println(this.mNeedMenuInvalidate);
        }
        if (this.mNoTransactionsBecause != null) {
            writer.print(prefix);
            writer.print("  mNoTransactionsBecause=");
            writer.println(this.mNoTransactionsBecause);
        }
        if (this.mAvailIndices != null && this.mAvailIndices.size() > 0) {
            writer.print(prefix);
            writer.print("  mAvailIndices: ");
            writer.println(java.util.Arrays.toString(this.mAvailIndices.toArray()));
        }
    }

    static android.view.animation.Animation makeOpenCloseAnimation(android.content.Context context, float startScale, float endScale, float startAlpha, float endAlpha) {
        android.view.animation.AnimationSet set = new android.view.animation.AnimationSet(false);
        android.view.animation.ScaleAnimation scale = new android.view.animation.ScaleAnimation(startScale, endScale, startScale, endScale, 1, 0.5f, 1, 0.5f);
        scale.setInterpolator(DECELERATE_QUINT);
        scale.setDuration(220);
        set.addAnimation(scale);
        android.view.animation.AlphaAnimation alpha = new android.view.animation.AlphaAnimation(startAlpha, endAlpha);
        alpha.setInterpolator(DECELERATE_CUBIC);
        alpha.setDuration(220);
        set.addAnimation(alpha);
        return set;
    }

    static android.view.animation.Animation makeFadeAnimation(android.content.Context context, float start, float end) {
        android.view.animation.AlphaAnimation anim = new android.view.animation.AlphaAnimation(start, end);
        anim.setInterpolator(DECELERATE_CUBIC);
        anim.setDuration(220);
        return anim;
    }

    /* access modifiers changed from: 0000 */
    public android.view.animation.Animation loadAnimation(android.support.v4.app.Fragment fragment, int transit, boolean enter, int transitionStyle) {
        android.view.animation.Animation animObj = fragment.onCreateAnimation(transit, enter, fragment.getNextAnim());
        if (animObj != null) {
            return animObj;
        }
        if (fragment.getNextAnim() != 0) {
            android.view.animation.Animation anim = android.view.animation.AnimationUtils.loadAnimation(this.mHost.getContext(), fragment.getNextAnim());
            if (anim != null) {
                return anim;
            }
        }
        if (transit == 0) {
            return null;
        }
        int styleIndex = transitToStyleIndex(transit, enter);
        if (styleIndex < 0) {
            return null;
        }
        switch (styleIndex) {
            case 1:
                return makeOpenCloseAnimation(this.mHost.getContext(), 1.125f, 1.0f, 0.0f, 1.0f);
            case 2:
                return makeOpenCloseAnimation(this.mHost.getContext(), 1.0f, 0.975f, 1.0f, 0.0f);
            case 3:
                return makeOpenCloseAnimation(this.mHost.getContext(), 0.975f, 1.0f, 0.0f, 1.0f);
            case 4:
                return makeOpenCloseAnimation(this.mHost.getContext(), 1.0f, 1.075f, 1.0f, 0.0f);
            case 5:
                return makeFadeAnimation(this.mHost.getContext(), 0.0f, 1.0f);
            case 6:
                return makeFadeAnimation(this.mHost.getContext(), 1.0f, 0.0f);
            default:
                if (transitionStyle == 0 && this.mHost.onHasWindowAnimations()) {
                    transitionStyle = this.mHost.onGetWindowAnimations();
                }
                if (transitionStyle == 0) {
                    return null;
                }
                return null;
        }
    }

    public void performPendingDeferredStart(android.support.v4.app.Fragment f) {
        if (!f.mDeferStart) {
            return;
        }
        if (this.mExecutingActions) {
            this.mHavePendingDeferredStart = true;
            return;
        }
        f.mDeferStart = false;
        moveToState(f, this.mCurState, 0, 0, false);
    }

    private void setHWLayerAnimListenerIfAlpha(android.view.View v, android.view.animation.Animation anim) {
        if (v != null && anim != null && shouldRunOnHWLayer(v, anim)) {
            android.view.animation.Animation.AnimationListener originalListener = null;
            try {
                if (sAnimationListenerField == null) {
                    sAnimationListenerField = android.view.animation.Animation.class.getDeclaredField("mListener");
                    sAnimationListenerField.setAccessible(true);
                }
                originalListener = (android.view.animation.Animation.AnimationListener) sAnimationListenerField.get(anim);
            } catch (java.lang.NoSuchFieldException e) {
                android.util.Log.e(TAG, "No field with the name mListener is found in Animation class", e);
            } catch (java.lang.IllegalAccessException e2) {
                android.util.Log.e(TAG, "Cannot access Animation's mListener field", e2);
            }
            android.support.v4.view.ViewCompat.setLayerType(v, 2, null);
            anim.setAnimationListener(new android.support.v4.app.FragmentManagerImpl.AnimateOnHWLayerIfNeededListener(v, anim, originalListener));
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean isStateAtLeast(int state) {
        return this.mCurState >= state;
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Code restructure failed: missing block: B:130:0x0464, code lost:
        r14 = android.support.v4.os.EnvironmentCompat.MEDIA_UNKNOWN;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:139:0x0494, code lost:
        if (r19 >= 1) goto L_0x0071;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:141:0x049a, code lost:
        if (r17.mDestroyed == false) goto L_0x04af;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:143:0x04a0, code lost:
        if (r18.getAnimatingAway() == null) goto L_0x04af;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:144:0x04a2, code lost:
        r15 = r18.getAnimatingAway();
        r18.setAnimatingAway(null);
        r15.clearAnimation();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:146:0x04b3, code lost:
        if (r18.getAnimatingAway() == null) goto L_0x060a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:147:0x04b5, code lost:
        r18.setStateAfterAnimating(r19);
        r19 = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:155:0x04ed, code lost:
        if (r19 >= 4) goto L_0x0518;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:157:0x04f1, code lost:
        if (DEBUG == false) goto L_0x050d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:158:0x04f3, code lost:
        android.util.Log.v(TAG, "movefrom STARTED: " + r18);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:159:0x050d, code lost:
        r18.performStop();
        dispatchOnFragmentStopped(r18, false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:161:0x051b, code lost:
        if (r19 >= 3) goto L_0x053e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:163:0x051f, code lost:
        if (DEBUG == false) goto L_0x053b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:164:0x0521, code lost:
        android.util.Log.v(TAG, "movefrom STOPPED: " + r18);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:165:0x053b, code lost:
        r18.performReallyStop();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:167:0x0541, code lost:
        if (r19 >= 2) goto L_0x0491;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:169:0x0545, code lost:
        if (DEBUG == false) goto L_0x0561;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:170:0x0547, code lost:
        android.util.Log.v(TAG, "movefrom ACTIVITY_CREATED: " + r18);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:172:0x0565, code lost:
        if (r18.mView == null) goto L_0x057c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:174:0x0571, code lost:
        if (r17.mHost.onShouldSaveFragmentState(r18) == false) goto L_0x057c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:176:0x0577, code lost:
        if (r18.mSavedViewState != null) goto L_0x057c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:177:0x0579, code lost:
        saveFragmentViewState(r18);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:178:0x057c, code lost:
        r18.performDestroyView();
        dispatchOnFragmentViewDestroyed(r18, false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:179:0x058b, code lost:
        if (r18.mView == null) goto L_0x05f9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:181:0x0591, code lost:
        if (r18.mContainer == null) goto L_0x05f9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:182:0x0593, code lost:
        r10 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:183:0x0598, code lost:
        if (r17.mCurState <= 0) goto L_0x05c0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:185:0x059e, code lost:
        if (r17.mDestroyed != false) goto L_0x05c0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:187:0x05a8, code lost:
        if (r18.mView.getVisibility() != 0) goto L_0x05c0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:189:0x05b1, code lost:
        if (r18.mPostponedAlpha < 0.0f) goto L_0x05c0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:190:0x05b3, code lost:
        r10 = loadAnimation(r18, r20, false, r21);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:191:0x05c0, code lost:
        r18.mPostponedAlpha = 0.0f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:192:0x05c5, code lost:
        if (r10 == null) goto L_0x05ee;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:193:0x05c7, code lost:
        r13 = r18;
        r18.setAnimatingAway(r18.mView);
        r18.setStateAfterAnimating(r19);
        r10.setAnimationListener(new android.support.v4.app.FragmentManagerImpl.AnonymousClass2(r17, r18.mView, r10));
        r18.mView.startAnimation(r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:194:0x05ee, code lost:
        r18.mContainer.removeView(r18.mView);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:195:0x05f9, code lost:
        r18.mContainer = null;
        r18.mView = null;
        r18.mInnerView = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:197:0x060c, code lost:
        if (DEBUG == false) goto L_0x0628;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:198:0x060e, code lost:
        android.util.Log.v(TAG, "movefrom CREATED: " + r18);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:200:0x062c, code lost:
        if (r18.mRetaining != false) goto L_0x0651;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:201:0x062e, code lost:
        r18.performDestroy();
        dispatchOnFragmentDestroyed(r18, false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:202:0x0639, code lost:
        r18.performDetach();
        dispatchOnFragmentDetached(r18, false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:203:0x0644, code lost:
        if (r22 != false) goto L_0x0071;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:205:0x064a, code lost:
        if (r18.mRetaining != false) goto L_0x0657;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:206:0x064c, code lost:
        makeInactive(r18);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:207:0x0651, code lost:
        r18.mState = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:208:0x0657, code lost:
        r18.mHost = null;
        r18.mParentFragment = null;
        r18.mFragmentManager = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x0260, code lost:
        if (r19 <= 1) goto L_0x03c1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x0264, code lost:
        if (DEBUG == false) goto L_0x0280;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x0266, code lost:
        android.util.Log.v(TAG, "moveto ACTIVITY_CREATED: " + r18);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x0284, code lost:
        if (r18.mFromLayout != false) goto L_0x0398;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x0286, code lost:
        r11 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x028b, code lost:
        if (r18.mContainerId == 0) goto L_0x0316;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x0292, code lost:
        if (r18.mContainerId != -1) goto L_0x02b9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x0294, code lost:
        throwException(new java.lang.IllegalArgumentException("Cannot create fragment " + r18 + " for a container view with no id"));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x02b9, code lost:
        r11 = (android.view.ViewGroup) r17.mContainer.onFindViewById(r18.mContainerId);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x02c7, code lost:
        if (r11 != null) goto L_0x0316;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x02cd, code lost:
        if (r18.mRestored != false) goto L_0x0316;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:?, code lost:
        r14 = r18.getResources().getResourceName(r18.mContainerId);
     */
    /* JADX WARNING: Removed duplicated region for block: B:210:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0079  */
    public void moveToState(android.support.v4.app.Fragment f, int newState, int transit, int transitionStyle, boolean keepActive) {
        android.view.ViewGroup container;
        java.lang.String resName;
        android.support.v4.app.FragmentManagerImpl fragmentManagerImpl;
        if ((!f.mAdded || f.mDetached) && newState > 1) {
            newState = 1;
        }
        if (f.mRemoving && newState > f.mState) {
            newState = f.mState;
        }
        if (f.mDeferStart && f.mState < 4 && newState > 3) {
            newState = 3;
        }
        if (f.mState >= newState) {
            if (f.mState > newState) {
                switch (f.mState) {
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                    case 5:
                        if (newState < 5) {
                            if (DEBUG) {
                                android.util.Log.v(TAG, "movefrom RESUMED: " + f);
                            }
                            f.performPause();
                            dispatchOnFragmentPaused(f, false);
                            break;
                        }
                        break;
                }
            }
        } else if (!f.mFromLayout || f.mInLayout) {
            if (f.getAnimatingAway() != null) {
                f.setAnimatingAway(null);
                moveToState(f, f.getStateAfterAnimating(), 0, 0, true);
            }
            switch (f.mState) {
                case 0:
                    if (DEBUG) {
                        android.util.Log.v(TAG, "moveto CREATED: " + f);
                    }
                    if (f.mSavedFragmentState != null) {
                        f.mSavedFragmentState.setClassLoader(this.mHost.getContext().getClassLoader());
                        f.mSavedViewState = f.mSavedFragmentState.getSparseParcelableArray(VIEW_STATE_TAG);
                        f.mTarget = getFragment(f.mSavedFragmentState, TARGET_STATE_TAG);
                        if (f.mTarget != null) {
                            f.mTargetRequestCode = f.mSavedFragmentState.getInt(TARGET_REQUEST_CODE_STATE_TAG, 0);
                        }
                        f.mUserVisibleHint = f.mSavedFragmentState.getBoolean(USER_VISIBLE_HINT_TAG, true);
                        if (!f.mUserVisibleHint) {
                            f.mDeferStart = true;
                            if (newState > 3) {
                                newState = 3;
                            }
                        }
                    }
                    f.mHost = this.mHost;
                    f.mParentFragment = this.mParent;
                    if (this.mParent != null) {
                        fragmentManagerImpl = this.mParent.mChildFragmentManager;
                    } else {
                        fragmentManagerImpl = this.mHost.getFragmentManagerImpl();
                    }
                    f.mFragmentManager = fragmentManagerImpl;
                    dispatchOnFragmentPreAttached(f, this.mHost.getContext(), false);
                    f.mCalled = false;
                    f.onAttach(this.mHost.getContext());
                    if (f.mCalled) {
                        if (f.mParentFragment == null) {
                            this.mHost.onAttachFragment(f);
                        } else {
                            f.mParentFragment.onAttachFragment(f);
                        }
                        dispatchOnFragmentAttached(f, this.mHost.getContext(), false);
                        if (!f.mRetaining) {
                            f.performCreate(f.mSavedFragmentState);
                            dispatchOnFragmentCreated(f, f.mSavedFragmentState, false);
                        } else {
                            f.restoreChildFragmentState(f.mSavedFragmentState);
                            f.mState = 1;
                        }
                        f.mRetaining = false;
                        if (f.mFromLayout) {
                            f.mView = f.performCreateView(f.getLayoutInflater(f.mSavedFragmentState), null, f.mSavedFragmentState);
                            if (f.mView == null) {
                                f.mInnerView = null;
                                break;
                            } else {
                                f.mInnerView = f.mView;
                                if (android.os.Build.VERSION.SDK_INT >= 11) {
                                    android.support.v4.view.ViewCompat.setSaveFromParentEnabled(f.mView, false);
                                } else {
                                    f.mView = android.support.v4.app.NoSaveStateFrameLayout.wrap(f.mView);
                                }
                                if (f.mHidden) {
                                    f.mView.setVisibility(8);
                                }
                                f.onViewCreated(f.mView, f.mSavedFragmentState);
                                dispatchOnFragmentViewCreated(f, f.mView, f.mSavedFragmentState, false);
                                break;
                            }
                        }
                    } else {
                        throw new android.support.v4.app.SuperNotCalledException("Fragment " + f + " did not call through to super.onAttach()");
                    }
                    break;
                case 1:
                    break;
                case 2:
                    break;
                case 3:
                    break;
                case 4:
                    break;
            }
        } else {
            return;
        }
        if (f.mState == newState) {
            android.util.Log.w(TAG, "moveToState: Fragment state for " + f + " not updated inline; " + "expected state " + newState + " found " + f.mState);
            f.mState = newState;
            return;
        }
        return;
        throwException(new java.lang.IllegalArgumentException("No view found for id 0x" + java.lang.Integer.toHexString(f.mContainerId) + " (" + resName + ") for fragment " + f));
        f.mContainer = container;
        f.mView = f.performCreateView(f.getLayoutInflater(f.mSavedFragmentState), container, f.mSavedFragmentState);
        if (f.mView != null) {
            f.mInnerView = f.mView;
            if (android.os.Build.VERSION.SDK_INT >= 11) {
                android.support.v4.view.ViewCompat.setSaveFromParentEnabled(f.mView, false);
            } else {
                f.mView = android.support.v4.app.NoSaveStateFrameLayout.wrap(f.mView);
            }
            if (container != null) {
                container.addView(f.mView);
            }
            if (f.mHidden) {
                f.mView.setVisibility(8);
            }
            f.onViewCreated(f.mView, f.mSavedFragmentState);
            dispatchOnFragmentViewCreated(f, f.mView, f.mSavedFragmentState, false);
            f.mIsNewlyAdded = f.mView.getVisibility() == 0 && f.mContainer != null;
        } else {
            f.mInnerView = null;
        }
        f.performActivityCreated(f.mSavedFragmentState);
        dispatchOnFragmentActivityCreated(f, f.mSavedFragmentState, false);
        if (f.mView != null) {
            f.restoreViewState(f.mSavedFragmentState);
        }
        f.mSavedFragmentState = null;
        if (newState > 2) {
            f.mState = 3;
        }
        if (newState > 3) {
            if (DEBUG) {
                android.util.Log.v(TAG, "moveto STARTED: " + f);
            }
            f.performStart();
            dispatchOnFragmentStarted(f, false);
        }
        if (newState > 4) {
            if (DEBUG) {
                android.util.Log.v(TAG, "moveto RESUMED: " + f);
            }
            f.performResume();
            dispatchOnFragmentResumed(f, false);
            f.mSavedFragmentState = null;
            f.mSavedViewState = null;
        }
        if (f.mState == newState) {
        }
    }

    /* access modifiers changed from: 0000 */
    public void moveToState(android.support.v4.app.Fragment f) {
        moveToState(f, this.mCurState, 0, 0, false);
    }

    /* access modifiers changed from: 0000 */
    public void completeShowHideFragment(android.support.v4.app.Fragment fragment) {
        int visibility;
        if (fragment.mView != null) {
            android.view.animation.Animation anim = loadAnimation(fragment, fragment.getNextTransition(), !fragment.mHidden, fragment.getNextTransitionStyle());
            if (anim != null) {
                setHWLayerAnimListenerIfAlpha(fragment.mView, anim);
                fragment.mView.startAnimation(anim);
                setHWLayerAnimListenerIfAlpha(fragment.mView, anim);
                anim.start();
            }
            if (!fragment.mHidden || fragment.isHideReplaced()) {
                visibility = 0;
            } else {
                visibility = 8;
            }
            fragment.mView.setVisibility(visibility);
            if (fragment.isHideReplaced()) {
                fragment.setHideReplaced(false);
            }
        }
        if (fragment.mAdded && fragment.mHasMenu && fragment.mMenuVisible) {
            this.mNeedMenuInvalidate = true;
        }
        fragment.mHiddenChanged = false;
        fragment.onHiddenChanged(fragment.mHidden);
    }

    /* access modifiers changed from: 0000 */
    public void moveFragmentToExpectedState(android.support.v4.app.Fragment f) {
        if (f != null) {
            int nextState = this.mCurState;
            if (f.mRemoving) {
                if (f.isInBackStack()) {
                    nextState = java.lang.Math.min(nextState, 1);
                } else {
                    nextState = java.lang.Math.min(nextState, 0);
                }
            }
            moveToState(f, nextState, f.getNextTransition(), f.getNextTransitionStyle(), false);
            if (f.mView != null) {
                android.support.v4.app.Fragment underFragment = findFragmentUnder(f);
                if (underFragment != null) {
                    android.view.View underView = underFragment.mView;
                    android.view.ViewGroup container = f.mContainer;
                    int underIndex = container.indexOfChild(underView);
                    int viewIndex = container.indexOfChild(f.mView);
                    if (viewIndex < underIndex) {
                        container.removeViewAt(viewIndex);
                        container.addView(f.mView, underIndex);
                    }
                }
                if (f.mIsNewlyAdded && f.mContainer != null) {
                    if (android.os.Build.VERSION.SDK_INT < 11) {
                        f.mView.setVisibility(0);
                    } else if (f.mPostponedAlpha > 0.0f) {
                        f.mView.setAlpha(f.mPostponedAlpha);
                    }
                    f.mPostponedAlpha = 0.0f;
                    f.mIsNewlyAdded = false;
                    android.view.animation.Animation anim = loadAnimation(f, f.getNextTransition(), true, f.getNextTransitionStyle());
                    if (anim != null) {
                        setHWLayerAnimListenerIfAlpha(f.mView, anim);
                        f.mView.startAnimation(anim);
                    }
                }
            }
            if (f.mHiddenChanged) {
                completeShowHideFragment(f);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void moveToState(int newState, boolean always) {
        if (this.mHost == null && newState != 0) {
            throw new java.lang.IllegalStateException("No activity");
        } else if (always || newState != this.mCurState) {
            this.mCurState = newState;
            if (this.mActive != null) {
                boolean loadersRunning = false;
                if (this.mAdded != null) {
                    int numAdded = this.mAdded.size();
                    for (int i = 0; i < numAdded; i++) {
                        android.support.v4.app.Fragment f = (android.support.v4.app.Fragment) this.mAdded.get(i);
                        moveFragmentToExpectedState(f);
                        if (f.mLoaderManager != null) {
                            loadersRunning |= f.mLoaderManager.hasRunningLoaders();
                        }
                    }
                }
                int numActive = this.mActive.size();
                for (int i2 = 0; i2 < numActive; i2++) {
                    android.support.v4.app.Fragment f2 = (android.support.v4.app.Fragment) this.mActive.get(i2);
                    if (f2 != null && ((f2.mRemoving || f2.mDetached) && !f2.mIsNewlyAdded)) {
                        moveFragmentToExpectedState(f2);
                        if (f2.mLoaderManager != null) {
                            loadersRunning |= f2.mLoaderManager.hasRunningLoaders();
                        }
                    }
                }
                if (!loadersRunning) {
                    startPendingDeferredFragments();
                }
                if (this.mNeedMenuInvalidate && this.mHost != null && this.mCurState == 5) {
                    this.mHost.onSupportInvalidateOptionsMenu();
                    this.mNeedMenuInvalidate = false;
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void startPendingDeferredFragments() {
        if (this.mActive != null) {
            for (int i = 0; i < this.mActive.size(); i++) {
                android.support.v4.app.Fragment f = (android.support.v4.app.Fragment) this.mActive.get(i);
                if (f != null) {
                    performPendingDeferredStart(f);
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void makeActive(android.support.v4.app.Fragment f) {
        if (f.mIndex < 0) {
            if (this.mAvailIndices == null || this.mAvailIndices.size() <= 0) {
                if (this.mActive == null) {
                    this.mActive = new java.util.ArrayList<>();
                }
                f.setIndex(this.mActive.size(), this.mParent);
                this.mActive.add(f);
            } else {
                f.setIndex(((java.lang.Integer) this.mAvailIndices.remove(this.mAvailIndices.size() - 1)).intValue(), this.mParent);
                this.mActive.set(f.mIndex, f);
            }
            if (DEBUG) {
                android.util.Log.v(TAG, "Allocated fragment index " + f);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void makeInactive(android.support.v4.app.Fragment f) {
        if (f.mIndex >= 0) {
            if (DEBUG) {
                android.util.Log.v(TAG, "Freeing fragment index " + f);
            }
            this.mActive.set(f.mIndex, null);
            if (this.mAvailIndices == null) {
                this.mAvailIndices = new java.util.ArrayList<>();
            }
            this.mAvailIndices.add(java.lang.Integer.valueOf(f.mIndex));
            this.mHost.inactivateFragment(f.mWho);
            f.initState();
        }
    }

    public void addFragment(android.support.v4.app.Fragment fragment, boolean moveToStateNow) {
        if (this.mAdded == null) {
            this.mAdded = new java.util.ArrayList<>();
        }
        if (DEBUG) {
            android.util.Log.v(TAG, "add: " + fragment);
        }
        makeActive(fragment);
        if (fragment.mDetached) {
            return;
        }
        if (this.mAdded.contains(fragment)) {
            throw new java.lang.IllegalStateException("Fragment already added: " + fragment);
        }
        this.mAdded.add(fragment);
        fragment.mAdded = true;
        fragment.mRemoving = false;
        if (fragment.mView == null) {
            fragment.mHiddenChanged = false;
        }
        if (fragment.mHasMenu && fragment.mMenuVisible) {
            this.mNeedMenuInvalidate = true;
        }
        if (moveToStateNow) {
            moveToState(fragment);
        }
    }

    public void removeFragment(android.support.v4.app.Fragment fragment) {
        boolean inactive;
        if (DEBUG) {
            android.util.Log.v(TAG, "remove: " + fragment + " nesting=" + fragment.mBackStackNesting);
        }
        if (!fragment.isInBackStack()) {
            inactive = true;
        } else {
            inactive = false;
        }
        if (!fragment.mDetached || inactive) {
            if (this.mAdded != null) {
                this.mAdded.remove(fragment);
            }
            if (fragment.mHasMenu && fragment.mMenuVisible) {
                this.mNeedMenuInvalidate = true;
            }
            fragment.mAdded = false;
            fragment.mRemoving = true;
        }
    }

    public void hideFragment(android.support.v4.app.Fragment fragment) {
        boolean z = true;
        if (DEBUG) {
            android.util.Log.v(TAG, "hide: " + fragment);
        }
        if (!fragment.mHidden) {
            fragment.mHidden = true;
            if (fragment.mHiddenChanged) {
                z = false;
            }
            fragment.mHiddenChanged = z;
        }
    }

    public void showFragment(android.support.v4.app.Fragment fragment) {
        boolean z = false;
        if (DEBUG) {
            android.util.Log.v(TAG, "show: " + fragment);
        }
        if (fragment.mHidden) {
            fragment.mHidden = false;
            if (!fragment.mHiddenChanged) {
                z = true;
            }
            fragment.mHiddenChanged = z;
        }
    }

    public void detachFragment(android.support.v4.app.Fragment fragment) {
        if (DEBUG) {
            android.util.Log.v(TAG, "detach: " + fragment);
        }
        if (!fragment.mDetached) {
            fragment.mDetached = true;
            if (fragment.mAdded) {
                if (this.mAdded != null) {
                    if (DEBUG) {
                        android.util.Log.v(TAG, "remove from detach: " + fragment);
                    }
                    this.mAdded.remove(fragment);
                }
                if (fragment.mHasMenu && fragment.mMenuVisible) {
                    this.mNeedMenuInvalidate = true;
                }
                fragment.mAdded = false;
            }
        }
    }

    public void attachFragment(android.support.v4.app.Fragment fragment) {
        if (DEBUG) {
            android.util.Log.v(TAG, "attach: " + fragment);
        }
        if (fragment.mDetached) {
            fragment.mDetached = false;
            if (!fragment.mAdded) {
                if (this.mAdded == null) {
                    this.mAdded = new java.util.ArrayList<>();
                }
                if (this.mAdded.contains(fragment)) {
                    throw new java.lang.IllegalStateException("Fragment already added: " + fragment);
                }
                if (DEBUG) {
                    android.util.Log.v(TAG, "add from attach: " + fragment);
                }
                this.mAdded.add(fragment);
                fragment.mAdded = true;
                if (fragment.mHasMenu && fragment.mMenuVisible) {
                    this.mNeedMenuInvalidate = true;
                }
            }
        }
    }

    public android.support.v4.app.Fragment findFragmentById(int id) {
        if (this.mAdded != null) {
            for (int i = this.mAdded.size() - 1; i >= 0; i--) {
                android.support.v4.app.Fragment f = (android.support.v4.app.Fragment) this.mAdded.get(i);
                if (f != null && f.mFragmentId == id) {
                    return f;
                }
            }
        }
        if (this.mActive != null) {
            for (int i2 = this.mActive.size() - 1; i2 >= 0; i2--) {
                android.support.v4.app.Fragment f2 = (android.support.v4.app.Fragment) this.mActive.get(i2);
                if (f2 != null && f2.mFragmentId == id) {
                    return f2;
                }
            }
        }
        return null;
    }

    public android.support.v4.app.Fragment findFragmentByTag(java.lang.String tag) {
        if (!(this.mAdded == null || tag == null)) {
            for (int i = this.mAdded.size() - 1; i >= 0; i--) {
                android.support.v4.app.Fragment f = (android.support.v4.app.Fragment) this.mAdded.get(i);
                if (f != null && tag.equals(f.mTag)) {
                    return f;
                }
            }
        }
        if (!(this.mActive == null || tag == null)) {
            for (int i2 = this.mActive.size() - 1; i2 >= 0; i2--) {
                android.support.v4.app.Fragment f2 = (android.support.v4.app.Fragment) this.mActive.get(i2);
                if (f2 != null && tag.equals(f2.mTag)) {
                    return f2;
                }
            }
        }
        return null;
    }

    public android.support.v4.app.Fragment findFragmentByWho(java.lang.String who) {
        if (!(this.mActive == null || who == null)) {
            for (int i = this.mActive.size() - 1; i >= 0; i--) {
                android.support.v4.app.Fragment f = (android.support.v4.app.Fragment) this.mActive.get(i);
                if (f != null) {
                    android.support.v4.app.Fragment f2 = f.findFragmentByWho(who);
                    if (f2 != null) {
                        return f2;
                    }
                }
            }
        }
        return null;
    }

    private void checkStateLoss() {
        if (this.mStateSaved) {
            throw new java.lang.IllegalStateException("Can not perform this action after onSaveInstanceState");
        } else if (this.mNoTransactionsBecause != null) {
            throw new java.lang.IllegalStateException("Can not perform this action inside of " + this.mNoTransactionsBecause);
        }
    }

    public void enqueueAction(android.support.v4.app.FragmentManagerImpl.OpGenerator action, boolean allowStateLoss) {
        if (!allowStateLoss) {
            checkStateLoss();
        }
        synchronized (this) {
            if (this.mDestroyed || this.mHost == null) {
                throw new java.lang.IllegalStateException("Activity has been destroyed");
            }
            if (this.mPendingActions == null) {
                this.mPendingActions = new java.util.ArrayList<>();
            }
            this.mPendingActions.add(action);
            scheduleCommit();
        }
    }

    /* access modifiers changed from: private */
    public void scheduleCommit() {
        boolean postponeReady;
        boolean pendingReady = true;
        synchronized (this) {
            if (this.mPostponedTransactions == null || this.mPostponedTransactions.isEmpty()) {
                postponeReady = false;
            } else {
                postponeReady = true;
            }
            if (this.mPendingActions == null || this.mPendingActions.size() != 1) {
                pendingReady = false;
            }
            if (postponeReady || pendingReady) {
                this.mHost.getHandler().removeCallbacks(this.mExecCommit);
                this.mHost.getHandler().post(this.mExecCommit);
            }
        }
    }

    public int allocBackStackIndex(android.support.v4.app.BackStackRecord bse) {
        synchronized (this) {
            if (this.mAvailBackStackIndices == null || this.mAvailBackStackIndices.size() <= 0) {
                if (this.mBackStackIndices == null) {
                    this.mBackStackIndices = new java.util.ArrayList<>();
                }
                int index = this.mBackStackIndices.size();
                if (DEBUG) {
                    android.util.Log.v(TAG, "Setting back stack index " + index + " to " + bse);
                }
                this.mBackStackIndices.add(bse);
                return index;
            }
            int index2 = ((java.lang.Integer) this.mAvailBackStackIndices.remove(this.mAvailBackStackIndices.size() - 1)).intValue();
            if (DEBUG) {
                android.util.Log.v(TAG, "Adding back stack index " + index2 + " with " + bse);
            }
            this.mBackStackIndices.set(index2, bse);
            return index2;
        }
    }

    public void setBackStackIndex(int index, android.support.v4.app.BackStackRecord bse) {
        synchronized (this) {
            if (this.mBackStackIndices == null) {
                this.mBackStackIndices = new java.util.ArrayList<>();
            }
            int N = this.mBackStackIndices.size();
            if (index < N) {
                if (DEBUG) {
                    android.util.Log.v(TAG, "Setting back stack index " + index + " to " + bse);
                }
                this.mBackStackIndices.set(index, bse);
            } else {
                while (N < index) {
                    this.mBackStackIndices.add(null);
                    if (this.mAvailBackStackIndices == null) {
                        this.mAvailBackStackIndices = new java.util.ArrayList<>();
                    }
                    if (DEBUG) {
                        android.util.Log.v(TAG, "Adding available back stack index " + N);
                    }
                    this.mAvailBackStackIndices.add(java.lang.Integer.valueOf(N));
                    N++;
                }
                if (DEBUG) {
                    android.util.Log.v(TAG, "Adding back stack index " + index + " with " + bse);
                }
                this.mBackStackIndices.add(bse);
            }
        }
    }

    public void freeBackStackIndex(int index) {
        synchronized (this) {
            this.mBackStackIndices.set(index, null);
            if (this.mAvailBackStackIndices == null) {
                this.mAvailBackStackIndices = new java.util.ArrayList<>();
            }
            if (DEBUG) {
                android.util.Log.v(TAG, "Freeing back stack index " + index);
            }
            this.mAvailBackStackIndices.add(java.lang.Integer.valueOf(index));
        }
    }

    private void ensureExecReady(boolean allowStateLoss) {
        if (this.mExecutingActions) {
            throw new java.lang.IllegalStateException("FragmentManager is already executing transactions");
        } else if (android.os.Looper.myLooper() != this.mHost.getHandler().getLooper()) {
            throw new java.lang.IllegalStateException("Must be called from main thread of fragment host");
        } else {
            if (!allowStateLoss) {
                checkStateLoss();
            }
            if (this.mTmpRecords == null) {
                this.mTmpRecords = new java.util.ArrayList<>();
                this.mTmpIsPop = new java.util.ArrayList<>();
            }
            this.mExecutingActions = true;
            try {
                executePostponedTransaction(null, null);
            } finally {
                this.mExecutingActions = false;
            }
        }
    }

    public void execSingleAction(android.support.v4.app.FragmentManagerImpl.OpGenerator action, boolean allowStateLoss) {
        ensureExecReady(allowStateLoss);
        if (action.generateOps(this.mTmpRecords, this.mTmpIsPop)) {
            this.mExecutingActions = true;
            try {
                optimizeAndExecuteOps(this.mTmpRecords, this.mTmpIsPop);
            } finally {
                cleanupExec();
            }
        }
        doPendingDeferredStart();
    }

    private void cleanupExec() {
        this.mExecutingActions = false;
        this.mTmpIsPop.clear();
        this.mTmpRecords.clear();
    }

    /* JADX INFO: finally extract failed */
    public boolean execPendingActions() {
        ensureExecReady(true);
        boolean didSomething = false;
        while (generateOpsForPendingActions(this.mTmpRecords, this.mTmpIsPop)) {
            this.mExecutingActions = true;
            try {
                optimizeAndExecuteOps(this.mTmpRecords, this.mTmpIsPop);
                cleanupExec();
                didSomething = true;
            } catch (Throwable th) {
                cleanupExec();
                throw th;
            }
        }
        doPendingDeferredStart();
        return didSomething;
    }

    private void executePostponedTransaction(java.util.ArrayList<android.support.v4.app.BackStackRecord> records, java.util.ArrayList<java.lang.Boolean> isRecordPop) {
        int numPostponed = this.mPostponedTransactions == null ? 0 : this.mPostponedTransactions.size();
        int i = 0;
        while (i < numPostponed) {
            android.support.v4.app.FragmentManagerImpl.StartEnterTransitionListener listener = (android.support.v4.app.FragmentManagerImpl.StartEnterTransitionListener) this.mPostponedTransactions.get(i);
            if (records != null && !listener.mIsBack) {
                int index = records.indexOf(listener.mRecord);
                if (index != -1 && ((java.lang.Boolean) isRecordPop.get(index)).booleanValue()) {
                    listener.cancelTransaction();
                    i++;
                }
            }
            if (listener.isReady() || (records != null && listener.mRecord.interactsWith(records, 0, records.size()))) {
                this.mPostponedTransactions.remove(i);
                i--;
                numPostponed--;
                if (records != null && !listener.mIsBack) {
                    int index2 = records.indexOf(listener.mRecord);
                    if (index2 != -1 && ((java.lang.Boolean) isRecordPop.get(index2)).booleanValue()) {
                        listener.cancelTransaction();
                    }
                }
                listener.completeTransaction();
            }
            i++;
        }
    }

    private void optimizeAndExecuteOps(java.util.ArrayList<android.support.v4.app.BackStackRecord> records, java.util.ArrayList<java.lang.Boolean> isRecordPop) {
        if (records != null && !records.isEmpty()) {
            if (isRecordPop == null || records.size() != isRecordPop.size()) {
                throw new java.lang.IllegalStateException("Internal error with the back stack records");
            }
            executePostponedTransaction(records, isRecordPop);
            int numRecords = records.size();
            int startIndex = 0;
            int recordNum = 0;
            while (recordNum < numRecords) {
                if (!((android.support.v4.app.BackStackRecord) records.get(recordNum)).mAllowOptimization) {
                    if (startIndex != recordNum) {
                        executeOpsTogether(records, isRecordPop, startIndex, recordNum);
                    }
                    int optimizeEnd = recordNum + 1;
                    if (((java.lang.Boolean) isRecordPop.get(recordNum)).booleanValue()) {
                        while (optimizeEnd < numRecords && ((java.lang.Boolean) isRecordPop.get(optimizeEnd)).booleanValue() && !((android.support.v4.app.BackStackRecord) records.get(optimizeEnd)).mAllowOptimization) {
                            optimizeEnd++;
                        }
                    }
                    executeOpsTogether(records, isRecordPop, recordNum, optimizeEnd);
                    startIndex = optimizeEnd;
                    recordNum = optimizeEnd - 1;
                }
                recordNum++;
            }
            if (startIndex != numRecords) {
                executeOpsTogether(records, isRecordPop, startIndex, numRecords);
            }
        }
    }

    private void executeOpsTogether(java.util.ArrayList<android.support.v4.app.BackStackRecord> records, java.util.ArrayList<java.lang.Boolean> isRecordPop, int startIndex, int endIndex) {
        boolean allowOptimization = ((android.support.v4.app.BackStackRecord) records.get(startIndex)).mAllowOptimization;
        boolean addToBackStack = false;
        if (this.mTmpAddedFragments == null) {
            this.mTmpAddedFragments = new java.util.ArrayList<>();
        } else {
            this.mTmpAddedFragments.clear();
        }
        if (this.mAdded != null) {
            this.mTmpAddedFragments.addAll(this.mAdded);
        }
        for (int recordNum = startIndex; recordNum < endIndex; recordNum++) {
            android.support.v4.app.BackStackRecord record = (android.support.v4.app.BackStackRecord) records.get(recordNum);
            if (!((java.lang.Boolean) isRecordPop.get(recordNum)).booleanValue()) {
                record.expandReplaceOps(this.mTmpAddedFragments);
            } else {
                record.trackAddedFragmentsInPop(this.mTmpAddedFragments);
            }
            if (addToBackStack || record.mAddToBackStack) {
                addToBackStack = true;
            } else {
                addToBackStack = false;
            }
        }
        this.mTmpAddedFragments.clear();
        if (!allowOptimization) {
            android.support.v4.app.FragmentTransition.startTransitions(this, records, isRecordPop, startIndex, endIndex, false);
        }
        executeOps(records, isRecordPop, startIndex, endIndex);
        int postponeIndex = endIndex;
        if (allowOptimization) {
            android.support.v4.util.ArraySet<android.support.v4.app.Fragment> addedFragments = new android.support.v4.util.ArraySet<>();
            addAddedFragments(addedFragments);
            postponeIndex = postponePostponableTransactions(records, isRecordPop, startIndex, endIndex, addedFragments);
            makeRemovedFragmentsInvisible(addedFragments);
        }
        if (postponeIndex != startIndex && allowOptimization) {
            android.support.v4.app.FragmentTransition.startTransitions(this, records, isRecordPop, startIndex, postponeIndex, true);
            moveToState(this.mCurState, true);
        }
        for (int recordNum2 = startIndex; recordNum2 < endIndex; recordNum2++) {
            android.support.v4.app.BackStackRecord record2 = (android.support.v4.app.BackStackRecord) records.get(recordNum2);
            if (((java.lang.Boolean) isRecordPop.get(recordNum2)).booleanValue() && record2.mIndex >= 0) {
                freeBackStackIndex(record2.mIndex);
                record2.mIndex = -1;
            }
        }
        if (addToBackStack) {
            reportBackStackChanged();
        }
    }

    private void makeRemovedFragmentsInvisible(android.support.v4.util.ArraySet<android.support.v4.app.Fragment> fragments) {
        int numAdded = fragments.size();
        for (int i = 0; i < numAdded; i++) {
            android.support.v4.app.Fragment fragment = (android.support.v4.app.Fragment) fragments.valueAt(i);
            if (!fragment.mAdded) {
                android.view.View view = fragment.getView();
                if (android.os.Build.VERSION.SDK_INT < 11) {
                    fragment.getView().setVisibility(4);
                } else {
                    fragment.mPostponedAlpha = view.getAlpha();
                    view.setAlpha(0.0f);
                }
            }
        }
    }

    private int postponePostponableTransactions(java.util.ArrayList<android.support.v4.app.BackStackRecord> records, java.util.ArrayList<java.lang.Boolean> isRecordPop, int startIndex, int endIndex, android.support.v4.util.ArraySet<android.support.v4.app.Fragment> added) {
        boolean isPostponed;
        int postponeIndex = endIndex;
        for (int i = endIndex - 1; i >= startIndex; i--) {
            android.support.v4.app.BackStackRecord record = (android.support.v4.app.BackStackRecord) records.get(i);
            boolean isPop = ((java.lang.Boolean) isRecordPop.get(i)).booleanValue();
            if (!record.isPostponed() || record.interactsWith(records, i + 1, endIndex)) {
                isPostponed = false;
            } else {
                isPostponed = true;
            }
            if (isPostponed) {
                if (this.mPostponedTransactions == null) {
                    this.mPostponedTransactions = new java.util.ArrayList<>();
                }
                android.support.v4.app.FragmentManagerImpl.StartEnterTransitionListener listener = new android.support.v4.app.FragmentManagerImpl.StartEnterTransitionListener(record, isPop);
                this.mPostponedTransactions.add(listener);
                record.setOnStartPostponedListener(listener);
                if (isPop) {
                    record.executeOps();
                } else {
                    record.executePopOps(false);
                }
                postponeIndex--;
                if (i != postponeIndex) {
                    records.remove(i);
                    records.add(postponeIndex, record);
                }
                addAddedFragments(added);
            }
        }
        return postponeIndex;
    }

    /* access modifiers changed from: private */
    public void completeExecute(android.support.v4.app.BackStackRecord record, boolean isPop, boolean runTransitions, boolean moveToState) {
        java.util.ArrayList<android.support.v4.app.BackStackRecord> records = new java.util.ArrayList<>(1);
        java.util.ArrayList<java.lang.Boolean> isRecordPop = new java.util.ArrayList<>(1);
        records.add(record);
        isRecordPop.add(java.lang.Boolean.valueOf(isPop));
        executeOps(records, isRecordPop, 0, 1);
        if (runTransitions) {
            android.support.v4.app.FragmentTransition.startTransitions(this, records, isRecordPop, 0, 1, true);
        }
        if (moveToState) {
            moveToState(this.mCurState, true);
        }
        if (this.mActive != null) {
            int numActive = this.mActive.size();
            for (int i = 0; i < numActive; i++) {
                android.support.v4.app.Fragment fragment = (android.support.v4.app.Fragment) this.mActive.get(i);
                if (fragment != null && fragment.mView != null && fragment.mIsNewlyAdded && record.interactsWith(fragment.mContainerId)) {
                    if (android.os.Build.VERSION.SDK_INT >= 11 && fragment.mPostponedAlpha > 0.0f) {
                        fragment.mView.setAlpha(fragment.mPostponedAlpha);
                    }
                    if (moveToState) {
                        fragment.mPostponedAlpha = 0.0f;
                    } else {
                        fragment.mPostponedAlpha = -1.0f;
                        fragment.mIsNewlyAdded = false;
                    }
                }
            }
        }
    }

    private android.support.v4.app.Fragment findFragmentUnder(android.support.v4.app.Fragment f) {
        android.view.ViewGroup container = f.mContainer;
        android.view.View view = f.mView;
        if (container == null || view == null) {
            return null;
        }
        for (int i = this.mAdded.indexOf(f) - 1; i >= 0; i--) {
            android.support.v4.app.Fragment underFragment = (android.support.v4.app.Fragment) this.mAdded.get(i);
            if (underFragment.mContainer == container && underFragment.mView != null) {
                return underFragment;
            }
        }
        return null;
    }

    private static void executeOps(java.util.ArrayList<android.support.v4.app.BackStackRecord> records, java.util.ArrayList<java.lang.Boolean> isRecordPop, int startIndex, int endIndex) {
        int i = startIndex;
        while (i < endIndex) {
            android.support.v4.app.BackStackRecord record = (android.support.v4.app.BackStackRecord) records.get(i);
            if (((java.lang.Boolean) isRecordPop.get(i)).booleanValue()) {
                record.bumpBackStackNesting(-1);
                record.executePopOps(i == endIndex + -1);
            } else {
                record.bumpBackStackNesting(1);
                record.executeOps();
            }
            i++;
        }
    }

    private void addAddedFragments(android.support.v4.util.ArraySet<android.support.v4.app.Fragment> added) {
        if (this.mCurState >= 1) {
            int state = java.lang.Math.min(this.mCurState, 4);
            int numAdded = this.mAdded == null ? 0 : this.mAdded.size();
            for (int i = 0; i < numAdded; i++) {
                android.support.v4.app.Fragment fragment = (android.support.v4.app.Fragment) this.mAdded.get(i);
                if (fragment.mState < state) {
                    moveToState(fragment, state, fragment.getNextAnim(), fragment.getNextTransition(), false);
                    if (fragment.mView != null && !fragment.mHidden && fragment.mIsNewlyAdded) {
                        added.add(fragment);
                    }
                }
            }
        }
    }

    private void forcePostponedTransactions() {
        if (this.mPostponedTransactions != null) {
            while (!this.mPostponedTransactions.isEmpty()) {
                ((android.support.v4.app.FragmentManagerImpl.StartEnterTransitionListener) this.mPostponedTransactions.remove(0)).completeTransaction();
            }
        }
    }

    private void endAnimatingAwayFragments() {
        int numFragments;
        if (this.mActive == null) {
            numFragments = 0;
        } else {
            numFragments = this.mActive.size();
        }
        for (int i = 0; i < numFragments; i++) {
            android.support.v4.app.Fragment fragment = (android.support.v4.app.Fragment) this.mActive.get(i);
            if (!(fragment == null || fragment.getAnimatingAway() == null)) {
                int stateAfterAnimating = fragment.getStateAfterAnimating();
                android.view.View animatingAway = fragment.getAnimatingAway();
                fragment.setAnimatingAway(null);
                android.view.animation.Animation animation = animatingAway.getAnimation();
                if (animation != null) {
                    animation.cancel();
                }
                moveToState(fragment, stateAfterAnimating, 0, 0, false);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0039, code lost:
        if (r1 <= 0) goto L_0x0040;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        return false;
     */
    private boolean generateOpsForPendingActions(java.util.ArrayList<android.support.v4.app.BackStackRecord> records, java.util.ArrayList<java.lang.Boolean> isPop) {
        synchronized (this) {
            if (this.mPendingActions != null && this.mPendingActions.size() != 0) {
                int numActions = this.mPendingActions.size();
                for (int i = 0; i < numActions; i++) {
                    ((android.support.v4.app.FragmentManagerImpl.OpGenerator) this.mPendingActions.get(i)).generateOps(records, isPop);
                }
                this.mPendingActions.clear();
                this.mHost.getHandler().removeCallbacks(this.mExecCommit);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void doPendingDeferredStart() {
        if (this.mHavePendingDeferredStart) {
            boolean loadersRunning = false;
            for (int i = 0; i < this.mActive.size(); i++) {
                android.support.v4.app.Fragment f = (android.support.v4.app.Fragment) this.mActive.get(i);
                if (!(f == null || f.mLoaderManager == null)) {
                    loadersRunning |= f.mLoaderManager.hasRunningLoaders();
                }
            }
            if (!loadersRunning) {
                this.mHavePendingDeferredStart = false;
                startPendingDeferredFragments();
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void reportBackStackChanged() {
        if (this.mBackStackChangeListeners != null) {
            for (int i = 0; i < this.mBackStackChangeListeners.size(); i++) {
                ((android.support.v4.app.FragmentManager.OnBackStackChangedListener) this.mBackStackChangeListeners.get(i)).onBackStackChanged();
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void addBackStackState(android.support.v4.app.BackStackRecord state) {
        if (this.mBackStack == null) {
            this.mBackStack = new java.util.ArrayList<>();
        }
        this.mBackStack.add(state);
        reportBackStackChanged();
    }

    /* access modifiers changed from: 0000 */
    public boolean popBackStackState(java.util.ArrayList<android.support.v4.app.BackStackRecord> records, java.util.ArrayList<java.lang.Boolean> isRecordPop, java.lang.String name, int id, int flags) {
        if (this.mBackStack == null) {
            return false;
        }
        if (name == null && id < 0 && (flags & 1) == 0) {
            int last = this.mBackStack.size() - 1;
            if (last < 0) {
                return false;
            }
            records.add(this.mBackStack.remove(last));
            isRecordPop.add(java.lang.Boolean.valueOf(true));
        } else {
            int index = -1;
            if (name != null || id >= 0) {
                int index2 = this.mBackStack.size() - 1;
                while (index >= 0) {
                    android.support.v4.app.BackStackRecord bss = (android.support.v4.app.BackStackRecord) this.mBackStack.get(index);
                    if ((name != null && name.equals(bss.getName())) || (id >= 0 && id == bss.mIndex)) {
                        break;
                    }
                    index2 = index - 1;
                }
                if (index < 0) {
                    return false;
                }
                if ((flags & 1) != 0) {
                    index--;
                    while (index >= 0) {
                        android.support.v4.app.BackStackRecord bss2 = (android.support.v4.app.BackStackRecord) this.mBackStack.get(index);
                        if ((name == null || !name.equals(bss2.getName())) && (id < 0 || id != bss2.mIndex)) {
                            break;
                        }
                        index--;
                    }
                }
            }
            if (index == this.mBackStack.size() - 1) {
                return false;
            }
            for (int i = this.mBackStack.size() - 1; i > index; i--) {
                records.add(this.mBackStack.remove(i));
                isRecordPop.add(java.lang.Boolean.valueOf(true));
            }
        }
        return true;
    }

    /* access modifiers changed from: 0000 */
    public android.support.v4.app.FragmentManagerNonConfig retainNonConfig() {
        java.util.ArrayList<android.support.v4.app.Fragment> fragments = null;
        java.util.ArrayList<android.support.v4.app.FragmentManagerNonConfig> childFragments = null;
        if (this.mActive != null) {
            for (int i = 0; i < this.mActive.size(); i++) {
                android.support.v4.app.Fragment f = (android.support.v4.app.Fragment) this.mActive.get(i);
                if (f != null) {
                    if (f.mRetainInstance) {
                        if (fragments == null) {
                            fragments = new java.util.ArrayList<>();
                        }
                        fragments.add(f);
                        f.mRetaining = true;
                        f.mTargetIndex = f.mTarget != null ? f.mTarget.mIndex : -1;
                        if (DEBUG) {
                            android.util.Log.v(TAG, "retainNonConfig: keeping retained " + f);
                        }
                    }
                    boolean addedChild = false;
                    if (f.mChildFragmentManager != null) {
                        android.support.v4.app.FragmentManagerNonConfig child = f.mChildFragmentManager.retainNonConfig();
                        if (child != null) {
                            if (childFragments == null) {
                                childFragments = new java.util.ArrayList<>();
                                for (int j = 0; j < i; j++) {
                                    childFragments.add(null);
                                }
                            }
                            childFragments.add(child);
                            addedChild = true;
                        }
                    }
                    if (childFragments != null && !addedChild) {
                        childFragments.add(null);
                    }
                }
            }
        }
        if (fragments == null && childFragments == null) {
            return null;
        }
        return new android.support.v4.app.FragmentManagerNonConfig(fragments, childFragments);
    }

    /* access modifiers changed from: 0000 */
    public void saveFragmentViewState(android.support.v4.app.Fragment f) {
        if (f.mInnerView != null) {
            if (this.mStateArray == null) {
                this.mStateArray = new android.util.SparseArray<>();
            } else {
                this.mStateArray.clear();
            }
            f.mInnerView.saveHierarchyState(this.mStateArray);
            if (this.mStateArray.size() > 0) {
                f.mSavedViewState = this.mStateArray;
                this.mStateArray = null;
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public android.os.Bundle saveFragmentBasicState(android.support.v4.app.Fragment f) {
        android.os.Bundle result = null;
        if (this.mStateBundle == null) {
            this.mStateBundle = new android.os.Bundle();
        }
        f.performSaveInstanceState(this.mStateBundle);
        dispatchOnFragmentSaveInstanceState(f, this.mStateBundle, false);
        if (!this.mStateBundle.isEmpty()) {
            result = this.mStateBundle;
            this.mStateBundle = null;
        }
        if (f.mView != null) {
            saveFragmentViewState(f);
        }
        if (f.mSavedViewState != null) {
            if (result == null) {
                result = new android.os.Bundle();
            }
            result.putSparseParcelableArray(VIEW_STATE_TAG, f.mSavedViewState);
        }
        if (!f.mUserVisibleHint) {
            if (result == null) {
                result = new android.os.Bundle();
            }
            result.putBoolean(USER_VISIBLE_HINT_TAG, f.mUserVisibleHint);
        }
        return result;
    }

    /* access modifiers changed from: 0000 */
    public android.os.Parcelable saveAllState() {
        forcePostponedTransactions();
        endAnimatingAwayFragments();
        execPendingActions();
        if (HONEYCOMB) {
            this.mStateSaved = true;
        }
        if (this.mActive == null || this.mActive.size() <= 0) {
            return null;
        }
        int N = this.mActive.size();
        android.support.v4.app.FragmentState[] active = new android.support.v4.app.FragmentState[N];
        boolean haveFragments = false;
        for (int i = 0; i < N; i++) {
            android.support.v4.app.Fragment f = (android.support.v4.app.Fragment) this.mActive.get(i);
            if (f != null) {
                if (f.mIndex < 0) {
                    throwException(new java.lang.IllegalStateException("Failure saving state: active " + f + " has cleared index: " + f.mIndex));
                }
                haveFragments = true;
                android.support.v4.app.FragmentState fs = new android.support.v4.app.FragmentState(f);
                active[i] = fs;
                if (f.mState <= 0 || fs.mSavedFragmentState != null) {
                    fs.mSavedFragmentState = f.mSavedFragmentState;
                } else {
                    fs.mSavedFragmentState = saveFragmentBasicState(f);
                    if (f.mTarget != null) {
                        if (f.mTarget.mIndex < 0) {
                            throwException(new java.lang.IllegalStateException("Failure saving state: " + f + " has target not in fragment manager: " + f.mTarget));
                        }
                        if (fs.mSavedFragmentState == null) {
                            fs.mSavedFragmentState = new android.os.Bundle();
                        }
                        putFragment(fs.mSavedFragmentState, TARGET_STATE_TAG, f.mTarget);
                        if (f.mTargetRequestCode != 0) {
                            fs.mSavedFragmentState.putInt(TARGET_REQUEST_CODE_STATE_TAG, f.mTargetRequestCode);
                        }
                    }
                }
                if (DEBUG) {
                    android.util.Log.v(TAG, "Saved state of " + f + ": " + fs.mSavedFragmentState);
                }
            }
        }
        if (haveFragments) {
            int[] added = null;
            android.support.v4.app.BackStackState[] backStack = null;
            if (this.mAdded != null) {
                int N2 = this.mAdded.size();
                if (N2 > 0) {
                    added = new int[N2];
                    for (int i2 = 0; i2 < N2; i2++) {
                        added[i2] = ((android.support.v4.app.Fragment) this.mAdded.get(i2)).mIndex;
                        if (added[i2] < 0) {
                            throwException(new java.lang.IllegalStateException("Failure saving state: active " + this.mAdded.get(i2) + " has cleared index: " + added[i2]));
                        }
                        if (DEBUG) {
                            android.util.Log.v(TAG, "saveAllState: adding fragment #" + i2 + ": " + this.mAdded.get(i2));
                        }
                    }
                }
            }
            if (this.mBackStack != null) {
                int N3 = this.mBackStack.size();
                if (N3 > 0) {
                    backStack = new android.support.v4.app.BackStackState[N3];
                    for (int i3 = 0; i3 < N3; i3++) {
                        backStack[i3] = new android.support.v4.app.BackStackState((android.support.v4.app.BackStackRecord) this.mBackStack.get(i3));
                        if (DEBUG) {
                            android.util.Log.v(TAG, "saveAllState: adding back stack #" + i3 + ": " + this.mBackStack.get(i3));
                        }
                    }
                }
            }
            android.support.v4.app.FragmentManagerState fms = new android.support.v4.app.FragmentManagerState();
            fms.mActive = active;
            fms.mAdded = added;
            fms.mBackStack = backStack;
            return fms;
        } else if (!DEBUG) {
            return null;
        } else {
            android.util.Log.v(TAG, "saveAllState: no fragments!");
            return null;
        }
    }

    /* access modifiers changed from: 0000 */
    public void restoreAllState(android.os.Parcelable state, android.support.v4.app.FragmentManagerNonConfig nonConfig) {
        if (state != null) {
            android.support.v4.app.FragmentManagerState fms = (android.support.v4.app.FragmentManagerState) state;
            if (fms.mActive != null) {
                java.util.List<android.support.v4.app.FragmentManagerNonConfig> childNonConfigs = null;
                if (nonConfig != null) {
                    java.util.List<android.support.v4.app.Fragment> nonConfigFragments = nonConfig.getFragments();
                    childNonConfigs = nonConfig.getChildNonConfigs();
                    int count = nonConfigFragments != null ? nonConfigFragments.size() : 0;
                    for (int i = 0; i < count; i++) {
                        android.support.v4.app.Fragment f = (android.support.v4.app.Fragment) nonConfigFragments.get(i);
                        if (DEBUG) {
                            android.util.Log.v(TAG, "restoreAllState: re-attaching retained " + f);
                        }
                        android.support.v4.app.FragmentState fs = fms.mActive[f.mIndex];
                        fs.mInstance = f;
                        f.mSavedViewState = null;
                        f.mBackStackNesting = 0;
                        f.mInLayout = false;
                        f.mAdded = false;
                        f.mTarget = null;
                        if (fs.mSavedFragmentState != null) {
                            fs.mSavedFragmentState.setClassLoader(this.mHost.getContext().getClassLoader());
                            f.mSavedViewState = fs.mSavedFragmentState.getSparseParcelableArray(VIEW_STATE_TAG);
                            f.mSavedFragmentState = fs.mSavedFragmentState;
                        }
                    }
                }
                this.mActive = new java.util.ArrayList<>(fms.mActive.length);
                if (this.mAvailIndices != null) {
                    this.mAvailIndices.clear();
                }
                for (int i2 = 0; i2 < fms.mActive.length; i2++) {
                    android.support.v4.app.FragmentState fs2 = fms.mActive[i2];
                    if (fs2 != null) {
                        android.support.v4.app.FragmentManagerNonConfig childNonConfig = null;
                        if (childNonConfigs != null && i2 < childNonConfigs.size()) {
                            childNonConfig = (android.support.v4.app.FragmentManagerNonConfig) childNonConfigs.get(i2);
                        }
                        android.support.v4.app.Fragment f2 = fs2.instantiate(this.mHost, this.mParent, childNonConfig);
                        if (DEBUG) {
                            android.util.Log.v(TAG, "restoreAllState: active #" + i2 + ": " + f2);
                        }
                        this.mActive.add(f2);
                        fs2.mInstance = null;
                    } else {
                        this.mActive.add(null);
                        if (this.mAvailIndices == null) {
                            this.mAvailIndices = new java.util.ArrayList<>();
                        }
                        if (DEBUG) {
                            android.util.Log.v(TAG, "restoreAllState: avail #" + i2);
                        }
                        this.mAvailIndices.add(java.lang.Integer.valueOf(i2));
                    }
                }
                if (nonConfig != null) {
                    java.util.List<android.support.v4.app.Fragment> nonConfigFragments2 = nonConfig.getFragments();
                    int count2 = nonConfigFragments2 != null ? nonConfigFragments2.size() : 0;
                    for (int i3 = 0; i3 < count2; i3++) {
                        android.support.v4.app.Fragment f3 = (android.support.v4.app.Fragment) nonConfigFragments2.get(i3);
                        if (f3.mTargetIndex >= 0) {
                            if (f3.mTargetIndex < this.mActive.size()) {
                                f3.mTarget = (android.support.v4.app.Fragment) this.mActive.get(f3.mTargetIndex);
                            } else {
                                android.util.Log.w(TAG, "Re-attaching retained fragment " + f3 + " target no longer exists: " + f3.mTargetIndex);
                                f3.mTarget = null;
                            }
                        }
                    }
                }
                if (fms.mAdded != null) {
                    this.mAdded = new java.util.ArrayList<>(fms.mAdded.length);
                    for (int i4 = 0; i4 < fms.mAdded.length; i4++) {
                        android.support.v4.app.Fragment f4 = (android.support.v4.app.Fragment) this.mActive.get(fms.mAdded[i4]);
                        if (f4 == null) {
                            throwException(new java.lang.IllegalStateException("No instantiated fragment for index #" + fms.mAdded[i4]));
                        }
                        f4.mAdded = true;
                        if (DEBUG) {
                            android.util.Log.v(TAG, "restoreAllState: added #" + i4 + ": " + f4);
                        }
                        if (this.mAdded.contains(f4)) {
                            throw new java.lang.IllegalStateException("Already added!");
                        }
                        this.mAdded.add(f4);
                    }
                } else {
                    this.mAdded = null;
                }
                if (fms.mBackStack != null) {
                    this.mBackStack = new java.util.ArrayList<>(fms.mBackStack.length);
                    for (int i5 = 0; i5 < fms.mBackStack.length; i5++) {
                        android.support.v4.app.BackStackRecord bse = fms.mBackStack[i5].instantiate(this);
                        if (DEBUG) {
                            android.util.Log.v(TAG, "restoreAllState: back stack #" + i5 + " (index " + bse.mIndex + "): " + bse);
                            java.io.PrintWriter pw = new java.io.PrintWriter(new android.support.v4.util.LogWriter(TAG));
                            bse.dump("  ", pw, false);
                            pw.close();
                        }
                        this.mBackStack.add(bse);
                        if (bse.mIndex >= 0) {
                            setBackStackIndex(bse.mIndex, bse);
                        }
                    }
                    return;
                }
                this.mBackStack = null;
            }
        }
    }

    public void attachController(android.support.v4.app.FragmentHostCallback host, android.support.v4.app.FragmentContainer container, android.support.v4.app.Fragment parent) {
        if (this.mHost != null) {
            throw new java.lang.IllegalStateException("Already attached");
        }
        this.mHost = host;
        this.mContainer = container;
        this.mParent = parent;
    }

    public void noteStateNotSaved() {
        this.mStateSaved = false;
    }

    public void dispatchCreate() {
        this.mStateSaved = false;
        this.mExecutingActions = true;
        moveToState(1, false);
        this.mExecutingActions = false;
    }

    public void dispatchActivityCreated() {
        this.mStateSaved = false;
        this.mExecutingActions = true;
        moveToState(2, false);
        this.mExecutingActions = false;
    }

    public void dispatchStart() {
        this.mStateSaved = false;
        this.mExecutingActions = true;
        moveToState(4, false);
        this.mExecutingActions = false;
    }

    public void dispatchResume() {
        this.mStateSaved = false;
        this.mExecutingActions = true;
        moveToState(5, false);
        this.mExecutingActions = false;
    }

    public void dispatchPause() {
        this.mExecutingActions = true;
        moveToState(4, false);
        this.mExecutingActions = false;
    }

    public void dispatchStop() {
        this.mStateSaved = true;
        this.mExecutingActions = true;
        moveToState(3, false);
        this.mExecutingActions = false;
    }

    public void dispatchReallyStop() {
        this.mExecutingActions = true;
        moveToState(2, false);
        this.mExecutingActions = false;
    }

    public void dispatchDestroyView() {
        this.mExecutingActions = true;
        moveToState(1, false);
        this.mExecutingActions = false;
    }

    public void dispatchDestroy() {
        this.mDestroyed = true;
        execPendingActions();
        this.mExecutingActions = true;
        moveToState(0, false);
        this.mExecutingActions = false;
        this.mHost = null;
        this.mContainer = null;
        this.mParent = null;
    }

    public void dispatchMultiWindowModeChanged(boolean isInMultiWindowMode) {
        if (this.mAdded != null) {
            for (int i = this.mAdded.size() - 1; i >= 0; i--) {
                android.support.v4.app.Fragment f = (android.support.v4.app.Fragment) this.mAdded.get(i);
                if (f != null) {
                    f.performMultiWindowModeChanged(isInMultiWindowMode);
                }
            }
        }
    }

    public void dispatchPictureInPictureModeChanged(boolean isInPictureInPictureMode) {
        if (this.mAdded != null) {
            for (int i = this.mAdded.size() - 1; i >= 0; i--) {
                android.support.v4.app.Fragment f = (android.support.v4.app.Fragment) this.mAdded.get(i);
                if (f != null) {
                    f.performPictureInPictureModeChanged(isInPictureInPictureMode);
                }
            }
        }
    }

    public void dispatchConfigurationChanged(android.content.res.Configuration newConfig) {
        if (this.mAdded != null) {
            for (int i = 0; i < this.mAdded.size(); i++) {
                android.support.v4.app.Fragment f = (android.support.v4.app.Fragment) this.mAdded.get(i);
                if (f != null) {
                    f.performConfigurationChanged(newConfig);
                }
            }
        }
    }

    public void dispatchLowMemory() {
        if (this.mAdded != null) {
            for (int i = 0; i < this.mAdded.size(); i++) {
                android.support.v4.app.Fragment f = (android.support.v4.app.Fragment) this.mAdded.get(i);
                if (f != null) {
                    f.performLowMemory();
                }
            }
        }
    }

    public boolean dispatchCreateOptionsMenu(android.view.Menu menu, android.view.MenuInflater inflater) {
        boolean show = false;
        java.util.ArrayList<android.support.v4.app.Fragment> newMenus = null;
        if (this.mAdded != null) {
            for (int i = 0; i < this.mAdded.size(); i++) {
                android.support.v4.app.Fragment f = (android.support.v4.app.Fragment) this.mAdded.get(i);
                if (f != null && f.performCreateOptionsMenu(menu, inflater)) {
                    show = true;
                    if (newMenus == null) {
                        newMenus = new java.util.ArrayList<>();
                    }
                    newMenus.add(f);
                }
            }
        }
        if (this.mCreatedMenus != null) {
            for (int i2 = 0; i2 < this.mCreatedMenus.size(); i2++) {
                android.support.v4.app.Fragment f2 = (android.support.v4.app.Fragment) this.mCreatedMenus.get(i2);
                if (newMenus == null || !newMenus.contains(f2)) {
                    f2.onDestroyOptionsMenu();
                }
            }
        }
        this.mCreatedMenus = newMenus;
        return show;
    }

    public boolean dispatchPrepareOptionsMenu(android.view.Menu menu) {
        boolean show = false;
        if (this.mAdded != null) {
            for (int i = 0; i < this.mAdded.size(); i++) {
                android.support.v4.app.Fragment f = (android.support.v4.app.Fragment) this.mAdded.get(i);
                if (f != null && f.performPrepareOptionsMenu(menu)) {
                    show = true;
                }
            }
        }
        return show;
    }

    public boolean dispatchOptionsItemSelected(android.view.MenuItem item) {
        if (this.mAdded != null) {
            for (int i = 0; i < this.mAdded.size(); i++) {
                android.support.v4.app.Fragment f = (android.support.v4.app.Fragment) this.mAdded.get(i);
                if (f != null && f.performOptionsItemSelected(item)) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean dispatchContextItemSelected(android.view.MenuItem item) {
        if (this.mAdded != null) {
            for (int i = 0; i < this.mAdded.size(); i++) {
                android.support.v4.app.Fragment f = (android.support.v4.app.Fragment) this.mAdded.get(i);
                if (f != null && f.performContextItemSelected(item)) {
                    return true;
                }
            }
        }
        return false;
    }

    public void dispatchOptionsMenuClosed(android.view.Menu menu) {
        if (this.mAdded != null) {
            for (int i = 0; i < this.mAdded.size(); i++) {
                android.support.v4.app.Fragment f = (android.support.v4.app.Fragment) this.mAdded.get(i);
                if (f != null) {
                    f.performOptionsMenuClosed(menu);
                }
            }
        }
    }

    public void registerFragmentLifecycleCallbacks(android.support.v4.app.FragmentManager.FragmentLifecycleCallbacks cb, boolean recursive) {
        if (this.mLifecycleCallbacks == null) {
            this.mLifecycleCallbacks = new java.util.concurrent.CopyOnWriteArrayList<>();
        }
        this.mLifecycleCallbacks.add(new android.support.v4.util.Pair(cb, java.lang.Boolean.valueOf(recursive)));
    }

    public void unregisterFragmentLifecycleCallbacks(android.support.v4.app.FragmentManager.FragmentLifecycleCallbacks cb) {
        if (this.mLifecycleCallbacks != null) {
            synchronized (this.mLifecycleCallbacks) {
                int i = 0;
                int N = this.mLifecycleCallbacks.size();
                while (true) {
                    if (i >= N) {
                        break;
                    } else if (((android.support.v4.util.Pair) this.mLifecycleCallbacks.get(i)).first == cb) {
                        this.mLifecycleCallbacks.remove(i);
                        break;
                    } else {
                        i++;
                    }
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void dispatchOnFragmentPreAttached(android.support.v4.app.Fragment f, android.content.Context context, boolean onlyRecursive) {
        if (this.mParent != null) {
            android.support.v4.app.FragmentManager parentManager = this.mParent.getFragmentManager();
            if (parentManager instanceof android.support.v4.app.FragmentManagerImpl) {
                ((android.support.v4.app.FragmentManagerImpl) parentManager).dispatchOnFragmentPreAttached(f, context, true);
            }
        }
        if (this.mLifecycleCallbacks != null) {
            java.util.Iterator it = this.mLifecycleCallbacks.iterator();
            while (it.hasNext()) {
                android.support.v4.util.Pair<android.support.v4.app.FragmentManager.FragmentLifecycleCallbacks, java.lang.Boolean> p = (android.support.v4.util.Pair) it.next();
                if (!onlyRecursive || ((java.lang.Boolean) p.second).booleanValue()) {
                    ((android.support.v4.app.FragmentManager.FragmentLifecycleCallbacks) p.first).onFragmentPreAttached(this, f, context);
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void dispatchOnFragmentAttached(android.support.v4.app.Fragment f, android.content.Context context, boolean onlyRecursive) {
        if (this.mParent != null) {
            android.support.v4.app.FragmentManager parentManager = this.mParent.getFragmentManager();
            if (parentManager instanceof android.support.v4.app.FragmentManagerImpl) {
                ((android.support.v4.app.FragmentManagerImpl) parentManager).dispatchOnFragmentAttached(f, context, true);
            }
        }
        if (this.mLifecycleCallbacks != null) {
            java.util.Iterator it = this.mLifecycleCallbacks.iterator();
            while (it.hasNext()) {
                android.support.v4.util.Pair<android.support.v4.app.FragmentManager.FragmentLifecycleCallbacks, java.lang.Boolean> p = (android.support.v4.util.Pair) it.next();
                if (!onlyRecursive || ((java.lang.Boolean) p.second).booleanValue()) {
                    ((android.support.v4.app.FragmentManager.FragmentLifecycleCallbacks) p.first).onFragmentAttached(this, f, context);
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void dispatchOnFragmentCreated(android.support.v4.app.Fragment f, android.os.Bundle savedInstanceState, boolean onlyRecursive) {
        if (this.mParent != null) {
            android.support.v4.app.FragmentManager parentManager = this.mParent.getFragmentManager();
            if (parentManager instanceof android.support.v4.app.FragmentManagerImpl) {
                ((android.support.v4.app.FragmentManagerImpl) parentManager).dispatchOnFragmentCreated(f, savedInstanceState, true);
            }
        }
        if (this.mLifecycleCallbacks != null) {
            java.util.Iterator it = this.mLifecycleCallbacks.iterator();
            while (it.hasNext()) {
                android.support.v4.util.Pair<android.support.v4.app.FragmentManager.FragmentLifecycleCallbacks, java.lang.Boolean> p = (android.support.v4.util.Pair) it.next();
                if (!onlyRecursive || ((java.lang.Boolean) p.second).booleanValue()) {
                    ((android.support.v4.app.FragmentManager.FragmentLifecycleCallbacks) p.first).onFragmentCreated(this, f, savedInstanceState);
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void dispatchOnFragmentActivityCreated(android.support.v4.app.Fragment f, android.os.Bundle savedInstanceState, boolean onlyRecursive) {
        if (this.mParent != null) {
            android.support.v4.app.FragmentManager parentManager = this.mParent.getFragmentManager();
            if (parentManager instanceof android.support.v4.app.FragmentManagerImpl) {
                ((android.support.v4.app.FragmentManagerImpl) parentManager).dispatchOnFragmentActivityCreated(f, savedInstanceState, true);
            }
        }
        if (this.mLifecycleCallbacks != null) {
            java.util.Iterator it = this.mLifecycleCallbacks.iterator();
            while (it.hasNext()) {
                android.support.v4.util.Pair<android.support.v4.app.FragmentManager.FragmentLifecycleCallbacks, java.lang.Boolean> p = (android.support.v4.util.Pair) it.next();
                if (!onlyRecursive || ((java.lang.Boolean) p.second).booleanValue()) {
                    ((android.support.v4.app.FragmentManager.FragmentLifecycleCallbacks) p.first).onFragmentActivityCreated(this, f, savedInstanceState);
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void dispatchOnFragmentViewCreated(android.support.v4.app.Fragment f, android.view.View v, android.os.Bundle savedInstanceState, boolean onlyRecursive) {
        if (this.mParent != null) {
            android.support.v4.app.FragmentManager parentManager = this.mParent.getFragmentManager();
            if (parentManager instanceof android.support.v4.app.FragmentManagerImpl) {
                ((android.support.v4.app.FragmentManagerImpl) parentManager).dispatchOnFragmentViewCreated(f, v, savedInstanceState, true);
            }
        }
        if (this.mLifecycleCallbacks != null) {
            java.util.Iterator it = this.mLifecycleCallbacks.iterator();
            while (it.hasNext()) {
                android.support.v4.util.Pair<android.support.v4.app.FragmentManager.FragmentLifecycleCallbacks, java.lang.Boolean> p = (android.support.v4.util.Pair) it.next();
                if (!onlyRecursive || ((java.lang.Boolean) p.second).booleanValue()) {
                    ((android.support.v4.app.FragmentManager.FragmentLifecycleCallbacks) p.first).onFragmentViewCreated(this, f, v, savedInstanceState);
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void dispatchOnFragmentStarted(android.support.v4.app.Fragment f, boolean onlyRecursive) {
        if (this.mParent != null) {
            android.support.v4.app.FragmentManager parentManager = this.mParent.getFragmentManager();
            if (parentManager instanceof android.support.v4.app.FragmentManagerImpl) {
                ((android.support.v4.app.FragmentManagerImpl) parentManager).dispatchOnFragmentStarted(f, true);
            }
        }
        if (this.mLifecycleCallbacks != null) {
            java.util.Iterator it = this.mLifecycleCallbacks.iterator();
            while (it.hasNext()) {
                android.support.v4.util.Pair<android.support.v4.app.FragmentManager.FragmentLifecycleCallbacks, java.lang.Boolean> p = (android.support.v4.util.Pair) it.next();
                if (!onlyRecursive || ((java.lang.Boolean) p.second).booleanValue()) {
                    ((android.support.v4.app.FragmentManager.FragmentLifecycleCallbacks) p.first).onFragmentStarted(this, f);
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void dispatchOnFragmentResumed(android.support.v4.app.Fragment f, boolean onlyRecursive) {
        if (this.mParent != null) {
            android.support.v4.app.FragmentManager parentManager = this.mParent.getFragmentManager();
            if (parentManager instanceof android.support.v4.app.FragmentManagerImpl) {
                ((android.support.v4.app.FragmentManagerImpl) parentManager).dispatchOnFragmentResumed(f, true);
            }
        }
        if (this.mLifecycleCallbacks != null) {
            java.util.Iterator it = this.mLifecycleCallbacks.iterator();
            while (it.hasNext()) {
                android.support.v4.util.Pair<android.support.v4.app.FragmentManager.FragmentLifecycleCallbacks, java.lang.Boolean> p = (android.support.v4.util.Pair) it.next();
                if (!onlyRecursive || ((java.lang.Boolean) p.second).booleanValue()) {
                    ((android.support.v4.app.FragmentManager.FragmentLifecycleCallbacks) p.first).onFragmentResumed(this, f);
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void dispatchOnFragmentPaused(android.support.v4.app.Fragment f, boolean onlyRecursive) {
        if (this.mParent != null) {
            android.support.v4.app.FragmentManager parentManager = this.mParent.getFragmentManager();
            if (parentManager instanceof android.support.v4.app.FragmentManagerImpl) {
                ((android.support.v4.app.FragmentManagerImpl) parentManager).dispatchOnFragmentPaused(f, true);
            }
        }
        if (this.mLifecycleCallbacks != null) {
            java.util.Iterator it = this.mLifecycleCallbacks.iterator();
            while (it.hasNext()) {
                android.support.v4.util.Pair<android.support.v4.app.FragmentManager.FragmentLifecycleCallbacks, java.lang.Boolean> p = (android.support.v4.util.Pair) it.next();
                if (!onlyRecursive || ((java.lang.Boolean) p.second).booleanValue()) {
                    ((android.support.v4.app.FragmentManager.FragmentLifecycleCallbacks) p.first).onFragmentPaused(this, f);
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void dispatchOnFragmentStopped(android.support.v4.app.Fragment f, boolean onlyRecursive) {
        if (this.mParent != null) {
            android.support.v4.app.FragmentManager parentManager = this.mParent.getFragmentManager();
            if (parentManager instanceof android.support.v4.app.FragmentManagerImpl) {
                ((android.support.v4.app.FragmentManagerImpl) parentManager).dispatchOnFragmentStopped(f, true);
            }
        }
        if (this.mLifecycleCallbacks != null) {
            java.util.Iterator it = this.mLifecycleCallbacks.iterator();
            while (it.hasNext()) {
                android.support.v4.util.Pair<android.support.v4.app.FragmentManager.FragmentLifecycleCallbacks, java.lang.Boolean> p = (android.support.v4.util.Pair) it.next();
                if (!onlyRecursive || ((java.lang.Boolean) p.second).booleanValue()) {
                    ((android.support.v4.app.FragmentManager.FragmentLifecycleCallbacks) p.first).onFragmentStopped(this, f);
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void dispatchOnFragmentSaveInstanceState(android.support.v4.app.Fragment f, android.os.Bundle outState, boolean onlyRecursive) {
        if (this.mParent != null) {
            android.support.v4.app.FragmentManager parentManager = this.mParent.getFragmentManager();
            if (parentManager instanceof android.support.v4.app.FragmentManagerImpl) {
                ((android.support.v4.app.FragmentManagerImpl) parentManager).dispatchOnFragmentSaveInstanceState(f, outState, true);
            }
        }
        if (this.mLifecycleCallbacks != null) {
            java.util.Iterator it = this.mLifecycleCallbacks.iterator();
            while (it.hasNext()) {
                android.support.v4.util.Pair<android.support.v4.app.FragmentManager.FragmentLifecycleCallbacks, java.lang.Boolean> p = (android.support.v4.util.Pair) it.next();
                if (!onlyRecursive || ((java.lang.Boolean) p.second).booleanValue()) {
                    ((android.support.v4.app.FragmentManager.FragmentLifecycleCallbacks) p.first).onFragmentSaveInstanceState(this, f, outState);
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void dispatchOnFragmentViewDestroyed(android.support.v4.app.Fragment f, boolean onlyRecursive) {
        if (this.mParent != null) {
            android.support.v4.app.FragmentManager parentManager = this.mParent.getFragmentManager();
            if (parentManager instanceof android.support.v4.app.FragmentManagerImpl) {
                ((android.support.v4.app.FragmentManagerImpl) parentManager).dispatchOnFragmentViewDestroyed(f, true);
            }
        }
        if (this.mLifecycleCallbacks != null) {
            java.util.Iterator it = this.mLifecycleCallbacks.iterator();
            while (it.hasNext()) {
                android.support.v4.util.Pair<android.support.v4.app.FragmentManager.FragmentLifecycleCallbacks, java.lang.Boolean> p = (android.support.v4.util.Pair) it.next();
                if (!onlyRecursive || ((java.lang.Boolean) p.second).booleanValue()) {
                    ((android.support.v4.app.FragmentManager.FragmentLifecycleCallbacks) p.first).onFragmentViewDestroyed(this, f);
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void dispatchOnFragmentDestroyed(android.support.v4.app.Fragment f, boolean onlyRecursive) {
        if (this.mParent != null) {
            android.support.v4.app.FragmentManager parentManager = this.mParent.getFragmentManager();
            if (parentManager instanceof android.support.v4.app.FragmentManagerImpl) {
                ((android.support.v4.app.FragmentManagerImpl) parentManager).dispatchOnFragmentDestroyed(f, true);
            }
        }
        if (this.mLifecycleCallbacks != null) {
            java.util.Iterator it = this.mLifecycleCallbacks.iterator();
            while (it.hasNext()) {
                android.support.v4.util.Pair<android.support.v4.app.FragmentManager.FragmentLifecycleCallbacks, java.lang.Boolean> p = (android.support.v4.util.Pair) it.next();
                if (!onlyRecursive || ((java.lang.Boolean) p.second).booleanValue()) {
                    ((android.support.v4.app.FragmentManager.FragmentLifecycleCallbacks) p.first).onFragmentDestroyed(this, f);
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void dispatchOnFragmentDetached(android.support.v4.app.Fragment f, boolean onlyRecursive) {
        if (this.mParent != null) {
            android.support.v4.app.FragmentManager parentManager = this.mParent.getFragmentManager();
            if (parentManager instanceof android.support.v4.app.FragmentManagerImpl) {
                ((android.support.v4.app.FragmentManagerImpl) parentManager).dispatchOnFragmentDetached(f, true);
            }
        }
        if (this.mLifecycleCallbacks != null) {
            java.util.Iterator it = this.mLifecycleCallbacks.iterator();
            while (it.hasNext()) {
                android.support.v4.util.Pair<android.support.v4.app.FragmentManager.FragmentLifecycleCallbacks, java.lang.Boolean> p = (android.support.v4.util.Pair) it.next();
                if (!onlyRecursive || ((java.lang.Boolean) p.second).booleanValue()) {
                    ((android.support.v4.app.FragmentManager.FragmentLifecycleCallbacks) p.first).onFragmentDetached(this, f);
                }
            }
        }
    }

    public static int reverseTransit(int transit) {
        switch (transit) {
            case android.support.v4.app.FragmentTransaction.TRANSIT_FRAGMENT_OPEN /*4097*/:
                return 8194;
            case android.support.v4.app.FragmentTransaction.TRANSIT_FRAGMENT_FADE /*4099*/:
                return android.support.v4.app.FragmentTransaction.TRANSIT_FRAGMENT_FADE;
            case 8194:
                return android.support.v4.app.FragmentTransaction.TRANSIT_FRAGMENT_OPEN;
            default:
                return 0;
        }
    }

    public static int transitToStyleIndex(int transit, boolean enter) {
        switch (transit) {
            case android.support.v4.app.FragmentTransaction.TRANSIT_FRAGMENT_OPEN /*4097*/:
                return enter ? 1 : 2;
            case android.support.v4.app.FragmentTransaction.TRANSIT_FRAGMENT_FADE /*4099*/:
                return enter ? 5 : 6;
            case 8194:
                return enter ? 3 : 4;
            default:
                return -1;
        }
    }

    public android.view.View onCreateView(android.view.View parent, java.lang.String name, android.content.Context context, android.util.AttributeSet attrs) {
        int containerId;
        android.support.v4.app.Fragment fragment;
        int i;
        if (!"fragment".equals(name)) {
            return null;
        }
        java.lang.String fname = attrs.getAttributeValue(null, ch.qos.logback.core.joran.action.Action.CLASS_ATTRIBUTE);
        android.content.res.TypedArray a = context.obtainStyledAttributes(attrs, android.support.v4.app.FragmentManagerImpl.FragmentTag.Fragment);
        if (fname == null) {
            fname = a.getString(0);
        }
        int id = a.getResourceId(1, -1);
        java.lang.String tag = a.getString(2);
        a.recycle();
        if (!android.support.v4.app.Fragment.isSupportFragmentClass(this.mHost.getContext(), fname)) {
            return null;
        }
        if (parent != null) {
            containerId = parent.getId();
        } else {
            containerId = 0;
        }
        if (containerId == -1 && id == -1 && tag == null) {
            throw new java.lang.IllegalArgumentException(attrs.getPositionDescription() + ": Must specify unique android:id, android:tag, or have a parent with an id for " + fname);
        }
        if (id != -1) {
            fragment = findFragmentById(id);
        } else {
            fragment = null;
        }
        if (fragment == null && tag != null) {
            fragment = findFragmentByTag(tag);
        }
        if (fragment == null && containerId != -1) {
            fragment = findFragmentById(containerId);
        }
        if (DEBUG) {
            android.util.Log.v(TAG, "onCreateView: id=0x" + java.lang.Integer.toHexString(id) + " fname=" + fname + " existing=" + fragment);
        }
        if (fragment == null) {
            fragment = android.support.v4.app.Fragment.instantiate(context, fname);
            fragment.mFromLayout = true;
            if (id != 0) {
                i = id;
            } else {
                i = containerId;
            }
            fragment.mFragmentId = i;
            fragment.mContainerId = containerId;
            fragment.mTag = tag;
            fragment.mInLayout = true;
            fragment.mFragmentManager = this;
            fragment.mHost = this.mHost;
            fragment.onInflate(this.mHost.getContext(), attrs, fragment.mSavedFragmentState);
            addFragment(fragment, true);
        } else if (fragment.mInLayout) {
            throw new java.lang.IllegalArgumentException(attrs.getPositionDescription() + ": Duplicate id 0x" + java.lang.Integer.toHexString(id) + ", tag " + tag + ", or parent id 0x" + java.lang.Integer.toHexString(containerId) + " with another fragment for " + fname);
        } else {
            fragment.mInLayout = true;
            fragment.mHost = this.mHost;
            if (!fragment.mRetaining) {
                fragment.onInflate(this.mHost.getContext(), attrs, fragment.mSavedFragmentState);
            }
        }
        if (this.mCurState >= 1 || !fragment.mFromLayout) {
            moveToState(fragment);
        } else {
            moveToState(fragment, 1, 0, 0, false);
        }
        if (fragment.mView == null) {
            throw new java.lang.IllegalStateException("Fragment " + fname + " did not create a view.");
        }
        if (id != 0) {
            fragment.mView.setId(id);
        }
        if (fragment.mView.getTag() == null) {
            fragment.mView.setTag(tag);
        }
        return fragment.mView;
    }

    /* access modifiers changed from: 0000 */
    public android.support.v4.view.LayoutInflaterFactory getLayoutInflaterFactory() {
        return this;
    }
}
