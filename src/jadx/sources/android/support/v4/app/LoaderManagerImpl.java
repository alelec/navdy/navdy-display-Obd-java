package android.support.v4.app;

/* compiled from: LoaderManager */
class LoaderManagerImpl extends android.support.v4.app.LoaderManager {
    static boolean DEBUG = false;
    static final java.lang.String TAG = "LoaderManager";
    boolean mCreatingLoader;
    android.support.v4.app.FragmentHostCallback mHost;
    final android.support.v4.util.SparseArrayCompat<android.support.v4.app.LoaderManagerImpl.LoaderInfo> mInactiveLoaders = new android.support.v4.util.SparseArrayCompat<>();
    final android.support.v4.util.SparseArrayCompat<android.support.v4.app.LoaderManagerImpl.LoaderInfo> mLoaders = new android.support.v4.util.SparseArrayCompat<>();
    boolean mRetaining;
    boolean mRetainingStarted;
    boolean mStarted;
    final java.lang.String mWho;

    /* compiled from: LoaderManager */
    final class LoaderInfo implements android.support.v4.content.Loader.OnLoadCompleteListener<java.lang.Object>, android.support.v4.content.Loader.OnLoadCanceledListener<java.lang.Object> {
        final android.os.Bundle mArgs;
        android.support.v4.app.LoaderManager.LoaderCallbacks<java.lang.Object> mCallbacks;
        java.lang.Object mData;
        boolean mDeliveredData;
        boolean mDestroyed;
        boolean mHaveData;
        final int mId;
        boolean mListenerRegistered;
        android.support.v4.content.Loader<java.lang.Object> mLoader;
        android.support.v4.app.LoaderManagerImpl.LoaderInfo mPendingLoader;
        boolean mReportNextStart;
        boolean mRetaining;
        boolean mRetainingStarted;
        boolean mStarted;

        public LoaderInfo(int id, android.os.Bundle args, android.support.v4.app.LoaderManager.LoaderCallbacks<java.lang.Object> callbacks) {
            this.mId = id;
            this.mArgs = args;
            this.mCallbacks = callbacks;
        }

        /* access modifiers changed from: 0000 */
        public void start() {
            if (this.mRetaining && this.mRetainingStarted) {
                this.mStarted = true;
            } else if (!this.mStarted) {
                this.mStarted = true;
                if (android.support.v4.app.LoaderManagerImpl.DEBUG) {
                    android.util.Log.v(android.support.v4.app.LoaderManagerImpl.TAG, "  Starting: " + this);
                }
                if (this.mLoader == null && this.mCallbacks != null) {
                    this.mLoader = this.mCallbacks.onCreateLoader(this.mId, this.mArgs);
                }
                if (this.mLoader == null) {
                    return;
                }
                if (!this.mLoader.getClass().isMemberClass() || java.lang.reflect.Modifier.isStatic(this.mLoader.getClass().getModifiers())) {
                    if (!this.mListenerRegistered) {
                        this.mLoader.registerListener(this.mId, this);
                        this.mLoader.registerOnLoadCanceledListener(this);
                        this.mListenerRegistered = true;
                    }
                    this.mLoader.startLoading();
                    return;
                }
                throw new java.lang.IllegalArgumentException("Object returned from onCreateLoader must not be a non-static inner member class: " + this.mLoader);
            }
        }

        /* access modifiers changed from: 0000 */
        public void retain() {
            if (android.support.v4.app.LoaderManagerImpl.DEBUG) {
                android.util.Log.v(android.support.v4.app.LoaderManagerImpl.TAG, "  Retaining: " + this);
            }
            this.mRetaining = true;
            this.mRetainingStarted = this.mStarted;
            this.mStarted = false;
            this.mCallbacks = null;
        }

        /* access modifiers changed from: 0000 */
        public void finishRetain() {
            if (this.mRetaining) {
                if (android.support.v4.app.LoaderManagerImpl.DEBUG) {
                    android.util.Log.v(android.support.v4.app.LoaderManagerImpl.TAG, "  Finished Retaining: " + this);
                }
                this.mRetaining = false;
                if (this.mStarted != this.mRetainingStarted && !this.mStarted) {
                    stop();
                }
            }
            if (this.mStarted && this.mHaveData && !this.mReportNextStart) {
                callOnLoadFinished(this.mLoader, this.mData);
            }
        }

        /* access modifiers changed from: 0000 */
        public void reportStart() {
            if (this.mStarted && this.mReportNextStart) {
                this.mReportNextStart = false;
                if (this.mHaveData && !this.mRetaining) {
                    callOnLoadFinished(this.mLoader, this.mData);
                }
            }
        }

        /* access modifiers changed from: 0000 */
        public void stop() {
            if (android.support.v4.app.LoaderManagerImpl.DEBUG) {
                android.util.Log.v(android.support.v4.app.LoaderManagerImpl.TAG, "  Stopping: " + this);
            }
            this.mStarted = false;
            if (!this.mRetaining && this.mLoader != null && this.mListenerRegistered) {
                this.mListenerRegistered = false;
                this.mLoader.unregisterListener(this);
                this.mLoader.unregisterOnLoadCanceledListener(this);
                this.mLoader.stopLoading();
            }
        }

        /* access modifiers changed from: 0000 */
        public boolean cancel() {
            if (android.support.v4.app.LoaderManagerImpl.DEBUG) {
                android.util.Log.v(android.support.v4.app.LoaderManagerImpl.TAG, "  Canceling: " + this);
            }
            if (!this.mStarted || this.mLoader == null || !this.mListenerRegistered) {
                return false;
            }
            boolean cancelLoadResult = this.mLoader.cancelLoad();
            if (cancelLoadResult) {
                return cancelLoadResult;
            }
            onLoadCanceled(this.mLoader);
            return cancelLoadResult;
        }

        /* access modifiers changed from: 0000 */
        public void destroy() {
            if (android.support.v4.app.LoaderManagerImpl.DEBUG) {
                android.util.Log.v(android.support.v4.app.LoaderManagerImpl.TAG, "  Destroying: " + this);
            }
            this.mDestroyed = true;
            boolean needReset = this.mDeliveredData;
            this.mDeliveredData = false;
            if (this.mCallbacks != null && this.mLoader != null && this.mHaveData && needReset) {
                if (android.support.v4.app.LoaderManagerImpl.DEBUG) {
                    android.util.Log.v(android.support.v4.app.LoaderManagerImpl.TAG, "  Resetting: " + this);
                }
                java.lang.String lastBecause = null;
                if (android.support.v4.app.LoaderManagerImpl.this.mHost != null) {
                    lastBecause = android.support.v4.app.LoaderManagerImpl.this.mHost.mFragmentManager.mNoTransactionsBecause;
                    android.support.v4.app.LoaderManagerImpl.this.mHost.mFragmentManager.mNoTransactionsBecause = "onLoaderReset";
                }
                try {
                    this.mCallbacks.onLoaderReset(this.mLoader);
                } finally {
                    if (android.support.v4.app.LoaderManagerImpl.this.mHost != null) {
                        android.support.v4.app.LoaderManagerImpl.this.mHost.mFragmentManager.mNoTransactionsBecause = lastBecause;
                    }
                }
            }
            this.mCallbacks = null;
            this.mData = null;
            this.mHaveData = false;
            if (this.mLoader != null) {
                if (this.mListenerRegistered) {
                    this.mListenerRegistered = false;
                    this.mLoader.unregisterListener(this);
                    this.mLoader.unregisterOnLoadCanceledListener(this);
                }
                this.mLoader.reset();
            }
            if (this.mPendingLoader != null) {
                this.mPendingLoader.destroy();
            }
        }

        public void onLoadCanceled(android.support.v4.content.Loader<java.lang.Object> loader) {
            if (android.support.v4.app.LoaderManagerImpl.DEBUG) {
                android.util.Log.v(android.support.v4.app.LoaderManagerImpl.TAG, "onLoadCanceled: " + this);
            }
            if (this.mDestroyed) {
                if (android.support.v4.app.LoaderManagerImpl.DEBUG) {
                    android.util.Log.v(android.support.v4.app.LoaderManagerImpl.TAG, "  Ignoring load canceled -- destroyed");
                }
            } else if (android.support.v4.app.LoaderManagerImpl.this.mLoaders.get(this.mId) == this) {
                android.support.v4.app.LoaderManagerImpl.LoaderInfo pending = this.mPendingLoader;
                if (pending != null) {
                    if (android.support.v4.app.LoaderManagerImpl.DEBUG) {
                        android.util.Log.v(android.support.v4.app.LoaderManagerImpl.TAG, "  Switching to pending loader: " + pending);
                    }
                    this.mPendingLoader = null;
                    android.support.v4.app.LoaderManagerImpl.this.mLoaders.put(this.mId, null);
                    destroy();
                    android.support.v4.app.LoaderManagerImpl.this.installLoader(pending);
                }
            } else if (android.support.v4.app.LoaderManagerImpl.DEBUG) {
                android.util.Log.v(android.support.v4.app.LoaderManagerImpl.TAG, "  Ignoring load canceled -- not active");
            }
        }

        public void onLoadComplete(android.support.v4.content.Loader<java.lang.Object> loader, java.lang.Object data) {
            if (android.support.v4.app.LoaderManagerImpl.DEBUG) {
                android.util.Log.v(android.support.v4.app.LoaderManagerImpl.TAG, "onLoadComplete: " + this);
            }
            if (this.mDestroyed) {
                if (android.support.v4.app.LoaderManagerImpl.DEBUG) {
                    android.util.Log.v(android.support.v4.app.LoaderManagerImpl.TAG, "  Ignoring load complete -- destroyed");
                }
            } else if (android.support.v4.app.LoaderManagerImpl.this.mLoaders.get(this.mId) == this) {
                android.support.v4.app.LoaderManagerImpl.LoaderInfo pending = this.mPendingLoader;
                if (pending != null) {
                    if (android.support.v4.app.LoaderManagerImpl.DEBUG) {
                        android.util.Log.v(android.support.v4.app.LoaderManagerImpl.TAG, "  Switching to pending loader: " + pending);
                    }
                    this.mPendingLoader = null;
                    android.support.v4.app.LoaderManagerImpl.this.mLoaders.put(this.mId, null);
                    destroy();
                    android.support.v4.app.LoaderManagerImpl.this.installLoader(pending);
                    return;
                }
                if (this.mData != data || !this.mHaveData) {
                    this.mData = data;
                    this.mHaveData = true;
                    if (this.mStarted) {
                        callOnLoadFinished(loader, data);
                    }
                }
                android.support.v4.app.LoaderManagerImpl.LoaderInfo info = (android.support.v4.app.LoaderManagerImpl.LoaderInfo) android.support.v4.app.LoaderManagerImpl.this.mInactiveLoaders.get(this.mId);
                if (!(info == null || info == this)) {
                    info.mDeliveredData = false;
                    info.destroy();
                    android.support.v4.app.LoaderManagerImpl.this.mInactiveLoaders.remove(this.mId);
                }
                if (android.support.v4.app.LoaderManagerImpl.this.mHost != null && !android.support.v4.app.LoaderManagerImpl.this.hasRunningLoaders()) {
                    android.support.v4.app.LoaderManagerImpl.this.mHost.mFragmentManager.startPendingDeferredFragments();
                }
            } else if (android.support.v4.app.LoaderManagerImpl.DEBUG) {
                android.util.Log.v(android.support.v4.app.LoaderManagerImpl.TAG, "  Ignoring load complete -- not active");
            }
        }

        /* access modifiers changed from: 0000 */
        public void callOnLoadFinished(android.support.v4.content.Loader<java.lang.Object> loader, java.lang.Object data) {
            if (this.mCallbacks != null) {
                java.lang.String lastBecause = null;
                if (android.support.v4.app.LoaderManagerImpl.this.mHost != null) {
                    lastBecause = android.support.v4.app.LoaderManagerImpl.this.mHost.mFragmentManager.mNoTransactionsBecause;
                    android.support.v4.app.LoaderManagerImpl.this.mHost.mFragmentManager.mNoTransactionsBecause = "onLoadFinished";
                }
                try {
                    if (android.support.v4.app.LoaderManagerImpl.DEBUG) {
                        android.util.Log.v(android.support.v4.app.LoaderManagerImpl.TAG, "  onLoadFinished in " + loader + ": " + loader.dataToString(data));
                    }
                    this.mCallbacks.onLoadFinished(loader, data);
                    this.mDeliveredData = true;
                } finally {
                    if (android.support.v4.app.LoaderManagerImpl.this.mHost != null) {
                        android.support.v4.app.LoaderManagerImpl.this.mHost.mFragmentManager.mNoTransactionsBecause = lastBecause;
                    }
                }
            }
        }

        public java.lang.String toString() {
            java.lang.StringBuilder sb = new java.lang.StringBuilder(64);
            sb.append("LoaderInfo{");
            sb.append(java.lang.Integer.toHexString(java.lang.System.identityHashCode(this)));
            sb.append(" #");
            sb.append(this.mId);
            sb.append(" : ");
            android.support.v4.util.DebugUtils.buildShortClassTag(this.mLoader, sb);
            sb.append("}}");
            return sb.toString();
        }

        public void dump(java.lang.String prefix, java.io.FileDescriptor fd, java.io.PrintWriter writer, java.lang.String[] args) {
            writer.print(prefix);
            writer.print("mId=");
            writer.print(this.mId);
            writer.print(" mArgs=");
            writer.println(this.mArgs);
            writer.print(prefix);
            writer.print("mCallbacks=");
            writer.println(this.mCallbacks);
            writer.print(prefix);
            writer.print("mLoader=");
            writer.println(this.mLoader);
            if (this.mLoader != null) {
                this.mLoader.dump(prefix + "  ", fd, writer, args);
            }
            if (this.mHaveData || this.mDeliveredData) {
                writer.print(prefix);
                writer.print("mHaveData=");
                writer.print(this.mHaveData);
                writer.print("  mDeliveredData=");
                writer.println(this.mDeliveredData);
                writer.print(prefix);
                writer.print("mData=");
                writer.println(this.mData);
            }
            writer.print(prefix);
            writer.print("mStarted=");
            writer.print(this.mStarted);
            writer.print(" mReportNextStart=");
            writer.print(this.mReportNextStart);
            writer.print(" mDestroyed=");
            writer.println(this.mDestroyed);
            writer.print(prefix);
            writer.print("mRetaining=");
            writer.print(this.mRetaining);
            writer.print(" mRetainingStarted=");
            writer.print(this.mRetainingStarted);
            writer.print(" mListenerRegistered=");
            writer.println(this.mListenerRegistered);
            if (this.mPendingLoader != null) {
                writer.print(prefix);
                writer.println("Pending Loader ");
                writer.print(this.mPendingLoader);
                writer.println(":");
                this.mPendingLoader.dump(prefix + "  ", fd, writer, args);
            }
        }
    }

    LoaderManagerImpl(java.lang.String who, android.support.v4.app.FragmentHostCallback host, boolean started) {
        this.mWho = who;
        this.mHost = host;
        this.mStarted = started;
    }

    /* access modifiers changed from: 0000 */
    public void updateHostController(android.support.v4.app.FragmentHostCallback host) {
        this.mHost = host;
    }

    private android.support.v4.app.LoaderManagerImpl.LoaderInfo createLoader(int id, android.os.Bundle args, android.support.v4.app.LoaderManager.LoaderCallbacks<java.lang.Object> callback) {
        android.support.v4.app.LoaderManagerImpl.LoaderInfo info = new android.support.v4.app.LoaderManagerImpl.LoaderInfo(id, args, callback);
        info.mLoader = callback.onCreateLoader(id, args);
        return info;
    }

    private android.support.v4.app.LoaderManagerImpl.LoaderInfo createAndInstallLoader(int id, android.os.Bundle args, android.support.v4.app.LoaderManager.LoaderCallbacks<java.lang.Object> callback) {
        try {
            this.mCreatingLoader = true;
            android.support.v4.app.LoaderManagerImpl.LoaderInfo info = createLoader(id, args, callback);
            installLoader(info);
            return info;
        } finally {
            this.mCreatingLoader = false;
        }
    }

    /* access modifiers changed from: 0000 */
    public void installLoader(android.support.v4.app.LoaderManagerImpl.LoaderInfo info) {
        this.mLoaders.put(info.mId, info);
        if (this.mStarted) {
            info.start();
        }
    }

    public <D> android.support.v4.content.Loader<D> initLoader(int id, android.os.Bundle args, android.support.v4.app.LoaderManager.LoaderCallbacks<D> callback) {
        if (this.mCreatingLoader) {
            throw new java.lang.IllegalStateException("Called while creating a loader");
        }
        android.support.v4.app.LoaderManagerImpl.LoaderInfo info = (android.support.v4.app.LoaderManagerImpl.LoaderInfo) this.mLoaders.get(id);
        if (DEBUG) {
            android.util.Log.v(TAG, "initLoader in " + this + ": args=" + args);
        }
        if (info == null) {
            info = createAndInstallLoader(id, args, callback);
            if (DEBUG) {
                android.util.Log.v(TAG, "  Created new loader " + info);
            }
        } else {
            if (DEBUG) {
                android.util.Log.v(TAG, "  Re-using existing loader " + info);
            }
            info.mCallbacks = callback;
        }
        if (info.mHaveData && this.mStarted) {
            info.callOnLoadFinished(info.mLoader, info.mData);
        }
        return info.mLoader;
    }

    public <D> android.support.v4.content.Loader<D> restartLoader(int id, android.os.Bundle args, android.support.v4.app.LoaderManager.LoaderCallbacks<D> callback) {
        if (this.mCreatingLoader) {
            throw new java.lang.IllegalStateException("Called while creating a loader");
        }
        android.support.v4.app.LoaderManagerImpl.LoaderInfo info = (android.support.v4.app.LoaderManagerImpl.LoaderInfo) this.mLoaders.get(id);
        if (DEBUG) {
            android.util.Log.v(TAG, "restartLoader in " + this + ": args=" + args);
        }
        if (info != null) {
            android.support.v4.app.LoaderManagerImpl.LoaderInfo inactive = (android.support.v4.app.LoaderManagerImpl.LoaderInfo) this.mInactiveLoaders.get(id);
            if (inactive == null) {
                if (DEBUG) {
                    android.util.Log.v(TAG, "  Making last loader inactive: " + info);
                }
                info.mLoader.abandon();
                this.mInactiveLoaders.put(id, info);
            } else if (info.mHaveData) {
                if (DEBUG) {
                    android.util.Log.v(TAG, "  Removing last inactive loader: " + info);
                }
                inactive.mDeliveredData = false;
                inactive.destroy();
                info.mLoader.abandon();
                this.mInactiveLoaders.put(id, info);
            } else if (!info.cancel()) {
                if (DEBUG) {
                    android.util.Log.v(TAG, "  Current loader is stopped; replacing");
                }
                this.mLoaders.put(id, null);
                info.destroy();
            } else {
                if (DEBUG) {
                    android.util.Log.v(TAG, "  Current loader is running; configuring pending loader");
                }
                if (info.mPendingLoader != null) {
                    if (DEBUG) {
                        android.util.Log.v(TAG, "  Removing pending loader: " + info.mPendingLoader);
                    }
                    info.mPendingLoader.destroy();
                    info.mPendingLoader = null;
                }
                if (DEBUG) {
                    android.util.Log.v(TAG, "  Enqueuing as new pending loader");
                }
                info.mPendingLoader = createLoader(id, args, callback);
                return info.mPendingLoader.mLoader;
            }
        }
        return createAndInstallLoader(id, args, callback).mLoader;
    }

    public void destroyLoader(int id) {
        if (this.mCreatingLoader) {
            throw new java.lang.IllegalStateException("Called while creating a loader");
        }
        if (DEBUG) {
            android.util.Log.v(TAG, "destroyLoader in " + this + " of " + id);
        }
        int idx = this.mLoaders.indexOfKey(id);
        if (idx >= 0) {
            android.support.v4.app.LoaderManagerImpl.LoaderInfo info = (android.support.v4.app.LoaderManagerImpl.LoaderInfo) this.mLoaders.valueAt(idx);
            this.mLoaders.removeAt(idx);
            info.destroy();
        }
        int idx2 = this.mInactiveLoaders.indexOfKey(id);
        if (idx2 >= 0) {
            android.support.v4.app.LoaderManagerImpl.LoaderInfo info2 = (android.support.v4.app.LoaderManagerImpl.LoaderInfo) this.mInactiveLoaders.valueAt(idx2);
            this.mInactiveLoaders.removeAt(idx2);
            info2.destroy();
        }
        if (this.mHost != null && !hasRunningLoaders()) {
            this.mHost.mFragmentManager.startPendingDeferredFragments();
        }
    }

    public <D> android.support.v4.content.Loader<D> getLoader(int id) {
        if (this.mCreatingLoader) {
            throw new java.lang.IllegalStateException("Called while creating a loader");
        }
        android.support.v4.app.LoaderManagerImpl.LoaderInfo loaderInfo = (android.support.v4.app.LoaderManagerImpl.LoaderInfo) this.mLoaders.get(id);
        if (loaderInfo == null) {
            return null;
        }
        if (loaderInfo.mPendingLoader != null) {
            return loaderInfo.mPendingLoader.mLoader;
        }
        return loaderInfo.mLoader;
    }

    /* access modifiers changed from: 0000 */
    public void doStart() {
        if (DEBUG) {
            android.util.Log.v(TAG, "Starting in " + this);
        }
        if (this.mStarted) {
            java.lang.RuntimeException e = new java.lang.RuntimeException("here");
            e.fillInStackTrace();
            android.util.Log.w(TAG, "Called doStart when already started: " + this, e);
            return;
        }
        this.mStarted = true;
        for (int i = this.mLoaders.size() - 1; i >= 0; i--) {
            ((android.support.v4.app.LoaderManagerImpl.LoaderInfo) this.mLoaders.valueAt(i)).start();
        }
    }

    /* access modifiers changed from: 0000 */
    public void doStop() {
        if (DEBUG) {
            android.util.Log.v(TAG, "Stopping in " + this);
        }
        if (!this.mStarted) {
            java.lang.RuntimeException e = new java.lang.RuntimeException("here");
            e.fillInStackTrace();
            android.util.Log.w(TAG, "Called doStop when not started: " + this, e);
            return;
        }
        for (int i = this.mLoaders.size() - 1; i >= 0; i--) {
            ((android.support.v4.app.LoaderManagerImpl.LoaderInfo) this.mLoaders.valueAt(i)).stop();
        }
        this.mStarted = false;
    }

    /* access modifiers changed from: 0000 */
    public void doRetain() {
        if (DEBUG) {
            android.util.Log.v(TAG, "Retaining in " + this);
        }
        if (!this.mStarted) {
            java.lang.RuntimeException e = new java.lang.RuntimeException("here");
            e.fillInStackTrace();
            android.util.Log.w(TAG, "Called doRetain when not started: " + this, e);
            return;
        }
        this.mRetaining = true;
        this.mStarted = false;
        for (int i = this.mLoaders.size() - 1; i >= 0; i--) {
            ((android.support.v4.app.LoaderManagerImpl.LoaderInfo) this.mLoaders.valueAt(i)).retain();
        }
    }

    /* access modifiers changed from: 0000 */
    public void finishRetain() {
        if (this.mRetaining) {
            if (DEBUG) {
                android.util.Log.v(TAG, "Finished Retaining in " + this);
            }
            this.mRetaining = false;
            for (int i = this.mLoaders.size() - 1; i >= 0; i--) {
                ((android.support.v4.app.LoaderManagerImpl.LoaderInfo) this.mLoaders.valueAt(i)).finishRetain();
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void doReportNextStart() {
        for (int i = this.mLoaders.size() - 1; i >= 0; i--) {
            ((android.support.v4.app.LoaderManagerImpl.LoaderInfo) this.mLoaders.valueAt(i)).mReportNextStart = true;
        }
    }

    /* access modifiers changed from: 0000 */
    public void doReportStart() {
        for (int i = this.mLoaders.size() - 1; i >= 0; i--) {
            ((android.support.v4.app.LoaderManagerImpl.LoaderInfo) this.mLoaders.valueAt(i)).reportStart();
        }
    }

    /* access modifiers changed from: 0000 */
    public void doDestroy() {
        if (!this.mRetaining) {
            if (DEBUG) {
                android.util.Log.v(TAG, "Destroying Active in " + this);
            }
            for (int i = this.mLoaders.size() - 1; i >= 0; i--) {
                ((android.support.v4.app.LoaderManagerImpl.LoaderInfo) this.mLoaders.valueAt(i)).destroy();
            }
            this.mLoaders.clear();
        }
        if (DEBUG) {
            android.util.Log.v(TAG, "Destroying Inactive in " + this);
        }
        for (int i2 = this.mInactiveLoaders.size() - 1; i2 >= 0; i2--) {
            ((android.support.v4.app.LoaderManagerImpl.LoaderInfo) this.mInactiveLoaders.valueAt(i2)).destroy();
        }
        this.mInactiveLoaders.clear();
        this.mHost = null;
    }

    public java.lang.String toString() {
        java.lang.StringBuilder sb = new java.lang.StringBuilder(128);
        sb.append("LoaderManager{");
        sb.append(java.lang.Integer.toHexString(java.lang.System.identityHashCode(this)));
        sb.append(" in ");
        android.support.v4.util.DebugUtils.buildShortClassTag(this.mHost, sb);
        sb.append("}}");
        return sb.toString();
    }

    public void dump(java.lang.String prefix, java.io.FileDescriptor fd, java.io.PrintWriter writer, java.lang.String[] args) {
        if (this.mLoaders.size() > 0) {
            writer.print(prefix);
            writer.println("Active Loaders:");
            java.lang.String innerPrefix = prefix + "    ";
            for (int i = 0; i < this.mLoaders.size(); i++) {
                android.support.v4.app.LoaderManagerImpl.LoaderInfo li = (android.support.v4.app.LoaderManagerImpl.LoaderInfo) this.mLoaders.valueAt(i);
                writer.print(prefix);
                writer.print("  #");
                writer.print(this.mLoaders.keyAt(i));
                writer.print(": ");
                writer.println(li.toString());
                li.dump(innerPrefix, fd, writer, args);
            }
        }
        if (this.mInactiveLoaders.size() > 0) {
            writer.print(prefix);
            writer.println("Inactive Loaders:");
            java.lang.String innerPrefix2 = prefix + "    ";
            for (int i2 = 0; i2 < this.mInactiveLoaders.size(); i2++) {
                android.support.v4.app.LoaderManagerImpl.LoaderInfo li2 = (android.support.v4.app.LoaderManagerImpl.LoaderInfo) this.mInactiveLoaders.valueAt(i2);
                writer.print(prefix);
                writer.print("  #");
                writer.print(this.mInactiveLoaders.keyAt(i2));
                writer.print(": ");
                writer.println(li2.toString());
                li2.dump(innerPrefix2, fd, writer, args);
            }
        }
    }

    public boolean hasRunningLoaders() {
        boolean loadersRunning = false;
        for (int i = 0; i < this.mLoaders.size(); i++) {
            android.support.v4.app.LoaderManagerImpl.LoaderInfo li = (android.support.v4.app.LoaderManagerImpl.LoaderInfo) this.mLoaders.valueAt(i);
            loadersRunning |= li.mStarted && !li.mDeliveredData;
        }
        return loadersRunning;
    }
}
