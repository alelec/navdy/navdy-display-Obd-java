package android.support.v4.app;

abstract class BaseFragmentActivityHoneycomb extends android.support.v4.app.BaseFragmentActivityGingerbread {
    BaseFragmentActivityHoneycomb() {
    }

    public android.view.View onCreateView(android.view.View parent, java.lang.String name, android.content.Context context, android.util.AttributeSet attrs) {
        android.view.View v = dispatchFragmentsOnCreateView(parent, name, context, attrs);
        if (v != null || android.os.Build.VERSION.SDK_INT < 11) {
            return v;
        }
        return super.onCreateView(parent, name, context, attrs);
    }
}
