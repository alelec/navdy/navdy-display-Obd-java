package android.support.v4.app;

@android.annotation.TargetApi(18)
@android.support.annotation.RequiresApi(18)
class ActionBarDrawerToggleJellybeanMR2 {
    private static final java.lang.String TAG = "ActionBarDrawerToggleImplJellybeanMR2";
    private static final int[] THEME_ATTRS = {16843531};

    ActionBarDrawerToggleJellybeanMR2() {
    }

    public static java.lang.Object setActionBarUpIndicator(java.lang.Object info, android.app.Activity activity, android.graphics.drawable.Drawable drawable, int contentDescRes) {
        android.app.ActionBar actionBar = activity.getActionBar();
        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(drawable);
            actionBar.setHomeActionContentDescription(contentDescRes);
        }
        return info;
    }

    public static java.lang.Object setActionBarDescription(java.lang.Object info, android.app.Activity activity, int contentDescRes) {
        android.app.ActionBar actionBar = activity.getActionBar();
        if (actionBar != null) {
            actionBar.setHomeActionContentDescription(contentDescRes);
        }
        return info;
    }

    public static android.graphics.drawable.Drawable getThemeUpIndicator(android.app.Activity activity) {
        android.content.Context context;
        android.app.ActionBar actionBar = activity.getActionBar();
        if (actionBar != null) {
            context = actionBar.getThemedContext();
        } else {
            context = activity;
        }
        android.content.res.TypedArray a = context.obtainStyledAttributes(null, THEME_ATTRS, 16843470, 0);
        android.graphics.drawable.Drawable result = a.getDrawable(0);
        a.recycle();
        return result;
    }
}
