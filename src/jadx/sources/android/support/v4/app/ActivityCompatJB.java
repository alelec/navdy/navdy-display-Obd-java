package android.support.v4.app;

@android.annotation.TargetApi(16)
@android.support.annotation.RequiresApi(16)
class ActivityCompatJB {
    ActivityCompatJB() {
    }

    public static void startActivityForResult(android.app.Activity activity, android.content.Intent intent, int requestCode, android.os.Bundle options) {
        activity.startActivityForResult(intent, requestCode, options);
    }

    public static void startIntentSenderForResult(android.app.Activity activity, android.content.IntentSender intent, int requestCode, android.content.Intent fillInIntent, int flagsMask, int flagsValues, int extraFlags, android.os.Bundle options) throws android.content.IntentSender.SendIntentException {
        activity.startIntentSenderForResult(intent, requestCode, fillInIntent, flagsMask, flagsValues, extraFlags, options);
    }

    public static void finishAffinity(android.app.Activity activity) {
        activity.finishAffinity();
    }
}
