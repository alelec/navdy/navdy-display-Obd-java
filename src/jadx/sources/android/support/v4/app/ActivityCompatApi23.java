package android.support.v4.app;

@android.annotation.TargetApi(23)
@android.support.annotation.RequiresApi(23)
class ActivityCompatApi23 {

    public interface OnSharedElementsReadyListenerBridge {
        void onSharedElementsReady();
    }

    public interface RequestPermissionsRequestCodeValidator {
        void validateRequestPermissionsRequestCode(int i);
    }

    public static abstract class SharedElementCallback23 extends android.support.v4.app.ActivityCompatApi21.SharedElementCallback21 {
        public abstract void onSharedElementsArrived(java.util.List<java.lang.String> list, java.util.List<android.view.View> list2, android.support.v4.app.ActivityCompatApi23.OnSharedElementsReadyListenerBridge onSharedElementsReadyListenerBridge);
    }

    private static class SharedElementCallbackImpl extends android.app.SharedElementCallback {
        private android.support.v4.app.ActivityCompatApi23.SharedElementCallback23 mCallback;

        public SharedElementCallbackImpl(android.support.v4.app.ActivityCompatApi23.SharedElementCallback23 callback) {
            this.mCallback = callback;
        }

        public void onSharedElementStart(java.util.List<java.lang.String> sharedElementNames, java.util.List<android.view.View> sharedElements, java.util.List<android.view.View> sharedElementSnapshots) {
            this.mCallback.onSharedElementStart(sharedElementNames, sharedElements, sharedElementSnapshots);
        }

        public void onSharedElementEnd(java.util.List<java.lang.String> sharedElementNames, java.util.List<android.view.View> sharedElements, java.util.List<android.view.View> sharedElementSnapshots) {
            this.mCallback.onSharedElementEnd(sharedElementNames, sharedElements, sharedElementSnapshots);
        }

        public void onRejectSharedElements(java.util.List<android.view.View> rejectedSharedElements) {
            this.mCallback.onRejectSharedElements(rejectedSharedElements);
        }

        public void onMapSharedElements(java.util.List<java.lang.String> names, java.util.Map<java.lang.String, android.view.View> sharedElements) {
            this.mCallback.onMapSharedElements(names, sharedElements);
        }

        public android.os.Parcelable onCaptureSharedElementSnapshot(android.view.View sharedElement, android.graphics.Matrix viewToGlobalMatrix, android.graphics.RectF screenBounds) {
            return this.mCallback.onCaptureSharedElementSnapshot(sharedElement, viewToGlobalMatrix, screenBounds);
        }

        public android.view.View onCreateSnapshotView(android.content.Context context, android.os.Parcelable snapshot) {
            return this.mCallback.onCreateSnapshotView(context, snapshot);
        }

        public void onSharedElementsArrived(java.util.List<java.lang.String> sharedElementNames, java.util.List<android.view.View> sharedElements, final android.app.SharedElementCallback.OnSharedElementsReadyListener listener) {
            this.mCallback.onSharedElementsArrived(sharedElementNames, sharedElements, new android.support.v4.app.ActivityCompatApi23.OnSharedElementsReadyListenerBridge() {
                public void onSharedElementsReady() {
                    listener.onSharedElementsReady();
                }
            });
        }
    }

    ActivityCompatApi23() {
    }

    public static void requestPermissions(android.app.Activity activity, java.lang.String[] permissions, int requestCode) {
        if (activity instanceof android.support.v4.app.ActivityCompatApi23.RequestPermissionsRequestCodeValidator) {
            ((android.support.v4.app.ActivityCompatApi23.RequestPermissionsRequestCodeValidator) activity).validateRequestPermissionsRequestCode(requestCode);
        }
        activity.requestPermissions(permissions, requestCode);
    }

    public static boolean shouldShowRequestPermissionRationale(android.app.Activity activity, java.lang.String permission) {
        return activity.shouldShowRequestPermissionRationale(permission);
    }

    public static void setEnterSharedElementCallback(android.app.Activity activity, android.support.v4.app.ActivityCompatApi23.SharedElementCallback23 callback) {
        activity.setEnterSharedElementCallback(createCallback(callback));
    }

    public static void setExitSharedElementCallback(android.app.Activity activity, android.support.v4.app.ActivityCompatApi23.SharedElementCallback23 callback) {
        activity.setExitSharedElementCallback(createCallback(callback));
    }

    private static android.app.SharedElementCallback createCallback(android.support.v4.app.ActivityCompatApi23.SharedElementCallback23 callback) {
        if (callback != null) {
            return new android.support.v4.app.ActivityCompatApi23.SharedElementCallbackImpl(callback);
        }
        return null;
    }
}
