package android.support.v4.app;

class FragmentTransition {
    private static final int[] INVERSE_OPS = {0, 3, 0, 1, 5, 4, 7, 6};

    static class FragmentContainerTransition {
        public android.support.v4.app.Fragment firstOut;
        public boolean firstOutIsPop;
        public android.support.v4.app.BackStackRecord firstOutTransaction;
        public android.support.v4.app.Fragment lastIn;
        public boolean lastInIsPop;
        public android.support.v4.app.BackStackRecord lastInTransaction;

        FragmentContainerTransition() {
        }
    }

    FragmentTransition() {
    }

    static void startTransitions(android.support.v4.app.FragmentManagerImpl fragmentManager, java.util.ArrayList<android.support.v4.app.BackStackRecord> records, java.util.ArrayList<java.lang.Boolean> isRecordPop, int startIndex, int endIndex, boolean isOptimized) {
        if (fragmentManager.mCurState >= 1 && android.os.Build.VERSION.SDK_INT >= 21) {
            android.util.SparseArray<android.support.v4.app.FragmentTransition.FragmentContainerTransition> transitioningFragments = new android.util.SparseArray<>();
            for (int i = startIndex; i < endIndex; i++) {
                android.support.v4.app.BackStackRecord record = (android.support.v4.app.BackStackRecord) records.get(i);
                if (((java.lang.Boolean) isRecordPop.get(i)).booleanValue()) {
                    calculatePopFragments(record, transitioningFragments, isOptimized);
                } else {
                    calculateFragments(record, transitioningFragments, isOptimized);
                }
            }
            if (transitioningFragments.size() != 0) {
                android.view.View nonExistentView = new android.view.View(fragmentManager.mHost.getContext());
                int numContainers = transitioningFragments.size();
                for (int i2 = 0; i2 < numContainers; i2++) {
                    int containerId = transitioningFragments.keyAt(i2);
                    android.support.v4.util.ArrayMap<java.lang.String, java.lang.String> nameOverrides = calculateNameOverrides(containerId, records, isRecordPop, startIndex, endIndex);
                    android.support.v4.app.FragmentTransition.FragmentContainerTransition containerTransition = (android.support.v4.app.FragmentTransition.FragmentContainerTransition) transitioningFragments.valueAt(i2);
                    if (isOptimized) {
                        configureTransitionsOptimized(fragmentManager, containerId, containerTransition, nonExistentView, nameOverrides);
                    } else {
                        configureTransitionsUnoptimized(fragmentManager, containerId, containerTransition, nonExistentView, nameOverrides);
                    }
                }
            }
        }
    }

    private static android.support.v4.util.ArrayMap<java.lang.String, java.lang.String> calculateNameOverrides(int containerId, java.util.ArrayList<android.support.v4.app.BackStackRecord> records, java.util.ArrayList<java.lang.Boolean> isRecordPop, int startIndex, int endIndex) {
        java.util.ArrayList<java.lang.String> sources;
        java.util.ArrayList<java.lang.String> targets;
        android.support.v4.util.ArrayMap<java.lang.String, java.lang.String> nameOverrides = new android.support.v4.util.ArrayMap<>();
        for (int recordNum = endIndex - 1; recordNum >= startIndex; recordNum--) {
            android.support.v4.app.BackStackRecord record = (android.support.v4.app.BackStackRecord) records.get(recordNum);
            if (record.interactsWith(containerId)) {
                boolean isPop = ((java.lang.Boolean) isRecordPop.get(recordNum)).booleanValue();
                if (record.mSharedElementSourceNames != null) {
                    int numSharedElements = record.mSharedElementSourceNames.size();
                    if (isPop) {
                        targets = record.mSharedElementSourceNames;
                        sources = record.mSharedElementTargetNames;
                    } else {
                        sources = record.mSharedElementSourceNames;
                        targets = record.mSharedElementTargetNames;
                    }
                    for (int i = 0; i < numSharedElements; i++) {
                        java.lang.String sourceName = (java.lang.String) sources.get(i);
                        java.lang.String targetName = (java.lang.String) targets.get(i);
                        java.lang.String previousTarget = (java.lang.String) nameOverrides.remove(targetName);
                        if (previousTarget != null) {
                            nameOverrides.put(sourceName, previousTarget);
                        } else {
                            nameOverrides.put(sourceName, targetName);
                        }
                    }
                }
            }
        }
        return nameOverrides;
    }

    private static void configureTransitionsOptimized(android.support.v4.app.FragmentManagerImpl fragmentManager, int containerId, android.support.v4.app.FragmentTransition.FragmentContainerTransition fragments, android.view.View nonExistentView, android.support.v4.util.ArrayMap<java.lang.String, java.lang.String> nameOverrides) {
        android.view.ViewGroup sceneRoot = null;
        if (fragmentManager.mContainer.onHasView()) {
            sceneRoot = (android.view.ViewGroup) fragmentManager.mContainer.onFindViewById(containerId);
        }
        if (sceneRoot != null) {
            android.support.v4.app.Fragment inFragment = fragments.lastIn;
            android.support.v4.app.Fragment outFragment = fragments.firstOut;
            boolean inIsPop = fragments.lastInIsPop;
            boolean outIsPop = fragments.firstOutIsPop;
            java.util.ArrayList<android.view.View> sharedElementsIn = new java.util.ArrayList<>();
            java.util.ArrayList<android.view.View> sharedElementsOut = new java.util.ArrayList<>();
            java.lang.Object enterTransition = getEnterTransition(inFragment, inIsPop);
            java.lang.Object exitTransition = getExitTransition(outFragment, outIsPop);
            java.lang.Object sharedElementTransition = configureSharedElementsOptimized(sceneRoot, nonExistentView, nameOverrides, fragments, sharedElementsOut, sharedElementsIn, enterTransition, exitTransition);
            if (enterTransition != null || sharedElementTransition != null || exitTransition != null) {
                java.util.ArrayList<android.view.View> exitingViews = configureEnteringExitingViews(exitTransition, outFragment, sharedElementsOut, nonExistentView);
                java.util.ArrayList<android.view.View> enteringViews = configureEnteringExitingViews(enterTransition, inFragment, sharedElementsIn, nonExistentView);
                setViewVisibility(enteringViews, 4);
                java.lang.Object transition = mergeTransitions(enterTransition, exitTransition, sharedElementTransition, inFragment, inIsPop);
                if (transition != null) {
                    replaceHide(exitTransition, outFragment, exitingViews);
                    java.util.ArrayList<java.lang.String> inNames = android.support.v4.app.FragmentTransitionCompat21.prepareSetNameOverridesOptimized(sharedElementsIn);
                    android.support.v4.app.FragmentTransitionCompat21.scheduleRemoveTargets(transition, enterTransition, enteringViews, exitTransition, exitingViews, sharedElementTransition, sharedElementsIn);
                    android.support.v4.app.FragmentTransitionCompat21.beginDelayedTransition(sceneRoot, transition);
                    android.support.v4.app.FragmentTransitionCompat21.setNameOverridesOptimized(sceneRoot, sharedElementsOut, sharedElementsIn, inNames, nameOverrides);
                    setViewVisibility(enteringViews, 0);
                    android.support.v4.app.FragmentTransitionCompat21.swapSharedElementTargets(sharedElementTransition, sharedElementsOut, sharedElementsIn);
                }
            }
        }
    }

    private static void replaceHide(java.lang.Object exitTransition, android.support.v4.app.Fragment exitingFragment, final java.util.ArrayList<android.view.View> exitingViews) {
        if (exitingFragment != null && exitTransition != null && exitingFragment.mAdded && exitingFragment.mHidden && exitingFragment.mHiddenChanged) {
            exitingFragment.setHideReplaced(true);
            android.support.v4.app.FragmentTransitionCompat21.scheduleHideFragmentView(exitTransition, exitingFragment.getView(), exitingViews);
            android.support.v4.app.OneShotPreDrawListener.add(exitingFragment.mContainer, new java.lang.Runnable() {
                public void run() {
                    android.support.v4.app.FragmentTransition.setViewVisibility(exitingViews, 4);
                }
            });
        }
    }

    private static void configureTransitionsUnoptimized(android.support.v4.app.FragmentManagerImpl fragmentManager, int containerId, android.support.v4.app.FragmentTransition.FragmentContainerTransition fragments, android.view.View nonExistentView, android.support.v4.util.ArrayMap<java.lang.String, java.lang.String> nameOverrides) {
        android.view.ViewGroup sceneRoot = null;
        if (fragmentManager.mContainer.onHasView()) {
            sceneRoot = (android.view.ViewGroup) fragmentManager.mContainer.onFindViewById(containerId);
        }
        if (sceneRoot != null) {
            android.support.v4.app.Fragment inFragment = fragments.lastIn;
            android.support.v4.app.Fragment outFragment = fragments.firstOut;
            boolean inIsPop = fragments.lastInIsPop;
            boolean outIsPop = fragments.firstOutIsPop;
            java.lang.Object enterTransition = getEnterTransition(inFragment, inIsPop);
            java.lang.Object exitTransition = getExitTransition(outFragment, outIsPop);
            java.util.ArrayList<android.view.View> sharedElementsOut = new java.util.ArrayList<>();
            java.util.ArrayList<android.view.View> sharedElementsIn = new java.util.ArrayList<>();
            java.lang.Object sharedElementTransition = configureSharedElementsUnoptimized(sceneRoot, nonExistentView, nameOverrides, fragments, sharedElementsOut, sharedElementsIn, enterTransition, exitTransition);
            if (enterTransition != null || sharedElementTransition != null || exitTransition != null) {
                java.util.ArrayList<android.view.View> exitingViews = configureEnteringExitingViews(exitTransition, outFragment, sharedElementsOut, nonExistentView);
                if (exitingViews == null || exitingViews.isEmpty()) {
                    exitTransition = null;
                }
                android.support.v4.app.FragmentTransitionCompat21.addTarget(enterTransition, nonExistentView);
                java.lang.Object transition = mergeTransitions(enterTransition, exitTransition, sharedElementTransition, inFragment, fragments.lastInIsPop);
                if (transition != null) {
                    java.util.ArrayList<android.view.View> enteringViews = new java.util.ArrayList<>();
                    android.support.v4.app.FragmentTransitionCompat21.scheduleRemoveTargets(transition, enterTransition, enteringViews, exitTransition, exitingViews, sharedElementTransition, sharedElementsIn);
                    scheduleTargetChange(sceneRoot, inFragment, nonExistentView, sharedElementsIn, enterTransition, enteringViews, exitTransition, exitingViews);
                    android.support.v4.app.FragmentTransitionCompat21.setNameOverridesUnoptimized(sceneRoot, sharedElementsIn, nameOverrides);
                    android.support.v4.app.FragmentTransitionCompat21.beginDelayedTransition(sceneRoot, transition);
                    android.support.v4.app.FragmentTransitionCompat21.scheduleNameReset(sceneRoot, sharedElementsIn, nameOverrides);
                }
            }
        }
    }

    private static void scheduleTargetChange(android.view.ViewGroup sceneRoot, android.support.v4.app.Fragment inFragment, android.view.View nonExistentView, java.util.ArrayList<android.view.View> sharedElementsIn, java.lang.Object enterTransition, java.util.ArrayList<android.view.View> enteringViews, java.lang.Object exitTransition, java.util.ArrayList<android.view.View> exitingViews) {
        final java.lang.Object obj = enterTransition;
        final android.view.View view = nonExistentView;
        final android.support.v4.app.Fragment fragment = inFragment;
        final java.util.ArrayList<android.view.View> arrayList = sharedElementsIn;
        final java.util.ArrayList<android.view.View> arrayList2 = enteringViews;
        final java.util.ArrayList<android.view.View> arrayList3 = exitingViews;
        final java.lang.Object obj2 = exitTransition;
        android.support.v4.app.OneShotPreDrawListener.add(sceneRoot, new java.lang.Runnable() {
            public void run() {
                if (obj != null) {
                    android.support.v4.app.FragmentTransitionCompat21.removeTarget(obj, view);
                    arrayList2.addAll(android.support.v4.app.FragmentTransition.configureEnteringExitingViews(obj, fragment, arrayList, view));
                }
                if (arrayList3 != null) {
                    if (obj2 != null) {
                        java.util.ArrayList<android.view.View> tempExiting = new java.util.ArrayList<>();
                        tempExiting.add(view);
                        android.support.v4.app.FragmentTransitionCompat21.replaceTargets(obj2, arrayList3, tempExiting);
                    }
                    arrayList3.clear();
                    arrayList3.add(view);
                }
            }
        });
    }

    private static java.lang.Object getSharedElementTransition(android.support.v4.app.Fragment inFragment, android.support.v4.app.Fragment outFragment, boolean isPop) {
        java.lang.Object sharedElementEnterTransition;
        if (inFragment == null || outFragment == null) {
            return null;
        }
        if (isPop) {
            sharedElementEnterTransition = outFragment.getSharedElementReturnTransition();
        } else {
            sharedElementEnterTransition = inFragment.getSharedElementEnterTransition();
        }
        return android.support.v4.app.FragmentTransitionCompat21.wrapTransitionInSet(android.support.v4.app.FragmentTransitionCompat21.cloneTransition(sharedElementEnterTransition));
    }

    private static java.lang.Object getEnterTransition(android.support.v4.app.Fragment inFragment, boolean isPop) {
        java.lang.Object enterTransition;
        if (inFragment == null) {
            return null;
        }
        if (isPop) {
            enterTransition = inFragment.getReenterTransition();
        } else {
            enterTransition = inFragment.getEnterTransition();
        }
        return android.support.v4.app.FragmentTransitionCompat21.cloneTransition(enterTransition);
    }

    private static java.lang.Object getExitTransition(android.support.v4.app.Fragment outFragment, boolean isPop) {
        java.lang.Object exitTransition;
        if (outFragment == null) {
            return null;
        }
        if (isPop) {
            exitTransition = outFragment.getReturnTransition();
        } else {
            exitTransition = outFragment.getExitTransition();
        }
        return android.support.v4.app.FragmentTransitionCompat21.cloneTransition(exitTransition);
    }

    private static java.lang.Object configureSharedElementsOptimized(android.view.ViewGroup sceneRoot, android.view.View nonExistentView, android.support.v4.util.ArrayMap<java.lang.String, java.lang.String> nameOverrides, android.support.v4.app.FragmentTransition.FragmentContainerTransition fragments, java.util.ArrayList<android.view.View> sharedElementsOut, java.util.ArrayList<android.view.View> sharedElementsIn, java.lang.Object enterTransition, java.lang.Object exitTransition) {
        java.lang.Object sharedElementTransition;
        final android.graphics.Rect epicenter;
        final android.view.View epicenterView;
        final android.support.v4.app.Fragment inFragment = fragments.lastIn;
        final android.support.v4.app.Fragment outFragment = fragments.firstOut;
        if (inFragment != null) {
            inFragment.getView().setVisibility(0);
        }
        if (inFragment == null || outFragment == null) {
            return null;
        }
        final boolean inIsPop = fragments.lastInIsPop;
        if (nameOverrides.isEmpty()) {
            sharedElementTransition = null;
        } else {
            sharedElementTransition = getSharedElementTransition(inFragment, outFragment, inIsPop);
        }
        android.support.v4.util.ArrayMap<java.lang.String, android.view.View> outSharedElements = captureOutSharedElements(nameOverrides, sharedElementTransition, fragments);
        final android.support.v4.util.ArrayMap<java.lang.String, android.view.View> inSharedElements = captureInSharedElements(nameOverrides, sharedElementTransition, fragments);
        if (nameOverrides.isEmpty()) {
            sharedElementTransition = null;
            if (outSharedElements != null) {
                outSharedElements.clear();
            }
            if (inSharedElements != null) {
                inSharedElements.clear();
            }
        } else {
            addSharedElementsWithMatchingNames(sharedElementsOut, outSharedElements, nameOverrides.keySet());
            addSharedElementsWithMatchingNames(sharedElementsIn, inSharedElements, nameOverrides.values());
        }
        if (enterTransition == null && exitTransition == null && sharedElementTransition == null) {
            return null;
        }
        callSharedElementStartEnd(inFragment, outFragment, inIsPop, outSharedElements, true);
        if (sharedElementTransition != null) {
            sharedElementsIn.add(nonExistentView);
            android.support.v4.app.FragmentTransitionCompat21.setSharedElementTargets(sharedElementTransition, nonExistentView, sharedElementsOut);
            setOutEpicenter(sharedElementTransition, exitTransition, outSharedElements, fragments.firstOutIsPop, fragments.firstOutTransaction);
            epicenter = new android.graphics.Rect();
            epicenterView = getInEpicenterView(inSharedElements, fragments, enterTransition, inIsPop);
            if (epicenterView != null) {
                android.support.v4.app.FragmentTransitionCompat21.setEpicenter(enterTransition, epicenter);
            }
        } else {
            epicenter = null;
            epicenterView = null;
        }
        android.support.v4.app.OneShotPreDrawListener.add(sceneRoot, new java.lang.Runnable() {
            public void run() {
                android.support.v4.app.FragmentTransition.callSharedElementStartEnd(inFragment, outFragment, inIsPop, inSharedElements, false);
                if (epicenterView != null) {
                    android.support.v4.app.FragmentTransitionCompat21.getBoundsOnScreen(epicenterView, epicenter);
                }
            }
        });
        return sharedElementTransition;
    }

    private static void addSharedElementsWithMatchingNames(java.util.ArrayList<android.view.View> views, android.support.v4.util.ArrayMap<java.lang.String, android.view.View> sharedElements, java.util.Collection<java.lang.String> nameOverridesSet) {
        for (int i = sharedElements.size() - 1; i >= 0; i--) {
            android.view.View view = (android.view.View) sharedElements.valueAt(i);
            if (nameOverridesSet.contains(android.support.v4.view.ViewCompat.getTransitionName(view))) {
                views.add(view);
            }
        }
    }

    private static java.lang.Object configureSharedElementsUnoptimized(android.view.ViewGroup sceneRoot, android.view.View nonExistentView, android.support.v4.util.ArrayMap<java.lang.String, java.lang.String> nameOverrides, android.support.v4.app.FragmentTransition.FragmentContainerTransition fragments, java.util.ArrayList<android.view.View> sharedElementsOut, java.util.ArrayList<android.view.View> sharedElementsIn, java.lang.Object enterTransition, java.lang.Object exitTransition) {
        java.lang.Object sharedElementTransition;
        final java.lang.Object sharedElementTransition2;
        final android.graphics.Rect inEpicenter;
        final android.support.v4.app.Fragment inFragment = fragments.lastIn;
        final android.support.v4.app.Fragment outFragment = fragments.firstOut;
        if (inFragment == null || outFragment == null) {
            return null;
        }
        final boolean inIsPop = fragments.lastInIsPop;
        if (nameOverrides.isEmpty()) {
            sharedElementTransition = null;
        } else {
            sharedElementTransition = getSharedElementTransition(inFragment, outFragment, inIsPop);
        }
        android.support.v4.util.ArrayMap<java.lang.String, android.view.View> outSharedElements = captureOutSharedElements(nameOverrides, sharedElementTransition, fragments);
        if (nameOverrides.isEmpty()) {
            sharedElementTransition2 = null;
        } else {
            sharedElementsOut.addAll(outSharedElements.values());
            sharedElementTransition2 = sharedElementTransition;
        }
        if (enterTransition == null && exitTransition == null && sharedElementTransition2 == null) {
            return null;
        }
        callSharedElementStartEnd(inFragment, outFragment, inIsPop, outSharedElements, true);
        if (sharedElementTransition2 != null) {
            inEpicenter = new android.graphics.Rect();
            android.support.v4.app.FragmentTransitionCompat21.setSharedElementTargets(sharedElementTransition2, nonExistentView, sharedElementsOut);
            setOutEpicenter(sharedElementTransition2, exitTransition, outSharedElements, fragments.firstOutIsPop, fragments.firstOutTransaction);
            if (enterTransition != null) {
                android.support.v4.app.FragmentTransitionCompat21.setEpicenter(enterTransition, inEpicenter);
            }
        } else {
            inEpicenter = null;
        }
        final android.support.v4.util.ArrayMap<java.lang.String, java.lang.String> arrayMap = nameOverrides;
        final android.support.v4.app.FragmentTransition.FragmentContainerTransition fragmentContainerTransition = fragments;
        final java.util.ArrayList<android.view.View> arrayList = sharedElementsIn;
        final android.view.View view = nonExistentView;
        final java.util.ArrayList<android.view.View> arrayList2 = sharedElementsOut;
        final java.lang.Object obj = enterTransition;
        android.support.v4.app.OneShotPreDrawListener.add(sceneRoot, new java.lang.Runnable() {
            public void run() {
                android.support.v4.util.ArrayMap<java.lang.String, android.view.View> inSharedElements = android.support.v4.app.FragmentTransition.captureInSharedElements(arrayMap, sharedElementTransition2, fragmentContainerTransition);
                if (inSharedElements != null) {
                    arrayList.addAll(inSharedElements.values());
                    arrayList.add(view);
                }
                android.support.v4.app.FragmentTransition.callSharedElementStartEnd(inFragment, outFragment, inIsPop, inSharedElements, false);
                if (sharedElementTransition2 != null) {
                    android.support.v4.app.FragmentTransitionCompat21.swapSharedElementTargets(sharedElementTransition2, arrayList2, arrayList);
                    android.view.View inEpicenterView = android.support.v4.app.FragmentTransition.getInEpicenterView(inSharedElements, fragmentContainerTransition, obj, inIsPop);
                    if (inEpicenterView != null) {
                        android.support.v4.app.FragmentTransitionCompat21.getBoundsOnScreen(inEpicenterView, inEpicenter);
                    }
                }
            }
        });
        return sharedElementTransition2;
    }

    private static android.support.v4.util.ArrayMap<java.lang.String, android.view.View> captureOutSharedElements(android.support.v4.util.ArrayMap<java.lang.String, java.lang.String> nameOverrides, java.lang.Object sharedElementTransition, android.support.v4.app.FragmentTransition.FragmentContainerTransition fragments) {
        android.support.v4.app.SharedElementCallback sharedElementCallback;
        java.util.ArrayList<java.lang.String> names;
        if (nameOverrides.isEmpty() || sharedElementTransition == null) {
            nameOverrides.clear();
            return null;
        }
        android.support.v4.app.Fragment outFragment = fragments.firstOut;
        android.support.v4.util.ArrayMap<java.lang.String, android.view.View> outSharedElements = new android.support.v4.util.ArrayMap<>();
        android.support.v4.app.FragmentTransitionCompat21.findNamedViews(outSharedElements, outFragment.getView());
        android.support.v4.app.BackStackRecord outTransaction = fragments.firstOutTransaction;
        if (fragments.firstOutIsPop) {
            sharedElementCallback = outFragment.getEnterTransitionCallback();
            names = outTransaction.mSharedElementTargetNames;
        } else {
            sharedElementCallback = outFragment.getExitTransitionCallback();
            names = outTransaction.mSharedElementSourceNames;
        }
        outSharedElements.retainAll(names);
        if (sharedElementCallback != null) {
            sharedElementCallback.onMapSharedElements(names, outSharedElements);
            for (int i = names.size() - 1; i >= 0; i--) {
                java.lang.String name = (java.lang.String) names.get(i);
                android.view.View view = (android.view.View) outSharedElements.get(name);
                if (view == null) {
                    nameOverrides.remove(name);
                } else if (!name.equals(android.support.v4.view.ViewCompat.getTransitionName(view))) {
                    nameOverrides.put(android.support.v4.view.ViewCompat.getTransitionName(view), (java.lang.String) nameOverrides.remove(name));
                }
            }
            return outSharedElements;
        }
        nameOverrides.retainAll(outSharedElements.keySet());
        return outSharedElements;
    }

    /* access modifiers changed from: private */
    public static android.support.v4.util.ArrayMap<java.lang.String, android.view.View> captureInSharedElements(android.support.v4.util.ArrayMap<java.lang.String, java.lang.String> nameOverrides, java.lang.Object sharedElementTransition, android.support.v4.app.FragmentTransition.FragmentContainerTransition fragments) {
        android.support.v4.app.SharedElementCallback sharedElementCallback;
        java.util.ArrayList<java.lang.String> names;
        android.support.v4.app.Fragment inFragment = fragments.lastIn;
        android.view.View fragmentView = inFragment.getView();
        if (nameOverrides.isEmpty() || sharedElementTransition == null || fragmentView == null) {
            nameOverrides.clear();
            return null;
        }
        android.support.v4.util.ArrayMap<java.lang.String, android.view.View> inSharedElements = new android.support.v4.util.ArrayMap<>();
        android.support.v4.app.FragmentTransitionCompat21.findNamedViews(inSharedElements, fragmentView);
        android.support.v4.app.BackStackRecord inTransaction = fragments.lastInTransaction;
        if (fragments.lastInIsPop) {
            sharedElementCallback = inFragment.getExitTransitionCallback();
            names = inTransaction.mSharedElementSourceNames;
        } else {
            sharedElementCallback = inFragment.getEnterTransitionCallback();
            names = inTransaction.mSharedElementTargetNames;
        }
        if (names != null) {
            inSharedElements.retainAll(names);
        }
        if (sharedElementCallback != null) {
            sharedElementCallback.onMapSharedElements(names, inSharedElements);
            for (int i = names.size() - 1; i >= 0; i--) {
                java.lang.String name = (java.lang.String) names.get(i);
                android.view.View view = (android.view.View) inSharedElements.get(name);
                if (view == null) {
                    java.lang.String key = findKeyForValue(nameOverrides, name);
                    if (key != null) {
                        nameOverrides.remove(key);
                    }
                } else if (!name.equals(android.support.v4.view.ViewCompat.getTransitionName(view))) {
                    java.lang.String key2 = findKeyForValue(nameOverrides, name);
                    if (key2 != null) {
                        nameOverrides.put(key2, android.support.v4.view.ViewCompat.getTransitionName(view));
                    }
                }
            }
            return inSharedElements;
        }
        retainValues(nameOverrides, inSharedElements);
        return inSharedElements;
    }

    private static java.lang.String findKeyForValue(android.support.v4.util.ArrayMap<java.lang.String, java.lang.String> map, java.lang.String value) {
        int numElements = map.size();
        for (int i = 0; i < numElements; i++) {
            if (value.equals(map.valueAt(i))) {
                return (java.lang.String) map.keyAt(i);
            }
        }
        return null;
    }

    /* access modifiers changed from: private */
    public static android.view.View getInEpicenterView(android.support.v4.util.ArrayMap<java.lang.String, android.view.View> inSharedElements, android.support.v4.app.FragmentTransition.FragmentContainerTransition fragments, java.lang.Object enterTransition, boolean inIsPop) {
        java.lang.String targetName;
        android.support.v4.app.BackStackRecord inTransaction = fragments.lastInTransaction;
        if (enterTransition == null || inSharedElements == null || inTransaction.mSharedElementSourceNames == null || inTransaction.mSharedElementSourceNames.isEmpty()) {
            return null;
        }
        if (inIsPop) {
            targetName = (java.lang.String) inTransaction.mSharedElementSourceNames.get(0);
        } else {
            targetName = (java.lang.String) inTransaction.mSharedElementTargetNames.get(0);
        }
        return (android.view.View) inSharedElements.get(targetName);
    }

    private static void setOutEpicenter(java.lang.Object sharedElementTransition, java.lang.Object exitTransition, android.support.v4.util.ArrayMap<java.lang.String, android.view.View> outSharedElements, boolean outIsPop, android.support.v4.app.BackStackRecord outTransaction) {
        java.lang.String sourceName;
        if (outTransaction.mSharedElementSourceNames != null && !outTransaction.mSharedElementSourceNames.isEmpty()) {
            if (outIsPop) {
                sourceName = (java.lang.String) outTransaction.mSharedElementTargetNames.get(0);
            } else {
                sourceName = (java.lang.String) outTransaction.mSharedElementSourceNames.get(0);
            }
            android.view.View outEpicenterView = (android.view.View) outSharedElements.get(sourceName);
            android.support.v4.app.FragmentTransitionCompat21.setEpicenter(sharedElementTransition, outEpicenterView);
            if (exitTransition != null) {
                android.support.v4.app.FragmentTransitionCompat21.setEpicenter(exitTransition, outEpicenterView);
            }
        }
    }

    private static void retainValues(android.support.v4.util.ArrayMap<java.lang.String, java.lang.String> nameOverrides, android.support.v4.util.ArrayMap<java.lang.String, android.view.View> namedViews) {
        for (int i = nameOverrides.size() - 1; i >= 0; i--) {
            if (!namedViews.containsKey((java.lang.String) nameOverrides.valueAt(i))) {
                nameOverrides.removeAt(i);
            }
        }
    }

    /* access modifiers changed from: private */
    public static void callSharedElementStartEnd(android.support.v4.app.Fragment inFragment, android.support.v4.app.Fragment outFragment, boolean isPop, android.support.v4.util.ArrayMap<java.lang.String, android.view.View> sharedElements, boolean isStart) {
        android.support.v4.app.SharedElementCallback sharedElementCallback;
        if (isPop) {
            sharedElementCallback = outFragment.getEnterTransitionCallback();
        } else {
            sharedElementCallback = inFragment.getEnterTransitionCallback();
        }
        if (sharedElementCallback != null) {
            java.util.ArrayList<android.view.View> views = new java.util.ArrayList<>();
            java.util.ArrayList<java.lang.String> names = new java.util.ArrayList<>();
            int count = sharedElements == null ? 0 : sharedElements.size();
            for (int i = 0; i < count; i++) {
                names.add(sharedElements.keyAt(i));
                views.add(sharedElements.valueAt(i));
            }
            if (isStart) {
                sharedElementCallback.onSharedElementStart(names, views, null);
            } else {
                sharedElementCallback.onSharedElementEnd(names, views, null);
            }
        }
    }

    /* access modifiers changed from: private */
    public static java.util.ArrayList<android.view.View> configureEnteringExitingViews(java.lang.Object transition, android.support.v4.app.Fragment fragment, java.util.ArrayList<android.view.View> sharedElements, android.view.View nonExistentView) {
        java.util.ArrayList<android.view.View> viewList = null;
        if (transition != null) {
            viewList = new java.util.ArrayList<>();
            android.view.View root = fragment.getView();
            if (root != null) {
                android.support.v4.app.FragmentTransitionCompat21.captureTransitioningViews(viewList, root);
            }
            if (sharedElements != null) {
                viewList.removeAll(sharedElements);
            }
            if (!viewList.isEmpty()) {
                viewList.add(nonExistentView);
                android.support.v4.app.FragmentTransitionCompat21.addTargets(transition, viewList);
            }
        }
        return viewList;
    }

    /* access modifiers changed from: private */
    public static void setViewVisibility(java.util.ArrayList<android.view.View> views, int visibility) {
        if (views != null) {
            for (int i = views.size() - 1; i >= 0; i--) {
                ((android.view.View) views.get(i)).setVisibility(visibility);
            }
        }
    }

    private static java.lang.Object mergeTransitions(java.lang.Object enterTransition, java.lang.Object exitTransition, java.lang.Object sharedElementTransition, android.support.v4.app.Fragment inFragment, boolean isPop) {
        boolean overlap = true;
        if (!(enterTransition == null || exitTransition == null || inFragment == null)) {
            overlap = isPop ? inFragment.getAllowReturnTransitionOverlap() : inFragment.getAllowEnterTransitionOverlap();
        }
        if (overlap) {
            return android.support.v4.app.FragmentTransitionCompat21.mergeTransitionsTogether(exitTransition, enterTransition, sharedElementTransition);
        }
        return android.support.v4.app.FragmentTransitionCompat21.mergeTransitionsInSequence(exitTransition, enterTransition, sharedElementTransition);
    }

    public static void calculateFragments(android.support.v4.app.BackStackRecord transaction, android.util.SparseArray<android.support.v4.app.FragmentTransition.FragmentContainerTransition> transitioningFragments, boolean isOptimized) {
        int numOps = transaction.mOps.size();
        for (int opNum = 0; opNum < numOps; opNum++) {
            addToFirstInLastOut(transaction, (android.support.v4.app.BackStackRecord.Op) transaction.mOps.get(opNum), transitioningFragments, false, isOptimized);
        }
    }

    public static void calculatePopFragments(android.support.v4.app.BackStackRecord transaction, android.util.SparseArray<android.support.v4.app.FragmentTransition.FragmentContainerTransition> transitioningFragments, boolean isOptimized) {
        if (transaction.mManager.mContainer.onHasView()) {
            for (int opNum = transaction.mOps.size() - 1; opNum >= 0; opNum--) {
                addToFirstInLastOut(transaction, (android.support.v4.app.BackStackRecord.Op) transaction.mOps.get(opNum), transitioningFragments, true, isOptimized);
            }
        }
    }

    private static void addToFirstInLastOut(android.support.v4.app.BackStackRecord transaction, android.support.v4.app.BackStackRecord.Op op, android.util.SparseArray<android.support.v4.app.FragmentTransition.FragmentContainerTransition> transitioningFragments, boolean isPop, boolean isOptimizedTransaction) {
        android.support.v4.app.Fragment fragment = op.fragment;
        int containerId = fragment.mContainerId;
        if (containerId != 0) {
            boolean setLastIn = false;
            boolean wasRemoved = false;
            boolean setFirstOut = false;
            boolean wasAdded = false;
            switch (isPop ? INVERSE_OPS[op.cmd] : op.cmd) {
                case 1:
                case 7:
                    boolean setLastIn2 = isOptimizedTransaction ? fragment.mIsNewlyAdded : !fragment.mAdded && !fragment.mHidden;
                    wasAdded = true;
                    break;
                case 3:
                case 6:
                    boolean setFirstOut2 = isOptimizedTransaction ? !fragment.mAdded && fragment.mView != null && fragment.mView.getVisibility() == 0 && fragment.mPostponedAlpha >= 0.0f : fragment.mAdded && !fragment.mHidden;
                    wasRemoved = true;
                    break;
                case 4:
                    setFirstOut = isOptimizedTransaction ? fragment.mHiddenChanged && fragment.mAdded && fragment.mHidden : fragment.mAdded && !fragment.mHidden;
                    wasRemoved = true;
                    break;
                case 5:
                    setLastIn = isOptimizedTransaction ? fragment.mHiddenChanged && !fragment.mHidden && fragment.mAdded : fragment.mHidden;
                    wasAdded = true;
                    break;
            }
            android.support.v4.app.FragmentTransition.FragmentContainerTransition containerTransition = (android.support.v4.app.FragmentTransition.FragmentContainerTransition) transitioningFragments.get(containerId);
            if (setLastIn) {
                containerTransition = ensureContainer(containerTransition, transitioningFragments, containerId);
                containerTransition.lastIn = fragment;
                containerTransition.lastInIsPop = isPop;
                containerTransition.lastInTransaction = transaction;
            }
            if (!isOptimizedTransaction && wasAdded) {
                if (containerTransition != null && containerTransition.firstOut == fragment) {
                    containerTransition.firstOut = null;
                }
                android.support.v4.app.FragmentManagerImpl manager = transaction.mManager;
                if (fragment.mState < 1 && manager.mCurState >= 1 && !transaction.mAllowOptimization) {
                    manager.makeActive(fragment);
                    manager.moveToState(fragment, 1, 0, 0, false);
                }
            }
            if (setFirstOut && (containerTransition == null || containerTransition.firstOut == null)) {
                containerTransition = ensureContainer(containerTransition, transitioningFragments, containerId);
                containerTransition.firstOut = fragment;
                containerTransition.firstOutIsPop = isPop;
                containerTransition.firstOutTransaction = transaction;
            }
            if (!isOptimizedTransaction && wasRemoved && containerTransition != null && containerTransition.lastIn == fragment) {
                containerTransition.lastIn = null;
            }
        }
    }

    private static android.support.v4.app.FragmentTransition.FragmentContainerTransition ensureContainer(android.support.v4.app.FragmentTransition.FragmentContainerTransition containerTransition, android.util.SparseArray<android.support.v4.app.FragmentTransition.FragmentContainerTransition> transitioningFragments, int containerId) {
        if (containerTransition != null) {
            return containerTransition;
        }
        android.support.v4.app.FragmentTransition.FragmentContainerTransition containerTransition2 = new android.support.v4.app.FragmentTransition.FragmentContainerTransition();
        transitioningFragments.put(containerId, containerTransition2);
        return containerTransition2;
    }
}
