package android.support.v4.app;

@android.annotation.TargetApi(21)
@android.support.annotation.RequiresApi(21)
class FragmentTransitionCompat21 {
    FragmentTransitionCompat21() {
    }

    public static java.lang.Object cloneTransition(java.lang.Object transition) {
        if (transition != null) {
            return ((android.transition.Transition) transition).clone();
        }
        return null;
    }

    public static java.lang.Object wrapTransitionInSet(java.lang.Object transition) {
        if (transition == null) {
            return null;
        }
        android.transition.TransitionSet transitionSet = new android.transition.TransitionSet();
        transitionSet.addTransition((android.transition.Transition) transition);
        return transitionSet;
    }

    public static void setSharedElementTargets(java.lang.Object transitionObj, android.view.View nonExistentView, java.util.ArrayList<android.view.View> sharedViews) {
        android.transition.TransitionSet transition = (android.transition.TransitionSet) transitionObj;
        java.util.List<android.view.View> views = transition.getTargets();
        views.clear();
        int count = sharedViews.size();
        for (int i = 0; i < count; i++) {
            bfsAddViewChildren(views, (android.view.View) sharedViews.get(i));
        }
        views.add(nonExistentView);
        sharedViews.add(nonExistentView);
        addTargets(transition, sharedViews);
    }

    private static void bfsAddViewChildren(java.util.List<android.view.View> views, android.view.View startView) {
        int startIndex = views.size();
        if (!containedBeforeIndex(views, startView, startIndex)) {
            views.add(startView);
            for (int index = startIndex; index < views.size(); index++) {
                android.view.View view = (android.view.View) views.get(index);
                if (view instanceof android.view.ViewGroup) {
                    android.view.ViewGroup viewGroup = (android.view.ViewGroup) view;
                    int childCount = viewGroup.getChildCount();
                    for (int childIndex = 0; childIndex < childCount; childIndex++) {
                        android.view.View child = viewGroup.getChildAt(childIndex);
                        if (!containedBeforeIndex(views, child, startIndex)) {
                            views.add(child);
                        }
                    }
                }
            }
        }
    }

    private static boolean containedBeforeIndex(java.util.List<android.view.View> views, android.view.View view, int maxIndex) {
        for (int i = 0; i < maxIndex; i++) {
            if (views.get(i) == view) {
                return true;
            }
        }
        return false;
    }

    public static void setEpicenter(java.lang.Object transitionObj, android.view.View view) {
        if (view != null) {
            android.transition.Transition transition = (android.transition.Transition) transitionObj;
            final android.graphics.Rect epicenter = new android.graphics.Rect();
            getBoundsOnScreen(view, epicenter);
            transition.setEpicenterCallback(new android.transition.Transition.EpicenterCallback() {
                public android.graphics.Rect onGetEpicenter(android.transition.Transition transition) {
                    return epicenter;
                }
            });
        }
    }

    public static void getBoundsOnScreen(android.view.View view, android.graphics.Rect epicenter) {
        int[] loc = new int[2];
        view.getLocationOnScreen(loc);
        epicenter.set(loc[0], loc[1], loc[0] + view.getWidth(), loc[1] + view.getHeight());
    }

    public static void addTargets(java.lang.Object transitionObj, java.util.ArrayList<android.view.View> views) {
        android.transition.Transition transition = (android.transition.Transition) transitionObj;
        if (transition != null) {
            if (transition instanceof android.transition.TransitionSet) {
                android.transition.TransitionSet set = (android.transition.TransitionSet) transition;
                int numTransitions = set.getTransitionCount();
                for (int i = 0; i < numTransitions; i++) {
                    addTargets(set.getTransitionAt(i), views);
                }
            } else if (!hasSimpleTarget(transition) && isNullOrEmpty(transition.getTargets())) {
                int numViews = views.size();
                for (int i2 = 0; i2 < numViews; i2++) {
                    transition.addTarget((android.view.View) views.get(i2));
                }
            }
        }
    }

    private static boolean hasSimpleTarget(android.transition.Transition transition) {
        return !isNullOrEmpty(transition.getTargetIds()) || !isNullOrEmpty(transition.getTargetNames()) || !isNullOrEmpty(transition.getTargetTypes());
    }

    private static boolean isNullOrEmpty(java.util.List list) {
        return list == null || list.isEmpty();
    }

    public static java.lang.Object mergeTransitionsTogether(java.lang.Object transition1, java.lang.Object transition2, java.lang.Object transition3) {
        android.transition.TransitionSet transitionSet = new android.transition.TransitionSet();
        if (transition1 != null) {
            transitionSet.addTransition((android.transition.Transition) transition1);
        }
        if (transition2 != null) {
            transitionSet.addTransition((android.transition.Transition) transition2);
        }
        if (transition3 != null) {
            transitionSet.addTransition((android.transition.Transition) transition3);
        }
        return transitionSet;
    }

    public static void scheduleHideFragmentView(java.lang.Object exitTransitionObj, final android.view.View fragmentView, final java.util.ArrayList<android.view.View> exitingViews) {
        ((android.transition.Transition) exitTransitionObj).addListener(new android.transition.Transition.TransitionListener() {
            public void onTransitionStart(android.transition.Transition transition) {
            }

            public void onTransitionEnd(android.transition.Transition transition) {
                transition.removeListener(this);
                fragmentView.setVisibility(8);
                int numViews = exitingViews.size();
                for (int i = 0; i < numViews; i++) {
                    ((android.view.View) exitingViews.get(i)).setVisibility(0);
                }
            }

            public void onTransitionCancel(android.transition.Transition transition) {
            }

            public void onTransitionPause(android.transition.Transition transition) {
            }

            public void onTransitionResume(android.transition.Transition transition) {
            }
        });
    }

    public static java.lang.Object mergeTransitionsInSequence(java.lang.Object exitTransitionObj, java.lang.Object enterTransitionObj, java.lang.Object sharedElementTransitionObj) {
        android.transition.Transition staggered = null;
        android.transition.Transition exitTransition = (android.transition.Transition) exitTransitionObj;
        android.transition.Transition enterTransition = (android.transition.Transition) enterTransitionObj;
        android.transition.Transition sharedElementTransition = (android.transition.Transition) sharedElementTransitionObj;
        if (exitTransition != null && enterTransition != null) {
            staggered = new android.transition.TransitionSet().addTransition(exitTransition).addTransition(enterTransition).setOrdering(1);
        } else if (exitTransition != null) {
            staggered = exitTransition;
        } else if (enterTransition != null) {
            staggered = enterTransition;
        }
        if (sharedElementTransition == null) {
            return staggered;
        }
        android.transition.TransitionSet together = new android.transition.TransitionSet();
        if (staggered != null) {
            together.addTransition(staggered);
        }
        together.addTransition(sharedElementTransition);
        return together;
    }

    public static void beginDelayedTransition(android.view.ViewGroup sceneRoot, java.lang.Object transition) {
        android.transition.TransitionManager.beginDelayedTransition(sceneRoot, (android.transition.Transition) transition);
    }

    public static java.util.ArrayList<java.lang.String> prepareSetNameOverridesOptimized(java.util.ArrayList<android.view.View> sharedElementsIn) {
        java.util.ArrayList<java.lang.String> names = new java.util.ArrayList<>();
        int numSharedElements = sharedElementsIn.size();
        for (int i = 0; i < numSharedElements; i++) {
            android.view.View view = (android.view.View) sharedElementsIn.get(i);
            names.add(view.getTransitionName());
            view.setTransitionName(null);
        }
        return names;
    }

    public static void setNameOverridesOptimized(android.view.View sceneRoot, java.util.ArrayList<android.view.View> sharedElementsOut, java.util.ArrayList<android.view.View> sharedElementsIn, java.util.ArrayList<java.lang.String> inNames, java.util.Map<java.lang.String, java.lang.String> nameOverrides) {
        final int numSharedElements = sharedElementsIn.size();
        final java.util.ArrayList<java.lang.String> outNames = new java.util.ArrayList<>();
        for (int i = 0; i < numSharedElements; i++) {
            android.view.View view = (android.view.View) sharedElementsOut.get(i);
            java.lang.String name = view.getTransitionName();
            outNames.add(name);
            if (name != null) {
                view.setTransitionName(null);
                java.lang.String inName = (java.lang.String) nameOverrides.get(name);
                int j = 0;
                while (true) {
                    if (j >= numSharedElements) {
                        break;
                    } else if (inName.equals(inNames.get(j))) {
                        ((android.view.View) sharedElementsIn.get(j)).setTransitionName(name);
                        break;
                    } else {
                        j++;
                    }
                }
            }
        }
        final java.util.ArrayList<android.view.View> arrayList = sharedElementsIn;
        final java.util.ArrayList<java.lang.String> arrayList2 = inNames;
        final java.util.ArrayList<android.view.View> arrayList3 = sharedElementsOut;
        android.support.v4.app.OneShotPreDrawListener.add(sceneRoot, new java.lang.Runnable() {
            public void run() {
                for (int i = 0; i < numSharedElements; i++) {
                    ((android.view.View) arrayList.get(i)).setTransitionName((java.lang.String) arrayList2.get(i));
                    ((android.view.View) arrayList3.get(i)).setTransitionName((java.lang.String) outNames.get(i));
                }
            }
        });
    }

    public static void captureTransitioningViews(java.util.ArrayList<android.view.View> transitioningViews, android.view.View view) {
        if (view.getVisibility() != 0) {
            return;
        }
        if (view instanceof android.view.ViewGroup) {
            android.view.ViewGroup viewGroup = (android.view.ViewGroup) view;
            if (viewGroup.isTransitionGroup()) {
                transitioningViews.add(viewGroup);
                return;
            }
            int count = viewGroup.getChildCount();
            for (int i = 0; i < count; i++) {
                captureTransitioningViews(transitioningViews, viewGroup.getChildAt(i));
            }
            return;
        }
        transitioningViews.add(view);
    }

    public static void findNamedViews(java.util.Map<java.lang.String, android.view.View> namedViews, android.view.View view) {
        if (view.getVisibility() == 0) {
            java.lang.String transitionName = view.getTransitionName();
            if (transitionName != null) {
                namedViews.put(transitionName, view);
            }
            if (view instanceof android.view.ViewGroup) {
                android.view.ViewGroup viewGroup = (android.view.ViewGroup) view;
                int count = viewGroup.getChildCount();
                for (int i = 0; i < count; i++) {
                    findNamedViews(namedViews, viewGroup.getChildAt(i));
                }
            }
        }
    }

    public static void setNameOverridesUnoptimized(android.view.View sceneRoot, final java.util.ArrayList<android.view.View> sharedElementsIn, final java.util.Map<java.lang.String, java.lang.String> nameOverrides) {
        android.support.v4.app.OneShotPreDrawListener.add(sceneRoot, new java.lang.Runnable() {
            public void run() {
                int numSharedElements = sharedElementsIn.size();
                for (int i = 0; i < numSharedElements; i++) {
                    android.view.View view = (android.view.View) sharedElementsIn.get(i);
                    java.lang.String name = view.getTransitionName();
                    if (name != null) {
                        view.setTransitionName(android.support.v4.app.FragmentTransitionCompat21.findKeyForValue(nameOverrides, name));
                    }
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public static java.lang.String findKeyForValue(java.util.Map<java.lang.String, java.lang.String> map, java.lang.String value) {
        for (java.util.Map.Entry<java.lang.String, java.lang.String> entry : map.entrySet()) {
            if (value.equals(entry.getValue())) {
                return (java.lang.String) entry.getKey();
            }
        }
        return null;
    }

    public static void scheduleRemoveTargets(java.lang.Object overallTransitionObj, java.lang.Object enterTransition, java.util.ArrayList<android.view.View> enteringViews, java.lang.Object exitTransition, java.util.ArrayList<android.view.View> exitingViews, java.lang.Object sharedElementTransition, java.util.ArrayList<android.view.View> sharedElementsIn) {
        final java.lang.Object obj = enterTransition;
        final java.util.ArrayList<android.view.View> arrayList = enteringViews;
        final java.lang.Object obj2 = exitTransition;
        final java.util.ArrayList<android.view.View> arrayList2 = exitingViews;
        final java.lang.Object obj3 = sharedElementTransition;
        final java.util.ArrayList<android.view.View> arrayList3 = sharedElementsIn;
        ((android.transition.Transition) overallTransitionObj).addListener(new android.transition.Transition.TransitionListener() {
            public void onTransitionStart(android.transition.Transition transition) {
                if (obj != null) {
                    android.support.v4.app.FragmentTransitionCompat21.replaceTargets(obj, arrayList, null);
                }
                if (obj2 != null) {
                    android.support.v4.app.FragmentTransitionCompat21.replaceTargets(obj2, arrayList2, null);
                }
                if (obj3 != null) {
                    android.support.v4.app.FragmentTransitionCompat21.replaceTargets(obj3, arrayList3, null);
                }
            }

            public void onTransitionEnd(android.transition.Transition transition) {
            }

            public void onTransitionCancel(android.transition.Transition transition) {
            }

            public void onTransitionPause(android.transition.Transition transition) {
            }

            public void onTransitionResume(android.transition.Transition transition) {
            }
        });
    }

    public static void swapSharedElementTargets(java.lang.Object sharedElementTransitionObj, java.util.ArrayList<android.view.View> sharedElementsOut, java.util.ArrayList<android.view.View> sharedElementsIn) {
        android.transition.TransitionSet sharedElementTransition = (android.transition.TransitionSet) sharedElementTransitionObj;
        if (sharedElementTransition != null) {
            sharedElementTransition.getTargets().clear();
            sharedElementTransition.getTargets().addAll(sharedElementsIn);
            replaceTargets(sharedElementTransition, sharedElementsOut, sharedElementsIn);
        }
    }

    public static void replaceTargets(java.lang.Object transitionObj, java.util.ArrayList<android.view.View> oldTargets, java.util.ArrayList<android.view.View> newTargets) {
        android.transition.Transition transition = (android.transition.Transition) transitionObj;
        if (transition instanceof android.transition.TransitionSet) {
            android.transition.TransitionSet set = (android.transition.TransitionSet) transition;
            int numTransitions = set.getTransitionCount();
            for (int i = 0; i < numTransitions; i++) {
                replaceTargets(set.getTransitionAt(i), oldTargets, newTargets);
            }
        } else if (!hasSimpleTarget(transition)) {
            java.util.List<android.view.View> targets = transition.getTargets();
            if (targets != null && targets.size() == oldTargets.size() && targets.containsAll(oldTargets)) {
                int targetCount = newTargets == null ? 0 : newTargets.size();
                for (int i2 = 0; i2 < targetCount; i2++) {
                    transition.addTarget((android.view.View) newTargets.get(i2));
                }
                for (int i3 = oldTargets.size() - 1; i3 >= 0; i3--) {
                    transition.removeTarget((android.view.View) oldTargets.get(i3));
                }
            }
        }
    }

    public static void addTarget(java.lang.Object transitionObj, android.view.View view) {
        if (transitionObj != null) {
            ((android.transition.Transition) transitionObj).addTarget(view);
        }
    }

    public static void removeTarget(java.lang.Object transitionObj, android.view.View view) {
        if (transitionObj != null) {
            ((android.transition.Transition) transitionObj).removeTarget(view);
        }
    }

    public static void setEpicenter(java.lang.Object transitionObj, final android.graphics.Rect epicenter) {
        if (transitionObj != null) {
            ((android.transition.Transition) transitionObj).setEpicenterCallback(new android.transition.Transition.EpicenterCallback() {
                public android.graphics.Rect onGetEpicenter(android.transition.Transition transition) {
                    if (epicenter == null || epicenter.isEmpty()) {
                        return null;
                    }
                    return epicenter;
                }
            });
        }
    }

    public static void scheduleNameReset(android.view.ViewGroup sceneRoot, final java.util.ArrayList<android.view.View> sharedElementsIn, final java.util.Map<java.lang.String, java.lang.String> nameOverrides) {
        android.support.v4.app.OneShotPreDrawListener.add(sceneRoot, new java.lang.Runnable() {
            public void run() {
                int numSharedElements = sharedElementsIn.size();
                for (int i = 0; i < numSharedElements; i++) {
                    android.view.View view = (android.view.View) sharedElementsIn.get(i);
                    view.setTransitionName((java.lang.String) nameOverrides.get(view.getTransitionName()));
                }
            }
        });
    }
}
