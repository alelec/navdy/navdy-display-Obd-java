package android.support.v4.app;

public class AppLaunchChecker {
    private static final java.lang.String KEY_STARTED_FROM_LAUNCHER = "startedFromLauncher";
    private static final java.lang.String SHARED_PREFS_NAME = "android.support.AppLaunchChecker";

    public static boolean hasStartedFromLauncher(android.content.Context context) {
        return context.getSharedPreferences(SHARED_PREFS_NAME, 0).getBoolean(KEY_STARTED_FROM_LAUNCHER, false);
    }

    public static void onActivityCreate(android.app.Activity activity) {
        android.content.SharedPreferences sp = activity.getSharedPreferences(SHARED_PREFS_NAME, 0);
        if (!sp.getBoolean(KEY_STARTED_FROM_LAUNCHER, false)) {
            android.content.Intent launchIntent = activity.getIntent();
            if (launchIntent != null && "android.intent.action.MAIN".equals(launchIntent.getAction())) {
                if (launchIntent.hasCategory("android.intent.category.LAUNCHER") || launchIntent.hasCategory(android.support.v4.content.IntentCompat.CATEGORY_LEANBACK_LAUNCHER)) {
                    android.support.v4.content.SharedPreferencesCompat.EditorCompat.getInstance().apply(sp.edit().putBoolean(KEY_STARTED_FROM_LAUNCHER, true));
                }
            }
        }
    }
}
