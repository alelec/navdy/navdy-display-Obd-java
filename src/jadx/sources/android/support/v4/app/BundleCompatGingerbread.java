package android.support.v4.app;

@android.annotation.TargetApi(9)
@android.support.annotation.RequiresApi(9)
class BundleCompatGingerbread {
    private static final java.lang.String TAG = "BundleCompatGingerbread";
    private static java.lang.reflect.Method sGetIBinderMethod;
    private static boolean sGetIBinderMethodFetched;
    private static java.lang.reflect.Method sPutIBinderMethod;
    private static boolean sPutIBinderMethodFetched;

    BundleCompatGingerbread() {
    }

    public static android.os.IBinder getBinder(android.os.Bundle bundle, java.lang.String key) {
        if (!sGetIBinderMethodFetched) {
            try {
                sGetIBinderMethod = android.os.Bundle.class.getMethod("getIBinder", new java.lang.Class[]{java.lang.String.class});
                sGetIBinderMethod.setAccessible(true);
            } catch (java.lang.NoSuchMethodException e) {
                android.util.Log.i(TAG, "Failed to retrieve getIBinder method", e);
            }
            sGetIBinderMethodFetched = true;
        }
        if (sGetIBinderMethod != null) {
            try {
                return (android.os.IBinder) sGetIBinderMethod.invoke(bundle, new java.lang.Object[]{key});
            } catch (java.lang.IllegalAccessException | java.lang.IllegalArgumentException | java.lang.reflect.InvocationTargetException e2) {
                android.util.Log.i(TAG, "Failed to invoke getIBinder via reflection", e2);
                sGetIBinderMethod = null;
            }
        }
        return null;
    }

    public static void putBinder(android.os.Bundle bundle, java.lang.String key, android.os.IBinder binder) {
        if (!sPutIBinderMethodFetched) {
            try {
                sPutIBinderMethod = android.os.Bundle.class.getMethod("putIBinder", new java.lang.Class[]{java.lang.String.class, android.os.IBinder.class});
                sPutIBinderMethod.setAccessible(true);
            } catch (java.lang.NoSuchMethodException e) {
                android.util.Log.i(TAG, "Failed to retrieve putIBinder method", e);
            }
            sPutIBinderMethodFetched = true;
        }
        if (sPutIBinderMethod != null) {
            try {
                sPutIBinderMethod.invoke(bundle, new java.lang.Object[]{key, binder});
            } catch (java.lang.IllegalAccessException | java.lang.IllegalArgumentException | java.lang.reflect.InvocationTargetException e2) {
                android.util.Log.i(TAG, "Failed to invoke putIBinder via reflection", e2);
                sPutIBinderMethod = null;
            }
        }
    }
}
