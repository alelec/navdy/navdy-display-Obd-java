package android.support.v4.app;

public class FragmentManagerNonConfig {
    private final java.util.List<android.support.v4.app.FragmentManagerNonConfig> mChildNonConfigs;
    private final java.util.List<android.support.v4.app.Fragment> mFragments;

    FragmentManagerNonConfig(java.util.List<android.support.v4.app.Fragment> fragments, java.util.List<android.support.v4.app.FragmentManagerNonConfig> childNonConfigs) {
        this.mFragments = fragments;
        this.mChildNonConfigs = childNonConfigs;
    }

    /* access modifiers changed from: 0000 */
    public java.util.List<android.support.v4.app.Fragment> getFragments() {
        return this.mFragments;
    }

    /* access modifiers changed from: 0000 */
    public java.util.List<android.support.v4.app.FragmentManagerNonConfig> getChildNonConfigs() {
        return this.mChildNonConfigs;
    }
}
