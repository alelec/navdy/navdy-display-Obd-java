package android.support.v4.app;

/* compiled from: BackStackRecord */
final class BackStackState implements android.os.Parcelable {
    public static final android.os.Parcelable.Creator<android.support.v4.app.BackStackState> CREATOR = new android.os.Parcelable.Creator<android.support.v4.app.BackStackState>() {
        public android.support.v4.app.BackStackState createFromParcel(android.os.Parcel in) {
            return new android.support.v4.app.BackStackState(in);
        }

        public android.support.v4.app.BackStackState[] newArray(int size) {
            return new android.support.v4.app.BackStackState[size];
        }
    };
    final boolean mAllowOptimization;
    final int mBreadCrumbShortTitleRes;
    final java.lang.CharSequence mBreadCrumbShortTitleText;
    final int mBreadCrumbTitleRes;
    final java.lang.CharSequence mBreadCrumbTitleText;
    final int mIndex;
    final java.lang.String mName;
    final int[] mOps;
    final java.util.ArrayList<java.lang.String> mSharedElementSourceNames;
    final java.util.ArrayList<java.lang.String> mSharedElementTargetNames;
    final int mTransition;
    final int mTransitionStyle;

    public BackStackState(android.support.v4.app.BackStackRecord bse) {
        int numOps = bse.mOps.size();
        this.mOps = new int[(numOps * 6)];
        if (!bse.mAddToBackStack) {
            throw new java.lang.IllegalStateException("Not on back stack");
        }
        int pos = 0;
        for (int opNum = 0; opNum < numOps; opNum++) {
            android.support.v4.app.BackStackRecord.Op op = (android.support.v4.app.BackStackRecord.Op) bse.mOps.get(opNum);
            int pos2 = pos + 1;
            this.mOps[pos] = op.cmd;
            int pos3 = pos2 + 1;
            this.mOps[pos2] = op.fragment != null ? op.fragment.mIndex : -1;
            int pos4 = pos3 + 1;
            this.mOps[pos3] = op.enterAnim;
            int pos5 = pos4 + 1;
            this.mOps[pos4] = op.exitAnim;
            int pos6 = pos5 + 1;
            this.mOps[pos5] = op.popEnterAnim;
            pos = pos6 + 1;
            this.mOps[pos6] = op.popExitAnim;
        }
        this.mTransition = bse.mTransition;
        this.mTransitionStyle = bse.mTransitionStyle;
        this.mName = bse.mName;
        this.mIndex = bse.mIndex;
        this.mBreadCrumbTitleRes = bse.mBreadCrumbTitleRes;
        this.mBreadCrumbTitleText = bse.mBreadCrumbTitleText;
        this.mBreadCrumbShortTitleRes = bse.mBreadCrumbShortTitleRes;
        this.mBreadCrumbShortTitleText = bse.mBreadCrumbShortTitleText;
        this.mSharedElementSourceNames = bse.mSharedElementSourceNames;
        this.mSharedElementTargetNames = bse.mSharedElementTargetNames;
        this.mAllowOptimization = bse.mAllowOptimization;
    }

    public BackStackState(android.os.Parcel in) {
        this.mOps = in.createIntArray();
        this.mTransition = in.readInt();
        this.mTransitionStyle = in.readInt();
        this.mName = in.readString();
        this.mIndex = in.readInt();
        this.mBreadCrumbTitleRes = in.readInt();
        this.mBreadCrumbTitleText = (java.lang.CharSequence) android.text.TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(in);
        this.mBreadCrumbShortTitleRes = in.readInt();
        this.mBreadCrumbShortTitleText = (java.lang.CharSequence) android.text.TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(in);
        this.mSharedElementSourceNames = in.createStringArrayList();
        this.mSharedElementTargetNames = in.createStringArrayList();
        this.mAllowOptimization = in.readInt() != 0;
    }

    public android.support.v4.app.BackStackRecord instantiate(android.support.v4.app.FragmentManagerImpl fm) {
        android.support.v4.app.BackStackRecord bse = new android.support.v4.app.BackStackRecord(fm);
        int pos = 0;
        int num = 0;
        while (pos < this.mOps.length) {
            android.support.v4.app.BackStackRecord.Op op = new android.support.v4.app.BackStackRecord.Op();
            int pos2 = pos + 1;
            op.cmd = this.mOps[pos];
            if (android.support.v4.app.FragmentManagerImpl.DEBUG) {
                android.util.Log.v("FragmentManager", "Instantiate " + bse + " op #" + num + " base fragment #" + this.mOps[pos2]);
            }
            int pos3 = pos2 + 1;
            int findex = this.mOps[pos2];
            if (findex >= 0) {
                op.fragment = (android.support.v4.app.Fragment) fm.mActive.get(findex);
            } else {
                op.fragment = null;
            }
            int pos4 = pos3 + 1;
            op.enterAnim = this.mOps[pos3];
            int pos5 = pos4 + 1;
            op.exitAnim = this.mOps[pos4];
            int pos6 = pos5 + 1;
            op.popEnterAnim = this.mOps[pos5];
            pos = pos6 + 1;
            op.popExitAnim = this.mOps[pos6];
            bse.mEnterAnim = op.enterAnim;
            bse.mExitAnim = op.exitAnim;
            bse.mPopEnterAnim = op.popEnterAnim;
            bse.mPopExitAnim = op.popExitAnim;
            bse.addOp(op);
            num++;
        }
        bse.mTransition = this.mTransition;
        bse.mTransitionStyle = this.mTransitionStyle;
        bse.mName = this.mName;
        bse.mIndex = this.mIndex;
        bse.mAddToBackStack = true;
        bse.mBreadCrumbTitleRes = this.mBreadCrumbTitleRes;
        bse.mBreadCrumbTitleText = this.mBreadCrumbTitleText;
        bse.mBreadCrumbShortTitleRes = this.mBreadCrumbShortTitleRes;
        bse.mBreadCrumbShortTitleText = this.mBreadCrumbShortTitleText;
        bse.mSharedElementSourceNames = this.mSharedElementSourceNames;
        bse.mSharedElementTargetNames = this.mSharedElementTargetNames;
        bse.mAllowOptimization = this.mAllowOptimization;
        bse.bumpBackStackNesting(1);
        return bse;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        int i = 0;
        dest.writeIntArray(this.mOps);
        dest.writeInt(this.mTransition);
        dest.writeInt(this.mTransitionStyle);
        dest.writeString(this.mName);
        dest.writeInt(this.mIndex);
        dest.writeInt(this.mBreadCrumbTitleRes);
        android.text.TextUtils.writeToParcel(this.mBreadCrumbTitleText, dest, 0);
        dest.writeInt(this.mBreadCrumbShortTitleRes);
        android.text.TextUtils.writeToParcel(this.mBreadCrumbShortTitleText, dest, 0);
        dest.writeStringList(this.mSharedElementSourceNames);
        dest.writeStringList(this.mSharedElementTargetNames);
        if (this.mAllowOptimization) {
            i = 1;
        }
        dest.writeInt(i);
    }
}
