package android.support.v4.net;

@android.annotation.TargetApi(16)
@android.support.annotation.RequiresApi(16)
class ConnectivityManagerCompatJellyBean {
    ConnectivityManagerCompatJellyBean() {
    }

    public static boolean isActiveNetworkMetered(android.net.ConnectivityManager cm) {
        return cm.isActiveNetworkMetered();
    }
}
