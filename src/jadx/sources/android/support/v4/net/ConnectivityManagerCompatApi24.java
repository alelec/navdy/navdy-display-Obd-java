package android.support.v4.net;

@android.annotation.TargetApi(24)
@android.support.annotation.RequiresApi(24)
class ConnectivityManagerCompatApi24 {
    ConnectivityManagerCompatApi24() {
    }

    public static int getRestrictBackgroundStatus(android.net.ConnectivityManager cm) {
        return cm.getRestrictBackgroundStatus();
    }
}
