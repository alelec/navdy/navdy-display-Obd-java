package android.support.v4.net;

@android.annotation.TargetApi(14)
@android.support.annotation.RequiresApi(14)
class TrafficStatsCompatIcs {
    TrafficStatsCompatIcs() {
    }

    public static void clearThreadStatsTag() {
        android.net.TrafficStats.clearThreadStatsTag();
    }

    public static int getThreadStatsTag() {
        return android.net.TrafficStats.getThreadStatsTag();
    }

    public static void incrementOperationCount(int operationCount) {
        android.net.TrafficStats.incrementOperationCount(operationCount);
    }

    public static void incrementOperationCount(int tag, int operationCount) {
        android.net.TrafficStats.incrementOperationCount(tag, operationCount);
    }

    public static void setThreadStatsTag(int tag) {
        android.net.TrafficStats.setThreadStatsTag(tag);
    }

    public static void tagSocket(java.net.Socket socket) throws java.net.SocketException {
        android.net.TrafficStats.tagSocket(socket);
    }

    public static void untagSocket(java.net.Socket socket) throws java.net.SocketException {
        android.net.TrafficStats.untagSocket(socket);
    }

    public static void tagDatagramSocket(java.net.DatagramSocket socket) throws java.net.SocketException {
        android.os.ParcelFileDescriptor pfd = android.os.ParcelFileDescriptor.fromDatagramSocket(socket);
        android.net.TrafficStats.tagSocket(new android.support.v4.net.DatagramSocketWrapper(socket, pfd.getFileDescriptor()));
        pfd.detachFd();
    }

    public static void untagDatagramSocket(java.net.DatagramSocket socket) throws java.net.SocketException {
        android.os.ParcelFileDescriptor pfd = android.os.ParcelFileDescriptor.fromDatagramSocket(socket);
        android.net.TrafficStats.untagSocket(new android.support.v4.net.DatagramSocketWrapper(socket, pfd.getFileDescriptor()));
        pfd.detachFd();
    }
}
