package android.support.v4.net;

class DatagramSocketWrapper extends java.net.Socket {

    private static class DatagramSocketImplWrapper extends java.net.SocketImpl {
        public DatagramSocketImplWrapper(java.net.DatagramSocket socket, java.io.FileDescriptor fd) {
            this.localport = socket.getLocalPort();
            this.fd = fd;
        }

        /* access modifiers changed from: protected */
        public void accept(java.net.SocketImpl newSocket) throws java.io.IOException {
            throw new java.lang.UnsupportedOperationException();
        }

        /* access modifiers changed from: protected */
        public int available() throws java.io.IOException {
            throw new java.lang.UnsupportedOperationException();
        }

        /* access modifiers changed from: protected */
        public void bind(java.net.InetAddress address, int port) throws java.io.IOException {
            throw new java.lang.UnsupportedOperationException();
        }

        /* access modifiers changed from: protected */
        public void close() throws java.io.IOException {
            throw new java.lang.UnsupportedOperationException();
        }

        /* access modifiers changed from: protected */
        public void connect(java.lang.String host, int port) throws java.io.IOException {
            throw new java.lang.UnsupportedOperationException();
        }

        /* access modifiers changed from: protected */
        public void connect(java.net.InetAddress address, int port) throws java.io.IOException {
            throw new java.lang.UnsupportedOperationException();
        }

        /* access modifiers changed from: protected */
        public void create(boolean isStreaming) throws java.io.IOException {
            throw new java.lang.UnsupportedOperationException();
        }

        /* access modifiers changed from: protected */
        public java.io.InputStream getInputStream() throws java.io.IOException {
            throw new java.lang.UnsupportedOperationException();
        }

        /* access modifiers changed from: protected */
        public java.io.OutputStream getOutputStream() throws java.io.IOException {
            throw new java.lang.UnsupportedOperationException();
        }

        /* access modifiers changed from: protected */
        public void listen(int backlog) throws java.io.IOException {
            throw new java.lang.UnsupportedOperationException();
        }

        /* access modifiers changed from: protected */
        public void connect(java.net.SocketAddress remoteAddr, int timeout) throws java.io.IOException {
            throw new java.lang.UnsupportedOperationException();
        }

        /* access modifiers changed from: protected */
        public void sendUrgentData(int value) throws java.io.IOException {
            throw new java.lang.UnsupportedOperationException();
        }

        public java.lang.Object getOption(int optID) throws java.net.SocketException {
            throw new java.lang.UnsupportedOperationException();
        }

        public void setOption(int optID, java.lang.Object val) throws java.net.SocketException {
            throw new java.lang.UnsupportedOperationException();
        }
    }

    public DatagramSocketWrapper(java.net.DatagramSocket socket, java.io.FileDescriptor fd) throws java.net.SocketException {
        super(new android.support.v4.net.DatagramSocketWrapper.DatagramSocketImplWrapper(socket, fd));
    }
}
