package android.support.v4.net;

@android.annotation.TargetApi(24)
@android.support.annotation.RequiresApi(24)
@android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
public class TrafficStatsCompatApi24 {
    public static void tagDatagramSocket(java.net.DatagramSocket socket) throws java.net.SocketException {
        android.net.TrafficStats.tagDatagramSocket(socket);
    }

    public static void untagDatagramSocket(java.net.DatagramSocket socket) throws java.net.SocketException {
        android.net.TrafficStats.untagDatagramSocket(socket);
    }
}
