package android.support.v4.net;

public final class ConnectivityManagerCompat {
    private static final android.support.v4.net.ConnectivityManagerCompat.ConnectivityManagerCompatImpl IMPL;
    public static final int RESTRICT_BACKGROUND_STATUS_DISABLED = 1;
    public static final int RESTRICT_BACKGROUND_STATUS_ENABLED = 3;
    public static final int RESTRICT_BACKGROUND_STATUS_WHITELISTED = 2;

    static class Api24ConnectivityManagerCompatImpl extends android.support.v4.net.ConnectivityManagerCompat.JellyBeanConnectivityManagerCompatImpl {
        Api24ConnectivityManagerCompatImpl() {
        }

        public int getRestrictBackgroundStatus(android.net.ConnectivityManager cm) {
            return android.support.v4.net.ConnectivityManagerCompatApi24.getRestrictBackgroundStatus(cm);
        }
    }

    static class BaseConnectivityManagerCompatImpl implements android.support.v4.net.ConnectivityManagerCompat.ConnectivityManagerCompatImpl {
        BaseConnectivityManagerCompatImpl() {
        }

        public boolean isActiveNetworkMetered(android.net.ConnectivityManager cm) {
            android.net.NetworkInfo info = cm.getActiveNetworkInfo();
            if (info == null) {
                return true;
            }
            switch (info.getType()) {
                case 1:
                    return false;
                default:
                    return true;
            }
        }

        public int getRestrictBackgroundStatus(android.net.ConnectivityManager cm) {
            return 3;
        }
    }

    interface ConnectivityManagerCompatImpl {
        int getRestrictBackgroundStatus(android.net.ConnectivityManager connectivityManager);

        boolean isActiveNetworkMetered(android.net.ConnectivityManager connectivityManager);
    }

    static class HoneycombMR2ConnectivityManagerCompatImpl extends android.support.v4.net.ConnectivityManagerCompat.BaseConnectivityManagerCompatImpl {
        HoneycombMR2ConnectivityManagerCompatImpl() {
        }

        public boolean isActiveNetworkMetered(android.net.ConnectivityManager cm) {
            return android.support.v4.net.ConnectivityManagerCompatHoneycombMR2.isActiveNetworkMetered(cm);
        }
    }

    static class JellyBeanConnectivityManagerCompatImpl extends android.support.v4.net.ConnectivityManagerCompat.HoneycombMR2ConnectivityManagerCompatImpl {
        JellyBeanConnectivityManagerCompatImpl() {
        }

        public boolean isActiveNetworkMetered(android.net.ConnectivityManager cm) {
            return android.support.v4.net.ConnectivityManagerCompatJellyBean.isActiveNetworkMetered(cm);
        }
    }

    @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
    @java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.SOURCE)
    public @interface RestrictBackgroundStatus {
    }

    static {
        if (android.os.Build.VERSION.SDK_INT >= 24) {
            IMPL = new android.support.v4.net.ConnectivityManagerCompat.Api24ConnectivityManagerCompatImpl();
        } else if (android.os.Build.VERSION.SDK_INT >= 16) {
            IMPL = new android.support.v4.net.ConnectivityManagerCompat.JellyBeanConnectivityManagerCompatImpl();
        } else if (android.os.Build.VERSION.SDK_INT >= 13) {
            IMPL = new android.support.v4.net.ConnectivityManagerCompat.HoneycombMR2ConnectivityManagerCompatImpl();
        } else {
            IMPL = new android.support.v4.net.ConnectivityManagerCompat.BaseConnectivityManagerCompatImpl();
        }
    }

    public static boolean isActiveNetworkMetered(android.net.ConnectivityManager cm) {
        return IMPL.isActiveNetworkMetered(cm);
    }

    public static android.net.NetworkInfo getNetworkInfoFromBroadcast(android.net.ConnectivityManager cm, android.content.Intent intent) {
        android.net.NetworkInfo info = (android.net.NetworkInfo) intent.getParcelableExtra("networkInfo");
        if (info != null) {
            return cm.getNetworkInfo(info.getType());
        }
        return null;
    }

    public static int getRestrictBackgroundStatus(android.net.ConnectivityManager cm) {
        return IMPL.getRestrictBackgroundStatus(cm);
    }

    private ConnectivityManagerCompat() {
    }
}
