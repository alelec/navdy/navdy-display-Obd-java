package android.support.v4.net;

public final class TrafficStatsCompat {
    private static final android.support.v4.net.TrafficStatsCompat.TrafficStatsCompatImpl IMPL;

    static class Api24TrafficStatsCompatImpl extends android.support.v4.net.TrafficStatsCompat.IcsTrafficStatsCompatImpl {
        Api24TrafficStatsCompatImpl() {
        }

        public void tagDatagramSocket(java.net.DatagramSocket socket) throws java.net.SocketException {
            android.support.v4.net.TrafficStatsCompatApi24.tagDatagramSocket(socket);
        }

        public void untagDatagramSocket(java.net.DatagramSocket socket) throws java.net.SocketException {
            android.support.v4.net.TrafficStatsCompatApi24.untagDatagramSocket(socket);
        }
    }

    static class BaseTrafficStatsCompatImpl implements android.support.v4.net.TrafficStatsCompat.TrafficStatsCompatImpl {
        private java.lang.ThreadLocal<android.support.v4.net.TrafficStatsCompat.BaseTrafficStatsCompatImpl.SocketTags> mThreadSocketTags = new java.lang.ThreadLocal<android.support.v4.net.TrafficStatsCompat.BaseTrafficStatsCompatImpl.SocketTags>() {
            /* access modifiers changed from: protected */
            public android.support.v4.net.TrafficStatsCompat.BaseTrafficStatsCompatImpl.SocketTags initialValue() {
                return new android.support.v4.net.TrafficStatsCompat.BaseTrafficStatsCompatImpl.SocketTags();
            }
        };

        private static class SocketTags {
            public int statsTag = -1;

            SocketTags() {
            }
        }

        BaseTrafficStatsCompatImpl() {
        }

        public void clearThreadStatsTag() {
            ((android.support.v4.net.TrafficStatsCompat.BaseTrafficStatsCompatImpl.SocketTags) this.mThreadSocketTags.get()).statsTag = -1;
        }

        public int getThreadStatsTag() {
            return ((android.support.v4.net.TrafficStatsCompat.BaseTrafficStatsCompatImpl.SocketTags) this.mThreadSocketTags.get()).statsTag;
        }

        public void incrementOperationCount(int operationCount) {
        }

        public void incrementOperationCount(int tag, int operationCount) {
        }

        public void setThreadStatsTag(int tag) {
            ((android.support.v4.net.TrafficStatsCompat.BaseTrafficStatsCompatImpl.SocketTags) this.mThreadSocketTags.get()).statsTag = tag;
        }

        public void tagSocket(java.net.Socket socket) {
        }

        public void untagSocket(java.net.Socket socket) {
        }

        public void tagDatagramSocket(java.net.DatagramSocket socket) {
        }

        public void untagDatagramSocket(java.net.DatagramSocket socket) {
        }
    }

    static class IcsTrafficStatsCompatImpl implements android.support.v4.net.TrafficStatsCompat.TrafficStatsCompatImpl {
        IcsTrafficStatsCompatImpl() {
        }

        public void clearThreadStatsTag() {
            android.support.v4.net.TrafficStatsCompatIcs.clearThreadStatsTag();
        }

        public int getThreadStatsTag() {
            return android.support.v4.net.TrafficStatsCompatIcs.getThreadStatsTag();
        }

        public void incrementOperationCount(int operationCount) {
            android.support.v4.net.TrafficStatsCompatIcs.incrementOperationCount(operationCount);
        }

        public void incrementOperationCount(int tag, int operationCount) {
            android.support.v4.net.TrafficStatsCompatIcs.incrementOperationCount(tag, operationCount);
        }

        public void setThreadStatsTag(int tag) {
            android.support.v4.net.TrafficStatsCompatIcs.setThreadStatsTag(tag);
        }

        public void tagSocket(java.net.Socket socket) throws java.net.SocketException {
            android.support.v4.net.TrafficStatsCompatIcs.tagSocket(socket);
        }

        public void untagSocket(java.net.Socket socket) throws java.net.SocketException {
            android.support.v4.net.TrafficStatsCompatIcs.untagSocket(socket);
        }

        public void tagDatagramSocket(java.net.DatagramSocket socket) throws java.net.SocketException {
            android.support.v4.net.TrafficStatsCompatIcs.tagDatagramSocket(socket);
        }

        public void untagDatagramSocket(java.net.DatagramSocket socket) throws java.net.SocketException {
            android.support.v4.net.TrafficStatsCompatIcs.untagDatagramSocket(socket);
        }
    }

    interface TrafficStatsCompatImpl {
        void clearThreadStatsTag();

        int getThreadStatsTag();

        void incrementOperationCount(int i);

        void incrementOperationCount(int i, int i2);

        void setThreadStatsTag(int i);

        void tagDatagramSocket(java.net.DatagramSocket datagramSocket) throws java.net.SocketException;

        void tagSocket(java.net.Socket socket) throws java.net.SocketException;

        void untagDatagramSocket(java.net.DatagramSocket datagramSocket) throws java.net.SocketException;

        void untagSocket(java.net.Socket socket) throws java.net.SocketException;
    }

    static {
        if ("N".equals(android.os.Build.VERSION.CODENAME)) {
            IMPL = new android.support.v4.net.TrafficStatsCompat.Api24TrafficStatsCompatImpl();
        } else if (android.os.Build.VERSION.SDK_INT >= 14) {
            IMPL = new android.support.v4.net.TrafficStatsCompat.IcsTrafficStatsCompatImpl();
        } else {
            IMPL = new android.support.v4.net.TrafficStatsCompat.BaseTrafficStatsCompatImpl();
        }
    }

    public static void clearThreadStatsTag() {
        IMPL.clearThreadStatsTag();
    }

    public static int getThreadStatsTag() {
        return IMPL.getThreadStatsTag();
    }

    public static void incrementOperationCount(int operationCount) {
        IMPL.incrementOperationCount(operationCount);
    }

    public static void incrementOperationCount(int tag, int operationCount) {
        IMPL.incrementOperationCount(tag, operationCount);
    }

    public static void setThreadStatsTag(int tag) {
        IMPL.setThreadStatsTag(tag);
    }

    public static void tagSocket(java.net.Socket socket) throws java.net.SocketException {
        IMPL.tagSocket(socket);
    }

    public static void untagSocket(java.net.Socket socket) throws java.net.SocketException {
        IMPL.untagSocket(socket);
    }

    public static void tagDatagramSocket(java.net.DatagramSocket socket) throws java.net.SocketException {
        IMPL.tagDatagramSocket(socket);
    }

    public static void untagDatagramSocket(java.net.DatagramSocket socket) throws java.net.SocketException {
        IMPL.untagDatagramSocket(socket);
    }

    private TrafficStatsCompat() {
    }
}
