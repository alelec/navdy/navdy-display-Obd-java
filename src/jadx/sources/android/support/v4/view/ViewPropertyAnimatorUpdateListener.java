package android.support.v4.view;

public interface ViewPropertyAnimatorUpdateListener {
    void onAnimationUpdate(android.view.View view);
}
