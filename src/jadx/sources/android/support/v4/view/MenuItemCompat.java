package android.support.v4.view;

public final class MenuItemCompat {
    static final android.support.v4.view.MenuItemCompat.MenuVersionImpl IMPL;
    public static final int SHOW_AS_ACTION_ALWAYS = 2;
    public static final int SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW = 8;
    public static final int SHOW_AS_ACTION_IF_ROOM = 1;
    public static final int SHOW_AS_ACTION_NEVER = 0;
    public static final int SHOW_AS_ACTION_WITH_TEXT = 4;
    private static final java.lang.String TAG = "MenuItemCompat";

    static class BaseMenuVersionImpl implements android.support.v4.view.MenuItemCompat.MenuVersionImpl {
        BaseMenuVersionImpl() {
        }

        public void setShowAsAction(android.view.MenuItem item, int actionEnum) {
        }

        public android.view.MenuItem setActionView(android.view.MenuItem item, android.view.View view) {
            return item;
        }

        public android.view.MenuItem setActionView(android.view.MenuItem item, int resId) {
            return item;
        }

        public android.view.View getActionView(android.view.MenuItem item) {
            return null;
        }

        public boolean expandActionView(android.view.MenuItem item) {
            return false;
        }

        public boolean collapseActionView(android.view.MenuItem item) {
            return false;
        }

        public boolean isActionViewExpanded(android.view.MenuItem item) {
            return false;
        }

        public android.view.MenuItem setOnActionExpandListener(android.view.MenuItem item, android.support.v4.view.MenuItemCompat.OnActionExpandListener listener) {
            return item;
        }
    }

    static class HoneycombMenuVersionImpl implements android.support.v4.view.MenuItemCompat.MenuVersionImpl {
        HoneycombMenuVersionImpl() {
        }

        public void setShowAsAction(android.view.MenuItem item, int actionEnum) {
            android.support.v4.view.MenuItemCompatHoneycomb.setShowAsAction(item, actionEnum);
        }

        public android.view.MenuItem setActionView(android.view.MenuItem item, android.view.View view) {
            return android.support.v4.view.MenuItemCompatHoneycomb.setActionView(item, view);
        }

        public android.view.MenuItem setActionView(android.view.MenuItem item, int resId) {
            return android.support.v4.view.MenuItemCompatHoneycomb.setActionView(item, resId);
        }

        public android.view.View getActionView(android.view.MenuItem item) {
            return android.support.v4.view.MenuItemCompatHoneycomb.getActionView(item);
        }

        public boolean expandActionView(android.view.MenuItem item) {
            return false;
        }

        public boolean collapseActionView(android.view.MenuItem item) {
            return false;
        }

        public boolean isActionViewExpanded(android.view.MenuItem item) {
            return false;
        }

        public android.view.MenuItem setOnActionExpandListener(android.view.MenuItem item, android.support.v4.view.MenuItemCompat.OnActionExpandListener listener) {
            return item;
        }
    }

    static class IcsMenuVersionImpl extends android.support.v4.view.MenuItemCompat.HoneycombMenuVersionImpl {
        IcsMenuVersionImpl() {
        }

        public boolean expandActionView(android.view.MenuItem item) {
            return android.support.v4.view.MenuItemCompatIcs.expandActionView(item);
        }

        public boolean collapseActionView(android.view.MenuItem item) {
            return android.support.v4.view.MenuItemCompatIcs.collapseActionView(item);
        }

        public boolean isActionViewExpanded(android.view.MenuItem item) {
            return android.support.v4.view.MenuItemCompatIcs.isActionViewExpanded(item);
        }

        public android.view.MenuItem setOnActionExpandListener(android.view.MenuItem item, final android.support.v4.view.MenuItemCompat.OnActionExpandListener listener) {
            if (listener == null) {
                return android.support.v4.view.MenuItemCompatIcs.setOnActionExpandListener(item, null);
            }
            return android.support.v4.view.MenuItemCompatIcs.setOnActionExpandListener(item, new android.support.v4.view.MenuItemCompatIcs.SupportActionExpandProxy() {
                public boolean onMenuItemActionExpand(android.view.MenuItem item) {
                    return listener.onMenuItemActionExpand(item);
                }

                public boolean onMenuItemActionCollapse(android.view.MenuItem item) {
                    return listener.onMenuItemActionCollapse(item);
                }
            });
        }
    }

    interface MenuVersionImpl {
        boolean collapseActionView(android.view.MenuItem menuItem);

        boolean expandActionView(android.view.MenuItem menuItem);

        android.view.View getActionView(android.view.MenuItem menuItem);

        boolean isActionViewExpanded(android.view.MenuItem menuItem);

        android.view.MenuItem setActionView(android.view.MenuItem menuItem, int i);

        android.view.MenuItem setActionView(android.view.MenuItem menuItem, android.view.View view);

        android.view.MenuItem setOnActionExpandListener(android.view.MenuItem menuItem, android.support.v4.view.MenuItemCompat.OnActionExpandListener onActionExpandListener);

        void setShowAsAction(android.view.MenuItem menuItem, int i);
    }

    public interface OnActionExpandListener {
        boolean onMenuItemActionCollapse(android.view.MenuItem menuItem);

        boolean onMenuItemActionExpand(android.view.MenuItem menuItem);
    }

    static {
        if (android.os.Build.VERSION.SDK_INT >= 14) {
            IMPL = new android.support.v4.view.MenuItemCompat.IcsMenuVersionImpl();
        } else if (android.os.Build.VERSION.SDK_INT >= 11) {
            IMPL = new android.support.v4.view.MenuItemCompat.HoneycombMenuVersionImpl();
        } else {
            IMPL = new android.support.v4.view.MenuItemCompat.BaseMenuVersionImpl();
        }
    }

    public static void setShowAsAction(android.view.MenuItem item, int actionEnum) {
        if (item instanceof android.support.v4.internal.view.SupportMenuItem) {
            ((android.support.v4.internal.view.SupportMenuItem) item).setShowAsAction(actionEnum);
        } else {
            IMPL.setShowAsAction(item, actionEnum);
        }
    }

    public static android.view.MenuItem setActionView(android.view.MenuItem item, android.view.View view) {
        if (item instanceof android.support.v4.internal.view.SupportMenuItem) {
            return ((android.support.v4.internal.view.SupportMenuItem) item).setActionView(view);
        }
        return IMPL.setActionView(item, view);
    }

    public static android.view.MenuItem setActionView(android.view.MenuItem item, int resId) {
        if (item instanceof android.support.v4.internal.view.SupportMenuItem) {
            return ((android.support.v4.internal.view.SupportMenuItem) item).setActionView(resId);
        }
        return IMPL.setActionView(item, resId);
    }

    public static android.view.View getActionView(android.view.MenuItem item) {
        if (item instanceof android.support.v4.internal.view.SupportMenuItem) {
            return ((android.support.v4.internal.view.SupportMenuItem) item).getActionView();
        }
        return IMPL.getActionView(item);
    }

    public static android.view.MenuItem setActionProvider(android.view.MenuItem item, android.support.v4.view.ActionProvider provider) {
        if (item instanceof android.support.v4.internal.view.SupportMenuItem) {
            return ((android.support.v4.internal.view.SupportMenuItem) item).setSupportActionProvider(provider);
        }
        android.util.Log.w(TAG, "setActionProvider: item does not implement SupportMenuItem; ignoring");
        return item;
    }

    public static android.support.v4.view.ActionProvider getActionProvider(android.view.MenuItem item) {
        if (item instanceof android.support.v4.internal.view.SupportMenuItem) {
            return ((android.support.v4.internal.view.SupportMenuItem) item).getSupportActionProvider();
        }
        android.util.Log.w(TAG, "getActionProvider: item does not implement SupportMenuItem; returning null");
        return null;
    }

    public static boolean expandActionView(android.view.MenuItem item) {
        if (item instanceof android.support.v4.internal.view.SupportMenuItem) {
            return ((android.support.v4.internal.view.SupportMenuItem) item).expandActionView();
        }
        return IMPL.expandActionView(item);
    }

    public static boolean collapseActionView(android.view.MenuItem item) {
        if (item instanceof android.support.v4.internal.view.SupportMenuItem) {
            return ((android.support.v4.internal.view.SupportMenuItem) item).collapseActionView();
        }
        return IMPL.collapseActionView(item);
    }

    public static boolean isActionViewExpanded(android.view.MenuItem item) {
        if (item instanceof android.support.v4.internal.view.SupportMenuItem) {
            return ((android.support.v4.internal.view.SupportMenuItem) item).isActionViewExpanded();
        }
        return IMPL.isActionViewExpanded(item);
    }

    public static android.view.MenuItem setOnActionExpandListener(android.view.MenuItem item, android.support.v4.view.MenuItemCompat.OnActionExpandListener listener) {
        if (item instanceof android.support.v4.internal.view.SupportMenuItem) {
            return ((android.support.v4.internal.view.SupportMenuItem) item).setSupportOnActionExpandListener(listener);
        }
        return IMPL.setOnActionExpandListener(item, listener);
    }

    private MenuItemCompat() {
    }
}
