package android.support.v4.view;

public final class MotionEventCompat {
    public static final int ACTION_HOVER_ENTER = 9;
    public static final int ACTION_HOVER_EXIT = 10;
    public static final int ACTION_HOVER_MOVE = 7;
    public static final int ACTION_MASK = 255;
    public static final int ACTION_POINTER_DOWN = 5;
    public static final int ACTION_POINTER_INDEX_MASK = 65280;
    public static final int ACTION_POINTER_INDEX_SHIFT = 8;
    public static final int ACTION_POINTER_UP = 6;
    public static final int ACTION_SCROLL = 8;
    public static final int AXIS_BRAKE = 23;
    public static final int AXIS_DISTANCE = 24;
    public static final int AXIS_GAS = 22;
    public static final int AXIS_GENERIC_1 = 32;
    public static final int AXIS_GENERIC_10 = 41;
    public static final int AXIS_GENERIC_11 = 42;
    public static final int AXIS_GENERIC_12 = 43;
    public static final int AXIS_GENERIC_13 = 44;
    public static final int AXIS_GENERIC_14 = 45;
    public static final int AXIS_GENERIC_15 = 46;
    public static final int AXIS_GENERIC_16 = 47;
    public static final int AXIS_GENERIC_2 = 33;
    public static final int AXIS_GENERIC_3 = 34;
    public static final int AXIS_GENERIC_4 = 35;
    public static final int AXIS_GENERIC_5 = 36;
    public static final int AXIS_GENERIC_6 = 37;
    public static final int AXIS_GENERIC_7 = 38;
    public static final int AXIS_GENERIC_8 = 39;
    public static final int AXIS_GENERIC_9 = 40;
    public static final int AXIS_HAT_X = 15;
    public static final int AXIS_HAT_Y = 16;
    public static final int AXIS_HSCROLL = 10;
    public static final int AXIS_LTRIGGER = 17;
    public static final int AXIS_ORIENTATION = 8;
    public static final int AXIS_PRESSURE = 2;
    public static final int AXIS_RELATIVE_X = 27;
    public static final int AXIS_RELATIVE_Y = 28;
    public static final int AXIS_RTRIGGER = 18;
    public static final int AXIS_RUDDER = 20;
    public static final int AXIS_RX = 12;
    public static final int AXIS_RY = 13;
    public static final int AXIS_RZ = 14;
    public static final int AXIS_SIZE = 3;
    public static final int AXIS_THROTTLE = 19;
    public static final int AXIS_TILT = 25;
    public static final int AXIS_TOOL_MAJOR = 6;
    public static final int AXIS_TOOL_MINOR = 7;
    public static final int AXIS_TOUCH_MAJOR = 4;
    public static final int AXIS_TOUCH_MINOR = 5;
    public static final int AXIS_VSCROLL = 9;
    public static final int AXIS_WHEEL = 21;
    public static final int AXIS_X = 0;
    public static final int AXIS_Y = 1;
    public static final int AXIS_Z = 11;
    public static final int BUTTON_PRIMARY = 1;
    static final android.support.v4.view.MotionEventCompat.MotionEventVersionImpl IMPL;

    static class BaseMotionEventVersionImpl implements android.support.v4.view.MotionEventCompat.MotionEventVersionImpl {
        BaseMotionEventVersionImpl() {
        }

        public float getAxisValue(android.view.MotionEvent event, int axis) {
            return 0.0f;
        }

        public float getAxisValue(android.view.MotionEvent event, int axis, int pointerIndex) {
            return 0.0f;
        }

        public int getButtonState(android.view.MotionEvent event) {
            return 0;
        }
    }

    static class HoneycombMr1MotionEventVersionImpl extends android.support.v4.view.MotionEventCompat.BaseMotionEventVersionImpl {
        HoneycombMr1MotionEventVersionImpl() {
        }

        public float getAxisValue(android.view.MotionEvent event, int axis) {
            return android.support.v4.view.MotionEventCompatHoneycombMr1.getAxisValue(event, axis);
        }

        public float getAxisValue(android.view.MotionEvent event, int axis, int pointerIndex) {
            return android.support.v4.view.MotionEventCompatHoneycombMr1.getAxisValue(event, axis, pointerIndex);
        }
    }

    private static class ICSMotionEventVersionImpl extends android.support.v4.view.MotionEventCompat.HoneycombMr1MotionEventVersionImpl {
        ICSMotionEventVersionImpl() {
        }

        public int getButtonState(android.view.MotionEvent event) {
            return android.support.v4.view.MotionEventCompatICS.getButtonState(event);
        }
    }

    interface MotionEventVersionImpl {
        float getAxisValue(android.view.MotionEvent motionEvent, int i);

        float getAxisValue(android.view.MotionEvent motionEvent, int i, int i2);

        int getButtonState(android.view.MotionEvent motionEvent);
    }

    static {
        if (android.os.Build.VERSION.SDK_INT >= 14) {
            IMPL = new android.support.v4.view.MotionEventCompat.ICSMotionEventVersionImpl();
        } else if (android.os.Build.VERSION.SDK_INT >= 12) {
            IMPL = new android.support.v4.view.MotionEventCompat.HoneycombMr1MotionEventVersionImpl();
        } else {
            IMPL = new android.support.v4.view.MotionEventCompat.BaseMotionEventVersionImpl();
        }
    }

    public static int getActionMasked(android.view.MotionEvent event) {
        return event.getAction() & 255;
    }

    public static int getActionIndex(android.view.MotionEvent event) {
        return (event.getAction() & ACTION_POINTER_INDEX_MASK) >> 8;
    }

    @java.lang.Deprecated
    public static int findPointerIndex(android.view.MotionEvent event, int pointerId) {
        return event.findPointerIndex(pointerId);
    }

    @java.lang.Deprecated
    public static int getPointerId(android.view.MotionEvent event, int pointerIndex) {
        return event.getPointerId(pointerIndex);
    }

    @java.lang.Deprecated
    public static float getX(android.view.MotionEvent event, int pointerIndex) {
        return event.getX(pointerIndex);
    }

    @java.lang.Deprecated
    public static float getY(android.view.MotionEvent event, int pointerIndex) {
        return event.getY(pointerIndex);
    }

    @java.lang.Deprecated
    public static int getPointerCount(android.view.MotionEvent event) {
        return event.getPointerCount();
    }

    @java.lang.Deprecated
    public static int getSource(android.view.MotionEvent event) {
        return event.getSource();
    }

    public static boolean isFromSource(android.view.MotionEvent event, int source) {
        return (event.getSource() & source) == source;
    }

    public static float getAxisValue(android.view.MotionEvent event, int axis) {
        return IMPL.getAxisValue(event, axis);
    }

    public static float getAxisValue(android.view.MotionEvent event, int axis, int pointerIndex) {
        return IMPL.getAxisValue(event, axis, pointerIndex);
    }

    public static int getButtonState(android.view.MotionEvent event) {
        return IMPL.getButtonState(event);
    }

    private MotionEventCompat() {
    }
}
