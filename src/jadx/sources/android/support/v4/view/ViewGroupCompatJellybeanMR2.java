package android.support.v4.view;

@android.annotation.TargetApi(18)
@android.support.annotation.RequiresApi(18)
class ViewGroupCompatJellybeanMR2 {
    ViewGroupCompatJellybeanMR2() {
    }

    public static int getLayoutMode(android.view.ViewGroup group) {
        return group.getLayoutMode();
    }

    public static void setLayoutMode(android.view.ViewGroup group, int mode) {
        group.setLayoutMode(mode);
    }
}
