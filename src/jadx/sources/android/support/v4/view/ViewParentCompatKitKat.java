package android.support.v4.view;

@android.annotation.TargetApi(19)
@android.support.annotation.RequiresApi(19)
class ViewParentCompatKitKat {
    ViewParentCompatKitKat() {
    }

    public static void notifySubtreeAccessibilityStateChanged(android.view.ViewParent parent, android.view.View child, android.view.View source, int changeType) {
        parent.notifySubtreeAccessibilityStateChanged(child, source, changeType);
    }
}
