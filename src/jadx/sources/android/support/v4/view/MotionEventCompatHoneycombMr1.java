package android.support.v4.view;

@android.annotation.TargetApi(12)
@android.support.annotation.RequiresApi(12)
class MotionEventCompatHoneycombMr1 {
    MotionEventCompatHoneycombMr1() {
    }

    static float getAxisValue(android.view.MotionEvent event, int axis) {
        return event.getAxisValue(axis);
    }

    static float getAxisValue(android.view.MotionEvent event, int axis, int pointerIndex) {
        return event.getAxisValue(axis, pointerIndex);
    }
}
