package android.support.v4.view;

@android.annotation.TargetApi(11)
@android.support.annotation.RequiresApi(11)
class LayoutInflaterCompatHC {
    private static final java.lang.String TAG = "LayoutInflaterCompatHC";
    private static boolean sCheckedField;
    private static java.lang.reflect.Field sLayoutInflaterFactory2Field;

    static class FactoryWrapperHC extends android.support.v4.view.LayoutInflaterCompatBase.FactoryWrapper implements android.view.LayoutInflater.Factory2 {
        FactoryWrapperHC(android.support.v4.view.LayoutInflaterFactory delegateFactory) {
            super(delegateFactory);
        }

        public android.view.View onCreateView(android.view.View parent, java.lang.String name, android.content.Context context, android.util.AttributeSet attributeSet) {
            return this.mDelegateFactory.onCreateView(parent, name, context, attributeSet);
        }
    }

    LayoutInflaterCompatHC() {
    }

    static void setFactory(android.view.LayoutInflater inflater, android.support.v4.view.LayoutInflaterFactory factory) {
        android.view.LayoutInflater.Factory2 factory2 = factory != null ? new android.support.v4.view.LayoutInflaterCompatHC.FactoryWrapperHC(factory) : null;
        inflater.setFactory2(factory2);
        android.view.LayoutInflater.Factory f = inflater.getFactory();
        if (f instanceof android.view.LayoutInflater.Factory2) {
            forceSetFactory2(inflater, (android.view.LayoutInflater.Factory2) f);
        } else {
            forceSetFactory2(inflater, factory2);
        }
    }

    static void forceSetFactory2(android.view.LayoutInflater inflater, android.view.LayoutInflater.Factory2 factory) {
        if (!sCheckedField) {
            try {
                sLayoutInflaterFactory2Field = android.view.LayoutInflater.class.getDeclaredField("mFactory2");
                sLayoutInflaterFactory2Field.setAccessible(true);
            } catch (java.lang.NoSuchFieldException e) {
                android.util.Log.e(TAG, "forceSetFactory2 Could not find field 'mFactory2' on class " + android.view.LayoutInflater.class.getName() + "; inflation may have unexpected results.", e);
            }
            sCheckedField = true;
        }
        if (sLayoutInflaterFactory2Field != null) {
            try {
                sLayoutInflaterFactory2Field.set(inflater, factory);
            } catch (java.lang.IllegalAccessException e2) {
                android.util.Log.e(TAG, "forceSetFactory2 could not set the Factory2 on LayoutInflater " + inflater + "; inflation may have unexpected results.", e2);
            }
        }
    }
}
