package android.support.v4.view;

@android.annotation.TargetApi(18)
@android.support.annotation.RequiresApi(18)
class ViewCompatJellybeanMr2 {
    ViewCompatJellybeanMr2() {
    }

    public static android.graphics.Rect getClipBounds(android.view.View view) {
        return view.getClipBounds();
    }

    public static void setClipBounds(android.view.View view, android.graphics.Rect clipBounds) {
        view.setClipBounds(clipBounds);
    }

    public static boolean isInLayout(android.view.View view) {
        return view.isInLayout();
    }
}
