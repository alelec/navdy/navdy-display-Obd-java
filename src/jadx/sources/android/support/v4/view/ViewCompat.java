package android.support.v4.view;

public class ViewCompat {
    public static final int ACCESSIBILITY_LIVE_REGION_ASSERTIVE = 2;
    public static final int ACCESSIBILITY_LIVE_REGION_NONE = 0;
    public static final int ACCESSIBILITY_LIVE_REGION_POLITE = 1;
    private static final long FAKE_FRAME_TIME = 10;
    static final android.support.v4.view.ViewCompat.ViewCompatImpl IMPL;
    public static final int IMPORTANT_FOR_ACCESSIBILITY_AUTO = 0;
    public static final int IMPORTANT_FOR_ACCESSIBILITY_NO = 2;
    public static final int IMPORTANT_FOR_ACCESSIBILITY_NO_HIDE_DESCENDANTS = 4;
    public static final int IMPORTANT_FOR_ACCESSIBILITY_YES = 1;
    public static final int LAYER_TYPE_HARDWARE = 2;
    public static final int LAYER_TYPE_NONE = 0;
    public static final int LAYER_TYPE_SOFTWARE = 1;
    public static final int LAYOUT_DIRECTION_INHERIT = 2;
    public static final int LAYOUT_DIRECTION_LOCALE = 3;
    public static final int LAYOUT_DIRECTION_LTR = 0;
    public static final int LAYOUT_DIRECTION_RTL = 1;
    public static final int MEASURED_HEIGHT_STATE_SHIFT = 16;
    public static final int MEASURED_SIZE_MASK = 16777215;
    public static final int MEASURED_STATE_MASK = -16777216;
    public static final int MEASURED_STATE_TOO_SMALL = 16777216;
    @java.lang.Deprecated
    public static final int OVER_SCROLL_ALWAYS = 0;
    @java.lang.Deprecated
    public static final int OVER_SCROLL_IF_CONTENT_SCROLLS = 1;
    @java.lang.Deprecated
    public static final int OVER_SCROLL_NEVER = 2;
    public static final int SCROLL_AXIS_HORIZONTAL = 1;
    public static final int SCROLL_AXIS_NONE = 0;
    public static final int SCROLL_AXIS_VERTICAL = 2;
    public static final int SCROLL_INDICATOR_BOTTOM = 2;
    public static final int SCROLL_INDICATOR_END = 32;
    public static final int SCROLL_INDICATOR_LEFT = 4;
    public static final int SCROLL_INDICATOR_RIGHT = 8;
    public static final int SCROLL_INDICATOR_START = 16;
    public static final int SCROLL_INDICATOR_TOP = 1;
    private static final java.lang.String TAG = "ViewCompat";

    static class Api24ViewCompatImpl extends android.support.v4.view.ViewCompat.MarshmallowViewCompatImpl {
        Api24ViewCompatImpl() {
        }

        public void setPointerIcon(android.view.View view, android.support.v4.view.PointerIconCompat pointerIconCompat) {
            android.support.v4.view.ViewCompatApi24.setPointerIcon(view, pointerIconCompat != null ? pointerIconCompat.getPointerIcon() : null);
        }
    }

    static class BaseViewCompatImpl implements android.support.v4.view.ViewCompat.ViewCompatImpl {
        private static java.lang.reflect.Method sChildrenDrawingOrderMethod;
        private java.lang.reflect.Method mDispatchFinishTemporaryDetach;
        private java.lang.reflect.Method mDispatchStartTemporaryDetach;
        private boolean mTempDetachBound;
        java.util.WeakHashMap<android.view.View, android.support.v4.view.ViewPropertyAnimatorCompat> mViewPropertyAnimatorCompatMap = null;

        BaseViewCompatImpl() {
        }

        public boolean canScrollHorizontally(android.view.View v, int direction) {
            return (v instanceof android.support.v4.view.ScrollingView) && canScrollingViewScrollHorizontally((android.support.v4.view.ScrollingView) v, direction);
        }

        public boolean canScrollVertically(android.view.View v, int direction) {
            return (v instanceof android.support.v4.view.ScrollingView) && canScrollingViewScrollVertically((android.support.v4.view.ScrollingView) v, direction);
        }

        public void setAccessibilityDelegate(android.view.View v, android.support.v4.view.AccessibilityDelegateCompat delegate) {
        }

        public boolean hasAccessibilityDelegate(android.view.View v) {
            return false;
        }

        public void onPopulateAccessibilityEvent(android.view.View v, android.view.accessibility.AccessibilityEvent event) {
        }

        public void onInitializeAccessibilityEvent(android.view.View v, android.view.accessibility.AccessibilityEvent event) {
        }

        public void onInitializeAccessibilityNodeInfo(android.view.View v, android.support.v4.view.accessibility.AccessibilityNodeInfoCompat info) {
        }

        public boolean hasTransientState(android.view.View view) {
            return false;
        }

        public void setHasTransientState(android.view.View view, boolean hasTransientState) {
        }

        public void postInvalidateOnAnimation(android.view.View view) {
            view.invalidate();
        }

        public void postInvalidateOnAnimation(android.view.View view, int left, int top, int right, int bottom) {
            view.invalidate(left, top, right, bottom);
        }

        public void postOnAnimation(android.view.View view, java.lang.Runnable action) {
            view.postDelayed(action, getFrameTime());
        }

        public void postOnAnimationDelayed(android.view.View view, java.lang.Runnable action, long delayMillis) {
            view.postDelayed(action, getFrameTime() + delayMillis);
        }

        /* access modifiers changed from: 0000 */
        public long getFrameTime() {
            return android.support.v4.view.ViewCompat.FAKE_FRAME_TIME;
        }

        public int getImportantForAccessibility(android.view.View view) {
            return 0;
        }

        public void setImportantForAccessibility(android.view.View view, int mode) {
        }

        public boolean isImportantForAccessibility(android.view.View view) {
            return true;
        }

        public boolean performAccessibilityAction(android.view.View view, int action, android.os.Bundle arguments) {
            return false;
        }

        public android.support.v4.view.accessibility.AccessibilityNodeProviderCompat getAccessibilityNodeProvider(android.view.View view) {
            return null;
        }

        public float getAlpha(android.view.View view) {
            return 1.0f;
        }

        public void setLayerType(android.view.View view, int layerType, android.graphics.Paint paint) {
        }

        public int getLayerType(android.view.View view) {
            return 0;
        }

        public int getLabelFor(android.view.View view) {
            return 0;
        }

        public void setLabelFor(android.view.View view, int id) {
        }

        public void setLayerPaint(android.view.View view, android.graphics.Paint p) {
        }

        public int getLayoutDirection(android.view.View view) {
            return 0;
        }

        public void setLayoutDirection(android.view.View view, int layoutDirection) {
        }

        public android.view.ViewParent getParentForAccessibility(android.view.View view) {
            return view.getParent();
        }

        public int resolveSizeAndState(int size, int measureSpec, int childMeasuredState) {
            return android.view.View.resolveSize(size, measureSpec);
        }

        public int getMeasuredWidthAndState(android.view.View view) {
            return view.getMeasuredWidth();
        }

        public int getMeasuredHeightAndState(android.view.View view) {
            return view.getMeasuredHeight();
        }

        public int getMeasuredState(android.view.View view) {
            return 0;
        }

        public int getAccessibilityLiveRegion(android.view.View view) {
            return 0;
        }

        public void setAccessibilityLiveRegion(android.view.View view, int mode) {
        }

        public int getPaddingStart(android.view.View view) {
            return view.getPaddingLeft();
        }

        public int getPaddingEnd(android.view.View view) {
            return view.getPaddingRight();
        }

        public void setPaddingRelative(android.view.View view, int start, int top, int end, int bottom) {
            view.setPadding(start, top, end, bottom);
        }

        public void dispatchStartTemporaryDetach(android.view.View view) {
            if (!this.mTempDetachBound) {
                bindTempDetach();
            }
            if (this.mDispatchStartTemporaryDetach != null) {
                try {
                    this.mDispatchStartTemporaryDetach.invoke(view, new java.lang.Object[0]);
                } catch (java.lang.Exception e) {
                    android.util.Log.d(android.support.v4.view.ViewCompat.TAG, "Error calling dispatchStartTemporaryDetach", e);
                }
            } else {
                view.onStartTemporaryDetach();
            }
        }

        public void dispatchFinishTemporaryDetach(android.view.View view) {
            if (!this.mTempDetachBound) {
                bindTempDetach();
            }
            if (this.mDispatchFinishTemporaryDetach != null) {
                try {
                    this.mDispatchFinishTemporaryDetach.invoke(view, new java.lang.Object[0]);
                } catch (java.lang.Exception e) {
                    android.util.Log.d(android.support.v4.view.ViewCompat.TAG, "Error calling dispatchFinishTemporaryDetach", e);
                }
            } else {
                view.onFinishTemporaryDetach();
            }
        }

        public boolean hasOverlappingRendering(android.view.View view) {
            return true;
        }

        private void bindTempDetach() {
            try {
                this.mDispatchStartTemporaryDetach = android.view.View.class.getDeclaredMethod("dispatchStartTemporaryDetach", new java.lang.Class[0]);
                this.mDispatchFinishTemporaryDetach = android.view.View.class.getDeclaredMethod("dispatchFinishTemporaryDetach", new java.lang.Class[0]);
            } catch (java.lang.NoSuchMethodException e) {
                android.util.Log.e(android.support.v4.view.ViewCompat.TAG, "Couldn't find method", e);
            }
            this.mTempDetachBound = true;
        }

        public float getTranslationX(android.view.View view) {
            return 0.0f;
        }

        public float getTranslationY(android.view.View view) {
            return 0.0f;
        }

        public float getX(android.view.View view) {
            return (float) view.getLeft();
        }

        public float getY(android.view.View view) {
            return (float) view.getTop();
        }

        public float getRotation(android.view.View view) {
            return 0.0f;
        }

        public float getRotationX(android.view.View view) {
            return 0.0f;
        }

        public float getRotationY(android.view.View view) {
            return 0.0f;
        }

        public float getScaleX(android.view.View view) {
            return 0.0f;
        }

        public float getScaleY(android.view.View view) {
            return 0.0f;
        }

        public android.graphics.Matrix getMatrix(android.view.View view) {
            return null;
        }

        public int getMinimumWidth(android.view.View view) {
            return android.support.v4.view.ViewCompatBase.getMinimumWidth(view);
        }

        public int getMinimumHeight(android.view.View view) {
            return android.support.v4.view.ViewCompatBase.getMinimumHeight(view);
        }

        public android.support.v4.view.ViewPropertyAnimatorCompat animate(android.view.View view) {
            return new android.support.v4.view.ViewPropertyAnimatorCompat(view);
        }

        public void setRotation(android.view.View view, float value) {
        }

        public void setTranslationX(android.view.View view, float value) {
        }

        public void setTranslationY(android.view.View view, float value) {
        }

        public void setAlpha(android.view.View view, float value) {
        }

        public void setRotationX(android.view.View view, float value) {
        }

        public void setRotationY(android.view.View view, float value) {
        }

        public void setScaleX(android.view.View view, float value) {
        }

        public void setScaleY(android.view.View view, float value) {
        }

        public void setX(android.view.View view, float value) {
        }

        public void setY(android.view.View view, float value) {
        }

        public void setPivotX(android.view.View view, float value) {
        }

        public void setPivotY(android.view.View view, float value) {
        }

        public float getPivotX(android.view.View view) {
            return 0.0f;
        }

        public float getPivotY(android.view.View view) {
            return 0.0f;
        }

        public void setTransitionName(android.view.View view, java.lang.String transitionName) {
        }

        public java.lang.String getTransitionName(android.view.View view) {
            return null;
        }

        public int getWindowSystemUiVisibility(android.view.View view) {
            return 0;
        }

        public void requestApplyInsets(android.view.View view) {
        }

        public void setElevation(android.view.View view, float elevation) {
        }

        public float getElevation(android.view.View view) {
            return 0.0f;
        }

        public void setTranslationZ(android.view.View view, float translationZ) {
        }

        public float getTranslationZ(android.view.View view) {
            return 0.0f;
        }

        public void setClipBounds(android.view.View view, android.graphics.Rect clipBounds) {
        }

        public android.graphics.Rect getClipBounds(android.view.View view) {
            return null;
        }

        public void setChildrenDrawingOrderEnabled(android.view.ViewGroup viewGroup, boolean enabled) {
            if (sChildrenDrawingOrderMethod == null) {
                try {
                    sChildrenDrawingOrderMethod = android.view.ViewGroup.class.getDeclaredMethod("setChildrenDrawingOrderEnabled", new java.lang.Class[]{java.lang.Boolean.TYPE});
                } catch (java.lang.NoSuchMethodException e) {
                    android.util.Log.e(android.support.v4.view.ViewCompat.TAG, "Unable to find childrenDrawingOrderEnabled", e);
                }
                sChildrenDrawingOrderMethod.setAccessible(true);
            }
            try {
                sChildrenDrawingOrderMethod.invoke(viewGroup, new java.lang.Object[]{java.lang.Boolean.valueOf(enabled)});
            } catch (java.lang.IllegalAccessException e2) {
                android.util.Log.e(android.support.v4.view.ViewCompat.TAG, "Unable to invoke childrenDrawingOrderEnabled", e2);
            } catch (java.lang.IllegalArgumentException e3) {
                android.util.Log.e(android.support.v4.view.ViewCompat.TAG, "Unable to invoke childrenDrawingOrderEnabled", e3);
            } catch (java.lang.reflect.InvocationTargetException e4) {
                android.util.Log.e(android.support.v4.view.ViewCompat.TAG, "Unable to invoke childrenDrawingOrderEnabled", e4);
            }
        }

        public boolean getFitsSystemWindows(android.view.View view) {
            return false;
        }

        public void setFitsSystemWindows(android.view.View view, boolean fitSystemWindows) {
        }

        public void jumpDrawablesToCurrentState(android.view.View view) {
        }

        public void setOnApplyWindowInsetsListener(android.view.View view, android.support.v4.view.OnApplyWindowInsetsListener listener) {
        }

        public android.support.v4.view.WindowInsetsCompat onApplyWindowInsets(android.view.View v, android.support.v4.view.WindowInsetsCompat insets) {
            return insets;
        }

        public android.support.v4.view.WindowInsetsCompat dispatchApplyWindowInsets(android.view.View v, android.support.v4.view.WindowInsetsCompat insets) {
            return insets;
        }

        public void setSaveFromParentEnabled(android.view.View v, boolean enabled) {
        }

        public void setActivated(android.view.View view, boolean activated) {
        }

        public boolean isPaddingRelative(android.view.View view) {
            return false;
        }

        public void setNestedScrollingEnabled(android.view.View view, boolean enabled) {
            if (view instanceof android.support.v4.view.NestedScrollingChild) {
                ((android.support.v4.view.NestedScrollingChild) view).setNestedScrollingEnabled(enabled);
            }
        }

        public boolean isNestedScrollingEnabled(android.view.View view) {
            if (view instanceof android.support.v4.view.NestedScrollingChild) {
                return ((android.support.v4.view.NestedScrollingChild) view).isNestedScrollingEnabled();
            }
            return false;
        }

        public void setBackground(android.view.View view, android.graphics.drawable.Drawable background) {
            view.setBackgroundDrawable(background);
        }

        public android.content.res.ColorStateList getBackgroundTintList(android.view.View view) {
            return android.support.v4.view.ViewCompatBase.getBackgroundTintList(view);
        }

        public void setBackgroundTintList(android.view.View view, android.content.res.ColorStateList tintList) {
            android.support.v4.view.ViewCompatBase.setBackgroundTintList(view, tintList);
        }

        public void setBackgroundTintMode(android.view.View view, android.graphics.PorterDuff.Mode mode) {
            android.support.v4.view.ViewCompatBase.setBackgroundTintMode(view, mode);
        }

        public android.graphics.PorterDuff.Mode getBackgroundTintMode(android.view.View view) {
            return android.support.v4.view.ViewCompatBase.getBackgroundTintMode(view);
        }

        private boolean canScrollingViewScrollHorizontally(android.support.v4.view.ScrollingView view, int direction) {
            int offset = view.computeHorizontalScrollOffset();
            int range = view.computeHorizontalScrollRange() - view.computeHorizontalScrollExtent();
            if (range == 0) {
                return false;
            }
            if (direction < 0) {
                if (offset <= 0) {
                    return false;
                }
                return true;
            } else if (offset >= range - 1) {
                return false;
            } else {
                return true;
            }
        }

        private boolean canScrollingViewScrollVertically(android.support.v4.view.ScrollingView view, int direction) {
            int offset = view.computeVerticalScrollOffset();
            int range = view.computeVerticalScrollRange() - view.computeVerticalScrollExtent();
            if (range == 0) {
                return false;
            }
            if (direction < 0) {
                if (offset <= 0) {
                    return false;
                }
                return true;
            } else if (offset >= range - 1) {
                return false;
            } else {
                return true;
            }
        }

        public boolean startNestedScroll(android.view.View view, int axes) {
            if (view instanceof android.support.v4.view.NestedScrollingChild) {
                return ((android.support.v4.view.NestedScrollingChild) view).startNestedScroll(axes);
            }
            return false;
        }

        public void stopNestedScroll(android.view.View view) {
            if (view instanceof android.support.v4.view.NestedScrollingChild) {
                ((android.support.v4.view.NestedScrollingChild) view).stopNestedScroll();
            }
        }

        public boolean hasNestedScrollingParent(android.view.View view) {
            if (view instanceof android.support.v4.view.NestedScrollingChild) {
                return ((android.support.v4.view.NestedScrollingChild) view).hasNestedScrollingParent();
            }
            return false;
        }

        public boolean dispatchNestedScroll(android.view.View view, int dxConsumed, int dyConsumed, int dxUnconsumed, int dyUnconsumed, int[] offsetInWindow) {
            if (view instanceof android.support.v4.view.NestedScrollingChild) {
                return ((android.support.v4.view.NestedScrollingChild) view).dispatchNestedScroll(dxConsumed, dyConsumed, dxUnconsumed, dyUnconsumed, offsetInWindow);
            }
            return false;
        }

        public boolean dispatchNestedPreScroll(android.view.View view, int dx, int dy, int[] consumed, int[] offsetInWindow) {
            if (view instanceof android.support.v4.view.NestedScrollingChild) {
                return ((android.support.v4.view.NestedScrollingChild) view).dispatchNestedPreScroll(dx, dy, consumed, offsetInWindow);
            }
            return false;
        }

        public boolean dispatchNestedFling(android.view.View view, float velocityX, float velocityY, boolean consumed) {
            if (view instanceof android.support.v4.view.NestedScrollingChild) {
                return ((android.support.v4.view.NestedScrollingChild) view).dispatchNestedFling(velocityX, velocityY, consumed);
            }
            return false;
        }

        public boolean dispatchNestedPreFling(android.view.View view, float velocityX, float velocityY) {
            if (view instanceof android.support.v4.view.NestedScrollingChild) {
                return ((android.support.v4.view.NestedScrollingChild) view).dispatchNestedPreFling(velocityX, velocityY);
            }
            return false;
        }

        public boolean isInLayout(android.view.View view) {
            return false;
        }

        public boolean isLaidOut(android.view.View view) {
            return android.support.v4.view.ViewCompatBase.isLaidOut(view);
        }

        public boolean isLayoutDirectionResolved(android.view.View view) {
            return false;
        }

        public int combineMeasuredStates(int curState, int newState) {
            return curState | newState;
        }

        public float getZ(android.view.View view) {
            return getTranslationZ(view) + getElevation(view);
        }

        public void setZ(android.view.View view, float z) {
        }

        public boolean isAttachedToWindow(android.view.View view) {
            return android.support.v4.view.ViewCompatBase.isAttachedToWindow(view);
        }

        public boolean hasOnClickListeners(android.view.View view) {
            return false;
        }

        public int getScrollIndicators(android.view.View view) {
            return 0;
        }

        public void setScrollIndicators(android.view.View view, int indicators) {
        }

        public void setScrollIndicators(android.view.View view, int indicators, int mask) {
        }

        public void offsetLeftAndRight(android.view.View view, int offset) {
            android.support.v4.view.ViewCompatBase.offsetLeftAndRight(view, offset);
        }

        public void offsetTopAndBottom(android.view.View view, int offset) {
            android.support.v4.view.ViewCompatBase.offsetTopAndBottom(view, offset);
        }

        public void setPointerIcon(android.view.View view, android.support.v4.view.PointerIconCompat pointerIcon) {
        }

        public android.view.Display getDisplay(android.view.View view) {
            return android.support.v4.view.ViewCompatBase.getDisplay(view);
        }
    }

    @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
    @java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.SOURCE)
    public @interface FocusDirection {
    }

    @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
    @java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.SOURCE)
    public @interface FocusRealDirection {
    }

    @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
    @java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.SOURCE)
    public @interface FocusRelativeDirection {
    }

    static class HCViewCompatImpl extends android.support.v4.view.ViewCompat.BaseViewCompatImpl {
        HCViewCompatImpl() {
        }

        /* access modifiers changed from: 0000 */
        public long getFrameTime() {
            return android.support.v4.view.ViewCompatHC.getFrameTime();
        }

        public float getAlpha(android.view.View view) {
            return android.support.v4.view.ViewCompatHC.getAlpha(view);
        }

        public void setLayerType(android.view.View view, int layerType, android.graphics.Paint paint) {
            android.support.v4.view.ViewCompatHC.setLayerType(view, layerType, paint);
        }

        public int getLayerType(android.view.View view) {
            return android.support.v4.view.ViewCompatHC.getLayerType(view);
        }

        public void setLayerPaint(android.view.View view, android.graphics.Paint paint) {
            setLayerType(view, getLayerType(view), paint);
            view.invalidate();
        }

        public int resolveSizeAndState(int size, int measureSpec, int childMeasuredState) {
            return android.support.v4.view.ViewCompatHC.resolveSizeAndState(size, measureSpec, childMeasuredState);
        }

        public int getMeasuredWidthAndState(android.view.View view) {
            return android.support.v4.view.ViewCompatHC.getMeasuredWidthAndState(view);
        }

        public int getMeasuredHeightAndState(android.view.View view) {
            return android.support.v4.view.ViewCompatHC.getMeasuredHeightAndState(view);
        }

        public int getMeasuredState(android.view.View view) {
            return android.support.v4.view.ViewCompatHC.getMeasuredState(view);
        }

        public float getTranslationX(android.view.View view) {
            return android.support.v4.view.ViewCompatHC.getTranslationX(view);
        }

        public float getTranslationY(android.view.View view) {
            return android.support.v4.view.ViewCompatHC.getTranslationY(view);
        }

        public android.graphics.Matrix getMatrix(android.view.View view) {
            return android.support.v4.view.ViewCompatHC.getMatrix(view);
        }

        public void setTranslationX(android.view.View view, float value) {
            android.support.v4.view.ViewCompatHC.setTranslationX(view, value);
        }

        public void setTranslationY(android.view.View view, float value) {
            android.support.v4.view.ViewCompatHC.setTranslationY(view, value);
        }

        public void setAlpha(android.view.View view, float value) {
            android.support.v4.view.ViewCompatHC.setAlpha(view, value);
        }

        public void setX(android.view.View view, float value) {
            android.support.v4.view.ViewCompatHC.setX(view, value);
        }

        public void setY(android.view.View view, float value) {
            android.support.v4.view.ViewCompatHC.setY(view, value);
        }

        public void setRotation(android.view.View view, float value) {
            android.support.v4.view.ViewCompatHC.setRotation(view, value);
        }

        public void setRotationX(android.view.View view, float value) {
            android.support.v4.view.ViewCompatHC.setRotationX(view, value);
        }

        public void setRotationY(android.view.View view, float value) {
            android.support.v4.view.ViewCompatHC.setRotationY(view, value);
        }

        public void setScaleX(android.view.View view, float value) {
            android.support.v4.view.ViewCompatHC.setScaleX(view, value);
        }

        public void setScaleY(android.view.View view, float value) {
            android.support.v4.view.ViewCompatHC.setScaleY(view, value);
        }

        public void setPivotX(android.view.View view, float value) {
            android.support.v4.view.ViewCompatHC.setPivotX(view, value);
        }

        public void setPivotY(android.view.View view, float value) {
            android.support.v4.view.ViewCompatHC.setPivotY(view, value);
        }

        public float getX(android.view.View view) {
            return android.support.v4.view.ViewCompatHC.getX(view);
        }

        public float getY(android.view.View view) {
            return android.support.v4.view.ViewCompatHC.getY(view);
        }

        public float getRotation(android.view.View view) {
            return android.support.v4.view.ViewCompatHC.getRotation(view);
        }

        public float getRotationX(android.view.View view) {
            return android.support.v4.view.ViewCompatHC.getRotationX(view);
        }

        public float getRotationY(android.view.View view) {
            return android.support.v4.view.ViewCompatHC.getRotationY(view);
        }

        public float getScaleX(android.view.View view) {
            return android.support.v4.view.ViewCompatHC.getScaleX(view);
        }

        public float getScaleY(android.view.View view) {
            return android.support.v4.view.ViewCompatHC.getScaleY(view);
        }

        public float getPivotX(android.view.View view) {
            return android.support.v4.view.ViewCompatHC.getPivotX(view);
        }

        public float getPivotY(android.view.View view) {
            return android.support.v4.view.ViewCompatHC.getPivotY(view);
        }

        public void jumpDrawablesToCurrentState(android.view.View view) {
            android.support.v4.view.ViewCompatHC.jumpDrawablesToCurrentState(view);
        }

        public void setSaveFromParentEnabled(android.view.View view, boolean enabled) {
            android.support.v4.view.ViewCompatHC.setSaveFromParentEnabled(view, enabled);
        }

        public void setActivated(android.view.View view, boolean activated) {
            android.support.v4.view.ViewCompatHC.setActivated(view, activated);
        }

        public int combineMeasuredStates(int curState, int newState) {
            return android.support.v4.view.ViewCompatHC.combineMeasuredStates(curState, newState);
        }

        public void offsetLeftAndRight(android.view.View view, int offset) {
            android.support.v4.view.ViewCompatHC.offsetLeftAndRight(view, offset);
        }

        public void offsetTopAndBottom(android.view.View view, int offset) {
            android.support.v4.view.ViewCompatHC.offsetTopAndBottom(view, offset);
        }
    }

    static class ICSMr1ViewCompatImpl extends android.support.v4.view.ViewCompat.ICSViewCompatImpl {
        ICSMr1ViewCompatImpl() {
        }

        public boolean hasOnClickListeners(android.view.View view) {
            return android.support.v4.view.ViewCompatICSMr1.hasOnClickListeners(view);
        }
    }

    static class ICSViewCompatImpl extends android.support.v4.view.ViewCompat.HCViewCompatImpl {
        static boolean accessibilityDelegateCheckFailed = false;
        static java.lang.reflect.Field mAccessibilityDelegateField;

        ICSViewCompatImpl() {
        }

        public boolean canScrollHorizontally(android.view.View v, int direction) {
            return android.support.v4.view.ViewCompatICS.canScrollHorizontally(v, direction);
        }

        public boolean canScrollVertically(android.view.View v, int direction) {
            return android.support.v4.view.ViewCompatICS.canScrollVertically(v, direction);
        }

        public void onPopulateAccessibilityEvent(android.view.View v, android.view.accessibility.AccessibilityEvent event) {
            android.support.v4.view.ViewCompatICS.onPopulateAccessibilityEvent(v, event);
        }

        public void onInitializeAccessibilityEvent(android.view.View v, android.view.accessibility.AccessibilityEvent event) {
            android.support.v4.view.ViewCompatICS.onInitializeAccessibilityEvent(v, event);
        }

        public void onInitializeAccessibilityNodeInfo(android.view.View v, android.support.v4.view.accessibility.AccessibilityNodeInfoCompat info) {
            android.support.v4.view.ViewCompatICS.onInitializeAccessibilityNodeInfo(v, info.getInfo());
        }

        public void setAccessibilityDelegate(android.view.View v, @android.support.annotation.Nullable android.support.v4.view.AccessibilityDelegateCompat delegate) {
            java.lang.Object bridge;
            if (delegate == null) {
                bridge = null;
            } else {
                bridge = delegate.getBridge();
            }
            android.support.v4.view.ViewCompatICS.setAccessibilityDelegate(v, bridge);
        }

        public boolean hasAccessibilityDelegate(android.view.View v) {
            boolean z = true;
            if (accessibilityDelegateCheckFailed) {
                return false;
            }
            if (mAccessibilityDelegateField == null) {
                try {
                    mAccessibilityDelegateField = android.view.View.class.getDeclaredField("mAccessibilityDelegate");
                    mAccessibilityDelegateField.setAccessible(true);
                } catch (Throwable th) {
                    accessibilityDelegateCheckFailed = true;
                    return false;
                }
            }
            try {
                if (mAccessibilityDelegateField.get(v) == null) {
                    z = false;
                }
                return z;
            } catch (Throwable th2) {
                accessibilityDelegateCheckFailed = true;
                return false;
            }
        }

        public android.support.v4.view.ViewPropertyAnimatorCompat animate(android.view.View view) {
            if (this.mViewPropertyAnimatorCompatMap == null) {
                this.mViewPropertyAnimatorCompatMap = new java.util.WeakHashMap();
            }
            android.support.v4.view.ViewPropertyAnimatorCompat vpa = (android.support.v4.view.ViewPropertyAnimatorCompat) this.mViewPropertyAnimatorCompatMap.get(view);
            if (vpa != null) {
                return vpa;
            }
            android.support.v4.view.ViewPropertyAnimatorCompat vpa2 = new android.support.v4.view.ViewPropertyAnimatorCompat(view);
            this.mViewPropertyAnimatorCompatMap.put(view, vpa2);
            return vpa2;
        }

        public void setFitsSystemWindows(android.view.View view, boolean fitSystemWindows) {
            android.support.v4.view.ViewCompatICS.setFitsSystemWindows(view, fitSystemWindows);
        }
    }

    static class JBViewCompatImpl extends android.support.v4.view.ViewCompat.ICSMr1ViewCompatImpl {
        JBViewCompatImpl() {
        }

        public boolean hasTransientState(android.view.View view) {
            return android.support.v4.view.ViewCompatJB.hasTransientState(view);
        }

        public void setHasTransientState(android.view.View view, boolean hasTransientState) {
            android.support.v4.view.ViewCompatJB.setHasTransientState(view, hasTransientState);
        }

        public void postInvalidateOnAnimation(android.view.View view) {
            android.support.v4.view.ViewCompatJB.postInvalidateOnAnimation(view);
        }

        public void postInvalidateOnAnimation(android.view.View view, int left, int top, int right, int bottom) {
            android.support.v4.view.ViewCompatJB.postInvalidateOnAnimation(view, left, top, right, bottom);
        }

        public void postOnAnimation(android.view.View view, java.lang.Runnable action) {
            android.support.v4.view.ViewCompatJB.postOnAnimation(view, action);
        }

        public void postOnAnimationDelayed(android.view.View view, java.lang.Runnable action, long delayMillis) {
            android.support.v4.view.ViewCompatJB.postOnAnimationDelayed(view, action, delayMillis);
        }

        public int getImportantForAccessibility(android.view.View view) {
            return android.support.v4.view.ViewCompatJB.getImportantForAccessibility(view);
        }

        public void setImportantForAccessibility(android.view.View view, int mode) {
            if (mode == 4) {
                mode = 2;
            }
            android.support.v4.view.ViewCompatJB.setImportantForAccessibility(view, mode);
        }

        public boolean performAccessibilityAction(android.view.View view, int action, android.os.Bundle arguments) {
            return android.support.v4.view.ViewCompatJB.performAccessibilityAction(view, action, arguments);
        }

        public android.support.v4.view.accessibility.AccessibilityNodeProviderCompat getAccessibilityNodeProvider(android.view.View view) {
            java.lang.Object compat = android.support.v4.view.ViewCompatJB.getAccessibilityNodeProvider(view);
            if (compat != null) {
                return new android.support.v4.view.accessibility.AccessibilityNodeProviderCompat(compat);
            }
            return null;
        }

        public android.view.ViewParent getParentForAccessibility(android.view.View view) {
            return android.support.v4.view.ViewCompatJB.getParentForAccessibility(view);
        }

        public int getMinimumWidth(android.view.View view) {
            return android.support.v4.view.ViewCompatJB.getMinimumWidth(view);
        }

        public int getMinimumHeight(android.view.View view) {
            return android.support.v4.view.ViewCompatJB.getMinimumHeight(view);
        }

        public void requestApplyInsets(android.view.View view) {
            android.support.v4.view.ViewCompatJB.requestApplyInsets(view);
        }

        public boolean getFitsSystemWindows(android.view.View view) {
            return android.support.v4.view.ViewCompatJB.getFitsSystemWindows(view);
        }

        public boolean hasOverlappingRendering(android.view.View view) {
            return android.support.v4.view.ViewCompatJB.hasOverlappingRendering(view);
        }

        public void setBackground(android.view.View view, android.graphics.drawable.Drawable background) {
            android.support.v4.view.ViewCompatJB.setBackground(view, background);
        }
    }

    static class JbMr1ViewCompatImpl extends android.support.v4.view.ViewCompat.JBViewCompatImpl {
        JbMr1ViewCompatImpl() {
        }

        public int getLabelFor(android.view.View view) {
            return android.support.v4.view.ViewCompatJellybeanMr1.getLabelFor(view);
        }

        public void setLabelFor(android.view.View view, int id) {
            android.support.v4.view.ViewCompatJellybeanMr1.setLabelFor(view, id);
        }

        public void setLayerPaint(android.view.View view, android.graphics.Paint paint) {
            android.support.v4.view.ViewCompatJellybeanMr1.setLayerPaint(view, paint);
        }

        public int getLayoutDirection(android.view.View view) {
            return android.support.v4.view.ViewCompatJellybeanMr1.getLayoutDirection(view);
        }

        public void setLayoutDirection(android.view.View view, int layoutDirection) {
            android.support.v4.view.ViewCompatJellybeanMr1.setLayoutDirection(view, layoutDirection);
        }

        public int getPaddingStart(android.view.View view) {
            return android.support.v4.view.ViewCompatJellybeanMr1.getPaddingStart(view);
        }

        public int getPaddingEnd(android.view.View view) {
            return android.support.v4.view.ViewCompatJellybeanMr1.getPaddingEnd(view);
        }

        public void setPaddingRelative(android.view.View view, int start, int top, int end, int bottom) {
            android.support.v4.view.ViewCompatJellybeanMr1.setPaddingRelative(view, start, top, end, bottom);
        }

        public int getWindowSystemUiVisibility(android.view.View view) {
            return android.support.v4.view.ViewCompatJellybeanMr1.getWindowSystemUiVisibility(view);
        }

        public boolean isPaddingRelative(android.view.View view) {
            return android.support.v4.view.ViewCompatJellybeanMr1.isPaddingRelative(view);
        }

        public android.view.Display getDisplay(android.view.View view) {
            return android.support.v4.view.ViewCompatJellybeanMr1.getDisplay(view);
        }
    }

    static class JbMr2ViewCompatImpl extends android.support.v4.view.ViewCompat.JbMr1ViewCompatImpl {
        JbMr2ViewCompatImpl() {
        }

        public void setClipBounds(android.view.View view, android.graphics.Rect clipBounds) {
            android.support.v4.view.ViewCompatJellybeanMr2.setClipBounds(view, clipBounds);
        }

        public android.graphics.Rect getClipBounds(android.view.View view) {
            return android.support.v4.view.ViewCompatJellybeanMr2.getClipBounds(view);
        }

        public boolean isInLayout(android.view.View view) {
            return android.support.v4.view.ViewCompatJellybeanMr2.isInLayout(view);
        }
    }

    static class KitKatViewCompatImpl extends android.support.v4.view.ViewCompat.JbMr2ViewCompatImpl {
        KitKatViewCompatImpl() {
        }

        public int getAccessibilityLiveRegion(android.view.View view) {
            return android.support.v4.view.ViewCompatKitKat.getAccessibilityLiveRegion(view);
        }

        public void setAccessibilityLiveRegion(android.view.View view, int mode) {
            android.support.v4.view.ViewCompatKitKat.setAccessibilityLiveRegion(view, mode);
        }

        public void setImportantForAccessibility(android.view.View view, int mode) {
            android.support.v4.view.ViewCompatJB.setImportantForAccessibility(view, mode);
        }

        public boolean isLaidOut(android.view.View view) {
            return android.support.v4.view.ViewCompatKitKat.isLaidOut(view);
        }

        public boolean isLayoutDirectionResolved(android.view.View view) {
            return android.support.v4.view.ViewCompatKitKat.isLayoutDirectionResolved(view);
        }

        public boolean isAttachedToWindow(android.view.View view) {
            return android.support.v4.view.ViewCompatKitKat.isAttachedToWindow(view);
        }
    }

    static class LollipopViewCompatImpl extends android.support.v4.view.ViewCompat.KitKatViewCompatImpl {
        LollipopViewCompatImpl() {
        }

        public void setTransitionName(android.view.View view, java.lang.String transitionName) {
            android.support.v4.view.ViewCompatLollipop.setTransitionName(view, transitionName);
        }

        public java.lang.String getTransitionName(android.view.View view) {
            return android.support.v4.view.ViewCompatLollipop.getTransitionName(view);
        }

        public void requestApplyInsets(android.view.View view) {
            android.support.v4.view.ViewCompatLollipop.requestApplyInsets(view);
        }

        public void setElevation(android.view.View view, float elevation) {
            android.support.v4.view.ViewCompatLollipop.setElevation(view, elevation);
        }

        public float getElevation(android.view.View view) {
            return android.support.v4.view.ViewCompatLollipop.getElevation(view);
        }

        public void setTranslationZ(android.view.View view, float translationZ) {
            android.support.v4.view.ViewCompatLollipop.setTranslationZ(view, translationZ);
        }

        public float getTranslationZ(android.view.View view) {
            return android.support.v4.view.ViewCompatLollipop.getTranslationZ(view);
        }

        public void setOnApplyWindowInsetsListener(android.view.View view, final android.support.v4.view.OnApplyWindowInsetsListener listener) {
            if (listener == null) {
                android.support.v4.view.ViewCompatLollipop.setOnApplyWindowInsetsListener(view, null);
            } else {
                android.support.v4.view.ViewCompatLollipop.setOnApplyWindowInsetsListener(view, new android.support.v4.view.ViewCompatLollipop.OnApplyWindowInsetsListenerBridge() {
                    public java.lang.Object onApplyWindowInsets(android.view.View v, java.lang.Object insets) {
                        return android.support.v4.view.WindowInsetsCompat.unwrap(listener.onApplyWindowInsets(v, android.support.v4.view.WindowInsetsCompat.wrap(insets)));
                    }
                });
            }
        }

        public void setNestedScrollingEnabled(android.view.View view, boolean enabled) {
            android.support.v4.view.ViewCompatLollipop.setNestedScrollingEnabled(view, enabled);
        }

        public boolean isNestedScrollingEnabled(android.view.View view) {
            return android.support.v4.view.ViewCompatLollipop.isNestedScrollingEnabled(view);
        }

        public boolean startNestedScroll(android.view.View view, int axes) {
            return android.support.v4.view.ViewCompatLollipop.startNestedScroll(view, axes);
        }

        public void stopNestedScroll(android.view.View view) {
            android.support.v4.view.ViewCompatLollipop.stopNestedScroll(view);
        }

        public boolean hasNestedScrollingParent(android.view.View view) {
            return android.support.v4.view.ViewCompatLollipop.hasNestedScrollingParent(view);
        }

        public boolean dispatchNestedScroll(android.view.View view, int dxConsumed, int dyConsumed, int dxUnconsumed, int dyUnconsumed, int[] offsetInWindow) {
            return android.support.v4.view.ViewCompatLollipop.dispatchNestedScroll(view, dxConsumed, dyConsumed, dxUnconsumed, dyUnconsumed, offsetInWindow);
        }

        public boolean dispatchNestedPreScroll(android.view.View view, int dx, int dy, int[] consumed, int[] offsetInWindow) {
            return android.support.v4.view.ViewCompatLollipop.dispatchNestedPreScroll(view, dx, dy, consumed, offsetInWindow);
        }

        public boolean dispatchNestedFling(android.view.View view, float velocityX, float velocityY, boolean consumed) {
            return android.support.v4.view.ViewCompatLollipop.dispatchNestedFling(view, velocityX, velocityY, consumed);
        }

        public boolean dispatchNestedPreFling(android.view.View view, float velocityX, float velocityY) {
            return android.support.v4.view.ViewCompatLollipop.dispatchNestedPreFling(view, velocityX, velocityY);
        }

        public boolean isImportantForAccessibility(android.view.View view) {
            return android.support.v4.view.ViewCompatLollipop.isImportantForAccessibility(view);
        }

        public android.content.res.ColorStateList getBackgroundTintList(android.view.View view) {
            return android.support.v4.view.ViewCompatLollipop.getBackgroundTintList(view);
        }

        public void setBackgroundTintList(android.view.View view, android.content.res.ColorStateList tintList) {
            android.support.v4.view.ViewCompatLollipop.setBackgroundTintList(view, tintList);
        }

        public void setBackgroundTintMode(android.view.View view, android.graphics.PorterDuff.Mode mode) {
            android.support.v4.view.ViewCompatLollipop.setBackgroundTintMode(view, mode);
        }

        public android.graphics.PorterDuff.Mode getBackgroundTintMode(android.view.View view) {
            return android.support.v4.view.ViewCompatLollipop.getBackgroundTintMode(view);
        }

        public android.support.v4.view.WindowInsetsCompat onApplyWindowInsets(android.view.View v, android.support.v4.view.WindowInsetsCompat insets) {
            return android.support.v4.view.WindowInsetsCompat.wrap(android.support.v4.view.ViewCompatLollipop.onApplyWindowInsets(v, android.support.v4.view.WindowInsetsCompat.unwrap(insets)));
        }

        public android.support.v4.view.WindowInsetsCompat dispatchApplyWindowInsets(android.view.View v, android.support.v4.view.WindowInsetsCompat insets) {
            return android.support.v4.view.WindowInsetsCompat.wrap(android.support.v4.view.ViewCompatLollipop.dispatchApplyWindowInsets(v, android.support.v4.view.WindowInsetsCompat.unwrap(insets)));
        }

        public float getZ(android.view.View view) {
            return android.support.v4.view.ViewCompatLollipop.getZ(view);
        }

        public void setZ(android.view.View view, float z) {
            android.support.v4.view.ViewCompatLollipop.setZ(view, z);
        }

        public void offsetLeftAndRight(android.view.View view, int offset) {
            android.support.v4.view.ViewCompatLollipop.offsetLeftAndRight(view, offset);
        }

        public void offsetTopAndBottom(android.view.View view, int offset) {
            android.support.v4.view.ViewCompatLollipop.offsetTopAndBottom(view, offset);
        }
    }

    static class MarshmallowViewCompatImpl extends android.support.v4.view.ViewCompat.LollipopViewCompatImpl {
        MarshmallowViewCompatImpl() {
        }

        public void setScrollIndicators(android.view.View view, int indicators) {
            android.support.v4.view.ViewCompatMarshmallow.setScrollIndicators(view, indicators);
        }

        public void setScrollIndicators(android.view.View view, int indicators, int mask) {
            android.support.v4.view.ViewCompatMarshmallow.setScrollIndicators(view, indicators, mask);
        }

        public int getScrollIndicators(android.view.View view) {
            return android.support.v4.view.ViewCompatMarshmallow.getScrollIndicators(view);
        }

        public void offsetLeftAndRight(android.view.View view, int offset) {
            android.support.v4.view.ViewCompatMarshmallow.offsetLeftAndRight(view, offset);
        }

        public void offsetTopAndBottom(android.view.View view, int offset) {
            android.support.v4.view.ViewCompatMarshmallow.offsetTopAndBottom(view, offset);
        }
    }

    @java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.SOURCE)
    public @interface ScrollIndicators {
    }

    interface ViewCompatImpl {
        android.support.v4.view.ViewPropertyAnimatorCompat animate(android.view.View view);

        boolean canScrollHorizontally(android.view.View view, int i);

        boolean canScrollVertically(android.view.View view, int i);

        int combineMeasuredStates(int i, int i2);

        android.support.v4.view.WindowInsetsCompat dispatchApplyWindowInsets(android.view.View view, android.support.v4.view.WindowInsetsCompat windowInsetsCompat);

        void dispatchFinishTemporaryDetach(android.view.View view);

        boolean dispatchNestedFling(android.view.View view, float f, float f2, boolean z);

        boolean dispatchNestedPreFling(android.view.View view, float f, float f2);

        boolean dispatchNestedPreScroll(android.view.View view, int i, int i2, int[] iArr, int[] iArr2);

        boolean dispatchNestedScroll(android.view.View view, int i, int i2, int i3, int i4, int[] iArr);

        void dispatchStartTemporaryDetach(android.view.View view);

        int getAccessibilityLiveRegion(android.view.View view);

        android.support.v4.view.accessibility.AccessibilityNodeProviderCompat getAccessibilityNodeProvider(android.view.View view);

        float getAlpha(android.view.View view);

        android.content.res.ColorStateList getBackgroundTintList(android.view.View view);

        android.graphics.PorterDuff.Mode getBackgroundTintMode(android.view.View view);

        android.graphics.Rect getClipBounds(android.view.View view);

        android.view.Display getDisplay(android.view.View view);

        float getElevation(android.view.View view);

        boolean getFitsSystemWindows(android.view.View view);

        int getImportantForAccessibility(android.view.View view);

        int getLabelFor(android.view.View view);

        int getLayerType(android.view.View view);

        int getLayoutDirection(android.view.View view);

        @android.support.annotation.Nullable
        android.graphics.Matrix getMatrix(android.view.View view);

        int getMeasuredHeightAndState(android.view.View view);

        int getMeasuredState(android.view.View view);

        int getMeasuredWidthAndState(android.view.View view);

        int getMinimumHeight(android.view.View view);

        int getMinimumWidth(android.view.View view);

        int getPaddingEnd(android.view.View view);

        int getPaddingStart(android.view.View view);

        android.view.ViewParent getParentForAccessibility(android.view.View view);

        float getPivotX(android.view.View view);

        float getPivotY(android.view.View view);

        float getRotation(android.view.View view);

        float getRotationX(android.view.View view);

        float getRotationY(android.view.View view);

        float getScaleX(android.view.View view);

        float getScaleY(android.view.View view);

        int getScrollIndicators(android.view.View view);

        java.lang.String getTransitionName(android.view.View view);

        float getTranslationX(android.view.View view);

        float getTranslationY(android.view.View view);

        float getTranslationZ(android.view.View view);

        int getWindowSystemUiVisibility(android.view.View view);

        float getX(android.view.View view);

        float getY(android.view.View view);

        float getZ(android.view.View view);

        boolean hasAccessibilityDelegate(android.view.View view);

        boolean hasNestedScrollingParent(android.view.View view);

        boolean hasOnClickListeners(android.view.View view);

        boolean hasOverlappingRendering(android.view.View view);

        boolean hasTransientState(android.view.View view);

        boolean isAttachedToWindow(android.view.View view);

        boolean isImportantForAccessibility(android.view.View view);

        boolean isInLayout(android.view.View view);

        boolean isLaidOut(android.view.View view);

        boolean isLayoutDirectionResolved(android.view.View view);

        boolean isNestedScrollingEnabled(android.view.View view);

        boolean isPaddingRelative(android.view.View view);

        void jumpDrawablesToCurrentState(android.view.View view);

        void offsetLeftAndRight(android.view.View view, int i);

        void offsetTopAndBottom(android.view.View view, int i);

        android.support.v4.view.WindowInsetsCompat onApplyWindowInsets(android.view.View view, android.support.v4.view.WindowInsetsCompat windowInsetsCompat);

        void onInitializeAccessibilityEvent(android.view.View view, android.view.accessibility.AccessibilityEvent accessibilityEvent);

        void onInitializeAccessibilityNodeInfo(android.view.View view, android.support.v4.view.accessibility.AccessibilityNodeInfoCompat accessibilityNodeInfoCompat);

        void onPopulateAccessibilityEvent(android.view.View view, android.view.accessibility.AccessibilityEvent accessibilityEvent);

        boolean performAccessibilityAction(android.view.View view, int i, android.os.Bundle bundle);

        void postInvalidateOnAnimation(android.view.View view);

        void postInvalidateOnAnimation(android.view.View view, int i, int i2, int i3, int i4);

        void postOnAnimation(android.view.View view, java.lang.Runnable runnable);

        void postOnAnimationDelayed(android.view.View view, java.lang.Runnable runnable, long j);

        void requestApplyInsets(android.view.View view);

        int resolveSizeAndState(int i, int i2, int i3);

        void setAccessibilityDelegate(android.view.View view, @android.support.annotation.Nullable android.support.v4.view.AccessibilityDelegateCompat accessibilityDelegateCompat);

        void setAccessibilityLiveRegion(android.view.View view, int i);

        void setActivated(android.view.View view, boolean z);

        void setAlpha(android.view.View view, float f);

        void setBackground(android.view.View view, android.graphics.drawable.Drawable drawable);

        void setBackgroundTintList(android.view.View view, android.content.res.ColorStateList colorStateList);

        void setBackgroundTintMode(android.view.View view, android.graphics.PorterDuff.Mode mode);

        void setChildrenDrawingOrderEnabled(android.view.ViewGroup viewGroup, boolean z);

        void setClipBounds(android.view.View view, android.graphics.Rect rect);

        void setElevation(android.view.View view, float f);

        void setFitsSystemWindows(android.view.View view, boolean z);

        void setHasTransientState(android.view.View view, boolean z);

        void setImportantForAccessibility(android.view.View view, int i);

        void setLabelFor(android.view.View view, int i);

        void setLayerPaint(android.view.View view, android.graphics.Paint paint);

        void setLayerType(android.view.View view, int i, android.graphics.Paint paint);

        void setLayoutDirection(android.view.View view, int i);

        void setNestedScrollingEnabled(android.view.View view, boolean z);

        void setOnApplyWindowInsetsListener(android.view.View view, android.support.v4.view.OnApplyWindowInsetsListener onApplyWindowInsetsListener);

        void setPaddingRelative(android.view.View view, int i, int i2, int i3, int i4);

        void setPivotX(android.view.View view, float f);

        void setPivotY(android.view.View view, float f);

        void setPointerIcon(android.view.View view, android.support.v4.view.PointerIconCompat pointerIconCompat);

        void setRotation(android.view.View view, float f);

        void setRotationX(android.view.View view, float f);

        void setRotationY(android.view.View view, float f);

        void setSaveFromParentEnabled(android.view.View view, boolean z);

        void setScaleX(android.view.View view, float f);

        void setScaleY(android.view.View view, float f);

        void setScrollIndicators(android.view.View view, int i);

        void setScrollIndicators(android.view.View view, int i, int i2);

        void setTransitionName(android.view.View view, java.lang.String str);

        void setTranslationX(android.view.View view, float f);

        void setTranslationY(android.view.View view, float f);

        void setTranslationZ(android.view.View view, float f);

        void setX(android.view.View view, float f);

        void setY(android.view.View view, float f);

        void setZ(android.view.View view, float f);

        boolean startNestedScroll(android.view.View view, int i);

        void stopNestedScroll(android.view.View view);
    }

    static {
        int version = android.os.Build.VERSION.SDK_INT;
        if (android.support.v4.os.BuildCompat.isAtLeastN()) {
            IMPL = new android.support.v4.view.ViewCompat.Api24ViewCompatImpl();
        } else if (version >= 23) {
            IMPL = new android.support.v4.view.ViewCompat.MarshmallowViewCompatImpl();
        } else if (version >= 21) {
            IMPL = new android.support.v4.view.ViewCompat.LollipopViewCompatImpl();
        } else if (version >= 19) {
            IMPL = new android.support.v4.view.ViewCompat.KitKatViewCompatImpl();
        } else if (version >= 18) {
            IMPL = new android.support.v4.view.ViewCompat.JbMr2ViewCompatImpl();
        } else if (version >= 17) {
            IMPL = new android.support.v4.view.ViewCompat.JbMr1ViewCompatImpl();
        } else if (version >= 16) {
            IMPL = new android.support.v4.view.ViewCompat.JBViewCompatImpl();
        } else if (version >= 15) {
            IMPL = new android.support.v4.view.ViewCompat.ICSMr1ViewCompatImpl();
        } else if (version >= 14) {
            IMPL = new android.support.v4.view.ViewCompat.ICSViewCompatImpl();
        } else if (version >= 11) {
            IMPL = new android.support.v4.view.ViewCompat.HCViewCompatImpl();
        } else {
            IMPL = new android.support.v4.view.ViewCompat.BaseViewCompatImpl();
        }
    }

    public static boolean canScrollHorizontally(android.view.View v, int direction) {
        return IMPL.canScrollHorizontally(v, direction);
    }

    public static boolean canScrollVertically(android.view.View v, int direction) {
        return IMPL.canScrollVertically(v, direction);
    }

    @java.lang.Deprecated
    public static int getOverScrollMode(android.view.View v) {
        return v.getOverScrollMode();
    }

    @java.lang.Deprecated
    public static void setOverScrollMode(android.view.View v, int overScrollMode) {
        v.setOverScrollMode(overScrollMode);
    }

    public static void onPopulateAccessibilityEvent(android.view.View v, android.view.accessibility.AccessibilityEvent event) {
        IMPL.onPopulateAccessibilityEvent(v, event);
    }

    public static void onInitializeAccessibilityEvent(android.view.View v, android.view.accessibility.AccessibilityEvent event) {
        IMPL.onInitializeAccessibilityEvent(v, event);
    }

    public static void onInitializeAccessibilityNodeInfo(android.view.View v, android.support.v4.view.accessibility.AccessibilityNodeInfoCompat info) {
        IMPL.onInitializeAccessibilityNodeInfo(v, info);
    }

    public static void setAccessibilityDelegate(android.view.View v, android.support.v4.view.AccessibilityDelegateCompat delegate) {
        IMPL.setAccessibilityDelegate(v, delegate);
    }

    public static boolean hasAccessibilityDelegate(android.view.View v) {
        return IMPL.hasAccessibilityDelegate(v);
    }

    public static boolean hasTransientState(android.view.View view) {
        return IMPL.hasTransientState(view);
    }

    public static void setHasTransientState(android.view.View view, boolean hasTransientState) {
        IMPL.setHasTransientState(view, hasTransientState);
    }

    public static void postInvalidateOnAnimation(android.view.View view) {
        IMPL.postInvalidateOnAnimation(view);
    }

    public static void postInvalidateOnAnimation(android.view.View view, int left, int top, int right, int bottom) {
        IMPL.postInvalidateOnAnimation(view, left, top, right, bottom);
    }

    public static void postOnAnimation(android.view.View view, java.lang.Runnable action) {
        IMPL.postOnAnimation(view, action);
    }

    public static void postOnAnimationDelayed(android.view.View view, java.lang.Runnable action, long delayMillis) {
        IMPL.postOnAnimationDelayed(view, action, delayMillis);
    }

    public static int getImportantForAccessibility(android.view.View view) {
        return IMPL.getImportantForAccessibility(view);
    }

    public static void setImportantForAccessibility(android.view.View view, int mode) {
        IMPL.setImportantForAccessibility(view, mode);
    }

    public static boolean isImportantForAccessibility(android.view.View view) {
        return IMPL.isImportantForAccessibility(view);
    }

    public static boolean performAccessibilityAction(android.view.View view, int action, android.os.Bundle arguments) {
        return IMPL.performAccessibilityAction(view, action, arguments);
    }

    public static android.support.v4.view.accessibility.AccessibilityNodeProviderCompat getAccessibilityNodeProvider(android.view.View view) {
        return IMPL.getAccessibilityNodeProvider(view);
    }

    public static float getAlpha(android.view.View view) {
        return IMPL.getAlpha(view);
    }

    public static void setLayerType(android.view.View view, int layerType, android.graphics.Paint paint) {
        IMPL.setLayerType(view, layerType, paint);
    }

    public static int getLayerType(android.view.View view) {
        return IMPL.getLayerType(view);
    }

    public static int getLabelFor(android.view.View view) {
        return IMPL.getLabelFor(view);
    }

    public static void setLabelFor(android.view.View view, @android.support.annotation.IdRes int labeledId) {
        IMPL.setLabelFor(view, labeledId);
    }

    public static void setLayerPaint(android.view.View view, android.graphics.Paint paint) {
        IMPL.setLayerPaint(view, paint);
    }

    public static int getLayoutDirection(android.view.View view) {
        return IMPL.getLayoutDirection(view);
    }

    public static void setLayoutDirection(android.view.View view, int layoutDirection) {
        IMPL.setLayoutDirection(view, layoutDirection);
    }

    public static android.view.ViewParent getParentForAccessibility(android.view.View view) {
        return IMPL.getParentForAccessibility(view);
    }

    @java.lang.Deprecated
    public static boolean isOpaque(android.view.View view) {
        return view.isOpaque();
    }

    public static int resolveSizeAndState(int size, int measureSpec, int childMeasuredState) {
        return IMPL.resolveSizeAndState(size, measureSpec, childMeasuredState);
    }

    public static int getMeasuredWidthAndState(android.view.View view) {
        return IMPL.getMeasuredWidthAndState(view);
    }

    public static int getMeasuredHeightAndState(android.view.View view) {
        return IMPL.getMeasuredHeightAndState(view);
    }

    public static int getMeasuredState(android.view.View view) {
        return IMPL.getMeasuredState(view);
    }

    public static int combineMeasuredStates(int curState, int newState) {
        return IMPL.combineMeasuredStates(curState, newState);
    }

    public static int getAccessibilityLiveRegion(android.view.View view) {
        return IMPL.getAccessibilityLiveRegion(view);
    }

    public static void setAccessibilityLiveRegion(android.view.View view, int mode) {
        IMPL.setAccessibilityLiveRegion(view, mode);
    }

    public static int getPaddingStart(android.view.View view) {
        return IMPL.getPaddingStart(view);
    }

    public static int getPaddingEnd(android.view.View view) {
        return IMPL.getPaddingEnd(view);
    }

    public static void setPaddingRelative(android.view.View view, int start, int top, int end, int bottom) {
        IMPL.setPaddingRelative(view, start, top, end, bottom);
    }

    public static void dispatchStartTemporaryDetach(android.view.View view) {
        IMPL.dispatchStartTemporaryDetach(view);
    }

    public static void dispatchFinishTemporaryDetach(android.view.View view) {
        IMPL.dispatchFinishTemporaryDetach(view);
    }

    public static float getTranslationX(android.view.View view) {
        return IMPL.getTranslationX(view);
    }

    public static float getTranslationY(android.view.View view) {
        return IMPL.getTranslationY(view);
    }

    @android.support.annotation.Nullable
    public static android.graphics.Matrix getMatrix(android.view.View view) {
        return IMPL.getMatrix(view);
    }

    public static int getMinimumWidth(android.view.View view) {
        return IMPL.getMinimumWidth(view);
    }

    public static int getMinimumHeight(android.view.View view) {
        return IMPL.getMinimumHeight(view);
    }

    public static android.support.v4.view.ViewPropertyAnimatorCompat animate(android.view.View view) {
        return IMPL.animate(view);
    }

    public static void setTranslationX(android.view.View view, float value) {
        IMPL.setTranslationX(view, value);
    }

    public static void setTranslationY(android.view.View view, float value) {
        IMPL.setTranslationY(view, value);
    }

    public static void setAlpha(android.view.View view, @android.support.annotation.FloatRange(from = 0.0d, to = 1.0d) float value) {
        IMPL.setAlpha(view, value);
    }

    public static void setX(android.view.View view, float value) {
        IMPL.setX(view, value);
    }

    public static void setY(android.view.View view, float value) {
        IMPL.setY(view, value);
    }

    public static void setRotation(android.view.View view, float value) {
        IMPL.setRotation(view, value);
    }

    public static void setRotationX(android.view.View view, float value) {
        IMPL.setRotationX(view, value);
    }

    public static void setRotationY(android.view.View view, float value) {
        IMPL.setRotationY(view, value);
    }

    public static void setScaleX(android.view.View view, float value) {
        IMPL.setScaleX(view, value);
    }

    public static void setScaleY(android.view.View view, float value) {
        IMPL.setScaleY(view, value);
    }

    public static float getPivotX(android.view.View view) {
        return IMPL.getPivotX(view);
    }

    public static void setPivotX(android.view.View view, float value) {
        IMPL.setPivotX(view, value);
    }

    public static float getPivotY(android.view.View view) {
        return IMPL.getPivotY(view);
    }

    public static void setPivotY(android.view.View view, float value) {
        IMPL.setPivotY(view, value);
    }

    public static float getRotation(android.view.View view) {
        return IMPL.getRotation(view);
    }

    public static float getRotationX(android.view.View view) {
        return IMPL.getRotationX(view);
    }

    public static float getRotationY(android.view.View view) {
        return IMPL.getRotationY(view);
    }

    public static float getScaleX(android.view.View view) {
        return IMPL.getScaleX(view);
    }

    public static float getScaleY(android.view.View view) {
        return IMPL.getScaleY(view);
    }

    public static float getX(android.view.View view) {
        return IMPL.getX(view);
    }

    public static float getY(android.view.View view) {
        return IMPL.getY(view);
    }

    public static void setElevation(android.view.View view, float elevation) {
        IMPL.setElevation(view, elevation);
    }

    public static float getElevation(android.view.View view) {
        return IMPL.getElevation(view);
    }

    public static void setTranslationZ(android.view.View view, float translationZ) {
        IMPL.setTranslationZ(view, translationZ);
    }

    public static float getTranslationZ(android.view.View view) {
        return IMPL.getTranslationZ(view);
    }

    public static void setTransitionName(android.view.View view, java.lang.String transitionName) {
        IMPL.setTransitionName(view, transitionName);
    }

    public static java.lang.String getTransitionName(android.view.View view) {
        return IMPL.getTransitionName(view);
    }

    public static int getWindowSystemUiVisibility(android.view.View view) {
        return IMPL.getWindowSystemUiVisibility(view);
    }

    public static void requestApplyInsets(android.view.View view) {
        IMPL.requestApplyInsets(view);
    }

    public static void setChildrenDrawingOrderEnabled(android.view.ViewGroup viewGroup, boolean enabled) {
        IMPL.setChildrenDrawingOrderEnabled(viewGroup, enabled);
    }

    public static boolean getFitsSystemWindows(android.view.View v) {
        return IMPL.getFitsSystemWindows(v);
    }

    public static void setFitsSystemWindows(android.view.View view, boolean fitSystemWindows) {
        IMPL.setFitsSystemWindows(view, fitSystemWindows);
    }

    public static void jumpDrawablesToCurrentState(android.view.View v) {
        IMPL.jumpDrawablesToCurrentState(v);
    }

    public static void setOnApplyWindowInsetsListener(android.view.View v, android.support.v4.view.OnApplyWindowInsetsListener listener) {
        IMPL.setOnApplyWindowInsetsListener(v, listener);
    }

    public static android.support.v4.view.WindowInsetsCompat onApplyWindowInsets(android.view.View view, android.support.v4.view.WindowInsetsCompat insets) {
        return IMPL.onApplyWindowInsets(view, insets);
    }

    public static android.support.v4.view.WindowInsetsCompat dispatchApplyWindowInsets(android.view.View view, android.support.v4.view.WindowInsetsCompat insets) {
        return IMPL.dispatchApplyWindowInsets(view, insets);
    }

    public static void setSaveFromParentEnabled(android.view.View v, boolean enabled) {
        IMPL.setSaveFromParentEnabled(v, enabled);
    }

    public static void setActivated(android.view.View view, boolean activated) {
        IMPL.setActivated(view, activated);
    }

    public static boolean hasOverlappingRendering(android.view.View view) {
        return IMPL.hasOverlappingRendering(view);
    }

    public static boolean isPaddingRelative(android.view.View view) {
        return IMPL.isPaddingRelative(view);
    }

    public static void setBackground(android.view.View view, android.graphics.drawable.Drawable background) {
        IMPL.setBackground(view, background);
    }

    public static android.content.res.ColorStateList getBackgroundTintList(android.view.View view) {
        return IMPL.getBackgroundTintList(view);
    }

    public static void setBackgroundTintList(android.view.View view, android.content.res.ColorStateList tintList) {
        IMPL.setBackgroundTintList(view, tintList);
    }

    public static android.graphics.PorterDuff.Mode getBackgroundTintMode(android.view.View view) {
        return IMPL.getBackgroundTintMode(view);
    }

    public static void setBackgroundTintMode(android.view.View view, android.graphics.PorterDuff.Mode mode) {
        IMPL.setBackgroundTintMode(view, mode);
    }

    public static void setNestedScrollingEnabled(android.view.View view, boolean enabled) {
        IMPL.setNestedScrollingEnabled(view, enabled);
    }

    public static boolean isNestedScrollingEnabled(android.view.View view) {
        return IMPL.isNestedScrollingEnabled(view);
    }

    public static boolean startNestedScroll(android.view.View view, int axes) {
        return IMPL.startNestedScroll(view, axes);
    }

    public static void stopNestedScroll(android.view.View view) {
        IMPL.stopNestedScroll(view);
    }

    public static boolean hasNestedScrollingParent(android.view.View view) {
        return IMPL.hasNestedScrollingParent(view);
    }

    public static boolean dispatchNestedScroll(android.view.View view, int dxConsumed, int dyConsumed, int dxUnconsumed, int dyUnconsumed, int[] offsetInWindow) {
        return IMPL.dispatchNestedScroll(view, dxConsumed, dyConsumed, dxUnconsumed, dyUnconsumed, offsetInWindow);
    }

    public static boolean dispatchNestedPreScroll(android.view.View view, int dx, int dy, int[] consumed, int[] offsetInWindow) {
        return IMPL.dispatchNestedPreScroll(view, dx, dy, consumed, offsetInWindow);
    }

    public static boolean dispatchNestedFling(android.view.View view, float velocityX, float velocityY, boolean consumed) {
        return IMPL.dispatchNestedFling(view, velocityX, velocityY, consumed);
    }

    public static boolean dispatchNestedPreFling(android.view.View view, float velocityX, float velocityY) {
        return IMPL.dispatchNestedPreFling(view, velocityX, velocityY);
    }

    public static boolean isInLayout(android.view.View view) {
        return IMPL.isInLayout(view);
    }

    public static boolean isLaidOut(android.view.View view) {
        return IMPL.isLaidOut(view);
    }

    public static boolean isLayoutDirectionResolved(android.view.View view) {
        return IMPL.isLayoutDirectionResolved(view);
    }

    public static float getZ(android.view.View view) {
        return IMPL.getZ(view);
    }

    public static void setZ(android.view.View view, float z) {
        IMPL.setZ(view, z);
    }

    public static void offsetTopAndBottom(android.view.View view, int offset) {
        IMPL.offsetTopAndBottom(view, offset);
    }

    public static void offsetLeftAndRight(android.view.View view, int offset) {
        IMPL.offsetLeftAndRight(view, offset);
    }

    public static void setClipBounds(android.view.View view, android.graphics.Rect clipBounds) {
        IMPL.setClipBounds(view, clipBounds);
    }

    public static android.graphics.Rect getClipBounds(android.view.View view) {
        return IMPL.getClipBounds(view);
    }

    public static boolean isAttachedToWindow(android.view.View view) {
        return IMPL.isAttachedToWindow(view);
    }

    public static boolean hasOnClickListeners(android.view.View view) {
        return IMPL.hasOnClickListeners(view);
    }

    public static void setScrollIndicators(@android.support.annotation.NonNull android.view.View view, int indicators) {
        IMPL.setScrollIndicators(view, indicators);
    }

    public static void setScrollIndicators(@android.support.annotation.NonNull android.view.View view, int indicators, int mask) {
        IMPL.setScrollIndicators(view, indicators, mask);
    }

    public static int getScrollIndicators(@android.support.annotation.NonNull android.view.View view) {
        return IMPL.getScrollIndicators(view);
    }

    public static void setPointerIcon(@android.support.annotation.NonNull android.view.View view, android.support.v4.view.PointerIconCompat pointerIcon) {
        IMPL.setPointerIcon(view, pointerIcon);
    }

    public static android.view.Display getDisplay(@android.support.annotation.NonNull android.view.View view) {
        return IMPL.getDisplay(view);
    }

    protected ViewCompat() {
    }
}
