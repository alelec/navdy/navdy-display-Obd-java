package android.support.v4.view;

public class NestedScrollingParentHelper {
    private int mNestedScrollAxes;
    private final android.view.ViewGroup mViewGroup;

    public NestedScrollingParentHelper(android.view.ViewGroup viewGroup) {
        this.mViewGroup = viewGroup;
    }

    public void onNestedScrollAccepted(android.view.View child, android.view.View target, int axes) {
        this.mNestedScrollAxes = axes;
    }

    public int getNestedScrollAxes() {
        return this.mNestedScrollAxes;
    }

    public void onStopNestedScroll(android.view.View target) {
        this.mNestedScrollAxes = 0;
    }
}
