package android.support.v4.view;

@android.annotation.TargetApi(17)
@android.support.annotation.RequiresApi(17)
class ViewCompatJellybeanMr1 {
    ViewCompatJellybeanMr1() {
    }

    public static int getLabelFor(android.view.View view) {
        return view.getLabelFor();
    }

    public static void setLabelFor(android.view.View view, int id) {
        view.setLabelFor(id);
    }

    public static void setLayerPaint(android.view.View view, android.graphics.Paint paint) {
        view.setLayerPaint(paint);
    }

    public static int getLayoutDirection(android.view.View view) {
        return view.getLayoutDirection();
    }

    public static void setLayoutDirection(android.view.View view, int layoutDirection) {
        view.setLayoutDirection(layoutDirection);
    }

    public static int getPaddingStart(android.view.View view) {
        return view.getPaddingStart();
    }

    public static int getPaddingEnd(android.view.View view) {
        return view.getPaddingEnd();
    }

    public static void setPaddingRelative(android.view.View view, int start, int top, int end, int bottom) {
        view.setPaddingRelative(start, top, end, bottom);
    }

    public static int getWindowSystemUiVisibility(android.view.View view) {
        return view.getWindowSystemUiVisibility();
    }

    public static boolean isPaddingRelative(android.view.View view) {
        return view.isPaddingRelative();
    }

    public static android.view.Display getDisplay(android.view.View view) {
        return view.getDisplay();
    }
}
