package android.support.v4.view.animation;

@android.annotation.TargetApi(21)
@android.support.annotation.RequiresApi(21)
class PathInterpolatorCompatApi21 {
    private PathInterpolatorCompatApi21() {
    }

    public static android.view.animation.Interpolator create(android.graphics.Path path) {
        return new android.view.animation.PathInterpolator(path);
    }

    public static android.view.animation.Interpolator create(float controlX, float controlY) {
        return new android.view.animation.PathInterpolator(controlX, controlY);
    }

    public static android.view.animation.Interpolator create(float controlX1, float controlY1, float controlX2, float controlY2) {
        return new android.view.animation.PathInterpolator(controlX1, controlY1, controlX2, controlY2);
    }
}
