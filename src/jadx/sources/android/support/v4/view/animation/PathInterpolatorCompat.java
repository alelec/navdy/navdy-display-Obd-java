package android.support.v4.view.animation;

public final class PathInterpolatorCompat {
    private PathInterpolatorCompat() {
    }

    public static android.view.animation.Interpolator create(android.graphics.Path path) {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            return android.support.v4.view.animation.PathInterpolatorCompatApi21.create(path);
        }
        return android.support.v4.view.animation.PathInterpolatorCompatBase.create(path);
    }

    public static android.view.animation.Interpolator create(float controlX, float controlY) {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            return android.support.v4.view.animation.PathInterpolatorCompatApi21.create(controlX, controlY);
        }
        return android.support.v4.view.animation.PathInterpolatorCompatBase.create(controlX, controlY);
    }

    public static android.view.animation.Interpolator create(float controlX1, float controlY1, float controlX2, float controlY2) {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            return android.support.v4.view.animation.PathInterpolatorCompatApi21.create(controlX1, controlY1, controlX2, controlY2);
        }
        return android.support.v4.view.animation.PathInterpolatorCompatBase.create(controlX1, controlY1, controlX2, controlY2);
    }
}
