package android.support.v4.view.animation;

@android.annotation.TargetApi(9)
@android.support.annotation.RequiresApi(9)
class PathInterpolatorCompatBase {
    private PathInterpolatorCompatBase() {
    }

    public static android.view.animation.Interpolator create(android.graphics.Path path) {
        return new android.support.v4.view.animation.PathInterpolatorGingerbread(path);
    }

    public static android.view.animation.Interpolator create(float controlX, float controlY) {
        return new android.support.v4.view.animation.PathInterpolatorGingerbread(controlX, controlY);
    }

    public static android.view.animation.Interpolator create(float controlX1, float controlY1, float controlX2, float controlY2) {
        return new android.support.v4.view.animation.PathInterpolatorGingerbread(controlX1, controlY1, controlX2, controlY2);
    }
}
