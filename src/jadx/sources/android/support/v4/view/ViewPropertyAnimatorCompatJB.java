package android.support.v4.view;

@android.annotation.TargetApi(16)
@android.support.annotation.RequiresApi(16)
class ViewPropertyAnimatorCompatJB {
    ViewPropertyAnimatorCompatJB() {
    }

    public static void withStartAction(android.view.View view, java.lang.Runnable runnable) {
        view.animate().withStartAction(runnable);
    }

    public static void withEndAction(android.view.View view, java.lang.Runnable runnable) {
        view.animate().withEndAction(runnable);
    }

    public static void withLayer(android.view.View view) {
        view.animate().withLayer();
    }

    public static void setListener(final android.view.View view, final android.support.v4.view.ViewPropertyAnimatorListener listener) {
        if (listener != null) {
            view.animate().setListener(new android.animation.AnimatorListenerAdapter() {
                public void onAnimationCancel(android.animation.Animator animation) {
                    listener.onAnimationCancel(view);
                }

                public void onAnimationEnd(android.animation.Animator animation) {
                    listener.onAnimationEnd(view);
                }

                public void onAnimationStart(android.animation.Animator animation) {
                    listener.onAnimationStart(view);
                }
            });
        } else {
            view.animate().setListener(null);
        }
    }
}
