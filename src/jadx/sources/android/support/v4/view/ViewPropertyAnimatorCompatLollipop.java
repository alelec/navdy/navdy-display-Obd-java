package android.support.v4.view;

@android.annotation.TargetApi(21)
@android.support.annotation.RequiresApi(21)
class ViewPropertyAnimatorCompatLollipop {
    ViewPropertyAnimatorCompatLollipop() {
    }

    public static void translationZ(android.view.View view, float value) {
        view.animate().translationZ(value);
    }

    public static void translationZBy(android.view.View view, float value) {
        view.animate().translationZBy(value);
    }

    public static void z(android.view.View view, float value) {
        view.animate().z(value);
    }

    public static void zBy(android.view.View view, float value) {
        view.animate().zBy(value);
    }
}
