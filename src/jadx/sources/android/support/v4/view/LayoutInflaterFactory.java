package android.support.v4.view;

public interface LayoutInflaterFactory {
    android.view.View onCreateView(android.view.View view, java.lang.String str, android.content.Context context, android.util.AttributeSet attributeSet);
}
