package android.support.v4.view;

@android.annotation.TargetApi(14)
@android.support.annotation.RequiresApi(14)
class MotionEventCompatICS {
    MotionEventCompatICS() {
    }

    public static int getButtonState(android.view.MotionEvent event) {
        return event.getButtonState();
    }
}
