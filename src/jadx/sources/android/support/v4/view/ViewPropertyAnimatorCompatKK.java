package android.support.v4.view;

@android.annotation.TargetApi(19)
@android.support.annotation.RequiresApi(19)
class ViewPropertyAnimatorCompatKK {
    ViewPropertyAnimatorCompatKK() {
    }

    public static void setUpdateListener(final android.view.View view, final android.support.v4.view.ViewPropertyAnimatorUpdateListener listener) {
        android.animation.ValueAnimator.AnimatorUpdateListener wrapped = null;
        if (listener != null) {
            wrapped = new android.animation.ValueAnimator.AnimatorUpdateListener() {
                public void onAnimationUpdate(android.animation.ValueAnimator valueAnimator) {
                    listener.onAnimationUpdate(view);
                }
            };
        }
        view.animate().setUpdateListener(wrapped);
    }
}
