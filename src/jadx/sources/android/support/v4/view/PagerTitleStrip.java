package android.support.v4.view;

@android.support.v4.view.ViewPager.DecorView
public class PagerTitleStrip extends android.view.ViewGroup {
    private static final int[] ATTRS = {16842804, 16842901, 16842904, 16842927};
    private static final android.support.v4.view.PagerTitleStrip.PagerTitleStripImpl IMPL;
    private static final float SIDE_ALPHA = 0.6f;
    private static final java.lang.String TAG = "PagerTitleStrip";
    private static final int[] TEXT_ATTRS = {16843660};
    private static final int TEXT_SPACING = 16;
    android.widget.TextView mCurrText;
    private int mGravity;
    private int mLastKnownCurrentPage;
    float mLastKnownPositionOffset;
    android.widget.TextView mNextText;
    private int mNonPrimaryAlpha;
    private final android.support.v4.view.PagerTitleStrip.PageListener mPageListener;
    android.support.v4.view.ViewPager mPager;
    android.widget.TextView mPrevText;
    private int mScaledTextSpacing;
    int mTextColor;
    private boolean mUpdatingPositions;
    private boolean mUpdatingText;
    private java.lang.ref.WeakReference<android.support.v4.view.PagerAdapter> mWatchingAdapter;

    private class PageListener extends android.database.DataSetObserver implements android.support.v4.view.ViewPager.OnPageChangeListener, android.support.v4.view.ViewPager.OnAdapterChangeListener {
        private int mScrollState;

        PageListener() {
        }

        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            if (positionOffset > 0.5f) {
                position++;
            }
            android.support.v4.view.PagerTitleStrip.this.updateTextPositions(position, positionOffset, false);
        }

        public void onPageSelected(int position) {
            float offset = 0.0f;
            if (this.mScrollState == 0) {
                android.support.v4.view.PagerTitleStrip.this.updateText(android.support.v4.view.PagerTitleStrip.this.mPager.getCurrentItem(), android.support.v4.view.PagerTitleStrip.this.mPager.getAdapter());
                if (android.support.v4.view.PagerTitleStrip.this.mLastKnownPositionOffset >= 0.0f) {
                    offset = android.support.v4.view.PagerTitleStrip.this.mLastKnownPositionOffset;
                }
                android.support.v4.view.PagerTitleStrip.this.updateTextPositions(android.support.v4.view.PagerTitleStrip.this.mPager.getCurrentItem(), offset, true);
            }
        }

        public void onPageScrollStateChanged(int state) {
            this.mScrollState = state;
        }

        public void onAdapterChanged(android.support.v4.view.ViewPager viewPager, android.support.v4.view.PagerAdapter oldAdapter, android.support.v4.view.PagerAdapter newAdapter) {
            android.support.v4.view.PagerTitleStrip.this.updateAdapter(oldAdapter, newAdapter);
        }

        public void onChanged() {
            float offset = 0.0f;
            android.support.v4.view.PagerTitleStrip.this.updateText(android.support.v4.view.PagerTitleStrip.this.mPager.getCurrentItem(), android.support.v4.view.PagerTitleStrip.this.mPager.getAdapter());
            if (android.support.v4.view.PagerTitleStrip.this.mLastKnownPositionOffset >= 0.0f) {
                offset = android.support.v4.view.PagerTitleStrip.this.mLastKnownPositionOffset;
            }
            android.support.v4.view.PagerTitleStrip.this.updateTextPositions(android.support.v4.view.PagerTitleStrip.this.mPager.getCurrentItem(), offset, true);
        }
    }

    interface PagerTitleStripImpl {
        void setSingleLineAllCaps(android.widget.TextView textView);
    }

    static class PagerTitleStripImplBase implements android.support.v4.view.PagerTitleStrip.PagerTitleStripImpl {
        PagerTitleStripImplBase() {
        }

        public void setSingleLineAllCaps(android.widget.TextView text) {
            text.setSingleLine();
        }
    }

    static class PagerTitleStripImplIcs implements android.support.v4.view.PagerTitleStrip.PagerTitleStripImpl {
        PagerTitleStripImplIcs() {
        }

        public void setSingleLineAllCaps(android.widget.TextView text) {
            android.support.v4.view.PagerTitleStripIcs.setSingleLineAllCaps(text);
        }
    }

    static {
        if (android.os.Build.VERSION.SDK_INT >= 14) {
            IMPL = new android.support.v4.view.PagerTitleStrip.PagerTitleStripImplIcs();
        } else {
            IMPL = new android.support.v4.view.PagerTitleStrip.PagerTitleStripImplBase();
        }
    }

    private static void setSingleLineAllCaps(android.widget.TextView text) {
        IMPL.setSingleLineAllCaps(text);
    }

    public PagerTitleStrip(android.content.Context context) {
        this(context, null);
    }

    public PagerTitleStrip(android.content.Context context, android.util.AttributeSet attrs) {
        super(context, attrs);
        this.mLastKnownCurrentPage = -1;
        this.mLastKnownPositionOffset = -1.0f;
        this.mPageListener = new android.support.v4.view.PagerTitleStrip.PageListener();
        android.widget.TextView textView = new android.widget.TextView(context);
        this.mPrevText = textView;
        addView(textView);
        android.widget.TextView textView2 = new android.widget.TextView(context);
        this.mCurrText = textView2;
        addView(textView2);
        android.widget.TextView textView3 = new android.widget.TextView(context);
        this.mNextText = textView3;
        addView(textView3);
        android.content.res.TypedArray a = context.obtainStyledAttributes(attrs, ATTRS);
        int textAppearance = a.getResourceId(0, 0);
        if (textAppearance != 0) {
            android.support.v4.widget.TextViewCompat.setTextAppearance(this.mPrevText, textAppearance);
            android.support.v4.widget.TextViewCompat.setTextAppearance(this.mCurrText, textAppearance);
            android.support.v4.widget.TextViewCompat.setTextAppearance(this.mNextText, textAppearance);
        }
        int textSize = a.getDimensionPixelSize(1, 0);
        if (textSize != 0) {
            setTextSize(0, (float) textSize);
        }
        if (a.hasValue(2)) {
            int textColor = a.getColor(2, 0);
            this.mPrevText.setTextColor(textColor);
            this.mCurrText.setTextColor(textColor);
            this.mNextText.setTextColor(textColor);
        }
        this.mGravity = a.getInteger(3, 80);
        a.recycle();
        this.mTextColor = this.mCurrText.getTextColors().getDefaultColor();
        setNonPrimaryAlpha(SIDE_ALPHA);
        this.mPrevText.setEllipsize(android.text.TextUtils.TruncateAt.END);
        this.mCurrText.setEllipsize(android.text.TextUtils.TruncateAt.END);
        this.mNextText.setEllipsize(android.text.TextUtils.TruncateAt.END);
        boolean allCaps = false;
        if (textAppearance != 0) {
            android.content.res.TypedArray ta = context.obtainStyledAttributes(textAppearance, TEXT_ATTRS);
            allCaps = ta.getBoolean(0, false);
            ta.recycle();
        }
        if (allCaps) {
            setSingleLineAllCaps(this.mPrevText);
            setSingleLineAllCaps(this.mCurrText);
            setSingleLineAllCaps(this.mNextText);
        } else {
            this.mPrevText.setSingleLine();
            this.mCurrText.setSingleLine();
            this.mNextText.setSingleLine();
        }
        this.mScaledTextSpacing = (int) (16.0f * context.getResources().getDisplayMetrics().density);
    }

    public void setTextSpacing(int spacingPixels) {
        this.mScaledTextSpacing = spacingPixels;
        requestLayout();
    }

    public int getTextSpacing() {
        return this.mScaledTextSpacing;
    }

    public void setNonPrimaryAlpha(@android.support.annotation.FloatRange(from = 0.0d, to = 1.0d) float alpha) {
        this.mNonPrimaryAlpha = ((int) (255.0f * alpha)) & 255;
        int transparentColor = (this.mNonPrimaryAlpha << 24) | (this.mTextColor & android.support.v4.view.ViewCompat.MEASURED_SIZE_MASK);
        this.mPrevText.setTextColor(transparentColor);
        this.mNextText.setTextColor(transparentColor);
    }

    public void setTextColor(@android.support.annotation.ColorInt int color) {
        this.mTextColor = color;
        this.mCurrText.setTextColor(color);
        int transparentColor = (this.mNonPrimaryAlpha << 24) | (this.mTextColor & android.support.v4.view.ViewCompat.MEASURED_SIZE_MASK);
        this.mPrevText.setTextColor(transparentColor);
        this.mNextText.setTextColor(transparentColor);
    }

    public void setTextSize(int unit, float size) {
        this.mPrevText.setTextSize(unit, size);
        this.mCurrText.setTextSize(unit, size);
        this.mNextText.setTextSize(unit, size);
    }

    public void setGravity(int gravity) {
        this.mGravity = gravity;
        requestLayout();
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        android.view.ViewParent parent = getParent();
        if (!(parent instanceof android.support.v4.view.ViewPager)) {
            throw new java.lang.IllegalStateException("PagerTitleStrip must be a direct child of a ViewPager.");
        }
        android.support.v4.view.ViewPager pager = (android.support.v4.view.ViewPager) parent;
        android.support.v4.view.PagerAdapter adapter = pager.getAdapter();
        pager.setInternalPageChangeListener(this.mPageListener);
        pager.addOnAdapterChangeListener(this.mPageListener);
        this.mPager = pager;
        updateAdapter(this.mWatchingAdapter != null ? (android.support.v4.view.PagerAdapter) this.mWatchingAdapter.get() : null, adapter);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.mPager != null) {
            updateAdapter(this.mPager.getAdapter(), null);
            this.mPager.setInternalPageChangeListener(null);
            this.mPager.removeOnAdapterChangeListener(this.mPageListener);
            this.mPager = null;
        }
    }

    /* access modifiers changed from: 0000 */
    public void updateText(int currentItem, android.support.v4.view.PagerAdapter adapter) {
        int itemCount;
        if (adapter != null) {
            itemCount = adapter.getCount();
        } else {
            itemCount = 0;
        }
        this.mUpdatingText = true;
        java.lang.CharSequence text = null;
        if (currentItem >= 1 && adapter != null) {
            text = adapter.getPageTitle(currentItem - 1);
        }
        this.mPrevText.setText(text);
        this.mCurrText.setText((adapter == null || currentItem >= itemCount) ? null : adapter.getPageTitle(currentItem));
        java.lang.CharSequence text2 = null;
        if (currentItem + 1 < itemCount && adapter != null) {
            text2 = adapter.getPageTitle(currentItem + 1);
        }
        this.mNextText.setText(text2);
        int childWidthSpec = android.view.View.MeasureSpec.makeMeasureSpec(java.lang.Math.max(0, (int) (((float) ((getWidth() - getPaddingLeft()) - getPaddingRight())) * 0.8f)), Integer.MIN_VALUE);
        int childHeightSpec = android.view.View.MeasureSpec.makeMeasureSpec(java.lang.Math.max(0, (getHeight() - getPaddingTop()) - getPaddingBottom()), Integer.MIN_VALUE);
        this.mPrevText.measure(childWidthSpec, childHeightSpec);
        this.mCurrText.measure(childWidthSpec, childHeightSpec);
        this.mNextText.measure(childWidthSpec, childHeightSpec);
        this.mLastKnownCurrentPage = currentItem;
        if (!this.mUpdatingPositions) {
            updateTextPositions(currentItem, this.mLastKnownPositionOffset, false);
        }
        this.mUpdatingText = false;
    }

    public void requestLayout() {
        if (!this.mUpdatingText) {
            super.requestLayout();
        }
    }

    /* access modifiers changed from: 0000 */
    public void updateAdapter(android.support.v4.view.PagerAdapter oldAdapter, android.support.v4.view.PagerAdapter newAdapter) {
        if (oldAdapter != null) {
            oldAdapter.unregisterDataSetObserver(this.mPageListener);
            this.mWatchingAdapter = null;
        }
        if (newAdapter != null) {
            newAdapter.registerDataSetObserver(this.mPageListener);
            this.mWatchingAdapter = new java.lang.ref.WeakReference<>(newAdapter);
        }
        if (this.mPager != null) {
            this.mLastKnownCurrentPage = -1;
            this.mLastKnownPositionOffset = -1.0f;
            updateText(this.mPager.getCurrentItem(), newAdapter);
            requestLayout();
        }
    }

    /* access modifiers changed from: 0000 */
    public void updateTextPositions(int position, float positionOffset, boolean force) {
        int prevTop;
        int currTop;
        int nextTop;
        if (position != this.mLastKnownCurrentPage) {
            updateText(position, this.mPager.getAdapter());
        } else if (!force && positionOffset == this.mLastKnownPositionOffset) {
            return;
        }
        this.mUpdatingPositions = true;
        int prevWidth = this.mPrevText.getMeasuredWidth();
        int currWidth = this.mCurrText.getMeasuredWidth();
        int nextWidth = this.mNextText.getMeasuredWidth();
        int halfCurrWidth = currWidth / 2;
        int stripWidth = getWidth();
        int stripHeight = getHeight();
        int paddingLeft = getPaddingLeft();
        int paddingRight = getPaddingRight();
        int paddingTop = getPaddingTop();
        int paddingBottom = getPaddingBottom();
        int textPaddedRight = paddingRight + halfCurrWidth;
        int contentWidth = (stripWidth - (paddingLeft + halfCurrWidth)) - textPaddedRight;
        float currOffset = positionOffset + 0.5f;
        if (currOffset > 1.0f) {
            currOffset -= 1.0f;
        }
        int currLeft = ((stripWidth - textPaddedRight) - ((int) (((float) contentWidth) * currOffset))) - (currWidth / 2);
        int currRight = currLeft + currWidth;
        int prevBaseline = this.mPrevText.getBaseline();
        int currBaseline = this.mCurrText.getBaseline();
        int nextBaseline = this.mNextText.getBaseline();
        int maxBaseline = java.lang.Math.max(java.lang.Math.max(prevBaseline, currBaseline), nextBaseline);
        int prevTopOffset = maxBaseline - prevBaseline;
        int currTopOffset = maxBaseline - currBaseline;
        int nextTopOffset = maxBaseline - nextBaseline;
        int alignedNextHeight = nextTopOffset + this.mNextText.getMeasuredHeight();
        int maxTextHeight = java.lang.Math.max(java.lang.Math.max(prevTopOffset + this.mPrevText.getMeasuredHeight(), currTopOffset + this.mCurrText.getMeasuredHeight()), alignedNextHeight);
        switch (this.mGravity & ch.qos.logback.core.net.SyslogConstants.LOG_ALERT) {
            case 16:
                int centeredTop = (((stripHeight - paddingTop) - paddingBottom) - maxTextHeight) / 2;
                prevTop = centeredTop + prevTopOffset;
                currTop = centeredTop + currTopOffset;
                nextTop = centeredTop + nextTopOffset;
                break;
            case ch.qos.logback.core.net.SyslogConstants.LOG_AUTHPRIV /*80*/:
                int bottomGravTop = (stripHeight - paddingBottom) - maxTextHeight;
                prevTop = bottomGravTop + prevTopOffset;
                currTop = bottomGravTop + currTopOffset;
                nextTop = bottomGravTop + nextTopOffset;
                break;
            default:
                prevTop = paddingTop + prevTopOffset;
                currTop = paddingTop + currTopOffset;
                nextTop = paddingTop + nextTopOffset;
                break;
        }
        this.mCurrText.layout(currLeft, currTop, currRight, this.mCurrText.getMeasuredHeight() + currTop);
        int prevLeft = java.lang.Math.min(paddingLeft, (currLeft - this.mScaledTextSpacing) - prevWidth);
        this.mPrevText.layout(prevLeft, prevTop, prevLeft + prevWidth, this.mPrevText.getMeasuredHeight() + prevTop);
        int nextLeft = java.lang.Math.max((stripWidth - paddingRight) - nextWidth, this.mScaledTextSpacing + currRight);
        this.mNextText.layout(nextLeft, nextTop, nextLeft + nextWidth, this.mNextText.getMeasuredHeight() + nextTop);
        this.mLastKnownPositionOffset = positionOffset;
        this.mUpdatingPositions = false;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int height;
        if (android.view.View.MeasureSpec.getMode(widthMeasureSpec) != 1073741824) {
            throw new java.lang.IllegalStateException("Must measure with an exact width");
        }
        int heightPadding = getPaddingTop() + getPaddingBottom();
        int childHeightSpec = getChildMeasureSpec(heightMeasureSpec, heightPadding, -2);
        int widthSize = android.view.View.MeasureSpec.getSize(widthMeasureSpec);
        int childWidthSpec = getChildMeasureSpec(widthMeasureSpec, (int) (((float) widthSize) * 0.2f), -2);
        this.mPrevText.measure(childWidthSpec, childHeightSpec);
        this.mCurrText.measure(childWidthSpec, childHeightSpec);
        this.mNextText.measure(childWidthSpec, childHeightSpec);
        if (android.view.View.MeasureSpec.getMode(heightMeasureSpec) == 1073741824) {
            height = android.view.View.MeasureSpec.getSize(heightMeasureSpec);
        } else {
            height = java.lang.Math.max(getMinHeight(), this.mCurrText.getMeasuredHeight() + heightPadding);
        }
        setMeasuredDimension(widthSize, android.support.v4.view.ViewCompat.resolveSizeAndState(height, heightMeasureSpec, android.support.v4.view.ViewCompat.getMeasuredState(this.mCurrText) << 16));
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean changed, int l, int t, int r, int b) {
        float offset = 0.0f;
        if (this.mPager != null) {
            if (this.mLastKnownPositionOffset >= 0.0f) {
                offset = this.mLastKnownPositionOffset;
            }
            updateTextPositions(this.mLastKnownCurrentPage, offset, true);
        }
    }

    /* access modifiers changed from: 0000 */
    public int getMinHeight() {
        android.graphics.drawable.Drawable bg = getBackground();
        if (bg != null) {
            return bg.getIntrinsicHeight();
        }
        return 0;
    }
}
