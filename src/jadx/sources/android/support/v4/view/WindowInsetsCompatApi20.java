package android.support.v4.view;

@android.annotation.TargetApi(20)
@android.support.annotation.RequiresApi(20)
class WindowInsetsCompatApi20 {
    WindowInsetsCompatApi20() {
    }

    public static java.lang.Object consumeSystemWindowInsets(java.lang.Object insets) {
        return ((android.view.WindowInsets) insets).consumeSystemWindowInsets();
    }

    public static int getSystemWindowInsetBottom(java.lang.Object insets) {
        return ((android.view.WindowInsets) insets).getSystemWindowInsetBottom();
    }

    public static int getSystemWindowInsetLeft(java.lang.Object insets) {
        return ((android.view.WindowInsets) insets).getSystemWindowInsetLeft();
    }

    public static int getSystemWindowInsetRight(java.lang.Object insets) {
        return ((android.view.WindowInsets) insets).getSystemWindowInsetRight();
    }

    public static int getSystemWindowInsetTop(java.lang.Object insets) {
        return ((android.view.WindowInsets) insets).getSystemWindowInsetTop();
    }

    public static boolean hasInsets(java.lang.Object insets) {
        return ((android.view.WindowInsets) insets).hasInsets();
    }

    public static boolean hasSystemWindowInsets(java.lang.Object insets) {
        return ((android.view.WindowInsets) insets).hasSystemWindowInsets();
    }

    public static boolean isRound(java.lang.Object insets) {
        return ((android.view.WindowInsets) insets).isRound();
    }

    public static java.lang.Object replaceSystemWindowInsets(java.lang.Object insets, int left, int top, int right, int bottom) {
        return ((android.view.WindowInsets) insets).replaceSystemWindowInsets(left, top, right, bottom);
    }

    public static java.lang.Object getSourceWindowInsets(java.lang.Object src) {
        return new android.view.WindowInsets((android.view.WindowInsets) src);
    }
}
