package android.support.v4.view;

public final class ScaleGestureDetectorCompat {
    static final android.support.v4.view.ScaleGestureDetectorCompat.ScaleGestureDetectorImpl IMPL;

    private static class BaseScaleGestureDetectorImpl implements android.support.v4.view.ScaleGestureDetectorCompat.ScaleGestureDetectorImpl {
        BaseScaleGestureDetectorImpl() {
        }

        public void setQuickScaleEnabled(java.lang.Object o, boolean enabled) {
        }

        public boolean isQuickScaleEnabled(java.lang.Object o) {
            return false;
        }
    }

    private static class ScaleGestureDetectorCompatKitKatImpl implements android.support.v4.view.ScaleGestureDetectorCompat.ScaleGestureDetectorImpl {
        ScaleGestureDetectorCompatKitKatImpl() {
        }

        public void setQuickScaleEnabled(java.lang.Object o, boolean enabled) {
            android.support.v4.view.ScaleGestureDetectorCompatKitKat.setQuickScaleEnabled(o, enabled);
        }

        public boolean isQuickScaleEnabled(java.lang.Object o) {
            return android.support.v4.view.ScaleGestureDetectorCompatKitKat.isQuickScaleEnabled(o);
        }
    }

    interface ScaleGestureDetectorImpl {
        boolean isQuickScaleEnabled(java.lang.Object obj);

        void setQuickScaleEnabled(java.lang.Object obj, boolean z);
    }

    static {
        if (android.os.Build.VERSION.SDK_INT >= 19) {
            IMPL = new android.support.v4.view.ScaleGestureDetectorCompat.ScaleGestureDetectorCompatKitKatImpl();
        } else {
            IMPL = new android.support.v4.view.ScaleGestureDetectorCompat.BaseScaleGestureDetectorImpl();
        }
    }

    private ScaleGestureDetectorCompat() {
    }

    public static void setQuickScaleEnabled(java.lang.Object scaleGestureDetector, boolean enabled) {
        IMPL.setQuickScaleEnabled(scaleGestureDetector, enabled);
    }

    public static boolean isQuickScaleEnabled(java.lang.Object scaleGestureDetector) {
        return IMPL.isQuickScaleEnabled(scaleGestureDetector);
    }
}
