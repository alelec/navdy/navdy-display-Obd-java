package android.support.v4.view;

@android.annotation.TargetApi(19)
@android.support.annotation.RequiresApi(19)
class ViewCompatKitKat {
    ViewCompatKitKat() {
    }

    public static int getAccessibilityLiveRegion(android.view.View view) {
        return view.getAccessibilityLiveRegion();
    }

    public static void setAccessibilityLiveRegion(android.view.View view, int mode) {
        view.setAccessibilityLiveRegion(mode);
    }

    public static boolean isLaidOut(android.view.View view) {
        return view.isLaidOut();
    }

    public static boolean isAttachedToWindow(android.view.View view) {
        return view.isAttachedToWindow();
    }

    public static boolean isLayoutDirectionResolved(android.view.View view) {
        return view.isLayoutDirectionResolved();
    }
}
