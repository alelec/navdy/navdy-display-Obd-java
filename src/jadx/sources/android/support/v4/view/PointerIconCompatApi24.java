package android.support.v4.view;

@android.annotation.TargetApi(24)
@android.support.annotation.RequiresApi(24)
class PointerIconCompatApi24 {
    PointerIconCompatApi24() {
    }

    public static java.lang.Object getSystemIcon(android.content.Context context, int style) {
        return android.view.PointerIcon.getSystemIcon(context, style);
    }

    public static java.lang.Object create(android.graphics.Bitmap bitmap, float hotSpotX, float hotSpotY) {
        return android.view.PointerIcon.create(bitmap, hotSpotX, hotSpotY);
    }

    public static java.lang.Object load(android.content.res.Resources resources, int resourceId) {
        return android.view.PointerIcon.load(resources, resourceId);
    }
}
