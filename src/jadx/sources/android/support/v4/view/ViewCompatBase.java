package android.support.v4.view;

@android.annotation.TargetApi(9)
@android.support.annotation.RequiresApi(9)
class ViewCompatBase {
    private static final java.lang.String TAG = "ViewCompatBase";
    private static java.lang.reflect.Field sMinHeightField;
    private static boolean sMinHeightFieldFetched;
    private static java.lang.reflect.Field sMinWidthField;
    private static boolean sMinWidthFieldFetched;

    ViewCompatBase() {
    }

    static android.content.res.ColorStateList getBackgroundTintList(android.view.View view) {
        if (view instanceof android.support.v4.view.TintableBackgroundView) {
            return ((android.support.v4.view.TintableBackgroundView) view).getSupportBackgroundTintList();
        }
        return null;
    }

    static void setBackgroundTintList(android.view.View view, android.content.res.ColorStateList tintList) {
        if (view instanceof android.support.v4.view.TintableBackgroundView) {
            ((android.support.v4.view.TintableBackgroundView) view).setSupportBackgroundTintList(tintList);
        }
    }

    static android.graphics.PorterDuff.Mode getBackgroundTintMode(android.view.View view) {
        if (view instanceof android.support.v4.view.TintableBackgroundView) {
            return ((android.support.v4.view.TintableBackgroundView) view).getSupportBackgroundTintMode();
        }
        return null;
    }

    static void setBackgroundTintMode(android.view.View view, android.graphics.PorterDuff.Mode mode) {
        if (view instanceof android.support.v4.view.TintableBackgroundView) {
            ((android.support.v4.view.TintableBackgroundView) view).setSupportBackgroundTintMode(mode);
        }
    }

    static boolean isLaidOut(android.view.View view) {
        return view.getWidth() > 0 && view.getHeight() > 0;
    }

    static int getMinimumWidth(android.view.View view) {
        if (!sMinWidthFieldFetched) {
            try {
                sMinWidthField = android.view.View.class.getDeclaredField("mMinWidth");
                sMinWidthField.setAccessible(true);
            } catch (java.lang.NoSuchFieldException e) {
            }
            sMinWidthFieldFetched = true;
        }
        if (sMinWidthField != null) {
            try {
                return ((java.lang.Integer) sMinWidthField.get(view)).intValue();
            } catch (java.lang.Exception e2) {
            }
        }
        return 0;
    }

    static int getMinimumHeight(android.view.View view) {
        if (!sMinHeightFieldFetched) {
            try {
                sMinHeightField = android.view.View.class.getDeclaredField("mMinHeight");
                sMinHeightField.setAccessible(true);
            } catch (java.lang.NoSuchFieldException e) {
            }
            sMinHeightFieldFetched = true;
        }
        if (sMinHeightField != null) {
            try {
                return ((java.lang.Integer) sMinHeightField.get(view)).intValue();
            } catch (java.lang.Exception e2) {
            }
        }
        return 0;
    }

    static boolean isAttachedToWindow(android.view.View view) {
        return view.getWindowToken() != null;
    }

    static void offsetTopAndBottom(android.view.View view, int offset) {
        int currentTop = view.getTop();
        view.offsetTopAndBottom(offset);
        if (offset != 0) {
            android.view.ViewParent parent = view.getParent();
            if (parent instanceof android.view.View) {
                int absOffset = java.lang.Math.abs(offset);
                ((android.view.View) parent).invalidate(view.getLeft(), currentTop - absOffset, view.getRight(), view.getHeight() + currentTop + absOffset);
                return;
            }
            view.invalidate();
        }
    }

    static void offsetLeftAndRight(android.view.View view, int offset) {
        int currentLeft = view.getLeft();
        view.offsetLeftAndRight(offset);
        if (offset != 0) {
            android.view.ViewParent parent = view.getParent();
            if (parent instanceof android.view.View) {
                int absOffset = java.lang.Math.abs(offset);
                ((android.view.View) parent).invalidate(currentLeft - absOffset, view.getTop(), view.getWidth() + currentLeft + absOffset, view.getBottom());
                return;
            }
            view.invalidate();
        }
    }

    static android.view.Display getDisplay(android.view.View view) {
        if (isAttachedToWindow(view)) {
            return ((android.view.WindowManager) view.getContext().getSystemService("window")).getDefaultDisplay();
        }
        return null;
    }
}
