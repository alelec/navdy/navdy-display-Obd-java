package android.support.v4.view;

@android.annotation.TargetApi(15)
@android.support.annotation.RequiresApi(15)
class ViewCompatICSMr1 {
    ViewCompatICSMr1() {
    }

    public static boolean hasOnClickListeners(android.view.View v) {
        return v.hasOnClickListeners();
    }
}
