package android.support.v4.view.accessibility;

@android.annotation.TargetApi(23)
@android.support.annotation.RequiresApi(23)
class AccessibilityNodeInfoCompatApi23 {
    AccessibilityNodeInfoCompatApi23() {
    }

    public static java.lang.Object getActionScrollToPosition() {
        return android.view.accessibility.AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_TO_POSITION;
    }

    public static boolean isContextClickable(java.lang.Object info) {
        return ((android.view.accessibility.AccessibilityNodeInfo) info).isContextClickable();
    }

    public static void setContextClickable(java.lang.Object info, boolean contextClickable) {
        ((android.view.accessibility.AccessibilityNodeInfo) info).setContextClickable(contextClickable);
    }

    public static java.lang.Object getActionShowOnScreen() {
        return android.view.accessibility.AccessibilityNodeInfo.AccessibilityAction.ACTION_SHOW_ON_SCREEN;
    }

    public static java.lang.Object getActionScrollUp() {
        return android.view.accessibility.AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_UP;
    }

    public static java.lang.Object getActionScrollDown() {
        return android.view.accessibility.AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_DOWN;
    }

    public static java.lang.Object getActionScrollLeft() {
        return android.view.accessibility.AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_LEFT;
    }

    public static java.lang.Object getActionScrollRight() {
        return android.view.accessibility.AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_RIGHT;
    }

    public static java.lang.Object getActionContextClick() {
        return android.view.accessibility.AccessibilityNodeInfo.AccessibilityAction.ACTION_CONTEXT_CLICK;
    }
}
