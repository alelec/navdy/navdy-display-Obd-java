package android.support.v4.view.accessibility;

@android.annotation.TargetApi(19)
@android.support.annotation.RequiresApi(19)
class AccessibilityEventCompatKitKat {
    AccessibilityEventCompatKitKat() {
    }

    public static void setContentChangeTypes(android.view.accessibility.AccessibilityEvent event, int changeTypes) {
        event.setContentChangeTypes(changeTypes);
    }

    public static int getContentChangeTypes(android.view.accessibility.AccessibilityEvent event) {
        return event.getContentChangeTypes();
    }
}
