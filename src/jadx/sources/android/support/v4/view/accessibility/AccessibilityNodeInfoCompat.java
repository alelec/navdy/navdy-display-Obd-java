package android.support.v4.view.accessibility;

public class AccessibilityNodeInfoCompat {
    public static final int ACTION_ACCESSIBILITY_FOCUS = 64;
    public static final java.lang.String ACTION_ARGUMENT_COLUMN_INT = "android.view.accessibility.action.ARGUMENT_COLUMN_INT";
    public static final java.lang.String ACTION_ARGUMENT_EXTEND_SELECTION_BOOLEAN = "ACTION_ARGUMENT_EXTEND_SELECTION_BOOLEAN";
    public static final java.lang.String ACTION_ARGUMENT_HTML_ELEMENT_STRING = "ACTION_ARGUMENT_HTML_ELEMENT_STRING";
    public static final java.lang.String ACTION_ARGUMENT_MOVEMENT_GRANULARITY_INT = "ACTION_ARGUMENT_MOVEMENT_GRANULARITY_INT";
    public static final java.lang.String ACTION_ARGUMENT_PROGRESS_VALUE = "android.view.accessibility.action.ARGUMENT_PROGRESS_VALUE";
    public static final java.lang.String ACTION_ARGUMENT_ROW_INT = "android.view.accessibility.action.ARGUMENT_ROW_INT";
    public static final java.lang.String ACTION_ARGUMENT_SELECTION_END_INT = "ACTION_ARGUMENT_SELECTION_END_INT";
    public static final java.lang.String ACTION_ARGUMENT_SELECTION_START_INT = "ACTION_ARGUMENT_SELECTION_START_INT";
    public static final java.lang.String ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE = "ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE";
    public static final int ACTION_CLEAR_ACCESSIBILITY_FOCUS = 128;
    public static final int ACTION_CLEAR_FOCUS = 2;
    public static final int ACTION_CLEAR_SELECTION = 8;
    public static final int ACTION_CLICK = 16;
    public static final int ACTION_COLLAPSE = 524288;
    public static final int ACTION_COPY = 16384;
    public static final int ACTION_CUT = 65536;
    public static final int ACTION_DISMISS = 1048576;
    public static final int ACTION_EXPAND = 262144;
    public static final int ACTION_FOCUS = 1;
    public static final int ACTION_LONG_CLICK = 32;
    public static final int ACTION_NEXT_AT_MOVEMENT_GRANULARITY = 256;
    public static final int ACTION_NEXT_HTML_ELEMENT = 1024;
    public static final int ACTION_PASTE = 32768;
    public static final int ACTION_PREVIOUS_AT_MOVEMENT_GRANULARITY = 512;
    public static final int ACTION_PREVIOUS_HTML_ELEMENT = 2048;
    public static final int ACTION_SCROLL_BACKWARD = 8192;
    public static final int ACTION_SCROLL_FORWARD = 4096;
    public static final int ACTION_SELECT = 4;
    public static final int ACTION_SET_SELECTION = 131072;
    public static final int ACTION_SET_TEXT = 2097152;
    public static final int FOCUS_ACCESSIBILITY = 2;
    public static final int FOCUS_INPUT = 1;
    static final android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityNodeInfoImpl IMPL;
    public static final int MOVEMENT_GRANULARITY_CHARACTER = 1;
    public static final int MOVEMENT_GRANULARITY_LINE = 4;
    public static final int MOVEMENT_GRANULARITY_PAGE = 16;
    public static final int MOVEMENT_GRANULARITY_PARAGRAPH = 8;
    public static final int MOVEMENT_GRANULARITY_WORD = 2;
    private final java.lang.Object mInfo;
    @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
    public int mParentVirtualDescendantId = -1;

    public static class AccessibilityActionCompat {
        public static final android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat ACTION_ACCESSIBILITY_FOCUS = new android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat(64, null);
        public static final android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat ACTION_CLEAR_ACCESSIBILITY_FOCUS = new android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat(128, null);
        public static final android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat ACTION_CLEAR_FOCUS = new android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat(2, null);
        public static final android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat ACTION_CLEAR_SELECTION = new android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat(8, null);
        public static final android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat ACTION_CLICK = new android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat(16, null);
        public static final android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat ACTION_COLLAPSE = new android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat(524288, null);
        public static final android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat ACTION_CONTEXT_CLICK = new android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat(android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.IMPL.getActionContextClick());
        public static final android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat ACTION_COPY = new android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat(16384, null);
        public static final android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat ACTION_CUT = new android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat(65536, null);
        public static final android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat ACTION_DISMISS = new android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat(1048576, null);
        public static final android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat ACTION_EXPAND = new android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat(262144, null);
        public static final android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat ACTION_FOCUS = new android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat(1, null);
        public static final android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat ACTION_LONG_CLICK = new android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat(32, null);
        public static final android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat ACTION_NEXT_AT_MOVEMENT_GRANULARITY = new android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat(256, null);
        public static final android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat ACTION_NEXT_HTML_ELEMENT = new android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat(1024, null);
        public static final android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat ACTION_PASTE = new android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat(32768, null);
        public static final android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat ACTION_PREVIOUS_AT_MOVEMENT_GRANULARITY = new android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat(512, null);
        public static final android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat ACTION_PREVIOUS_HTML_ELEMENT = new android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat(2048, null);
        public static final android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat ACTION_SCROLL_BACKWARD = new android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat(8192, null);
        public static final android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat ACTION_SCROLL_DOWN = new android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat(android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.IMPL.getActionScrollDown());
        public static final android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat ACTION_SCROLL_FORWARD = new android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat(4096, null);
        public static final android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat ACTION_SCROLL_LEFT = new android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat(android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.IMPL.getActionScrollLeft());
        public static final android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat ACTION_SCROLL_RIGHT = new android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat(android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.IMPL.getActionScrollRight());
        public static final android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat ACTION_SCROLL_TO_POSITION = new android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat(android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.IMPL.getActionScrollToPosition());
        public static final android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat ACTION_SCROLL_UP = new android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat(android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.IMPL.getActionScrollUp());
        public static final android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat ACTION_SELECT = new android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat(4, null);
        public static final android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat ACTION_SET_PROGRESS = new android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat(android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.IMPL.getActionSetProgress());
        public static final android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat ACTION_SET_SELECTION = new android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat(131072, null);
        public static final android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat ACTION_SET_TEXT = new android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat(2097152, null);
        public static final android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat ACTION_SHOW_ON_SCREEN = new android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat(android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.IMPL.getActionShowOnScreen());
        final java.lang.Object mAction;

        public AccessibilityActionCompat(int actionId, java.lang.CharSequence label) {
            this(android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.IMPL.newAccessibilityAction(actionId, label));
        }

        AccessibilityActionCompat(java.lang.Object action) {
            this.mAction = action;
        }

        public int getId() {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.IMPL.getAccessibilityActionId(this.mAction);
        }

        public java.lang.CharSequence getLabel() {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.IMPL.getAccessibilityActionLabel(this.mAction);
        }
    }

    static class AccessibilityNodeInfoApi21Impl extends android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityNodeInfoKitKatImpl {
        AccessibilityNodeInfoApi21Impl() {
        }

        public java.lang.Object newAccessibilityAction(int actionId, java.lang.CharSequence label) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatApi21.newAccessibilityAction(actionId, label);
        }

        public java.util.List<java.lang.Object> getActionList(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatApi21.getActionList(info);
        }

        public java.lang.Object obtainCollectionInfo(int rowCount, int columnCount, boolean hierarchical, int selectionMode) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatApi21.obtainCollectionInfo(rowCount, columnCount, hierarchical, selectionMode);
        }

        public void addAction(java.lang.Object info, java.lang.Object action) {
            android.support.v4.view.accessibility.AccessibilityNodeInfoCompatApi21.addAction(info, action);
        }

        public boolean removeAction(java.lang.Object info, java.lang.Object action) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatApi21.removeAction(info, action);
        }

        public int getAccessibilityActionId(java.lang.Object action) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatApi21.getAccessibilityActionId(action);
        }

        public java.lang.CharSequence getAccessibilityActionLabel(java.lang.Object action) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatApi21.getAccessibilityActionLabel(action);
        }

        public java.lang.Object obtainCollectionItemInfo(int rowIndex, int rowSpan, int columnIndex, int columnSpan, boolean heading, boolean selected) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatApi21.obtainCollectionItemInfo(rowIndex, rowSpan, columnIndex, columnSpan, heading, selected);
        }

        public boolean isCollectionItemSelected(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatApi21.CollectionItemInfo.isSelected(info);
        }

        public java.lang.CharSequence getError(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatApi21.getError(info);
        }

        public void setError(java.lang.Object info, java.lang.CharSequence error) {
            android.support.v4.view.accessibility.AccessibilityNodeInfoCompatApi21.setError(info, error);
        }

        public void setMaxTextLength(java.lang.Object info, int max) {
            android.support.v4.view.accessibility.AccessibilityNodeInfoCompatApi21.setMaxTextLength(info, max);
        }

        public int getMaxTextLength(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatApi21.getMaxTextLength(info);
        }

        public java.lang.Object getWindow(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatApi21.getWindow(info);
        }

        public boolean removeChild(java.lang.Object info, android.view.View child) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatApi21.removeChild(info, child);
        }

        public boolean removeChild(java.lang.Object info, android.view.View root, int virtualDescendantId) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatApi21.removeChild(info, root, virtualDescendantId);
        }

        public int getCollectionInfoSelectionMode(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatApi21.CollectionInfo.getSelectionMode(info);
        }
    }

    static class AccessibilityNodeInfoApi22Impl extends android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityNodeInfoApi21Impl {
        AccessibilityNodeInfoApi22Impl() {
        }

        public java.lang.Object getTraversalBefore(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatApi22.getTraversalBefore(info);
        }

        public void setTraversalBefore(java.lang.Object info, android.view.View view) {
            android.support.v4.view.accessibility.AccessibilityNodeInfoCompatApi22.setTraversalBefore(info, view);
        }

        public void setTraversalBefore(java.lang.Object info, android.view.View root, int virtualDescendantId) {
            android.support.v4.view.accessibility.AccessibilityNodeInfoCompatApi22.setTraversalBefore(info, root, virtualDescendantId);
        }

        public java.lang.Object getTraversalAfter(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatApi22.getTraversalAfter(info);
        }

        public void setTraversalAfter(java.lang.Object info, android.view.View view) {
            android.support.v4.view.accessibility.AccessibilityNodeInfoCompatApi22.setTraversalAfter(info, view);
        }

        public void setTraversalAfter(java.lang.Object info, android.view.View root, int virtualDescendantId) {
            android.support.v4.view.accessibility.AccessibilityNodeInfoCompatApi22.setTraversalAfter(info, root, virtualDescendantId);
        }
    }

    static class AccessibilityNodeInfoApi23Impl extends android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityNodeInfoApi22Impl {
        AccessibilityNodeInfoApi23Impl() {
        }

        public java.lang.Object getActionScrollToPosition() {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatApi23.getActionScrollToPosition();
        }

        public java.lang.Object getActionShowOnScreen() {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatApi23.getActionShowOnScreen();
        }

        public java.lang.Object getActionScrollUp() {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatApi23.getActionScrollUp();
        }

        public java.lang.Object getActionScrollDown() {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatApi23.getActionScrollDown();
        }

        public java.lang.Object getActionScrollLeft() {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatApi23.getActionScrollLeft();
        }

        public java.lang.Object getActionScrollRight() {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatApi23.getActionScrollRight();
        }

        public java.lang.Object getActionContextClick() {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatApi23.getActionContextClick();
        }

        public boolean isContextClickable(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatApi23.isContextClickable(info);
        }

        public void setContextClickable(java.lang.Object info, boolean contextClickable) {
            android.support.v4.view.accessibility.AccessibilityNodeInfoCompatApi23.setContextClickable(info, contextClickable);
        }
    }

    static class AccessibilityNodeInfoApi24Impl extends android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityNodeInfoApi23Impl {
        AccessibilityNodeInfoApi24Impl() {
        }

        public java.lang.Object getActionSetProgress() {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatApi24.getActionSetProgress();
        }

        public int getDrawingOrder(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatApi24.getDrawingOrder(info);
        }

        public void setDrawingOrder(java.lang.Object info, int drawingOrderInParent) {
            android.support.v4.view.accessibility.AccessibilityNodeInfoCompatApi24.setDrawingOrder(info, drawingOrderInParent);
        }

        public boolean isImportantForAccessibility(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatApi24.isImportantForAccessibility(info);
        }

        public void setImportantForAccessibility(java.lang.Object info, boolean importantForAccessibility) {
            android.support.v4.view.accessibility.AccessibilityNodeInfoCompatApi24.setImportantForAccessibility(info, importantForAccessibility);
        }
    }

    static class AccessibilityNodeInfoIcsImpl extends android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityNodeInfoStubImpl {
        AccessibilityNodeInfoIcsImpl() {
        }

        public java.lang.Object obtain() {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.obtain();
        }

        public java.lang.Object obtain(android.view.View source) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.obtain(source);
        }

        public java.lang.Object obtain(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.obtain(info);
        }

        public void addAction(java.lang.Object info, int action) {
            android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.addAction(info, action);
        }

        public void addChild(java.lang.Object info, android.view.View child) {
            android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.addChild(info, child);
        }

        public java.util.List<java.lang.Object> findAccessibilityNodeInfosByText(java.lang.Object info, java.lang.String text) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.findAccessibilityNodeInfosByText(info, text);
        }

        public int getActions(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.getActions(info);
        }

        public void getBoundsInParent(java.lang.Object info, android.graphics.Rect outBounds) {
            android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.getBoundsInParent(info, outBounds);
        }

        public void getBoundsInScreen(java.lang.Object info, android.graphics.Rect outBounds) {
            android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.getBoundsInScreen(info, outBounds);
        }

        public java.lang.Object getChild(java.lang.Object info, int index) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.getChild(info, index);
        }

        public int getChildCount(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.getChildCount(info);
        }

        public java.lang.CharSequence getClassName(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.getClassName(info);
        }

        public java.lang.CharSequence getContentDescription(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.getContentDescription(info);
        }

        public java.lang.CharSequence getPackageName(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.getPackageName(info);
        }

        public java.lang.Object getParent(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.getParent(info);
        }

        public java.lang.CharSequence getText(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.getText(info);
        }

        public int getWindowId(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.getWindowId(info);
        }

        public boolean isCheckable(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.isCheckable(info);
        }

        public boolean isChecked(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.isChecked(info);
        }

        public boolean isClickable(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.isClickable(info);
        }

        public boolean isEnabled(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.isEnabled(info);
        }

        public boolean isFocusable(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.isFocusable(info);
        }

        public boolean isFocused(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.isFocused(info);
        }

        public boolean isLongClickable(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.isLongClickable(info);
        }

        public boolean isPassword(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.isPassword(info);
        }

        public boolean isScrollable(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.isScrollable(info);
        }

        public boolean isSelected(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.isSelected(info);
        }

        public boolean performAction(java.lang.Object info, int action) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.performAction(info, action);
        }

        public void setBoundsInParent(java.lang.Object info, android.graphics.Rect bounds) {
            android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.setBoundsInParent(info, bounds);
        }

        public void setBoundsInScreen(java.lang.Object info, android.graphics.Rect bounds) {
            android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.setBoundsInScreen(info, bounds);
        }

        public void setCheckable(java.lang.Object info, boolean checkable) {
            android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.setCheckable(info, checkable);
        }

        public void setChecked(java.lang.Object info, boolean checked) {
            android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.setChecked(info, checked);
        }

        public void setClassName(java.lang.Object info, java.lang.CharSequence className) {
            android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.setClassName(info, className);
        }

        public void setClickable(java.lang.Object info, boolean clickable) {
            android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.setClickable(info, clickable);
        }

        public void setContentDescription(java.lang.Object info, java.lang.CharSequence contentDescription) {
            android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.setContentDescription(info, contentDescription);
        }

        public void setEnabled(java.lang.Object info, boolean enabled) {
            android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.setEnabled(info, enabled);
        }

        public void setFocusable(java.lang.Object info, boolean focusable) {
            android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.setFocusable(info, focusable);
        }

        public void setFocused(java.lang.Object info, boolean focused) {
            android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.setFocused(info, focused);
        }

        public void setLongClickable(java.lang.Object info, boolean longClickable) {
            android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.setLongClickable(info, longClickable);
        }

        public void setPackageName(java.lang.Object info, java.lang.CharSequence packageName) {
            android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.setPackageName(info, packageName);
        }

        public void setParent(java.lang.Object info, android.view.View parent) {
            android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.setParent(info, parent);
        }

        public void setPassword(java.lang.Object info, boolean password) {
            android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.setPassword(info, password);
        }

        public void setScrollable(java.lang.Object info, boolean scrollable) {
            android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.setScrollable(info, scrollable);
        }

        public void setSelected(java.lang.Object info, boolean selected) {
            android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.setSelected(info, selected);
        }

        public void setSource(java.lang.Object info, android.view.View source) {
            android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.setSource(info, source);
        }

        public void setText(java.lang.Object info, java.lang.CharSequence text) {
            android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.setText(info, text);
        }

        public void recycle(java.lang.Object info) {
            android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.recycle(info);
        }
    }

    interface AccessibilityNodeInfoImpl {
        void addAction(java.lang.Object obj, int i);

        void addAction(java.lang.Object obj, java.lang.Object obj2);

        void addChild(java.lang.Object obj, android.view.View view);

        void addChild(java.lang.Object obj, android.view.View view, int i);

        boolean canOpenPopup(java.lang.Object obj);

        java.util.List<java.lang.Object> findAccessibilityNodeInfosByText(java.lang.Object obj, java.lang.String str);

        java.util.List<java.lang.Object> findAccessibilityNodeInfosByViewId(java.lang.Object obj, java.lang.String str);

        java.lang.Object findFocus(java.lang.Object obj, int i);

        java.lang.Object focusSearch(java.lang.Object obj, int i);

        int getAccessibilityActionId(java.lang.Object obj);

        java.lang.CharSequence getAccessibilityActionLabel(java.lang.Object obj);

        java.lang.Object getActionContextClick();

        java.util.List<java.lang.Object> getActionList(java.lang.Object obj);

        java.lang.Object getActionScrollDown();

        java.lang.Object getActionScrollLeft();

        java.lang.Object getActionScrollRight();

        java.lang.Object getActionScrollToPosition();

        java.lang.Object getActionScrollUp();

        java.lang.Object getActionSetProgress();

        java.lang.Object getActionShowOnScreen();

        int getActions(java.lang.Object obj);

        void getBoundsInParent(java.lang.Object obj, android.graphics.Rect rect);

        void getBoundsInScreen(java.lang.Object obj, android.graphics.Rect rect);

        java.lang.Object getChild(java.lang.Object obj, int i);

        int getChildCount(java.lang.Object obj);

        java.lang.CharSequence getClassName(java.lang.Object obj);

        java.lang.Object getCollectionInfo(java.lang.Object obj);

        int getCollectionInfoColumnCount(java.lang.Object obj);

        int getCollectionInfoRowCount(java.lang.Object obj);

        int getCollectionInfoSelectionMode(java.lang.Object obj);

        int getCollectionItemColumnIndex(java.lang.Object obj);

        int getCollectionItemColumnSpan(java.lang.Object obj);

        java.lang.Object getCollectionItemInfo(java.lang.Object obj);

        int getCollectionItemRowIndex(java.lang.Object obj);

        int getCollectionItemRowSpan(java.lang.Object obj);

        java.lang.CharSequence getContentDescription(java.lang.Object obj);

        int getDrawingOrder(java.lang.Object obj);

        java.lang.CharSequence getError(java.lang.Object obj);

        android.os.Bundle getExtras(java.lang.Object obj);

        int getInputType(java.lang.Object obj);

        java.lang.Object getLabelFor(java.lang.Object obj);

        java.lang.Object getLabeledBy(java.lang.Object obj);

        int getLiveRegion(java.lang.Object obj);

        int getMaxTextLength(java.lang.Object obj);

        int getMovementGranularities(java.lang.Object obj);

        java.lang.CharSequence getPackageName(java.lang.Object obj);

        java.lang.Object getParent(java.lang.Object obj);

        java.lang.Object getRangeInfo(java.lang.Object obj);

        java.lang.CharSequence getRoleDescription(java.lang.Object obj);

        java.lang.CharSequence getText(java.lang.Object obj);

        int getTextSelectionEnd(java.lang.Object obj);

        int getTextSelectionStart(java.lang.Object obj);

        java.lang.Object getTraversalAfter(java.lang.Object obj);

        java.lang.Object getTraversalBefore(java.lang.Object obj);

        java.lang.String getViewIdResourceName(java.lang.Object obj);

        java.lang.Object getWindow(java.lang.Object obj);

        int getWindowId(java.lang.Object obj);

        boolean isAccessibilityFocused(java.lang.Object obj);

        boolean isCheckable(java.lang.Object obj);

        boolean isChecked(java.lang.Object obj);

        boolean isClickable(java.lang.Object obj);

        boolean isCollectionInfoHierarchical(java.lang.Object obj);

        boolean isCollectionItemHeading(java.lang.Object obj);

        boolean isCollectionItemSelected(java.lang.Object obj);

        boolean isContentInvalid(java.lang.Object obj);

        boolean isContextClickable(java.lang.Object obj);

        boolean isDismissable(java.lang.Object obj);

        boolean isEditable(java.lang.Object obj);

        boolean isEnabled(java.lang.Object obj);

        boolean isFocusable(java.lang.Object obj);

        boolean isFocused(java.lang.Object obj);

        boolean isImportantForAccessibility(java.lang.Object obj);

        boolean isLongClickable(java.lang.Object obj);

        boolean isMultiLine(java.lang.Object obj);

        boolean isPassword(java.lang.Object obj);

        boolean isScrollable(java.lang.Object obj);

        boolean isSelected(java.lang.Object obj);

        boolean isVisibleToUser(java.lang.Object obj);

        java.lang.Object newAccessibilityAction(int i, java.lang.CharSequence charSequence);

        java.lang.Object obtain();

        java.lang.Object obtain(android.view.View view);

        java.lang.Object obtain(android.view.View view, int i);

        java.lang.Object obtain(java.lang.Object obj);

        java.lang.Object obtainCollectionInfo(int i, int i2, boolean z);

        java.lang.Object obtainCollectionInfo(int i, int i2, boolean z, int i3);

        java.lang.Object obtainCollectionItemInfo(int i, int i2, int i3, int i4, boolean z);

        java.lang.Object obtainCollectionItemInfo(int i, int i2, int i3, int i4, boolean z, boolean z2);

        java.lang.Object obtainRangeInfo(int i, float f, float f2, float f3);

        boolean performAction(java.lang.Object obj, int i);

        boolean performAction(java.lang.Object obj, int i, android.os.Bundle bundle);

        void recycle(java.lang.Object obj);

        boolean refresh(java.lang.Object obj);

        boolean removeAction(java.lang.Object obj, java.lang.Object obj2);

        boolean removeChild(java.lang.Object obj, android.view.View view);

        boolean removeChild(java.lang.Object obj, android.view.View view, int i);

        void setAccessibilityFocused(java.lang.Object obj, boolean z);

        void setBoundsInParent(java.lang.Object obj, android.graphics.Rect rect);

        void setBoundsInScreen(java.lang.Object obj, android.graphics.Rect rect);

        void setCanOpenPopup(java.lang.Object obj, boolean z);

        void setCheckable(java.lang.Object obj, boolean z);

        void setChecked(java.lang.Object obj, boolean z);

        void setClassName(java.lang.Object obj, java.lang.CharSequence charSequence);

        void setClickable(java.lang.Object obj, boolean z);

        void setCollectionInfo(java.lang.Object obj, java.lang.Object obj2);

        void setCollectionItemInfo(java.lang.Object obj, java.lang.Object obj2);

        void setContentDescription(java.lang.Object obj, java.lang.CharSequence charSequence);

        void setContentInvalid(java.lang.Object obj, boolean z);

        void setContextClickable(java.lang.Object obj, boolean z);

        void setDismissable(java.lang.Object obj, boolean z);

        void setDrawingOrder(java.lang.Object obj, int i);

        void setEditable(java.lang.Object obj, boolean z);

        void setEnabled(java.lang.Object obj, boolean z);

        void setError(java.lang.Object obj, java.lang.CharSequence charSequence);

        void setFocusable(java.lang.Object obj, boolean z);

        void setFocused(java.lang.Object obj, boolean z);

        void setImportantForAccessibility(java.lang.Object obj, boolean z);

        void setInputType(java.lang.Object obj, int i);

        void setLabelFor(java.lang.Object obj, android.view.View view);

        void setLabelFor(java.lang.Object obj, android.view.View view, int i);

        void setLabeledBy(java.lang.Object obj, android.view.View view);

        void setLabeledBy(java.lang.Object obj, android.view.View view, int i);

        void setLiveRegion(java.lang.Object obj, int i);

        void setLongClickable(java.lang.Object obj, boolean z);

        void setMaxTextLength(java.lang.Object obj, int i);

        void setMovementGranularities(java.lang.Object obj, int i);

        void setMultiLine(java.lang.Object obj, boolean z);

        void setPackageName(java.lang.Object obj, java.lang.CharSequence charSequence);

        void setParent(java.lang.Object obj, android.view.View view);

        void setParent(java.lang.Object obj, android.view.View view, int i);

        void setPassword(java.lang.Object obj, boolean z);

        void setRangeInfo(java.lang.Object obj, java.lang.Object obj2);

        void setRoleDescription(java.lang.Object obj, java.lang.CharSequence charSequence);

        void setScrollable(java.lang.Object obj, boolean z);

        void setSelected(java.lang.Object obj, boolean z);

        void setSource(java.lang.Object obj, android.view.View view);

        void setSource(java.lang.Object obj, android.view.View view, int i);

        void setText(java.lang.Object obj, java.lang.CharSequence charSequence);

        void setTextSelection(java.lang.Object obj, int i, int i2);

        void setTraversalAfter(java.lang.Object obj, android.view.View view);

        void setTraversalAfter(java.lang.Object obj, android.view.View view, int i);

        void setTraversalBefore(java.lang.Object obj, android.view.View view);

        void setTraversalBefore(java.lang.Object obj, android.view.View view, int i);

        void setViewIdResourceName(java.lang.Object obj, java.lang.String str);

        void setVisibleToUser(java.lang.Object obj, boolean z);
    }

    static class AccessibilityNodeInfoJellybeanImpl extends android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityNodeInfoIcsImpl {
        AccessibilityNodeInfoJellybeanImpl() {
        }

        public java.lang.Object obtain(android.view.View root, int virtualDescendantId) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatJellyBean.obtain(root, virtualDescendantId);
        }

        public java.lang.Object findFocus(java.lang.Object info, int focus) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatJellyBean.findFocus(info, focus);
        }

        public java.lang.Object focusSearch(java.lang.Object info, int direction) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatJellyBean.focusSearch(info, direction);
        }

        public void addChild(java.lang.Object info, android.view.View child, int virtualDescendantId) {
            android.support.v4.view.accessibility.AccessibilityNodeInfoCompatJellyBean.addChild(info, child, virtualDescendantId);
        }

        public void setSource(java.lang.Object info, android.view.View root, int virtualDescendantId) {
            android.support.v4.view.accessibility.AccessibilityNodeInfoCompatJellyBean.setSource(info, root, virtualDescendantId);
        }

        public boolean isVisibleToUser(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatJellyBean.isVisibleToUser(info);
        }

        public void setVisibleToUser(java.lang.Object info, boolean visibleToUser) {
            android.support.v4.view.accessibility.AccessibilityNodeInfoCompatJellyBean.setVisibleToUser(info, visibleToUser);
        }

        public boolean isAccessibilityFocused(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatJellyBean.isAccessibilityFocused(info);
        }

        public void setAccessibilityFocused(java.lang.Object info, boolean focused) {
            android.support.v4.view.accessibility.AccessibilityNodeInfoCompatJellyBean.setAccesibilityFocused(info, focused);
        }

        public boolean performAction(java.lang.Object info, int action, android.os.Bundle arguments) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatJellyBean.performAction(info, action, arguments);
        }

        public void setMovementGranularities(java.lang.Object info, int granularities) {
            android.support.v4.view.accessibility.AccessibilityNodeInfoCompatJellyBean.setMovementGranularities(info, granularities);
        }

        public int getMovementGranularities(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatJellyBean.getMovementGranularities(info);
        }

        public void setParent(java.lang.Object info, android.view.View root, int virtualDescendantId) {
            android.support.v4.view.accessibility.AccessibilityNodeInfoCompatJellyBean.setParent(info, root, virtualDescendantId);
        }
    }

    static class AccessibilityNodeInfoJellybeanMr1Impl extends android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityNodeInfoJellybeanImpl {
        AccessibilityNodeInfoJellybeanMr1Impl() {
        }

        public void setLabelFor(java.lang.Object info, android.view.View labeled) {
            android.support.v4.view.accessibility.AccessibilityNodeInfoCompatJellybeanMr1.setLabelFor(info, labeled);
        }

        public void setLabelFor(java.lang.Object info, android.view.View root, int virtualDescendantId) {
            android.support.v4.view.accessibility.AccessibilityNodeInfoCompatJellybeanMr1.setLabelFor(info, root, virtualDescendantId);
        }

        public java.lang.Object getLabelFor(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatJellybeanMr1.getLabelFor(info);
        }

        public void setLabeledBy(java.lang.Object info, android.view.View labeled) {
            android.support.v4.view.accessibility.AccessibilityNodeInfoCompatJellybeanMr1.setLabeledBy(info, labeled);
        }

        public void setLabeledBy(java.lang.Object info, android.view.View root, int virtualDescendantId) {
            android.support.v4.view.accessibility.AccessibilityNodeInfoCompatJellybeanMr1.setLabeledBy(info, root, virtualDescendantId);
        }

        public java.lang.Object getLabeledBy(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatJellybeanMr1.getLabeledBy(info);
        }
    }

    static class AccessibilityNodeInfoJellybeanMr2Impl extends android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityNodeInfoJellybeanMr1Impl {
        AccessibilityNodeInfoJellybeanMr2Impl() {
        }

        public java.lang.String getViewIdResourceName(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatJellybeanMr2.getViewIdResourceName(info);
        }

        public void setViewIdResourceName(java.lang.Object info, java.lang.String viewId) {
            android.support.v4.view.accessibility.AccessibilityNodeInfoCompatJellybeanMr2.setViewIdResourceName(info, viewId);
        }

        public java.util.List<java.lang.Object> findAccessibilityNodeInfosByViewId(java.lang.Object info, java.lang.String viewId) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatJellybeanMr2.findAccessibilityNodeInfosByViewId(info, viewId);
        }

        public void setTextSelection(java.lang.Object info, int start, int end) {
            android.support.v4.view.accessibility.AccessibilityNodeInfoCompatJellybeanMr2.setTextSelection(info, start, end);
        }

        public int getTextSelectionStart(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatJellybeanMr2.getTextSelectionStart(info);
        }

        public int getTextSelectionEnd(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatJellybeanMr2.getTextSelectionEnd(info);
        }

        public boolean isEditable(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatJellybeanMr2.isEditable(info);
        }

        public void setEditable(java.lang.Object info, boolean editable) {
            android.support.v4.view.accessibility.AccessibilityNodeInfoCompatJellybeanMr2.setEditable(info, editable);
        }

        public boolean refresh(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatJellybeanMr2.refresh(info);
        }
    }

    static class AccessibilityNodeInfoKitKatImpl extends android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityNodeInfoJellybeanMr2Impl {
        AccessibilityNodeInfoKitKatImpl() {
        }

        public int getLiveRegion(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatKitKat.getLiveRegion(info);
        }

        public void setLiveRegion(java.lang.Object info, int mode) {
            android.support.v4.view.accessibility.AccessibilityNodeInfoCompatKitKat.setLiveRegion(info, mode);
        }

        public java.lang.Object getCollectionInfo(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatKitKat.getCollectionInfo(info);
        }

        public void setCollectionInfo(java.lang.Object info, java.lang.Object collectionInfo) {
            android.support.v4.view.accessibility.AccessibilityNodeInfoCompatKitKat.setCollectionInfo(info, collectionInfo);
        }

        public java.lang.Object obtainCollectionInfo(int rowCount, int columnCount, boolean hierarchical, int selectionMode) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatKitKat.obtainCollectionInfo(rowCount, columnCount, hierarchical, selectionMode);
        }

        public java.lang.Object obtainCollectionInfo(int rowCount, int columnCount, boolean hierarchical) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatKitKat.obtainCollectionInfo(rowCount, columnCount, hierarchical);
        }

        public java.lang.Object obtainCollectionItemInfo(int rowIndex, int rowSpan, int columnIndex, int columnSpan, boolean heading, boolean selected) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatKitKat.obtainCollectionItemInfo(rowIndex, rowSpan, columnIndex, columnSpan, heading);
        }

        public java.lang.Object obtainCollectionItemInfo(int rowIndex, int rowSpan, int columnIndex, int columnSpan, boolean heading) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatKitKat.obtainCollectionItemInfo(rowIndex, rowSpan, columnIndex, columnSpan, heading);
        }

        public int getCollectionInfoColumnCount(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatKitKat.CollectionInfo.getColumnCount(info);
        }

        public int getCollectionInfoRowCount(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatKitKat.CollectionInfo.getRowCount(info);
        }

        public boolean isCollectionInfoHierarchical(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatKitKat.CollectionInfo.isHierarchical(info);
        }

        public java.lang.Object getCollectionItemInfo(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatKitKat.getCollectionItemInfo(info);
        }

        public java.lang.Object getRangeInfo(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatKitKat.getRangeInfo(info);
        }

        public void setRangeInfo(java.lang.Object info, java.lang.Object rangeInfo) {
            android.support.v4.view.accessibility.AccessibilityNodeInfoCompatKitKat.setRangeInfo(info, rangeInfo);
        }

        public int getCollectionItemColumnIndex(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatKitKat.CollectionItemInfo.getColumnIndex(info);
        }

        public int getCollectionItemColumnSpan(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatKitKat.CollectionItemInfo.getColumnSpan(info);
        }

        public int getCollectionItemRowIndex(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatKitKat.CollectionItemInfo.getRowIndex(info);
        }

        public int getCollectionItemRowSpan(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatKitKat.CollectionItemInfo.getRowSpan(info);
        }

        public boolean isCollectionItemHeading(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatKitKat.CollectionItemInfo.isHeading(info);
        }

        public void setCollectionItemInfo(java.lang.Object info, java.lang.Object collectionItemInfo) {
            android.support.v4.view.accessibility.AccessibilityNodeInfoCompatKitKat.setCollectionItemInfo(info, collectionItemInfo);
        }

        public java.lang.Object obtainRangeInfo(int type, float min, float max, float current) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatKitKat.obtainRangeInfo(type, min, max, current);
        }

        public void setContentInvalid(java.lang.Object info, boolean contentInvalid) {
            android.support.v4.view.accessibility.AccessibilityNodeInfoCompatKitKat.setContentInvalid(info, contentInvalid);
        }

        public boolean isContentInvalid(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatKitKat.isContentInvalid(info);
        }

        public boolean canOpenPopup(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatKitKat.canOpenPopup(info);
        }

        public void setCanOpenPopup(java.lang.Object info, boolean opensPopup) {
            android.support.v4.view.accessibility.AccessibilityNodeInfoCompatKitKat.setCanOpenPopup(info, opensPopup);
        }

        public android.os.Bundle getExtras(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatKitKat.getExtras(info);
        }

        public int getInputType(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatKitKat.getInputType(info);
        }

        public void setInputType(java.lang.Object info, int inputType) {
            android.support.v4.view.accessibility.AccessibilityNodeInfoCompatKitKat.setInputType(info, inputType);
        }

        public boolean isDismissable(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatKitKat.isDismissable(info);
        }

        public void setDismissable(java.lang.Object info, boolean dismissable) {
            android.support.v4.view.accessibility.AccessibilityNodeInfoCompatKitKat.setDismissable(info, dismissable);
        }

        public boolean isMultiLine(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatKitKat.isMultiLine(info);
        }

        public void setMultiLine(java.lang.Object info, boolean multiLine) {
            android.support.v4.view.accessibility.AccessibilityNodeInfoCompatKitKat.setMultiLine(info, multiLine);
        }

        public java.lang.CharSequence getRoleDescription(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatKitKat.getRoleDescription(info);
        }

        public void setRoleDescription(java.lang.Object info, java.lang.CharSequence roleDescription) {
            android.support.v4.view.accessibility.AccessibilityNodeInfoCompatKitKat.setRoleDescription(info, roleDescription);
        }
    }

    static class AccessibilityNodeInfoStubImpl implements android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityNodeInfoImpl {
        AccessibilityNodeInfoStubImpl() {
        }

        public java.lang.Object newAccessibilityAction(int actionId, java.lang.CharSequence label) {
            return null;
        }

        public java.lang.Object obtain() {
            return null;
        }

        public java.lang.Object obtain(android.view.View source) {
            return null;
        }

        public java.lang.Object obtain(android.view.View root, int virtualDescendantId) {
            return null;
        }

        public java.lang.Object obtain(java.lang.Object info) {
            return null;
        }

        public void addAction(java.lang.Object info, int action) {
        }

        public void addAction(java.lang.Object info, java.lang.Object action) {
        }

        public boolean removeAction(java.lang.Object info, java.lang.Object action) {
            return false;
        }

        public int getAccessibilityActionId(java.lang.Object action) {
            return 0;
        }

        public java.lang.CharSequence getAccessibilityActionLabel(java.lang.Object action) {
            return null;
        }

        public void addChild(java.lang.Object info, android.view.View child) {
        }

        public void addChild(java.lang.Object info, android.view.View child, int virtualDescendantId) {
        }

        public boolean removeChild(java.lang.Object info, android.view.View child) {
            return false;
        }

        public boolean removeChild(java.lang.Object info, android.view.View root, int virtualDescendantId) {
            return false;
        }

        public java.util.List<java.lang.Object> findAccessibilityNodeInfosByText(java.lang.Object info, java.lang.String text) {
            return java.util.Collections.emptyList();
        }

        public int getActions(java.lang.Object info) {
            return 0;
        }

        public void getBoundsInParent(java.lang.Object info, android.graphics.Rect outBounds) {
        }

        public void getBoundsInScreen(java.lang.Object info, android.graphics.Rect outBounds) {
        }

        public java.lang.Object getChild(java.lang.Object info, int index) {
            return null;
        }

        public int getChildCount(java.lang.Object info) {
            return 0;
        }

        public java.lang.CharSequence getClassName(java.lang.Object info) {
            return null;
        }

        public java.lang.CharSequence getContentDescription(java.lang.Object info) {
            return null;
        }

        public java.lang.CharSequence getPackageName(java.lang.Object info) {
            return null;
        }

        public java.lang.Object getParent(java.lang.Object info) {
            return null;
        }

        public java.lang.CharSequence getText(java.lang.Object info) {
            return null;
        }

        public int getWindowId(java.lang.Object info) {
            return 0;
        }

        public boolean isCheckable(java.lang.Object info) {
            return false;
        }

        public boolean isChecked(java.lang.Object info) {
            return false;
        }

        public boolean isClickable(java.lang.Object info) {
            return false;
        }

        public boolean isEnabled(java.lang.Object info) {
            return false;
        }

        public boolean isFocusable(java.lang.Object info) {
            return false;
        }

        public boolean isFocused(java.lang.Object info) {
            return false;
        }

        public boolean isVisibleToUser(java.lang.Object info) {
            return false;
        }

        public boolean isAccessibilityFocused(java.lang.Object info) {
            return false;
        }

        public boolean isLongClickable(java.lang.Object info) {
            return false;
        }

        public boolean isPassword(java.lang.Object info) {
            return false;
        }

        public boolean isScrollable(java.lang.Object info) {
            return false;
        }

        public boolean isSelected(java.lang.Object info) {
            return false;
        }

        public boolean performAction(java.lang.Object info, int action) {
            return false;
        }

        public boolean performAction(java.lang.Object info, int action, android.os.Bundle arguments) {
            return false;
        }

        public void setMovementGranularities(java.lang.Object info, int granularities) {
        }

        public int getMovementGranularities(java.lang.Object info) {
            return 0;
        }

        public void setBoundsInParent(java.lang.Object info, android.graphics.Rect bounds) {
        }

        public void setBoundsInScreen(java.lang.Object info, android.graphics.Rect bounds) {
        }

        public void setCheckable(java.lang.Object info, boolean checkable) {
        }

        public void setChecked(java.lang.Object info, boolean checked) {
        }

        public void setClassName(java.lang.Object info, java.lang.CharSequence className) {
        }

        public void setClickable(java.lang.Object info, boolean clickable) {
        }

        public void setContentDescription(java.lang.Object info, java.lang.CharSequence contentDescription) {
        }

        public void setEnabled(java.lang.Object info, boolean enabled) {
        }

        public void setFocusable(java.lang.Object info, boolean focusable) {
        }

        public void setFocused(java.lang.Object info, boolean focused) {
        }

        public void setVisibleToUser(java.lang.Object info, boolean visibleToUser) {
        }

        public void setAccessibilityFocused(java.lang.Object info, boolean focused) {
        }

        public void setLongClickable(java.lang.Object info, boolean longClickable) {
        }

        public void setPackageName(java.lang.Object info, java.lang.CharSequence packageName) {
        }

        public void setParent(java.lang.Object info, android.view.View parent) {
        }

        public void setPassword(java.lang.Object info, boolean password) {
        }

        public void setScrollable(java.lang.Object info, boolean scrollable) {
        }

        public void setSelected(java.lang.Object info, boolean selected) {
        }

        public void setSource(java.lang.Object info, android.view.View source) {
        }

        public void setSource(java.lang.Object info, android.view.View root, int virtualDescendantId) {
        }

        public java.lang.Object findFocus(java.lang.Object info, int focus) {
            return null;
        }

        public java.lang.Object focusSearch(java.lang.Object info, int direction) {
            return null;
        }

        public void setText(java.lang.Object info, java.lang.CharSequence text) {
        }

        public void recycle(java.lang.Object info) {
        }

        public void setParent(java.lang.Object info, android.view.View root, int virtualDescendantId) {
        }

        public java.lang.String getViewIdResourceName(java.lang.Object info) {
            return null;
        }

        public void setViewIdResourceName(java.lang.Object info, java.lang.String viewId) {
        }

        public int getLiveRegion(java.lang.Object info) {
            return 0;
        }

        public void setLiveRegion(java.lang.Object info, int mode) {
        }

        public java.lang.Object getCollectionInfo(java.lang.Object info) {
            return null;
        }

        public void setCollectionInfo(java.lang.Object info, java.lang.Object collectionInfo) {
        }

        public java.lang.Object getCollectionItemInfo(java.lang.Object info) {
            return null;
        }

        public void setCollectionItemInfo(java.lang.Object info, java.lang.Object collectionItemInfo) {
        }

        public java.lang.Object getRangeInfo(java.lang.Object info) {
            return null;
        }

        public void setRangeInfo(java.lang.Object info, java.lang.Object rangeInfo) {
        }

        public java.util.List<java.lang.Object> getActionList(java.lang.Object info) {
            return null;
        }

        public java.lang.Object obtainCollectionInfo(int rowCount, int columnCount, boolean hierarchical, int selectionMode) {
            return null;
        }

        public java.lang.Object obtainCollectionInfo(int rowCount, int columnCount, boolean hierarchical) {
            return null;
        }

        public int getCollectionInfoColumnCount(java.lang.Object info) {
            return 0;
        }

        public int getCollectionInfoRowCount(java.lang.Object info) {
            return 0;
        }

        public boolean isCollectionInfoHierarchical(java.lang.Object info) {
            return false;
        }

        public java.lang.Object obtainCollectionItemInfo(int rowIndex, int rowSpan, int columnIndex, int columnSpan, boolean heading, boolean selected) {
            return null;
        }

        public java.lang.Object obtainCollectionItemInfo(int rowIndex, int rowSpan, int columnIndex, int columnSpan, boolean heading) {
            return null;
        }

        public int getCollectionItemColumnIndex(java.lang.Object info) {
            return 0;
        }

        public int getCollectionItemColumnSpan(java.lang.Object info) {
            return 0;
        }

        public int getCollectionItemRowIndex(java.lang.Object info) {
            return 0;
        }

        public int getCollectionItemRowSpan(java.lang.Object info) {
            return 0;
        }

        public boolean isCollectionItemHeading(java.lang.Object info) {
            return false;
        }

        public boolean isCollectionItemSelected(java.lang.Object info) {
            return false;
        }

        public java.lang.Object obtainRangeInfo(int type, float min, float max, float current) {
            return null;
        }

        public java.lang.Object getTraversalBefore(java.lang.Object info) {
            return null;
        }

        public void setTraversalBefore(java.lang.Object info, android.view.View view) {
        }

        public void setTraversalBefore(java.lang.Object info, android.view.View root, int virtualDescendantId) {
        }

        public java.lang.Object getTraversalAfter(java.lang.Object info) {
            return null;
        }

        public void setTraversalAfter(java.lang.Object info, android.view.View view) {
        }

        public void setTraversalAfter(java.lang.Object info, android.view.View root, int virtualDescendantId) {
        }

        public void setContentInvalid(java.lang.Object info, boolean contentInvalid) {
        }

        public boolean isContentInvalid(java.lang.Object info) {
            return false;
        }

        public void setError(java.lang.Object info, java.lang.CharSequence error) {
        }

        public java.lang.CharSequence getError(java.lang.Object info) {
            return null;
        }

        public void setLabelFor(java.lang.Object info, android.view.View labeled) {
        }

        public void setLabelFor(java.lang.Object info, android.view.View root, int virtualDescendantId) {
        }

        public java.lang.Object getLabelFor(java.lang.Object info) {
            return null;
        }

        public void setLabeledBy(java.lang.Object info, android.view.View labeled) {
        }

        public void setLabeledBy(java.lang.Object info, android.view.View root, int virtualDescendantId) {
        }

        public java.lang.Object getLabeledBy(java.lang.Object info) {
            return null;
        }

        public boolean canOpenPopup(java.lang.Object info) {
            return false;
        }

        public void setCanOpenPopup(java.lang.Object info, boolean opensPopup) {
        }

        public java.util.List<java.lang.Object> findAccessibilityNodeInfosByViewId(java.lang.Object info, java.lang.String viewId) {
            return java.util.Collections.emptyList();
        }

        public android.os.Bundle getExtras(java.lang.Object info) {
            return new android.os.Bundle();
        }

        public int getInputType(java.lang.Object info) {
            return 0;
        }

        public void setInputType(java.lang.Object info, int inputType) {
        }

        public void setMaxTextLength(java.lang.Object info, int max) {
        }

        public int getMaxTextLength(java.lang.Object info) {
            return -1;
        }

        public void setTextSelection(java.lang.Object info, int start, int end) {
        }

        public int getTextSelectionStart(java.lang.Object info) {
            return -1;
        }

        public int getTextSelectionEnd(java.lang.Object info) {
            return -1;
        }

        public java.lang.Object getWindow(java.lang.Object info) {
            return null;
        }

        public boolean isDismissable(java.lang.Object info) {
            return false;
        }

        public void setDismissable(java.lang.Object info, boolean dismissable) {
        }

        public boolean isEditable(java.lang.Object info) {
            return false;
        }

        public void setEditable(java.lang.Object info, boolean editable) {
        }

        public boolean isMultiLine(java.lang.Object info) {
            return false;
        }

        public void setMultiLine(java.lang.Object info, boolean multiLine) {
        }

        public boolean refresh(java.lang.Object info) {
            return false;
        }

        public java.lang.CharSequence getRoleDescription(java.lang.Object info) {
            return null;
        }

        public void setRoleDescription(java.lang.Object info, java.lang.CharSequence roleDescription) {
        }

        public java.lang.Object getActionScrollToPosition() {
            return null;
        }

        public java.lang.Object getActionSetProgress() {
            return null;
        }

        public boolean isContextClickable(java.lang.Object info) {
            return false;
        }

        public void setContextClickable(java.lang.Object info, boolean contextClickable) {
        }

        public java.lang.Object getActionShowOnScreen() {
            return null;
        }

        public java.lang.Object getActionScrollUp() {
            return null;
        }

        public java.lang.Object getActionScrollDown() {
            return null;
        }

        public java.lang.Object getActionScrollLeft() {
            return null;
        }

        public java.lang.Object getActionScrollRight() {
            return null;
        }

        public java.lang.Object getActionContextClick() {
            return null;
        }

        public int getCollectionInfoSelectionMode(java.lang.Object info) {
            return 0;
        }

        public int getDrawingOrder(java.lang.Object info) {
            return 0;
        }

        public void setDrawingOrder(java.lang.Object info, int drawingOrderInParent) {
        }

        public boolean isImportantForAccessibility(java.lang.Object info) {
            return true;
        }

        public void setImportantForAccessibility(java.lang.Object info, boolean importantForAccessibility) {
        }
    }

    public static class CollectionInfoCompat {
        public static final int SELECTION_MODE_MULTIPLE = 2;
        public static final int SELECTION_MODE_NONE = 0;
        public static final int SELECTION_MODE_SINGLE = 1;
        final java.lang.Object mInfo;

        public static android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.CollectionInfoCompat obtain(int rowCount, int columnCount, boolean hierarchical, int selectionMode) {
            return new android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.CollectionInfoCompat(android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.IMPL.obtainCollectionInfo(rowCount, columnCount, hierarchical, selectionMode));
        }

        public static android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.CollectionInfoCompat obtain(int rowCount, int columnCount, boolean hierarchical) {
            return new android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.CollectionInfoCompat(android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.IMPL.obtainCollectionInfo(rowCount, columnCount, hierarchical));
        }

        CollectionInfoCompat(java.lang.Object info) {
            this.mInfo = info;
        }

        public int getColumnCount() {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.IMPL.getCollectionInfoColumnCount(this.mInfo);
        }

        public int getRowCount() {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.IMPL.getCollectionInfoRowCount(this.mInfo);
        }

        public boolean isHierarchical() {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.IMPL.isCollectionInfoHierarchical(this.mInfo);
        }

        public int getSelectionMode() {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.IMPL.getCollectionInfoSelectionMode(this.mInfo);
        }
    }

    public static class CollectionItemInfoCompat {
        final java.lang.Object mInfo;

        public static android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.CollectionItemInfoCompat obtain(int rowIndex, int rowSpan, int columnIndex, int columnSpan, boolean heading, boolean selected) {
            return new android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.CollectionItemInfoCompat(android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.IMPL.obtainCollectionItemInfo(rowIndex, rowSpan, columnIndex, columnSpan, heading, selected));
        }

        public static android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.CollectionItemInfoCompat obtain(int rowIndex, int rowSpan, int columnIndex, int columnSpan, boolean heading) {
            return new android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.CollectionItemInfoCompat(android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.IMPL.obtainCollectionItemInfo(rowIndex, rowSpan, columnIndex, columnSpan, heading));
        }

        CollectionItemInfoCompat(java.lang.Object info) {
            this.mInfo = info;
        }

        public int getColumnIndex() {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.IMPL.getCollectionItemColumnIndex(this.mInfo);
        }

        public int getColumnSpan() {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.IMPL.getCollectionItemColumnSpan(this.mInfo);
        }

        public int getRowIndex() {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.IMPL.getCollectionItemRowIndex(this.mInfo);
        }

        public int getRowSpan() {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.IMPL.getCollectionItemRowSpan(this.mInfo);
        }

        public boolean isHeading() {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.IMPL.isCollectionItemHeading(this.mInfo);
        }

        public boolean isSelected() {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.IMPL.isCollectionItemSelected(this.mInfo);
        }
    }

    public static class RangeInfoCompat {
        public static final int RANGE_TYPE_FLOAT = 1;
        public static final int RANGE_TYPE_INT = 0;
        public static final int RANGE_TYPE_PERCENT = 2;
        final java.lang.Object mInfo;

        public static android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.RangeInfoCompat obtain(int type, float min, float max, float current) {
            return new android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.RangeInfoCompat(android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.IMPL.obtainRangeInfo(type, min, max, current));
        }

        RangeInfoCompat(java.lang.Object info) {
            this.mInfo = info;
        }

        public float getCurrent() {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatKitKat.RangeInfo.getCurrent(this.mInfo);
        }

        public float getMax() {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatKitKat.RangeInfo.getMax(this.mInfo);
        }

        public float getMin() {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatKitKat.RangeInfo.getMin(this.mInfo);
        }

        public int getType() {
            return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatKitKat.RangeInfo.getType(this.mInfo);
        }
    }

    static {
        if (android.os.Build.VERSION.SDK_INT >= 24) {
            IMPL = new android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityNodeInfoApi24Impl();
        } else if (android.os.Build.VERSION.SDK_INT >= 23) {
            IMPL = new android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityNodeInfoApi23Impl();
        } else if (android.os.Build.VERSION.SDK_INT >= 22) {
            IMPL = new android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityNodeInfoApi22Impl();
        } else if (android.os.Build.VERSION.SDK_INT >= 21) {
            IMPL = new android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityNodeInfoApi21Impl();
        } else if (android.os.Build.VERSION.SDK_INT >= 19) {
            IMPL = new android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityNodeInfoKitKatImpl();
        } else if (android.os.Build.VERSION.SDK_INT >= 18) {
            IMPL = new android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityNodeInfoJellybeanMr2Impl();
        } else if (android.os.Build.VERSION.SDK_INT >= 17) {
            IMPL = new android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityNodeInfoJellybeanMr1Impl();
        } else if (android.os.Build.VERSION.SDK_INT >= 16) {
            IMPL = new android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityNodeInfoJellybeanImpl();
        } else if (android.os.Build.VERSION.SDK_INT >= 14) {
            IMPL = new android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityNodeInfoIcsImpl();
        } else {
            IMPL = new android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityNodeInfoStubImpl();
        }
    }

    static android.support.v4.view.accessibility.AccessibilityNodeInfoCompat wrapNonNullInstance(java.lang.Object object) {
        if (object != null) {
            return new android.support.v4.view.accessibility.AccessibilityNodeInfoCompat(object);
        }
        return null;
    }

    public AccessibilityNodeInfoCompat(java.lang.Object info) {
        this.mInfo = info;
    }

    public java.lang.Object getInfo() {
        return this.mInfo;
    }

    public static android.support.v4.view.accessibility.AccessibilityNodeInfoCompat obtain(android.view.View source) {
        return wrapNonNullInstance(IMPL.obtain(source));
    }

    public static android.support.v4.view.accessibility.AccessibilityNodeInfoCompat obtain(android.view.View root, int virtualDescendantId) {
        return wrapNonNullInstance(IMPL.obtain(root, virtualDescendantId));
    }

    public static android.support.v4.view.accessibility.AccessibilityNodeInfoCompat obtain() {
        return wrapNonNullInstance(IMPL.obtain());
    }

    public static android.support.v4.view.accessibility.AccessibilityNodeInfoCompat obtain(android.support.v4.view.accessibility.AccessibilityNodeInfoCompat info) {
        return wrapNonNullInstance(IMPL.obtain(info.mInfo));
    }

    public void setSource(android.view.View source) {
        IMPL.setSource(this.mInfo, source);
    }

    public void setSource(android.view.View root, int virtualDescendantId) {
        IMPL.setSource(this.mInfo, root, virtualDescendantId);
    }

    public android.support.v4.view.accessibility.AccessibilityNodeInfoCompat findFocus(int focus) {
        return wrapNonNullInstance(IMPL.findFocus(this.mInfo, focus));
    }

    public android.support.v4.view.accessibility.AccessibilityNodeInfoCompat focusSearch(int direction) {
        return wrapNonNullInstance(IMPL.focusSearch(this.mInfo, direction));
    }

    public int getWindowId() {
        return IMPL.getWindowId(this.mInfo);
    }

    public int getChildCount() {
        return IMPL.getChildCount(this.mInfo);
    }

    public android.support.v4.view.accessibility.AccessibilityNodeInfoCompat getChild(int index) {
        return wrapNonNullInstance(IMPL.getChild(this.mInfo, index));
    }

    public void addChild(android.view.View child) {
        IMPL.addChild(this.mInfo, child);
    }

    public void addChild(android.view.View root, int virtualDescendantId) {
        IMPL.addChild(this.mInfo, root, virtualDescendantId);
    }

    public boolean removeChild(android.view.View child) {
        return IMPL.removeChild(this.mInfo, child);
    }

    public boolean removeChild(android.view.View root, int virtualDescendantId) {
        return IMPL.removeChild(this.mInfo, root, virtualDescendantId);
    }

    public int getActions() {
        return IMPL.getActions(this.mInfo);
    }

    public void addAction(int action) {
        IMPL.addAction(this.mInfo, action);
    }

    public void addAction(android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat action) {
        IMPL.addAction(this.mInfo, action.mAction);
    }

    public boolean removeAction(android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat action) {
        return IMPL.removeAction(this.mInfo, action.mAction);
    }

    public boolean performAction(int action) {
        return IMPL.performAction(this.mInfo, action);
    }

    public boolean performAction(int action, android.os.Bundle arguments) {
        return IMPL.performAction(this.mInfo, action, arguments);
    }

    public void setMovementGranularities(int granularities) {
        IMPL.setMovementGranularities(this.mInfo, granularities);
    }

    public int getMovementGranularities() {
        return IMPL.getMovementGranularities(this.mInfo);
    }

    public java.util.List<android.support.v4.view.accessibility.AccessibilityNodeInfoCompat> findAccessibilityNodeInfosByText(java.lang.String text) {
        java.util.List<android.support.v4.view.accessibility.AccessibilityNodeInfoCompat> result = new java.util.ArrayList<>();
        java.util.List<java.lang.Object> infos = IMPL.findAccessibilityNodeInfosByText(this.mInfo, text);
        int infoCount = infos.size();
        for (int i = 0; i < infoCount; i++) {
            result.add(new android.support.v4.view.accessibility.AccessibilityNodeInfoCompat(infos.get(i)));
        }
        return result;
    }

    public android.support.v4.view.accessibility.AccessibilityNodeInfoCompat getParent() {
        return wrapNonNullInstance(IMPL.getParent(this.mInfo));
    }

    public void setParent(android.view.View parent) {
        IMPL.setParent(this.mInfo, parent);
    }

    public void setParent(android.view.View root, int virtualDescendantId) {
        this.mParentVirtualDescendantId = virtualDescendantId;
        IMPL.setParent(this.mInfo, root, virtualDescendantId);
    }

    public void getBoundsInParent(android.graphics.Rect outBounds) {
        IMPL.getBoundsInParent(this.mInfo, outBounds);
    }

    public void setBoundsInParent(android.graphics.Rect bounds) {
        IMPL.setBoundsInParent(this.mInfo, bounds);
    }

    public void getBoundsInScreen(android.graphics.Rect outBounds) {
        IMPL.getBoundsInScreen(this.mInfo, outBounds);
    }

    public void setBoundsInScreen(android.graphics.Rect bounds) {
        IMPL.setBoundsInScreen(this.mInfo, bounds);
    }

    public boolean isCheckable() {
        return IMPL.isCheckable(this.mInfo);
    }

    public void setCheckable(boolean checkable) {
        IMPL.setCheckable(this.mInfo, checkable);
    }

    public boolean isChecked() {
        return IMPL.isChecked(this.mInfo);
    }

    public void setChecked(boolean checked) {
        IMPL.setChecked(this.mInfo, checked);
    }

    public boolean isFocusable() {
        return IMPL.isFocusable(this.mInfo);
    }

    public void setFocusable(boolean focusable) {
        IMPL.setFocusable(this.mInfo, focusable);
    }

    public boolean isFocused() {
        return IMPL.isFocused(this.mInfo);
    }

    public void setFocused(boolean focused) {
        IMPL.setFocused(this.mInfo, focused);
    }

    public boolean isVisibleToUser() {
        return IMPL.isVisibleToUser(this.mInfo);
    }

    public void setVisibleToUser(boolean visibleToUser) {
        IMPL.setVisibleToUser(this.mInfo, visibleToUser);
    }

    public boolean isAccessibilityFocused() {
        return IMPL.isAccessibilityFocused(this.mInfo);
    }

    public void setAccessibilityFocused(boolean focused) {
        IMPL.setAccessibilityFocused(this.mInfo, focused);
    }

    public boolean isSelected() {
        return IMPL.isSelected(this.mInfo);
    }

    public void setSelected(boolean selected) {
        IMPL.setSelected(this.mInfo, selected);
    }

    public boolean isClickable() {
        return IMPL.isClickable(this.mInfo);
    }

    public void setClickable(boolean clickable) {
        IMPL.setClickable(this.mInfo, clickable);
    }

    public boolean isLongClickable() {
        return IMPL.isLongClickable(this.mInfo);
    }

    public void setLongClickable(boolean longClickable) {
        IMPL.setLongClickable(this.mInfo, longClickable);
    }

    public boolean isEnabled() {
        return IMPL.isEnabled(this.mInfo);
    }

    public void setEnabled(boolean enabled) {
        IMPL.setEnabled(this.mInfo, enabled);
    }

    public boolean isPassword() {
        return IMPL.isPassword(this.mInfo);
    }

    public void setPassword(boolean password) {
        IMPL.setPassword(this.mInfo, password);
    }

    public boolean isScrollable() {
        return IMPL.isScrollable(this.mInfo);
    }

    public void setScrollable(boolean scrollable) {
        IMPL.setScrollable(this.mInfo, scrollable);
    }

    public boolean isImportantForAccessibility() {
        return IMPL.isImportantForAccessibility(this.mInfo);
    }

    public void setImportantForAccessibility(boolean important) {
        IMPL.setImportantForAccessibility(this.mInfo, important);
    }

    public java.lang.CharSequence getPackageName() {
        return IMPL.getPackageName(this.mInfo);
    }

    public void setPackageName(java.lang.CharSequence packageName) {
        IMPL.setPackageName(this.mInfo, packageName);
    }

    public java.lang.CharSequence getClassName() {
        return IMPL.getClassName(this.mInfo);
    }

    public void setClassName(java.lang.CharSequence className) {
        IMPL.setClassName(this.mInfo, className);
    }

    public java.lang.CharSequence getText() {
        return IMPL.getText(this.mInfo);
    }

    public void setText(java.lang.CharSequence text) {
        IMPL.setText(this.mInfo, text);
    }

    public java.lang.CharSequence getContentDescription() {
        return IMPL.getContentDescription(this.mInfo);
    }

    public void setContentDescription(java.lang.CharSequence contentDescription) {
        IMPL.setContentDescription(this.mInfo, contentDescription);
    }

    public void recycle() {
        IMPL.recycle(this.mInfo);
    }

    public void setViewIdResourceName(java.lang.String viewId) {
        IMPL.setViewIdResourceName(this.mInfo, viewId);
    }

    public java.lang.String getViewIdResourceName() {
        return IMPL.getViewIdResourceName(this.mInfo);
    }

    public int getLiveRegion() {
        return IMPL.getLiveRegion(this.mInfo);
    }

    public void setLiveRegion(int mode) {
        IMPL.setLiveRegion(this.mInfo, mode);
    }

    public int getDrawingOrder() {
        return IMPL.getDrawingOrder(this.mInfo);
    }

    public void setDrawingOrder(int drawingOrderInParent) {
        IMPL.setDrawingOrder(this.mInfo, drawingOrderInParent);
    }

    public android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.CollectionInfoCompat getCollectionInfo() {
        java.lang.Object info = IMPL.getCollectionInfo(this.mInfo);
        if (info == null) {
            return null;
        }
        return new android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.CollectionInfoCompat(info);
    }

    public void setCollectionInfo(java.lang.Object collectionInfo) {
        IMPL.setCollectionInfo(this.mInfo, ((android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.CollectionInfoCompat) collectionInfo).mInfo);
    }

    public void setCollectionItemInfo(java.lang.Object collectionItemInfo) {
        IMPL.setCollectionItemInfo(this.mInfo, ((android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.CollectionItemInfoCompat) collectionItemInfo).mInfo);
    }

    public android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.CollectionItemInfoCompat getCollectionItemInfo() {
        java.lang.Object info = IMPL.getCollectionItemInfo(this.mInfo);
        if (info == null) {
            return null;
        }
        return new android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.CollectionItemInfoCompat(info);
    }

    public android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.RangeInfoCompat getRangeInfo() {
        java.lang.Object info = IMPL.getRangeInfo(this.mInfo);
        if (info == null) {
            return null;
        }
        return new android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.RangeInfoCompat(info);
    }

    public void setRangeInfo(android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.RangeInfoCompat rangeInfo) {
        IMPL.setRangeInfo(this.mInfo, rangeInfo.mInfo);
    }

    public java.util.List<android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat> getActionList() {
        java.util.List<java.lang.Object> actions = IMPL.getActionList(this.mInfo);
        if (actions == null) {
            return java.util.Collections.emptyList();
        }
        java.util.List<android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat> result = new java.util.ArrayList<>();
        int actionCount = actions.size();
        for (int i = 0; i < actionCount; i++) {
            result.add(new android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat(actions.get(i)));
        }
        return result;
    }

    public void setContentInvalid(boolean contentInvalid) {
        IMPL.setContentInvalid(this.mInfo, contentInvalid);
    }

    public boolean isContentInvalid() {
        return IMPL.isContentInvalid(this.mInfo);
    }

    public boolean isContextClickable() {
        return IMPL.isContextClickable(this.mInfo);
    }

    public void setContextClickable(boolean contextClickable) {
        IMPL.setContextClickable(this.mInfo, contextClickable);
    }

    public void setError(java.lang.CharSequence error) {
        IMPL.setError(this.mInfo, error);
    }

    public java.lang.CharSequence getError() {
        return IMPL.getError(this.mInfo);
    }

    public void setLabelFor(android.view.View labeled) {
        IMPL.setLabelFor(this.mInfo, labeled);
    }

    public void setLabelFor(android.view.View root, int virtualDescendantId) {
        IMPL.setLabelFor(this.mInfo, root, virtualDescendantId);
    }

    public android.support.v4.view.accessibility.AccessibilityNodeInfoCompat getLabelFor() {
        return wrapNonNullInstance(IMPL.getLabelFor(this.mInfo));
    }

    public void setLabeledBy(android.view.View label) {
        IMPL.setLabeledBy(this.mInfo, label);
    }

    public void setLabeledBy(android.view.View root, int virtualDescendantId) {
        IMPL.setLabeledBy(this.mInfo, root, virtualDescendantId);
    }

    public android.support.v4.view.accessibility.AccessibilityNodeInfoCompat getLabeledBy() {
        return wrapNonNullInstance(IMPL.getLabeledBy(this.mInfo));
    }

    public boolean canOpenPopup() {
        return IMPL.canOpenPopup(this.mInfo);
    }

    public void setCanOpenPopup(boolean opensPopup) {
        IMPL.setCanOpenPopup(this.mInfo, opensPopup);
    }

    public java.util.List<android.support.v4.view.accessibility.AccessibilityNodeInfoCompat> findAccessibilityNodeInfosByViewId(java.lang.String viewId) {
        java.util.List<java.lang.Object> nodes = IMPL.findAccessibilityNodeInfosByViewId(this.mInfo, viewId);
        if (nodes == null) {
            return java.util.Collections.emptyList();
        }
        java.util.List<android.support.v4.view.accessibility.AccessibilityNodeInfoCompat> result = new java.util.ArrayList<>();
        for (java.lang.Object node : nodes) {
            result.add(new android.support.v4.view.accessibility.AccessibilityNodeInfoCompat(node));
        }
        return result;
    }

    public android.os.Bundle getExtras() {
        return IMPL.getExtras(this.mInfo);
    }

    public int getInputType() {
        return IMPL.getInputType(this.mInfo);
    }

    public void setInputType(int inputType) {
        IMPL.setInputType(this.mInfo, inputType);
    }

    public void setMaxTextLength(int max) {
        IMPL.setMaxTextLength(this.mInfo, max);
    }

    public int getMaxTextLength() {
        return IMPL.getMaxTextLength(this.mInfo);
    }

    public void setTextSelection(int start, int end) {
        IMPL.setTextSelection(this.mInfo, start, end);
    }

    public int getTextSelectionStart() {
        return IMPL.getTextSelectionStart(this.mInfo);
    }

    public int getTextSelectionEnd() {
        return IMPL.getTextSelectionEnd(this.mInfo);
    }

    public android.support.v4.view.accessibility.AccessibilityNodeInfoCompat getTraversalBefore() {
        return wrapNonNullInstance(IMPL.getTraversalBefore(this.mInfo));
    }

    public void setTraversalBefore(android.view.View view) {
        IMPL.setTraversalBefore(this.mInfo, view);
    }

    public void setTraversalBefore(android.view.View root, int virtualDescendantId) {
        IMPL.setTraversalBefore(this.mInfo, root, virtualDescendantId);
    }

    public android.support.v4.view.accessibility.AccessibilityNodeInfoCompat getTraversalAfter() {
        return wrapNonNullInstance(IMPL.getTraversalAfter(this.mInfo));
    }

    public void setTraversalAfter(android.view.View view) {
        IMPL.setTraversalAfter(this.mInfo, view);
    }

    public void setTraversalAfter(android.view.View root, int virtualDescendantId) {
        IMPL.setTraversalAfter(this.mInfo, root, virtualDescendantId);
    }

    public android.support.v4.view.accessibility.AccessibilityWindowInfoCompat getWindow() {
        return android.support.v4.view.accessibility.AccessibilityWindowInfoCompat.wrapNonNullInstance(IMPL.getWindow(this.mInfo));
    }

    public boolean isDismissable() {
        return IMPL.isDismissable(this.mInfo);
    }

    public void setDismissable(boolean dismissable) {
        IMPL.setDismissable(this.mInfo, dismissable);
    }

    public boolean isEditable() {
        return IMPL.isEditable(this.mInfo);
    }

    public void setEditable(boolean editable) {
        IMPL.setEditable(this.mInfo, editable);
    }

    public boolean isMultiLine() {
        return IMPL.isMultiLine(this.mInfo);
    }

    public void setMultiLine(boolean multiLine) {
        IMPL.setMultiLine(this.mInfo, multiLine);
    }

    public boolean refresh() {
        return IMPL.refresh(this.mInfo);
    }

    @android.support.annotation.Nullable
    public java.lang.CharSequence getRoleDescription() {
        return IMPL.getRoleDescription(this.mInfo);
    }

    public void setRoleDescription(@android.support.annotation.Nullable java.lang.CharSequence roleDescription) {
        IMPL.setRoleDescription(this.mInfo, roleDescription);
    }

    public int hashCode() {
        if (this.mInfo == null) {
            return 0;
        }
        return this.mInfo.hashCode();
    }

    public boolean equals(java.lang.Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        android.support.v4.view.accessibility.AccessibilityNodeInfoCompat other = (android.support.v4.view.accessibility.AccessibilityNodeInfoCompat) obj;
        if (this.mInfo == null) {
            if (other.mInfo != null) {
                return false;
            }
            return true;
        } else if (!this.mInfo.equals(other.mInfo)) {
            return false;
        } else {
            return true;
        }
    }

    public java.lang.String toString() {
        java.lang.StringBuilder builder = new java.lang.StringBuilder();
        builder.append(super.toString());
        android.graphics.Rect bounds = new android.graphics.Rect();
        getBoundsInParent(bounds);
        builder.append("; boundsInParent: " + bounds);
        getBoundsInScreen(bounds);
        builder.append("; boundsInScreen: " + bounds);
        builder.append("; packageName: ").append(getPackageName());
        builder.append("; className: ").append(getClassName());
        builder.append("; text: ").append(getText());
        builder.append("; contentDescription: ").append(getContentDescription());
        builder.append("; viewId: ").append(getViewIdResourceName());
        builder.append("; checkable: ").append(isCheckable());
        builder.append("; checked: ").append(isChecked());
        builder.append("; focusable: ").append(isFocusable());
        builder.append("; focused: ").append(isFocused());
        builder.append("; selected: ").append(isSelected());
        builder.append("; clickable: ").append(isClickable());
        builder.append("; longClickable: ").append(isLongClickable());
        builder.append("; enabled: ").append(isEnabled());
        builder.append("; password: ").append(isPassword());
        builder.append("; scrollable: " + isScrollable());
        builder.append("; [");
        int actionBits = getActions();
        while (actionBits != 0) {
            int action = 1 << java.lang.Integer.numberOfTrailingZeros(actionBits);
            actionBits &= action ^ -1;
            builder.append(getActionSymbolicName(action));
            if (actionBits != 0) {
                builder.append(", ");
            }
        }
        builder.append("]");
        return builder.toString();
    }

    private static java.lang.String getActionSymbolicName(int action) {
        switch (action) {
            case 1:
                return "ACTION_FOCUS";
            case 2:
                return "ACTION_CLEAR_FOCUS";
            case 4:
                return "ACTION_SELECT";
            case 8:
                return "ACTION_CLEAR_SELECTION";
            case 16:
                return "ACTION_CLICK";
            case 32:
                return "ACTION_LONG_CLICK";
            case 64:
                return "ACTION_ACCESSIBILITY_FOCUS";
            case 128:
                return "ACTION_CLEAR_ACCESSIBILITY_FOCUS";
            case 256:
                return "ACTION_NEXT_AT_MOVEMENT_GRANULARITY";
            case 512:
                return "ACTION_PREVIOUS_AT_MOVEMENT_GRANULARITY";
            case 1024:
                return "ACTION_NEXT_HTML_ELEMENT";
            case 2048:
                return "ACTION_PREVIOUS_HTML_ELEMENT";
            case 4096:
                return "ACTION_SCROLL_FORWARD";
            case 8192:
                return "ACTION_SCROLL_BACKWARD";
            case 16384:
                return "ACTION_COPY";
            case 32768:
                return "ACTION_PASTE";
            case 65536:
                return "ACTION_CUT";
            case 131072:
                return "ACTION_SET_SELECTION";
            default:
                return "ACTION_UNKNOWN";
        }
    }
}
