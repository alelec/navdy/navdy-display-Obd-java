package android.support.v4.view.accessibility;

@android.annotation.TargetApi(22)
@android.support.annotation.RequiresApi(22)
class AccessibilityNodeInfoCompatApi22 {
    AccessibilityNodeInfoCompatApi22() {
    }

    public static java.lang.Object getTraversalBefore(java.lang.Object info) {
        return ((android.view.accessibility.AccessibilityNodeInfo) info).getTraversalBefore();
    }

    public static void setTraversalBefore(java.lang.Object info, android.view.View view) {
        ((android.view.accessibility.AccessibilityNodeInfo) info).setTraversalBefore(view);
    }

    public static void setTraversalBefore(java.lang.Object info, android.view.View root, int virtualDescendantId) {
        ((android.view.accessibility.AccessibilityNodeInfo) info).setTraversalBefore(root, virtualDescendantId);
    }

    public static java.lang.Object getTraversalAfter(java.lang.Object info) {
        return ((android.view.accessibility.AccessibilityNodeInfo) info).getTraversalAfter();
    }

    public static void setTraversalAfter(java.lang.Object info, android.view.View view) {
        ((android.view.accessibility.AccessibilityNodeInfo) info).setTraversalAfter(view);
    }

    public static void setTraversalAfter(java.lang.Object info, android.view.View root, int virtualDescendantId) {
        ((android.view.accessibility.AccessibilityNodeInfo) info).setTraversalAfter(root, virtualDescendantId);
    }
}
