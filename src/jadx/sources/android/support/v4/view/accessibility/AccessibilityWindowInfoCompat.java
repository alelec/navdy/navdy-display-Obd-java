package android.support.v4.view.accessibility;

public class AccessibilityWindowInfoCompat {
    private static final android.support.v4.view.accessibility.AccessibilityWindowInfoCompat.AccessibilityWindowInfoImpl IMPL;
    public static final int TYPE_ACCESSIBILITY_OVERLAY = 4;
    public static final int TYPE_APPLICATION = 1;
    public static final int TYPE_INPUT_METHOD = 2;
    public static final int TYPE_SPLIT_SCREEN_DIVIDER = 5;
    public static final int TYPE_SYSTEM = 3;
    private static final int UNDEFINED = -1;
    private java.lang.Object mInfo;

    private static class AccessibilityWindowInfoApi21Impl extends android.support.v4.view.accessibility.AccessibilityWindowInfoCompat.AccessibilityWindowInfoStubImpl {
        AccessibilityWindowInfoApi21Impl() {
        }

        public java.lang.Object obtain() {
            return android.support.v4.view.accessibility.AccessibilityWindowInfoCompatApi21.obtain();
        }

        public java.lang.Object obtain(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityWindowInfoCompatApi21.obtain(info);
        }

        public int getType(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityWindowInfoCompatApi21.getType(info);
        }

        public int getLayer(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityWindowInfoCompatApi21.getLayer(info);
        }

        public java.lang.Object getRoot(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityWindowInfoCompatApi21.getRoot(info);
        }

        public java.lang.Object getParent(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityWindowInfoCompatApi21.getParent(info);
        }

        public int getId(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityWindowInfoCompatApi21.getId(info);
        }

        public void getBoundsInScreen(java.lang.Object info, android.graphics.Rect outBounds) {
            android.support.v4.view.accessibility.AccessibilityWindowInfoCompatApi21.getBoundsInScreen(info, outBounds);
        }

        public boolean isActive(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityWindowInfoCompatApi21.isActive(info);
        }

        public boolean isFocused(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityWindowInfoCompatApi21.isFocused(info);
        }

        public boolean isAccessibilityFocused(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityWindowInfoCompatApi21.isAccessibilityFocused(info);
        }

        public int getChildCount(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityWindowInfoCompatApi21.getChildCount(info);
        }

        public java.lang.Object getChild(java.lang.Object info, int index) {
            return android.support.v4.view.accessibility.AccessibilityWindowInfoCompatApi21.getChild(info, index);
        }

        public void recycle(java.lang.Object info) {
            android.support.v4.view.accessibility.AccessibilityWindowInfoCompatApi21.recycle(info);
        }
    }

    private static class AccessibilityWindowInfoApi24Impl extends android.support.v4.view.accessibility.AccessibilityWindowInfoCompat.AccessibilityWindowInfoApi21Impl {
        AccessibilityWindowInfoApi24Impl() {
        }

        public java.lang.CharSequence getTitle(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityWindowInfoCompatApi24.getTitle(info);
        }

        public java.lang.Object getAnchor(java.lang.Object info) {
            return android.support.v4.view.accessibility.AccessibilityWindowInfoCompatApi24.getAnchor(info);
        }
    }

    private interface AccessibilityWindowInfoImpl {
        java.lang.Object getAnchor(java.lang.Object obj);

        void getBoundsInScreen(java.lang.Object obj, android.graphics.Rect rect);

        java.lang.Object getChild(java.lang.Object obj, int i);

        int getChildCount(java.lang.Object obj);

        int getId(java.lang.Object obj);

        int getLayer(java.lang.Object obj);

        java.lang.Object getParent(java.lang.Object obj);

        java.lang.Object getRoot(java.lang.Object obj);

        java.lang.CharSequence getTitle(java.lang.Object obj);

        int getType(java.lang.Object obj);

        boolean isAccessibilityFocused(java.lang.Object obj);

        boolean isActive(java.lang.Object obj);

        boolean isFocused(java.lang.Object obj);

        java.lang.Object obtain();

        java.lang.Object obtain(java.lang.Object obj);

        void recycle(java.lang.Object obj);
    }

    private static class AccessibilityWindowInfoStubImpl implements android.support.v4.view.accessibility.AccessibilityWindowInfoCompat.AccessibilityWindowInfoImpl {
        AccessibilityWindowInfoStubImpl() {
        }

        public java.lang.Object obtain() {
            return null;
        }

        public java.lang.Object obtain(java.lang.Object info) {
            return null;
        }

        public int getType(java.lang.Object info) {
            return -1;
        }

        public int getLayer(java.lang.Object info) {
            return -1;
        }

        public java.lang.Object getRoot(java.lang.Object info) {
            return null;
        }

        public java.lang.Object getParent(java.lang.Object info) {
            return null;
        }

        public int getId(java.lang.Object info) {
            return -1;
        }

        public void getBoundsInScreen(java.lang.Object info, android.graphics.Rect outBounds) {
        }

        public boolean isActive(java.lang.Object info) {
            return true;
        }

        public boolean isFocused(java.lang.Object info) {
            return true;
        }

        public boolean isAccessibilityFocused(java.lang.Object info) {
            return true;
        }

        public int getChildCount(java.lang.Object info) {
            return 0;
        }

        public java.lang.Object getChild(java.lang.Object info, int index) {
            return null;
        }

        public void recycle(java.lang.Object info) {
        }

        public java.lang.CharSequence getTitle(java.lang.Object info) {
            return null;
        }

        public java.lang.Object getAnchor(java.lang.Object info) {
            return null;
        }
    }

    static {
        if (android.os.Build.VERSION.SDK_INT >= 24) {
            IMPL = new android.support.v4.view.accessibility.AccessibilityWindowInfoCompat.AccessibilityWindowInfoApi24Impl();
        } else if (android.os.Build.VERSION.SDK_INT >= 21) {
            IMPL = new android.support.v4.view.accessibility.AccessibilityWindowInfoCompat.AccessibilityWindowInfoApi21Impl();
        } else {
            IMPL = new android.support.v4.view.accessibility.AccessibilityWindowInfoCompat.AccessibilityWindowInfoStubImpl();
        }
    }

    static android.support.v4.view.accessibility.AccessibilityWindowInfoCompat wrapNonNullInstance(java.lang.Object object) {
        if (object != null) {
            return new android.support.v4.view.accessibility.AccessibilityWindowInfoCompat(object);
        }
        return null;
    }

    private AccessibilityWindowInfoCompat(java.lang.Object info) {
        this.mInfo = info;
    }

    public int getType() {
        return IMPL.getType(this.mInfo);
    }

    public int getLayer() {
        return IMPL.getLayer(this.mInfo);
    }

    public android.support.v4.view.accessibility.AccessibilityNodeInfoCompat getRoot() {
        return android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.wrapNonNullInstance(IMPL.getRoot(this.mInfo));
    }

    public android.support.v4.view.accessibility.AccessibilityWindowInfoCompat getParent() {
        return wrapNonNullInstance(IMPL.getParent(this.mInfo));
    }

    public int getId() {
        return IMPL.getId(this.mInfo);
    }

    public void getBoundsInScreen(android.graphics.Rect outBounds) {
        IMPL.getBoundsInScreen(this.mInfo, outBounds);
    }

    public boolean isActive() {
        return IMPL.isActive(this.mInfo);
    }

    public boolean isFocused() {
        return IMPL.isFocused(this.mInfo);
    }

    public boolean isAccessibilityFocused() {
        return IMPL.isAccessibilityFocused(this.mInfo);
    }

    public int getChildCount() {
        return IMPL.getChildCount(this.mInfo);
    }

    public android.support.v4.view.accessibility.AccessibilityWindowInfoCompat getChild(int index) {
        return wrapNonNullInstance(IMPL.getChild(this.mInfo, index));
    }

    public java.lang.CharSequence getTitle() {
        return IMPL.getTitle(this.mInfo);
    }

    public android.support.v4.view.accessibility.AccessibilityNodeInfoCompat getAnchor() {
        return android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.wrapNonNullInstance(IMPL.getAnchor(this.mInfo));
    }

    public static android.support.v4.view.accessibility.AccessibilityWindowInfoCompat obtain() {
        return wrapNonNullInstance(IMPL.obtain());
    }

    public static android.support.v4.view.accessibility.AccessibilityWindowInfoCompat obtain(android.support.v4.view.accessibility.AccessibilityWindowInfoCompat info) {
        if (info == null) {
            return null;
        }
        return wrapNonNullInstance(IMPL.obtain(info.mInfo));
    }

    public void recycle() {
        IMPL.recycle(this.mInfo);
    }

    public int hashCode() {
        if (this.mInfo == null) {
            return 0;
        }
        return this.mInfo.hashCode();
    }

    public boolean equals(java.lang.Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        android.support.v4.view.accessibility.AccessibilityWindowInfoCompat other = (android.support.v4.view.accessibility.AccessibilityWindowInfoCompat) obj;
        if (this.mInfo == null) {
            if (other.mInfo != null) {
                return false;
            }
            return true;
        } else if (!this.mInfo.equals(other.mInfo)) {
            return false;
        } else {
            return true;
        }
    }

    public java.lang.String toString() {
        boolean z;
        boolean z2 = true;
        java.lang.StringBuilder builder = new java.lang.StringBuilder();
        android.graphics.Rect bounds = new android.graphics.Rect();
        getBoundsInScreen(bounds);
        builder.append("AccessibilityWindowInfo[");
        builder.append("id=").append(getId());
        builder.append(", type=").append(typeToString(getType()));
        builder.append(", layer=").append(getLayer());
        builder.append(", bounds=").append(bounds);
        builder.append(", focused=").append(isFocused());
        builder.append(", active=").append(isActive());
        java.lang.StringBuilder append = builder.append(", hasParent=");
        if (getParent() != null) {
            z = true;
        } else {
            z = false;
        }
        append.append(z);
        java.lang.StringBuilder append2 = builder.append(", hasChildren=");
        if (getChildCount() <= 0) {
            z2 = false;
        }
        append2.append(z2);
        builder.append(']');
        return builder.toString();
    }

    private static java.lang.String typeToString(int type) {
        switch (type) {
            case 1:
                return "TYPE_APPLICATION";
            case 2:
                return "TYPE_INPUT_METHOD";
            case 3:
                return "TYPE_SYSTEM";
            case 4:
                return "TYPE_ACCESSIBILITY_OVERLAY";
            default:
                return "<UNKNOWN>";
        }
    }
}
