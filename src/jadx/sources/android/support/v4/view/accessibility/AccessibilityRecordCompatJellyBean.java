package android.support.v4.view.accessibility;

@android.annotation.TargetApi(16)
@android.support.annotation.RequiresApi(16)
class AccessibilityRecordCompatJellyBean {
    AccessibilityRecordCompatJellyBean() {
    }

    public static void setSource(java.lang.Object record, android.view.View root, int virtualDescendantId) {
        ((android.view.accessibility.AccessibilityRecord) record).setSource(root, virtualDescendantId);
    }
}
