package android.support.v4.view.accessibility;

@android.annotation.TargetApi(16)
@android.support.annotation.RequiresApi(16)
class AccessibilityEventCompatJellyBean {
    AccessibilityEventCompatJellyBean() {
    }

    public static void setMovementGranularity(android.view.accessibility.AccessibilityEvent event, int granularity) {
        event.setMovementGranularity(granularity);
    }

    public static int getMovementGranularity(android.view.accessibility.AccessibilityEvent event) {
        return event.getMovementGranularity();
    }

    public static void setAction(android.view.accessibility.AccessibilityEvent event, int action) {
        event.setAction(action);
    }

    public static int getAction(android.view.accessibility.AccessibilityEvent event) {
        return event.getAction();
    }
}
