package android.support.v4.view.accessibility;

@android.annotation.TargetApi(18)
@android.support.annotation.RequiresApi(18)
class AccessibilityNodeInfoCompatJellybeanMr2 {
    AccessibilityNodeInfoCompatJellybeanMr2() {
    }

    public static void setViewIdResourceName(java.lang.Object info, java.lang.String viewId) {
        ((android.view.accessibility.AccessibilityNodeInfo) info).setViewIdResourceName(viewId);
    }

    public static java.lang.String getViewIdResourceName(java.lang.Object info) {
        return ((android.view.accessibility.AccessibilityNodeInfo) info).getViewIdResourceName();
    }

    public static java.util.List<java.lang.Object> findAccessibilityNodeInfosByViewId(java.lang.Object info, java.lang.String viewId) {
        return ((android.view.accessibility.AccessibilityNodeInfo) info).findAccessibilityNodeInfosByViewId(viewId);
    }

    public static void setTextSelection(java.lang.Object info, int start, int end) {
        ((android.view.accessibility.AccessibilityNodeInfo) info).setTextSelection(start, end);
    }

    public static int getTextSelectionStart(java.lang.Object info) {
        return ((android.view.accessibility.AccessibilityNodeInfo) info).getTextSelectionStart();
    }

    public static int getTextSelectionEnd(java.lang.Object info) {
        return ((android.view.accessibility.AccessibilityNodeInfo) info).getTextSelectionEnd();
    }

    public static boolean isEditable(java.lang.Object info) {
        return ((android.view.accessibility.AccessibilityNodeInfo) info).isEditable();
    }

    public static void setEditable(java.lang.Object info, boolean editable) {
        ((android.view.accessibility.AccessibilityNodeInfo) info).setEditable(editable);
    }

    public static boolean refresh(java.lang.Object info) {
        return ((android.view.accessibility.AccessibilityNodeInfo) info).refresh();
    }
}
