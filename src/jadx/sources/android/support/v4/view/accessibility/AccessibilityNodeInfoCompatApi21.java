package android.support.v4.view.accessibility;

@android.annotation.TargetApi(21)
@android.support.annotation.RequiresApi(21)
class AccessibilityNodeInfoCompatApi21 {

    static class CollectionInfo {
        CollectionInfo() {
        }

        public static int getSelectionMode(java.lang.Object info) {
            return ((android.view.accessibility.AccessibilityNodeInfo.CollectionInfo) info).getSelectionMode();
        }
    }

    static class CollectionItemInfo {
        CollectionItemInfo() {
        }

        public static boolean isSelected(java.lang.Object info) {
            return ((android.view.accessibility.AccessibilityNodeInfo.CollectionItemInfo) info).isSelected();
        }
    }

    AccessibilityNodeInfoCompatApi21() {
    }

    static java.util.List<java.lang.Object> getActionList(java.lang.Object info) {
        return ((android.view.accessibility.AccessibilityNodeInfo) info).getActionList();
    }

    static void addAction(java.lang.Object info, java.lang.Object action) {
        ((android.view.accessibility.AccessibilityNodeInfo) info).addAction((android.view.accessibility.AccessibilityNodeInfo.AccessibilityAction) action);
    }

    public static boolean removeAction(java.lang.Object info, java.lang.Object action) {
        return ((android.view.accessibility.AccessibilityNodeInfo) info).removeAction((android.view.accessibility.AccessibilityNodeInfo.AccessibilityAction) action);
    }

    public static java.lang.Object obtainCollectionInfo(int rowCount, int columnCount, boolean hierarchical, int selectionMode) {
        return android.view.accessibility.AccessibilityNodeInfo.CollectionInfo.obtain(rowCount, columnCount, hierarchical, selectionMode);
    }

    public static java.lang.Object obtainCollectionItemInfo(int rowIndex, int rowSpan, int columnIndex, int columnSpan, boolean heading, boolean selected) {
        return android.view.accessibility.AccessibilityNodeInfo.CollectionItemInfo.obtain(rowIndex, rowSpan, columnIndex, columnSpan, heading, selected);
    }

    public static java.lang.CharSequence getError(java.lang.Object info) {
        return ((android.view.accessibility.AccessibilityNodeInfo) info).getError();
    }

    public static void setError(java.lang.Object info, java.lang.CharSequence error) {
        ((android.view.accessibility.AccessibilityNodeInfo) info).setError(error);
    }

    public static void setMaxTextLength(java.lang.Object info, int max) {
        ((android.view.accessibility.AccessibilityNodeInfo) info).setMaxTextLength(max);
    }

    public static int getMaxTextLength(java.lang.Object info) {
        return ((android.view.accessibility.AccessibilityNodeInfo) info).getMaxTextLength();
    }

    public static java.lang.Object getWindow(java.lang.Object info) {
        return ((android.view.accessibility.AccessibilityNodeInfo) info).getWindow();
    }

    public static boolean removeChild(java.lang.Object info, android.view.View child) {
        return ((android.view.accessibility.AccessibilityNodeInfo) info).removeChild(child);
    }

    public static boolean removeChild(java.lang.Object info, android.view.View root, int virtualDescendantId) {
        return ((android.view.accessibility.AccessibilityNodeInfo) info).removeChild(root, virtualDescendantId);
    }

    static java.lang.Object newAccessibilityAction(int actionId, java.lang.CharSequence label) {
        return new android.view.accessibility.AccessibilityNodeInfo.AccessibilityAction(actionId, label);
    }

    static int getAccessibilityActionId(java.lang.Object action) {
        return ((android.view.accessibility.AccessibilityNodeInfo.AccessibilityAction) action).getId();
    }

    static java.lang.CharSequence getAccessibilityActionLabel(java.lang.Object action) {
        return ((android.view.accessibility.AccessibilityNodeInfo.AccessibilityAction) action).getLabel();
    }
}
