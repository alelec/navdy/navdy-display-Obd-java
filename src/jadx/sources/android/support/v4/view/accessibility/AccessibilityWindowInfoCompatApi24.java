package android.support.v4.view.accessibility;

@android.annotation.TargetApi(24)
@android.support.annotation.RequiresApi(24)
class AccessibilityWindowInfoCompatApi24 {
    AccessibilityWindowInfoCompatApi24() {
    }

    public static java.lang.CharSequence getTitle(java.lang.Object info) {
        return ((android.view.accessibility.AccessibilityWindowInfo) info).getTitle();
    }

    public static java.lang.Object getAnchor(java.lang.Object info) {
        return ((android.view.accessibility.AccessibilityWindowInfo) info).getAnchor();
    }
}
