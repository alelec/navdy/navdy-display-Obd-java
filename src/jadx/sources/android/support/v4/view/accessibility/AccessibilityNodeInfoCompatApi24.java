package android.support.v4.view.accessibility;

@android.annotation.TargetApi(24)
@android.support.annotation.RequiresApi(24)
class AccessibilityNodeInfoCompatApi24 {
    AccessibilityNodeInfoCompatApi24() {
    }

    public static java.lang.Object getActionSetProgress() {
        return android.view.accessibility.AccessibilityNodeInfo.AccessibilityAction.ACTION_SET_PROGRESS;
    }

    public static int getDrawingOrder(java.lang.Object info) {
        return ((android.view.accessibility.AccessibilityNodeInfo) info).getDrawingOrder();
    }

    public static void setDrawingOrder(java.lang.Object info, int drawingOrderInParent) {
        ((android.view.accessibility.AccessibilityNodeInfo) info).setDrawingOrder(drawingOrderInParent);
    }

    public static boolean isImportantForAccessibility(java.lang.Object info) {
        return ((android.view.accessibility.AccessibilityNodeInfo) info).isImportantForAccessibility();
    }

    public static void setImportantForAccessibility(java.lang.Object info, boolean importantForAccessibility) {
        ((android.view.accessibility.AccessibilityNodeInfo) info).setImportantForAccessibility(importantForAccessibility);
    }
}
