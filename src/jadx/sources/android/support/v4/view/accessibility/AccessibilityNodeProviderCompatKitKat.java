package android.support.v4.view.accessibility;

@android.annotation.TargetApi(19)
@android.support.annotation.RequiresApi(19)
class AccessibilityNodeProviderCompatKitKat {

    interface AccessibilityNodeInfoBridge {
        java.lang.Object createAccessibilityNodeInfo(int i);

        java.util.List<java.lang.Object> findAccessibilityNodeInfosByText(java.lang.String str, int i);

        java.lang.Object findFocus(int i);

        boolean performAction(int i, int i2, android.os.Bundle bundle);
    }

    AccessibilityNodeProviderCompatKitKat() {
    }

    public static java.lang.Object newAccessibilityNodeProviderBridge(final android.support.v4.view.accessibility.AccessibilityNodeProviderCompatKitKat.AccessibilityNodeInfoBridge bridge) {
        return new android.view.accessibility.AccessibilityNodeProvider() {
            public android.view.accessibility.AccessibilityNodeInfo createAccessibilityNodeInfo(int virtualViewId) {
                return (android.view.accessibility.AccessibilityNodeInfo) bridge.createAccessibilityNodeInfo(virtualViewId);
            }

            public java.util.List<android.view.accessibility.AccessibilityNodeInfo> findAccessibilityNodeInfosByText(java.lang.String text, int virtualViewId) {
                return bridge.findAccessibilityNodeInfosByText(text, virtualViewId);
            }

            public boolean performAction(int virtualViewId, int action, android.os.Bundle arguments) {
                return bridge.performAction(virtualViewId, action, arguments);
            }

            public android.view.accessibility.AccessibilityNodeInfo findFocus(int focus) {
                return (android.view.accessibility.AccessibilityNodeInfo) bridge.findFocus(focus);
            }
        };
    }
}
