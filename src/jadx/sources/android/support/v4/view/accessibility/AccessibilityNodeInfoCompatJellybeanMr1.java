package android.support.v4.view.accessibility;

@android.annotation.TargetApi(17)
@android.support.annotation.RequiresApi(17)
class AccessibilityNodeInfoCompatJellybeanMr1 {
    AccessibilityNodeInfoCompatJellybeanMr1() {
    }

    public static void setLabelFor(java.lang.Object info, android.view.View labeled) {
        ((android.view.accessibility.AccessibilityNodeInfo) info).setLabelFor(labeled);
    }

    public static void setLabelFor(java.lang.Object info, android.view.View root, int virtualDescendantId) {
        ((android.view.accessibility.AccessibilityNodeInfo) info).setLabelFor(root, virtualDescendantId);
    }

    public static java.lang.Object getLabelFor(java.lang.Object info) {
        return ((android.view.accessibility.AccessibilityNodeInfo) info).getLabelFor();
    }

    public static void setLabeledBy(java.lang.Object info, android.view.View labeled) {
        ((android.view.accessibility.AccessibilityNodeInfo) info).setLabeledBy(labeled);
    }

    public static void setLabeledBy(java.lang.Object info, android.view.View root, int virtualDescendantId) {
        ((android.view.accessibility.AccessibilityNodeInfo) info).setLabeledBy(root, virtualDescendantId);
    }

    public static java.lang.Object getLabeledBy(java.lang.Object info) {
        return ((android.view.accessibility.AccessibilityNodeInfo) info).getLabeledBy();
    }
}
