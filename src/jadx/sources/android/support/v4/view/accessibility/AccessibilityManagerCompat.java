package android.support.v4.view.accessibility;

public final class AccessibilityManagerCompat {
    private static final android.support.v4.view.accessibility.AccessibilityManagerCompat.AccessibilityManagerVersionImpl IMPL;

    static class AccessibilityManagerIcsImpl extends android.support.v4.view.accessibility.AccessibilityManagerCompat.AccessibilityManagerStubImpl {
        AccessibilityManagerIcsImpl() {
        }

        public android.support.v4.view.accessibility.AccessibilityManagerCompatIcs.AccessibilityStateChangeListenerWrapper newAccessibilityStateChangeListener(final android.support.v4.view.accessibility.AccessibilityManagerCompat.AccessibilityStateChangeListener listener) {
            return new android.support.v4.view.accessibility.AccessibilityManagerCompatIcs.AccessibilityStateChangeListenerWrapper(listener, new android.support.v4.view.accessibility.AccessibilityManagerCompatIcs.AccessibilityStateChangeListenerBridge() {
                public void onAccessibilityStateChanged(boolean enabled) {
                    listener.onAccessibilityStateChanged(enabled);
                }
            });
        }

        public boolean addAccessibilityStateChangeListener(android.view.accessibility.AccessibilityManager manager, android.support.v4.view.accessibility.AccessibilityManagerCompat.AccessibilityStateChangeListener listener) {
            return android.support.v4.view.accessibility.AccessibilityManagerCompatIcs.addAccessibilityStateChangeListener(manager, newAccessibilityStateChangeListener(listener));
        }

        public boolean removeAccessibilityStateChangeListener(android.view.accessibility.AccessibilityManager manager, android.support.v4.view.accessibility.AccessibilityManagerCompat.AccessibilityStateChangeListener listener) {
            return android.support.v4.view.accessibility.AccessibilityManagerCompatIcs.removeAccessibilityStateChangeListener(manager, newAccessibilityStateChangeListener(listener));
        }

        public java.util.List<android.accessibilityservice.AccessibilityServiceInfo> getEnabledAccessibilityServiceList(android.view.accessibility.AccessibilityManager manager, int feedbackTypeFlags) {
            return android.support.v4.view.accessibility.AccessibilityManagerCompatIcs.getEnabledAccessibilityServiceList(manager, feedbackTypeFlags);
        }

        public java.util.List<android.accessibilityservice.AccessibilityServiceInfo> getInstalledAccessibilityServiceList(android.view.accessibility.AccessibilityManager manager) {
            return android.support.v4.view.accessibility.AccessibilityManagerCompatIcs.getInstalledAccessibilityServiceList(manager);
        }

        public boolean isTouchExplorationEnabled(android.view.accessibility.AccessibilityManager manager) {
            return android.support.v4.view.accessibility.AccessibilityManagerCompatIcs.isTouchExplorationEnabled(manager);
        }
    }

    static class AccessibilityManagerKitKatImpl extends android.support.v4.view.accessibility.AccessibilityManagerCompat.AccessibilityManagerIcsImpl {
        AccessibilityManagerKitKatImpl() {
        }

        public android.support.v4.view.accessibility.AccessibilityManagerCompatKitKat.TouchExplorationStateChangeListenerWrapper newTouchExplorationStateChangeListener(final android.support.v4.view.accessibility.AccessibilityManagerCompat.TouchExplorationStateChangeListener listener) {
            return new android.support.v4.view.accessibility.AccessibilityManagerCompatKitKat.TouchExplorationStateChangeListenerWrapper(listener, new android.support.v4.view.accessibility.AccessibilityManagerCompatKitKat.TouchExplorationStateChangeListenerBridge() {
                public void onTouchExplorationStateChanged(boolean enabled) {
                    listener.onTouchExplorationStateChanged(enabled);
                }
            });
        }

        public boolean addTouchExplorationStateChangeListener(android.view.accessibility.AccessibilityManager manager, android.support.v4.view.accessibility.AccessibilityManagerCompat.TouchExplorationStateChangeListener listener) {
            return android.support.v4.view.accessibility.AccessibilityManagerCompatKitKat.addTouchExplorationStateChangeListener(manager, newTouchExplorationStateChangeListener(listener));
        }

        public boolean removeTouchExplorationStateChangeListener(android.view.accessibility.AccessibilityManager manager, android.support.v4.view.accessibility.AccessibilityManagerCompat.TouchExplorationStateChangeListener listener) {
            return android.support.v4.view.accessibility.AccessibilityManagerCompatKitKat.removeTouchExplorationStateChangeListener(manager, newTouchExplorationStateChangeListener(listener));
        }
    }

    static class AccessibilityManagerStubImpl implements android.support.v4.view.accessibility.AccessibilityManagerCompat.AccessibilityManagerVersionImpl {
        AccessibilityManagerStubImpl() {
        }

        public android.support.v4.view.accessibility.AccessibilityManagerCompatIcs.AccessibilityStateChangeListenerWrapper newAccessibilityStateChangeListener(android.support.v4.view.accessibility.AccessibilityManagerCompat.AccessibilityStateChangeListener listener) {
            return null;
        }

        public boolean addAccessibilityStateChangeListener(android.view.accessibility.AccessibilityManager manager, android.support.v4.view.accessibility.AccessibilityManagerCompat.AccessibilityStateChangeListener listener) {
            return false;
        }

        public boolean removeAccessibilityStateChangeListener(android.view.accessibility.AccessibilityManager manager, android.support.v4.view.accessibility.AccessibilityManagerCompat.AccessibilityStateChangeListener listener) {
            return false;
        }

        public java.util.List<android.accessibilityservice.AccessibilityServiceInfo> getEnabledAccessibilityServiceList(android.view.accessibility.AccessibilityManager manager, int feedbackTypeFlags) {
            return java.util.Collections.emptyList();
        }

        public java.util.List<android.accessibilityservice.AccessibilityServiceInfo> getInstalledAccessibilityServiceList(android.view.accessibility.AccessibilityManager manager) {
            return java.util.Collections.emptyList();
        }

        public boolean isTouchExplorationEnabled(android.view.accessibility.AccessibilityManager manager) {
            return false;
        }

        public android.support.v4.view.accessibility.AccessibilityManagerCompatKitKat.TouchExplorationStateChangeListenerWrapper newTouchExplorationStateChangeListener(android.support.v4.view.accessibility.AccessibilityManagerCompat.TouchExplorationStateChangeListener listener) {
            return null;
        }

        public boolean addTouchExplorationStateChangeListener(android.view.accessibility.AccessibilityManager manager, android.support.v4.view.accessibility.AccessibilityManagerCompat.TouchExplorationStateChangeListener listener) {
            return false;
        }

        public boolean removeTouchExplorationStateChangeListener(android.view.accessibility.AccessibilityManager manager, android.support.v4.view.accessibility.AccessibilityManagerCompat.TouchExplorationStateChangeListener listener) {
            return false;
        }
    }

    interface AccessibilityManagerVersionImpl {
        boolean addAccessibilityStateChangeListener(android.view.accessibility.AccessibilityManager accessibilityManager, android.support.v4.view.accessibility.AccessibilityManagerCompat.AccessibilityStateChangeListener accessibilityStateChangeListener);

        boolean addTouchExplorationStateChangeListener(android.view.accessibility.AccessibilityManager accessibilityManager, android.support.v4.view.accessibility.AccessibilityManagerCompat.TouchExplorationStateChangeListener touchExplorationStateChangeListener);

        java.util.List<android.accessibilityservice.AccessibilityServiceInfo> getEnabledAccessibilityServiceList(android.view.accessibility.AccessibilityManager accessibilityManager, int i);

        java.util.List<android.accessibilityservice.AccessibilityServiceInfo> getInstalledAccessibilityServiceList(android.view.accessibility.AccessibilityManager accessibilityManager);

        boolean isTouchExplorationEnabled(android.view.accessibility.AccessibilityManager accessibilityManager);

        android.support.v4.view.accessibility.AccessibilityManagerCompatIcs.AccessibilityStateChangeListenerWrapper newAccessibilityStateChangeListener(android.support.v4.view.accessibility.AccessibilityManagerCompat.AccessibilityStateChangeListener accessibilityStateChangeListener);

        android.support.v4.view.accessibility.AccessibilityManagerCompatKitKat.TouchExplorationStateChangeListenerWrapper newTouchExplorationStateChangeListener(android.support.v4.view.accessibility.AccessibilityManagerCompat.TouchExplorationStateChangeListener touchExplorationStateChangeListener);

        boolean removeAccessibilityStateChangeListener(android.view.accessibility.AccessibilityManager accessibilityManager, android.support.v4.view.accessibility.AccessibilityManagerCompat.AccessibilityStateChangeListener accessibilityStateChangeListener);

        boolean removeTouchExplorationStateChangeListener(android.view.accessibility.AccessibilityManager accessibilityManager, android.support.v4.view.accessibility.AccessibilityManagerCompat.TouchExplorationStateChangeListener touchExplorationStateChangeListener);
    }

    public interface AccessibilityStateChangeListener {
        void onAccessibilityStateChanged(boolean z);
    }

    @java.lang.Deprecated
    public static abstract class AccessibilityStateChangeListenerCompat implements android.support.v4.view.accessibility.AccessibilityManagerCompat.AccessibilityStateChangeListener {
    }

    public interface TouchExplorationStateChangeListener {
        void onTouchExplorationStateChanged(boolean z);
    }

    static {
        if (android.os.Build.VERSION.SDK_INT >= 19) {
            IMPL = new android.support.v4.view.accessibility.AccessibilityManagerCompat.AccessibilityManagerKitKatImpl();
        } else if (android.os.Build.VERSION.SDK_INT >= 14) {
            IMPL = new android.support.v4.view.accessibility.AccessibilityManagerCompat.AccessibilityManagerIcsImpl();
        } else {
            IMPL = new android.support.v4.view.accessibility.AccessibilityManagerCompat.AccessibilityManagerStubImpl();
        }
    }

    public static boolean addAccessibilityStateChangeListener(android.view.accessibility.AccessibilityManager manager, android.support.v4.view.accessibility.AccessibilityManagerCompat.AccessibilityStateChangeListener listener) {
        return IMPL.addAccessibilityStateChangeListener(manager, listener);
    }

    public static boolean removeAccessibilityStateChangeListener(android.view.accessibility.AccessibilityManager manager, android.support.v4.view.accessibility.AccessibilityManagerCompat.AccessibilityStateChangeListener listener) {
        return IMPL.removeAccessibilityStateChangeListener(manager, listener);
    }

    public static java.util.List<android.accessibilityservice.AccessibilityServiceInfo> getInstalledAccessibilityServiceList(android.view.accessibility.AccessibilityManager manager) {
        return IMPL.getInstalledAccessibilityServiceList(manager);
    }

    public static java.util.List<android.accessibilityservice.AccessibilityServiceInfo> getEnabledAccessibilityServiceList(android.view.accessibility.AccessibilityManager manager, int feedbackTypeFlags) {
        return IMPL.getEnabledAccessibilityServiceList(manager, feedbackTypeFlags);
    }

    public static boolean isTouchExplorationEnabled(android.view.accessibility.AccessibilityManager manager) {
        return IMPL.isTouchExplorationEnabled(manager);
    }

    public static boolean addTouchExplorationStateChangeListener(android.view.accessibility.AccessibilityManager manager, android.support.v4.view.accessibility.AccessibilityManagerCompat.TouchExplorationStateChangeListener listener) {
        return IMPL.addTouchExplorationStateChangeListener(manager, listener);
    }

    public static boolean removeTouchExplorationStateChangeListener(android.view.accessibility.AccessibilityManager manager, android.support.v4.view.accessibility.AccessibilityManagerCompat.TouchExplorationStateChangeListener listener) {
        return IMPL.removeTouchExplorationStateChangeListener(manager, listener);
    }

    private AccessibilityManagerCompat() {
    }
}
