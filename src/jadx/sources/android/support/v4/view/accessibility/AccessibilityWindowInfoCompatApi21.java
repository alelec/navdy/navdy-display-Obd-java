package android.support.v4.view.accessibility;

@android.annotation.TargetApi(21)
@android.support.annotation.RequiresApi(21)
class AccessibilityWindowInfoCompatApi21 {
    AccessibilityWindowInfoCompatApi21() {
    }

    public static java.lang.Object obtain() {
        return android.view.accessibility.AccessibilityWindowInfo.obtain();
    }

    public static java.lang.Object obtain(java.lang.Object info) {
        return android.view.accessibility.AccessibilityWindowInfo.obtain((android.view.accessibility.AccessibilityWindowInfo) info);
    }

    public static int getType(java.lang.Object info) {
        return ((android.view.accessibility.AccessibilityWindowInfo) info).getType();
    }

    public static int getLayer(java.lang.Object info) {
        return ((android.view.accessibility.AccessibilityWindowInfo) info).getLayer();
    }

    public static java.lang.Object getRoot(java.lang.Object info) {
        return ((android.view.accessibility.AccessibilityWindowInfo) info).getRoot();
    }

    public static java.lang.Object getParent(java.lang.Object info) {
        return ((android.view.accessibility.AccessibilityWindowInfo) info).getParent();
    }

    public static int getId(java.lang.Object info) {
        return ((android.view.accessibility.AccessibilityWindowInfo) info).getId();
    }

    public static void getBoundsInScreen(java.lang.Object info, android.graphics.Rect outBounds) {
        ((android.view.accessibility.AccessibilityWindowInfo) info).getBoundsInScreen(outBounds);
    }

    public static boolean isActive(java.lang.Object info) {
        return ((android.view.accessibility.AccessibilityWindowInfo) info).isActive();
    }

    public static boolean isFocused(java.lang.Object info) {
        return ((android.view.accessibility.AccessibilityWindowInfo) info).isFocused();
    }

    public static boolean isAccessibilityFocused(java.lang.Object info) {
        return ((android.view.accessibility.AccessibilityWindowInfo) info).isAccessibilityFocused();
    }

    public static int getChildCount(java.lang.Object info) {
        return ((android.view.accessibility.AccessibilityWindowInfo) info).getChildCount();
    }

    public static java.lang.Object getChild(java.lang.Object info, int index) {
        return ((android.view.accessibility.AccessibilityWindowInfo) info).getChild(index);
    }

    public static void recycle(java.lang.Object info) {
        ((android.view.accessibility.AccessibilityWindowInfo) info).recycle();
    }
}
