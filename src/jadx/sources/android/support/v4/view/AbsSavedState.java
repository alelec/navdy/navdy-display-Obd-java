package android.support.v4.view;

public abstract class AbsSavedState implements android.os.Parcelable {
    public static final android.os.Parcelable.Creator<android.support.v4.view.AbsSavedState> CREATOR = android.support.v4.os.ParcelableCompat.newCreator(new android.support.v4.os.ParcelableCompatCreatorCallbacks<android.support.v4.view.AbsSavedState>() {
        public android.support.v4.view.AbsSavedState createFromParcel(android.os.Parcel in, java.lang.ClassLoader loader) {
            if (in.readParcelable(loader) == null) {
                return android.support.v4.view.AbsSavedState.EMPTY_STATE;
            }
            throw new java.lang.IllegalStateException("superState must be null");
        }

        public android.support.v4.view.AbsSavedState[] newArray(int size) {
            return new android.support.v4.view.AbsSavedState[size];
        }
    });
    public static final android.support.v4.view.AbsSavedState EMPTY_STATE = new android.support.v4.view.AbsSavedState() {
    };
    private final android.os.Parcelable mSuperState;

    private AbsSavedState() {
        this.mSuperState = null;
    }

    protected AbsSavedState(android.os.Parcelable superState) {
        if (superState == null) {
            throw new java.lang.IllegalArgumentException("superState must not be null");
        }
        if (superState == EMPTY_STATE) {
            superState = null;
        }
        this.mSuperState = superState;
    }

    protected AbsSavedState(android.os.Parcel source) {
        this(source, null);
    }

    protected AbsSavedState(android.os.Parcel source, java.lang.ClassLoader loader) {
        android.os.Parcelable superState = source.readParcelable(loader);
        if (superState == null) {
            superState = EMPTY_STATE;
        }
        this.mSuperState = superState;
    }

    public final android.os.Parcelable getSuperState() {
        return this.mSuperState;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeParcelable(this.mSuperState, flags);
    }
}
