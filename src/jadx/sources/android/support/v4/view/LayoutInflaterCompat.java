package android.support.v4.view;

public final class LayoutInflaterCompat {
    static final android.support.v4.view.LayoutInflaterCompat.LayoutInflaterCompatImpl IMPL;

    interface LayoutInflaterCompatImpl {
        android.support.v4.view.LayoutInflaterFactory getFactory(android.view.LayoutInflater layoutInflater);

        void setFactory(android.view.LayoutInflater layoutInflater, android.support.v4.view.LayoutInflaterFactory layoutInflaterFactory);
    }

    static class LayoutInflaterCompatImplBase implements android.support.v4.view.LayoutInflaterCompat.LayoutInflaterCompatImpl {
        LayoutInflaterCompatImplBase() {
        }

        public void setFactory(android.view.LayoutInflater layoutInflater, android.support.v4.view.LayoutInflaterFactory factory) {
            android.support.v4.view.LayoutInflaterCompatBase.setFactory(layoutInflater, factory);
        }

        public android.support.v4.view.LayoutInflaterFactory getFactory(android.view.LayoutInflater layoutInflater) {
            return android.support.v4.view.LayoutInflaterCompatBase.getFactory(layoutInflater);
        }
    }

    static class LayoutInflaterCompatImplV11 extends android.support.v4.view.LayoutInflaterCompat.LayoutInflaterCompatImplBase {
        LayoutInflaterCompatImplV11() {
        }

        public void setFactory(android.view.LayoutInflater layoutInflater, android.support.v4.view.LayoutInflaterFactory factory) {
            android.support.v4.view.LayoutInflaterCompatHC.setFactory(layoutInflater, factory);
        }
    }

    static class LayoutInflaterCompatImplV21 extends android.support.v4.view.LayoutInflaterCompat.LayoutInflaterCompatImplV11 {
        LayoutInflaterCompatImplV21() {
        }

        public void setFactory(android.view.LayoutInflater layoutInflater, android.support.v4.view.LayoutInflaterFactory factory) {
            android.support.v4.view.LayoutInflaterCompatLollipop.setFactory(layoutInflater, factory);
        }
    }

    static {
        int version = android.os.Build.VERSION.SDK_INT;
        if (version >= 21) {
            IMPL = new android.support.v4.view.LayoutInflaterCompat.LayoutInflaterCompatImplV21();
        } else if (version >= 11) {
            IMPL = new android.support.v4.view.LayoutInflaterCompat.LayoutInflaterCompatImplV11();
        } else {
            IMPL = new android.support.v4.view.LayoutInflaterCompat.LayoutInflaterCompatImplBase();
        }
    }

    private LayoutInflaterCompat() {
    }

    public static void setFactory(android.view.LayoutInflater inflater, android.support.v4.view.LayoutInflaterFactory factory) {
        IMPL.setFactory(inflater, factory);
    }

    public static android.support.v4.view.LayoutInflaterFactory getFactory(android.view.LayoutInflater inflater) {
        return IMPL.getFactory(inflater);
    }
}
