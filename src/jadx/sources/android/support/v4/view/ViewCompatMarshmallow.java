package android.support.v4.view;

@android.annotation.TargetApi(23)
@android.support.annotation.RequiresApi(23)
class ViewCompatMarshmallow {
    ViewCompatMarshmallow() {
    }

    public static void setScrollIndicators(android.view.View view, int indicators) {
        view.setScrollIndicators(indicators);
    }

    public static void setScrollIndicators(android.view.View view, int indicators, int mask) {
        view.setScrollIndicators(indicators, mask);
    }

    public static int getScrollIndicators(android.view.View view) {
        return view.getScrollIndicators();
    }

    static void offsetTopAndBottom(android.view.View view, int offset) {
        view.offsetTopAndBottom(offset);
    }

    static void offsetLeftAndRight(android.view.View view, int offset) {
        view.offsetLeftAndRight(offset);
    }
}
