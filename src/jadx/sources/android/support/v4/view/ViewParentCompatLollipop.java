package android.support.v4.view;

@android.annotation.TargetApi(21)
@android.support.annotation.RequiresApi(21)
class ViewParentCompatLollipop {
    private static final java.lang.String TAG = "ViewParentCompat";

    ViewParentCompatLollipop() {
    }

    public static boolean onStartNestedScroll(android.view.ViewParent parent, android.view.View child, android.view.View target, int nestedScrollAxes) {
        try {
            return parent.onStartNestedScroll(child, target, nestedScrollAxes);
        } catch (java.lang.AbstractMethodError e) {
            android.util.Log.e(TAG, "ViewParent " + parent + " does not implement interface " + "method onStartNestedScroll", e);
            return false;
        }
    }

    public static void onNestedScrollAccepted(android.view.ViewParent parent, android.view.View child, android.view.View target, int nestedScrollAxes) {
        try {
            parent.onNestedScrollAccepted(child, target, nestedScrollAxes);
        } catch (java.lang.AbstractMethodError e) {
            android.util.Log.e(TAG, "ViewParent " + parent + " does not implement interface " + "method onNestedScrollAccepted", e);
        }
    }

    public static void onStopNestedScroll(android.view.ViewParent parent, android.view.View target) {
        try {
            parent.onStopNestedScroll(target);
        } catch (java.lang.AbstractMethodError e) {
            android.util.Log.e(TAG, "ViewParent " + parent + " does not implement interface " + "method onStopNestedScroll", e);
        }
    }

    public static void onNestedScroll(android.view.ViewParent parent, android.view.View target, int dxConsumed, int dyConsumed, int dxUnconsumed, int dyUnconsumed) {
        try {
            parent.onNestedScroll(target, dxConsumed, dyConsumed, dxUnconsumed, dyUnconsumed);
        } catch (java.lang.AbstractMethodError e) {
            android.util.Log.e(TAG, "ViewParent " + parent + " does not implement interface " + "method onNestedScroll", e);
        }
    }

    public static void onNestedPreScroll(android.view.ViewParent parent, android.view.View target, int dx, int dy, int[] consumed) {
        try {
            parent.onNestedPreScroll(target, dx, dy, consumed);
        } catch (java.lang.AbstractMethodError e) {
            android.util.Log.e(TAG, "ViewParent " + parent + " does not implement interface " + "method onNestedPreScroll", e);
        }
    }

    public static boolean onNestedFling(android.view.ViewParent parent, android.view.View target, float velocityX, float velocityY, boolean consumed) {
        try {
            return parent.onNestedFling(target, velocityX, velocityY, consumed);
        } catch (java.lang.AbstractMethodError e) {
            android.util.Log.e(TAG, "ViewParent " + parent + " does not implement interface " + "method onNestedFling", e);
            return false;
        }
    }

    public static boolean onNestedPreFling(android.view.ViewParent parent, android.view.View target, float velocityX, float velocityY) {
        try {
            return parent.onNestedPreFling(target, velocityX, velocityY);
        } catch (java.lang.AbstractMethodError e) {
            android.util.Log.e(TAG, "ViewParent " + parent + " does not implement interface " + "method onNestedPreFling", e);
            return false;
        }
    }
}
