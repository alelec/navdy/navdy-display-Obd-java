package android.support.v4.view;

public final class ViewGroupCompat {
    static final android.support.v4.view.ViewGroupCompat.ViewGroupCompatImpl IMPL;
    public static final int LAYOUT_MODE_CLIP_BOUNDS = 0;
    public static final int LAYOUT_MODE_OPTICAL_BOUNDS = 1;

    static class ViewGroupCompatHCImpl extends android.support.v4.view.ViewGroupCompat.ViewGroupCompatStubImpl {
        ViewGroupCompatHCImpl() {
        }

        public void setMotionEventSplittingEnabled(android.view.ViewGroup group, boolean split) {
            android.support.v4.view.ViewGroupCompatHC.setMotionEventSplittingEnabled(group, split);
        }
    }

    static class ViewGroupCompatIcsImpl extends android.support.v4.view.ViewGroupCompat.ViewGroupCompatHCImpl {
        ViewGroupCompatIcsImpl() {
        }

        public boolean onRequestSendAccessibilityEvent(android.view.ViewGroup group, android.view.View child, android.view.accessibility.AccessibilityEvent event) {
            return android.support.v4.view.ViewGroupCompatIcs.onRequestSendAccessibilityEvent(group, child, event);
        }
    }

    interface ViewGroupCompatImpl {
        int getLayoutMode(android.view.ViewGroup viewGroup);

        int getNestedScrollAxes(android.view.ViewGroup viewGroup);

        boolean isTransitionGroup(android.view.ViewGroup viewGroup);

        boolean onRequestSendAccessibilityEvent(android.view.ViewGroup viewGroup, android.view.View view, android.view.accessibility.AccessibilityEvent accessibilityEvent);

        void setLayoutMode(android.view.ViewGroup viewGroup, int i);

        void setMotionEventSplittingEnabled(android.view.ViewGroup viewGroup, boolean z);

        void setTransitionGroup(android.view.ViewGroup viewGroup, boolean z);
    }

    static class ViewGroupCompatJellybeanMR2Impl extends android.support.v4.view.ViewGroupCompat.ViewGroupCompatIcsImpl {
        ViewGroupCompatJellybeanMR2Impl() {
        }

        public int getLayoutMode(android.view.ViewGroup group) {
            return android.support.v4.view.ViewGroupCompatJellybeanMR2.getLayoutMode(group);
        }

        public void setLayoutMode(android.view.ViewGroup group, int mode) {
            android.support.v4.view.ViewGroupCompatJellybeanMR2.setLayoutMode(group, mode);
        }
    }

    static class ViewGroupCompatLollipopImpl extends android.support.v4.view.ViewGroupCompat.ViewGroupCompatJellybeanMR2Impl {
        ViewGroupCompatLollipopImpl() {
        }

        public void setTransitionGroup(android.view.ViewGroup group, boolean isTransitionGroup) {
            android.support.v4.view.ViewGroupCompatLollipop.setTransitionGroup(group, isTransitionGroup);
        }

        public boolean isTransitionGroup(android.view.ViewGroup group) {
            return android.support.v4.view.ViewGroupCompatLollipop.isTransitionGroup(group);
        }

        public int getNestedScrollAxes(android.view.ViewGroup group) {
            return android.support.v4.view.ViewGroupCompatLollipop.getNestedScrollAxes(group);
        }
    }

    static class ViewGroupCompatStubImpl implements android.support.v4.view.ViewGroupCompat.ViewGroupCompatImpl {
        ViewGroupCompatStubImpl() {
        }

        public boolean onRequestSendAccessibilityEvent(android.view.ViewGroup group, android.view.View child, android.view.accessibility.AccessibilityEvent event) {
            return true;
        }

        public void setMotionEventSplittingEnabled(android.view.ViewGroup group, boolean split) {
        }

        public int getLayoutMode(android.view.ViewGroup group) {
            return 0;
        }

        public void setLayoutMode(android.view.ViewGroup group, int mode) {
        }

        public void setTransitionGroup(android.view.ViewGroup group, boolean isTransitionGroup) {
        }

        public boolean isTransitionGroup(android.view.ViewGroup group) {
            return false;
        }

        public int getNestedScrollAxes(android.view.ViewGroup group) {
            if (group instanceof android.support.v4.view.NestedScrollingParent) {
                return ((android.support.v4.view.NestedScrollingParent) group).getNestedScrollAxes();
            }
            return 0;
        }
    }

    static {
        int version = android.os.Build.VERSION.SDK_INT;
        if (version >= 21) {
            IMPL = new android.support.v4.view.ViewGroupCompat.ViewGroupCompatLollipopImpl();
        } else if (version >= 18) {
            IMPL = new android.support.v4.view.ViewGroupCompat.ViewGroupCompatJellybeanMR2Impl();
        } else if (version >= 14) {
            IMPL = new android.support.v4.view.ViewGroupCompat.ViewGroupCompatIcsImpl();
        } else if (version >= 11) {
            IMPL = new android.support.v4.view.ViewGroupCompat.ViewGroupCompatHCImpl();
        } else {
            IMPL = new android.support.v4.view.ViewGroupCompat.ViewGroupCompatStubImpl();
        }
    }

    private ViewGroupCompat() {
    }

    public static boolean onRequestSendAccessibilityEvent(android.view.ViewGroup group, android.view.View child, android.view.accessibility.AccessibilityEvent event) {
        return IMPL.onRequestSendAccessibilityEvent(group, child, event);
    }

    public static void setMotionEventSplittingEnabled(android.view.ViewGroup group, boolean split) {
        IMPL.setMotionEventSplittingEnabled(group, split);
    }

    public static int getLayoutMode(android.view.ViewGroup group) {
        return IMPL.getLayoutMode(group);
    }

    public static void setLayoutMode(android.view.ViewGroup group, int mode) {
        IMPL.setLayoutMode(group, mode);
    }

    public static void setTransitionGroup(android.view.ViewGroup group, boolean isTransitionGroup) {
        IMPL.setTransitionGroup(group, isTransitionGroup);
    }

    public static boolean isTransitionGroup(android.view.ViewGroup group) {
        return IMPL.isTransitionGroup(group);
    }

    public static int getNestedScrollAxes(android.view.ViewGroup group) {
        return IMPL.getNestedScrollAxes(group);
    }
}
