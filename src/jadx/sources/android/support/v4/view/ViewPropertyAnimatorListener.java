package android.support.v4.view;

public interface ViewPropertyAnimatorListener {
    void onAnimationCancel(android.view.View view);

    void onAnimationEnd(android.view.View view);

    void onAnimationStart(android.view.View view);
}
