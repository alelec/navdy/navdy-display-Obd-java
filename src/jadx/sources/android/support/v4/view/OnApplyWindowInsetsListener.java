package android.support.v4.view;

public interface OnApplyWindowInsetsListener {
    android.support.v4.view.WindowInsetsCompat onApplyWindowInsets(android.view.View view, android.support.v4.view.WindowInsetsCompat windowInsetsCompat);
}
