package android.support.v4.view;

@android.annotation.TargetApi(11)
@android.support.annotation.RequiresApi(11)
class ViewGroupCompatHC {
    private ViewGroupCompatHC() {
    }

    public static void setMotionEventSplittingEnabled(android.view.ViewGroup group, boolean split) {
        group.setMotionEventSplittingEnabled(split);
    }
}
