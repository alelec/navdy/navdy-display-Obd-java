package android.support.v4.view;

@android.annotation.TargetApi(11)
@android.support.annotation.RequiresApi(11)
class KeyEventCompatHoneycomb {
    KeyEventCompatHoneycomb() {
    }

    public static int normalizeMetaState(int metaState) {
        return android.view.KeyEvent.normalizeMetaState(metaState);
    }

    public static boolean metaStateHasModifiers(int metaState, int modifiers) {
        return android.view.KeyEvent.metaStateHasModifiers(metaState, modifiers);
    }

    public static boolean metaStateHasNoModifiers(int metaState) {
        return android.view.KeyEvent.metaStateHasNoModifiers(metaState);
    }

    public static boolean isCtrlPressed(android.view.KeyEvent event) {
        return event.isCtrlPressed();
    }
}
