package android.support.v4.view;

@android.annotation.TargetApi(18)
@android.support.annotation.RequiresApi(18)
class ViewPropertyAnimatorCompatJellybeanMr2 {
    ViewPropertyAnimatorCompatJellybeanMr2() {
    }

    public static android.view.animation.Interpolator getInterpolator(android.view.View view) {
        return (android.view.animation.Interpolator) view.animate().getInterpolator();
    }
}
