package android.support.v4.view;

@android.annotation.TargetApi(21)
@android.support.annotation.RequiresApi(21)
class WindowInsetsCompatApi21 {
    WindowInsetsCompatApi21() {
    }

    public static java.lang.Object consumeStableInsets(java.lang.Object insets) {
        return ((android.view.WindowInsets) insets).consumeStableInsets();
    }

    public static int getStableInsetBottom(java.lang.Object insets) {
        return ((android.view.WindowInsets) insets).getStableInsetBottom();
    }

    public static int getStableInsetLeft(java.lang.Object insets) {
        return ((android.view.WindowInsets) insets).getStableInsetLeft();
    }

    public static int getStableInsetRight(java.lang.Object insets) {
        return ((android.view.WindowInsets) insets).getStableInsetRight();
    }

    public static int getStableInsetTop(java.lang.Object insets) {
        return ((android.view.WindowInsets) insets).getStableInsetTop();
    }

    public static boolean hasStableInsets(java.lang.Object insets) {
        return ((android.view.WindowInsets) insets).hasStableInsets();
    }

    public static boolean isConsumed(java.lang.Object insets) {
        return ((android.view.WindowInsets) insets).isConsumed();
    }

    public static java.lang.Object replaceSystemWindowInsets(java.lang.Object insets, android.graphics.Rect systemWindowInsets) {
        return ((android.view.WindowInsets) insets).replaceSystemWindowInsets(systemWindowInsets);
    }
}
