package android.support.v4.view;

@android.annotation.TargetApi(14)
@android.support.annotation.RequiresApi(14)
class ViewParentCompatICS {
    ViewParentCompatICS() {
    }

    public static boolean requestSendAccessibilityEvent(android.view.ViewParent parent, android.view.View child, android.view.accessibility.AccessibilityEvent event) {
        return parent.requestSendAccessibilityEvent(child, event);
    }
}
