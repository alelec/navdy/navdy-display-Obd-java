package android.support.v4.view;

public final class AsyncLayoutInflater {
    private static final java.lang.String TAG = "AsyncLayoutInflater";
    android.os.Handler mHandler;
    private android.os.Handler.Callback mHandlerCallback = new android.os.Handler.Callback() {
        public boolean handleMessage(android.os.Message msg) {
            android.support.v4.view.AsyncLayoutInflater.InflateRequest request = (android.support.v4.view.AsyncLayoutInflater.InflateRequest) msg.obj;
            if (request.view == null) {
                request.view = android.support.v4.view.AsyncLayoutInflater.this.mInflater.inflate(request.resid, request.parent, false);
            }
            request.callback.onInflateFinished(request.view, request.resid, request.parent);
            android.support.v4.view.AsyncLayoutInflater.this.mInflateThread.releaseRequest(request);
            return true;
        }
    };
    android.support.v4.view.AsyncLayoutInflater.InflateThread mInflateThread;
    android.view.LayoutInflater mInflater;

    private static class BasicInflater extends android.view.LayoutInflater {
        private static final java.lang.String[] sClassPrefixList = {"android.widget.", "android.webkit.", "android.app."};

        BasicInflater(android.content.Context context) {
            super(context);
        }

        public android.view.LayoutInflater cloneInContext(android.content.Context newContext) {
            return new android.support.v4.view.AsyncLayoutInflater.BasicInflater(newContext);
        }

        /* access modifiers changed from: protected */
        public android.view.View onCreateView(java.lang.String name, android.util.AttributeSet attrs) throws java.lang.ClassNotFoundException {
            java.lang.String[] strArr = sClassPrefixList;
            int length = strArr.length;
            int i = 0;
            while (i < length) {
                try {
                    android.view.View view = createView(name, strArr[i], attrs);
                    if (view != null) {
                        return view;
                    }
                    i++;
                } catch (java.lang.ClassNotFoundException e) {
                }
            }
            return super.onCreateView(name, attrs);
        }
    }

    private static class InflateRequest {
        android.support.v4.view.AsyncLayoutInflater.OnInflateFinishedListener callback;
        android.support.v4.view.AsyncLayoutInflater inflater;
        android.view.ViewGroup parent;
        int resid;
        android.view.View view;

        InflateRequest() {
        }
    }

    private static class InflateThread extends java.lang.Thread {
        private static final android.support.v4.view.AsyncLayoutInflater.InflateThread sInstance = new android.support.v4.view.AsyncLayoutInflater.InflateThread();
        private java.util.concurrent.ArrayBlockingQueue<android.support.v4.view.AsyncLayoutInflater.InflateRequest> mQueue = new java.util.concurrent.ArrayBlockingQueue<>(10);
        private android.support.v4.util.Pools.SynchronizedPool<android.support.v4.view.AsyncLayoutInflater.InflateRequest> mRequestPool = new android.support.v4.util.Pools.SynchronizedPool<>(10);

        private InflateThread() {
        }

        static {
            sInstance.start();
        }

        public static android.support.v4.view.AsyncLayoutInflater.InflateThread getInstance() {
            return sInstance;
        }

        public void run() {
            while (true) {
                try {
                    android.support.v4.view.AsyncLayoutInflater.InflateRequest request = (android.support.v4.view.AsyncLayoutInflater.InflateRequest) this.mQueue.take();
                    try {
                        request.view = request.inflater.mInflater.inflate(request.resid, request.parent, false);
                    } catch (java.lang.RuntimeException ex) {
                        android.util.Log.w(android.support.v4.view.AsyncLayoutInflater.TAG, "Failed to inflate resource in the background! Retrying on the UI thread", ex);
                    }
                    android.os.Message.obtain(request.inflater.mHandler, 0, request).sendToTarget();
                } catch (java.lang.InterruptedException ex2) {
                    android.util.Log.w(android.support.v4.view.AsyncLayoutInflater.TAG, ex2);
                }
            }
        }

        public android.support.v4.view.AsyncLayoutInflater.InflateRequest obtainRequest() {
            android.support.v4.view.AsyncLayoutInflater.InflateRequest obj = (android.support.v4.view.AsyncLayoutInflater.InflateRequest) this.mRequestPool.acquire();
            if (obj == null) {
                return new android.support.v4.view.AsyncLayoutInflater.InflateRequest();
            }
            return obj;
        }

        public void releaseRequest(android.support.v4.view.AsyncLayoutInflater.InflateRequest obj) {
            obj.callback = null;
            obj.inflater = null;
            obj.parent = null;
            obj.resid = 0;
            obj.view = null;
            this.mRequestPool.release(obj);
        }

        public void enqueue(android.support.v4.view.AsyncLayoutInflater.InflateRequest request) {
            try {
                this.mQueue.put(request);
            } catch (java.lang.InterruptedException e) {
                throw new java.lang.RuntimeException("Failed to enqueue async inflate request", e);
            }
        }
    }

    public interface OnInflateFinishedListener {
        void onInflateFinished(android.view.View view, int i, android.view.ViewGroup viewGroup);
    }

    public AsyncLayoutInflater(@android.support.annotation.NonNull android.content.Context context) {
        this.mInflater = new android.support.v4.view.AsyncLayoutInflater.BasicInflater(context);
        this.mHandler = new android.os.Handler(this.mHandlerCallback);
        this.mInflateThread = android.support.v4.view.AsyncLayoutInflater.InflateThread.getInstance();
    }

    @android.support.annotation.UiThread
    public void inflate(@android.support.annotation.LayoutRes int resid, @android.support.annotation.Nullable android.view.ViewGroup parent, @android.support.annotation.NonNull android.support.v4.view.AsyncLayoutInflater.OnInflateFinishedListener callback) {
        if (callback == null) {
            throw new java.lang.NullPointerException("callback argument may not be null!");
        }
        android.support.v4.view.AsyncLayoutInflater.InflateRequest request = this.mInflateThread.obtainRequest();
        request.inflater = this;
        request.resid = resid;
        request.parent = parent;
        request.callback = callback;
        this.mInflateThread.enqueue(request);
    }
}
