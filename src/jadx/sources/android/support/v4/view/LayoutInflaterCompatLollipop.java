package android.support.v4.view;

@android.annotation.TargetApi(21)
@android.support.annotation.RequiresApi(21)
class LayoutInflaterCompatLollipop {
    LayoutInflaterCompatLollipop() {
    }

    static void setFactory(android.view.LayoutInflater inflater, android.support.v4.view.LayoutInflaterFactory factory) {
        inflater.setFactory2(factory != null ? new android.support.v4.view.LayoutInflaterCompatHC.FactoryWrapperHC(factory) : null);
    }
}
