package android.support.v4.view;

public final class ViewParentCompat {
    static final android.support.v4.view.ViewParentCompat.ViewParentCompatImpl IMPL;

    static class ViewParentCompatICSImpl extends android.support.v4.view.ViewParentCompat.ViewParentCompatStubImpl {
        ViewParentCompatICSImpl() {
        }

        public boolean requestSendAccessibilityEvent(android.view.ViewParent parent, android.view.View child, android.view.accessibility.AccessibilityEvent event) {
            return android.support.v4.view.ViewParentCompatICS.requestSendAccessibilityEvent(parent, child, event);
        }
    }

    interface ViewParentCompatImpl {
        void notifySubtreeAccessibilityStateChanged(android.view.ViewParent viewParent, android.view.View view, android.view.View view2, int i);

        boolean onNestedFling(android.view.ViewParent viewParent, android.view.View view, float f, float f2, boolean z);

        boolean onNestedPreFling(android.view.ViewParent viewParent, android.view.View view, float f, float f2);

        void onNestedPreScroll(android.view.ViewParent viewParent, android.view.View view, int i, int i2, int[] iArr);

        void onNestedScroll(android.view.ViewParent viewParent, android.view.View view, int i, int i2, int i3, int i4);

        void onNestedScrollAccepted(android.view.ViewParent viewParent, android.view.View view, android.view.View view2, int i);

        boolean onStartNestedScroll(android.view.ViewParent viewParent, android.view.View view, android.view.View view2, int i);

        void onStopNestedScroll(android.view.ViewParent viewParent, android.view.View view);

        boolean requestSendAccessibilityEvent(android.view.ViewParent viewParent, android.view.View view, android.view.accessibility.AccessibilityEvent accessibilityEvent);
    }

    static class ViewParentCompatKitKatImpl extends android.support.v4.view.ViewParentCompat.ViewParentCompatICSImpl {
        ViewParentCompatKitKatImpl() {
        }

        public void notifySubtreeAccessibilityStateChanged(android.view.ViewParent parent, android.view.View child, android.view.View source, int changeType) {
            android.support.v4.view.ViewParentCompatKitKat.notifySubtreeAccessibilityStateChanged(parent, child, source, changeType);
        }
    }

    static class ViewParentCompatLollipopImpl extends android.support.v4.view.ViewParentCompat.ViewParentCompatKitKatImpl {
        ViewParentCompatLollipopImpl() {
        }

        public boolean onStartNestedScroll(android.view.ViewParent parent, android.view.View child, android.view.View target, int nestedScrollAxes) {
            return android.support.v4.view.ViewParentCompatLollipop.onStartNestedScroll(parent, child, target, nestedScrollAxes);
        }

        public void onNestedScrollAccepted(android.view.ViewParent parent, android.view.View child, android.view.View target, int nestedScrollAxes) {
            android.support.v4.view.ViewParentCompatLollipop.onNestedScrollAccepted(parent, child, target, nestedScrollAxes);
        }

        public void onStopNestedScroll(android.view.ViewParent parent, android.view.View target) {
            android.support.v4.view.ViewParentCompatLollipop.onStopNestedScroll(parent, target);
        }

        public void onNestedScroll(android.view.ViewParent parent, android.view.View target, int dxConsumed, int dyConsumed, int dxUnconsumed, int dyUnconsumed) {
            android.support.v4.view.ViewParentCompatLollipop.onNestedScroll(parent, target, dxConsumed, dyConsumed, dxUnconsumed, dyUnconsumed);
        }

        public void onNestedPreScroll(android.view.ViewParent parent, android.view.View target, int dx, int dy, int[] consumed) {
            android.support.v4.view.ViewParentCompatLollipop.onNestedPreScroll(parent, target, dx, dy, consumed);
        }

        public boolean onNestedFling(android.view.ViewParent parent, android.view.View target, float velocityX, float velocityY, boolean consumed) {
            return android.support.v4.view.ViewParentCompatLollipop.onNestedFling(parent, target, velocityX, velocityY, consumed);
        }

        public boolean onNestedPreFling(android.view.ViewParent parent, android.view.View target, float velocityX, float velocityY) {
            return android.support.v4.view.ViewParentCompatLollipop.onNestedPreFling(parent, target, velocityX, velocityY);
        }
    }

    static class ViewParentCompatStubImpl implements android.support.v4.view.ViewParentCompat.ViewParentCompatImpl {
        ViewParentCompatStubImpl() {
        }

        public boolean requestSendAccessibilityEvent(android.view.ViewParent parent, android.view.View child, android.view.accessibility.AccessibilityEvent event) {
            if (child == null) {
                return false;
            }
            ((android.view.accessibility.AccessibilityManager) child.getContext().getSystemService("accessibility")).sendAccessibilityEvent(event);
            return true;
        }

        public boolean onStartNestedScroll(android.view.ViewParent parent, android.view.View child, android.view.View target, int nestedScrollAxes) {
            if (parent instanceof android.support.v4.view.NestedScrollingParent) {
                return ((android.support.v4.view.NestedScrollingParent) parent).onStartNestedScroll(child, target, nestedScrollAxes);
            }
            return false;
        }

        public void onNestedScrollAccepted(android.view.ViewParent parent, android.view.View child, android.view.View target, int nestedScrollAxes) {
            if (parent instanceof android.support.v4.view.NestedScrollingParent) {
                ((android.support.v4.view.NestedScrollingParent) parent).onNestedScrollAccepted(child, target, nestedScrollAxes);
            }
        }

        public void onStopNestedScroll(android.view.ViewParent parent, android.view.View target) {
            if (parent instanceof android.support.v4.view.NestedScrollingParent) {
                ((android.support.v4.view.NestedScrollingParent) parent).onStopNestedScroll(target);
            }
        }

        public void onNestedScroll(android.view.ViewParent parent, android.view.View target, int dxConsumed, int dyConsumed, int dxUnconsumed, int dyUnconsumed) {
            if (parent instanceof android.support.v4.view.NestedScrollingParent) {
                ((android.support.v4.view.NestedScrollingParent) parent).onNestedScroll(target, dxConsumed, dyConsumed, dxUnconsumed, dyUnconsumed);
            }
        }

        public void onNestedPreScroll(android.view.ViewParent parent, android.view.View target, int dx, int dy, int[] consumed) {
            if (parent instanceof android.support.v4.view.NestedScrollingParent) {
                ((android.support.v4.view.NestedScrollingParent) parent).onNestedPreScroll(target, dx, dy, consumed);
            }
        }

        public boolean onNestedFling(android.view.ViewParent parent, android.view.View target, float velocityX, float velocityY, boolean consumed) {
            if (parent instanceof android.support.v4.view.NestedScrollingParent) {
                return ((android.support.v4.view.NestedScrollingParent) parent).onNestedFling(target, velocityX, velocityY, consumed);
            }
            return false;
        }

        public boolean onNestedPreFling(android.view.ViewParent parent, android.view.View target, float velocityX, float velocityY) {
            if (parent instanceof android.support.v4.view.NestedScrollingParent) {
                return ((android.support.v4.view.NestedScrollingParent) parent).onNestedPreFling(target, velocityX, velocityY);
            }
            return false;
        }

        public void notifySubtreeAccessibilityStateChanged(android.view.ViewParent parent, android.view.View child, android.view.View source, int changeType) {
        }
    }

    static {
        int version = android.os.Build.VERSION.SDK_INT;
        if (version >= 21) {
            IMPL = new android.support.v4.view.ViewParentCompat.ViewParentCompatLollipopImpl();
        } else if (version >= 19) {
            IMPL = new android.support.v4.view.ViewParentCompat.ViewParentCompatKitKatImpl();
        } else if (version >= 14) {
            IMPL = new android.support.v4.view.ViewParentCompat.ViewParentCompatICSImpl();
        } else {
            IMPL = new android.support.v4.view.ViewParentCompat.ViewParentCompatStubImpl();
        }
    }

    private ViewParentCompat() {
    }

    public static boolean requestSendAccessibilityEvent(android.view.ViewParent parent, android.view.View child, android.view.accessibility.AccessibilityEvent event) {
        return IMPL.requestSendAccessibilityEvent(parent, child, event);
    }

    public static boolean onStartNestedScroll(android.view.ViewParent parent, android.view.View child, android.view.View target, int nestedScrollAxes) {
        return IMPL.onStartNestedScroll(parent, child, target, nestedScrollAxes);
    }

    public static void onNestedScrollAccepted(android.view.ViewParent parent, android.view.View child, android.view.View target, int nestedScrollAxes) {
        IMPL.onNestedScrollAccepted(parent, child, target, nestedScrollAxes);
    }

    public static void onStopNestedScroll(android.view.ViewParent parent, android.view.View target) {
        IMPL.onStopNestedScroll(parent, target);
    }

    public static void onNestedScroll(android.view.ViewParent parent, android.view.View target, int dxConsumed, int dyConsumed, int dxUnconsumed, int dyUnconsumed) {
        IMPL.onNestedScroll(parent, target, dxConsumed, dyConsumed, dxUnconsumed, dyUnconsumed);
    }

    public static void onNestedPreScroll(android.view.ViewParent parent, android.view.View target, int dx, int dy, int[] consumed) {
        IMPL.onNestedPreScroll(parent, target, dx, dy, consumed);
    }

    public static boolean onNestedFling(android.view.ViewParent parent, android.view.View target, float velocityX, float velocityY, boolean consumed) {
        return IMPL.onNestedFling(parent, target, velocityX, velocityY, consumed);
    }

    public static boolean onNestedPreFling(android.view.ViewParent parent, android.view.View target, float velocityX, float velocityY) {
        return IMPL.onNestedPreFling(parent, target, velocityX, velocityY);
    }

    public static void notifySubtreeAccessibilityStateChanged(android.view.ViewParent parent, android.view.View child, android.view.View source, int changeType) {
        IMPL.notifySubtreeAccessibilityStateChanged(parent, child, source, changeType);
    }
}
