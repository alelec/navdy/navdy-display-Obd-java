package android.support.v4.view;

public final class VelocityTrackerCompat {
    static final android.support.v4.view.VelocityTrackerCompat.VelocityTrackerVersionImpl IMPL;

    static class BaseVelocityTrackerVersionImpl implements android.support.v4.view.VelocityTrackerCompat.VelocityTrackerVersionImpl {
        BaseVelocityTrackerVersionImpl() {
        }

        public float getXVelocity(android.view.VelocityTracker tracker, int pointerId) {
            return tracker.getXVelocity();
        }

        public float getYVelocity(android.view.VelocityTracker tracker, int pointerId) {
            return tracker.getYVelocity();
        }
    }

    static class HoneycombVelocityTrackerVersionImpl implements android.support.v4.view.VelocityTrackerCompat.VelocityTrackerVersionImpl {
        HoneycombVelocityTrackerVersionImpl() {
        }

        public float getXVelocity(android.view.VelocityTracker tracker, int pointerId) {
            return android.support.v4.view.VelocityTrackerCompatHoneycomb.getXVelocity(tracker, pointerId);
        }

        public float getYVelocity(android.view.VelocityTracker tracker, int pointerId) {
            return android.support.v4.view.VelocityTrackerCompatHoneycomb.getYVelocity(tracker, pointerId);
        }
    }

    interface VelocityTrackerVersionImpl {
        float getXVelocity(android.view.VelocityTracker velocityTracker, int i);

        float getYVelocity(android.view.VelocityTracker velocityTracker, int i);
    }

    static {
        if (android.os.Build.VERSION.SDK_INT >= 11) {
            IMPL = new android.support.v4.view.VelocityTrackerCompat.HoneycombVelocityTrackerVersionImpl();
        } else {
            IMPL = new android.support.v4.view.VelocityTrackerCompat.BaseVelocityTrackerVersionImpl();
        }
    }

    public static float getXVelocity(android.view.VelocityTracker tracker, int pointerId) {
        return IMPL.getXVelocity(tracker, pointerId);
    }

    public static float getYVelocity(android.view.VelocityTracker tracker, int pointerId) {
        return IMPL.getYVelocity(tracker, pointerId);
    }

    private VelocityTrackerCompat() {
    }
}
