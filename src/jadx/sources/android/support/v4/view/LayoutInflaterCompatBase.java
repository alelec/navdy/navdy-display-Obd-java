package android.support.v4.view;

@android.annotation.TargetApi(9)
@android.support.annotation.RequiresApi(9)
class LayoutInflaterCompatBase {

    static class FactoryWrapper implements android.view.LayoutInflater.Factory {
        final android.support.v4.view.LayoutInflaterFactory mDelegateFactory;

        FactoryWrapper(android.support.v4.view.LayoutInflaterFactory delegateFactory) {
            this.mDelegateFactory = delegateFactory;
        }

        public android.view.View onCreateView(java.lang.String name, android.content.Context context, android.util.AttributeSet attrs) {
            return this.mDelegateFactory.onCreateView(null, name, context, attrs);
        }

        public java.lang.String toString() {
            return getClass().getName() + "{" + this.mDelegateFactory + "}";
        }
    }

    LayoutInflaterCompatBase() {
    }

    static void setFactory(android.view.LayoutInflater inflater, android.support.v4.view.LayoutInflaterFactory factory) {
        inflater.setFactory(factory != null ? new android.support.v4.view.LayoutInflaterCompatBase.FactoryWrapper(factory) : null);
    }

    static android.support.v4.view.LayoutInflaterFactory getFactory(android.view.LayoutInflater inflater) {
        android.view.LayoutInflater.Factory factory = inflater.getFactory();
        if (factory instanceof android.support.v4.view.LayoutInflaterCompatBase.FactoryWrapper) {
            return ((android.support.v4.view.LayoutInflaterCompatBase.FactoryWrapper) factory).mDelegateFactory;
        }
        return null;
    }
}
