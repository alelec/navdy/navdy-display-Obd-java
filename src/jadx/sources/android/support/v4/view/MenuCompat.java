package android.support.v4.view;

public final class MenuCompat {
    @java.lang.Deprecated
    public static void setShowAsAction(android.view.MenuItem item, int actionEnum) {
        android.support.v4.view.MenuItemCompat.setShowAsAction(item, actionEnum);
    }

    private MenuCompat() {
    }
}
