package android.support.v4.view;

@android.annotation.TargetApi(16)
@android.support.annotation.RequiresApi(16)
class ViewCompatJB {
    ViewCompatJB() {
    }

    public static boolean hasTransientState(android.view.View view) {
        return view.hasTransientState();
    }

    public static void setHasTransientState(android.view.View view, boolean hasTransientState) {
        view.setHasTransientState(hasTransientState);
    }

    public static void postInvalidateOnAnimation(android.view.View view) {
        view.postInvalidateOnAnimation();
    }

    public static void postInvalidateOnAnimation(android.view.View view, int left, int top, int right, int bottom) {
        view.postInvalidate(left, top, right, bottom);
    }

    public static void postOnAnimation(android.view.View view, java.lang.Runnable action) {
        view.postOnAnimation(action);
    }

    public static void postOnAnimationDelayed(android.view.View view, java.lang.Runnable action, long delayMillis) {
        view.postOnAnimationDelayed(action, delayMillis);
    }

    public static int getImportantForAccessibility(android.view.View view) {
        return view.getImportantForAccessibility();
    }

    public static void setImportantForAccessibility(android.view.View view, int mode) {
        view.setImportantForAccessibility(mode);
    }

    public static boolean performAccessibilityAction(android.view.View view, int action, android.os.Bundle arguments) {
        return view.performAccessibilityAction(action, arguments);
    }

    public static java.lang.Object getAccessibilityNodeProvider(android.view.View view) {
        return view.getAccessibilityNodeProvider();
    }

    public static android.view.ViewParent getParentForAccessibility(android.view.View view) {
        return view.getParentForAccessibility();
    }

    public static int getMinimumWidth(android.view.View view) {
        return view.getMinimumWidth();
    }

    public static int getMinimumHeight(android.view.View view) {
        return view.getMinimumHeight();
    }

    public static void requestApplyInsets(android.view.View view) {
        view.requestFitSystemWindows();
    }

    public static boolean getFitsSystemWindows(android.view.View view) {
        return view.getFitsSystemWindows();
    }

    public static boolean hasOverlappingRendering(android.view.View view) {
        return view.hasOverlappingRendering();
    }

    public static void setBackground(android.view.View view, android.graphics.drawable.Drawable drawable) {
        view.setBackground(drawable);
    }
}
