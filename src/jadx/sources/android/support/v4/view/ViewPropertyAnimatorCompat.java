package android.support.v4.view;

public final class ViewPropertyAnimatorCompat {
    static final android.support.v4.view.ViewPropertyAnimatorCompat.ViewPropertyAnimatorCompatImpl IMPL;
    static final int LISTENER_TAG_ID = 2113929216;
    private static final java.lang.String TAG = "ViewAnimatorCompat";
    java.lang.Runnable mEndAction = null;
    int mOldLayerType = -1;
    java.lang.Runnable mStartAction = null;
    private java.lang.ref.WeakReference<android.view.View> mView;

    static class BaseViewPropertyAnimatorCompatImpl implements android.support.v4.view.ViewPropertyAnimatorCompat.ViewPropertyAnimatorCompatImpl {
        java.util.WeakHashMap<android.view.View, java.lang.Runnable> mStarterMap = null;

        class Starter implements java.lang.Runnable {
            java.lang.ref.WeakReference<android.view.View> mViewRef;
            android.support.v4.view.ViewPropertyAnimatorCompat mVpa;

            Starter(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view) {
                this.mViewRef = new java.lang.ref.WeakReference<>(view);
                this.mVpa = vpa;
            }

            public void run() {
                android.view.View view = (android.view.View) this.mViewRef.get();
                if (view != null) {
                    android.support.v4.view.ViewPropertyAnimatorCompat.BaseViewPropertyAnimatorCompatImpl.this.startAnimation(this.mVpa, view);
                }
            }
        }

        BaseViewPropertyAnimatorCompatImpl() {
        }

        public void setDuration(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, long value) {
        }

        public void alpha(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, float value) {
            postStartMessage(vpa, view);
        }

        public void translationX(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, float value) {
            postStartMessage(vpa, view);
        }

        public void translationY(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, float value) {
            postStartMessage(vpa, view);
        }

        public void withEndAction(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, java.lang.Runnable runnable) {
            vpa.mEndAction = runnable;
            postStartMessage(vpa, view);
        }

        public long getDuration(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view) {
            return 0;
        }

        public void setInterpolator(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, android.view.animation.Interpolator value) {
        }

        public android.view.animation.Interpolator getInterpolator(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view) {
            return null;
        }

        public void setStartDelay(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, long value) {
        }

        public long getStartDelay(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view) {
            return 0;
        }

        public void alphaBy(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, float value) {
            postStartMessage(vpa, view);
        }

        public void rotation(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, float value) {
            postStartMessage(vpa, view);
        }

        public void rotationBy(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, float value) {
            postStartMessage(vpa, view);
        }

        public void rotationX(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, float value) {
            postStartMessage(vpa, view);
        }

        public void rotationXBy(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, float value) {
            postStartMessage(vpa, view);
        }

        public void rotationY(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, float value) {
            postStartMessage(vpa, view);
        }

        public void rotationYBy(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, float value) {
            postStartMessage(vpa, view);
        }

        public void scaleX(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, float value) {
            postStartMessage(vpa, view);
        }

        public void scaleXBy(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, float value) {
            postStartMessage(vpa, view);
        }

        public void scaleY(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, float value) {
            postStartMessage(vpa, view);
        }

        public void scaleYBy(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, float value) {
            postStartMessage(vpa, view);
        }

        public void cancel(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view) {
            postStartMessage(vpa, view);
        }

        public void x(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, float value) {
            postStartMessage(vpa, view);
        }

        public void xBy(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, float value) {
            postStartMessage(vpa, view);
        }

        public void y(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, float value) {
            postStartMessage(vpa, view);
        }

        public void yBy(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, float value) {
            postStartMessage(vpa, view);
        }

        public void z(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, float value) {
        }

        public void zBy(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, float value) {
        }

        public void translationXBy(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, float value) {
            postStartMessage(vpa, view);
        }

        public void translationYBy(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, float value) {
            postStartMessage(vpa, view);
        }

        public void translationZ(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, float value) {
        }

        public void translationZBy(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, float value) {
        }

        public void start(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view) {
            removeStartMessage(view);
            startAnimation(vpa, view);
        }

        public void withLayer(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view) {
        }

        public void withStartAction(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, java.lang.Runnable runnable) {
            vpa.mStartAction = runnable;
            postStartMessage(vpa, view);
        }

        public void setListener(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, android.support.v4.view.ViewPropertyAnimatorListener listener) {
            view.setTag(android.support.v4.view.ViewPropertyAnimatorCompat.LISTENER_TAG_ID, listener);
        }

        public void setUpdateListener(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, android.support.v4.view.ViewPropertyAnimatorUpdateListener listener) {
        }

        /* access modifiers changed from: 0000 */
        public void startAnimation(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view) {
            java.lang.Object listenerTag = view.getTag(android.support.v4.view.ViewPropertyAnimatorCompat.LISTENER_TAG_ID);
            android.support.v4.view.ViewPropertyAnimatorListener listener = null;
            if (listenerTag instanceof android.support.v4.view.ViewPropertyAnimatorListener) {
                listener = (android.support.v4.view.ViewPropertyAnimatorListener) listenerTag;
            }
            java.lang.Runnable startAction = vpa.mStartAction;
            java.lang.Runnable endAction = vpa.mEndAction;
            vpa.mStartAction = null;
            vpa.mEndAction = null;
            if (startAction != null) {
                startAction.run();
            }
            if (listener != null) {
                listener.onAnimationStart(view);
                listener.onAnimationEnd(view);
            }
            if (endAction != null) {
                endAction.run();
            }
            if (this.mStarterMap != null) {
                this.mStarterMap.remove(view);
            }
        }

        private void removeStartMessage(android.view.View view) {
            if (this.mStarterMap != null) {
                java.lang.Runnable starter = (java.lang.Runnable) this.mStarterMap.get(view);
                if (starter != null) {
                    view.removeCallbacks(starter);
                }
            }
        }

        private void postStartMessage(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view) {
            java.lang.Runnable starter = null;
            if (this.mStarterMap != null) {
                starter = (java.lang.Runnable) this.mStarterMap.get(view);
            }
            if (starter == null) {
                starter = new android.support.v4.view.ViewPropertyAnimatorCompat.BaseViewPropertyAnimatorCompatImpl.Starter(vpa, view);
                if (this.mStarterMap == null) {
                    this.mStarterMap = new java.util.WeakHashMap<>();
                }
                this.mStarterMap.put(view, starter);
            }
            view.removeCallbacks(starter);
            view.post(starter);
        }
    }

    static class ICSViewPropertyAnimatorCompatImpl extends android.support.v4.view.ViewPropertyAnimatorCompat.BaseViewPropertyAnimatorCompatImpl {
        java.util.WeakHashMap<android.view.View, java.lang.Integer> mLayerMap = null;

        static class MyVpaListener implements android.support.v4.view.ViewPropertyAnimatorListener {
            boolean mAnimEndCalled;
            android.support.v4.view.ViewPropertyAnimatorCompat mVpa;

            MyVpaListener(android.support.v4.view.ViewPropertyAnimatorCompat vpa) {
                this.mVpa = vpa;
            }

            public void onAnimationStart(android.view.View view) {
                this.mAnimEndCalled = false;
                if (this.mVpa.mOldLayerType >= 0) {
                    android.support.v4.view.ViewCompat.setLayerType(view, 2, null);
                }
                if (this.mVpa.mStartAction != null) {
                    java.lang.Runnable startAction = this.mVpa.mStartAction;
                    this.mVpa.mStartAction = null;
                    startAction.run();
                }
                java.lang.Object listenerTag = view.getTag(android.support.v4.view.ViewPropertyAnimatorCompat.LISTENER_TAG_ID);
                android.support.v4.view.ViewPropertyAnimatorListener listener = null;
                if (listenerTag instanceof android.support.v4.view.ViewPropertyAnimatorListener) {
                    listener = (android.support.v4.view.ViewPropertyAnimatorListener) listenerTag;
                }
                if (listener != null) {
                    listener.onAnimationStart(view);
                }
            }

            public void onAnimationEnd(android.view.View view) {
                if (this.mVpa.mOldLayerType >= 0) {
                    android.support.v4.view.ViewCompat.setLayerType(view, this.mVpa.mOldLayerType, null);
                    this.mVpa.mOldLayerType = -1;
                }
                if (android.os.Build.VERSION.SDK_INT >= 16 || !this.mAnimEndCalled) {
                    if (this.mVpa.mEndAction != null) {
                        java.lang.Runnable endAction = this.mVpa.mEndAction;
                        this.mVpa.mEndAction = null;
                        endAction.run();
                    }
                    java.lang.Object listenerTag = view.getTag(android.support.v4.view.ViewPropertyAnimatorCompat.LISTENER_TAG_ID);
                    android.support.v4.view.ViewPropertyAnimatorListener listener = null;
                    if (listenerTag instanceof android.support.v4.view.ViewPropertyAnimatorListener) {
                        listener = (android.support.v4.view.ViewPropertyAnimatorListener) listenerTag;
                    }
                    if (listener != null) {
                        listener.onAnimationEnd(view);
                    }
                    this.mAnimEndCalled = true;
                }
            }

            public void onAnimationCancel(android.view.View view) {
                java.lang.Object listenerTag = view.getTag(android.support.v4.view.ViewPropertyAnimatorCompat.LISTENER_TAG_ID);
                android.support.v4.view.ViewPropertyAnimatorListener listener = null;
                if (listenerTag instanceof android.support.v4.view.ViewPropertyAnimatorListener) {
                    listener = (android.support.v4.view.ViewPropertyAnimatorListener) listenerTag;
                }
                if (listener != null) {
                    listener.onAnimationCancel(view);
                }
            }
        }

        ICSViewPropertyAnimatorCompatImpl() {
        }

        public void setDuration(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, long value) {
            android.support.v4.view.ViewPropertyAnimatorCompatICS.setDuration(view, value);
        }

        public void alpha(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, float value) {
            android.support.v4.view.ViewPropertyAnimatorCompatICS.alpha(view, value);
        }

        public void translationX(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, float value) {
            android.support.v4.view.ViewPropertyAnimatorCompatICS.translationX(view, value);
        }

        public void translationY(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, float value) {
            android.support.v4.view.ViewPropertyAnimatorCompatICS.translationY(view, value);
        }

        public long getDuration(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view) {
            return android.support.v4.view.ViewPropertyAnimatorCompatICS.getDuration(view);
        }

        public void setInterpolator(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, android.view.animation.Interpolator value) {
            android.support.v4.view.ViewPropertyAnimatorCompatICS.setInterpolator(view, value);
        }

        public void setStartDelay(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, long value) {
            android.support.v4.view.ViewPropertyAnimatorCompatICS.setStartDelay(view, value);
        }

        public long getStartDelay(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view) {
            return android.support.v4.view.ViewPropertyAnimatorCompatICS.getStartDelay(view);
        }

        public void alphaBy(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, float value) {
            android.support.v4.view.ViewPropertyAnimatorCompatICS.alphaBy(view, value);
        }

        public void rotation(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, float value) {
            android.support.v4.view.ViewPropertyAnimatorCompatICS.rotation(view, value);
        }

        public void rotationBy(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, float value) {
            android.support.v4.view.ViewPropertyAnimatorCompatICS.rotationBy(view, value);
        }

        public void rotationX(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, float value) {
            android.support.v4.view.ViewPropertyAnimatorCompatICS.rotationX(view, value);
        }

        public void rotationXBy(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, float value) {
            android.support.v4.view.ViewPropertyAnimatorCompatICS.rotationXBy(view, value);
        }

        public void rotationY(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, float value) {
            android.support.v4.view.ViewPropertyAnimatorCompatICS.rotationY(view, value);
        }

        public void rotationYBy(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, float value) {
            android.support.v4.view.ViewPropertyAnimatorCompatICS.rotationYBy(view, value);
        }

        public void scaleX(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, float value) {
            android.support.v4.view.ViewPropertyAnimatorCompatICS.scaleX(view, value);
        }

        public void scaleXBy(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, float value) {
            android.support.v4.view.ViewPropertyAnimatorCompatICS.scaleXBy(view, value);
        }

        public void scaleY(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, float value) {
            android.support.v4.view.ViewPropertyAnimatorCompatICS.scaleY(view, value);
        }

        public void scaleYBy(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, float value) {
            android.support.v4.view.ViewPropertyAnimatorCompatICS.scaleYBy(view, value);
        }

        public void cancel(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view) {
            android.support.v4.view.ViewPropertyAnimatorCompatICS.cancel(view);
        }

        public void x(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, float value) {
            android.support.v4.view.ViewPropertyAnimatorCompatICS.x(view, value);
        }

        public void xBy(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, float value) {
            android.support.v4.view.ViewPropertyAnimatorCompatICS.xBy(view, value);
        }

        public void y(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, float value) {
            android.support.v4.view.ViewPropertyAnimatorCompatICS.y(view, value);
        }

        public void yBy(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, float value) {
            android.support.v4.view.ViewPropertyAnimatorCompatICS.yBy(view, value);
        }

        public void translationXBy(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, float value) {
            android.support.v4.view.ViewPropertyAnimatorCompatICS.translationXBy(view, value);
        }

        public void translationYBy(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, float value) {
            android.support.v4.view.ViewPropertyAnimatorCompatICS.translationYBy(view, value);
        }

        public void start(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view) {
            android.support.v4.view.ViewPropertyAnimatorCompatICS.start(view);
        }

        public void setListener(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, android.support.v4.view.ViewPropertyAnimatorListener listener) {
            view.setTag(android.support.v4.view.ViewPropertyAnimatorCompat.LISTENER_TAG_ID, listener);
            android.support.v4.view.ViewPropertyAnimatorCompatICS.setListener(view, new android.support.v4.view.ViewPropertyAnimatorCompat.ICSViewPropertyAnimatorCompatImpl.MyVpaListener(vpa));
        }

        public void withEndAction(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, java.lang.Runnable runnable) {
            android.support.v4.view.ViewPropertyAnimatorCompatICS.setListener(view, new android.support.v4.view.ViewPropertyAnimatorCompat.ICSViewPropertyAnimatorCompatImpl.MyVpaListener(vpa));
            vpa.mEndAction = runnable;
        }

        public void withStartAction(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, java.lang.Runnable runnable) {
            android.support.v4.view.ViewPropertyAnimatorCompatICS.setListener(view, new android.support.v4.view.ViewPropertyAnimatorCompat.ICSViewPropertyAnimatorCompatImpl.MyVpaListener(vpa));
            vpa.mStartAction = runnable;
        }

        public void withLayer(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view) {
            vpa.mOldLayerType = android.support.v4.view.ViewCompat.getLayerType(view);
            android.support.v4.view.ViewPropertyAnimatorCompatICS.setListener(view, new android.support.v4.view.ViewPropertyAnimatorCompat.ICSViewPropertyAnimatorCompatImpl.MyVpaListener(vpa));
        }
    }

    static class JBMr2ViewPropertyAnimatorCompatImpl extends android.support.v4.view.ViewPropertyAnimatorCompat.JBViewPropertyAnimatorCompatImpl {
        JBMr2ViewPropertyAnimatorCompatImpl() {
        }

        public android.view.animation.Interpolator getInterpolator(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view) {
            return android.support.v4.view.ViewPropertyAnimatorCompatJellybeanMr2.getInterpolator(view);
        }
    }

    static class JBViewPropertyAnimatorCompatImpl extends android.support.v4.view.ViewPropertyAnimatorCompat.ICSViewPropertyAnimatorCompatImpl {
        JBViewPropertyAnimatorCompatImpl() {
        }

        public void setListener(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, android.support.v4.view.ViewPropertyAnimatorListener listener) {
            android.support.v4.view.ViewPropertyAnimatorCompatJB.setListener(view, listener);
        }

        public void withStartAction(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, java.lang.Runnable runnable) {
            android.support.v4.view.ViewPropertyAnimatorCompatJB.withStartAction(view, runnable);
        }

        public void withEndAction(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, java.lang.Runnable runnable) {
            android.support.v4.view.ViewPropertyAnimatorCompatJB.withEndAction(view, runnable);
        }

        public void withLayer(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view) {
            android.support.v4.view.ViewPropertyAnimatorCompatJB.withLayer(view);
        }
    }

    static class KitKatViewPropertyAnimatorCompatImpl extends android.support.v4.view.ViewPropertyAnimatorCompat.JBMr2ViewPropertyAnimatorCompatImpl {
        KitKatViewPropertyAnimatorCompatImpl() {
        }

        public void setUpdateListener(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, android.support.v4.view.ViewPropertyAnimatorUpdateListener listener) {
            android.support.v4.view.ViewPropertyAnimatorCompatKK.setUpdateListener(view, listener);
        }
    }

    static class LollipopViewPropertyAnimatorCompatImpl extends android.support.v4.view.ViewPropertyAnimatorCompat.KitKatViewPropertyAnimatorCompatImpl {
        LollipopViewPropertyAnimatorCompatImpl() {
        }

        public void translationZ(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, float value) {
            android.support.v4.view.ViewPropertyAnimatorCompatLollipop.translationZ(view, value);
        }

        public void translationZBy(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, float value) {
            android.support.v4.view.ViewPropertyAnimatorCompatLollipop.translationZBy(view, value);
        }

        public void z(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, float value) {
            android.support.v4.view.ViewPropertyAnimatorCompatLollipop.z(view, value);
        }

        public void zBy(android.support.v4.view.ViewPropertyAnimatorCompat vpa, android.view.View view, float value) {
            android.support.v4.view.ViewPropertyAnimatorCompatLollipop.zBy(view, value);
        }
    }

    interface ViewPropertyAnimatorCompatImpl {
        void alpha(android.support.v4.view.ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, android.view.View view, float f);

        void alphaBy(android.support.v4.view.ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, android.view.View view, float f);

        void cancel(android.support.v4.view.ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, android.view.View view);

        long getDuration(android.support.v4.view.ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, android.view.View view);

        android.view.animation.Interpolator getInterpolator(android.support.v4.view.ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, android.view.View view);

        long getStartDelay(android.support.v4.view.ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, android.view.View view);

        void rotation(android.support.v4.view.ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, android.view.View view, float f);

        void rotationBy(android.support.v4.view.ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, android.view.View view, float f);

        void rotationX(android.support.v4.view.ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, android.view.View view, float f);

        void rotationXBy(android.support.v4.view.ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, android.view.View view, float f);

        void rotationY(android.support.v4.view.ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, android.view.View view, float f);

        void rotationYBy(android.support.v4.view.ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, android.view.View view, float f);

        void scaleX(android.support.v4.view.ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, android.view.View view, float f);

        void scaleXBy(android.support.v4.view.ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, android.view.View view, float f);

        void scaleY(android.support.v4.view.ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, android.view.View view, float f);

        void scaleYBy(android.support.v4.view.ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, android.view.View view, float f);

        void setDuration(android.support.v4.view.ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, android.view.View view, long j);

        void setInterpolator(android.support.v4.view.ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, android.view.View view, android.view.animation.Interpolator interpolator);

        void setListener(android.support.v4.view.ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, android.view.View view, android.support.v4.view.ViewPropertyAnimatorListener viewPropertyAnimatorListener);

        void setStartDelay(android.support.v4.view.ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, android.view.View view, long j);

        void setUpdateListener(android.support.v4.view.ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, android.view.View view, android.support.v4.view.ViewPropertyAnimatorUpdateListener viewPropertyAnimatorUpdateListener);

        void start(android.support.v4.view.ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, android.view.View view);

        void translationX(android.support.v4.view.ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, android.view.View view, float f);

        void translationXBy(android.support.v4.view.ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, android.view.View view, float f);

        void translationY(android.support.v4.view.ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, android.view.View view, float f);

        void translationYBy(android.support.v4.view.ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, android.view.View view, float f);

        void translationZ(android.support.v4.view.ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, android.view.View view, float f);

        void translationZBy(android.support.v4.view.ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, android.view.View view, float f);

        void withEndAction(android.support.v4.view.ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, android.view.View view, java.lang.Runnable runnable);

        void withLayer(android.support.v4.view.ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, android.view.View view);

        void withStartAction(android.support.v4.view.ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, android.view.View view, java.lang.Runnable runnable);

        void x(android.support.v4.view.ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, android.view.View view, float f);

        void xBy(android.support.v4.view.ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, android.view.View view, float f);

        void y(android.support.v4.view.ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, android.view.View view, float f);

        void yBy(android.support.v4.view.ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, android.view.View view, float f);

        void z(android.support.v4.view.ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, android.view.View view, float f);

        void zBy(android.support.v4.view.ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, android.view.View view, float f);
    }

    ViewPropertyAnimatorCompat(android.view.View view) {
        this.mView = new java.lang.ref.WeakReference<>(view);
    }

    static {
        int version = android.os.Build.VERSION.SDK_INT;
        if (version >= 21) {
            IMPL = new android.support.v4.view.ViewPropertyAnimatorCompat.LollipopViewPropertyAnimatorCompatImpl();
        } else if (version >= 19) {
            IMPL = new android.support.v4.view.ViewPropertyAnimatorCompat.KitKatViewPropertyAnimatorCompatImpl();
        } else if (version >= 18) {
            IMPL = new android.support.v4.view.ViewPropertyAnimatorCompat.JBMr2ViewPropertyAnimatorCompatImpl();
        } else if (version >= 16) {
            IMPL = new android.support.v4.view.ViewPropertyAnimatorCompat.JBViewPropertyAnimatorCompatImpl();
        } else if (version >= 14) {
            IMPL = new android.support.v4.view.ViewPropertyAnimatorCompat.ICSViewPropertyAnimatorCompatImpl();
        } else {
            IMPL = new android.support.v4.view.ViewPropertyAnimatorCompat.BaseViewPropertyAnimatorCompatImpl();
        }
    }

    public android.support.v4.view.ViewPropertyAnimatorCompat setDuration(long value) {
        android.view.View view = (android.view.View) this.mView.get();
        if (view != null) {
            IMPL.setDuration(this, view, value);
        }
        return this;
    }

    public android.support.v4.view.ViewPropertyAnimatorCompat alpha(float value) {
        android.view.View view = (android.view.View) this.mView.get();
        if (view != null) {
            IMPL.alpha(this, view, value);
        }
        return this;
    }

    public android.support.v4.view.ViewPropertyAnimatorCompat alphaBy(float value) {
        android.view.View view = (android.view.View) this.mView.get();
        if (view != null) {
            IMPL.alphaBy(this, view, value);
        }
        return this;
    }

    public android.support.v4.view.ViewPropertyAnimatorCompat translationX(float value) {
        android.view.View view = (android.view.View) this.mView.get();
        if (view != null) {
            IMPL.translationX(this, view, value);
        }
        return this;
    }

    public android.support.v4.view.ViewPropertyAnimatorCompat translationY(float value) {
        android.view.View view = (android.view.View) this.mView.get();
        if (view != null) {
            IMPL.translationY(this, view, value);
        }
        return this;
    }

    public android.support.v4.view.ViewPropertyAnimatorCompat withEndAction(java.lang.Runnable runnable) {
        android.view.View view = (android.view.View) this.mView.get();
        if (view != null) {
            IMPL.withEndAction(this, view, runnable);
        }
        return this;
    }

    public long getDuration() {
        android.view.View view = (android.view.View) this.mView.get();
        if (view != null) {
            return IMPL.getDuration(this, view);
        }
        return 0;
    }

    public android.support.v4.view.ViewPropertyAnimatorCompat setInterpolator(android.view.animation.Interpolator value) {
        android.view.View view = (android.view.View) this.mView.get();
        if (view != null) {
            IMPL.setInterpolator(this, view, value);
        }
        return this;
    }

    public android.view.animation.Interpolator getInterpolator() {
        android.view.View view = (android.view.View) this.mView.get();
        if (view != null) {
            return IMPL.getInterpolator(this, view);
        }
        return null;
    }

    public android.support.v4.view.ViewPropertyAnimatorCompat setStartDelay(long value) {
        android.view.View view = (android.view.View) this.mView.get();
        if (view != null) {
            IMPL.setStartDelay(this, view, value);
        }
        return this;
    }

    public long getStartDelay() {
        android.view.View view = (android.view.View) this.mView.get();
        if (view != null) {
            return IMPL.getStartDelay(this, view);
        }
        return 0;
    }

    public android.support.v4.view.ViewPropertyAnimatorCompat rotation(float value) {
        android.view.View view = (android.view.View) this.mView.get();
        if (view != null) {
            IMPL.rotation(this, view, value);
        }
        return this;
    }

    public android.support.v4.view.ViewPropertyAnimatorCompat rotationBy(float value) {
        android.view.View view = (android.view.View) this.mView.get();
        if (view != null) {
            IMPL.rotationBy(this, view, value);
        }
        return this;
    }

    public android.support.v4.view.ViewPropertyAnimatorCompat rotationX(float value) {
        android.view.View view = (android.view.View) this.mView.get();
        if (view != null) {
            IMPL.rotationX(this, view, value);
        }
        return this;
    }

    public android.support.v4.view.ViewPropertyAnimatorCompat rotationXBy(float value) {
        android.view.View view = (android.view.View) this.mView.get();
        if (view != null) {
            IMPL.rotationXBy(this, view, value);
        }
        return this;
    }

    public android.support.v4.view.ViewPropertyAnimatorCompat rotationY(float value) {
        android.view.View view = (android.view.View) this.mView.get();
        if (view != null) {
            IMPL.rotationY(this, view, value);
        }
        return this;
    }

    public android.support.v4.view.ViewPropertyAnimatorCompat rotationYBy(float value) {
        android.view.View view = (android.view.View) this.mView.get();
        if (view != null) {
            IMPL.rotationYBy(this, view, value);
        }
        return this;
    }

    public android.support.v4.view.ViewPropertyAnimatorCompat scaleX(float value) {
        android.view.View view = (android.view.View) this.mView.get();
        if (view != null) {
            IMPL.scaleX(this, view, value);
        }
        return this;
    }

    public android.support.v4.view.ViewPropertyAnimatorCompat scaleXBy(float value) {
        android.view.View view = (android.view.View) this.mView.get();
        if (view != null) {
            IMPL.scaleXBy(this, view, value);
        }
        return this;
    }

    public android.support.v4.view.ViewPropertyAnimatorCompat scaleY(float value) {
        android.view.View view = (android.view.View) this.mView.get();
        if (view != null) {
            IMPL.scaleY(this, view, value);
        }
        return this;
    }

    public android.support.v4.view.ViewPropertyAnimatorCompat scaleYBy(float value) {
        android.view.View view = (android.view.View) this.mView.get();
        if (view != null) {
            IMPL.scaleYBy(this, view, value);
        }
        return this;
    }

    public void cancel() {
        android.view.View view = (android.view.View) this.mView.get();
        if (view != null) {
            IMPL.cancel(this, view);
        }
    }

    public android.support.v4.view.ViewPropertyAnimatorCompat x(float value) {
        android.view.View view = (android.view.View) this.mView.get();
        if (view != null) {
            IMPL.x(this, view, value);
        }
        return this;
    }

    public android.support.v4.view.ViewPropertyAnimatorCompat xBy(float value) {
        android.view.View view = (android.view.View) this.mView.get();
        if (view != null) {
            IMPL.xBy(this, view, value);
        }
        return this;
    }

    public android.support.v4.view.ViewPropertyAnimatorCompat y(float value) {
        android.view.View view = (android.view.View) this.mView.get();
        if (view != null) {
            IMPL.y(this, view, value);
        }
        return this;
    }

    public android.support.v4.view.ViewPropertyAnimatorCompat yBy(float value) {
        android.view.View view = (android.view.View) this.mView.get();
        if (view != null) {
            IMPL.yBy(this, view, value);
        }
        return this;
    }

    public android.support.v4.view.ViewPropertyAnimatorCompat translationXBy(float value) {
        android.view.View view = (android.view.View) this.mView.get();
        if (view != null) {
            IMPL.translationXBy(this, view, value);
        }
        return this;
    }

    public android.support.v4.view.ViewPropertyAnimatorCompat translationYBy(float value) {
        android.view.View view = (android.view.View) this.mView.get();
        if (view != null) {
            IMPL.translationYBy(this, view, value);
        }
        return this;
    }

    public android.support.v4.view.ViewPropertyAnimatorCompat translationZBy(float value) {
        android.view.View view = (android.view.View) this.mView.get();
        if (view != null) {
            IMPL.translationZBy(this, view, value);
        }
        return this;
    }

    public android.support.v4.view.ViewPropertyAnimatorCompat translationZ(float value) {
        android.view.View view = (android.view.View) this.mView.get();
        if (view != null) {
            IMPL.translationZ(this, view, value);
        }
        return this;
    }

    public android.support.v4.view.ViewPropertyAnimatorCompat z(float value) {
        android.view.View view = (android.view.View) this.mView.get();
        if (view != null) {
            IMPL.z(this, view, value);
        }
        return this;
    }

    public android.support.v4.view.ViewPropertyAnimatorCompat zBy(float value) {
        android.view.View view = (android.view.View) this.mView.get();
        if (view != null) {
            IMPL.zBy(this, view, value);
        }
        return this;
    }

    public void start() {
        android.view.View view = (android.view.View) this.mView.get();
        if (view != null) {
            IMPL.start(this, view);
        }
    }

    public android.support.v4.view.ViewPropertyAnimatorCompat withLayer() {
        android.view.View view = (android.view.View) this.mView.get();
        if (view != null) {
            IMPL.withLayer(this, view);
        }
        return this;
    }

    public android.support.v4.view.ViewPropertyAnimatorCompat withStartAction(java.lang.Runnable runnable) {
        android.view.View view = (android.view.View) this.mView.get();
        if (view != null) {
            IMPL.withStartAction(this, view, runnable);
        }
        return this;
    }

    public android.support.v4.view.ViewPropertyAnimatorCompat setListener(android.support.v4.view.ViewPropertyAnimatorListener listener) {
        android.view.View view = (android.view.View) this.mView.get();
        if (view != null) {
            IMPL.setListener(this, view, listener);
        }
        return this;
    }

    public android.support.v4.view.ViewPropertyAnimatorCompat setUpdateListener(android.support.v4.view.ViewPropertyAnimatorUpdateListener listener) {
        android.view.View view = (android.view.View) this.mView.get();
        if (view != null) {
            IMPL.setUpdateListener(this, view, listener);
        }
        return this;
    }
}
