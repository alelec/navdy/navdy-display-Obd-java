package android.support.v4.view;

@android.annotation.TargetApi(24)
@android.support.annotation.RequiresApi(24)
class ViewCompatApi24 {
    ViewCompatApi24() {
    }

    public static void setPointerIcon(android.view.View view, java.lang.Object pointerIcon) {
        view.setPointerIcon((android.view.PointerIcon) pointerIcon);
    }
}
