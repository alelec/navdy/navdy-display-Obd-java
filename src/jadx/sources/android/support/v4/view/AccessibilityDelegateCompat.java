package android.support.v4.view;

public class AccessibilityDelegateCompat {
    private static final java.lang.Object DEFAULT_DELEGATE = IMPL.newAccessiblityDelegateDefaultImpl();
    private static final android.support.v4.view.AccessibilityDelegateCompat.AccessibilityDelegateImpl IMPL;
    final java.lang.Object mBridge = IMPL.newAccessiblityDelegateBridge(this);

    static class AccessibilityDelegateIcsImpl extends android.support.v4.view.AccessibilityDelegateCompat.AccessibilityDelegateStubImpl {
        AccessibilityDelegateIcsImpl() {
        }

        public java.lang.Object newAccessiblityDelegateDefaultImpl() {
            return android.support.v4.view.AccessibilityDelegateCompatIcs.newAccessibilityDelegateDefaultImpl();
        }

        public java.lang.Object newAccessiblityDelegateBridge(final android.support.v4.view.AccessibilityDelegateCompat compat) {
            return android.support.v4.view.AccessibilityDelegateCompatIcs.newAccessibilityDelegateBridge(new android.support.v4.view.AccessibilityDelegateCompatIcs.AccessibilityDelegateBridge() {
                public boolean dispatchPopulateAccessibilityEvent(android.view.View host, android.view.accessibility.AccessibilityEvent event) {
                    return compat.dispatchPopulateAccessibilityEvent(host, event);
                }

                public void onInitializeAccessibilityEvent(android.view.View host, android.view.accessibility.AccessibilityEvent event) {
                    compat.onInitializeAccessibilityEvent(host, event);
                }

                public void onInitializeAccessibilityNodeInfo(android.view.View host, java.lang.Object info) {
                    compat.onInitializeAccessibilityNodeInfo(host, new android.support.v4.view.accessibility.AccessibilityNodeInfoCompat(info));
                }

                public void onPopulateAccessibilityEvent(android.view.View host, android.view.accessibility.AccessibilityEvent event) {
                    compat.onPopulateAccessibilityEvent(host, event);
                }

                public boolean onRequestSendAccessibilityEvent(android.view.ViewGroup host, android.view.View child, android.view.accessibility.AccessibilityEvent event) {
                    return compat.onRequestSendAccessibilityEvent(host, child, event);
                }

                public void sendAccessibilityEvent(android.view.View host, int eventType) {
                    compat.sendAccessibilityEvent(host, eventType);
                }

                public void sendAccessibilityEventUnchecked(android.view.View host, android.view.accessibility.AccessibilityEvent event) {
                    compat.sendAccessibilityEventUnchecked(host, event);
                }
            });
        }

        public boolean dispatchPopulateAccessibilityEvent(java.lang.Object delegate, android.view.View host, android.view.accessibility.AccessibilityEvent event) {
            return android.support.v4.view.AccessibilityDelegateCompatIcs.dispatchPopulateAccessibilityEvent(delegate, host, event);
        }

        public void onInitializeAccessibilityEvent(java.lang.Object delegate, android.view.View host, android.view.accessibility.AccessibilityEvent event) {
            android.support.v4.view.AccessibilityDelegateCompatIcs.onInitializeAccessibilityEvent(delegate, host, event);
        }

        public void onInitializeAccessibilityNodeInfo(java.lang.Object delegate, android.view.View host, android.support.v4.view.accessibility.AccessibilityNodeInfoCompat info) {
            android.support.v4.view.AccessibilityDelegateCompatIcs.onInitializeAccessibilityNodeInfo(delegate, host, info.getInfo());
        }

        public void onPopulateAccessibilityEvent(java.lang.Object delegate, android.view.View host, android.view.accessibility.AccessibilityEvent event) {
            android.support.v4.view.AccessibilityDelegateCompatIcs.onPopulateAccessibilityEvent(delegate, host, event);
        }

        public boolean onRequestSendAccessibilityEvent(java.lang.Object delegate, android.view.ViewGroup host, android.view.View child, android.view.accessibility.AccessibilityEvent event) {
            return android.support.v4.view.AccessibilityDelegateCompatIcs.onRequestSendAccessibilityEvent(delegate, host, child, event);
        }

        public void sendAccessibilityEvent(java.lang.Object delegate, android.view.View host, int eventType) {
            android.support.v4.view.AccessibilityDelegateCompatIcs.sendAccessibilityEvent(delegate, host, eventType);
        }

        public void sendAccessibilityEventUnchecked(java.lang.Object delegate, android.view.View host, android.view.accessibility.AccessibilityEvent event) {
            android.support.v4.view.AccessibilityDelegateCompatIcs.sendAccessibilityEventUnchecked(delegate, host, event);
        }
    }

    interface AccessibilityDelegateImpl {
        boolean dispatchPopulateAccessibilityEvent(java.lang.Object obj, android.view.View view, android.view.accessibility.AccessibilityEvent accessibilityEvent);

        android.support.v4.view.accessibility.AccessibilityNodeProviderCompat getAccessibilityNodeProvider(java.lang.Object obj, android.view.View view);

        java.lang.Object newAccessiblityDelegateBridge(android.support.v4.view.AccessibilityDelegateCompat accessibilityDelegateCompat);

        java.lang.Object newAccessiblityDelegateDefaultImpl();

        void onInitializeAccessibilityEvent(java.lang.Object obj, android.view.View view, android.view.accessibility.AccessibilityEvent accessibilityEvent);

        void onInitializeAccessibilityNodeInfo(java.lang.Object obj, android.view.View view, android.support.v4.view.accessibility.AccessibilityNodeInfoCompat accessibilityNodeInfoCompat);

        void onPopulateAccessibilityEvent(java.lang.Object obj, android.view.View view, android.view.accessibility.AccessibilityEvent accessibilityEvent);

        boolean onRequestSendAccessibilityEvent(java.lang.Object obj, android.view.ViewGroup viewGroup, android.view.View view, android.view.accessibility.AccessibilityEvent accessibilityEvent);

        boolean performAccessibilityAction(java.lang.Object obj, android.view.View view, int i, android.os.Bundle bundle);

        void sendAccessibilityEvent(java.lang.Object obj, android.view.View view, int i);

        void sendAccessibilityEventUnchecked(java.lang.Object obj, android.view.View view, android.view.accessibility.AccessibilityEvent accessibilityEvent);
    }

    static class AccessibilityDelegateJellyBeanImpl extends android.support.v4.view.AccessibilityDelegateCompat.AccessibilityDelegateIcsImpl {
        AccessibilityDelegateJellyBeanImpl() {
        }

        public java.lang.Object newAccessiblityDelegateBridge(final android.support.v4.view.AccessibilityDelegateCompat compat) {
            return android.support.v4.view.AccessibilityDelegateCompatJellyBean.newAccessibilityDelegateBridge(new android.support.v4.view.AccessibilityDelegateCompatJellyBean.AccessibilityDelegateBridgeJellyBean() {
                public boolean dispatchPopulateAccessibilityEvent(android.view.View host, android.view.accessibility.AccessibilityEvent event) {
                    return compat.dispatchPopulateAccessibilityEvent(host, event);
                }

                public void onInitializeAccessibilityEvent(android.view.View host, android.view.accessibility.AccessibilityEvent event) {
                    compat.onInitializeAccessibilityEvent(host, event);
                }

                public void onInitializeAccessibilityNodeInfo(android.view.View host, java.lang.Object info) {
                    compat.onInitializeAccessibilityNodeInfo(host, new android.support.v4.view.accessibility.AccessibilityNodeInfoCompat(info));
                }

                public void onPopulateAccessibilityEvent(android.view.View host, android.view.accessibility.AccessibilityEvent event) {
                    compat.onPopulateAccessibilityEvent(host, event);
                }

                public boolean onRequestSendAccessibilityEvent(android.view.ViewGroup host, android.view.View child, android.view.accessibility.AccessibilityEvent event) {
                    return compat.onRequestSendAccessibilityEvent(host, child, event);
                }

                public void sendAccessibilityEvent(android.view.View host, int eventType) {
                    compat.sendAccessibilityEvent(host, eventType);
                }

                public void sendAccessibilityEventUnchecked(android.view.View host, android.view.accessibility.AccessibilityEvent event) {
                    compat.sendAccessibilityEventUnchecked(host, event);
                }

                public java.lang.Object getAccessibilityNodeProvider(android.view.View host) {
                    android.support.v4.view.accessibility.AccessibilityNodeProviderCompat provider = compat.getAccessibilityNodeProvider(host);
                    if (provider != null) {
                        return provider.getProvider();
                    }
                    return null;
                }

                public boolean performAccessibilityAction(android.view.View host, int action, android.os.Bundle args) {
                    return compat.performAccessibilityAction(host, action, args);
                }
            });
        }

        public android.support.v4.view.accessibility.AccessibilityNodeProviderCompat getAccessibilityNodeProvider(java.lang.Object delegate, android.view.View host) {
            java.lang.Object provider = android.support.v4.view.AccessibilityDelegateCompatJellyBean.getAccessibilityNodeProvider(delegate, host);
            if (provider != null) {
                return new android.support.v4.view.accessibility.AccessibilityNodeProviderCompat(provider);
            }
            return null;
        }

        public boolean performAccessibilityAction(java.lang.Object delegate, android.view.View host, int action, android.os.Bundle args) {
            return android.support.v4.view.AccessibilityDelegateCompatJellyBean.performAccessibilityAction(delegate, host, action, args);
        }
    }

    static class AccessibilityDelegateStubImpl implements android.support.v4.view.AccessibilityDelegateCompat.AccessibilityDelegateImpl {
        AccessibilityDelegateStubImpl() {
        }

        public java.lang.Object newAccessiblityDelegateDefaultImpl() {
            return null;
        }

        public java.lang.Object newAccessiblityDelegateBridge(android.support.v4.view.AccessibilityDelegateCompat listener) {
            return null;
        }

        public boolean dispatchPopulateAccessibilityEvent(java.lang.Object delegate, android.view.View host, android.view.accessibility.AccessibilityEvent event) {
            return false;
        }

        public void onInitializeAccessibilityEvent(java.lang.Object delegate, android.view.View host, android.view.accessibility.AccessibilityEvent event) {
        }

        public void onInitializeAccessibilityNodeInfo(java.lang.Object delegate, android.view.View host, android.support.v4.view.accessibility.AccessibilityNodeInfoCompat info) {
        }

        public void onPopulateAccessibilityEvent(java.lang.Object delegate, android.view.View host, android.view.accessibility.AccessibilityEvent event) {
        }

        public boolean onRequestSendAccessibilityEvent(java.lang.Object delegate, android.view.ViewGroup host, android.view.View child, android.view.accessibility.AccessibilityEvent event) {
            return true;
        }

        public void sendAccessibilityEvent(java.lang.Object delegate, android.view.View host, int eventType) {
        }

        public void sendAccessibilityEventUnchecked(java.lang.Object delegate, android.view.View host, android.view.accessibility.AccessibilityEvent event) {
        }

        public android.support.v4.view.accessibility.AccessibilityNodeProviderCompat getAccessibilityNodeProvider(java.lang.Object delegate, android.view.View host) {
            return null;
        }

        public boolean performAccessibilityAction(java.lang.Object delegate, android.view.View host, int action, android.os.Bundle args) {
            return false;
        }
    }

    static {
        if (android.os.Build.VERSION.SDK_INT >= 16) {
            IMPL = new android.support.v4.view.AccessibilityDelegateCompat.AccessibilityDelegateJellyBeanImpl();
        } else if (android.os.Build.VERSION.SDK_INT >= 14) {
            IMPL = new android.support.v4.view.AccessibilityDelegateCompat.AccessibilityDelegateIcsImpl();
        } else {
            IMPL = new android.support.v4.view.AccessibilityDelegateCompat.AccessibilityDelegateStubImpl();
        }
    }

    /* access modifiers changed from: 0000 */
    public java.lang.Object getBridge() {
        return this.mBridge;
    }

    public void sendAccessibilityEvent(android.view.View host, int eventType) {
        IMPL.sendAccessibilityEvent(DEFAULT_DELEGATE, host, eventType);
    }

    public void sendAccessibilityEventUnchecked(android.view.View host, android.view.accessibility.AccessibilityEvent event) {
        IMPL.sendAccessibilityEventUnchecked(DEFAULT_DELEGATE, host, event);
    }

    public boolean dispatchPopulateAccessibilityEvent(android.view.View host, android.view.accessibility.AccessibilityEvent event) {
        return IMPL.dispatchPopulateAccessibilityEvent(DEFAULT_DELEGATE, host, event);
    }

    public void onPopulateAccessibilityEvent(android.view.View host, android.view.accessibility.AccessibilityEvent event) {
        IMPL.onPopulateAccessibilityEvent(DEFAULT_DELEGATE, host, event);
    }

    public void onInitializeAccessibilityEvent(android.view.View host, android.view.accessibility.AccessibilityEvent event) {
        IMPL.onInitializeAccessibilityEvent(DEFAULT_DELEGATE, host, event);
    }

    public void onInitializeAccessibilityNodeInfo(android.view.View host, android.support.v4.view.accessibility.AccessibilityNodeInfoCompat info) {
        IMPL.onInitializeAccessibilityNodeInfo(DEFAULT_DELEGATE, host, info);
    }

    public boolean onRequestSendAccessibilityEvent(android.view.ViewGroup host, android.view.View child, android.view.accessibility.AccessibilityEvent event) {
        return IMPL.onRequestSendAccessibilityEvent(DEFAULT_DELEGATE, host, child, event);
    }

    public android.support.v4.view.accessibility.AccessibilityNodeProviderCompat getAccessibilityNodeProvider(android.view.View host) {
        return IMPL.getAccessibilityNodeProvider(DEFAULT_DELEGATE, host);
    }

    public boolean performAccessibilityAction(android.view.View host, int action, android.os.Bundle args) {
        return IMPL.performAccessibilityAction(DEFAULT_DELEGATE, host, action, args);
    }
}
