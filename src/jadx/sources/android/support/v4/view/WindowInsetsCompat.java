package android.support.v4.view;

public class WindowInsetsCompat {
    private static final android.support.v4.view.WindowInsetsCompat.WindowInsetsCompatImpl IMPL;
    private final java.lang.Object mInsets;

    private static class WindowInsetsCompatApi20Impl extends android.support.v4.view.WindowInsetsCompat.WindowInsetsCompatBaseImpl {
        WindowInsetsCompatApi20Impl() {
        }

        public android.support.v4.view.WindowInsetsCompat consumeSystemWindowInsets(java.lang.Object insets) {
            return new android.support.v4.view.WindowInsetsCompat(android.support.v4.view.WindowInsetsCompatApi20.consumeSystemWindowInsets(insets));
        }

        public int getSystemWindowInsetBottom(java.lang.Object insets) {
            return android.support.v4.view.WindowInsetsCompatApi20.getSystemWindowInsetBottom(insets);
        }

        public int getSystemWindowInsetLeft(java.lang.Object insets) {
            return android.support.v4.view.WindowInsetsCompatApi20.getSystemWindowInsetLeft(insets);
        }

        public int getSystemWindowInsetRight(java.lang.Object insets) {
            return android.support.v4.view.WindowInsetsCompatApi20.getSystemWindowInsetRight(insets);
        }

        public int getSystemWindowInsetTop(java.lang.Object insets) {
            return android.support.v4.view.WindowInsetsCompatApi20.getSystemWindowInsetTop(insets);
        }

        public boolean hasInsets(java.lang.Object insets) {
            return android.support.v4.view.WindowInsetsCompatApi20.hasInsets(insets);
        }

        public boolean hasSystemWindowInsets(java.lang.Object insets) {
            return android.support.v4.view.WindowInsetsCompatApi20.hasSystemWindowInsets(insets);
        }

        public boolean isRound(java.lang.Object insets) {
            return android.support.v4.view.WindowInsetsCompatApi20.isRound(insets);
        }

        public android.support.v4.view.WindowInsetsCompat replaceSystemWindowInsets(java.lang.Object insets, int left, int top, int right, int bottom) {
            return new android.support.v4.view.WindowInsetsCompat(android.support.v4.view.WindowInsetsCompatApi20.replaceSystemWindowInsets(insets, left, top, right, bottom));
        }

        public java.lang.Object getSourceWindowInsets(java.lang.Object src) {
            return android.support.v4.view.WindowInsetsCompatApi20.getSourceWindowInsets(src);
        }
    }

    private static class WindowInsetsCompatApi21Impl extends android.support.v4.view.WindowInsetsCompat.WindowInsetsCompatApi20Impl {
        WindowInsetsCompatApi21Impl() {
        }

        public android.support.v4.view.WindowInsetsCompat consumeStableInsets(java.lang.Object insets) {
            return new android.support.v4.view.WindowInsetsCompat(android.support.v4.view.WindowInsetsCompatApi21.consumeStableInsets(insets));
        }

        public int getStableInsetBottom(java.lang.Object insets) {
            return android.support.v4.view.WindowInsetsCompatApi21.getStableInsetBottom(insets);
        }

        public int getStableInsetLeft(java.lang.Object insets) {
            return android.support.v4.view.WindowInsetsCompatApi21.getStableInsetLeft(insets);
        }

        public int getStableInsetRight(java.lang.Object insets) {
            return android.support.v4.view.WindowInsetsCompatApi21.getStableInsetRight(insets);
        }

        public int getStableInsetTop(java.lang.Object insets) {
            return android.support.v4.view.WindowInsetsCompatApi21.getStableInsetTop(insets);
        }

        public boolean hasStableInsets(java.lang.Object insets) {
            return android.support.v4.view.WindowInsetsCompatApi21.hasStableInsets(insets);
        }

        public boolean isConsumed(java.lang.Object insets) {
            return android.support.v4.view.WindowInsetsCompatApi21.isConsumed(insets);
        }

        public android.support.v4.view.WindowInsetsCompat replaceSystemWindowInsets(java.lang.Object insets, android.graphics.Rect systemWindowInsets) {
            return new android.support.v4.view.WindowInsetsCompat(android.support.v4.view.WindowInsetsCompatApi21.replaceSystemWindowInsets(insets, systemWindowInsets));
        }
    }

    private static class WindowInsetsCompatBaseImpl implements android.support.v4.view.WindowInsetsCompat.WindowInsetsCompatImpl {
        WindowInsetsCompatBaseImpl() {
        }

        public int getSystemWindowInsetLeft(java.lang.Object insets) {
            return 0;
        }

        public int getSystemWindowInsetTop(java.lang.Object insets) {
            return 0;
        }

        public int getSystemWindowInsetRight(java.lang.Object insets) {
            return 0;
        }

        public int getSystemWindowInsetBottom(java.lang.Object insets) {
            return 0;
        }

        public boolean hasSystemWindowInsets(java.lang.Object insets) {
            return false;
        }

        public boolean hasInsets(java.lang.Object insets) {
            return false;
        }

        public boolean isConsumed(java.lang.Object insets) {
            return false;
        }

        public boolean isRound(java.lang.Object insets) {
            return false;
        }

        public android.support.v4.view.WindowInsetsCompat consumeSystemWindowInsets(java.lang.Object insets) {
            return null;
        }

        public android.support.v4.view.WindowInsetsCompat replaceSystemWindowInsets(java.lang.Object insets, int left, int top, int right, int bottom) {
            return null;
        }

        public android.support.v4.view.WindowInsetsCompat replaceSystemWindowInsets(java.lang.Object insets, android.graphics.Rect systemWindowInsets) {
            return null;
        }

        public int getStableInsetTop(java.lang.Object insets) {
            return 0;
        }

        public int getStableInsetLeft(java.lang.Object insets) {
            return 0;
        }

        public int getStableInsetRight(java.lang.Object insets) {
            return 0;
        }

        public int getStableInsetBottom(java.lang.Object insets) {
            return 0;
        }

        public boolean hasStableInsets(java.lang.Object insets) {
            return false;
        }

        public android.support.v4.view.WindowInsetsCompat consumeStableInsets(java.lang.Object insets) {
            return null;
        }

        public java.lang.Object getSourceWindowInsets(java.lang.Object src) {
            return null;
        }
    }

    private interface WindowInsetsCompatImpl {
        android.support.v4.view.WindowInsetsCompat consumeStableInsets(java.lang.Object obj);

        android.support.v4.view.WindowInsetsCompat consumeSystemWindowInsets(java.lang.Object obj);

        java.lang.Object getSourceWindowInsets(java.lang.Object obj);

        int getStableInsetBottom(java.lang.Object obj);

        int getStableInsetLeft(java.lang.Object obj);

        int getStableInsetRight(java.lang.Object obj);

        int getStableInsetTop(java.lang.Object obj);

        int getSystemWindowInsetBottom(java.lang.Object obj);

        int getSystemWindowInsetLeft(java.lang.Object obj);

        int getSystemWindowInsetRight(java.lang.Object obj);

        int getSystemWindowInsetTop(java.lang.Object obj);

        boolean hasInsets(java.lang.Object obj);

        boolean hasStableInsets(java.lang.Object obj);

        boolean hasSystemWindowInsets(java.lang.Object obj);

        boolean isConsumed(java.lang.Object obj);

        boolean isRound(java.lang.Object obj);

        android.support.v4.view.WindowInsetsCompat replaceSystemWindowInsets(java.lang.Object obj, int i, int i2, int i3, int i4);

        android.support.v4.view.WindowInsetsCompat replaceSystemWindowInsets(java.lang.Object obj, android.graphics.Rect rect);
    }

    static {
        int version = android.os.Build.VERSION.SDK_INT;
        if (version >= 21) {
            IMPL = new android.support.v4.view.WindowInsetsCompat.WindowInsetsCompatApi21Impl();
        } else if (version >= 20) {
            IMPL = new android.support.v4.view.WindowInsetsCompat.WindowInsetsCompatApi20Impl();
        } else {
            IMPL = new android.support.v4.view.WindowInsetsCompat.WindowInsetsCompatBaseImpl();
        }
    }

    WindowInsetsCompat(java.lang.Object insets) {
        this.mInsets = insets;
    }

    public WindowInsetsCompat(android.support.v4.view.WindowInsetsCompat src) {
        this.mInsets = src == null ? null : IMPL.getSourceWindowInsets(src.mInsets);
    }

    public int getSystemWindowInsetLeft() {
        return IMPL.getSystemWindowInsetLeft(this.mInsets);
    }

    public int getSystemWindowInsetTop() {
        return IMPL.getSystemWindowInsetTop(this.mInsets);
    }

    public int getSystemWindowInsetRight() {
        return IMPL.getSystemWindowInsetRight(this.mInsets);
    }

    public int getSystemWindowInsetBottom() {
        return IMPL.getSystemWindowInsetBottom(this.mInsets);
    }

    public boolean hasSystemWindowInsets() {
        return IMPL.hasSystemWindowInsets(this.mInsets);
    }

    public boolean hasInsets() {
        return IMPL.hasInsets(this.mInsets);
    }

    public boolean isConsumed() {
        return IMPL.isConsumed(this.mInsets);
    }

    public boolean isRound() {
        return IMPL.isRound(this.mInsets);
    }

    public android.support.v4.view.WindowInsetsCompat consumeSystemWindowInsets() {
        return IMPL.consumeSystemWindowInsets(this.mInsets);
    }

    public android.support.v4.view.WindowInsetsCompat replaceSystemWindowInsets(int left, int top, int right, int bottom) {
        return IMPL.replaceSystemWindowInsets(this.mInsets, left, top, right, bottom);
    }

    public android.support.v4.view.WindowInsetsCompat replaceSystemWindowInsets(android.graphics.Rect systemWindowInsets) {
        return IMPL.replaceSystemWindowInsets(this.mInsets, systemWindowInsets);
    }

    public int getStableInsetTop() {
        return IMPL.getStableInsetTop(this.mInsets);
    }

    public int getStableInsetLeft() {
        return IMPL.getStableInsetLeft(this.mInsets);
    }

    public int getStableInsetRight() {
        return IMPL.getStableInsetRight(this.mInsets);
    }

    public int getStableInsetBottom() {
        return IMPL.getStableInsetBottom(this.mInsets);
    }

    public boolean hasStableInsets() {
        return IMPL.hasStableInsets(this.mInsets);
    }

    public android.support.v4.view.WindowInsetsCompat consumeStableInsets() {
        return IMPL.consumeStableInsets(this.mInsets);
    }

    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        android.support.v4.view.WindowInsetsCompat other = (android.support.v4.view.WindowInsetsCompat) o;
        if (this.mInsets != null) {
            return this.mInsets.equals(other.mInsets);
        }
        if (other.mInsets != null) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        if (this.mInsets == null) {
            return 0;
        }
        return this.mInsets.hashCode();
    }

    static android.support.v4.view.WindowInsetsCompat wrap(java.lang.Object insets) {
        if (insets == null) {
            return null;
        }
        return new android.support.v4.view.WindowInsetsCompat(insets);
    }

    static java.lang.Object unwrap(android.support.v4.view.WindowInsetsCompat insets) {
        if (insets == null) {
            return null;
        }
        return insets.mInsets;
    }
}
