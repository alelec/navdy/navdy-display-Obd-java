package android.support.v4.view;

@android.annotation.TargetApi(21)
@android.support.annotation.RequiresApi(21)
class ViewGroupCompatLollipop {
    ViewGroupCompatLollipop() {
    }

    public static void setTransitionGroup(android.view.ViewGroup group, boolean isTransitionGroup) {
        group.setTransitionGroup(isTransitionGroup);
    }

    public static boolean isTransitionGroup(android.view.ViewGroup group) {
        return group.isTransitionGroup();
    }

    public static int getNestedScrollAxes(android.view.ViewGroup group) {
        return group.getNestedScrollAxes();
    }
}
