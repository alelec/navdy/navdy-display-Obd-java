package android.support.v4.view;

public final class ViewConfigurationCompat {
    static final android.support.v4.view.ViewConfigurationCompat.ViewConfigurationVersionImpl IMPL;

    static class BaseViewConfigurationVersionImpl implements android.support.v4.view.ViewConfigurationCompat.ViewConfigurationVersionImpl {
        BaseViewConfigurationVersionImpl() {
        }

        public boolean hasPermanentMenuKey(android.view.ViewConfiguration config) {
            return true;
        }
    }

    static class HoneycombViewConfigurationVersionImpl extends android.support.v4.view.ViewConfigurationCompat.BaseViewConfigurationVersionImpl {
        HoneycombViewConfigurationVersionImpl() {
        }

        public boolean hasPermanentMenuKey(android.view.ViewConfiguration config) {
            return false;
        }
    }

    static class IcsViewConfigurationVersionImpl extends android.support.v4.view.ViewConfigurationCompat.HoneycombViewConfigurationVersionImpl {
        IcsViewConfigurationVersionImpl() {
        }

        public boolean hasPermanentMenuKey(android.view.ViewConfiguration config) {
            return android.support.v4.view.ViewConfigurationCompatICS.hasPermanentMenuKey(config);
        }
    }

    interface ViewConfigurationVersionImpl {
        boolean hasPermanentMenuKey(android.view.ViewConfiguration viewConfiguration);
    }

    static {
        if (android.os.Build.VERSION.SDK_INT >= 14) {
            IMPL = new android.support.v4.view.ViewConfigurationCompat.IcsViewConfigurationVersionImpl();
        } else if (android.os.Build.VERSION.SDK_INT >= 11) {
            IMPL = new android.support.v4.view.ViewConfigurationCompat.HoneycombViewConfigurationVersionImpl();
        } else {
            IMPL = new android.support.v4.view.ViewConfigurationCompat.BaseViewConfigurationVersionImpl();
        }
    }

    @java.lang.Deprecated
    public static int getScaledPagingTouchSlop(android.view.ViewConfiguration config) {
        return config.getScaledPagingTouchSlop();
    }

    public static boolean hasPermanentMenuKey(android.view.ViewConfiguration config) {
        return IMPL.hasPermanentMenuKey(config);
    }

    private ViewConfigurationCompat() {
    }
}
