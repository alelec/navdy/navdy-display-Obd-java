package android.support.v4.accessibilityservice;

@android.annotation.TargetApi(18)
@android.support.annotation.RequiresApi(18)
class AccessibilityServiceInfoCompatJellyBeanMr2 {
    AccessibilityServiceInfoCompatJellyBeanMr2() {
    }

    public static int getCapabilities(android.accessibilityservice.AccessibilityServiceInfo info) {
        return info.getCapabilities();
    }
}
