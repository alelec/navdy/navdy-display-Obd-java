package android.support.v4.accessibilityservice;

@android.annotation.TargetApi(16)
@android.support.annotation.RequiresApi(16)
class AccessibilityServiceInfoCompatJellyBean {
    AccessibilityServiceInfoCompatJellyBean() {
    }

    public static java.lang.String loadDescription(android.accessibilityservice.AccessibilityServiceInfo info, android.content.pm.PackageManager pm) {
        return info.loadDescription(pm);
    }
}
