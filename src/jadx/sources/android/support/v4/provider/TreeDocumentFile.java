package android.support.v4.provider;

class TreeDocumentFile extends android.support.v4.provider.DocumentFile {
    private android.content.Context mContext;
    private android.net.Uri mUri;

    TreeDocumentFile(android.support.v4.provider.DocumentFile parent, android.content.Context context, android.net.Uri uri) {
        super(parent);
        this.mContext = context;
        this.mUri = uri;
    }

    public android.support.v4.provider.DocumentFile createFile(java.lang.String mimeType, java.lang.String displayName) {
        android.net.Uri result = android.support.v4.provider.DocumentsContractApi21.createFile(this.mContext, this.mUri, mimeType, displayName);
        if (result != null) {
            return new android.support.v4.provider.TreeDocumentFile(this, this.mContext, result);
        }
        return null;
    }

    public android.support.v4.provider.DocumentFile createDirectory(java.lang.String displayName) {
        android.net.Uri result = android.support.v4.provider.DocumentsContractApi21.createDirectory(this.mContext, this.mUri, displayName);
        if (result != null) {
            return new android.support.v4.provider.TreeDocumentFile(this, this.mContext, result);
        }
        return null;
    }

    public android.net.Uri getUri() {
        return this.mUri;
    }

    public java.lang.String getName() {
        return android.support.v4.provider.DocumentsContractApi19.getName(this.mContext, this.mUri);
    }

    public java.lang.String getType() {
        return android.support.v4.provider.DocumentsContractApi19.getType(this.mContext, this.mUri);
    }

    public boolean isDirectory() {
        return android.support.v4.provider.DocumentsContractApi19.isDirectory(this.mContext, this.mUri);
    }

    public boolean isFile() {
        return android.support.v4.provider.DocumentsContractApi19.isFile(this.mContext, this.mUri);
    }

    public boolean isVirtual() {
        return android.support.v4.provider.DocumentsContractApi19.isVirtual(this.mContext, this.mUri);
    }

    public long lastModified() {
        return android.support.v4.provider.DocumentsContractApi19.lastModified(this.mContext, this.mUri);
    }

    public long length() {
        return android.support.v4.provider.DocumentsContractApi19.length(this.mContext, this.mUri);
    }

    public boolean canRead() {
        return android.support.v4.provider.DocumentsContractApi19.canRead(this.mContext, this.mUri);
    }

    public boolean canWrite() {
        return android.support.v4.provider.DocumentsContractApi19.canWrite(this.mContext, this.mUri);
    }

    public boolean delete() {
        return android.support.v4.provider.DocumentsContractApi19.delete(this.mContext, this.mUri);
    }

    public boolean exists() {
        return android.support.v4.provider.DocumentsContractApi19.exists(this.mContext, this.mUri);
    }

    public android.support.v4.provider.DocumentFile[] listFiles() {
        android.net.Uri[] result = android.support.v4.provider.DocumentsContractApi21.listFiles(this.mContext, this.mUri);
        android.support.v4.provider.DocumentFile[] resultFiles = new android.support.v4.provider.DocumentFile[result.length];
        for (int i = 0; i < result.length; i++) {
            resultFiles[i] = new android.support.v4.provider.TreeDocumentFile(this, this.mContext, result[i]);
        }
        return resultFiles;
    }

    public boolean renameTo(java.lang.String displayName) {
        android.net.Uri result = android.support.v4.provider.DocumentsContractApi21.renameTo(this.mContext, this.mUri, displayName);
        if (result == null) {
            return false;
        }
        this.mUri = result;
        return true;
    }
}
