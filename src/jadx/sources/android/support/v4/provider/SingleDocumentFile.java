package android.support.v4.provider;

class SingleDocumentFile extends android.support.v4.provider.DocumentFile {
    private android.content.Context mContext;
    private android.net.Uri mUri;

    SingleDocumentFile(android.support.v4.provider.DocumentFile parent, android.content.Context context, android.net.Uri uri) {
        super(parent);
        this.mContext = context;
        this.mUri = uri;
    }

    public android.support.v4.provider.DocumentFile createFile(java.lang.String mimeType, java.lang.String displayName) {
        throw new java.lang.UnsupportedOperationException();
    }

    public android.support.v4.provider.DocumentFile createDirectory(java.lang.String displayName) {
        throw new java.lang.UnsupportedOperationException();
    }

    public android.net.Uri getUri() {
        return this.mUri;
    }

    public java.lang.String getName() {
        return android.support.v4.provider.DocumentsContractApi19.getName(this.mContext, this.mUri);
    }

    public java.lang.String getType() {
        return android.support.v4.provider.DocumentsContractApi19.getType(this.mContext, this.mUri);
    }

    public boolean isDirectory() {
        return android.support.v4.provider.DocumentsContractApi19.isDirectory(this.mContext, this.mUri);
    }

    public boolean isFile() {
        return android.support.v4.provider.DocumentsContractApi19.isFile(this.mContext, this.mUri);
    }

    public boolean isVirtual() {
        return android.support.v4.provider.DocumentsContractApi19.isVirtual(this.mContext, this.mUri);
    }

    public long lastModified() {
        return android.support.v4.provider.DocumentsContractApi19.lastModified(this.mContext, this.mUri);
    }

    public long length() {
        return android.support.v4.provider.DocumentsContractApi19.length(this.mContext, this.mUri);
    }

    public boolean canRead() {
        return android.support.v4.provider.DocumentsContractApi19.canRead(this.mContext, this.mUri);
    }

    public boolean canWrite() {
        return android.support.v4.provider.DocumentsContractApi19.canWrite(this.mContext, this.mUri);
    }

    public boolean delete() {
        return android.support.v4.provider.DocumentsContractApi19.delete(this.mContext, this.mUri);
    }

    public boolean exists() {
        return android.support.v4.provider.DocumentsContractApi19.exists(this.mContext, this.mUri);
    }

    public android.support.v4.provider.DocumentFile[] listFiles() {
        throw new java.lang.UnsupportedOperationException();
    }

    public boolean renameTo(java.lang.String displayName) {
        throw new java.lang.UnsupportedOperationException();
    }
}
