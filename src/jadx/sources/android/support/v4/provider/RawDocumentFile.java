package android.support.v4.provider;

class RawDocumentFile extends android.support.v4.provider.DocumentFile {
    private java.io.File mFile;

    RawDocumentFile(android.support.v4.provider.DocumentFile parent, java.io.File file) {
        super(parent);
        this.mFile = file;
    }

    public android.support.v4.provider.DocumentFile createFile(java.lang.String mimeType, java.lang.String displayName) {
        java.lang.String extension = android.webkit.MimeTypeMap.getSingleton().getExtensionFromMimeType(mimeType);
        if (extension != null) {
            displayName = displayName + "." + extension;
        }
        java.io.File target = new java.io.File(this.mFile, displayName);
        try {
            target.createNewFile();
            return new android.support.v4.provider.RawDocumentFile(this, target);
        } catch (java.io.IOException e) {
            android.util.Log.w("DocumentFile", "Failed to createFile: " + e);
            return null;
        }
    }

    public android.support.v4.provider.DocumentFile createDirectory(java.lang.String displayName) {
        java.io.File target = new java.io.File(this.mFile, displayName);
        if (target.isDirectory() || target.mkdir()) {
            return new android.support.v4.provider.RawDocumentFile(this, target);
        }
        return null;
    }

    public android.net.Uri getUri() {
        return android.net.Uri.fromFile(this.mFile);
    }

    public java.lang.String getName() {
        return this.mFile.getName();
    }

    public java.lang.String getType() {
        if (this.mFile.isDirectory()) {
            return null;
        }
        return getTypeForName(this.mFile.getName());
    }

    public boolean isDirectory() {
        return this.mFile.isDirectory();
    }

    public boolean isFile() {
        return this.mFile.isFile();
    }

    public boolean isVirtual() {
        return false;
    }

    public long lastModified() {
        return this.mFile.lastModified();
    }

    public long length() {
        return this.mFile.length();
    }

    public boolean canRead() {
        return this.mFile.canRead();
    }

    public boolean canWrite() {
        return this.mFile.canWrite();
    }

    public boolean delete() {
        deleteContents(this.mFile);
        return this.mFile.delete();
    }

    public boolean exists() {
        return this.mFile.exists();
    }

    public android.support.v4.provider.DocumentFile[] listFiles() {
        java.util.ArrayList<android.support.v4.provider.DocumentFile> results = new java.util.ArrayList<>();
        java.io.File[] files = this.mFile.listFiles();
        if (files != null) {
            for (java.io.File file : files) {
                results.add(new android.support.v4.provider.RawDocumentFile(this, file));
            }
        }
        return (android.support.v4.provider.DocumentFile[]) results.toArray(new android.support.v4.provider.DocumentFile[results.size()]);
    }

    public boolean renameTo(java.lang.String displayName) {
        java.io.File target = new java.io.File(this.mFile.getParentFile(), displayName);
        if (!this.mFile.renameTo(target)) {
            return false;
        }
        this.mFile = target;
        return true;
    }

    private static java.lang.String getTypeForName(java.lang.String name) {
        int lastDot = name.lastIndexOf(46);
        if (lastDot >= 0) {
            java.lang.String mime = android.webkit.MimeTypeMap.getSingleton().getMimeTypeFromExtension(name.substring(lastDot + 1).toLowerCase());
            if (mime != null) {
                return mime;
            }
        }
        return "application/octet-stream";
    }

    private static boolean deleteContents(java.io.File dir) {
        java.io.File[] files = dir.listFiles();
        boolean success = true;
        if (files != null) {
            for (java.io.File file : files) {
                if (file.isDirectory()) {
                    success &= deleteContents(file);
                }
                if (!file.delete()) {
                    android.util.Log.w("DocumentFile", "Failed to delete " + file);
                    success = false;
                }
            }
        }
        return success;
    }
}
