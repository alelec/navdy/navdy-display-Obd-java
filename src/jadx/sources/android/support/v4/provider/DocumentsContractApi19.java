package android.support.v4.provider;

@android.annotation.TargetApi(19)
@android.support.annotation.RequiresApi(19)
class DocumentsContractApi19 {
    private static final int FLAG_VIRTUAL_DOCUMENT = 512;
    private static final java.lang.String TAG = "DocumentFile";

    DocumentsContractApi19() {
    }

    public static boolean isDocumentUri(android.content.Context context, android.net.Uri self) {
        return android.provider.DocumentsContract.isDocumentUri(context, self);
    }

    public static boolean isVirtual(android.content.Context context, android.net.Uri self) {
        if (isDocumentUri(context, self) && (getFlags(context, self) & 512) != 0) {
            return true;
        }
        return false;
    }

    public static java.lang.String getName(android.content.Context context, android.net.Uri self) {
        return queryForString(context, self, "_display_name", null);
    }

    private static java.lang.String getRawType(android.content.Context context, android.net.Uri self) {
        return queryForString(context, self, "mime_type", null);
    }

    public static java.lang.String getType(android.content.Context context, android.net.Uri self) {
        java.lang.String rawType = getRawType(context, self);
        if ("vnd.android.document/directory".equals(rawType)) {
            return null;
        }
        return rawType;
    }

    public static long getFlags(android.content.Context context, android.net.Uri self) {
        return queryForLong(context, self, "flags", 0);
    }

    public static boolean isDirectory(android.content.Context context, android.net.Uri self) {
        return "vnd.android.document/directory".equals(getRawType(context, self));
    }

    public static boolean isFile(android.content.Context context, android.net.Uri self) {
        java.lang.String type = getRawType(context, self);
        if ("vnd.android.document/directory".equals(type) || android.text.TextUtils.isEmpty(type)) {
            return false;
        }
        return true;
    }

    public static long lastModified(android.content.Context context, android.net.Uri self) {
        return queryForLong(context, self, "last_modified", 0);
    }

    public static long length(android.content.Context context, android.net.Uri self) {
        return queryForLong(context, self, "_size", 0);
    }

    public static boolean canRead(android.content.Context context, android.net.Uri self) {
        if (context.checkCallingOrSelfUriPermission(self, 1) == 0 && !android.text.TextUtils.isEmpty(getRawType(context, self))) {
            return true;
        }
        return false;
    }

    public static boolean canWrite(android.content.Context context, android.net.Uri self) {
        if (context.checkCallingOrSelfUriPermission(self, 2) != 0) {
            return false;
        }
        java.lang.String type = getRawType(context, self);
        int flags = queryForInt(context, self, "flags", 0);
        if (android.text.TextUtils.isEmpty(type)) {
            return false;
        }
        if ((flags & 4) != 0) {
            return true;
        }
        if ("vnd.android.document/directory".equals(type) && (flags & 8) != 0) {
            return true;
        }
        if (android.text.TextUtils.isEmpty(type) || (flags & 2) == 0) {
            return false;
        }
        return true;
    }

    public static boolean delete(android.content.Context context, android.net.Uri self) {
        return android.provider.DocumentsContract.deleteDocument(context.getContentResolver(), self);
    }

    /* JADX INFO: finally extract failed */
    public static boolean exists(android.content.Context context, android.net.Uri self) {
        android.database.Cursor c = null;
        try {
            c = context.getContentResolver().query(self, new java.lang.String[]{"document_id"}, null, null, null);
            boolean z = c.getCount() > 0;
            closeQuietly(c);
            return z;
        } catch (java.lang.Exception e) {
            android.util.Log.w(TAG, "Failed query: " + e);
            closeQuietly(c);
            return false;
        } catch (Throwable th) {
            closeQuietly(c);
            throw th;
        }
    }

    private static java.lang.String queryForString(android.content.Context context, android.net.Uri self, java.lang.String column, java.lang.String defaultValue) {
        android.database.Cursor c = null;
        try {
            c = context.getContentResolver().query(self, new java.lang.String[]{column}, null, null, null);
            if (!c.moveToFirst() || c.isNull(0)) {
                closeQuietly(c);
                return defaultValue;
            }
            defaultValue = c.getString(0);
            return defaultValue;
        } catch (java.lang.Exception e) {
            android.util.Log.w(TAG, "Failed query: " + e);
        } finally {
            closeQuietly(c);
        }
    }

    private static int queryForInt(android.content.Context context, android.net.Uri self, java.lang.String column, int defaultValue) {
        return (int) queryForLong(context, self, column, (long) defaultValue);
    }

    private static long queryForLong(android.content.Context context, android.net.Uri self, java.lang.String column, long defaultValue) {
        android.database.Cursor c = null;
        try {
            c = context.getContentResolver().query(self, new java.lang.String[]{column}, null, null, null);
            if (!c.moveToFirst() || c.isNull(0)) {
                closeQuietly(c);
                return defaultValue;
            }
            defaultValue = c.getLong(0);
            return defaultValue;
        } catch (java.lang.Exception e) {
            android.util.Log.w(TAG, "Failed query: " + e);
        } finally {
            closeQuietly(c);
        }
    }

    private static void closeQuietly(java.lang.AutoCloseable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (java.lang.RuntimeException rethrown) {
                throw rethrown;
            } catch (java.lang.Exception e) {
            }
        }
    }
}
