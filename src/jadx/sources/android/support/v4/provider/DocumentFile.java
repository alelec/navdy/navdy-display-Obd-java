package android.support.v4.provider;

public abstract class DocumentFile {
    static final java.lang.String TAG = "DocumentFile";
    private final android.support.v4.provider.DocumentFile mParent;

    public abstract boolean canRead();

    public abstract boolean canWrite();

    public abstract android.support.v4.provider.DocumentFile createDirectory(java.lang.String str);

    public abstract android.support.v4.provider.DocumentFile createFile(java.lang.String str, java.lang.String str2);

    public abstract boolean delete();

    public abstract boolean exists();

    public abstract java.lang.String getName();

    public abstract java.lang.String getType();

    public abstract android.net.Uri getUri();

    public abstract boolean isDirectory();

    public abstract boolean isFile();

    public abstract boolean isVirtual();

    public abstract long lastModified();

    public abstract long length();

    public abstract android.support.v4.provider.DocumentFile[] listFiles();

    public abstract boolean renameTo(java.lang.String str);

    DocumentFile(android.support.v4.provider.DocumentFile parent) {
        this.mParent = parent;
    }

    public static android.support.v4.provider.DocumentFile fromFile(java.io.File file) {
        return new android.support.v4.provider.RawDocumentFile(null, file);
    }

    public static android.support.v4.provider.DocumentFile fromSingleUri(android.content.Context context, android.net.Uri singleUri) {
        if (android.os.Build.VERSION.SDK_INT >= 19) {
            return new android.support.v4.provider.SingleDocumentFile(null, context, singleUri);
        }
        return null;
    }

    public static android.support.v4.provider.DocumentFile fromTreeUri(android.content.Context context, android.net.Uri treeUri) {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            return new android.support.v4.provider.TreeDocumentFile(null, context, android.support.v4.provider.DocumentsContractApi21.prepareTreeUri(treeUri));
        }
        return null;
    }

    public static boolean isDocumentUri(android.content.Context context, android.net.Uri uri) {
        if (android.os.Build.VERSION.SDK_INT >= 19) {
            return android.support.v4.provider.DocumentsContractApi19.isDocumentUri(context, uri);
        }
        return false;
    }

    public android.support.v4.provider.DocumentFile getParentFile() {
        return this.mParent;
    }

    public android.support.v4.provider.DocumentFile findFile(java.lang.String displayName) {
        android.support.v4.provider.DocumentFile[] listFiles;
        for (android.support.v4.provider.DocumentFile doc : listFiles()) {
            if (displayName.equals(doc.getName())) {
                return doc;
            }
        }
        return null;
    }
}
