package android.support.v4.provider;

@android.annotation.TargetApi(21)
@android.support.annotation.RequiresApi(21)
class DocumentsContractApi21 {
    private static final java.lang.String TAG = "DocumentFile";

    DocumentsContractApi21() {
    }

    public static android.net.Uri createFile(android.content.Context context, android.net.Uri self, java.lang.String mimeType, java.lang.String displayName) {
        return android.provider.DocumentsContract.createDocument(context.getContentResolver(), self, mimeType, displayName);
    }

    public static android.net.Uri createDirectory(android.content.Context context, android.net.Uri self, java.lang.String displayName) {
        return createFile(context, self, "vnd.android.document/directory", displayName);
    }

    public static android.net.Uri prepareTreeUri(android.net.Uri treeUri) {
        return android.provider.DocumentsContract.buildDocumentUriUsingTree(treeUri, android.provider.DocumentsContract.getTreeDocumentId(treeUri));
    }

    public static android.net.Uri[] listFiles(android.content.Context context, android.net.Uri self) {
        android.content.ContentResolver resolver = context.getContentResolver();
        android.net.Uri childrenUri = android.provider.DocumentsContract.buildChildDocumentsUriUsingTree(self, android.provider.DocumentsContract.getDocumentId(self));
        java.util.ArrayList<android.net.Uri> results = new java.util.ArrayList<>();
        android.database.Cursor c = null;
        try {
            c = resolver.query(childrenUri, new java.lang.String[]{"document_id"}, null, null, null);
            while (c.moveToNext()) {
                results.add(android.provider.DocumentsContract.buildDocumentUriUsingTree(self, c.getString(0)));
            }
        } catch (java.lang.Exception e) {
            android.util.Log.w(TAG, "Failed query: " + e);
        } finally {
            closeQuietly(c);
        }
        return (android.net.Uri[]) results.toArray(new android.net.Uri[results.size()]);
    }

    public static android.net.Uri renameTo(android.content.Context context, android.net.Uri self, java.lang.String displayName) {
        return android.provider.DocumentsContract.renameDocument(context.getContentResolver(), self, displayName);
    }

    private static void closeQuietly(java.lang.AutoCloseable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (java.lang.RuntimeException rethrown) {
                throw rethrown;
            } catch (java.lang.Exception e) {
            }
        }
    }
}
