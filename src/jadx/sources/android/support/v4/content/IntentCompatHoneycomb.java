package android.support.v4.content;

@android.annotation.TargetApi(11)
@android.support.annotation.RequiresApi(11)
class IntentCompatHoneycomb {
    IntentCompatHoneycomb() {
    }

    public static android.content.Intent makeMainActivity(android.content.ComponentName mainActivity) {
        return android.content.Intent.makeMainActivity(mainActivity);
    }

    public static android.content.Intent makeRestartActivityTask(android.content.ComponentName mainActivity) {
        return android.content.Intent.makeRestartActivityTask(mainActivity);
    }
}
