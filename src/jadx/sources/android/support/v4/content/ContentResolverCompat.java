package android.support.v4.content;

public final class ContentResolverCompat {
    private static final android.support.v4.content.ContentResolverCompat.ContentResolverCompatImpl IMPL;

    interface ContentResolverCompatImpl {
        android.database.Cursor query(android.content.ContentResolver contentResolver, android.net.Uri uri, java.lang.String[] strArr, java.lang.String str, java.lang.String[] strArr2, java.lang.String str2, android.support.v4.os.CancellationSignal cancellationSignal);
    }

    static class ContentResolverCompatImplBase implements android.support.v4.content.ContentResolverCompat.ContentResolverCompatImpl {
        ContentResolverCompatImplBase() {
        }

        public android.database.Cursor query(android.content.ContentResolver resolver, android.net.Uri uri, java.lang.String[] projection, java.lang.String selection, java.lang.String[] selectionArgs, java.lang.String sortOrder, android.support.v4.os.CancellationSignal cancellationSignal) {
            if (cancellationSignal != null) {
                cancellationSignal.throwIfCanceled();
            }
            return resolver.query(uri, projection, selection, selectionArgs, sortOrder);
        }
    }

    static class ContentResolverCompatImplJB extends android.support.v4.content.ContentResolverCompat.ContentResolverCompatImplBase {
        ContentResolverCompatImplJB() {
        }

        public android.database.Cursor query(android.content.ContentResolver resolver, android.net.Uri uri, java.lang.String[] projection, java.lang.String selection, java.lang.String[] selectionArgs, java.lang.String sortOrder, android.support.v4.os.CancellationSignal cancellationSignal) {
            java.lang.Object obj;
            if (cancellationSignal != null) {
                try {
                    obj = cancellationSignal.getCancellationSignalObject();
                } catch (java.lang.Exception e) {
                    if (android.support.v4.content.ContentResolverCompatJellybean.isFrameworkOperationCanceledException(e)) {
                        throw new android.support.v4.os.OperationCanceledException();
                    }
                    throw e;
                }
            } else {
                obj = null;
            }
            return android.support.v4.content.ContentResolverCompatJellybean.query(resolver, uri, projection, selection, selectionArgs, sortOrder, obj);
        }
    }

    static {
        if (android.os.Build.VERSION.SDK_INT >= 16) {
            IMPL = new android.support.v4.content.ContentResolverCompat.ContentResolverCompatImplJB();
        } else {
            IMPL = new android.support.v4.content.ContentResolverCompat.ContentResolverCompatImplBase();
        }
    }

    private ContentResolverCompat() {
    }

    public static android.database.Cursor query(android.content.ContentResolver resolver, android.net.Uri uri, java.lang.String[] projection, java.lang.String selection, java.lang.String[] selectionArgs, java.lang.String sortOrder, android.support.v4.os.CancellationSignal cancellationSignal) {
        return IMPL.query(resolver, uri, projection, selection, selectionArgs, sortOrder, cancellationSignal);
    }
}
