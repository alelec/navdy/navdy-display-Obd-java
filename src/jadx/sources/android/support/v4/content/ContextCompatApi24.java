package android.support.v4.content;

@android.annotation.TargetApi(24)
@android.support.annotation.RequiresApi(24)
class ContextCompatApi24 {
    ContextCompatApi24() {
    }

    public static java.io.File getDataDir(android.content.Context context) {
        return context.getDataDir();
    }

    public static android.content.Context createDeviceProtectedStorageContext(android.content.Context context) {
        return context.createDeviceProtectedStorageContext();
    }

    public static boolean isDeviceProtectedStorage(android.content.Context context) {
        return context.isDeviceProtectedStorage();
    }
}
