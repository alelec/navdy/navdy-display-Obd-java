package android.support.v4.content;

@android.annotation.TargetApi(16)
@android.support.annotation.RequiresApi(16)
class ContextCompatJellybean {
    ContextCompatJellybean() {
    }

    public static void startActivities(android.content.Context context, android.content.Intent[] intents, android.os.Bundle options) {
        context.startActivities(intents, options);
    }

    public static void startActivity(android.content.Context context, android.content.Intent intent, android.os.Bundle options) {
        context.startActivity(intent, options);
    }
}
