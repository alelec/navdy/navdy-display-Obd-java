package android.support.v4.content;

@android.annotation.TargetApi(21)
@android.support.annotation.RequiresApi(21)
class ContextCompatApi21 {
    ContextCompatApi21() {
    }

    public static android.graphics.drawable.Drawable getDrawable(android.content.Context context, int id) {
        return context.getDrawable(id);
    }

    public static java.io.File getNoBackupFilesDir(android.content.Context context) {
        return context.getNoBackupFilesDir();
    }

    public static java.io.File getCodeCacheDir(android.content.Context context) {
        return context.getCodeCacheDir();
    }
}
