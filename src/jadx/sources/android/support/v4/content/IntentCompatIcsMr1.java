package android.support.v4.content;

@android.annotation.TargetApi(15)
@android.support.annotation.RequiresApi(15)
class IntentCompatIcsMr1 {
    IntentCompatIcsMr1() {
    }

    public static android.content.Intent makeMainSelectorActivity(java.lang.String selectorAction, java.lang.String selectorCategory) {
        return android.content.Intent.makeMainSelectorActivity(selectorAction, selectorCategory);
    }
}
