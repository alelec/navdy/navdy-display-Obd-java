package android.support.v4.content;

public class ContextCompat {
    private static final java.lang.String DIR_ANDROID = "Android";
    private static final java.lang.String DIR_OBB = "obb";
    private static final java.lang.String TAG = "ContextCompat";
    private static final java.lang.Object sLock = new java.lang.Object();
    private static android.util.TypedValue sTempValue;

    protected ContextCompat() {
    }

    public static boolean startActivities(android.content.Context context, android.content.Intent[] intents) {
        return startActivities(context, intents, null);
    }

    public static boolean startActivities(android.content.Context context, android.content.Intent[] intents, android.os.Bundle options) {
        int version = android.os.Build.VERSION.SDK_INT;
        if (version >= 16) {
            android.support.v4.content.ContextCompatJellybean.startActivities(context, intents, options);
            return true;
        } else if (version < 11) {
            return false;
        } else {
            android.support.v4.content.ContextCompatHoneycomb.startActivities(context, intents);
            return true;
        }
    }

    public static void startActivity(android.content.Context context, android.content.Intent intent, @android.support.annotation.Nullable android.os.Bundle options) {
        if (android.os.Build.VERSION.SDK_INT >= 16) {
            android.support.v4.content.ContextCompatJellybean.startActivity(context, intent, options);
        } else {
            context.startActivity(intent);
        }
    }

    public static java.io.File getDataDir(android.content.Context context) {
        if (android.support.v4.os.BuildCompat.isAtLeastN()) {
            return android.support.v4.content.ContextCompatApi24.getDataDir(context);
        }
        java.lang.String dataDir = context.getApplicationInfo().dataDir;
        if (dataDir != null) {
            return new java.io.File(dataDir);
        }
        return null;
    }

    public static java.io.File[] getObbDirs(android.content.Context context) {
        java.io.File single;
        int version = android.os.Build.VERSION.SDK_INT;
        if (version >= 19) {
            return android.support.v4.content.ContextCompatKitKat.getObbDirs(context);
        }
        if (version >= 11) {
            single = android.support.v4.content.ContextCompatHoneycomb.getObbDir(context);
        } else {
            single = buildPath(android.os.Environment.getExternalStorageDirectory(), DIR_ANDROID, DIR_OBB, context.getPackageName());
        }
        return new java.io.File[]{single};
    }

    public static java.io.File[] getExternalFilesDirs(android.content.Context context, java.lang.String type) {
        if (android.os.Build.VERSION.SDK_INT >= 19) {
            return android.support.v4.content.ContextCompatKitKat.getExternalFilesDirs(context, type);
        }
        return new java.io.File[]{context.getExternalFilesDir(type)};
    }

    public static java.io.File[] getExternalCacheDirs(android.content.Context context) {
        if (android.os.Build.VERSION.SDK_INT >= 19) {
            return android.support.v4.content.ContextCompatKitKat.getExternalCacheDirs(context);
        }
        return new java.io.File[]{context.getExternalCacheDir()};
    }

    private static java.io.File buildPath(java.io.File base, java.lang.String... segments) {
        java.io.File cur;
        java.io.File cur2 = base;
        int length = segments.length;
        int i = 0;
        java.io.File cur3 = cur2;
        while (i < length) {
            java.lang.String segment = segments[i];
            if (cur3 == null) {
                cur = new java.io.File(segment);
            } else if (segment != null) {
                cur = new java.io.File(cur3, segment);
            } else {
                cur = cur3;
            }
            i++;
            cur3 = cur;
        }
        return cur3;
    }

    public static final android.graphics.drawable.Drawable getDrawable(android.content.Context context, @android.support.annotation.DrawableRes int id) {
        int resolvedId;
        int version = android.os.Build.VERSION.SDK_INT;
        if (version >= 21) {
            return android.support.v4.content.ContextCompatApi21.getDrawable(context, id);
        }
        if (version >= 16) {
            return context.getResources().getDrawable(id);
        }
        synchronized (sLock) {
            if (sTempValue == null) {
                sTempValue = new android.util.TypedValue();
            }
            context.getResources().getValue(id, sTempValue, true);
            resolvedId = sTempValue.resourceId;
        }
        return context.getResources().getDrawable(resolvedId);
    }

    public static final android.content.res.ColorStateList getColorStateList(android.content.Context context, @android.support.annotation.ColorRes int id) {
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            return android.support.v4.content.ContextCompatApi23.getColorStateList(context, id);
        }
        return context.getResources().getColorStateList(id);
    }

    @android.support.annotation.ColorInt
    public static final int getColor(android.content.Context context, @android.support.annotation.ColorRes int id) {
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            return android.support.v4.content.ContextCompatApi23.getColor(context, id);
        }
        return context.getResources().getColor(id);
    }

    public static int checkSelfPermission(@android.support.annotation.NonNull android.content.Context context, @android.support.annotation.NonNull java.lang.String permission) {
        if (permission != null) {
            return context.checkPermission(permission, android.os.Process.myPid(), android.os.Process.myUid());
        }
        throw new java.lang.IllegalArgumentException("permission is null");
    }

    public static final java.io.File getNoBackupFilesDir(android.content.Context context) {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            return android.support.v4.content.ContextCompatApi21.getNoBackupFilesDir(context);
        }
        return createFilesDir(new java.io.File(context.getApplicationInfo().dataDir, "no_backup"));
    }

    public static java.io.File getCodeCacheDir(android.content.Context context) {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            return android.support.v4.content.ContextCompatApi21.getCodeCacheDir(context);
        }
        return createFilesDir(new java.io.File(context.getApplicationInfo().dataDir, "code_cache"));
    }

    private static synchronized java.io.File createFilesDir(java.io.File file) {
        synchronized (android.support.v4.content.ContextCompat.class) {
            if (!file.exists() && !file.mkdirs() && !file.exists()) {
                android.util.Log.w(TAG, "Unable to create files subdir " + file.getPath());
                file = null;
            }
        }
        return file;
    }

    public static android.content.Context createDeviceProtectedStorageContext(android.content.Context context) {
        if (android.support.v4.os.BuildCompat.isAtLeastN()) {
            return android.support.v4.content.ContextCompatApi24.createDeviceProtectedStorageContext(context);
        }
        return null;
    }

    public static boolean isDeviceProtectedStorage(android.content.Context context) {
        if (android.support.v4.os.BuildCompat.isAtLeastN()) {
            return android.support.v4.content.ContextCompatApi24.isDeviceProtectedStorage(context);
        }
        return false;
    }
}
