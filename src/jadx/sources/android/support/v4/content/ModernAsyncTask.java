package android.support.v4.content;

abstract class ModernAsyncTask<Params, Progress, Result> {
    private static final int CORE_POOL_SIZE = 5;
    private static final int KEEP_ALIVE = 1;
    private static final java.lang.String LOG_TAG = "AsyncTask";
    private static final int MAXIMUM_POOL_SIZE = 128;
    private static final int MESSAGE_POST_PROGRESS = 2;
    private static final int MESSAGE_POST_RESULT = 1;
    public static final java.util.concurrent.Executor THREAD_POOL_EXECUTOR = new java.util.concurrent.ThreadPoolExecutor(5, 128, 1, java.util.concurrent.TimeUnit.SECONDS, sPoolWorkQueue, sThreadFactory);
    private static volatile java.util.concurrent.Executor sDefaultExecutor = THREAD_POOL_EXECUTOR;
    private static android.support.v4.content.ModernAsyncTask.InternalHandler sHandler;
    private static final java.util.concurrent.BlockingQueue<java.lang.Runnable> sPoolWorkQueue = new java.util.concurrent.LinkedBlockingQueue(10);
    private static final java.util.concurrent.ThreadFactory sThreadFactory = new java.util.concurrent.ThreadFactory() {
        private final java.util.concurrent.atomic.AtomicInteger mCount = new java.util.concurrent.atomic.AtomicInteger(1);

        public java.lang.Thread newThread(java.lang.Runnable r) {
            return new java.lang.Thread(r, "ModernAsyncTask #" + this.mCount.getAndIncrement());
        }
    };
    /* access modifiers changed from: private */
    public final java.util.concurrent.atomic.AtomicBoolean mCancelled = new java.util.concurrent.atomic.AtomicBoolean();
    private final java.util.concurrent.FutureTask<Result> mFuture = new java.util.concurrent.FutureTask<Result>(this.mWorker) {
        /* access modifiers changed from: protected */
        public void done() {
            try {
                android.support.v4.content.ModernAsyncTask.this.postResultIfNotInvoked(get());
            } catch (java.lang.InterruptedException e) {
                android.util.Log.w(android.support.v4.content.ModernAsyncTask.LOG_TAG, e);
            } catch (java.util.concurrent.ExecutionException e2) {
                throw new java.lang.RuntimeException("An error occurred while executing doInBackground()", e2.getCause());
            } catch (java.util.concurrent.CancellationException e3) {
                android.support.v4.content.ModernAsyncTask.this.postResultIfNotInvoked(null);
            } catch (Throwable t) {
                throw new java.lang.RuntimeException("An error occurred while executing doInBackground()", t);
            }
        }
    };
    private volatile android.support.v4.content.ModernAsyncTask.Status mStatus = android.support.v4.content.ModernAsyncTask.Status.PENDING;
    /* access modifiers changed from: private */
    public final java.util.concurrent.atomic.AtomicBoolean mTaskInvoked = new java.util.concurrent.atomic.AtomicBoolean();
    private final android.support.v4.content.ModernAsyncTask.WorkerRunnable<Params, Result> mWorker = new android.support.v4.content.ModernAsyncTask.WorkerRunnable<Params, Result>() {
        public Result call() throws java.lang.Exception {
            android.support.v4.content.ModernAsyncTask.this.mTaskInvoked.set(true);
            Result result = null;
            try {
                android.os.Process.setThreadPriority(10);
                result = android.support.v4.content.ModernAsyncTask.this.doInBackground(this.mParams);
                android.os.Binder.flushPendingCommands();
                android.support.v4.content.ModernAsyncTask.this.postResult(result);
                return result;
            } catch (Throwable th) {
                android.support.v4.content.ModernAsyncTask.this.postResult(result);
                throw th;
            }
        }
    };

    private static class AsyncTaskResult<Data> {
        final Data[] mData;
        final android.support.v4.content.ModernAsyncTask mTask;

        AsyncTaskResult(android.support.v4.content.ModernAsyncTask task, Data... data) {
            this.mTask = task;
            this.mData = data;
        }
    }

    private static class InternalHandler extends android.os.Handler {
        public InternalHandler() {
            super(android.os.Looper.getMainLooper());
        }

        public void handleMessage(android.os.Message msg) {
            android.support.v4.content.ModernAsyncTask.AsyncTaskResult result = (android.support.v4.content.ModernAsyncTask.AsyncTaskResult) msg.obj;
            switch (msg.what) {
                case 1:
                    result.mTask.finish(result.mData[0]);
                    return;
                case 2:
                    result.mTask.onProgressUpdate(result.mData);
                    return;
                default:
                    return;
            }
        }
    }

    public enum Status {
        PENDING,
        RUNNING,
        FINISHED
    }

    private static abstract class WorkerRunnable<Params, Result> implements java.util.concurrent.Callable<Result> {
        Params[] mParams;

        WorkerRunnable() {
        }
    }

    /* access modifiers changed from: protected */
    public abstract Result doInBackground(Params... paramsArr);

    private static android.os.Handler getHandler() {
        android.support.v4.content.ModernAsyncTask.InternalHandler internalHandler;
        synchronized (android.support.v4.content.ModernAsyncTask.class) {
            if (sHandler == null) {
                sHandler = new android.support.v4.content.ModernAsyncTask.InternalHandler();
            }
            internalHandler = sHandler;
        }
        return internalHandler;
    }

    @android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
    public static void setDefaultExecutor(java.util.concurrent.Executor exec) {
        sDefaultExecutor = exec;
    }

    /* access modifiers changed from: 0000 */
    public void postResultIfNotInvoked(Result result) {
        if (!this.mTaskInvoked.get()) {
            postResult(result);
        }
    }

    /* access modifiers changed from: 0000 */
    public Result postResult(Result result) {
        getHandler().obtainMessage(1, new android.support.v4.content.ModernAsyncTask.AsyncTaskResult(this, result)).sendToTarget();
        return result;
    }

    public final android.support.v4.content.ModernAsyncTask.Status getStatus() {
        return this.mStatus;
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Result result) {
    }

    /* access modifiers changed from: protected */
    public void onProgressUpdate(Progress... progressArr) {
    }

    /* access modifiers changed from: protected */
    public void onCancelled(Result result) {
        onCancelled();
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
    }

    public final boolean isCancelled() {
        return this.mCancelled.get();
    }

    public final boolean cancel(boolean mayInterruptIfRunning) {
        this.mCancelled.set(true);
        return this.mFuture.cancel(mayInterruptIfRunning);
    }

    public final Result get() throws java.lang.InterruptedException, java.util.concurrent.ExecutionException {
        return this.mFuture.get();
    }

    public final Result get(long timeout, java.util.concurrent.TimeUnit unit) throws java.lang.InterruptedException, java.util.concurrent.ExecutionException, java.util.concurrent.TimeoutException {
        return this.mFuture.get(timeout, unit);
    }

    public final android.support.v4.content.ModernAsyncTask<Params, Progress, Result> execute(Params... params) {
        return executeOnExecutor(sDefaultExecutor, params);
    }

    public final android.support.v4.content.ModernAsyncTask<Params, Progress, Result> executeOnExecutor(java.util.concurrent.Executor exec, Params... params) {
        if (this.mStatus != android.support.v4.content.ModernAsyncTask.Status.PENDING) {
            switch (this.mStatus) {
                case RUNNING:
                    throw new java.lang.IllegalStateException("Cannot execute task: the task is already running.");
                case FINISHED:
                    throw new java.lang.IllegalStateException("Cannot execute task: the task has already been executed (a task can be executed only once)");
            }
        }
        this.mStatus = android.support.v4.content.ModernAsyncTask.Status.RUNNING;
        onPreExecute();
        this.mWorker.mParams = params;
        exec.execute(this.mFuture);
        return this;
    }

    public static void execute(java.lang.Runnable runnable) {
        sDefaultExecutor.execute(runnable);
    }

    /* access modifiers changed from: protected */
    public final void publishProgress(Progress... values) {
        if (!isCancelled()) {
            getHandler().obtainMessage(2, new android.support.v4.content.ModernAsyncTask.AsyncTaskResult(this, values)).sendToTarget();
        }
    }

    /* access modifiers changed from: 0000 */
    public void finish(Result result) {
        if (isCancelled()) {
            onCancelled(result);
        } else {
            onPostExecute(result);
        }
        this.mStatus = android.support.v4.content.ModernAsyncTask.Status.FINISHED;
    }
}
