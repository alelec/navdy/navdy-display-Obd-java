package android.support.v4.content;

public final class LocalBroadcastManager {
    private static final boolean DEBUG = false;
    static final int MSG_EXEC_PENDING_BROADCASTS = 1;
    private static final java.lang.String TAG = "LocalBroadcastManager";
    private static android.support.v4.content.LocalBroadcastManager mInstance;
    private static final java.lang.Object mLock = new java.lang.Object();
    private final java.util.HashMap<java.lang.String, java.util.ArrayList<android.support.v4.content.LocalBroadcastManager.ReceiverRecord>> mActions = new java.util.HashMap<>();
    private final android.content.Context mAppContext;
    private final android.os.Handler mHandler;
    private final java.util.ArrayList<android.support.v4.content.LocalBroadcastManager.BroadcastRecord> mPendingBroadcasts = new java.util.ArrayList<>();
    private final java.util.HashMap<android.content.BroadcastReceiver, java.util.ArrayList<android.content.IntentFilter>> mReceivers = new java.util.HashMap<>();

    private static class BroadcastRecord {
        final android.content.Intent intent;
        final java.util.ArrayList<android.support.v4.content.LocalBroadcastManager.ReceiverRecord> receivers;

        BroadcastRecord(android.content.Intent _intent, java.util.ArrayList<android.support.v4.content.LocalBroadcastManager.ReceiverRecord> _receivers) {
            this.intent = _intent;
            this.receivers = _receivers;
        }
    }

    private static class ReceiverRecord {
        boolean broadcasting;
        final android.content.IntentFilter filter;
        final android.content.BroadcastReceiver receiver;

        ReceiverRecord(android.content.IntentFilter _filter, android.content.BroadcastReceiver _receiver) {
            this.filter = _filter;
            this.receiver = _receiver;
        }

        public java.lang.String toString() {
            java.lang.StringBuilder builder = new java.lang.StringBuilder(128);
            builder.append("Receiver{");
            builder.append(this.receiver);
            builder.append(" filter=");
            builder.append(this.filter);
            builder.append("}");
            return builder.toString();
        }
    }

    public static android.support.v4.content.LocalBroadcastManager getInstance(android.content.Context context) {
        android.support.v4.content.LocalBroadcastManager localBroadcastManager;
        synchronized (mLock) {
            if (mInstance == null) {
                mInstance = new android.support.v4.content.LocalBroadcastManager(context.getApplicationContext());
            }
            localBroadcastManager = mInstance;
        }
        return localBroadcastManager;
    }

    private LocalBroadcastManager(android.content.Context context) {
        this.mAppContext = context;
        this.mHandler = new android.os.Handler(context.getMainLooper()) {
            public void handleMessage(android.os.Message msg) {
                switch (msg.what) {
                    case 1:
                        android.support.v4.content.LocalBroadcastManager.this.executePendingBroadcasts();
                        return;
                    default:
                        super.handleMessage(msg);
                        return;
                }
            }
        };
    }

    public void registerReceiver(android.content.BroadcastReceiver receiver, android.content.IntentFilter filter) {
        synchronized (this.mReceivers) {
            android.support.v4.content.LocalBroadcastManager.ReceiverRecord entry = new android.support.v4.content.LocalBroadcastManager.ReceiverRecord(filter, receiver);
            java.util.ArrayList<android.content.IntentFilter> filters = (java.util.ArrayList) this.mReceivers.get(receiver);
            if (filters == null) {
                filters = new java.util.ArrayList<>(1);
                this.mReceivers.put(receiver, filters);
            }
            filters.add(filter);
            for (int i = 0; i < filter.countActions(); i++) {
                java.lang.String action = filter.getAction(i);
                java.util.ArrayList<android.support.v4.content.LocalBroadcastManager.ReceiverRecord> entries = (java.util.ArrayList) this.mActions.get(action);
                if (entries == null) {
                    entries = new java.util.ArrayList<>(1);
                    this.mActions.put(action, entries);
                }
                entries.add(entry);
            }
        }
    }

    public void unregisterReceiver(android.content.BroadcastReceiver receiver) {
        synchronized (this.mReceivers) {
            java.util.ArrayList<android.content.IntentFilter> filters = (java.util.ArrayList) this.mReceivers.remove(receiver);
            if (filters != null) {
                for (int i = 0; i < filters.size(); i++) {
                    android.content.IntentFilter filter = (android.content.IntentFilter) filters.get(i);
                    for (int j = 0; j < filter.countActions(); j++) {
                        java.lang.String action = filter.getAction(j);
                        java.util.ArrayList<android.support.v4.content.LocalBroadcastManager.ReceiverRecord> receivers = (java.util.ArrayList) this.mActions.get(action);
                        if (receivers != null) {
                            int k = 0;
                            while (k < receivers.size()) {
                                if (((android.support.v4.content.LocalBroadcastManager.ReceiverRecord) receivers.get(k)).receiver == receiver) {
                                    receivers.remove(k);
                                    k--;
                                }
                                k++;
                            }
                            if (receivers.size() <= 0) {
                                this.mActions.remove(action);
                            }
                        }
                    }
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:64:?, code lost:
        return false;
     */
    public boolean sendBroadcast(android.content.Intent intent) {
        java.lang.String reason;
        synchronized (this.mReceivers) {
            java.lang.String action = intent.getAction();
            java.lang.String type = intent.resolveTypeIfNeeded(this.mAppContext.getContentResolver());
            android.net.Uri data = intent.getData();
            java.lang.String scheme = intent.getScheme();
            java.util.Set<java.lang.String> categories = intent.getCategories();
            boolean debug = (intent.getFlags() & 8) != 0;
            if (debug) {
                android.util.Log.v(TAG, "Resolving type " + type + " scheme " + scheme + " of intent " + intent);
            }
            java.util.ArrayList<android.support.v4.content.LocalBroadcastManager.ReceiverRecord> entries = (java.util.ArrayList) this.mActions.get(intent.getAction());
            if (entries != null) {
                if (debug) {
                    android.util.Log.v(TAG, "Action list: " + entries);
                }
                java.util.ArrayList<android.support.v4.content.LocalBroadcastManager.ReceiverRecord> receivers = null;
                for (int i = 0; i < entries.size(); i++) {
                    android.support.v4.content.LocalBroadcastManager.ReceiverRecord receiver = (android.support.v4.content.LocalBroadcastManager.ReceiverRecord) entries.get(i);
                    if (debug) {
                        android.util.Log.v(TAG, "Matching against filter " + receiver.filter);
                    }
                    if (!receiver.broadcasting) {
                        int match = receiver.filter.match(action, type, scheme, data, categories, TAG);
                        if (match >= 0) {
                            if (debug) {
                                android.util.Log.v(TAG, "  Filter matched!  match=0x" + java.lang.Integer.toHexString(match));
                            }
                            if (receivers == null) {
                                receivers = new java.util.ArrayList<>();
                            }
                            receivers.add(receiver);
                            receiver.broadcasting = true;
                        } else if (debug) {
                            switch (match) {
                                case -4:
                                    reason = "category";
                                    break;
                                case -3:
                                    reason = "action";
                                    break;
                                case -2:
                                    reason = "data";
                                    break;
                                case -1:
                                    reason = "type";
                                    break;
                                default:
                                    reason = "unknown reason";
                                    break;
                            }
                            android.util.Log.v(TAG, "  Filter did not match: " + reason);
                        } else {
                            continue;
                        }
                    } else if (debug) {
                        android.util.Log.v(TAG, "  Filter's target already added");
                    }
                }
                if (receivers != null) {
                    for (int i2 = 0; i2 < receivers.size(); i2++) {
                        ((android.support.v4.content.LocalBroadcastManager.ReceiverRecord) receivers.get(i2)).broadcasting = false;
                    }
                    this.mPendingBroadcasts.add(new android.support.v4.content.LocalBroadcastManager.BroadcastRecord(intent, receivers));
                    if (!this.mHandler.hasMessages(1)) {
                        this.mHandler.sendEmptyMessage(1);
                    }
                    return true;
                }
            }
        }
    }

    public void sendBroadcastSync(android.content.Intent intent) {
        if (sendBroadcast(intent)) {
            executePendingBroadcasts();
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001d, code lost:
        if (r3 >= r2.length) goto L_0x0000;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001f, code lost:
        r1 = r2[r3];
        r4 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0028, code lost:
        if (r4 >= r1.receivers.size()) goto L_0x0041;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002a, code lost:
        ((android.support.v4.content.LocalBroadcastManager.ReceiverRecord) r1.receivers.get(r4)).receiver.onReceive(r8.mAppContext, r1.intent);
        r4 = r4 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0041, code lost:
        r3 = r3 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001b, code lost:
        r3 = 0;
     */
    public void executePendingBroadcasts() {
        while (true) {
            synchronized (this.mReceivers) {
                int N = this.mPendingBroadcasts.size();
                if (N > 0) {
                    android.support.v4.content.LocalBroadcastManager.BroadcastRecord[] brs = new android.support.v4.content.LocalBroadcastManager.BroadcastRecord[N];
                    this.mPendingBroadcasts.toArray(brs);
                    this.mPendingBroadcasts.clear();
                } else {
                    return;
                }
            }
        }
        while (true) {
        }
    }
}
