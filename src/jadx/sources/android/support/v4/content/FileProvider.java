package android.support.v4.content;

public class FileProvider extends android.content.ContentProvider {
    private static final java.lang.String ATTR_NAME = "name";
    private static final java.lang.String ATTR_PATH = "path";
    private static final java.lang.String[] COLUMNS = {"_display_name", "_size"};
    private static final java.io.File DEVICE_ROOT = new java.io.File("/");
    private static final java.lang.String META_DATA_FILE_PROVIDER_PATHS = "android.support.FILE_PROVIDER_PATHS";
    private static final java.lang.String TAG_CACHE_PATH = "cache-path";
    private static final java.lang.String TAG_EXTERNAL = "external-path";
    private static final java.lang.String TAG_EXTERNAL_CACHE = "external-cache-path";
    private static final java.lang.String TAG_EXTERNAL_FILES = "external-files-path";
    private static final java.lang.String TAG_FILES_PATH = "files-path";
    private static final java.lang.String TAG_ROOT_PATH = "root-path";
    private static java.util.HashMap<java.lang.String, android.support.v4.content.FileProvider.PathStrategy> sCache = new java.util.HashMap<>();
    private android.support.v4.content.FileProvider.PathStrategy mStrategy;

    interface PathStrategy {
        java.io.File getFileForUri(android.net.Uri uri);

        android.net.Uri getUriForFile(java.io.File file);
    }

    static class SimplePathStrategy implements android.support.v4.content.FileProvider.PathStrategy {
        private final java.lang.String mAuthority;
        private final java.util.HashMap<java.lang.String, java.io.File> mRoots = new java.util.HashMap<>();

        public SimplePathStrategy(java.lang.String authority) {
            this.mAuthority = authority;
        }

        public void addRoot(java.lang.String name, java.io.File root) {
            if (android.text.TextUtils.isEmpty(name)) {
                throw new java.lang.IllegalArgumentException("Name must not be empty");
            }
            try {
                this.mRoots.put(name, root.getCanonicalFile());
            } catch (java.io.IOException e) {
                throw new java.lang.IllegalArgumentException("Failed to resolve canonical path for " + root, e);
            }
        }

        public android.net.Uri getUriForFile(java.io.File file) {
            java.lang.String path;
            try {
                java.lang.String path2 = file.getCanonicalPath();
                java.util.Map.Entry<java.lang.String, java.io.File> mostSpecific = null;
                for (java.util.Map.Entry<java.lang.String, java.io.File> root : this.mRoots.entrySet()) {
                    java.lang.String rootPath = ((java.io.File) root.getValue()).getPath();
                    if (path2.startsWith(rootPath) && (mostSpecific == null || rootPath.length() > ((java.io.File) mostSpecific.getValue()).getPath().length())) {
                        mostSpecific = root;
                    }
                }
                if (mostSpecific == null) {
                    throw new java.lang.IllegalArgumentException("Failed to find configured root that contains " + path2);
                }
                java.lang.String rootPath2 = ((java.io.File) mostSpecific.getValue()).getPath();
                if (rootPath2.endsWith("/")) {
                    path = path2.substring(rootPath2.length());
                } else {
                    path = path2.substring(rootPath2.length() + 1);
                }
                return new android.net.Uri.Builder().scheme("content").authority(this.mAuthority).encodedPath(android.net.Uri.encode((java.lang.String) mostSpecific.getKey()) + '/' + android.net.Uri.encode(path, "/")).build();
            } catch (java.io.IOException e) {
                throw new java.lang.IllegalArgumentException("Failed to resolve canonical path for " + file);
            }
        }

        public java.io.File getFileForUri(android.net.Uri uri) {
            java.lang.String path = uri.getEncodedPath();
            int splitIndex = path.indexOf(47, 1);
            java.lang.String tag = android.net.Uri.decode(path.substring(1, splitIndex));
            java.lang.String path2 = android.net.Uri.decode(path.substring(splitIndex + 1));
            java.io.File root = (java.io.File) this.mRoots.get(tag);
            if (root == null) {
                throw new java.lang.IllegalArgumentException("Unable to find configured root for " + uri);
            }
            java.io.File file = new java.io.File(root, path2);
            try {
                java.io.File file2 = file.getCanonicalFile();
                if (file2.getPath().startsWith(root.getPath())) {
                    return file2;
                }
                throw new java.lang.SecurityException("Resolved path jumped beyond configured root");
            } catch (java.io.IOException e) {
                throw new java.lang.IllegalArgumentException("Failed to resolve canonical path for " + file);
            }
        }
    }

    public boolean onCreate() {
        return true;
    }

    public void attachInfo(android.content.Context context, android.content.pm.ProviderInfo info) {
        super.attachInfo(context, info);
        if (info.exported) {
            throw new java.lang.SecurityException("Provider must not be exported");
        } else if (!info.grantUriPermissions) {
            throw new java.lang.SecurityException("Provider must grant uri permissions");
        } else {
            this.mStrategy = getPathStrategy(context, info.authority);
        }
    }

    public static android.net.Uri getUriForFile(android.content.Context context, java.lang.String authority, java.io.File file) {
        return getPathStrategy(context, authority).getUriForFile(file);
    }

    public android.database.Cursor query(android.net.Uri uri, java.lang.String[] projection, java.lang.String selection, java.lang.String[] selectionArgs, java.lang.String sortOrder) {
        int i;
        java.io.File file = this.mStrategy.getFileForUri(uri);
        if (projection == null) {
            projection = COLUMNS;
        }
        java.lang.String[] cols = new java.lang.String[projection.length];
        java.lang.Object[] values = new java.lang.Object[projection.length];
        int length = projection.length;
        int i2 = 0;
        int i3 = 0;
        while (i2 < length) {
            java.lang.String col = projection[i2];
            if ("_display_name".equals(col)) {
                cols[i3] = "_display_name";
                i = i3 + 1;
                values[i3] = file.getName();
            } else if ("_size".equals(col)) {
                cols[i3] = "_size";
                i = i3 + 1;
                values[i3] = java.lang.Long.valueOf(file.length());
            } else {
                i = i3;
            }
            i2++;
            i3 = i;
        }
        java.lang.String[] cols2 = copyOf(cols, i3);
        java.lang.Object[] values2 = copyOf(values, i3);
        android.database.MatrixCursor cursor = new android.database.MatrixCursor(cols2, 1);
        cursor.addRow(values2);
        return cursor;
    }

    public java.lang.String getType(android.net.Uri uri) {
        java.io.File file = this.mStrategy.getFileForUri(uri);
        int lastDot = file.getName().lastIndexOf(46);
        if (lastDot >= 0) {
            java.lang.String mime = android.webkit.MimeTypeMap.getSingleton().getMimeTypeFromExtension(file.getName().substring(lastDot + 1));
            if (mime != null) {
                return mime;
            }
        }
        return "application/octet-stream";
    }

    public android.net.Uri insert(android.net.Uri uri, android.content.ContentValues values) {
        throw new java.lang.UnsupportedOperationException("No external inserts");
    }

    public int update(android.net.Uri uri, android.content.ContentValues values, java.lang.String selection, java.lang.String[] selectionArgs) {
        throw new java.lang.UnsupportedOperationException("No external updates");
    }

    public int delete(android.net.Uri uri, java.lang.String selection, java.lang.String[] selectionArgs) {
        return this.mStrategy.getFileForUri(uri).delete() ? 1 : 0;
    }

    public android.os.ParcelFileDescriptor openFile(android.net.Uri uri, java.lang.String mode) throws java.io.FileNotFoundException {
        return android.os.ParcelFileDescriptor.open(this.mStrategy.getFileForUri(uri), modeToMode(mode));
    }

    private static android.support.v4.content.FileProvider.PathStrategy getPathStrategy(android.content.Context context, java.lang.String authority) {
        android.support.v4.content.FileProvider.PathStrategy strat;
        synchronized (sCache) {
            strat = (android.support.v4.content.FileProvider.PathStrategy) sCache.get(authority);
            if (strat == null) {
                try {
                    strat = parsePathStrategy(context, authority);
                    sCache.put(authority, strat);
                } catch (java.io.IOException e) {
                    throw new java.lang.IllegalArgumentException("Failed to parse android.support.FILE_PROVIDER_PATHS meta-data", e);
                } catch (org.xmlpull.v1.XmlPullParserException e2) {
                    throw new java.lang.IllegalArgumentException("Failed to parse android.support.FILE_PROVIDER_PATHS meta-data", e2);
                }
            }
        }
        return strat;
    }

    private static android.support.v4.content.FileProvider.PathStrategy parsePathStrategy(android.content.Context context, java.lang.String authority) throws java.io.IOException, org.xmlpull.v1.XmlPullParserException {
        android.support.v4.content.FileProvider.SimplePathStrategy strat = new android.support.v4.content.FileProvider.SimplePathStrategy(authority);
        android.content.res.XmlResourceParser in = context.getPackageManager().resolveContentProvider(authority, 128).loadXmlMetaData(context.getPackageManager(), META_DATA_FILE_PROVIDER_PATHS);
        if (in == null) {
            throw new java.lang.IllegalArgumentException("Missing android.support.FILE_PROVIDER_PATHS meta-data");
        }
        while (true) {
            int type = in.next();
            if (type == 1) {
                return strat;
            }
            if (type == 2) {
                java.lang.String tag = in.getName();
                java.lang.String name = in.getAttributeValue(null, "name");
                java.lang.String path = in.getAttributeValue(null, ATTR_PATH);
                java.io.File target = null;
                if (TAG_ROOT_PATH.equals(tag)) {
                    target = DEVICE_ROOT;
                } else if (TAG_FILES_PATH.equals(tag)) {
                    target = context.getFilesDir();
                } else if (TAG_CACHE_PATH.equals(tag)) {
                    target = context.getCacheDir();
                } else if (TAG_EXTERNAL.equals(tag)) {
                    target = android.os.Environment.getExternalStorageDirectory();
                } else if (TAG_EXTERNAL_FILES.equals(tag)) {
                    java.io.File[] externalFilesDirs = android.support.v4.content.ContextCompat.getExternalFilesDirs(context, null);
                    if (externalFilesDirs.length > 0) {
                        target = externalFilesDirs[0];
                    }
                } else if (TAG_EXTERNAL_CACHE.equals(tag)) {
                    java.io.File[] externalCacheDirs = android.support.v4.content.ContextCompat.getExternalCacheDirs(context);
                    if (externalCacheDirs.length > 0) {
                        target = externalCacheDirs[0];
                    }
                }
                if (target != null) {
                    strat.addRoot(name, buildPath(target, path));
                }
            }
        }
    }

    private static int modeToMode(java.lang.String mode) {
        if ("r".equals(mode)) {
            return 268435456;
        }
        if ("w".equals(mode) || "wt".equals(mode)) {
            return 738197504;
        }
        if ("wa".equals(mode)) {
            return 704643072;
        }
        if ("rw".equals(mode)) {
            return 939524096;
        }
        if ("rwt".equals(mode)) {
            return 1006632960;
        }
        throw new java.lang.IllegalArgumentException("Invalid mode: " + mode);
    }

    private static java.io.File buildPath(java.io.File base, java.lang.String... segments) {
        java.io.File cur;
        java.io.File cur2 = base;
        int length = segments.length;
        int i = 0;
        java.io.File cur3 = cur2;
        while (i < length) {
            java.lang.String segment = segments[i];
            if (segment != null) {
                cur = new java.io.File(cur3, segment);
            } else {
                cur = cur3;
            }
            i++;
            cur3 = cur;
        }
        return cur3;
    }

    private static java.lang.String[] copyOf(java.lang.String[] original, int newLength) {
        java.lang.String[] result = new java.lang.String[newLength];
        java.lang.System.arraycopy(original, 0, result, 0, newLength);
        return result;
    }

    private static java.lang.Object[] copyOf(java.lang.Object[] original, int newLength) {
        java.lang.Object[] result = new java.lang.Object[newLength];
        java.lang.System.arraycopy(original, 0, result, 0, newLength);
        return result;
    }
}
