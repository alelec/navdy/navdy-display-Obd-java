package android.support.v4.content;

@android.annotation.TargetApi(11)
@android.support.annotation.RequiresApi(11)
class ContextCompatHoneycomb {
    ContextCompatHoneycomb() {
    }

    static void startActivities(android.content.Context context, android.content.Intent[] intents) {
        context.startActivities(intents);
    }

    public static java.io.File getObbDir(android.content.Context context) {
        return context.getObbDir();
    }
}
