package android.support.v4.content.res;

@android.annotation.TargetApi(13)
@android.support.annotation.RequiresApi(13)
class ConfigurationHelperHoneycombMr2 {
    ConfigurationHelperHoneycombMr2() {
    }

    static int getScreenHeightDp(@android.support.annotation.NonNull android.content.res.Resources resources) {
        return resources.getConfiguration().screenHeightDp;
    }

    static int getScreenWidthDp(@android.support.annotation.NonNull android.content.res.Resources resources) {
        return resources.getConfiguration().screenWidthDp;
    }

    static int getSmallestScreenWidthDp(@android.support.annotation.NonNull android.content.res.Resources resources) {
        return resources.getConfiguration().smallestScreenWidthDp;
    }
}
