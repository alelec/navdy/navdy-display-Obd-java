package android.support.v4.content.res;

public final class ResourcesCompat {
    @android.support.annotation.Nullable
    public static android.graphics.drawable.Drawable getDrawable(@android.support.annotation.NonNull android.content.res.Resources res, @android.support.annotation.DrawableRes int id, @android.support.annotation.Nullable android.content.res.Resources.Theme theme) throws android.content.res.Resources.NotFoundException {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            return android.support.v4.content.res.ResourcesCompatApi21.getDrawable(res, id, theme);
        }
        return res.getDrawable(id);
    }

    @android.support.annotation.Nullable
    public static android.graphics.drawable.Drawable getDrawableForDensity(@android.support.annotation.NonNull android.content.res.Resources res, @android.support.annotation.DrawableRes int id, int density, @android.support.annotation.Nullable android.content.res.Resources.Theme theme) throws android.content.res.Resources.NotFoundException {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            return android.support.v4.content.res.ResourcesCompatApi21.getDrawableForDensity(res, id, density, theme);
        }
        if (android.os.Build.VERSION.SDK_INT >= 15) {
            return android.support.v4.content.res.ResourcesCompatIcsMr1.getDrawableForDensity(res, id, density);
        }
        return res.getDrawable(id);
    }

    @android.support.annotation.ColorInt
    public static int getColor(@android.support.annotation.NonNull android.content.res.Resources res, @android.support.annotation.ColorRes int id, @android.support.annotation.Nullable android.content.res.Resources.Theme theme) throws android.content.res.Resources.NotFoundException {
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            return android.support.v4.content.res.ResourcesCompatApi23.getColor(res, id, theme);
        }
        return res.getColor(id);
    }

    @android.support.annotation.Nullable
    public static android.content.res.ColorStateList getColorStateList(@android.support.annotation.NonNull android.content.res.Resources res, @android.support.annotation.ColorRes int id, @android.support.annotation.Nullable android.content.res.Resources.Theme theme) throws android.content.res.Resources.NotFoundException {
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            return android.support.v4.content.res.ResourcesCompatApi23.getColorStateList(res, id, theme);
        }
        return res.getColorStateList(id);
    }

    private ResourcesCompat() {
    }
}
