package android.support.v4.content.res;

@android.annotation.TargetApi(17)
@android.support.annotation.RequiresApi(17)
class ConfigurationHelperJellybeanMr1 {
    ConfigurationHelperJellybeanMr1() {
    }

    static int getDensityDpi(@android.support.annotation.NonNull android.content.res.Resources resources) {
        return resources.getConfiguration().densityDpi;
    }
}
