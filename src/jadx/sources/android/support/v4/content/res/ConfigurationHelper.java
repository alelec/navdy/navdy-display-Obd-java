package android.support.v4.content.res;

public final class ConfigurationHelper {
    private static final android.support.v4.content.res.ConfigurationHelper.ConfigurationHelperImpl IMPL;

    private interface ConfigurationHelperImpl {
        int getDensityDpi(@android.support.annotation.NonNull android.content.res.Resources resources);

        int getScreenHeightDp(@android.support.annotation.NonNull android.content.res.Resources resources);

        int getScreenWidthDp(@android.support.annotation.NonNull android.content.res.Resources resources);

        int getSmallestScreenWidthDp(@android.support.annotation.NonNull android.content.res.Resources resources);
    }

    private static class GingerbreadImpl implements android.support.v4.content.res.ConfigurationHelper.ConfigurationHelperImpl {
        GingerbreadImpl() {
        }

        public int getScreenHeightDp(@android.support.annotation.NonNull android.content.res.Resources resources) {
            return android.support.v4.content.res.ConfigurationHelperGingerbread.getScreenHeightDp(resources);
        }

        public int getScreenWidthDp(@android.support.annotation.NonNull android.content.res.Resources resources) {
            return android.support.v4.content.res.ConfigurationHelperGingerbread.getScreenWidthDp(resources);
        }

        public int getSmallestScreenWidthDp(@android.support.annotation.NonNull android.content.res.Resources resources) {
            return android.support.v4.content.res.ConfigurationHelperGingerbread.getSmallestScreenWidthDp(resources);
        }

        public int getDensityDpi(@android.support.annotation.NonNull android.content.res.Resources resources) {
            return android.support.v4.content.res.ConfigurationHelperGingerbread.getDensityDpi(resources);
        }
    }

    private static class HoneycombMr2Impl extends android.support.v4.content.res.ConfigurationHelper.GingerbreadImpl {
        HoneycombMr2Impl() {
        }

        public int getScreenHeightDp(@android.support.annotation.NonNull android.content.res.Resources resources) {
            return android.support.v4.content.res.ConfigurationHelperHoneycombMr2.getScreenHeightDp(resources);
        }

        public int getScreenWidthDp(@android.support.annotation.NonNull android.content.res.Resources resources) {
            return android.support.v4.content.res.ConfigurationHelperHoneycombMr2.getScreenWidthDp(resources);
        }

        public int getSmallestScreenWidthDp(@android.support.annotation.NonNull android.content.res.Resources resources) {
            return android.support.v4.content.res.ConfigurationHelperHoneycombMr2.getSmallestScreenWidthDp(resources);
        }
    }

    private static class JellybeanMr1Impl extends android.support.v4.content.res.ConfigurationHelper.HoneycombMr2Impl {
        JellybeanMr1Impl() {
        }

        public int getDensityDpi(@android.support.annotation.NonNull android.content.res.Resources resources) {
            return android.support.v4.content.res.ConfigurationHelperJellybeanMr1.getDensityDpi(resources);
        }
    }

    static {
        int sdk = android.os.Build.VERSION.SDK_INT;
        if (sdk >= 17) {
            IMPL = new android.support.v4.content.res.ConfigurationHelper.JellybeanMr1Impl();
        } else if (sdk >= 13) {
            IMPL = new android.support.v4.content.res.ConfigurationHelper.HoneycombMr2Impl();
        } else {
            IMPL = new android.support.v4.content.res.ConfigurationHelper.GingerbreadImpl();
        }
    }

    private ConfigurationHelper() {
    }

    public static int getScreenHeightDp(@android.support.annotation.NonNull android.content.res.Resources resources) {
        return IMPL.getScreenHeightDp(resources);
    }

    public static int getScreenWidthDp(@android.support.annotation.NonNull android.content.res.Resources resources) {
        return IMPL.getScreenWidthDp(resources);
    }

    public static int getSmallestScreenWidthDp(@android.support.annotation.NonNull android.content.res.Resources resources) {
        return IMPL.getSmallestScreenWidthDp(resources);
    }

    public static int getDensityDpi(@android.support.annotation.NonNull android.content.res.Resources resources) {
        return IMPL.getDensityDpi(resources);
    }
}
