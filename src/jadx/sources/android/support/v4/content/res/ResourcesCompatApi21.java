package android.support.v4.content.res;

@android.annotation.TargetApi(21)
@android.support.annotation.RequiresApi(21)
class ResourcesCompatApi21 {
    ResourcesCompatApi21() {
    }

    public static android.graphics.drawable.Drawable getDrawable(android.content.res.Resources res, int id, android.content.res.Resources.Theme theme) throws android.content.res.Resources.NotFoundException {
        return res.getDrawable(id, theme);
    }

    public static android.graphics.drawable.Drawable getDrawableForDensity(android.content.res.Resources res, int id, int density, android.content.res.Resources.Theme theme) throws android.content.res.Resources.NotFoundException {
        return res.getDrawableForDensity(id, density, theme);
    }
}
