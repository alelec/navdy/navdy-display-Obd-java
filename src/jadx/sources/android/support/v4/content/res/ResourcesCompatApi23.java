package android.support.v4.content.res;

@android.annotation.TargetApi(23)
@android.support.annotation.RequiresApi(23)
class ResourcesCompatApi23 {
    ResourcesCompatApi23() {
    }

    public static int getColor(android.content.res.Resources res, int id, android.content.res.Resources.Theme theme) throws android.content.res.Resources.NotFoundException {
        return res.getColor(id, theme);
    }

    public static android.content.res.ColorStateList getColorStateList(android.content.res.Resources res, int id, android.content.res.Resources.Theme theme) throws android.content.res.Resources.NotFoundException {
        return res.getColorStateList(id, theme);
    }
}
