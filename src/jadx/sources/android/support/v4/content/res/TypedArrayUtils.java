package android.support.v4.content.res;

@android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
public class TypedArrayUtils {
    public static boolean getBoolean(android.content.res.TypedArray a, @android.support.annotation.StyleableRes int index, @android.support.annotation.StyleableRes int fallbackIndex, boolean defaultValue) {
        return a.getBoolean(index, a.getBoolean(fallbackIndex, defaultValue));
    }

    public static android.graphics.drawable.Drawable getDrawable(android.content.res.TypedArray a, @android.support.annotation.StyleableRes int index, @android.support.annotation.StyleableRes int fallbackIndex) {
        android.graphics.drawable.Drawable val = a.getDrawable(index);
        if (val == null) {
            return a.getDrawable(fallbackIndex);
        }
        return val;
    }

    public static int getInt(android.content.res.TypedArray a, @android.support.annotation.StyleableRes int index, @android.support.annotation.StyleableRes int fallbackIndex, int defaultValue) {
        return a.getInt(index, a.getInt(fallbackIndex, defaultValue));
    }

    @android.support.annotation.AnyRes
    public static int getResourceId(android.content.res.TypedArray a, @android.support.annotation.StyleableRes int index, @android.support.annotation.StyleableRes int fallbackIndex, @android.support.annotation.AnyRes int defaultValue) {
        return a.getResourceId(index, a.getResourceId(fallbackIndex, defaultValue));
    }

    public static java.lang.String getString(android.content.res.TypedArray a, @android.support.annotation.StyleableRes int index, @android.support.annotation.StyleableRes int fallbackIndex) {
        java.lang.String val = a.getString(index);
        if (val == null) {
            return a.getString(fallbackIndex);
        }
        return val;
    }

    public static java.lang.CharSequence getText(android.content.res.TypedArray a, @android.support.annotation.StyleableRes int index, @android.support.annotation.StyleableRes int fallbackIndex) {
        java.lang.CharSequence val = a.getText(index);
        if (val == null) {
            return a.getText(fallbackIndex);
        }
        return val;
    }

    public static java.lang.CharSequence[] getTextArray(android.content.res.TypedArray a, @android.support.annotation.StyleableRes int index, @android.support.annotation.StyleableRes int fallbackIndex) {
        java.lang.CharSequence[] val = a.getTextArray(index);
        if (val == null) {
            return a.getTextArray(fallbackIndex);
        }
        return val;
    }

    public static int getAttr(android.content.Context context, int attr, int fallbackAttr) {
        android.util.TypedValue value = new android.util.TypedValue();
        context.getTheme().resolveAttribute(attr, value, true);
        return value.resourceId != 0 ? attr : fallbackAttr;
    }
}
