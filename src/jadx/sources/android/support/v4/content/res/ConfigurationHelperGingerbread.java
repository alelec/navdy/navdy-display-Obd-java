package android.support.v4.content.res;

@android.annotation.TargetApi(9)
@android.support.annotation.RequiresApi(9)
class ConfigurationHelperGingerbread {
    ConfigurationHelperGingerbread() {
    }

    static int getScreenHeightDp(@android.support.annotation.NonNull android.content.res.Resources resources) {
        android.util.DisplayMetrics metrics = resources.getDisplayMetrics();
        return (int) (((float) metrics.heightPixels) / metrics.density);
    }

    static int getScreenWidthDp(@android.support.annotation.NonNull android.content.res.Resources resources) {
        android.util.DisplayMetrics metrics = resources.getDisplayMetrics();
        return (int) (((float) metrics.widthPixels) / metrics.density);
    }

    static int getSmallestScreenWidthDp(@android.support.annotation.NonNull android.content.res.Resources resources) {
        return java.lang.Math.min(getScreenWidthDp(resources), getScreenHeightDp(resources));
    }

    static int getDensityDpi(@android.support.annotation.NonNull android.content.res.Resources resources) {
        return resources.getDisplayMetrics().densityDpi;
    }
}
