package android.support.v4.content.res;

@android.annotation.TargetApi(15)
@android.support.annotation.RequiresApi(15)
class ResourcesCompatIcsMr1 {
    ResourcesCompatIcsMr1() {
    }

    public static android.graphics.drawable.Drawable getDrawableForDensity(android.content.res.Resources res, int id, int density) throws android.content.res.Resources.NotFoundException {
        return res.getDrawableForDensity(id, density);
    }
}
