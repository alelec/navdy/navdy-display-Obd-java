package android.support.v4.content;

@android.annotation.TargetApi(23)
@android.support.annotation.RequiresApi(23)
class ContextCompatApi23 {
    ContextCompatApi23() {
    }

    public static android.content.res.ColorStateList getColorStateList(android.content.Context context, int id) {
        return context.getColorStateList(id);
    }

    public static int getColor(android.content.Context context, int id) {
        return context.getColor(id);
    }
}
