package android.support.v4.content;

@android.annotation.TargetApi(16)
@android.support.annotation.RequiresApi(16)
class ContentResolverCompatJellybean {
    ContentResolverCompatJellybean() {
    }

    public static android.database.Cursor query(android.content.ContentResolver resolver, android.net.Uri uri, java.lang.String[] projection, java.lang.String selection, java.lang.String[] selectionArgs, java.lang.String sortOrder, java.lang.Object cancellationSignalObj) {
        return resolver.query(uri, projection, selection, selectionArgs, sortOrder, (android.os.CancellationSignal) cancellationSignalObj);
    }

    static boolean isFrameworkOperationCanceledException(java.lang.Exception e) {
        return e instanceof android.os.OperationCanceledException;
    }
}
