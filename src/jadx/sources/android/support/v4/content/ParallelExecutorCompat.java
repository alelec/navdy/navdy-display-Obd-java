package android.support.v4.content;

public final class ParallelExecutorCompat {
    public static java.util.concurrent.Executor getParallelExecutor() {
        if (android.os.Build.VERSION.SDK_INT >= 11) {
            return android.support.v4.content.ExecutorCompatHoneycomb.getParallelExecutor();
        }
        return android.support.v4.content.ModernAsyncTask.THREAD_POOL_EXECUTOR;
    }

    private ParallelExecutorCompat() {
    }
}
