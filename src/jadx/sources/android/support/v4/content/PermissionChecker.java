package android.support.v4.content;

public final class PermissionChecker {
    public static final int PERMISSION_DENIED = -1;
    public static final int PERMISSION_DENIED_APP_OP = -2;
    public static final int PERMISSION_GRANTED = 0;

    @java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.SOURCE)
    public @interface PermissionResult {
    }

    private PermissionChecker() {
    }

    public static int checkPermission(@android.support.annotation.NonNull android.content.Context context, @android.support.annotation.NonNull java.lang.String permission, int pid, int uid, java.lang.String packageName) {
        if (context.checkPermission(permission, pid, uid) == -1) {
            return -1;
        }
        java.lang.String op = android.support.v4.app.AppOpsManagerCompat.permissionToOp(permission);
        if (op == null) {
            return 0;
        }
        if (packageName == null) {
            java.lang.String[] packageNames = context.getPackageManager().getPackagesForUid(uid);
            if (packageNames == null || packageNames.length <= 0) {
                return -1;
            }
            packageName = packageNames[0];
        }
        return android.support.v4.app.AppOpsManagerCompat.noteProxyOp(context, op, packageName) != 0 ? -2 : 0;
    }

    public static int checkSelfPermission(@android.support.annotation.NonNull android.content.Context context, @android.support.annotation.NonNull java.lang.String permission) {
        return checkPermission(context, permission, android.os.Process.myPid(), android.os.Process.myUid(), context.getPackageName());
    }

    public static int checkCallingPermission(@android.support.annotation.NonNull android.content.Context context, @android.support.annotation.NonNull java.lang.String permission, java.lang.String packageName) {
        if (android.os.Binder.getCallingPid() == android.os.Process.myPid()) {
            return -1;
        }
        return checkPermission(context, permission, android.os.Binder.getCallingPid(), android.os.Binder.getCallingUid(), packageName);
    }

    public static int checkCallingOrSelfPermission(@android.support.annotation.NonNull android.content.Context context, @android.support.annotation.NonNull java.lang.String permission) {
        return checkPermission(context, permission, android.os.Binder.getCallingPid(), android.os.Binder.getCallingUid(), android.os.Binder.getCallingPid() == android.os.Process.myPid() ? context.getPackageName() : null);
    }
}
