package android.support.v4.content;

@android.annotation.TargetApi(11)
@android.support.annotation.RequiresApi(11)
class ExecutorCompatHoneycomb {
    ExecutorCompatHoneycomb() {
    }

    public static java.util.concurrent.Executor getParallelExecutor() {
        return android.os.AsyncTask.THREAD_POOL_EXECUTOR;
    }
}
