package android.support.v4.graphics;

@android.support.annotation.RequiresApi(9)
class PaintCompatGingerbread {
    private static final java.lang.String TOFU_STRING = "\udb3f\udffd";
    private static final java.lang.ThreadLocal<android.support.v4.util.Pair<android.graphics.Rect, android.graphics.Rect>> sRectThreadLocal = new java.lang.ThreadLocal<>();

    PaintCompatGingerbread() {
    }

    static boolean hasGlyph(@android.support.annotation.NonNull android.graphics.Paint paint, @android.support.annotation.NonNull java.lang.String string) {
        int length = string.length();
        if (length == 1 && java.lang.Character.isWhitespace(string.charAt(0))) {
            return true;
        }
        float missingGlyphWidth = paint.measureText(TOFU_STRING);
        float width = paint.measureText(string);
        if (width == 0.0f) {
            return false;
        }
        if (string.codePointCount(0, string.length()) > 1) {
            if (width > 2.0f * missingGlyphWidth) {
                return false;
            }
            float sumWidth = 0.0f;
            int i = 0;
            while (i < length) {
                int charCount = java.lang.Character.charCount(string.codePointAt(i));
                sumWidth += paint.measureText(string, i, i + charCount);
                i += charCount;
            }
            if (width >= sumWidth) {
                return false;
            }
        }
        if (width != missingGlyphWidth) {
            return true;
        }
        android.support.v4.util.Pair<android.graphics.Rect, android.graphics.Rect> rects = obtainEmptyRects();
        paint.getTextBounds(TOFU_STRING, 0, TOFU_STRING.length(), (android.graphics.Rect) rects.first);
        paint.getTextBounds(string, 0, length, (android.graphics.Rect) rects.second);
        return !((android.graphics.Rect) rects.first).equals(rects.second);
    }

    private static android.support.v4.util.Pair<android.graphics.Rect, android.graphics.Rect> obtainEmptyRects() {
        android.support.v4.util.Pair<android.graphics.Rect, android.graphics.Rect> rects = (android.support.v4.util.Pair) sRectThreadLocal.get();
        if (rects == null) {
            android.support.v4.util.Pair<android.graphics.Rect, android.graphics.Rect> rects2 = new android.support.v4.util.Pair<>(new android.graphics.Rect(), new android.graphics.Rect());
            sRectThreadLocal.set(rects2);
            return rects2;
        }
        ((android.graphics.Rect) rects.first).setEmpty();
        ((android.graphics.Rect) rects.second).setEmpty();
        return rects;
    }
}
