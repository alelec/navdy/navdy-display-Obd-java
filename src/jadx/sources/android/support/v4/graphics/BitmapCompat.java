package android.support.v4.graphics;

public final class BitmapCompat {
    static final android.support.v4.graphics.BitmapCompat.BitmapImpl IMPL;

    static class BaseBitmapImpl implements android.support.v4.graphics.BitmapCompat.BitmapImpl {
        BaseBitmapImpl() {
        }

        public boolean hasMipMap(android.graphics.Bitmap bitmap) {
            return false;
        }

        public void setHasMipMap(android.graphics.Bitmap bitmap, boolean hasMipMap) {
        }

        public int getAllocationByteCount(android.graphics.Bitmap bitmap) {
            return bitmap.getRowBytes() * bitmap.getHeight();
        }
    }

    interface BitmapImpl {
        int getAllocationByteCount(android.graphics.Bitmap bitmap);

        boolean hasMipMap(android.graphics.Bitmap bitmap);

        void setHasMipMap(android.graphics.Bitmap bitmap, boolean z);
    }

    static class HcMr1BitmapCompatImpl extends android.support.v4.graphics.BitmapCompat.BaseBitmapImpl {
        HcMr1BitmapCompatImpl() {
        }

        public int getAllocationByteCount(android.graphics.Bitmap bitmap) {
            return android.support.v4.graphics.BitmapCompatHoneycombMr1.getAllocationByteCount(bitmap);
        }
    }

    static class JbMr2BitmapCompatImpl extends android.support.v4.graphics.BitmapCompat.HcMr1BitmapCompatImpl {
        JbMr2BitmapCompatImpl() {
        }

        public boolean hasMipMap(android.graphics.Bitmap bitmap) {
            return android.support.v4.graphics.BitmapCompatJellybeanMR2.hasMipMap(bitmap);
        }

        public void setHasMipMap(android.graphics.Bitmap bitmap, boolean hasMipMap) {
            android.support.v4.graphics.BitmapCompatJellybeanMR2.setHasMipMap(bitmap, hasMipMap);
        }
    }

    static class KitKatBitmapCompatImpl extends android.support.v4.graphics.BitmapCompat.JbMr2BitmapCompatImpl {
        KitKatBitmapCompatImpl() {
        }

        public int getAllocationByteCount(android.graphics.Bitmap bitmap) {
            return android.support.v4.graphics.BitmapCompatKitKat.getAllocationByteCount(bitmap);
        }
    }

    static {
        int version = android.os.Build.VERSION.SDK_INT;
        if (version >= 19) {
            IMPL = new android.support.v4.graphics.BitmapCompat.KitKatBitmapCompatImpl();
        } else if (version >= 18) {
            IMPL = new android.support.v4.graphics.BitmapCompat.JbMr2BitmapCompatImpl();
        } else if (version >= 12) {
            IMPL = new android.support.v4.graphics.BitmapCompat.HcMr1BitmapCompatImpl();
        } else {
            IMPL = new android.support.v4.graphics.BitmapCompat.BaseBitmapImpl();
        }
    }

    public static boolean hasMipMap(android.graphics.Bitmap bitmap) {
        return IMPL.hasMipMap(bitmap);
    }

    public static void setHasMipMap(android.graphics.Bitmap bitmap, boolean hasMipMap) {
        IMPL.setHasMipMap(bitmap, hasMipMap);
    }

    public static int getAllocationByteCount(android.graphics.Bitmap bitmap) {
        return IMPL.getAllocationByteCount(bitmap);
    }

    private BitmapCompat() {
    }
}
