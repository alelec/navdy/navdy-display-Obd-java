package android.support.v4.graphics;

@android.support.annotation.RequiresApi(23)
class PaintCompatApi23 {
    PaintCompatApi23() {
    }

    static boolean hasGlyph(@android.support.annotation.NonNull android.graphics.Paint paint, @android.support.annotation.NonNull java.lang.String string) {
        return paint.hasGlyph(string);
    }
}
