package android.support.v4.graphics;

public final class ColorUtils {
    private static final int MIN_ALPHA_SEARCH_MAX_ITERATIONS = 10;
    private static final int MIN_ALPHA_SEARCH_PRECISION = 1;
    private static final java.lang.ThreadLocal<double[]> TEMP_ARRAY = new java.lang.ThreadLocal<>();
    private static final double XYZ_EPSILON = 0.008856d;
    private static final double XYZ_KAPPA = 903.3d;
    private static final double XYZ_WHITE_REFERENCE_X = 95.047d;
    private static final double XYZ_WHITE_REFERENCE_Y = 100.0d;
    private static final double XYZ_WHITE_REFERENCE_Z = 108.883d;

    private ColorUtils() {
    }

    public static int compositeColors(@android.support.annotation.ColorInt int foreground, @android.support.annotation.ColorInt int background) {
        int bgAlpha = android.graphics.Color.alpha(background);
        int fgAlpha = android.graphics.Color.alpha(foreground);
        int a = compositeAlpha(fgAlpha, bgAlpha);
        return android.graphics.Color.argb(a, compositeComponent(android.graphics.Color.red(foreground), fgAlpha, android.graphics.Color.red(background), bgAlpha, a), compositeComponent(android.graphics.Color.green(foreground), fgAlpha, android.graphics.Color.green(background), bgAlpha, a), compositeComponent(android.graphics.Color.blue(foreground), fgAlpha, android.graphics.Color.blue(background), bgAlpha, a));
    }

    private static int compositeAlpha(int foregroundAlpha, int backgroundAlpha) {
        return 255 - (((255 - backgroundAlpha) * (255 - foregroundAlpha)) / 255);
    }

    private static int compositeComponent(int fgC, int fgA, int bgC, int bgA, int a) {
        if (a == 0) {
            return 0;
        }
        return (((fgC * 255) * fgA) + ((bgC * bgA) * (255 - fgA))) / (a * 255);
    }

    @android.support.annotation.FloatRange(from = 0.0d, to = 1.0d)
    public static double calculateLuminance(@android.support.annotation.ColorInt int color) {
        double[] result = getTempDouble3Array();
        colorToXYZ(color, result);
        return result[1] / 100.0d;
    }

    public static double calculateContrast(@android.support.annotation.ColorInt int foreground, @android.support.annotation.ColorInt int background) {
        if (android.graphics.Color.alpha(background) != 255) {
            throw new java.lang.IllegalArgumentException("background can not be translucent: #" + java.lang.Integer.toHexString(background));
        }
        if (android.graphics.Color.alpha(foreground) < 255) {
            foreground = compositeColors(foreground, background);
        }
        double luminance1 = calculateLuminance(foreground) + 0.05d;
        double luminance2 = calculateLuminance(background) + 0.05d;
        return java.lang.Math.max(luminance1, luminance2) / java.lang.Math.min(luminance1, luminance2);
    }

    public static int calculateMinimumAlpha(@android.support.annotation.ColorInt int foreground, @android.support.annotation.ColorInt int background, float minContrastRatio) {
        if (android.graphics.Color.alpha(background) != 255) {
            throw new java.lang.IllegalArgumentException("background can not be translucent: #" + java.lang.Integer.toHexString(background));
        } else if (calculateContrast(setAlphaComponent(foreground, 255), background) < ((double) minContrastRatio)) {
            return -1;
        } else {
            int minAlpha = 0;
            int maxAlpha = 255;
            for (int numIterations = 0; numIterations <= 10 && maxAlpha - minAlpha > 1; numIterations++) {
                int testAlpha = (minAlpha + maxAlpha) / 2;
                if (calculateContrast(setAlphaComponent(foreground, testAlpha), background) < ((double) minContrastRatio)) {
                    minAlpha = testAlpha;
                } else {
                    maxAlpha = testAlpha;
                }
            }
            return maxAlpha;
        }
    }

    public static void RGBToHSL(@android.support.annotation.IntRange(from = 0, to = 255) int r, @android.support.annotation.IntRange(from = 0, to = 255) int g, @android.support.annotation.IntRange(from = 0, to = 255) int b, @android.support.annotation.NonNull float[] outHsl) {
        float h;
        float s;
        float rf = ((float) r) / 255.0f;
        float gf = ((float) g) / 255.0f;
        float bf = ((float) b) / 255.0f;
        float max = java.lang.Math.max(rf, java.lang.Math.max(gf, bf));
        float min = java.lang.Math.min(rf, java.lang.Math.min(gf, bf));
        float deltaMaxMin = max - min;
        float l = (max + min) / 2.0f;
        if (max == min) {
            s = 0.0f;
            h = 0.0f;
        } else {
            if (max == rf) {
                h = ((gf - bf) / deltaMaxMin) % 6.0f;
            } else if (max == gf) {
                h = ((bf - rf) / deltaMaxMin) + 2.0f;
            } else {
                h = ((rf - gf) / deltaMaxMin) + 4.0f;
            }
            s = deltaMaxMin / (1.0f - java.lang.Math.abs((2.0f * l) - 1.0f));
        }
        float h2 = (60.0f * h) % 360.0f;
        if (h2 < 0.0f) {
            h2 += 360.0f;
        }
        outHsl[0] = constrain(h2, 0.0f, 360.0f);
        outHsl[1] = constrain(s, 0.0f, 1.0f);
        outHsl[2] = constrain(l, 0.0f, 1.0f);
    }

    public static void colorToHSL(@android.support.annotation.ColorInt int color, @android.support.annotation.NonNull float[] outHsl) {
        RGBToHSL(android.graphics.Color.red(color), android.graphics.Color.green(color), android.graphics.Color.blue(color), outHsl);
    }

    @android.support.annotation.ColorInt
    public static int HSLToColor(@android.support.annotation.NonNull float[] hsl) {
        float h = hsl[0];
        float s = hsl[1];
        float l = hsl[2];
        float c = (1.0f - java.lang.Math.abs((2.0f * l) - 1.0f)) * s;
        float m = l - (0.5f * c);
        float x = c * (1.0f - java.lang.Math.abs(((h / 60.0f) % 2.0f) - 1.0f));
        int r = 0;
        int g = 0;
        int b = 0;
        switch (((int) h) / 60) {
            case 0:
                r = java.lang.Math.round(255.0f * (c + m));
                g = java.lang.Math.round(255.0f * (x + m));
                b = java.lang.Math.round(255.0f * m);
                break;
            case 1:
                r = java.lang.Math.round(255.0f * (x + m));
                g = java.lang.Math.round(255.0f * (c + m));
                b = java.lang.Math.round(255.0f * m);
                break;
            case 2:
                r = java.lang.Math.round(255.0f * m);
                g = java.lang.Math.round(255.0f * (c + m));
                b = java.lang.Math.round(255.0f * (x + m));
                break;
            case 3:
                r = java.lang.Math.round(255.0f * m);
                g = java.lang.Math.round(255.0f * (x + m));
                b = java.lang.Math.round(255.0f * (c + m));
                break;
            case 4:
                r = java.lang.Math.round(255.0f * (x + m));
                g = java.lang.Math.round(255.0f * m);
                b = java.lang.Math.round(255.0f * (c + m));
                break;
            case 5:
            case 6:
                r = java.lang.Math.round(255.0f * (c + m));
                g = java.lang.Math.round(255.0f * m);
                b = java.lang.Math.round(255.0f * (x + m));
                break;
        }
        return android.graphics.Color.rgb(constrain(r, 0, 255), constrain(g, 0, 255), constrain(b, 0, 255));
    }

    @android.support.annotation.ColorInt
    public static int setAlphaComponent(@android.support.annotation.ColorInt int color, @android.support.annotation.IntRange(from = 0, to = 255) int alpha) {
        if (alpha >= 0 && alpha <= 255) {
            return (16777215 & color) | (alpha << 24);
        }
        throw new java.lang.IllegalArgumentException("alpha must be between 0 and 255.");
    }

    public static void colorToLAB(@android.support.annotation.ColorInt int color, @android.support.annotation.NonNull double[] outLab) {
        RGBToLAB(android.graphics.Color.red(color), android.graphics.Color.green(color), android.graphics.Color.blue(color), outLab);
    }

    public static void RGBToLAB(@android.support.annotation.IntRange(from = 0, to = 255) int r, @android.support.annotation.IntRange(from = 0, to = 255) int g, @android.support.annotation.IntRange(from = 0, to = 255) int b, @android.support.annotation.NonNull double[] outLab) {
        RGBToXYZ(r, g, b, outLab);
        XYZToLAB(outLab[0], outLab[1], outLab[2], outLab);
    }

    public static void colorToXYZ(@android.support.annotation.ColorInt int color, @android.support.annotation.NonNull double[] outXyz) {
        RGBToXYZ(android.graphics.Color.red(color), android.graphics.Color.green(color), android.graphics.Color.blue(color), outXyz);
    }

    public static void RGBToXYZ(@android.support.annotation.IntRange(from = 0, to = 255) int r, @android.support.annotation.IntRange(from = 0, to = 255) int g, @android.support.annotation.IntRange(from = 0, to = 255) int b, @android.support.annotation.NonNull double[] outXyz) {
        if (outXyz.length != 3) {
            throw new java.lang.IllegalArgumentException("outXyz must have a length of 3.");
        }
        double sr = ((double) r) / 255.0d;
        double sr2 = sr < 0.04045d ? sr / 12.92d : java.lang.Math.pow((0.055d + sr) / 1.055d, 2.4d);
        double sg = ((double) g) / 255.0d;
        double sg2 = sg < 0.04045d ? sg / 12.92d : java.lang.Math.pow((0.055d + sg) / 1.055d, 2.4d);
        double sb = ((double) b) / 255.0d;
        double sb2 = sb < 0.04045d ? sb / 12.92d : java.lang.Math.pow((0.055d + sb) / 1.055d, 2.4d);
        outXyz[0] = 100.0d * ((0.4124d * sr2) + (0.3576d * sg2) + (0.1805d * sb2));
        outXyz[1] = 100.0d * ((0.2126d * sr2) + (0.7152d * sg2) + (0.0722d * sb2));
        outXyz[2] = 100.0d * ((0.0193d * sr2) + (0.1192d * sg2) + (0.9505d * sb2));
    }

    public static void XYZToLAB(@android.support.annotation.FloatRange(from = 0.0d, to = 95.047d) double x, @android.support.annotation.FloatRange(from = 0.0d, to = 100.0d) double y, @android.support.annotation.FloatRange(from = 0.0d, to = 108.883d) double z, @android.support.annotation.NonNull double[] outLab) {
        if (outLab.length != 3) {
            throw new java.lang.IllegalArgumentException("outLab must have a length of 3.");
        }
        double x2 = pivotXyzComponent(x / XYZ_WHITE_REFERENCE_X);
        double y2 = pivotXyzComponent(y / 100.0d);
        double z2 = pivotXyzComponent(z / XYZ_WHITE_REFERENCE_Z);
        outLab[0] = java.lang.Math.max(0.0d, (116.0d * y2) - 16.0d);
        outLab[1] = 500.0d * (x2 - y2);
        outLab[2] = 200.0d * (y2 - z2);
    }

    public static void LABToXYZ(@android.support.annotation.FloatRange(from = 0.0d, to = 100.0d) double l, @android.support.annotation.FloatRange(from = -128.0d, to = 127.0d) double a, @android.support.annotation.FloatRange(from = -128.0d, to = 127.0d) double b, @android.support.annotation.NonNull double[] outXyz) {
        double fy = (16.0d + l) / 116.0d;
        double fx = (a / 500.0d) + fy;
        double fz = fy - (b / 200.0d);
        double tmp = java.lang.Math.pow(fx, 3.0d);
        double xr = tmp > XYZ_EPSILON ? tmp : ((116.0d * fx) - 16.0d) / XYZ_KAPPA;
        double yr = l > 7.9996247999999985d ? java.lang.Math.pow(fy, 3.0d) : l / XYZ_KAPPA;
        double tmp2 = java.lang.Math.pow(fz, 3.0d);
        double zr = tmp2 > XYZ_EPSILON ? tmp2 : ((116.0d * fz) - 16.0d) / XYZ_KAPPA;
        outXyz[0] = XYZ_WHITE_REFERENCE_X * xr;
        outXyz[1] = 100.0d * yr;
        outXyz[2] = XYZ_WHITE_REFERENCE_Z * zr;
    }

    @android.support.annotation.ColorInt
    public static int XYZToColor(@android.support.annotation.FloatRange(from = 0.0d, to = 95.047d) double x, @android.support.annotation.FloatRange(from = 0.0d, to = 100.0d) double y, @android.support.annotation.FloatRange(from = 0.0d, to = 108.883d) double z) {
        double r = (((3.2406d * x) + (-1.5372d * y)) + (-0.4986d * z)) / 100.0d;
        double g = (((-0.9689d * x) + (1.8758d * y)) + (0.0415d * z)) / 100.0d;
        double b = (((0.0557d * x) + (-0.204d * y)) + (1.057d * z)) / 100.0d;
        return android.graphics.Color.rgb(constrain((int) java.lang.Math.round(255.0d * (r > 0.0031308d ? (1.055d * java.lang.Math.pow(r, 0.4166666666666667d)) - 0.055d : r * 12.92d)), 0, 255), constrain((int) java.lang.Math.round(255.0d * (g > 0.0031308d ? (1.055d * java.lang.Math.pow(g, 0.4166666666666667d)) - 0.055d : g * 12.92d)), 0, 255), constrain((int) java.lang.Math.round(255.0d * (b > 0.0031308d ? (1.055d * java.lang.Math.pow(b, 0.4166666666666667d)) - 0.055d : b * 12.92d)), 0, 255));
    }

    @android.support.annotation.ColorInt
    public static int LABToColor(@android.support.annotation.FloatRange(from = 0.0d, to = 100.0d) double l, @android.support.annotation.FloatRange(from = -128.0d, to = 127.0d) double a, @android.support.annotation.FloatRange(from = -128.0d, to = 127.0d) double b) {
        double[] result = getTempDouble3Array();
        LABToXYZ(l, a, b, result);
        return XYZToColor(result[0], result[1], result[2]);
    }

    public static double distanceEuclidean(@android.support.annotation.NonNull double[] labX, @android.support.annotation.NonNull double[] labY) {
        return java.lang.Math.sqrt(java.lang.Math.pow(labX[0] - labY[0], 2.0d) + java.lang.Math.pow(labX[1] - labY[1], 2.0d) + java.lang.Math.pow(labX[2] - labY[2], 2.0d));
    }

    private static float constrain(float amount, float low, float high) {
        if (amount < low) {
            return low;
        }
        return amount > high ? high : amount;
    }

    private static int constrain(int amount, int low, int high) {
        if (amount < low) {
            return low;
        }
        return amount > high ? high : amount;
    }

    private static double pivotXyzComponent(double component) {
        return component > XYZ_EPSILON ? java.lang.Math.pow(component, 0.3333333333333333d) : ((XYZ_KAPPA * component) + 16.0d) / 116.0d;
    }

    @android.support.annotation.ColorInt
    public static int blendARGB(@android.support.annotation.ColorInt int color1, @android.support.annotation.ColorInt int color2, @android.support.annotation.FloatRange(from = 0.0d, to = 1.0d) float ratio) {
        float inverseRatio = 1.0f - ratio;
        return android.graphics.Color.argb((int) ((((float) android.graphics.Color.alpha(color1)) * inverseRatio) + (((float) android.graphics.Color.alpha(color2)) * ratio)), (int) ((((float) android.graphics.Color.red(color1)) * inverseRatio) + (((float) android.graphics.Color.red(color2)) * ratio)), (int) ((((float) android.graphics.Color.green(color1)) * inverseRatio) + (((float) android.graphics.Color.green(color2)) * ratio)), (int) ((((float) android.graphics.Color.blue(color1)) * inverseRatio) + (((float) android.graphics.Color.blue(color2)) * ratio)));
    }

    public static void blendHSL(@android.support.annotation.NonNull float[] hsl1, @android.support.annotation.NonNull float[] hsl2, @android.support.annotation.FloatRange(from = 0.0d, to = 1.0d) float ratio, @android.support.annotation.NonNull float[] outResult) {
        if (outResult.length != 3) {
            throw new java.lang.IllegalArgumentException("result must have a length of 3.");
        }
        float inverseRatio = 1.0f - ratio;
        outResult[0] = circularInterpolate(hsl1[0], hsl2[0], ratio);
        outResult[1] = (hsl1[1] * inverseRatio) + (hsl2[1] * ratio);
        outResult[2] = (hsl1[2] * inverseRatio) + (hsl2[2] * ratio);
    }

    public static void blendLAB(@android.support.annotation.NonNull double[] lab1, @android.support.annotation.NonNull double[] lab2, @android.support.annotation.FloatRange(from = 0.0d, to = 1.0d) double ratio, @android.support.annotation.NonNull double[] outResult) {
        if (outResult.length != 3) {
            throw new java.lang.IllegalArgumentException("outResult must have a length of 3.");
        }
        double inverseRatio = 1.0d - ratio;
        outResult[0] = (lab1[0] * inverseRatio) + (lab2[0] * ratio);
        outResult[1] = (lab1[1] * inverseRatio) + (lab2[1] * ratio);
        outResult[2] = (lab1[2] * inverseRatio) + (lab2[2] * ratio);
    }

    @android.support.annotation.VisibleForTesting
    static float circularInterpolate(float a, float b, float f) {
        if (java.lang.Math.abs(b - a) > 180.0f) {
            if (b > a) {
                a += 360.0f;
            } else {
                b += 360.0f;
            }
        }
        return (((b - a) * f) + a) % 360.0f;
    }

    private static double[] getTempDouble3Array() {
        double[] result = (double[]) TEMP_ARRAY.get();
        if (result != null) {
            return result;
        }
        double[] result2 = new double[3];
        TEMP_ARRAY.set(result2);
        return result2;
    }
}
