package android.support.v4.graphics;

@android.annotation.TargetApi(12)
@android.support.annotation.RequiresApi(12)
class BitmapCompatHoneycombMr1 {
    BitmapCompatHoneycombMr1() {
    }

    static int getAllocationByteCount(android.graphics.Bitmap bitmap) {
        return bitmap.getByteCount();
    }
}
