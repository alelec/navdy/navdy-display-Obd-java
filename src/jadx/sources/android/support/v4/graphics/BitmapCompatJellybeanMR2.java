package android.support.v4.graphics;

@android.annotation.TargetApi(18)
@android.support.annotation.RequiresApi(18)
class BitmapCompatJellybeanMR2 {
    BitmapCompatJellybeanMR2() {
    }

    public static boolean hasMipMap(android.graphics.Bitmap bitmap) {
        return bitmap.hasMipMap();
    }

    public static void setHasMipMap(android.graphics.Bitmap bitmap, boolean hasMipMap) {
        bitmap.setHasMipMap(hasMipMap);
    }
}
