package android.support.v4.graphics.drawable;

@android.annotation.TargetApi(23)
@android.support.annotation.RequiresApi(23)
class DrawableCompatApi23 {
    DrawableCompatApi23() {
    }

    public static boolean setLayoutDirection(android.graphics.drawable.Drawable drawable, int layoutDirection) {
        return drawable.setLayoutDirection(layoutDirection);
    }

    public static int getLayoutDirection(android.graphics.drawable.Drawable drawable) {
        return drawable.getLayoutDirection();
    }
}
