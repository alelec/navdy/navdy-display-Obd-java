package android.support.v4.graphics.drawable;

@android.annotation.TargetApi(21)
@android.support.annotation.RequiresApi(21)
class DrawableCompatLollipop {
    DrawableCompatLollipop() {
    }

    public static void setHotspot(android.graphics.drawable.Drawable drawable, float x, float y) {
        drawable.setHotspot(x, y);
    }

    public static void setHotspotBounds(android.graphics.drawable.Drawable drawable, int left, int top, int right, int bottom) {
        drawable.setHotspotBounds(left, top, right, bottom);
    }

    public static void setTint(android.graphics.drawable.Drawable drawable, int tint) {
        drawable.setTint(tint);
    }

    public static void setTintList(android.graphics.drawable.Drawable drawable, android.content.res.ColorStateList tint) {
        drawable.setTintList(tint);
    }

    public static void setTintMode(android.graphics.drawable.Drawable drawable, android.graphics.PorterDuff.Mode tintMode) {
        drawable.setTintMode(tintMode);
    }

    public static android.graphics.drawable.Drawable wrapForTinting(android.graphics.drawable.Drawable drawable) {
        if (!(drawable instanceof android.support.v4.graphics.drawable.TintAwareDrawable)) {
            return new android.support.v4.graphics.drawable.DrawableWrapperLollipop(drawable);
        }
        return drawable;
    }

    public static void applyTheme(android.graphics.drawable.Drawable drawable, android.content.res.Resources.Theme t) {
        drawable.applyTheme(t);
    }

    public static boolean canApplyTheme(android.graphics.drawable.Drawable drawable) {
        return drawable.canApplyTheme();
    }

    public static android.graphics.ColorFilter getColorFilter(android.graphics.drawable.Drawable drawable) {
        return drawable.getColorFilter();
    }

    public static void clearColorFilter(android.graphics.drawable.Drawable drawable) {
        drawable.clearColorFilter();
        if (drawable instanceof android.graphics.drawable.InsetDrawable) {
            clearColorFilter(((android.graphics.drawable.InsetDrawable) drawable).getDrawable());
        } else if (drawable instanceof android.support.v4.graphics.drawable.DrawableWrapper) {
            clearColorFilter(((android.support.v4.graphics.drawable.DrawableWrapper) drawable).getWrappedDrawable());
        } else if (drawable instanceof android.graphics.drawable.DrawableContainer) {
            android.graphics.drawable.DrawableContainer.DrawableContainerState state = (android.graphics.drawable.DrawableContainer.DrawableContainerState) ((android.graphics.drawable.DrawableContainer) drawable).getConstantState();
            if (state != null) {
                int count = state.getChildCount();
                for (int i = 0; i < count; i++) {
                    android.graphics.drawable.Drawable child = state.getChild(i);
                    if (child != null) {
                        clearColorFilter(child);
                    }
                }
            }
        }
    }

    public static void inflate(android.graphics.drawable.Drawable drawable, android.content.res.Resources res, org.xmlpull.v1.XmlPullParser parser, android.util.AttributeSet attrs, android.content.res.Resources.Theme t) throws java.io.IOException, org.xmlpull.v1.XmlPullParserException {
        drawable.inflate(res, parser, attrs, t);
    }
}
