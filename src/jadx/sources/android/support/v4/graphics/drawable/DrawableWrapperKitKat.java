package android.support.v4.graphics.drawable;

@android.annotation.TargetApi(19)
@android.support.annotation.RequiresApi(19)
class DrawableWrapperKitKat extends android.support.v4.graphics.drawable.DrawableWrapperHoneycomb {

    private static class DrawableWrapperStateKitKat extends android.support.v4.graphics.drawable.DrawableWrapperGingerbread.DrawableWrapperState {
        DrawableWrapperStateKitKat(@android.support.annotation.Nullable android.support.v4.graphics.drawable.DrawableWrapperGingerbread.DrawableWrapperState orig, @android.support.annotation.Nullable android.content.res.Resources res) {
            super(orig, res);
        }

        public android.graphics.drawable.Drawable newDrawable(@android.support.annotation.Nullable android.content.res.Resources res) {
            return new android.support.v4.graphics.drawable.DrawableWrapperKitKat(this, res);
        }
    }

    DrawableWrapperKitKat(android.graphics.drawable.Drawable drawable) {
        super(drawable);
    }

    DrawableWrapperKitKat(android.support.v4.graphics.drawable.DrawableWrapperGingerbread.DrawableWrapperState state, android.content.res.Resources resources) {
        super(state, resources);
    }

    public void setAutoMirrored(boolean mirrored) {
        this.mDrawable.setAutoMirrored(mirrored);
    }

    public boolean isAutoMirrored() {
        return this.mDrawable.isAutoMirrored();
    }

    /* access modifiers changed from: 0000 */
    @android.support.annotation.NonNull
    public android.support.v4.graphics.drawable.DrawableWrapperGingerbread.DrawableWrapperState mutateConstantState() {
        return new android.support.v4.graphics.drawable.DrawableWrapperKitKat.DrawableWrapperStateKitKat(this.mState, null);
    }
}
