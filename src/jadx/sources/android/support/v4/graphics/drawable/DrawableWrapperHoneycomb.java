package android.support.v4.graphics.drawable;

@android.annotation.TargetApi(11)
@android.support.annotation.RequiresApi(11)
class DrawableWrapperHoneycomb extends android.support.v4.graphics.drawable.DrawableWrapperGingerbread {

    private static class DrawableWrapperStateHoneycomb extends android.support.v4.graphics.drawable.DrawableWrapperGingerbread.DrawableWrapperState {
        DrawableWrapperStateHoneycomb(@android.support.annotation.Nullable android.support.v4.graphics.drawable.DrawableWrapperGingerbread.DrawableWrapperState orig, @android.support.annotation.Nullable android.content.res.Resources res) {
            super(orig, res);
        }

        public android.graphics.drawable.Drawable newDrawable(@android.support.annotation.Nullable android.content.res.Resources res) {
            return new android.support.v4.graphics.drawable.DrawableWrapperHoneycomb(this, res);
        }
    }

    DrawableWrapperHoneycomb(android.graphics.drawable.Drawable drawable) {
        super(drawable);
    }

    DrawableWrapperHoneycomb(android.support.v4.graphics.drawable.DrawableWrapperGingerbread.DrawableWrapperState state, android.content.res.Resources resources) {
        super(state, resources);
    }

    public void jumpToCurrentState() {
        this.mDrawable.jumpToCurrentState();
    }

    /* access modifiers changed from: 0000 */
    @android.support.annotation.NonNull
    public android.support.v4.graphics.drawable.DrawableWrapperGingerbread.DrawableWrapperState mutateConstantState() {
        return new android.support.v4.graphics.drawable.DrawableWrapperHoneycomb.DrawableWrapperStateHoneycomb(this.mState, null);
    }
}
