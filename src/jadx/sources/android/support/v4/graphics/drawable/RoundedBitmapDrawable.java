package android.support.v4.graphics.drawable;

@android.annotation.TargetApi(9)
@android.support.annotation.RequiresApi(9)
public abstract class RoundedBitmapDrawable extends android.graphics.drawable.Drawable {
    private static final int DEFAULT_PAINT_FLAGS = 3;
    private boolean mApplyGravity = true;
    final android.graphics.Bitmap mBitmap;
    private int mBitmapHeight;
    private final android.graphics.BitmapShader mBitmapShader;
    private int mBitmapWidth;
    private float mCornerRadius;
    final android.graphics.Rect mDstRect = new android.graphics.Rect();
    private final android.graphics.RectF mDstRectF = new android.graphics.RectF();
    private int mGravity = 119;
    private boolean mIsCircular;
    private final android.graphics.Paint mPaint = new android.graphics.Paint(3);
    private final android.graphics.Matrix mShaderMatrix = new android.graphics.Matrix();
    private int mTargetDensity = ch.qos.logback.core.net.SyslogConstants.LOG_LOCAL4;

    public final android.graphics.Paint getPaint() {
        return this.mPaint;
    }

    public final android.graphics.Bitmap getBitmap() {
        return this.mBitmap;
    }

    private void computeBitmapSize() {
        this.mBitmapWidth = this.mBitmap.getScaledWidth(this.mTargetDensity);
        this.mBitmapHeight = this.mBitmap.getScaledHeight(this.mTargetDensity);
    }

    public void setTargetDensity(android.graphics.Canvas canvas) {
        setTargetDensity(canvas.getDensity());
    }

    public void setTargetDensity(android.util.DisplayMetrics metrics) {
        setTargetDensity(metrics.densityDpi);
    }

    public void setTargetDensity(int density) {
        if (this.mTargetDensity != density) {
            if (density == 0) {
                density = ch.qos.logback.core.net.SyslogConstants.LOG_LOCAL4;
            }
            this.mTargetDensity = density;
            if (this.mBitmap != null) {
                computeBitmapSize();
            }
            invalidateSelf();
        }
    }

    public int getGravity() {
        return this.mGravity;
    }

    public void setGravity(int gravity) {
        if (this.mGravity != gravity) {
            this.mGravity = gravity;
            this.mApplyGravity = true;
            invalidateSelf();
        }
    }

    public void setMipMap(boolean mipMap) {
        throw new java.lang.UnsupportedOperationException();
    }

    public boolean hasMipMap() {
        throw new java.lang.UnsupportedOperationException();
    }

    public void setAntiAlias(boolean aa) {
        this.mPaint.setAntiAlias(aa);
        invalidateSelf();
    }

    public boolean hasAntiAlias() {
        return this.mPaint.isAntiAlias();
    }

    public void setFilterBitmap(boolean filter) {
        this.mPaint.setFilterBitmap(filter);
        invalidateSelf();
    }

    public void setDither(boolean dither) {
        this.mPaint.setDither(dither);
        invalidateSelf();
    }

    /* access modifiers changed from: 0000 */
    public void gravityCompatApply(int gravity, int bitmapWidth, int bitmapHeight, android.graphics.Rect bounds, android.graphics.Rect outRect) {
        throw new java.lang.UnsupportedOperationException();
    }

    /* access modifiers changed from: 0000 */
    public void updateDstRect() {
        if (this.mApplyGravity) {
            if (this.mIsCircular) {
                int minDimen = java.lang.Math.min(this.mBitmapWidth, this.mBitmapHeight);
                gravityCompatApply(this.mGravity, minDimen, minDimen, getBounds(), this.mDstRect);
                int minDrawDimen = java.lang.Math.min(this.mDstRect.width(), this.mDstRect.height());
                this.mDstRect.inset(java.lang.Math.max(0, (this.mDstRect.width() - minDrawDimen) / 2), java.lang.Math.max(0, (this.mDstRect.height() - minDrawDimen) / 2));
                this.mCornerRadius = 0.5f * ((float) minDrawDimen);
            } else {
                gravityCompatApply(this.mGravity, this.mBitmapWidth, this.mBitmapHeight, getBounds(), this.mDstRect);
            }
            this.mDstRectF.set(this.mDstRect);
            if (this.mBitmapShader != null) {
                this.mShaderMatrix.setTranslate(this.mDstRectF.left, this.mDstRectF.top);
                this.mShaderMatrix.preScale(this.mDstRectF.width() / ((float) this.mBitmap.getWidth()), this.mDstRectF.height() / ((float) this.mBitmap.getHeight()));
                this.mBitmapShader.setLocalMatrix(this.mShaderMatrix);
                this.mPaint.setShader(this.mBitmapShader);
            }
            this.mApplyGravity = false;
        }
    }

    public void draw(android.graphics.Canvas canvas) {
        android.graphics.Bitmap bitmap = this.mBitmap;
        if (bitmap != null) {
            updateDstRect();
            if (this.mPaint.getShader() == null) {
                canvas.drawBitmap(bitmap, null, this.mDstRect, this.mPaint);
            } else {
                canvas.drawRoundRect(this.mDstRectF, this.mCornerRadius, this.mCornerRadius, this.mPaint);
            }
        }
    }

    public void setAlpha(int alpha) {
        if (alpha != this.mPaint.getAlpha()) {
            this.mPaint.setAlpha(alpha);
            invalidateSelf();
        }
    }

    public int getAlpha() {
        return this.mPaint.getAlpha();
    }

    public void setColorFilter(android.graphics.ColorFilter cf) {
        this.mPaint.setColorFilter(cf);
        invalidateSelf();
    }

    public android.graphics.ColorFilter getColorFilter() {
        return this.mPaint.getColorFilter();
    }

    public void setCircular(boolean circular) {
        this.mIsCircular = circular;
        this.mApplyGravity = true;
        if (circular) {
            updateCircularCornerRadius();
            this.mPaint.setShader(this.mBitmapShader);
            invalidateSelf();
            return;
        }
        setCornerRadius(0.0f);
    }

    private void updateCircularCornerRadius() {
        this.mCornerRadius = (float) (java.lang.Math.min(this.mBitmapHeight, this.mBitmapWidth) / 2);
    }

    public boolean isCircular() {
        return this.mIsCircular;
    }

    public void setCornerRadius(float cornerRadius) {
        if (this.mCornerRadius != cornerRadius) {
            this.mIsCircular = false;
            if (isGreaterThanZero(cornerRadius)) {
                this.mPaint.setShader(this.mBitmapShader);
            } else {
                this.mPaint.setShader(null);
            }
            this.mCornerRadius = cornerRadius;
            invalidateSelf();
        }
    }

    /* access modifiers changed from: protected */
    public void onBoundsChange(android.graphics.Rect bounds) {
        super.onBoundsChange(bounds);
        if (this.mIsCircular) {
            updateCircularCornerRadius();
        }
        this.mApplyGravity = true;
    }

    public float getCornerRadius() {
        return this.mCornerRadius;
    }

    public int getIntrinsicWidth() {
        return this.mBitmapWidth;
    }

    public int getIntrinsicHeight() {
        return this.mBitmapHeight;
    }

    public int getOpacity() {
        if (this.mGravity != 119 || this.mIsCircular) {
            return -3;
        }
        android.graphics.Bitmap bm = this.mBitmap;
        if (bm == null || bm.hasAlpha() || this.mPaint.getAlpha() < 255 || isGreaterThanZero(this.mCornerRadius)) {
            return -3;
        }
        return -1;
    }

    RoundedBitmapDrawable(android.content.res.Resources res, android.graphics.Bitmap bitmap) {
        if (res != null) {
            this.mTargetDensity = res.getDisplayMetrics().densityDpi;
        }
        this.mBitmap = bitmap;
        if (this.mBitmap != null) {
            computeBitmapSize();
            this.mBitmapShader = new android.graphics.BitmapShader(this.mBitmap, android.graphics.Shader.TileMode.CLAMP, android.graphics.Shader.TileMode.CLAMP);
            return;
        }
        this.mBitmapHeight = -1;
        this.mBitmapWidth = -1;
        this.mBitmapShader = null;
    }

    private static boolean isGreaterThanZero(float toCompare) {
        return toCompare > 0.05f;
    }
}
