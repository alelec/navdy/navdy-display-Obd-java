package android.support.v4.graphics.drawable;

@android.annotation.TargetApi(9)
@android.support.annotation.RequiresApi(9)
class DrawableWrapperGingerbread extends android.graphics.drawable.Drawable implements android.graphics.drawable.Drawable.Callback, android.support.v4.graphics.drawable.DrawableWrapper, android.support.v4.graphics.drawable.TintAwareDrawable {
    static final android.graphics.PorterDuff.Mode DEFAULT_TINT_MODE = android.graphics.PorterDuff.Mode.SRC_IN;
    private boolean mColorFilterSet;
    private int mCurrentColor;
    private android.graphics.PorterDuff.Mode mCurrentMode;
    android.graphics.drawable.Drawable mDrawable;
    private boolean mMutated;
    android.support.v4.graphics.drawable.DrawableWrapperGingerbread.DrawableWrapperState mState;

    protected static abstract class DrawableWrapperState extends android.graphics.drawable.Drawable.ConstantState {
        int mChangingConfigurations;
        android.graphics.drawable.Drawable.ConstantState mDrawableState;
        android.content.res.ColorStateList mTint = null;
        android.graphics.PorterDuff.Mode mTintMode = android.support.v4.graphics.drawable.DrawableWrapperGingerbread.DEFAULT_TINT_MODE;

        public abstract android.graphics.drawable.Drawable newDrawable(@android.support.annotation.Nullable android.content.res.Resources resources);

        DrawableWrapperState(@android.support.annotation.Nullable android.support.v4.graphics.drawable.DrawableWrapperGingerbread.DrawableWrapperState orig, @android.support.annotation.Nullable android.content.res.Resources res) {
            if (orig != null) {
                this.mChangingConfigurations = orig.mChangingConfigurations;
                this.mDrawableState = orig.mDrawableState;
                this.mTint = orig.mTint;
                this.mTintMode = orig.mTintMode;
            }
        }

        public android.graphics.drawable.Drawable newDrawable() {
            return newDrawable(null);
        }

        public int getChangingConfigurations() {
            return (this.mDrawableState != null ? this.mDrawableState.getChangingConfigurations() : 0) | this.mChangingConfigurations;
        }

        /* access modifiers changed from: 0000 */
        public boolean canConstantState() {
            return this.mDrawableState != null;
        }
    }

    private static class DrawableWrapperStateBase extends android.support.v4.graphics.drawable.DrawableWrapperGingerbread.DrawableWrapperState {
        DrawableWrapperStateBase(@android.support.annotation.Nullable android.support.v4.graphics.drawable.DrawableWrapperGingerbread.DrawableWrapperState orig, @android.support.annotation.Nullable android.content.res.Resources res) {
            super(orig, res);
        }

        public android.graphics.drawable.Drawable newDrawable(@android.support.annotation.Nullable android.content.res.Resources res) {
            return new android.support.v4.graphics.drawable.DrawableWrapperGingerbread(this, res);
        }
    }

    DrawableWrapperGingerbread(@android.support.annotation.NonNull android.support.v4.graphics.drawable.DrawableWrapperGingerbread.DrawableWrapperState state, @android.support.annotation.Nullable android.content.res.Resources res) {
        this.mState = state;
        updateLocalState(res);
    }

    DrawableWrapperGingerbread(@android.support.annotation.Nullable android.graphics.drawable.Drawable dr) {
        this.mState = mutateConstantState();
        setWrappedDrawable(dr);
    }

    private void updateLocalState(@android.support.annotation.Nullable android.content.res.Resources res) {
        if (this.mState != null && this.mState.mDrawableState != null) {
            setWrappedDrawable(newDrawableFromState(this.mState.mDrawableState, res));
        }
    }

    /* access modifiers changed from: protected */
    public android.graphics.drawable.Drawable newDrawableFromState(@android.support.annotation.NonNull android.graphics.drawable.Drawable.ConstantState state, @android.support.annotation.Nullable android.content.res.Resources res) {
        return state.newDrawable(res);
    }

    public void draw(android.graphics.Canvas canvas) {
        this.mDrawable.draw(canvas);
    }

    /* access modifiers changed from: protected */
    public void onBoundsChange(android.graphics.Rect bounds) {
        if (this.mDrawable != null) {
            this.mDrawable.setBounds(bounds);
        }
    }

    public void setChangingConfigurations(int configs) {
        this.mDrawable.setChangingConfigurations(configs);
    }

    public int getChangingConfigurations() {
        return (this.mState != null ? this.mState.getChangingConfigurations() : 0) | super.getChangingConfigurations() | this.mDrawable.getChangingConfigurations();
    }

    public void setDither(boolean dither) {
        this.mDrawable.setDither(dither);
    }

    public void setFilterBitmap(boolean filter) {
        this.mDrawable.setFilterBitmap(filter);
    }

    public void setAlpha(int alpha) {
        this.mDrawable.setAlpha(alpha);
    }

    public void setColorFilter(android.graphics.ColorFilter cf) {
        this.mDrawable.setColorFilter(cf);
    }

    public boolean isStateful() {
        android.content.res.ColorStateList tintList = (!isCompatTintEnabled() || this.mState == null) ? null : this.mState.mTint;
        return (tintList != null && tintList.isStateful()) || this.mDrawable.isStateful();
    }

    public boolean setState(int[] stateSet) {
        return updateTint(stateSet) || this.mDrawable.setState(stateSet);
    }

    public int[] getState() {
        return this.mDrawable.getState();
    }

    public android.graphics.drawable.Drawable getCurrent() {
        return this.mDrawable.getCurrent();
    }

    public boolean setVisible(boolean visible, boolean restart) {
        return super.setVisible(visible, restart) || this.mDrawable.setVisible(visible, restart);
    }

    public int getOpacity() {
        return this.mDrawable.getOpacity();
    }

    public android.graphics.Region getTransparentRegion() {
        return this.mDrawable.getTransparentRegion();
    }

    public int getIntrinsicWidth() {
        return this.mDrawable.getIntrinsicWidth();
    }

    public int getIntrinsicHeight() {
        return this.mDrawable.getIntrinsicHeight();
    }

    public int getMinimumWidth() {
        return this.mDrawable.getMinimumWidth();
    }

    public int getMinimumHeight() {
        return this.mDrawable.getMinimumHeight();
    }

    public boolean getPadding(android.graphics.Rect padding) {
        return this.mDrawable.getPadding(padding);
    }

    @android.support.annotation.Nullable
    public android.graphics.drawable.Drawable.ConstantState getConstantState() {
        if (this.mState == null || !this.mState.canConstantState()) {
            return null;
        }
        this.mState.mChangingConfigurations = getChangingConfigurations();
        return this.mState;
    }

    public android.graphics.drawable.Drawable mutate() {
        if (!this.mMutated && super.mutate() == this) {
            this.mState = mutateConstantState();
            if (this.mDrawable != null) {
                this.mDrawable.mutate();
            }
            if (this.mState != null) {
                this.mState.mDrawableState = this.mDrawable != null ? this.mDrawable.getConstantState() : null;
            }
            this.mMutated = true;
        }
        return this;
    }

    /* access modifiers changed from: 0000 */
    @android.support.annotation.NonNull
    public android.support.v4.graphics.drawable.DrawableWrapperGingerbread.DrawableWrapperState mutateConstantState() {
        return new android.support.v4.graphics.drawable.DrawableWrapperGingerbread.DrawableWrapperStateBase(this.mState, null);
    }

    public void invalidateDrawable(android.graphics.drawable.Drawable who) {
        invalidateSelf();
    }

    public void scheduleDrawable(android.graphics.drawable.Drawable who, java.lang.Runnable what, long when) {
        scheduleSelf(what, when);
    }

    public void unscheduleDrawable(android.graphics.drawable.Drawable who, java.lang.Runnable what) {
        unscheduleSelf(what);
    }

    /* access modifiers changed from: protected */
    public boolean onLevelChange(int level) {
        return this.mDrawable.setLevel(level);
    }

    public void setTint(int tint) {
        setTintList(android.content.res.ColorStateList.valueOf(tint));
    }

    public void setTintList(android.content.res.ColorStateList tint) {
        this.mState.mTint = tint;
        updateTint(getState());
    }

    public void setTintMode(android.graphics.PorterDuff.Mode tintMode) {
        this.mState.mTintMode = tintMode;
        updateTint(getState());
    }

    private boolean updateTint(int[] state) {
        if (!isCompatTintEnabled()) {
            return false;
        }
        android.content.res.ColorStateList tintList = this.mState.mTint;
        android.graphics.PorterDuff.Mode tintMode = this.mState.mTintMode;
        if (tintList == null || tintMode == null) {
            this.mColorFilterSet = false;
            clearColorFilter();
            return false;
        }
        int color = tintList.getColorForState(state, tintList.getDefaultColor());
        if (this.mColorFilterSet && color == this.mCurrentColor && tintMode == this.mCurrentMode) {
            return false;
        }
        setColorFilter(color, tintMode);
        this.mCurrentColor = color;
        this.mCurrentMode = tintMode;
        this.mColorFilterSet = true;
        return true;
    }

    public final android.graphics.drawable.Drawable getWrappedDrawable() {
        return this.mDrawable;
    }

    public final void setWrappedDrawable(android.graphics.drawable.Drawable dr) {
        if (this.mDrawable != null) {
            this.mDrawable.setCallback(null);
        }
        this.mDrawable = dr;
        if (dr != null) {
            dr.setCallback(this);
            setVisible(dr.isVisible(), true);
            setState(dr.getState());
            setLevel(dr.getLevel());
            setBounds(dr.getBounds());
            if (this.mState != null) {
                this.mState.mDrawableState = dr.getConstantState();
            }
        }
        invalidateSelf();
    }

    /* access modifiers changed from: protected */
    public boolean isCompatTintEnabled() {
        return true;
    }
}
