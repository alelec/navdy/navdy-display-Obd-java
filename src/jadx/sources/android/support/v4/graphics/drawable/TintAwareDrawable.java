package android.support.v4.graphics.drawable;

@android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
public interface TintAwareDrawable {
    void setTint(@android.support.annotation.ColorInt int i);

    void setTintList(android.content.res.ColorStateList colorStateList);

    void setTintMode(android.graphics.PorterDuff.Mode mode);
}
