package android.support.v4.graphics.drawable;

@android.support.annotation.RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
public interface DrawableWrapper {
    android.graphics.drawable.Drawable getWrappedDrawable();

    void setWrappedDrawable(android.graphics.drawable.Drawable drawable);
}
