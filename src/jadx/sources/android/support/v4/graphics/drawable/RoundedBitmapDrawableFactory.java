package android.support.v4.graphics.drawable;

public final class RoundedBitmapDrawableFactory {
    private static final java.lang.String TAG = "RoundedBitmapDrawableFactory";

    private static class DefaultRoundedBitmapDrawable extends android.support.v4.graphics.drawable.RoundedBitmapDrawable {
        DefaultRoundedBitmapDrawable(android.content.res.Resources res, android.graphics.Bitmap bitmap) {
            super(res, bitmap);
        }

        public void setMipMap(boolean mipMap) {
            if (this.mBitmap != null) {
                android.support.v4.graphics.BitmapCompat.setHasMipMap(this.mBitmap, mipMap);
                invalidateSelf();
            }
        }

        public boolean hasMipMap() {
            return this.mBitmap != null && android.support.v4.graphics.BitmapCompat.hasMipMap(this.mBitmap);
        }

        /* access modifiers changed from: 0000 */
        public void gravityCompatApply(int gravity, int bitmapWidth, int bitmapHeight, android.graphics.Rect bounds, android.graphics.Rect outRect) {
            android.support.v4.view.GravityCompat.apply(gravity, bitmapWidth, bitmapHeight, bounds, outRect, 0);
        }
    }

    public static android.support.v4.graphics.drawable.RoundedBitmapDrawable create(android.content.res.Resources res, android.graphics.Bitmap bitmap) {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            return new android.support.v4.graphics.drawable.RoundedBitmapDrawable21(res, bitmap);
        }
        return new android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory.DefaultRoundedBitmapDrawable(res, bitmap);
    }

    public static android.support.v4.graphics.drawable.RoundedBitmapDrawable create(android.content.res.Resources res, java.lang.String filepath) {
        android.support.v4.graphics.drawable.RoundedBitmapDrawable drawable = create(res, android.graphics.BitmapFactory.decodeFile(filepath));
        if (drawable.getBitmap() == null) {
            android.util.Log.w(TAG, "RoundedBitmapDrawable cannot decode " + filepath);
        }
        return drawable;
    }

    public static android.support.v4.graphics.drawable.RoundedBitmapDrawable create(android.content.res.Resources res, java.io.InputStream is) {
        android.support.v4.graphics.drawable.RoundedBitmapDrawable drawable = create(res, android.graphics.BitmapFactory.decodeStream(is));
        if (drawable.getBitmap() == null) {
            android.util.Log.w(TAG, "RoundedBitmapDrawable cannot decode " + is);
        }
        return drawable;
    }

    private RoundedBitmapDrawableFactory() {
    }
}
