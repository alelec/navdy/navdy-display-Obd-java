package android.support.v4.graphics.drawable;

@android.annotation.TargetApi(9)
@android.support.annotation.RequiresApi(9)
class DrawableCompatBase {
    DrawableCompatBase() {
    }

    public static void setTint(android.graphics.drawable.Drawable drawable, int tint) {
        if (drawable instanceof android.support.v4.graphics.drawable.TintAwareDrawable) {
            ((android.support.v4.graphics.drawable.TintAwareDrawable) drawable).setTint(tint);
        }
    }

    public static void setTintList(android.graphics.drawable.Drawable drawable, android.content.res.ColorStateList tint) {
        if (drawable instanceof android.support.v4.graphics.drawable.TintAwareDrawable) {
            ((android.support.v4.graphics.drawable.TintAwareDrawable) drawable).setTintList(tint);
        }
    }

    public static void setTintMode(android.graphics.drawable.Drawable drawable, android.graphics.PorterDuff.Mode tintMode) {
        if (drawable instanceof android.support.v4.graphics.drawable.TintAwareDrawable) {
            ((android.support.v4.graphics.drawable.TintAwareDrawable) drawable).setTintMode(tintMode);
        }
    }

    public static android.graphics.drawable.Drawable wrapForTinting(android.graphics.drawable.Drawable drawable) {
        if (!(drawable instanceof android.support.v4.graphics.drawable.TintAwareDrawable)) {
            return new android.support.v4.graphics.drawable.DrawableWrapperGingerbread(drawable);
        }
        return drawable;
    }

    public static void inflate(android.graphics.drawable.Drawable drawable, android.content.res.Resources res, org.xmlpull.v1.XmlPullParser parser, android.util.AttributeSet attrs, android.content.res.Resources.Theme t) throws java.io.IOException, org.xmlpull.v1.XmlPullParserException {
        drawable.inflate(res, parser, attrs);
    }
}
