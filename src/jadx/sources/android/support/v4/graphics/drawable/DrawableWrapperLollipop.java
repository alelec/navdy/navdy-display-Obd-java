package android.support.v4.graphics.drawable;

@android.annotation.TargetApi(21)
@android.support.annotation.RequiresApi(21)
class DrawableWrapperLollipop extends android.support.v4.graphics.drawable.DrawableWrapperKitKat {

    private static class DrawableWrapperStateLollipop extends android.support.v4.graphics.drawable.DrawableWrapperGingerbread.DrawableWrapperState {
        DrawableWrapperStateLollipop(@android.support.annotation.Nullable android.support.v4.graphics.drawable.DrawableWrapperGingerbread.DrawableWrapperState orig, @android.support.annotation.Nullable android.content.res.Resources res) {
            super(orig, res);
        }

        public android.graphics.drawable.Drawable newDrawable(@android.support.annotation.Nullable android.content.res.Resources res) {
            return new android.support.v4.graphics.drawable.DrawableWrapperLollipop(this, res);
        }
    }

    DrawableWrapperLollipop(android.graphics.drawable.Drawable drawable) {
        super(drawable);
    }

    DrawableWrapperLollipop(android.support.v4.graphics.drawable.DrawableWrapperGingerbread.DrawableWrapperState state, android.content.res.Resources resources) {
        super(state, resources);
    }

    public void setHotspot(float x, float y) {
        this.mDrawable.setHotspot(x, y);
    }

    public void setHotspotBounds(int left, int top, int right, int bottom) {
        this.mDrawable.setHotspotBounds(left, top, right, bottom);
    }

    public void getOutline(android.graphics.Outline outline) {
        this.mDrawable.getOutline(outline);
    }

    public android.graphics.Rect getDirtyBounds() {
        return this.mDrawable.getDirtyBounds();
    }

    public void setTintList(android.content.res.ColorStateList tint) {
        if (isCompatTintEnabled()) {
            super.setTintList(tint);
        } else {
            this.mDrawable.setTintList(tint);
        }
    }

    public void setTint(int tintColor) {
        if (isCompatTintEnabled()) {
            super.setTint(tintColor);
        } else {
            this.mDrawable.setTint(tintColor);
        }
    }

    public void setTintMode(android.graphics.PorterDuff.Mode tintMode) {
        if (isCompatTintEnabled()) {
            super.setTintMode(tintMode);
        } else {
            this.mDrawable.setTintMode(tintMode);
        }
    }

    public boolean setState(int[] stateSet) {
        if (!super.setState(stateSet)) {
            return false;
        }
        invalidateSelf();
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean isCompatTintEnabled() {
        if (android.os.Build.VERSION.SDK_INT != 21) {
            return false;
        }
        android.graphics.drawable.Drawable drawable = this.mDrawable;
        if ((drawable instanceof android.graphics.drawable.GradientDrawable) || (drawable instanceof android.graphics.drawable.DrawableContainer) || (drawable instanceof android.graphics.drawable.InsetDrawable)) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: 0000 */
    @android.support.annotation.NonNull
    public android.support.v4.graphics.drawable.DrawableWrapperGingerbread.DrawableWrapperState mutateConstantState() {
        return new android.support.v4.graphics.drawable.DrawableWrapperLollipop.DrawableWrapperStateLollipop(this.mState, null);
    }
}
