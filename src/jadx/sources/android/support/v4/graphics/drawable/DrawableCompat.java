package android.support.v4.graphics.drawable;

public final class DrawableCompat {
    static final android.support.v4.graphics.drawable.DrawableCompat.DrawableImpl IMPL;

    static class BaseDrawableImpl implements android.support.v4.graphics.drawable.DrawableCompat.DrawableImpl {
        BaseDrawableImpl() {
        }

        public void jumpToCurrentState(android.graphics.drawable.Drawable drawable) {
        }

        public void setAutoMirrored(android.graphics.drawable.Drawable drawable, boolean mirrored) {
        }

        public boolean isAutoMirrored(android.graphics.drawable.Drawable drawable) {
            return false;
        }

        public void setHotspot(android.graphics.drawable.Drawable drawable, float x, float y) {
        }

        public void setHotspotBounds(android.graphics.drawable.Drawable drawable, int left, int top, int right, int bottom) {
        }

        public void setTint(android.graphics.drawable.Drawable drawable, int tint) {
            android.support.v4.graphics.drawable.DrawableCompatBase.setTint(drawable, tint);
        }

        public void setTintList(android.graphics.drawable.Drawable drawable, android.content.res.ColorStateList tint) {
            android.support.v4.graphics.drawable.DrawableCompatBase.setTintList(drawable, tint);
        }

        public void setTintMode(android.graphics.drawable.Drawable drawable, android.graphics.PorterDuff.Mode tintMode) {
            android.support.v4.graphics.drawable.DrawableCompatBase.setTintMode(drawable, tintMode);
        }

        public android.graphics.drawable.Drawable wrap(android.graphics.drawable.Drawable drawable) {
            return android.support.v4.graphics.drawable.DrawableCompatBase.wrapForTinting(drawable);
        }

        public boolean setLayoutDirection(android.graphics.drawable.Drawable drawable, int layoutDirection) {
            return false;
        }

        public int getLayoutDirection(android.graphics.drawable.Drawable drawable) {
            return 0;
        }

        public int getAlpha(android.graphics.drawable.Drawable drawable) {
            return 0;
        }

        public void applyTheme(android.graphics.drawable.Drawable drawable, android.content.res.Resources.Theme t) {
        }

        public boolean canApplyTheme(android.graphics.drawable.Drawable drawable) {
            return false;
        }

        public android.graphics.ColorFilter getColorFilter(android.graphics.drawable.Drawable drawable) {
            return null;
        }

        public void clearColorFilter(android.graphics.drawable.Drawable drawable) {
            drawable.clearColorFilter();
        }

        public void inflate(android.graphics.drawable.Drawable drawable, android.content.res.Resources res, org.xmlpull.v1.XmlPullParser parser, android.util.AttributeSet attrs, android.content.res.Resources.Theme t) throws java.io.IOException, org.xmlpull.v1.XmlPullParserException {
            android.support.v4.graphics.drawable.DrawableCompatBase.inflate(drawable, res, parser, attrs, t);
        }
    }

    interface DrawableImpl {
        void applyTheme(android.graphics.drawable.Drawable drawable, android.content.res.Resources.Theme theme);

        boolean canApplyTheme(android.graphics.drawable.Drawable drawable);

        void clearColorFilter(android.graphics.drawable.Drawable drawable);

        int getAlpha(android.graphics.drawable.Drawable drawable);

        android.graphics.ColorFilter getColorFilter(android.graphics.drawable.Drawable drawable);

        int getLayoutDirection(android.graphics.drawable.Drawable drawable);

        void inflate(android.graphics.drawable.Drawable drawable, android.content.res.Resources resources, org.xmlpull.v1.XmlPullParser xmlPullParser, android.util.AttributeSet attributeSet, android.content.res.Resources.Theme theme) throws java.io.IOException, org.xmlpull.v1.XmlPullParserException;

        boolean isAutoMirrored(android.graphics.drawable.Drawable drawable);

        void jumpToCurrentState(android.graphics.drawable.Drawable drawable);

        void setAutoMirrored(android.graphics.drawable.Drawable drawable, boolean z);

        void setHotspot(android.graphics.drawable.Drawable drawable, float f, float f2);

        void setHotspotBounds(android.graphics.drawable.Drawable drawable, int i, int i2, int i3, int i4);

        boolean setLayoutDirection(android.graphics.drawable.Drawable drawable, int i);

        void setTint(android.graphics.drawable.Drawable drawable, int i);

        void setTintList(android.graphics.drawable.Drawable drawable, android.content.res.ColorStateList colorStateList);

        void setTintMode(android.graphics.drawable.Drawable drawable, android.graphics.PorterDuff.Mode mode);

        android.graphics.drawable.Drawable wrap(android.graphics.drawable.Drawable drawable);
    }

    static class HoneycombDrawableImpl extends android.support.v4.graphics.drawable.DrawableCompat.BaseDrawableImpl {
        HoneycombDrawableImpl() {
        }

        public void jumpToCurrentState(android.graphics.drawable.Drawable drawable) {
            android.support.v4.graphics.drawable.DrawableCompatHoneycomb.jumpToCurrentState(drawable);
        }

        public android.graphics.drawable.Drawable wrap(android.graphics.drawable.Drawable drawable) {
            return android.support.v4.graphics.drawable.DrawableCompatHoneycomb.wrapForTinting(drawable);
        }
    }

    static class JellybeanMr1DrawableImpl extends android.support.v4.graphics.drawable.DrawableCompat.HoneycombDrawableImpl {
        JellybeanMr1DrawableImpl() {
        }

        public boolean setLayoutDirection(android.graphics.drawable.Drawable drawable, int layoutDirection) {
            return android.support.v4.graphics.drawable.DrawableCompatJellybeanMr1.setLayoutDirection(drawable, layoutDirection);
        }

        public int getLayoutDirection(android.graphics.drawable.Drawable drawable) {
            int dir = android.support.v4.graphics.drawable.DrawableCompatJellybeanMr1.getLayoutDirection(drawable);
            if (dir >= 0) {
                return dir;
            }
            return 0;
        }
    }

    static class KitKatDrawableImpl extends android.support.v4.graphics.drawable.DrawableCompat.JellybeanMr1DrawableImpl {
        KitKatDrawableImpl() {
        }

        public void setAutoMirrored(android.graphics.drawable.Drawable drawable, boolean mirrored) {
            android.support.v4.graphics.drawable.DrawableCompatKitKat.setAutoMirrored(drawable, mirrored);
        }

        public boolean isAutoMirrored(android.graphics.drawable.Drawable drawable) {
            return android.support.v4.graphics.drawable.DrawableCompatKitKat.isAutoMirrored(drawable);
        }

        public android.graphics.drawable.Drawable wrap(android.graphics.drawable.Drawable drawable) {
            return android.support.v4.graphics.drawable.DrawableCompatKitKat.wrapForTinting(drawable);
        }

        public int getAlpha(android.graphics.drawable.Drawable drawable) {
            return android.support.v4.graphics.drawable.DrawableCompatKitKat.getAlpha(drawable);
        }
    }

    static class LollipopDrawableImpl extends android.support.v4.graphics.drawable.DrawableCompat.KitKatDrawableImpl {
        LollipopDrawableImpl() {
        }

        public void setHotspot(android.graphics.drawable.Drawable drawable, float x, float y) {
            android.support.v4.graphics.drawable.DrawableCompatLollipop.setHotspot(drawable, x, y);
        }

        public void setHotspotBounds(android.graphics.drawable.Drawable drawable, int left, int top, int right, int bottom) {
            android.support.v4.graphics.drawable.DrawableCompatLollipop.setHotspotBounds(drawable, left, top, right, bottom);
        }

        public void setTint(android.graphics.drawable.Drawable drawable, int tint) {
            android.support.v4.graphics.drawable.DrawableCompatLollipop.setTint(drawable, tint);
        }

        public void setTintList(android.graphics.drawable.Drawable drawable, android.content.res.ColorStateList tint) {
            android.support.v4.graphics.drawable.DrawableCompatLollipop.setTintList(drawable, tint);
        }

        public void setTintMode(android.graphics.drawable.Drawable drawable, android.graphics.PorterDuff.Mode tintMode) {
            android.support.v4.graphics.drawable.DrawableCompatLollipop.setTintMode(drawable, tintMode);
        }

        public android.graphics.drawable.Drawable wrap(android.graphics.drawable.Drawable drawable) {
            return android.support.v4.graphics.drawable.DrawableCompatLollipop.wrapForTinting(drawable);
        }

        public void applyTheme(android.graphics.drawable.Drawable drawable, android.content.res.Resources.Theme t) {
            android.support.v4.graphics.drawable.DrawableCompatLollipop.applyTheme(drawable, t);
        }

        public boolean canApplyTheme(android.graphics.drawable.Drawable drawable) {
            return android.support.v4.graphics.drawable.DrawableCompatLollipop.canApplyTheme(drawable);
        }

        public android.graphics.ColorFilter getColorFilter(android.graphics.drawable.Drawable drawable) {
            return android.support.v4.graphics.drawable.DrawableCompatLollipop.getColorFilter(drawable);
        }

        public void clearColorFilter(android.graphics.drawable.Drawable drawable) {
            android.support.v4.graphics.drawable.DrawableCompatLollipop.clearColorFilter(drawable);
        }

        public void inflate(android.graphics.drawable.Drawable drawable, android.content.res.Resources res, org.xmlpull.v1.XmlPullParser parser, android.util.AttributeSet attrs, android.content.res.Resources.Theme t) throws java.io.IOException, org.xmlpull.v1.XmlPullParserException {
            android.support.v4.graphics.drawable.DrawableCompatLollipop.inflate(drawable, res, parser, attrs, t);
        }
    }

    static class MDrawableImpl extends android.support.v4.graphics.drawable.DrawableCompat.LollipopDrawableImpl {
        MDrawableImpl() {
        }

        public boolean setLayoutDirection(android.graphics.drawable.Drawable drawable, int layoutDirection) {
            return android.support.v4.graphics.drawable.DrawableCompatApi23.setLayoutDirection(drawable, layoutDirection);
        }

        public int getLayoutDirection(android.graphics.drawable.Drawable drawable) {
            return android.support.v4.graphics.drawable.DrawableCompatApi23.getLayoutDirection(drawable);
        }

        public android.graphics.drawable.Drawable wrap(android.graphics.drawable.Drawable drawable) {
            return drawable;
        }

        public void clearColorFilter(android.graphics.drawable.Drawable drawable) {
            drawable.clearColorFilter();
        }
    }

    static {
        int version = android.os.Build.VERSION.SDK_INT;
        if (version >= 23) {
            IMPL = new android.support.v4.graphics.drawable.DrawableCompat.MDrawableImpl();
        } else if (version >= 21) {
            IMPL = new android.support.v4.graphics.drawable.DrawableCompat.LollipopDrawableImpl();
        } else if (version >= 19) {
            IMPL = new android.support.v4.graphics.drawable.DrawableCompat.KitKatDrawableImpl();
        } else if (version >= 17) {
            IMPL = new android.support.v4.graphics.drawable.DrawableCompat.JellybeanMr1DrawableImpl();
        } else if (version >= 11) {
            IMPL = new android.support.v4.graphics.drawable.DrawableCompat.HoneycombDrawableImpl();
        } else {
            IMPL = new android.support.v4.graphics.drawable.DrawableCompat.BaseDrawableImpl();
        }
    }

    public static void jumpToCurrentState(@android.support.annotation.NonNull android.graphics.drawable.Drawable drawable) {
        IMPL.jumpToCurrentState(drawable);
    }

    public static void setAutoMirrored(@android.support.annotation.NonNull android.graphics.drawable.Drawable drawable, boolean mirrored) {
        IMPL.setAutoMirrored(drawable, mirrored);
    }

    public static boolean isAutoMirrored(@android.support.annotation.NonNull android.graphics.drawable.Drawable drawable) {
        return IMPL.isAutoMirrored(drawable);
    }

    public static void setHotspot(@android.support.annotation.NonNull android.graphics.drawable.Drawable drawable, float x, float y) {
        IMPL.setHotspot(drawable, x, y);
    }

    public static void setHotspotBounds(@android.support.annotation.NonNull android.graphics.drawable.Drawable drawable, int left, int top, int right, int bottom) {
        IMPL.setHotspotBounds(drawable, left, top, right, bottom);
    }

    public static void setTint(@android.support.annotation.NonNull android.graphics.drawable.Drawable drawable, @android.support.annotation.ColorInt int tint) {
        IMPL.setTint(drawable, tint);
    }

    public static void setTintList(@android.support.annotation.NonNull android.graphics.drawable.Drawable drawable, @android.support.annotation.Nullable android.content.res.ColorStateList tint) {
        IMPL.setTintList(drawable, tint);
    }

    public static void setTintMode(@android.support.annotation.NonNull android.graphics.drawable.Drawable drawable, @android.support.annotation.Nullable android.graphics.PorterDuff.Mode tintMode) {
        IMPL.setTintMode(drawable, tintMode);
    }

    public static int getAlpha(@android.support.annotation.NonNull android.graphics.drawable.Drawable drawable) {
        return IMPL.getAlpha(drawable);
    }

    public static void applyTheme(@android.support.annotation.NonNull android.graphics.drawable.Drawable drawable, @android.support.annotation.NonNull android.content.res.Resources.Theme t) {
        IMPL.applyTheme(drawable, t);
    }

    public static boolean canApplyTheme(@android.support.annotation.NonNull android.graphics.drawable.Drawable drawable) {
        return IMPL.canApplyTheme(drawable);
    }

    public static android.graphics.ColorFilter getColorFilter(@android.support.annotation.NonNull android.graphics.drawable.Drawable drawable) {
        return IMPL.getColorFilter(drawable);
    }

    public static void clearColorFilter(@android.support.annotation.NonNull android.graphics.drawable.Drawable drawable) {
        IMPL.clearColorFilter(drawable);
    }

    public static void inflate(@android.support.annotation.NonNull android.graphics.drawable.Drawable drawable, @android.support.annotation.NonNull android.content.res.Resources res, @android.support.annotation.NonNull org.xmlpull.v1.XmlPullParser parser, @android.support.annotation.NonNull android.util.AttributeSet attrs, @android.support.annotation.Nullable android.content.res.Resources.Theme theme) throws org.xmlpull.v1.XmlPullParserException, java.io.IOException {
        IMPL.inflate(drawable, res, parser, attrs, theme);
    }

    public static android.graphics.drawable.Drawable wrap(@android.support.annotation.NonNull android.graphics.drawable.Drawable drawable) {
        return IMPL.wrap(drawable);
    }

    public static <T extends android.graphics.drawable.Drawable> T unwrap(@android.support.annotation.NonNull android.graphics.drawable.Drawable drawable) {
        if (drawable instanceof android.support.v4.graphics.drawable.DrawableWrapper) {
            return ((android.support.v4.graphics.drawable.DrawableWrapper) drawable).getWrappedDrawable();
        }
        return drawable;
    }

    public static boolean setLayoutDirection(@android.support.annotation.NonNull android.graphics.drawable.Drawable drawable, int layoutDirection) {
        return IMPL.setLayoutDirection(drawable, layoutDirection);
    }

    public static int getLayoutDirection(@android.support.annotation.NonNull android.graphics.drawable.Drawable drawable) {
        return IMPL.getLayoutDirection(drawable);
    }

    private DrawableCompat() {
    }
}
