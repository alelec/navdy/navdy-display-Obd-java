package android.support.v4.graphics.drawable;

@android.annotation.TargetApi(11)
@android.support.annotation.RequiresApi(11)
class DrawableCompatHoneycomb {
    DrawableCompatHoneycomb() {
    }

    public static void jumpToCurrentState(android.graphics.drawable.Drawable drawable) {
        drawable.jumpToCurrentState();
    }

    public static android.graphics.drawable.Drawable wrapForTinting(android.graphics.drawable.Drawable drawable) {
        if (!(drawable instanceof android.support.v4.graphics.drawable.TintAwareDrawable)) {
            return new android.support.v4.graphics.drawable.DrawableWrapperHoneycomb(drawable);
        }
        return drawable;
    }
}
