package android.support.v4.graphics.drawable;

@android.annotation.TargetApi(17)
@android.support.annotation.RequiresApi(17)
class DrawableCompatJellybeanMr1 {
    private static final java.lang.String TAG = "DrawableCompatJellybeanMr1";
    private static java.lang.reflect.Method sGetLayoutDirectionMethod;
    private static boolean sGetLayoutDirectionMethodFetched;
    private static java.lang.reflect.Method sSetLayoutDirectionMethod;
    private static boolean sSetLayoutDirectionMethodFetched;

    DrawableCompatJellybeanMr1() {
    }

    public static boolean setLayoutDirection(android.graphics.drawable.Drawable drawable, int layoutDirection) {
        if (!sSetLayoutDirectionMethodFetched) {
            try {
                sSetLayoutDirectionMethod = android.graphics.drawable.Drawable.class.getDeclaredMethod("setLayoutDirection", new java.lang.Class[]{java.lang.Integer.TYPE});
                sSetLayoutDirectionMethod.setAccessible(true);
            } catch (java.lang.NoSuchMethodException e) {
                android.util.Log.i(TAG, "Failed to retrieve setLayoutDirection(int) method", e);
            }
            sSetLayoutDirectionMethodFetched = true;
        }
        if (sSetLayoutDirectionMethod != null) {
            try {
                sSetLayoutDirectionMethod.invoke(drawable, new java.lang.Object[]{java.lang.Integer.valueOf(layoutDirection)});
                return true;
            } catch (java.lang.Exception e2) {
                android.util.Log.i(TAG, "Failed to invoke setLayoutDirection(int) via reflection", e2);
                sSetLayoutDirectionMethod = null;
            }
        }
        return false;
    }

    public static int getLayoutDirection(android.graphics.drawable.Drawable drawable) {
        if (!sGetLayoutDirectionMethodFetched) {
            try {
                sGetLayoutDirectionMethod = android.graphics.drawable.Drawable.class.getDeclaredMethod("getLayoutDirection", new java.lang.Class[0]);
                sGetLayoutDirectionMethod.setAccessible(true);
            } catch (java.lang.NoSuchMethodException e) {
                android.util.Log.i(TAG, "Failed to retrieve getLayoutDirection() method", e);
            }
            sGetLayoutDirectionMethodFetched = true;
        }
        if (sGetLayoutDirectionMethod != null) {
            try {
                return ((java.lang.Integer) sGetLayoutDirectionMethod.invoke(drawable, new java.lang.Object[0])).intValue();
            } catch (java.lang.Exception e2) {
                android.util.Log.i(TAG, "Failed to invoke getLayoutDirection() via reflection", e2);
                sGetLayoutDirectionMethod = null;
            }
        }
        return -1;
    }
}
