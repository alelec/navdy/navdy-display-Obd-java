package android.support.v4.graphics.drawable;

@android.annotation.TargetApi(19)
@android.support.annotation.RequiresApi(19)
class DrawableCompatKitKat {
    DrawableCompatKitKat() {
    }

    public static void setAutoMirrored(android.graphics.drawable.Drawable drawable, boolean mirrored) {
        drawable.setAutoMirrored(mirrored);
    }

    public static boolean isAutoMirrored(android.graphics.drawable.Drawable drawable) {
        return drawable.isAutoMirrored();
    }

    public static android.graphics.drawable.Drawable wrapForTinting(android.graphics.drawable.Drawable drawable) {
        if (!(drawable instanceof android.support.v4.graphics.drawable.TintAwareDrawable)) {
            return new android.support.v4.graphics.drawable.DrawableWrapperKitKat(drawable);
        }
        return drawable;
    }

    public static int getAlpha(android.graphics.drawable.Drawable drawable) {
        return drawable.getAlpha();
    }
}
