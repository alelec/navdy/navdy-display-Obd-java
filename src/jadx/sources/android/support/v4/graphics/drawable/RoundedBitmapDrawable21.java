package android.support.v4.graphics.drawable;

@android.annotation.TargetApi(21)
@android.support.annotation.RequiresApi(21)
class RoundedBitmapDrawable21 extends android.support.v4.graphics.drawable.RoundedBitmapDrawable {
    protected RoundedBitmapDrawable21(android.content.res.Resources res, android.graphics.Bitmap bitmap) {
        super(res, bitmap);
    }

    public void getOutline(android.graphics.Outline outline) {
        updateDstRect();
        outline.setRoundRect(this.mDstRect, getCornerRadius());
    }

    public void setMipMap(boolean mipMap) {
        if (this.mBitmap != null) {
            this.mBitmap.setHasMipMap(mipMap);
            invalidateSelf();
        }
    }

    public boolean hasMipMap() {
        return this.mBitmap != null && this.mBitmap.hasMipMap();
    }

    /* access modifiers changed from: 0000 */
    public void gravityCompatApply(int gravity, int bitmapWidth, int bitmapHeight, android.graphics.Rect bounds, android.graphics.Rect outRect) {
        android.view.Gravity.apply(gravity, bitmapWidth, bitmapHeight, bounds, outRect, 0);
    }
}
