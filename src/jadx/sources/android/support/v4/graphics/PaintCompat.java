package android.support.v4.graphics;

public final class PaintCompat {
    public static boolean hasGlyph(@android.support.annotation.NonNull android.graphics.Paint paint, @android.support.annotation.NonNull java.lang.String string) {
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            return android.support.v4.graphics.PaintCompatApi23.hasGlyph(paint, string);
        }
        return android.support.v4.graphics.PaintCompatGingerbread.hasGlyph(paint, string);
    }

    private PaintCompat() {
    }
}
