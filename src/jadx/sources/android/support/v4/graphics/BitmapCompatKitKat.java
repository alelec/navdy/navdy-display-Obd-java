package android.support.v4.graphics;

@android.annotation.TargetApi(19)
@android.support.annotation.RequiresApi(19)
class BitmapCompatKitKat {
    BitmapCompatKitKat() {
    }

    static int getAllocationByteCount(android.graphics.Bitmap bitmap) {
        return bitmap.getAllocationByteCount();
    }
}
