package android.support.v4.print;

@android.annotation.TargetApi(24)
@android.support.annotation.RequiresApi(24)
class PrintHelperApi24 extends android.support.v4.print.PrintHelperApi23 {
    PrintHelperApi24(android.content.Context context) {
        super(context);
        this.mIsMinMarginsHandlingCorrect = true;
        this.mPrintActivityRespectsOrientation = true;
    }
}
