package android.support.v4.print;

@android.annotation.TargetApi(23)
@android.support.annotation.RequiresApi(23)
class PrintHelperApi23 extends android.support.v4.print.PrintHelperApi20 {
    /* access modifiers changed from: protected */
    public android.print.PrintAttributes.Builder copyAttributes(android.print.PrintAttributes other) {
        android.print.PrintAttributes.Builder b = super.copyAttributes(other);
        if (other.getDuplexMode() != 0) {
            b.setDuplexMode(other.getDuplexMode());
        }
        return b;
    }

    PrintHelperApi23(android.content.Context context) {
        super(context);
        this.mIsMinMarginsHandlingCorrect = false;
    }
}
