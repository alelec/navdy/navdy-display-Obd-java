package android.support.v4.print;

@android.annotation.TargetApi(20)
@android.support.annotation.RequiresApi(20)
class PrintHelperApi20 extends android.support.v4.print.PrintHelperKitkat {
    PrintHelperApi20(android.content.Context context) {
        super(context);
        this.mPrintActivityRespectsOrientation = false;
    }
}
