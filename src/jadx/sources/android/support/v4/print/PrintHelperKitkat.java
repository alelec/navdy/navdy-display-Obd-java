package android.support.v4.print;

@android.annotation.TargetApi(19)
@android.support.annotation.RequiresApi(19)
class PrintHelperKitkat {
    public static final int COLOR_MODE_COLOR = 2;
    public static final int COLOR_MODE_MONOCHROME = 1;
    private static final java.lang.String LOG_TAG = "PrintHelperKitkat";
    private static final int MAX_PRINT_SIZE = 3500;
    public static final int ORIENTATION_LANDSCAPE = 1;
    public static final int ORIENTATION_PORTRAIT = 2;
    public static final int SCALE_MODE_FILL = 2;
    public static final int SCALE_MODE_FIT = 1;
    int mColorMode = 2;
    final android.content.Context mContext;
    android.graphics.BitmapFactory.Options mDecodeOptions = null;
    protected boolean mIsMinMarginsHandlingCorrect = true;
    /* access modifiers changed from: private */
    public final java.lang.Object mLock = new java.lang.Object();
    int mOrientation;
    protected boolean mPrintActivityRespectsOrientation = true;
    int mScaleMode = 2;

    public interface OnPrintFinishCallback {
        void onFinish();
    }

    PrintHelperKitkat(android.content.Context context) {
        this.mContext = context;
    }

    public void setScaleMode(int scaleMode) {
        this.mScaleMode = scaleMode;
    }

    public int getScaleMode() {
        return this.mScaleMode;
    }

    public void setColorMode(int colorMode) {
        this.mColorMode = colorMode;
    }

    public void setOrientation(int orientation) {
        this.mOrientation = orientation;
    }

    public int getOrientation() {
        if (this.mOrientation == 0) {
            return 1;
        }
        return this.mOrientation;
    }

    public int getColorMode() {
        return this.mColorMode;
    }

    /* access modifiers changed from: private */
    public static boolean isPortrait(android.graphics.Bitmap bitmap) {
        if (bitmap.getWidth() <= bitmap.getHeight()) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public android.print.PrintAttributes.Builder copyAttributes(android.print.PrintAttributes other) {
        android.print.PrintAttributes.Builder b = new android.print.PrintAttributes.Builder().setMediaSize(other.getMediaSize()).setResolution(other.getResolution()).setMinMargins(other.getMinMargins());
        if (other.getColorMode() != 0) {
            b.setColorMode(other.getColorMode());
        }
        return b;
    }

    public void printBitmap(java.lang.String jobName, android.graphics.Bitmap bitmap, android.support.v4.print.PrintHelperKitkat.OnPrintFinishCallback callback) {
        android.print.PrintAttributes.MediaSize mediaSize;
        if (bitmap != null) {
            final int fittingMode = this.mScaleMode;
            android.print.PrintManager printManager = (android.print.PrintManager) this.mContext.getSystemService("print");
            if (isPortrait(bitmap)) {
                mediaSize = android.print.PrintAttributes.MediaSize.UNKNOWN_PORTRAIT;
            } else {
                mediaSize = android.print.PrintAttributes.MediaSize.UNKNOWN_LANDSCAPE;
            }
            final java.lang.String str = jobName;
            final android.graphics.Bitmap bitmap2 = bitmap;
            final android.support.v4.print.PrintHelperKitkat.OnPrintFinishCallback onPrintFinishCallback = callback;
            printManager.print(jobName, new android.print.PrintDocumentAdapter() {
                private android.print.PrintAttributes mAttributes;

                public void onLayout(android.print.PrintAttributes oldPrintAttributes, android.print.PrintAttributes newPrintAttributes, android.os.CancellationSignal cancellationSignal, android.print.PrintDocumentAdapter.LayoutResultCallback layoutResultCallback, android.os.Bundle bundle) {
                    boolean changed = true;
                    this.mAttributes = newPrintAttributes;
                    android.print.PrintDocumentInfo info = new android.print.PrintDocumentInfo.Builder(str).setContentType(1).setPageCount(1).build();
                    if (newPrintAttributes.equals(oldPrintAttributes)) {
                        changed = false;
                    }
                    layoutResultCallback.onLayoutFinished(info, changed);
                }

                public void onWrite(android.print.PageRange[] pageRanges, android.os.ParcelFileDescriptor fileDescriptor, android.os.CancellationSignal cancellationSignal, android.print.PrintDocumentAdapter.WriteResultCallback writeResultCallback) {
                    android.support.v4.print.PrintHelperKitkat.this.writeBitmap(this.mAttributes, fittingMode, bitmap2, fileDescriptor, cancellationSignal, writeResultCallback);
                }

                public void onFinish() {
                    if (onPrintFinishCallback != null) {
                        onPrintFinishCallback.onFinish();
                    }
                }
            }, new android.print.PrintAttributes.Builder().setMediaSize(mediaSize).setColorMode(this.mColorMode).build());
        }
    }

    /* access modifiers changed from: private */
    public android.graphics.Matrix getMatrix(int imageWidth, int imageHeight, android.graphics.RectF content, int fittingMode) {
        float scale;
        android.graphics.Matrix matrix = new android.graphics.Matrix();
        float scale2 = content.width() / ((float) imageWidth);
        if (fittingMode == 2) {
            scale = java.lang.Math.max(scale2, content.height() / ((float) imageHeight));
        } else {
            scale = java.lang.Math.min(scale2, content.height() / ((float) imageHeight));
        }
        matrix.postScale(scale, scale);
        matrix.postTranslate((content.width() - (((float) imageWidth) * scale)) / 2.0f, (content.height() - (((float) imageHeight) * scale)) / 2.0f);
        return matrix;
    }

    /* access modifiers changed from: private */
    public void writeBitmap(android.print.PrintAttributes attributes, int fittingMode, android.graphics.Bitmap bitmap, android.os.ParcelFileDescriptor fileDescriptor, android.os.CancellationSignal cancellationSignal, android.print.PrintDocumentAdapter.WriteResultCallback writeResultCallback) {
        final android.print.PrintAttributes pdfAttributes;
        if (this.mIsMinMarginsHandlingCorrect) {
            pdfAttributes = attributes;
        } else {
            pdfAttributes = copyAttributes(attributes).setMinMargins(new android.print.PrintAttributes.Margins(0, 0, 0, 0)).build();
        }
        final android.os.CancellationSignal cancellationSignal2 = cancellationSignal;
        final android.graphics.Bitmap bitmap2 = bitmap;
        final android.print.PrintAttributes printAttributes = attributes;
        final int i = fittingMode;
        final android.os.ParcelFileDescriptor parcelFileDescriptor = fileDescriptor;
        final android.print.PrintDocumentAdapter.WriteResultCallback writeResultCallback2 = writeResultCallback;
        new android.os.AsyncTask<java.lang.Void, java.lang.Void, java.lang.Throwable>() {
            /* access modifiers changed from: protected */
            /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
            /* JADX WARNING: Unknown top exception splitter block from list: {B:33:0x00b3=Splitter:B:33:0x00b3, B:46:0x00e4=Splitter:B:46:0x00e4, B:20:0x0078=Splitter:B:20:0x0078} */
            public java.lang.Throwable doInBackground(java.lang.Void... params) {
                android.print.pdf.PrintedPdfDocument pdfDocument;
                android.graphics.Bitmap maybeGrayscale;
                android.graphics.RectF contentRect;
                try {
                    if (cancellationSignal2.isCanceled()) {
                        return null;
                    }
                    pdfDocument = new android.print.pdf.PrintedPdfDocument(android.support.v4.print.PrintHelperKitkat.this.mContext, pdfAttributes);
                    maybeGrayscale = android.support.v4.print.PrintHelperKitkat.this.convertBitmapForColorMode(bitmap2, pdfAttributes.getColorMode());
                    if (cancellationSignal2.isCanceled()) {
                        return null;
                    }
                    android.graphics.pdf.PdfDocument.Page page = pdfDocument.startPage(1);
                    if (android.support.v4.print.PrintHelperKitkat.this.mIsMinMarginsHandlingCorrect) {
                        contentRect = new android.graphics.RectF(page.getInfo().getContentRect());
                    } else {
                        android.print.pdf.PrintedPdfDocument dummyDocument = new android.print.pdf.PrintedPdfDocument(android.support.v4.print.PrintHelperKitkat.this.mContext, printAttributes);
                        android.graphics.pdf.PdfDocument.Page dummyPage = dummyDocument.startPage(1);
                        contentRect = new android.graphics.RectF(dummyPage.getInfo().getContentRect());
                        dummyDocument.finishPage(dummyPage);
                        dummyDocument.close();
                    }
                    android.graphics.Matrix matrix = android.support.v4.print.PrintHelperKitkat.this.getMatrix(maybeGrayscale.getWidth(), maybeGrayscale.getHeight(), contentRect, i);
                    if (!android.support.v4.print.PrintHelperKitkat.this.mIsMinMarginsHandlingCorrect) {
                        matrix.postTranslate(contentRect.left, contentRect.top);
                        page.getCanvas().clipRect(contentRect);
                    }
                    page.getCanvas().drawBitmap(maybeGrayscale, matrix, null);
                    pdfDocument.finishPage(page);
                    if (cancellationSignal2.isCanceled()) {
                        pdfDocument.close();
                        if (parcelFileDescriptor != null) {
                            try {
                                parcelFileDescriptor.close();
                            } catch (java.io.IOException e) {
                            }
                        }
                        if (maybeGrayscale == bitmap2) {
                            return null;
                        }
                        maybeGrayscale.recycle();
                        return null;
                    }
                    pdfDocument.writeTo(new java.io.FileOutputStream(parcelFileDescriptor.getFileDescriptor()));
                    pdfDocument.close();
                    if (parcelFileDescriptor != null) {
                        try {
                            parcelFileDescriptor.close();
                        } catch (java.io.IOException e2) {
                        }
                    }
                    if (maybeGrayscale == bitmap2) {
                        return null;
                    }
                    maybeGrayscale.recycle();
                    return null;
                } catch (Throwable th) {
                    return th;
                }
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(java.lang.Throwable throwable) {
                if (cancellationSignal2.isCanceled()) {
                    writeResultCallback2.onWriteCancelled();
                } else if (throwable == null) {
                    writeResultCallback2.onWriteFinished(new android.print.PageRange[]{android.print.PageRange.ALL_PAGES});
                } else {
                    android.util.Log.e(android.support.v4.print.PrintHelperKitkat.LOG_TAG, "Error writing printed content", throwable);
                    writeResultCallback2.onWriteFailed(null);
                }
            }
        }.execute(new java.lang.Void[0]);
    }

    public void printBitmap(java.lang.String jobName, android.net.Uri imageFile, android.support.v4.print.PrintHelperKitkat.OnPrintFinishCallback callback) throws java.io.FileNotFoundException {
        final int fittingMode = this.mScaleMode;
        final java.lang.String str = jobName;
        final android.net.Uri uri = imageFile;
        final android.support.v4.print.PrintHelperKitkat.OnPrintFinishCallback onPrintFinishCallback = callback;
        android.print.PrintDocumentAdapter printDocumentAdapter = new android.print.PrintDocumentAdapter() {
            /* access modifiers changed from: private */
            public android.print.PrintAttributes mAttributes;
            android.graphics.Bitmap mBitmap = null;
            android.os.AsyncTask<android.net.Uri, java.lang.Boolean, android.graphics.Bitmap> mLoadBitmap;

            public void onLayout(android.print.PrintAttributes oldPrintAttributes, android.print.PrintAttributes newPrintAttributes, android.os.CancellationSignal cancellationSignal, android.print.PrintDocumentAdapter.LayoutResultCallback layoutResultCallback, android.os.Bundle bundle) {
                boolean changed = true;
                synchronized (this) {
                    this.mAttributes = newPrintAttributes;
                }
                if (cancellationSignal.isCanceled()) {
                    layoutResultCallback.onLayoutCancelled();
                } else if (this.mBitmap != null) {
                    android.print.PrintDocumentInfo info = new android.print.PrintDocumentInfo.Builder(str).setContentType(1).setPageCount(1).build();
                    if (newPrintAttributes.equals(oldPrintAttributes)) {
                        changed = false;
                    }
                    layoutResultCallback.onLayoutFinished(info, changed);
                } else {
                    final android.os.CancellationSignal cancellationSignal2 = cancellationSignal;
                    final android.print.PrintAttributes printAttributes = newPrintAttributes;
                    final android.print.PrintAttributes printAttributes2 = oldPrintAttributes;
                    final android.print.PrintDocumentAdapter.LayoutResultCallback layoutResultCallback2 = layoutResultCallback;
                    this.mLoadBitmap = new android.os.AsyncTask<android.net.Uri, java.lang.Boolean, android.graphics.Bitmap>() {
                        /* access modifiers changed from: protected */
                        public void onPreExecute() {
                            cancellationSignal2.setOnCancelListener(new android.os.CancellationSignal.OnCancelListener() {
                                public void onCancel() {
                                    android.support.v4.print.PrintHelperKitkat.AnonymousClass3.this.cancelLoad();
                                    android.support.v4.print.PrintHelperKitkat.AnonymousClass3.AnonymousClass1.this.cancel(false);
                                }
                            });
                        }

                        /* access modifiers changed from: protected */
                        public android.graphics.Bitmap doInBackground(android.net.Uri... uris) {
                            try {
                                return android.support.v4.print.PrintHelperKitkat.this.loadConstrainedBitmap(uri, android.support.v4.print.PrintHelperKitkat.MAX_PRINT_SIZE);
                            } catch (java.io.FileNotFoundException e) {
                                return null;
                            }
                        }

                        /* access modifiers changed from: protected */
                        public void onPostExecute(android.graphics.Bitmap bitmap) {
                            boolean changed;
                            android.print.PrintAttributes.MediaSize mediaSize;
                            super.onPostExecute(bitmap);
                            if (bitmap != null && (!android.support.v4.print.PrintHelperKitkat.this.mPrintActivityRespectsOrientation || android.support.v4.print.PrintHelperKitkat.this.mOrientation == 0)) {
                                synchronized (this) {
                                    mediaSize = android.support.v4.print.PrintHelperKitkat.AnonymousClass3.this.mAttributes.getMediaSize();
                                }
                                if (!(mediaSize == null || mediaSize.isPortrait() == android.support.v4.print.PrintHelperKitkat.isPortrait(bitmap))) {
                                    android.graphics.Matrix rotation = new android.graphics.Matrix();
                                    rotation.postRotate(90.0f);
                                    bitmap = android.graphics.Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), rotation, true);
                                }
                            }
                            android.support.v4.print.PrintHelperKitkat.AnonymousClass3.this.mBitmap = bitmap;
                            if (bitmap != null) {
                                android.print.PrintDocumentInfo info = new android.print.PrintDocumentInfo.Builder(str).setContentType(1).setPageCount(1).build();
                                if (!printAttributes.equals(printAttributes2)) {
                                    changed = true;
                                } else {
                                    changed = false;
                                }
                                layoutResultCallback2.onLayoutFinished(info, changed);
                            } else {
                                layoutResultCallback2.onLayoutFailed(null);
                            }
                            android.support.v4.print.PrintHelperKitkat.AnonymousClass3.this.mLoadBitmap = null;
                        }

                        /* access modifiers changed from: protected */
                        public void onCancelled(android.graphics.Bitmap result) {
                            layoutResultCallback2.onLayoutCancelled();
                            android.support.v4.print.PrintHelperKitkat.AnonymousClass3.this.mLoadBitmap = null;
                        }
                    }.execute(new android.net.Uri[0]);
                }
            }

            /* access modifiers changed from: private */
            public void cancelLoad() {
                synchronized (android.support.v4.print.PrintHelperKitkat.this.mLock) {
                    if (android.support.v4.print.PrintHelperKitkat.this.mDecodeOptions != null) {
                        android.support.v4.print.PrintHelperKitkat.this.mDecodeOptions.requestCancelDecode();
                        android.support.v4.print.PrintHelperKitkat.this.mDecodeOptions = null;
                    }
                }
            }

            public void onFinish() {
                super.onFinish();
                cancelLoad();
                if (this.mLoadBitmap != null) {
                    this.mLoadBitmap.cancel(true);
                }
                if (onPrintFinishCallback != null) {
                    onPrintFinishCallback.onFinish();
                }
                if (this.mBitmap != null) {
                    this.mBitmap.recycle();
                    this.mBitmap = null;
                }
            }

            public void onWrite(android.print.PageRange[] pageRanges, android.os.ParcelFileDescriptor fileDescriptor, android.os.CancellationSignal cancellationSignal, android.print.PrintDocumentAdapter.WriteResultCallback writeResultCallback) {
                android.support.v4.print.PrintHelperKitkat.this.writeBitmap(this.mAttributes, fittingMode, this.mBitmap, fileDescriptor, cancellationSignal, writeResultCallback);
            }
        };
        android.print.PrintManager printManager = (android.print.PrintManager) this.mContext.getSystemService("print");
        android.print.PrintAttributes.Builder builder = new android.print.PrintAttributes.Builder();
        builder.setColorMode(this.mColorMode);
        if (this.mOrientation == 1 || this.mOrientation == 0) {
            builder.setMediaSize(android.print.PrintAttributes.MediaSize.UNKNOWN_LANDSCAPE);
        } else if (this.mOrientation == 2) {
            builder.setMediaSize(android.print.PrintAttributes.MediaSize.UNKNOWN_PORTRAIT);
        }
        printManager.print(jobName, printDocumentAdapter, builder.build());
    }

    /* access modifiers changed from: private */
    public android.graphics.Bitmap loadConstrainedBitmap(android.net.Uri uri, int maxSideLength) throws java.io.FileNotFoundException {
        android.graphics.BitmapFactory.Options decodeOptions;
        android.graphics.Bitmap bitmap = null;
        if (maxSideLength <= 0 || uri == null || this.mContext == null) {
            throw new java.lang.IllegalArgumentException("bad argument to getScaledBitmap");
        }
        android.graphics.BitmapFactory.Options opt = new android.graphics.BitmapFactory.Options();
        opt.inJustDecodeBounds = true;
        loadBitmap(uri, opt);
        int w = opt.outWidth;
        int h = opt.outHeight;
        if (w > 0 && h > 0) {
            int imageSide = java.lang.Math.max(w, h);
            int sampleSize = 1;
            while (imageSide > maxSideLength) {
                imageSide >>>= 1;
                sampleSize <<= 1;
            }
            if (sampleSize > 0 && java.lang.Math.min(w, h) / sampleSize > 0) {
                synchronized (this.mLock) {
                    this.mDecodeOptions = new android.graphics.BitmapFactory.Options();
                    this.mDecodeOptions.inMutable = true;
                    this.mDecodeOptions.inSampleSize = sampleSize;
                    decodeOptions = this.mDecodeOptions;
                }
                try {
                    bitmap = loadBitmap(uri, decodeOptions);
                    synchronized (this.mLock) {
                        this.mDecodeOptions = null;
                    }
                } catch (Throwable th) {
                    synchronized (this.mLock) {
                        this.mDecodeOptions = null;
                        throw th;
                    }
                }
            }
        }
        return bitmap;
    }

    private android.graphics.Bitmap loadBitmap(android.net.Uri uri, android.graphics.BitmapFactory.Options o) throws java.io.FileNotFoundException {
        if (uri == null || this.mContext == null) {
            throw new java.lang.IllegalArgumentException("bad argument to loadBitmap");
        }
        java.io.InputStream is = null;
        try {
            is = this.mContext.getContentResolver().openInputStream(uri);
            android.graphics.Bitmap decodeStream = android.graphics.BitmapFactory.decodeStream(is, null, o);
            if (is != null) {
                try {
                    is.close();
                } catch (java.io.IOException t) {
                    android.util.Log.w(LOG_TAG, "close fail ", t);
                }
            }
            return decodeStream;
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (java.io.IOException t2) {
                    android.util.Log.w(LOG_TAG, "close fail ", t2);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public android.graphics.Bitmap convertBitmapForColorMode(android.graphics.Bitmap original, int colorMode) {
        if (colorMode != 1) {
            return original;
        }
        android.graphics.Bitmap grayscale = android.graphics.Bitmap.createBitmap(original.getWidth(), original.getHeight(), android.graphics.Bitmap.Config.ARGB_8888);
        android.graphics.Canvas c = new android.graphics.Canvas(grayscale);
        android.graphics.Paint p = new android.graphics.Paint();
        android.graphics.ColorMatrix cm = new android.graphics.ColorMatrix();
        cm.setSaturation(0.0f);
        p.setColorFilter(new android.graphics.ColorMatrixColorFilter(cm));
        c.drawBitmap(original, 0.0f, 0.0f, p);
        c.setBitmap(null);
        return grayscale;
    }
}
