package butterknife;

@butterknife.internal.ListenerClass(callbacks = butterknife.OnTextChanged.Callback.class, setter = "addTextChangedListener", targetType = "android.widget.TextView", type = "android.text.TextWatcher")
@java.lang.annotation.Target({java.lang.annotation.ElementType.METHOD})
@java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.CLASS)
public @interface OnTextChanged {

    public enum Callback {
        TEXT_CHANGED,
        BEFORE_TEXT_CHANGED,
        AFTER_TEXT_CHANGED
    }

    butterknife.OnTextChanged.Callback callback() default butterknife.OnTextChanged.Callback.TEXT_CHANGED;

    int[] value() default {-1};
}
