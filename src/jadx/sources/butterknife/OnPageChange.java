package butterknife;

@butterknife.internal.ListenerClass(callbacks = butterknife.OnPageChange.Callback.class, setter = "setOnPageChangeListener", targetType = "android.support.v4.view.ViewPager", type = "android.support.v4.view.ViewPager.OnPageChangeListener")
@java.lang.annotation.Target({java.lang.annotation.ElementType.METHOD})
@java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.CLASS)
public @interface OnPageChange {

    public enum Callback {
        PAGE_SELECTED,
        PAGE_SCROLLED,
        PAGE_SCROLL_STATE_CHANGED
    }

    butterknife.OnPageChange.Callback callback() default butterknife.OnPageChange.Callback.PAGE_SELECTED;

    int[] value() default {-1};
}
