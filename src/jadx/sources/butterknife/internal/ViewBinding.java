package butterknife.internal;

final class ViewBinding implements butterknife.internal.Binding {
    private final java.lang.String name;
    private final boolean required;
    private final java.lang.String type;

    ViewBinding(java.lang.String name2, java.lang.String type2, boolean required2) {
        this.name = name2;
        this.type = type2;
        this.required = required2;
    }

    public java.lang.String getName() {
        return this.name;
    }

    public java.lang.String getType() {
        return this.type;
    }

    public java.lang.String getDescription() {
        return "field '" + this.name + "'";
    }

    public boolean isRequired() {
        return this.required;
    }
}
