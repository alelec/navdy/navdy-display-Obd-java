package butterknife.internal;

@java.lang.annotation.Target({java.lang.annotation.ElementType.FIELD})
@java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
public @interface ListenerMethod {
    java.lang.String defaultReturn() default "null";

    java.lang.String name();

    java.lang.String[] parameters() default {};

    java.lang.String returnType() default "void";
}
