package butterknife.internal;

final class CollectionBinding implements butterknife.internal.Binding {
    private final butterknife.internal.CollectionBinding.Kind kind;
    private final java.lang.String name;
    private final boolean required;
    private final java.lang.String type;

    enum Kind {
        ARRAY,
        LIST
    }

    CollectionBinding(java.lang.String name2, java.lang.String type2, butterknife.internal.CollectionBinding.Kind kind2, boolean required2) {
        this.name = name2;
        this.type = type2;
        this.kind = kind2;
        this.required = required2;
    }

    public java.lang.String getName() {
        return this.name;
    }

    public java.lang.String getType() {
        return this.type;
    }

    public butterknife.internal.CollectionBinding.Kind getKind() {
        return this.kind;
    }

    public boolean isRequired() {
        return this.required;
    }

    public java.lang.String getDescription() {
        return "field '" + this.name + "'";
    }
}
