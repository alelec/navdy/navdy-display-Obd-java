package butterknife.internal;

final class ViewInjector {
    private final java.lang.String className;
    private final java.lang.String classPackage;
    private final java.util.Map<butterknife.internal.CollectionBinding, int[]> collectionBindings = new java.util.LinkedHashMap();
    private java.lang.String parentInjector;
    private final java.lang.String targetClass;
    private final java.util.Map<java.lang.Integer, butterknife.internal.ViewInjection> viewIdMap = new java.util.LinkedHashMap();

    ViewInjector(java.lang.String classPackage2, java.lang.String className2, java.lang.String targetClass2) {
        this.classPackage = classPackage2;
        this.className = className2;
        this.targetClass = targetClass2;
    }

    /* access modifiers changed from: 0000 */
    public void addView(int id, butterknife.internal.ViewBinding binding) {
        getOrCreateViewInjection(id).addViewBinding(binding);
    }

    /* access modifiers changed from: 0000 */
    public boolean addListener(int id, butterknife.internal.ListenerClass listener, butterknife.internal.ListenerMethod method, butterknife.internal.ListenerBinding binding) {
        butterknife.internal.ViewInjection viewInjection = getOrCreateViewInjection(id);
        if (viewInjection.hasListenerBinding(listener, method) && !"void".equals(method.returnType())) {
            return false;
        }
        viewInjection.addListenerBinding(listener, method, binding);
        return true;
    }

    /* access modifiers changed from: 0000 */
    public void addCollection(int[] ids, butterknife.internal.CollectionBinding binding) {
        this.collectionBindings.put(binding, ids);
    }

    /* access modifiers changed from: 0000 */
    public void setParentInjector(java.lang.String parentInjector2) {
        this.parentInjector = parentInjector2;
    }

    /* access modifiers changed from: 0000 */
    public butterknife.internal.ViewInjection getViewInjection(int id) {
        return (butterknife.internal.ViewInjection) this.viewIdMap.get(java.lang.Integer.valueOf(id));
    }

    private butterknife.internal.ViewInjection getOrCreateViewInjection(int id) {
        butterknife.internal.ViewInjection viewId = (butterknife.internal.ViewInjection) this.viewIdMap.get(java.lang.Integer.valueOf(id));
        if (viewId != null) {
            return viewId;
        }
        butterknife.internal.ViewInjection viewId2 = new butterknife.internal.ViewInjection(id);
        this.viewIdMap.put(java.lang.Integer.valueOf(id), viewId2);
        return viewId2;
    }

    /* access modifiers changed from: 0000 */
    public java.lang.String getFqcn() {
        return this.classPackage + "." + this.className;
    }

    /* access modifiers changed from: 0000 */
    public java.lang.String brewJava() {
        java.lang.StringBuilder builder = new java.lang.StringBuilder();
        builder.append("// Generated code from Butter Knife. Do not modify!\n");
        builder.append("package ").append(this.classPackage).append(";\n\n");
        builder.append("import android.view.View;\n");
        builder.append("import butterknife.ButterKnife.Finder;\n\n");
        builder.append("public class ").append(this.className).append(" {\n");
        emitInject(builder);
        builder.append(10);
        emitReset(builder);
        builder.append("}\n");
        return builder.toString();
    }

    private void emitInject(java.lang.StringBuilder builder) {
        builder.append("  public static void inject(Finder finder, final ").append(this.targetClass).append(" target, Object source) {\n");
        if (this.parentInjector != null) {
            builder.append("    ").append(this.parentInjector).append(".inject(finder, target, source);\n\n");
        }
        builder.append("    View view;\n");
        for (butterknife.internal.ViewInjection injection : this.viewIdMap.values()) {
            emitViewInjection(builder, injection);
        }
        for (java.util.Map.Entry<butterknife.internal.CollectionBinding, int[]> entry : this.collectionBindings.entrySet()) {
            emitCollectionBinding(builder, (butterknife.internal.CollectionBinding) entry.getKey(), (int[]) entry.getValue());
        }
        builder.append("  }\n");
    }

    private void emitCollectionBinding(java.lang.StringBuilder builder, butterknife.internal.CollectionBinding binding, int[] ids) {
        builder.append("    target.").append(binding.getName()).append(" = ");
        switch (binding.getKind()) {
            case ARRAY:
                builder.append("Finder.arrayOf(");
                break;
            case LIST:
                builder.append("Finder.listOf(");
                break;
            default:
                throw new java.lang.IllegalStateException("Unknown kind: " + binding.getKind());
        }
        for (int i = 0; i < ids.length; i++) {
            if (i > 0) {
                builder.append(ch.qos.logback.core.CoreConstants.COMMA_CHAR);
            }
            builder.append("\n        ");
            emitCastIfNeeded(builder, binding.getType());
            if (binding.isRequired()) {
                builder.append("finder.findRequiredView(source, ").append(ids[i]).append(", \"").append(binding.getName()).append("\")");
            } else {
                builder.append("finder.findOptionalView(source, ").append(ids[i]).append(")");
            }
        }
        builder.append("\n    );");
    }

    private void emitViewInjection(java.lang.StringBuilder builder, butterknife.internal.ViewInjection injection) {
        builder.append("    view = ");
        java.util.List<butterknife.internal.Binding> requiredBindings = injection.getRequiredBindings();
        if (requiredBindings.isEmpty()) {
            builder.append("finder.findOptionalView(source, ").append(injection.getId()).append(");\n");
        } else if (injection.getId() == -1) {
            builder.append("target;\n");
        } else {
            builder.append("finder.findRequiredView(source, ").append(injection.getId()).append(", \"");
            emitHumanDescription(builder, requiredBindings);
            builder.append("\");\n");
        }
        emitViewBindings(builder, injection);
        emitListenerBindings(builder, injection);
    }

    private void emitViewBindings(java.lang.StringBuilder builder, butterknife.internal.ViewInjection injection) {
        java.util.Collection<butterknife.internal.ViewBinding> viewBindings = injection.getViewBindings();
        if (!viewBindings.isEmpty()) {
            for (butterknife.internal.ViewBinding viewBinding : viewBindings) {
                builder.append("    target.").append(viewBinding.getName()).append(" = ");
                emitCastIfNeeded(builder, viewBinding.getType());
                builder.append("view;\n");
            }
        }
    }

    private void emitListenerBindings(java.lang.StringBuilder builder, butterknife.internal.ViewInjection injection) {
        java.util.Map<butterknife.internal.ListenerClass, java.util.Map<butterknife.internal.ListenerMethod, java.util.Set<butterknife.internal.ListenerBinding>>> bindings = injection.getListenerBindings();
        if (!bindings.isEmpty()) {
            java.lang.String extraIndent = "";
            boolean needsNullChecked = injection.getRequiredBindings().isEmpty();
            if (needsNullChecked) {
                builder.append("    if (view != null) {\n");
                extraIndent = "  ";
            }
            for (java.util.Map.Entry<butterknife.internal.ListenerClass, java.util.Map<butterknife.internal.ListenerMethod, java.util.Set<butterknife.internal.ListenerBinding>>> e : bindings.entrySet()) {
                butterknife.internal.ListenerClass listener = (butterknife.internal.ListenerClass) e.getKey();
                java.util.Map<butterknife.internal.ListenerMethod, java.util.Set<butterknife.internal.ListenerBinding>> methodBindings = (java.util.Map) e.getValue();
                boolean needsCast = !"android.view.View".equals(listener.targetType());
                builder.append(extraIndent).append("    ");
                if (needsCast) {
                    builder.append("((").append(listener.targetType());
                    if (listener.genericArguments() > 0) {
                        builder.append(kotlin.text.Typography.less);
                        for (int i = 0; i < listener.genericArguments(); i++) {
                            if (i > 0) {
                                builder.append(", ");
                            }
                            builder.append('?');
                        }
                        builder.append('>');
                    }
                    builder.append(") ");
                }
                builder.append("view");
                if (needsCast) {
                    builder.append(ch.qos.logback.core.CoreConstants.RIGHT_PARENTHESIS_CHAR);
                }
                builder.append(ch.qos.logback.core.CoreConstants.DOT).append(listener.setter()).append("(\n");
                builder.append(extraIndent).append("      new ").append(listener.type()).append("() {\n");
                for (butterknife.internal.ListenerMethod method : getListenerMethods(listener)) {
                    builder.append(extraIndent).append("        @Override public ").append(method.returnType()).append(' ').append(method.name()).append("(\n");
                    java.lang.String[] parameterTypes = method.parameters();
                    int count = parameterTypes.length;
                    for (int i2 = 0; i2 < count; i2++) {
                        builder.append(extraIndent).append("          ").append(parameterTypes[i2]).append(" p").append(i2);
                        if (i2 < count - 1) {
                            builder.append(ch.qos.logback.core.CoreConstants.COMMA_CHAR);
                        }
                        builder.append(10);
                    }
                    builder.append(extraIndent).append("        ) {\n");
                    builder.append(extraIndent).append("          ");
                    boolean hasReturnType = !"void".equals(method.returnType());
                    if (hasReturnType) {
                        builder.append("return ");
                    }
                    if (methodBindings.containsKey(method)) {
                        java.util.Iterator<butterknife.internal.ListenerBinding> iterator = ((java.util.Set) methodBindings.get(method)).iterator();
                        while (iterator.hasNext()) {
                            butterknife.internal.ListenerBinding binding = (butterknife.internal.ListenerBinding) iterator.next();
                            builder.append("target.").append(binding.getName()).append(ch.qos.logback.core.CoreConstants.LEFT_PARENTHESIS_CHAR);
                            java.util.List<butterknife.internal.Parameter> parameters = binding.getParameters();
                            java.lang.String[] listenerParameters = method.parameters();
                            int count2 = parameters.size();
                            for (int i3 = 0; i3 < count2; i3++) {
                                butterknife.internal.Parameter parameter = (butterknife.internal.Parameter) parameters.get(i3);
                                int listenerPosition = parameter.getListenerPosition();
                                emitCastIfNeeded(builder, listenerParameters[listenerPosition], parameter.getType());
                                builder.append('p').append(listenerPosition);
                                if (i3 < count2 - 1) {
                                    builder.append(", ");
                                }
                            }
                            builder.append(");");
                            if (iterator.hasNext()) {
                                builder.append("\n").append("          ");
                            }
                        }
                    } else if (hasReturnType) {
                        builder.append(method.defaultReturn()).append(';');
                    }
                    builder.append(10);
                    builder.append(extraIndent).append("        }\n");
                }
                builder.append(extraIndent).append("      });\n");
            }
            if (needsNullChecked) {
                builder.append("    }\n");
            }
        }
    }

    static java.util.List<butterknife.internal.ListenerMethod> getListenerMethods(butterknife.internal.ListenerClass listener) {
        java.lang.Enum<?>[] enumArr;
        if (listener.method().length == 1) {
            return java.util.Arrays.asList(listener.method());
        }
        try {
            java.util.List<butterknife.internal.ListenerMethod> methods = new java.util.ArrayList<>();
            java.lang.Class<? extends java.lang.Enum<?>> callbacks = listener.callbacks();
            for (java.lang.Enum<?> callbackMethod : (java.lang.Enum[]) callbacks.getEnumConstants()) {
                butterknife.internal.ListenerMethod method = (butterknife.internal.ListenerMethod) callbacks.getField(callbackMethod.name()).getAnnotation(butterknife.internal.ListenerMethod.class);
                if (method == null) {
                    throw new java.lang.IllegalStateException(java.lang.String.format("@%s's %s.%s missing @%s annotation.", new java.lang.Object[]{callbacks.getEnclosingClass().getSimpleName(), callbacks.getSimpleName(), callbackMethod.name(), butterknife.internal.ListenerMethod.class.getSimpleName()}));
                }
                methods.add(method);
            }
            return methods;
        } catch (java.lang.NoSuchFieldException e) {
            throw new java.lang.AssertionError(e);
        }
    }

    private void emitReset(java.lang.StringBuilder builder) {
        builder.append("  public static void reset(").append(this.targetClass).append(" target) {\n");
        if (this.parentInjector != null) {
            builder.append("    ").append(this.parentInjector).append(".reset(target);\n\n");
        }
        for (butterknife.internal.ViewInjection injection : this.viewIdMap.values()) {
            for (butterknife.internal.ViewBinding viewBinding : injection.getViewBindings()) {
                builder.append("    target.").append(viewBinding.getName()).append(" = null;\n");
            }
        }
        for (butterknife.internal.CollectionBinding collectionBinding : this.collectionBindings.keySet()) {
            builder.append("    target.").append(collectionBinding.getName()).append(" = null;\n");
        }
        builder.append("  }\n");
    }

    static void emitCastIfNeeded(java.lang.StringBuilder builder, java.lang.String viewType) {
        emitCastIfNeeded(builder, "android.view.View", viewType);
    }

    static void emitCastIfNeeded(java.lang.StringBuilder builder, java.lang.String sourceType, java.lang.String destinationType) {
        if (!sourceType.equals(destinationType)) {
            builder.append(ch.qos.logback.core.CoreConstants.LEFT_PARENTHESIS_CHAR).append(destinationType).append(") ");
        }
    }

    static void emitHumanDescription(java.lang.StringBuilder builder, java.util.List<butterknife.internal.Binding> bindings) {
        switch (bindings.size()) {
            case 1:
                builder.append(((butterknife.internal.Binding) bindings.get(0)).getDescription());
                return;
            case 2:
                builder.append(((butterknife.internal.Binding) bindings.get(0)).getDescription()).append(" and ").append(((butterknife.internal.Binding) bindings.get(1)).getDescription());
                return;
            default:
                int count = bindings.size();
                for (int i = 0; i < count; i++) {
                    butterknife.internal.Binding requiredField = (butterknife.internal.Binding) bindings.get(i);
                    if (i != 0) {
                        builder.append(", ");
                    }
                    if (i == count - 1) {
                        builder.append("and ");
                    }
                    builder.append(requiredField.getDescription());
                }
                return;
        }
    }
}
