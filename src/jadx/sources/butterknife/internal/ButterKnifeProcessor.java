package butterknife.internal;

public final class ButterKnifeProcessor extends javax.annotation.processing.AbstractProcessor {
    static final /* synthetic */ boolean $assertionsDisabled = (!butterknife.internal.ButterKnifeProcessor.class.desiredAssertionStatus());
    public static final java.lang.String ANDROID_PREFIX = "android.";
    public static final java.lang.String JAVA_PREFIX = "java.";
    private static final java.util.List<java.lang.Class<? extends java.lang.annotation.Annotation>> LISTENERS = java.util.Arrays.asList(new java.lang.Class[]{butterknife.OnCheckedChanged.class, butterknife.OnClick.class, butterknife.OnEditorAction.class, butterknife.OnFocusChange.class, butterknife.OnItemClick.class, butterknife.OnItemLongClick.class, butterknife.OnItemSelected.class, butterknife.OnLongClick.class, butterknife.OnPageChange.class, butterknife.OnTextChanged.class, butterknife.OnTouch.class});
    private static final java.lang.String LIST_TYPE = java.util.List.class.getCanonicalName();
    public static final java.lang.String SUFFIX = "$$ViewInjector";
    static final java.lang.String VIEW_TYPE = "android.view.View";
    private javax.lang.model.util.Elements elementUtils;
    private javax.annotation.processing.Filer filer;
    private javax.lang.model.util.Types typeUtils;

    public synchronized void init(javax.annotation.processing.ProcessingEnvironment env) {
        butterknife.internal.ButterKnifeProcessor.super.init(env);
        this.elementUtils = env.getElementUtils();
        this.typeUtils = env.getTypeUtils();
        this.filer = env.getFiler();
    }

    public java.util.Set<java.lang.String> getSupportedAnnotationTypes() {
        java.util.Set<java.lang.String> supportTypes = new java.util.LinkedHashSet<>();
        supportTypes.add(butterknife.InjectView.class.getCanonicalName());
        supportTypes.add(butterknife.InjectViews.class.getCanonicalName());
        for (java.lang.Class<? extends java.lang.annotation.Annotation> listener : LISTENERS) {
            supportTypes.add(listener.getCanonicalName());
        }
        return supportTypes;
    }

    public boolean process(java.util.Set<? extends javax.lang.model.element.TypeElement> set, javax.annotation.processing.RoundEnvironment env) {
        for (java.util.Map.Entry<javax.lang.model.element.TypeElement, butterknife.internal.ViewInjector> entry : findAndParseTargets(env).entrySet()) {
            javax.lang.model.element.TypeElement typeElement = (javax.lang.model.element.TypeElement) entry.getKey();
            butterknife.internal.ViewInjector viewInjector = (butterknife.internal.ViewInjector) entry.getValue();
            try {
                java.io.Writer writer = this.filer.createSourceFile(viewInjector.getFqcn(), new javax.lang.model.element.Element[]{typeElement}).openWriter();
                writer.write(viewInjector.brewJava());
                writer.flush();
                writer.close();
            } catch (java.io.IOException e) {
                error(typeElement, "Unable to write injector for type %s: %s", typeElement, e.getMessage());
            }
        }
        return true;
    }

    private java.util.Map<javax.lang.model.element.TypeElement, butterknife.internal.ViewInjector> findAndParseTargets(javax.annotation.processing.RoundEnvironment env) {
        java.util.Map<javax.lang.model.element.TypeElement, butterknife.internal.ViewInjector> targetClassMap = new java.util.LinkedHashMap<>();
        java.util.Set<java.lang.String> erasedTargetNames = new java.util.LinkedHashSet<>();
        for (javax.lang.model.element.Element element : env.getElementsAnnotatedWith(butterknife.InjectView.class)) {
            try {
                parseInjectView(element, targetClassMap, erasedTargetNames);
            } catch (java.lang.Exception e) {
                java.io.StringWriter stackTrace = new java.io.StringWriter();
                e.printStackTrace(new java.io.PrintWriter(stackTrace));
                error(element, "Unable to generate view injector for @InjectView.\n\n%s", stackTrace);
            }
        }
        for (javax.lang.model.element.Element element2 : env.getElementsAnnotatedWith(butterknife.InjectViews.class)) {
            try {
                parseInjectViews(element2, targetClassMap, erasedTargetNames);
            } catch (java.lang.Exception e2) {
                java.io.StringWriter stackTrace2 = new java.io.StringWriter();
                e2.printStackTrace(new java.io.PrintWriter(stackTrace2));
                error(element2, "Unable to generate view injector for @InjectViews.\n\n%s", stackTrace2);
            }
        }
        for (java.lang.Class<? extends java.lang.annotation.Annotation> listener : LISTENERS) {
            findAndParseListener(env, listener, targetClassMap, erasedTargetNames);
        }
        for (java.util.Map.Entry<javax.lang.model.element.TypeElement, butterknife.internal.ViewInjector> entry : targetClassMap.entrySet()) {
            java.lang.String parentClassFqcn = findParentFqcn((javax.lang.model.element.TypeElement) entry.getKey(), erasedTargetNames);
            if (parentClassFqcn != null) {
                ((butterknife.internal.ViewInjector) entry.getValue()).setParentInjector(parentClassFqcn + SUFFIX);
            }
        }
        return targetClassMap;
    }

    private boolean isValidForGeneratedCode(java.lang.Class<? extends java.lang.annotation.Annotation> annotationClass, java.lang.String targetThing, javax.lang.model.element.Element element) {
        boolean hasError = false;
        javax.lang.model.element.TypeElement enclosingElement = element.getEnclosingElement();
        java.util.Set<javax.lang.model.element.Modifier> modifiers = element.getModifiers();
        if (modifiers.contains(javax.lang.model.element.Modifier.PRIVATE) || modifiers.contains(javax.lang.model.element.Modifier.STATIC)) {
            error(element, "@%s %s must not be private or static. (%s.%s)", annotationClass.getSimpleName(), targetThing, enclosingElement.getQualifiedName(), element.getSimpleName());
            hasError = true;
        }
        if (enclosingElement.getKind() != javax.lang.model.element.ElementKind.CLASS) {
            error(enclosingElement, "@%s %s may only be contained in classes. (%s.%s)", annotationClass.getSimpleName(), targetThing, enclosingElement.getQualifiedName(), element.getSimpleName());
            hasError = true;
        }
        if (!enclosingElement.getModifiers().contains(javax.lang.model.element.Modifier.PRIVATE)) {
            return hasError;
        }
        error(enclosingElement, "@%s %s may not be contained in private classes. (%s.%s)", annotationClass.getSimpleName(), targetThing, enclosingElement.getQualifiedName(), element.getSimpleName());
        return true;
    }

    private boolean isBindingInWrongPackage(java.lang.Class<? extends java.lang.annotation.Annotation> annotationClass, javax.lang.model.element.Element element) {
        java.lang.String qualifiedName = element.getEnclosingElement().getQualifiedName().toString();
        if (qualifiedName.startsWith(ANDROID_PREFIX)) {
            error(element, "@%s-annotated class incorrectly in Android framework package. (%s)", annotationClass.getSimpleName(), qualifiedName);
            return true;
        } else if (!qualifiedName.startsWith(JAVA_PREFIX)) {
            return false;
        } else {
            error(element, "@%s-annotated class incorrectly in Java framework package. (%s)", annotationClass.getSimpleName(), qualifiedName);
            return true;
        }
    }

    private void parseInjectView(javax.lang.model.element.Element element, java.util.Map<javax.lang.model.element.TypeElement, butterknife.internal.ViewInjector> targetClassMap, java.util.Set<java.lang.String> erasedTargetNames) {
        boolean hasError = false;
        javax.lang.model.element.TypeElement enclosingElement = element.getEnclosingElement();
        javax.lang.model.type.TypeMirror elementType = element.asType();
        if (elementType instanceof javax.lang.model.type.TypeVariable) {
            elementType = ((javax.lang.model.type.TypeVariable) elementType).getUpperBound();
        }
        if (!isSubtypeOfType(elementType, VIEW_TYPE)) {
            error(element, "@InjectView fields must extend from View. (%s.%s)", enclosingElement.getQualifiedName(), element.getSimpleName());
            hasError = true;
        }
        boolean hasError2 = hasError | isValidForGeneratedCode(butterknife.InjectView.class, "fields", element) | isBindingInWrongPackage(butterknife.InjectView.class, element);
        if (element.getAnnotation(butterknife.InjectViews.class) != null) {
            error(element, "Only one of @InjectView and @InjectViews is allowed. (%s.%s)", enclosingElement.getQualifiedName(), element.getSimpleName());
            hasError2 = true;
        }
        if (!hasError2) {
            int id = ((butterknife.InjectView) element.getAnnotation(butterknife.InjectView.class)).value();
            butterknife.internal.ViewInjector injector = (butterknife.internal.ViewInjector) targetClassMap.get(enclosingElement);
            if (injector != null) {
                butterknife.internal.ViewInjection viewInjection = injector.getViewInjection(id);
                if (viewInjection != null) {
                    java.util.Iterator<butterknife.internal.ViewBinding> iterator = viewInjection.getViewBindings().iterator();
                    if (iterator.hasNext()) {
                        error(element, "Attempt to use @InjectView for an already injected ID %d on '%s'. (%s.%s)", java.lang.Integer.valueOf(id), ((butterknife.internal.ViewBinding) iterator.next()).getName(), enclosingElement.getQualifiedName(), element.getSimpleName());
                        return;
                    }
                }
            }
            getOrCreateTargetClass(targetClassMap, enclosingElement).addView(id, new butterknife.internal.ViewBinding(element.getSimpleName().toString(), elementType.toString(), element.getAnnotation(butterknife.Optional.class) == null));
            erasedTargetNames.add(enclosingElement.toString());
        }
    }

    private void parseInjectViews(javax.lang.model.element.Element element, java.util.Map<javax.lang.model.element.TypeElement, butterknife.internal.ViewInjector> targetClassMap, java.util.Set<java.lang.String> erasedTargetNames) {
        boolean hasError = false;
        javax.lang.model.element.TypeElement enclosingElement = element.getEnclosingElement();
        javax.lang.model.type.TypeMirror elementType = element.asType();
        java.lang.String erasedType = doubleErasure(elementType);
        javax.lang.model.type.TypeMirror viewType = null;
        butterknife.internal.CollectionBinding.Kind kind = null;
        if (elementType.getKind() == javax.lang.model.type.TypeKind.ARRAY) {
            viewType = ((javax.lang.model.type.ArrayType) elementType).getComponentType();
            kind = butterknife.internal.CollectionBinding.Kind.ARRAY;
        } else if (LIST_TYPE.equals(erasedType)) {
            java.util.List<? extends javax.lang.model.type.TypeMirror> typeArguments = ((javax.lang.model.type.DeclaredType) elementType).getTypeArguments();
            if (typeArguments.size() != 1) {
                error(element, "@InjectViews List must have a generic component. (%s.%s)", enclosingElement.getQualifiedName(), element.getSimpleName());
                hasError = true;
            } else {
                viewType = (javax.lang.model.type.TypeMirror) typeArguments.get(0);
            }
            kind = butterknife.internal.CollectionBinding.Kind.LIST;
        } else {
            error(element, "@InjectViews must be a List or array. (%s.%s)", enclosingElement.getQualifiedName(), element.getSimpleName());
            hasError = true;
        }
        if (viewType instanceof javax.lang.model.type.TypeVariable) {
            viewType = ((javax.lang.model.type.TypeVariable) viewType).getUpperBound();
        }
        if (viewType != null && !isSubtypeOfType(viewType, VIEW_TYPE)) {
            error(element, "@InjectViews type must extend from View. (%s.%s)", enclosingElement.getQualifiedName(), element.getSimpleName());
            hasError = true;
        }
        if (!(hasError | isValidForGeneratedCode(butterknife.InjectViews.class, "fields", element)) && !isBindingInWrongPackage(butterknife.InjectViews.class, element)) {
            java.lang.String name = element.getSimpleName().toString();
            int[] ids = ((butterknife.InjectViews) element.getAnnotation(butterknife.InjectViews.class)).value();
            if (ids.length == 0) {
                error(element, "@InjectViews must specify at least one ID. (%s.%s)", enclosingElement.getQualifiedName(), element.getSimpleName());
                return;
            }
            java.lang.Integer duplicateId = findDuplicate(ids);
            if (duplicateId != null) {
                error(element, "@InjectViews annotation contains duplicate ID %d. (%s.%s)", duplicateId, enclosingElement.getQualifiedName(), element.getSimpleName());
            }
            if ($assertionsDisabled || viewType != null) {
                getOrCreateTargetClass(targetClassMap, enclosingElement).addCollection(ids, new butterknife.internal.CollectionBinding(name, viewType.toString(), kind, element.getAnnotation(butterknife.Optional.class) == null));
                erasedTargetNames.add(enclosingElement.toString());
                return;
            }
            throw new java.lang.AssertionError();
        }
    }

    private static java.lang.Integer findDuplicate(int[] array) {
        java.util.Set<java.lang.Integer> seenElements = new java.util.LinkedHashSet<>();
        for (int element : array) {
            if (!seenElements.add(java.lang.Integer.valueOf(element))) {
                return java.lang.Integer.valueOf(element);
            }
        }
        return null;
    }

    private java.lang.String doubleErasure(javax.lang.model.type.TypeMirror elementType) {
        java.lang.String name = this.typeUtils.erasure(elementType).toString();
        int typeParamStart = name.indexOf(60);
        if (typeParamStart != -1) {
            return name.substring(0, typeParamStart);
        }
        return name;
    }

    private void findAndParseListener(javax.annotation.processing.RoundEnvironment env, java.lang.Class<? extends java.lang.annotation.Annotation> annotationClass, java.util.Map<javax.lang.model.element.TypeElement, butterknife.internal.ViewInjector> targetClassMap, java.util.Set<java.lang.String> erasedTargetNames) {
        for (javax.lang.model.element.Element element : env.getElementsAnnotatedWith(annotationClass)) {
            try {
                parseListenerAnnotation(annotationClass, element, targetClassMap, erasedTargetNames);
            } catch (java.lang.Exception e) {
                java.io.StringWriter stackTrace = new java.io.StringWriter();
                e.printStackTrace(new java.io.PrintWriter(stackTrace));
                error(element, "Unable to generate view injector for @%s.\n\n%s", annotationClass.getSimpleName(), stackTrace.toString());
            }
        }
    }

    private void parseListenerAnnotation(java.lang.Class<? extends java.lang.annotation.Annotation> annotationClass, javax.lang.model.element.Element element, java.util.Map<javax.lang.model.element.TypeElement, butterknife.internal.ViewInjector> targetClassMap, java.util.Set<java.lang.String> erasedTargetNames) throws java.lang.Exception {
        butterknife.internal.ListenerMethod method;
        if (!(element instanceof javax.lang.model.element.ExecutableElement) || element.getKind() != javax.lang.model.element.ElementKind.METHOD) {
            throw new java.lang.IllegalStateException(java.lang.String.format("@%s annotation must be on a method.", new java.lang.Object[]{annotationClass.getSimpleName()}));
        }
        javax.lang.model.element.ExecutableElement executableElement = (javax.lang.model.element.ExecutableElement) element;
        javax.lang.model.element.TypeElement enclosingElement = element.getEnclosingElement();
        java.lang.annotation.Annotation annotation = element.getAnnotation(annotationClass);
        java.lang.reflect.Method annotationValue = annotationClass.getDeclaredMethod("value", new java.lang.Class[0]);
        if (annotationValue.getReturnType() != int[].class) {
            throw new java.lang.IllegalStateException(java.lang.String.format("@%s annotation value() type not int[].", new java.lang.Object[]{annotationClass}));
        }
        int[] ids = (int[]) annotationValue.invoke(annotation, new java.lang.Object[0]);
        java.lang.String name = executableElement.getSimpleName().toString();
        boolean required = element.getAnnotation(butterknife.Optional.class) == null;
        boolean hasError = isValidForGeneratedCode(annotationClass, "methods", element) | isBindingInWrongPackage(annotationClass, element);
        java.lang.Integer duplicateId = findDuplicate(ids);
        if (duplicateId != null) {
            error(element, "@%s annotation for method contains duplicate ID %d. (%s.%s)", annotationClass.getSimpleName(), duplicateId, enclosingElement.getQualifiedName(), element.getSimpleName());
            hasError = true;
        }
        butterknife.internal.ListenerClass listener = (butterknife.internal.ListenerClass) annotationClass.getAnnotation(butterknife.internal.ListenerClass.class);
        if (listener == null) {
            throw new java.lang.IllegalStateException(java.lang.String.format("No @%s defined on @%s.", new java.lang.Object[]{butterknife.internal.ListenerClass.class.getSimpleName(), annotationClass.getSimpleName()}));
        }
        int length = ids.length;
        for (int i = 0; i < length; i++) {
            int id = ids[i];
            if (id == -1) {
                if (ids.length == 1) {
                    if (!required) {
                        error(element, "ID free injection must not be annotated with @Optional. (%s.%s)", enclosingElement.getQualifiedName(), element.getSimpleName());
                        hasError = true;
                    }
                    java.lang.String targetType = listener.targetType();
                    if (!isSubtypeOfType(enclosingElement.asType(), targetType)) {
                        error(element, "@%s annotation without an ID may only be used with an object of type \"%s\". (%s.%s)", annotationClass.getSimpleName(), targetType, enclosingElement.getQualifiedName(), element.getSimpleName());
                        hasError = true;
                    }
                } else {
                    error(element, "@%s annotation contains invalid ID %d. (%s.%s)", annotationClass.getSimpleName(), java.lang.Integer.valueOf(id), enclosingElement.getQualifiedName(), element.getSimpleName());
                    hasError = true;
                }
            }
        }
        butterknife.internal.ListenerMethod[] methods = listener.method();
        if (methods.length > 1) {
            throw new java.lang.IllegalStateException(java.lang.String.format("Multiple listener methods specified on @%s.", new java.lang.Object[]{annotationClass.getSimpleName()}));
        }
        if (methods.length != 1) {
            java.lang.Enum<?> callback = (java.lang.Enum) annotationClass.getDeclaredMethod("callback", new java.lang.Class[0]).invoke(annotation, new java.lang.Object[0]);
            method = (butterknife.internal.ListenerMethod) callback.getDeclaringClass().getField(callback.name()).getAnnotation(butterknife.internal.ListenerMethod.class);
            if (method == null) {
                throw new java.lang.IllegalStateException(java.lang.String.format("No @%s defined on @%s's %s.%s.", new java.lang.Object[]{butterknife.internal.ListenerMethod.class.getSimpleName(), annotationClass.getSimpleName(), callback.getDeclaringClass().getSimpleName(), callback.name()}));
            }
        } else if (listener.callbacks() != butterknife.internal.ListenerClass.NONE.class) {
            throw new java.lang.IllegalStateException(java.lang.String.format("Both method() and callback() defined on @%s.", new java.lang.Object[]{annotationClass.getSimpleName()}));
        } else {
            method = methods[0];
        }
        java.util.List<? extends javax.lang.model.element.VariableElement> methodParameters = executableElement.getParameters();
        if (methodParameters.size() > method.parameters().length) {
            error(element, "@%s methods can have at most %s parameter(s). (%s.%s)", annotationClass.getSimpleName(), java.lang.Integer.valueOf(method.parameters().length), enclosingElement.getQualifiedName(), element.getSimpleName());
            hasError = true;
        }
        javax.lang.model.type.TypeMirror returnType = executableElement.getReturnType();
        if (returnType instanceof javax.lang.model.type.TypeVariable) {
            returnType = ((javax.lang.model.type.TypeVariable) returnType).getUpperBound();
        }
        if (!returnType.toString().equals(method.returnType())) {
            error(element, "@%s methods must have a '%s' return type. (%s.%s)", annotationClass.getSimpleName(), method.returnType(), enclosingElement.getQualifiedName(), element.getSimpleName());
            hasError = true;
        }
        if (!hasError) {
            butterknife.internal.Parameter[] parameters = butterknife.internal.Parameter.NONE;
            if (!methodParameters.isEmpty()) {
                parameters = new butterknife.internal.Parameter[methodParameters.size()];
                java.util.BitSet bitSet = new java.util.BitSet(methodParameters.size());
                java.lang.String[] parameterTypes = method.parameters();
                for (int i2 = 0; i2 < methodParameters.size(); i2++) {
                    javax.lang.model.type.TypeMirror methodParameterType = ((javax.lang.model.element.VariableElement) methodParameters.get(i2)).asType();
                    if (methodParameterType instanceof javax.lang.model.type.TypeVariable) {
                        methodParameterType = ((javax.lang.model.type.TypeVariable) methodParameterType).getUpperBound();
                    }
                    int j = 0;
                    while (true) {
                        if (j >= parameterTypes.length) {
                            break;
                        } else if (!bitSet.get(j) && isSubtypeOfType(methodParameterType, parameterTypes[j])) {
                            butterknife.internal.Parameter parameter = new butterknife.internal.Parameter(j, methodParameterType.toString());
                            parameters[i2] = parameter;
                            bitSet.set(j);
                            break;
                        } else {
                            j++;
                        }
                    }
                    if (parameters[i2] == null) {
                        java.lang.StringBuilder builder = new java.lang.StringBuilder();
                        builder.append("Unable to match @").append(annotationClass.getSimpleName()).append(" method arguments. (").append(enclosingElement.getQualifiedName()).append(ch.qos.logback.core.CoreConstants.DOT).append(element.getSimpleName()).append(ch.qos.logback.core.CoreConstants.RIGHT_PARENTHESIS_CHAR);
                        for (int j2 = 0; j2 < parameters.length; j2++) {
                            butterknife.internal.Parameter parameter2 = parameters[j2];
                            builder.append("\n\n  Parameter #").append(j2 + 1).append(": ").append(((javax.lang.model.element.VariableElement) methodParameters.get(j2)).asType().toString()).append("\n    ");
                            if (parameter2 == null) {
                                builder.append("did not match any listener parameters");
                            } else {
                                builder.append("matched listener parameter #").append(parameter2.getListenerPosition() + 1).append(": ").append(parameter2.getType());
                            }
                        }
                        builder.append("\n\nMethods may have up to ").append(method.parameters().length).append(" parameter(s):\n");
                        java.lang.String[] parameters2 = method.parameters();
                        int length2 = parameters2.length;
                        for (int i3 = 0; i3 < length2; i3++) {
                            builder.append("\n  ").append(parameters2[i3]);
                        }
                        builder.append("\n\nThese may be listed in any order but will be searched for from top to bottom.");
                        error(executableElement, builder.toString(), new java.lang.Object[0]);
                        return;
                    }
                }
            }
            butterknife.internal.ListenerBinding binding = new butterknife.internal.ListenerBinding(name, java.util.Arrays.asList(parameters), required);
            butterknife.internal.ViewInjector viewInjector = getOrCreateTargetClass(targetClassMap, enclosingElement);
            int length3 = ids.length;
            for (int i4 = 0; i4 < length3; i4++) {
                int id2 = ids[i4];
                if (!viewInjector.addListener(id2, listener, method, binding)) {
                    error(element, "Multiple listener methods with return value specified for ID %d. (%s.%s)", java.lang.Integer.valueOf(id2), enclosingElement.getQualifiedName(), element.getSimpleName());
                    return;
                }
            }
            erasedTargetNames.add(enclosingElement.toString());
        }
    }

    private boolean isSubtypeOfType(javax.lang.model.type.TypeMirror typeMirror, java.lang.String otherType) {
        if (otherType.equals(typeMirror.toString())) {
            return true;
        }
        if (!(typeMirror instanceof javax.lang.model.type.DeclaredType)) {
            return false;
        }
        javax.lang.model.type.DeclaredType declaredType = (javax.lang.model.type.DeclaredType) typeMirror;
        java.util.List<? extends javax.lang.model.type.TypeMirror> typeArguments = declaredType.getTypeArguments();
        if (typeArguments.size() > 0) {
            java.lang.StringBuilder typeString = new java.lang.StringBuilder(declaredType.asElement().toString());
            typeString.append(kotlin.text.Typography.less);
            for (int i = 0; i < typeArguments.size(); i++) {
                if (i > 0) {
                    typeString.append(ch.qos.logback.core.CoreConstants.COMMA_CHAR);
                }
                typeString.append('?');
            }
            typeString.append('>');
            if (typeString.toString().equals(otherType)) {
                return true;
            }
        }
        javax.lang.model.element.Element element = declaredType.asElement();
        if (!(element instanceof javax.lang.model.element.TypeElement)) {
            return false;
        }
        javax.lang.model.element.TypeElement typeElement = (javax.lang.model.element.TypeElement) element;
        if (isSubtypeOfType(typeElement.getSuperclass(), otherType)) {
            return true;
        }
        for (javax.lang.model.type.TypeMirror interfaceType : typeElement.getInterfaces()) {
            if (isSubtypeOfType(interfaceType, otherType)) {
                return true;
            }
        }
        return false;
    }

    private butterknife.internal.ViewInjector getOrCreateTargetClass(java.util.Map<javax.lang.model.element.TypeElement, butterknife.internal.ViewInjector> targetClassMap, javax.lang.model.element.TypeElement enclosingElement) {
        butterknife.internal.ViewInjector viewInjector = (butterknife.internal.ViewInjector) targetClassMap.get(enclosingElement);
        if (viewInjector != null) {
            return viewInjector;
        }
        java.lang.String targetType = enclosingElement.getQualifiedName().toString();
        java.lang.String classPackage = getPackageName(enclosingElement);
        butterknife.internal.ViewInjector viewInjector2 = new butterknife.internal.ViewInjector(classPackage, getClassName(enclosingElement, classPackage) + SUFFIX, targetType);
        targetClassMap.put(enclosingElement, viewInjector2);
        return viewInjector2;
    }

    private static java.lang.String getClassName(javax.lang.model.element.TypeElement type, java.lang.String packageName) {
        return type.getQualifiedName().toString().substring(packageName.length() + 1).replace(ch.qos.logback.core.CoreConstants.DOT, '$');
    }

    private java.lang.String findParentFqcn(javax.lang.model.element.TypeElement typeElement, java.util.Set<java.lang.String> parents) {
        do {
            javax.lang.model.type.TypeMirror type = typeElement.getSuperclass();
            if (type.getKind() == javax.lang.model.type.TypeKind.NONE) {
                return null;
            }
            typeElement = ((javax.lang.model.type.DeclaredType) type).asElement();
        } while (!parents.contains(typeElement.toString()));
        java.lang.String packageName = getPackageName(typeElement);
        return packageName + "." + getClassName(typeElement, packageName);
    }

    public javax.lang.model.SourceVersion getSupportedSourceVersion() {
        return javax.lang.model.SourceVersion.latestSupported();
    }

    private void error(javax.lang.model.element.Element element, java.lang.String message, java.lang.Object... args) {
        if (args.length > 0) {
            message = java.lang.String.format(message, args);
        }
        this.processingEnv.getMessager().printMessage(javax.tools.Diagnostic.Kind.ERROR, message, element);
    }

    private java.lang.String getPackageName(javax.lang.model.element.TypeElement type) {
        return this.elementUtils.getPackageOf(type).getQualifiedName().toString();
    }
}
