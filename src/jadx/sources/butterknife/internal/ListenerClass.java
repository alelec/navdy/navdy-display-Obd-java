package butterknife.internal;

@java.lang.annotation.Target({java.lang.annotation.ElementType.ANNOTATION_TYPE})
@java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
public @interface ListenerClass {

    public enum NONE {
    }

    java.lang.Class<? extends java.lang.Enum<?>> callbacks() default butterknife.internal.ListenerClass.NONE.class;

    int genericArguments() default 0;

    butterknife.internal.ListenerMethod[] method() default {};

    java.lang.String setter();

    java.lang.String targetType();

    java.lang.String type();
}
