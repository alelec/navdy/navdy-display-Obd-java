package butterknife.internal;

final class Parameter {
    static final butterknife.internal.Parameter[] NONE = new butterknife.internal.Parameter[0];
    private final int listenerPosition;
    private final java.lang.String type;

    Parameter(int listenerPosition2, java.lang.String type2) {
        this.listenerPosition = listenerPosition2;
        this.type = type2;
    }

    /* access modifiers changed from: 0000 */
    public int getListenerPosition() {
        return this.listenerPosition;
    }

    /* access modifiers changed from: 0000 */
    public java.lang.String getType() {
        return this.type;
    }
}
