package butterknife.internal;

final class ViewInjection {
    private final int id;
    private final java.util.LinkedHashMap<butterknife.internal.ListenerClass, java.util.Map<butterknife.internal.ListenerMethod, java.util.Set<butterknife.internal.ListenerBinding>>> listenerBindings = new java.util.LinkedHashMap<>();
    private final java.util.Set<butterknife.internal.ViewBinding> viewBindings = new java.util.LinkedHashSet();

    ViewInjection(int id2) {
        this.id = id2;
    }

    public int getId() {
        return this.id;
    }

    public java.util.Collection<butterknife.internal.ViewBinding> getViewBindings() {
        return this.viewBindings;
    }

    public java.util.Map<butterknife.internal.ListenerClass, java.util.Map<butterknife.internal.ListenerMethod, java.util.Set<butterknife.internal.ListenerBinding>>> getListenerBindings() {
        return this.listenerBindings;
    }

    public boolean hasListenerBinding(butterknife.internal.ListenerClass listener, butterknife.internal.ListenerMethod method) {
        java.util.Map<butterknife.internal.ListenerMethod, java.util.Set<butterknife.internal.ListenerBinding>> methods = (java.util.Map) this.listenerBindings.get(listener);
        return methods != null && methods.containsKey(method);
    }

    public void addListenerBinding(butterknife.internal.ListenerClass listener, butterknife.internal.ListenerMethod method, butterknife.internal.ListenerBinding binding) {
        java.util.Map<butterknife.internal.ListenerMethod, java.util.Set<butterknife.internal.ListenerBinding>> methods = (java.util.Map) this.listenerBindings.get(listener);
        java.util.Set<butterknife.internal.ListenerBinding> set = null;
        if (methods == null) {
            methods = new java.util.LinkedHashMap<>();
            this.listenerBindings.put(listener, methods);
        } else {
            set = (java.util.Set) methods.get(method);
        }
        if (set == null) {
            set = new java.util.LinkedHashSet<>();
            methods.put(method, set);
        }
        set.add(binding);
    }

    public void addViewBinding(butterknife.internal.ViewBinding viewBinding) {
        this.viewBindings.add(viewBinding);
    }

    public java.util.List<butterknife.internal.Binding> getRequiredBindings() {
        java.util.List<butterknife.internal.Binding> requiredBindings = new java.util.ArrayList<>();
        for (butterknife.internal.ViewBinding viewBinding : this.viewBindings) {
            if (viewBinding.isRequired()) {
                requiredBindings.add(viewBinding);
            }
        }
        for (java.util.Map<butterknife.internal.ListenerMethod, java.util.Set<butterknife.internal.ListenerBinding>> methodBinding : this.listenerBindings.values()) {
            for (java.util.Set<butterknife.internal.ListenerBinding> set : methodBinding.values()) {
                for (butterknife.internal.ListenerBinding binding : set) {
                    if (binding.isRequired()) {
                        requiredBindings.add(binding);
                    }
                }
            }
        }
        return requiredBindings;
    }
}
