package butterknife.internal;

public abstract class DebouncingOnClickListener implements android.view.View.OnClickListener {
    private static final java.lang.Runnable ENABLE_AGAIN = new java.lang.Runnable() {
        public void run() {
            butterknife.internal.DebouncingOnClickListener.enabled = true;
        }
    };
    /* access modifiers changed from: private */
    public static boolean enabled = true;

    public abstract void doClick(android.view.View view);

    public final void onClick(android.view.View v) {
        if (enabled) {
            enabled = false;
            v.post(ENABLE_AGAIN);
            doClick(v);
        }
    }
}
