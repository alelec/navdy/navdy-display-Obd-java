package butterknife.internal;

interface Binding {
    java.lang.String getDescription();
}
