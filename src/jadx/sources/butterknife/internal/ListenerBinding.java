package butterknife.internal;

final class ListenerBinding implements butterknife.internal.Binding {
    private final java.lang.String name;
    private final java.util.List<butterknife.internal.Parameter> parameters;
    private final boolean required;

    ListenerBinding(java.lang.String name2, java.util.List<butterknife.internal.Parameter> parameters2, boolean required2) {
        this.name = name2;
        this.parameters = java.util.Collections.unmodifiableList(new java.util.ArrayList(parameters2));
        this.required = required2;
    }

    public java.lang.String getName() {
        return this.name;
    }

    public java.util.List<butterknife.internal.Parameter> getParameters() {
        return this.parameters;
    }

    public java.lang.String getDescription() {
        return "method '" + this.name + "'";
    }

    public boolean isRequired() {
        return this.required;
    }
}
