package butterknife;

public final class ButterKnife {
    static final java.util.Map<java.lang.Class<?>, java.lang.reflect.Method> INJECTORS = new java.util.LinkedHashMap();
    static final java.lang.reflect.Method NO_OP = null;
    static final java.util.Map<java.lang.Class<?>, java.lang.reflect.Method> RESETTERS = new java.util.LinkedHashMap();
    private static final java.lang.String TAG = "ButterKnife";
    private static boolean debug = false;

    public interface Action<T extends android.view.View> {
        void apply(T t, int i);
    }

    public enum Finder {
        VIEW {
            public android.view.View findOptionalView(java.lang.Object source, int id) {
                return ((android.view.View) source).findViewById(id);
            }

            /* access modifiers changed from: protected */
            public android.content.Context getContext(java.lang.Object source) {
                return ((android.view.View) source).getContext();
            }
        },
        ACTIVITY {
            public android.view.View findOptionalView(java.lang.Object source, int id) {
                return ((android.app.Activity) source).findViewById(id);
            }

            /* access modifiers changed from: protected */
            public android.content.Context getContext(java.lang.Object source) {
                return (android.app.Activity) source;
            }
        },
        DIALOG {
            public android.view.View findOptionalView(java.lang.Object source, int id) {
                return ((android.app.Dialog) source).findViewById(id);
            }

            /* access modifiers changed from: protected */
            public android.content.Context getContext(java.lang.Object source) {
                return ((android.app.Dialog) source).getContext();
            }
        };

        public abstract android.view.View findOptionalView(java.lang.Object obj, int i);

        /* access modifiers changed from: protected */
        public abstract android.content.Context getContext(java.lang.Object obj);

        public static <T extends android.view.View> T[] arrayOf(T... views) {
            return views;
        }

        public static <T extends android.view.View> java.util.List<T> listOf(T... views) {
            return new butterknife.ImmutableViewList(views);
        }

        public android.view.View findRequiredView(java.lang.Object source, int id, java.lang.String who) {
            android.view.View view = findOptionalView(source, id);
            if (view != null) {
                return view;
            }
            throw new java.lang.IllegalStateException("Required view '" + getContext(source).getResources().getResourceEntryName(id) + "' with ID " + id + " for " + who + " was not found. If this view is optional add '@Optional' annotation.");
        }
    }

    public interface Setter<T extends android.view.View, V> {
        void set(T t, V v, int i);
    }

    private ButterKnife() {
        throw new java.lang.AssertionError("No instances.");
    }

    public static void setDebug(boolean debug2) {
        debug = debug2;
    }

    public static void inject(android.app.Activity target) {
        inject(target, target, butterknife.ButterKnife.Finder.ACTIVITY);
    }

    public static void inject(android.view.View target) {
        inject(target, target, butterknife.ButterKnife.Finder.VIEW);
    }

    public static void inject(android.app.Dialog target) {
        inject(target, target, butterknife.ButterKnife.Finder.DIALOG);
    }

    public static void inject(java.lang.Object target, android.app.Activity source) {
        inject(target, source, butterknife.ButterKnife.Finder.ACTIVITY);
    }

    public static void inject(java.lang.Object target, android.view.View source) {
        inject(target, source, butterknife.ButterKnife.Finder.VIEW);
    }

    public static void inject(java.lang.Object target, android.app.Dialog source) {
        inject(target, source, butterknife.ButterKnife.Finder.DIALOG);
    }

    public static void reset(java.lang.Object target) {
        java.lang.Class<?> targetClass = target.getClass();
        try {
            if (debug) {
                android.util.Log.d(TAG, "Looking up view injector for " + targetClass.getName());
            }
            java.lang.reflect.Method reset = findResettersForClass(targetClass);
            if (reset != null) {
                reset.invoke(null, new java.lang.Object[]{target});
            }
        } catch (java.lang.RuntimeException e) {
            throw e;
        } catch (java.lang.Exception e2) {
            java.lang.Throwable t = e2;
            if (t instanceof java.lang.reflect.InvocationTargetException) {
                t = t.getCause();
            }
            throw new java.lang.RuntimeException("Unable to reset views for " + target, t);
        }
    }

    static void inject(java.lang.Object target, java.lang.Object source, butterknife.ButterKnife.Finder finder) {
        java.lang.Class<?> targetClass = target.getClass();
        try {
            if (debug) {
                android.util.Log.d(TAG, "Looking up view injector for " + targetClass.getName());
            }
            java.lang.reflect.Method inject = findInjectorForClass(targetClass);
            if (inject != null) {
                inject.invoke(null, new java.lang.Object[]{finder, target, source});
            }
        } catch (java.lang.RuntimeException e) {
            throw e;
        } catch (java.lang.Exception e2) {
            java.lang.Throwable t = e2;
            if (t instanceof java.lang.reflect.InvocationTargetException) {
                t = t.getCause();
            }
            throw new java.lang.RuntimeException("Unable to inject views for " + target, t);
        }
    }

    private static java.lang.reflect.Method findInjectorForClass(java.lang.Class<?> cls) throws java.lang.NoSuchMethodException {
        java.lang.reflect.Method inject;
        java.lang.reflect.Method inject2 = (java.lang.reflect.Method) INJECTORS.get(cls);
        if (inject2 != null) {
            if (debug) {
                android.util.Log.d(TAG, "HIT: Cached in injector map.");
            }
            return inject2;
        }
        java.lang.String clsName = cls.getName();
        if (clsName.startsWith(butterknife.internal.ButterKnifeProcessor.ANDROID_PREFIX) || clsName.startsWith(butterknife.internal.ButterKnifeProcessor.JAVA_PREFIX)) {
            if (debug) {
                android.util.Log.d(TAG, "MISS: Reached framework class. Abandoning search.");
            }
            return NO_OP;
        }
        try {
            inject = java.lang.Class.forName(clsName + butterknife.internal.ButterKnifeProcessor.SUFFIX).getMethod("inject", new java.lang.Class[]{butterknife.ButterKnife.Finder.class, cls, java.lang.Object.class});
            if (debug) {
                android.util.Log.d(TAG, "HIT: Class loaded injection class.");
            }
        } catch (java.lang.ClassNotFoundException e) {
            if (debug) {
                android.util.Log.d(TAG, "Not found. Trying superclass " + cls.getSuperclass().getName());
            }
            inject = findInjectorForClass(cls.getSuperclass());
        }
        INJECTORS.put(cls, inject);
        return inject;
    }

    private static java.lang.reflect.Method findResettersForClass(java.lang.Class<?> cls) throws java.lang.NoSuchMethodException {
        java.lang.reflect.Method inject;
        java.lang.reflect.Method inject2 = (java.lang.reflect.Method) RESETTERS.get(cls);
        if (inject2 != null) {
            if (debug) {
                android.util.Log.d(TAG, "HIT: Cached in injector map.");
            }
            return inject2;
        }
        java.lang.String clsName = cls.getName();
        if (clsName.startsWith(butterknife.internal.ButterKnifeProcessor.ANDROID_PREFIX) || clsName.startsWith(butterknife.internal.ButterKnifeProcessor.JAVA_PREFIX)) {
            if (debug) {
                android.util.Log.d(TAG, "MISS: Reached framework class. Abandoning search.");
            }
            return NO_OP;
        }
        try {
            inject = java.lang.Class.forName(clsName + butterknife.internal.ButterKnifeProcessor.SUFFIX).getMethod("reset", new java.lang.Class[]{cls});
            if (debug) {
                android.util.Log.d(TAG, "HIT: Class loaded injection class.");
            }
        } catch (java.lang.ClassNotFoundException e) {
            if (debug) {
                android.util.Log.d(TAG, "Not found. Trying superclass " + cls.getSuperclass().getName());
            }
            inject = findResettersForClass(cls.getSuperclass());
        }
        RESETTERS.put(cls, inject);
        return inject;
    }

    public static <T extends android.view.View> void apply(java.util.List<T> list, butterknife.ButterKnife.Action<? super T> action) {
        int count = list.size();
        for (int i = 0; i < count; i++) {
            action.apply((android.view.View) list.get(i), i);
        }
    }

    public static <T extends android.view.View, V> void apply(java.util.List<T> list, butterknife.ButterKnife.Setter<? super T, V> setter, V value) {
        int count = list.size();
        for (int i = 0; i < count; i++) {
            setter.set((android.view.View) list.get(i), value, i);
        }
    }

    @android.annotation.TargetApi(14)
    public static <T extends android.view.View, V> void apply(java.util.List<T> list, android.util.Property<? super T, V> setter, V value) {
        int count = list.size();
        for (int i = 0; i < count; i++) {
            setter.set(list.get(i), value);
        }
    }

    public static <T extends android.view.View> T findById(android.view.View view, int id) {
        return view.findViewById(id);
    }

    public static <T extends android.view.View> T findById(android.app.Activity activity, int id) {
        return activity.findViewById(id);
    }

    public static <T extends android.view.View> T findById(android.app.Dialog dialog, int id) {
        return dialog.findViewById(id);
    }
}
