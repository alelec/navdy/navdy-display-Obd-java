package butterknife;

final class ImmutableViewList<T extends android.view.View> extends java.util.AbstractList<T> implements java.util.RandomAccess {
    private final T[] views;

    ImmutableViewList(T[] views2) {
        this.views = views2;
    }

    public T get(int index) {
        return this.views[index];
    }

    public int size() {
        return this.views.length;
    }

    public boolean contains(java.lang.Object o) {
        for (android.view.View view : this.views) {
            if (view == o) {
                return true;
            }
        }
        return false;
    }
}
