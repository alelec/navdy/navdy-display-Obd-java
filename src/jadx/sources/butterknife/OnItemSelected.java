package butterknife;

@butterknife.internal.ListenerClass(callbacks = butterknife.OnItemSelected.Callback.class, setter = "setOnItemSelectedListener", targetType = "android.widget.AdapterView<?>", type = "android.widget.AdapterView.OnItemSelectedListener")
@java.lang.annotation.Target({java.lang.annotation.ElementType.METHOD})
@java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.CLASS)
public @interface OnItemSelected {

    public enum Callback {
        ITEM_SELECTED,
        NOTHING_SELECTED
    }

    butterknife.OnItemSelected.Callback callback() default butterknife.OnItemSelected.Callback.ITEM_SELECTED;

    int[] value() default {-1};
}
