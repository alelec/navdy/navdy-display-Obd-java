package butterknife;

@butterknife.internal.ListenerClass(method = {@butterknife.internal.ListenerMethod(defaultReturn = "false", name = "onLongClick", parameters = {"android.view.View"}, returnType = "boolean")}, setter = "setOnLongClickListener", targetType = "android.view.View", type = "android.view.View.OnLongClickListener")
@java.lang.annotation.Target({java.lang.annotation.ElementType.METHOD})
@java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.CLASS)
public @interface OnLongClick {
    int[] value() default {-1};
}
