package kotlin.text;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000&\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\f\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0011\u001a\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\nH\u0001\u001a\u0018\u0010\f\u001a\u00020\n2\u0006\u0010\r\u001a\u00020\u00022\u0006\u0010\u000b\u001a\u00020\nH\u0000\u001a\r\u0010\u000e\u001a\u00020\u000f*\u00020\u0002H\u0087\b\u001a\r\u0010\u0010\u001a\u00020\u000f*\u00020\u0002H\u0087\b\u001a\r\u0010\u0011\u001a\u00020\u000f*\u00020\u0002H\u0087\b\u001a\r\u0010\u0012\u001a\u00020\u000f*\u00020\u0002H\u0087\b\u001a\r\u0010\u0013\u001a\u00020\u000f*\u00020\u0002H\u0087\b\u001a\r\u0010\u0014\u001a\u00020\u000f*\u00020\u0002H\u0087\b\u001a\r\u0010\u0015\u001a\u00020\u000f*\u00020\u0002H\u0087\b\u001a\r\u0010\u0016\u001a\u00020\u000f*\u00020\u0002H\u0087\b\u001a\r\u0010\u0017\u001a\u00020\u000f*\u00020\u0002H\u0087\b\u001a\r\u0010\u0018\u001a\u00020\u000f*\u00020\u0002H\u0087\b\u001a\r\u0010\u0019\u001a\u00020\u000f*\u00020\u0002H\u0087\b\u001a\r\u0010\u001a\u001a\u00020\u000f*\u00020\u0002H\u0087\b\u001a\r\u0010\u001b\u001a\u00020\u000f*\u00020\u0002H\u0087\b\u001a\n\u0010\u001c\u001a\u00020\u000f*\u00020\u0002\u001a\r\u0010\u001d\u001a\u00020\u0002*\u00020\u0002H\u0087\b\u001a\r\u0010\u001e\u001a\u00020\u0002*\u00020\u0002H\u0087\b\u001a\r\u0010\u001f\u001a\u00020\u0002*\u00020\u0002H\u0087\b\"\u0015\u0010\u0000\u001a\u00020\u0001*\u00020\u00028F\u00a2\u0006\u0006\u001a\u0004\b\u0003\u0010\u0004\"\u0015\u0010\u0005\u001a\u00020\u0006*\u00020\u00028F\u00a2\u0006\u0006\u001a\u0004\b\u0007\u0010\b\u00a8\u0006 "}, d2 = {"category", "Lkotlin/text/CharCategory;", "", "getCategory", "(C)Lkotlin/text/CharCategory;", "directionality", "Lkotlin/text/CharDirectionality;", "getDirectionality", "(C)Lkotlin/text/CharDirectionality;", "checkRadix", "", "radix", "digitOf", "char", "isDefined", "", "isDigit", "isHighSurrogate", "isISOControl", "isIdentifierIgnorable", "isJavaIdentifierPart", "isJavaIdentifierStart", "isLetter", "isLetterOrDigit", "isLowSurrogate", "isLowerCase", "isTitleCase", "isUpperCase", "isWhitespace", "toLowerCase", "toTitleCase", "toUpperCase", "kotlin-stdlib"}, k = 5, mv = {1, 1, 6}, xi = 1, xs = "kotlin/text/CharsKt")
/* compiled from: CharJVM.kt */
class CharsKt__CharJVMKt {
    @kotlin.internal.InlineOnly
    private static final boolean isDefined(char $receiver) {
        return java.lang.Character.isDefined($receiver);
    }

    @kotlin.internal.InlineOnly
    private static final boolean isLetter(char $receiver) {
        return java.lang.Character.isLetter($receiver);
    }

    @kotlin.internal.InlineOnly
    private static final boolean isLetterOrDigit(char $receiver) {
        return java.lang.Character.isLetterOrDigit($receiver);
    }

    @kotlin.internal.InlineOnly
    private static final boolean isDigit(char $receiver) {
        return java.lang.Character.isDigit($receiver);
    }

    @kotlin.internal.InlineOnly
    private static final boolean isIdentifierIgnorable(char $receiver) {
        return java.lang.Character.isIdentifierIgnorable($receiver);
    }

    @kotlin.internal.InlineOnly
    private static final boolean isISOControl(char $receiver) {
        return java.lang.Character.isISOControl($receiver);
    }

    @kotlin.internal.InlineOnly
    private static final boolean isJavaIdentifierPart(char $receiver) {
        return java.lang.Character.isJavaIdentifierPart($receiver);
    }

    @kotlin.internal.InlineOnly
    private static final boolean isJavaIdentifierStart(char $receiver) {
        return java.lang.Character.isJavaIdentifierStart($receiver);
    }

    public static final boolean isWhitespace(char $receiver) {
        return java.lang.Character.isWhitespace($receiver) || java.lang.Character.isSpaceChar($receiver);
    }

    @kotlin.internal.InlineOnly
    private static final boolean isUpperCase(char $receiver) {
        return java.lang.Character.isUpperCase($receiver);
    }

    @kotlin.internal.InlineOnly
    private static final boolean isLowerCase(char $receiver) {
        return java.lang.Character.isLowerCase($receiver);
    }

    @kotlin.internal.InlineOnly
    private static final char toUpperCase(char $receiver) {
        return java.lang.Character.toUpperCase($receiver);
    }

    @kotlin.internal.InlineOnly
    private static final char toLowerCase(char $receiver) {
        return java.lang.Character.toLowerCase($receiver);
    }

    @kotlin.internal.InlineOnly
    private static final boolean isTitleCase(char $receiver) {
        return java.lang.Character.isTitleCase($receiver);
    }

    @kotlin.internal.InlineOnly
    private static final char toTitleCase(char $receiver) {
        return java.lang.Character.toTitleCase($receiver);
    }

    @org.jetbrains.annotations.NotNull
    public static final kotlin.text.CharCategory getCategory(char $receiver) {
        return kotlin.text.CharCategory.Companion.valueOf(java.lang.Character.getType($receiver));
    }

    @org.jetbrains.annotations.NotNull
    public static final kotlin.text.CharDirectionality getDirectionality(char $receiver) {
        return kotlin.text.CharDirectionality.Companion.valueOf(java.lang.Character.getDirectionality($receiver));
    }

    @kotlin.internal.InlineOnly
    private static final boolean isHighSurrogate(char $receiver) {
        return java.lang.Character.isHighSurrogate($receiver);
    }

    @kotlin.internal.InlineOnly
    private static final boolean isLowSurrogate(char $receiver) {
        return java.lang.Character.isLowSurrogate($receiver);
    }

    public static final int digitOf(char c, int radix) {
        return java.lang.Character.digit(c, radix);
    }

    @kotlin.PublishedApi
    public static final int checkRadix(int radix) {
        if (!(2 > radix || radix > 36)) {
            return radix;
        }
        throw new java.lang.IllegalArgumentException("radix " + radix + " was not in valid range " + new kotlin.ranges.IntRange(2, 36));
    }
}
