package kotlin.text;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u001e\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\bf\u0018\u00002\n\u0012\u0006\u0012\u0004\u0018\u00010\u00020\u0001J\u0013\u0010\u0003\u001a\u0004\u0018\u00010\u00022\u0006\u0010\u0004\u001a\u00020\u0005H\u00a6\u0002\u00a8\u0006\u0006"}, d2 = {"Lkotlin/text/MatchGroupCollection;", "", "Lkotlin/text/MatchGroup;", "get", "index", "", "kotlin-stdlib"}, k = 1, mv = {1, 1, 6})
/* compiled from: MatchResult.kt */
public interface MatchGroupCollection extends java.util.Collection<kotlin.text.MatchGroup>, kotlin.jvm.internal.markers.KMappedMarker {
    @org.jetbrains.annotations.Nullable
    kotlin.text.MatchGroup get(int i);
}
