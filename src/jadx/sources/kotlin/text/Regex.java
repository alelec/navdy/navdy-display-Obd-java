package kotlin.text;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\b\u0005\u0018\u0000 (2\u00020\u0001:\u0001(B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004B\u0017\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007B\u001d\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00060\t\u00a2\u0006\u0002\u0010\nB\u000f\b\u0001\u0012\u0006\u0010\u000b\u001a\u00020\f\u00a2\u0006\u0002\u0010\rJ\u000e\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0015J\u001a\u0010\u0016\u001a\u0004\u0018\u00010\u00172\u0006\u0010\u0014\u001a\u00020\u00152\b\b\u0002\u0010\u0018\u001a\u00020\u0019J\u001e\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u00170\u001b2\u0006\u0010\u0014\u001a\u00020\u00152\b\b\u0002\u0010\u0018\u001a\u00020\u0019J\u0010\u0010\u001c\u001a\u0004\u0018\u00010\u00172\u0006\u0010\u0014\u001a\u00020\u0015J\u0011\u0010\u001d\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0015H\u0086\u0004J\"\u0010\u001e\u001a\u00020\u00032\u0006\u0010\u0014\u001a\u00020\u00152\u0012\u0010\u001f\u001a\u000e\u0012\u0004\u0012\u00020\u0017\u0012\u0004\u0012\u00020\u00150 J\u0016\u0010\u001e\u001a\u00020\u00032\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010!\u001a\u00020\u0003J\u0016\u0010\"\u001a\u00020\u00032\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010!\u001a\u00020\u0003J\u001e\u0010#\u001a\b\u0012\u0004\u0012\u00020\u00030$2\u0006\u0010\u0014\u001a\u00020\u00152\b\b\u0002\u0010%\u001a\u00020\u0019J\u0006\u0010&\u001a\u00020\fJ\b\u0010'\u001a\u00020\u0003H\u0016R\u000e\u0010\u000b\u001a\u00020\fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0017\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00060\t\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0011\u0010\u0002\u001a\u00020\u00038F\u00a2\u0006\u0006\u001a\u0004\b\u0010\u0010\u0011\u00a8\u0006)"}, d2 = {"Lkotlin/text/Regex;", "", "pattern", "", "(Ljava/lang/String;)V", "option", "Lkotlin/text/RegexOption;", "(Ljava/lang/String;Lkotlin/text/RegexOption;)V", "options", "", "(Ljava/lang/String;Ljava/util/Set;)V", "nativePattern", "Ljava/util/regex/Pattern;", "(Ljava/util/regex/Pattern;)V", "getOptions", "()Ljava/util/Set;", "getPattern", "()Ljava/lang/String;", "containsMatchIn", "", "input", "", "find", "Lkotlin/text/MatchResult;", "startIndex", "", "findAll", "Lkotlin/sequences/Sequence;", "matchEntire", "matches", "replace", "transform", "Lkotlin/Function1;", "replacement", "replaceFirst", "split", "", "limit", "toPattern", "toString", "Companion", "kotlin-stdlib"}, k = 1, mv = {1, 1, 6})
/* compiled from: Regex.kt */
public final class Regex {
    public static final kotlin.text.Regex.Companion Companion = new kotlin.text.Regex.Companion(null);
    private final java.util.regex.Pattern nativePattern;
    @org.jetbrains.annotations.NotNull
    private final java.util.Set<kotlin.text.RegexOption> options;

    @kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0004H\u0002J\u000e\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u0007J\u000e\u0010\t\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u0007J\u000e\u0010\n\u001a\u00020\u000b2\u0006\u0010\b\u001a\u00020\u0007\u00a8\u0006\f"}, d2 = {"Lkotlin/text/Regex$Companion;", "", "()V", "ensureUnicodeCase", "", "flags", "escape", "", "literal", "escapeReplacement", "fromLiteral", "Lkotlin/text/Regex;", "kotlin-stdlib"}, k = 1, mv = {1, 1, 6})
    /* compiled from: Regex.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(kotlin.jvm.internal.DefaultConstructorMarker $constructor_marker) {
            this();
        }

        @org.jetbrains.annotations.NotNull
        public final kotlin.text.Regex fromLiteral(@org.jetbrains.annotations.NotNull java.lang.String literal) {
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(literal, "literal");
            return new kotlin.text.Regex(literal, kotlin.text.RegexOption.LITERAL);
        }

        @org.jetbrains.annotations.NotNull
        public final java.lang.String escape(@org.jetbrains.annotations.NotNull java.lang.String literal) {
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(literal, "literal");
            java.lang.String quote = java.util.regex.Pattern.quote(literal);
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(quote, "Pattern.quote(literal)");
            return quote;
        }

        @org.jetbrains.annotations.NotNull
        public final java.lang.String escapeReplacement(@org.jetbrains.annotations.NotNull java.lang.String literal) {
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(literal, "literal");
            java.lang.String quoteReplacement = java.util.regex.Matcher.quoteReplacement(literal);
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(quoteReplacement, "Matcher.quoteReplacement(literal)");
            return quoteReplacement;
        }

        /* access modifiers changed from: private */
        public final int ensureUnicodeCase(int flags) {
            return (flags & 2) != 0 ? flags | 64 : flags;
        }
    }

    @kotlin.PublishedApi
    public Regex(@org.jetbrains.annotations.NotNull java.util.regex.Pattern nativePattern2) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(nativePattern2, "nativePattern");
        this.nativePattern = nativePattern2;
        int flags = this.nativePattern.flags();
        java.util.EnumSet $receiver$iv = java.util.EnumSet.allOf(kotlin.text.RegexOption.class);
        kotlin.collections.CollectionsKt.retainAll((java.lang.Iterable<? extends T>) $receiver$iv, (kotlin.jvm.functions.Function1<? super T, java.lang.Boolean>) new kotlin.text.Regex$fromInt$$inlined$apply$lambda$1<java.lang.Object,java.lang.Boolean>(flags));
        java.util.Set<kotlin.text.RegexOption> unmodifiableSet = java.util.Collections.unmodifiableSet($receiver$iv);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(unmodifiableSet, "Collections.unmodifiable\u2026 == it.value }\n        })");
        this.options = unmodifiableSet;
    }

    public Regex(@org.jetbrains.annotations.NotNull java.lang.String pattern) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(pattern, "pattern");
        java.util.regex.Pattern compile = java.util.regex.Pattern.compile(pattern);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(compile, "Pattern.compile(pattern)");
        this(compile);
    }

    public Regex(@org.jetbrains.annotations.NotNull java.lang.String pattern, @org.jetbrains.annotations.NotNull kotlin.text.RegexOption option) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(pattern, "pattern");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(option, "option");
        java.util.regex.Pattern compile = java.util.regex.Pattern.compile(pattern, Companion.ensureUnicodeCase(option.getValue()));
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(compile, "Pattern.compile(pattern,\u2026nicodeCase(option.value))");
        this(compile);
    }

    public Regex(@org.jetbrains.annotations.NotNull java.lang.String pattern, @org.jetbrains.annotations.NotNull java.util.Set<? extends kotlin.text.RegexOption> options2) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(pattern, "pattern");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(options2, "options");
        java.util.regex.Pattern compile = java.util.regex.Pattern.compile(pattern, Companion.ensureUnicodeCase(kotlin.text.RegexKt.toInt(options2)));
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(compile, "Pattern.compile(pattern,\u2026odeCase(options.toInt()))");
        this(compile);
    }

    @org.jetbrains.annotations.NotNull
    public final java.lang.String getPattern() {
        java.lang.String pattern = this.nativePattern.pattern();
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(pattern, "nativePattern.pattern()");
        return pattern;
    }

    @org.jetbrains.annotations.NotNull
    public final java.util.Set<kotlin.text.RegexOption> getOptions() {
        return this.options;
    }

    public final boolean matches(@org.jetbrains.annotations.NotNull java.lang.CharSequence input) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(input, "input");
        return this.nativePattern.matcher(input).matches();
    }

    public final boolean containsMatchIn(@org.jetbrains.annotations.NotNull java.lang.CharSequence input) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(input, "input");
        return this.nativePattern.matcher(input).find();
    }

    @org.jetbrains.annotations.Nullable
    public static /* bridge */ /* synthetic */ kotlin.text.MatchResult find$default(kotlin.text.Regex regex, java.lang.CharSequence charSequence, int i, int i2, java.lang.Object obj) {
        if ((i2 & 2) != 0) {
            i = 0;
        }
        return regex.find(charSequence, i);
    }

    @org.jetbrains.annotations.Nullable
    public final kotlin.text.MatchResult find(@org.jetbrains.annotations.NotNull java.lang.CharSequence input, int startIndex) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(input, "input");
        return kotlin.text.RegexKt.findNext(this.nativePattern.matcher(input), startIndex, input);
    }

    @org.jetbrains.annotations.NotNull
    public static /* bridge */ /* synthetic */ kotlin.sequences.Sequence findAll$default(kotlin.text.Regex regex, java.lang.CharSequence charSequence, int i, int i2, java.lang.Object obj) {
        if ((i2 & 2) != 0) {
            i = 0;
        }
        return regex.findAll(charSequence, i);
    }

    @org.jetbrains.annotations.NotNull
    public final kotlin.sequences.Sequence<kotlin.text.MatchResult> findAll(@org.jetbrains.annotations.NotNull java.lang.CharSequence input, int startIndex) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(input, "input");
        return kotlin.sequences.SequencesKt.generateSequence((kotlin.jvm.functions.Function0<? extends T>) new kotlin.text.Regex$findAll$1<java.lang.Object>(this, input, startIndex), (kotlin.jvm.functions.Function1<? super T, ? extends T>) kotlin.text.Regex$findAll$2.INSTANCE);
    }

    @org.jetbrains.annotations.Nullable
    public final kotlin.text.MatchResult matchEntire(@org.jetbrains.annotations.NotNull java.lang.CharSequence input) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(input, "input");
        return kotlin.text.RegexKt.matchEntire(this.nativePattern.matcher(input), input);
    }

    @org.jetbrains.annotations.NotNull
    public final java.lang.String replace(@org.jetbrains.annotations.NotNull java.lang.CharSequence input, @org.jetbrains.annotations.NotNull java.lang.String replacement) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(input, "input");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(replacement, "replacement");
        java.lang.String replaceAll = this.nativePattern.matcher(input).replaceAll(replacement);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(replaceAll, "nativePattern.matcher(in\u2026).replaceAll(replacement)");
        return replaceAll;
    }

    @org.jetbrains.annotations.NotNull
    public final java.lang.String replace(@org.jetbrains.annotations.NotNull java.lang.CharSequence input, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super kotlin.text.MatchResult, ? extends java.lang.CharSequence> transform) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(input, "input");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(transform, "transform");
        kotlin.text.MatchResult match = find$default(this, input, 0, 2, null);
        if (match == null) {
            return input.toString();
        }
        int lastStart = 0;
        int length = input.length();
        java.lang.StringBuilder sb = new java.lang.StringBuilder(length);
        do {
            if (match == null) {
                kotlin.jvm.internal.Intrinsics.throwNpe();
            }
            kotlin.text.MatchResult foundMatch = match;
            sb.append(input, lastStart, foundMatch.getRange().getStart().intValue());
            sb.append((java.lang.CharSequence) transform.invoke(foundMatch));
            lastStart = foundMatch.getRange().getEndInclusive().intValue() + 1;
            match = foundMatch.next();
            if (lastStart >= length) {
                break;
            }
        } while (match != null);
        if (lastStart < length) {
            sb.append(input, lastStart, length);
        }
        java.lang.String sb2 = sb.toString();
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(sb2, "sb.toString()");
        return sb2;
    }

    @org.jetbrains.annotations.NotNull
    public final java.lang.String replaceFirst(@org.jetbrains.annotations.NotNull java.lang.CharSequence input, @org.jetbrains.annotations.NotNull java.lang.String replacement) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(input, "input");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(replacement, "replacement");
        java.lang.String replaceFirst = this.nativePattern.matcher(input).replaceFirst(replacement);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(replaceFirst, "nativePattern.matcher(in\u2026replaceFirst(replacement)");
        return replaceFirst;
    }

    @org.jetbrains.annotations.NotNull
    public static /* bridge */ /* synthetic */ java.util.List split$default(kotlin.text.Regex regex, java.lang.CharSequence charSequence, int i, int i2, java.lang.Object obj) {
        if ((i2 & 2) != 0) {
            i = 0;
        }
        return regex.split(charSequence, i);
    }

    @org.jetbrains.annotations.NotNull
    public final java.util.List<java.lang.String> split(@org.jetbrains.annotations.NotNull java.lang.CharSequence input, int limit) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(input, "input");
        if (!(limit >= 0)) {
            throw new java.lang.IllegalArgumentException(("Limit must be non-negative, but was " + limit + ".").toString());
        }
        java.util.regex.Pattern pattern = this.nativePattern;
        if (limit == 0) {
            limit = -1;
        }
        return kotlin.collections.ArraysKt.asList((T[]) (java.lang.Object[]) pattern.split(input, limit));
    }

    @org.jetbrains.annotations.NotNull
    public java.lang.String toString() {
        java.lang.String pattern = this.nativePattern.toString();
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(pattern, "nativePattern.toString()");
        return pattern;
    }

    @org.jetbrains.annotations.NotNull
    public final java.util.regex.Pattern toPattern() {
        return this.nativePattern;
    }
}
