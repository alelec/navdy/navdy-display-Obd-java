package kotlin.text;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0014\n\u0000\n\u0002\u0010\u000b\n\u0002\u0010\f\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0000\u001a\u001c\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u00022\b\b\u0002\u0010\u0004\u001a\u00020\u0001\u001a\n\u0010\u0005\u001a\u00020\u0001*\u00020\u0002\u001a\u0015\u0010\u0006\u001a\u00020\u0007*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0007H\u0087\n\u00a8\u0006\b"}, d2 = {"equals", "", "", "other", "ignoreCase", "isSurrogate", "plus", "", "kotlin-stdlib"}, k = 5, mv = {1, 1, 6}, xi = 1, xs = "kotlin/text/CharsKt")
/* compiled from: Char.kt */
class CharsKt__CharKt extends kotlin.text.CharsKt__CharJVMKt {
    @kotlin.internal.InlineOnly
    private static final java.lang.String plus(char $receiver, java.lang.String other) {
        return java.lang.String.valueOf($receiver) + other;
    }

    public static /* bridge */ /* synthetic */ boolean equals$default(char c, char c2, boolean z, int i, java.lang.Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        return kotlin.text.CharsKt.equals(c, c2, z);
    }

    public static final boolean equals(char $receiver, char other, boolean ignoreCase) {
        if ($receiver == other) {
            return true;
        }
        if (!ignoreCase) {
            return false;
        }
        if (java.lang.Character.toUpperCase($receiver) == java.lang.Character.toUpperCase(other) || java.lang.Character.toLowerCase($receiver) == java.lang.Character.toLowerCase(other)) {
            return true;
        }
        return false;
    }

    public static final boolean isSurrogate(char $receiver) {
        return 55296 <= $receiver && $receiver <= 57343;
    }
}
