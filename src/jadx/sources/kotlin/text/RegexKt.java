package kotlin.text;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000>\n\u0000\n\u0002\u0010\"\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\r\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u001c\n\u0000\u001a-\u0010\u0000\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0014\b\u0000\u0010\u0002\u0018\u0001*\u00020\u0003*\b\u0012\u0004\u0012\u0002H\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0082\b\u001a\u001e\u0010\u0007\u001a\u0004\u0018\u00010\b*\u00020\t2\u0006\u0010\n\u001a\u00020\u00062\u0006\u0010\u000b\u001a\u00020\fH\u0002\u001a\u0016\u0010\r\u001a\u0004\u0018\u00010\b*\u00020\t2\u0006\u0010\u000b\u001a\u00020\fH\u0002\u001a\f\u0010\u000e\u001a\u00020\u000f*\u00020\u0010H\u0002\u001a\u0014\u0010\u000e\u001a\u00020\u000f*\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0006H\u0002\u001a\u0012\u0010\u0012\u001a\u00020\u0006*\b\u0012\u0004\u0012\u00020\u00030\u0013H\u0002\u00a8\u0006\u0014"}, d2 = {"fromInt", "", "T", "Lkotlin/text/FlagEnum;", "", "value", "", "findNext", "Lkotlin/text/MatchResult;", "Ljava/util/regex/Matcher;", "from", "input", "", "matchEntire", "range", "Lkotlin/ranges/IntRange;", "Ljava/util/regex/MatchResult;", "groupIndex", "toInt", "", "kotlin-stdlib"}, k = 2, mv = {1, 1, 6})
/* compiled from: Regex.kt */
public final class RegexKt {
    /* access modifiers changed from: private */
    public static final int toInt(@org.jetbrains.annotations.NotNull java.lang.Iterable<? extends kotlin.text.FlagEnum> $receiver) {
        int accumulator$iv = 0;
        for (kotlin.text.FlagEnum value : $receiver) {
            accumulator$iv |= value.getValue();
        }
        return accumulator$iv;
    }

    private static final <T extends java.lang.Enum<T> & kotlin.text.FlagEnum> java.util.Set<T> fromInt(int value) {
        kotlin.jvm.internal.Intrinsics.reifiedOperationMarker(4, "T");
        java.util.EnumSet $receiver = java.util.EnumSet.allOf(java.lang.Enum.class);
        kotlin.collections.CollectionsKt.retainAll((java.lang.Iterable<? extends T>) $receiver, (kotlin.jvm.functions.Function1<? super T, java.lang.Boolean>) new kotlin.text.RegexKt$fromInt$$inlined$apply$lambda$1<java.lang.Object,java.lang.Boolean>(value));
        java.util.Set<T> unmodifiableSet = java.util.Collections.unmodifiableSet($receiver);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(unmodifiableSet, "Collections.unmodifiable\u2026 == it.value }\n        })");
        return unmodifiableSet;
    }

    /* access modifiers changed from: private */
    public static final kotlin.text.MatchResult findNext(@org.jetbrains.annotations.NotNull java.util.regex.Matcher $receiver, int from, java.lang.CharSequence input) {
        if (!$receiver.find(from)) {
            return null;
        }
        return new kotlin.text.MatcherMatchResult($receiver, input);
    }

    /* access modifiers changed from: private */
    public static final kotlin.text.MatchResult matchEntire(@org.jetbrains.annotations.NotNull java.util.regex.Matcher $receiver, java.lang.CharSequence input) {
        if (!$receiver.matches()) {
            return null;
        }
        return new kotlin.text.MatcherMatchResult($receiver, input);
    }

    /* access modifiers changed from: private */
    public static final kotlin.ranges.IntRange range(@org.jetbrains.annotations.NotNull java.util.regex.MatchResult $receiver) {
        return new kotlin.ranges.IntRange($receiver.start(), $receiver.end() - 1);
    }

    /* access modifiers changed from: private */
    public static final kotlin.ranges.IntRange range(@org.jetbrains.annotations.NotNull java.util.regex.MatchResult $receiver, int groupIndex) {
        return new kotlin.ranges.IntRange($receiver.start(groupIndex), $receiver.end(groupIndex) - 1);
    }
}
