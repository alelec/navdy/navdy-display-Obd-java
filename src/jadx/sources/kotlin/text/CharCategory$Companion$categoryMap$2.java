package kotlin.text;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010$\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001H\n\u00a2\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "", "", "Lkotlin/text/CharCategory;", "invoke"}, k = 3, mv = {1, 1, 6})
/* compiled from: CharCategory.kt */
final class CharCategory$Companion$categoryMap$2 extends kotlin.jvm.internal.Lambda implements kotlin.jvm.functions.Function0<java.util.Map<java.lang.Integer, ? extends kotlin.text.CharCategory>> {
    public static final kotlin.text.CharCategory$Companion$categoryMap$2 INSTANCE = new kotlin.text.CharCategory$Companion$categoryMap$2();

    CharCategory$Companion$categoryMap$2() {
        super(0);
    }

    @org.jetbrains.annotations.NotNull
    public final java.util.Map<java.lang.Integer, kotlin.text.CharCategory> invoke() {
        java.lang.Object[] $receiver$iv = (java.lang.Object[]) kotlin.text.CharCategory.values();
        java.util.Map destination$iv$iv = new java.util.LinkedHashMap(kotlin.ranges.RangesKt.coerceAtLeast(kotlin.collections.MapsKt.mapCapacity($receiver$iv.length), 16));
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= $receiver$iv.length) {
                return destination$iv$iv;
            }
            java.lang.Object element$iv$iv = $receiver$iv[i2];
            destination$iv$iv.put(java.lang.Integer.valueOf(((kotlin.text.CharCategory) element$iv$iv).getValue()), element$iv$iv);
            i = i2 + 1;
        }
    }
}
