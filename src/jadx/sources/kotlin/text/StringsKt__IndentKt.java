package kotlin.text;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u001e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\b\u000b\u001a!\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0002H\u0002\u00a2\u0006\u0002\b\u0004\u001a\u0011\u0010\u0005\u001a\u00020\u0006*\u00020\u0002H\u0002\u00a2\u0006\u0002\b\u0007\u001a\u0014\u0010\b\u001a\u00020\u0002*\u00020\u00022\b\b\u0002\u0010\u0003\u001a\u00020\u0002\u001aJ\u0010\t\u001a\u00020\u0002*\b\u0012\u0004\u0012\u00020\u00020\n2\u0006\u0010\u000b\u001a\u00020\u00062\u0012\u0010\f\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00020\u00012\u0014\u0010\r\u001a\u0010\u0012\u0004\u0012\u00020\u0002\u0012\u0006\u0012\u0004\u0018\u00010\u00020\u0001H\u0082\b\u00a2\u0006\u0002\b\u000e\u001a\u0014\u0010\u000f\u001a\u00020\u0002*\u00020\u00022\b\b\u0002\u0010\u0010\u001a\u00020\u0002\u001a\u001e\u0010\u0011\u001a\u00020\u0002*\u00020\u00022\b\b\u0002\u0010\u0010\u001a\u00020\u00022\b\b\u0002\u0010\u0012\u001a\u00020\u0002\u001a\n\u0010\u0013\u001a\u00020\u0002*\u00020\u0002\u001a\u0014\u0010\u0014\u001a\u00020\u0002*\u00020\u00022\b\b\u0002\u0010\u0012\u001a\u00020\u0002\u00a8\u0006\u0015"}, d2 = {"getIndentFunction", "Lkotlin/Function1;", "", "indent", "getIndentFunction$StringsKt__IndentKt", "indentWidth", "", "indentWidth$StringsKt__IndentKt", "prependIndent", "reindent", "", "resultSizeEstimate", "indentAddFunction", "indentCutFunction", "reindent$StringsKt__IndentKt", "replaceIndent", "newIndent", "replaceIndentByMargin", "marginPrefix", "trimIndent", "trimMargin", "kotlin-stdlib"}, k = 5, mv = {1, 1, 6}, xi = 1, xs = "kotlin/text/StringsKt")
/* compiled from: Indent.kt */
class StringsKt__IndentKt {
    @org.jetbrains.annotations.NotNull
    public static /* bridge */ /* synthetic */ java.lang.String trimMargin$default(java.lang.String str, java.lang.String str2, int i, java.lang.Object obj) {
        if ((i & 1) != 0) {
            str2 = "|";
        }
        return kotlin.text.StringsKt.trimMargin(str, str2);
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.String trimMargin(@org.jetbrains.annotations.NotNull java.lang.String $receiver, @org.jetbrains.annotations.NotNull java.lang.String marginPrefix) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(marginPrefix, "marginPrefix");
        return kotlin.text.StringsKt.replaceIndentByMargin($receiver, "", marginPrefix);
    }

    @org.jetbrains.annotations.NotNull
    public static /* bridge */ /* synthetic */ java.lang.String replaceIndentByMargin$default(java.lang.String str, java.lang.String str2, java.lang.String str3, int i, java.lang.Object obj) {
        if ((i & 1) != 0) {
            str2 = "";
        }
        if ((i & 2) != 0) {
            str3 = "|";
        }
        return kotlin.text.StringsKt.replaceIndentByMargin(str, str2, str3);
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.String replaceIndentByMargin(@org.jetbrains.annotations.NotNull java.lang.String $receiver, @org.jetbrains.annotations.NotNull java.lang.String newIndent, @org.jetbrains.annotations.NotNull java.lang.String marginPrefix) {
        int firstNonWhitespaceIndex;
        java.lang.String str;
        java.lang.String it$iv$iv$iv;
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(newIndent, "newIndent");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(marginPrefix, "marginPrefix");
        if (!(!kotlin.text.StringsKt.isBlank(marginPrefix))) {
            throw new java.lang.IllegalArgumentException("marginPrefix must be non-blank string.".toString());
        }
        java.util.List lines = kotlin.text.StringsKt.lines($receiver);
        int length = $receiver.length() + (newIndent.length() * lines.size());
        kotlin.jvm.functions.Function1 indentAddFunction$iv = getIndentFunction$StringsKt__IndentKt(newIndent);
        int lastIndex$iv = kotlin.collections.CollectionsKt.getLastIndex(lines);
        java.lang.Iterable<java.lang.String> $receiver$iv$iv = lines;
        java.util.Collection destination$iv$iv$iv = new java.util.ArrayList();
        int index$iv$iv$iv$iv = 0;
        for (java.lang.String line : $receiver$iv$iv) {
            int index$iv$iv$iv$iv2 = index$iv$iv$iv$iv + 1;
            int index$iv = index$iv$iv$iv$iv;
            if ((index$iv == 0 || index$iv == lastIndex$iv) && kotlin.text.StringsKt.isBlank(line)) {
                it$iv$iv$iv = null;
            } else {
                java.lang.CharSequence $receiver$iv = line;
                int index$iv2 = 0;
                int length2 = $receiver$iv.length() - 1;
                if (0 <= length2) {
                    while (true) {
                        if (!(!kotlin.text.CharsKt.isWhitespace($receiver$iv.charAt(index$iv2)))) {
                            if (index$iv2 == length2) {
                                break;
                            }
                            index$iv2++;
                        } else {
                            firstNonWhitespaceIndex = index$iv2;
                            break;
                        }
                    }
                }
                firstNonWhitespaceIndex = -1;
                if (firstNonWhitespaceIndex == -1) {
                    str = null;
                } else if (kotlin.text.StringsKt.startsWith$default(line, marginPrefix, firstNonWhitespaceIndex, false, 4, null)) {
                    int length3 = marginPrefix.length() + firstNonWhitespaceIndex;
                    if (line == null) {
                        throw new kotlin.TypeCastException("null cannot be cast to non-null type java.lang.String");
                    }
                    str = line.substring(length3);
                    kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(str, "(this as java.lang.String).substring(startIndex)");
                } else {
                    str = null;
                }
                if (str != null) {
                    java.lang.String str2 = (java.lang.String) indentAddFunction$iv.invoke(str);
                    if (str2 != null) {
                        it$iv$iv$iv = str2;
                    }
                }
                it$iv$iv$iv = line;
            }
            if (it$iv$iv$iv != null) {
                destination$iv$iv$iv.add(it$iv$iv$iv);
            }
            index$iv$iv$iv$iv = index$iv$iv$iv$iv2;
        }
        java.lang.String sb = ((java.lang.StringBuilder) kotlin.collections.CollectionsKt.joinTo$default((java.util.List) destination$iv$iv$iv, new java.lang.StringBuilder(length), "\n", null, null, 0, null, null, 124, null)).toString();
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(sb, "mapIndexedNotNull { inde\u2026\"\\n\")\n        .toString()");
        return sb;
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.String trimIndent(@org.jetbrains.annotations.NotNull java.lang.String $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        return kotlin.text.StringsKt.replaceIndent($receiver, "");
    }

    @org.jetbrains.annotations.NotNull
    public static /* bridge */ /* synthetic */ java.lang.String replaceIndent$default(java.lang.String str, java.lang.String str2, int i, java.lang.Object obj) {
        if ((i & 1) != 0) {
            str2 = "";
        }
        return kotlin.text.StringsKt.replaceIndent(str, str2);
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.String replaceIndent(@org.jetbrains.annotations.NotNull java.lang.String $receiver, @org.jetbrains.annotations.NotNull java.lang.String newIndent) {
        java.lang.String it$iv$iv$iv;
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(newIndent, "newIndent");
        java.util.List lines = kotlin.text.StringsKt.lines($receiver);
        java.lang.Iterable iterable = lines;
        java.util.Collection destination$iv$iv = new java.util.ArrayList();
        for (java.lang.Object element$iv$iv : iterable) {
            if (!kotlin.text.StringsKt.isBlank((java.lang.String) element$iv$iv)) {
                destination$iv$iv.add(element$iv$iv);
            }
        }
        java.lang.Iterable<java.lang.String> iterable2 = (java.util.List) destination$iv$iv;
        java.util.Collection destination$iv$iv2 = new java.util.ArrayList(kotlin.collections.CollectionsKt.collectionSizeOrDefault(iterable2, 10));
        for (java.lang.String indentWidth$StringsKt__IndentKt : iterable2) {
            destination$iv$iv2.add(java.lang.Integer.valueOf(indentWidth$StringsKt__IndentKt(indentWidth$StringsKt__IndentKt)));
        }
        java.lang.Integer num = (java.lang.Integer) kotlin.collections.CollectionsKt.min((java.lang.Iterable<? extends T>) (java.util.List) destination$iv$iv2);
        int minCommonIndent = num != null ? num.intValue() : 0;
        int length = $receiver.length() + (newIndent.length() * lines.size());
        kotlin.jvm.functions.Function1 indentAddFunction$iv = getIndentFunction$StringsKt__IndentKt(newIndent);
        int lastIndex$iv = kotlin.collections.CollectionsKt.getLastIndex(lines);
        java.lang.Iterable<java.lang.String> $receiver$iv$iv = lines;
        java.util.Collection destination$iv$iv$iv = new java.util.ArrayList();
        int index$iv$iv$iv$iv = 0;
        for (java.lang.String line : $receiver$iv$iv) {
            int index$iv$iv$iv$iv2 = index$iv$iv$iv$iv + 1;
            int index$iv = index$iv$iv$iv$iv;
            if ((index$iv == 0 || index$iv == lastIndex$iv) && kotlin.text.StringsKt.isBlank(line)) {
                it$iv$iv$iv = null;
            } else {
                java.lang.String drop = kotlin.text.StringsKt.drop(line, minCommonIndent);
                if (drop != null) {
                    java.lang.String str = (java.lang.String) indentAddFunction$iv.invoke(drop);
                    if (str != null) {
                        it$iv$iv$iv = str;
                    }
                }
                it$iv$iv$iv = line;
            }
            if (it$iv$iv$iv != null) {
                destination$iv$iv$iv.add(it$iv$iv$iv);
            }
            index$iv$iv$iv$iv = index$iv$iv$iv$iv2;
        }
        java.lang.String sb = ((java.lang.StringBuilder) kotlin.collections.CollectionsKt.joinTo$default((java.util.List) destination$iv$iv$iv, new java.lang.StringBuilder(length), "\n", null, null, 0, null, null, 124, null)).toString();
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(sb, "mapIndexedNotNull { inde\u2026\"\\n\")\n        .toString()");
        return sb;
    }

    @org.jetbrains.annotations.NotNull
    public static /* bridge */ /* synthetic */ java.lang.String prependIndent$default(java.lang.String str, java.lang.String str2, int i, java.lang.Object obj) {
        if ((i & 1) != 0) {
            str2 = "    ";
        }
        return kotlin.text.StringsKt.prependIndent(str, str2);
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.String prependIndent(@org.jetbrains.annotations.NotNull java.lang.String $receiver, @org.jetbrains.annotations.NotNull java.lang.String indent) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(indent, "indent");
        return kotlin.sequences.SequencesKt.joinToString$default(kotlin.sequences.SequencesKt.map(kotlin.text.StringsKt.lineSequence($receiver), new kotlin.text.StringsKt__IndentKt$prependIndent$1(indent)), "\n", null, null, 0, null, null, 62, null);
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x001e  */
    private static final int indentWidth$StringsKt__IndentKt(@org.jetbrains.annotations.NotNull java.lang.String $receiver) {
        int it;
        java.lang.CharSequence $receiver$iv = $receiver;
        int length = $receiver$iv.length() - 1;
        if (0 <= length) {
            int i = 0;
            while (true) {
                if (!(!kotlin.text.CharsKt.isWhitespace($receiver$iv.charAt(i)))) {
                    if (i == length) {
                        break;
                    }
                    i++;
                } else {
                    it = i;
                    break;
                }
            }
            return it != -1 ? $receiver.length() : it;
        }
        it = -1;
        if (it != -1) {
        }
    }

    private static final kotlin.jvm.functions.Function1<java.lang.String, java.lang.String> getIndentFunction$StringsKt__IndentKt(java.lang.String indent) {
        if (indent.length() == 0) {
            return kotlin.text.StringsKt__IndentKt$getIndentFunction$1.INSTANCE;
        }
        return new kotlin.text.StringsKt__IndentKt$getIndentFunction$2<>(indent);
    }

    private static final java.lang.String reindent$StringsKt__IndentKt(@org.jetbrains.annotations.NotNull java.util.List<java.lang.String> $receiver, int resultSizeEstimate, kotlin.jvm.functions.Function1<? super java.lang.String, java.lang.String> indentAddFunction, kotlin.jvm.functions.Function1<? super java.lang.String, java.lang.String> indentCutFunction) {
        java.lang.String it$iv$iv;
        int lastIndex = kotlin.collections.CollectionsKt.getLastIndex($receiver);
        java.lang.Iterable<java.lang.String> $receiver$iv = $receiver;
        java.util.Collection destination$iv$iv = new java.util.ArrayList();
        int index$iv$iv$iv = 0;
        for (java.lang.String str : $receiver$iv) {
            int index$iv$iv$iv2 = index$iv$iv$iv + 1;
            int index = index$iv$iv$iv;
            if ((index == 0 || index == lastIndex) && kotlin.text.StringsKt.isBlank(str)) {
                it$iv$iv = null;
            } else {
                java.lang.String str2 = (java.lang.String) indentCutFunction.invoke(str);
                if (str2 != null) {
                    java.lang.String str3 = (java.lang.String) indentAddFunction.invoke(str2);
                    if (str3 != null) {
                        it$iv$iv = str3;
                    }
                }
                it$iv$iv = str;
            }
            if (it$iv$iv != null) {
                destination$iv$iv.add(it$iv$iv);
            }
            index$iv$iv$iv = index$iv$iv$iv2;
        }
        java.lang.String sb = ((java.lang.StringBuilder) kotlin.collections.CollectionsKt.joinTo$default((java.util.List) destination$iv$iv, new java.lang.StringBuilder(resultSizeEstimate), "\n", null, null, 0, null, null, 124, null)).toString();
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(sb, "mapIndexedNotNull { inde\u2026\"\\n\")\n        .toString()");
        return sb;
    }
}
