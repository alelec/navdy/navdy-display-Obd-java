package kotlin.text;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\b\u0002"}, d2 = {"<anonymous>", "Lkotlin/collections/CharIterator;", "invoke"}, k = 3, mv = {1, 1, 6})
/* compiled from: _Strings.kt */
final class StringsKt___StringsKt$withIndex$1 extends kotlin.jvm.internal.Lambda implements kotlin.jvm.functions.Function0<kotlin.collections.CharIterator> {
    final /* synthetic */ java.lang.CharSequence receiver$0;

    StringsKt___StringsKt$withIndex$1(java.lang.CharSequence charSequence) {
        this.receiver$0 = charSequence;
        super(0);
    }

    @org.jetbrains.annotations.NotNull
    public final kotlin.collections.CharIterator invoke() {
        return kotlin.text.StringsKt.iterator(this.receiver$0);
    }
}
