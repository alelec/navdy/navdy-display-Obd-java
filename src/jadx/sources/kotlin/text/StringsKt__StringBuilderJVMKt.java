package kotlin.text;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000J\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\f\n\u0002\u0010\r\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\u0010\u0005\n\u0002\u0010\u0019\n\u0002\u0010\u0006\n\u0002\u0010\u0007\n\u0002\u0010\b\n\u0002\u0010\t\n\u0002\u0010\n\n\u0002\u0010\u000e\n\u0000\u001a\u0012\u0010\u0000\u001a\u00060\u0001j\u0002`\u0002*\u00060\u0001j\u0002`\u0002\u001a\u001d\u0010\u0000\u001a\u00060\u0001j\u0002`\u0002*\u00060\u0001j\u0002`\u00022\u0006\u0010\u0003\u001a\u00020\u0004H\u0087\b\u001a\u001f\u0010\u0000\u001a\u00060\u0001j\u0002`\u0002*\u00060\u0001j\u0002`\u00022\b\u0010\u0003\u001a\u0004\u0018\u00010\u0005H\u0087\b\u001a\u0012\u0010\u0000\u001a\u00060\u0006j\u0002`\u0007*\u00060\u0006j\u0002`\u0007\u001a\u001f\u0010\u0000\u001a\u00060\u0006j\u0002`\u0007*\u00060\u0006j\u0002`\u00072\b\u0010\u0003\u001a\u0004\u0018\u00010\bH\u0087\b\u001a\u001f\u0010\u0000\u001a\u00060\u0006j\u0002`\u0007*\u00060\u0006j\u0002`\u00072\b\u0010\u0003\u001a\u0004\u0018\u00010\tH\u0087\b\u001a\u001d\u0010\u0000\u001a\u00060\u0006j\u0002`\u0007*\u00060\u0006j\u0002`\u00072\u0006\u0010\u0003\u001a\u00020\nH\u0087\b\u001a\u001d\u0010\u0000\u001a\u00060\u0006j\u0002`\u0007*\u00060\u0006j\u0002`\u00072\u0006\u0010\u0003\u001a\u00020\u000bH\u0087\b\u001a\u001d\u0010\u0000\u001a\u00060\u0006j\u0002`\u0007*\u00060\u0006j\u0002`\u00072\u0006\u0010\u0003\u001a\u00020\u0004H\u0087\b\u001a\u001d\u0010\u0000\u001a\u00060\u0006j\u0002`\u0007*\u00060\u0006j\u0002`\u00072\u0006\u0010\u0003\u001a\u00020\fH\u0087\b\u001a\u001f\u0010\u0000\u001a\u00060\u0006j\u0002`\u0007*\u00060\u0006j\u0002`\u00072\b\u0010\u0003\u001a\u0004\u0018\u00010\u0005H\u0087\b\u001a\u001d\u0010\u0000\u001a\u00060\u0006j\u0002`\u0007*\u00060\u0006j\u0002`\u00072\u0006\u0010\u0003\u001a\u00020\rH\u0087\b\u001a\u001d\u0010\u0000\u001a\u00060\u0006j\u0002`\u0007*\u00060\u0006j\u0002`\u00072\u0006\u0010\u0003\u001a\u00020\u000eH\u0087\b\u001a\u001d\u0010\u0000\u001a\u00060\u0006j\u0002`\u0007*\u00060\u0006j\u0002`\u00072\u0006\u0010\u0003\u001a\u00020\u000fH\u0087\b\u001a\u001d\u0010\u0000\u001a\u00060\u0006j\u0002`\u0007*\u00060\u0006j\u0002`\u00072\u0006\u0010\u0003\u001a\u00020\u0010H\u0087\b\u001a\u001d\u0010\u0000\u001a\u00060\u0006j\u0002`\u0007*\u00060\u0006j\u0002`\u00072\u0006\u0010\u0003\u001a\u00020\u0011H\u0087\b\u001a\u001f\u0010\u0000\u001a\u00060\u0006j\u0002`\u0007*\u00060\u0006j\u0002`\u00072\b\u0010\u0003\u001a\u0004\u0018\u00010\u0012H\u0087\b\u001a%\u0010\u0000\u001a\u00060\u0006j\u0002`\u0007*\u00060\u0006j\u0002`\u00072\u000e\u0010\u0003\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u0007H\u0087\b\u00a8\u0006\u0013"}, d2 = {"appendln", "Ljava/lang/Appendable;", "Lkotlin/text/Appendable;", "value", "", "", "Ljava/lang/StringBuilder;", "Lkotlin/text/StringBuilder;", "Ljava/lang/StringBuffer;", "", "", "", "", "", "", "", "", "", "", "kotlin-stdlib"}, k = 5, mv = {1, 1, 6}, xi = 1, xs = "kotlin/text/StringsKt")
/* compiled from: StringBuilderJVM.kt */
class StringsKt__StringBuilderJVMKt extends kotlin.text.StringsKt__RegexExtensionsKt {
    @org.jetbrains.annotations.NotNull
    public static final java.lang.Appendable appendln(@org.jetbrains.annotations.NotNull java.lang.Appendable $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        java.lang.Appendable append = $receiver.append(kotlin.text.SystemProperties.LINE_SEPARATOR);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(append, "append(SystemProperties.LINE_SEPARATOR)");
        return append;
    }

    @kotlin.internal.InlineOnly
    private static final java.lang.Appendable appendln(@org.jetbrains.annotations.NotNull java.lang.Appendable $receiver, java.lang.CharSequence value) {
        return kotlin.text.StringsKt.appendln($receiver.append(value));
    }

    @kotlin.internal.InlineOnly
    private static final java.lang.Appendable appendln(@org.jetbrains.annotations.NotNull java.lang.Appendable $receiver, char value) {
        return kotlin.text.StringsKt.appendln($receiver.append(value));
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.StringBuilder appendln(@org.jetbrains.annotations.NotNull java.lang.StringBuilder $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        java.lang.StringBuilder append = $receiver.append(kotlin.text.SystemProperties.LINE_SEPARATOR);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(append, "append(SystemProperties.LINE_SEPARATOR)");
        return append;
    }

    @kotlin.internal.InlineOnly
    private static final java.lang.StringBuilder appendln(@org.jetbrains.annotations.NotNull java.lang.StringBuilder $receiver, java.lang.StringBuffer value) {
        return kotlin.text.StringsKt.appendln($receiver.append(value));
    }

    @kotlin.internal.InlineOnly
    private static final java.lang.StringBuilder appendln(@org.jetbrains.annotations.NotNull java.lang.StringBuilder $receiver, java.lang.CharSequence value) {
        return kotlin.text.StringsKt.appendln($receiver.append(value));
    }

    @kotlin.internal.InlineOnly
    private static final java.lang.StringBuilder appendln(@org.jetbrains.annotations.NotNull java.lang.StringBuilder $receiver, java.lang.String value) {
        return kotlin.text.StringsKt.appendln($receiver.append(value));
    }

    @kotlin.internal.InlineOnly
    private static final java.lang.StringBuilder appendln(@org.jetbrains.annotations.NotNull java.lang.StringBuilder $receiver, java.lang.Object value) {
        return kotlin.text.StringsKt.appendln($receiver.append(value));
    }

    @kotlin.internal.InlineOnly
    private static final java.lang.StringBuilder appendln(@org.jetbrains.annotations.NotNull java.lang.StringBuilder $receiver, java.lang.StringBuilder value) {
        return kotlin.text.StringsKt.appendln($receiver.append(value));
    }

    @kotlin.internal.InlineOnly
    private static final java.lang.StringBuilder appendln(@org.jetbrains.annotations.NotNull java.lang.StringBuilder $receiver, char[] value) {
        return kotlin.text.StringsKt.appendln($receiver.append(value));
    }

    @kotlin.internal.InlineOnly
    private static final java.lang.StringBuilder appendln(@org.jetbrains.annotations.NotNull java.lang.StringBuilder $receiver, char value) {
        return kotlin.text.StringsKt.appendln($receiver.append(value));
    }

    @kotlin.internal.InlineOnly
    private static final java.lang.StringBuilder appendln(@org.jetbrains.annotations.NotNull java.lang.StringBuilder $receiver, boolean value) {
        return kotlin.text.StringsKt.appendln($receiver.append(value));
    }

    @kotlin.internal.InlineOnly
    private static final java.lang.StringBuilder appendln(@org.jetbrains.annotations.NotNull java.lang.StringBuilder $receiver, int value) {
        return kotlin.text.StringsKt.appendln($receiver.append(value));
    }

    @kotlin.internal.InlineOnly
    private static final java.lang.StringBuilder appendln(@org.jetbrains.annotations.NotNull java.lang.StringBuilder $receiver, short value) {
        return kotlin.text.StringsKt.appendln($receiver.append(value));
    }

    @kotlin.internal.InlineOnly
    private static final java.lang.StringBuilder appendln(@org.jetbrains.annotations.NotNull java.lang.StringBuilder $receiver, byte value) {
        return kotlin.text.StringsKt.appendln($receiver.append(value));
    }

    @kotlin.internal.InlineOnly
    private static final java.lang.StringBuilder appendln(@org.jetbrains.annotations.NotNull java.lang.StringBuilder $receiver, long value) {
        return kotlin.text.StringsKt.appendln($receiver.append(value));
    }

    @kotlin.internal.InlineOnly
    private static final java.lang.StringBuilder appendln(@org.jetbrains.annotations.NotNull java.lang.StringBuilder $receiver, float value) {
        return kotlin.text.StringsKt.appendln($receiver.append(value));
    }

    @kotlin.internal.InlineOnly
    private static final java.lang.StringBuilder appendln(@org.jetbrains.annotations.NotNull java.lang.StringBuilder $receiver, double value) {
        return kotlin.text.StringsKt.appendln($receiver.append(value));
    }
}
