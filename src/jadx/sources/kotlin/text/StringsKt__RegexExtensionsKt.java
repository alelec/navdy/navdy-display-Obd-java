package kotlin.text;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u001c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\b\u0002\u001a\r\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\u0087\b\u001a\r\u0010\u0000\u001a\u00020\u0001*\u00020\u0003H\u0087\b\u001a\u001b\u0010\u0000\u001a\u00020\u0001*\u00020\u00032\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005H\u0087\b\u001a\u0015\u0010\u0000\u001a\u00020\u0001*\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u0006H\u0087\b\u00a8\u0006\b"}, d2 = {"toRegex", "Lkotlin/text/Regex;", "Ljava/util/regex/Pattern;", "", "options", "", "Lkotlin/text/RegexOption;", "option", "kotlin-stdlib"}, k = 5, mv = {1, 1, 6}, xi = 1, xs = "kotlin/text/StringsKt")
/* compiled from: RegexExtensions.kt */
class StringsKt__RegexExtensionsKt extends kotlin.text.StringsKt__IndentKt {
    @kotlin.internal.InlineOnly
    private static final kotlin.text.Regex toRegex(@org.jetbrains.annotations.NotNull java.lang.String $receiver) {
        return new kotlin.text.Regex($receiver);
    }

    @kotlin.internal.InlineOnly
    private static final kotlin.text.Regex toRegex(@org.jetbrains.annotations.NotNull java.lang.String $receiver, kotlin.text.RegexOption option) {
        return new kotlin.text.Regex($receiver, option);
    }

    @kotlin.internal.InlineOnly
    private static final kotlin.text.Regex toRegex(@org.jetbrains.annotations.NotNull java.lang.String $receiver, java.util.Set<? extends kotlin.text.RegexOption> options) {
        return new kotlin.text.Regex($receiver, options);
    }

    @kotlin.internal.InlineOnly
    private static final kotlin.text.Regex toRegex(@org.jetbrains.annotations.NotNull java.util.regex.Pattern $receiver) {
        return new kotlin.text.Regex($receiver);
    }
}
