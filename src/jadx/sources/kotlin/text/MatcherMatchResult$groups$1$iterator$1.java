package kotlin.text;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\u0010\u0000\u001a\u0004\u0018\u00010\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "Lkotlin/text/MatchGroup;", "it", "", "invoke"}, k = 3, mv = {1, 1, 6})
/* compiled from: Regex.kt */
final class MatcherMatchResult$groups$1$iterator$1 extends kotlin.jvm.internal.Lambda implements kotlin.jvm.functions.Function1<java.lang.Integer, kotlin.text.MatchGroup> {
    final /* synthetic */ kotlin.text.MatcherMatchResult$groups$1 this$0;

    MatcherMatchResult$groups$1$iterator$1(kotlin.text.MatcherMatchResult$groups$1 matcherMatchResult$groups$1) {
        this.this$0 = matcherMatchResult$groups$1;
        super(1);
    }

    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        return invoke(((java.lang.Number) obj).intValue());
    }

    @org.jetbrains.annotations.Nullable
    public final kotlin.text.MatchGroup invoke(int it) {
        return this.this$0.get(it);
    }
}
