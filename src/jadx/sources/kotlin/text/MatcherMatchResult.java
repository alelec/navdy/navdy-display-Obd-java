package kotlin.text;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\n\u0010\u001b\u001a\u0004\u0018\u00010\u0001H\u0016R\u001a\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\b\n\u0010\u000bR\u0016\u0010\f\u001a\n\u0012\u0004\u0012\u00020\t\u0018\u00010\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\u00020\u000eX\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0011\u001a\n \u0013*\u0004\u0018\u00010\u00120\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0014\u001a\u00020\u00158VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0016\u0010\u0017R\u0014\u0010\u0018\u001a\u00020\t8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0019\u0010\u001a\u00a8\u0006\u001c"}, d2 = {"Lkotlin/text/MatcherMatchResult;", "Lkotlin/text/MatchResult;", "matcher", "Ljava/util/regex/Matcher;", "input", "", "(Ljava/util/regex/Matcher;Ljava/lang/CharSequence;)V", "groupValues", "", "", "getGroupValues", "()Ljava/util/List;", "groupValues_", "groups", "Lkotlin/text/MatchGroupCollection;", "getGroups", "()Lkotlin/text/MatchGroupCollection;", "matchResult", "Ljava/util/regex/MatchResult;", "kotlin.jvm.PlatformType", "range", "Lkotlin/ranges/IntRange;", "getRange", "()Lkotlin/ranges/IntRange;", "value", "getValue", "()Ljava/lang/String;", "next", "kotlin-stdlib"}, k = 1, mv = {1, 1, 6})
/* compiled from: Regex.kt */
final class MatcherMatchResult implements kotlin.text.MatchResult {
    private java.util.List<java.lang.String> groupValues_;
    @org.jetbrains.annotations.NotNull
    private final kotlin.text.MatchGroupCollection groups = new kotlin.text.MatcherMatchResult$groups$1(this);
    private final java.lang.CharSequence input;
    /* access modifiers changed from: private */
    public final java.util.regex.MatchResult matchResult = this.matcher.toMatchResult();
    private final java.util.regex.Matcher matcher;

    public MatcherMatchResult(@org.jetbrains.annotations.NotNull java.util.regex.Matcher matcher2, @org.jetbrains.annotations.NotNull java.lang.CharSequence input2) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(matcher2, "matcher");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(input2, "input");
        this.matcher = matcher2;
        this.input = input2;
    }

    @org.jetbrains.annotations.NotNull
    public kotlin.text.MatchResult.Destructured getDestructured() {
        return kotlin.text.MatchResult.DefaultImpls.getDestructured(this);
    }

    @org.jetbrains.annotations.NotNull
    public kotlin.ranges.IntRange getRange() {
        return kotlin.text.RegexKt.range(this.matchResult);
    }

    @org.jetbrains.annotations.NotNull
    public java.lang.String getValue() {
        java.lang.String group = this.matchResult.group();
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(group, "matchResult.group()");
        return group;
    }

    @org.jetbrains.annotations.NotNull
    public kotlin.text.MatchGroupCollection getGroups() {
        return this.groups;
    }

    @org.jetbrains.annotations.NotNull
    public java.util.List<java.lang.String> getGroupValues() {
        if (this.groupValues_ == null) {
            this.groupValues_ = new kotlin.text.MatcherMatchResult$groupValues$1(this);
        }
        java.util.List<java.lang.String> list = this.groupValues_;
        if (list == null) {
            kotlin.jvm.internal.Intrinsics.throwNpe();
        }
        return list;
    }

    @org.jetbrains.annotations.Nullable
    public kotlin.text.MatchResult next() {
        int nextIndex = this.matchResult.end() + (this.matchResult.end() == this.matchResult.start() ? 1 : 0);
        if (nextIndex <= this.input.length()) {
            return kotlin.text.RegexKt.findNext(this.matcher, nextIndex, this.input);
        }
        return null;
    }
}
