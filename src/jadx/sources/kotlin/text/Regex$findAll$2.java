package kotlin.text;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u0004\u0018\u00010\u00012\u0006\u0010\u0002\u001a\u00020\u0001\u00a2\u0006\u0002\b\u0003"}, d2 = {"<anonymous>", "Lkotlin/text/MatchResult;", "p1", "invoke"}, k = 3, mv = {1, 1, 6})
/* compiled from: Regex.kt */
final class Regex$findAll$2 extends kotlin.jvm.internal.FunctionReference implements kotlin.jvm.functions.Function1<kotlin.text.MatchResult, kotlin.text.MatchResult> {
    public static final kotlin.text.Regex$findAll$2 INSTANCE = new kotlin.text.Regex$findAll$2();

    Regex$findAll$2() {
        super(1);
    }

    public final java.lang.String getName() {
        return "next";
    }

    public final kotlin.reflect.KDeclarationContainer getOwner() {
        return kotlin.jvm.internal.Reflection.getOrCreateKotlinClass(kotlin.text.MatchResult.class);
    }

    public final java.lang.String getSignature() {
        return "next()Lkotlin/text/MatchResult;";
    }

    @org.jetbrains.annotations.Nullable
    public final kotlin.text.MatchResult invoke(@org.jetbrains.annotations.NotNull kotlin.text.MatchResult p1) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(p1, "p1");
        return p1.next();
    }
}
