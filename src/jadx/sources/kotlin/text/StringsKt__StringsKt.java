package kotlin.text;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000t\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\r\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\f\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0019\n\u0002\b\u0004\n\u0002\u0010\u001e\n\u0002\b\u000e\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\b\b\n\u0002\u0010\u0011\n\u0002\b\u000f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u001a\u001a\u001c\u0010\t\u001a\u00020\n*\u00020\u00022\u0006\u0010\u000b\u001a\u00020\u00022\b\b\u0002\u0010\f\u001a\u00020\r\u001a\u001c\u0010\u000e\u001a\u00020\n*\u00020\u00022\u0006\u0010\u000b\u001a\u00020\u00022\b\b\u0002\u0010\f\u001a\u00020\r\u001a\u001f\u0010\u000f\u001a\u00020\r*\u00020\u00022\u0006\u0010\u0010\u001a\u00020\u00112\b\b\u0002\u0010\f\u001a\u00020\rH\u0087\u0002\u001a\u001f\u0010\u000f\u001a\u00020\r*\u00020\u00022\u0006\u0010\u000b\u001a\u00020\u00022\b\b\u0002\u0010\f\u001a\u00020\rH\u0087\u0002\u001a\u0015\u0010\u000f\u001a\u00020\r*\u00020\u00022\u0006\u0010\u0012\u001a\u00020\u0013H\u0087\n\u001a\u001c\u0010\u0014\u001a\u00020\r*\u00020\u00022\u0006\u0010\u0010\u001a\u00020\u00112\b\b\u0002\u0010\f\u001a\u00020\r\u001a\u001c\u0010\u0014\u001a\u00020\r*\u00020\u00022\u0006\u0010\u0015\u001a\u00020\u00022\b\b\u0002\u0010\f\u001a\u00020\r\u001a?\u0010\u0016\u001a\u0010\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u0011\u0018\u00010\u0017*\u00020\u00022\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u00062\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u001b\u001a\u00020\rH\u0002\u00a2\u0006\u0002\b\u001c\u001a:\u0010\u0016\u001a\u0010\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\n\u0018\u00010\u0017*\u00020\u00022\f\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\n0\u001e2\b\b\u0002\u0010\u001a\u001a\u00020\u00062\b\b\u0002\u0010\f\u001a\u00020\r\u001aE\u0010\u0016\u001a\u0010\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\n\u0018\u00010\u0017*\u00020\u00022\f\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\n0\u001e2\u0006\u0010\u001a\u001a\u00020\u00062\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u001b\u001a\u00020\rH\u0002\u00a2\u0006\u0002\b\u001c\u001a:\u0010\u001f\u001a\u0010\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\n\u0018\u00010\u0017*\u00020\u00022\f\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\n0\u001e2\b\b\u0002\u0010\u001a\u001a\u00020\u00062\b\b\u0002\u0010\f\u001a\u00020\r\u001a\u0012\u0010 \u001a\u00020\r*\u00020\u00022\u0006\u0010!\u001a\u00020\u0006\u001a&\u0010\"\u001a\u00020\u0006*\u00020\u00022\u0006\u0010\u0010\u001a\u00020\u00112\b\b\u0002\u0010\u001a\u001a\u00020\u00062\b\b\u0002\u0010\f\u001a\u00020\r\u001a;\u0010\"\u001a\u00020\u0006*\u00020\u00022\u0006\u0010\u000b\u001a\u00020\u00022\u0006\u0010\u001a\u001a\u00020\u00062\u0006\u0010#\u001a\u00020\u00062\u0006\u0010\f\u001a\u00020\r2\b\b\u0002\u0010\u001b\u001a\u00020\rH\u0002\u00a2\u0006\u0002\b$\u001a&\u0010\"\u001a\u00020\u0006*\u00020\u00022\u0006\u0010%\u001a\u00020\n2\b\b\u0002\u0010\u001a\u001a\u00020\u00062\b\b\u0002\u0010\f\u001a\u00020\r\u001a&\u0010&\u001a\u00020\u0006*\u00020\u00022\u0006\u0010\u0018\u001a\u00020\u00192\b\b\u0002\u0010\u001a\u001a\u00020\u00062\b\b\u0002\u0010\f\u001a\u00020\r\u001a,\u0010&\u001a\u00020\u0006*\u00020\u00022\f\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\n0\u001e2\b\b\u0002\u0010\u001a\u001a\u00020\u00062\b\b\u0002\u0010\f\u001a\u00020\r\u001a\r\u0010'\u001a\u00020\r*\u00020\u0002H\u0087\b\u001a\r\u0010(\u001a\u00020\r*\u00020\u0002H\u0087\b\u001a\r\u0010)\u001a\u00020\r*\u00020\u0002H\u0087\b\u001a\u000f\u0010*\u001a\u00020\r*\u0004\u0018\u00010\u0002H\u0087\b\u001a\u000f\u0010+\u001a\u00020\r*\u0004\u0018\u00010\u0002H\u0087\b\u001a\r\u0010,\u001a\u00020-*\u00020\u0002H\u0086\u0002\u001a&\u0010.\u001a\u00020\u0006*\u00020\u00022\u0006\u0010\u0010\u001a\u00020\u00112\b\b\u0002\u0010\u001a\u001a\u00020\u00062\b\b\u0002\u0010\f\u001a\u00020\r\u001a&\u0010.\u001a\u00020\u0006*\u00020\u00022\u0006\u0010%\u001a\u00020\n2\b\b\u0002\u0010\u001a\u001a\u00020\u00062\b\b\u0002\u0010\f\u001a\u00020\r\u001a&\u0010/\u001a\u00020\u0006*\u00020\u00022\u0006\u0010\u0018\u001a\u00020\u00192\b\b\u0002\u0010\u001a\u001a\u00020\u00062\b\b\u0002\u0010\f\u001a\u00020\r\u001a,\u0010/\u001a\u00020\u0006*\u00020\u00022\f\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\n0\u001e2\b\b\u0002\u0010\u001a\u001a\u00020\u00062\b\b\u0002\u0010\f\u001a\u00020\r\u001a\u0010\u00100\u001a\b\u0012\u0004\u0012\u00020\n01*\u00020\u0002\u001a\u0010\u00102\u001a\b\u0012\u0004\u0012\u00020\n03*\u00020\u0002\u001a\u0015\u00104\u001a\u00020\r*\u00020\u00022\u0006\u0010\u0012\u001a\u00020\u0013H\u0087\f\u001a\u000f\u00105\u001a\u00020\n*\u0004\u0018\u00010\nH\u0087\b\u001a\u001c\u00106\u001a\u00020\u0002*\u00020\u00022\u0006\u00107\u001a\u00020\u00062\b\b\u0002\u00108\u001a\u00020\u0011\u001a\u001c\u00106\u001a\u00020\n*\u00020\n2\u0006\u00107\u001a\u00020\u00062\b\b\u0002\u00108\u001a\u00020\u0011\u001a\u001c\u00109\u001a\u00020\u0002*\u00020\u00022\u0006\u00107\u001a\u00020\u00062\b\b\u0002\u00108\u001a\u00020\u0011\u001a\u001c\u00109\u001a\u00020\n*\u00020\n2\u0006\u00107\u001a\u00020\u00062\b\b\u0002\u00108\u001a\u00020\u0011\u001aG\u0010:\u001a\b\u0012\u0004\u0012\u00020\u000101*\u00020\u00022\u000e\u0010;\u001a\n\u0012\u0006\b\u0001\u0012\u00020\n0<2\b\b\u0002\u0010\u001a\u001a\u00020\u00062\b\b\u0002\u0010\f\u001a\u00020\r2\b\b\u0002\u0010=\u001a\u00020\u0006H\u0002\u00a2\u0006\u0004\b>\u0010?\u001a=\u0010:\u001a\b\u0012\u0004\u0012\u00020\u000101*\u00020\u00022\u0006\u0010;\u001a\u00020\u00192\b\b\u0002\u0010\u001a\u001a\u00020\u00062\b\b\u0002\u0010\f\u001a\u00020\r2\b\b\u0002\u0010=\u001a\u00020\u0006H\u0002\u00a2\u0006\u0002\b>\u001a4\u0010@\u001a\u00020\r*\u00020\u00022\u0006\u0010A\u001a\u00020\u00062\u0006\u0010\u000b\u001a\u00020\u00022\u0006\u0010B\u001a\u00020\u00062\u0006\u00107\u001a\u00020\u00062\u0006\u0010\f\u001a\u00020\rH\u0000\u001a\u0012\u0010C\u001a\u00020\u0002*\u00020\u00022\u0006\u0010D\u001a\u00020\u0002\u001a\u0012\u0010C\u001a\u00020\n*\u00020\n2\u0006\u0010D\u001a\u00020\u0002\u001a\u001a\u0010E\u001a\u00020\u0002*\u00020\u00022\u0006\u0010\u001a\u001a\u00020\u00062\u0006\u0010#\u001a\u00020\u0006\u001a\u0012\u0010E\u001a\u00020\u0002*\u00020\u00022\u0006\u0010F\u001a\u00020\u0001\u001a\u001d\u0010E\u001a\u00020\n*\u00020\n2\u0006\u0010\u001a\u001a\u00020\u00062\u0006\u0010#\u001a\u00020\u0006H\u0087\b\u001a\u0015\u0010E\u001a\u00020\n*\u00020\n2\u0006\u0010F\u001a\u00020\u0001H\u0087\b\u001a\u0012\u0010G\u001a\u00020\u0002*\u00020\u00022\u0006\u0010\u0015\u001a\u00020\u0002\u001a\u0012\u0010G\u001a\u00020\n*\u00020\n2\u0006\u0010\u0015\u001a\u00020\u0002\u001a\u0012\u0010H\u001a\u00020\u0002*\u00020\u00022\u0006\u0010I\u001a\u00020\u0002\u001a\u001a\u0010H\u001a\u00020\u0002*\u00020\u00022\u0006\u0010D\u001a\u00020\u00022\u0006\u0010\u0015\u001a\u00020\u0002\u001a\u0012\u0010H\u001a\u00020\n*\u00020\n2\u0006\u0010I\u001a\u00020\u0002\u001a\u001a\u0010H\u001a\u00020\n*\u00020\n2\u0006\u0010D\u001a\u00020\u00022\u0006\u0010\u0015\u001a\u00020\u0002\u001a+\u0010J\u001a\u00020\n*\u00020\u00022\u0006\u0010\u0012\u001a\u00020\u00132\u0014\b\b\u0010K\u001a\u000e\u0012\u0004\u0012\u00020M\u0012\u0004\u0012\u00020\u00020LH\u0087\b\u001a\u001d\u0010J\u001a\u00020\n*\u00020\u00022\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010N\u001a\u00020\nH\u0087\b\u001a$\u0010O\u001a\u00020\n*\u00020\n2\u0006\u0010I\u001a\u00020\u00112\u0006\u0010N\u001a\u00020\n2\b\b\u0002\u0010P\u001a\u00020\n\u001a$\u0010O\u001a\u00020\n*\u00020\n2\u0006\u0010I\u001a\u00020\n2\u0006\u0010N\u001a\u00020\n2\b\b\u0002\u0010P\u001a\u00020\n\u001a$\u0010Q\u001a\u00020\n*\u00020\n2\u0006\u0010I\u001a\u00020\u00112\u0006\u0010N\u001a\u00020\n2\b\b\u0002\u0010P\u001a\u00020\n\u001a$\u0010Q\u001a\u00020\n*\u00020\n2\u0006\u0010I\u001a\u00020\n2\u0006\u0010N\u001a\u00020\n2\b\b\u0002\u0010P\u001a\u00020\n\u001a$\u0010R\u001a\u00020\n*\u00020\n2\u0006\u0010I\u001a\u00020\u00112\u0006\u0010N\u001a\u00020\n2\b\b\u0002\u0010P\u001a\u00020\n\u001a$\u0010R\u001a\u00020\n*\u00020\n2\u0006\u0010I\u001a\u00020\n2\u0006\u0010N\u001a\u00020\n2\b\b\u0002\u0010P\u001a\u00020\n\u001a$\u0010S\u001a\u00020\n*\u00020\n2\u0006\u0010I\u001a\u00020\u00112\u0006\u0010N\u001a\u00020\n2\b\b\u0002\u0010P\u001a\u00020\n\u001a$\u0010S\u001a\u00020\n*\u00020\n2\u0006\u0010I\u001a\u00020\n2\u0006\u0010N\u001a\u00020\n2\b\b\u0002\u0010P\u001a\u00020\n\u001a\u001d\u0010T\u001a\u00020\n*\u00020\u00022\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010N\u001a\u00020\nH\u0087\b\u001a\"\u0010U\u001a\u00020\u0002*\u00020\u00022\u0006\u0010\u001a\u001a\u00020\u00062\u0006\u0010#\u001a\u00020\u00062\u0006\u0010N\u001a\u00020\u0002\u001a\u001a\u0010U\u001a\u00020\u0002*\u00020\u00022\u0006\u0010F\u001a\u00020\u00012\u0006\u0010N\u001a\u00020\u0002\u001a%\u0010U\u001a\u00020\n*\u00020\n2\u0006\u0010\u001a\u001a\u00020\u00062\u0006\u0010#\u001a\u00020\u00062\u0006\u0010N\u001a\u00020\u0002H\u0087\b\u001a\u001d\u0010U\u001a\u00020\n*\u00020\n2\u0006\u0010F\u001a\u00020\u00012\u0006\u0010N\u001a\u00020\u0002H\u0087\b\u001a=\u0010V\u001a\b\u0012\u0004\u0012\u00020\n03*\u00020\u00022\u0012\u0010;\u001a\n\u0012\u0006\b\u0001\u0012\u00020\n0<\"\u00020\n2\b\b\u0002\u0010\f\u001a\u00020\r2\b\b\u0002\u0010=\u001a\u00020\u0006\u00a2\u0006\u0002\u0010W\u001a0\u0010V\u001a\b\u0012\u0004\u0012\u00020\n03*\u00020\u00022\n\u0010;\u001a\u00020\u0019\"\u00020\u00112\b\b\u0002\u0010\f\u001a\u00020\r2\b\b\u0002\u0010=\u001a\u00020\u0006\u001a%\u0010V\u001a\b\u0012\u0004\u0012\u00020\n03*\u00020\u00022\u0006\u0010\u0012\u001a\u00020\u00132\b\b\u0002\u0010=\u001a\u00020\u0006H\u0087\b\u001a=\u0010X\u001a\b\u0012\u0004\u0012\u00020\n01*\u00020\u00022\u0012\u0010;\u001a\n\u0012\u0006\b\u0001\u0012\u00020\n0<\"\u00020\n2\b\b\u0002\u0010\f\u001a\u00020\r2\b\b\u0002\u0010=\u001a\u00020\u0006\u00a2\u0006\u0002\u0010Y\u001a0\u0010X\u001a\b\u0012\u0004\u0012\u00020\n01*\u00020\u00022\n\u0010;\u001a\u00020\u0019\"\u00020\u00112\b\b\u0002\u0010\f\u001a\u00020\r2\b\b\u0002\u0010=\u001a\u00020\u0006\u001a\u001c\u0010Z\u001a\u00020\r*\u00020\u00022\u0006\u0010\u0010\u001a\u00020\u00112\b\b\u0002\u0010\f\u001a\u00020\r\u001a\u001c\u0010Z\u001a\u00020\r*\u00020\u00022\u0006\u0010D\u001a\u00020\u00022\b\b\u0002\u0010\f\u001a\u00020\r\u001a$\u0010Z\u001a\u00020\r*\u00020\u00022\u0006\u0010D\u001a\u00020\u00022\u0006\u0010\u001a\u001a\u00020\u00062\b\b\u0002\u0010\f\u001a\u00020\r\u001a\u0012\u0010[\u001a\u00020\u0002*\u00020\u00022\u0006\u0010F\u001a\u00020\u0001\u001a\u001d\u0010[\u001a\u00020\u0002*\u00020\n2\u0006\u0010\\\u001a\u00020\u00062\u0006\u0010]\u001a\u00020\u0006H\u0087\b\u001a\u001f\u0010^\u001a\u00020\n*\u00020\u00022\u0006\u0010\u001a\u001a\u00020\u00062\b\b\u0002\u0010#\u001a\u00020\u0006H\u0087\b\u001a\u0012\u0010^\u001a\u00020\n*\u00020\u00022\u0006\u0010F\u001a\u00020\u0001\u001a\u0012\u0010^\u001a\u00020\n*\u00020\n2\u0006\u0010F\u001a\u00020\u0001\u001a\u001c\u0010_\u001a\u00020\n*\u00020\n2\u0006\u0010I\u001a\u00020\u00112\b\b\u0002\u0010P\u001a\u00020\n\u001a\u001c\u0010_\u001a\u00020\n*\u00020\n2\u0006\u0010I\u001a\u00020\n2\b\b\u0002\u0010P\u001a\u00020\n\u001a\u001c\u0010`\u001a\u00020\n*\u00020\n2\u0006\u0010I\u001a\u00020\u00112\b\b\u0002\u0010P\u001a\u00020\n\u001a\u001c\u0010`\u001a\u00020\n*\u00020\n2\u0006\u0010I\u001a\u00020\n2\b\b\u0002\u0010P\u001a\u00020\n\u001a\u001c\u0010a\u001a\u00020\n*\u00020\n2\u0006\u0010I\u001a\u00020\u00112\b\b\u0002\u0010P\u001a\u00020\n\u001a\u001c\u0010a\u001a\u00020\n*\u00020\n2\u0006\u0010I\u001a\u00020\n2\b\b\u0002\u0010P\u001a\u00020\n\u001a\u001c\u0010b\u001a\u00020\n*\u00020\n2\u0006\u0010I\u001a\u00020\u00112\b\b\u0002\u0010P\u001a\u00020\n\u001a\u001c\u0010b\u001a\u00020\n*\u00020\n2\u0006\u0010I\u001a\u00020\n2\b\b\u0002\u0010P\u001a\u00020\n\u001a\n\u0010c\u001a\u00020\u0002*\u00020\u0002\u001a!\u0010c\u001a\u00020\u0002*\u00020\u00022\u0012\u0010d\u001a\u000e\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u00020\r0LH\u0086\b\u001a\u0016\u0010c\u001a\u00020\u0002*\u00020\u00022\n\u0010\u0018\u001a\u00020\u0019\"\u00020\u0011\u001a\r\u0010c\u001a\u00020\n*\u00020\nH\u0087\b\u001a!\u0010c\u001a\u00020\n*\u00020\n2\u0012\u0010d\u001a\u000e\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u00020\r0LH\u0086\b\u001a\u0016\u0010c\u001a\u00020\n*\u00020\n2\n\u0010\u0018\u001a\u00020\u0019\"\u00020\u0011\u001a\n\u0010e\u001a\u00020\u0002*\u00020\u0002\u001a!\u0010e\u001a\u00020\u0002*\u00020\u00022\u0012\u0010d\u001a\u000e\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u00020\r0LH\u0086\b\u001a\u0016\u0010e\u001a\u00020\u0002*\u00020\u00022\n\u0010\u0018\u001a\u00020\u0019\"\u00020\u0011\u001a\r\u0010e\u001a\u00020\n*\u00020\nH\u0087\b\u001a!\u0010e\u001a\u00020\n*\u00020\n2\u0012\u0010d\u001a\u000e\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u00020\r0LH\u0086\b\u001a\u0016\u0010e\u001a\u00020\n*\u00020\n2\n\u0010\u0018\u001a\u00020\u0019\"\u00020\u0011\u001a\n\u0010f\u001a\u00020\u0002*\u00020\u0002\u001a!\u0010f\u001a\u00020\u0002*\u00020\u00022\u0012\u0010d\u001a\u000e\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u00020\r0LH\u0086\b\u001a\u0016\u0010f\u001a\u00020\u0002*\u00020\u00022\n\u0010\u0018\u001a\u00020\u0019\"\u00020\u0011\u001a\r\u0010f\u001a\u00020\n*\u00020\nH\u0087\b\u001a!\u0010f\u001a\u00020\n*\u00020\n2\u0012\u0010d\u001a\u000e\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u00020\r0LH\u0086\b\u001a\u0016\u0010f\u001a\u00020\n*\u00020\n2\n\u0010\u0018\u001a\u00020\u0019\"\u00020\u0011\"\u0015\u0010\u0000\u001a\u00020\u0001*\u00020\u00028F\u00a2\u0006\u0006\u001a\u0004\b\u0003\u0010\u0004\"\u0015\u0010\u0005\u001a\u00020\u0006*\u00020\u00028F\u00a2\u0006\u0006\u001a\u0004\b\u0007\u0010\b\u00a8\u0006g"}, d2 = {"indices", "Lkotlin/ranges/IntRange;", "", "getIndices", "(Ljava/lang/CharSequence;)Lkotlin/ranges/IntRange;", "lastIndex", "", "getLastIndex", "(Ljava/lang/CharSequence;)I", "commonPrefixWith", "", "other", "ignoreCase", "", "commonSuffixWith", "contains", "char", "", "regex", "Lkotlin/text/Regex;", "endsWith", "suffix", "findAnyOf", "Lkotlin/Pair;", "chars", "", "startIndex", "last", "findAnyOf$StringsKt__StringsKt", "strings", "", "findLastAnyOf", "hasSurrogatePairAt", "index", "indexOf", "endIndex", "indexOf$StringsKt__StringsKt", "string", "indexOfAny", "isEmpty", "isNotBlank", "isNotEmpty", "isNullOrBlank", "isNullOrEmpty", "iterator", "Lkotlin/collections/CharIterator;", "lastIndexOf", "lastIndexOfAny", "lineSequence", "Lkotlin/sequences/Sequence;", "lines", "", "matches", "orEmpty", "padEnd", "length", "padChar", "padStart", "rangesDelimitedBy", "delimiters", "", "limit", "rangesDelimitedBy$StringsKt__StringsKt", "(Ljava/lang/CharSequence;[Ljava/lang/String;IZI)Lkotlin/sequences/Sequence;", "regionMatchesImpl", "thisOffset", "otherOffset", "removePrefix", "prefix", "removeRange", "range", "removeSuffix", "removeSurrounding", "delimiter", "replace", "transform", "Lkotlin/Function1;", "Lkotlin/text/MatchResult;", "replacement", "replaceAfter", "missingDelimiterValue", "replaceAfterLast", "replaceBefore", "replaceBeforeLast", "replaceFirst", "replaceRange", "split", "(Ljava/lang/CharSequence;[Ljava/lang/String;ZI)Ljava/util/List;", "splitToSequence", "(Ljava/lang/CharSequence;[Ljava/lang/String;ZI)Lkotlin/sequences/Sequence;", "startsWith", "subSequence", "start", "end", "substring", "substringAfter", "substringAfterLast", "substringBefore", "substringBeforeLast", "trim", "predicate", "trimEnd", "trimStart", "kotlin-stdlib"}, k = 5, mv = {1, 1, 6}, xi = 1, xs = "kotlin/text/StringsKt")
/* compiled from: Strings.kt */
class StringsKt__StringsKt extends kotlin.text.StringsKt__StringsJVMKt {
    @org.jetbrains.annotations.NotNull
    public static final java.lang.CharSequence trim(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.Character, java.lang.Boolean> predicate) {
        int index;
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(predicate, "predicate");
        int startIndex = 0;
        int endIndex = $receiver.length() - 1;
        boolean startFound = false;
        while (startIndex <= endIndex) {
            if (!startFound) {
                index = startIndex;
            } else {
                index = endIndex;
            }
            boolean match = ((java.lang.Boolean) predicate.invoke(java.lang.Character.valueOf($receiver.charAt(index)))).booleanValue();
            if (!startFound) {
                if (!match) {
                    startFound = true;
                } else {
                    startIndex++;
                }
            } else if (!match) {
                break;
            } else {
                endIndex--;
            }
        }
        return $receiver.subSequence(startIndex, endIndex + 1);
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.String trim(@org.jetbrains.annotations.NotNull java.lang.String $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.Character, java.lang.Boolean> predicate) {
        int index$iv;
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(predicate, "predicate");
        java.lang.CharSequence $receiver$iv = $receiver;
        int startIndex$iv = 0;
        int endIndex$iv = $receiver$iv.length() - 1;
        boolean startFound$iv = false;
        while (startIndex$iv <= endIndex$iv) {
            if (!startFound$iv) {
                index$iv = startIndex$iv;
            } else {
                index$iv = endIndex$iv;
            }
            boolean match$iv = ((java.lang.Boolean) predicate.invoke(java.lang.Character.valueOf($receiver$iv.charAt(index$iv)))).booleanValue();
            if (!startFound$iv) {
                if (!match$iv) {
                    startFound$iv = true;
                } else {
                    startIndex$iv++;
                }
            } else if (!match$iv) {
                break;
            } else {
                endIndex$iv--;
            }
        }
        return $receiver$iv.subSequence(startIndex$iv, endIndex$iv + 1).toString();
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.CharSequence trimStart(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.Character, java.lang.Boolean> predicate) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(predicate, "predicate");
        int i = 0;
        int length = $receiver.length() - 1;
        if (0 <= length) {
            while (((java.lang.Boolean) predicate.invoke(java.lang.Character.valueOf($receiver.charAt(i)))).booleanValue()) {
                if (i != length) {
                    i++;
                }
            }
            return $receiver.subSequence(i, $receiver.length());
        }
        return "";
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.String trimStart(@org.jetbrains.annotations.NotNull java.lang.String $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.Character, java.lang.Boolean> predicate) {
        java.lang.CharSequence charSequence;
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(predicate, "predicate");
        java.lang.CharSequence $receiver$iv = $receiver;
        int i = 0;
        int length = $receiver$iv.length() - 1;
        if (0 <= length) {
            while (true) {
                if (((java.lang.Boolean) predicate.invoke(java.lang.Character.valueOf($receiver$iv.charAt(i)))).booleanValue()) {
                    if (i == length) {
                        break;
                    }
                    i++;
                } else {
                    charSequence = $receiver$iv.subSequence(i, $receiver$iv.length());
                    break;
                }
            }
            return charSequence.toString();
        }
        charSequence = "";
        return charSequence.toString();
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.CharSequence trimEnd(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.Character, java.lang.Boolean> predicate) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(predicate, "predicate");
        kotlin.ranges.IntProgression reversed = kotlin.ranges.RangesKt.reversed((kotlin.ranges.IntProgression) kotlin.text.StringsKt.getIndices($receiver));
        int first = reversed.getFirst();
        int last = reversed.getLast();
        int step = reversed.getStep();
        if (step <= 0 ? first >= last : first <= last) {
            while (true) {
                int i = first;
                if (((java.lang.Boolean) predicate.invoke(java.lang.Character.valueOf($receiver.charAt(i)))).booleanValue()) {
                    if (i == last) {
                        break;
                    }
                    first = i + step;
                } else {
                    return $receiver.subSequence(0, i + 1).toString();
                }
            }
        }
        return "";
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.String trimEnd(@org.jetbrains.annotations.NotNull java.lang.String $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.Character, java.lang.Boolean> predicate) {
        java.lang.CharSequence obj;
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(predicate, "predicate");
        java.lang.CharSequence $receiver$iv = $receiver;
        kotlin.ranges.IntProgression reversed = kotlin.ranges.RangesKt.reversed((kotlin.ranges.IntProgression) kotlin.text.StringsKt.getIndices($receiver$iv));
        int first = reversed.getFirst();
        int last = reversed.getLast();
        int step = reversed.getStep();
        if (step <= 0 ? first >= last : first <= last) {
            while (true) {
                int i = first;
                if (((java.lang.Boolean) predicate.invoke(java.lang.Character.valueOf($receiver$iv.charAt(i)))).booleanValue()) {
                    if (i == last) {
                        break;
                    }
                    first = i + step;
                } else {
                    obj = $receiver$iv.subSequence(0, i + 1).toString();
                    break;
                }
            }
            return obj.toString();
        }
        obj = "";
        return obj.toString();
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.CharSequence trim(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull char... chars) {
        int index$iv;
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(chars, "chars");
        java.lang.CharSequence $receiver$iv = $receiver;
        int startIndex$iv = 0;
        int endIndex$iv = $receiver$iv.length() - 1;
        boolean startFound$iv = false;
        while (startIndex$iv <= endIndex$iv) {
            if (!startFound$iv) {
                index$iv = startIndex$iv;
            } else {
                index$iv = endIndex$iv;
            }
            boolean match$iv = kotlin.collections.ArraysKt.contains(chars, $receiver$iv.charAt(index$iv));
            if (!startFound$iv) {
                if (!match$iv) {
                    startFound$iv = true;
                } else {
                    startIndex$iv++;
                }
            } else if (!match$iv) {
                break;
            } else {
                endIndex$iv--;
            }
        }
        return $receiver$iv.subSequence(startIndex$iv, endIndex$iv + 1);
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.String trim(@org.jetbrains.annotations.NotNull java.lang.String $receiver, @org.jetbrains.annotations.NotNull char... chars) {
        int index$iv$iv;
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(chars, "chars");
        java.lang.CharSequence $receiver$iv$iv = $receiver;
        int startIndex$iv$iv = 0;
        int endIndex$iv$iv = $receiver$iv$iv.length() - 1;
        boolean startFound$iv$iv = false;
        while (startIndex$iv$iv <= endIndex$iv$iv) {
            if (!startFound$iv$iv) {
                index$iv$iv = startIndex$iv$iv;
            } else {
                index$iv$iv = endIndex$iv$iv;
            }
            boolean match$iv$iv = kotlin.collections.ArraysKt.contains(chars, $receiver$iv$iv.charAt(index$iv$iv));
            if (!startFound$iv$iv) {
                if (!match$iv$iv) {
                    startFound$iv$iv = true;
                } else {
                    startIndex$iv$iv++;
                }
            } else if (!match$iv$iv) {
                break;
            } else {
                endIndex$iv$iv--;
            }
        }
        return $receiver$iv$iv.subSequence(startIndex$iv$iv, endIndex$iv$iv + 1).toString();
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.CharSequence trimStart(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull char... chars) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(chars, "chars");
        java.lang.CharSequence $receiver$iv = $receiver;
        int i = 0;
        int length = $receiver$iv.length() - 1;
        if (0 <= length) {
            while (kotlin.collections.ArraysKt.contains(chars, $receiver$iv.charAt(i))) {
                if (i != length) {
                    i++;
                }
            }
            return $receiver$iv.subSequence(i, $receiver$iv.length());
        }
        return "";
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.String trimStart(@org.jetbrains.annotations.NotNull java.lang.String $receiver, @org.jetbrains.annotations.NotNull char... chars) {
        java.lang.CharSequence charSequence;
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(chars, "chars");
        java.lang.CharSequence $receiver$iv$iv = $receiver;
        int i = 0;
        int length = $receiver$iv$iv.length() - 1;
        if (0 <= length) {
            while (true) {
                if (kotlin.collections.ArraysKt.contains(chars, $receiver$iv$iv.charAt(i))) {
                    if (i == length) {
                        break;
                    }
                    i++;
                } else {
                    charSequence = $receiver$iv$iv.subSequence(i, $receiver$iv$iv.length());
                    break;
                }
            }
            return charSequence.toString();
        }
        charSequence = "";
        return charSequence.toString();
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.CharSequence trimEnd(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull char... chars) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(chars, "chars");
        java.lang.CharSequence $receiver$iv = $receiver;
        kotlin.ranges.IntProgression reversed = kotlin.ranges.RangesKt.reversed((kotlin.ranges.IntProgression) kotlin.text.StringsKt.getIndices($receiver$iv));
        int first = reversed.getFirst();
        int last = reversed.getLast();
        int step = reversed.getStep();
        if (step <= 0 ? first >= last : first <= last) {
            while (kotlin.collections.ArraysKt.contains(chars, $receiver$iv.charAt(first))) {
                if (first != last) {
                    first += step;
                }
            }
            return $receiver$iv.subSequence(0, first + 1).toString();
        }
        return "";
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.String trimEnd(@org.jetbrains.annotations.NotNull java.lang.String $receiver, @org.jetbrains.annotations.NotNull char... chars) {
        java.lang.CharSequence obj;
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(chars, "chars");
        java.lang.CharSequence $receiver$iv$iv = $receiver;
        kotlin.ranges.IntProgression reversed = kotlin.ranges.RangesKt.reversed((kotlin.ranges.IntProgression) kotlin.text.StringsKt.getIndices($receiver$iv$iv));
        int first = reversed.getFirst();
        int last = reversed.getLast();
        int step = reversed.getStep();
        if (step <= 0 ? first >= last : first <= last) {
            while (true) {
                if (kotlin.collections.ArraysKt.contains(chars, $receiver$iv$iv.charAt(first))) {
                    if (first == last) {
                        break;
                    }
                    first += step;
                } else {
                    obj = $receiver$iv$iv.subSequence(0, first + 1).toString();
                    break;
                }
            }
        }
        obj = "";
        return obj.toString();
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.CharSequence trim(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver) {
        int index$iv;
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        java.lang.CharSequence $receiver$iv = $receiver;
        int startIndex$iv = 0;
        int endIndex$iv = $receiver$iv.length() - 1;
        boolean startFound$iv = false;
        while (startIndex$iv <= endIndex$iv) {
            if (!startFound$iv) {
                index$iv = startIndex$iv;
            } else {
                index$iv = endIndex$iv;
            }
            boolean match$iv = kotlin.text.CharsKt.isWhitespace($receiver$iv.charAt(index$iv));
            if (!startFound$iv) {
                if (!match$iv) {
                    startFound$iv = true;
                } else {
                    startIndex$iv++;
                }
            } else if (!match$iv) {
                break;
            } else {
                endIndex$iv--;
            }
        }
        return $receiver$iv.subSequence(startIndex$iv, endIndex$iv + 1);
    }

    @kotlin.internal.InlineOnly
    private static final java.lang.String trim(@org.jetbrains.annotations.NotNull java.lang.String $receiver) {
        if ($receiver != null) {
            return kotlin.text.StringsKt.trim((java.lang.CharSequence) $receiver).toString();
        }
        throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.CharSequence trimStart(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        java.lang.CharSequence $receiver$iv = $receiver;
        int i = 0;
        int length = $receiver$iv.length() - 1;
        if (0 <= length) {
            while (kotlin.text.CharsKt.isWhitespace($receiver$iv.charAt(i))) {
                if (i != length) {
                    i++;
                }
            }
            return $receiver$iv.subSequence(i, $receiver$iv.length());
        }
        return "";
    }

    @kotlin.internal.InlineOnly
    private static final java.lang.String trimStart(@org.jetbrains.annotations.NotNull java.lang.String $receiver) {
        if ($receiver != null) {
            return kotlin.text.StringsKt.trimStart((java.lang.CharSequence) $receiver).toString();
        }
        throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.CharSequence trimEnd(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        java.lang.CharSequence $receiver$iv = $receiver;
        kotlin.ranges.IntProgression reversed = kotlin.ranges.RangesKt.reversed((kotlin.ranges.IntProgression) kotlin.text.StringsKt.getIndices($receiver$iv));
        int first = reversed.getFirst();
        int last = reversed.getLast();
        int step = reversed.getStep();
        if (step <= 0 ? first >= last : first <= last) {
            while (kotlin.text.CharsKt.isWhitespace($receiver$iv.charAt(first))) {
                if (first != last) {
                    first += step;
                }
            }
            return $receiver$iv.subSequence(0, first + 1).toString();
        }
        return "";
    }

    @kotlin.internal.InlineOnly
    private static final java.lang.String trimEnd(@org.jetbrains.annotations.NotNull java.lang.String $receiver) {
        if ($receiver != null) {
            return kotlin.text.StringsKt.trimEnd((java.lang.CharSequence) $receiver).toString();
        }
        throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
    }

    @org.jetbrains.annotations.NotNull
    public static /* bridge */ /* synthetic */ java.lang.CharSequence padStart$default(java.lang.CharSequence charSequence, int i, char c, int i2, java.lang.Object obj) {
        if ((i2 & 2) != 0) {
            c = ' ';
        }
        return kotlin.text.StringsKt.padStart(charSequence, i, c);
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.CharSequence padStart(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, int length, char padChar) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        if (length < 0) {
            throw new java.lang.IllegalArgumentException("Desired length " + length + " is less than zero.");
        } else if (length <= $receiver.length()) {
            return $receiver.subSequence(0, $receiver.length());
        } else {
            java.lang.StringBuilder sb = new java.lang.StringBuilder(length);
            int i = 1;
            int length2 = length - $receiver.length();
            if (1 <= length2) {
                while (true) {
                    sb.append(padChar);
                    if (i == length2) {
                        break;
                    }
                    i++;
                }
            }
            sb.append($receiver);
            return sb;
        }
    }

    @org.jetbrains.annotations.NotNull
    public static /* bridge */ /* synthetic */ java.lang.String padStart$default(java.lang.String str, int i, char c, int i2, java.lang.Object obj) {
        if ((i2 & 2) != 0) {
            c = ' ';
        }
        return kotlin.text.StringsKt.padStart(str, i, c);
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.String padStart(@org.jetbrains.annotations.NotNull java.lang.String $receiver, int length, char padChar) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        return kotlin.text.StringsKt.padStart((java.lang.CharSequence) $receiver, length, padChar).toString();
    }

    @org.jetbrains.annotations.NotNull
    public static /* bridge */ /* synthetic */ java.lang.CharSequence padEnd$default(java.lang.CharSequence charSequence, int i, char c, int i2, java.lang.Object obj) {
        if ((i2 & 2) != 0) {
            c = ' ';
        }
        return kotlin.text.StringsKt.padEnd(charSequence, i, c);
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.CharSequence padEnd(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, int length, char padChar) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        if (length < 0) {
            throw new java.lang.IllegalArgumentException("Desired length " + length + " is less than zero.");
        } else if (length <= $receiver.length()) {
            return $receiver.subSequence(0, $receiver.length());
        } else {
            java.lang.StringBuilder sb = new java.lang.StringBuilder(length);
            sb.append($receiver);
            int i = 1;
            int length2 = length - $receiver.length();
            if (1 <= length2) {
                while (true) {
                    sb.append(padChar);
                    if (i == length2) {
                        break;
                    }
                    i++;
                }
            }
            return sb;
        }
    }

    @org.jetbrains.annotations.NotNull
    public static /* bridge */ /* synthetic */ java.lang.String padEnd$default(java.lang.String str, int i, char c, int i2, java.lang.Object obj) {
        if ((i2 & 2) != 0) {
            c = ' ';
        }
        return kotlin.text.StringsKt.padEnd(str, i, c);
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.String padEnd(@org.jetbrains.annotations.NotNull java.lang.String $receiver, int length, char padChar) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        return kotlin.text.StringsKt.padEnd((java.lang.CharSequence) $receiver, length, padChar).toString();
    }

    @kotlin.internal.InlineOnly
    private static final boolean isNullOrEmpty(@org.jetbrains.annotations.Nullable java.lang.CharSequence $receiver) {
        return $receiver == null || $receiver.length() == 0;
    }

    @kotlin.internal.InlineOnly
    private static final boolean isEmpty(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver) {
        return $receiver.length() == 0;
    }

    @kotlin.internal.InlineOnly
    private static final boolean isNotEmpty(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver) {
        return $receiver.length() > 0;
    }

    @kotlin.internal.InlineOnly
    private static final boolean isNotBlank(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver) {
        return !kotlin.text.StringsKt.isBlank($receiver);
    }

    @kotlin.internal.InlineOnly
    private static final boolean isNullOrBlank(@org.jetbrains.annotations.Nullable java.lang.CharSequence $receiver) {
        return $receiver == null || kotlin.text.StringsKt.isBlank($receiver);
    }

    @org.jetbrains.annotations.NotNull
    public static final kotlin.collections.CharIterator iterator(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        return new kotlin.text.StringsKt__StringsKt$iterator$1($receiver);
    }

    @kotlin.internal.InlineOnly
    private static final java.lang.String orEmpty(@org.jetbrains.annotations.Nullable java.lang.String $receiver) {
        return $receiver != null ? $receiver : "";
    }

    @org.jetbrains.annotations.NotNull
    public static final kotlin.ranges.IntRange getIndices(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        return new kotlin.ranges.IntRange(0, $receiver.length() - 1);
    }

    public static final int getLastIndex(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        return $receiver.length() - 1;
    }

    public static final boolean hasSurrogatePairAt(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, int index) {
        boolean z;
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        if (index < 0 || index > $receiver.length() - 2) {
            z = false;
        } else {
            z = true;
        }
        return z && java.lang.Character.isHighSurrogate($receiver.charAt(index)) && java.lang.Character.isLowSurrogate($receiver.charAt(index + 1));
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.String substring(@org.jetbrains.annotations.NotNull java.lang.String $receiver, @org.jetbrains.annotations.NotNull kotlin.ranges.IntRange range) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(range, "range");
        java.lang.String substring = $receiver.substring(range.getStart().intValue(), range.getEndInclusive().intValue() + 1);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        return substring;
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.CharSequence subSequence(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull kotlin.ranges.IntRange range) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(range, "range");
        return $receiver.subSequence(range.getStart().intValue(), range.getEndInclusive().intValue() + 1);
    }

    @kotlin.Deprecated(message = "Use parameters named startIndex and endIndex.", replaceWith = @kotlin.ReplaceWith(expression = "subSequence(startIndex = start, endIndex = end)", imports = {}))
    @kotlin.internal.InlineOnly
    private static final java.lang.CharSequence subSequence(@org.jetbrains.annotations.NotNull java.lang.String $receiver, int start, int end) {
        return $receiver.subSequence(start, end);
    }

    @kotlin.internal.InlineOnly
    private static final java.lang.String substring(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, int startIndex, int endIndex) {
        return $receiver.subSequence(startIndex, endIndex).toString();
    }

    @kotlin.internal.InlineOnly
    static /* bridge */ /* synthetic */ java.lang.String substring$default(java.lang.CharSequence $receiver, int startIndex, int endIndex, int i, java.lang.Object obj) {
        if ((i & 2) != 0) {
            endIndex = $receiver.length();
        }
        return $receiver.subSequence(startIndex, endIndex).toString();
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.String substring(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull kotlin.ranges.IntRange range) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(range, "range");
        return $receiver.subSequence(range.getStart().intValue(), range.getEndInclusive().intValue() + 1).toString();
    }

    @org.jetbrains.annotations.NotNull
    public static /* bridge */ /* synthetic */ java.lang.String substringBefore$default(java.lang.String str, char c, java.lang.String str2, int i, java.lang.Object obj) {
        if ((i & 2) != 0) {
            str2 = str;
        }
        return kotlin.text.StringsKt.substringBefore(str, c, str2);
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.String substringBefore(@org.jetbrains.annotations.NotNull java.lang.String $receiver, char delimiter, @org.jetbrains.annotations.NotNull java.lang.String missingDelimiterValue) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(missingDelimiterValue, "missingDelimiterValue");
        int index = kotlin.text.StringsKt.indexOf$default((java.lang.CharSequence) $receiver, delimiter, 0, false, 6, (java.lang.Object) null);
        if (index == -1) {
            return missingDelimiterValue;
        }
        java.lang.String missingDelimiterValue2 = $receiver.substring(0, index);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(missingDelimiterValue2, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        return missingDelimiterValue2;
    }

    @org.jetbrains.annotations.NotNull
    public static /* bridge */ /* synthetic */ java.lang.String substringBefore$default(java.lang.String str, java.lang.String str2, java.lang.String str3, int i, java.lang.Object obj) {
        if ((i & 2) != 0) {
            str3 = str;
        }
        return kotlin.text.StringsKt.substringBefore(str, str2, str3);
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.String substringBefore(@org.jetbrains.annotations.NotNull java.lang.String $receiver, @org.jetbrains.annotations.NotNull java.lang.String delimiter, @org.jetbrains.annotations.NotNull java.lang.String missingDelimiterValue) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(delimiter, "delimiter");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(missingDelimiterValue, "missingDelimiterValue");
        int index = kotlin.text.StringsKt.indexOf$default((java.lang.CharSequence) $receiver, delimiter, 0, false, 6, (java.lang.Object) null);
        if (index == -1) {
            return missingDelimiterValue;
        }
        java.lang.String missingDelimiterValue2 = $receiver.substring(0, index);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(missingDelimiterValue2, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        return missingDelimiterValue2;
    }

    @org.jetbrains.annotations.NotNull
    public static /* bridge */ /* synthetic */ java.lang.String substringAfter$default(java.lang.String str, char c, java.lang.String str2, int i, java.lang.Object obj) {
        if ((i & 2) != 0) {
            str2 = str;
        }
        return kotlin.text.StringsKt.substringAfter(str, c, str2);
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.String substringAfter(@org.jetbrains.annotations.NotNull java.lang.String $receiver, char delimiter, @org.jetbrains.annotations.NotNull java.lang.String missingDelimiterValue) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(missingDelimiterValue, "missingDelimiterValue");
        int index = kotlin.text.StringsKt.indexOf$default((java.lang.CharSequence) $receiver, delimiter, 0, false, 6, (java.lang.Object) null);
        if (index == -1) {
            return missingDelimiterValue;
        }
        java.lang.String missingDelimiterValue2 = $receiver.substring(index + 1, $receiver.length());
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(missingDelimiterValue2, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        return missingDelimiterValue2;
    }

    @org.jetbrains.annotations.NotNull
    public static /* bridge */ /* synthetic */ java.lang.String substringAfter$default(java.lang.String str, java.lang.String str2, java.lang.String str3, int i, java.lang.Object obj) {
        if ((i & 2) != 0) {
            str3 = str;
        }
        return kotlin.text.StringsKt.substringAfter(str, str2, str3);
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.String substringAfter(@org.jetbrains.annotations.NotNull java.lang.String $receiver, @org.jetbrains.annotations.NotNull java.lang.String delimiter, @org.jetbrains.annotations.NotNull java.lang.String missingDelimiterValue) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(delimiter, "delimiter");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(missingDelimiterValue, "missingDelimiterValue");
        int index = kotlin.text.StringsKt.indexOf$default((java.lang.CharSequence) $receiver, delimiter, 0, false, 6, (java.lang.Object) null);
        if (index == -1) {
            return missingDelimiterValue;
        }
        java.lang.String missingDelimiterValue2 = $receiver.substring(delimiter.length() + index, $receiver.length());
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(missingDelimiterValue2, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        return missingDelimiterValue2;
    }

    @org.jetbrains.annotations.NotNull
    public static /* bridge */ /* synthetic */ java.lang.String substringBeforeLast$default(java.lang.String str, char c, java.lang.String str2, int i, java.lang.Object obj) {
        if ((i & 2) != 0) {
            str2 = str;
        }
        return kotlin.text.StringsKt.substringBeforeLast(str, c, str2);
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.String substringBeforeLast(@org.jetbrains.annotations.NotNull java.lang.String $receiver, char delimiter, @org.jetbrains.annotations.NotNull java.lang.String missingDelimiterValue) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(missingDelimiterValue, "missingDelimiterValue");
        int index = kotlin.text.StringsKt.lastIndexOf$default((java.lang.CharSequence) $receiver, delimiter, 0, false, 6, (java.lang.Object) null);
        if (index == -1) {
            return missingDelimiterValue;
        }
        java.lang.String missingDelimiterValue2 = $receiver.substring(0, index);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(missingDelimiterValue2, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        return missingDelimiterValue2;
    }

    @org.jetbrains.annotations.NotNull
    public static /* bridge */ /* synthetic */ java.lang.String substringBeforeLast$default(java.lang.String str, java.lang.String str2, java.lang.String str3, int i, java.lang.Object obj) {
        if ((i & 2) != 0) {
            str3 = str;
        }
        return kotlin.text.StringsKt.substringBeforeLast(str, str2, str3);
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.String substringBeforeLast(@org.jetbrains.annotations.NotNull java.lang.String $receiver, @org.jetbrains.annotations.NotNull java.lang.String delimiter, @org.jetbrains.annotations.NotNull java.lang.String missingDelimiterValue) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(delimiter, "delimiter");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(missingDelimiterValue, "missingDelimiterValue");
        int index = kotlin.text.StringsKt.lastIndexOf$default((java.lang.CharSequence) $receiver, delimiter, 0, false, 6, (java.lang.Object) null);
        if (index == -1) {
            return missingDelimiterValue;
        }
        java.lang.String missingDelimiterValue2 = $receiver.substring(0, index);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(missingDelimiterValue2, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        return missingDelimiterValue2;
    }

    @org.jetbrains.annotations.NotNull
    public static /* bridge */ /* synthetic */ java.lang.String substringAfterLast$default(java.lang.String str, char c, java.lang.String str2, int i, java.lang.Object obj) {
        if ((i & 2) != 0) {
            str2 = str;
        }
        return kotlin.text.StringsKt.substringAfterLast(str, c, str2);
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.String substringAfterLast(@org.jetbrains.annotations.NotNull java.lang.String $receiver, char delimiter, @org.jetbrains.annotations.NotNull java.lang.String missingDelimiterValue) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(missingDelimiterValue, "missingDelimiterValue");
        int index = kotlin.text.StringsKt.lastIndexOf$default((java.lang.CharSequence) $receiver, delimiter, 0, false, 6, (java.lang.Object) null);
        if (index == -1) {
            return missingDelimiterValue;
        }
        java.lang.String missingDelimiterValue2 = $receiver.substring(index + 1, $receiver.length());
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(missingDelimiterValue2, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        return missingDelimiterValue2;
    }

    @org.jetbrains.annotations.NotNull
    public static /* bridge */ /* synthetic */ java.lang.String substringAfterLast$default(java.lang.String str, java.lang.String str2, java.lang.String str3, int i, java.lang.Object obj) {
        if ((i & 2) != 0) {
            str3 = str;
        }
        return kotlin.text.StringsKt.substringAfterLast(str, str2, str3);
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.String substringAfterLast(@org.jetbrains.annotations.NotNull java.lang.String $receiver, @org.jetbrains.annotations.NotNull java.lang.String delimiter, @org.jetbrains.annotations.NotNull java.lang.String missingDelimiterValue) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(delimiter, "delimiter");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(missingDelimiterValue, "missingDelimiterValue");
        int index = kotlin.text.StringsKt.lastIndexOf$default((java.lang.CharSequence) $receiver, delimiter, 0, false, 6, (java.lang.Object) null);
        if (index == -1) {
            return missingDelimiterValue;
        }
        java.lang.String missingDelimiterValue2 = $receiver.substring(delimiter.length() + index, $receiver.length());
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(missingDelimiterValue2, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        return missingDelimiterValue2;
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.CharSequence replaceRange(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, int startIndex, int endIndex, @org.jetbrains.annotations.NotNull java.lang.CharSequence replacement) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(replacement, "replacement");
        if (endIndex < startIndex) {
            throw new java.lang.IndexOutOfBoundsException("End index (" + endIndex + ") is less than start index (" + startIndex + ").");
        }
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        sb.append($receiver, 0, startIndex);
        sb.append(replacement);
        sb.append($receiver, endIndex, $receiver.length());
        return sb;
    }

    @kotlin.internal.InlineOnly
    private static final java.lang.String replaceRange(@org.jetbrains.annotations.NotNull java.lang.String $receiver, int startIndex, int endIndex, java.lang.CharSequence replacement) {
        if ($receiver != null) {
            return kotlin.text.StringsKt.replaceRange((java.lang.CharSequence) $receiver, startIndex, endIndex, replacement).toString();
        }
        throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.CharSequence replaceRange(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull kotlin.ranges.IntRange range, @org.jetbrains.annotations.NotNull java.lang.CharSequence replacement) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(range, "range");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(replacement, "replacement");
        return kotlin.text.StringsKt.replaceRange($receiver, range.getStart().intValue(), range.getEndInclusive().intValue() + 1, replacement);
    }

    @kotlin.internal.InlineOnly
    private static final java.lang.String replaceRange(@org.jetbrains.annotations.NotNull java.lang.String $receiver, kotlin.ranges.IntRange range, java.lang.CharSequence replacement) {
        if ($receiver != null) {
            return kotlin.text.StringsKt.replaceRange((java.lang.CharSequence) $receiver, range, replacement).toString();
        }
        throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.CharSequence removeRange(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, int startIndex, int endIndex) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        if (endIndex < startIndex) {
            throw new java.lang.IndexOutOfBoundsException("End index (" + endIndex + ") is less than start index (" + startIndex + ").");
        } else if (endIndex == startIndex) {
            return $receiver.subSequence(0, $receiver.length());
        } else {
            java.lang.StringBuilder sb = new java.lang.StringBuilder($receiver.length() - (endIndex - startIndex));
            sb.append($receiver, 0, startIndex);
            sb.append($receiver, endIndex, $receiver.length());
            return sb;
        }
    }

    @kotlin.internal.InlineOnly
    private static final java.lang.String removeRange(@org.jetbrains.annotations.NotNull java.lang.String $receiver, int startIndex, int endIndex) {
        if ($receiver != null) {
            return kotlin.text.StringsKt.removeRange((java.lang.CharSequence) $receiver, startIndex, endIndex).toString();
        }
        throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.CharSequence removeRange(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull kotlin.ranges.IntRange range) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(range, "range");
        return kotlin.text.StringsKt.removeRange($receiver, range.getStart().intValue(), range.getEndInclusive().intValue() + 1);
    }

    @kotlin.internal.InlineOnly
    private static final java.lang.String removeRange(@org.jetbrains.annotations.NotNull java.lang.String $receiver, kotlin.ranges.IntRange range) {
        if ($receiver != null) {
            return kotlin.text.StringsKt.removeRange((java.lang.CharSequence) $receiver, range).toString();
        }
        throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.CharSequence removePrefix(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull java.lang.CharSequence prefix) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(prefix, "prefix");
        if (kotlin.text.StringsKt.startsWith$default($receiver, prefix, false, 2, (java.lang.Object) null)) {
            return $receiver.subSequence(prefix.length(), $receiver.length());
        }
        return $receiver.subSequence(0, $receiver.length());
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.String removePrefix(@org.jetbrains.annotations.NotNull java.lang.String $receiver, @org.jetbrains.annotations.NotNull java.lang.CharSequence prefix) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(prefix, "prefix");
        if (!kotlin.text.StringsKt.startsWith$default((java.lang.CharSequence) $receiver, prefix, false, 2, (java.lang.Object) null)) {
            return $receiver;
        }
        java.lang.String $receiver2 = $receiver.substring(prefix.length());
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull($receiver2, "(this as java.lang.String).substring(startIndex)");
        return $receiver2;
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.CharSequence removeSuffix(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull java.lang.CharSequence suffix) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(suffix, "suffix");
        if (kotlin.text.StringsKt.endsWith$default($receiver, suffix, false, 2, (java.lang.Object) null)) {
            return $receiver.subSequence(0, $receiver.length() - suffix.length());
        }
        return $receiver.subSequence(0, $receiver.length());
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.String removeSuffix(@org.jetbrains.annotations.NotNull java.lang.String $receiver, @org.jetbrains.annotations.NotNull java.lang.CharSequence suffix) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(suffix, "suffix");
        if (!kotlin.text.StringsKt.endsWith$default((java.lang.CharSequence) $receiver, suffix, false, 2, (java.lang.Object) null)) {
            return $receiver;
        }
        java.lang.String $receiver2 = $receiver.substring(0, $receiver.length() - suffix.length());
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull($receiver2, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        return $receiver2;
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.CharSequence removeSurrounding(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull java.lang.CharSequence prefix, @org.jetbrains.annotations.NotNull java.lang.CharSequence suffix) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(prefix, "prefix");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(suffix, "suffix");
        if ($receiver.length() < prefix.length() + suffix.length() || !kotlin.text.StringsKt.startsWith$default($receiver, prefix, false, 2, (java.lang.Object) null) || !kotlin.text.StringsKt.endsWith$default($receiver, suffix, false, 2, (java.lang.Object) null)) {
            return $receiver.subSequence(0, $receiver.length());
        }
        return $receiver.subSequence(prefix.length(), $receiver.length() - suffix.length());
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.String removeSurrounding(@org.jetbrains.annotations.NotNull java.lang.String $receiver, @org.jetbrains.annotations.NotNull java.lang.CharSequence prefix, @org.jetbrains.annotations.NotNull java.lang.CharSequence suffix) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(prefix, "prefix");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(suffix, "suffix");
        if ($receiver.length() < prefix.length() + suffix.length() || !kotlin.text.StringsKt.startsWith$default((java.lang.CharSequence) $receiver, prefix, false, 2, (java.lang.Object) null) || !kotlin.text.StringsKt.endsWith$default((java.lang.CharSequence) $receiver, suffix, false, 2, (java.lang.Object) null)) {
            return $receiver;
        }
        java.lang.String $receiver2 = $receiver.substring(prefix.length(), $receiver.length() - suffix.length());
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull($receiver2, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        return $receiver2;
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.CharSequence removeSurrounding(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull java.lang.CharSequence delimiter) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(delimiter, "delimiter");
        return kotlin.text.StringsKt.removeSurrounding($receiver, delimiter, delimiter);
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.String removeSurrounding(@org.jetbrains.annotations.NotNull java.lang.String $receiver, @org.jetbrains.annotations.NotNull java.lang.CharSequence delimiter) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(delimiter, "delimiter");
        return kotlin.text.StringsKt.removeSurrounding($receiver, delimiter, delimiter);
    }

    @org.jetbrains.annotations.NotNull
    public static /* bridge */ /* synthetic */ java.lang.String replaceBefore$default(java.lang.String str, char c, java.lang.String str2, java.lang.String str3, int i, java.lang.Object obj) {
        if ((i & 4) != 0) {
            str3 = str;
        }
        return kotlin.text.StringsKt.replaceBefore(str, c, str2, str3);
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.String replaceBefore(@org.jetbrains.annotations.NotNull java.lang.String $receiver, char delimiter, @org.jetbrains.annotations.NotNull java.lang.String replacement, @org.jetbrains.annotations.NotNull java.lang.String missingDelimiterValue) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(replacement, "replacement");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(missingDelimiterValue, "missingDelimiterValue");
        int index = kotlin.text.StringsKt.indexOf$default((java.lang.CharSequence) $receiver, delimiter, 0, false, 6, (java.lang.Object) null);
        return index == -1 ? missingDelimiterValue : kotlin.text.StringsKt.replaceRange((java.lang.CharSequence) $receiver, 0, index, (java.lang.CharSequence) replacement).toString();
    }

    @org.jetbrains.annotations.NotNull
    public static /* bridge */ /* synthetic */ java.lang.String replaceBefore$default(java.lang.String str, java.lang.String str2, java.lang.String str3, java.lang.String str4, int i, java.lang.Object obj) {
        if ((i & 4) != 0) {
            str4 = str;
        }
        return kotlin.text.StringsKt.replaceBefore(str, str2, str3, str4);
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.String replaceBefore(@org.jetbrains.annotations.NotNull java.lang.String $receiver, @org.jetbrains.annotations.NotNull java.lang.String delimiter, @org.jetbrains.annotations.NotNull java.lang.String replacement, @org.jetbrains.annotations.NotNull java.lang.String missingDelimiterValue) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(delimiter, "delimiter");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(replacement, "replacement");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(missingDelimiterValue, "missingDelimiterValue");
        int index = kotlin.text.StringsKt.indexOf$default((java.lang.CharSequence) $receiver, delimiter, 0, false, 6, (java.lang.Object) null);
        return index == -1 ? missingDelimiterValue : kotlin.text.StringsKt.replaceRange((java.lang.CharSequence) $receiver, 0, index, (java.lang.CharSequence) replacement).toString();
    }

    @org.jetbrains.annotations.NotNull
    public static /* bridge */ /* synthetic */ java.lang.String replaceAfter$default(java.lang.String str, char c, java.lang.String str2, java.lang.String str3, int i, java.lang.Object obj) {
        if ((i & 4) != 0) {
            str3 = str;
        }
        return kotlin.text.StringsKt.replaceAfter(str, c, str2, str3);
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.String replaceAfter(@org.jetbrains.annotations.NotNull java.lang.String $receiver, char delimiter, @org.jetbrains.annotations.NotNull java.lang.String replacement, @org.jetbrains.annotations.NotNull java.lang.String missingDelimiterValue) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(replacement, "replacement");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(missingDelimiterValue, "missingDelimiterValue");
        int index = kotlin.text.StringsKt.indexOf$default((java.lang.CharSequence) $receiver, delimiter, 0, false, 6, (java.lang.Object) null);
        if (index == -1) {
            return missingDelimiterValue;
        }
        return kotlin.text.StringsKt.replaceRange((java.lang.CharSequence) $receiver, index + 1, $receiver.length(), (java.lang.CharSequence) replacement).toString();
    }

    @org.jetbrains.annotations.NotNull
    public static /* bridge */ /* synthetic */ java.lang.String replaceAfter$default(java.lang.String str, java.lang.String str2, java.lang.String str3, java.lang.String str4, int i, java.lang.Object obj) {
        if ((i & 4) != 0) {
            str4 = str;
        }
        return kotlin.text.StringsKt.replaceAfter(str, str2, str3, str4);
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.String replaceAfter(@org.jetbrains.annotations.NotNull java.lang.String $receiver, @org.jetbrains.annotations.NotNull java.lang.String delimiter, @org.jetbrains.annotations.NotNull java.lang.String replacement, @org.jetbrains.annotations.NotNull java.lang.String missingDelimiterValue) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(delimiter, "delimiter");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(replacement, "replacement");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(missingDelimiterValue, "missingDelimiterValue");
        int index = kotlin.text.StringsKt.indexOf$default((java.lang.CharSequence) $receiver, delimiter, 0, false, 6, (java.lang.Object) null);
        if (index == -1) {
            return missingDelimiterValue;
        }
        return kotlin.text.StringsKt.replaceRange((java.lang.CharSequence) $receiver, delimiter.length() + index, $receiver.length(), (java.lang.CharSequence) replacement).toString();
    }

    @org.jetbrains.annotations.NotNull
    public static /* bridge */ /* synthetic */ java.lang.String replaceAfterLast$default(java.lang.String str, java.lang.String str2, java.lang.String str3, java.lang.String str4, int i, java.lang.Object obj) {
        if ((i & 4) != 0) {
            str4 = str;
        }
        return kotlin.text.StringsKt.replaceAfterLast(str, str2, str3, str4);
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.String replaceAfterLast(@org.jetbrains.annotations.NotNull java.lang.String $receiver, @org.jetbrains.annotations.NotNull java.lang.String delimiter, @org.jetbrains.annotations.NotNull java.lang.String replacement, @org.jetbrains.annotations.NotNull java.lang.String missingDelimiterValue) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(delimiter, "delimiter");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(replacement, "replacement");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(missingDelimiterValue, "missingDelimiterValue");
        int index = kotlin.text.StringsKt.lastIndexOf$default((java.lang.CharSequence) $receiver, delimiter, 0, false, 6, (java.lang.Object) null);
        if (index == -1) {
            return missingDelimiterValue;
        }
        return kotlin.text.StringsKt.replaceRange((java.lang.CharSequence) $receiver, delimiter.length() + index, $receiver.length(), (java.lang.CharSequence) replacement).toString();
    }

    @org.jetbrains.annotations.NotNull
    public static /* bridge */ /* synthetic */ java.lang.String replaceAfterLast$default(java.lang.String str, char c, java.lang.String str2, java.lang.String str3, int i, java.lang.Object obj) {
        if ((i & 4) != 0) {
            str3 = str;
        }
        return kotlin.text.StringsKt.replaceAfterLast(str, c, str2, str3);
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.String replaceAfterLast(@org.jetbrains.annotations.NotNull java.lang.String $receiver, char delimiter, @org.jetbrains.annotations.NotNull java.lang.String replacement, @org.jetbrains.annotations.NotNull java.lang.String missingDelimiterValue) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(replacement, "replacement");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(missingDelimiterValue, "missingDelimiterValue");
        int index = kotlin.text.StringsKt.lastIndexOf$default((java.lang.CharSequence) $receiver, delimiter, 0, false, 6, (java.lang.Object) null);
        if (index == -1) {
            return missingDelimiterValue;
        }
        return kotlin.text.StringsKt.replaceRange((java.lang.CharSequence) $receiver, index + 1, $receiver.length(), (java.lang.CharSequence) replacement).toString();
    }

    @org.jetbrains.annotations.NotNull
    public static /* bridge */ /* synthetic */ java.lang.String replaceBeforeLast$default(java.lang.String str, char c, java.lang.String str2, java.lang.String str3, int i, java.lang.Object obj) {
        if ((i & 4) != 0) {
            str3 = str;
        }
        return kotlin.text.StringsKt.replaceBeforeLast(str, c, str2, str3);
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.String replaceBeforeLast(@org.jetbrains.annotations.NotNull java.lang.String $receiver, char delimiter, @org.jetbrains.annotations.NotNull java.lang.String replacement, @org.jetbrains.annotations.NotNull java.lang.String missingDelimiterValue) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(replacement, "replacement");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(missingDelimiterValue, "missingDelimiterValue");
        int index = kotlin.text.StringsKt.lastIndexOf$default((java.lang.CharSequence) $receiver, delimiter, 0, false, 6, (java.lang.Object) null);
        return index == -1 ? missingDelimiterValue : kotlin.text.StringsKt.replaceRange((java.lang.CharSequence) $receiver, 0, index, (java.lang.CharSequence) replacement).toString();
    }

    @org.jetbrains.annotations.NotNull
    public static /* bridge */ /* synthetic */ java.lang.String replaceBeforeLast$default(java.lang.String str, java.lang.String str2, java.lang.String str3, java.lang.String str4, int i, java.lang.Object obj) {
        if ((i & 4) != 0) {
            str4 = str;
        }
        return kotlin.text.StringsKt.replaceBeforeLast(str, str2, str3, str4);
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.String replaceBeforeLast(@org.jetbrains.annotations.NotNull java.lang.String $receiver, @org.jetbrains.annotations.NotNull java.lang.String delimiter, @org.jetbrains.annotations.NotNull java.lang.String replacement, @org.jetbrains.annotations.NotNull java.lang.String missingDelimiterValue) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(delimiter, "delimiter");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(replacement, "replacement");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(missingDelimiterValue, "missingDelimiterValue");
        int index = kotlin.text.StringsKt.lastIndexOf$default((java.lang.CharSequence) $receiver, delimiter, 0, false, 6, (java.lang.Object) null);
        return index == -1 ? missingDelimiterValue : kotlin.text.StringsKt.replaceRange((java.lang.CharSequence) $receiver, 0, index, (java.lang.CharSequence) replacement).toString();
    }

    @kotlin.internal.InlineOnly
    private static final java.lang.String replace(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, kotlin.text.Regex regex, java.lang.String replacement) {
        return regex.replace($receiver, replacement);
    }

    @kotlin.internal.InlineOnly
    private static final java.lang.String replace(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, kotlin.text.Regex regex, kotlin.jvm.functions.Function1<? super kotlin.text.MatchResult, ? extends java.lang.CharSequence> transform) {
        return regex.replace($receiver, transform);
    }

    @kotlin.internal.InlineOnly
    private static final java.lang.String replaceFirst(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, kotlin.text.Regex regex, java.lang.String replacement) {
        return regex.replaceFirst($receiver, replacement);
    }

    @kotlin.internal.InlineOnly
    private static final boolean matches(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, kotlin.text.Regex regex) {
        return regex.matches($receiver);
    }

    public static final boolean regionMatchesImpl(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, int thisOffset, @org.jetbrains.annotations.NotNull java.lang.CharSequence other, int otherOffset, int length, boolean ignoreCase) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(other, "other");
        if (otherOffset < 0 || thisOffset < 0 || thisOffset > $receiver.length() - length || otherOffset > other.length() - length) {
            return false;
        }
        int i = length - 1;
        if (0 <= i) {
            int i2 = 0;
            while (kotlin.text.CharsKt.equals($receiver.charAt(thisOffset + i2), other.charAt(otherOffset + i2), ignoreCase)) {
                if (i2 != i) {
                    i2++;
                }
            }
            return false;
        }
        return true;
    }

    public static /* bridge */ /* synthetic */ boolean startsWith$default(java.lang.CharSequence charSequence, char c, boolean z, int i, java.lang.Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        return kotlin.text.StringsKt.startsWith(charSequence, c, z);
    }

    public static final boolean startsWith(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, char c, boolean ignoreCase) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        return $receiver.length() > 0 && kotlin.text.CharsKt.equals($receiver.charAt(0), c, ignoreCase);
    }

    public static /* bridge */ /* synthetic */ boolean endsWith$default(java.lang.CharSequence charSequence, char c, boolean z, int i, java.lang.Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        return kotlin.text.StringsKt.endsWith(charSequence, c, z);
    }

    public static final boolean endsWith(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, char c, boolean ignoreCase) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        return $receiver.length() > 0 && kotlin.text.CharsKt.equals($receiver.charAt(kotlin.text.StringsKt.getLastIndex($receiver)), c, ignoreCase);
    }

    public static /* bridge */ /* synthetic */ boolean startsWith$default(java.lang.CharSequence charSequence, java.lang.CharSequence charSequence2, boolean z, int i, java.lang.Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        return kotlin.text.StringsKt.startsWith(charSequence, charSequence2, z);
    }

    public static final boolean startsWith(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull java.lang.CharSequence prefix, boolean ignoreCase) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(prefix, "prefix");
        if (!ignoreCase && ($receiver instanceof java.lang.String) && (prefix instanceof java.lang.String)) {
            return kotlin.text.StringsKt.startsWith$default((java.lang.String) $receiver, (java.lang.String) prefix, false, 2, null);
        }
        return kotlin.text.StringsKt.regionMatchesImpl($receiver, 0, prefix, 0, prefix.length(), ignoreCase);
    }

    public static /* bridge */ /* synthetic */ boolean startsWith$default(java.lang.CharSequence charSequence, java.lang.CharSequence charSequence2, int i, boolean z, int i2, java.lang.Object obj) {
        if ((i2 & 4) != 0) {
            z = false;
        }
        return kotlin.text.StringsKt.startsWith(charSequence, charSequence2, i, z);
    }

    public static final boolean startsWith(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull java.lang.CharSequence prefix, int startIndex, boolean ignoreCase) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(prefix, "prefix");
        if (!ignoreCase && ($receiver instanceof java.lang.String) && (prefix instanceof java.lang.String)) {
            return kotlin.text.StringsKt.startsWith$default((java.lang.String) $receiver, (java.lang.String) prefix, startIndex, false, 4, null);
        }
        return kotlin.text.StringsKt.regionMatchesImpl($receiver, startIndex, prefix, 0, prefix.length(), ignoreCase);
    }

    public static /* bridge */ /* synthetic */ boolean endsWith$default(java.lang.CharSequence charSequence, java.lang.CharSequence charSequence2, boolean z, int i, java.lang.Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        return kotlin.text.StringsKt.endsWith(charSequence, charSequence2, z);
    }

    public static final boolean endsWith(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull java.lang.CharSequence suffix, boolean ignoreCase) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(suffix, "suffix");
        if (!ignoreCase && ($receiver instanceof java.lang.String) && (suffix instanceof java.lang.String)) {
            return kotlin.text.StringsKt.endsWith$default((java.lang.String) $receiver, (java.lang.String) suffix, false, 2, null);
        }
        return kotlin.text.StringsKt.regionMatchesImpl($receiver, $receiver.length() - suffix.length(), suffix, 0, suffix.length(), ignoreCase);
    }

    @org.jetbrains.annotations.NotNull
    public static /* bridge */ /* synthetic */ java.lang.String commonPrefixWith$default(java.lang.CharSequence charSequence, java.lang.CharSequence charSequence2, boolean z, int i, java.lang.Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        return kotlin.text.StringsKt.commonPrefixWith(charSequence, charSequence2, z);
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.String commonPrefixWith(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull java.lang.CharSequence other, boolean ignoreCase) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(other, "other");
        int shortestLength = java.lang.Math.min($receiver.length(), other.length());
        int i = 0;
        while (i < shortestLength && kotlin.text.CharsKt.equals($receiver.charAt(i), other.charAt(i), ignoreCase)) {
            i++;
        }
        if (kotlin.text.StringsKt.hasSurrogatePairAt($receiver, i - 1) || kotlin.text.StringsKt.hasSurrogatePairAt(other, i - 1)) {
            i--;
        }
        return $receiver.subSequence(0, i).toString();
    }

    @org.jetbrains.annotations.NotNull
    public static /* bridge */ /* synthetic */ java.lang.String commonSuffixWith$default(java.lang.CharSequence charSequence, java.lang.CharSequence charSequence2, boolean z, int i, java.lang.Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        return kotlin.text.StringsKt.commonSuffixWith(charSequence, charSequence2, z);
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.String commonSuffixWith(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull java.lang.CharSequence other, boolean ignoreCase) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(other, "other");
        int thisLength = $receiver.length();
        int otherLength = other.length();
        int shortestLength = java.lang.Math.min(thisLength, otherLength);
        int i = 0;
        while (i < shortestLength && kotlin.text.CharsKt.equals($receiver.charAt((thisLength - i) - 1), other.charAt((otherLength - i) - 1), ignoreCase)) {
            i++;
        }
        if (kotlin.text.StringsKt.hasSurrogatePairAt($receiver, (thisLength - i) - 1) || kotlin.text.StringsKt.hasSurrogatePairAt(other, (otherLength - i) - 1)) {
            i--;
        }
        return $receiver.subSequence(thisLength - i, thisLength).toString();
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0099  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x006f A[SYNTHETIC] */
    public static final kotlin.Pair<java.lang.Integer, java.lang.Character> findAnyOf$StringsKt__StringsKt(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, char[] chars, int startIndex, boolean ignoreCase, boolean last) {
        int matchingCharIndex;
        if (ignoreCase || chars.length != 1 || !($receiver instanceof java.lang.String)) {
            kotlin.ranges.IntProgression indices = !last ? new kotlin.ranges.IntRange(kotlin.ranges.RangesKt.coerceAtLeast(startIndex, 0), kotlin.text.StringsKt.getLastIndex($receiver)) : kotlin.ranges.RangesKt.downTo(kotlin.ranges.RangesKt.coerceAtMost(startIndex, kotlin.text.StringsKt.getLastIndex($receiver)), 0);
            int first = indices.getFirst();
            int last2 = indices.getLast();
            int step = indices.getStep();
            if (step <= 0 ? first >= last2 : first <= last2) {
                while (true) {
                    char charAtIndex = $receiver.charAt(first);
                    char[] $receiver$iv = chars;
                    int i = 0;
                    int length = $receiver$iv.length - 1;
                    if (0 <= length) {
                        while (true) {
                            if (!kotlin.text.CharsKt.equals($receiver$iv[i], charAtIndex, ignoreCase)) {
                                if (i == length) {
                                    break;
                                }
                                i++;
                            } else {
                                matchingCharIndex = i;
                                break;
                            }
                        }
                        if (matchingCharIndex >= 0) {
                            if (first == last2) {
                                break;
                            }
                            first += step;
                        } else {
                            return kotlin.TuplesKt.to(java.lang.Integer.valueOf(first), java.lang.Character.valueOf(chars[matchingCharIndex]));
                        }
                    }
                    matchingCharIndex = -1;
                    if (matchingCharIndex >= 0) {
                    }
                }
            }
            return null;
        }
        char single = kotlin.collections.ArraysKt.single(chars);
        int index = !last ? ((java.lang.String) $receiver).indexOf(single, startIndex) : ((java.lang.String) $receiver).lastIndexOf(single, startIndex);
        if (index < 0) {
            return null;
        }
        return kotlin.TuplesKt.to(java.lang.Integer.valueOf(index), java.lang.Character.valueOf(single));
    }

    public static /* bridge */ /* synthetic */ int indexOfAny$default(java.lang.CharSequence charSequence, char[] cArr, int i, boolean z, int i2, java.lang.Object obj) {
        if ((i2 & 2) != 0) {
            i = 0;
        }
        if ((i2 & 4) != 0) {
            z = false;
        }
        return kotlin.text.StringsKt.indexOfAny(charSequence, cArr, i, z);
    }

    public static final int indexOfAny(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull char[] chars, int startIndex, boolean ignoreCase) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(chars, "chars");
        kotlin.Pair findAnyOf$StringsKt__StringsKt = findAnyOf$StringsKt__StringsKt($receiver, chars, startIndex, ignoreCase, false);
        if (findAnyOf$StringsKt__StringsKt != null) {
            java.lang.Integer num = (java.lang.Integer) findAnyOf$StringsKt__StringsKt.getFirst();
            if (num != null) {
                return num.intValue();
            }
        }
        return -1;
    }

    public static /* bridge */ /* synthetic */ int lastIndexOfAny$default(java.lang.CharSequence charSequence, char[] cArr, int i, boolean z, int i2, java.lang.Object obj) {
        if ((i2 & 2) != 0) {
            i = kotlin.text.StringsKt.getLastIndex(charSequence);
        }
        if ((i2 & 4) != 0) {
            z = false;
        }
        return kotlin.text.StringsKt.lastIndexOfAny(charSequence, cArr, i, z);
    }

    public static final int lastIndexOfAny(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull char[] chars, int startIndex, boolean ignoreCase) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(chars, "chars");
        kotlin.Pair findAnyOf$StringsKt__StringsKt = findAnyOf$StringsKt__StringsKt($receiver, chars, startIndex, ignoreCase, true);
        if (findAnyOf$StringsKt__StringsKt != null) {
            java.lang.Integer num = (java.lang.Integer) findAnyOf$StringsKt__StringsKt.getFirst();
            if (num != null) {
                return num.intValue();
            }
        }
        return -1;
    }

    static /* bridge */ /* synthetic */ int indexOf$StringsKt__StringsKt$default(java.lang.CharSequence charSequence, java.lang.CharSequence charSequence2, int i, int i2, boolean z, boolean z2, int i3, java.lang.Object obj) {
        return indexOf$StringsKt__StringsKt(charSequence, charSequence2, i, i2, z, (i3 & 16) != 0 ? false : z2);
    }

    private static final int indexOf$StringsKt__StringsKt(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, java.lang.CharSequence other, int startIndex, int endIndex, boolean ignoreCase, boolean last) {
        kotlin.ranges.IntProgression indices;
        if (!last) {
            indices = new kotlin.ranges.IntRange(kotlin.ranges.RangesKt.coerceAtLeast(startIndex, 0), kotlin.ranges.RangesKt.coerceAtMost(endIndex, $receiver.length()));
        } else {
            indices = kotlin.ranges.RangesKt.downTo(kotlin.ranges.RangesKt.coerceAtMost(startIndex, kotlin.text.StringsKt.getLastIndex($receiver)), kotlin.ranges.RangesKt.coerceAtLeast(endIndex, 0));
        }
        if (!($receiver instanceof java.lang.String) || !(other instanceof java.lang.String)) {
            int first = indices.getFirst();
            int last2 = indices.getLast();
            int step = indices.getStep();
            if (step <= 0 ? first >= last2 : first <= last2) {
                while (true) {
                    if (!kotlin.text.StringsKt.regionMatchesImpl(other, 0, $receiver, first, other.length(), ignoreCase)) {
                        if (first == last2) {
                            break;
                        }
                        first += step;
                    } else {
                        return first;
                    }
                }
            }
        } else {
            int first2 = indices.getFirst();
            int last3 = indices.getLast();
            int step2 = indices.getStep();
            if (step2 <= 0 ? first2 >= last3 : first2 <= last3) {
                while (!kotlin.text.StringsKt.regionMatches((java.lang.String) other, 0, (java.lang.String) $receiver, first2, other.length(), ignoreCase)) {
                    if (first2 != last3) {
                        first2 += step2;
                    }
                }
                return first2;
            }
        }
        return -1;
    }

    /* access modifiers changed from: private */
    public static final kotlin.Pair<java.lang.Integer, java.lang.String> findAnyOf$StringsKt__StringsKt(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, java.util.Collection<java.lang.String> strings, int startIndex, boolean ignoreCase, boolean last) {
        java.lang.Object obj;
        java.lang.Object obj2;
        if (ignoreCase || strings.size() != 1) {
            kotlin.ranges.IntProgression indices = !last ? new kotlin.ranges.IntRange(kotlin.ranges.RangesKt.coerceAtLeast(startIndex, 0), $receiver.length()) : kotlin.ranges.RangesKt.downTo(kotlin.ranges.RangesKt.coerceAtMost(startIndex, kotlin.text.StringsKt.getLastIndex($receiver)), 0);
            if ($receiver instanceof java.lang.String) {
                int first = indices.getFirst();
                int last2 = indices.getLast();
                int step = indices.getStep();
                if (step <= 0 ? first >= last2 : first <= last2) {
                    while (true) {
                        java.util.Iterator it = strings.iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                obj2 = null;
                                break;
                            }
                            java.lang.Object element$iv = it.next();
                            java.lang.String it2 = (java.lang.String) element$iv;
                            if (kotlin.text.StringsKt.regionMatches(it2, 0, (java.lang.String) $receiver, first, it2.length(), ignoreCase)) {
                                obj2 = element$iv;
                                break;
                            }
                        }
                        java.lang.String matchingString = (java.lang.String) obj2;
                        if (matchingString == null) {
                            if (first == last2) {
                                break;
                            }
                            first += step;
                        } else {
                            return kotlin.TuplesKt.to(java.lang.Integer.valueOf(first), matchingString);
                        }
                    }
                }
            } else {
                int first2 = indices.getFirst();
                int last3 = indices.getLast();
                int step2 = indices.getStep();
                if (step2 <= 0 ? first2 >= last3 : first2 <= last3) {
                    while (true) {
                        java.util.Iterator it3 = strings.iterator();
                        while (true) {
                            if (!it3.hasNext()) {
                                obj = null;
                                break;
                            }
                            java.lang.Object element$iv2 = it3.next();
                            java.lang.String it4 = (java.lang.String) element$iv2;
                            if (kotlin.text.StringsKt.regionMatchesImpl(it4, 0, $receiver, first2, it4.length(), ignoreCase)) {
                                obj = element$iv2;
                                break;
                            }
                        }
                        java.lang.String matchingString2 = (java.lang.String) obj;
                        if (matchingString2 == null) {
                            if (first2 == last3) {
                                break;
                            }
                            first2 += step2;
                        } else {
                            return kotlin.TuplesKt.to(java.lang.Integer.valueOf(first2), matchingString2);
                        }
                    }
                }
            }
            return null;
        }
        java.lang.String string = (java.lang.String) kotlin.collections.CollectionsKt.single((java.lang.Iterable<? extends T>) strings);
        int index = !last ? kotlin.text.StringsKt.indexOf$default($receiver, string, startIndex, false, 4, (java.lang.Object) null) : kotlin.text.StringsKt.lastIndexOf$default($receiver, string, startIndex, false, 4, (java.lang.Object) null);
        if (index < 0) {
            return null;
        }
        return kotlin.TuplesKt.to(java.lang.Integer.valueOf(index), string);
    }

    @org.jetbrains.annotations.Nullable
    public static /* bridge */ /* synthetic */ kotlin.Pair findAnyOf$default(java.lang.CharSequence charSequence, java.util.Collection collection, int i, boolean z, int i2, java.lang.Object obj) {
        if ((i2 & 2) != 0) {
            i = 0;
        }
        if ((i2 & 4) != 0) {
            z = false;
        }
        return kotlin.text.StringsKt.findAnyOf(charSequence, collection, i, z);
    }

    @org.jetbrains.annotations.Nullable
    public static final kotlin.Pair<java.lang.Integer, java.lang.String> findAnyOf(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull java.util.Collection<java.lang.String> strings, int startIndex, boolean ignoreCase) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(strings, "strings");
        return findAnyOf$StringsKt__StringsKt($receiver, strings, startIndex, ignoreCase, false);
    }

    @org.jetbrains.annotations.Nullable
    public static /* bridge */ /* synthetic */ kotlin.Pair findLastAnyOf$default(java.lang.CharSequence charSequence, java.util.Collection collection, int i, boolean z, int i2, java.lang.Object obj) {
        if ((i2 & 2) != 0) {
            i = kotlin.text.StringsKt.getLastIndex(charSequence);
        }
        if ((i2 & 4) != 0) {
            z = false;
        }
        return kotlin.text.StringsKt.findLastAnyOf(charSequence, collection, i, z);
    }

    @org.jetbrains.annotations.Nullable
    public static final kotlin.Pair<java.lang.Integer, java.lang.String> findLastAnyOf(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull java.util.Collection<java.lang.String> strings, int startIndex, boolean ignoreCase) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(strings, "strings");
        return findAnyOf$StringsKt__StringsKt($receiver, strings, startIndex, ignoreCase, true);
    }

    public static /* bridge */ /* synthetic */ int indexOfAny$default(java.lang.CharSequence charSequence, java.util.Collection collection, int i, boolean z, int i2, java.lang.Object obj) {
        if ((i2 & 2) != 0) {
            i = 0;
        }
        if ((i2 & 4) != 0) {
            z = false;
        }
        return kotlin.text.StringsKt.indexOfAny(charSequence, collection, i, z);
    }

    public static final int indexOfAny(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull java.util.Collection<java.lang.String> strings, int startIndex, boolean ignoreCase) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(strings, "strings");
        kotlin.Pair findAnyOf$StringsKt__StringsKt = findAnyOf$StringsKt__StringsKt($receiver, strings, startIndex, ignoreCase, false);
        if (findAnyOf$StringsKt__StringsKt != null) {
            java.lang.Integer num = (java.lang.Integer) findAnyOf$StringsKt__StringsKt.getFirst();
            if (num != null) {
                return num.intValue();
            }
        }
        return -1;
    }

    public static /* bridge */ /* synthetic */ int lastIndexOfAny$default(java.lang.CharSequence charSequence, java.util.Collection collection, int i, boolean z, int i2, java.lang.Object obj) {
        if ((i2 & 2) != 0) {
            i = kotlin.text.StringsKt.getLastIndex(charSequence);
        }
        if ((i2 & 4) != 0) {
            z = false;
        }
        return kotlin.text.StringsKt.lastIndexOfAny(charSequence, collection, i, z);
    }

    public static final int lastIndexOfAny(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull java.util.Collection<java.lang.String> strings, int startIndex, boolean ignoreCase) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(strings, "strings");
        kotlin.Pair findAnyOf$StringsKt__StringsKt = findAnyOf$StringsKt__StringsKt($receiver, strings, startIndex, ignoreCase, true);
        if (findAnyOf$StringsKt__StringsKt != null) {
            java.lang.Integer num = (java.lang.Integer) findAnyOf$StringsKt__StringsKt.getFirst();
            if (num != null) {
                return num.intValue();
            }
        }
        return -1;
    }

    public static /* bridge */ /* synthetic */ int indexOf$default(java.lang.CharSequence charSequence, char c, int i, boolean z, int i2, java.lang.Object obj) {
        if ((i2 & 2) != 0) {
            i = 0;
        }
        if ((i2 & 4) != 0) {
            z = false;
        }
        return kotlin.text.StringsKt.indexOf(charSequence, c, i, z);
    }

    public static final int indexOf(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, char c, int startIndex, boolean ignoreCase) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        if (!ignoreCase && ($receiver instanceof java.lang.String)) {
            return ((java.lang.String) $receiver).indexOf(c, startIndex);
        }
        return kotlin.text.StringsKt.indexOfAny($receiver, new char[]{c}, startIndex, ignoreCase);
    }

    public static /* bridge */ /* synthetic */ int indexOf$default(java.lang.CharSequence charSequence, java.lang.String str, int i, boolean z, int i2, java.lang.Object obj) {
        if ((i2 & 2) != 0) {
            i = 0;
        }
        if ((i2 & 4) != 0) {
            z = false;
        }
        return kotlin.text.StringsKt.indexOf(charSequence, str, i, z);
    }

    public static final int indexOf(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull java.lang.String string, int startIndex, boolean ignoreCase) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(string, "string");
        if (!ignoreCase && ($receiver instanceof java.lang.String)) {
            return ((java.lang.String) $receiver).indexOf(string, startIndex);
        }
        return indexOf$StringsKt__StringsKt$default($receiver, string, startIndex, $receiver.length(), ignoreCase, false, 16, null);
    }

    public static /* bridge */ /* synthetic */ int lastIndexOf$default(java.lang.CharSequence charSequence, char c, int i, boolean z, int i2, java.lang.Object obj) {
        if ((i2 & 2) != 0) {
            i = kotlin.text.StringsKt.getLastIndex(charSequence);
        }
        if ((i2 & 4) != 0) {
            z = false;
        }
        return kotlin.text.StringsKt.lastIndexOf(charSequence, c, i, z);
    }

    public static final int lastIndexOf(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, char c, int startIndex, boolean ignoreCase) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        if (!ignoreCase && ($receiver instanceof java.lang.String)) {
            return ((java.lang.String) $receiver).lastIndexOf(c, startIndex);
        }
        return kotlin.text.StringsKt.lastIndexOfAny($receiver, new char[]{c}, startIndex, ignoreCase);
    }

    public static /* bridge */ /* synthetic */ int lastIndexOf$default(java.lang.CharSequence charSequence, java.lang.String str, int i, boolean z, int i2, java.lang.Object obj) {
        if ((i2 & 2) != 0) {
            i = kotlin.text.StringsKt.getLastIndex(charSequence);
        }
        if ((i2 & 4) != 0) {
            z = false;
        }
        return kotlin.text.StringsKt.lastIndexOf(charSequence, str, i, z);
    }

    public static final int lastIndexOf(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull java.lang.String string, int startIndex, boolean ignoreCase) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(string, "string");
        if (!ignoreCase && ($receiver instanceof java.lang.String)) {
            return ((java.lang.String) $receiver).lastIndexOf(string, startIndex);
        }
        return indexOf$StringsKt__StringsKt($receiver, string, startIndex, 0, ignoreCase, true);
    }

    public static /* bridge */ /* synthetic */ boolean contains$default(java.lang.CharSequence charSequence, java.lang.CharSequence charSequence2, boolean z, int i, java.lang.Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        return kotlin.text.StringsKt.contains(charSequence, charSequence2, z);
    }

    public static final boolean contains(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull java.lang.CharSequence other, boolean ignoreCase) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(other, "other");
        if (other instanceof java.lang.String) {
            return kotlin.text.StringsKt.indexOf$default($receiver, (java.lang.String) other, 0, ignoreCase, 2, (java.lang.Object) null) >= 0;
        }
        return indexOf$StringsKt__StringsKt$default($receiver, other, 0, $receiver.length(), ignoreCase, false, 16, null) >= 0;
    }

    public static /* bridge */ /* synthetic */ boolean contains$default(java.lang.CharSequence charSequence, char c, boolean z, int i, java.lang.Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        return kotlin.text.StringsKt.contains(charSequence, c, z);
    }

    public static final boolean contains(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, char c, boolean ignoreCase) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        return kotlin.text.StringsKt.indexOf$default($receiver, c, 0, ignoreCase, 2, (java.lang.Object) null) >= 0;
    }

    @kotlin.internal.InlineOnly
    private static final boolean contains(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, kotlin.text.Regex regex) {
        return regex.containsMatchIn($receiver);
    }

    static /* bridge */ /* synthetic */ kotlin.sequences.Sequence rangesDelimitedBy$StringsKt__StringsKt$default(java.lang.CharSequence charSequence, char[] cArr, int i, boolean z, int i2, int i3, java.lang.Object obj) {
        if ((i3 & 2) != 0) {
            i = 0;
        }
        if ((i3 & 4) != 0) {
            z = false;
        }
        if ((i3 & 8) != 0) {
            i2 = 0;
        }
        return rangesDelimitedBy$StringsKt__StringsKt(charSequence, cArr, i, z, i2);
    }

    private static final kotlin.sequences.Sequence<kotlin.ranges.IntRange> rangesDelimitedBy$StringsKt__StringsKt(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, char[] delimiters, int startIndex, boolean ignoreCase, int limit) {
        if (limit >= 0) {
            return new kotlin.text.DelimitedRangesSequence<>($receiver, startIndex, limit, new kotlin.text.StringsKt__StringsKt$rangesDelimitedBy$2(delimiters, ignoreCase));
        }
        throw new java.lang.IllegalArgumentException(("Limit must be non-negative, but was " + limit + ".").toString());
    }

    static /* bridge */ /* synthetic */ kotlin.sequences.Sequence rangesDelimitedBy$StringsKt__StringsKt$default(java.lang.CharSequence charSequence, java.lang.String[] strArr, int i, boolean z, int i2, int i3, java.lang.Object obj) {
        if ((i3 & 2) != 0) {
            i = 0;
        }
        if ((i3 & 4) != 0) {
            z = false;
        }
        if ((i3 & 8) != 0) {
            i2 = 0;
        }
        return rangesDelimitedBy$StringsKt__StringsKt(charSequence, strArr, i, z, i2);
    }

    private static final kotlin.sequences.Sequence<kotlin.ranges.IntRange> rangesDelimitedBy$StringsKt__StringsKt(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, java.lang.String[] delimiters, int startIndex, boolean ignoreCase, int limit) {
        if (limit >= 0) {
            return new kotlin.text.DelimitedRangesSequence<>($receiver, startIndex, limit, new kotlin.text.StringsKt__StringsKt$rangesDelimitedBy$4(kotlin.collections.ArraysKt.asList((T[]) (java.lang.Object[]) delimiters), ignoreCase));
        }
        throw new java.lang.IllegalArgumentException(("Limit must be non-negative, but was " + limit + ".").toString());
    }

    @org.jetbrains.annotations.NotNull
    public static /* bridge */ /* synthetic */ kotlin.sequences.Sequence splitToSequence$default(java.lang.CharSequence charSequence, java.lang.String[] strArr, boolean z, int i, int i2, java.lang.Object obj) {
        if ((i2 & 2) != 0) {
            z = false;
        }
        if ((i2 & 4) != 0) {
            i = 0;
        }
        return kotlin.text.StringsKt.splitToSequence(charSequence, strArr, z, i);
    }

    @org.jetbrains.annotations.NotNull
    public static final kotlin.sequences.Sequence<java.lang.String> splitToSequence(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull java.lang.String[] delimiters, boolean ignoreCase, int limit) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(delimiters, "delimiters");
        return kotlin.sequences.SequencesKt.map(rangesDelimitedBy$StringsKt__StringsKt$default($receiver, delimiters, 0, ignoreCase, limit, 2, (java.lang.Object) null), new kotlin.text.StringsKt__StringsKt$splitToSequence$1($receiver));
    }

    @org.jetbrains.annotations.NotNull
    public static /* bridge */ /* synthetic */ java.util.List split$default(java.lang.CharSequence charSequence, java.lang.String[] strArr, boolean z, int i, int i2, java.lang.Object obj) {
        if ((i2 & 2) != 0) {
            z = false;
        }
        if ((i2 & 4) != 0) {
            i = 0;
        }
        return kotlin.text.StringsKt.split(charSequence, strArr, z, i);
    }

    @org.jetbrains.annotations.NotNull
    public static final java.util.List<java.lang.String> split(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull java.lang.String[] delimiters, boolean ignoreCase, int limit) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(delimiters, "delimiters");
        java.lang.Iterable<kotlin.ranges.IntRange> $receiver$iv = kotlin.sequences.SequencesKt.asIterable(rangesDelimitedBy$StringsKt__StringsKt$default($receiver, delimiters, 0, ignoreCase, limit, 2, (java.lang.Object) null));
        java.util.Collection destination$iv$iv = new java.util.ArrayList(kotlin.collections.CollectionsKt.collectionSizeOrDefault($receiver$iv, 10));
        for (kotlin.ranges.IntRange substring : $receiver$iv) {
            destination$iv$iv.add(kotlin.text.StringsKt.substring($receiver, substring));
        }
        return (java.util.List) destination$iv$iv;
    }

    @org.jetbrains.annotations.NotNull
    public static /* bridge */ /* synthetic */ kotlin.sequences.Sequence splitToSequence$default(java.lang.CharSequence charSequence, char[] cArr, boolean z, int i, int i2, java.lang.Object obj) {
        if ((i2 & 2) != 0) {
            z = false;
        }
        if ((i2 & 4) != 0) {
            i = 0;
        }
        return kotlin.text.StringsKt.splitToSequence(charSequence, cArr, z, i);
    }

    @org.jetbrains.annotations.NotNull
    public static final kotlin.sequences.Sequence<java.lang.String> splitToSequence(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull char[] delimiters, boolean ignoreCase, int limit) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(delimiters, "delimiters");
        return kotlin.sequences.SequencesKt.map(rangesDelimitedBy$StringsKt__StringsKt$default($receiver, delimiters, 0, ignoreCase, limit, 2, (java.lang.Object) null), new kotlin.text.StringsKt__StringsKt$splitToSequence$2($receiver));
    }

    @org.jetbrains.annotations.NotNull
    public static /* bridge */ /* synthetic */ java.util.List split$default(java.lang.CharSequence charSequence, char[] cArr, boolean z, int i, int i2, java.lang.Object obj) {
        if ((i2 & 2) != 0) {
            z = false;
        }
        if ((i2 & 4) != 0) {
            i = 0;
        }
        return kotlin.text.StringsKt.split(charSequence, cArr, z, i);
    }

    @org.jetbrains.annotations.NotNull
    public static final java.util.List<java.lang.String> split(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull char[] delimiters, boolean ignoreCase, int limit) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(delimiters, "delimiters");
        java.lang.Iterable<kotlin.ranges.IntRange> $receiver$iv = kotlin.sequences.SequencesKt.asIterable(rangesDelimitedBy$StringsKt__StringsKt$default($receiver, delimiters, 0, ignoreCase, limit, 2, (java.lang.Object) null));
        java.util.Collection destination$iv$iv = new java.util.ArrayList(kotlin.collections.CollectionsKt.collectionSizeOrDefault($receiver$iv, 10));
        for (kotlin.ranges.IntRange substring : $receiver$iv) {
            destination$iv$iv.add(kotlin.text.StringsKt.substring($receiver, substring));
        }
        return (java.util.List) destination$iv$iv;
    }

    @kotlin.internal.InlineOnly
    private static final java.util.List<java.lang.String> split(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, kotlin.text.Regex regex, int limit) {
        return regex.split($receiver, limit);
    }

    @kotlin.internal.InlineOnly
    static /* bridge */ /* synthetic */ java.util.List split$default(java.lang.CharSequence $receiver, kotlin.text.Regex regex, int limit, int i, java.lang.Object obj) {
        if ((i & 2) != 0) {
            limit = 0;
        }
        return regex.split($receiver, limit);
    }

    @org.jetbrains.annotations.NotNull
    public static final kotlin.sequences.Sequence<java.lang.String> lineSequence(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        return kotlin.text.StringsKt.splitToSequence$default($receiver, new java.lang.String[]{"\r\n", "\n", "\r"}, false, 0, 6, (java.lang.Object) null);
    }

    @org.jetbrains.annotations.NotNull
    public static final java.util.List<java.lang.String> lines(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        return kotlin.sequences.SequencesKt.toList(kotlin.text.StringsKt.lineSequence($receiver));
    }
}
