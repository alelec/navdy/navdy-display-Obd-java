package kotlin.text;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\f\u001a\u00020\u0005H\u00c6\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0011\u001a\u00020\u0012H\u00d6\u0001J\t\u0010\u0013\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n\u00a8\u0006\u0014"}, d2 = {"Lkotlin/text/MatchGroup;", "", "value", "", "range", "Lkotlin/ranges/IntRange;", "(Ljava/lang/String;Lkotlin/ranges/IntRange;)V", "getRange", "()Lkotlin/ranges/IntRange;", "getValue", "()Ljava/lang/String;", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "kotlin-stdlib"}, k = 1, mv = {1, 1, 6})
/* compiled from: Regex.kt */
public final class MatchGroup {
    @org.jetbrains.annotations.NotNull
    private final kotlin.ranges.IntRange range;
    @org.jetbrains.annotations.NotNull
    private final java.lang.String value;

    @org.jetbrains.annotations.NotNull
    public static /* bridge */ /* synthetic */ kotlin.text.MatchGroup copy$default(kotlin.text.MatchGroup matchGroup, java.lang.String str, kotlin.ranges.IntRange intRange, int i, java.lang.Object obj) {
        if ((i & 1) != 0) {
            str = matchGroup.value;
        }
        if ((i & 2) != 0) {
            intRange = matchGroup.range;
        }
        return matchGroup.copy(str, intRange);
    }

    @org.jetbrains.annotations.NotNull
    public final java.lang.String component1() {
        return this.value;
    }

    @org.jetbrains.annotations.NotNull
    public final kotlin.ranges.IntRange component2() {
        return this.range;
    }

    @org.jetbrains.annotations.NotNull
    public final kotlin.text.MatchGroup copy(@org.jetbrains.annotations.NotNull java.lang.String value2, @org.jetbrains.annotations.NotNull kotlin.ranges.IntRange range2) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(value2, "value");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(range2, "range");
        return new kotlin.text.MatchGroup(value2, range2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x001a, code lost:
        if (kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) r2.range, (java.lang.Object) r3.range) != false) goto L_0x001c;
     */
    public boolean equals(java.lang.Object obj) {
        if (this != obj) {
            if (obj instanceof kotlin.text.MatchGroup) {
                kotlin.text.MatchGroup matchGroup = (kotlin.text.MatchGroup) obj;
                if (kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) this.value, (java.lang.Object) matchGroup.value)) {
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i = 0;
        java.lang.String str = this.value;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        kotlin.ranges.IntRange intRange = this.range;
        if (intRange != null) {
            i = intRange.hashCode();
        }
        return hashCode + i;
    }

    public java.lang.String toString() {
        return "MatchGroup(value=" + this.value + ", range=" + this.range + ")";
    }

    public MatchGroup(@org.jetbrains.annotations.NotNull java.lang.String value2, @org.jetbrains.annotations.NotNull kotlin.ranges.IntRange range2) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(value2, "value");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(range2, "range");
        this.value = value2;
        this.range = range2;
    }

    @org.jetbrains.annotations.NotNull
    public final kotlin.ranges.IntRange getRange() {
        return this.range;
    }

    @org.jetbrains.annotations.NotNull
    public final java.lang.String getValue() {
        return this.value;
    }
}
