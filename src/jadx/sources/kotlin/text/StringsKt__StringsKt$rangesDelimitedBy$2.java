package kotlin.text;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\u0010\r\n\u0002\b\u0002\u0010\u0000\u001a\u0010\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0002\u0018\u00010\u0001*\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0002H\n\u00a2\u0006\u0002\b\u0005"}, d2 = {"<anonymous>", "Lkotlin/Pair;", "", "", "startIndex", "invoke"}, k = 3, mv = {1, 1, 6})
/* compiled from: Strings.kt */
final class StringsKt__StringsKt$rangesDelimitedBy$2 extends kotlin.jvm.internal.Lambda implements kotlin.jvm.functions.Function2<java.lang.CharSequence, java.lang.Integer, kotlin.Pair<? extends java.lang.Integer, ? extends java.lang.Integer>> {
    final /* synthetic */ char[] $delimiters;
    final /* synthetic */ boolean $ignoreCase;

    StringsKt__StringsKt$rangesDelimitedBy$2(char[] cArr, boolean z) {
        this.$delimiters = cArr;
        this.$ignoreCase = z;
        super(2);
    }

    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return invoke((java.lang.CharSequence) obj, ((java.lang.Number) obj2).intValue());
    }

    @org.jetbrains.annotations.Nullable
    public final kotlin.Pair<java.lang.Integer, java.lang.Integer> invoke(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, int startIndex) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.Pair it = kotlin.text.StringsKt__StringsKt.findAnyOf$StringsKt__StringsKt($receiver, this.$delimiters, startIndex, this.$ignoreCase, false);
        if (it != null) {
            return kotlin.TuplesKt.to(it.getFirst(), java.lang.Integer.valueOf(1));
        }
        return null;
    }
}
