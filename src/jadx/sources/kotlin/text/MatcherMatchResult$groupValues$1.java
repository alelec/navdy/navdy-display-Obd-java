package kotlin.text;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0019\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005*\u0001\u0000\b\n\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0011\u0010\b\u001a\u00020\u00022\u0006\u0010\t\u001a\u00020\u0005H\u0096\u0002R\u0014\u0010\u0004\u001a\u00020\u00058VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0006\u0010\u0007\u00a8\u0006\n"}, d2 = {"kotlin/text/MatcherMatchResult$groupValues$1", "Lkotlin/collections/AbstractList;", "", "(Lkotlin/text/MatcherMatchResult;)V", "size", "", "getSize", "()I", "get", "index", "kotlin-stdlib"}, k = 1, mv = {1, 1, 6})
/* compiled from: Regex.kt */
public final class MatcherMatchResult$groupValues$1 extends kotlin.collections.AbstractList<java.lang.String> {
    final /* synthetic */ kotlin.text.MatcherMatchResult this$0;

    MatcherMatchResult$groupValues$1(kotlin.text.MatcherMatchResult $outer) {
        this.this$0 = $outer;
    }

    public final /* bridge */ boolean contains(java.lang.Object obj) {
        if (obj instanceof java.lang.String) {
            return contains((java.lang.String) obj);
        }
        return false;
    }

    public /* bridge */ boolean contains(java.lang.String str) {
        return super.contains(str);
    }

    public final /* bridge */ int indexOf(java.lang.Object obj) {
        if (obj instanceof java.lang.String) {
            return indexOf((java.lang.String) obj);
        }
        return -1;
    }

    public /* bridge */ int indexOf(java.lang.String str) {
        return super.indexOf(str);
    }

    public final /* bridge */ int lastIndexOf(java.lang.Object obj) {
        if (obj instanceof java.lang.String) {
            return lastIndexOf((java.lang.String) obj);
        }
        return -1;
    }

    public /* bridge */ int lastIndexOf(java.lang.String str) {
        return super.lastIndexOf(str);
    }

    public int getSize() {
        return this.this$0.matchResult.groupCount() + 1;
    }

    @org.jetbrains.annotations.NotNull
    public java.lang.String get(int index) {
        java.lang.String group = this.this$0.matchResult.group(index);
        return group != null ? group : "";
    }
}
