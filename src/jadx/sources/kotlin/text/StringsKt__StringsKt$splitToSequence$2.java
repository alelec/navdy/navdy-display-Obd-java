package kotlin.text;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "", "it", "Lkotlin/ranges/IntRange;", "invoke"}, k = 3, mv = {1, 1, 6})
/* compiled from: Strings.kt */
final class StringsKt__StringsKt$splitToSequence$2 extends kotlin.jvm.internal.Lambda implements kotlin.jvm.functions.Function1<kotlin.ranges.IntRange, java.lang.String> {
    final /* synthetic */ java.lang.CharSequence receiver$0;

    StringsKt__StringsKt$splitToSequence$2(java.lang.CharSequence charSequence) {
        this.receiver$0 = charSequence;
        super(1);
    }

    @org.jetbrains.annotations.NotNull
    public final java.lang.String invoke(@org.jetbrains.annotations.NotNull kotlin.ranges.IntRange it) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(it, "it");
        return kotlin.text.StringsKt.substring(this.receiver$0, it);
    }
}
