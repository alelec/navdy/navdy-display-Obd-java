package kotlin.text;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\n\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0001H\n\u00a2\u0006\u0002\b\u0003"}, d2 = {"<anonymous>", "", "it", "invoke"}, k = 3, mv = {1, 1, 6})
/* compiled from: Indent.kt */
final class StringsKt__IndentKt$prependIndent$1 extends kotlin.jvm.internal.Lambda implements kotlin.jvm.functions.Function1<java.lang.String, java.lang.String> {
    final /* synthetic */ java.lang.String $indent;

    StringsKt__IndentKt$prependIndent$1(java.lang.String str) {
        this.$indent = str;
        super(1);
    }

    @org.jetbrains.annotations.NotNull
    public final java.lang.String invoke(@org.jetbrains.annotations.NotNull java.lang.String it) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(it, "it");
        if (!kotlin.text.StringsKt.isBlank(it)) {
            return this.$indent + it;
        }
        if (it.length() < this.$indent.length()) {
            return this.$indent;
        }
        return it;
    }
}
