package kotlin.text;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000/\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010(\n\u0000*\u0001\u0000\b\n\u0018\u00002\u00020\u00012\n\u0012\u0006\u0012\u0004\u0018\u00010\u00030\u0002B\u0005\u00a2\u0006\u0002\u0010\u0004J\u0013\u0010\t\u001a\u0004\u0018\u00010\u00032\u0006\u0010\n\u001a\u00020\u0006H\u0096\u0002J\u0013\u0010\t\u001a\u0004\u0018\u00010\u00032\u0006\u0010\u000b\u001a\u00020\fH\u0096\u0002J\b\u0010\r\u001a\u00020\u000eH\u0016J\u0011\u0010\u000f\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00030\u0010H\u0096\u0002R\u0014\u0010\u0005\u001a\u00020\u00068VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0007\u0010\b\u00a8\u0006\u0011"}, d2 = {"kotlin/text/MatcherMatchResult$groups$1", "Lkotlin/text/MatchNamedGroupCollection;", "Lkotlin/collections/AbstractCollection;", "Lkotlin/text/MatchGroup;", "(Lkotlin/text/MatcherMatchResult;)V", "size", "", "getSize", "()I", "get", "index", "name", "", "isEmpty", "", "iterator", "", "kotlin-stdlib"}, k = 1, mv = {1, 1, 6})
/* compiled from: Regex.kt */
public final class MatcherMatchResult$groups$1 extends kotlin.collections.AbstractCollection<kotlin.text.MatchGroup> implements kotlin.text.MatchNamedGroupCollection {
    final /* synthetic */ kotlin.text.MatcherMatchResult this$0;

    MatcherMatchResult$groups$1(kotlin.text.MatcherMatchResult $outer) {
        this.this$0 = $outer;
    }

    public final /* bridge */ boolean contains(java.lang.Object obj) {
        if (obj != null ? obj instanceof kotlin.text.MatchGroup : true) {
            return contains((kotlin.text.MatchGroup) obj);
        }
        return false;
    }

    public /* bridge */ boolean contains(kotlin.text.MatchGroup matchGroup) {
        return super.contains(matchGroup);
    }

    public int getSize() {
        return this.this$0.matchResult.groupCount() + 1;
    }

    public boolean isEmpty() {
        return false;
    }

    @org.jetbrains.annotations.NotNull
    public java.util.Iterator<kotlin.text.MatchGroup> iterator() {
        return kotlin.sequences.SequencesKt.map(kotlin.collections.CollectionsKt.asSequence(kotlin.collections.CollectionsKt.getIndices(this)), new kotlin.text.MatcherMatchResult$groups$1$iterator$1(this)).iterator();
    }

    @org.jetbrains.annotations.Nullable
    public kotlin.text.MatchGroup get(int index) {
        kotlin.ranges.IntRange range = kotlin.text.RegexKt.range(this.this$0.matchResult, index);
        if (range.getStart().intValue() < 0) {
            return null;
        }
        java.lang.String group = this.this$0.matchResult.group(index);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(group, "matchResult.group(index)");
        return new kotlin.text.MatchGroup(group, range);
    }

    @org.jetbrains.annotations.Nullable
    public kotlin.text.MatchGroup get(@org.jetbrains.annotations.NotNull java.lang.String name) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(name, ch.qos.logback.core.joran.action.Action.NAME_ATTRIBUTE);
        kotlin.internal.PlatformImplementations platformImplementations = kotlin.internal.PlatformImplementationsKt.IMPLEMENTATIONS;
        java.util.regex.MatchResult access$getMatchResult$p = this.this$0.matchResult;
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(access$getMatchResult$p, "matchResult");
        return platformImplementations.getMatchResultNamedGroup(access$getMatchResult$p, name);
    }
}
