package kotlin.text;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000F\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0005\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u0006\n\u0002\b\u0003\n\u0002\u0010\u0007\n\u0002\b\u0007\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0010\n\n\u0002\b\u0005\u001a4\u0010\u0000\u001a\u0004\u0018\u0001H\u0001\"\u0004\b\u0000\u0010\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u0002H\u00010\u0005H\u0083\b\u00a2\u0006\u0004\b\u0006\u0010\u0007\u001a\r\u0010\b\u001a\u00020\t*\u00020\u0003H\u0087\b\u001a\r\u0010\n\u001a\u00020\u000b*\u00020\u0003H\u0087\b\u001a\u0015\u0010\n\u001a\u00020\u000b*\u00020\u00032\u0006\u0010\f\u001a\u00020\rH\u0087\b\u001a\u0013\u0010\u000e\u001a\u0004\u0018\u00010\u000b*\u00020\u0003H\u0007\u00a2\u0006\u0002\u0010\u000f\u001a\u001b\u0010\u000e\u001a\u0004\u0018\u00010\u000b*\u00020\u00032\u0006\u0010\f\u001a\u00020\rH\u0007\u00a2\u0006\u0002\u0010\u0010\u001a\r\u0010\u0011\u001a\u00020\u0012*\u00020\u0003H\u0087\b\u001a\u0013\u0010\u0013\u001a\u0004\u0018\u00010\u0012*\u00020\u0003H\u0007\u00a2\u0006\u0002\u0010\u0014\u001a\r\u0010\u0015\u001a\u00020\u0016*\u00020\u0003H\u0087\b\u001a\u0013\u0010\u0017\u001a\u0004\u0018\u00010\u0016*\u00020\u0003H\u0007\u00a2\u0006\u0002\u0010\u0018\u001a\r\u0010\u0019\u001a\u00020\r*\u00020\u0003H\u0087\b\u001a\u0015\u0010\u0019\u001a\u00020\r*\u00020\u00032\u0006\u0010\f\u001a\u00020\rH\u0087\b\u001a\u0013\u0010\u001a\u001a\u0004\u0018\u00010\r*\u00020\u0003H\u0007\u00a2\u0006\u0002\u0010\u001b\u001a\u001b\u0010\u001a\u001a\u0004\u0018\u00010\r*\u00020\u00032\u0006\u0010\f\u001a\u00020\rH\u0007\u00a2\u0006\u0002\u0010\u001c\u001a\r\u0010\u001d\u001a\u00020\u001e*\u00020\u0003H\u0087\b\u001a\u0015\u0010\u001d\u001a\u00020\u001e*\u00020\u00032\u0006\u0010\f\u001a\u00020\rH\u0087\b\u001a\u0013\u0010\u001f\u001a\u0004\u0018\u00010\u001e*\u00020\u0003H\u0007\u00a2\u0006\u0002\u0010 \u001a\u001b\u0010\u001f\u001a\u0004\u0018\u00010\u001e*\u00020\u00032\u0006\u0010\f\u001a\u00020\rH\u0007\u00a2\u0006\u0002\u0010!\u001a\r\u0010\"\u001a\u00020#*\u00020\u0003H\u0087\b\u001a\u0015\u0010\"\u001a\u00020#*\u00020\u00032\u0006\u0010\f\u001a\u00020\rH\u0087\b\u001a\u0013\u0010$\u001a\u0004\u0018\u00010#*\u00020\u0003H\u0007\u00a2\u0006\u0002\u0010%\u001a\u001b\u0010$\u001a\u0004\u0018\u00010#*\u00020\u00032\u0006\u0010\f\u001a\u00020\rH\u0007\u00a2\u0006\u0002\u0010&\u001a\u0015\u0010'\u001a\u00020\u0003*\u00020\u000b2\u0006\u0010\f\u001a\u00020\rH\u0087\b\u001a\u0015\u0010'\u001a\u00020\u0003*\u00020\r2\u0006\u0010\f\u001a\u00020\rH\u0087\b\u001a\u0015\u0010'\u001a\u00020\u0003*\u00020\u001e2\u0006\u0010\f\u001a\u00020\rH\u0087\b\u001a\u0015\u0010'\u001a\u00020\u0003*\u00020#2\u0006\u0010\f\u001a\u00020\rH\u0087\b\u00a8\u0006("}, d2 = {"screenFloatValue", "T", "str", "", "parse", "Lkotlin/Function1;", "screenFloatValue$StringsKt__StringNumberConversionsKt", "(Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;", "toBoolean", "", "toByte", "", "radix", "", "toByteOrNull", "(Ljava/lang/String;)Ljava/lang/Byte;", "(Ljava/lang/String;I)Ljava/lang/Byte;", "toDouble", "", "toDoubleOrNull", "(Ljava/lang/String;)Ljava/lang/Double;", "toFloat", "", "toFloatOrNull", "(Ljava/lang/String;)Ljava/lang/Float;", "toInt", "toIntOrNull", "(Ljava/lang/String;)Ljava/lang/Integer;", "(Ljava/lang/String;I)Ljava/lang/Integer;", "toLong", "", "toLongOrNull", "(Ljava/lang/String;)Ljava/lang/Long;", "(Ljava/lang/String;I)Ljava/lang/Long;", "toShort", "", "toShortOrNull", "(Ljava/lang/String;)Ljava/lang/Short;", "(Ljava/lang/String;I)Ljava/lang/Short;", "toString", "kotlin-stdlib"}, k = 5, mv = {1, 1, 6}, xi = 1, xs = "kotlin/text/StringsKt")
/* compiled from: StringNumberConversions.kt */
class StringsKt__StringNumberConversionsKt extends kotlin.text.StringsKt__StringBuilderKt {
    @kotlin.SinceKotlin(version = "1.1")
    @kotlin.internal.InlineOnly
    private static final java.lang.String toString(byte $receiver, int radix) {
        java.lang.String num = java.lang.Integer.toString($receiver, kotlin.text.CharsKt.checkRadix(kotlin.text.CharsKt.checkRadix(radix)));
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(num, "java.lang.Integer.toStri\u2026(this, checkRadix(radix))");
        return num;
    }

    @kotlin.SinceKotlin(version = "1.1")
    @kotlin.internal.InlineOnly
    private static final java.lang.String toString(short $receiver, int radix) {
        java.lang.String num = java.lang.Integer.toString($receiver, kotlin.text.CharsKt.checkRadix(kotlin.text.CharsKt.checkRadix(radix)));
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(num, "java.lang.Integer.toStri\u2026(this, checkRadix(radix))");
        return num;
    }

    @kotlin.SinceKotlin(version = "1.1")
    @kotlin.internal.InlineOnly
    private static final java.lang.String toString(int $receiver, int radix) {
        java.lang.String num = java.lang.Integer.toString($receiver, kotlin.text.CharsKt.checkRadix(radix));
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(num, "java.lang.Integer.toStri\u2026(this, checkRadix(radix))");
        return num;
    }

    @kotlin.SinceKotlin(version = "1.1")
    @kotlin.internal.InlineOnly
    private static final java.lang.String toString(long $receiver, int radix) {
        java.lang.String l = java.lang.Long.toString($receiver, kotlin.text.CharsKt.checkRadix(radix));
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(l, "java.lang.Long.toString(this, checkRadix(radix))");
        return l;
    }

    @kotlin.internal.InlineOnly
    private static final boolean toBoolean(@org.jetbrains.annotations.NotNull java.lang.String $receiver) {
        return java.lang.Boolean.parseBoolean($receiver);
    }

    @kotlin.internal.InlineOnly
    private static final byte toByte(@org.jetbrains.annotations.NotNull java.lang.String $receiver) {
        return java.lang.Byte.parseByte($receiver);
    }

    @kotlin.SinceKotlin(version = "1.1")
    @kotlin.internal.InlineOnly
    private static final byte toByte(@org.jetbrains.annotations.NotNull java.lang.String $receiver, int radix) {
        return java.lang.Byte.parseByte($receiver, kotlin.text.CharsKt.checkRadix(radix));
    }

    @kotlin.internal.InlineOnly
    private static final short toShort(@org.jetbrains.annotations.NotNull java.lang.String $receiver) {
        return java.lang.Short.parseShort($receiver);
    }

    @kotlin.SinceKotlin(version = "1.1")
    @kotlin.internal.InlineOnly
    private static final short toShort(@org.jetbrains.annotations.NotNull java.lang.String $receiver, int radix) {
        return java.lang.Short.parseShort($receiver, kotlin.text.CharsKt.checkRadix(radix));
    }

    @kotlin.internal.InlineOnly
    private static final int toInt(@org.jetbrains.annotations.NotNull java.lang.String $receiver) {
        return java.lang.Integer.parseInt($receiver);
    }

    @kotlin.SinceKotlin(version = "1.1")
    @kotlin.internal.InlineOnly
    private static final int toInt(@org.jetbrains.annotations.NotNull java.lang.String $receiver, int radix) {
        return java.lang.Integer.parseInt($receiver, kotlin.text.CharsKt.checkRadix(radix));
    }

    @kotlin.internal.InlineOnly
    private static final long toLong(@org.jetbrains.annotations.NotNull java.lang.String $receiver) {
        return java.lang.Long.parseLong($receiver);
    }

    @kotlin.SinceKotlin(version = "1.1")
    @kotlin.internal.InlineOnly
    private static final long toLong(@org.jetbrains.annotations.NotNull java.lang.String $receiver, int radix) {
        return java.lang.Long.parseLong($receiver, kotlin.text.CharsKt.checkRadix(radix));
    }

    @kotlin.internal.InlineOnly
    private static final float toFloat(@org.jetbrains.annotations.NotNull java.lang.String $receiver) {
        return java.lang.Float.parseFloat($receiver);
    }

    @kotlin.internal.InlineOnly
    private static final double toDouble(@org.jetbrains.annotations.NotNull java.lang.String $receiver) {
        return java.lang.Double.parseDouble($receiver);
    }

    @kotlin.SinceKotlin(version = "1.1")
    @org.jetbrains.annotations.Nullable
    public static final java.lang.Byte toByteOrNull(@org.jetbrains.annotations.NotNull java.lang.String $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        return kotlin.text.StringsKt.toByteOrNull($receiver, 10);
    }

    @kotlin.SinceKotlin(version = "1.1")
    @org.jetbrains.annotations.Nullable
    public static final java.lang.Byte toByteOrNull(@org.jetbrains.annotations.NotNull java.lang.String $receiver, int radix) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        java.lang.Integer intOrNull = kotlin.text.StringsKt.toIntOrNull($receiver, radix);
        if (intOrNull == null) {
            return null;
        }
        int intValue = intOrNull.intValue();
        if (intValue < -128 || intValue > 127) {
            return null;
        }
        return java.lang.Byte.valueOf((byte) intValue);
    }

    @kotlin.SinceKotlin(version = "1.1")
    @org.jetbrains.annotations.Nullable
    public static final java.lang.Short toShortOrNull(@org.jetbrains.annotations.NotNull java.lang.String $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        return kotlin.text.StringsKt.toShortOrNull($receiver, 10);
    }

    @kotlin.SinceKotlin(version = "1.1")
    @org.jetbrains.annotations.Nullable
    public static final java.lang.Short toShortOrNull(@org.jetbrains.annotations.NotNull java.lang.String $receiver, int radix) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        java.lang.Integer intOrNull = kotlin.text.StringsKt.toIntOrNull($receiver, radix);
        if (intOrNull == null) {
            return null;
        }
        int intValue = intOrNull.intValue();
        if (intValue < -32768 || intValue > 32767) {
            return null;
        }
        return java.lang.Short.valueOf((short) intValue);
    }

    @kotlin.SinceKotlin(version = "1.1")
    @org.jetbrains.annotations.Nullable
    public static final java.lang.Integer toIntOrNull(@org.jetbrains.annotations.NotNull java.lang.String $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        return kotlin.text.StringsKt.toIntOrNull($receiver, 10);
    }

    @kotlin.SinceKotlin(version = "1.1")
    @org.jetbrains.annotations.Nullable
    public static final java.lang.Integer toIntOrNull(@org.jetbrains.annotations.NotNull java.lang.String $receiver, int radix) {
        int start;
        boolean isNegative;
        int limit;
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.text.CharsKt.checkRadix(radix);
        int length = $receiver.length();
        if (length == 0) {
            return null;
        }
        char firstChar = $receiver.charAt(0);
        if (firstChar >= '0') {
            start = 0;
            isNegative = false;
            limit = -2147483647;
        } else if (length == 1) {
            return null;
        } else {
            start = 1;
            if (firstChar == '-') {
                isNegative = true;
                limit = Integer.MIN_VALUE;
            } else if (firstChar != '+') {
                return null;
            } else {
                isNegative = false;
                limit = -2147483647;
            }
        }
        int limitBeforeMul = limit / radix;
        int result = 0;
        int i = length - 1;
        if (start <= i) {
            while (true) {
                int digit = kotlin.text.CharsKt.digitOf($receiver.charAt(start), radix);
                if (digit < 0 || result < limitBeforeMul) {
                    return null;
                }
                int result2 = result * radix;
                if (result2 >= limit + digit) {
                    result = result2 - digit;
                    if (start == i) {
                        break;
                    }
                    start++;
                } else {
                    return null;
                }
            }
        }
        return isNegative ? java.lang.Integer.valueOf(result) : java.lang.Integer.valueOf(-result);
    }

    @kotlin.SinceKotlin(version = "1.1")
    @org.jetbrains.annotations.Nullable
    public static final java.lang.Long toLongOrNull(@org.jetbrains.annotations.NotNull java.lang.String $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        return kotlin.text.StringsKt.toLongOrNull($receiver, 10);
    }

    @kotlin.SinceKotlin(version = "1.1")
    @org.jetbrains.annotations.Nullable
    public static final java.lang.Long toLongOrNull(@org.jetbrains.annotations.NotNull java.lang.String $receiver, int radix) {
        int start;
        boolean isNegative;
        long limit;
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.text.CharsKt.checkRadix(radix);
        int length = $receiver.length();
        if (length == 0) {
            return null;
        }
        char firstChar = $receiver.charAt(0);
        if (firstChar >= '0') {
            start = 0;
            isNegative = false;
            limit = -9223372036854775807L;
        } else if (length == 1) {
            return null;
        } else {
            start = 1;
            if (firstChar == '-') {
                isNegative = true;
                limit = Long.MIN_VALUE;
            } else if (firstChar != '+') {
                return null;
            } else {
                isNegative = false;
                limit = -9223372036854775807L;
            }
        }
        long limitBeforeMul = limit / ((long) radix);
        long result = 0;
        int i = length - 1;
        if (start <= i) {
            while (true) {
                int digit = kotlin.text.CharsKt.digitOf($receiver.charAt(start), radix);
                if (digit >= 0) {
                    if (result >= limitBeforeMul) {
                        long result2 = result * ((long) radix);
                        if (result2 >= ((long) digit) + limit) {
                            result = result2 - ((long) digit);
                            if (start == i) {
                                break;
                            }
                            start++;
                        } else {
                            return null;
                        }
                    } else {
                        return null;
                    }
                } else {
                    return null;
                }
            }
        }
        return isNegative ? java.lang.Long.valueOf(result) : java.lang.Long.valueOf(-result);
    }

    @kotlin.SinceKotlin(version = "1.1")
    @org.jetbrains.annotations.Nullable
    public static final java.lang.Float toFloatOrNull(@org.jetbrains.annotations.NotNull java.lang.String $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        try {
            if (kotlin.text.ScreenFloatValueRegEx.value.matches($receiver)) {
                return java.lang.Float.valueOf(java.lang.Float.parseFloat($receiver));
            }
            return null;
        } catch (java.lang.NumberFormatException e) {
            return null;
        }
    }

    @kotlin.SinceKotlin(version = "1.1")
    @org.jetbrains.annotations.Nullable
    public static final java.lang.Double toDoubleOrNull(@org.jetbrains.annotations.NotNull java.lang.String $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        try {
            if (kotlin.text.ScreenFloatValueRegEx.value.matches($receiver)) {
                return java.lang.Double.valueOf(java.lang.Double.parseDouble($receiver));
            }
            return null;
        } catch (java.lang.NumberFormatException e) {
            return null;
        }
    }

    private static final <T> T screenFloatValue$StringsKt__StringNumberConversionsKt(java.lang.String str, kotlin.jvm.functions.Function1<? super java.lang.String, ? extends T> parse) {
        try {
            if (kotlin.text.ScreenFloatValueRegEx.value.matches(str)) {
                return parse.invoke(str);
            }
            return null;
        } catch (java.lang.NumberFormatException e) {
            return null;
        }
    }
}
