package kotlin.text;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u00e2\u0001\n\u0000\n\u0002\u0010\u000b\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\f\n\u0002\b\u0002\n\u0002\u0010\u001c\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010%\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0010\u001f\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0010!\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u0000\n\u0002\b\b\n\u0002\u0010\u000f\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0012\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0006\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\"\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\u001a!\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0004H\u0086\b\u001a\n\u0010\u0006\u001a\u00020\u0001*\u00020\u0002\u001a!\u0010\u0006\u001a\u00020\u0001*\u00020\u00022\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0004H\u0086\b\u001a\u0010\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00050\b*\u00020\u0002\u001a\u0010\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00050\n*\u00020\u0002\u001aE\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u0002H\r\u0012\u0004\u0012\u0002H\u000e0\f\"\u0004\b\u0000\u0010\r\"\u0004\b\u0001\u0010\u000e*\u00020\u00022\u001e\u0010\u000f\u001a\u001a\u0012\u0004\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\r\u0012\u0004\u0012\u0002H\u000e0\u00100\u0004H\u0086\b\u001a3\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u0002H\r\u0012\u0004\u0012\u00020\u00050\f\"\u0004\b\u0000\u0010\r*\u00020\u00022\u0012\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H\r0\u0004H\u0086\b\u001aM\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u0002H\r\u0012\u0004\u0012\u0002H\u000e0\f\"\u0004\b\u0000\u0010\r\"\u0004\b\u0001\u0010\u000e*\u00020\u00022\u0012\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H\r0\u00042\u0012\u0010\u0013\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H\u000e0\u0004H\u0086\b\u001aN\u0010\u0014\u001a\u0002H\u0015\"\u0004\b\u0000\u0010\r\"\u0018\b\u0001\u0010\u0015*\u0012\u0012\u0006\b\u0000\u0012\u0002H\r\u0012\u0006\b\u0000\u0012\u00020\u00050\u0016*\u00020\u00022\u0006\u0010\u0017\u001a\u0002H\u00152\u0012\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H\r0\u0004H\u0086\b\u00a2\u0006\u0002\u0010\u0018\u001ah\u0010\u0014\u001a\u0002H\u0015\"\u0004\b\u0000\u0010\r\"\u0004\b\u0001\u0010\u000e\"\u0018\b\u0002\u0010\u0015*\u0012\u0012\u0006\b\u0000\u0012\u0002H\r\u0012\u0006\b\u0000\u0012\u0002H\u000e0\u0016*\u00020\u00022\u0006\u0010\u0017\u001a\u0002H\u00152\u0012\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H\r0\u00042\u0012\u0010\u0013\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H\u000e0\u0004H\u0086\b\u00a2\u0006\u0002\u0010\u0019\u001a`\u0010\u001a\u001a\u0002H\u0015\"\u0004\b\u0000\u0010\r\"\u0004\b\u0001\u0010\u000e\"\u0018\b\u0002\u0010\u0015*\u0012\u0012\u0006\b\u0000\u0012\u0002H\r\u0012\u0006\b\u0000\u0012\u0002H\u000e0\u0016*\u00020\u00022\u0006\u0010\u0017\u001a\u0002H\u00152\u001e\u0010\u000f\u001a\u001a\u0012\u0004\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\r\u0012\u0004\u0012\u0002H\u000e0\u00100\u0004H\u0086\b\u00a2\u0006\u0002\u0010\u0018\u001a\r\u0010\u001b\u001a\u00020\u001c*\u00020\u0002H\u0087\b\u001a!\u0010\u001b\u001a\u00020\u001c*\u00020\u00022\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0004H\u0086\b\u001a\u0012\u0010\u001d\u001a\u00020\u0002*\u00020\u00022\u0006\u0010\u001e\u001a\u00020\u001c\u001a\u0012\u0010\u001d\u001a\u00020\u001f*\u00020\u001f2\u0006\u0010\u001e\u001a\u00020\u001c\u001a\u0012\u0010 \u001a\u00020\u0002*\u00020\u00022\u0006\u0010\u001e\u001a\u00020\u001c\u001a\u0012\u0010 \u001a\u00020\u001f*\u00020\u001f2\u0006\u0010\u001e\u001a\u00020\u001c\u001a!\u0010!\u001a\u00020\u0002*\u00020\u00022\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0004H\u0086\b\u001a!\u0010!\u001a\u00020\u001f*\u00020\u001f2\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0004H\u0086\b\u001a!\u0010\"\u001a\u00020\u0002*\u00020\u00022\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0004H\u0086\b\u001a!\u0010\"\u001a\u00020\u001f*\u00020\u001f2\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0004H\u0086\b\u001a\u0015\u0010#\u001a\u00020\u0005*\u00020\u00022\u0006\u0010$\u001a\u00020\u001cH\u0087\b\u001a)\u0010%\u001a\u00020\u0005*\u00020\u00022\u0006\u0010$\u001a\u00020\u001c2\u0012\u0010&\u001a\u000e\u0012\u0004\u0012\u00020\u001c\u0012\u0004\u0012\u00020\u00050\u0004H\u0087\b\u001a\u001c\u0010'\u001a\u0004\u0018\u00010\u0005*\u00020\u00022\u0006\u0010$\u001a\u00020\u001cH\u0087\b\u00a2\u0006\u0002\u0010(\u001a!\u0010)\u001a\u00020\u0002*\u00020\u00022\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0004H\u0086\b\u001a!\u0010)\u001a\u00020\u001f*\u00020\u001f2\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0004H\u0086\b\u001a6\u0010*\u001a\u00020\u0002*\u00020\u00022'\u0010\u0003\u001a#\u0012\u0013\u0012\u00110\u001c\u00a2\u0006\f\b,\u0012\b\b-\u0012\u0004\b\b($\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010+H\u0086\b\u001a6\u0010*\u001a\u00020\u001f*\u00020\u001f2'\u0010\u0003\u001a#\u0012\u0013\u0012\u00110\u001c\u00a2\u0006\f\b,\u0012\b\b-\u0012\u0004\b\b($\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010+H\u0086\b\u001aQ\u0010.\u001a\u0002H/\"\f\b\u0000\u0010/*\u000600j\u0002`1*\u00020\u00022\u0006\u0010\u0017\u001a\u0002H/2'\u0010\u0003\u001a#\u0012\u0013\u0012\u00110\u001c\u00a2\u0006\f\b,\u0012\b\b-\u0012\u0004\b\b($\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010+H\u0086\b\u00a2\u0006\u0002\u00102\u001a!\u00103\u001a\u00020\u0002*\u00020\u00022\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0004H\u0086\b\u001a!\u00103\u001a\u00020\u001f*\u00020\u001f2\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0004H\u0086\b\u001a<\u00104\u001a\u0002H/\"\f\b\u0000\u0010/*\u000600j\u0002`1*\u00020\u00022\u0006\u0010\u0017\u001a\u0002H/2\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0004H\u0086\b\u00a2\u0006\u0002\u00105\u001a<\u00106\u001a\u0002H/\"\f\b\u0000\u0010/*\u000600j\u0002`1*\u00020\u00022\u0006\u0010\u0017\u001a\u0002H/2\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0004H\u0086\b\u00a2\u0006\u0002\u00105\u001a(\u00107\u001a\u0004\u0018\u00010\u0005*\u00020\u00022\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0004H\u0087\b\u00a2\u0006\u0002\u00108\u001a(\u00109\u001a\u0004\u0018\u00010\u0005*\u00020\u00022\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0004H\u0087\b\u00a2\u0006\u0002\u00108\u001a\n\u0010:\u001a\u00020\u0005*\u00020\u0002\u001a!\u0010:\u001a\u00020\u0005*\u00020\u00022\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0004H\u0086\b\u001a\u0011\u0010;\u001a\u0004\u0018\u00010\u0005*\u00020\u0002\u00a2\u0006\u0002\u0010<\u001a(\u0010;\u001a\u0004\u0018\u00010\u0005*\u00020\u00022\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0004H\u0086\b\u00a2\u0006\u0002\u00108\u001a3\u0010=\u001a\b\u0012\u0004\u0012\u0002H?0>\"\u0004\b\u0000\u0010?*\u00020\u00022\u0018\u0010\u000f\u001a\u0014\u0012\u0004\u0012\u00020\u0005\u0012\n\u0012\b\u0012\u0004\u0012\u0002H?0\b0\u0004H\u0086\b\u001aL\u0010@\u001a\u0002H/\"\u0004\b\u0000\u0010?\"\u0010\b\u0001\u0010/*\n\u0012\u0006\b\u0000\u0012\u0002H?0A*\u00020\u00022\u0006\u0010\u0017\u001a\u0002H/2\u0018\u0010\u000f\u001a\u0014\u0012\u0004\u0012\u00020\u0005\u0012\n\u0012\b\u0012\u0004\u0012\u0002H?0\b0\u0004H\u0086\b\u00a2\u0006\u0002\u0010B\u001aI\u0010C\u001a\u0002H?\"\u0004\b\u0000\u0010?*\u00020\u00022\u0006\u0010D\u001a\u0002H?2'\u0010E\u001a#\u0012\u0013\u0012\u0011H?\u00a2\u0006\f\b,\u0012\b\b-\u0012\u0004\b\b(F\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H?0+H\u0086\b\u00a2\u0006\u0002\u0010G\u001a^\u0010H\u001a\u0002H?\"\u0004\b\u0000\u0010?*\u00020\u00022\u0006\u0010D\u001a\u0002H?2<\u0010E\u001a8\u0012\u0013\u0012\u00110\u001c\u00a2\u0006\f\b,\u0012\b\b-\u0012\u0004\b\b($\u0012\u0013\u0012\u0011H?\u00a2\u0006\f\b,\u0012\b\b-\u0012\u0004\b\b(F\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H?0IH\u0086\b\u00a2\u0006\u0002\u0010J\u001aI\u0010K\u001a\u0002H?\"\u0004\b\u0000\u0010?*\u00020\u00022\u0006\u0010D\u001a\u0002H?2'\u0010E\u001a#\u0012\u0004\u0012\u00020\u0005\u0012\u0013\u0012\u0011H?\u00a2\u0006\f\b,\u0012\b\b-\u0012\u0004\b\b(F\u0012\u0004\u0012\u0002H?0+H\u0086\b\u00a2\u0006\u0002\u0010G\u001a^\u0010L\u001a\u0002H?\"\u0004\b\u0000\u0010?*\u00020\u00022\u0006\u0010D\u001a\u0002H?2<\u0010E\u001a8\u0012\u0013\u0012\u00110\u001c\u00a2\u0006\f\b,\u0012\b\b-\u0012\u0004\b\b($\u0012\u0004\u0012\u00020\u0005\u0012\u0013\u0012\u0011H?\u00a2\u0006\f\b,\u0012\b\b-\u0012\u0004\b\b(F\u0012\u0004\u0012\u0002H?0IH\u0086\b\u00a2\u0006\u0002\u0010J\u001a!\u0010M\u001a\u00020N*\u00020\u00022\u0012\u0010O\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020N0\u0004H\u0086\b\u001a6\u0010P\u001a\u00020N*\u00020\u00022'\u0010O\u001a#\u0012\u0013\u0012\u00110\u001c\u00a2\u0006\f\b,\u0012\b\b-\u0012\u0004\b\b($\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020N0+H\u0086\b\u001a)\u0010Q\u001a\u00020\u0005*\u00020\u00022\u0006\u0010$\u001a\u00020\u001c2\u0012\u0010&\u001a\u000e\u0012\u0004\u0012\u00020\u001c\u0012\u0004\u0012\u00020\u00050\u0004H\u0087\b\u001a\u0019\u0010R\u001a\u0004\u0018\u00010\u0005*\u00020\u00022\u0006\u0010$\u001a\u00020\u001c\u00a2\u0006\u0002\u0010(\u001a9\u0010S\u001a\u0014\u0012\u0004\u0012\u0002H\r\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050>0\f\"\u0004\b\u0000\u0010\r*\u00020\u00022\u0012\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H\r0\u0004H\u0086\b\u001aS\u0010S\u001a\u0014\u0012\u0004\u0012\u0002H\r\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u000e0>0\f\"\u0004\b\u0000\u0010\r\"\u0004\b\u0001\u0010\u000e*\u00020\u00022\u0012\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H\r0\u00042\u0012\u0010\u0013\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H\u000e0\u0004H\u0086\b\u001aR\u0010T\u001a\u0002H\u0015\"\u0004\b\u0000\u0010\r\"\u001c\b\u0001\u0010\u0015*\u0016\u0012\u0006\b\u0000\u0012\u0002H\r\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050U0\u0016*\u00020\u00022\u0006\u0010\u0017\u001a\u0002H\u00152\u0012\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H\r0\u0004H\u0086\b\u00a2\u0006\u0002\u0010\u0018\u001al\u0010T\u001a\u0002H\u0015\"\u0004\b\u0000\u0010\r\"\u0004\b\u0001\u0010\u000e\"\u001c\b\u0002\u0010\u0015*\u0016\u0012\u0006\b\u0000\u0012\u0002H\r\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u000e0U0\u0016*\u00020\u00022\u0006\u0010\u0017\u001a\u0002H\u00152\u0012\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H\r0\u00042\u0012\u0010\u0013\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H\u000e0\u0004H\u0086\b\u00a2\u0006\u0002\u0010\u0019\u001a5\u0010V\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H\r0W\"\u0004\b\u0000\u0010\r*\u00020\u00022\u0014\b\u0004\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H\r0\u0004H\u0087\b\u001a!\u0010X\u001a\u00020\u001c*\u00020\u00022\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0004H\u0086\b\u001a!\u0010Y\u001a\u00020\u001c*\u00020\u00022\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0004H\u0086\b\u001a\n\u0010Z\u001a\u00020\u0005*\u00020\u0002\u001a!\u0010Z\u001a\u00020\u0005*\u00020\u00022\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0004H\u0086\b\u001a\u0011\u0010[\u001a\u0004\u0018\u00010\u0005*\u00020\u0002\u00a2\u0006\u0002\u0010<\u001a(\u0010[\u001a\u0004\u0018\u00010\u0005*\u00020\u00022\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0004H\u0086\b\u00a2\u0006\u0002\u00108\u001a-\u0010\\\u001a\b\u0012\u0004\u0012\u0002H?0>\"\u0004\b\u0000\u0010?*\u00020\u00022\u0012\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H?0\u0004H\u0086\b\u001aB\u0010]\u001a\b\u0012\u0004\u0012\u0002H?0>\"\u0004\b\u0000\u0010?*\u00020\u00022'\u0010\u000f\u001a#\u0012\u0013\u0012\u00110\u001c\u00a2\u0006\f\b,\u0012\b\b-\u0012\u0004\b\b($\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H?0+H\u0086\b\u001aH\u0010^\u001a\b\u0012\u0004\u0012\u0002H?0>\"\b\b\u0000\u0010?*\u00020_*\u00020\u00022)\u0010\u000f\u001a%\u0012\u0013\u0012\u00110\u001c\u00a2\u0006\f\b,\u0012\b\b-\u0012\u0004\b\b($\u0012\u0004\u0012\u00020\u0005\u0012\u0006\u0012\u0004\u0018\u0001H?0+H\u0086\b\u001aa\u0010`\u001a\u0002H/\"\b\b\u0000\u0010?*\u00020_\"\u0010\b\u0001\u0010/*\n\u0012\u0006\b\u0000\u0012\u0002H?0A*\u00020\u00022\u0006\u0010\u0017\u001a\u0002H/2)\u0010\u000f\u001a%\u0012\u0013\u0012\u00110\u001c\u00a2\u0006\f\b,\u0012\b\b-\u0012\u0004\b\b($\u0012\u0004\u0012\u00020\u0005\u0012\u0006\u0012\u0004\u0018\u0001H?0+H\u0086\b\u00a2\u0006\u0002\u0010a\u001a[\u0010b\u001a\u0002H/\"\u0004\b\u0000\u0010?\"\u0010\b\u0001\u0010/*\n\u0012\u0006\b\u0000\u0012\u0002H?0A*\u00020\u00022\u0006\u0010\u0017\u001a\u0002H/2'\u0010\u000f\u001a#\u0012\u0013\u0012\u00110\u001c\u00a2\u0006\f\b,\u0012\b\b-\u0012\u0004\b\b($\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H?0+H\u0086\b\u00a2\u0006\u0002\u0010a\u001a3\u0010c\u001a\b\u0012\u0004\u0012\u0002H?0>\"\b\b\u0000\u0010?*\u00020_*\u00020\u00022\u0014\u0010\u000f\u001a\u0010\u0012\u0004\u0012\u00020\u0005\u0012\u0006\u0012\u0004\u0018\u0001H?0\u0004H\u0086\b\u001aL\u0010d\u001a\u0002H/\"\b\b\u0000\u0010?*\u00020_\"\u0010\b\u0001\u0010/*\n\u0012\u0006\b\u0000\u0012\u0002H?0A*\u00020\u00022\u0006\u0010\u0017\u001a\u0002H/2\u0014\u0010\u000f\u001a\u0010\u0012\u0004\u0012\u00020\u0005\u0012\u0006\u0012\u0004\u0018\u0001H?0\u0004H\u0086\b\u00a2\u0006\u0002\u0010B\u001aF\u0010e\u001a\u0002H/\"\u0004\b\u0000\u0010?\"\u0010\b\u0001\u0010/*\n\u0012\u0006\b\u0000\u0012\u0002H?0A*\u00020\u00022\u0006\u0010\u0017\u001a\u0002H/2\u0012\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H?0\u0004H\u0086\b\u00a2\u0006\u0002\u0010B\u001a\u0011\u0010f\u001a\u0004\u0018\u00010\u0005*\u00020\u0002\u00a2\u0006\u0002\u0010<\u001a8\u0010g\u001a\u0004\u0018\u00010\u0005\"\u000e\b\u0000\u0010?*\b\u0012\u0004\u0012\u0002H?0h*\u00020\u00022\u0012\u0010i\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H?0\u0004H\u0086\b\u00a2\u0006\u0002\u00108\u001a-\u0010j\u001a\u0004\u0018\u00010\u0005*\u00020\u00022\u001a\u0010k\u001a\u0016\u0012\u0006\b\u0000\u0012\u00020\u00050lj\n\u0012\u0006\b\u0000\u0012\u00020\u0005`m\u00a2\u0006\u0002\u0010n\u001a\u0011\u0010o\u001a\u0004\u0018\u00010\u0005*\u00020\u0002\u00a2\u0006\u0002\u0010<\u001a8\u0010p\u001a\u0004\u0018\u00010\u0005\"\u000e\b\u0000\u0010?*\b\u0012\u0004\u0012\u0002H?0h*\u00020\u00022\u0012\u0010i\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H?0\u0004H\u0086\b\u00a2\u0006\u0002\u00108\u001a-\u0010q\u001a\u0004\u0018\u00010\u0005*\u00020\u00022\u001a\u0010k\u001a\u0016\u0012\u0006\b\u0000\u0012\u00020\u00050lj\n\u0012\u0006\b\u0000\u0012\u00020\u0005`m\u00a2\u0006\u0002\u0010n\u001a\n\u0010r\u001a\u00020\u0001*\u00020\u0002\u001a!\u0010r\u001a\u00020\u0001*\u00020\u00022\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0004H\u0086\b\u001a0\u0010s\u001a\u0002Ht\"\b\b\u0000\u0010t*\u00020\u0002*\u0002Ht2\u0012\u0010O\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020N0\u0004H\u0087\b\u00a2\u0006\u0002\u0010u\u001a-\u0010v\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00020\u0010*\u00020\u00022\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0004H\u0086\b\u001a-\u0010v\u001a\u000e\u0012\u0004\u0012\u00020\u001f\u0012\u0004\u0012\u00020\u001f0\u0010*\u00020\u001f2\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0004H\u0086\b\u001a6\u0010w\u001a\u00020\u0005*\u00020\u00022'\u0010E\u001a#\u0012\u0013\u0012\u00110\u0005\u00a2\u0006\f\b,\u0012\b\b-\u0012\u0004\b\b(F\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00050+H\u0086\b\u001aK\u0010x\u001a\u00020\u0005*\u00020\u00022<\u0010E\u001a8\u0012\u0013\u0012\u00110\u001c\u00a2\u0006\f\b,\u0012\b\b-\u0012\u0004\b\b($\u0012\u0013\u0012\u00110\u0005\u00a2\u0006\f\b,\u0012\b\b-\u0012\u0004\b\b(F\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00050IH\u0086\b\u001a6\u0010y\u001a\u00020\u0005*\u00020\u00022'\u0010E\u001a#\u0012\u0004\u0012\u00020\u0005\u0012\u0013\u0012\u00110\u0005\u00a2\u0006\f\b,\u0012\b\b-\u0012\u0004\b\b(F\u0012\u0004\u0012\u00020\u00050+H\u0086\b\u001aK\u0010z\u001a\u00020\u0005*\u00020\u00022<\u0010E\u001a8\u0012\u0013\u0012\u00110\u001c\u00a2\u0006\f\b,\u0012\b\b-\u0012\u0004\b\b($\u0012\u0004\u0012\u00020\u0005\u0012\u0013\u0012\u00110\u0005\u00a2\u0006\f\b,\u0012\b\b-\u0012\u0004\b\b(F\u0012\u0004\u0012\u00020\u00050IH\u0086\b\u001a\n\u0010{\u001a\u00020\u0002*\u00020\u0002\u001a\r\u0010{\u001a\u00020\u001f*\u00020\u001fH\u0087\b\u001a\n\u0010|\u001a\u00020\u0005*\u00020\u0002\u001a!\u0010|\u001a\u00020\u0005*\u00020\u00022\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0004H\u0086\b\u001a\u0011\u0010}\u001a\u0004\u0018\u00010\u0005*\u00020\u0002\u00a2\u0006\u0002\u0010<\u001a(\u0010}\u001a\u0004\u0018\u00010\u0005*\u00020\u00022\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0004H\u0086\b\u00a2\u0006\u0002\u00108\u001a\u0018\u0010~\u001a\u00020\u0002*\u00020\u00022\f\u0010\u007f\u001a\b\u0012\u0004\u0012\u00020\u001c0\b\u001a\u0013\u0010~\u001a\u00020\u0002*\u00020\u00022\u0007\u0010\u007f\u001a\u00030\u0080\u0001\u001a\u001b\u0010~\u001a\u00020\u001f*\u00020\u001f2\f\u0010\u007f\u001a\b\u0012\u0004\u0012\u00020\u001c0\bH\u0087\b\u001a\u0013\u0010~\u001a\u00020\u001f*\u00020\u001f2\u0007\u0010\u007f\u001a\u00030\u0080\u0001\u001a\"\u0010\u0081\u0001\u001a\u00020\u001c*\u00020\u00022\u0012\u0010i\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u001c0\u0004H\u0086\b\u001a$\u0010\u0082\u0001\u001a\u00030\u0083\u0001*\u00020\u00022\u0013\u0010i\u001a\u000f\u0012\u0004\u0012\u00020\u0005\u0012\u0005\u0012\u00030\u0083\u00010\u0004H\u0086\b\u001a\u0013\u0010\u0084\u0001\u001a\u00020\u0002*\u00020\u00022\u0006\u0010\u001e\u001a\u00020\u001c\u001a\u0013\u0010\u0084\u0001\u001a\u00020\u001f*\u00020\u001f2\u0006\u0010\u001e\u001a\u00020\u001c\u001a\u0013\u0010\u0085\u0001\u001a\u00020\u0002*\u00020\u00022\u0006\u0010\u001e\u001a\u00020\u001c\u001a\u0013\u0010\u0085\u0001\u001a\u00020\u001f*\u00020\u001f2\u0006\u0010\u001e\u001a\u00020\u001c\u001a\"\u0010\u0086\u0001\u001a\u00020\u0002*\u00020\u00022\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0004H\u0086\b\u001a\"\u0010\u0086\u0001\u001a\u00020\u001f*\u00020\u001f2\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0004H\u0086\b\u001a\"\u0010\u0087\u0001\u001a\u00020\u0002*\u00020\u00022\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0004H\u0086\b\u001a\"\u0010\u0087\u0001\u001a\u00020\u001f*\u00020\u001f2\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0004H\u0086\b\u001a+\u0010\u0088\u0001\u001a\u0002H/\"\u0010\b\u0000\u0010/*\n\u0012\u0006\b\u0000\u0012\u00020\u00050A*\u00020\u00022\u0006\u0010\u0017\u001a\u0002H/\u00a2\u0006\u0003\u0010\u0089\u0001\u001a\u001d\u0010\u008a\u0001\u001a\u0014\u0012\u0004\u0012\u00020\u00050\u008b\u0001j\t\u0012\u0004\u0012\u00020\u0005`\u008c\u0001*\u00020\u0002\u001a\u0011\u0010\u008d\u0001\u001a\b\u0012\u0004\u0012\u00020\u00050>*\u00020\u0002\u001a\u0011\u0010\u008e\u0001\u001a\b\u0012\u0004\u0012\u00020\u00050U*\u00020\u0002\u001a\u0012\u0010\u008f\u0001\u001a\t\u0012\u0004\u0012\u00020\u00050\u0090\u0001*\u00020\u0002\u001a\u001f\u0010\u0091\u0001\u001a\u0014\u0012\u0004\u0012\u00020\u00050\u0092\u0001j\t\u0012\u0004\u0012\u00020\u0005`\u0093\u0001*\u00020\u0002H\u0007\u001a\u0018\u0010\u0094\u0001\u001a\u000f\u0012\u000b\u0012\t\u0012\u0004\u0012\u00020\u00050\u0095\u00010\b*\u00020\u0002\u001a)\u0010\u0096\u0001\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00050\u00100>*\u00020\u00022\u0007\u0010\u0097\u0001\u001a\u00020\u0002H\u0086\u0004\u001a]\u0010\u0096\u0001\u001a\b\u0012\u0004\u0012\u0002H\u000e0>\"\u0004\b\u0000\u0010\u000e*\u00020\u00022\u0007\u0010\u0097\u0001\u001a\u00020\u000228\u0010\u000f\u001a4\u0012\u0014\u0012\u00120\u0005\u00a2\u0006\r\b,\u0012\t\b-\u0012\u0005\b\b(\u0098\u0001\u0012\u0014\u0012\u00120\u0005\u00a2\u0006\r\b,\u0012\t\b-\u0012\u0005\b\b(\u0099\u0001\u0012\u0004\u0012\u0002H\u000e0+H\u0086\b\u00a8\u0006\u009a\u0001"}, d2 = {"all", "", "", "predicate", "Lkotlin/Function1;", "", "any", "asIterable", "", "asSequence", "Lkotlin/sequences/Sequence;", "associate", "", "K", "V", "transform", "Lkotlin/Pair;", "associateBy", "keySelector", "valueTransform", "associateByTo", "M", "", "destination", "(Ljava/lang/CharSequence;Ljava/util/Map;Lkotlin/jvm/functions/Function1;)Ljava/util/Map;", "(Ljava/lang/CharSequence;Ljava/util/Map;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)Ljava/util/Map;", "associateTo", "count", "", "drop", "n", "", "dropLast", "dropLastWhile", "dropWhile", "elementAt", "index", "elementAtOrElse", "defaultValue", "elementAtOrNull", "(Ljava/lang/CharSequence;I)Ljava/lang/Character;", "filter", "filterIndexed", "Lkotlin/Function2;", "Lkotlin/ParameterName;", "name", "filterIndexedTo", "C", "Ljava/lang/Appendable;", "Lkotlin/text/Appendable;", "(Ljava/lang/CharSequence;Ljava/lang/Appendable;Lkotlin/jvm/functions/Function2;)Ljava/lang/Appendable;", "filterNot", "filterNotTo", "(Ljava/lang/CharSequence;Ljava/lang/Appendable;Lkotlin/jvm/functions/Function1;)Ljava/lang/Appendable;", "filterTo", "find", "(Ljava/lang/CharSequence;Lkotlin/jvm/functions/Function1;)Ljava/lang/Character;", "findLast", "first", "firstOrNull", "(Ljava/lang/CharSequence;)Ljava/lang/Character;", "flatMap", "", "R", "flatMapTo", "", "(Ljava/lang/CharSequence;Ljava/util/Collection;Lkotlin/jvm/functions/Function1;)Ljava/util/Collection;", "fold", "initial", "operation", "acc", "(Ljava/lang/CharSequence;Ljava/lang/Object;Lkotlin/jvm/functions/Function2;)Ljava/lang/Object;", "foldIndexed", "Lkotlin/Function3;", "(Ljava/lang/CharSequence;Ljava/lang/Object;Lkotlin/jvm/functions/Function3;)Ljava/lang/Object;", "foldRight", "foldRightIndexed", "forEach", "", "action", "forEachIndexed", "getOrElse", "getOrNull", "groupBy", "groupByTo", "", "groupingBy", "Lkotlin/collections/Grouping;", "indexOfFirst", "indexOfLast", "last", "lastOrNull", "map", "mapIndexed", "mapIndexedNotNull", "", "mapIndexedNotNullTo", "(Ljava/lang/CharSequence;Ljava/util/Collection;Lkotlin/jvm/functions/Function2;)Ljava/util/Collection;", "mapIndexedTo", "mapNotNull", "mapNotNullTo", "mapTo", "max", "maxBy", "", "selector", "maxWith", "comparator", "Ljava/util/Comparator;", "Lkotlin/Comparator;", "(Ljava/lang/CharSequence;Ljava/util/Comparator;)Ljava/lang/Character;", "min", "minBy", "minWith", "none", "onEach", "S", "(Ljava/lang/CharSequence;Lkotlin/jvm/functions/Function1;)Ljava/lang/CharSequence;", "partition", "reduce", "reduceIndexed", "reduceRight", "reduceRightIndexed", "reversed", "single", "singleOrNull", "slice", "indices", "Lkotlin/ranges/IntRange;", "sumBy", "sumByDouble", "", "take", "takeLast", "takeLastWhile", "takeWhile", "toCollection", "(Ljava/lang/CharSequence;Ljava/util/Collection;)Ljava/util/Collection;", "toHashSet", "Ljava/util/HashSet;", "Lkotlin/collections/HashSet;", "toList", "toMutableList", "toSet", "", "toSortedSet", "Ljava/util/SortedSet;", "Lkotlin/collections/SortedSet;", "withIndex", "Lkotlin/collections/IndexedValue;", "zip", "other", "a", "b", "kotlin-stdlib"}, k = 5, mv = {1, 1, 6}, xi = 1, xs = "kotlin/text/StringsKt")
/* compiled from: _Strings.kt */
class StringsKt___StringsKt extends kotlin.text.StringsKt__StringsKt {
    @kotlin.internal.InlineOnly
    private static final char elementAt(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, int index) {
        return $receiver.charAt(index);
    }

    @kotlin.internal.InlineOnly
    private static final char elementAtOrElse(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, int index, kotlin.jvm.functions.Function1<? super java.lang.Integer, java.lang.Character> defaultValue) {
        return (index < 0 || index > kotlin.text.StringsKt.getLastIndex($receiver)) ? ((java.lang.Character) defaultValue.invoke(java.lang.Integer.valueOf(index))).charValue() : $receiver.charAt(index);
    }

    @kotlin.internal.InlineOnly
    private static final java.lang.Character elementAtOrNull(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, int index) {
        return kotlin.text.StringsKt.getOrNull($receiver, index);
    }

    @kotlin.internal.InlineOnly
    private static final java.lang.Character find(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, kotlin.jvm.functions.Function1<? super java.lang.Character, java.lang.Boolean> predicate) {
        kotlin.collections.CharIterator it = kotlin.text.StringsKt.iterator($receiver);
        while (it.hasNext()) {
            char element$iv = it.nextChar();
            if (((java.lang.Boolean) predicate.invoke(java.lang.Character.valueOf(element$iv))).booleanValue()) {
                return java.lang.Character.valueOf(element$iv);
            }
        }
        return null;
    }

    @kotlin.internal.InlineOnly
    private static final java.lang.Character findLast(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, kotlin.jvm.functions.Function1<? super java.lang.Character, java.lang.Boolean> predicate) {
        java.lang.CharSequence $receiver$iv = $receiver;
        kotlin.ranges.IntProgression reversed = kotlin.ranges.RangesKt.reversed((kotlin.ranges.IntProgression) kotlin.text.StringsKt.getIndices($receiver$iv));
        int first = reversed.getFirst();
        int last = reversed.getLast();
        int step = reversed.getStep();
        if (step <= 0 ? first >= last : first <= last) {
            while (true) {
                int i = first;
                char element$iv = $receiver$iv.charAt(i);
                if (!((java.lang.Boolean) predicate.invoke(java.lang.Character.valueOf(element$iv))).booleanValue()) {
                    if (i == last) {
                        break;
                    }
                    first = i + step;
                } else {
                    return java.lang.Character.valueOf(element$iv);
                }
            }
        }
        return null;
    }

    public static final char first(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver) {
        boolean z;
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        if ($receiver.length() == 0) {
            z = true;
        } else {
            z = false;
        }
        if (!z) {
            return $receiver.charAt(0);
        }
        throw new java.util.NoSuchElementException("Char sequence is empty.");
    }

    public static final char first(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.Character, java.lang.Boolean> predicate) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(predicate, "predicate");
        kotlin.collections.CharIterator it = kotlin.text.StringsKt.iterator($receiver);
        while (it.hasNext()) {
            char element = it.nextChar();
            if (((java.lang.Boolean) predicate.invoke(java.lang.Character.valueOf(element))).booleanValue()) {
                return element;
            }
        }
        throw new java.util.NoSuchElementException("Char sequence contains no character matching the predicate.");
    }

    @org.jetbrains.annotations.Nullable
    public static final java.lang.Character firstOrNull(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        if ($receiver.length() == 0) {
            return null;
        }
        return java.lang.Character.valueOf($receiver.charAt(0));
    }

    @org.jetbrains.annotations.Nullable
    public static final java.lang.Character firstOrNull(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.Character, java.lang.Boolean> predicate) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(predicate, "predicate");
        kotlin.collections.CharIterator it = kotlin.text.StringsKt.iterator($receiver);
        while (it.hasNext()) {
            char element = it.nextChar();
            if (((java.lang.Boolean) predicate.invoke(java.lang.Character.valueOf(element))).booleanValue()) {
                return java.lang.Character.valueOf(element);
            }
        }
        return null;
    }

    @kotlin.internal.InlineOnly
    private static final char getOrElse(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, int index, kotlin.jvm.functions.Function1<? super java.lang.Integer, java.lang.Character> defaultValue) {
        return (index < 0 || index > kotlin.text.StringsKt.getLastIndex($receiver)) ? ((java.lang.Character) defaultValue.invoke(java.lang.Integer.valueOf(index))).charValue() : $receiver.charAt(index);
    }

    @org.jetbrains.annotations.Nullable
    public static final java.lang.Character getOrNull(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, int index) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        if (index < 0 || index > kotlin.text.StringsKt.getLastIndex($receiver)) {
            return null;
        }
        return java.lang.Character.valueOf($receiver.charAt(index));
    }

    public static final int indexOfFirst(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.Character, java.lang.Boolean> predicate) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(predicate, "predicate");
        int i = 0;
        int length = $receiver.length() - 1;
        if (0 <= length) {
            while (!((java.lang.Boolean) predicate.invoke(java.lang.Character.valueOf($receiver.charAt(i)))).booleanValue()) {
                if (i != length) {
                    i++;
                }
            }
            return i;
        }
        return -1;
    }

    public static final int indexOfLast(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.Character, java.lang.Boolean> predicate) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(predicate, "predicate");
        kotlin.ranges.IntProgression reversed = kotlin.ranges.RangesKt.reversed((kotlin.ranges.IntProgression) kotlin.text.StringsKt.getIndices($receiver));
        int first = reversed.getFirst();
        int last = reversed.getLast();
        int step = reversed.getStep();
        if (step <= 0 ? first >= last : first <= last) {
            while (true) {
                int i = first;
                if (!((java.lang.Boolean) predicate.invoke(java.lang.Character.valueOf($receiver.charAt(i)))).booleanValue()) {
                    if (i == last) {
                        break;
                    }
                    first = i + step;
                } else {
                    return i;
                }
            }
        }
        return -1;
    }

    public static final char last(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        if (!($receiver.length() == 0)) {
            return $receiver.charAt(kotlin.text.StringsKt.getLastIndex($receiver));
        }
        throw new java.util.NoSuchElementException("Char sequence is empty.");
    }

    public static final char last(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.Character, java.lang.Boolean> predicate) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(predicate, "predicate");
        kotlin.ranges.IntProgression reversed = kotlin.ranges.RangesKt.reversed((kotlin.ranges.IntProgression) kotlin.text.StringsKt.getIndices($receiver));
        int first = reversed.getFirst();
        int last = reversed.getLast();
        int step = reversed.getStep();
        if (step <= 0 ? first >= last : first <= last) {
            while (true) {
                int i = first;
                char element = $receiver.charAt(i);
                if (!((java.lang.Boolean) predicate.invoke(java.lang.Character.valueOf(element))).booleanValue()) {
                    if (i == last) {
                        break;
                    }
                    first = i + step;
                } else {
                    return element;
                }
            }
        }
        throw new java.util.NoSuchElementException("Char sequence contains no character matching the predicate.");
    }

    @org.jetbrains.annotations.Nullable
    public static final java.lang.Character lastOrNull(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        if ($receiver.length() == 0) {
            return null;
        }
        return java.lang.Character.valueOf($receiver.charAt($receiver.length() - 1));
    }

    @org.jetbrains.annotations.Nullable
    public static final java.lang.Character lastOrNull(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.Character, java.lang.Boolean> predicate) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(predicate, "predicate");
        kotlin.ranges.IntProgression reversed = kotlin.ranges.RangesKt.reversed((kotlin.ranges.IntProgression) kotlin.text.StringsKt.getIndices($receiver));
        int first = reversed.getFirst();
        int last = reversed.getLast();
        int step = reversed.getStep();
        if (step <= 0 ? first >= last : first <= last) {
            while (true) {
                int i = first;
                char element = $receiver.charAt(i);
                if (!((java.lang.Boolean) predicate.invoke(java.lang.Character.valueOf(element))).booleanValue()) {
                    if (i == last) {
                        break;
                    }
                    first = i + step;
                } else {
                    return java.lang.Character.valueOf(element);
                }
            }
        }
        return null;
    }

    public static final char single(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        switch ($receiver.length()) {
            case 0:
                throw new java.util.NoSuchElementException("Char sequence is empty.");
            case 1:
                return $receiver.charAt(0);
            default:
                throw new java.lang.IllegalArgumentException("Char sequence has more than one element.");
        }
    }

    public static final char single(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.Character, java.lang.Boolean> predicate) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(predicate, "predicate");
        java.lang.Character single = null;
        boolean found = false;
        kotlin.collections.CharIterator it = kotlin.text.StringsKt.iterator($receiver);
        while (it.hasNext()) {
            char element = it.nextChar();
            if (((java.lang.Boolean) predicate.invoke(java.lang.Character.valueOf(element))).booleanValue()) {
                if (found) {
                    throw new java.lang.IllegalArgumentException("Char sequence contains more than one matching element.");
                }
                single = java.lang.Character.valueOf(element);
                found = true;
            }
        }
        if (!found) {
            throw new java.util.NoSuchElementException("Char sequence contains no character matching the predicate.");
        } else if (single != null) {
            return single.charValue();
        } else {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.Char");
        }
    }

    @org.jetbrains.annotations.Nullable
    public static final java.lang.Character singleOrNull(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        if ($receiver.length() == 1) {
            return java.lang.Character.valueOf($receiver.charAt(0));
        }
        return null;
    }

    @org.jetbrains.annotations.Nullable
    public static final java.lang.Character singleOrNull(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.Character, java.lang.Boolean> predicate) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(predicate, "predicate");
        java.lang.Character single = null;
        boolean found = false;
        kotlin.collections.CharIterator it = kotlin.text.StringsKt.iterator($receiver);
        while (it.hasNext()) {
            char element = it.nextChar();
            if (((java.lang.Boolean) predicate.invoke(java.lang.Character.valueOf(element))).booleanValue()) {
                if (found) {
                    return null;
                }
                single = java.lang.Character.valueOf(element);
                found = true;
            }
        }
        if (found) {
            return single;
        }
        return null;
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.CharSequence drop(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, int n) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        if (n >= 0) {
            return $receiver.subSequence(kotlin.ranges.RangesKt.coerceAtMost(n, $receiver.length()), $receiver.length());
        }
        throw new java.lang.IllegalArgumentException(("Requested character count " + n + " is less than zero.").toString());
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.String drop(@org.jetbrains.annotations.NotNull java.lang.String $receiver, int n) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        if (!(n >= 0)) {
            throw new java.lang.IllegalArgumentException(("Requested character count " + n + " is less than zero.").toString());
        }
        java.lang.String substring = $receiver.substring(kotlin.ranges.RangesKt.coerceAtMost(n, $receiver.length()));
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(substring, "(this as java.lang.String).substring(startIndex)");
        return substring;
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.CharSequence dropLast(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, int n) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        if (n >= 0) {
            return kotlin.text.StringsKt.take($receiver, kotlin.ranges.RangesKt.coerceAtLeast($receiver.length() - n, 0));
        }
        throw new java.lang.IllegalArgumentException(("Requested character count " + n + " is less than zero.").toString());
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.String dropLast(@org.jetbrains.annotations.NotNull java.lang.String $receiver, int n) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        if (n >= 0) {
            return kotlin.text.StringsKt.take($receiver, kotlin.ranges.RangesKt.coerceAtLeast($receiver.length() - n, 0));
        }
        throw new java.lang.IllegalArgumentException(("Requested character count " + n + " is less than zero.").toString());
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.CharSequence dropLastWhile(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.Character, java.lang.Boolean> predicate) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(predicate, "predicate");
        kotlin.ranges.IntProgression reversed = kotlin.ranges.RangesKt.reversed((kotlin.ranges.IntProgression) kotlin.text.StringsKt.getIndices($receiver));
        int first = reversed.getFirst();
        int last = reversed.getLast();
        int step = reversed.getStep();
        if (step <= 0 ? first >= last : first <= last) {
            while (true) {
                int i = first;
                if (((java.lang.Boolean) predicate.invoke(java.lang.Character.valueOf($receiver.charAt(i)))).booleanValue()) {
                    if (i == last) {
                        break;
                    }
                    first = i + step;
                } else {
                    return $receiver.subSequence(0, i + 1);
                }
            }
        }
        return "";
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.String dropLastWhile(@org.jetbrains.annotations.NotNull java.lang.String $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.Character, java.lang.Boolean> predicate) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(predicate, "predicate");
        kotlin.ranges.IntProgression reversed = kotlin.ranges.RangesKt.reversed((kotlin.ranges.IntProgression) kotlin.text.StringsKt.getIndices($receiver));
        int first = reversed.getFirst();
        int last = reversed.getLast();
        int step = reversed.getStep();
        if (step <= 0 ? first >= last : first <= last) {
            while (true) {
                int i = first;
                if (((java.lang.Boolean) predicate.invoke(java.lang.Character.valueOf($receiver.charAt(i)))).booleanValue()) {
                    if (i == last) {
                        break;
                    }
                    first = i + step;
                } else {
                    java.lang.String substring = $receiver.substring(0, i + 1);
                    kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
                    return substring;
                }
            }
        }
        return "";
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.CharSequence dropWhile(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.Character, java.lang.Boolean> predicate) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(predicate, "predicate");
        int i = 0;
        int length = $receiver.length() - 1;
        if (0 <= length) {
            while (((java.lang.Boolean) predicate.invoke(java.lang.Character.valueOf($receiver.charAt(i)))).booleanValue()) {
                if (i != length) {
                    i++;
                }
            }
            return $receiver.subSequence(i, $receiver.length());
        }
        return "";
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.String dropWhile(@org.jetbrains.annotations.NotNull java.lang.String $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.Character, java.lang.Boolean> predicate) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(predicate, "predicate");
        int i = 0;
        int length = $receiver.length() - 1;
        if (0 <= length) {
            while (((java.lang.Boolean) predicate.invoke(java.lang.Character.valueOf($receiver.charAt(i)))).booleanValue()) {
                if (i != length) {
                    i++;
                }
            }
            java.lang.String substring = $receiver.substring(i);
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(substring, "(this as java.lang.String).substring(startIndex)");
            return substring;
        }
        return "";
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.CharSequence filter(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.Character, java.lang.Boolean> predicate) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(predicate, "predicate");
        java.lang.Appendable destination$iv = new java.lang.StringBuilder();
        int i = 0;
        int length = $receiver.length() - 1;
        if (0 <= length) {
            while (true) {
                char element$iv = $receiver.charAt(i);
                if (((java.lang.Boolean) predicate.invoke(java.lang.Character.valueOf(element$iv))).booleanValue()) {
                    destination$iv.append(element$iv);
                }
                if (i == length) {
                    break;
                }
                i++;
            }
        }
        return (java.lang.CharSequence) destination$iv;
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.String filter(@org.jetbrains.annotations.NotNull java.lang.String $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.Character, java.lang.Boolean> predicate) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(predicate, "predicate");
        java.lang.CharSequence charSequence = $receiver;
        java.lang.Appendable destination$iv = new java.lang.StringBuilder();
        int i = 0;
        int length = charSequence.length() - 1;
        if (0 <= length) {
            while (true) {
                char element$iv = charSequence.charAt(i);
                if (((java.lang.Boolean) predicate.invoke(java.lang.Character.valueOf(element$iv))).booleanValue()) {
                    destination$iv.append(element$iv);
                }
                if (i == length) {
                    break;
                }
                i++;
            }
        }
        java.lang.String sb = ((java.lang.StringBuilder) destination$iv).toString();
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(sb, "filterTo(StringBuilder(), predicate).toString()");
        return sb;
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.CharSequence filterIndexed(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function2<? super java.lang.Integer, ? super java.lang.Character, java.lang.Boolean> predicate) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(predicate, "predicate");
        java.lang.Appendable destination$iv = new java.lang.StringBuilder();
        int index$iv$iv = 0;
        kotlin.collections.CharIterator it = kotlin.text.StringsKt.iterator($receiver);
        while (it.hasNext()) {
            char item$iv$iv = it.nextChar();
            int index$iv$iv2 = index$iv$iv + 1;
            if (((java.lang.Boolean) predicate.invoke(java.lang.Integer.valueOf(index$iv$iv), java.lang.Character.valueOf(item$iv$iv))).booleanValue()) {
                destination$iv.append(item$iv$iv);
            }
            index$iv$iv = index$iv$iv2;
        }
        return (java.lang.CharSequence) destination$iv;
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.String filterIndexed(@org.jetbrains.annotations.NotNull java.lang.String $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function2<? super java.lang.Integer, ? super java.lang.Character, java.lang.Boolean> predicate) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(predicate, "predicate");
        java.lang.CharSequence $receiver$iv$iv = $receiver;
        java.lang.Appendable destination$iv = new java.lang.StringBuilder();
        int index$iv$iv = 0;
        kotlin.collections.CharIterator it = kotlin.text.StringsKt.iterator($receiver$iv$iv);
        while (it.hasNext()) {
            char item$iv$iv = it.nextChar();
            int index$iv$iv2 = index$iv$iv + 1;
            if (((java.lang.Boolean) predicate.invoke(java.lang.Integer.valueOf(index$iv$iv), java.lang.Character.valueOf(item$iv$iv))).booleanValue()) {
                destination$iv.append(item$iv$iv);
            }
            index$iv$iv = index$iv$iv2;
        }
        java.lang.String sb = ((java.lang.StringBuilder) destination$iv).toString();
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(sb, "filterIndexedTo(StringBu\u2026(), predicate).toString()");
        return sb;
    }

    @org.jetbrains.annotations.NotNull
    public static final <C extends java.lang.Appendable> C filterIndexedTo(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull C destination, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function2<? super java.lang.Integer, ? super java.lang.Character, java.lang.Boolean> predicate) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(destination, "destination");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(predicate, "predicate");
        int index$iv = 0;
        kotlin.collections.CharIterator it = kotlin.text.StringsKt.iterator($receiver);
        while (it.hasNext()) {
            char item$iv = it.nextChar();
            int index$iv2 = index$iv + 1;
            if (((java.lang.Boolean) predicate.invoke(java.lang.Integer.valueOf(index$iv), java.lang.Character.valueOf(item$iv))).booleanValue()) {
                destination.append(item$iv);
            }
            index$iv = index$iv2;
        }
        return destination;
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.CharSequence filterNot(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.Character, java.lang.Boolean> predicate) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(predicate, "predicate");
        java.lang.Appendable destination$iv = new java.lang.StringBuilder();
        kotlin.collections.CharIterator it = kotlin.text.StringsKt.iterator($receiver);
        while (it.hasNext()) {
            char element$iv = it.nextChar();
            if (!((java.lang.Boolean) predicate.invoke(java.lang.Character.valueOf(element$iv))).booleanValue()) {
                destination$iv.append(element$iv);
            }
        }
        return (java.lang.CharSequence) destination$iv;
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.String filterNot(@org.jetbrains.annotations.NotNull java.lang.String $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.Character, java.lang.Boolean> predicate) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(predicate, "predicate");
        java.lang.CharSequence charSequence = $receiver;
        java.lang.Appendable destination$iv = new java.lang.StringBuilder();
        kotlin.collections.CharIterator it = kotlin.text.StringsKt.iterator(charSequence);
        while (it.hasNext()) {
            char element$iv = it.nextChar();
            if (!((java.lang.Boolean) predicate.invoke(java.lang.Character.valueOf(element$iv))).booleanValue()) {
                destination$iv.append(element$iv);
            }
        }
        java.lang.String sb = ((java.lang.StringBuilder) destination$iv).toString();
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(sb, "filterNotTo(StringBuilder(), predicate).toString()");
        return sb;
    }

    @org.jetbrains.annotations.NotNull
    public static final <C extends java.lang.Appendable> C filterNotTo(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull C destination, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.Character, java.lang.Boolean> predicate) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(destination, "destination");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(predicate, "predicate");
        kotlin.collections.CharIterator it = kotlin.text.StringsKt.iterator($receiver);
        while (it.hasNext()) {
            char element = it.nextChar();
            if (!((java.lang.Boolean) predicate.invoke(java.lang.Character.valueOf(element))).booleanValue()) {
                destination.append(element);
            }
        }
        return destination;
    }

    @org.jetbrains.annotations.NotNull
    public static final <C extends java.lang.Appendable> C filterTo(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull C destination, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.Character, java.lang.Boolean> predicate) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(destination, "destination");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(predicate, "predicate");
        int i = 0;
        int length = $receiver.length() - 1;
        if (0 <= length) {
            while (true) {
                char element = $receiver.charAt(i);
                if (((java.lang.Boolean) predicate.invoke(java.lang.Character.valueOf(element))).booleanValue()) {
                    destination.append(element);
                }
                if (i == length) {
                    break;
                }
                i++;
            }
        }
        return destination;
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.CharSequence slice(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull kotlin.ranges.IntRange indices) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(indices, "indices");
        if (indices.isEmpty()) {
            return "";
        }
        return kotlin.text.StringsKt.subSequence($receiver, indices);
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.String slice(@org.jetbrains.annotations.NotNull java.lang.String $receiver, @org.jetbrains.annotations.NotNull kotlin.ranges.IntRange indices) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(indices, "indices");
        if (indices.isEmpty()) {
            return "";
        }
        return kotlin.text.StringsKt.substring($receiver, indices);
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.CharSequence slice(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull java.lang.Iterable<java.lang.Integer> indices) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(indices, "indices");
        int size = kotlin.collections.CollectionsKt.collectionSizeOrDefault(indices, 10);
        if (size == 0) {
            return "";
        }
        java.lang.StringBuilder result = new java.lang.StringBuilder(size);
        for (java.lang.Number intValue : indices) {
            result.append($receiver.charAt(intValue.intValue()));
        }
        return result;
    }

    @kotlin.internal.InlineOnly
    private static final java.lang.String slice(@org.jetbrains.annotations.NotNull java.lang.String $receiver, java.lang.Iterable<java.lang.Integer> indices) {
        if ($receiver != null) {
            return kotlin.text.StringsKt.slice((java.lang.CharSequence) $receiver, indices).toString();
        }
        throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.CharSequence take(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, int n) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        if (n >= 0) {
            return $receiver.subSequence(0, kotlin.ranges.RangesKt.coerceAtMost(n, $receiver.length()));
        }
        throw new java.lang.IllegalArgumentException(("Requested character count " + n + " is less than zero.").toString());
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.String take(@org.jetbrains.annotations.NotNull java.lang.String $receiver, int n) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        if (!(n >= 0)) {
            throw new java.lang.IllegalArgumentException(("Requested character count " + n + " is less than zero.").toString());
        }
        java.lang.String substring = $receiver.substring(0, kotlin.ranges.RangesKt.coerceAtMost(n, $receiver.length()));
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        return substring;
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.CharSequence takeLast(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, int n) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        if (!(n >= 0)) {
            throw new java.lang.IllegalArgumentException(("Requested character count " + n + " is less than zero.").toString());
        }
        int length = $receiver.length();
        return $receiver.subSequence(length - kotlin.ranges.RangesKt.coerceAtMost(n, length), length);
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.String takeLast(@org.jetbrains.annotations.NotNull java.lang.String $receiver, int n) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        if (!(n >= 0)) {
            throw new java.lang.IllegalArgumentException(("Requested character count " + n + " is less than zero.").toString());
        }
        int length = $receiver.length();
        java.lang.String substring = $receiver.substring(length - kotlin.ranges.RangesKt.coerceAtMost(n, length));
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(substring, "(this as java.lang.String).substring(startIndex)");
        return substring;
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.CharSequence takeLastWhile(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.Character, java.lang.Boolean> predicate) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(predicate, "predicate");
        int lastIndex = kotlin.text.StringsKt.getLastIndex($receiver);
        if (lastIndex >= 0) {
            while (((java.lang.Boolean) predicate.invoke(java.lang.Character.valueOf($receiver.charAt(lastIndex)))).booleanValue()) {
                if (lastIndex != 0) {
                    lastIndex--;
                }
            }
            return $receiver.subSequence(lastIndex + 1, $receiver.length());
        }
        return $receiver.subSequence(0, $receiver.length());
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.String takeLastWhile(@org.jetbrains.annotations.NotNull java.lang.String $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.Character, java.lang.Boolean> predicate) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(predicate, "predicate");
        int lastIndex = kotlin.text.StringsKt.getLastIndex($receiver);
        if (lastIndex < 0) {
            return $receiver;
        }
        while (((java.lang.Boolean) predicate.invoke(java.lang.Character.valueOf($receiver.charAt(lastIndex)))).booleanValue()) {
            if (lastIndex == 0) {
                return $receiver;
            }
            lastIndex--;
        }
        java.lang.String $receiver2 = $receiver.substring(lastIndex + 1);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull($receiver2, "(this as java.lang.String).substring(startIndex)");
        return $receiver2;
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.CharSequence takeWhile(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.Character, java.lang.Boolean> predicate) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(predicate, "predicate");
        int length = $receiver.length() - 1;
        if (0 <= length) {
            int i = 0;
            while (((java.lang.Boolean) predicate.invoke(java.lang.Character.valueOf($receiver.charAt(i)))).booleanValue()) {
                if (i != length) {
                    i++;
                }
            }
            return $receiver.subSequence(0, i);
        }
        return $receiver.subSequence(0, $receiver.length());
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.String takeWhile(@org.jetbrains.annotations.NotNull java.lang.String $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.Character, java.lang.Boolean> predicate) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(predicate, "predicate");
        int length = $receiver.length() - 1;
        if (0 > length) {
            return $receiver;
        }
        int i = 0;
        while (((java.lang.Boolean) predicate.invoke(java.lang.Character.valueOf($receiver.charAt(i)))).booleanValue()) {
            if (i == length) {
                return $receiver;
            }
            i++;
        }
        java.lang.String $receiver2 = $receiver.substring(0, i);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull($receiver2, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        return $receiver2;
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.CharSequence reversed(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        java.lang.StringBuilder reverse = new java.lang.StringBuilder($receiver).reverse();
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(reverse, "StringBuilder(this).reverse()");
        return reverse;
    }

    @kotlin.internal.InlineOnly
    private static final java.lang.String reversed(@org.jetbrains.annotations.NotNull java.lang.String $receiver) {
        if ($receiver != null) {
            return kotlin.text.StringsKt.reversed((java.lang.CharSequence) $receiver).toString();
        }
        throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
    }

    @org.jetbrains.annotations.NotNull
    public static final <K, V> java.util.Map<K, V> associate(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.Character, ? extends kotlin.Pair<? extends K, ? extends V>> transform) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(transform, "transform");
        java.util.Map destination$iv = new java.util.LinkedHashMap(kotlin.ranges.RangesKt.coerceAtLeast(kotlin.collections.MapsKt.mapCapacity($receiver.length()), 16));
        kotlin.collections.CharIterator it = kotlin.text.StringsKt.iterator($receiver);
        while (it.hasNext()) {
            kotlin.Pair pair = (kotlin.Pair) transform.invoke(java.lang.Character.valueOf(it.nextChar()));
            destination$iv.put(pair.getFirst(), pair.getSecond());
        }
        return destination$iv;
    }

    @org.jetbrains.annotations.NotNull
    public static final <K> java.util.Map<K, java.lang.Character> associateBy(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.Character, ? extends K> keySelector) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(keySelector, "keySelector");
        java.util.Map destination$iv = new java.util.LinkedHashMap(kotlin.ranges.RangesKt.coerceAtLeast(kotlin.collections.MapsKt.mapCapacity($receiver.length()), 16));
        kotlin.collections.CharIterator it = kotlin.text.StringsKt.iterator($receiver);
        while (it.hasNext()) {
            char element$iv = it.nextChar();
            destination$iv.put(keySelector.invoke(java.lang.Character.valueOf(element$iv)), java.lang.Character.valueOf(element$iv));
        }
        return destination$iv;
    }

    @org.jetbrains.annotations.NotNull
    public static final <K, V> java.util.Map<K, V> associateBy(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.Character, ? extends K> keySelector, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.Character, ? extends V> valueTransform) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(keySelector, "keySelector");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(valueTransform, "valueTransform");
        java.util.Map destination$iv = new java.util.LinkedHashMap(kotlin.ranges.RangesKt.coerceAtLeast(kotlin.collections.MapsKt.mapCapacity($receiver.length()), 16));
        kotlin.collections.CharIterator it = kotlin.text.StringsKt.iterator($receiver);
        while (it.hasNext()) {
            char element$iv = it.nextChar();
            destination$iv.put(keySelector.invoke(java.lang.Character.valueOf(element$iv)), valueTransform.invoke(java.lang.Character.valueOf(element$iv)));
        }
        return destination$iv;
    }

    @org.jetbrains.annotations.NotNull
    public static final <K, M extends java.util.Map<? super K, ? super java.lang.Character>> M associateByTo(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull M destination, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.Character, ? extends K> keySelector) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(destination, "destination");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(keySelector, "keySelector");
        kotlin.collections.CharIterator it = kotlin.text.StringsKt.iterator($receiver);
        while (it.hasNext()) {
            char element = it.nextChar();
            destination.put(keySelector.invoke(java.lang.Character.valueOf(element)), java.lang.Character.valueOf(element));
        }
        return destination;
    }

    @org.jetbrains.annotations.NotNull
    public static final <K, V, M extends java.util.Map<? super K, ? super V>> M associateByTo(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull M destination, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.Character, ? extends K> keySelector, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.Character, ? extends V> valueTransform) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(destination, "destination");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(keySelector, "keySelector");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(valueTransform, "valueTransform");
        kotlin.collections.CharIterator it = kotlin.text.StringsKt.iterator($receiver);
        while (it.hasNext()) {
            char element = it.nextChar();
            destination.put(keySelector.invoke(java.lang.Character.valueOf(element)), valueTransform.invoke(java.lang.Character.valueOf(element)));
        }
        return destination;
    }

    @org.jetbrains.annotations.NotNull
    public static final <K, V, M extends java.util.Map<? super K, ? super V>> M associateTo(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull M destination, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.Character, ? extends kotlin.Pair<? extends K, ? extends V>> transform) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(destination, "destination");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(transform, "transform");
        kotlin.collections.CharIterator it = kotlin.text.StringsKt.iterator($receiver);
        while (it.hasNext()) {
            kotlin.Pair pair = (kotlin.Pair) transform.invoke(java.lang.Character.valueOf(it.nextChar()));
            destination.put(pair.getFirst(), pair.getSecond());
        }
        return destination;
    }

    @org.jetbrains.annotations.NotNull
    public static final <C extends java.util.Collection<? super java.lang.Character>> C toCollection(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull C destination) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(destination, "destination");
        kotlin.collections.CharIterator it = kotlin.text.StringsKt.iterator($receiver);
        while (it.hasNext()) {
            destination.add(java.lang.Character.valueOf(it.nextChar()));
        }
        return destination;
    }

    @org.jetbrains.annotations.NotNull
    public static final java.util.HashSet<java.lang.Character> toHashSet(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        return (java.util.HashSet) kotlin.text.StringsKt.toCollection($receiver, new java.util.HashSet(kotlin.collections.MapsKt.mapCapacity($receiver.length())));
    }

    @org.jetbrains.annotations.NotNull
    public static final java.util.List<java.lang.Character> toList(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        switch ($receiver.length()) {
            case 0:
                return kotlin.collections.CollectionsKt.emptyList();
            case 1:
                return kotlin.collections.CollectionsKt.listOf(java.lang.Character.valueOf($receiver.charAt(0)));
            default:
                return kotlin.text.StringsKt.toMutableList($receiver);
        }
    }

    @org.jetbrains.annotations.NotNull
    public static final java.util.List<java.lang.Character> toMutableList(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        return (java.util.List) kotlin.text.StringsKt.toCollection($receiver, new java.util.ArrayList($receiver.length()));
    }

    @org.jetbrains.annotations.NotNull
    public static final java.util.Set<java.lang.Character> toSet(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        switch ($receiver.length()) {
            case 0:
                return kotlin.collections.SetsKt.emptySet();
            case 1:
                return kotlin.collections.SetsKt.setOf(java.lang.Character.valueOf($receiver.charAt(0)));
            default:
                return (java.util.Set) kotlin.text.StringsKt.toCollection($receiver, new java.util.LinkedHashSet(kotlin.collections.MapsKt.mapCapacity($receiver.length())));
        }
    }

    @org.jetbrains.annotations.NotNull
    public static final java.util.SortedSet<java.lang.Character> toSortedSet(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        return (java.util.SortedSet) kotlin.text.StringsKt.toCollection($receiver, new java.util.TreeSet());
    }

    @org.jetbrains.annotations.NotNull
    public static final <R> java.util.List<R> flatMap(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.Character, ? extends java.lang.Iterable<? extends R>> transform) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(transform, "transform");
        java.util.Collection destination$iv = new java.util.ArrayList();
        kotlin.collections.CharIterator it = kotlin.text.StringsKt.iterator($receiver);
        while (it.hasNext()) {
            kotlin.collections.CollectionsKt.addAll(destination$iv, (java.lang.Iterable) transform.invoke(java.lang.Character.valueOf(it.nextChar())));
        }
        return (java.util.List) destination$iv;
    }

    @org.jetbrains.annotations.NotNull
    public static final <R, C extends java.util.Collection<? super R>> C flatMapTo(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull C destination, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.Character, ? extends java.lang.Iterable<? extends R>> transform) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(destination, "destination");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(transform, "transform");
        kotlin.collections.CharIterator it = kotlin.text.StringsKt.iterator($receiver);
        while (it.hasNext()) {
            kotlin.collections.CollectionsKt.addAll((java.util.Collection<? super T>) destination, (java.lang.Iterable) transform.invoke(java.lang.Character.valueOf(it.nextChar())));
        }
        return destination;
    }

    @org.jetbrains.annotations.NotNull
    public static final <K> java.util.Map<K, java.util.List<java.lang.Character>> groupBy(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.Character, ? extends K> keySelector) {
        java.lang.Object obj;
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(keySelector, "keySelector");
        java.util.Map linkedHashMap = new java.util.LinkedHashMap();
        kotlin.collections.CharIterator it = kotlin.text.StringsKt.iterator($receiver);
        while (it.hasNext()) {
            char element$iv = it.nextChar();
            java.lang.Object key$iv = keySelector.invoke(java.lang.Character.valueOf(element$iv));
            java.util.Map $receiver$iv$iv = linkedHashMap;
            java.lang.Object value$iv$iv = $receiver$iv$iv.get(key$iv);
            if (value$iv$iv == null) {
                java.util.ArrayList answer$iv$iv = new java.util.ArrayList();
                $receiver$iv$iv.put(key$iv, answer$iv$iv);
                obj = answer$iv$iv;
            } else {
                obj = value$iv$iv;
            }
            ((java.util.List) obj).add(java.lang.Character.valueOf(element$iv));
        }
        return linkedHashMap;
    }

    @org.jetbrains.annotations.NotNull
    public static final <K, V> java.util.Map<K, java.util.List<V>> groupBy(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.Character, ? extends K> keySelector, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.Character, ? extends V> valueTransform) {
        java.lang.Object obj;
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(keySelector, "keySelector");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(valueTransform, "valueTransform");
        java.util.Map linkedHashMap = new java.util.LinkedHashMap();
        kotlin.collections.CharIterator it = kotlin.text.StringsKt.iterator($receiver);
        while (it.hasNext()) {
            char element$iv = it.nextChar();
            java.lang.Object key$iv = keySelector.invoke(java.lang.Character.valueOf(element$iv));
            java.util.Map $receiver$iv$iv = linkedHashMap;
            java.lang.Object value$iv$iv = $receiver$iv$iv.get(key$iv);
            if (value$iv$iv == null) {
                java.util.ArrayList answer$iv$iv = new java.util.ArrayList();
                $receiver$iv$iv.put(key$iv, answer$iv$iv);
                obj = answer$iv$iv;
            } else {
                obj = value$iv$iv;
            }
            ((java.util.List) obj).add(valueTransform.invoke(java.lang.Character.valueOf(element$iv)));
        }
        return linkedHashMap;
    }

    @org.jetbrains.annotations.NotNull
    public static final <K, M extends java.util.Map<? super K, java.util.List<java.lang.Character>>> M groupByTo(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull M destination, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.Character, ? extends K> keySelector) {
        java.lang.Object obj;
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(destination, "destination");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(keySelector, "keySelector");
        kotlin.collections.CharIterator it = kotlin.text.StringsKt.iterator($receiver);
        while (it.hasNext()) {
            char element = it.nextChar();
            java.lang.Object key = keySelector.invoke(java.lang.Character.valueOf(element));
            java.util.Map $receiver$iv = destination;
            java.lang.Object value$iv = $receiver$iv.get(key);
            if (value$iv == null) {
                java.util.ArrayList answer$iv = new java.util.ArrayList();
                $receiver$iv.put(key, answer$iv);
                obj = answer$iv;
            } else {
                obj = value$iv;
            }
            ((java.util.List) obj).add(java.lang.Character.valueOf(element));
        }
        return destination;
    }

    @org.jetbrains.annotations.NotNull
    public static final <K, V, M extends java.util.Map<? super K, java.util.List<V>>> M groupByTo(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull M destination, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.Character, ? extends K> keySelector, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.Character, ? extends V> valueTransform) {
        java.lang.Object obj;
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(destination, "destination");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(keySelector, "keySelector");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(valueTransform, "valueTransform");
        kotlin.collections.CharIterator it = kotlin.text.StringsKt.iterator($receiver);
        while (it.hasNext()) {
            char element = it.nextChar();
            java.lang.Object key = keySelector.invoke(java.lang.Character.valueOf(element));
            java.util.Map $receiver$iv = destination;
            java.lang.Object value$iv = $receiver$iv.get(key);
            if (value$iv == null) {
                java.util.ArrayList answer$iv = new java.util.ArrayList();
                $receiver$iv.put(key, answer$iv);
                obj = answer$iv;
            } else {
                obj = value$iv;
            }
            ((java.util.List) obj).add(valueTransform.invoke(java.lang.Character.valueOf(element)));
        }
        return destination;
    }

    @org.jetbrains.annotations.NotNull
    @kotlin.SinceKotlin(version = "1.1")
    public static final <K> kotlin.collections.Grouping<java.lang.Character, K> groupingBy(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.Character, ? extends K> keySelector) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(keySelector, "keySelector");
        return new kotlin.text.StringsKt___StringsKt$groupingBy$1<>($receiver, keySelector);
    }

    @org.jetbrains.annotations.NotNull
    public static final <R> java.util.List<R> map(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.Character, ? extends R> transform) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(transform, "transform");
        java.util.Collection destination$iv = new java.util.ArrayList($receiver.length());
        kotlin.collections.CharIterator it = kotlin.text.StringsKt.iterator($receiver);
        while (it.hasNext()) {
            destination$iv.add(transform.invoke(java.lang.Character.valueOf(it.nextChar())));
        }
        return (java.util.List) destination$iv;
    }

    @org.jetbrains.annotations.NotNull
    public static final <R> java.util.List<R> mapIndexed(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function2<? super java.lang.Integer, ? super java.lang.Character, ? extends R> transform) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(transform, "transform");
        java.util.Collection destination$iv = new java.util.ArrayList($receiver.length());
        int index$iv = 0;
        kotlin.collections.CharIterator it = kotlin.text.StringsKt.iterator($receiver);
        while (it.hasNext()) {
            char item$iv = it.nextChar();
            java.lang.Integer valueOf = java.lang.Integer.valueOf(index$iv);
            index$iv++;
            destination$iv.add(transform.invoke(valueOf, java.lang.Character.valueOf(item$iv)));
        }
        return (java.util.List) destination$iv;
    }

    @org.jetbrains.annotations.NotNull
    public static final <R> java.util.List<R> mapIndexedNotNull(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function2<? super java.lang.Integer, ? super java.lang.Character, ? extends R> transform) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(transform, "transform");
        java.util.Collection destination$iv = new java.util.ArrayList();
        int index$iv$iv = 0;
        kotlin.collections.CharIterator it = kotlin.text.StringsKt.iterator($receiver);
        while (it.hasNext()) {
            int index$iv$iv2 = index$iv$iv + 1;
            java.lang.Object it$iv = transform.invoke(java.lang.Integer.valueOf(index$iv$iv), java.lang.Character.valueOf(it.nextChar()));
            if (it$iv != null) {
                destination$iv.add(it$iv);
            }
            index$iv$iv = index$iv$iv2;
        }
        return (java.util.List) destination$iv;
    }

    @org.jetbrains.annotations.NotNull
    public static final <R, C extends java.util.Collection<? super R>> C mapIndexedNotNullTo(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull C destination, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function2<? super java.lang.Integer, ? super java.lang.Character, ? extends R> transform) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(destination, "destination");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(transform, "transform");
        int index$iv = 0;
        kotlin.collections.CharIterator it = kotlin.text.StringsKt.iterator($receiver);
        while (it.hasNext()) {
            int index$iv2 = index$iv + 1;
            java.lang.Object it2 = transform.invoke(java.lang.Integer.valueOf(index$iv), java.lang.Character.valueOf(it.nextChar()));
            if (it2 != null) {
                destination.add(it2);
            }
            index$iv = index$iv2;
        }
        return destination;
    }

    @org.jetbrains.annotations.NotNull
    public static final <R, C extends java.util.Collection<? super R>> C mapIndexedTo(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull C destination, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function2<? super java.lang.Integer, ? super java.lang.Character, ? extends R> transform) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(destination, "destination");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(transform, "transform");
        int index = 0;
        kotlin.collections.CharIterator it = kotlin.text.StringsKt.iterator($receiver);
        while (it.hasNext()) {
            char item = it.nextChar();
            java.lang.Integer valueOf = java.lang.Integer.valueOf(index);
            index++;
            destination.add(transform.invoke(valueOf, java.lang.Character.valueOf(item)));
        }
        return destination;
    }

    @org.jetbrains.annotations.NotNull
    public static final <R> java.util.List<R> mapNotNull(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.Character, ? extends R> transform) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(transform, "transform");
        java.util.Collection destination$iv = new java.util.ArrayList();
        kotlin.collections.CharIterator it = kotlin.text.StringsKt.iterator($receiver);
        while (it.hasNext()) {
            java.lang.Object it$iv = transform.invoke(java.lang.Character.valueOf(it.nextChar()));
            if (it$iv != null) {
                destination$iv.add(it$iv);
            }
        }
        return (java.util.List) destination$iv;
    }

    @org.jetbrains.annotations.NotNull
    public static final <R, C extends java.util.Collection<? super R>> C mapNotNullTo(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull C destination, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.Character, ? extends R> transform) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(destination, "destination");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(transform, "transform");
        kotlin.collections.CharIterator it = kotlin.text.StringsKt.iterator($receiver);
        while (it.hasNext()) {
            java.lang.Object it2 = transform.invoke(java.lang.Character.valueOf(it.nextChar()));
            if (it2 != null) {
                destination.add(it2);
            }
        }
        return destination;
    }

    @org.jetbrains.annotations.NotNull
    public static final <R, C extends java.util.Collection<? super R>> C mapTo(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull C destination, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.Character, ? extends R> transform) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(destination, "destination");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(transform, "transform");
        kotlin.collections.CharIterator it = kotlin.text.StringsKt.iterator($receiver);
        while (it.hasNext()) {
            destination.add(transform.invoke(java.lang.Character.valueOf(it.nextChar())));
        }
        return destination;
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.Iterable<kotlin.collections.IndexedValue<java.lang.Character>> withIndex(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        return new kotlin.collections.IndexingIterable<>(new kotlin.text.StringsKt___StringsKt$withIndex$1($receiver));
    }

    public static final boolean all(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.Character, java.lang.Boolean> predicate) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(predicate, "predicate");
        kotlin.collections.CharIterator it = kotlin.text.StringsKt.iterator($receiver);
        while (it.hasNext()) {
            if (!((java.lang.Boolean) predicate.invoke(java.lang.Character.valueOf(it.nextChar()))).booleanValue()) {
                return false;
            }
        }
        return true;
    }

    public static final boolean any(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.collections.CharIterator it = kotlin.text.StringsKt.iterator($receiver);
        if (!it.hasNext()) {
            return false;
        }
        char nextChar = it.nextChar();
        return true;
    }

    public static final boolean any(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.Character, java.lang.Boolean> predicate) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(predicate, "predicate");
        kotlin.collections.CharIterator it = kotlin.text.StringsKt.iterator($receiver);
        while (it.hasNext()) {
            if (((java.lang.Boolean) predicate.invoke(java.lang.Character.valueOf(it.nextChar()))).booleanValue()) {
                return true;
            }
        }
        return false;
    }

    @kotlin.internal.InlineOnly
    private static final int count(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver) {
        return $receiver.length();
    }

    public static final int count(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.Character, java.lang.Boolean> predicate) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(predicate, "predicate");
        int count = 0;
        kotlin.collections.CharIterator it = kotlin.text.StringsKt.iterator($receiver);
        while (it.hasNext()) {
            if (((java.lang.Boolean) predicate.invoke(java.lang.Character.valueOf(it.nextChar()))).booleanValue()) {
                count++;
            }
        }
        return count;
    }

    public static final <R> R fold(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, R initial, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function2<? super R, ? super java.lang.Character, ? extends R> operation) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(operation, "operation");
        java.lang.Object accumulator = initial;
        kotlin.collections.CharIterator it = kotlin.text.StringsKt.iterator($receiver);
        while (it.hasNext()) {
            accumulator = operation.invoke(accumulator, java.lang.Character.valueOf(it.nextChar()));
        }
        return accumulator;
    }

    public static final <R> R foldIndexed(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, R initial, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function3<? super java.lang.Integer, ? super R, ? super java.lang.Character, ? extends R> operation) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(operation, "operation");
        int index = 0;
        java.lang.Object accumulator = initial;
        kotlin.collections.CharIterator it = kotlin.text.StringsKt.iterator($receiver);
        while (it.hasNext()) {
            char element = it.nextChar();
            java.lang.Integer valueOf = java.lang.Integer.valueOf(index);
            index++;
            accumulator = operation.invoke(valueOf, accumulator, java.lang.Character.valueOf(element));
        }
        return accumulator;
    }

    public static final <R> R foldRight(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, R initial, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function2<? super java.lang.Character, ? super R, ? extends R> operation) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(operation, "operation");
        java.lang.Object accumulator = initial;
        int index = kotlin.text.StringsKt.getLastIndex($receiver);
        while (index >= 0) {
            int index2 = index - 1;
            accumulator = operation.invoke(java.lang.Character.valueOf($receiver.charAt(index)), accumulator);
            index = index2;
        }
        return accumulator;
    }

    public static final <R> R foldRightIndexed(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, R initial, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function3<? super java.lang.Integer, ? super java.lang.Character, ? super R, ? extends R> operation) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(operation, "operation");
        java.lang.Object accumulator = initial;
        for (int index = kotlin.text.StringsKt.getLastIndex($receiver); index >= 0; index--) {
            accumulator = operation.invoke(java.lang.Integer.valueOf(index), java.lang.Character.valueOf($receiver.charAt(index)), accumulator);
        }
        return accumulator;
    }

    public static final void forEach(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.Character, kotlin.Unit> action) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(action, "action");
        kotlin.collections.CharIterator it = kotlin.text.StringsKt.iterator($receiver);
        while (it.hasNext()) {
            action.invoke(java.lang.Character.valueOf(it.nextChar()));
        }
    }

    public static final void forEachIndexed(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function2<? super java.lang.Integer, ? super java.lang.Character, kotlin.Unit> action) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(action, "action");
        int index = 0;
        kotlin.collections.CharIterator it = kotlin.text.StringsKt.iterator($receiver);
        while (it.hasNext()) {
            char item = it.nextChar();
            java.lang.Integer valueOf = java.lang.Integer.valueOf(index);
            index++;
            action.invoke(valueOf, java.lang.Character.valueOf(item));
        }
    }

    @org.jetbrains.annotations.Nullable
    public static final java.lang.Character max(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver) {
        int i = 1;
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        if ($receiver.length() == 0) {
            return null;
        }
        char max = $receiver.charAt(0);
        int lastIndex = kotlin.text.StringsKt.getLastIndex($receiver);
        if (1 <= lastIndex) {
            while (true) {
                char e = $receiver.charAt(i);
                if (max < e) {
                    max = e;
                }
                if (i == lastIndex) {
                    break;
                }
                i++;
            }
        }
        return java.lang.Character.valueOf(max);
    }

    @org.jetbrains.annotations.Nullable
    public static final <R extends java.lang.Comparable<? super R>> java.lang.Character maxBy(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.Character, ? extends R> selector) {
        boolean z;
        int i = 1;
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(selector, "selector");
        if ($receiver.length() == 0) {
            z = true;
        } else {
            z = false;
        }
        if (z) {
            return null;
        }
        char maxElem = $receiver.charAt(0);
        java.lang.Comparable maxValue = (java.lang.Comparable) selector.invoke(java.lang.Character.valueOf(maxElem));
        int lastIndex = kotlin.text.StringsKt.getLastIndex($receiver);
        if (1 <= lastIndex) {
            while (true) {
                char e = $receiver.charAt(i);
                java.lang.Comparable v = (java.lang.Comparable) selector.invoke(java.lang.Character.valueOf(e));
                if (maxValue.compareTo(v) < 0) {
                    maxElem = e;
                    maxValue = v;
                }
                if (i == lastIndex) {
                    break;
                }
                i++;
            }
        }
        return java.lang.Character.valueOf(maxElem);
    }

    @org.jetbrains.annotations.Nullable
    public static final java.lang.Character maxWith(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull java.util.Comparator<? super java.lang.Character> comparator) {
        int i = 1;
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(comparator, "comparator");
        if ($receiver.length() == 0) {
            return null;
        }
        char max = $receiver.charAt(0);
        int lastIndex = kotlin.text.StringsKt.getLastIndex($receiver);
        if (1 <= lastIndex) {
            while (true) {
                char e = $receiver.charAt(i);
                if (comparator.compare(java.lang.Character.valueOf(max), java.lang.Character.valueOf(e)) < 0) {
                    max = e;
                }
                if (i == lastIndex) {
                    break;
                }
                i++;
            }
        }
        return java.lang.Character.valueOf(max);
    }

    @org.jetbrains.annotations.Nullable
    public static final java.lang.Character min(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver) {
        int i = 1;
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        if ($receiver.length() == 0) {
            return null;
        }
        char min = $receiver.charAt(0);
        int lastIndex = kotlin.text.StringsKt.getLastIndex($receiver);
        if (1 <= lastIndex) {
            while (true) {
                char e = $receiver.charAt(i);
                if (min > e) {
                    min = e;
                }
                if (i == lastIndex) {
                    break;
                }
                i++;
            }
        }
        return java.lang.Character.valueOf(min);
    }

    @org.jetbrains.annotations.Nullable
    public static final <R extends java.lang.Comparable<? super R>> java.lang.Character minBy(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.Character, ? extends R> selector) {
        boolean z;
        int i = 1;
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(selector, "selector");
        if ($receiver.length() == 0) {
            z = true;
        } else {
            z = false;
        }
        if (z) {
            return null;
        }
        char minElem = $receiver.charAt(0);
        java.lang.Comparable minValue = (java.lang.Comparable) selector.invoke(java.lang.Character.valueOf(minElem));
        int lastIndex = kotlin.text.StringsKt.getLastIndex($receiver);
        if (1 <= lastIndex) {
            while (true) {
                char e = $receiver.charAt(i);
                java.lang.Comparable v = (java.lang.Comparable) selector.invoke(java.lang.Character.valueOf(e));
                if (minValue.compareTo(v) > 0) {
                    minElem = e;
                    minValue = v;
                }
                if (i == lastIndex) {
                    break;
                }
                i++;
            }
        }
        return java.lang.Character.valueOf(minElem);
    }

    @org.jetbrains.annotations.Nullable
    public static final java.lang.Character minWith(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull java.util.Comparator<? super java.lang.Character> comparator) {
        int i = 1;
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(comparator, "comparator");
        if ($receiver.length() == 0) {
            return null;
        }
        char min = $receiver.charAt(0);
        int lastIndex = kotlin.text.StringsKt.getLastIndex($receiver);
        if (1 <= lastIndex) {
            while (true) {
                char e = $receiver.charAt(i);
                if (comparator.compare(java.lang.Character.valueOf(min), java.lang.Character.valueOf(e)) > 0) {
                    min = e;
                }
                if (i == lastIndex) {
                    break;
                }
                i++;
            }
        }
        return java.lang.Character.valueOf(min);
    }

    public static final boolean none(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.collections.CharIterator it = kotlin.text.StringsKt.iterator($receiver);
        if (!it.hasNext()) {
            return true;
        }
        char nextChar = it.nextChar();
        return false;
    }

    public static final boolean none(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.Character, java.lang.Boolean> predicate) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(predicate, "predicate");
        kotlin.collections.CharIterator it = kotlin.text.StringsKt.iterator($receiver);
        while (it.hasNext()) {
            if (((java.lang.Boolean) predicate.invoke(java.lang.Character.valueOf(it.nextChar()))).booleanValue()) {
                return false;
            }
        }
        return true;
    }

    @org.jetbrains.annotations.NotNull
    @kotlin.SinceKotlin(version = "1.1")
    public static final <S extends java.lang.CharSequence> S onEach(@org.jetbrains.annotations.NotNull S $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.Character, kotlin.Unit> action) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(action, "action");
        kotlin.collections.CharIterator it = kotlin.text.StringsKt.iterator($receiver);
        while (it.hasNext()) {
            action.invoke(java.lang.Character.valueOf(it.nextChar()));
        }
        return $receiver;
    }

    public static final char reduce(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function2<? super java.lang.Character, ? super java.lang.Character, java.lang.Character> operation) {
        boolean z;
        int i = 1;
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(operation, "operation");
        if ($receiver.length() == 0) {
            z = true;
        } else {
            z = false;
        }
        if (z) {
            throw new java.lang.UnsupportedOperationException("Empty char sequence can't be reduced.");
        }
        char accumulator = $receiver.charAt(0);
        int lastIndex = kotlin.text.StringsKt.getLastIndex($receiver);
        if (1 <= lastIndex) {
            while (true) {
                accumulator = ((java.lang.Character) operation.invoke(java.lang.Character.valueOf(accumulator), java.lang.Character.valueOf($receiver.charAt(i)))).charValue();
                if (i == lastIndex) {
                    break;
                }
                i++;
            }
        }
        return accumulator;
    }

    public static final char reduceIndexed(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function3<? super java.lang.Integer, ? super java.lang.Character, ? super java.lang.Character, java.lang.Character> operation) {
        boolean z;
        int i = 1;
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(operation, "operation");
        if ($receiver.length() == 0) {
            z = true;
        } else {
            z = false;
        }
        if (z) {
            throw new java.lang.UnsupportedOperationException("Empty char sequence can't be reduced.");
        }
        char accumulator = $receiver.charAt(0);
        int lastIndex = kotlin.text.StringsKt.getLastIndex($receiver);
        if (1 <= lastIndex) {
            while (true) {
                accumulator = ((java.lang.Character) operation.invoke(java.lang.Integer.valueOf(i), java.lang.Character.valueOf(accumulator), java.lang.Character.valueOf($receiver.charAt(i)))).charValue();
                if (i == lastIndex) {
                    break;
                }
                i++;
            }
        }
        return accumulator;
    }

    public static final char reduceRight(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function2<? super java.lang.Character, ? super java.lang.Character, java.lang.Character> operation) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(operation, "operation");
        int index = kotlin.text.StringsKt.getLastIndex($receiver);
        if (index < 0) {
            throw new java.lang.UnsupportedOperationException("Empty char sequence can't be reduced.");
        }
        int index2 = index - 1;
        char accumulator = $receiver.charAt(index);
        while (index2 >= 0) {
            int index3 = index2 - 1;
            accumulator = ((java.lang.Character) operation.invoke(java.lang.Character.valueOf($receiver.charAt(index2)), java.lang.Character.valueOf(accumulator))).charValue();
            index2 = index3;
        }
        return accumulator;
    }

    public static final char reduceRightIndexed(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function3<? super java.lang.Integer, ? super java.lang.Character, ? super java.lang.Character, java.lang.Character> operation) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(operation, "operation");
        int index = kotlin.text.StringsKt.getLastIndex($receiver);
        if (index < 0) {
            throw new java.lang.UnsupportedOperationException("Empty char sequence can't be reduced.");
        }
        int index2 = index - 1;
        char accumulator = $receiver.charAt(index);
        for (int index3 = index2; index3 >= 0; index3--) {
            accumulator = ((java.lang.Character) operation.invoke(java.lang.Integer.valueOf(index3), java.lang.Character.valueOf($receiver.charAt(index3)), java.lang.Character.valueOf(accumulator))).charValue();
        }
        return accumulator;
    }

    public static final int sumBy(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.Character, java.lang.Integer> selector) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(selector, "selector");
        int sum = 0;
        kotlin.collections.CharIterator it = kotlin.text.StringsKt.iterator($receiver);
        while (it.hasNext()) {
            sum += ((java.lang.Number) selector.invoke(java.lang.Character.valueOf(it.nextChar()))).intValue();
        }
        return sum;
    }

    public static final double sumByDouble(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.Character, java.lang.Double> selector) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(selector, "selector");
        double sum = 0.0d;
        kotlin.collections.CharIterator it = kotlin.text.StringsKt.iterator($receiver);
        while (it.hasNext()) {
            sum += ((java.lang.Number) selector.invoke(java.lang.Character.valueOf(it.nextChar()))).doubleValue();
        }
        return sum;
    }

    @org.jetbrains.annotations.NotNull
    public static final kotlin.Pair<java.lang.CharSequence, java.lang.CharSequence> partition(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.Character, java.lang.Boolean> predicate) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(predicate, "predicate");
        java.lang.StringBuilder first = new java.lang.StringBuilder();
        java.lang.StringBuilder second = new java.lang.StringBuilder();
        kotlin.collections.CharIterator it = kotlin.text.StringsKt.iterator($receiver);
        while (it.hasNext()) {
            char element = it.nextChar();
            if (((java.lang.Boolean) predicate.invoke(java.lang.Character.valueOf(element))).booleanValue()) {
                first.append(element);
            } else {
                second.append(element);
            }
        }
        return new kotlin.Pair<>(first, second);
    }

    @org.jetbrains.annotations.NotNull
    public static final kotlin.Pair<java.lang.String, java.lang.String> partition(@org.jetbrains.annotations.NotNull java.lang.String $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.Character, java.lang.Boolean> predicate) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(predicate, "predicate");
        java.lang.StringBuilder first = new java.lang.StringBuilder();
        java.lang.StringBuilder second = new java.lang.StringBuilder();
        kotlin.collections.CharIterator it = kotlin.text.StringsKt.iterator($receiver);
        while (it.hasNext()) {
            char element = it.nextChar();
            if (((java.lang.Boolean) predicate.invoke(java.lang.Character.valueOf(element))).booleanValue()) {
                first.append(element);
            } else {
                second.append(element);
            }
        }
        return new kotlin.Pair<>(first.toString(), second.toString());
    }

    @org.jetbrains.annotations.NotNull
    public static final java.util.List<kotlin.Pair<java.lang.Character, java.lang.Character>> zip(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull java.lang.CharSequence other) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(other, "other");
        java.lang.CharSequence $receiver$iv = $receiver;
        int length$iv = java.lang.Math.min($receiver$iv.length(), other.length());
        java.util.ArrayList list$iv = new java.util.ArrayList(length$iv);
        int i = 0;
        int i2 = length$iv - 1;
        if (0 <= i2) {
            while (true) {
                list$iv.add(kotlin.TuplesKt.to(java.lang.Character.valueOf($receiver$iv.charAt(i)), java.lang.Character.valueOf(other.charAt(i))));
                if (i == i2) {
                    break;
                }
                i++;
            }
        }
        return list$iv;
    }

    @org.jetbrains.annotations.NotNull
    public static final <V> java.util.List<V> zip(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull java.lang.CharSequence other, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function2<? super java.lang.Character, ? super java.lang.Character, ? extends V> transform) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(other, "other");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(transform, "transform");
        int length = java.lang.Math.min($receiver.length(), other.length());
        java.util.ArrayList list = new java.util.ArrayList(length);
        int i = 0;
        int i2 = length - 1;
        if (0 <= i2) {
            while (true) {
                list.add(transform.invoke(java.lang.Character.valueOf($receiver.charAt(i)), java.lang.Character.valueOf(other.charAt(i))));
                if (i == i2) {
                    break;
                }
                i++;
            }
        }
        return list;
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.Iterable<java.lang.Character> asIterable(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        if ($receiver instanceof java.lang.String) {
            if ($receiver.length() == 0) {
                return kotlin.collections.CollectionsKt.emptyList();
            }
        }
        return new kotlin.text.StringsKt___StringsKt$asIterable$$inlined$Iterable$1<>($receiver);
    }

    @org.jetbrains.annotations.NotNull
    public static final kotlin.sequences.Sequence<java.lang.Character> asSequence(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        if ($receiver instanceof java.lang.String) {
            if ($receiver.length() == 0) {
                return kotlin.sequences.SequencesKt.emptySequence();
            }
        }
        return new kotlin.text.StringsKt___StringsKt$asSequence$$inlined$Sequence$1<>($receiver);
    }
}
