package kotlin.text;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000'\n\u0000\n\u0002\u0010(\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0013\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002*\u0001\u0000\b\n\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\b\u0010\u0018\u001a\u00020\u0019H\u0002J\t\u0010\u001a\u001a\u00020\u001bH\u0096\u0002J\t\u0010\u001c\u001a\u00020\u0002H\u0096\u0002R\u001a\u0010\u0004\u001a\u00020\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\tR\u001a\u0010\n\u001a\u00020\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\u0007\"\u0004\b\f\u0010\tR\u001c\u0010\r\u001a\u0004\u0018\u00010\u0002X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011R\u001a\u0010\u0012\u001a\u00020\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0007\"\u0004\b\u0014\u0010\tR\u001a\u0010\u0015\u001a\u00020\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0016\u0010\u0007\"\u0004\b\u0017\u0010\t\u00a8\u0006\u001d"}, d2 = {"kotlin/text/DelimitedRangesSequence$iterator$1", "", "Lkotlin/ranges/IntRange;", "(Lkotlin/text/DelimitedRangesSequence;)V", "counter", "", "getCounter", "()I", "setCounter", "(I)V", "currentStartIndex", "getCurrentStartIndex", "setCurrentStartIndex", "nextItem", "getNextItem", "()Lkotlin/ranges/IntRange;", "setNextItem", "(Lkotlin/ranges/IntRange;)V", "nextSearchIndex", "getNextSearchIndex", "setNextSearchIndex", "nextState", "getNextState", "setNextState", "calcNext", "", "hasNext", "", "next", "kotlin-stdlib"}, k = 1, mv = {1, 1, 6})
/* compiled from: Strings.kt */
public final class DelimitedRangesSequence$iterator$1 implements java.util.Iterator<kotlin.ranges.IntRange>, kotlin.jvm.internal.markers.KMappedMarker {
    private int counter;
    private int currentStartIndex;
    @org.jetbrains.annotations.Nullable
    private kotlin.ranges.IntRange nextItem;
    private int nextSearchIndex;
    private int nextState = -1;
    final /* synthetic */ kotlin.text.DelimitedRangesSequence this$0;

    public void remove() {
        throw new java.lang.UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    DelimitedRangesSequence$iterator$1(kotlin.text.DelimitedRangesSequence $outer) {
        this.this$0 = $outer;
        this.currentStartIndex = kotlin.ranges.RangesKt.coerceIn($outer.startIndex, 0, $outer.input.length());
        this.nextSearchIndex = this.currentStartIndex;
    }

    public final int getNextState() {
        return this.nextState;
    }

    public final void setNextState(int i) {
        this.nextState = i;
    }

    public final int getCurrentStartIndex() {
        return this.currentStartIndex;
    }

    public final void setCurrentStartIndex(int i) {
        this.currentStartIndex = i;
    }

    public final int getNextSearchIndex() {
        return this.nextSearchIndex;
    }

    public final void setNextSearchIndex(int i) {
        this.nextSearchIndex = i;
    }

    @org.jetbrains.annotations.Nullable
    public final kotlin.ranges.IntRange getNextItem() {
        return this.nextItem;
    }

    public final void setNextItem(@org.jetbrains.annotations.Nullable kotlin.ranges.IntRange intRange) {
        this.nextItem = intRange;
    }

    public final int getCounter() {
        return this.counter;
    }

    public final void setCounter(int i) {
        this.counter = i;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0025, code lost:
        if (r8.counter < r8.this$0.limit) goto L_0x0027;
     */
    private final void calcNext() {
        if (this.nextSearchIndex < 0) {
            this.nextState = 0;
            this.nextItem = null;
            return;
        }
        if (this.this$0.limit > 0) {
            this.counter++;
        }
        if (this.nextSearchIndex <= this.this$0.input.length()) {
            kotlin.Pair match = (kotlin.Pair) this.this$0.getNextMatch.invoke(this.this$0.input, java.lang.Integer.valueOf(this.nextSearchIndex));
            if (match == null) {
                this.nextItem = new kotlin.ranges.IntRange(this.currentStartIndex, kotlin.text.StringsKt.getLastIndex(this.this$0.input));
                this.nextSearchIndex = -1;
            } else {
                int intValue = ((java.lang.Number) match.component1()).intValue();
                int intValue2 = ((java.lang.Number) match.component2()).intValue();
                this.nextItem = new kotlin.ranges.IntRange(this.currentStartIndex, intValue - 1);
                this.currentStartIndex = intValue + intValue2;
                this.nextSearchIndex = (intValue2 == 0 ? 1 : 0) + this.currentStartIndex;
            }
            this.nextState = 1;
        }
        this.nextItem = new kotlin.ranges.IntRange(this.currentStartIndex, kotlin.text.StringsKt.getLastIndex(this.this$0.input));
        this.nextSearchIndex = -1;
        this.nextState = 1;
    }

    @org.jetbrains.annotations.NotNull
    public kotlin.ranges.IntRange next() {
        if (this.nextState == -1) {
            calcNext();
        }
        if (this.nextState == 0) {
            throw new java.util.NoSuchElementException();
        }
        kotlin.ranges.IntRange result = this.nextItem;
        if (result == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.ranges.IntRange");
        }
        this.nextItem = null;
        this.nextState = -1;
        return result;
    }

    public boolean hasNext() {
        if (this.nextState == -1) {
            calcNext();
        }
        if (this.nextState == 1) {
            return true;
        }
        return false;
    }
}
