package kotlin.text;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000x\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0012\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0019\n\u0000\n\u0002\u0010\u0015\n\u0002\b\n\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\r\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0010\u0000\n\u0002\b\b\n\u0002\u0010\f\n\u0002\b\u0011\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000e\u001a\u0011\u0010\u0006\u001a\u00020\u00022\u0006\u0010\u0007\u001a\u00020\bH\u0087\b\u001a\u0011\u0010\u0006\u001a\u00020\u00022\u0006\u0010\t\u001a\u00020\nH\u0087\b\u001a\u0011\u0010\u0006\u001a\u00020\u00022\u0006\u0010\u000b\u001a\u00020\fH\u0087\b\u001a\u0019\u0010\u0006\u001a\u00020\u00022\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0087\b\u001a!\u0010\u0006\u001a\u00020\u00022\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0010H\u0087\b\u001a)\u0010\u0006\u001a\u00020\u00022\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00102\u0006\u0010\r\u001a\u00020\u000eH\u0087\b\u001a\u0011\u0010\u0006\u001a\u00020\u00022\u0006\u0010\u0012\u001a\u00020\u0013H\u0087\b\u001a!\u0010\u0006\u001a\u00020\u00022\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0010H\u0087\b\u001a!\u0010\u0006\u001a\u00020\u00022\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0010H\u0087\b\u001a\n\u0010\u0016\u001a\u00020\u0002*\u00020\u0002\u001a\u0015\u0010\u0017\u001a\u00020\u0010*\u00020\u00022\u0006\u0010\u0018\u001a\u00020\u0010H\u0087\b\u001a\u0015\u0010\u0019\u001a\u00020\u0010*\u00020\u00022\u0006\u0010\u0018\u001a\u00020\u0010H\u0087\b\u001a\u001d\u0010\u001a\u001a\u00020\u0010*\u00020\u00022\u0006\u0010\u001b\u001a\u00020\u00102\u0006\u0010\u001c\u001a\u00020\u0010H\u0087\b\u001a\u001c\u0010\u001d\u001a\u00020\u0010*\u00020\u00022\u0006\u0010\u001e\u001a\u00020\u00022\b\b\u0002\u0010\u001f\u001a\u00020 \u001a\u0015\u0010!\u001a\u00020 *\u00020\u00022\u0006\u0010\t\u001a\u00020\bH\u0087\b\u001a\u0015\u0010!\u001a\u00020 *\u00020\u00022\u0006\u0010\"\u001a\u00020#H\u0087\b\u001a\n\u0010$\u001a\u00020\u0002*\u00020\u0002\u001a\u001c\u0010%\u001a\u00020 *\u00020\u00022\u0006\u0010&\u001a\u00020\u00022\b\b\u0002\u0010\u001f\u001a\u00020 \u001a \u0010'\u001a\u00020 *\u0004\u0018\u00010\u00022\b\u0010\u001e\u001a\u0004\u0018\u00010\u00022\b\b\u0002\u0010\u001f\u001a\u00020 \u001a2\u0010(\u001a\u00020\u0002*\u00020\u00022\u0006\u0010)\u001a\u00020*2\u0016\u0010+\u001a\f\u0012\b\b\u0001\u0012\u0004\u0018\u00010-0,\"\u0004\u0018\u00010-H\u0087\b\u00a2\u0006\u0002\u0010.\u001a*\u0010(\u001a\u00020\u0002*\u00020\u00022\u0016\u0010+\u001a\f\u0012\b\b\u0001\u0012\u0004\u0018\u00010-0,\"\u0004\u0018\u00010-H\u0087\b\u00a2\u0006\u0002\u0010/\u001a:\u0010(\u001a\u00020\u0002*\u00020\u00032\u0006\u0010)\u001a\u00020*2\u0006\u0010(\u001a\u00020\u00022\u0016\u0010+\u001a\f\u0012\b\b\u0001\u0012\u0004\u0018\u00010-0,\"\u0004\u0018\u00010-H\u0087\b\u00a2\u0006\u0002\u00100\u001a2\u0010(\u001a\u00020\u0002*\u00020\u00032\u0006\u0010(\u001a\u00020\u00022\u0016\u0010+\u001a\f\u0012\b\b\u0001\u0012\u0004\u0018\u00010-0,\"\u0004\u0018\u00010-H\u0087\b\u00a2\u0006\u0002\u00101\u001a\r\u00102\u001a\u00020\u0002*\u00020\u0002H\u0087\b\u001a\n\u00103\u001a\u00020 *\u00020#\u001a\u001d\u00104\u001a\u00020\u0010*\u00020\u00022\u0006\u00105\u001a\u0002062\u0006\u00107\u001a\u00020\u0010H\u0081\b\u001a\u001d\u00104\u001a\u00020\u0010*\u00020\u00022\u0006\u00108\u001a\u00020\u00022\u0006\u00107\u001a\u00020\u0010H\u0081\b\u001a\u001d\u00109\u001a\u00020\u0010*\u00020\u00022\u0006\u00105\u001a\u0002062\u0006\u00107\u001a\u00020\u0010H\u0081\b\u001a\u001d\u00109\u001a\u00020\u0010*\u00020\u00022\u0006\u00108\u001a\u00020\u00022\u0006\u00107\u001a\u00020\u0010H\u0081\b\u001a\u001d\u0010:\u001a\u00020\u0010*\u00020\u00022\u0006\u0010\u0018\u001a\u00020\u00102\u0006\u0010;\u001a\u00020\u0010H\u0087\b\u001a4\u0010<\u001a\u00020 *\u00020#2\u0006\u0010=\u001a\u00020\u00102\u0006\u0010\u001e\u001a\u00020#2\u0006\u0010>\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00102\b\b\u0002\u0010\u001f\u001a\u00020 \u001a4\u0010<\u001a\u00020 *\u00020\u00022\u0006\u0010=\u001a\u00020\u00102\u0006\u0010\u001e\u001a\u00020\u00022\u0006\u0010>\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00102\b\b\u0002\u0010\u001f\u001a\u00020 \u001a\u0012\u0010?\u001a\u00020\u0002*\u00020#2\u0006\u0010@\u001a\u00020\u0010\u001a$\u0010A\u001a\u00020\u0002*\u00020\u00022\u0006\u0010B\u001a\u0002062\u0006\u0010C\u001a\u0002062\b\b\u0002\u0010\u001f\u001a\u00020 \u001a$\u0010A\u001a\u00020\u0002*\u00020\u00022\u0006\u0010D\u001a\u00020\u00022\u0006\u0010E\u001a\u00020\u00022\b\b\u0002\u0010\u001f\u001a\u00020 \u001a$\u0010F\u001a\u00020\u0002*\u00020\u00022\u0006\u0010B\u001a\u0002062\u0006\u0010C\u001a\u0002062\b\b\u0002\u0010\u001f\u001a\u00020 \u001a$\u0010F\u001a\u00020\u0002*\u00020\u00022\u0006\u0010D\u001a\u00020\u00022\u0006\u0010E\u001a\u00020\u00022\b\b\u0002\u0010\u001f\u001a\u00020 \u001a\"\u0010G\u001a\b\u0012\u0004\u0012\u00020\u00020H*\u00020#2\u0006\u0010I\u001a\u00020J2\b\b\u0002\u0010K\u001a\u00020\u0010\u001a\u001c\u0010L\u001a\u00020 *\u00020\u00022\u0006\u0010M\u001a\u00020\u00022\b\b\u0002\u0010\u001f\u001a\u00020 \u001a$\u0010L\u001a\u00020 *\u00020\u00022\u0006\u0010M\u001a\u00020\u00022\u0006\u0010N\u001a\u00020\u00102\b\b\u0002\u0010\u001f\u001a\u00020 \u001a\u0015\u0010O\u001a\u00020\u0002*\u00020\u00022\u0006\u0010N\u001a\u00020\u0010H\u0087\b\u001a\u001d\u0010O\u001a\u00020\u0002*\u00020\u00022\u0006\u0010N\u001a\u00020\u00102\u0006\u0010\u001c\u001a\u00020\u0010H\u0087\b\u001a\u0017\u0010P\u001a\u00020\f*\u00020\u00022\b\b\u0002\u0010\r\u001a\u00020\u000eH\u0087\b\u001a\r\u0010Q\u001a\u00020\u0013*\u00020\u0002H\u0087\b\u001a3\u0010Q\u001a\u00020\u0013*\u00020\u00022\u0006\u0010R\u001a\u00020\u00132\b\b\u0002\u0010S\u001a\u00020\u00102\b\b\u0002\u0010N\u001a\u00020\u00102\b\b\u0002\u0010\u001c\u001a\u00020\u0010H\u0087\b\u001a\r\u0010T\u001a\u00020\u0002*\u00020\u0002H\u0087\b\u001a\u0015\u0010T\u001a\u00020\u0002*\u00020\u00022\u0006\u0010)\u001a\u00020*H\u0087\b\u001a\u0017\u0010U\u001a\u00020J*\u00020\u00022\b\b\u0002\u0010V\u001a\u00020\u0010H\u0087\b\u001a\r\u0010W\u001a\u00020\u0002*\u00020\u0002H\u0087\b\u001a\u0015\u0010W\u001a\u00020\u0002*\u00020\u00022\u0006\u0010)\u001a\u00020*H\u0087\b\"\u001b\u0010\u0000\u001a\b\u0012\u0004\u0012\u00020\u00020\u0001*\u00020\u00038F\u00a2\u0006\u0006\u001a\u0004\b\u0004\u0010\u0005\u00a8\u0006X"}, d2 = {"CASE_INSENSITIVE_ORDER", "Ljava/util/Comparator;", "", "Lkotlin/String$Companion;", "getCASE_INSENSITIVE_ORDER", "(Lkotlin/jvm/internal/StringCompanionObject;)Ljava/util/Comparator;", "String", "stringBuffer", "Ljava/lang/StringBuffer;", "stringBuilder", "Ljava/lang/StringBuilder;", "bytes", "", "charset", "Ljava/nio/charset/Charset;", "offset", "", "length", "chars", "", "codePoints", "", "capitalize", "codePointAt", "index", "codePointBefore", "codePointCount", "beginIndex", "endIndex", "compareTo", "other", "ignoreCase", "", "contentEquals", "charSequence", "", "decapitalize", "endsWith", "suffix", "equals", "format", "locale", "Ljava/util/Locale;", "args", "", "", "(Ljava/lang/String;Ljava/util/Locale;[Ljava/lang/Object;)Ljava/lang/String;", "(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;", "(Lkotlin/jvm/internal/StringCompanionObject;Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;", "(Lkotlin/jvm/internal/StringCompanionObject;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;", "intern", "isBlank", "nativeIndexOf", "ch", "", "fromIndex", "str", "nativeLastIndexOf", "offsetByCodePoints", "codePointOffset", "regionMatches", "thisOffset", "otherOffset", "repeat", "n", "replace", "oldChar", "newChar", "oldValue", "newValue", "replaceFirst", "split", "", "regex", "Ljava/util/regex/Pattern;", "limit", "startsWith", "prefix", "startIndex", "substring", "toByteArray", "toCharArray", "destination", "destinationOffset", "toLowerCase", "toPattern", "flags", "toUpperCase", "kotlin-stdlib"}, k = 5, mv = {1, 1, 6}, xi = 1, xs = "kotlin/text/StringsKt")
/* compiled from: StringsJVM.kt */
class StringsKt__StringsJVMKt extends kotlin.text.StringsKt__StringNumberConversionsKt {
    @kotlin.internal.InlineOnly
    private static final int nativeIndexOf(@org.jetbrains.annotations.NotNull java.lang.String $receiver, char ch2, int fromIndex) {
        if ($receiver != null) {
            return $receiver.indexOf(ch2, fromIndex);
        }
        throw new kotlin.TypeCastException("null cannot be cast to non-null type java.lang.String");
    }

    @kotlin.internal.InlineOnly
    private static final int nativeIndexOf(@org.jetbrains.annotations.NotNull java.lang.String $receiver, java.lang.String str, int fromIndex) {
        if ($receiver != null) {
            return $receiver.indexOf(str, fromIndex);
        }
        throw new kotlin.TypeCastException("null cannot be cast to non-null type java.lang.String");
    }

    @kotlin.internal.InlineOnly
    private static final int nativeLastIndexOf(@org.jetbrains.annotations.NotNull java.lang.String $receiver, char ch2, int fromIndex) {
        if ($receiver != null) {
            return $receiver.lastIndexOf(ch2, fromIndex);
        }
        throw new kotlin.TypeCastException("null cannot be cast to non-null type java.lang.String");
    }

    @kotlin.internal.InlineOnly
    private static final int nativeLastIndexOf(@org.jetbrains.annotations.NotNull java.lang.String $receiver, java.lang.String str, int fromIndex) {
        if ($receiver != null) {
            return $receiver.lastIndexOf(str, fromIndex);
        }
        throw new kotlin.TypeCastException("null cannot be cast to non-null type java.lang.String");
    }

    public static /* bridge */ /* synthetic */ boolean equals$default(java.lang.String str, java.lang.String str2, boolean z, int i, java.lang.Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        return kotlin.text.StringsKt.equals(str, str2, z);
    }

    public static final boolean equals(@org.jetbrains.annotations.Nullable java.lang.String $receiver, @org.jetbrains.annotations.Nullable java.lang.String other, boolean ignoreCase) {
        if ($receiver == null) {
            return other == null;
        }
        if (!ignoreCase) {
            return $receiver.equals(other);
        }
        return $receiver.equalsIgnoreCase(other);
    }

    @org.jetbrains.annotations.NotNull
    public static /* bridge */ /* synthetic */ java.lang.String replace$default(java.lang.String str, char c, char c2, boolean z, int i, java.lang.Object obj) {
        if ((i & 4) != 0) {
            z = false;
        }
        return kotlin.text.StringsKt.replace(str, c, c2, z);
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.String replace(@org.jetbrains.annotations.NotNull java.lang.String $receiver, char oldChar, char newChar, boolean ignoreCase) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        if (!ignoreCase) {
            java.lang.String replace = $receiver.replace(oldChar, newChar);
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(replace, "(this as java.lang.Strin\u2026replace(oldChar, newChar)");
            return replace;
        }
        return kotlin.sequences.SequencesKt.joinToString$default(kotlin.text.StringsKt.splitToSequence$default((java.lang.CharSequence) $receiver, new char[]{oldChar}, ignoreCase, 0, 4, (java.lang.Object) null), java.lang.String.valueOf(newChar), null, null, 0, null, null, 62, null);
    }

    @org.jetbrains.annotations.NotNull
    public static /* bridge */ /* synthetic */ java.lang.String replace$default(java.lang.String str, java.lang.String str2, java.lang.String str3, boolean z, int i, java.lang.Object obj) {
        if ((i & 4) != 0) {
            z = false;
        }
        return kotlin.text.StringsKt.replace(str, str2, str3, z);
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.String replace(@org.jetbrains.annotations.NotNull java.lang.String $receiver, @org.jetbrains.annotations.NotNull java.lang.String oldValue, @org.jetbrains.annotations.NotNull java.lang.String newValue, boolean ignoreCase) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(oldValue, "oldValue");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(newValue, "newValue");
        return kotlin.sequences.SequencesKt.joinToString$default(kotlin.text.StringsKt.splitToSequence$default((java.lang.CharSequence) $receiver, new java.lang.String[]{oldValue}, ignoreCase, 0, 4, (java.lang.Object) null), newValue, null, null, 0, null, null, 62, null);
    }

    @org.jetbrains.annotations.NotNull
    public static /* bridge */ /* synthetic */ java.lang.String replaceFirst$default(java.lang.String str, char c, char c2, boolean z, int i, java.lang.Object obj) {
        if ((i & 4) != 0) {
            z = false;
        }
        return kotlin.text.StringsKt.replaceFirst(str, c, c2, z);
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.String replaceFirst(@org.jetbrains.annotations.NotNull java.lang.String $receiver, char oldChar, char newChar, boolean ignoreCase) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        int index = kotlin.text.StringsKt.indexOf$default((java.lang.CharSequence) $receiver, oldChar, 0, ignoreCase, 2, (java.lang.Object) null);
        if (index < 0) {
            return $receiver;
        }
        return kotlin.text.StringsKt.replaceRange((java.lang.CharSequence) $receiver, index, index + 1, (java.lang.CharSequence) java.lang.String.valueOf(newChar)).toString();
    }

    @org.jetbrains.annotations.NotNull
    public static /* bridge */ /* synthetic */ java.lang.String replaceFirst$default(java.lang.String str, java.lang.String str2, java.lang.String str3, boolean z, int i, java.lang.Object obj) {
        if ((i & 4) != 0) {
            z = false;
        }
        return kotlin.text.StringsKt.replaceFirst(str, str2, str3, z);
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.String replaceFirst(@org.jetbrains.annotations.NotNull java.lang.String $receiver, @org.jetbrains.annotations.NotNull java.lang.String oldValue, @org.jetbrains.annotations.NotNull java.lang.String newValue, boolean ignoreCase) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(oldValue, "oldValue");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(newValue, "newValue");
        int index = kotlin.text.StringsKt.indexOf$default((java.lang.CharSequence) $receiver, oldValue, 0, ignoreCase, 2, (java.lang.Object) null);
        if (index < 0) {
            return $receiver;
        }
        return kotlin.text.StringsKt.replaceRange((java.lang.CharSequence) $receiver, index, oldValue.length() + index, (java.lang.CharSequence) newValue).toString();
    }

    @kotlin.internal.InlineOnly
    private static final java.lang.String toUpperCase(@org.jetbrains.annotations.NotNull java.lang.String $receiver) {
        if ($receiver == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type java.lang.String");
        }
        java.lang.String upperCase = $receiver.toUpperCase();
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(upperCase, "(this as java.lang.String).toUpperCase()");
        return upperCase;
    }

    @kotlin.internal.InlineOnly
    private static final java.lang.String toLowerCase(@org.jetbrains.annotations.NotNull java.lang.String $receiver) {
        if ($receiver == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type java.lang.String");
        }
        java.lang.String lowerCase = $receiver.toLowerCase();
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(lowerCase, "(this as java.lang.String).toLowerCase()");
        return lowerCase;
    }

    @kotlin.internal.InlineOnly
    private static final char[] toCharArray(@org.jetbrains.annotations.NotNull java.lang.String $receiver) {
        if ($receiver == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type java.lang.String");
        }
        char[] charArray = $receiver.toCharArray();
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(charArray, "(this as java.lang.String).toCharArray()");
        return charArray;
    }

    @kotlin.internal.InlineOnly
    static /* bridge */ /* synthetic */ char[] toCharArray$default(java.lang.String $receiver, char[] destination, int destinationOffset, int startIndex, int endIndex, int i, java.lang.Object obj) {
        if ((i & 2) != 0) {
            destinationOffset = 0;
        }
        if ((i & 4) != 0) {
            startIndex = 0;
        }
        if ((i & 8) != 0) {
            endIndex = $receiver.length();
        }
        if ($receiver == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type java.lang.String");
        }
        $receiver.getChars(startIndex, endIndex, destination, destinationOffset);
        return destination;
    }

    @kotlin.internal.InlineOnly
    private static final char[] toCharArray(@org.jetbrains.annotations.NotNull java.lang.String $receiver, char[] destination, int destinationOffset, int startIndex, int endIndex) {
        if ($receiver == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type java.lang.String");
        }
        $receiver.getChars(startIndex, endIndex, destination, destinationOffset);
        return destination;
    }

    @kotlin.internal.InlineOnly
    private static final java.lang.String format(@org.jetbrains.annotations.NotNull java.lang.String $receiver, java.lang.Object... args) {
        java.lang.String format = java.lang.String.format($receiver, java.util.Arrays.copyOf(args, args.length));
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(format, "java.lang.String.format(this, *args)");
        return format;
    }

    @kotlin.internal.InlineOnly
    private static final java.lang.String format(@org.jetbrains.annotations.NotNull kotlin.jvm.internal.StringCompanionObject $receiver, java.lang.String format, java.lang.Object... args) {
        java.lang.String format2 = java.lang.String.format(format, java.util.Arrays.copyOf(args, args.length));
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(format2, "java.lang.String.format(format, *args)");
        return format2;
    }

    @kotlin.internal.InlineOnly
    private static final java.lang.String format(@org.jetbrains.annotations.NotNull java.lang.String $receiver, java.util.Locale locale, java.lang.Object... args) {
        java.lang.String format = java.lang.String.format(locale, $receiver, java.util.Arrays.copyOf(args, args.length));
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(format, "java.lang.String.format(locale, this, *args)");
        return format;
    }

    @kotlin.internal.InlineOnly
    private static final java.lang.String format(@org.jetbrains.annotations.NotNull kotlin.jvm.internal.StringCompanionObject $receiver, java.util.Locale locale, java.lang.String format, java.lang.Object... args) {
        java.lang.String format2 = java.lang.String.format(locale, format, java.util.Arrays.copyOf(args, args.length));
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(format2, "java.lang.String.format(locale, format, *args)");
        return format2;
    }

    @org.jetbrains.annotations.NotNull
    public static /* bridge */ /* synthetic */ java.util.List split$default(java.lang.CharSequence charSequence, java.util.regex.Pattern pattern, int i, int i2, java.lang.Object obj) {
        if ((i2 & 2) != 0) {
            i = 0;
        }
        return kotlin.text.StringsKt.split(charSequence, pattern, i);
    }

    @org.jetbrains.annotations.NotNull
    public static final java.util.List<java.lang.String> split(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, @org.jetbrains.annotations.NotNull java.util.regex.Pattern regex, int limit) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(regex, "regex");
        if (!(limit >= 0)) {
            throw new java.lang.IllegalArgumentException(("Limit must be non-negative, but was " + limit + ".").toString());
        }
        if (limit == 0) {
            limit = -1;
        }
        return kotlin.collections.ArraysKt.asList((T[]) (java.lang.Object[]) regex.split($receiver, limit));
    }

    @kotlin.internal.InlineOnly
    private static final java.lang.String substring(@org.jetbrains.annotations.NotNull java.lang.String $receiver, int startIndex) {
        if ($receiver == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type java.lang.String");
        }
        java.lang.String substring = $receiver.substring(startIndex);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(substring, "(this as java.lang.String).substring(startIndex)");
        return substring;
    }

    @kotlin.internal.InlineOnly
    private static final java.lang.String substring(@org.jetbrains.annotations.NotNull java.lang.String $receiver, int startIndex, int endIndex) {
        if ($receiver == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type java.lang.String");
        }
        java.lang.String substring = $receiver.substring(startIndex, endIndex);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        return substring;
    }

    public static /* bridge */ /* synthetic */ boolean startsWith$default(java.lang.String str, java.lang.String str2, boolean z, int i, java.lang.Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        return kotlin.text.StringsKt.startsWith(str, str2, z);
    }

    public static final boolean startsWith(@org.jetbrains.annotations.NotNull java.lang.String $receiver, @org.jetbrains.annotations.NotNull java.lang.String prefix, boolean ignoreCase) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(prefix, "prefix");
        if (!ignoreCase) {
            return $receiver.startsWith(prefix);
        }
        return kotlin.text.StringsKt.regionMatches($receiver, 0, prefix, 0, prefix.length(), ignoreCase);
    }

    public static /* bridge */ /* synthetic */ boolean startsWith$default(java.lang.String str, java.lang.String str2, int i, boolean z, int i2, java.lang.Object obj) {
        if ((i2 & 4) != 0) {
            z = false;
        }
        return kotlin.text.StringsKt.startsWith(str, str2, i, z);
    }

    public static final boolean startsWith(@org.jetbrains.annotations.NotNull java.lang.String $receiver, @org.jetbrains.annotations.NotNull java.lang.String prefix, int startIndex, boolean ignoreCase) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(prefix, "prefix");
        if (!ignoreCase) {
            return $receiver.startsWith(prefix, startIndex);
        }
        return kotlin.text.StringsKt.regionMatches($receiver, startIndex, prefix, 0, prefix.length(), ignoreCase);
    }

    public static /* bridge */ /* synthetic */ boolean endsWith$default(java.lang.String str, java.lang.String str2, boolean z, int i, java.lang.Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        return kotlin.text.StringsKt.endsWith(str, str2, z);
    }

    public static final boolean endsWith(@org.jetbrains.annotations.NotNull java.lang.String $receiver, @org.jetbrains.annotations.NotNull java.lang.String suffix, boolean ignoreCase) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(suffix, "suffix");
        if (!ignoreCase) {
            return $receiver.endsWith(suffix);
        }
        return kotlin.text.StringsKt.regionMatches($receiver, $receiver.length() - suffix.length(), suffix, 0, suffix.length(), true);
    }

    @kotlin.internal.InlineOnly
    private static final java.lang.String String(byte[] bytes, int offset, int length, java.nio.charset.Charset charset) {
        return new java.lang.String(bytes, offset, length, charset);
    }

    @kotlin.internal.InlineOnly
    private static final java.lang.String String(byte[] bytes, java.nio.charset.Charset charset) {
        return new java.lang.String(bytes, charset);
    }

    @kotlin.internal.InlineOnly
    private static final java.lang.String String(byte[] bytes, int offset, int length) {
        return new java.lang.String(bytes, offset, length, kotlin.text.Charsets.UTF_8);
    }

    @kotlin.internal.InlineOnly
    private static final java.lang.String String(byte[] bytes) {
        return new java.lang.String(bytes, kotlin.text.Charsets.UTF_8);
    }

    @kotlin.internal.InlineOnly
    private static final java.lang.String String(char[] chars) {
        return new java.lang.String(chars);
    }

    @kotlin.internal.InlineOnly
    private static final java.lang.String String(char[] chars, int offset, int length) {
        return new java.lang.String(chars, offset, length);
    }

    @kotlin.internal.InlineOnly
    private static final java.lang.String String(int[] codePoints, int offset, int length) {
        return new java.lang.String(codePoints, offset, length);
    }

    @kotlin.internal.InlineOnly
    private static final java.lang.String String(java.lang.StringBuffer stringBuffer) {
        return new java.lang.String(stringBuffer);
    }

    @kotlin.internal.InlineOnly
    private static final java.lang.String String(java.lang.StringBuilder stringBuilder) {
        return new java.lang.String(stringBuilder);
    }

    @kotlin.internal.InlineOnly
    private static final int codePointAt(@org.jetbrains.annotations.NotNull java.lang.String $receiver, int index) {
        if ($receiver != null) {
            return $receiver.codePointAt(index);
        }
        throw new kotlin.TypeCastException("null cannot be cast to non-null type java.lang.String");
    }

    @kotlin.internal.InlineOnly
    private static final int codePointBefore(@org.jetbrains.annotations.NotNull java.lang.String $receiver, int index) {
        if ($receiver != null) {
            return $receiver.codePointBefore(index);
        }
        throw new kotlin.TypeCastException("null cannot be cast to non-null type java.lang.String");
    }

    @kotlin.internal.InlineOnly
    private static final int codePointCount(@org.jetbrains.annotations.NotNull java.lang.String $receiver, int beginIndex, int endIndex) {
        if ($receiver != null) {
            return $receiver.codePointCount(beginIndex, endIndex);
        }
        throw new kotlin.TypeCastException("null cannot be cast to non-null type java.lang.String");
    }

    public static /* bridge */ /* synthetic */ int compareTo$default(java.lang.String str, java.lang.String str2, boolean z, int i, java.lang.Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        return kotlin.text.StringsKt.compareTo(str, str2, z);
    }

    public static final int compareTo(@org.jetbrains.annotations.NotNull java.lang.String $receiver, @org.jetbrains.annotations.NotNull java.lang.String other, boolean ignoreCase) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(other, "other");
        if (ignoreCase) {
            return $receiver.compareToIgnoreCase(other);
        }
        return $receiver.compareTo(other);
    }

    @kotlin.internal.InlineOnly
    private static final boolean contentEquals(@org.jetbrains.annotations.NotNull java.lang.String $receiver, java.lang.CharSequence charSequence) {
        if ($receiver != null) {
            return $receiver.contentEquals(charSequence);
        }
        throw new kotlin.TypeCastException("null cannot be cast to non-null type java.lang.String");
    }

    @kotlin.internal.InlineOnly
    private static final boolean contentEquals(@org.jetbrains.annotations.NotNull java.lang.String $receiver, java.lang.StringBuffer stringBuilder) {
        if ($receiver != null) {
            return $receiver.contentEquals(stringBuilder);
        }
        throw new kotlin.TypeCastException("null cannot be cast to non-null type java.lang.String");
    }

    @kotlin.internal.InlineOnly
    private static final java.lang.String intern(@org.jetbrains.annotations.NotNull java.lang.String $receiver) {
        if ($receiver == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type java.lang.String");
        }
        java.lang.String intern = $receiver.intern();
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(intern, "(this as java.lang.String).intern()");
        return intern;
    }

    public static final boolean isBlank(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver) {
        boolean z;
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        if ($receiver.length() != 0) {
            java.util.Iterator it = kotlin.text.StringsKt.getIndices($receiver).iterator();
            while (true) {
                if (it.hasNext()) {
                    if (!kotlin.text.CharsKt.isWhitespace($receiver.charAt(((kotlin.collections.IntIterator) it).nextInt()))) {
                        z = false;
                        break;
                    }
                } else {
                    z = true;
                    break;
                }
            }
            if (!z) {
                return false;
            }
        }
        return true;
    }

    @kotlin.internal.InlineOnly
    private static final int offsetByCodePoints(@org.jetbrains.annotations.NotNull java.lang.String $receiver, int index, int codePointOffset) {
        if ($receiver != null) {
            return $receiver.offsetByCodePoints(index, codePointOffset);
        }
        throw new kotlin.TypeCastException("null cannot be cast to non-null type java.lang.String");
    }

    public static /* bridge */ /* synthetic */ boolean regionMatches$default(java.lang.CharSequence charSequence, int i, java.lang.CharSequence charSequence2, int i2, int i3, boolean z, int i4, java.lang.Object obj) {
        return kotlin.text.StringsKt.regionMatches(charSequence, i, charSequence2, i2, i3, (i4 & 16) != 0 ? false : z);
    }

    public static final boolean regionMatches(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, int thisOffset, @org.jetbrains.annotations.NotNull java.lang.CharSequence other, int otherOffset, int length, boolean ignoreCase) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(other, "other");
        if (!($receiver instanceof java.lang.String) || !(other instanceof java.lang.String)) {
            return kotlin.text.StringsKt.regionMatchesImpl($receiver, thisOffset, other, otherOffset, length, ignoreCase);
        }
        return kotlin.text.StringsKt.regionMatches((java.lang.String) $receiver, thisOffset, (java.lang.String) other, otherOffset, length, ignoreCase);
    }

    public static /* bridge */ /* synthetic */ boolean regionMatches$default(java.lang.String str, int i, java.lang.String str2, int i2, int i3, boolean z, int i4, java.lang.Object obj) {
        return kotlin.text.StringsKt.regionMatches(str, i, str2, i2, i3, (i4 & 16) != 0 ? false : z);
    }

    public static final boolean regionMatches(@org.jetbrains.annotations.NotNull java.lang.String $receiver, int thisOffset, @org.jetbrains.annotations.NotNull java.lang.String other, int otherOffset, int length, boolean ignoreCase) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(other, "other");
        if (!ignoreCase) {
            return $receiver.regionMatches(thisOffset, other, otherOffset, length);
        }
        return $receiver.regionMatches(ignoreCase, thisOffset, other, otherOffset, length);
    }

    @kotlin.internal.InlineOnly
    private static final java.lang.String toLowerCase(@org.jetbrains.annotations.NotNull java.lang.String $receiver, java.util.Locale locale) {
        if ($receiver == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type java.lang.String");
        }
        java.lang.String lowerCase = $receiver.toLowerCase(locale);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(lowerCase, "(this as java.lang.String).toLowerCase(locale)");
        return lowerCase;
    }

    @kotlin.internal.InlineOnly
    private static final java.lang.String toUpperCase(@org.jetbrains.annotations.NotNull java.lang.String $receiver, java.util.Locale locale) {
        if ($receiver == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type java.lang.String");
        }
        java.lang.String upperCase = $receiver.toUpperCase(locale);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(upperCase, "(this as java.lang.String).toUpperCase(locale)");
        return upperCase;
    }

    @kotlin.internal.InlineOnly
    private static final byte[] toByteArray(@org.jetbrains.annotations.NotNull java.lang.String $receiver, java.nio.charset.Charset charset) {
        if ($receiver == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type java.lang.String");
        }
        byte[] bytes = $receiver.getBytes(charset);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(bytes, "(this as java.lang.String).getBytes(charset)");
        return bytes;
    }

    @kotlin.internal.InlineOnly
    static /* bridge */ /* synthetic */ byte[] toByteArray$default(java.lang.String $receiver, java.nio.charset.Charset charset, int i, java.lang.Object obj) {
        if ((i & 1) != 0) {
            charset = kotlin.text.Charsets.UTF_8;
        }
        if ($receiver == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type java.lang.String");
        }
        byte[] bytes = $receiver.getBytes(charset);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(bytes, "(this as java.lang.String).getBytes(charset)");
        return bytes;
    }

    @kotlin.internal.InlineOnly
    static /* bridge */ /* synthetic */ java.util.regex.Pattern toPattern$default(java.lang.String $receiver, int flags, int i, java.lang.Object obj) {
        if ((i & 1) != 0) {
            flags = 0;
        }
        java.util.regex.Pattern compile = java.util.regex.Pattern.compile($receiver, flags);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(compile, "java.util.regex.Pattern.compile(this, flags)");
        return compile;
    }

    @kotlin.internal.InlineOnly
    private static final java.util.regex.Pattern toPattern(@org.jetbrains.annotations.NotNull java.lang.String $receiver, int flags) {
        java.util.regex.Pattern compile = java.util.regex.Pattern.compile($receiver, flags);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(compile, "java.util.regex.Pattern.compile(this, flags)");
        return compile;
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.String capitalize(@org.jetbrains.annotations.NotNull java.lang.String $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        if (!($receiver.length() > 0) || !java.lang.Character.isLowerCase($receiver.charAt(0))) {
            return $receiver;
        }
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        java.lang.String substring = $receiver.substring(0, 1);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        if (substring == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type java.lang.String");
        }
        java.lang.String upperCase = substring.toUpperCase();
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(upperCase, "(this as java.lang.String).toUpperCase()");
        java.lang.StringBuilder append = sb.append(upperCase);
        java.lang.String substring2 = $receiver.substring(1);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(substring2, "(this as java.lang.String).substring(startIndex)");
        return append.append(substring2).toString();
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.String decapitalize(@org.jetbrains.annotations.NotNull java.lang.String $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        if (!($receiver.length() > 0) || !java.lang.Character.isUpperCase($receiver.charAt(0))) {
            return $receiver;
        }
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        java.lang.String substring = $receiver.substring(0, 1);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        if (substring == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type java.lang.String");
        }
        java.lang.String lowerCase = substring.toLowerCase();
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(lowerCase, "(this as java.lang.String).toLowerCase()");
        java.lang.StringBuilder append = sb.append(lowerCase);
        java.lang.String substring2 = $receiver.substring(1);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(substring2, "(this as java.lang.String).substring(startIndex)");
        return append.append(substring2).toString();
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.String repeat(@org.jetbrains.annotations.NotNull java.lang.CharSequence $receiver, int n) {
        int i = 1;
        int i2 = 0;
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        if (!(n >= 0)) {
            throw new java.lang.IllegalArgumentException(("Count 'n' must be non-negative, but was " + n + ".").toString());
        }
        switch (n) {
            case 0:
                return "";
            case 1:
                return $receiver.toString();
            default:
                switch ($receiver.length()) {
                    case 0:
                        return "";
                    case 1:
                        char charAt = $receiver.charAt(0);
                        int size$iv = n;
                        char[] result$iv = new char[size$iv];
                        int i3 = size$iv - 1;
                        if (0 <= i3) {
                            while (true) {
                                result$iv[i2] = charAt;
                                if (i2 != i3) {
                                    i2++;
                                }
                            }
                        }
                        return new java.lang.String(result$iv);
                    default:
                        java.lang.StringBuilder sb = new java.lang.StringBuilder($receiver.length() * n);
                        if (1 <= n) {
                            while (true) {
                                sb.append($receiver);
                                if (i != n) {
                                    i++;
                                }
                            }
                        }
                        java.lang.String sb2 = sb.toString();
                        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(sb2, "sb.toString()");
                        return sb2;
                }
        }
    }

    @org.jetbrains.annotations.NotNull
    public static final java.util.Comparator<java.lang.String> getCASE_INSENSITIVE_ORDER(@org.jetbrains.annotations.NotNull kotlin.jvm.internal.StringCompanionObject $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        java.util.Comparator<java.lang.String> comparator = java.lang.String.CASE_INSENSITIVE_ORDER;
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(comparator, "java.lang.String.CASE_INSENSITIVE_ORDER");
        return comparator;
    }
}
