package kotlin.text;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000H\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0010\r\n\u0000\n\u0002\u0010\u0000\n\u0002\b\b\n\u0002\u0010\f\n\u0000\u001a.\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u001b\u0010\u0004\u001a\u0017\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\b0\u0005\u00a2\u0006\u0002\b\tH\u0087\b\u001a&\u0010\u0000\u001a\u00020\u00012\u001b\u0010\u0004\u001a\u0017\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\b0\u0005\u00a2\u0006\u0002\b\tH\u0087\b\u001a5\u0010\n\u001a\u0002H\u000b\"\f\b\u0000\u0010\u000b*\u00060\fj\u0002`\r*\u0002H\u000b2\u0016\u0010\u000e\u001a\f\u0012\b\b\u0001\u0012\u0004\u0018\u00010\u00100\u000f\"\u0004\u0018\u00010\u0010\u00a2\u0006\u0002\u0010\u0011\u001a/\u0010\n\u001a\u00060\u0006j\u0002`\u0007*\u00060\u0006j\u0002`\u00072\u0016\u0010\u000e\u001a\f\u0012\b\b\u0001\u0012\u0004\u0018\u00010\u00120\u000f\"\u0004\u0018\u00010\u0012\u00a2\u0006\u0002\u0010\u0013\u001a/\u0010\n\u001a\u00060\u0006j\u0002`\u0007*\u00060\u0006j\u0002`\u00072\u0016\u0010\u000e\u001a\f\u0012\b\b\u0001\u0012\u0004\u0018\u00010\u00010\u000f\"\u0004\u0018\u00010\u0001\u00a2\u0006\u0002\u0010\u0014\u001a9\u0010\u0015\u001a\u00020\b\"\u0004\b\u0000\u0010\u000b*\u00060\fj\u0002`\r2\u0006\u0010\u0016\u001a\u0002H\u000b2\u0014\u0010\u0017\u001a\u0010\u0012\u0004\u0012\u0002H\u000b\u0012\u0004\u0012\u00020\u0010\u0018\u00010\u0005H\u0000\u00a2\u0006\u0002\u0010\u0018\u001a!\u0010\u0019\u001a\u00020\b*\u00060\u0006j\u0002`\u00072\u0006\u0010\u001a\u001a\u00020\u00032\u0006\u0010\u000e\u001a\u00020\u001bH\u0087\n\u00a8\u0006\u001c"}, d2 = {"buildString", "", "capacity", "", "builderAction", "Lkotlin/Function1;", "Ljava/lang/StringBuilder;", "Lkotlin/text/StringBuilder;", "", "Lkotlin/ExtensionFunctionType;", "append", "T", "Ljava/lang/Appendable;", "Lkotlin/text/Appendable;", "value", "", "", "(Ljava/lang/Appendable;[Ljava/lang/CharSequence;)Ljava/lang/Appendable;", "", "(Ljava/lang/StringBuilder;[Ljava/lang/Object;)Ljava/lang/StringBuilder;", "(Ljava/lang/StringBuilder;[Ljava/lang/String;)Ljava/lang/StringBuilder;", "appendElement", "element", "transform", "(Ljava/lang/Appendable;Ljava/lang/Object;Lkotlin/jvm/functions/Function1;)V", "set", "index", "", "kotlin-stdlib"}, k = 5, mv = {1, 1, 6}, xi = 1, xs = "kotlin/text/StringsKt")
/* compiled from: StringBuilder.kt */
class StringsKt__StringBuilderKt extends kotlin.text.StringsKt__StringBuilderJVMKt {
    @kotlin.internal.InlineOnly
    private static final java.lang.String buildString(kotlin.jvm.functions.Function1<? super java.lang.StringBuilder, kotlin.Unit> builderAction) {
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        builderAction.invoke(sb);
        java.lang.String sb2 = sb.toString();
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(sb2, "StringBuilder().apply(builderAction).toString()");
        return sb2;
    }

    @kotlin.SinceKotlin(version = "1.1")
    @kotlin.internal.InlineOnly
    private static final java.lang.String buildString(int capacity, kotlin.jvm.functions.Function1<? super java.lang.StringBuilder, kotlin.Unit> builderAction) {
        java.lang.StringBuilder sb = new java.lang.StringBuilder(capacity);
        builderAction.invoke(sb);
        java.lang.String sb2 = sb.toString();
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(sb2, "StringBuilder(capacity).\u2026builderAction).toString()");
        return sb2;
    }

    @org.jetbrains.annotations.NotNull
    public static final <T extends java.lang.Appendable> T append(@org.jetbrains.annotations.NotNull T $receiver, @org.jetbrains.annotations.NotNull java.lang.CharSequence... value) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(value, "value");
        for (java.lang.CharSequence item : value) {
            $receiver.append(item);
        }
        return $receiver;
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.StringBuilder append(@org.jetbrains.annotations.NotNull java.lang.StringBuilder $receiver, @org.jetbrains.annotations.NotNull java.lang.String... value) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(value, "value");
        for (java.lang.String item : value) {
            $receiver.append(item);
        }
        return $receiver;
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.StringBuilder append(@org.jetbrains.annotations.NotNull java.lang.StringBuilder $receiver, @org.jetbrains.annotations.NotNull java.lang.Object... value) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(value, "value");
        for (java.lang.Object item : value) {
            $receiver.append(item);
        }
        return $receiver;
    }

    @kotlin.internal.InlineOnly
    private static final void set(@org.jetbrains.annotations.NotNull java.lang.StringBuilder $receiver, int index, char value) {
        $receiver.setCharAt(index, value);
    }

    public static final <T> void appendElement(@org.jetbrains.annotations.NotNull java.lang.Appendable $receiver, T element, @org.jetbrains.annotations.Nullable kotlin.jvm.functions.Function1<? super T, ? extends java.lang.CharSequence> transform) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        if (transform != null) {
            $receiver.append((java.lang.CharSequence) transform.invoke(element));
            return;
        }
        if (element != null ? element instanceof java.lang.CharSequence : true) {
            $receiver.append((java.lang.CharSequence) element);
        } else if (element instanceof java.lang.Character) {
            $receiver.append(((java.lang.Character) element).charValue());
        } else {
            $receiver.append(java.lang.String.valueOf(element));
        }
    }
}
