package kotlin.text;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u00c3\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u0010\u0010\u0003\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0005"}, d2 = {"Lkotlin/text/ScreenFloatValueRegEx;", "", "()V", "value", "Lkotlin/text/Regex;", "kotlin-stdlib"}, k = 1, mv = {1, 1, 6})
/* compiled from: StringNumberConversions.kt */
final class ScreenFloatValueRegEx {
    public static final kotlin.text.ScreenFloatValueRegEx INSTANCE = null;
    @org.jetbrains.annotations.NotNull
    @kotlin.jvm.JvmField
    public static final kotlin.text.Regex value = null;

    static {
        new kotlin.text.ScreenFloatValueRegEx();
    }

    private ScreenFloatValueRegEx() {
        INSTANCE = this;
        kotlin.text.ScreenFloatValueRegEx screenFloatValueRegEx = this;
        java.lang.String Digits = "(\\p{Digit}+)";
        java.lang.String HexDigits = "(\\p{XDigit}+)";
        java.lang.String Exp = "[eE][+-]?" + Digits;
        value = new kotlin.text.Regex("[" + "\\" + "x00-" + "\\" + "x20]*[+-]?(NaN|Infinity|((" + (("(" + Digits + "(" + "\\" + ".)?(" + Digits + "?)(" + Exp + ")?)|") + ("(" + "\\" + ".(" + Digits + ")(" + Exp + ")?)|") + ("((" + (("(0[xX]" + HexDigits + "(" + "\\" + ".)?)|") + ("(0[xX]" + HexDigits + "?(" + "\\" + ".)" + HexDigits + ")")) + ")[pP][+-]?" + Digits + ")")) + ")[fFdD]?))[" + "\\" + "x00-" + "\\" + "x20]*");
    }
}
