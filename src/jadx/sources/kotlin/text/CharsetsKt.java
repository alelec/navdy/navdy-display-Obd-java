package kotlin.text;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u001a\u0011\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\u0087\b\u00a8\u0006\u0004"}, d2 = {"charset", "Ljava/nio/charset/Charset;", "charsetName", "", "kotlin-stdlib"}, k = 2, mv = {1, 1, 6})
@kotlin.jvm.JvmName(name = "CharsetsKt")
/* compiled from: Charsets.kt */
public final class CharsetsKt {
    @kotlin.internal.InlineOnly
    private static final java.nio.charset.Charset charset(java.lang.String charsetName) {
        java.nio.charset.Charset forName = java.nio.charset.Charset.forName(charsetName);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(forName, "Charset.forName(charsetName)");
        return forName;
    }
}
