package kotlin.text;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0010\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u0010\u0010\u0003\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\b\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\t\u001a\u00020\u00048G\u00a2\u0006\u0006\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\f\u001a\u00020\u00048G\u00a2\u0006\u0006\u001a\u0004\b\r\u0010\u000bR\u0011\u0010\u000e\u001a\u00020\u00048G\u00a2\u0006\u0006\u001a\u0004\b\u000f\u0010\u000bR\u0010\u0010\u0010\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0011\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0012\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0013\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"}, d2 = {"Lkotlin/text/Charsets;", "", "()V", "ISO_8859_1", "Ljava/nio/charset/Charset;", "US_ASCII", "UTF_16", "UTF_16BE", "UTF_16LE", "UTF_32", "UTF32", "()Ljava/nio/charset/Charset;", "UTF_32BE", "UTF32_BE", "UTF_32LE", "UTF32_LE", "UTF_8", "utf_32", "utf_32be", "utf_32le", "kotlin-stdlib"}, k = 1, mv = {1, 1, 6})
/* compiled from: Charsets.kt */
public final class Charsets {
    public static final kotlin.text.Charsets INSTANCE = null;
    @org.jetbrains.annotations.NotNull
    @kotlin.jvm.JvmField
    public static final java.nio.charset.Charset ISO_8859_1 = null;
    @org.jetbrains.annotations.NotNull
    @kotlin.jvm.JvmField
    public static final java.nio.charset.Charset US_ASCII = null;
    @org.jetbrains.annotations.NotNull
    @kotlin.jvm.JvmField
    public static final java.nio.charset.Charset UTF_16 = null;
    @org.jetbrains.annotations.NotNull
    @kotlin.jvm.JvmField
    public static final java.nio.charset.Charset UTF_16BE = null;
    @org.jetbrains.annotations.NotNull
    @kotlin.jvm.JvmField
    public static final java.nio.charset.Charset UTF_16LE = null;
    @org.jetbrains.annotations.NotNull
    @kotlin.jvm.JvmField
    public static final java.nio.charset.Charset UTF_8 = null;
    private static java.nio.charset.Charset utf_32;
    private static java.nio.charset.Charset utf_32be;
    private static java.nio.charset.Charset utf_32le;

    static {
        new kotlin.text.Charsets();
    }

    private Charsets() {
        INSTANCE = this;
        java.nio.charset.Charset forName = java.nio.charset.Charset.forName(util.Util.UTF_8);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(forName, "Charset.forName(\"UTF-8\")");
        UTF_8 = forName;
        java.nio.charset.Charset forName2 = java.nio.charset.Charset.forName("UTF-16");
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(forName2, "Charset.forName(\"UTF-16\")");
        UTF_16 = forName2;
        java.nio.charset.Charset forName3 = java.nio.charset.Charset.forName("UTF-16BE");
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(forName3, "Charset.forName(\"UTF-16BE\")");
        UTF_16BE = forName3;
        java.nio.charset.Charset forName4 = java.nio.charset.Charset.forName("UTF-16LE");
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(forName4, "Charset.forName(\"UTF-16LE\")");
        UTF_16LE = forName4;
        java.nio.charset.Charset forName5 = java.nio.charset.Charset.forName("US-ASCII");
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(forName5, "Charset.forName(\"US-ASCII\")");
        US_ASCII = forName5;
        java.nio.charset.Charset forName6 = java.nio.charset.Charset.forName("ISO-8859-1");
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(forName6, "Charset.forName(\"ISO-8859-1\")");
        ISO_8859_1 = forName6;
    }

    @org.jetbrains.annotations.NotNull
    @kotlin.jvm.JvmName(name = "UTF32")
    public final java.nio.charset.Charset UTF32() {
        java.nio.charset.Charset charset = utf_32;
        if (charset != null) {
            return charset;
        }
        kotlin.text.Charsets charsets = this;
        java.nio.charset.Charset charset2 = java.nio.charset.Charset.forName("UTF-32");
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(charset2, "Charset.forName(\"UTF-32\")");
        utf_32 = charset2;
        return charset2;
    }

    @org.jetbrains.annotations.NotNull
    @kotlin.jvm.JvmName(name = "UTF32_LE")
    public final java.nio.charset.Charset UTF32_LE() {
        java.nio.charset.Charset charset = utf_32le;
        if (charset != null) {
            return charset;
        }
        kotlin.text.Charsets charsets = this;
        java.nio.charset.Charset charset2 = java.nio.charset.Charset.forName("UTF-32LE");
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(charset2, "Charset.forName(\"UTF-32LE\")");
        utf_32le = charset2;
        return charset2;
    }

    @org.jetbrains.annotations.NotNull
    @kotlin.jvm.JvmName(name = "UTF32_BE")
    public final java.nio.charset.Charset UTF32_BE() {
        java.nio.charset.Charset charset = utf_32be;
        if (charset != null) {
            return charset;
        }
        kotlin.text.Charsets charsets = this;
        java.nio.charset.Charset charset2 = java.nio.charset.Charset.forName("UTF-32BE");
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(charset2, "Charset.forName(\"UTF-32BE\")");
        utf_32be = charset2;
        return charset2;
    }
}
