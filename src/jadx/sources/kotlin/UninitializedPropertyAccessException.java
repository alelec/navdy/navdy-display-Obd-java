package kotlin;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0003\n\u0002\b\u0003\u0018\u00002\u00060\u0001j\u0002`\u0002B\u0007\b\u0016\u00a2\u0006\u0002\u0010\u0003B\u000f\b\u0016\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006B\u0017\b\u0016\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\b\u00a2\u0006\u0002\u0010\tB\u000f\b\u0016\u0012\u0006\u0010\u0007\u001a\u00020\b\u00a2\u0006\u0002\u0010\n\u00a8\u0006\u000b"}, d2 = {"Lkotlin/UninitializedPropertyAccessException;", "Ljava/lang/RuntimeException;", "Lkotlin/RuntimeException;", "()V", "message", "", "(Ljava/lang/String;)V", "cause", "", "(Ljava/lang/String;Ljava/lang/Throwable;)V", "(Ljava/lang/Throwable;)V", "kotlin-runtime"}, k = 1, mv = {1, 1, 6})
/* compiled from: UninitializedPropertyAccessException.kt */
public final class UninitializedPropertyAccessException extends java.lang.RuntimeException {
    public UninitializedPropertyAccessException() {
    }

    public UninitializedPropertyAccessException(@org.jetbrains.annotations.NotNull java.lang.String message) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(message, "message");
        super(message);
    }

    public UninitializedPropertyAccessException(@org.jetbrains.annotations.NotNull java.lang.String message, @org.jetbrains.annotations.NotNull java.lang.Throwable cause) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(message, "message");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(cause, "cause");
        super(message, cause);
    }

    public UninitializedPropertyAccessException(@org.jetbrains.annotations.NotNull java.lang.Throwable cause) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(cause, "cause");
        super(cause);
    }
}
