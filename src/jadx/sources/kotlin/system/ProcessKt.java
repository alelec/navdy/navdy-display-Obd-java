package kotlin.system;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0001\n\u0000\n\u0002\u0010\b\n\u0000\u001a\u0011\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\u0087\b\u00a8\u0006\u0004"}, d2 = {"exitProcess", "", "status", "", "kotlin-stdlib"}, k = 2, mv = {1, 1, 6})
@kotlin.jvm.JvmName(name = "ProcessKt")
/* compiled from: Process.kt */
public final class ProcessKt {
    @kotlin.internal.InlineOnly
    private static final java.lang.Void exitProcess(int status) {
        java.lang.System.exit(status);
        throw new java.lang.RuntimeException("System.exit returned normally, while it was supposed to halt JVM.");
    }
}
