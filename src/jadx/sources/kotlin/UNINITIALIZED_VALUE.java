package kotlin;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\b\u00c2\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lkotlin/UNINITIALIZED_VALUE;", "", "()V", "kotlin-stdlib"}, k = 1, mv = {1, 1, 6})
/* compiled from: Lazy.kt */
final class UNINITIALIZED_VALUE {
    public static final kotlin.UNINITIALIZED_VALUE INSTANCE = null;

    static {
        new kotlin.UNINITIALIZED_VALUE();
    }

    private UNINITIALIZED_VALUE() {
        INSTANCE = this;
    }
}
