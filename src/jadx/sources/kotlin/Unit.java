package kotlin;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0010\n\u0002\u0010\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0002\b\u0002J\b\u0010\u0002\u001a\u00020\u0003H\u0016\u00a8\u0006\u0004"}, d2 = {"", "", "toString", "", "kotlin-runtime"}, k = 1, mv = {1, 1, 6})
/* compiled from: Unit.kt */
public final class Unit {
    public static final kotlin.Unit INSTANCE = null;

    static {
        new kotlin.Unit();
    }

    private Unit() {
        INSTANCE = this;
    }

    @org.jetbrains.annotations.NotNull
    public java.lang.String toString() {
        return "kotlin.Unit";
    }
}
