package kotlin;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000:\n\u0000\n\u0002\u0010\u0001\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0002\u001a\t\u0010\u0000\u001a\u00020\u0001H\u0087\b\u001a\u0011\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\u0087\b\u001a%\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0012\u0010\b\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00050\tH\u0087\b\u001a\"\u0010\n\u001a\u0002H\u000b\"\u0004\b\u0000\u0010\u000b2\f\u0010\f\u001a\b\u0012\u0004\u0012\u0002H\u000b0\rH\u0087\b\u00a2\u0006\u0002\u0010\u000e\u001a;\u0010\u000f\u001a\u0002H\u000b\"\u0004\b\u0000\u0010\u0010\"\u0004\b\u0001\u0010\u000b2\u0006\u0010\u0011\u001a\u0002H\u00102\u0017\u0010\f\u001a\u0013\u0012\u0004\u0012\u0002H\u0010\u0012\u0004\u0012\u0002H\u000b0\t\u00a2\u0006\u0002\b\u0012H\u0087\b\u00a2\u0006\u0002\u0010\u0013\u001a,\u0010\u0014\u001a\u0002H\u0010\"\u0004\b\u0000\u0010\u0010*\u0002H\u00102\u0012\u0010\f\u001a\u000e\u0012\u0004\u0012\u0002H\u0010\u0012\u0004\u0012\u00020\u00050\tH\u0087\b\u00a2\u0006\u0002\u0010\u0013\u001a1\u0010\u0015\u001a\u0002H\u0010\"\u0004\b\u0000\u0010\u0010*\u0002H\u00102\u0017\u0010\f\u001a\u0013\u0012\u0004\u0012\u0002H\u0010\u0012\u0004\u0012\u00020\u00050\t\u00a2\u0006\u0002\b\u0012H\u0087\b\u00a2\u0006\u0002\u0010\u0013\u001a2\u0010\u0016\u001a\u0002H\u000b\"\u0004\b\u0000\u0010\u0010\"\u0004\b\u0001\u0010\u000b*\u0002H\u00102\u0012\u0010\f\u001a\u000e\u0012\u0004\u0012\u0002H\u0010\u0012\u0004\u0012\u0002H\u000b0\tH\u0087\b\u00a2\u0006\u0002\u0010\u0013\u001a7\u0010\n\u001a\u0002H\u000b\"\u0004\b\u0000\u0010\u0010\"\u0004\b\u0001\u0010\u000b*\u0002H\u00102\u0017\u0010\f\u001a\u0013\u0012\u0004\u0012\u0002H\u0010\u0012\u0004\u0012\u0002H\u000b0\t\u00a2\u0006\u0002\b\u0012H\u0087\b\u00a2\u0006\u0002\u0010\u0013\u001a.\u0010\u0017\u001a\u0004\u0018\u0001H\u0010\"\u0004\b\u0000\u0010\u0010*\u0002H\u00102\u0012\u0010\u0018\u001a\u000e\u0012\u0004\u0012\u0002H\u0010\u0012\u0004\u0012\u00020\u00190\tH\u0087\b\u00a2\u0006\u0002\u0010\u0013\u001a.\u0010\u001a\u001a\u0004\u0018\u0001H\u0010\"\u0004\b\u0000\u0010\u0010*\u0002H\u00102\u0012\u0010\u0018\u001a\u000e\u0012\u0004\u0012\u0002H\u0010\u0012\u0004\u0012\u00020\u00190\tH\u0087\b\u00a2\u0006\u0002\u0010\u0013\u00a8\u0006\u001b"}, d2 = {"TODO", "", "reason", "", "repeat", "", "times", "", "action", "Lkotlin/Function1;", "run", "R", "block", "Lkotlin/Function0;", "(Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;", "with", "T", "receiver", "Lkotlin/ExtensionFunctionType;", "(Ljava/lang/Object;Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;", "also", "apply", "let", "takeIf", "predicate", "", "takeUnless", "kotlin-stdlib"}, k = 5, mv = {1, 1, 6}, xi = 1, xs = "kotlin/StandardKt")
/* compiled from: Standard.kt */
class StandardKt__StandardKt {
    @kotlin.internal.InlineOnly
    private static final java.lang.Void TODO() {
        throw new kotlin.NotImplementedError(null, 1, null);
    }

    @kotlin.internal.InlineOnly
    private static final java.lang.Void TODO(java.lang.String reason) {
        throw new kotlin.NotImplementedError("An operation is not implemented: " + reason);
    }

    @kotlin.internal.InlineOnly
    private static final <R> R run(kotlin.jvm.functions.Function0<? extends R> block) {
        return block.invoke();
    }

    @kotlin.internal.InlineOnly
    private static final <T, R> R run(T $receiver, kotlin.jvm.functions.Function1<? super T, ? extends R> block) {
        return block.invoke($receiver);
    }

    @kotlin.internal.InlineOnly
    private static final <T, R> R with(T receiver, kotlin.jvm.functions.Function1<? super T, ? extends R> block) {
        return block.invoke(receiver);
    }

    @kotlin.internal.InlineOnly
    private static final <T> T apply(T $receiver, kotlin.jvm.functions.Function1<? super T, kotlin.Unit> block) {
        block.invoke($receiver);
        return $receiver;
    }

    @kotlin.SinceKotlin(version = "1.1")
    @kotlin.internal.InlineOnly
    private static final <T> T also(T $receiver, kotlin.jvm.functions.Function1<? super T, kotlin.Unit> block) {
        block.invoke($receiver);
        return $receiver;
    }

    @kotlin.internal.InlineOnly
    private static final <T, R> R let(T $receiver, kotlin.jvm.functions.Function1<? super T, ? extends R> block) {
        return block.invoke($receiver);
    }

    @kotlin.SinceKotlin(version = "1.1")
    @kotlin.internal.InlineOnly
    private static final <T> T takeIf(T $receiver, kotlin.jvm.functions.Function1<? super T, java.lang.Boolean> predicate) {
        if (((java.lang.Boolean) predicate.invoke($receiver)).booleanValue()) {
            return $receiver;
        }
        return null;
    }

    @kotlin.SinceKotlin(version = "1.1")
    @kotlin.internal.InlineOnly
    private static final <T> T takeUnless(T $receiver, kotlin.jvm.functions.Function1<? super T, java.lang.Boolean> predicate) {
        if (!((java.lang.Boolean) predicate.invoke($receiver)).booleanValue()) {
            return $receiver;
        }
        return null;
    }

    @kotlin.internal.InlineOnly
    private static final void repeat(int times, kotlin.jvm.functions.Function1<? super java.lang.Integer, kotlin.Unit> action) {
        int i = 0;
        int i2 = times - 1;
        if (0 <= i2) {
            while (true) {
                action.invoke(java.lang.Integer.valueOf(i));
                if (i != i2) {
                    i++;
                } else {
                    return;
                }
            }
        }
    }
}
