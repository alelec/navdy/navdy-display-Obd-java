package kotlin;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000&\n\u0000\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\u0010\u0003\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u0012\u0010\b\u001a\u00020\t*\u00020\u00032\u0006\u0010\n\u001a\u00020\u0003\u001a\r\u0010\u000b\u001a\u00020\t*\u00020\u0003H\u0087\b\u001a\u0015\u0010\u000b\u001a\u00020\t*\u00020\u00032\u0006\u0010\f\u001a\u00020\rH\u0087\b\u001a\u0015\u0010\u000b\u001a\u00020\t*\u00020\u00032\u0006\u0010\u000e\u001a\u00020\u000fH\u0087\b\"$\u0010\u0000\u001a\b\u0012\u0004\u0012\u00020\u00020\u0001*\u00020\u00038FX\u0087\u0004\u00a2\u0006\f\u0012\u0004\b\u0004\u0010\u0005\u001a\u0004\b\u0006\u0010\u0007\u00a8\u0006\u0010"}, d2 = {"stackTrace", "", "Ljava/lang/StackTraceElement;", "", "stackTrace$annotations", "(Ljava/lang/Throwable;)V", "getStackTrace", "(Ljava/lang/Throwable;)[Ljava/lang/StackTraceElement;", "addSuppressed", "", "exception", "printStackTrace", "stream", "Ljava/io/PrintStream;", "writer", "Ljava/io/PrintWriter;", "kotlin-stdlib"}, k = 5, mv = {1, 1, 6}, xi = 1, xs = "kotlin/ExceptionsKt")
/* compiled from: Exceptions.kt */
class ExceptionsKt__ExceptionsKt {
    public static /* synthetic */ void stackTrace$annotations(java.lang.Throwable th) {
    }

    @kotlin.internal.InlineOnly
    private static final void printStackTrace(@org.jetbrains.annotations.NotNull java.lang.Throwable $receiver) {
        if ($receiver == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type java.lang.Throwable");
        }
        $receiver.printStackTrace();
    }

    @kotlin.internal.InlineOnly
    private static final void printStackTrace(@org.jetbrains.annotations.NotNull java.lang.Throwable $receiver, java.io.PrintWriter writer) {
        if ($receiver == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type java.lang.Throwable");
        }
        $receiver.printStackTrace(writer);
    }

    @kotlin.internal.InlineOnly
    private static final void printStackTrace(@org.jetbrains.annotations.NotNull java.lang.Throwable $receiver, java.io.PrintStream stream) {
        if ($receiver == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type java.lang.Throwable");
        }
        $receiver.printStackTrace(stream);
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.StackTraceElement[] getStackTrace(@org.jetbrains.annotations.NotNull java.lang.Throwable $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        java.lang.StackTraceElement[] stackTrace = $receiver.getStackTrace();
        if (stackTrace == null) {
            kotlin.jvm.internal.Intrinsics.throwNpe();
        }
        return stackTrace;
    }

    public static final void addSuppressed(@org.jetbrains.annotations.NotNull java.lang.Throwable $receiver, @org.jetbrains.annotations.NotNull java.lang.Throwable exception) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(exception, "exception");
        kotlin.internal.PlatformImplementationsKt.IMPLEMENTATIONS.addSuppressed($receiver, exception);
    }
}
