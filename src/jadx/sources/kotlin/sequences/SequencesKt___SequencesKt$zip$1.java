package kotlin.sequences;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0001\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u00032\u0006\u0010\u0004\u001a\u0002H\u00022\u0006\u0010\u0005\u001a\u0002H\u0003H\n\u00a2\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"<anonymous>", "Lkotlin/Pair;", "T", "R", "t1", "t2", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;"}, k = 3, mv = {1, 1, 6})
/* compiled from: _Sequences.kt */
final class SequencesKt___SequencesKt$zip$1 extends kotlin.jvm.internal.Lambda implements kotlin.jvm.functions.Function2<T, R, kotlin.Pair<? extends T, ? extends R>> {
    public static final kotlin.sequences.SequencesKt___SequencesKt$zip$1 INSTANCE = new kotlin.sequences.SequencesKt___SequencesKt$zip$1();

    SequencesKt___SequencesKt$zip$1() {
        super(2);
    }

    @org.jetbrains.annotations.NotNull
    public final kotlin.Pair<T, R> invoke(T t1, R t2) {
        return kotlin.TuplesKt.to(t1, t2);
    }
}
