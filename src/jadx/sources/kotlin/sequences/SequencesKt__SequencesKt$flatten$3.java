package kotlin.sequences;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0004\n\u0002\b\u0006\u0010\u0000\u001a\u0002H\u0001\"\u0004\b\u0000\u0010\u0001\"\u0004\b\u0001\u0010\u00022\u0006\u0010\u0003\u001a\u0002H\u0001H\n\u00a2\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"<anonymous>", "T", "R", "it", "invoke", "(Ljava/lang/Object;)Ljava/lang/Object;"}, k = 3, mv = {1, 1, 6})
/* compiled from: Sequences.kt */
final class SequencesKt__SequencesKt$flatten$3 extends kotlin.jvm.internal.Lambda implements kotlin.jvm.functions.Function1<T, T> {
    public static final kotlin.sequences.SequencesKt__SequencesKt$flatten$3 INSTANCE = new kotlin.sequences.SequencesKt__SequencesKt$flatten$3();

    SequencesKt__SequencesKt$flatten$3() {
        super(1);
    }

    public final T invoke(T it) {
        return it;
    }
}
