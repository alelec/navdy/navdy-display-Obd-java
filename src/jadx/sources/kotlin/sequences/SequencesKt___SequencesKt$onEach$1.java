package kotlin.sequences;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0004\n\u0002\b\u0005\u0010\u0000\u001a\u0002H\u0001\"\u0004\b\u0000\u0010\u00012\u0006\u0010\u0002\u001a\u0002H\u0001H\n\u00a2\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"<anonymous>", "T", "it", "invoke", "(Ljava/lang/Object;)Ljava/lang/Object;"}, k = 3, mv = {1, 1, 6})
/* compiled from: _Sequences.kt */
final class SequencesKt___SequencesKt$onEach$1 extends kotlin.jvm.internal.Lambda implements kotlin.jvm.functions.Function1<T, T> {
    final /* synthetic */ kotlin.jvm.functions.Function1 $action;

    SequencesKt___SequencesKt$onEach$1(kotlin.jvm.functions.Function1 function1) {
        this.$action = function1;
        super(1);
    }

    public final T invoke(T it) {
        this.$action.invoke(it);
        return it;
    }
}
