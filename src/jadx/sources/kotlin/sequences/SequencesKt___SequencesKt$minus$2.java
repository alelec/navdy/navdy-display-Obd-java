package kotlin.sequences;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0013\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010(\n\u0000*\u0001\u0000\b\n\u0018\u00002\b\u0012\u0004\u0012\u00028\u00000\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u000f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00028\u00000\u0004H\u0096\u0002\u00a8\u0006\u0005"}, d2 = {"kotlin/sequences/SequencesKt___SequencesKt$minus$2", "Lkotlin/sequences/Sequence;", "(Lkotlin/sequences/Sequence;[Ljava/lang/Object;)V", "iterator", "", "kotlin-stdlib"}, k = 1, mv = {1, 1, 6})
/* compiled from: _Sequences.kt */
public final class SequencesKt___SequencesKt$minus$2 implements kotlin.sequences.Sequence<T> {
    final /* synthetic */ java.lang.Object[] $elements;
    final /* synthetic */ kotlin.sequences.Sequence receiver$0;

    SequencesKt___SequencesKt$minus$2(kotlin.sequences.Sequence<? extends T> $receiver, java.lang.Object[] $captured_local_variable$1) {
        this.receiver$0 = $receiver;
        this.$elements = $captured_local_variable$1;
    }

    @org.jetbrains.annotations.NotNull
    public java.util.Iterator<T> iterator() {
        return kotlin.sequences.SequencesKt.filterNot(this.receiver$0, new kotlin.sequences.SequencesKt___SequencesKt$minus$2$iterator$1(kotlin.collections.ArraysKt.toHashSet((T[]) this.$elements))).iterator();
    }
}
