package kotlin.sequences;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\u0010\u0000\u001a\u00020\u0001\"\u0006\b\u0000\u0010\u0002\u0018\u00012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004H\n\u00a2\u0006\u0002\b\u0005"}, d2 = {"<anonymous>", "", "R", "it", "", "invoke"}, k = 3, mv = {1, 1, 6})
/* compiled from: _Sequences.kt */
public final class SequencesKt___SequencesKt$filterIsInstance$1 extends kotlin.jvm.internal.Lambda implements kotlin.jvm.functions.Function1<java.lang.Object, java.lang.Boolean> {
    public static final kotlin.sequences.SequencesKt___SequencesKt$filterIsInstance$1 INSTANCE = new kotlin.sequences.SequencesKt___SequencesKt$filterIsInstance$1();

    public SequencesKt___SequencesKt$filterIsInstance$1() {
        super(1);
    }

    public final boolean invoke(@org.jetbrains.annotations.Nullable java.lang.Object it) {
        kotlin.jvm.internal.Intrinsics.reifiedOperationMarker(3, "R");
        return it instanceof java.lang.Object;
    }
}
