package kotlin.sequences;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u00022\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0004H\n\u00a2\u0006\u0002\b\u0005"}, d2 = {"<anonymous>", "", "T", "it", "Lkotlin/collections/IndexedValue;", "invoke"}, k = 3, mv = {1, 1, 6})
/* compiled from: _Sequences.kt */
final class SequencesKt___SequencesKt$filterIndexed$1 extends kotlin.jvm.internal.Lambda implements kotlin.jvm.functions.Function1<kotlin.collections.IndexedValue<? extends T>, java.lang.Boolean> {
    final /* synthetic */ kotlin.jvm.functions.Function2 $predicate;

    SequencesKt___SequencesKt$filterIndexed$1(kotlin.jvm.functions.Function2 function2) {
        this.$predicate = function2;
        super(1);
    }

    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        return java.lang.Boolean.valueOf(invoke((kotlin.collections.IndexedValue) obj));
    }

    public final boolean invoke(@org.jetbrains.annotations.NotNull kotlin.collections.IndexedValue<? extends T> it) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(it, "it");
        return ((java.lang.Boolean) this.$predicate.invoke(java.lang.Integer.valueOf(it.getIndex()), it.getValue())).booleanValue();
    }
}
