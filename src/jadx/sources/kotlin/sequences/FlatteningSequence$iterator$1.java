package kotlin.sequences;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0015\n\u0000\n\u0002\u0010(\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0004*\u0001\u0000\b\n\u0018\u00002\b\u0012\u0004\u0012\u00028\u00000\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\n\u001a\u00020\u000bH\u0002J\t\u0010\f\u001a\u00020\u000bH\u0096\u0002J\u000e\u0010\r\u001a\u00028\u0000H\u0096\u0002\u00a2\u0006\u0002\u0010\u000eR\"\u0010\u0003\u001a\n\u0012\u0004\u0012\u00028\u0000\u0018\u00010\u0001X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0004\u0010\u0005\"\u0004\b\u0006\u0010\u0007R\u0017\u0010\b\u001a\b\u0012\u0004\u0012\u00028\u00010\u0001\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\u0005\u00a8\u0006\u000f"}, d2 = {"kotlin/sequences/FlatteningSequence$iterator$1", "", "(Lkotlin/sequences/FlatteningSequence;)V", "itemIterator", "getItemIterator", "()Ljava/util/Iterator;", "setItemIterator", "(Ljava/util/Iterator;)V", "iterator", "getIterator", "ensureItemIterator", "", "hasNext", "next", "()Ljava/lang/Object;", "kotlin-stdlib"}, k = 1, mv = {1, 1, 6})
/* compiled from: Sequences.kt */
public final class FlatteningSequence$iterator$1 implements java.util.Iterator<E>, kotlin.jvm.internal.markers.KMappedMarker {
    @org.jetbrains.annotations.Nullable
    private java.util.Iterator<? extends E> itemIterator;
    @org.jetbrains.annotations.NotNull
    private final java.util.Iterator<T> iterator;
    final /* synthetic */ kotlin.sequences.FlatteningSequence this$0;

    public void remove() {
        throw new java.lang.UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    FlatteningSequence$iterator$1(kotlin.sequences.FlatteningSequence $outer) {
        this.this$0 = $outer;
        this.iterator = $outer.sequence.iterator();
    }

    @org.jetbrains.annotations.NotNull
    public final java.util.Iterator<T> getIterator() {
        return this.iterator;
    }

    @org.jetbrains.annotations.Nullable
    public final java.util.Iterator<E> getItemIterator() {
        return this.itemIterator;
    }

    public final void setItemIterator(@org.jetbrains.annotations.Nullable java.util.Iterator<? extends E> it) {
        this.itemIterator = it;
    }

    public E next() {
        if (!ensureItemIterator()) {
            throw new java.util.NoSuchElementException();
        }
        java.util.Iterator<? extends E> it = this.itemIterator;
        if (it == null) {
            kotlin.jvm.internal.Intrinsics.throwNpe();
        }
        return it.next();
    }

    public boolean hasNext() {
        return ensureItemIterator();
    }

    private final boolean ensureItemIterator() {
        java.util.Iterator<? extends E> it = this.itemIterator;
        if (kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) it != null ? java.lang.Boolean.valueOf(it.hasNext()) : null, (java.lang.Object) java.lang.Boolean.valueOf(false))) {
            this.itemIterator = null;
        }
        while (this.itemIterator == null) {
            if (!this.iterator.hasNext()) {
                return false;
            }
            java.util.Iterator nextItemIterator = (java.util.Iterator) this.this$0.iterator.invoke(this.this$0.transformer.invoke(this.iterator.next()));
            if (nextItemIterator.hasNext()) {
                this.itemIterator = nextItemIterator;
                return true;
            }
        }
        return true;
    }
}
