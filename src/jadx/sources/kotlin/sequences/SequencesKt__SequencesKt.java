package kotlin.sequences;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000H\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010(\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0011\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u001c\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0000\u001a+\u0010\u0000\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\b\u0000\u0010\u00022\u0014\b\u0004\u0010\u0003\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00020\u00050\u0004H\u0087\b\u001a\u0012\u0010\u0006\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\b\u0000\u0010\u0002\u001a&\u0010\u0007\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\b\b\u0000\u0010\u0002*\u00020\b2\u000e\u0010\t\u001a\n\u0012\u0006\u0012\u0004\u0018\u0001H\u00020\u0004\u001a<\u0010\u0007\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\b\b\u0000\u0010\u0002*\u00020\b2\u000e\u0010\n\u001a\n\u0012\u0006\u0012\u0004\u0018\u0001H\u00020\u00042\u0014\u0010\t\u001a\u0010\u0012\u0004\u0012\u0002H\u0002\u0012\u0006\u0012\u0004\u0018\u0001H\u00020\u000b\u001a=\u0010\u0007\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\b\b\u0000\u0010\u0002*\u00020\b2\b\u0010\f\u001a\u0004\u0018\u0001H\u00022\u0014\u0010\t\u001a\u0010\u0012\u0004\u0012\u0002H\u0002\u0012\u0006\u0012\u0004\u0018\u0001H\u00020\u000bH\u0007\u00a2\u0006\u0002\u0010\r\u001a+\u0010\u000e\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\b\u0000\u0010\u00022\u0012\u0010\u000f\u001a\n\u0012\u0006\b\u0001\u0012\u0002H\u00020\u0010\"\u0002H\u0002\u00a2\u0006\u0002\u0010\u0011\u001a\u001f\u0010\u0012\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u0013H\u0087\b\u001a\u001c\u0010\u0012\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u0005\u001a\u001c\u0010\u0014\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u0001\u001aC\u0010\u0015\u001a\b\u0012\u0004\u0012\u0002H\u00160\u0001\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0016*\b\u0012\u0004\u0012\u0002H\u00020\u00012\u0018\u0010\u0003\u001a\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00160\u00050\u000bH\u0002\u00a2\u0006\u0002\b\u0017\u001a)\u0010\u0015\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00020\u00180\u0001H\u0007\u00a2\u0006\u0002\b\u0019\u001a\"\u0010\u0015\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00020\u00010\u0001\u001a@\u0010\u001a\u001a\u001a\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00020\u001c\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00160\u001c0\u001b\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0016*\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00160\u001b0\u0001\u00a8\u0006\u001d"}, d2 = {"Sequence", "Lkotlin/sequences/Sequence;", "T", "iterator", "Lkotlin/Function0;", "", "emptySequence", "generateSequence", "", "nextFunction", "seedFunction", "Lkotlin/Function1;", "seed", "(Ljava/lang/Object;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;", "sequenceOf", "elements", "", "([Ljava/lang/Object;)Lkotlin/sequences/Sequence;", "asSequence", "Ljava/util/Enumeration;", "constrainOnce", "flatten", "R", "flatten$SequencesKt__SequencesKt", "", "flattenSequenceOfIterable", "unzip", "Lkotlin/Pair;", "", "kotlin-stdlib"}, k = 5, mv = {1, 1, 6}, xi = 1, xs = "kotlin/sequences/SequencesKt")
/* compiled from: Sequences.kt */
class SequencesKt__SequencesKt {
    @kotlin.internal.InlineOnly
    private static final <T> kotlin.sequences.Sequence<T> Sequence(kotlin.jvm.functions.Function0<? extends java.util.Iterator<? extends T>> iterator) {
        return new kotlin.sequences.SequencesKt__SequencesKt$Sequence$1<>(iterator);
    }

    @org.jetbrains.annotations.NotNull
    public static final <T> kotlin.sequences.Sequence<T> asSequence(@org.jetbrains.annotations.NotNull java.util.Iterator<? extends T> $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        return kotlin.sequences.SequencesKt.constrainOnce(new kotlin.sequences.SequencesKt__SequencesKt$asSequence$$inlined$Sequence$1($receiver));
    }

    @kotlin.internal.InlineOnly
    private static final <T> kotlin.sequences.Sequence<T> asSequence(@org.jetbrains.annotations.NotNull java.util.Enumeration<T> $receiver) {
        return kotlin.sequences.SequencesKt.asSequence(kotlin.collections.CollectionsKt.iterator($receiver));
    }

    @org.jetbrains.annotations.NotNull
    public static final <T> kotlin.sequences.Sequence<T> sequenceOf(@org.jetbrains.annotations.NotNull T... elements) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(elements, "elements");
        return elements.length == 0 ? kotlin.sequences.SequencesKt.emptySequence() : kotlin.collections.ArraysKt.asSequence(elements);
    }

    @org.jetbrains.annotations.NotNull
    public static final <T> kotlin.sequences.Sequence<T> emptySequence() {
        return kotlin.sequences.EmptySequence.INSTANCE;
    }

    @org.jetbrains.annotations.NotNull
    public static final <T> kotlin.sequences.Sequence<T> flatten(@org.jetbrains.annotations.NotNull kotlin.sequences.Sequence<? extends kotlin.sequences.Sequence<? extends T>> $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        return flatten$SequencesKt__SequencesKt($receiver, kotlin.sequences.SequencesKt__SequencesKt$flatten$1.INSTANCE);
    }

    @org.jetbrains.annotations.NotNull
    @kotlin.jvm.JvmName(name = "flattenSequenceOfIterable")
    public static final <T> kotlin.sequences.Sequence<T> flattenSequenceOfIterable(@org.jetbrains.annotations.NotNull kotlin.sequences.Sequence<? extends java.lang.Iterable<? extends T>> $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        return flatten$SequencesKt__SequencesKt($receiver, kotlin.sequences.SequencesKt__SequencesKt$flatten$2.INSTANCE);
    }

    private static final <T, R> kotlin.sequences.Sequence<R> flatten$SequencesKt__SequencesKt(@org.jetbrains.annotations.NotNull kotlin.sequences.Sequence<? extends T> $receiver, kotlin.jvm.functions.Function1<? super T, ? extends java.util.Iterator<? extends R>> iterator) {
        if ($receiver instanceof kotlin.sequences.TransformingSequence) {
            return ((kotlin.sequences.TransformingSequence) $receiver).flatten$kotlin_stdlib(iterator);
        }
        return new kotlin.sequences.FlatteningSequence<>($receiver, kotlin.sequences.SequencesKt__SequencesKt$flatten$3.INSTANCE, iterator);
    }

    @org.jetbrains.annotations.NotNull
    public static final <T, R> kotlin.Pair<java.util.List<T>, java.util.List<R>> unzip(@org.jetbrains.annotations.NotNull kotlin.sequences.Sequence<? extends kotlin.Pair<? extends T, ? extends R>> $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        java.util.ArrayList listT = new java.util.ArrayList();
        java.util.ArrayList listR = new java.util.ArrayList();
        for (kotlin.Pair pair : $receiver) {
            listT.add(pair.getFirst());
            listR.add(pair.getSecond());
        }
        return kotlin.TuplesKt.to(listT, listR);
    }

    @org.jetbrains.annotations.NotNull
    public static final <T> kotlin.sequences.Sequence<T> constrainOnce(@org.jetbrains.annotations.NotNull kotlin.sequences.Sequence<? extends T> $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        return $receiver instanceof kotlin.sequences.ConstrainedOnceSequence ? $receiver : new kotlin.sequences.ConstrainedOnceSequence($receiver);
    }

    @org.jetbrains.annotations.NotNull
    public static final <T> kotlin.sequences.Sequence<T> generateSequence(@org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function0<? extends T> nextFunction) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(nextFunction, "nextFunction");
        return kotlin.sequences.SequencesKt.constrainOnce(new kotlin.sequences.GeneratorSequence(nextFunction, new kotlin.sequences.SequencesKt__SequencesKt$generateSequence$1(nextFunction)));
    }

    @org.jetbrains.annotations.NotNull
    @kotlin.internal.LowPriorityInOverloadResolution
    public static final <T> kotlin.sequences.Sequence<T> generateSequence(@org.jetbrains.annotations.Nullable T seed, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super T, ? extends T> nextFunction) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(nextFunction, "nextFunction");
        if (seed == null) {
            return kotlin.sequences.EmptySequence.INSTANCE;
        }
        return new kotlin.sequences.GeneratorSequence<>(new kotlin.sequences.SequencesKt__SequencesKt$generateSequence$2(seed), nextFunction);
    }

    @org.jetbrains.annotations.NotNull
    public static final <T> kotlin.sequences.Sequence<T> generateSequence(@org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function0<? extends T> seedFunction, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super T, ? extends T> nextFunction) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(seedFunction, "seedFunction");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(nextFunction, "nextFunction");
        return new kotlin.sequences.GeneratorSequence<>(seedFunction, nextFunction);
    }
}
