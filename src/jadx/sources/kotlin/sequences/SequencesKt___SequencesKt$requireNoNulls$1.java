package kotlin.sequences;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\f\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\u0010\u0000\u001a\u0002H\u0001\"\b\b\u0000\u0010\u0001*\u00020\u00022\b\u0010\u0003\u001a\u0004\u0018\u0001H\u0001H\n\u00a2\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"<anonymous>", "T", "", "it", "invoke", "(Ljava/lang/Object;)Ljava/lang/Object;"}, k = 3, mv = {1, 1, 6})
/* compiled from: _Sequences.kt */
final class SequencesKt___SequencesKt$requireNoNulls$1 extends kotlin.jvm.internal.Lambda implements kotlin.jvm.functions.Function1<T, T> {
    final /* synthetic */ kotlin.sequences.Sequence receiver$0;

    SequencesKt___SequencesKt$requireNoNulls$1(kotlin.sequences.Sequence sequence) {
        this.receiver$0 = sequence;
        super(1);
    }

    @org.jetbrains.annotations.NotNull
    public final T invoke(@org.jetbrains.annotations.Nullable T it) {
        if (it != null) {
            return it;
        }
        throw new java.lang.IllegalArgumentException("null element found in " + this.receiver$0 + ".");
    }
}
