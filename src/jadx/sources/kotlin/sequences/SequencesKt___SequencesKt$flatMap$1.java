package kotlin.sequences;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010(\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\b\u0000\u0010\u0003\"\u0004\b\u0001\u0010\u00022\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0005H\n\u00a2\u0006\u0002\b\u0006"}, d2 = {"<anonymous>", "", "R", "T", "it", "Lkotlin/sequences/Sequence;", "invoke"}, k = 3, mv = {1, 1, 6})
/* compiled from: _Sequences.kt */
final class SequencesKt___SequencesKt$flatMap$1 extends kotlin.jvm.internal.Lambda implements kotlin.jvm.functions.Function1<kotlin.sequences.Sequence<? extends R>, java.util.Iterator<? extends R>> {
    public static final kotlin.sequences.SequencesKt___SequencesKt$flatMap$1 INSTANCE = new kotlin.sequences.SequencesKt___SequencesKt$flatMap$1();

    SequencesKt___SequencesKt$flatMap$1() {
        super(1);
    }

    @org.jetbrains.annotations.NotNull
    public final java.util.Iterator<R> invoke(@org.jetbrains.annotations.NotNull kotlin.sequences.Sequence<? extends R> it) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(it, "it");
        return it.iterator();
    }
}
