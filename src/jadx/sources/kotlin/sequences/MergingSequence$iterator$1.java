package kotlin.sequences;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0015\n\u0000\n\u0002\u0010(\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0002\b\u0003*\u0001\u0000\b\n\u0018\u00002\b\u0012\u0004\u0012\u00028\u00000\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\t\u0010\b\u001a\u00020\tH\u0096\u0002J\u000e\u0010\n\u001a\u00028\u0000H\u0096\u0002\u00a2\u0006\u0002\u0010\u000bR\u0017\u0010\u0003\u001a\b\u0012\u0004\u0012\u00028\u00010\u0001\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0004\u0010\u0005R\u0017\u0010\u0006\u001a\b\u0012\u0004\u0012\u00028\u00010\u0001\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\u0005\u00a8\u0006\f"}, d2 = {"kotlin/sequences/MergingSequence$iterator$1", "", "(Lkotlin/sequences/MergingSequence;)V", "iterator1", "getIterator1", "()Ljava/util/Iterator;", "iterator2", "getIterator2", "hasNext", "", "next", "()Ljava/lang/Object;", "kotlin-stdlib"}, k = 1, mv = {1, 1, 6})
/* compiled from: Sequences.kt */
public final class MergingSequence$iterator$1 implements java.util.Iterator<V>, kotlin.jvm.internal.markers.KMappedMarker {
    @org.jetbrains.annotations.NotNull
    private final java.util.Iterator<T1> iterator1;
    @org.jetbrains.annotations.NotNull
    private final java.util.Iterator<T2> iterator2;
    final /* synthetic */ kotlin.sequences.MergingSequence this$0;

    public void remove() {
        throw new java.lang.UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    MergingSequence$iterator$1(kotlin.sequences.MergingSequence $outer) {
        this.this$0 = $outer;
        this.iterator1 = $outer.sequence1.iterator();
        this.iterator2 = $outer.sequence2.iterator();
    }

    @org.jetbrains.annotations.NotNull
    public final java.util.Iterator<T1> getIterator1() {
        return this.iterator1;
    }

    @org.jetbrains.annotations.NotNull
    public final java.util.Iterator<T2> getIterator2() {
        return this.iterator2;
    }

    public V next() {
        return this.this$0.transform.invoke(this.iterator1.next(), this.iterator2.next());
    }

    public boolean hasNext() {
        return this.iterator1.hasNext() && this.iterator2.hasNext();
    }
}
