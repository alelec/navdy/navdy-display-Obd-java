package kotlin.sequences;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010(\n\u0002\b\u0002\b\u0000\u0018\u0000*\u0004\b\u0000\u0010\u0001*\u0004\b\u0001\u0010\u0002*\u0004\b\u0002\u0010\u00032\b\u0012\u0004\u0012\u0002H\u00030\u0004BA\u0012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00028\u00000\u0004\u0012\u0012\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0007\u0012\u0018\u0010\b\u001a\u0014\u0012\u0004\u0012\u00028\u0001\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00020\t0\u0007\u00a2\u0006\u0002\u0010\nJ\u000f\u0010\b\u001a\b\u0012\u0004\u0012\u00028\u00020\tH\u0096\u0002R \u0010\b\u001a\u0014\u0012\u0004\u0012\u00028\u0001\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00020\t0\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\b\u0012\u0004\u0012\u00028\u00000\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"}, d2 = {"Lkotlin/sequences/FlatteningSequence;", "T", "R", "E", "Lkotlin/sequences/Sequence;", "sequence", "transformer", "Lkotlin/Function1;", "iterator", "", "(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V", "kotlin-stdlib"}, k = 1, mv = {1, 1, 6})
/* compiled from: Sequences.kt */
public final class FlatteningSequence<T, R, E> implements kotlin.sequences.Sequence<E> {
    /* access modifiers changed from: private */
    public final kotlin.jvm.functions.Function1<R, java.util.Iterator<E>> iterator;
    /* access modifiers changed from: private */
    public final kotlin.sequences.Sequence<T> sequence;
    /* access modifiers changed from: private */
    public final kotlin.jvm.functions.Function1<T, R> transformer;

    public FlatteningSequence(@org.jetbrains.annotations.NotNull kotlin.sequences.Sequence<? extends T> sequence2, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super T, ? extends R> transformer2, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super R, ? extends java.util.Iterator<? extends E>> iterator2) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(sequence2, "sequence");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(transformer2, "transformer");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(iterator2, "iterator");
        this.sequence = sequence2;
        this.transformer = transformer2;
        this.iterator = iterator2;
    }

    @org.jetbrains.annotations.NotNull
    public java.util.Iterator<E> iterator() {
        return new kotlin.sequences.FlatteningSequence$iterator$1<>(this);
    }
}
