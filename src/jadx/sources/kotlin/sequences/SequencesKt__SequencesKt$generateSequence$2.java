package kotlin.sequences;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\f\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\u0010\u0000\u001a\u0004\u0018\u0001H\u0001\"\b\b\u0000\u0010\u0001*\u00020\u0002H\n\u00a2\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"<anonymous>", "T", "", "invoke", "()Ljava/lang/Object;"}, k = 3, mv = {1, 1, 6})
/* compiled from: Sequences.kt */
final class SequencesKt__SequencesKt$generateSequence$2 extends kotlin.jvm.internal.Lambda implements kotlin.jvm.functions.Function0<T> {
    final /* synthetic */ java.lang.Object $seed;

    SequencesKt__SequencesKt$generateSequence$2(java.lang.Object obj) {
        this.$seed = obj;
        super(0);
    }

    @org.jetbrains.annotations.Nullable
    public final T invoke() {
        return this.$seed;
    }
}
