package kotlin.sequences;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\n\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\u0010\u0000\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u00022\u0006\u0010\u0003\u001a\u0002H\u0002H\n\u00a2\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"<anonymous>", "", "T", "it", "invoke", "(Ljava/lang/Object;)Z"}, k = 3, mv = {1, 1, 6})
/* compiled from: _Sequences.kt */
final class SequencesKt___SequencesKt$minus$2$iterator$1 extends kotlin.jvm.internal.Lambda implements kotlin.jvm.functions.Function1<T, java.lang.Boolean> {
    final /* synthetic */ java.util.HashSet $other;

    SequencesKt___SequencesKt$minus$2$iterator$1(java.util.HashSet hashSet) {
        this.$other = hashSet;
        super(1);
    }

    public final boolean invoke(T it) {
        return this.$other.contains(it);
    }
}
