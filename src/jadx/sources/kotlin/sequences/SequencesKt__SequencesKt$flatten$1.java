package kotlin.sequences;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010(\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\b\u0000\u0010\u00022\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0004H\n\u00a2\u0006\u0002\b\u0005"}, d2 = {"<anonymous>", "", "T", "it", "Lkotlin/sequences/Sequence;", "invoke"}, k = 3, mv = {1, 1, 6})
/* compiled from: Sequences.kt */
final class SequencesKt__SequencesKt$flatten$1 extends kotlin.jvm.internal.Lambda implements kotlin.jvm.functions.Function1<kotlin.sequences.Sequence<? extends T>, java.util.Iterator<? extends T>> {
    public static final kotlin.sequences.SequencesKt__SequencesKt$flatten$1 INSTANCE = new kotlin.sequences.SequencesKt__SequencesKt$flatten$1();

    SequencesKt__SequencesKt$flatten$1() {
        super(1);
    }

    @org.jetbrains.annotations.NotNull
    public final java.util.Iterator<T> invoke(@org.jetbrains.annotations.NotNull kotlin.sequences.Sequence<? extends T> it) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(it, "it");
        return it.iterator();
    }
}
