package kotlin.sequences;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u001d\n\u0000\n\u0002\u0010(\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0003*\u0001\u0000\b\n\u0018\u00002\b\u0012\u0004\u0012\u00028\u00000\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\t\u0010\f\u001a\u00020\rH\u0096\u0002J\u000e\u0010\u000e\u001a\u00028\u0000H\u0096\u0002\u00a2\u0006\u0002\u0010\u000fR\u0017\u0010\u0003\u001a\b\u0012\u0004\u0012\u00028\u00000\u0001\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0004\u0010\u0005R\u001a\u0010\u0006\u001a\u00020\u0007X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\b\u0010\t\"\u0004\b\n\u0010\u000b\u00a8\u0006\u0010"}, d2 = {"kotlin/sequences/TakeSequence$iterator$1", "", "(Lkotlin/sequences/TakeSequence;)V", "iterator", "getIterator", "()Ljava/util/Iterator;", "left", "", "getLeft", "()I", "setLeft", "(I)V", "hasNext", "", "next", "()Ljava/lang/Object;", "kotlin-stdlib"}, k = 1, mv = {1, 1, 6})
/* compiled from: Sequences.kt */
public final class TakeSequence$iterator$1 implements java.util.Iterator<T>, kotlin.jvm.internal.markers.KMappedMarker {
    @org.jetbrains.annotations.NotNull
    private final java.util.Iterator<T> iterator;
    private int left;
    final /* synthetic */ kotlin.sequences.TakeSequence this$0;

    public void remove() {
        throw new java.lang.UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    TakeSequence$iterator$1(kotlin.sequences.TakeSequence $outer) {
        this.this$0 = $outer;
        this.left = $outer.count;
        this.iterator = $outer.sequence.iterator();
    }

    public final int getLeft() {
        return this.left;
    }

    public final void setLeft(int i) {
        this.left = i;
    }

    @org.jetbrains.annotations.NotNull
    public final java.util.Iterator<T> getIterator() {
        return this.iterator;
    }

    public T next() {
        if (this.left == 0) {
            throw new java.util.NoSuchElementException();
        }
        this.left--;
        return this.iterator.next();
    }

    public boolean hasNext() {
        return this.left > 0 && this.iterator.hasNext();
    }
}
