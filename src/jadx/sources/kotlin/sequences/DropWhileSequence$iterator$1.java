package kotlin.sequences;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000#\n\u0000\n\u0002\u0010(\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u000e\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002*\u0001\u0000\b\n\u0018\u00002\b\u0012\u0004\u0012\u00028\u00000\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0012\u001a\u00020\u0013H\u0002J\t\u0010\u0014\u001a\u00020\u0015H\u0096\u0002J\u000e\u0010\u0016\u001a\u00028\u0000H\u0096\u0002\u00a2\u0006\u0002\u0010\u000eR\u001a\u0010\u0003\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u0017\u0010\t\u001a\b\u0012\u0004\u0012\u00028\u00000\u0001\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u001e\u0010\f\u001a\u0004\u0018\u00018\u0000X\u0086\u000e\u00a2\u0006\u0010\n\u0002\u0010\u0011\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010\u00a8\u0006\u0017"}, d2 = {"kotlin/sequences/DropWhileSequence$iterator$1", "", "(Lkotlin/sequences/DropWhileSequence;)V", "dropState", "", "getDropState", "()I", "setDropState", "(I)V", "iterator", "getIterator", "()Ljava/util/Iterator;", "nextItem", "getNextItem", "()Ljava/lang/Object;", "setNextItem", "(Ljava/lang/Object;)V", "Ljava/lang/Object;", "drop", "", "hasNext", "", "next", "kotlin-stdlib"}, k = 1, mv = {1, 1, 6})
/* compiled from: Sequences.kt */
public final class DropWhileSequence$iterator$1 implements java.util.Iterator<T>, kotlin.jvm.internal.markers.KMappedMarker {
    private int dropState = -1;
    @org.jetbrains.annotations.NotNull
    private final java.util.Iterator<T> iterator;
    @org.jetbrains.annotations.Nullable
    private T nextItem;
    final /* synthetic */ kotlin.sequences.DropWhileSequence this$0;

    public void remove() {
        throw new java.lang.UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    DropWhileSequence$iterator$1(kotlin.sequences.DropWhileSequence $outer) {
        this.this$0 = $outer;
        this.iterator = $outer.sequence.iterator();
    }

    @org.jetbrains.annotations.NotNull
    public final java.util.Iterator<T> getIterator() {
        return this.iterator;
    }

    public final int getDropState() {
        return this.dropState;
    }

    public final void setDropState(int i) {
        this.dropState = i;
    }

    @org.jetbrains.annotations.Nullable
    public final T getNextItem() {
        return this.nextItem;
    }

    public final void setNextItem(@org.jetbrains.annotations.Nullable T t) {
        this.nextItem = t;
    }

    private final void drop() {
        while (this.iterator.hasNext()) {
            java.lang.Object item = this.iterator.next();
            if (!((java.lang.Boolean) this.this$0.predicate.invoke(item)).booleanValue()) {
                this.nextItem = item;
                this.dropState = 1;
                return;
            }
        }
        this.dropState = 0;
    }

    public T next() {
        if (this.dropState == -1) {
            drop();
        }
        if (this.dropState != 1) {
            return this.iterator.next();
        }
        java.lang.Object result = this.nextItem;
        this.nextItem = null;
        this.dropState = 0;
        return result;
    }

    public boolean hasNext() {
        if (this.dropState == -1) {
            drop();
        }
        if (this.dropState == 1 || this.iterator.hasNext()) {
            return true;
        }
        return false;
    }
}
