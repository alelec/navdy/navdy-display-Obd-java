package kotlin;

@kotlin.SinceKotlin(version = "1.1")
@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u000f\n\u0000\n\u0002\u0010\b\n\u0002\b\f\n\u0002\u0010\u000b\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0003\b\u0007\u0018\u0000 \u00172\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u0017B\u0017\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0005B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0007J\u0011\u0010\r\u001a\u00020\u00032\u0006\u0010\u000e\u001a\u00020\u0000H\u0096\u0002J\u0013\u0010\u000f\u001a\u00020\u00102\b\u0010\u000e\u001a\u0004\u0018\u00010\u0011H\u0096\u0002J\b\u0010\u0012\u001a\u00020\u0003H\u0016J\u0016\u0010\u0013\u001a\u00020\u00102\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0003J\u001e\u0010\u0013\u001a\u00020\u00102\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0003J\b\u0010\u0014\u001a\u00020\u0015H\u0016J \u0010\u0016\u001a\u00020\u00032\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0003H\u0002R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\tR\u0011\u0010\u0006\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\tR\u000e\u0010\f\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0018"}, d2 = {"Lkotlin/KotlinVersion;", "", "major", "", "minor", "(II)V", "patch", "(III)V", "getMajor", "()I", "getMinor", "getPatch", "version", "compareTo", "other", "equals", "", "", "hashCode", "isAtLeast", "toString", "", "versionOf", "Companion", "kotlin-stdlib"}, k = 1, mv = {1, 1, 6})
/* compiled from: KotlinVersion.kt */
public final class KotlinVersion implements java.lang.Comparable<kotlin.KotlinVersion> {
    @org.jetbrains.annotations.NotNull
    @kotlin.jvm.JvmField
    public static final kotlin.KotlinVersion CURRENT = new kotlin.KotlinVersion(1, 1, 2);
    public static final kotlin.KotlinVersion.Companion Companion = new kotlin.KotlinVersion.Companion(null);
    public static final int MAX_COMPONENT_VALUE = 255;
    private final int major;
    private final int minor;
    private final int patch;
    private final int version;

    @kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u0010\u0010\u0003\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0007"}, d2 = {"Lkotlin/KotlinVersion$Companion;", "", "()V", "CURRENT", "Lkotlin/KotlinVersion;", "MAX_COMPONENT_VALUE", "", "kotlin-stdlib"}, k = 1, mv = {1, 1, 6})
    /* compiled from: KotlinVersion.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(kotlin.jvm.internal.DefaultConstructorMarker $constructor_marker) {
            this();
        }
    }

    public KotlinVersion(int major2, int minor2, int patch2) {
        this.major = major2;
        this.minor = minor2;
        this.patch = patch2;
        this.version = versionOf(this.major, this.minor, this.patch);
    }

    public final int getMajor() {
        return this.major;
    }

    public final int getMinor() {
        return this.minor;
    }

    public final int getPatch() {
        return this.patch;
    }

    public KotlinVersion(int major2, int minor2) {
        this(major2, minor2, 0);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0017, code lost:
        if ((r7 >= 0 && r7 <= 255) != false) goto L_0x0019;
     */
    private final int versionOf(int major2, int minor2, int patch2) {
        boolean z = true;
        if (major2 >= 0 && major2 <= 255) {
            if (minor2 >= 0 && minor2 <= 255) {
            }
        }
        z = false;
        if (z) {
            return (major2 << (minor2 + 16)) << (patch2 + 8);
        }
        throw new java.lang.IllegalArgumentException(("Version components are out of range: " + major2 + "." + minor2 + "." + patch2).toString());
    }

    @org.jetbrains.annotations.NotNull
    public java.lang.String toString() {
        return this.major + "." + this.minor + "." + this.patch;
    }

    public boolean equals(@org.jetbrains.annotations.Nullable java.lang.Object other) {
        java.lang.Object obj;
        boolean z;
        if (this == other) {
            return true;
        }
        if (!(other instanceof kotlin.KotlinVersion)) {
            obj = null;
        } else {
            obj = other;
        }
        kotlin.KotlinVersion otherVersion = (kotlin.KotlinVersion) obj;
        if (otherVersion == null) {
            return false;
        }
        if (this.version == otherVersion.version) {
            z = true;
        } else {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        return this.version;
    }

    public int compareTo(@org.jetbrains.annotations.NotNull kotlin.KotlinVersion other) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(other, "other");
        return this.version - other.version;
    }

    public final boolean isAtLeast(int major2, int minor2) {
        return this.major > major2 || (this.major == major2 && this.minor >= minor2);
    }

    public final boolean isAtLeast(int major2, int minor2, int patch2) {
        return this.major > major2 || (this.major == major2 && (this.minor > minor2 || (this.minor == minor2 && this.patch >= patch2)));
    }
}
