package kotlin;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\u001a\u0011\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\u0087\b\u001a\u001f\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005H\u0087\b\u00a8\u0006\u0007"}, d2 = {"assert", "", "value", "", "lazyMessage", "Lkotlin/Function0;", "", "kotlin-stdlib"}, k = 5, mv = {1, 1, 6}, xi = 1, xs = "kotlin/PreconditionsKt")
/* compiled from: AssertionsJVM.kt */
class PreconditionsKt__AssertionsJVMKt {
    @kotlin.internal.InlineOnly
    /* renamed from: assert reason: not valid java name */
    private static final void m0assert(boolean value) {
        if (kotlin._Assertions.ENABLED && !value) {
            throw new java.lang.AssertionError("Assertion failed");
        }
    }

    @kotlin.internal.InlineOnly
    /* renamed from: assert reason: not valid java name */
    private static final void m1assert(boolean value, kotlin.jvm.functions.Function0<? extends java.lang.Object> lazyMessage) {
        if (kotlin._Assertions.ENABLED && !value) {
            throw new java.lang.AssertionError(lazyMessage.invoke());
        }
    }
}
