package kotlin.io;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000<\n\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0002\b\f\u001a(\u0010\t\u001a\u00020\u00022\b\b\u0002\u0010\n\u001a\u00020\u00012\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u00012\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u0002\u001a(\u0010\r\u001a\u00020\u00022\b\b\u0002\u0010\n\u001a\u00020\u00012\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u00012\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u0002\u001a8\u0010\u000e\u001a\u00020\u000f*\u00020\u00022\u0006\u0010\u0010\u001a\u00020\u00022\b\b\u0002\u0010\u0011\u001a\u00020\u000f2\u001a\b\u0002\u0010\u0012\u001a\u0014\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0014\u0012\u0004\u0012\u00020\u00150\u0013\u001a&\u0010\u0016\u001a\u00020\u0002*\u00020\u00022\u0006\u0010\u0010\u001a\u00020\u00022\b\b\u0002\u0010\u0011\u001a\u00020\u000f2\b\b\u0002\u0010\u0017\u001a\u00020\u0018\u001a\n\u0010\u0019\u001a\u00020\u000f*\u00020\u0002\u001a\u0012\u0010\u001a\u001a\u00020\u000f*\u00020\u00022\u0006\u0010\u001b\u001a\u00020\u0002\u001a\u0012\u0010\u001a\u001a\u00020\u000f*\u00020\u00022\u0006\u0010\u001b\u001a\u00020\u0001\u001a\n\u0010\u001c\u001a\u00020\u0002*\u00020\u0002\u001a\u001d\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u00020\u001d*\b\u0012\u0004\u0012\u00020\u00020\u001dH\u0002\u00a2\u0006\u0002\b\u001e\u001a\u0011\u0010\u001c\u001a\u00020\u001f*\u00020\u001fH\u0002\u00a2\u0006\u0002\b\u001e\u001a\u0012\u0010 \u001a\u00020\u0002*\u00020\u00022\u0006\u0010!\u001a\u00020\u0002\u001a\u0014\u0010\"\u001a\u0004\u0018\u00010\u0002*\u00020\u00022\u0006\u0010!\u001a\u00020\u0002\u001a\u0012\u0010#\u001a\u00020\u0002*\u00020\u00022\u0006\u0010!\u001a\u00020\u0002\u001a\u0012\u0010$\u001a\u00020\u0002*\u00020\u00022\u0006\u0010%\u001a\u00020\u0002\u001a\u0012\u0010$\u001a\u00020\u0002*\u00020\u00022\u0006\u0010%\u001a\u00020\u0001\u001a\u0012\u0010&\u001a\u00020\u0002*\u00020\u00022\u0006\u0010%\u001a\u00020\u0002\u001a\u0012\u0010&\u001a\u00020\u0002*\u00020\u00022\u0006\u0010%\u001a\u00020\u0001\u001a\u0012\u0010'\u001a\u00020\u000f*\u00020\u00022\u0006\u0010\u001b\u001a\u00020\u0002\u001a\u0012\u0010'\u001a\u00020\u000f*\u00020\u00022\u0006\u0010\u001b\u001a\u00020\u0001\u001a\u0012\u0010(\u001a\u00020\u0001*\u00020\u00022\u0006\u0010!\u001a\u00020\u0002\u001a\u001b\u0010)\u001a\u0004\u0018\u00010\u0001*\u00020\u00022\u0006\u0010!\u001a\u00020\u0002H\u0002\u00a2\u0006\u0002\b*\"\u0015\u0010\u0000\u001a\u00020\u0001*\u00020\u00028F\u00a2\u0006\u0006\u001a\u0004\b\u0003\u0010\u0004\"\u0015\u0010\u0005\u001a\u00020\u0001*\u00020\u00028F\u00a2\u0006\u0006\u001a\u0004\b\u0006\u0010\u0004\"\u0015\u0010\u0007\u001a\u00020\u0001*\u00020\u00028F\u00a2\u0006\u0006\u001a\u0004\b\b\u0010\u0004\u00a8\u0006+"}, d2 = {"extension", "", "Ljava/io/File;", "getExtension", "(Ljava/io/File;)Ljava/lang/String;", "invariantSeparatorsPath", "getInvariantSeparatorsPath", "nameWithoutExtension", "getNameWithoutExtension", "createTempDir", "prefix", "suffix", "directory", "createTempFile", "copyRecursively", "", "target", "overwrite", "onError", "Lkotlin/Function2;", "Ljava/io/IOException;", "Lkotlin/io/OnErrorAction;", "copyTo", "bufferSize", "", "deleteRecursively", "endsWith", "other", "normalize", "", "normalize$FilesKt__UtilsKt", "Lkotlin/io/FilePathComponents;", "relativeTo", "base", "relativeToOrNull", "relativeToOrSelf", "resolve", "relative", "resolveSibling", "startsWith", "toRelativeString", "toRelativeStringOrNull", "toRelativeStringOrNull$FilesKt__UtilsKt", "kotlin-stdlib"}, k = 5, mv = {1, 1, 6}, xi = 1, xs = "kotlin/io/FilesKt")
/* compiled from: Utils.kt */
class FilesKt__UtilsKt extends kotlin.io.FilesKt__FileTreeWalkKt {
    @org.jetbrains.annotations.NotNull
    public static /* bridge */ /* synthetic */ java.io.File createTempDir$default(java.lang.String str, java.lang.String str2, java.io.File file, int i, java.lang.Object obj) {
        if ((i & 1) != 0) {
            str = "tmp";
        }
        return kotlin.io.FilesKt.createTempDir(str, (i & 2) != 0 ? null : str2, (i & 4) != 0 ? null : file);
    }

    @org.jetbrains.annotations.NotNull
    public static final java.io.File createTempDir(@org.jetbrains.annotations.NotNull java.lang.String prefix, @org.jetbrains.annotations.Nullable java.lang.String suffix, @org.jetbrains.annotations.Nullable java.io.File directory) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(prefix, "prefix");
        java.io.File dir = java.io.File.createTempFile(prefix, suffix, directory);
        dir.delete();
        if (dir.mkdir()) {
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(dir, "dir");
            return dir;
        }
        throw new java.io.IOException("Unable to create temporary directory " + dir + ".");
    }

    @org.jetbrains.annotations.NotNull
    public static /* bridge */ /* synthetic */ java.io.File createTempFile$default(java.lang.String str, java.lang.String str2, java.io.File file, int i, java.lang.Object obj) {
        if ((i & 1) != 0) {
            str = "tmp";
        }
        return kotlin.io.FilesKt.createTempFile(str, (i & 2) != 0 ? null : str2, (i & 4) != 0 ? null : file);
    }

    @org.jetbrains.annotations.NotNull
    public static final java.io.File createTempFile(@org.jetbrains.annotations.NotNull java.lang.String prefix, @org.jetbrains.annotations.Nullable java.lang.String suffix, @org.jetbrains.annotations.Nullable java.io.File directory) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(prefix, "prefix");
        java.io.File createTempFile = java.io.File.createTempFile(prefix, suffix, directory);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(createTempFile, "File.createTempFile(prefix, suffix, directory)");
        return createTempFile;
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.String getExtension(@org.jetbrains.annotations.NotNull java.io.File $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        return kotlin.text.StringsKt.substringAfterLast($receiver.getName(), (char) ch.qos.logback.core.CoreConstants.DOT, "");
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.String getInvariantSeparatorsPath(@org.jetbrains.annotations.NotNull java.io.File $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        if (java.io.File.separatorChar != '/') {
            return kotlin.text.StringsKt.replace$default($receiver.getPath(), java.io.File.separatorChar, '/', false, 4, (java.lang.Object) null);
        }
        java.lang.String path = $receiver.getPath();
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(path, "path");
        return path;
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.String getNameWithoutExtension(@org.jetbrains.annotations.NotNull java.io.File $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        return kotlin.text.StringsKt.substringBeforeLast$default($receiver.getName(), ".", (java.lang.String) null, 2, (java.lang.Object) null);
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.String toRelativeString(@org.jetbrains.annotations.NotNull java.io.File $receiver, @org.jetbrains.annotations.NotNull java.io.File base) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(base, "base");
        java.lang.String relativeStringOrNull$FilesKt__UtilsKt = toRelativeStringOrNull$FilesKt__UtilsKt($receiver, base);
        if (relativeStringOrNull$FilesKt__UtilsKt != null) {
            return relativeStringOrNull$FilesKt__UtilsKt;
        }
        throw new java.lang.IllegalArgumentException("this and base files have different roots: " + $receiver + " and " + base + ".");
    }

    @org.jetbrains.annotations.NotNull
    public static final java.io.File relativeTo(@org.jetbrains.annotations.NotNull java.io.File $receiver, @org.jetbrains.annotations.NotNull java.io.File base) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(base, "base");
        return new java.io.File(kotlin.io.FilesKt.toRelativeString($receiver, base));
    }

    @org.jetbrains.annotations.NotNull
    public static final java.io.File relativeToOrSelf(@org.jetbrains.annotations.NotNull java.io.File $receiver, @org.jetbrains.annotations.NotNull java.io.File base) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(base, "base");
        java.lang.String p1 = toRelativeStringOrNull$FilesKt__UtilsKt($receiver, base);
        return p1 != null ? new java.io.File(p1) : $receiver;
    }

    @org.jetbrains.annotations.Nullable
    public static final java.io.File relativeToOrNull(@org.jetbrains.annotations.NotNull java.io.File $receiver, @org.jetbrains.annotations.NotNull java.io.File base) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(base, "base");
        java.lang.String p1 = toRelativeStringOrNull$FilesKt__UtilsKt($receiver, base);
        if (p1 != null) {
            return new java.io.File(p1);
        }
        return null;
    }

    private static final java.lang.String toRelativeStringOrNull$FilesKt__UtilsKt(@org.jetbrains.annotations.NotNull java.io.File $receiver, java.io.File base) {
        kotlin.io.FilePathComponents thisComponents = normalize$FilesKt__UtilsKt(kotlin.io.FilesKt.toComponents($receiver));
        kotlin.io.FilePathComponents baseComponents = normalize$FilesKt__UtilsKt(kotlin.io.FilesKt.toComponents(base));
        if (!kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) thisComponents.getRoot(), (java.lang.Object) baseComponents.getRoot())) {
            return null;
        }
        int baseCount = baseComponents.getSize();
        int thisCount = thisComponents.getSize();
        int i = 0;
        int maxSameCount = java.lang.Math.min(thisCount, baseCount);
        while (i < maxSameCount && kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) (java.io.File) thisComponents.getSegments().get(i), (java.lang.Object) (java.io.File) baseComponents.getSegments().get(i))) {
            i++;
        }
        int sameCount = i;
        java.lang.StringBuilder res = new java.lang.StringBuilder();
        int i2 = baseCount - 1;
        if (i2 >= sameCount) {
            while (!kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) ((java.io.File) baseComponents.getSegments().get(i2)).getName(), (java.lang.Object) "..")) {
                res.append("..");
                if (i2 != sameCount) {
                    res.append(java.io.File.separatorChar);
                }
                if (i2 != sameCount) {
                    i2--;
                }
            }
            return null;
        }
        if (sameCount < thisCount) {
            if (sameCount < baseCount) {
                res.append(java.io.File.separatorChar);
            }
            java.lang.Iterable drop = kotlin.collections.CollectionsKt.drop(thisComponents.getSegments(), sameCount);
            java.lang.Appendable appendable = res;
            java.lang.String str = java.io.File.separator;
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(str, "File.separator");
            kotlin.collections.CollectionsKt.joinTo$default(drop, appendable, str, null, null, 0, null, null, 124, null);
        }
        return res.toString();
    }

    @org.jetbrains.annotations.NotNull
    public static /* bridge */ /* synthetic */ java.io.File copyTo$default(java.io.File file, java.io.File file2, boolean z, int i, int i2, java.lang.Object obj) {
        if ((i2 & 2) != 0) {
            z = false;
        }
        if ((i2 & 4) != 0) {
            i = 8192;
        }
        return kotlin.io.FilesKt.copyTo(file, file2, z, i);
    }

    /* JADX WARNING: Removed duplicated region for block: B:43:0x0090 A[SYNTHETIC, Splitter:B:43:0x0090] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x00a0  */
    @org.jetbrains.annotations.NotNull
    public static final java.io.File copyTo(@org.jetbrains.annotations.NotNull java.io.File $receiver, @org.jetbrains.annotations.NotNull java.io.File target, boolean overwrite, int bufferSize) {
        boolean z;
        boolean z2 = false;
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(target, "target");
        if (!$receiver.exists()) {
            throw new kotlin.io.NoSuchFileException($receiver, null, "The source file doesn't exist.", 2, null);
        }
        if (target.exists()) {
            boolean stillExists = !overwrite ? true : !target.delete();
            if (stillExists) {
                throw new kotlin.io.FileAlreadyExistsException($receiver, target, "The destination file already exists.");
            }
        }
        if (!$receiver.isDirectory()) {
            java.io.File parentFile = target.getParentFile();
            if (parentFile != null) {
                parentFile.mkdirs();
            }
            java.io.Closeable fileInputStream = new java.io.FileInputStream($receiver);
            try {
                java.io.FileInputStream input = (java.io.FileInputStream) fileInputStream;
                java.io.Closeable fileOutputStream = new java.io.FileOutputStream(target);
                try {
                    kotlin.io.ByteStreamsKt.copyTo(input, (java.io.FileOutputStream) fileOutputStream, bufferSize);
                    fileOutputStream.close();
                    fileInputStream.close();
                } catch (java.lang.Exception e) {
                    try {
                        fileOutputStream.close();
                    } catch (java.lang.Exception e2) {
                    }
                    throw e;
                } catch (Throwable th) {
                    th = th;
                    z = true;
                    if (!z) {
                    }
                    throw th;
                }
            } catch (java.lang.Exception e3) {
                try {
                    fileInputStream.close();
                } catch (java.lang.Exception e4) {
                }
                throw e3;
            } catch (Throwable th2) {
                th = th2;
                z2 = true;
                if (!z2) {
                }
                throw th;
            }
        } else if (!target.mkdirs()) {
            throw new kotlin.io.FileSystemException($receiver, target, "Failed to create target directory.");
        }
        return target;
    }

    public static /* bridge */ /* synthetic */ boolean copyRecursively$default(java.io.File file, java.io.File file2, boolean z, kotlin.jvm.functions.Function2 function2, int i, java.lang.Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        return kotlin.io.FilesKt.copyRecursively(file, file2, z, (i & 4) != 0 ? kotlin.io.FilesKt__UtilsKt$copyRecursively$1.INSTANCE : function2);
    }

    public static final boolean copyRecursively(@org.jetbrains.annotations.NotNull java.io.File $receiver, @org.jetbrains.annotations.NotNull java.io.File target, boolean overwrite, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function2<? super java.io.File, ? super java.io.IOException, ? extends kotlin.io.OnErrorAction> onError) {
        boolean stillExists;
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(target, "target");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(onError, "onError");
        if (!$receiver.exists()) {
            return !kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) (kotlin.io.OnErrorAction) onError.invoke($receiver, new kotlin.io.NoSuchFileException($receiver, null, "The source file doesn't exist.", 2, null)), (java.lang.Object) kotlin.io.OnErrorAction.TERMINATE);
        }
        try {
            java.util.Iterator it = kotlin.io.FilesKt.walkTopDown($receiver).onFail(new kotlin.io.FilesKt__UtilsKt$copyRecursively$2(onError)).iterator();
            while (it.hasNext()) {
                java.io.File src = (java.io.File) it.next();
                if (!src.exists()) {
                    if (kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) (kotlin.io.OnErrorAction) onError.invoke(src, new kotlin.io.NoSuchFileException(src, null, "The source file doesn't exist.", 2, null)), (java.lang.Object) kotlin.io.OnErrorAction.TERMINATE)) {
                        return false;
                    }
                } else {
                    java.io.File dstFile = new java.io.File(target, kotlin.io.FilesKt.toRelativeString(src, $receiver));
                    if (dstFile.exists() && (!src.isDirectory() || !dstFile.isDirectory())) {
                        if (!overwrite) {
                            stillExists = true;
                        } else if (dstFile.isDirectory()) {
                            stillExists = !kotlin.io.FilesKt.deleteRecursively(dstFile);
                        } else {
                            stillExists = !dstFile.delete();
                        }
                        if (stillExists) {
                            if (kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) (kotlin.io.OnErrorAction) onError.invoke(dstFile, new kotlin.io.FileAlreadyExistsException(src, dstFile, "The destination file already exists.")), (java.lang.Object) kotlin.io.OnErrorAction.TERMINATE)) {
                                return false;
                            }
                        }
                    }
                    if (src.isDirectory()) {
                        dstFile.mkdirs();
                    } else if (kotlin.io.FilesKt.copyTo$default(src, dstFile, overwrite, 0, 4, null).length() != src.length()) {
                        if (kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) (kotlin.io.OnErrorAction) onError.invoke(src, new java.io.IOException("Source file wasn't copied completely, length of destination file differs.")), (java.lang.Object) kotlin.io.OnErrorAction.TERMINATE)) {
                            return false;
                        }
                    } else {
                        continue;
                    }
                }
            }
            return true;
        } catch (kotlin.io.TerminateException e) {
            return false;
        }
    }

    public static final boolean deleteRecursively(@org.jetbrains.annotations.NotNull java.io.File $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        boolean accumulator$iv = true;
        for (java.io.File file : kotlin.io.FilesKt.walkBottomUp($receiver)) {
            accumulator$iv = (file.delete() || !file.exists()) && accumulator$iv;
        }
        return accumulator$iv;
    }

    public static final boolean startsWith(@org.jetbrains.annotations.NotNull java.io.File $receiver, @org.jetbrains.annotations.NotNull java.io.File other) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(other, "other");
        kotlin.io.FilePathComponents components = kotlin.io.FilesKt.toComponents($receiver);
        kotlin.io.FilePathComponents otherComponents = kotlin.io.FilesKt.toComponents(other);
        if (!(!kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) components.getRoot(), (java.lang.Object) otherComponents.getRoot())) && components.getSize() >= otherComponents.getSize()) {
            return components.getSegments().subList(0, otherComponents.getSize()).equals(otherComponents.getSegments());
        }
        return false;
    }

    public static final boolean startsWith(@org.jetbrains.annotations.NotNull java.io.File $receiver, @org.jetbrains.annotations.NotNull java.lang.String other) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(other, "other");
        return kotlin.io.FilesKt.startsWith($receiver, new java.io.File(other));
    }

    public static final boolean endsWith(@org.jetbrains.annotations.NotNull java.io.File $receiver, @org.jetbrains.annotations.NotNull java.io.File other) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(other, "other");
        kotlin.io.FilePathComponents components = kotlin.io.FilesKt.toComponents($receiver);
        kotlin.io.FilePathComponents otherComponents = kotlin.io.FilesKt.toComponents(other);
        if (otherComponents.isRooted()) {
            return kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) $receiver, (java.lang.Object) other);
        }
        int shift = components.getSize() - otherComponents.getSize();
        if (shift < 0) {
            return false;
        }
        return components.getSegments().subList(shift, components.getSize()).equals(otherComponents.getSegments());
    }

    public static final boolean endsWith(@org.jetbrains.annotations.NotNull java.io.File $receiver, @org.jetbrains.annotations.NotNull java.lang.String other) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(other, "other");
        return kotlin.io.FilesKt.endsWith($receiver, new java.io.File(other));
    }

    @org.jetbrains.annotations.NotNull
    public static final java.io.File normalize(@org.jetbrains.annotations.NotNull java.io.File $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.io.FilePathComponents $receiver2 = kotlin.io.FilesKt.toComponents($receiver);
        java.io.File root = $receiver2.getRoot();
        java.lang.Iterable normalize$FilesKt__UtilsKt = normalize$FilesKt__UtilsKt($receiver2.getSegments());
        java.lang.String str = java.io.File.separator;
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(str, "File.separator");
        return kotlin.io.FilesKt.resolve(root, kotlin.collections.CollectionsKt.joinToString$default(normalize$FilesKt__UtilsKt, str, null, null, 0, null, null, 62, null));
    }

    private static final kotlin.io.FilePathComponents normalize$FilesKt__UtilsKt(@org.jetbrains.annotations.NotNull kotlin.io.FilePathComponents $receiver) {
        return new kotlin.io.FilePathComponents($receiver.getRoot(), normalize$FilesKt__UtilsKt($receiver.getSegments()));
    }

    private static final java.util.List<java.io.File> normalize$FilesKt__UtilsKt(@org.jetbrains.annotations.NotNull java.util.List<? extends java.io.File> $receiver) {
        java.util.List list = new java.util.ArrayList($receiver.size());
        for (java.io.File file : $receiver) {
            java.lang.String name = file.getName();
            if (name != null) {
                switch (name.hashCode()) {
                    case android.support.v4.view.MotionEventCompat.AXIS_GENERIC_15 /*46*/:
                        if (name.equals(".")) {
                            continue;
                        }
                    case 1472:
                        if (name.equals("..")) {
                            if (list.isEmpty() || !(!kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) ((java.io.File) kotlin.collections.CollectionsKt.last(list)).getName(), (java.lang.Object) ".."))) {
                                list.add(file);
                                break;
                            } else {
                                list.remove(list.size() - 1);
                                continue;
                            }
                        }
                        break;
                }
            }
            list.add(file);
        }
        return list;
    }

    @org.jetbrains.annotations.NotNull
    public static final java.io.File resolve(@org.jetbrains.annotations.NotNull java.io.File $receiver, @org.jetbrains.annotations.NotNull java.io.File relative) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(relative, "relative");
        if (kotlin.io.FilesKt.isRooted(relative)) {
            return relative;
        }
        java.lang.String baseName = $receiver.toString();
        return ((baseName.length() == 0) || kotlin.text.StringsKt.endsWith$default((java.lang.CharSequence) baseName, java.io.File.separatorChar, false, 2, (java.lang.Object) null)) ? new java.io.File(baseName + relative) : new java.io.File(baseName + java.io.File.separatorChar + relative);
    }

    @org.jetbrains.annotations.NotNull
    public static final java.io.File resolve(@org.jetbrains.annotations.NotNull java.io.File $receiver, @org.jetbrains.annotations.NotNull java.lang.String relative) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(relative, "relative");
        return kotlin.io.FilesKt.resolve($receiver, new java.io.File(relative));
    }

    @org.jetbrains.annotations.NotNull
    public static final java.io.File resolveSibling(@org.jetbrains.annotations.NotNull java.io.File $receiver, @org.jetbrains.annotations.NotNull java.io.File relative) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(relative, "relative");
        kotlin.io.FilePathComponents components = kotlin.io.FilesKt.toComponents($receiver);
        return kotlin.io.FilesKt.resolve(kotlin.io.FilesKt.resolve(components.getRoot(), components.getSize() == 0 ? new java.io.File("..") : components.subPath(0, components.getSize() - 1)), relative);
    }

    @org.jetbrains.annotations.NotNull
    public static final java.io.File resolveSibling(@org.jetbrains.annotations.NotNull java.io.File $receiver, @org.jetbrains.annotations.NotNull java.lang.String relative) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(relative, "relative");
        return kotlin.io.FilesKt.resolveSibling($receiver, new java.io.File(relative));
    }
}
