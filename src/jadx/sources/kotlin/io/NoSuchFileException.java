package kotlin.io;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0007\u00a8\u0006\b"}, d2 = {"Lkotlin/io/NoSuchFileException;", "Lkotlin/io/FileSystemException;", "file", "Ljava/io/File;", "other", "reason", "", "(Ljava/io/File;Ljava/io/File;Ljava/lang/String;)V", "kotlin-stdlib"}, k = 1, mv = {1, 1, 6})
/* compiled from: Exceptions.kt */
public final class NoSuchFileException extends kotlin.io.FileSystemException {
    public /* synthetic */ NoSuchFileException(java.io.File file, java.io.File file2, java.lang.String str, int i, kotlin.jvm.internal.DefaultConstructorMarker defaultConstructorMarker) {
        java.io.File file3;
        if ((i & 2) != 0) {
            file3 = null;
        } else {
            file3 = file2;
        }
        this(file, file3, (i & 4) != 0 ? null : str);
    }

    public NoSuchFileException(@org.jetbrains.annotations.NotNull java.io.File file, @org.jetbrains.annotations.Nullable java.io.File other, @org.jetbrains.annotations.Nullable java.lang.String reason) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(file, ch.qos.logback.core.joran.action.Action.FILE_ATTRIBUTE);
        super(file, other, reason);
    }
}
