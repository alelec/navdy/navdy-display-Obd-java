package kotlin.io;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\b\u0006"}, d2 = {"<anonymous>", "", "f", "Ljava/io/File;", "e", "Ljava/io/IOException;", "invoke"}, k = 3, mv = {1, 1, 6})
/* compiled from: Utils.kt */
final class FilesKt__UtilsKt$copyRecursively$2 extends kotlin.jvm.internal.Lambda implements kotlin.jvm.functions.Function2<java.io.File, java.io.IOException, kotlin.Unit> {
    final /* synthetic */ kotlin.jvm.functions.Function2 $onError;

    FilesKt__UtilsKt$copyRecursively$2(kotlin.jvm.functions.Function2 function2) {
        this.$onError = function2;
        super(2);
    }

    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        invoke((java.io.File) obj, (java.io.IOException) obj2);
        return kotlin.Unit.INSTANCE;
    }

    public final void invoke(@org.jetbrains.annotations.NotNull java.io.File f, @org.jetbrains.annotations.NotNull java.io.IOException e) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(f, "f");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(e, "e");
        if (kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) (kotlin.io.OnErrorAction) this.$onError.invoke(f, e), (java.lang.Object) kotlin.io.OnErrorAction.TERMINATE)) {
            throw new kotlin.io.TerminateException(f);
        }
    }
}
