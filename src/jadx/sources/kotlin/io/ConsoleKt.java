package kotlin.io;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000B\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\u0010\u0005\n\u0002\u0010\f\n\u0002\u0010\u0019\n\u0002\u0010\u0006\n\u0002\u0010\u0007\n\u0002\u0010\b\n\u0002\u0010\t\n\u0002\u0010\n\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\u001a\u0013\u0010\u0006\u001a\u00020\u00072\b\u0010\b\u001a\u0004\u0018\u00010\tH\u0087\b\u001a\u0011\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\nH\u0087\b\u001a\u0011\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u000bH\u0087\b\u001a\u0011\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\fH\u0087\b\u001a\u0011\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\rH\u0087\b\u001a\u0011\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u000eH\u0087\b\u001a\u0011\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u000fH\u0087\b\u001a\u0011\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u0010H\u0087\b\u001a\u0011\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u0011H\u0087\b\u001a\u0011\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u0012H\u0087\b\u001a\t\u0010\u0013\u001a\u00020\u0007H\u0087\b\u001a\u0013\u0010\u0013\u001a\u00020\u00072\b\u0010\b\u001a\u0004\u0018\u00010\tH\u0087\b\u001a\u0011\u0010\u0013\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\nH\u0087\b\u001a\u0011\u0010\u0013\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u000bH\u0087\b\u001a\u0011\u0010\u0013\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\fH\u0087\b\u001a\u0011\u0010\u0013\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\rH\u0087\b\u001a\u0011\u0010\u0013\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u000eH\u0087\b\u001a\u0011\u0010\u0013\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u000fH\u0087\b\u001a\u0011\u0010\u0013\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u0010H\u0087\b\u001a\u0011\u0010\u0013\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u0011H\u0087\b\u001a\u0011\u0010\u0013\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u0012H\u0087\b\u001a\b\u0010\u0014\u001a\u0004\u0018\u00010\u0015\"\u001b\u0010\u0000\u001a\u00020\u00018BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0004\u0010\u0005\u001a\u0004\b\u0002\u0010\u0003\u00a8\u0006\u0016"}, d2 = {"stdin", "Ljava/io/BufferedReader;", "getStdin", "()Ljava/io/BufferedReader;", "stdin$delegate", "Lkotlin/Lazy;", "print", "", "message", "", "", "", "", "", "", "", "", "", "", "println", "readLine", "", "kotlin-stdlib"}, k = 2, mv = {1, 1, 6})
@kotlin.jvm.JvmName(name = "ConsoleKt")
/* compiled from: Console.kt */
public final class ConsoleKt {
    static final /* synthetic */ kotlin.reflect.KProperty[] $$delegatedProperties = {kotlin.jvm.internal.Reflection.property0(new kotlin.jvm.internal.PropertyReference0Impl(kotlin.jvm.internal.Reflection.getOrCreateKotlinPackage(kotlin.io.ConsoleKt.class, "kotlin-stdlib"), "stdin", "getStdin()Ljava/io/BufferedReader;"))};
    private static final kotlin.Lazy stdin$delegate = kotlin.LazyKt.lazy(kotlin.io.ConsoleKt$stdin$2.INSTANCE);

    private static final java.io.BufferedReader getStdin() {
        kotlin.Lazy lazy = stdin$delegate;
        kotlin.reflect.KProperty kProperty = $$delegatedProperties[0];
        return (java.io.BufferedReader) lazy.getValue();
    }

    @kotlin.internal.InlineOnly
    private static final void print(java.lang.Object message) {
        java.lang.System.out.print(message);
    }

    @kotlin.internal.InlineOnly
    private static final void print(int message) {
        java.lang.System.out.print(message);
    }

    @kotlin.internal.InlineOnly
    private static final void print(long message) {
        java.lang.System.out.print(message);
    }

    @kotlin.internal.InlineOnly
    private static final void print(byte message) {
        java.lang.System.out.print(java.lang.Byte.valueOf(message));
    }

    @kotlin.internal.InlineOnly
    private static final void print(short message) {
        java.lang.System.out.print(java.lang.Short.valueOf(message));
    }

    @kotlin.internal.InlineOnly
    private static final void print(char message) {
        java.lang.System.out.print(message);
    }

    @kotlin.internal.InlineOnly
    private static final void print(boolean message) {
        java.lang.System.out.print(message);
    }

    @kotlin.internal.InlineOnly
    private static final void print(float message) {
        java.lang.System.out.print(message);
    }

    @kotlin.internal.InlineOnly
    private static final void print(double message) {
        java.lang.System.out.print(message);
    }

    @kotlin.internal.InlineOnly
    private static final void print(char[] message) {
        java.lang.System.out.print(message);
    }

    @kotlin.internal.InlineOnly
    private static final void println(java.lang.Object message) {
        java.lang.System.out.println(message);
    }

    @kotlin.internal.InlineOnly
    private static final void println(int message) {
        java.lang.System.out.println(message);
    }

    @kotlin.internal.InlineOnly
    private static final void println(long message) {
        java.lang.System.out.println(message);
    }

    @kotlin.internal.InlineOnly
    private static final void println(byte message) {
        java.lang.System.out.println(java.lang.Byte.valueOf(message));
    }

    @kotlin.internal.InlineOnly
    private static final void println(short message) {
        java.lang.System.out.println(java.lang.Short.valueOf(message));
    }

    @kotlin.internal.InlineOnly
    private static final void println(char message) {
        java.lang.System.out.println(message);
    }

    @kotlin.internal.InlineOnly
    private static final void println(boolean message) {
        java.lang.System.out.println(message);
    }

    @kotlin.internal.InlineOnly
    private static final void println(float message) {
        java.lang.System.out.println(message);
    }

    @kotlin.internal.InlineOnly
    private static final void println(double message) {
        java.lang.System.out.println(message);
    }

    @kotlin.internal.InlineOnly
    private static final void println(char[] message) {
        java.lang.System.out.println(message);
    }

    @kotlin.internal.InlineOnly
    private static final void println() {
        java.lang.System.out.println();
    }

    @org.jetbrains.annotations.Nullable
    public static final java.lang.String readLine() {
        return getStdin().readLine();
    }
}
