package kotlin.io;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000z\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0012\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\u001a\u0012\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0004\u001a\u001c\u0010\u0005\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0006\u001a\u00020\u00072\b\b\u0002\u0010\b\u001a\u00020\t\u001a!\u0010\n\u001a\u00020\u000b*\u00020\u00022\b\b\u0002\u0010\b\u001a\u00020\t2\b\b\u0002\u0010\f\u001a\u00020\rH\u0087\b\u001a!\u0010\u000e\u001a\u00020\u000f*\u00020\u00022\b\b\u0002\u0010\b\u001a\u00020\t2\b\b\u0002\u0010\f\u001a\u00020\rH\u0087\b\u001aB\u0010\u0010\u001a\u00020\u0001*\u00020\u000226\u0010\u0011\u001a2\u0012\u0013\u0012\u00110\u0004\u00a2\u0006\f\b\u0013\u0012\b\b\u0014\u0012\u0004\b\b(\u0015\u0012\u0013\u0012\u00110\r\u00a2\u0006\f\b\u0013\u0012\b\b\u0014\u0012\u0004\b\b(\u0016\u0012\u0004\u0012\u00020\u00010\u0012\u001aJ\u0010\u0010\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0017\u001a\u00020\r26\u0010\u0011\u001a2\u0012\u0013\u0012\u00110\u0004\u00a2\u0006\f\b\u0013\u0012\b\b\u0014\u0012\u0004\b\b(\u0015\u0012\u0013\u0012\u00110\r\u00a2\u0006\f\b\u0013\u0012\b\b\u0014\u0012\u0004\b\b(\u0016\u0012\u0004\u0012\u00020\u00010\u0012\u001a7\u0010\u0018\u001a\u00020\u0001*\u00020\u00022\b\b\u0002\u0010\b\u001a\u00020\t2!\u0010\u0011\u001a\u001d\u0012\u0013\u0012\u00110\u0007\u00a2\u0006\f\b\u0013\u0012\b\b\u0014\u0012\u0004\b\b(\u001a\u0012\u0004\u0012\u00020\u00010\u0019\u001a\r\u0010\u001b\u001a\u00020\u001c*\u00020\u0002H\u0087\b\u001a\r\u0010\u001d\u001a\u00020\u001e*\u00020\u0002H\u0087\b\u001a\u0017\u0010\u001f\u001a\u00020 *\u00020\u00022\b\b\u0002\u0010\b\u001a\u00020\tH\u0087\b\u001a\n\u0010!\u001a\u00020\u0004*\u00020\u0002\u001a\u001a\u0010\"\u001a\b\u0012\u0004\u0012\u00020\u00070#*\u00020\u00022\b\b\u0002\u0010\b\u001a\u00020\t\u001a\u0014\u0010$\u001a\u00020\u0007*\u00020\u00022\b\b\u0002\u0010\b\u001a\u00020\t\u001a\u0017\u0010%\u001a\u00020&*\u00020\u00022\b\b\u0002\u0010\b\u001a\u00020\tH\u0087\b\u001a<\u0010'\u001a\u0002H(\"\u0004\b\u0000\u0010(*\u00020\u00022\b\b\u0002\u0010\b\u001a\u00020\t2\u0018\u0010)\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00070*\u0012\u0004\u0012\u0002H(0\u0019H\u0086\b\u00a2\u0006\u0002\u0010+\u001a\u0012\u0010,\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0004\u001a\u001c\u0010-\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0006\u001a\u00020\u00072\b\b\u0002\u0010\b\u001a\u00020\t\u001a\u0017\u0010.\u001a\u00020/*\u00020\u00022\b\b\u0002\u0010\b\u001a\u00020\tH\u0087\b\u00a8\u00060"}, d2 = {"appendBytes", "", "Ljava/io/File;", "array", "", "appendText", "text", "", "charset", "Ljava/nio/charset/Charset;", "bufferedReader", "Ljava/io/BufferedReader;", "bufferSize", "", "bufferedWriter", "Ljava/io/BufferedWriter;", "forEachBlock", "action", "Lkotlin/Function2;", "Lkotlin/ParameterName;", "name", "buffer", "bytesRead", "blockSize", "forEachLine", "Lkotlin/Function1;", "line", "inputStream", "Ljava/io/FileInputStream;", "outputStream", "Ljava/io/FileOutputStream;", "printWriter", "Ljava/io/PrintWriter;", "readBytes", "readLines", "", "readText", "reader", "Ljava/io/InputStreamReader;", "useLines", "T", "block", "Lkotlin/sequences/Sequence;", "(Ljava/io/File;Ljava/nio/charset/Charset;Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;", "writeBytes", "writeText", "writer", "Ljava/io/OutputStreamWriter;", "kotlin-stdlib"}, k = 5, mv = {1, 1, 6}, xi = 1, xs = "kotlin/io/FilesKt")
/* compiled from: FileReadWrite.kt */
class FilesKt__FileReadWriteKt extends kotlin.io.FilesKt__FilePathComponentsKt {
    @kotlin.internal.InlineOnly
    private static final java.io.InputStreamReader reader(@org.jetbrains.annotations.NotNull java.io.File $receiver, java.nio.charset.Charset charset) {
        return new java.io.InputStreamReader(new java.io.FileInputStream($receiver), charset);
    }

    @kotlin.internal.InlineOnly
    static /* bridge */ /* synthetic */ java.io.InputStreamReader reader$default(java.io.File $receiver, java.nio.charset.Charset charset, int i, java.lang.Object obj) {
        if ((i & 1) != 0) {
            charset = kotlin.text.Charsets.UTF_8;
        }
        return new java.io.InputStreamReader(new java.io.FileInputStream($receiver), charset);
    }

    @kotlin.internal.InlineOnly
    private static final java.io.BufferedReader bufferedReader(@org.jetbrains.annotations.NotNull java.io.File $receiver, java.nio.charset.Charset charset, int bufferSize) {
        java.io.Reader inputStreamReader = new java.io.InputStreamReader(new java.io.FileInputStream($receiver), charset);
        return inputStreamReader instanceof java.io.BufferedReader ? (java.io.BufferedReader) inputStreamReader : new java.io.BufferedReader(inputStreamReader, bufferSize);
    }

    @kotlin.internal.InlineOnly
    static /* bridge */ /* synthetic */ java.io.BufferedReader bufferedReader$default(java.io.File $receiver, java.nio.charset.Charset charset, int bufferSize, int i, java.lang.Object obj) {
        if ((i & 1) != 0) {
            charset = kotlin.text.Charsets.UTF_8;
        }
        if ((i & 2) != 0) {
            bufferSize = 8192;
        }
        java.io.Reader inputStreamReader = new java.io.InputStreamReader(new java.io.FileInputStream($receiver), charset);
        return inputStreamReader instanceof java.io.BufferedReader ? (java.io.BufferedReader) inputStreamReader : new java.io.BufferedReader(inputStreamReader, bufferSize);
    }

    @kotlin.internal.InlineOnly
    private static final java.io.OutputStreamWriter writer(@org.jetbrains.annotations.NotNull java.io.File $receiver, java.nio.charset.Charset charset) {
        return new java.io.OutputStreamWriter(new java.io.FileOutputStream($receiver), charset);
    }

    @kotlin.internal.InlineOnly
    static /* bridge */ /* synthetic */ java.io.OutputStreamWriter writer$default(java.io.File $receiver, java.nio.charset.Charset charset, int i, java.lang.Object obj) {
        if ((i & 1) != 0) {
            charset = kotlin.text.Charsets.UTF_8;
        }
        return new java.io.OutputStreamWriter(new java.io.FileOutputStream($receiver), charset);
    }

    @kotlin.internal.InlineOnly
    private static final java.io.BufferedWriter bufferedWriter(@org.jetbrains.annotations.NotNull java.io.File $receiver, java.nio.charset.Charset charset, int bufferSize) {
        java.io.Writer outputStreamWriter = new java.io.OutputStreamWriter(new java.io.FileOutputStream($receiver), charset);
        return outputStreamWriter instanceof java.io.BufferedWriter ? (java.io.BufferedWriter) outputStreamWriter : new java.io.BufferedWriter(outputStreamWriter, bufferSize);
    }

    @kotlin.internal.InlineOnly
    static /* bridge */ /* synthetic */ java.io.BufferedWriter bufferedWriter$default(java.io.File $receiver, java.nio.charset.Charset charset, int bufferSize, int i, java.lang.Object obj) {
        if ((i & 1) != 0) {
            charset = kotlin.text.Charsets.UTF_8;
        }
        if ((i & 2) != 0) {
            bufferSize = 8192;
        }
        java.io.Writer outputStreamWriter = new java.io.OutputStreamWriter(new java.io.FileOutputStream($receiver), charset);
        return outputStreamWriter instanceof java.io.BufferedWriter ? (java.io.BufferedWriter) outputStreamWriter : new java.io.BufferedWriter(outputStreamWriter, bufferSize);
    }

    @kotlin.internal.InlineOnly
    private static final java.io.PrintWriter printWriter(@org.jetbrains.annotations.NotNull java.io.File $receiver, java.nio.charset.Charset charset) {
        java.io.Writer outputStreamWriter = new java.io.OutputStreamWriter(new java.io.FileOutputStream($receiver), charset);
        return new java.io.PrintWriter(outputStreamWriter instanceof java.io.BufferedWriter ? (java.io.BufferedWriter) outputStreamWriter : new java.io.BufferedWriter(outputStreamWriter, 8192));
    }

    @kotlin.internal.InlineOnly
    static /* bridge */ /* synthetic */ java.io.PrintWriter printWriter$default(java.io.File $receiver, java.nio.charset.Charset charset, int i, java.lang.Object obj) {
        if ((i & 1) != 0) {
            charset = kotlin.text.Charsets.UTF_8;
        }
        java.io.Writer outputStreamWriter = new java.io.OutputStreamWriter(new java.io.FileOutputStream($receiver), charset);
        return new java.io.PrintWriter(outputStreamWriter instanceof java.io.BufferedWriter ? (java.io.BufferedWriter) outputStreamWriter : new java.io.BufferedWriter(outputStreamWriter, 8192));
    }

    @org.jetbrains.annotations.NotNull
    public static final byte[] readBytes(@org.jetbrains.annotations.NotNull java.io.File $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        java.io.Closeable fileInputStream = new java.io.FileInputStream($receiver);
        try {
            java.io.FileInputStream input = (java.io.FileInputStream) fileInputStream;
            int offset = 0;
            long it = $receiver.length();
            if (it > ((long) Integer.MAX_VALUE)) {
                throw new java.lang.OutOfMemoryError("File " + $receiver + " is too big (" + it + " bytes) to fit in memory.");
            }
            int remaining = (int) it;
            byte[] result = new byte[remaining];
            while (remaining > 0) {
                int read = input.read(result, offset, remaining);
                if (read < 0) {
                    break;
                }
                remaining -= read;
                offset += read;
            }
            if (remaining != 0) {
                result = java.util.Arrays.copyOf(result, offset);
                kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(result, "java.util.Arrays.copyOf(this, newSize)");
            }
            fileInputStream.close();
            return result;
        } catch (java.lang.Exception e) {
            try {
                fileInputStream.close();
            } catch (java.lang.Exception e2) {
            }
            throw e;
        } catch (Throwable th) {
            if (1 == 0) {
                fileInputStream.close();
            }
            throw th;
        }
    }

    public static final void writeBytes(@org.jetbrains.annotations.NotNull java.io.File $receiver, @org.jetbrains.annotations.NotNull byte[] array) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(array, "array");
        java.io.Closeable fileOutputStream = new java.io.FileOutputStream($receiver);
        try {
            ((java.io.FileOutputStream) fileOutputStream).write(array);
            kotlin.Unit unit = kotlin.Unit.INSTANCE;
            fileOutputStream.close();
        } catch (java.lang.Exception e) {
            try {
                fileOutputStream.close();
            } catch (java.lang.Exception e2) {
            }
            throw e;
        } catch (Throwable th) {
            if (1 == 0) {
                fileOutputStream.close();
            }
            throw th;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x002d  */
    public static final void appendBytes(@org.jetbrains.annotations.NotNull java.io.File $receiver, @org.jetbrains.annotations.NotNull byte[] array) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(array, "array");
        java.io.Closeable fileOutputStream = new java.io.FileOutputStream($receiver, true);
        boolean z = false;
        try {
            ((java.io.FileOutputStream) fileOutputStream).write(array);
            kotlin.Unit unit = kotlin.Unit.INSTANCE;
            fileOutputStream.close();
        } catch (java.lang.Exception e) {
            try {
                fileOutputStream.close();
            } catch (java.lang.Exception e2) {
            }
            throw e;
        } catch (Throwable th) {
            th = th;
            z = true;
            if (!z) {
            }
            throw th;
        }
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.String readText(@org.jetbrains.annotations.NotNull java.io.File $receiver, @org.jetbrains.annotations.NotNull java.nio.charset.Charset charset) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(charset, "charset");
        return new java.lang.String(kotlin.io.FilesKt.readBytes($receiver), charset);
    }

    @org.jetbrains.annotations.NotNull
    public static /* bridge */ /* synthetic */ java.lang.String readText$default(java.io.File file, java.nio.charset.Charset charset, int i, java.lang.Object obj) {
        if ((i & 1) != 0) {
            charset = kotlin.text.Charsets.UTF_8;
        }
        return kotlin.io.FilesKt.readText(file, charset);
    }

    public static final void writeText(@org.jetbrains.annotations.NotNull java.io.File $receiver, @org.jetbrains.annotations.NotNull java.lang.String text, @org.jetbrains.annotations.NotNull java.nio.charset.Charset charset) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(text, "text");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(charset, "charset");
        byte[] bytes = text.getBytes(charset);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(bytes, "(this as java.lang.String).getBytes(charset)");
        kotlin.io.FilesKt.writeBytes($receiver, bytes);
    }

    public static /* bridge */ /* synthetic */ void writeText$default(java.io.File file, java.lang.String str, java.nio.charset.Charset charset, int i, java.lang.Object obj) {
        if ((i & 2) != 0) {
            charset = kotlin.text.Charsets.UTF_8;
        }
        kotlin.io.FilesKt.writeText(file, str, charset);
    }

    public static final void appendText(@org.jetbrains.annotations.NotNull java.io.File $receiver, @org.jetbrains.annotations.NotNull java.lang.String text, @org.jetbrains.annotations.NotNull java.nio.charset.Charset charset) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(text, "text");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(charset, "charset");
        byte[] bytes = text.getBytes(charset);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(bytes, "(this as java.lang.String).getBytes(charset)");
        kotlin.io.FilesKt.appendBytes($receiver, bytes);
    }

    public static /* bridge */ /* synthetic */ void appendText$default(java.io.File file, java.lang.String str, java.nio.charset.Charset charset, int i, java.lang.Object obj) {
        if ((i & 2) != 0) {
            charset = kotlin.text.Charsets.UTF_8;
        }
        kotlin.io.FilesKt.appendText(file, str, charset);
    }

    public static final void forEachBlock(@org.jetbrains.annotations.NotNull java.io.File $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function2<? super byte[], ? super java.lang.Integer, kotlin.Unit> action) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(action, "action");
        kotlin.io.FilesKt.forEachBlock($receiver, 4096, action);
    }

    public static final void forEachBlock(@org.jetbrains.annotations.NotNull java.io.File $receiver, int blockSize, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function2<? super byte[], ? super java.lang.Integer, kotlin.Unit> action) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(action, "action");
        byte[] arr = new byte[kotlin.ranges.RangesKt.coerceAtLeast(blockSize, 512)];
        java.io.FileInputStream fis = new java.io.FileInputStream($receiver);
        while (true) {
            try {
                int size = fis.read(arr);
                if (size > 0) {
                    action.invoke(arr, java.lang.Integer.valueOf(size));
                } else {
                    return;
                }
            } finally {
                fis.close();
            }
        }
    }

    public static /* bridge */ /* synthetic */ void forEachLine$default(java.io.File file, java.nio.charset.Charset charset, kotlin.jvm.functions.Function1 function1, int i, java.lang.Object obj) {
        if ((i & 1) != 0) {
            charset = kotlin.text.Charsets.UTF_8;
        }
        kotlin.io.FilesKt.forEachLine(file, charset, function1);
    }

    public static final void forEachLine(@org.jetbrains.annotations.NotNull java.io.File $receiver, @org.jetbrains.annotations.NotNull java.nio.charset.Charset charset, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.String, kotlin.Unit> action) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(charset, "charset");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(action, "action");
        kotlin.io.TextStreamsKt.forEachLine(new java.io.BufferedReader(new java.io.InputStreamReader(new java.io.FileInputStream($receiver), charset)), action);
    }

    @kotlin.internal.InlineOnly
    private static final java.io.FileInputStream inputStream(@org.jetbrains.annotations.NotNull java.io.File $receiver) {
        return new java.io.FileInputStream($receiver);
    }

    @kotlin.internal.InlineOnly
    private static final java.io.FileOutputStream outputStream(@org.jetbrains.annotations.NotNull java.io.File $receiver) {
        return new java.io.FileOutputStream($receiver);
    }

    @org.jetbrains.annotations.NotNull
    public static /* bridge */ /* synthetic */ java.util.List readLines$default(java.io.File file, java.nio.charset.Charset charset, int i, java.lang.Object obj) {
        if ((i & 1) != 0) {
            charset = kotlin.text.Charsets.UTF_8;
        }
        return kotlin.io.FilesKt.readLines(file, charset);
    }

    @org.jetbrains.annotations.NotNull
    public static final java.util.List<java.lang.String> readLines(@org.jetbrains.annotations.NotNull java.io.File $receiver, @org.jetbrains.annotations.NotNull java.nio.charset.Charset charset) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(charset, "charset");
        java.util.ArrayList result = new java.util.ArrayList();
        kotlin.io.FilesKt.forEachLine($receiver, charset, new kotlin.io.FilesKt__FileReadWriteKt$readLines$1(result));
        return result;
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x005e  */
    public static /* bridge */ /* synthetic */ java.lang.Object useLines$default(java.io.File $receiver, java.nio.charset.Charset charset, kotlin.jvm.functions.Function1 block, int i, java.lang.Object obj) {
        boolean z = false;
        if ((i & 1) != 0) {
            charset = kotlin.text.Charsets.UTF_8;
        }
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(charset, "charset");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(block, "block");
        java.io.Reader inputStreamReader = new java.io.InputStreamReader(new java.io.FileInputStream($receiver), charset);
        java.io.Closeable bufferedReader = inputStreamReader instanceof java.io.BufferedReader ? (java.io.BufferedReader) inputStreamReader : new java.io.BufferedReader(inputStreamReader, 8192);
        try {
            java.lang.Object invoke = block.invoke(kotlin.io.TextStreamsKt.lineSequence((java.io.BufferedReader) bufferedReader));
            kotlin.jvm.internal.InlineMarker.finallyStart(1);
            bufferedReader.close();
            kotlin.jvm.internal.InlineMarker.finallyEnd(1);
            return invoke;
        } catch (java.lang.Exception e) {
            try {
                bufferedReader.close();
            } catch (java.lang.Exception e2) {
            }
            throw e;
        } catch (Throwable th) {
            th = th;
            z = true;
            kotlin.jvm.internal.InlineMarker.finallyStart(1);
            if (!z) {
            }
            kotlin.jvm.internal.InlineMarker.finallyEnd(1);
            throw th;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x0058  */
    public static final <T> T useLines(@org.jetbrains.annotations.NotNull java.io.File $receiver, @org.jetbrains.annotations.NotNull java.nio.charset.Charset charset, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super kotlin.sequences.Sequence<java.lang.String>, ? extends T> block) {
        boolean z = false;
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(charset, "charset");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(block, "block");
        java.io.Reader inputStreamReader = new java.io.InputStreamReader(new java.io.FileInputStream($receiver), charset);
        java.io.Closeable bufferedReader = inputStreamReader instanceof java.io.BufferedReader ? (java.io.BufferedReader) inputStreamReader : new java.io.BufferedReader(inputStreamReader, 8192);
        try {
            T invoke = block.invoke(kotlin.io.TextStreamsKt.lineSequence((java.io.BufferedReader) bufferedReader));
            kotlin.jvm.internal.InlineMarker.finallyStart(1);
            bufferedReader.close();
            kotlin.jvm.internal.InlineMarker.finallyEnd(1);
            return invoke;
        } catch (java.lang.Exception e) {
            try {
                bufferedReader.close();
            } catch (java.lang.Exception e2) {
            }
            throw e;
        } catch (Throwable th) {
            th = th;
            z = true;
            kotlin.jvm.internal.InlineMarker.finallyStart(1);
            if (!z) {
            }
            kotlin.jvm.internal.InlineMarker.finallyEnd(1);
            throw th;
        }
    }
}
