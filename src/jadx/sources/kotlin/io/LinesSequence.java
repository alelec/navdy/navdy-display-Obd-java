package kotlin.io;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010(\n\u0000\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u000f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00020\u0007H\u0096\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\b"}, d2 = {"Lkotlin/io/LinesSequence;", "Lkotlin/sequences/Sequence;", "", "reader", "Ljava/io/BufferedReader;", "(Ljava/io/BufferedReader;)V", "iterator", "", "kotlin-stdlib"}, k = 1, mv = {1, 1, 6})
/* compiled from: ReadWrite.kt */
final class LinesSequence implements kotlin.sequences.Sequence<java.lang.String> {
    /* access modifiers changed from: private */
    public final java.io.BufferedReader reader;

    public LinesSequence(@org.jetbrains.annotations.NotNull java.io.BufferedReader reader2) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(reader2, "reader");
        this.reader = reader2;
    }

    @org.jetbrains.annotations.NotNull
    public java.util.Iterator<java.lang.String> iterator() {
        return new kotlin.io.LinesSequence$iterator$1<>(this);
    }
}
