package kotlin.io;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0007\b\u0016\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0007R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\tR\u0013\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\f\u00a8\u0006\r"}, d2 = {"Lkotlin/io/FileSystemException;", "Ljava/io/IOException;", "file", "Ljava/io/File;", "other", "reason", "", "(Ljava/io/File;Ljava/io/File;Ljava/lang/String;)V", "getFile", "()Ljava/io/File;", "getOther", "getReason", "()Ljava/lang/String;", "kotlin-stdlib"}, k = 1, mv = {1, 1, 6})
/* compiled from: Exceptions.kt */
public class FileSystemException extends java.io.IOException {
    @org.jetbrains.annotations.NotNull
    private final java.io.File file;
    @org.jetbrains.annotations.Nullable
    private final java.io.File other;
    @org.jetbrains.annotations.Nullable
    private final java.lang.String reason;

    public FileSystemException(@org.jetbrains.annotations.NotNull java.io.File file2, @org.jetbrains.annotations.Nullable java.io.File other2, @org.jetbrains.annotations.Nullable java.lang.String reason2) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(file2, ch.qos.logback.core.joran.action.Action.FILE_ATTRIBUTE);
        super(kotlin.io.ExceptionsKt.constructMessage(file2, other2, reason2));
        this.file = file2;
        this.other = other2;
        this.reason = reason2;
    }

    @org.jetbrains.annotations.NotNull
    public final java.io.File getFile() {
        return this.file;
    }

    public /* synthetic */ FileSystemException(java.io.File file2, java.io.File file3, java.lang.String str, int i, kotlin.jvm.internal.DefaultConstructorMarker defaultConstructorMarker) {
        java.io.File file4;
        if ((i & 2) != 0) {
            file4 = null;
        } else {
            file4 = file3;
        }
        this(file2, file4, (i & 4) != 0 ? null : str);
    }

    @org.jetbrains.annotations.Nullable
    public final java.io.File getOther() {
        return this.other;
    }

    @org.jetbrains.annotations.Nullable
    public final java.lang.String getReason() {
        return this.reason;
    }
}
