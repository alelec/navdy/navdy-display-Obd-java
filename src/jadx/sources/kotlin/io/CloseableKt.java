package kotlin.io;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u001c\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\u001a\u0018\u0010\u0000\u001a\u00020\u0001*\u0004\u0018\u00010\u00022\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004H\u0001\u001a8\u0010\u0005\u001a\u0002H\u0006\"\n\b\u0000\u0010\u0007*\u0004\u0018\u00010\u0002\"\u0004\b\u0001\u0010\u0006*\u0002H\u00072\u0012\u0010\b\u001a\u000e\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u0002H\u00060\tH\u0087\b\u00a2\u0006\u0002\u0010\n\u00a8\u0006\u000b"}, d2 = {"closeFinally", "", "Ljava/io/Closeable;", "cause", "", "use", "R", "T", "block", "Lkotlin/Function1;", "(Ljava/io/Closeable;Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;", "kotlin-stdlib"}, k = 2, mv = {1, 1, 6})
@kotlin.jvm.JvmName(name = "CloseableKt")
/* compiled from: Closeable.kt */
public final class CloseableKt {
    @kotlin.internal.InlineOnly
    private static final <T extends java.io.Closeable, R> R use(T $receiver, kotlin.jvm.functions.Function1<? super T, ? extends R> block) {
        try {
            R invoke = block.invoke($receiver);
            kotlin.jvm.internal.InlineMarker.finallyStart(1);
            if (0 == 0 && $receiver != null) {
                $receiver.close();
            }
            kotlin.jvm.internal.InlineMarker.finallyEnd(1);
            return invoke;
        } catch (java.lang.Exception e) {
            if ($receiver != null) {
                try {
                    $receiver.close();
                } catch (java.lang.Exception e2) {
                }
            }
            throw e;
        } catch (Throwable th) {
            kotlin.jvm.internal.InlineMarker.finallyStart(1);
            if (1 == 0 && $receiver != null) {
                $receiver.close();
            }
            kotlin.jvm.internal.InlineMarker.finallyEnd(1);
            throw th;
        }
    }

    @kotlin.SinceKotlin(version = "1.1")
    @kotlin.PublishedApi
    public static final void closeFinally(@org.jetbrains.annotations.Nullable java.io.Closeable $receiver, @org.jetbrains.annotations.Nullable java.lang.Throwable cause) {
        if ($receiver != null) {
            if (cause == null) {
                $receiver.close();
                return;
            }
            try {
                $receiver.close();
            } catch (Throwable closeException) {
                kotlin.ExceptionsKt.addSuppressed(cause, closeException);
            }
        }
    }
}
