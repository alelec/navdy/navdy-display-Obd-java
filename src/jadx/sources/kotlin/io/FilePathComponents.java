package kotlin.io;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\r\b\u0080\b\u0018\u00002\u00020\u0001B\u001d\b\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u0016\u001a\u00020\u0003H\u00c6\u0003J\u000f\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00030\u0005H\u00c6\u0003J#\u0010\u0018\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\u000e\b\u0002\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0005H\u00c6\u0001J\u0013\u0010\u0019\u001a\u00020\b2\b\u0010\u001a\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u001b\u001a\u00020\u0013H\u00d6\u0001J\u0016\u0010\u001c\u001a\u00020\u00032\u0006\u0010\u001d\u001a\u00020\u00132\u0006\u0010\u001e\u001a\u00020\u0013J\t\u0010\u001f\u001a\u00020\rH\u00d6\u0001R\u0011\u0010\u0007\u001a\u00020\b8F\u00a2\u0006\u0006\u001a\u0004\b\u0007\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\f\u001a\u00020\r8F\u00a2\u0006\u0006\u001a\u0004\b\u000e\u0010\u000fR\u0017\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0011\u0010\u0012\u001a\u00020\u00138F\u00a2\u0006\u0006\u001a\u0004\b\u0014\u0010\u0015\u00a8\u0006 "}, d2 = {"Lkotlin/io/FilePathComponents;", "", "root", "Ljava/io/File;", "segments", "", "(Ljava/io/File;Ljava/util/List;)V", "isRooted", "", "()Z", "getRoot", "()Ljava/io/File;", "rootName", "", "getRootName", "()Ljava/lang/String;", "getSegments", "()Ljava/util/List;", "size", "", "getSize", "()I", "component1", "component2", "copy", "equals", "other", "hashCode", "subPath", "beginIndex", "endIndex", "toString", "kotlin-stdlib"}, k = 1, mv = {1, 1, 6})
/* compiled from: FilePathComponents.kt */
public final class FilePathComponents {
    @org.jetbrains.annotations.NotNull
    private final java.io.File root;
    @org.jetbrains.annotations.NotNull
    private final java.util.List<java.io.File> segments;

    /* JADX WARNING: Incorrect type for immutable var: ssa=java.util.List, code=java.util.List<java.io.File>, for r3v0, types: [java.util.List] */
    @org.jetbrains.annotations.NotNull
    public static /* bridge */ /* synthetic */ kotlin.io.FilePathComponents copy$default(kotlin.io.FilePathComponents filePathComponents, java.io.File file, java.util.List<java.io.File> list, int i, java.lang.Object obj) {
        if ((i & 1) != 0) {
            file = filePathComponents.root;
        }
        if ((i & 2) != 0) {
            list = filePathComponents.segments;
        }
        return filePathComponents.copy(file, list);
    }

    @org.jetbrains.annotations.NotNull
    public final java.io.File component1() {
        return this.root;
    }

    @org.jetbrains.annotations.NotNull
    public final java.util.List<java.io.File> component2() {
        return this.segments;
    }

    @org.jetbrains.annotations.NotNull
    public final kotlin.io.FilePathComponents copy(@org.jetbrains.annotations.NotNull java.io.File root2, @org.jetbrains.annotations.NotNull java.util.List<? extends java.io.File> segments2) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(root2, "root");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(segments2, "segments");
        return new kotlin.io.FilePathComponents(root2, segments2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x001a, code lost:
        if (kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) r2.segments, (java.lang.Object) r3.segments) != false) goto L_0x001c;
     */
    public boolean equals(java.lang.Object obj) {
        if (this != obj) {
            if (obj instanceof kotlin.io.FilePathComponents) {
                kotlin.io.FilePathComponents filePathComponents = (kotlin.io.FilePathComponents) obj;
                if (kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) this.root, (java.lang.Object) filePathComponents.root)) {
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i = 0;
        java.io.File file = this.root;
        int hashCode = (file != null ? file.hashCode() : 0) * 31;
        java.util.List<java.io.File> list = this.segments;
        if (list != null) {
            i = list.hashCode();
        }
        return hashCode + i;
    }

    public java.lang.String toString() {
        return "FilePathComponents(root=" + this.root + ", segments=" + this.segments + ")";
    }

    public FilePathComponents(@org.jetbrains.annotations.NotNull java.io.File root2, @org.jetbrains.annotations.NotNull java.util.List<? extends java.io.File> segments2) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(root2, "root");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(segments2, "segments");
        this.root = root2;
        this.segments = segments2;
    }

    @org.jetbrains.annotations.NotNull
    public final java.io.File getRoot() {
        return this.root;
    }

    @org.jetbrains.annotations.NotNull
    public final java.util.List<java.io.File> getSegments() {
        return this.segments;
    }

    @org.jetbrains.annotations.NotNull
    public final java.lang.String getRootName() {
        java.lang.String path = this.root.getPath();
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(path, "root.path");
        return path;
    }

    public final boolean isRooted() {
        return this.root.getPath().length() > 0;
    }

    public final int getSize() {
        return this.segments.size();
    }

    @org.jetbrains.annotations.NotNull
    public final java.io.File subPath(int beginIndex, int endIndex) {
        if (beginIndex < 0 || beginIndex > endIndex || endIndex > getSize()) {
            throw new java.lang.IllegalArgumentException();
        }
        java.lang.Iterable subList = this.segments.subList(beginIndex, endIndex);
        java.lang.String str = java.io.File.separator;
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(str, "File.separator");
        return new java.io.File(kotlin.collections.CollectionsKt.joinToString$default(subList, str, null, null, 0, null, null, 62, null));
    }
}
