package kotlin.io;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010(\n\u0002\b\u0006\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0003\u001a\u001b\u001cB\u0019\b\u0010\u0012\u0006\u0010\u0003\u001a\u00020\u0002\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006B\u0089\u0001\b\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0002\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005\u0012\u0014\u0010\u0007\u001a\u0010\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\t\u0018\u00010\b\u0012\u0014\u0010\n\u001a\u0010\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u000b\u0018\u00010\b\u00128\u0010\f\u001a4\u0012\u0013\u0012\u00110\u0002\u00a2\u0006\f\b\u000e\u0012\b\b\u000f\u0012\u0004\b\b(\u0010\u0012\u0013\u0012\u00110\u0011\u00a2\u0006\f\b\u000e\u0012\b\b\u000f\u0012\u0004\b\b(\u0012\u0012\u0004\u0012\u00020\u000b\u0018\u00010\r\u0012\b\b\u0002\u0010\u0013\u001a\u00020\u0014\u00a2\u0006\u0002\u0010\u0015J\u000f\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00020\u0017H\u0096\u0002J\u000e\u0010\u0013\u001a\u00020\u00002\u0006\u0010\u0018\u001a\u00020\u0014J\u001a\u0010\u0007\u001a\u00020\u00002\u0012\u0010\u0019\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\t0\bJ \u0010\f\u001a\u00020\u00002\u0018\u0010\u0019\u001a\u0014\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u00020\u000b0\rJ\u001a\u0010\n\u001a\u00020\u00002\u0012\u0010\u0019\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u000b0\bR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0007\u001a\u0010\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\t\u0018\u00010\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R@\u0010\f\u001a4\u0012\u0013\u0012\u00110\u0002\u00a2\u0006\f\b\u000e\u0012\b\b\u000f\u0012\u0004\b\b(\u0010\u0012\u0013\u0012\u00110\u0011\u00a2\u0006\f\b\u000e\u0012\b\b\u000f\u0012\u0004\b\b(\u0012\u0012\u0004\u0012\u00020\u000b\u0018\u00010\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\n\u001a\u0010\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u000b\u0018\u00010\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0002X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001d"}, d2 = {"Lkotlin/io/FileTreeWalk;", "Lkotlin/sequences/Sequence;", "Ljava/io/File;", "start", "direction", "Lkotlin/io/FileWalkDirection;", "(Ljava/io/File;Lkotlin/io/FileWalkDirection;)V", "onEnter", "Lkotlin/Function1;", "", "onLeave", "", "onFail", "Lkotlin/Function2;", "Lkotlin/ParameterName;", "name", "f", "Ljava/io/IOException;", "e", "maxDepth", "", "(Ljava/io/File;Lkotlin/io/FileWalkDirection;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function2;I)V", "iterator", "", "depth", "function", "DirectoryState", "FileTreeWalkIterator", "WalkState", "kotlin-stdlib"}, k = 1, mv = {1, 1, 6})
/* compiled from: FileTreeWalk.kt */
public final class FileTreeWalk implements kotlin.sequences.Sequence<java.io.File> {
    /* access modifiers changed from: private */
    public final kotlin.io.FileWalkDirection direction;
    /* access modifiers changed from: private */
    public final int maxDepth;
    /* access modifiers changed from: private */
    public final kotlin.jvm.functions.Function1<java.io.File, java.lang.Boolean> onEnter;
    /* access modifiers changed from: private */
    public final kotlin.jvm.functions.Function2<java.io.File, java.io.IOException, kotlin.Unit> onFail;
    /* access modifiers changed from: private */
    public final kotlin.jvm.functions.Function1<java.io.File, kotlin.Unit> onLeave;
    /* access modifiers changed from: private */
    public final java.io.File start;

    @kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\"\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004\u00a8\u0006\u0005"}, d2 = {"Lkotlin/io/FileTreeWalk$DirectoryState;", "Lkotlin/io/FileTreeWalk$WalkState;", "rootDir", "Ljava/io/File;", "(Ljava/io/File;)V", "kotlin-stdlib"}, k = 1, mv = {1, 1, 6})
    /* compiled from: FileTreeWalk.kt */
    private static abstract class DirectoryState extends kotlin.io.FileTreeWalk.WalkState {
        public DirectoryState(@org.jetbrains.annotations.NotNull java.io.File rootDir) {
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(rootDir, "rootDir");
            super(rootDir);
            if (kotlin._Assertions.ENABLED) {
                boolean isDirectory = rootDir.isDirectory();
                if (kotlin._Assertions.ENABLED && !isDirectory) {
                    throw new java.lang.AssertionError("rootDir must be verified to be directory beforehand.");
                }
            }
        }
    }

    @kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0082\u0004\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0003\r\u000e\u000fB\u0005\u00a2\u0006\u0002\u0010\u0003J\b\u0010\u0007\u001a\u00020\bH\u0014J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u0002H\u0002J\u000b\u0010\f\u001a\u0004\u0018\u00010\u0002H\u0082\u0010R\u0014\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"}, d2 = {"Lkotlin/io/FileTreeWalk$FileTreeWalkIterator;", "Lkotlin/collections/AbstractIterator;", "Ljava/io/File;", "(Lkotlin/io/FileTreeWalk;)V", "state", "Ljava/util/Stack;", "Lkotlin/io/FileTreeWalk$WalkState;", "computeNext", "", "directoryState", "Lkotlin/io/FileTreeWalk$DirectoryState;", "root", "gotoNext", "BottomUpDirectoryState", "SingleFileState", "TopDownDirectoryState", "kotlin-stdlib"}, k = 1, mv = {1, 1, 6})
    /* compiled from: FileTreeWalk.kt */
    private final class FileTreeWalkIterator extends kotlin.collections.AbstractIterator<java.io.File> {
        private final java.util.Stack<kotlin.io.FileTreeWalk.WalkState> state = new java.util.Stack<>();

        @kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0011\n\u0002\b\u0004\b\u0082\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\n\u0010\r\u001a\u0004\u0018\u00010\u0003H\u0016R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0018\u0010\t\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\nX\u0082\u000e\u00a2\u0006\u0004\n\u0002\u0010\u000bR\u000e\u0010\f\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"}, d2 = {"Lkotlin/io/FileTreeWalk$FileTreeWalkIterator$BottomUpDirectoryState;", "Lkotlin/io/FileTreeWalk$DirectoryState;", "rootDir", "Ljava/io/File;", "(Lkotlin/io/FileTreeWalk$FileTreeWalkIterator;Ljava/io/File;)V", "failed", "", "fileIndex", "", "fileList", "", "[Ljava/io/File;", "rootVisited", "step", "kotlin-stdlib"}, k = 1, mv = {1, 1, 6})
        /* compiled from: FileTreeWalk.kt */
        private final class BottomUpDirectoryState extends kotlin.io.FileTreeWalk.DirectoryState {
            private boolean failed;
            private int fileIndex;
            private java.io.File[] fileList;
            private boolean rootVisited;
            final /* synthetic */ kotlin.io.FileTreeWalk.FileTreeWalkIterator this$0;

            public BottomUpDirectoryState(@org.jetbrains.annotations.NotNull kotlin.io.FileTreeWalk.FileTreeWalkIterator $outer, java.io.File rootDir) {
                kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(rootDir, "rootDir");
                this.this$0 = $outer;
                super(rootDir);
            }

            @org.jetbrains.annotations.Nullable
            public java.io.File step() {
                java.lang.Boolean bool;
                if (!this.failed && this.fileList == null) {
                    kotlin.jvm.functions.Function1 access$getOnEnter$p = kotlin.io.FileTreeWalk.this.onEnter;
                    if (access$getOnEnter$p != null) {
                        bool = (java.lang.Boolean) access$getOnEnter$p.invoke(getRoot());
                    } else {
                        bool = null;
                    }
                    if (kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) bool, (java.lang.Object) java.lang.Boolean.valueOf(false))) {
                        return null;
                    }
                    this.fileList = getRoot().listFiles();
                    if (this.fileList == null) {
                        kotlin.jvm.functions.Function2 access$getOnFail$p = kotlin.io.FileTreeWalk.this.onFail;
                        if (access$getOnFail$p != null) {
                            kotlin.Unit unit = (kotlin.Unit) access$getOnFail$p.invoke(getRoot(), new kotlin.io.AccessDeniedException(getRoot(), null, "Cannot list files in a directory", 2, null));
                        }
                        this.failed = true;
                    }
                }
                if (this.fileList != null) {
                    int i = this.fileIndex;
                    java.io.File[] fileArr = this.fileList;
                    if (fileArr == null) {
                        kotlin.jvm.internal.Intrinsics.throwNpe();
                    }
                    if (i < ((java.lang.Object[]) fileArr).length) {
                        java.io.File[] fileArr2 = this.fileList;
                        if (fileArr2 == null) {
                            kotlin.jvm.internal.Intrinsics.throwNpe();
                        }
                        int i2 = this.fileIndex;
                        this.fileIndex = i2 + 1;
                        return fileArr2[i2];
                    }
                }
                if (!this.rootVisited) {
                    this.rootVisited = true;
                    return getRoot();
                }
                kotlin.jvm.functions.Function1 access$getOnLeave$p = kotlin.io.FileTreeWalk.this.onLeave;
                if (access$getOnLeave$p == null) {
                    return null;
                }
                kotlin.Unit unit2 = (kotlin.Unit) access$getOnLeave$p.invoke(getRoot());
                return null;
            }
        }

        @kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\b\u0082\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\n\u0010\u0007\u001a\u0004\u0018\u00010\u0003H\u0016R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\b"}, d2 = {"Lkotlin/io/FileTreeWalk$FileTreeWalkIterator$SingleFileState;", "Lkotlin/io/FileTreeWalk$WalkState;", "rootFile", "Ljava/io/File;", "(Lkotlin/io/FileTreeWalk$FileTreeWalkIterator;Ljava/io/File;)V", "visited", "", "step", "kotlin-stdlib"}, k = 1, mv = {1, 1, 6})
        /* compiled from: FileTreeWalk.kt */
        private final class SingleFileState extends kotlin.io.FileTreeWalk.WalkState {
            final /* synthetic */ kotlin.io.FileTreeWalk.FileTreeWalkIterator this$0;
            private boolean visited;

            public SingleFileState(@org.jetbrains.annotations.NotNull kotlin.io.FileTreeWalk.FileTreeWalkIterator $outer, java.io.File rootFile) {
                kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(rootFile, "rootFile");
                this.this$0 = $outer;
                super(rootFile);
                if (kotlin._Assertions.ENABLED) {
                    boolean isFile = rootFile.isFile();
                    if (kotlin._Assertions.ENABLED && !isFile) {
                        throw new java.lang.AssertionError("rootFile must be verified to be file beforehand.");
                    }
                }
            }

            @org.jetbrains.annotations.Nullable
            public java.io.File step() {
                if (this.visited) {
                    return null;
                }
                this.visited = true;
                return getRoot();
            }
        }

        @kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0011\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\b\u0082\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\n\u0010\f\u001a\u0004\u0018\u00010\u0003H\u0016R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0018\u0010\u0007\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\bX\u0082\u000e\u00a2\u0006\u0004\n\u0002\u0010\tR\u000e\u0010\n\u001a\u00020\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"}, d2 = {"Lkotlin/io/FileTreeWalk$FileTreeWalkIterator$TopDownDirectoryState;", "Lkotlin/io/FileTreeWalk$DirectoryState;", "rootDir", "Ljava/io/File;", "(Lkotlin/io/FileTreeWalk$FileTreeWalkIterator;Ljava/io/File;)V", "fileIndex", "", "fileList", "", "[Ljava/io/File;", "rootVisited", "", "step", "kotlin-stdlib"}, k = 1, mv = {1, 1, 6})
        /* compiled from: FileTreeWalk.kt */
        private final class TopDownDirectoryState extends kotlin.io.FileTreeWalk.DirectoryState {
            private int fileIndex;
            private java.io.File[] fileList;
            private boolean rootVisited;
            final /* synthetic */ kotlin.io.FileTreeWalk.FileTreeWalkIterator this$0;

            public TopDownDirectoryState(@org.jetbrains.annotations.NotNull kotlin.io.FileTreeWalk.FileTreeWalkIterator $outer, java.io.File rootDir) {
                kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(rootDir, "rootDir");
                this.this$0 = $outer;
                super(rootDir);
            }

            /* JADX WARNING: Code restructure failed: missing block: B:29:0x0082, code lost:
                if (((java.lang.Object[]) r0).length == 0) goto L_0x0084;
             */
            @org.jetbrains.annotations.Nullable
            public java.io.File step() {
                java.lang.Boolean bool;
                if (!this.rootVisited) {
                    kotlin.jvm.functions.Function1 access$getOnEnter$p = kotlin.io.FileTreeWalk.this.onEnter;
                    if (access$getOnEnter$p != null) {
                        bool = (java.lang.Boolean) access$getOnEnter$p.invoke(getRoot());
                    } else {
                        bool = null;
                    }
                    if (kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) bool, (java.lang.Object) java.lang.Boolean.valueOf(false))) {
                        return null;
                    }
                    this.rootVisited = true;
                    return getRoot();
                }
                if (this.fileList != null) {
                    int i = this.fileIndex;
                    java.io.File[] fileArr = this.fileList;
                    if (fileArr == null) {
                        kotlin.jvm.internal.Intrinsics.throwNpe();
                    }
                    if (i >= ((java.lang.Object[]) fileArr).length) {
                        kotlin.jvm.functions.Function1 access$getOnLeave$p = kotlin.io.FileTreeWalk.this.onLeave;
                        if (access$getOnLeave$p == null) {
                            return null;
                        }
                        kotlin.Unit unit = (kotlin.Unit) access$getOnLeave$p.invoke(getRoot());
                        return null;
                    }
                }
                if (this.fileList == null) {
                    this.fileList = getRoot().listFiles();
                    if (this.fileList == null) {
                        kotlin.jvm.functions.Function2 access$getOnFail$p = kotlin.io.FileTreeWalk.this.onFail;
                        if (access$getOnFail$p != null) {
                            kotlin.Unit unit2 = (kotlin.Unit) access$getOnFail$p.invoke(getRoot(), new kotlin.io.AccessDeniedException(getRoot(), null, "Cannot list files in a directory", 2, null));
                        }
                    }
                    if (this.fileList != null) {
                        java.io.File[] fileArr2 = this.fileList;
                        if (fileArr2 == null) {
                            kotlin.jvm.internal.Intrinsics.throwNpe();
                        }
                    }
                    kotlin.jvm.functions.Function1 access$getOnLeave$p2 = kotlin.io.FileTreeWalk.this.onLeave;
                    if (access$getOnLeave$p2 == null) {
                        return null;
                    }
                    kotlin.Unit unit3 = (kotlin.Unit) access$getOnLeave$p2.invoke(getRoot());
                    return null;
                }
                java.io.File[] fileArr3 = this.fileList;
                if (fileArr3 == null) {
                    kotlin.jvm.internal.Intrinsics.throwNpe();
                }
                int i2 = this.fileIndex;
                this.fileIndex = i2 + 1;
                return fileArr3[i2];
            }
        }

        public FileTreeWalkIterator() {
            if (kotlin.io.FileTreeWalk.this.start.isDirectory()) {
                this.state.push(directoryState(kotlin.io.FileTreeWalk.this.start));
            } else if (kotlin.io.FileTreeWalk.this.start.isFile()) {
                this.state.push(new kotlin.io.FileTreeWalk.FileTreeWalkIterator.SingleFileState(this, kotlin.io.FileTreeWalk.this.start));
            } else {
                done();
            }
        }

        /* access modifiers changed from: protected */
        public void computeNext() {
            java.io.File nextFile = gotoNext();
            if (nextFile != null) {
                setNext(nextFile);
            } else {
                done();
            }
        }

        private final kotlin.io.FileTreeWalk.DirectoryState directoryState(java.io.File root) {
            switch (kotlin.io.FileTreeWalk.this.direction) {
                case TOP_DOWN:
                    return new kotlin.io.FileTreeWalk.FileTreeWalkIterator.TopDownDirectoryState(this, root);
                case BOTTOM_UP:
                    return new kotlin.io.FileTreeWalk.FileTreeWalkIterator.BottomUpDirectoryState(this, root);
                default:
                    throw new kotlin.NoWhenBranchMatchedException();
            }
        }

        private final java.io.File gotoNext() {
            while (!this.state.empty()) {
                java.lang.Object peek = this.state.peek();
                if (peek == null) {
                    kotlin.jvm.internal.Intrinsics.throwNpe();
                }
                kotlin.io.FileTreeWalk.WalkState topState = (kotlin.io.FileTreeWalk.WalkState) peek;
                java.io.File file = topState.step();
                if (file == null) {
                    this.state.pop();
                } else if (kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) file, (java.lang.Object) topState.getRoot()) || !file.isDirectory() || this.state.size() >= kotlin.io.FileTreeWalk.this.maxDepth) {
                    return file;
                } else {
                    this.state.push(directoryState(file));
                }
            }
            return null;
        }
    }

    @kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\"\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\n\u0010\u0007\u001a\u0004\u0018\u00010\u0003H&R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\b"}, d2 = {"Lkotlin/io/FileTreeWalk$WalkState;", "", "root", "Ljava/io/File;", "(Ljava/io/File;)V", "getRoot", "()Ljava/io/File;", "step", "kotlin-stdlib"}, k = 1, mv = {1, 1, 6})
    /* compiled from: FileTreeWalk.kt */
    private static abstract class WalkState {
        @org.jetbrains.annotations.NotNull
        private final java.io.File root;

        @org.jetbrains.annotations.Nullable
        public abstract java.io.File step();

        public WalkState(@org.jetbrains.annotations.NotNull java.io.File root2) {
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(root2, "root");
            this.root = root2;
        }

        @org.jetbrains.annotations.NotNull
        public final java.io.File getRoot() {
            return this.root;
        }
    }

    private FileTreeWalk(java.io.File start2, kotlin.io.FileWalkDirection direction2, kotlin.jvm.functions.Function1<? super java.io.File, java.lang.Boolean> onEnter2, kotlin.jvm.functions.Function1<? super java.io.File, kotlin.Unit> onLeave2, kotlin.jvm.functions.Function2<? super java.io.File, ? super java.io.IOException, kotlin.Unit> onFail2, int maxDepth2) {
        this.start = start2;
        this.direction = direction2;
        this.onEnter = onEnter2;
        this.onLeave = onLeave2;
        this.onFail = onFail2;
        this.maxDepth = maxDepth2;
    }

    /* synthetic */ FileTreeWalk(java.io.File file, kotlin.io.FileWalkDirection fileWalkDirection, kotlin.jvm.functions.Function1 function1, kotlin.jvm.functions.Function1 function12, kotlin.jvm.functions.Function2 function2, int i, int i2, kotlin.jvm.internal.DefaultConstructorMarker defaultConstructorMarker) {
        kotlin.io.FileWalkDirection fileWalkDirection2;
        if ((i2 & 2) != 0) {
            fileWalkDirection2 = kotlin.io.FileWalkDirection.TOP_DOWN;
        } else {
            fileWalkDirection2 = fileWalkDirection;
        }
        this(file, fileWalkDirection2, function1, function12, function2, (i2 & 32) != 0 ? Integer.MAX_VALUE : i);
    }

    public FileTreeWalk(@org.jetbrains.annotations.NotNull java.io.File start2, @org.jetbrains.annotations.NotNull kotlin.io.FileWalkDirection direction2) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(start2, "start");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(direction2, "direction");
        this(start2, direction2, null, null, null, 0, 32, null);
    }

    public /* synthetic */ FileTreeWalk(java.io.File file, kotlin.io.FileWalkDirection fileWalkDirection, int i, kotlin.jvm.internal.DefaultConstructorMarker defaultConstructorMarker) {
        if ((i & 2) != 0) {
            fileWalkDirection = kotlin.io.FileWalkDirection.TOP_DOWN;
        }
        this(file, fileWalkDirection);
    }

    @org.jetbrains.annotations.NotNull
    public java.util.Iterator<java.io.File> iterator() {
        return new kotlin.io.FileTreeWalk.FileTreeWalkIterator<>();
    }

    @org.jetbrains.annotations.NotNull
    public final kotlin.io.FileTreeWalk onEnter(@org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.io.File, java.lang.Boolean> function) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(function, "function");
        return new kotlin.io.FileTreeWalk(this.start, this.direction, function, this.onLeave, this.onFail, this.maxDepth);
    }

    @org.jetbrains.annotations.NotNull
    public final kotlin.io.FileTreeWalk onLeave(@org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.io.File, kotlin.Unit> function) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(function, "function");
        return new kotlin.io.FileTreeWalk(this.start, this.direction, this.onEnter, function, this.onFail, this.maxDepth);
    }

    @org.jetbrains.annotations.NotNull
    public final kotlin.io.FileTreeWalk onFail(@org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function2<? super java.io.File, ? super java.io.IOException, kotlin.Unit> function) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(function, "function");
        return new kotlin.io.FileTreeWalk(this.start, this.direction, this.onEnter, this.onLeave, function, this.maxDepth);
    }

    @org.jetbrains.annotations.NotNull
    public final kotlin.io.FileTreeWalk maxDepth(int depth) {
        if (depth > 0) {
            return new kotlin.io.FileTreeWalk(this.start, this.direction, this.onEnter, this.onLeave, this.onFail, depth);
        }
        throw new java.lang.IllegalArgumentException("depth must be positive, but was " + depth + ".");
    }
}
