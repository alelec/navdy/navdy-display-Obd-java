package kotlin.io;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\b\u0002"}, d2 = {"<anonymous>", "Ljava/io/BufferedReader;", "invoke"}, k = 3, mv = {1, 1, 6})
/* compiled from: Console.kt */
final class ConsoleKt$stdin$2 extends kotlin.jvm.internal.Lambda implements kotlin.jvm.functions.Function0<java.io.BufferedReader> {
    public static final kotlin.io.ConsoleKt$stdin$2 INSTANCE = new kotlin.io.ConsoleKt$stdin$2();

    ConsoleKt$stdin$2() {
        super(0);
    }

    @org.jetbrains.annotations.NotNull
    public final java.io.BufferedReader invoke() {
        return new java.io.BufferedReader(new java.io.InputStreamReader(new java.io.InputStream() {
            public int read() {
                return java.lang.System.in.read();
            }

            public void reset() {
                java.lang.System.in.reset();
            }

            public int read(@org.jetbrains.annotations.NotNull byte[] b) {
                kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(b, "b");
                return java.lang.System.in.read(b);
            }

            public void close() {
                java.lang.System.in.close();
            }

            public void mark(int readlimit) {
                java.lang.System.in.mark(readlimit);
            }

            public long skip(long n) {
                return java.lang.System.in.skip(n);
            }

            public int available() {
                return java.lang.System.in.available();
            }

            public boolean markSupported() {
                return java.lang.System.in.markSupported();
            }

            public int read(@org.jetbrains.annotations.NotNull byte[] b, int off, int len) {
                kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(b, "b");
                return java.lang.System.in.read(b, off, len);
            }
        }));
    }
}
