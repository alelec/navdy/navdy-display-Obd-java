package kotlin.io;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000Z\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\u0012\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u0017\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\b\b\u0002\u0010\u0003\u001a\u00020\u0004H\u0087\b\u001a\u0017\u0010\u0000\u001a\u00020\u0005*\u00020\u00062\b\b\u0002\u0010\u0003\u001a\u00020\u0004H\u0087\b\u001a\u0017\u0010\u0007\u001a\u00020\b*\u00020\u00022\b\b\u0002\u0010\t\u001a\u00020\nH\u0087\b\u001a\u0017\u0010\u000b\u001a\u00020\f*\u00020\u00062\b\b\u0002\u0010\t\u001a\u00020\nH\u0087\b\u001a\u0017\u0010\r\u001a\u00020\u000e*\u00020\u000f2\b\b\u0002\u0010\t\u001a\u00020\nH\u0087\b\u001a\u001c\u0010\u0010\u001a\u00020\u0011*\u00020\u00022\u0006\u0010\u0012\u001a\u00020\u00062\b\b\u0002\u0010\u0003\u001a\u00020\u0004\u001a\r\u0010\u0013\u001a\u00020\u000e*\u00020\u0014H\u0087\b\u001a\u001d\u0010\u0013\u001a\u00020\u000e*\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00042\u0006\u0010\u0016\u001a\u00020\u0004H\u0087\b\u001a\r\u0010\u0017\u001a\u00020\u0018*\u00020\u0001H\u0086\u0002\u001a\u0014\u0010\u0019\u001a\u00020\u0014*\u00020\u00022\b\b\u0002\u0010\u001a\u001a\u00020\u0004\u001a\u0017\u0010\u001b\u001a\u00020\u001c*\u00020\u00022\b\b\u0002\u0010\t\u001a\u00020\nH\u0087\b\u001a\u0017\u0010\u001d\u001a\u00020\u001e*\u00020\u00062\b\b\u0002\u0010\t\u001a\u00020\nH\u0087\b\u00a8\u0006\u001f"}, d2 = {"buffered", "Ljava/io/BufferedInputStream;", "Ljava/io/InputStream;", "bufferSize", "", "Ljava/io/BufferedOutputStream;", "Ljava/io/OutputStream;", "bufferedReader", "Ljava/io/BufferedReader;", "charset", "Ljava/nio/charset/Charset;", "bufferedWriter", "Ljava/io/BufferedWriter;", "byteInputStream", "Ljava/io/ByteArrayInputStream;", "", "copyTo", "", "out", "inputStream", "", "offset", "length", "iterator", "Lkotlin/collections/ByteIterator;", "readBytes", "estimatedSize", "reader", "Ljava/io/InputStreamReader;", "writer", "Ljava/io/OutputStreamWriter;", "kotlin-stdlib"}, k = 2, mv = {1, 1, 6})
@kotlin.jvm.JvmName(name = "ByteStreamsKt")
/* compiled from: IOStreams.kt */
public final class ByteStreamsKt {
    @org.jetbrains.annotations.NotNull
    public static final kotlin.collections.ByteIterator iterator(@org.jetbrains.annotations.NotNull java.io.BufferedInputStream $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        return new kotlin.io.ByteStreamsKt$iterator$1($receiver);
    }

    @kotlin.internal.InlineOnly
    private static final java.io.ByteArrayInputStream byteInputStream(@org.jetbrains.annotations.NotNull java.lang.String $receiver, java.nio.charset.Charset charset) {
        if ($receiver == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type java.lang.String");
        }
        byte[] bytes = $receiver.getBytes(charset);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(bytes, "(this as java.lang.String).getBytes(charset)");
        return new java.io.ByteArrayInputStream(bytes);
    }

    @kotlin.internal.InlineOnly
    static /* bridge */ /* synthetic */ java.io.ByteArrayInputStream byteInputStream$default(java.lang.String $receiver, java.nio.charset.Charset charset, int i, java.lang.Object obj) {
        if ((i & 1) != 0) {
            charset = kotlin.text.Charsets.UTF_8;
        }
        if ($receiver == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type java.lang.String");
        }
        byte[] bytes = $receiver.getBytes(charset);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(bytes, "(this as java.lang.String).getBytes(charset)");
        return new java.io.ByteArrayInputStream(bytes);
    }

    @kotlin.internal.InlineOnly
    private static final java.io.ByteArrayInputStream inputStream(@org.jetbrains.annotations.NotNull byte[] $receiver) {
        return new java.io.ByteArrayInputStream($receiver);
    }

    @kotlin.internal.InlineOnly
    private static final java.io.ByteArrayInputStream inputStream(@org.jetbrains.annotations.NotNull byte[] $receiver, int offset, int length) {
        return new java.io.ByteArrayInputStream($receiver, offset, length);
    }

    @kotlin.internal.InlineOnly
    static /* bridge */ /* synthetic */ java.io.BufferedInputStream buffered$default(java.io.InputStream $receiver, int bufferSize, int i, java.lang.Object obj) {
        if ((i & 1) != 0) {
            bufferSize = 8192;
        }
        return $receiver instanceof java.io.BufferedInputStream ? (java.io.BufferedInputStream) $receiver : new java.io.BufferedInputStream($receiver, bufferSize);
    }

    @kotlin.internal.InlineOnly
    private static final java.io.BufferedInputStream buffered(@org.jetbrains.annotations.NotNull java.io.InputStream $receiver, int bufferSize) {
        return $receiver instanceof java.io.BufferedInputStream ? (java.io.BufferedInputStream) $receiver : new java.io.BufferedInputStream($receiver, bufferSize);
    }

    @kotlin.internal.InlineOnly
    private static final java.io.InputStreamReader reader(@org.jetbrains.annotations.NotNull java.io.InputStream $receiver, java.nio.charset.Charset charset) {
        return new java.io.InputStreamReader($receiver, charset);
    }

    @kotlin.internal.InlineOnly
    static /* bridge */ /* synthetic */ java.io.InputStreamReader reader$default(java.io.InputStream $receiver, java.nio.charset.Charset charset, int i, java.lang.Object obj) {
        if ((i & 1) != 0) {
            charset = kotlin.text.Charsets.UTF_8;
        }
        return new java.io.InputStreamReader($receiver, charset);
    }

    @kotlin.internal.InlineOnly
    private static final java.io.BufferedReader bufferedReader(@org.jetbrains.annotations.NotNull java.io.InputStream $receiver, java.nio.charset.Charset charset) {
        java.io.Reader inputStreamReader = new java.io.InputStreamReader($receiver, charset);
        return inputStreamReader instanceof java.io.BufferedReader ? (java.io.BufferedReader) inputStreamReader : new java.io.BufferedReader(inputStreamReader, 8192);
    }

    @kotlin.internal.InlineOnly
    static /* bridge */ /* synthetic */ java.io.BufferedReader bufferedReader$default(java.io.InputStream $receiver, java.nio.charset.Charset charset, int i, java.lang.Object obj) {
        if ((i & 1) != 0) {
            charset = kotlin.text.Charsets.UTF_8;
        }
        java.io.Reader inputStreamReader = new java.io.InputStreamReader($receiver, charset);
        return inputStreamReader instanceof java.io.BufferedReader ? (java.io.BufferedReader) inputStreamReader : new java.io.BufferedReader(inputStreamReader, 8192);
    }

    @kotlin.internal.InlineOnly
    static /* bridge */ /* synthetic */ java.io.BufferedOutputStream buffered$default(java.io.OutputStream $receiver, int bufferSize, int i, java.lang.Object obj) {
        if ((i & 1) != 0) {
            bufferSize = 8192;
        }
        return $receiver instanceof java.io.BufferedOutputStream ? (java.io.BufferedOutputStream) $receiver : new java.io.BufferedOutputStream($receiver, bufferSize);
    }

    @kotlin.internal.InlineOnly
    private static final java.io.BufferedOutputStream buffered(@org.jetbrains.annotations.NotNull java.io.OutputStream $receiver, int bufferSize) {
        return $receiver instanceof java.io.BufferedOutputStream ? (java.io.BufferedOutputStream) $receiver : new java.io.BufferedOutputStream($receiver, bufferSize);
    }

    @kotlin.internal.InlineOnly
    private static final java.io.OutputStreamWriter writer(@org.jetbrains.annotations.NotNull java.io.OutputStream $receiver, java.nio.charset.Charset charset) {
        return new java.io.OutputStreamWriter($receiver, charset);
    }

    @kotlin.internal.InlineOnly
    static /* bridge */ /* synthetic */ java.io.OutputStreamWriter writer$default(java.io.OutputStream $receiver, java.nio.charset.Charset charset, int i, java.lang.Object obj) {
        if ((i & 1) != 0) {
            charset = kotlin.text.Charsets.UTF_8;
        }
        return new java.io.OutputStreamWriter($receiver, charset);
    }

    @kotlin.internal.InlineOnly
    private static final java.io.BufferedWriter bufferedWriter(@org.jetbrains.annotations.NotNull java.io.OutputStream $receiver, java.nio.charset.Charset charset) {
        java.io.Writer outputStreamWriter = new java.io.OutputStreamWriter($receiver, charset);
        return outputStreamWriter instanceof java.io.BufferedWriter ? (java.io.BufferedWriter) outputStreamWriter : new java.io.BufferedWriter(outputStreamWriter, 8192);
    }

    @kotlin.internal.InlineOnly
    static /* bridge */ /* synthetic */ java.io.BufferedWriter bufferedWriter$default(java.io.OutputStream $receiver, java.nio.charset.Charset charset, int i, java.lang.Object obj) {
        if ((i & 1) != 0) {
            charset = kotlin.text.Charsets.UTF_8;
        }
        java.io.Writer outputStreamWriter = new java.io.OutputStreamWriter($receiver, charset);
        return outputStreamWriter instanceof java.io.BufferedWriter ? (java.io.BufferedWriter) outputStreamWriter : new java.io.BufferedWriter(outputStreamWriter, 8192);
    }

    public static /* bridge */ /* synthetic */ long copyTo$default(java.io.InputStream inputStream, java.io.OutputStream outputStream, int i, int i2, java.lang.Object obj) {
        if ((i2 & 2) != 0) {
            i = 8192;
        }
        return copyTo(inputStream, outputStream, i);
    }

    public static final long copyTo(@org.jetbrains.annotations.NotNull java.io.InputStream $receiver, @org.jetbrains.annotations.NotNull java.io.OutputStream out, int bufferSize) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(out, "out");
        long bytesCopied = 0;
        byte[] buffer = new byte[bufferSize];
        int bytes = $receiver.read(buffer);
        while (bytes >= 0) {
            out.write(buffer, 0, bytes);
            bytesCopied += (long) bytes;
            bytes = $receiver.read(buffer);
        }
        return bytesCopied;
    }

    @org.jetbrains.annotations.NotNull
    public static /* bridge */ /* synthetic */ byte[] readBytes$default(java.io.InputStream inputStream, int i, int i2, java.lang.Object obj) {
        if ((i2 & 1) != 0) {
            i = 8192;
        }
        return readBytes(inputStream, i);
    }

    @org.jetbrains.annotations.NotNull
    public static final byte[] readBytes(@org.jetbrains.annotations.NotNull java.io.InputStream $receiver, int estimatedSize) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        java.io.ByteArrayOutputStream buffer = new java.io.ByteArrayOutputStream(java.lang.Math.max(estimatedSize, $receiver.available()));
        copyTo$default($receiver, buffer, 0, 2, null);
        byte[] byteArray = buffer.toByteArray();
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(byteArray, "buffer.toByteArray()");
        return byteArray;
    }
}
