package kotlin.io;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "", "it", "", "invoke"}, k = 3, mv = {1, 1, 6})
/* compiled from: ReadWrite.kt */
final class TextStreamsKt$readLines$1 extends kotlin.jvm.internal.Lambda implements kotlin.jvm.functions.Function1<java.lang.String, kotlin.Unit> {
    final /* synthetic */ java.util.ArrayList $result;

    TextStreamsKt$readLines$1(java.util.ArrayList arrayList) {
        this.$result = arrayList;
        super(1);
    }

    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        invoke((java.lang.String) obj);
        return kotlin.Unit.INSTANCE;
    }

    public final void invoke(@org.jetbrains.annotations.NotNull java.lang.String it) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(it, "it");
        this.$result.add(it);
    }
}
