package kotlin.io;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004\u00a8\u0006\u0005"}, d2 = {"Lkotlin/io/TerminateException;", "Lkotlin/io/FileSystemException;", "file", "Ljava/io/File;", "(Ljava/io/File;)V", "kotlin-stdlib"}, k = 1, mv = {1, 1, 6})
/* compiled from: Utils.kt */
final class TerminateException extends kotlin.io.FileSystemException {
    public TerminateException(@org.jetbrains.annotations.NotNull java.io.File file) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(file, ch.qos.logback.core.joran.action.Action.FILE_ATTRIBUTE);
        super(file, null, null, 6, null);
    }
}
