package kotlin.io;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000X\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0012\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\u001a\u0017\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\b\b\u0002\u0010\u0003\u001a\u00020\u0004H\u0087\b\u001a\u0017\u0010\u0000\u001a\u00020\u0005*\u00020\u00062\b\b\u0002\u0010\u0003\u001a\u00020\u0004H\u0087\b\u001a\u001c\u0010\u0007\u001a\u00020\b*\u00020\u00022\u0006\u0010\t\u001a\u00020\u00062\b\b\u0002\u0010\u0003\u001a\u00020\u0004\u001a\u001e\u0010\n\u001a\u00020\u000b*\u00020\u00022\u0012\u0010\f\u001a\u000e\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000b0\r\u001a\u0010\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u000e0\u0010*\u00020\u0001\u001a\n\u0010\u0011\u001a\u00020\u0012*\u00020\u0013\u001a\u0010\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u000e0\u0015*\u00020\u0002\u001a\n\u0010\u0016\u001a\u00020\u000e*\u00020\u0002\u001a\u0017\u0010\u0016\u001a\u00020\u000e*\u00020\u00132\b\b\u0002\u0010\u0017\u001a\u00020\u0018H\u0087\b\u001a\r\u0010\u0019\u001a\u00020\u001a*\u00020\u000eH\u0087\b\u001a2\u0010\u001b\u001a\u0002H\u001c\"\u0004\b\u0000\u0010\u001c*\u00020\u00022\u0018\u0010\u001d\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000e0\u0010\u0012\u0004\u0012\u0002H\u001c0\rH\u0086\b\u00a2\u0006\u0002\u0010\u001e\u00a8\u0006\u001f"}, d2 = {"buffered", "Ljava/io/BufferedReader;", "Ljava/io/Reader;", "bufferSize", "", "Ljava/io/BufferedWriter;", "Ljava/io/Writer;", "copyTo", "", "out", "forEachLine", "", "action", "Lkotlin/Function1;", "", "lineSequence", "Lkotlin/sequences/Sequence;", "readBytes", "", "Ljava/net/URL;", "readLines", "", "readText", "charset", "Ljava/nio/charset/Charset;", "reader", "Ljava/io/StringReader;", "useLines", "T", "block", "(Ljava/io/Reader;Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;", "kotlin-stdlib"}, k = 2, mv = {1, 1, 6})
@kotlin.jvm.JvmName(name = "TextStreamsKt")
/* compiled from: ReadWrite.kt */
public final class TextStreamsKt {
    @kotlin.internal.InlineOnly
    static /* bridge */ /* synthetic */ java.io.BufferedReader buffered$default(java.io.Reader $receiver, int bufferSize, int i, java.lang.Object obj) {
        if ((i & 1) != 0) {
            bufferSize = 8192;
        }
        return $receiver instanceof java.io.BufferedReader ? (java.io.BufferedReader) $receiver : new java.io.BufferedReader($receiver, bufferSize);
    }

    @kotlin.internal.InlineOnly
    private static final java.io.BufferedReader buffered(@org.jetbrains.annotations.NotNull java.io.Reader $receiver, int bufferSize) {
        return $receiver instanceof java.io.BufferedReader ? (java.io.BufferedReader) $receiver : new java.io.BufferedReader($receiver, bufferSize);
    }

    @kotlin.internal.InlineOnly
    static /* bridge */ /* synthetic */ java.io.BufferedWriter buffered$default(java.io.Writer $receiver, int bufferSize, int i, java.lang.Object obj) {
        if ((i & 1) != 0) {
            bufferSize = 8192;
        }
        return $receiver instanceof java.io.BufferedWriter ? (java.io.BufferedWriter) $receiver : new java.io.BufferedWriter($receiver, bufferSize);
    }

    @kotlin.internal.InlineOnly
    private static final java.io.BufferedWriter buffered(@org.jetbrains.annotations.NotNull java.io.Writer $receiver, int bufferSize) {
        return $receiver instanceof java.io.BufferedWriter ? (java.io.BufferedWriter) $receiver : new java.io.BufferedWriter($receiver, bufferSize);
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x003f  */
    public static final void forEachLine(@org.jetbrains.annotations.NotNull java.io.Reader $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.lang.String, kotlin.Unit> action) {
        boolean z;
        java.lang.Throwable th;
        boolean z2 = false;
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(action, "action");
        java.io.Reader $receiver$iv = $receiver;
        java.io.Closeable bufferedReader = $receiver$iv instanceof java.io.BufferedReader ? (java.io.BufferedReader) $receiver$iv : new java.io.BufferedReader($receiver$iv, 8192);
        try {
            kotlin.jvm.functions.Function1 action$iv = action;
            for (java.lang.Object element$iv : lineSequence((java.io.BufferedReader) bufferedReader)) {
                action$iv.invoke(element$iv);
            }
            kotlin.Unit unit = kotlin.Unit.INSTANCE;
            bufferedReader.close();
        } catch (java.lang.Exception e) {
            z = true;
            try {
                bufferedReader.close();
            } catch (java.lang.Exception e2) {
            }
            throw e;
        } catch (Throwable th2) {
            th = th2;
            if (!z) {
            }
            throw th;
        }
    }

    @org.jetbrains.annotations.NotNull
    public static final java.util.List<java.lang.String> readLines(@org.jetbrains.annotations.NotNull java.io.Reader $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        java.util.ArrayList result = new java.util.ArrayList();
        forEachLine($receiver, new kotlin.io.TextStreamsKt$readLines$1(result));
        return result;
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x0044  */
    public static final <T> T useLines(@org.jetbrains.annotations.NotNull java.io.Reader $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super kotlin.sequences.Sequence<java.lang.String>, ? extends T> block) {
        boolean z;
        java.lang.Throwable th;
        boolean z2 = false;
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(block, "block");
        java.io.Closeable bufferedReader = $receiver instanceof java.io.BufferedReader ? (java.io.BufferedReader) $receiver : new java.io.BufferedReader($receiver, 8192);
        try {
            T invoke = block.invoke(lineSequence((java.io.BufferedReader) bufferedReader));
            kotlin.jvm.internal.InlineMarker.finallyStart(1);
            bufferedReader.close();
            kotlin.jvm.internal.InlineMarker.finallyEnd(1);
            return invoke;
        } catch (java.lang.Exception e) {
            try {
                bufferedReader.close();
            } catch (java.lang.Exception e2) {
            }
            throw e;
        } catch (Throwable th2) {
            th = th2;
            z = true;
            kotlin.jvm.internal.InlineMarker.finallyStart(1);
            if (!z) {
            }
            kotlin.jvm.internal.InlineMarker.finallyEnd(1);
            throw th;
        }
    }

    @kotlin.internal.InlineOnly
    private static final java.io.StringReader reader(@org.jetbrains.annotations.NotNull java.lang.String $receiver) {
        return new java.io.StringReader($receiver);
    }

    @org.jetbrains.annotations.NotNull
    public static final kotlin.sequences.Sequence<java.lang.String> lineSequence(@org.jetbrains.annotations.NotNull java.io.BufferedReader $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        return kotlin.sequences.SequencesKt.constrainOnce(new kotlin.io.LinesSequence($receiver));
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.String readText(@org.jetbrains.annotations.NotNull java.io.Reader $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        java.io.StringWriter buffer = new java.io.StringWriter();
        copyTo$default($receiver, buffer, 0, 2, null);
        java.lang.String stringWriter = buffer.toString();
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(stringWriter, "buffer.toString()");
        return stringWriter;
    }

    public static /* bridge */ /* synthetic */ long copyTo$default(java.io.Reader reader, java.io.Writer writer, int i, int i2, java.lang.Object obj) {
        if ((i2 & 2) != 0) {
            i = 8192;
        }
        return copyTo(reader, writer, i);
    }

    public static final long copyTo(@org.jetbrains.annotations.NotNull java.io.Reader $receiver, @org.jetbrains.annotations.NotNull java.io.Writer out, int bufferSize) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(out, "out");
        long charsCopied = 0;
        char[] buffer = new char[bufferSize];
        int chars = $receiver.read(buffer);
        while (chars >= 0) {
            out.write(buffer, 0, chars);
            charsCopied += (long) chars;
            chars = $receiver.read(buffer);
        }
        return charsCopied;
    }

    @kotlin.internal.InlineOnly
    private static final java.lang.String readText(@org.jetbrains.annotations.NotNull java.net.URL $receiver, java.nio.charset.Charset charset) {
        return new java.lang.String(readBytes($receiver), charset);
    }

    @kotlin.internal.InlineOnly
    static /* bridge */ /* synthetic */ java.lang.String readText$default(java.net.URL $receiver, java.nio.charset.Charset charset, int i, java.lang.Object obj) {
        if ((i & 1) != 0) {
            charset = kotlin.text.Charsets.UTF_8;
        }
        return new java.lang.String(readBytes($receiver), charset);
    }

    @org.jetbrains.annotations.NotNull
    public static final byte[] readBytes(@org.jetbrains.annotations.NotNull java.net.URL $receiver) {
        boolean z = false;
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        java.io.Closeable openStream = $receiver.openStream();
        try {
            byte[] readBytes$default = kotlin.io.ByteStreamsKt.readBytes$default((java.io.InputStream) openStream, 0, 1, null);
            if (openStream != null) {
                openStream.close();
            }
            return readBytes$default;
        } catch (java.lang.Exception e) {
            if (openStream != null) {
                try {
                    openStream.close();
                } catch (java.lang.Exception e2) {
                }
            }
            throw e;
        } catch (Throwable th) {
            th = th;
            z = true;
            openStream.close();
            throw th;
        }
    }
}
