package kotlin.io;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0014\n\u0000\n\u0002\u0010\u0001\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\b\u0006"}, d2 = {"<anonymous>", "", "file", "Ljava/io/File;", "exception", "Ljava/io/IOException;", "invoke"}, k = 3, mv = {1, 1, 6})
/* compiled from: Utils.kt */
final class FilesKt__UtilsKt$copyRecursively$1 extends kotlin.jvm.internal.Lambda implements kotlin.jvm.functions.Function2 {
    public static final kotlin.io.FilesKt__UtilsKt$copyRecursively$1 INSTANCE = new kotlin.io.FilesKt__UtilsKt$copyRecursively$1();

    FilesKt__UtilsKt$copyRecursively$1() {
        super(2);
    }

    @org.jetbrains.annotations.NotNull
    public final java.lang.Void invoke(@org.jetbrains.annotations.NotNull java.io.File file, @org.jetbrains.annotations.NotNull java.io.IOException exception) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(file, ch.qos.logback.core.joran.action.Action.FILE_ATTRIBUTE);
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(exception, "exception");
        throw exception;
    }
}
