package kotlin.io;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000$\n\u0000\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\u001a\u0011\u0010\u000b\u001a\u00020\f*\u00020\bH\u0002\u00a2\u0006\u0002\b\r\u001a\u001c\u0010\u000e\u001a\u00020\u0002*\u00020\u00022\u0006\u0010\u000f\u001a\u00020\f2\u0006\u0010\u0010\u001a\u00020\fH\u0000\u001a\f\u0010\u0011\u001a\u00020\u0012*\u00020\u0002H\u0000\"\u0015\u0010\u0000\u001a\u00020\u0001*\u00020\u00028F\u00a2\u0006\u0006\u001a\u0004\b\u0000\u0010\u0003\"\u0018\u0010\u0004\u001a\u00020\u0002*\u00020\u00028@X\u0080\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0005\u0010\u0006\"\u0018\u0010\u0007\u001a\u00020\b*\u00020\u00028@X\u0080\u0004\u00a2\u0006\u0006\u001a\u0004\b\t\u0010\n\u00a8\u0006\u0013"}, d2 = {"isRooted", "", "Ljava/io/File;", "(Ljava/io/File;)Z", "root", "getRoot", "(Ljava/io/File;)Ljava/io/File;", "rootName", "", "getRootName", "(Ljava/io/File;)Ljava/lang/String;", "getRootLength", "", "getRootLength$FilesKt__FilePathComponentsKt", "subPath", "beginIndex", "endIndex", "toComponents", "Lkotlin/io/FilePathComponents;", "kotlin-stdlib"}, k = 5, mv = {1, 1, 6}, xi = 1, xs = "kotlin/io/FilesKt")
/* compiled from: FilePathComponents.kt */
class FilesKt__FilePathComponentsKt {
    private static final int getRootLength$FilesKt__FilePathComponentsKt(@org.jetbrains.annotations.NotNull java.lang.String $receiver) {
        int first = kotlin.text.StringsKt.indexOf$default((java.lang.CharSequence) $receiver, java.io.File.separatorChar, 0, false, 4, (java.lang.Object) null);
        if (first == 0) {
            if ($receiver.length() > 1 && $receiver.charAt(1) == java.io.File.separatorChar) {
                int first2 = kotlin.text.StringsKt.indexOf$default((java.lang.CharSequence) $receiver, java.io.File.separatorChar, 2, false, 4, (java.lang.Object) null);
                if (first2 >= 0) {
                    int first3 = kotlin.text.StringsKt.indexOf$default((java.lang.CharSequence) $receiver, java.io.File.separatorChar, first2 + 1, false, 4, (java.lang.Object) null);
                    if (first3 >= 0) {
                        return first3 + 1;
                    }
                    return $receiver.length();
                }
            }
            return 1;
        } else if (first > 0 && $receiver.charAt(first - 1) == ':') {
            return first + 1;
        } else {
            if (first != -1 || !kotlin.text.StringsKt.endsWith$default((java.lang.CharSequence) $receiver, (char) ch.qos.logback.core.CoreConstants.COLON_CHAR, false, 2, (java.lang.Object) null)) {
                return 0;
            }
            return $receiver.length();
        }
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.String getRootName(@org.jetbrains.annotations.NotNull java.io.File $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        java.lang.String path = $receiver.getPath();
        int rootLength$FilesKt__FilePathComponentsKt = getRootLength$FilesKt__FilePathComponentsKt($receiver.getPath());
        if (path == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type java.lang.String");
        }
        java.lang.String substring = path.substring(0, rootLength$FilesKt__FilePathComponentsKt);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        return substring;
    }

    @org.jetbrains.annotations.NotNull
    public static final java.io.File getRoot(@org.jetbrains.annotations.NotNull java.io.File $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        return new java.io.File(kotlin.io.FilesKt.getRootName($receiver));
    }

    public static final boolean isRooted(@org.jetbrains.annotations.NotNull java.io.File $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        return getRootLength$FilesKt__FilePathComponentsKt($receiver.getPath()) > 0;
    }

    @org.jetbrains.annotations.NotNull
    public static final kotlin.io.FilePathComponents toComponents(@org.jetbrains.annotations.NotNull java.io.File $receiver) {
        boolean z;
        java.util.List list;
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        java.lang.String path = $receiver.getPath();
        int rootLength = getRootLength$FilesKt__FilePathComponentsKt(path);
        if (path == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type java.lang.String");
        }
        java.lang.String rootName = path.substring(0, rootLength);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(rootName, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        if (path == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type java.lang.String");
        }
        java.lang.String subPath = path.substring(rootLength);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(subPath, "(this as java.lang.String).substring(startIndex)");
        if (subPath.length() == 0) {
            z = true;
        } else {
            z = false;
        }
        if (z) {
            list = kotlin.collections.CollectionsKt.emptyList();
        } else {
            java.lang.Iterable<java.lang.String> $receiver$iv = kotlin.text.StringsKt.split$default((java.lang.CharSequence) subPath, new char[]{java.io.File.separatorChar}, false, 0, 6, (java.lang.Object) null);
            java.util.Collection destination$iv$iv = new java.util.ArrayList(kotlin.collections.CollectionsKt.collectionSizeOrDefault($receiver$iv, 10));
            for (java.lang.String file : $receiver$iv) {
                destination$iv$iv.add(new java.io.File(file));
            }
            list = (java.util.List) destination$iv$iv;
        }
        return new kotlin.io.FilePathComponents(new java.io.File(rootName), list);
    }

    @org.jetbrains.annotations.NotNull
    public static final java.io.File subPath(@org.jetbrains.annotations.NotNull java.io.File $receiver, int beginIndex, int endIndex) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        return kotlin.io.FilesKt.toComponents($receiver).subPath(beginIndex, endIndex);
    }
}
