package kotlin;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0012\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u001a*\u0010\u0000\u001a\u0002H\u0001\"\u0004\b\u0000\u0010\u00012\u0006\u0010\u0002\u001a\u00020\u00032\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u0002H\u00010\u0005H\u0087\b\u00a2\u0006\u0002\u0010\u0006\u00a8\u0006\u0007"}, d2 = {"synchronized", "R", "lock", "", "block", "Lkotlin/Function0;", "(Ljava/lang/Object;Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;", "kotlin-stdlib"}, k = 5, mv = {1, 1, 6}, xi = 1, xs = "kotlin/StandardKt")
/* compiled from: Synchronized.kt */
class StandardKt__SynchronizedKt extends kotlin.StandardKt__StandardKt {
    @kotlin.internal.InlineOnly
    /* renamed from: synchronized reason: not valid java name */
    private static final <R> R m2synchronized(java.lang.Object lock, kotlin.jvm.functions.Function0<? extends R> block) {
        R invoke;
        synchronized (lock) {
            try {
                invoke = block.invoke();
            } finally {
                kotlin.jvm.internal.InlineMarker.finallyStart(1);
                kotlin.jvm.internal.InlineMarker.finallyEnd(1);
            }
        }
        kotlin.jvm.internal.InlineMarker.finallyEnd(1);
        return invoke;
    }
}
