package kotlin;

public class NoWhenBranchMatchedException extends java.lang.RuntimeException {
    public NoWhenBranchMatchedException() {
    }

    public NoWhenBranchMatchedException(java.lang.String message) {
        super(message);
    }

    public NoWhenBranchMatchedException(java.lang.String message, java.lang.Throwable cause) {
        super(message, cause);
    }

    public NoWhenBranchMatchedException(java.lang.Throwable cause) {
        super(cause);
    }
}
