package kotlin.internal;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\b\u0002\b\u0011\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0006H\u0016\u00a8\u0006\b"}, d2 = {"Lkotlin/internal/JRE7PlatformImplementations;", "Lkotlin/internal/PlatformImplementations;", "()V", "addSuppressed", "", "cause", "", "exception", "kotlin-stdlib-jre7"}, k = 1, mv = {1, 1, 6})
/* compiled from: JRE7PlatformImplementations.kt */
public class JRE7PlatformImplementations extends kotlin.internal.PlatformImplementations {
    public void addSuppressed(@org.jetbrains.annotations.NotNull java.lang.Throwable cause, @org.jetbrains.annotations.NotNull java.lang.Throwable exception) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(cause, "cause");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(exception, "exception");
        cause.addSuppressed(exception);
    }
}
