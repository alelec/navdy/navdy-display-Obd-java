package kotlin.internal;

@java.lang.annotation.Target({java.lang.annotation.ElementType.METHOD})
@kotlin.annotation.Target(allowedTargets = {kotlin.annotation.AnnotationTarget.FUNCTION, kotlin.annotation.AnnotationTarget.PROPERTY})
@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\n\n\u0002\u0018\u0002\n\u0002\u0010\u001b\n\u0000\b\u0081\u0002\u0018\u00002\u00020\u0001B\u0000\u00a8\u0006\u0002"}, d2 = {"Lkotlin/internal/HidesMembers;", "", "kotlin-stdlib"}, k = 1, mv = {1, 1, 6})
@kotlin.annotation.Retention(kotlin.annotation.AnnotationRetention.BINARY)
@java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.CLASS)
/* compiled from: Annotations.kt */
public @interface HidesMembers {
}
