package kotlin.internal;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\u001a\b\u0010\u0002\u001a\u00020\u0003H\u0002\"\u0010\u0010\u0000\u001a\u00020\u00018\u0000X\u0081\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0004"}, d2 = {"IMPLEMENTATIONS", "Lkotlin/internal/PlatformImplementations;", "getJavaVersion", "", "kotlin-stdlib"}, k = 2, mv = {1, 1, 6})
/* compiled from: PlatformImplementations.kt */
public final class PlatformImplementationsKt {
    @org.jetbrains.annotations.NotNull
    @kotlin.jvm.JvmField
    public static final kotlin.internal.PlatformImplementations IMPLEMENTATIONS;

    static {
        kotlin.internal.PlatformImplementations platformImplementations;
        int version = getJavaVersion();
        if (version >= 65544) {
            try {
                java.lang.Object newInstance = java.lang.Class.forName("kotlin.internal.JRE8PlatformImplementations").newInstance();
                if (newInstance == null) {
                    throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.internal.PlatformImplementations");
                }
                platformImplementations = (kotlin.internal.PlatformImplementations) newInstance;
                IMPLEMENTATIONS = platformImplementations;
            } catch (java.lang.ClassNotFoundException e) {
            }
        }
        if (version >= 65543) {
            try {
                java.lang.Object newInstance2 = java.lang.Class.forName("kotlin.internal.JRE7PlatformImplementations").newInstance();
                if (newInstance2 == null) {
                    throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.internal.PlatformImplementations");
                }
                platformImplementations = (kotlin.internal.PlatformImplementations) newInstance2;
                IMPLEMENTATIONS = platformImplementations;
            } catch (java.lang.ClassNotFoundException e2) {
            }
        }
        platformImplementations = new kotlin.internal.PlatformImplementations();
        IMPLEMENTATIONS = platformImplementations;
    }

    private static final int getJavaVersion() {
        java.lang.String version = java.lang.System.getProperty("java.specification.version");
        if (version == null) {
            return 65542;
        }
        int firstDot = kotlin.text.StringsKt.indexOf$default((java.lang.CharSequence) version, (char) ch.qos.logback.core.CoreConstants.DOT, 0, false, 6, (java.lang.Object) null);
        if (firstDot < 0) {
            try {
                return java.lang.Integer.parseInt(version) * 65536;
            } catch (java.lang.NumberFormatException e) {
                return 65542;
            }
        } else {
            int secondDot = kotlin.text.StringsKt.indexOf$default((java.lang.CharSequence) version, (char) ch.qos.logback.core.CoreConstants.DOT, firstDot + 1, false, 4, (java.lang.Object) null);
            if (secondDot < 0) {
                secondDot = version.length();
            }
            if (version == null) {
                throw new kotlin.TypeCastException("null cannot be cast to non-null type java.lang.String");
            }
            java.lang.String firstPart = version.substring(0, firstDot);
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(firstPart, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
            int i = firstDot + 1;
            if (version == null) {
                throw new kotlin.TypeCastException("null cannot be cast to non-null type java.lang.String");
            }
            java.lang.String secondPart = version.substring(i, secondDot);
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(secondPart, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
            try {
                return (java.lang.Integer.parseInt(firstPart) * 65536) + java.lang.Integer.parseInt(secondPart);
            } catch (java.lang.NumberFormatException e2) {
                return 65542;
            }
        }
    }
}
