package kotlin;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0002\u0018\u0000*\u0006\b\u0000\u0010\u0001 \u00012\b\u0012\u0004\u0012\u0002H\u00010\u00022\u00060\u0003j\u0002`\u0004B\u001f\u0012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00028\u00000\u0006\u0012\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\b\u00a2\u0006\u0002\u0010\tJ\b\u0010\u000e\u001a\u00020\u000fH\u0016J\b\u0010\u0010\u001a\u00020\u0011H\u0016J\b\u0010\u0012\u001a\u00020\bH\u0002R\u0014\u0010\n\u001a\u0004\u0018\u00010\b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0005\u001a\n\u0012\u0004\u0012\u00028\u0000\u0018\u00010\u0006X\u0088\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u00028\u00008VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\b\f\u0010\r\u00a8\u0006\u0013"}, d2 = {"Lkotlin/SynchronizedLazyImpl;", "T", "Lkotlin/Lazy;", "Ljava/io/Serializable;", "Lkotlin/io/Serializable;", "initializer", "Lkotlin/Function0;", "lock", "", "(Lkotlin/jvm/functions/Function0;Ljava/lang/Object;)V", "_value", "value", "getValue", "()Ljava/lang/Object;", "isInitialized", "", "toString", "", "writeReplace", "kotlin-stdlib"}, k = 1, mv = {1, 1, 6})
/* compiled from: Lazy.kt */
final class SynchronizedLazyImpl<T> implements kotlin.Lazy<T>, java.io.Serializable {
    private volatile java.lang.Object _value;
    private kotlin.jvm.functions.Function0<? extends T> initializer;
    private final java.lang.Object lock;

    public SynchronizedLazyImpl(@org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function0<? extends T> initializer2, @org.jetbrains.annotations.Nullable java.lang.Object lock2) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(initializer2, "initializer");
        this.initializer = initializer2;
        this._value = kotlin.UNINITIALIZED_VALUE.INSTANCE;
        if (lock2 == 0) {
            lock2 = this;
        }
        this.lock = lock2;
    }

    public /* synthetic */ SynchronizedLazyImpl(kotlin.jvm.functions.Function0 function0, java.lang.Object obj, int i, kotlin.jvm.internal.DefaultConstructorMarker defaultConstructorMarker) {
        if ((i & 2) != 0) {
            obj = null;
        }
        this(function0, obj);
    }

    public T getValue() {
        java.lang.Object _v2;
        java.lang.Object _v1 = this._value;
        if (_v1 != kotlin.UNINITIALIZED_VALUE.INSTANCE) {
            return _v1;
        }
        synchronized (this.lock) {
            _v2 = this._value;
            if (_v2 == kotlin.UNINITIALIZED_VALUE.INSTANCE) {
                kotlin.jvm.functions.Function0<? extends T> function0 = this.initializer;
                if (function0 == null) {
                    kotlin.jvm.internal.Intrinsics.throwNpe();
                }
                java.lang.Object typedValue = function0.invoke();
                this._value = typedValue;
                this.initializer = null;
                _v2 = typedValue;
            }
        }
        return _v2;
    }

    public boolean isInitialized() {
        return this._value != kotlin.UNINITIALIZED_VALUE.INSTANCE;
    }

    @org.jetbrains.annotations.NotNull
    public java.lang.String toString() {
        return isInitialized() ? java.lang.String.valueOf(getValue()) : "Lazy value not initialized yet.";
    }

    private final java.lang.Object writeReplace() {
        return new kotlin.InitializedLazyImpl(getValue());
    }
}
