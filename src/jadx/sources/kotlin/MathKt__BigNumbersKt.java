package kotlin;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\u001a\u0015\u0010\u0000\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0001H\u0087\n\u001a\u0015\u0010\u0000\u001a\u00020\u0003*\u00020\u00032\u0006\u0010\u0002\u001a\u00020\u0003H\u0087\n\u001a\u0015\u0010\u0004\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0001H\u0087\n\u001a\u0015\u0010\u0004\u001a\u00020\u0003*\u00020\u00032\u0006\u0010\u0002\u001a\u00020\u0003H\u0087\n\u001a\u0015\u0010\u0005\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0001H\u0087\n\u001a\u0015\u0010\u0006\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0001H\u0087\n\u001a\u0015\u0010\u0006\u001a\u00020\u0003*\u00020\u00032\u0006\u0010\u0002\u001a\u00020\u0003H\u0087\n\u001a\u0015\u0010\u0007\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0001H\u0087\n\u001a\u0015\u0010\u0007\u001a\u00020\u0003*\u00020\u00032\u0006\u0010\u0002\u001a\u00020\u0003H\u0087\n\u001a\u0015\u0010\b\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0001H\u0087\n\u001a\u0015\u0010\b\u001a\u00020\u0003*\u00020\u00032\u0006\u0010\u0002\u001a\u00020\u0003H\u0087\n\u001a\r\u0010\t\u001a\u00020\u0001*\u00020\u0001H\u0087\n\u001a\r\u0010\t\u001a\u00020\u0003*\u00020\u0003H\u0087\n\u00a8\u0006\n"}, d2 = {"div", "Ljava/math/BigDecimal;", "other", "Ljava/math/BigInteger;", "minus", "mod", "plus", "rem", "times", "unaryMinus", "kotlin-stdlib"}, k = 5, mv = {1, 1, 6}, xi = 1, xs = "kotlin/MathKt")
/* compiled from: BigNumbers.kt */
class MathKt__BigNumbersKt {
    @kotlin.internal.InlineOnly
    private static final java.math.BigInteger plus(@org.jetbrains.annotations.NotNull java.math.BigInteger $receiver, java.math.BigInteger other) {
        java.math.BigInteger add = $receiver.add(other);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(add, "this.add(other)");
        return add;
    }

    @kotlin.internal.InlineOnly
    private static final java.math.BigInteger minus(@org.jetbrains.annotations.NotNull java.math.BigInteger $receiver, java.math.BigInteger other) {
        java.math.BigInteger subtract = $receiver.subtract(other);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(subtract, "this.subtract(other)");
        return subtract;
    }

    @kotlin.internal.InlineOnly
    private static final java.math.BigInteger times(@org.jetbrains.annotations.NotNull java.math.BigInteger $receiver, java.math.BigInteger other) {
        java.math.BigInteger multiply = $receiver.multiply(other);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(multiply, "this.multiply(other)");
        return multiply;
    }

    @kotlin.internal.InlineOnly
    private static final java.math.BigInteger div(@org.jetbrains.annotations.NotNull java.math.BigInteger $receiver, java.math.BigInteger other) {
        java.math.BigInteger divide = $receiver.divide(other);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(divide, "this.divide(other)");
        return divide;
    }

    @kotlin.SinceKotlin(version = "1.1")
    @kotlin.internal.InlineOnly
    private static final java.math.BigInteger rem(@org.jetbrains.annotations.NotNull java.math.BigInteger $receiver, java.math.BigInteger other) {
        java.math.BigInteger remainder = $receiver.remainder(other);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(remainder, "this.remainder(other)");
        return remainder;
    }

    @kotlin.internal.InlineOnly
    private static final java.math.BigInteger unaryMinus(@org.jetbrains.annotations.NotNull java.math.BigInteger $receiver) {
        java.math.BigInteger negate = $receiver.negate();
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(negate, "this.negate()");
        return negate;
    }

    @kotlin.internal.InlineOnly
    private static final java.math.BigDecimal plus(@org.jetbrains.annotations.NotNull java.math.BigDecimal $receiver, java.math.BigDecimal other) {
        java.math.BigDecimal add = $receiver.add(other);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(add, "this.add(other)");
        return add;
    }

    @kotlin.internal.InlineOnly
    private static final java.math.BigDecimal minus(@org.jetbrains.annotations.NotNull java.math.BigDecimal $receiver, java.math.BigDecimal other) {
        java.math.BigDecimal subtract = $receiver.subtract(other);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(subtract, "this.subtract(other)");
        return subtract;
    }

    @kotlin.internal.InlineOnly
    private static final java.math.BigDecimal times(@org.jetbrains.annotations.NotNull java.math.BigDecimal $receiver, java.math.BigDecimal other) {
        java.math.BigDecimal multiply = $receiver.multiply(other);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(multiply, "this.multiply(other)");
        return multiply;
    }

    @kotlin.internal.InlineOnly
    private static final java.math.BigDecimal div(@org.jetbrains.annotations.NotNull java.math.BigDecimal $receiver, java.math.BigDecimal other) {
        java.math.BigDecimal divide = $receiver.divide(other, java.math.RoundingMode.HALF_EVEN);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(divide, "this.divide(other, RoundingMode.HALF_EVEN)");
        return divide;
    }

    @kotlin.Deprecated(level = kotlin.DeprecationLevel.WARNING, message = "Use rem(other) instead", replaceWith = @kotlin.ReplaceWith(expression = "rem(other)", imports = {}))
    @kotlin.internal.InlineOnly
    private static final java.math.BigDecimal mod(@org.jetbrains.annotations.NotNull java.math.BigDecimal $receiver, java.math.BigDecimal other) {
        java.math.BigDecimal remainder = $receiver.remainder(other);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(remainder, "this.remainder(other)");
        return remainder;
    }

    @kotlin.internal.InlineOnly
    private static final java.math.BigDecimal rem(@org.jetbrains.annotations.NotNull java.math.BigDecimal $receiver, java.math.BigDecimal other) {
        java.math.BigDecimal remainder = $receiver.remainder(other);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(remainder, "this.remainder(other)");
        return remainder;
    }

    @kotlin.internal.InlineOnly
    private static final java.math.BigDecimal unaryMinus(@org.jetbrains.annotations.NotNull java.math.BigDecimal $receiver) {
        java.math.BigDecimal negate = $receiver.negate();
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(negate, "this.negate()");
        return negate;
    }
}
