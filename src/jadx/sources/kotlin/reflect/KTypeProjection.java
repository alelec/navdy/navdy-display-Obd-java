package kotlin.reflect;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0087\b\u0018\u0000 \u00152\u00020\u0001:\u0001\u0015B\u0019\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0006J\u000b\u0010\u000b\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010\f\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003J!\u0010\r\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005H\u00c6\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0011\u001a\u00020\u0012H\u00d6\u0001J\t\u0010\u0013\u001a\u00020\u0014H\u00d6\u0001R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n\u00a8\u0006\u0016"}, d2 = {"Lkotlin/reflect/KTypeProjection;", "", "variance", "Lkotlin/reflect/KVariance;", "type", "Lkotlin/reflect/KType;", "(Lkotlin/reflect/KVariance;Lkotlin/reflect/KType;)V", "getType", "()Lkotlin/reflect/KType;", "getVariance", "()Lkotlin/reflect/KVariance;", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "", "Companion", "kotlin-runtime"}, k = 1, mv = {1, 1, 6})
@kotlin.SinceKotlin(version = "1.1")
/* compiled from: KType.kt */
public final class KTypeProjection {
    public static final kotlin.reflect.KTypeProjection.Companion Companion = new kotlin.reflect.KTypeProjection.Companion(null);
    /* access modifiers changed from: private */
    @org.jetbrains.annotations.NotNull
    public static final kotlin.reflect.KTypeProjection STAR = new kotlin.reflect.KTypeProjection(null, null);
    @org.jetbrains.annotations.Nullable
    private final kotlin.reflect.KType type;
    @org.jetbrains.annotations.Nullable
    private final kotlin.reflect.KVariance variance;

    @kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0007\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\tJ\u000e\u0010\n\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\tJ\u000e\u0010\u000b\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\tR\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\f"}, d2 = {"Lkotlin/reflect/KTypeProjection$Companion;", "", "()V", "STAR", "Lkotlin/reflect/KTypeProjection;", "getSTAR", "()Lkotlin/reflect/KTypeProjection;", "contravariant", "type", "Lkotlin/reflect/KType;", "covariant", "invariant", "kotlin-runtime"}, k = 1, mv = {1, 1, 6})
    /* compiled from: KType.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(kotlin.jvm.internal.DefaultConstructorMarker $constructor_marker) {
            this();
        }

        @org.jetbrains.annotations.NotNull
        public final kotlin.reflect.KTypeProjection getSTAR() {
            return kotlin.reflect.KTypeProjection.STAR;
        }

        @org.jetbrains.annotations.NotNull
        public final kotlin.reflect.KTypeProjection invariant(@org.jetbrains.annotations.NotNull kotlin.reflect.KType type) {
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(type, "type");
            return new kotlin.reflect.KTypeProjection(kotlin.reflect.KVariance.INVARIANT, type);
        }

        @org.jetbrains.annotations.NotNull
        public final kotlin.reflect.KTypeProjection contravariant(@org.jetbrains.annotations.NotNull kotlin.reflect.KType type) {
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(type, "type");
            return new kotlin.reflect.KTypeProjection(kotlin.reflect.KVariance.IN, type);
        }

        @org.jetbrains.annotations.NotNull
        public final kotlin.reflect.KTypeProjection covariant(@org.jetbrains.annotations.NotNull kotlin.reflect.KType type) {
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(type, "type");
            return new kotlin.reflect.KTypeProjection(kotlin.reflect.KVariance.OUT, type);
        }
    }

    @org.jetbrains.annotations.NotNull
    public static /* bridge */ /* synthetic */ kotlin.reflect.KTypeProjection copy$default(kotlin.reflect.KTypeProjection kTypeProjection, kotlin.reflect.KVariance kVariance, kotlin.reflect.KType kType, int i, java.lang.Object obj) {
        if ((i & 1) != 0) {
            kVariance = kTypeProjection.variance;
        }
        if ((i & 2) != 0) {
            kType = kTypeProjection.type;
        }
        return kTypeProjection.copy(kVariance, kType);
    }

    @org.jetbrains.annotations.Nullable
    public final kotlin.reflect.KVariance component1() {
        return this.variance;
    }

    @org.jetbrains.annotations.Nullable
    public final kotlin.reflect.KType component2() {
        return this.type;
    }

    @org.jetbrains.annotations.NotNull
    public final kotlin.reflect.KTypeProjection copy(@org.jetbrains.annotations.Nullable kotlin.reflect.KVariance variance2, @org.jetbrains.annotations.Nullable kotlin.reflect.KType type2) {
        return new kotlin.reflect.KTypeProjection(variance2, type2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x001a, code lost:
        if (kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) r2.type, (java.lang.Object) r3.type) != false) goto L_0x001c;
     */
    public boolean equals(java.lang.Object obj) {
        if (this != obj) {
            if (obj instanceof kotlin.reflect.KTypeProjection) {
                kotlin.reflect.KTypeProjection kTypeProjection = (kotlin.reflect.KTypeProjection) obj;
                if (kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) this.variance, (java.lang.Object) kTypeProjection.variance)) {
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i = 0;
        kotlin.reflect.KVariance kVariance = this.variance;
        int hashCode = (kVariance != null ? kVariance.hashCode() : 0) * 31;
        kotlin.reflect.KType kType = this.type;
        if (kType != null) {
            i = kType.hashCode();
        }
        return hashCode + i;
    }

    public java.lang.String toString() {
        return "KTypeProjection(variance=" + this.variance + ", type=" + this.type + ")";
    }

    public KTypeProjection(@org.jetbrains.annotations.Nullable kotlin.reflect.KVariance variance2, @org.jetbrains.annotations.Nullable kotlin.reflect.KType type2) {
        this.variance = variance2;
        this.type = type2;
    }

    @org.jetbrains.annotations.Nullable
    public final kotlin.reflect.KVariance getVariance() {
        return this.variance;
    }

    @org.jetbrains.annotations.Nullable
    public final kotlin.reflect.KType getType() {
        return this.type;
    }
}
