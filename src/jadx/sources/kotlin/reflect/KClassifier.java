package kotlin.reflect;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\n\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\bg\u0018\u00002\u00020\u0001\u00a8\u0006\u0002"}, d2 = {"Lkotlin/reflect/KClassifier;", "", "kotlin-runtime"}, k = 1, mv = {1, 1, 6})
@kotlin.SinceKotlin(version = "1.1")
/* compiled from: KClassifier.kt */
public interface KClassifier {
}
