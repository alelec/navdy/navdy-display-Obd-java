package kotlin.reflect;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0010\u001b\n\u0002\b\u0003\bf\u0018\u00002\u00020\u0001R\u0018\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0007"}, d2 = {"Lkotlin/reflect/KAnnotatedElement;", "", "annotations", "", "", "getAnnotations", "()Ljava/util/List;", "kotlin-runtime"}, k = 1, mv = {1, 1, 6})
/* compiled from: KAnnotatedElement.kt */
public interface KAnnotatedElement {
    @org.jetbrains.annotations.NotNull
    java.util.List<java.lang.annotation.Annotation> getAnnotations();
}
