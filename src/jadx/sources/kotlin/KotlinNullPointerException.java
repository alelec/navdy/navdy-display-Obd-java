package kotlin;

public class KotlinNullPointerException extends java.lang.NullPointerException {
    public KotlinNullPointerException() {
    }

    public KotlinNullPointerException(java.lang.String message) {
        super(message);
    }
}
