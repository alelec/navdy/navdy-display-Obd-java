package kotlin.coroutines.experimental;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\b'\u0018\u00002\u00020\u0001B\u0011\u0012\n\u0010\u0002\u001a\u0006\u0012\u0002\b\u00030\u0003\u00a2\u0006\u0002\u0010\u0004J5\u0010\u0007\u001a\u0002H\b\"\u0004\b\u0000\u0010\b2\u0006\u0010\t\u001a\u0002H\b2\u0018\u0010\n\u001a\u0014\u0012\u0004\u0012\u0002H\b\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u0002H\b0\u000bH\u0016\u00a2\u0006\u0002\u0010\fJ(\u0010\r\u001a\u0004\u0018\u0001H\u000e\"\b\b\u0000\u0010\u000e*\u00020\u00012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u0002H\u000e0\u0003H\u0097\u0002\u00a2\u0006\u0002\u0010\u000fJ\u0014\u0010\u0010\u001a\u00020\u00112\n\u0010\u0002\u001a\u0006\u0012\u0002\b\u00030\u0003H\u0016J\u0011\u0010\u0012\u001a\u00020\u00112\u0006\u0010\u0013\u001a\u00020\u0011H\u0096\u0002R\u0018\u0010\u0002\u001a\u0006\u0012\u0002\b\u00030\u0003X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0014"}, d2 = {"Lkotlin/coroutines/experimental/AbstractCoroutineContextElement;", "Lkotlin/coroutines/experimental/CoroutineContext$Element;", "key", "Lkotlin/coroutines/experimental/CoroutineContext$Key;", "(Lkotlin/coroutines/experimental/CoroutineContext$Key;)V", "getKey", "()Lkotlin/coroutines/experimental/CoroutineContext$Key;", "fold", "R", "initial", "operation", "Lkotlin/Function2;", "(Ljava/lang/Object;Lkotlin/jvm/functions/Function2;)Ljava/lang/Object;", "get", "E", "(Lkotlin/coroutines/experimental/CoroutineContext$Key;)Lkotlin/coroutines/experimental/CoroutineContext$Element;", "minusKey", "Lkotlin/coroutines/experimental/CoroutineContext;", "plus", "context", "kotlin-stdlib"}, k = 1, mv = {1, 1, 6})
@kotlin.SinceKotlin(version = "1.1")
/* compiled from: CoroutineContextImpl.kt */
public abstract class AbstractCoroutineContextElement implements kotlin.coroutines.experimental.CoroutineContext.Element {
    @org.jetbrains.annotations.NotNull
    private final kotlin.coroutines.experimental.CoroutineContext.Key<?> key;

    public AbstractCoroutineContextElement(@org.jetbrains.annotations.NotNull kotlin.coroutines.experimental.CoroutineContext.Key<?> key2) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(key2, ch.qos.logback.core.joran.action.Action.KEY_ATTRIBUTE);
        this.key = key2;
    }

    @org.jetbrains.annotations.NotNull
    public kotlin.coroutines.experimental.CoroutineContext.Key<?> getKey() {
        return this.key;
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    @org.jetbrains.annotations.Nullable
    public <E extends kotlin.coroutines.experimental.CoroutineContext.Element> E get(@org.jetbrains.annotations.NotNull kotlin.coroutines.experimental.CoroutineContext.Key<E> key2) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(key2, ch.qos.logback.core.joran.action.Action.KEY_ATTRIBUTE);
        if (getKey() != key2) {
            return null;
        }
        if (this != null) {
            return this;
        }
        throw new kotlin.TypeCastException("null cannot be cast to non-null type E");
    }

    public <R> R fold(R initial, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function2<? super R, ? super kotlin.coroutines.experimental.CoroutineContext.Element, ? extends R> operation) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(operation, "operation");
        return operation.invoke(initial, this);
    }

    @org.jetbrains.annotations.NotNull
    public kotlin.coroutines.experimental.CoroutineContext plus(@org.jetbrains.annotations.NotNull kotlin.coroutines.experimental.CoroutineContext context) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(context, ch.qos.logback.core.CoreConstants.CONTEXT_SCOPE_VALUE);
        return kotlin.coroutines.experimental.CoroutineContextImplKt.plusImpl(this, context);
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    @org.jetbrains.annotations.NotNull
    public kotlin.coroutines.experimental.CoroutineContext minusKey(@org.jetbrains.annotations.NotNull kotlin.coroutines.experimental.CoroutineContext.Key<?> key2) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(key2, ch.qos.logback.core.joran.action.Action.KEY_ATTRIBUTE);
        return getKey() == key2 ? kotlin.coroutines.experimental.EmptyCoroutineContext.INSTANCE : this;
    }
}
