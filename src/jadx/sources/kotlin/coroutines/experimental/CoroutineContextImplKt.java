package kotlin.coroutines.experimental;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u001a\u0014\u0010\u0000\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0001H\u0002\u00a8\u0006\u0003"}, d2 = {"plusImpl", "Lkotlin/coroutines/experimental/CoroutineContext;", "context", "kotlin-stdlib"}, k = 2, mv = {1, 1, 6})
/* compiled from: CoroutineContextImpl.kt */
public final class CoroutineContextImplKt {
    /* access modifiers changed from: private */
    public static final kotlin.coroutines.experimental.CoroutineContext plusImpl(@org.jetbrains.annotations.NotNull kotlin.coroutines.experimental.CoroutineContext $receiver, kotlin.coroutines.experimental.CoroutineContext context) {
        if (context == kotlin.coroutines.experimental.EmptyCoroutineContext.INSTANCE) {
            return $receiver;
        }
        return (kotlin.coroutines.experimental.CoroutineContext) context.fold($receiver, kotlin.coroutines.experimental.CoroutineContextImplKt$plusImpl$1.INSTANCE);
    }
}
