package kotlin.coroutines.experimental;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\b\u0005"}, d2 = {"<anonymous>", "", "acc", "element", "Lkotlin/coroutines/experimental/CoroutineContext$Element;", "invoke"}, k = 3, mv = {1, 1, 6})
/* compiled from: CoroutineContextImpl.kt */
final class CombinedContext$toString$1 extends kotlin.jvm.internal.Lambda implements kotlin.jvm.functions.Function2<java.lang.String, kotlin.coroutines.experimental.CoroutineContext.Element, java.lang.String> {
    public static final kotlin.coroutines.experimental.CombinedContext$toString$1 INSTANCE = new kotlin.coroutines.experimental.CombinedContext$toString$1();

    CombinedContext$toString$1() {
        super(2);
    }

    @org.jetbrains.annotations.NotNull
    public final java.lang.String invoke(@org.jetbrains.annotations.NotNull java.lang.String acc, @org.jetbrains.annotations.NotNull kotlin.coroutines.experimental.CoroutineContext.Element element) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(acc, "acc");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(element, "element");
        return acc.length() == 0 ? element.toString() : acc + ", " + element;
    }
}
