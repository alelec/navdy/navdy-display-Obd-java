package kotlin.coroutines.experimental;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0000\b\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0001\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0010\u0010\n\u001a\u00020\u000b2\u0006\u0010\u0003\u001a\u00020\u0004H\u0002J\u0010\u0010\f\u001a\u00020\u000b2\u0006\u0010\r\u001a\u00020\u0000H\u0002J\u0013\u0010\u000e\u001a\u00020\u000b2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u0096\u0002J5\u0010\u0011\u001a\u0002H\u0012\"\u0004\b\u0000\u0010\u00122\u0006\u0010\u0013\u001a\u0002H\u00122\u0018\u0010\u0014\u001a\u0014\u0012\u0004\u0012\u0002H\u0012\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u0002H\u00120\u0015H\u0016\u00a2\u0006\u0002\u0010\u0016J(\u0010\u0017\u001a\u0004\u0018\u0001H\u0018\"\b\b\u0000\u0010\u0018*\u00020\u00042\f\u0010\u0019\u001a\b\u0012\u0004\u0012\u0002H\u00180\u001aH\u0096\u0002\u00a2\u0006\u0002\u0010\u001bJ\b\u0010\u001c\u001a\u00020\u001dH\u0016J\u0014\u0010\u001e\u001a\u00020\u00012\n\u0010\u0019\u001a\u0006\u0012\u0002\b\u00030\u001aH\u0016J\u0011\u0010\u001f\u001a\u00020\u00012\u0006\u0010\r\u001a\u00020\u0001H\u0096\u0002J\b\u0010 \u001a\u00020\u001dH\u0002J\b\u0010!\u001a\u00020\"H\u0016R\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007R\u0011\u0010\u0002\u001a\u00020\u0001\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\t\u00a8\u0006#"}, d2 = {"Lkotlin/coroutines/experimental/CombinedContext;", "Lkotlin/coroutines/experimental/CoroutineContext;", "left", "element", "Lkotlin/coroutines/experimental/CoroutineContext$Element;", "(Lkotlin/coroutines/experimental/CoroutineContext;Lkotlin/coroutines/experimental/CoroutineContext$Element;)V", "getElement", "()Lkotlin/coroutines/experimental/CoroutineContext$Element;", "getLeft", "()Lkotlin/coroutines/experimental/CoroutineContext;", "contains", "", "containsAll", "context", "equals", "other", "", "fold", "R", "initial", "operation", "Lkotlin/Function2;", "(Ljava/lang/Object;Lkotlin/jvm/functions/Function2;)Ljava/lang/Object;", "get", "E", "key", "Lkotlin/coroutines/experimental/CoroutineContext$Key;", "(Lkotlin/coroutines/experimental/CoroutineContext$Key;)Lkotlin/coroutines/experimental/CoroutineContext$Element;", "hashCode", "", "minusKey", "plus", "size", "toString", "", "kotlin-stdlib"}, k = 1, mv = {1, 1, 6})
/* compiled from: CoroutineContextImpl.kt */
final class CombinedContext implements kotlin.coroutines.experimental.CoroutineContext {
    @org.jetbrains.annotations.NotNull
    private final kotlin.coroutines.experimental.CoroutineContext.Element element;
    @org.jetbrains.annotations.NotNull
    private final kotlin.coroutines.experimental.CoroutineContext left;

    public CombinedContext(@org.jetbrains.annotations.NotNull kotlin.coroutines.experimental.CoroutineContext left2, @org.jetbrains.annotations.NotNull kotlin.coroutines.experimental.CoroutineContext.Element element2) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(left2, "left");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(element2, "element");
        this.left = left2;
        this.element = element2;
    }

    @org.jetbrains.annotations.NotNull
    public final kotlin.coroutines.experimental.CoroutineContext.Element getElement() {
        return this.element;
    }

    @org.jetbrains.annotations.NotNull
    public final kotlin.coroutines.experimental.CoroutineContext getLeft() {
        return this.left;
    }

    @org.jetbrains.annotations.Nullable
    public <E extends kotlin.coroutines.experimental.CoroutineContext.Element> E get(@org.jetbrains.annotations.NotNull kotlin.coroutines.experimental.CoroutineContext.Key<E> key) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(key, ch.qos.logback.core.joran.action.Action.KEY_ATTRIBUTE);
        kotlin.coroutines.experimental.CoroutineContext coroutineContext = this;
        while (true) {
            kotlin.coroutines.experimental.CombinedContext cur = (kotlin.coroutines.experimental.CombinedContext) coroutineContext;
            E e = cur.element.get(key);
            if (e != null) {
                return e;
            }
            kotlin.coroutines.experimental.CoroutineContext next = cur.left;
            if (!(next instanceof kotlin.coroutines.experimental.CombinedContext)) {
                return next.get(key);
            }
            coroutineContext = next;
        }
    }

    public <R> R fold(R initial, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function2<? super R, ? super kotlin.coroutines.experimental.CoroutineContext.Element, ? extends R> operation) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(operation, "operation");
        return operation.invoke(this.left.fold(initial, operation), this.element);
    }

    @org.jetbrains.annotations.NotNull
    public kotlin.coroutines.experimental.CoroutineContext plus(@org.jetbrains.annotations.NotNull kotlin.coroutines.experimental.CoroutineContext context) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(context, ch.qos.logback.core.CoreConstants.CONTEXT_SCOPE_VALUE);
        return kotlin.coroutines.experimental.CoroutineContextImplKt.plusImpl(this, context);
    }

    /* Debug info: failed to restart local var, previous not found, register: 4 */
    @org.jetbrains.annotations.NotNull
    public kotlin.coroutines.experimental.CoroutineContext minusKey(@org.jetbrains.annotations.NotNull kotlin.coroutines.experimental.CoroutineContext.Key<?> key) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(key, ch.qos.logback.core.joran.action.Action.KEY_ATTRIBUTE);
        if (this.element.get(key) != null) {
            return this.left;
        }
        kotlin.coroutines.experimental.CoroutineContext newLeft = this.left.minusKey(key);
        if (newLeft == this.left) {
            return this;
        }
        if (newLeft == kotlin.coroutines.experimental.EmptyCoroutineContext.INSTANCE) {
            return this.element;
        }
        return new kotlin.coroutines.experimental.CombinedContext(newLeft, this.element);
    }

    private final int size() {
        if (this.left instanceof kotlin.coroutines.experimental.CombinedContext) {
            return ((kotlin.coroutines.experimental.CombinedContext) this.left).size() + 1;
        }
        return 2;
    }

    private final boolean contains(kotlin.coroutines.experimental.CoroutineContext.Element element2) {
        return kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) get(element2.getKey()), (java.lang.Object) element2);
    }

    private final boolean containsAll(kotlin.coroutines.experimental.CombinedContext context) {
        kotlin.coroutines.experimental.CombinedContext cur = context;
        while (contains(cur.element)) {
            kotlin.coroutines.experimental.CoroutineContext next = cur.left;
            if (next instanceof kotlin.coroutines.experimental.CombinedContext) {
                cur = (kotlin.coroutines.experimental.CombinedContext) next;
            } else if (next != null) {
                return contains((kotlin.coroutines.experimental.CoroutineContext.Element) next);
            } else {
                throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.coroutines.experimental.CoroutineContext.Element");
            }
        }
        return false;
    }

    public boolean equals(@org.jetbrains.annotations.Nullable java.lang.Object other) {
        return this == other || ((other instanceof kotlin.coroutines.experimental.CombinedContext) && ((kotlin.coroutines.experimental.CombinedContext) other).size() == size() && ((kotlin.coroutines.experimental.CombinedContext) other).containsAll(this));
    }

    public int hashCode() {
        return this.left.hashCode() + this.element.hashCode();
    }

    @org.jetbrains.annotations.NotNull
    public java.lang.String toString() {
        return "[" + ((java.lang.String) fold("", kotlin.coroutines.experimental.CombinedContext$toString$1.INSTANCE)) + "]";
    }
}
