package kotlin.coroutines.experimental;

@kotlin.SinceKotlin(version = "1.1")
@kotlin.coroutines.experimental.RestrictsSuspension
@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\u001c\n\u0002\b\u0002\n\u0002\u0010(\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b'\u0018\u0000*\u0006\b\u0000\u0010\u0001 \u00002\u00020\u0002B\u0007\b\u0000\u00a2\u0006\u0002\u0010\u0003J\u0019\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00028\u0000H\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0007J\u001f\u0010\b\u001a\u00020\u00052\f\u0010\t\u001a\b\u0012\u0004\u0012\u00028\u00000\nH\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u000bJ\u001f\u0010\b\u001a\u00020\u00052\f\u0010\f\u001a\b\u0012\u0004\u0012\u00028\u00000\rH\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u000eJ\u001f\u0010\b\u001a\u00020\u00052\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00028\u00000\u0010H\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0011\u0082\u0002\u0004\n\u0002\b\t\u00a8\u0006\u0012"}, d2 = {"Lkotlin/coroutines/experimental/SequenceBuilder;", "T", "", "()V", "yield", "", "value", "(Ljava/lang/Object;Lkotlin/coroutines/experimental/Continuation;)Ljava/lang/Object;", "yieldAll", "elements", "", "(Ljava/lang/Iterable;Lkotlin/coroutines/experimental/Continuation;)Ljava/lang/Object;", "iterator", "", "(Ljava/util/Iterator;Lkotlin/coroutines/experimental/Continuation;)Ljava/lang/Object;", "sequence", "Lkotlin/sequences/Sequence;", "(Lkotlin/sequences/Sequence;Lkotlin/coroutines/experimental/Continuation;)Ljava/lang/Object;", "kotlin-stdlib"}, k = 1, mv = {1, 1, 6})
/* compiled from: SequenceBuilder.kt */
public abstract class SequenceBuilder<T> {
    @org.jetbrains.annotations.Nullable
    public abstract java.lang.Object yield(T t, @org.jetbrains.annotations.NotNull kotlin.coroutines.experimental.Continuation<? super kotlin.Unit> continuation);

    @org.jetbrains.annotations.Nullable
    public abstract java.lang.Object yieldAll(@org.jetbrains.annotations.NotNull java.util.Iterator<? extends T> it, @org.jetbrains.annotations.NotNull kotlin.coroutines.experimental.Continuation<? super kotlin.Unit> continuation);

    @org.jetbrains.annotations.Nullable
    public final java.lang.Object yieldAll(@org.jetbrains.annotations.NotNull java.lang.Iterable<? extends T> elements, @org.jetbrains.annotations.NotNull kotlin.coroutines.experimental.Continuation<? super kotlin.Unit> $continuation) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(elements, "elements");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($continuation, "$continuation");
        if (!(elements instanceof java.util.Collection) || !((java.util.Collection) elements).isEmpty()) {
            return yieldAll(elements.iterator(), $continuation);
        }
        return kotlin.Unit.INSTANCE;
    }

    @org.jetbrains.annotations.Nullable
    public final java.lang.Object yieldAll(@org.jetbrains.annotations.NotNull kotlin.sequences.Sequence<? extends T> sequence, @org.jetbrains.annotations.NotNull kotlin.coroutines.experimental.Continuation<? super kotlin.Unit> $continuation) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(sequence, "sequence");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($continuation, "$continuation");
        return yieldAll(sequence.iterator(), $continuation);
    }
}
