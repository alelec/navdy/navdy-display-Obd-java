package kotlin.coroutines.experimental;

@kotlin.SinceKotlin(version = "1.1")
@java.lang.annotation.Target({java.lang.annotation.ElementType.TYPE})
@kotlin.annotation.Target(allowedTargets = {kotlin.annotation.AnnotationTarget.CLASS})
@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\n\n\u0002\u0018\u0002\n\u0002\u0010\u001b\n\u0000\b\u0087\u0002\u0018\u00002\u00020\u0001B\u0000\u00a8\u0006\u0002"}, d2 = {"Lkotlin/coroutines/experimental/RestrictsSuspension;", "", "kotlin-stdlib"}, k = 1, mv = {1, 1, 6})
@kotlin.annotation.Retention(kotlin.annotation.AnnotationRetention.BINARY)
@java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.CLASS)
/* compiled from: Coroutines.kt */
public @interface RestrictsSuspension {
}
