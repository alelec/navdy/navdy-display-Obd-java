package kotlin.coroutines.experimental;

@kotlin.PublishedApi
@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\u0003\n\u0002\b\u0003\b\u0001\u0018\u0000 \u0015*\u0006\b\u0000\u0010\u0001 \u00002\b\u0012\u0004\u0012\u0002H\u00010\u0002:\u0002\u0015\u0016B\u0015\b\u0011\u0012\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00028\u00000\u0002\u00a2\u0006\u0002\u0010\u0004B\u001f\b\u0000\u0012\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00028\u00000\u0002\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0007J\n\u0010\r\u001a\u0004\u0018\u00010\u0006H\u0001J\u0015\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00028\u0000H\u0016\u00a2\u0006\u0002\u0010\u0011J\u0010\u0010\u0012\u001a\u00020\u000f2\u0006\u0010\u0013\u001a\u00020\u0014H\u0016R\u0014\u0010\b\u001a\u00020\t8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\b\n\u0010\u000bR\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00028\u00000\u0002X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\f\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"}, d2 = {"Lkotlin/coroutines/experimental/SafeContinuation;", "T", "Lkotlin/coroutines/experimental/Continuation;", "delegate", "(Lkotlin/coroutines/experimental/Continuation;)V", "initialResult", "", "(Lkotlin/coroutines/experimental/Continuation;Ljava/lang/Object;)V", "context", "Lkotlin/coroutines/experimental/CoroutineContext;", "getContext", "()Lkotlin/coroutines/experimental/CoroutineContext;", "result", "getResult", "resume", "", "value", "(Ljava/lang/Object;)V", "resumeWithException", "exception", "", "Companion", "Fail", "kotlin-stdlib"}, k = 1, mv = {1, 1, 6})
/* compiled from: SafeContinuationJvm.kt */
public final class SafeContinuation<T> implements kotlin.coroutines.experimental.Continuation<T> {
    public static final kotlin.coroutines.experimental.SafeContinuation.Companion Companion = new kotlin.coroutines.experimental.SafeContinuation.Companion(null);
    /* access modifiers changed from: private */
    public static final java.util.concurrent.atomic.AtomicReferenceFieldUpdater<kotlin.coroutines.experimental.SafeContinuation<?>, java.lang.Object> RESULT = java.util.concurrent.atomic.AtomicReferenceFieldUpdater.newUpdater(kotlin.coroutines.experimental.SafeContinuation.class, java.lang.Object.class, "result");
    /* access modifiers changed from: private */
    public static final java.lang.Object RESUMED = new java.lang.Object();
    /* access modifiers changed from: private */
    public static final java.lang.Object UNDECIDED = new java.lang.Object();
    private final kotlin.coroutines.experimental.Continuation<T> delegate;
    private volatile java.lang.Object result;

    @kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\n\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R`\u0010\u0003\u001aF\u0012\u0014\u0012\u0012\u0012\u0002\b\u0003 \u0006*\b\u0012\u0002\b\u0003\u0018\u00010\u00050\u0005\u0012\u0006\u0012\u0004\u0018\u00010\u0001 \u0006*\"\u0012\u0014\u0012\u0012\u0012\u0002\b\u0003 \u0006*\b\u0012\u0002\b\u0003\u0018\u00010\u00050\u0005\u0012\u0006\u0012\u0004\u0018\u00010\u0001\u0018\u00010\u00040\u00048\u0002X\u0083\u0004\u00a2\u0006\u000e\n\u0000\u0012\u0004\b\u0007\u0010\u0002\u001a\u0004\b\b\u0010\tR\u0016\u0010\n\u001a\u0004\u0018\u00010\u0001X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0016\u0010\r\u001a\u0004\u0018\u00010\u0001X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\f\u00a8\u0006\u000f"}, d2 = {"Lkotlin/coroutines/experimental/SafeContinuation$Companion;", "", "()V", "RESULT", "Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;", "Lkotlin/coroutines/experimental/SafeContinuation;", "kotlin.jvm.PlatformType", "RESULT$annotations", "getRESULT", "()Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;", "RESUMED", "getRESUMED", "()Ljava/lang/Object;", "UNDECIDED", "getUNDECIDED", "kotlin-stdlib"}, k = 1, mv = {1, 1, 6})
    /* compiled from: SafeContinuationJvm.kt */
    public static final class Companion {
        @kotlin.jvm.JvmStatic
        private static /* synthetic */ void RESULT$annotations() {
        }

        private Companion() {
        }

        public /* synthetic */ Companion(kotlin.jvm.internal.DefaultConstructorMarker $constructor_marker) {
            this();
        }

        /* access modifiers changed from: private */
        public final java.lang.Object getUNDECIDED() {
            return kotlin.coroutines.experimental.SafeContinuation.UNDECIDED;
        }

        /* access modifiers changed from: private */
        public final java.lang.Object getRESUMED() {
            return kotlin.coroutines.experimental.SafeContinuation.RESUMED;
        }

        /* access modifiers changed from: private */
        public final java.util.concurrent.atomic.AtomicReferenceFieldUpdater<kotlin.coroutines.experimental.SafeContinuation<?>, java.lang.Object> getRESULT() {
            return kotlin.coroutines.experimental.SafeContinuation.RESULT;
        }
    }

    @kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0003\n\u0002\b\u0004\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0007"}, d2 = {"Lkotlin/coroutines/experimental/SafeContinuation$Fail;", "", "exception", "", "(Ljava/lang/Throwable;)V", "getException", "()Ljava/lang/Throwable;", "kotlin-stdlib"}, k = 1, mv = {1, 1, 6})
    /* compiled from: SafeContinuationJvm.kt */
    private static final class Fail {
        @org.jetbrains.annotations.NotNull
        private final java.lang.Throwable exception;

        public Fail(@org.jetbrains.annotations.NotNull java.lang.Throwable exception2) {
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(exception2, "exception");
            this.exception = exception2;
        }

        @org.jetbrains.annotations.NotNull
        public final java.lang.Throwable getException() {
            return this.exception;
        }
    }

    private static final java.util.concurrent.atomic.AtomicReferenceFieldUpdater<kotlin.coroutines.experimental.SafeContinuation<?>, java.lang.Object> getRESULT() {
        return Companion.getRESULT();
    }

    public SafeContinuation(@org.jetbrains.annotations.NotNull kotlin.coroutines.experimental.Continuation<? super T> delegate2, @org.jetbrains.annotations.Nullable java.lang.Object initialResult) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(delegate2, "delegate");
        this.delegate = delegate2;
        this.result = initialResult;
    }

    @kotlin.PublishedApi
    public SafeContinuation(@org.jetbrains.annotations.NotNull kotlin.coroutines.experimental.Continuation<? super T> delegate2) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(delegate2, "delegate");
        this(delegate2, Companion.getUNDECIDED());
    }

    @org.jetbrains.annotations.NotNull
    public kotlin.coroutines.experimental.CoroutineContext getContext() {
        return this.delegate.getContext();
    }

    public void resume(T value) {
        while (true) {
            java.lang.Object result2 = this.result;
            if (result2 == Companion.getUNDECIDED()) {
                if (Companion.getRESULT().compareAndSet(this, Companion.getUNDECIDED(), value)) {
                    return;
                }
            } else if (result2 != kotlin.coroutines.experimental.intrinsics.IntrinsicsKt.getCOROUTINE_SUSPENDED()) {
                throw new java.lang.IllegalStateException("Already resumed");
            } else if (Companion.getRESULT().compareAndSet(this, kotlin.coroutines.experimental.intrinsics.IntrinsicsKt.getCOROUTINE_SUSPENDED(), Companion.getRESUMED())) {
                this.delegate.resume(value);
                return;
            }
        }
    }

    public void resumeWithException(@org.jetbrains.annotations.NotNull java.lang.Throwable exception) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(exception, "exception");
        while (true) {
            java.lang.Object result2 = this.result;
            if (result2 == Companion.getUNDECIDED()) {
                if (Companion.getRESULT().compareAndSet(this, Companion.getUNDECIDED(), new kotlin.coroutines.experimental.SafeContinuation.Fail(exception))) {
                    return;
                }
            } else if (result2 != kotlin.coroutines.experimental.intrinsics.IntrinsicsKt.getCOROUTINE_SUSPENDED()) {
                throw new java.lang.IllegalStateException("Already resumed");
            } else if (Companion.getRESULT().compareAndSet(this, kotlin.coroutines.experimental.intrinsics.IntrinsicsKt.getCOROUTINE_SUSPENDED(), Companion.getRESUMED())) {
                this.delegate.resumeWithException(exception);
                return;
            }
        }
    }

    @org.jetbrains.annotations.Nullable
    @kotlin.PublishedApi
    public final java.lang.Object getResult() {
        java.lang.Object result2 = this.result;
        if (result2 == Companion.getUNDECIDED()) {
            if (Companion.getRESULT().compareAndSet(this, Companion.getUNDECIDED(), kotlin.coroutines.experimental.intrinsics.IntrinsicsKt.getCOROUTINE_SUSPENDED())) {
                return kotlin.coroutines.experimental.intrinsics.IntrinsicsKt.getCOROUTINE_SUSPENDED();
            }
            result2 = this.result;
        }
        if (result2 == Companion.getRESUMED()) {
            return kotlin.coroutines.experimental.intrinsics.IntrinsicsKt.getCOROUTINE_SUSPENDED();
        }
        if (!(result2 instanceof kotlin.coroutines.experimental.SafeContinuation.Fail)) {
            return result2;
        }
        throw ((kotlin.coroutines.experimental.SafeContinuation.Fail) result2).getException();
    }
}
