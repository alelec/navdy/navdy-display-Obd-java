package kotlin.coroutines.experimental;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\b\u0005"}, d2 = {"<anonymous>", "Lkotlin/coroutines/experimental/CoroutineContext;", "acc", "element", "Lkotlin/coroutines/experimental/CoroutineContext$Element;", "invoke"}, k = 3, mv = {1, 1, 6})
/* compiled from: CoroutineContextImpl.kt */
final class CoroutineContextImplKt$plusImpl$1 extends kotlin.jvm.internal.Lambda implements kotlin.jvm.functions.Function2<kotlin.coroutines.experimental.CoroutineContext, kotlin.coroutines.experimental.CoroutineContext.Element, kotlin.coroutines.experimental.CoroutineContext> {
    public static final kotlin.coroutines.experimental.CoroutineContextImplKt$plusImpl$1 INSTANCE = new kotlin.coroutines.experimental.CoroutineContextImplKt$plusImpl$1();

    CoroutineContextImplKt$plusImpl$1() {
        super(2);
    }

    @org.jetbrains.annotations.NotNull
    public final kotlin.coroutines.experimental.CoroutineContext invoke(@org.jetbrains.annotations.NotNull kotlin.coroutines.experimental.CoroutineContext acc, @org.jetbrains.annotations.NotNull kotlin.coroutines.experimental.CoroutineContext.Element element) {
        kotlin.coroutines.experimental.CombinedContext combinedContext;
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(acc, "acc");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(element, "element");
        kotlin.coroutines.experimental.CoroutineContext removed = acc.minusKey(element.getKey());
        if (removed == kotlin.coroutines.experimental.EmptyCoroutineContext.INSTANCE) {
            return element;
        }
        kotlin.coroutines.experimental.ContinuationInterceptor interceptor = (kotlin.coroutines.experimental.ContinuationInterceptor) removed.get(kotlin.coroutines.experimental.ContinuationInterceptor.Key);
        if (interceptor == null) {
            combinedContext = new kotlin.coroutines.experimental.CombinedContext(removed, element);
        } else {
            kotlin.coroutines.experimental.CoroutineContext left = removed.minusKey(kotlin.coroutines.experimental.ContinuationInterceptor.Key);
            if (left == kotlin.coroutines.experimental.EmptyCoroutineContext.INSTANCE) {
                combinedContext = new kotlin.coroutines.experimental.CombinedContext(element, interceptor);
            } else {
                combinedContext = new kotlin.coroutines.experimental.CombinedContext(new kotlin.coroutines.experimental.CombinedContext(left, element), interceptor);
            }
        }
        return combinedContext;
    }
}
