package kotlin.coroutines.experimental;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010(\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0003\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u000e\b\u0002\u0018\u0000*\u0004\b\u0000\u0010\u00012\b\u0012\u0004\u0012\u0002H\u00010\u00022\b\u0012\u0004\u0012\u0002H\u00010\u00032\b\u0012\u0004\u0012\u00020\u00050\u0004B\u0005\u00a2\u0006\u0002\u0010\u0006J\b\u0010\u0016\u001a\u00020\u0017H\u0002J\t\u0010\u0018\u001a\u00020\u0019H\u0096\u0002J\u000e\u0010\u001a\u001a\u00028\u0000H\u0096\u0002\u00a2\u0006\u0002\u0010\u001bJ\r\u0010\u001c\u001a\u00028\u0000H\u0002\u00a2\u0006\u0002\u0010\u001bJ\u0015\u0010\u001d\u001a\u00020\u00052\u0006\u0010\u001e\u001a\u00020\u0005H\u0016\u00a2\u0006\u0002\u0010\u001fJ\u0010\u0010 \u001a\u00020\u00052\u0006\u0010!\u001a\u00020\u0017H\u0016J\u0019\u0010\"\u001a\u00020\u00052\u0006\u0010\u001e\u001a\u00028\u0000H\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010#J\u001f\u0010$\u001a\u00020\u00052\f\u0010%\u001a\b\u0012\u0004\u0012\u00028\u00000\u0003H\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010&R\u0014\u0010\u0007\u001a\u00020\b8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\b\t\u0010\nR\u0016\u0010\u000b\u001a\n\u0012\u0004\u0012\u00028\u0000\u0018\u00010\u0003X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\"\u0010\f\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R\u0012\u0010\u0011\u001a\u0004\u0018\u00018\u0000X\u0082\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0012R\u0012\u0010\u0013\u001a\u00060\u0014j\u0002`\u0015X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\t\u00a8\u0006'"}, d2 = {"Lkotlin/coroutines/experimental/SequenceBuilderIterator;", "T", "Lkotlin/coroutines/experimental/SequenceBuilder;", "", "Lkotlin/coroutines/experimental/Continuation;", "", "()V", "context", "Lkotlin/coroutines/experimental/CoroutineContext;", "getContext", "()Lkotlin/coroutines/experimental/CoroutineContext;", "nextIterator", "nextStep", "getNextStep", "()Lkotlin/coroutines/experimental/Continuation;", "setNextStep", "(Lkotlin/coroutines/experimental/Continuation;)V", "nextValue", "Ljava/lang/Object;", "state", "", "Lkotlin/coroutines/experimental/State;", "exceptionalState", "", "hasNext", "", "next", "()Ljava/lang/Object;", "nextNotReady", "resume", "value", "(Lkotlin/Unit;)V", "resumeWithException", "exception", "yield", "(Ljava/lang/Object;Lkotlin/coroutines/experimental/Continuation;)Ljava/lang/Object;", "yieldAll", "iterator", "(Ljava/util/Iterator;Lkotlin/coroutines/experimental/Continuation;)Ljava/lang/Object;", "kotlin-stdlib"}, k = 1, mv = {1, 1, 6})
/* compiled from: SequenceBuilder.kt */
final class SequenceBuilderIterator<T> extends kotlin.coroutines.experimental.SequenceBuilder<T> implements java.util.Iterator<T>, kotlin.coroutines.experimental.Continuation<kotlin.Unit>, kotlin.jvm.internal.markers.KMappedMarker {
    private java.util.Iterator<? extends T> nextIterator;
    @org.jetbrains.annotations.Nullable
    private kotlin.coroutines.experimental.Continuation<? super kotlin.Unit> nextStep;
    private T nextValue;
    private int state;

    public void remove() {
        throw new java.lang.UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @org.jetbrains.annotations.Nullable
    public final kotlin.coroutines.experimental.Continuation<kotlin.Unit> getNextStep() {
        return this.nextStep;
    }

    public final void setNextStep(@org.jetbrains.annotations.Nullable kotlin.coroutines.experimental.Continuation<? super kotlin.Unit> continuation) {
        this.nextStep = continuation;
    }

    public boolean hasNext() {
        while (true) {
            switch (this.state) {
                case 0:
                    break;
                case 1:
                    java.util.Iterator<? extends T> it = this.nextIterator;
                    if (it == null) {
                        kotlin.jvm.internal.Intrinsics.throwNpe();
                    }
                    if (!it.hasNext()) {
                        this.nextIterator = null;
                        break;
                    } else {
                        this.state = 2;
                        return true;
                    }
                case 2:
                case 3:
                    return true;
                case 4:
                    return false;
                default:
                    throw exceptionalState();
            }
            this.state = 5;
            kotlin.coroutines.experimental.Continuation step = this.nextStep;
            if (step == null) {
                kotlin.jvm.internal.Intrinsics.throwNpe();
            }
            this.nextStep = null;
            step.resume(kotlin.Unit.INSTANCE);
        }
    }

    public T next() {
        switch (this.state) {
            case 0:
            case 1:
                return nextNotReady();
            case 2:
                this.state = 1;
                java.util.Iterator<? extends T> it = this.nextIterator;
                if (it == null) {
                    kotlin.jvm.internal.Intrinsics.throwNpe();
                }
                return it.next();
            case 3:
                this.state = 0;
                T t = this.nextValue;
                this.nextValue = null;
                return t;
            default:
                throw exceptionalState();
        }
    }

    private final T nextNotReady() {
        if (hasNext()) {
            return next();
        }
        throw new java.util.NoSuchElementException();
    }

    private final java.lang.Throwable exceptionalState() {
        switch (this.state) {
            case 4:
                return new java.util.NoSuchElementException();
            case 5:
                return new java.lang.IllegalStateException("Iterator has failed.");
            default:
                return new java.lang.IllegalStateException("Unexpected state of the iterator: " + this.state);
        }
    }

    @org.jetbrains.annotations.Nullable
    public java.lang.Object yield(T value, @org.jetbrains.annotations.NotNull kotlin.coroutines.experimental.Continuation<? super kotlin.Unit> $continuation) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($continuation, "$continuation");
        this.nextValue = value;
        this.state = 3;
        kotlin.jvm.internal.Intrinsics.throwNpe();
        return null;
    }

    @org.jetbrains.annotations.Nullable
    public java.lang.Object yieldAll(@org.jetbrains.annotations.NotNull java.util.Iterator<? extends T> iterator, @org.jetbrains.annotations.NotNull kotlin.coroutines.experimental.Continuation<? super kotlin.Unit> $continuation) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(iterator, "iterator");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($continuation, "$continuation");
        if (!iterator.hasNext()) {
            return kotlin.Unit.INSTANCE;
        }
        this.nextIterator = iterator;
        this.state = 2;
        kotlin.jvm.internal.Intrinsics.throwNpe();
        return null;
    }

    public void resume(@org.jetbrains.annotations.NotNull kotlin.Unit value) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(value, "value");
        this.state = 4;
    }

    public void resumeWithException(@org.jetbrains.annotations.NotNull java.lang.Throwable exception) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(exception, "exception");
        throw exception;
    }

    @org.jetbrains.annotations.NotNull
    public kotlin.coroutines.experimental.CoroutineContext getContext() {
        return kotlin.coroutines.experimental.EmptyCoroutineContext.INSTANCE;
    }
}
