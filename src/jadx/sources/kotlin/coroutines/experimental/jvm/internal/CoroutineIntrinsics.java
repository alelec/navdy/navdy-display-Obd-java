package kotlin.coroutines.experimental.jvm.internal;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u001a*\u0010\u0000\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\b\u0000\u0010\u00022\u0006\u0010\u0003\u001a\u00020\u00042\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001H\u0000\u001a \u0010\u0006\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\b\u0000\u0010\u00022\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\u00a8\u0006\u0007"}, d2 = {"interceptContinuationIfNeeded", "Lkotlin/coroutines/experimental/Continuation;", "T", "context", "Lkotlin/coroutines/experimental/CoroutineContext;", "continuation", "normalizeContinuation", "kotlin-stdlib"}, k = 2, mv = {1, 1, 6})
@kotlin.jvm.JvmName(name = "CoroutineIntrinsics")
/* compiled from: CoroutineIntrinsics.kt */
public final class CoroutineIntrinsics {
    @org.jetbrains.annotations.NotNull
    public static final <T> kotlin.coroutines.experimental.Continuation<T> normalizeContinuation(@org.jetbrains.annotations.NotNull kotlin.coroutines.experimental.Continuation<? super T> continuation) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(continuation, "continuation");
        kotlin.coroutines.experimental.jvm.internal.CoroutineImpl coroutineImpl = (kotlin.coroutines.experimental.jvm.internal.CoroutineImpl) (!(continuation instanceof kotlin.coroutines.experimental.jvm.internal.CoroutineImpl) ? null : continuation);
        if (coroutineImpl == null) {
            return continuation;
        }
        kotlin.coroutines.experimental.Continuation facade = coroutineImpl.getFacade();
        return facade != null ? facade : continuation;
    }

    @org.jetbrains.annotations.NotNull
    public static final <T> kotlin.coroutines.experimental.Continuation<T> interceptContinuationIfNeeded(@org.jetbrains.annotations.NotNull kotlin.coroutines.experimental.CoroutineContext context, @org.jetbrains.annotations.NotNull kotlin.coroutines.experimental.Continuation<? super T> continuation) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(context, ch.qos.logback.core.CoreConstants.CONTEXT_SCOPE_VALUE);
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(continuation, "continuation");
        kotlin.coroutines.experimental.ContinuationInterceptor continuationInterceptor = (kotlin.coroutines.experimental.ContinuationInterceptor) context.get(kotlin.coroutines.experimental.ContinuationInterceptor.Key);
        if (continuationInterceptor == null) {
            return continuation;
        }
        kotlin.coroutines.experimental.Continuation interceptContinuation = continuationInterceptor.interceptContinuation(continuation);
        return interceptContinuation != null ? interceptContinuation : continuation;
    }
}
