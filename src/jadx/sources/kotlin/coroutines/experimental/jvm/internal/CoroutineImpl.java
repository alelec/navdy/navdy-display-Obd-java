package kotlin.coroutines.experimental.jvm.internal;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\u0003\n\u0002\b\u0003\b&\u0018\u00002\u00020\u00012\n\u0012\u0006\u0012\u0004\u0018\u00010\u00030\u0002B\u001f\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0010\u0010\u0006\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u0003\u0018\u00010\u0002\u00a2\u0006\u0002\u0010\u0007J$\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00130\u00022\b\u0010\u0014\u001a\u0004\u0018\u00010\u00032\n\u0010\u0006\u001a\u0006\u0012\u0002\b\u00030\u0002H\u0016J\u001a\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00130\u00022\n\u0010\u0006\u001a\u0006\u0012\u0002\b\u00030\u0002H\u0016J\u001e\u0010\u0015\u001a\u0004\u0018\u00010\u00032\b\u0010\u0016\u001a\u0004\u0018\u00010\u00032\b\u0010\u0017\u001a\u0004\u0018\u00010\u0018H$J\u0012\u0010\u0019\u001a\u00020\u00132\b\u0010\u0014\u001a\u0004\u0018\u00010\u0003H\u0016J\u0010\u0010\u001a\u001a\u00020\u00132\u0006\u0010\u0017\u001a\u00020\u0018H\u0016R\u0010\u0010\b\u001a\u0004\u0018\u00010\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0018\u0010\n\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u0003\u0018\u00010\u0002X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0006\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u0003\u0018\u00010\u00028\u0004@\u0004X\u0085\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u00020\t8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\b\f\u0010\rR\u0019\u0010\u000e\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00030\u00028F\u00a2\u0006\u0006\u001a\u0004\b\u000f\u0010\u0010R\u0012\u0010\u0011\u001a\u00020\u00058\u0004@\u0004X\u0085\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001b"}, d2 = {"Lkotlin/coroutines/experimental/jvm/internal/CoroutineImpl;", "Lkotlin/jvm/internal/Lambda;", "Lkotlin/coroutines/experimental/Continuation;", "", "arity", "", "completion", "(ILkotlin/coroutines/experimental/Continuation;)V", "_context", "Lkotlin/coroutines/experimental/CoroutineContext;", "_facade", "context", "getContext", "()Lkotlin/coroutines/experimental/CoroutineContext;", "facade", "getFacade", "()Lkotlin/coroutines/experimental/Continuation;", "label", "create", "", "value", "doResume", "data", "exception", "", "resume", "resumeWithException", "kotlin-stdlib"}, k = 1, mv = {1, 1, 6})
/* compiled from: CoroutineImpl.kt */
public abstract class CoroutineImpl extends kotlin.jvm.internal.Lambda implements kotlin.coroutines.experimental.Continuation<java.lang.Object> {
    private final kotlin.coroutines.experimental.CoroutineContext _context;
    private kotlin.coroutines.experimental.Continuation<java.lang.Object> _facade;
    @org.jetbrains.annotations.Nullable
    @kotlin.jvm.JvmField
    protected kotlin.coroutines.experimental.Continuation<java.lang.Object> completion;
    @kotlin.jvm.JvmField
    protected int label;

    /* access modifiers changed from: protected */
    @org.jetbrains.annotations.Nullable
    public abstract java.lang.Object doResume(@org.jetbrains.annotations.Nullable java.lang.Object obj, @org.jetbrains.annotations.Nullable java.lang.Throwable th);

    public CoroutineImpl(int arity, @org.jetbrains.annotations.Nullable kotlin.coroutines.experimental.Continuation<java.lang.Object> completion2) {
        super(arity);
        this.completion = completion2;
        this.label = this.completion != null ? 0 : -1;
        kotlin.coroutines.experimental.Continuation<java.lang.Object> continuation = this.completion;
        this._context = continuation != null ? continuation.getContext() : null;
    }

    @org.jetbrains.annotations.NotNull
    public kotlin.coroutines.experimental.CoroutineContext getContext() {
        kotlin.coroutines.experimental.CoroutineContext coroutineContext = this._context;
        if (coroutineContext == null) {
            kotlin.jvm.internal.Intrinsics.throwNpe();
        }
        return coroutineContext;
    }

    @org.jetbrains.annotations.NotNull
    public final kotlin.coroutines.experimental.Continuation<java.lang.Object> getFacade() {
        if (this._facade == null) {
            kotlin.coroutines.experimental.CoroutineContext coroutineContext = this._context;
            if (coroutineContext == null) {
                kotlin.jvm.internal.Intrinsics.throwNpe();
            }
            this._facade = kotlin.coroutines.experimental.jvm.internal.CoroutineIntrinsics.interceptContinuationIfNeeded(coroutineContext, this);
        }
        kotlin.coroutines.experimental.Continuation<java.lang.Object> continuation = this._facade;
        if (continuation == null) {
            kotlin.jvm.internal.Intrinsics.throwNpe();
        }
        return continuation;
    }

    public void resume(@org.jetbrains.annotations.Nullable java.lang.Object value) {
        kotlin.coroutines.experimental.Continuation<java.lang.Object> continuation = this.completion;
        if (continuation == null) {
            kotlin.jvm.internal.Intrinsics.throwNpe();
        }
        try {
            java.lang.Object doResume = doResume(value, null);
            if (doResume == kotlin.coroutines.experimental.intrinsics.IntrinsicsKt.getCOROUTINE_SUSPENDED()) {
                return;
            }
            if (continuation == null) {
                throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.coroutines.experimental.Continuation<kotlin.Any?>");
            }
            continuation.resume(doResume);
        } catch (Throwable th) {
            continuation.resumeWithException(th);
        }
    }

    public void resumeWithException(@org.jetbrains.annotations.NotNull java.lang.Throwable exception) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(exception, "exception");
        kotlin.coroutines.experimental.Continuation<java.lang.Object> continuation = this.completion;
        if (continuation == null) {
            kotlin.jvm.internal.Intrinsics.throwNpe();
        }
        try {
            java.lang.Object doResume = doResume(null, exception);
            if (doResume == kotlin.coroutines.experimental.intrinsics.IntrinsicsKt.getCOROUTINE_SUSPENDED()) {
                return;
            }
            if (continuation == null) {
                throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.coroutines.experimental.Continuation<kotlin.Any?>");
            }
            continuation.resume(doResume);
        } catch (Throwable th) {
            continuation.resumeWithException(th);
        }
    }

    @org.jetbrains.annotations.NotNull
    public kotlin.coroutines.experimental.Continuation<kotlin.Unit> create(@org.jetbrains.annotations.NotNull kotlin.coroutines.experimental.Continuation<?> completion2) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(completion2, "completion");
        throw new java.lang.IllegalStateException("create(Continuation) has not been overridden");
    }

    @org.jetbrains.annotations.NotNull
    public kotlin.coroutines.experimental.Continuation<kotlin.Unit> create(@org.jetbrains.annotations.Nullable java.lang.Object value, @org.jetbrains.annotations.NotNull kotlin.coroutines.experimental.Continuation<?> completion2) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(completion2, "completion");
        throw new java.lang.IllegalStateException("create(Any?;Continuation) has not been overridden");
    }
}
