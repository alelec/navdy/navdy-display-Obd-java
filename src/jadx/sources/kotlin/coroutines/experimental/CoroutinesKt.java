package kotlin.coroutines.experimental;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000.\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u001a%\u0010\u0000\u001a\u00020\u00012\n\u0010\u0002\u001a\u0006\u0012\u0002\b\u00030\u00032\u000e\u0010\u0004\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00060\u0005H\u0081\b\u001a3\u0010\u0007\u001a\u0002H\b\"\u0004\b\u0000\u0010\b2\u001a\b\u0004\u0010\u0004\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\b0\u0003\u0012\u0004\u0012\u00020\u00010\tH\u0087H\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\n\u001aD\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00010\u0003\"\u0004\b\u0000\u0010\b*\u0018\b\u0001\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\b0\u0003\u0012\u0006\u0012\u0004\u0018\u00010\u00060\t2\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u0002H\b0\u0003H\u0007\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\f\u001a]\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00010\u0003\"\u0004\b\u0000\u0010\r\"\u0004\b\u0001\u0010\b*#\b\u0001\u0012\u0004\u0012\u0002H\r\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\b0\u0003\u0012\u0006\u0012\u0004\u0018\u00010\u00060\u000e\u00a2\u0006\u0002\b\u000f2\u0006\u0010\u0010\u001a\u0002H\r2\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u0002H\b0\u0003H\u0007\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0011\u001a>\u0010\u0012\u001a\u00020\u0001\"\u0004\b\u0000\u0010\b*\u0018\b\u0001\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\b0\u0003\u0012\u0006\u0012\u0004\u0018\u00010\u00060\t2\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u0002H\b0\u0003H\u0007\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0013\u001aW\u0010\u0012\u001a\u00020\u0001\"\u0004\b\u0000\u0010\r\"\u0004\b\u0001\u0010\b*#\b\u0001\u0012\u0004\u0012\u0002H\r\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\b0\u0003\u0012\u0006\u0012\u0004\u0018\u00010\u00060\u000e\u00a2\u0006\u0002\b\u000f2\u0006\u0010\u0010\u001a\u0002H\r2\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u0002H\b0\u0003H\u0007\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0014\u0082\u0002\u0004\n\u0002\b\t\u00a8\u0006\u0015"}, d2 = {"processBareContinuationResume", "", "completion", "Lkotlin/coroutines/experimental/Continuation;", "block", "Lkotlin/Function0;", "", "suspendCoroutine", "T", "Lkotlin/Function1;", "(Lkotlin/jvm/functions/Function1;Lkotlin/coroutines/experimental/Continuation;)Ljava/lang/Object;", "createCoroutine", "(Lkotlin/jvm/functions/Function1;Lkotlin/coroutines/experimental/Continuation;)Lkotlin/coroutines/experimental/Continuation;", "R", "Lkotlin/Function2;", "Lkotlin/ExtensionFunctionType;", "receiver", "(Lkotlin/jvm/functions/Function2;Ljava/lang/Object;Lkotlin/coroutines/experimental/Continuation;)Lkotlin/coroutines/experimental/Continuation;", "startCoroutine", "(Lkotlin/jvm/functions/Function1;Lkotlin/coroutines/experimental/Continuation;)V", "(Lkotlin/jvm/functions/Function2;Ljava/lang/Object;Lkotlin/coroutines/experimental/Continuation;)V", "kotlin-stdlib"}, k = 2, mv = {1, 1, 6})
@kotlin.jvm.JvmName(name = "CoroutinesKt")
/* compiled from: CoroutinesLibrary.kt */
public final class CoroutinesKt {
    @kotlin.SinceKotlin(version = "1.1")
    public static final <R, T> void startCoroutine(@org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function2<? super R, ? super kotlin.coroutines.experimental.Continuation<? super T>, ? extends java.lang.Object> $receiver, R receiver, @org.jetbrains.annotations.NotNull kotlin.coroutines.experimental.Continuation<? super T> completion) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(completion, "completion");
        kotlin.coroutines.experimental.intrinsics.IntrinsicsKt.createCoroutineUnchecked($receiver, receiver, completion).resume(kotlin.Unit.INSTANCE);
    }

    @kotlin.SinceKotlin(version = "1.1")
    public static final <T> void startCoroutine(@org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super kotlin.coroutines.experimental.Continuation<? super T>, ? extends java.lang.Object> $receiver, @org.jetbrains.annotations.NotNull kotlin.coroutines.experimental.Continuation<? super T> completion) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(completion, "completion");
        kotlin.coroutines.experimental.intrinsics.IntrinsicsKt.createCoroutineUnchecked($receiver, completion).resume(kotlin.Unit.INSTANCE);
    }

    @org.jetbrains.annotations.NotNull
    @kotlin.SinceKotlin(version = "1.1")
    public static final <R, T> kotlin.coroutines.experimental.Continuation<kotlin.Unit> createCoroutine(@org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function2<? super R, ? super kotlin.coroutines.experimental.Continuation<? super T>, ? extends java.lang.Object> $receiver, R receiver, @org.jetbrains.annotations.NotNull kotlin.coroutines.experimental.Continuation<? super T> completion) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(completion, "completion");
        return new kotlin.coroutines.experimental.SafeContinuation<>(kotlin.coroutines.experimental.intrinsics.IntrinsicsKt.createCoroutineUnchecked($receiver, receiver, completion), kotlin.coroutines.experimental.intrinsics.IntrinsicsKt.getCOROUTINE_SUSPENDED());
    }

    @org.jetbrains.annotations.NotNull
    @kotlin.SinceKotlin(version = "1.1")
    public static final <T> kotlin.coroutines.experimental.Continuation<kotlin.Unit> createCoroutine(@org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super kotlin.coroutines.experimental.Continuation<? super T>, ? extends java.lang.Object> $receiver, @org.jetbrains.annotations.NotNull kotlin.coroutines.experimental.Continuation<? super T> completion) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(completion, "completion");
        return new kotlin.coroutines.experimental.SafeContinuation<>(kotlin.coroutines.experimental.intrinsics.IntrinsicsKt.createCoroutineUnchecked($receiver, completion), kotlin.coroutines.experimental.intrinsics.IntrinsicsKt.getCOROUTINE_SUSPENDED());
    }

    @kotlin.SinceKotlin(version = "1.1")
    @org.jetbrains.annotations.Nullable
    public static final <T> java.lang.Object suspendCoroutine(@org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super kotlin.coroutines.experimental.Continuation<? super T>, kotlin.Unit> block, @org.jetbrains.annotations.NotNull kotlin.coroutines.experimental.Continuation<? super T> $continuation) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(block, "block");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($continuation, "$continuation");
        kotlin.jvm.internal.Intrinsics.throwNpe();
        return null;
    }

    @kotlin.internal.InlineOnly
    private static final void processBareContinuationResume(kotlin.coroutines.experimental.Continuation<?> completion, kotlin.jvm.functions.Function0<? extends java.lang.Object> block) {
        try {
            java.lang.Object result = block.invoke();
            if (result == kotlin.coroutines.experimental.intrinsics.IntrinsicsKt.getCOROUTINE_SUSPENDED()) {
                return;
            }
            if (completion == null) {
                throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.coroutines.experimental.Continuation<kotlin.Any?>");
            }
            completion.resume(result);
        } catch (Throwable t) {
            completion.resumeWithException(t);
        }
    }
}
