package kotlin.coroutines.experimental.intrinsics;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u001f\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u0003\n\u0000*\u0001\u0000\b\n\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0015\u0010\b\u001a\u00020\u00022\u0006\u0010\t\u001a\u00020\u0002H\u0016\u00a2\u0006\u0002\u0010\nJ\u0010\u0010\u000b\u001a\u00020\u00022\u0006\u0010\f\u001a\u00020\rH\u0016R\u0014\u0010\u0004\u001a\u00020\u00058VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0006\u0010\u0007\u00a8\u0006\u000e"}, d2 = {"kotlin/coroutines/experimental/intrinsics/IntrinsicsKt$buildContinuationByInvokeCall$continuation$1", "Lkotlin/coroutines/experimental/Continuation;", "", "(Lkotlin/coroutines/experimental/Continuation;Lkotlin/jvm/functions/Function0;)V", "context", "Lkotlin/coroutines/experimental/CoroutineContext;", "getContext", "()Lkotlin/coroutines/experimental/CoroutineContext;", "resume", "value", "(Lkotlin/Unit;)V", "resumeWithException", "exception", "", "kotlin-stdlib"}, k = 1, mv = {1, 1, 6})
/* compiled from: Intrinsics.kt */
public final class IntrinsicsKt$createCoroutineUnchecked$$inlined$buildContinuationByInvokeCall$1 implements kotlin.coroutines.experimental.Continuation<kotlin.Unit> {
    final /* synthetic */ kotlin.coroutines.experimental.Continuation $completion;
    final /* synthetic */ kotlin.coroutines.experimental.Continuation $completion$inlined;
    final /* synthetic */ kotlin.jvm.functions.Function1 receiver$0$inlined;

    public IntrinsicsKt$createCoroutineUnchecked$$inlined$buildContinuationByInvokeCall$1(kotlin.coroutines.experimental.Continuation $captured_local_variable$0, kotlin.jvm.functions.Function1 function1, kotlin.coroutines.experimental.Continuation continuation) {
        this.$completion = $captured_local_variable$0;
        this.receiver$0$inlined = function1;
        this.$completion$inlined = continuation;
    }

    public kotlin.coroutines.experimental.CoroutineContext getContext() {
        return this.$completion.getContext();
    }

    public void resume(kotlin.Unit value) {
        kotlin.coroutines.experimental.Continuation continuation = this.$completion;
        try {
            kotlin.jvm.functions.Function1 function1 = this.receiver$0$inlined;
            if (function1 == null) {
                throw new kotlin.TypeCastException("null cannot be cast to non-null type (kotlin.coroutines.experimental.Continuation<T>) -> kotlin.Any?");
            }
            java.lang.Object invoke = ((kotlin.jvm.functions.Function1) kotlin.jvm.internal.TypeIntrinsics.beforeCheckcastToFunctionOfArity(function1, 1)).invoke(this.$completion$inlined);
            if (invoke == kotlin.coroutines.experimental.intrinsics.IntrinsicsKt.getCOROUTINE_SUSPENDED()) {
                return;
            }
            if (continuation == null) {
                throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.coroutines.experimental.Continuation<kotlin.Any?>");
            }
            continuation.resume(invoke);
        } catch (Throwable th) {
            continuation.resumeWithException(th);
        }
    }

    public void resumeWithException(java.lang.Throwable exception) {
        this.$completion.resumeWithException(exception);
    }
}
