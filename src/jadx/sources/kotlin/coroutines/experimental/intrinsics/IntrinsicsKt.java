package kotlin.coroutines.experimental.intrinsics;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u00000\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u001a5\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007\"\u0004\b\u0000\u0010\t2\f\u0010\n\u001a\b\u0012\u0004\u0012\u0002H\t0\u00072\u0010\b\u0004\u0010\u000b\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00010\fH\u0083\b\u001a5\u0010\r\u001a\u0002H\t\"\u0004\b\u0000\u0010\t2\u001c\b\u0004\u0010\u000b\u001a\u0016\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\t0\u0007\u0012\u0006\u0012\u0004\u0018\u00010\u00010\u000eH\u0087H\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u000f\u001aD\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\b0\u0007\"\u0004\b\u0000\u0010\t*\u0018\b\u0001\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\t0\u0007\u0012\u0006\u0012\u0004\u0018\u00010\u00010\u000e2\f\u0010\n\u001a\b\u0012\u0004\u0012\u0002H\t0\u0007H\u0007\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0011\u001a]\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\b0\u0007\"\u0004\b\u0000\u0010\u0012\"\u0004\b\u0001\u0010\t*#\b\u0001\u0012\u0004\u0012\u0002H\u0012\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\t0\u0007\u0012\u0006\u0012\u0004\u0018\u00010\u00010\u0013\u00a2\u0006\u0002\b\u00142\u0006\u0010\u0015\u001a\u0002H\u00122\f\u0010\n\u001a\b\u0012\u0004\u0012\u0002H\t0\u0007H\u0007\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0016\"\u001c\u0010\u0000\u001a\u00020\u00018\u0006X\u0087\u0004\u00a2\u0006\u000e\n\u0000\u0012\u0004\b\u0002\u0010\u0003\u001a\u0004\b\u0004\u0010\u0005\u0082\u0002\u0004\n\u0002\b\t\u00a8\u0006\u0017"}, d2 = {"COROUTINE_SUSPENDED", "", "COROUTINE_SUSPENDED$annotations", "()V", "getCOROUTINE_SUSPENDED", "()Ljava/lang/Object;", "buildContinuationByInvokeCall", "Lkotlin/coroutines/experimental/Continuation;", "", "T", "completion", "block", "Lkotlin/Function0;", "suspendCoroutineOrReturn", "Lkotlin/Function1;", "(Lkotlin/jvm/functions/Function1;Lkotlin/coroutines/experimental/Continuation;)Ljava/lang/Object;", "createCoroutineUnchecked", "(Lkotlin/jvm/functions/Function1;Lkotlin/coroutines/experimental/Continuation;)Lkotlin/coroutines/experimental/Continuation;", "R", "Lkotlin/Function2;", "Lkotlin/ExtensionFunctionType;", "receiver", "(Lkotlin/jvm/functions/Function2;Ljava/lang/Object;Lkotlin/coroutines/experimental/Continuation;)Lkotlin/coroutines/experimental/Continuation;", "kotlin-stdlib"}, k = 2, mv = {1, 1, 6})
@kotlin.jvm.JvmName(name = "IntrinsicsKt")
/* compiled from: Intrinsics.kt */
public final class IntrinsicsKt {
    @org.jetbrains.annotations.NotNull
    private static final java.lang.Object COROUTINE_SUSPENDED = new java.lang.Object();

    @kotlin.SinceKotlin(version = "1.1")
    public static /* synthetic */ void COROUTINE_SUSPENDED$annotations() {
    }

    @kotlin.SinceKotlin(version = "1.1")
    @kotlin.internal.InlineOnly
    private static final <T> java.lang.Object suspendCoroutineOrReturn(kotlin.jvm.functions.Function1<? super kotlin.coroutines.experimental.Continuation<? super T>, ? extends java.lang.Object> block, kotlin.coroutines.experimental.Continuation<? super T> $continuation) {
        kotlin.jvm.internal.Intrinsics.throwNpe();
        return null;
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.Object getCOROUTINE_SUSPENDED() {
        return COROUTINE_SUSPENDED;
    }

    @org.jetbrains.annotations.NotNull
    @kotlin.SinceKotlin(version = "1.1")
    public static final <T> kotlin.coroutines.experimental.Continuation<kotlin.Unit> createCoroutineUnchecked(@org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super kotlin.coroutines.experimental.Continuation<? super T>, ? extends java.lang.Object> $receiver, @org.jetbrains.annotations.NotNull kotlin.coroutines.experimental.Continuation<? super T> completion) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(completion, "completion");
        if (!($receiver instanceof kotlin.coroutines.experimental.jvm.internal.CoroutineImpl)) {
            return kotlin.coroutines.experimental.jvm.internal.CoroutineIntrinsics.interceptContinuationIfNeeded(completion.getContext(), new kotlin.coroutines.experimental.intrinsics.IntrinsicsKt$createCoroutineUnchecked$$inlined$buildContinuationByInvokeCall$1(completion, $receiver, completion));
        }
        kotlin.coroutines.experimental.Continuation create = ((kotlin.coroutines.experimental.jvm.internal.CoroutineImpl) $receiver).create(completion);
        if (create != null) {
            return ((kotlin.coroutines.experimental.jvm.internal.CoroutineImpl) create).getFacade();
        }
        throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.coroutines.experimental.jvm.internal.CoroutineImpl");
    }

    @org.jetbrains.annotations.NotNull
    @kotlin.SinceKotlin(version = "1.1")
    public static final <R, T> kotlin.coroutines.experimental.Continuation<kotlin.Unit> createCoroutineUnchecked(@org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function2<? super R, ? super kotlin.coroutines.experimental.Continuation<? super T>, ? extends java.lang.Object> $receiver, R receiver, @org.jetbrains.annotations.NotNull kotlin.coroutines.experimental.Continuation<? super T> completion) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(completion, "completion");
        if (!($receiver instanceof kotlin.coroutines.experimental.jvm.internal.CoroutineImpl)) {
            return kotlin.coroutines.experimental.jvm.internal.CoroutineIntrinsics.interceptContinuationIfNeeded(completion.getContext(), new kotlin.coroutines.experimental.intrinsics.IntrinsicsKt$createCoroutineUnchecked$$inlined$buildContinuationByInvokeCall$2(completion, $receiver, receiver, completion));
        }
        kotlin.coroutines.experimental.Continuation create = ((kotlin.coroutines.experimental.jvm.internal.CoroutineImpl) $receiver).create(receiver, completion);
        if (create != null) {
            return ((kotlin.coroutines.experimental.jvm.internal.CoroutineImpl) create).getFacade();
        }
        throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.coroutines.experimental.jvm.internal.CoroutineImpl");
    }

    private static final <T> kotlin.coroutines.experimental.Continuation<kotlin.Unit> buildContinuationByInvokeCall(kotlin.coroutines.experimental.Continuation<? super T> completion, kotlin.jvm.functions.Function0<? extends java.lang.Object> block) {
        return kotlin.coroutines.experimental.jvm.internal.CoroutineIntrinsics.interceptContinuationIfNeeded(completion.getContext(), new kotlin.coroutines.experimental.intrinsics.IntrinsicsKt$buildContinuationByInvokeCall$continuation$1(completion, block));
    }
}
