package kotlin.coroutines.experimental;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\bg\u0018\u0000 \u00062\u00020\u0001:\u0001\u0006J\"\u0010\u0002\u001a\b\u0012\u0004\u0012\u0002H\u00040\u0003\"\u0004\b\u0000\u0010\u00042\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u0002H\u00040\u0003H&\u00a8\u0006\u0007"}, d2 = {"Lkotlin/coroutines/experimental/ContinuationInterceptor;", "Lkotlin/coroutines/experimental/CoroutineContext$Element;", "interceptContinuation", "Lkotlin/coroutines/experimental/Continuation;", "T", "continuation", "Key", "kotlin-stdlib"}, k = 1, mv = {1, 1, 6})
@kotlin.SinceKotlin(version = "1.1")
/* compiled from: ContinuationInterceptor.kt */
public interface ContinuationInterceptor extends kotlin.coroutines.experimental.CoroutineContext.Element {
    public static final kotlin.coroutines.experimental.ContinuationInterceptor.Key Key = new kotlin.coroutines.experimental.ContinuationInterceptor.Key(null);

    @kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0003\u00a8\u0006\u0004"}, d2 = {"Lkotlin/coroutines/experimental/ContinuationInterceptor$Key;", "Lkotlin/coroutines/experimental/CoroutineContext$Key;", "Lkotlin/coroutines/experimental/ContinuationInterceptor;", "()V", "kotlin-stdlib"}, k = 1, mv = {1, 1, 6})
    /* compiled from: ContinuationInterceptor.kt */
    public static final class Key implements kotlin.coroutines.experimental.CoroutineContext.Key<kotlin.coroutines.experimental.ContinuationInterceptor> {
        private Key() {
        }

        public /* synthetic */ Key(kotlin.jvm.internal.DefaultConstructorMarker $constructor_marker) {
            this();
        }
    }

    @org.jetbrains.annotations.NotNull
    <T> kotlin.coroutines.experimental.Continuation<T> interceptContinuation(@org.jetbrains.annotations.NotNull kotlin.coroutines.experimental.Continuation<? super T> continuation);
}
