package kotlin;

@kotlin.annotation.MustBeDocumented
@kotlin.SinceKotlin(version = "1.1")
@kotlin.annotation.Target(allowedTargets = {kotlin.annotation.AnnotationTarget.TYPE})
@java.lang.annotation.Documented
@java.lang.annotation.Target({})
@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u001b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0087\u0002\u0018\u00002\u00020\u0001B\b\u0012\u0006\u0010\u0002\u001a\u00020\u0003R\t\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0000\u00a8\u0006\u0004"}, d2 = {"Lkotlin/ParameterName;", "", "name", "", "kotlin-runtime"}, k = 1, mv = {1, 1, 6})
@java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
/* compiled from: Annotations.kt */
public @interface ParameterName {
    java.lang.String name();
}
