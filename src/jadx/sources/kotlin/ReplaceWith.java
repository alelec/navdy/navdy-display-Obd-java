package kotlin;

@kotlin.annotation.MustBeDocumented
@kotlin.annotation.Target(allowedTargets = {})
@kotlin.annotation.Retention(kotlin.annotation.AnnotationRetention.BINARY)
@java.lang.annotation.Documented
@java.lang.annotation.Target({})
@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u001b\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0011\n\u0000\b\u0087\u0002\u0018\u00002\u00020\u0001B\u001c\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0012\u0010\u0004\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00030\u0005\"\u00020\u0003R\t\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0000R\u0011\u0010\u0004\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00030\u0005\u00a2\u0006\u0000\u00a8\u0006\u0006"}, d2 = {"Lkotlin/ReplaceWith;", "", "expression", "", "imports", "", "kotlin-runtime"}, k = 1, mv = {1, 1, 6})
@java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.CLASS)
/* compiled from: Annotations.kt */
public @interface ReplaceWith {
    java.lang.String expression();

    java.lang.String[] imports();
}
