package kotlin.ranges;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000b\n\u0002\b\u0002\n\u0002\u0010\u000f\n\u0002\b\u0002\n\u0002\u0010\u0005\n\u0002\u0010\u0006\n\u0002\u0010\u0007\n\u0002\u0010\b\n\u0002\u0010\t\n\u0002\u0010\n\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\f\n\u0002\b\u0015\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001a'\u0010\u0000\u001a\u0002H\u0001\"\u000e\b\u0000\u0010\u0001*\b\u0012\u0004\u0012\u0002H\u00010\u0002*\u0002H\u00012\u0006\u0010\u0003\u001a\u0002H\u0001\u00a2\u0006\u0002\u0010\u0004\u001a\u0012\u0010\u0000\u001a\u00020\u0005*\u00020\u00052\u0006\u0010\u0003\u001a\u00020\u0005\u001a\u0012\u0010\u0000\u001a\u00020\u0006*\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u0006\u001a\u0012\u0010\u0000\u001a\u00020\u0007*\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u0007\u001a\u0012\u0010\u0000\u001a\u00020\b*\u00020\b2\u0006\u0010\u0003\u001a\u00020\b\u001a\u0012\u0010\u0000\u001a\u00020\t*\u00020\t2\u0006\u0010\u0003\u001a\u00020\t\u001a\u0012\u0010\u0000\u001a\u00020\n*\u00020\n2\u0006\u0010\u0003\u001a\u00020\n\u001a'\u0010\u000b\u001a\u0002H\u0001\"\u000e\b\u0000\u0010\u0001*\b\u0012\u0004\u0012\u0002H\u00010\u0002*\u0002H\u00012\u0006\u0010\f\u001a\u0002H\u0001\u00a2\u0006\u0002\u0010\u0004\u001a\u0012\u0010\u000b\u001a\u00020\u0005*\u00020\u00052\u0006\u0010\f\u001a\u00020\u0005\u001a\u0012\u0010\u000b\u001a\u00020\u0006*\u00020\u00062\u0006\u0010\f\u001a\u00020\u0006\u001a\u0012\u0010\u000b\u001a\u00020\u0007*\u00020\u00072\u0006\u0010\f\u001a\u00020\u0007\u001a\u0012\u0010\u000b\u001a\u00020\b*\u00020\b2\u0006\u0010\f\u001a\u00020\b\u001a\u0012\u0010\u000b\u001a\u00020\t*\u00020\t2\u0006\u0010\f\u001a\u00020\t\u001a\u0012\u0010\u000b\u001a\u00020\n*\u00020\n2\u0006\u0010\f\u001a\u00020\n\u001a3\u0010\r\u001a\u0002H\u0001\"\u000e\b\u0000\u0010\u0001*\b\u0012\u0004\u0012\u0002H\u00010\u0002*\u0002H\u00012\b\u0010\u0003\u001a\u0004\u0018\u0001H\u00012\b\u0010\f\u001a\u0004\u0018\u0001H\u0001\u00a2\u0006\u0002\u0010\u000e\u001a/\u0010\r\u001a\u0002H\u0001\"\u000e\b\u0000\u0010\u0001*\b\u0012\u0004\u0012\u0002H\u00010\u0002*\u0002H\u00012\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u0002H\u00010\u0010H\u0007\u00a2\u0006\u0002\u0010\u0011\u001a-\u0010\r\u001a\u0002H\u0001\"\u000e\b\u0000\u0010\u0001*\b\u0012\u0004\u0012\u0002H\u00010\u0002*\u0002H\u00012\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u0002H\u00010\u0012\u00a2\u0006\u0002\u0010\u0013\u001a\u001a\u0010\r\u001a\u00020\u0005*\u00020\u00052\u0006\u0010\u0003\u001a\u00020\u00052\u0006\u0010\f\u001a\u00020\u0005\u001a\u001a\u0010\r\u001a\u00020\u0006*\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00062\u0006\u0010\f\u001a\u00020\u0006\u001a\u001a\u0010\r\u001a\u00020\u0007*\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u00072\u0006\u0010\f\u001a\u00020\u0007\u001a\u001a\u0010\r\u001a\u00020\b*\u00020\b2\u0006\u0010\u0003\u001a\u00020\b2\u0006\u0010\f\u001a\u00020\b\u001a\u0018\u0010\r\u001a\u00020\b*\u00020\b2\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\b0\u0012\u001a\u001a\u0010\r\u001a\u00020\t*\u00020\t2\u0006\u0010\u0003\u001a\u00020\t2\u0006\u0010\f\u001a\u00020\t\u001a\u0018\u0010\r\u001a\u00020\t*\u00020\t2\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\t0\u0012\u001a\u001a\u0010\r\u001a\u00020\n*\u00020\n2\u0006\u0010\u0003\u001a\u00020\n2\u0006\u0010\f\u001a\u00020\n\u001a \u0010\u0014\u001a\u00020\u0015*\b\u0012\u0004\u0012\u00020\u00050\u00122\u0006\u0010\u0016\u001a\u00020\u0006H\u0087\u0002\u00a2\u0006\u0002\b\u0017\u001a \u0010\u0014\u001a\u00020\u0015*\b\u0012\u0004\u0012\u00020\u00050\u00122\u0006\u0010\u0016\u001a\u00020\u0007H\u0087\u0002\u00a2\u0006\u0002\b\u0017\u001a \u0010\u0014\u001a\u00020\u0015*\b\u0012\u0004\u0012\u00020\u00050\u00122\u0006\u0010\u0016\u001a\u00020\bH\u0087\u0002\u00a2\u0006\u0002\b\u0017\u001a \u0010\u0014\u001a\u00020\u0015*\b\u0012\u0004\u0012\u00020\u00050\u00122\u0006\u0010\u0016\u001a\u00020\tH\u0087\u0002\u00a2\u0006\u0002\b\u0017\u001a \u0010\u0014\u001a\u00020\u0015*\b\u0012\u0004\u0012\u00020\u00050\u00122\u0006\u0010\u0016\u001a\u00020\nH\u0087\u0002\u00a2\u0006\u0002\b\u0017\u001a \u0010\u0014\u001a\u00020\u0015*\b\u0012\u0004\u0012\u00020\u00060\u00122\u0006\u0010\u0016\u001a\u00020\u0005H\u0087\u0002\u00a2\u0006\u0002\b\u0018\u001a \u0010\u0014\u001a\u00020\u0015*\b\u0012\u0004\u0012\u00020\u00060\u00122\u0006\u0010\u0016\u001a\u00020\u0007H\u0087\u0002\u00a2\u0006\u0002\b\u0018\u001a \u0010\u0014\u001a\u00020\u0015*\b\u0012\u0004\u0012\u00020\u00060\u00122\u0006\u0010\u0016\u001a\u00020\bH\u0087\u0002\u00a2\u0006\u0002\b\u0018\u001a \u0010\u0014\u001a\u00020\u0015*\b\u0012\u0004\u0012\u00020\u00060\u00122\u0006\u0010\u0016\u001a\u00020\tH\u0087\u0002\u00a2\u0006\u0002\b\u0018\u001a \u0010\u0014\u001a\u00020\u0015*\b\u0012\u0004\u0012\u00020\u00060\u00122\u0006\u0010\u0016\u001a\u00020\nH\u0087\u0002\u00a2\u0006\u0002\b\u0018\u001a \u0010\u0014\u001a\u00020\u0015*\b\u0012\u0004\u0012\u00020\u00070\u00122\u0006\u0010\u0016\u001a\u00020\u0005H\u0087\u0002\u00a2\u0006\u0002\b\u0019\u001a \u0010\u0014\u001a\u00020\u0015*\b\u0012\u0004\u0012\u00020\u00070\u00122\u0006\u0010\u0016\u001a\u00020\u0006H\u0087\u0002\u00a2\u0006\u0002\b\u0019\u001a \u0010\u0014\u001a\u00020\u0015*\b\u0012\u0004\u0012\u00020\u00070\u00122\u0006\u0010\u0016\u001a\u00020\bH\u0087\u0002\u00a2\u0006\u0002\b\u0019\u001a \u0010\u0014\u001a\u00020\u0015*\b\u0012\u0004\u0012\u00020\u00070\u00122\u0006\u0010\u0016\u001a\u00020\tH\u0087\u0002\u00a2\u0006\u0002\b\u0019\u001a \u0010\u0014\u001a\u00020\u0015*\b\u0012\u0004\u0012\u00020\u00070\u00122\u0006\u0010\u0016\u001a\u00020\nH\u0087\u0002\u00a2\u0006\u0002\b\u0019\u001a \u0010\u0014\u001a\u00020\u0015*\b\u0012\u0004\u0012\u00020\b0\u00122\u0006\u0010\u0016\u001a\u00020\u0005H\u0087\u0002\u00a2\u0006\u0002\b\u001a\u001a \u0010\u0014\u001a\u00020\u0015*\b\u0012\u0004\u0012\u00020\b0\u00122\u0006\u0010\u0016\u001a\u00020\u0006H\u0087\u0002\u00a2\u0006\u0002\b\u001a\u001a \u0010\u0014\u001a\u00020\u0015*\b\u0012\u0004\u0012\u00020\b0\u00122\u0006\u0010\u0016\u001a\u00020\u0007H\u0087\u0002\u00a2\u0006\u0002\b\u001a\u001a \u0010\u0014\u001a\u00020\u0015*\b\u0012\u0004\u0012\u00020\b0\u00122\u0006\u0010\u0016\u001a\u00020\tH\u0087\u0002\u00a2\u0006\u0002\b\u001a\u001a \u0010\u0014\u001a\u00020\u0015*\b\u0012\u0004\u0012\u00020\b0\u00122\u0006\u0010\u0016\u001a\u00020\nH\u0087\u0002\u00a2\u0006\u0002\b\u001a\u001a \u0010\u0014\u001a\u00020\u0015*\b\u0012\u0004\u0012\u00020\t0\u00122\u0006\u0010\u0016\u001a\u00020\u0005H\u0087\u0002\u00a2\u0006\u0002\b\u001b\u001a \u0010\u0014\u001a\u00020\u0015*\b\u0012\u0004\u0012\u00020\t0\u00122\u0006\u0010\u0016\u001a\u00020\u0006H\u0087\u0002\u00a2\u0006\u0002\b\u001b\u001a \u0010\u0014\u001a\u00020\u0015*\b\u0012\u0004\u0012\u00020\t0\u00122\u0006\u0010\u0016\u001a\u00020\u0007H\u0087\u0002\u00a2\u0006\u0002\b\u001b\u001a \u0010\u0014\u001a\u00020\u0015*\b\u0012\u0004\u0012\u00020\t0\u00122\u0006\u0010\u0016\u001a\u00020\bH\u0087\u0002\u00a2\u0006\u0002\b\u001b\u001a \u0010\u0014\u001a\u00020\u0015*\b\u0012\u0004\u0012\u00020\t0\u00122\u0006\u0010\u0016\u001a\u00020\nH\u0087\u0002\u00a2\u0006\u0002\b\u001b\u001a \u0010\u0014\u001a\u00020\u0015*\b\u0012\u0004\u0012\u00020\n0\u00122\u0006\u0010\u0016\u001a\u00020\u0005H\u0087\u0002\u00a2\u0006\u0002\b\u001c\u001a \u0010\u0014\u001a\u00020\u0015*\b\u0012\u0004\u0012\u00020\n0\u00122\u0006\u0010\u0016\u001a\u00020\u0006H\u0087\u0002\u00a2\u0006\u0002\b\u001c\u001a \u0010\u0014\u001a\u00020\u0015*\b\u0012\u0004\u0012\u00020\n0\u00122\u0006\u0010\u0016\u001a\u00020\u0007H\u0087\u0002\u00a2\u0006\u0002\b\u001c\u001a \u0010\u0014\u001a\u00020\u0015*\b\u0012\u0004\u0012\u00020\n0\u00122\u0006\u0010\u0016\u001a\u00020\bH\u0087\u0002\u00a2\u0006\u0002\b\u001c\u001a \u0010\u0014\u001a\u00020\u0015*\b\u0012\u0004\u0012\u00020\n0\u00122\u0006\u0010\u0016\u001a\u00020\tH\u0087\u0002\u00a2\u0006\u0002\b\u001c\u001a\u0015\u0010\u001d\u001a\u00020\u001e*\u00020\u00052\u0006\u0010\u001f\u001a\u00020\u0005H\u0086\u0004\u001a\u0015\u0010\u001d\u001a\u00020\u001e*\u00020\u00052\u0006\u0010\u001f\u001a\u00020\bH\u0086\u0004\u001a\u0015\u0010\u001d\u001a\u00020 *\u00020\u00052\u0006\u0010\u001f\u001a\u00020\tH\u0086\u0004\u001a\u0015\u0010\u001d\u001a\u00020\u001e*\u00020\u00052\u0006\u0010\u001f\u001a\u00020\nH\u0086\u0004\u001a\u0015\u0010\u001d\u001a\u00020!*\u00020\"2\u0006\u0010\u001f\u001a\u00020\"H\u0086\u0004\u001a\u0015\u0010\u001d\u001a\u00020\u001e*\u00020\b2\u0006\u0010\u001f\u001a\u00020\u0005H\u0086\u0004\u001a\u0015\u0010\u001d\u001a\u00020\u001e*\u00020\b2\u0006\u0010\u001f\u001a\u00020\bH\u0086\u0004\u001a\u0015\u0010\u001d\u001a\u00020 *\u00020\b2\u0006\u0010\u001f\u001a\u00020\tH\u0086\u0004\u001a\u0015\u0010\u001d\u001a\u00020\u001e*\u00020\b2\u0006\u0010\u001f\u001a\u00020\nH\u0086\u0004\u001a\u0015\u0010\u001d\u001a\u00020 *\u00020\t2\u0006\u0010\u001f\u001a\u00020\u0005H\u0086\u0004\u001a\u0015\u0010\u001d\u001a\u00020 *\u00020\t2\u0006\u0010\u001f\u001a\u00020\bH\u0086\u0004\u001a\u0015\u0010\u001d\u001a\u00020 *\u00020\t2\u0006\u0010\u001f\u001a\u00020\tH\u0086\u0004\u001a\u0015\u0010\u001d\u001a\u00020 *\u00020\t2\u0006\u0010\u001f\u001a\u00020\nH\u0086\u0004\u001a\u0015\u0010\u001d\u001a\u00020\u001e*\u00020\n2\u0006\u0010\u001f\u001a\u00020\u0005H\u0086\u0004\u001a\u0015\u0010\u001d\u001a\u00020\u001e*\u00020\n2\u0006\u0010\u001f\u001a\u00020\bH\u0086\u0004\u001a\u0015\u0010\u001d\u001a\u00020 *\u00020\n2\u0006\u0010\u001f\u001a\u00020\tH\u0086\u0004\u001a\u0015\u0010\u001d\u001a\u00020\u001e*\u00020\n2\u0006\u0010\u001f\u001a\u00020\nH\u0086\u0004\u001a\n\u0010#\u001a\u00020!*\u00020!\u001a\n\u0010#\u001a\u00020\u001e*\u00020\u001e\u001a\n\u0010#\u001a\u00020 *\u00020 \u001a\u0015\u0010$\u001a\u00020!*\u00020!2\u0006\u0010$\u001a\u00020\bH\u0086\u0004\u001a\u0015\u0010$\u001a\u00020\u001e*\u00020\u001e2\u0006\u0010$\u001a\u00020\bH\u0086\u0004\u001a\u0015\u0010$\u001a\u00020 *\u00020 2\u0006\u0010$\u001a\u00020\tH\u0086\u0004\u001a\u0013\u0010%\u001a\u0004\u0018\u00010\u0005*\u00020\u0006H\u0000\u00a2\u0006\u0002\u0010&\u001a\u0013\u0010%\u001a\u0004\u0018\u00010\u0005*\u00020\u0007H\u0000\u00a2\u0006\u0002\u0010'\u001a\u0013\u0010%\u001a\u0004\u0018\u00010\u0005*\u00020\bH\u0000\u00a2\u0006\u0002\u0010(\u001a\u0013\u0010%\u001a\u0004\u0018\u00010\u0005*\u00020\tH\u0000\u00a2\u0006\u0002\u0010)\u001a\u0013\u0010%\u001a\u0004\u0018\u00010\u0005*\u00020\nH\u0000\u00a2\u0006\u0002\u0010*\u001a\u0013\u0010+\u001a\u0004\u0018\u00010\b*\u00020\u0006H\u0000\u00a2\u0006\u0002\u0010,\u001a\u0013\u0010+\u001a\u0004\u0018\u00010\b*\u00020\u0007H\u0000\u00a2\u0006\u0002\u0010-\u001a\u0013\u0010+\u001a\u0004\u0018\u00010\b*\u00020\tH\u0000\u00a2\u0006\u0002\u0010.\u001a\u0013\u0010/\u001a\u0004\u0018\u00010\t*\u00020\u0006H\u0000\u00a2\u0006\u0002\u00100\u001a\u0013\u0010/\u001a\u0004\u0018\u00010\t*\u00020\u0007H\u0000\u00a2\u0006\u0002\u00101\u001a\u0013\u00102\u001a\u0004\u0018\u00010\n*\u00020\u0006H\u0000\u00a2\u0006\u0002\u00103\u001a\u0013\u00102\u001a\u0004\u0018\u00010\n*\u00020\u0007H\u0000\u00a2\u0006\u0002\u00104\u001a\u0013\u00102\u001a\u0004\u0018\u00010\n*\u00020\bH\u0000\u00a2\u0006\u0002\u00105\u001a\u0013\u00102\u001a\u0004\u0018\u00010\n*\u00020\tH\u0000\u00a2\u0006\u0002\u00106\u001a\u0015\u00107\u001a\u000208*\u00020\u00052\u0006\u0010\u001f\u001a\u00020\u0005H\u0086\u0004\u001a\u0015\u00107\u001a\u000208*\u00020\u00052\u0006\u0010\u001f\u001a\u00020\bH\u0086\u0004\u001a\u0015\u00107\u001a\u000209*\u00020\u00052\u0006\u0010\u001f\u001a\u00020\tH\u0086\u0004\u001a\u0015\u00107\u001a\u000208*\u00020\u00052\u0006\u0010\u001f\u001a\u00020\nH\u0086\u0004\u001a\u0015\u00107\u001a\u00020:*\u00020\"2\u0006\u0010\u001f\u001a\u00020\"H\u0086\u0004\u001a\u0015\u00107\u001a\u000208*\u00020\b2\u0006\u0010\u001f\u001a\u00020\u0005H\u0086\u0004\u001a\u0015\u00107\u001a\u000208*\u00020\b2\u0006\u0010\u001f\u001a\u00020\bH\u0086\u0004\u001a\u0015\u00107\u001a\u000209*\u00020\b2\u0006\u0010\u001f\u001a\u00020\tH\u0086\u0004\u001a\u0015\u00107\u001a\u000208*\u00020\b2\u0006\u0010\u001f\u001a\u00020\nH\u0086\u0004\u001a\u0015\u00107\u001a\u000209*\u00020\t2\u0006\u0010\u001f\u001a\u00020\u0005H\u0086\u0004\u001a\u0015\u00107\u001a\u000209*\u00020\t2\u0006\u0010\u001f\u001a\u00020\bH\u0086\u0004\u001a\u0015\u00107\u001a\u000209*\u00020\t2\u0006\u0010\u001f\u001a\u00020\tH\u0086\u0004\u001a\u0015\u00107\u001a\u000209*\u00020\t2\u0006\u0010\u001f\u001a\u00020\nH\u0086\u0004\u001a\u0015\u00107\u001a\u000208*\u00020\n2\u0006\u0010\u001f\u001a\u00020\u0005H\u0086\u0004\u001a\u0015\u00107\u001a\u000208*\u00020\n2\u0006\u0010\u001f\u001a\u00020\bH\u0086\u0004\u001a\u0015\u00107\u001a\u000209*\u00020\n2\u0006\u0010\u001f\u001a\u00020\tH\u0086\u0004\u001a\u0015\u00107\u001a\u000208*\u00020\n2\u0006\u0010\u001f\u001a\u00020\nH\u0086\u0004\u00a8\u0006;"}, d2 = {"coerceAtLeast", "T", "", "minimumValue", "(Ljava/lang/Comparable;Ljava/lang/Comparable;)Ljava/lang/Comparable;", "", "", "", "", "", "", "coerceAtMost", "maximumValue", "coerceIn", "(Ljava/lang/Comparable;Ljava/lang/Comparable;Ljava/lang/Comparable;)Ljava/lang/Comparable;", "range", "Lkotlin/ranges/ClosedFloatingPointRange;", "(Ljava/lang/Comparable;Lkotlin/ranges/ClosedFloatingPointRange;)Ljava/lang/Comparable;", "Lkotlin/ranges/ClosedRange;", "(Ljava/lang/Comparable;Lkotlin/ranges/ClosedRange;)Ljava/lang/Comparable;", "contains", "", "value", "byteRangeContains", "doubleRangeContains", "floatRangeContains", "intRangeContains", "longRangeContains", "shortRangeContains", "downTo", "Lkotlin/ranges/IntProgression;", "to", "Lkotlin/ranges/LongProgression;", "Lkotlin/ranges/CharProgression;", "", "reversed", "step", "toByteExactOrNull", "(D)Ljava/lang/Byte;", "(F)Ljava/lang/Byte;", "(I)Ljava/lang/Byte;", "(J)Ljava/lang/Byte;", "(S)Ljava/lang/Byte;", "toIntExactOrNull", "(D)Ljava/lang/Integer;", "(F)Ljava/lang/Integer;", "(J)Ljava/lang/Integer;", "toLongExactOrNull", "(D)Ljava/lang/Long;", "(F)Ljava/lang/Long;", "toShortExactOrNull", "(D)Ljava/lang/Short;", "(F)Ljava/lang/Short;", "(I)Ljava/lang/Short;", "(J)Ljava/lang/Short;", "until", "Lkotlin/ranges/IntRange;", "Lkotlin/ranges/LongRange;", "Lkotlin/ranges/CharRange;", "kotlin-stdlib"}, k = 5, mv = {1, 1, 6}, xi = 1, xs = "kotlin/ranges/RangesKt")
/* compiled from: _Ranges.kt */
class RangesKt___RangesKt extends kotlin.ranges.RangesKt__RangesKt {
    @kotlin.jvm.JvmName(name = "intRangeContains")
    public static final boolean intRangeContains(@org.jetbrains.annotations.NotNull kotlin.ranges.ClosedRange<java.lang.Integer> $receiver, byte value) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        return $receiver.contains(java.lang.Integer.valueOf(value));
    }

    @kotlin.jvm.JvmName(name = "longRangeContains")
    public static final boolean longRangeContains(@org.jetbrains.annotations.NotNull kotlin.ranges.ClosedRange<java.lang.Long> $receiver, byte value) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        return $receiver.contains(java.lang.Long.valueOf((long) value));
    }

    @kotlin.jvm.JvmName(name = "shortRangeContains")
    public static final boolean shortRangeContains(@org.jetbrains.annotations.NotNull kotlin.ranges.ClosedRange<java.lang.Short> $receiver, byte value) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        return $receiver.contains(java.lang.Short.valueOf((short) value));
    }

    @kotlin.jvm.JvmName(name = "doubleRangeContains")
    public static final boolean doubleRangeContains(@org.jetbrains.annotations.NotNull kotlin.ranges.ClosedRange<java.lang.Double> $receiver, byte value) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        return $receiver.contains(java.lang.Double.valueOf((double) value));
    }

    @kotlin.jvm.JvmName(name = "floatRangeContains")
    public static final boolean floatRangeContains(@org.jetbrains.annotations.NotNull kotlin.ranges.ClosedRange<java.lang.Float> $receiver, byte value) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        return $receiver.contains(java.lang.Float.valueOf((float) value));
    }

    @kotlin.jvm.JvmName(name = "intRangeContains")
    public static final boolean intRangeContains(@org.jetbrains.annotations.NotNull kotlin.ranges.ClosedRange<java.lang.Integer> $receiver, double value) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        java.lang.Integer it = kotlin.ranges.RangesKt.toIntExactOrNull(value);
        if (it != null) {
            return $receiver.contains(it);
        }
        return false;
    }

    @kotlin.jvm.JvmName(name = "longRangeContains")
    public static final boolean longRangeContains(@org.jetbrains.annotations.NotNull kotlin.ranges.ClosedRange<java.lang.Long> $receiver, double value) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        java.lang.Long it = kotlin.ranges.RangesKt.toLongExactOrNull(value);
        if (it != null) {
            return $receiver.contains(it);
        }
        return false;
    }

    @kotlin.jvm.JvmName(name = "byteRangeContains")
    public static final boolean byteRangeContains(@org.jetbrains.annotations.NotNull kotlin.ranges.ClosedRange<java.lang.Byte> $receiver, double value) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        java.lang.Byte it = kotlin.ranges.RangesKt.toByteExactOrNull(value);
        if (it != null) {
            return $receiver.contains(it);
        }
        return false;
    }

    @kotlin.jvm.JvmName(name = "shortRangeContains")
    public static final boolean shortRangeContains(@org.jetbrains.annotations.NotNull kotlin.ranges.ClosedRange<java.lang.Short> $receiver, double value) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        java.lang.Short it = kotlin.ranges.RangesKt.toShortExactOrNull(value);
        if (it != null) {
            return $receiver.contains(it);
        }
        return false;
    }

    @kotlin.jvm.JvmName(name = "floatRangeContains")
    public static final boolean floatRangeContains(@org.jetbrains.annotations.NotNull kotlin.ranges.ClosedRange<java.lang.Float> $receiver, double value) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        return $receiver.contains(java.lang.Float.valueOf((float) value));
    }

    @kotlin.jvm.JvmName(name = "intRangeContains")
    public static final boolean intRangeContains(@org.jetbrains.annotations.NotNull kotlin.ranges.ClosedRange<java.lang.Integer> $receiver, float value) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        java.lang.Integer it = kotlin.ranges.RangesKt.toIntExactOrNull(value);
        if (it != null) {
            return $receiver.contains(it);
        }
        return false;
    }

    @kotlin.jvm.JvmName(name = "longRangeContains")
    public static final boolean longRangeContains(@org.jetbrains.annotations.NotNull kotlin.ranges.ClosedRange<java.lang.Long> $receiver, float value) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        java.lang.Long it = kotlin.ranges.RangesKt.toLongExactOrNull(value);
        if (it != null) {
            return $receiver.contains(it);
        }
        return false;
    }

    @kotlin.jvm.JvmName(name = "byteRangeContains")
    public static final boolean byteRangeContains(@org.jetbrains.annotations.NotNull kotlin.ranges.ClosedRange<java.lang.Byte> $receiver, float value) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        java.lang.Byte it = kotlin.ranges.RangesKt.toByteExactOrNull(value);
        if (it != null) {
            return $receiver.contains(it);
        }
        return false;
    }

    @kotlin.jvm.JvmName(name = "shortRangeContains")
    public static final boolean shortRangeContains(@org.jetbrains.annotations.NotNull kotlin.ranges.ClosedRange<java.lang.Short> $receiver, float value) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        java.lang.Short it = kotlin.ranges.RangesKt.toShortExactOrNull(value);
        if (it != null) {
            return $receiver.contains(it);
        }
        return false;
    }

    @kotlin.jvm.JvmName(name = "doubleRangeContains")
    public static final boolean doubleRangeContains(@org.jetbrains.annotations.NotNull kotlin.ranges.ClosedRange<java.lang.Double> $receiver, float value) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        return $receiver.contains(java.lang.Double.valueOf((double) value));
    }

    @kotlin.jvm.JvmName(name = "longRangeContains")
    public static final boolean longRangeContains(@org.jetbrains.annotations.NotNull kotlin.ranges.ClosedRange<java.lang.Long> $receiver, int value) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        return $receiver.contains(java.lang.Long.valueOf((long) value));
    }

    @kotlin.jvm.JvmName(name = "byteRangeContains")
    public static final boolean byteRangeContains(@org.jetbrains.annotations.NotNull kotlin.ranges.ClosedRange<java.lang.Byte> $receiver, int value) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        java.lang.Byte it = kotlin.ranges.RangesKt.toByteExactOrNull(value);
        if (it != null) {
            return $receiver.contains(it);
        }
        return false;
    }

    @kotlin.jvm.JvmName(name = "shortRangeContains")
    public static final boolean shortRangeContains(@org.jetbrains.annotations.NotNull kotlin.ranges.ClosedRange<java.lang.Short> $receiver, int value) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        java.lang.Short it = kotlin.ranges.RangesKt.toShortExactOrNull(value);
        if (it != null) {
            return $receiver.contains(it);
        }
        return false;
    }

    @kotlin.jvm.JvmName(name = "doubleRangeContains")
    public static final boolean doubleRangeContains(@org.jetbrains.annotations.NotNull kotlin.ranges.ClosedRange<java.lang.Double> $receiver, int value) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        return $receiver.contains(java.lang.Double.valueOf((double) value));
    }

    @kotlin.jvm.JvmName(name = "floatRangeContains")
    public static final boolean floatRangeContains(@org.jetbrains.annotations.NotNull kotlin.ranges.ClosedRange<java.lang.Float> $receiver, int value) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        return $receiver.contains(java.lang.Float.valueOf((float) value));
    }

    @kotlin.jvm.JvmName(name = "intRangeContains")
    public static final boolean intRangeContains(@org.jetbrains.annotations.NotNull kotlin.ranges.ClosedRange<java.lang.Integer> $receiver, long value) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        java.lang.Integer it = kotlin.ranges.RangesKt.toIntExactOrNull(value);
        if (it != null) {
            return $receiver.contains(it);
        }
        return false;
    }

    @kotlin.jvm.JvmName(name = "byteRangeContains")
    public static final boolean byteRangeContains(@org.jetbrains.annotations.NotNull kotlin.ranges.ClosedRange<java.lang.Byte> $receiver, long value) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        java.lang.Byte it = kotlin.ranges.RangesKt.toByteExactOrNull(value);
        if (it != null) {
            return $receiver.contains(it);
        }
        return false;
    }

    @kotlin.jvm.JvmName(name = "shortRangeContains")
    public static final boolean shortRangeContains(@org.jetbrains.annotations.NotNull kotlin.ranges.ClosedRange<java.lang.Short> $receiver, long value) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        java.lang.Short it = kotlin.ranges.RangesKt.toShortExactOrNull(value);
        if (it != null) {
            return $receiver.contains(it);
        }
        return false;
    }

    @kotlin.jvm.JvmName(name = "doubleRangeContains")
    public static final boolean doubleRangeContains(@org.jetbrains.annotations.NotNull kotlin.ranges.ClosedRange<java.lang.Double> $receiver, long value) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        return $receiver.contains(java.lang.Double.valueOf((double) value));
    }

    @kotlin.jvm.JvmName(name = "floatRangeContains")
    public static final boolean floatRangeContains(@org.jetbrains.annotations.NotNull kotlin.ranges.ClosedRange<java.lang.Float> $receiver, long value) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        return $receiver.contains(java.lang.Float.valueOf((float) value));
    }

    @kotlin.jvm.JvmName(name = "intRangeContains")
    public static final boolean intRangeContains(@org.jetbrains.annotations.NotNull kotlin.ranges.ClosedRange<java.lang.Integer> $receiver, short value) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        return $receiver.contains(java.lang.Integer.valueOf(value));
    }

    @kotlin.jvm.JvmName(name = "longRangeContains")
    public static final boolean longRangeContains(@org.jetbrains.annotations.NotNull kotlin.ranges.ClosedRange<java.lang.Long> $receiver, short value) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        return $receiver.contains(java.lang.Long.valueOf((long) value));
    }

    @kotlin.jvm.JvmName(name = "byteRangeContains")
    public static final boolean byteRangeContains(@org.jetbrains.annotations.NotNull kotlin.ranges.ClosedRange<java.lang.Byte> $receiver, short value) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        java.lang.Byte it = kotlin.ranges.RangesKt.toByteExactOrNull(value);
        if (it != null) {
            return $receiver.contains(it);
        }
        return false;
    }

    @kotlin.jvm.JvmName(name = "doubleRangeContains")
    public static final boolean doubleRangeContains(@org.jetbrains.annotations.NotNull kotlin.ranges.ClosedRange<java.lang.Double> $receiver, short value) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        return $receiver.contains(java.lang.Double.valueOf((double) value));
    }

    @kotlin.jvm.JvmName(name = "floatRangeContains")
    public static final boolean floatRangeContains(@org.jetbrains.annotations.NotNull kotlin.ranges.ClosedRange<java.lang.Float> $receiver, short value) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        return $receiver.contains(java.lang.Float.valueOf((float) value));
    }

    @org.jetbrains.annotations.NotNull
    public static final kotlin.ranges.IntProgression downTo(int $receiver, byte to) {
        return kotlin.ranges.IntProgression.Companion.fromClosedRange($receiver, to, -1);
    }

    @org.jetbrains.annotations.NotNull
    public static final kotlin.ranges.LongProgression downTo(long $receiver, byte to) {
        return kotlin.ranges.LongProgression.Companion.fromClosedRange($receiver, (long) to, -1);
    }

    @org.jetbrains.annotations.NotNull
    public static final kotlin.ranges.IntProgression downTo(byte $receiver, byte to) {
        return kotlin.ranges.IntProgression.Companion.fromClosedRange($receiver, to, -1);
    }

    @org.jetbrains.annotations.NotNull
    public static final kotlin.ranges.IntProgression downTo(short $receiver, byte to) {
        return kotlin.ranges.IntProgression.Companion.fromClosedRange($receiver, to, -1);
    }

    @org.jetbrains.annotations.NotNull
    public static final kotlin.ranges.CharProgression downTo(char $receiver, char to) {
        return kotlin.ranges.CharProgression.Companion.fromClosedRange($receiver, to, -1);
    }

    @org.jetbrains.annotations.NotNull
    public static final kotlin.ranges.IntProgression downTo(int $receiver, int to) {
        return kotlin.ranges.IntProgression.Companion.fromClosedRange($receiver, to, -1);
    }

    @org.jetbrains.annotations.NotNull
    public static final kotlin.ranges.LongProgression downTo(long $receiver, int to) {
        return kotlin.ranges.LongProgression.Companion.fromClosedRange($receiver, (long) to, -1);
    }

    @org.jetbrains.annotations.NotNull
    public static final kotlin.ranges.IntProgression downTo(byte $receiver, int to) {
        return kotlin.ranges.IntProgression.Companion.fromClosedRange($receiver, to, -1);
    }

    @org.jetbrains.annotations.NotNull
    public static final kotlin.ranges.IntProgression downTo(short $receiver, int to) {
        return kotlin.ranges.IntProgression.Companion.fromClosedRange($receiver, to, -1);
    }

    @org.jetbrains.annotations.NotNull
    public static final kotlin.ranges.LongProgression downTo(int $receiver, long to) {
        return kotlin.ranges.LongProgression.Companion.fromClosedRange((long) $receiver, to, -1);
    }

    @org.jetbrains.annotations.NotNull
    public static final kotlin.ranges.LongProgression downTo(long $receiver, long to) {
        return kotlin.ranges.LongProgression.Companion.fromClosedRange($receiver, to, -1);
    }

    @org.jetbrains.annotations.NotNull
    public static final kotlin.ranges.LongProgression downTo(byte $receiver, long to) {
        return kotlin.ranges.LongProgression.Companion.fromClosedRange((long) $receiver, to, -1);
    }

    @org.jetbrains.annotations.NotNull
    public static final kotlin.ranges.LongProgression downTo(short $receiver, long to) {
        return kotlin.ranges.LongProgression.Companion.fromClosedRange((long) $receiver, to, -1);
    }

    @org.jetbrains.annotations.NotNull
    public static final kotlin.ranges.IntProgression downTo(int $receiver, short to) {
        return kotlin.ranges.IntProgression.Companion.fromClosedRange($receiver, to, -1);
    }

    @org.jetbrains.annotations.NotNull
    public static final kotlin.ranges.LongProgression downTo(long $receiver, short to) {
        return kotlin.ranges.LongProgression.Companion.fromClosedRange($receiver, (long) to, -1);
    }

    @org.jetbrains.annotations.NotNull
    public static final kotlin.ranges.IntProgression downTo(byte $receiver, short to) {
        return kotlin.ranges.IntProgression.Companion.fromClosedRange($receiver, to, -1);
    }

    @org.jetbrains.annotations.NotNull
    public static final kotlin.ranges.IntProgression downTo(short $receiver, short to) {
        return kotlin.ranges.IntProgression.Companion.fromClosedRange($receiver, to, -1);
    }

    @org.jetbrains.annotations.NotNull
    public static final kotlin.ranges.IntProgression reversed(@org.jetbrains.annotations.NotNull kotlin.ranges.IntProgression $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        return kotlin.ranges.IntProgression.Companion.fromClosedRange($receiver.getLast(), $receiver.getFirst(), -$receiver.getStep());
    }

    @org.jetbrains.annotations.NotNull
    public static final kotlin.ranges.LongProgression reversed(@org.jetbrains.annotations.NotNull kotlin.ranges.LongProgression $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        return kotlin.ranges.LongProgression.Companion.fromClosedRange($receiver.getLast(), $receiver.getFirst(), -$receiver.getStep());
    }

    @org.jetbrains.annotations.NotNull
    public static final kotlin.ranges.CharProgression reversed(@org.jetbrains.annotations.NotNull kotlin.ranges.CharProgression $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        return kotlin.ranges.CharProgression.Companion.fromClosedRange($receiver.getLast(), $receiver.getFirst(), -$receiver.getStep());
    }

    @org.jetbrains.annotations.NotNull
    public static final kotlin.ranges.IntProgression step(@org.jetbrains.annotations.NotNull kotlin.ranges.IntProgression $receiver, int step) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.ranges.RangesKt.checkStepIsPositive(step > 0, java.lang.Integer.valueOf(step));
        kotlin.ranges.IntProgression.Companion companion = kotlin.ranges.IntProgression.Companion;
        int first = $receiver.getFirst();
        int last = $receiver.getLast();
        if ($receiver.getStep() <= 0) {
            step = -step;
        }
        return companion.fromClosedRange(first, last, step);
    }

    @org.jetbrains.annotations.NotNull
    public static final kotlin.ranges.LongProgression step(@org.jetbrains.annotations.NotNull kotlin.ranges.LongProgression $receiver, long step) {
        boolean z;
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        if (step > ((long) 0)) {
            z = true;
        } else {
            z = false;
        }
        kotlin.ranges.RangesKt.checkStepIsPositive(z, java.lang.Long.valueOf(step));
        return kotlin.ranges.LongProgression.Companion.fromClosedRange($receiver.getFirst(), $receiver.getLast(), $receiver.getStep() > ((long) 0) ? step : -step);
    }

    @org.jetbrains.annotations.NotNull
    public static final kotlin.ranges.CharProgression step(@org.jetbrains.annotations.NotNull kotlin.ranges.CharProgression $receiver, int step) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.ranges.RangesKt.checkStepIsPositive(step > 0, java.lang.Integer.valueOf(step));
        kotlin.ranges.CharProgression.Companion companion = kotlin.ranges.CharProgression.Companion;
        char first = $receiver.getFirst();
        char last = $receiver.getLast();
        if ($receiver.getStep() <= 0) {
            step = -step;
        }
        return companion.fromClosedRange(first, last, step);
    }

    @org.jetbrains.annotations.Nullable
    public static final java.lang.Byte toByteExactOrNull(int $receiver) {
        if (-128 <= $receiver && $receiver <= 127) {
            return java.lang.Byte.valueOf((byte) $receiver);
        }
        return null;
    }

    @org.jetbrains.annotations.Nullable
    public static final java.lang.Byte toByteExactOrNull(long $receiver) {
        if (((long) -128) <= $receiver && $receiver <= ((long) android.support.v4.media.TransportMediator.KEYCODE_MEDIA_PAUSE)) {
            return java.lang.Byte.valueOf((byte) ((int) $receiver));
        }
        return null;
    }

    @org.jetbrains.annotations.Nullable
    public static final java.lang.Byte toByteExactOrNull(short $receiver) {
        if (((short) -128) <= $receiver && $receiver <= ((short) android.support.v4.media.TransportMediator.KEYCODE_MEDIA_PAUSE)) {
            return java.lang.Byte.valueOf((byte) $receiver);
        }
        return null;
    }

    @org.jetbrains.annotations.Nullable
    public static final java.lang.Byte toByteExactOrNull(double $receiver) {
        if (((double) -128) <= $receiver && $receiver <= ((double) android.support.v4.media.TransportMediator.KEYCODE_MEDIA_PAUSE)) {
            return java.lang.Byte.valueOf((byte) ((int) $receiver));
        }
        return null;
    }

    @org.jetbrains.annotations.Nullable
    public static final java.lang.Byte toByteExactOrNull(float $receiver) {
        if (((float) -128) <= $receiver && $receiver <= ((float) android.support.v4.media.TransportMediator.KEYCODE_MEDIA_PAUSE)) {
            return java.lang.Byte.valueOf((byte) ((int) $receiver));
        }
        return null;
    }

    @org.jetbrains.annotations.Nullable
    public static final java.lang.Integer toIntExactOrNull(long $receiver) {
        if (((long) Integer.MIN_VALUE) <= $receiver && $receiver <= ((long) Integer.MAX_VALUE)) {
            return java.lang.Integer.valueOf((int) $receiver);
        }
        return null;
    }

    @org.jetbrains.annotations.Nullable
    public static final java.lang.Integer toIntExactOrNull(double $receiver) {
        if (((double) Integer.MIN_VALUE) <= $receiver && $receiver <= ((double) Integer.MAX_VALUE)) {
            return java.lang.Integer.valueOf((int) $receiver);
        }
        return null;
    }

    @org.jetbrains.annotations.Nullable
    public static final java.lang.Integer toIntExactOrNull(float $receiver) {
        if (((float) Integer.MIN_VALUE) <= $receiver && $receiver <= ((float) Integer.MAX_VALUE)) {
            return java.lang.Integer.valueOf((int) $receiver);
        }
        return null;
    }

    @org.jetbrains.annotations.Nullable
    public static final java.lang.Long toLongExactOrNull(double $receiver) {
        if (((double) Long.MIN_VALUE) <= $receiver && $receiver <= ((double) kotlin.jvm.internal.LongCompanionObject.MAX_VALUE)) {
            return java.lang.Long.valueOf((long) $receiver);
        }
        return null;
    }

    @org.jetbrains.annotations.Nullable
    public static final java.lang.Long toLongExactOrNull(float $receiver) {
        if (((float) Long.MIN_VALUE) <= $receiver && $receiver <= ((float) kotlin.jvm.internal.LongCompanionObject.MAX_VALUE)) {
            return java.lang.Long.valueOf((long) $receiver);
        }
        return null;
    }

    @org.jetbrains.annotations.Nullable
    public static final java.lang.Short toShortExactOrNull(int $receiver) {
        if (-32768 <= $receiver && $receiver <= 32767) {
            return java.lang.Short.valueOf((short) $receiver);
        }
        return null;
    }

    @org.jetbrains.annotations.Nullable
    public static final java.lang.Short toShortExactOrNull(long $receiver) {
        if (((long) -32768) <= $receiver && $receiver <= ((long) 32767)) {
            return java.lang.Short.valueOf((short) ((int) $receiver));
        }
        return null;
    }

    @org.jetbrains.annotations.Nullable
    public static final java.lang.Short toShortExactOrNull(double $receiver) {
        if (((double) -32768) <= $receiver && $receiver <= ((double) 32767)) {
            return java.lang.Short.valueOf((short) ((int) $receiver));
        }
        return null;
    }

    @org.jetbrains.annotations.Nullable
    public static final java.lang.Short toShortExactOrNull(float $receiver) {
        if (((float) -32768) <= $receiver && $receiver <= ((float) 32767)) {
            return java.lang.Short.valueOf((short) ((int) $receiver));
        }
        return null;
    }

    @org.jetbrains.annotations.NotNull
    public static final kotlin.ranges.IntRange until(int $receiver, byte to) {
        return new kotlin.ranges.IntRange($receiver, to - 1);
    }

    @org.jetbrains.annotations.NotNull
    public static final kotlin.ranges.LongRange until(long $receiver, byte to) {
        return new kotlin.ranges.LongRange($receiver, ((long) to) - ((long) 1));
    }

    @org.jetbrains.annotations.NotNull
    public static final kotlin.ranges.IntRange until(byte $receiver, byte to) {
        return new kotlin.ranges.IntRange($receiver, to - 1);
    }

    @org.jetbrains.annotations.NotNull
    public static final kotlin.ranges.IntRange until(short $receiver, byte to) {
        return new kotlin.ranges.IntRange($receiver, to - 1);
    }

    @org.jetbrains.annotations.NotNull
    public static final kotlin.ranges.CharRange until(char $receiver, char to) {
        if (to <= 0) {
            return kotlin.ranges.CharRange.Companion.getEMPTY();
        }
        return new kotlin.ranges.CharRange($receiver, (char) (to - 1));
    }

    @org.jetbrains.annotations.NotNull
    public static final kotlin.ranges.IntRange until(int $receiver, int to) {
        if (to <= Integer.MIN_VALUE) {
            return kotlin.ranges.IntRange.Companion.getEMPTY();
        }
        return new kotlin.ranges.IntRange($receiver, to - 1);
    }

    @org.jetbrains.annotations.NotNull
    public static final kotlin.ranges.LongRange until(long $receiver, int to) {
        return new kotlin.ranges.LongRange($receiver, ((long) to) - ((long) 1));
    }

    @org.jetbrains.annotations.NotNull
    public static final kotlin.ranges.IntRange until(byte $receiver, int to) {
        if (to <= Integer.MIN_VALUE) {
            return kotlin.ranges.IntRange.Companion.getEMPTY();
        }
        return new kotlin.ranges.IntRange($receiver, to - 1);
    }

    @org.jetbrains.annotations.NotNull
    public static final kotlin.ranges.IntRange until(short $receiver, int to) {
        if (to <= Integer.MIN_VALUE) {
            return kotlin.ranges.IntRange.Companion.getEMPTY();
        }
        return new kotlin.ranges.IntRange($receiver, to - 1);
    }

    @org.jetbrains.annotations.NotNull
    public static final kotlin.ranges.LongRange until(int $receiver, long to) {
        if (to <= Long.MIN_VALUE) {
            return kotlin.ranges.LongRange.Companion.getEMPTY();
        }
        return new kotlin.ranges.LongRange((long) $receiver, to - ((long) 1));
    }

    @org.jetbrains.annotations.NotNull
    public static final kotlin.ranges.LongRange until(long $receiver, long to) {
        if (to <= Long.MIN_VALUE) {
            return kotlin.ranges.LongRange.Companion.getEMPTY();
        }
        return new kotlin.ranges.LongRange($receiver, to - ((long) 1));
    }

    @org.jetbrains.annotations.NotNull
    public static final kotlin.ranges.LongRange until(byte $receiver, long to) {
        if (to <= Long.MIN_VALUE) {
            return kotlin.ranges.LongRange.Companion.getEMPTY();
        }
        return new kotlin.ranges.LongRange((long) $receiver, to - ((long) 1));
    }

    @org.jetbrains.annotations.NotNull
    public static final kotlin.ranges.LongRange until(short $receiver, long to) {
        if (to <= Long.MIN_VALUE) {
            return kotlin.ranges.LongRange.Companion.getEMPTY();
        }
        return new kotlin.ranges.LongRange((long) $receiver, to - ((long) 1));
    }

    @org.jetbrains.annotations.NotNull
    public static final kotlin.ranges.IntRange until(int $receiver, short to) {
        return new kotlin.ranges.IntRange($receiver, to - 1);
    }

    @org.jetbrains.annotations.NotNull
    public static final kotlin.ranges.LongRange until(long $receiver, short to) {
        return new kotlin.ranges.LongRange($receiver, ((long) to) - ((long) 1));
    }

    @org.jetbrains.annotations.NotNull
    public static final kotlin.ranges.IntRange until(byte $receiver, short to) {
        return new kotlin.ranges.IntRange($receiver, to - 1);
    }

    @org.jetbrains.annotations.NotNull
    public static final kotlin.ranges.IntRange until(short $receiver, short to) {
        return new kotlin.ranges.IntRange($receiver, to - 1);
    }

    @org.jetbrains.annotations.NotNull
    public static final <T extends java.lang.Comparable<? super T>> T coerceAtLeast(@org.jetbrains.annotations.NotNull T $receiver, @org.jetbrains.annotations.NotNull T minimumValue) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(minimumValue, "minimumValue");
        return $receiver.compareTo(minimumValue) < 0 ? minimumValue : $receiver;
    }

    public static final byte coerceAtLeast(byte $receiver, byte minimumValue) {
        return $receiver < minimumValue ? minimumValue : $receiver;
    }

    public static final short coerceAtLeast(short $receiver, short minimumValue) {
        return $receiver < minimumValue ? minimumValue : $receiver;
    }

    public static final int coerceAtLeast(int $receiver, int minimumValue) {
        return $receiver < minimumValue ? minimumValue : $receiver;
    }

    public static final long coerceAtLeast(long $receiver, long minimumValue) {
        return $receiver < minimumValue ? minimumValue : $receiver;
    }

    public static final float coerceAtLeast(float $receiver, float minimumValue) {
        return $receiver < minimumValue ? minimumValue : $receiver;
    }

    public static final double coerceAtLeast(double $receiver, double minimumValue) {
        return $receiver < minimumValue ? minimumValue : $receiver;
    }

    @org.jetbrains.annotations.NotNull
    public static final <T extends java.lang.Comparable<? super T>> T coerceAtMost(@org.jetbrains.annotations.NotNull T $receiver, @org.jetbrains.annotations.NotNull T maximumValue) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(maximumValue, "maximumValue");
        return $receiver.compareTo(maximumValue) > 0 ? maximumValue : $receiver;
    }

    public static final byte coerceAtMost(byte $receiver, byte maximumValue) {
        return $receiver > maximumValue ? maximumValue : $receiver;
    }

    public static final short coerceAtMost(short $receiver, short maximumValue) {
        return $receiver > maximumValue ? maximumValue : $receiver;
    }

    public static final int coerceAtMost(int $receiver, int maximumValue) {
        return $receiver > maximumValue ? maximumValue : $receiver;
    }

    public static final long coerceAtMost(long $receiver, long maximumValue) {
        return $receiver > maximumValue ? maximumValue : $receiver;
    }

    public static final float coerceAtMost(float $receiver, float maximumValue) {
        return $receiver > maximumValue ? maximumValue : $receiver;
    }

    public static final double coerceAtMost(double $receiver, double maximumValue) {
        return $receiver > maximumValue ? maximumValue : $receiver;
    }

    @org.jetbrains.annotations.NotNull
    public static final <T extends java.lang.Comparable<? super T>> T coerceIn(@org.jetbrains.annotations.NotNull T $receiver, @org.jetbrains.annotations.Nullable T minimumValue, @org.jetbrains.annotations.Nullable T maximumValue) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        if (minimumValue == null || maximumValue == null) {
            if (minimumValue != null && $receiver.compareTo(minimumValue) < 0) {
                return minimumValue;
            }
            if (maximumValue != null && $receiver.compareTo(maximumValue) > 0) {
                return maximumValue;
            }
        } else if (minimumValue.compareTo(maximumValue) > 0) {
            throw new java.lang.IllegalArgumentException("Cannot coerce value to an empty range: maximum " + maximumValue + " is less than minimum " + minimumValue + ".");
        } else if ($receiver.compareTo(minimumValue) < 0) {
            return minimumValue;
        } else {
            if ($receiver.compareTo(maximumValue) > 0) {
                return maximumValue;
            }
        }
        return $receiver;
    }

    public static final byte coerceIn(byte $receiver, byte minimumValue, byte maximumValue) {
        if (minimumValue > maximumValue) {
            throw new java.lang.IllegalArgumentException("Cannot coerce value to an empty range: maximum " + maximumValue + " is less than minimum " + minimumValue + ".");
        } else if ($receiver < minimumValue) {
            return minimumValue;
        } else {
            return $receiver > maximumValue ? maximumValue : $receiver;
        }
    }

    public static final short coerceIn(short $receiver, short minimumValue, short maximumValue) {
        if (minimumValue > maximumValue) {
            throw new java.lang.IllegalArgumentException("Cannot coerce value to an empty range: maximum " + maximumValue + " is less than minimum " + minimumValue + ".");
        } else if ($receiver < minimumValue) {
            return minimumValue;
        } else {
            return $receiver > maximumValue ? maximumValue : $receiver;
        }
    }

    public static final int coerceIn(int $receiver, int minimumValue, int maximumValue) {
        if (minimumValue > maximumValue) {
            throw new java.lang.IllegalArgumentException("Cannot coerce value to an empty range: maximum " + maximumValue + " is less than minimum " + minimumValue + ".");
        } else if ($receiver < minimumValue) {
            return minimumValue;
        } else {
            return $receiver > maximumValue ? maximumValue : $receiver;
        }
    }

    public static final long coerceIn(long $receiver, long minimumValue, long maximumValue) {
        if (minimumValue > maximumValue) {
            throw new java.lang.IllegalArgumentException("Cannot coerce value to an empty range: maximum " + maximumValue + " is less than minimum " + minimumValue + ".");
        } else if ($receiver < minimumValue) {
            return minimumValue;
        } else {
            return $receiver > maximumValue ? maximumValue : $receiver;
        }
    }

    public static final float coerceIn(float $receiver, float minimumValue, float maximumValue) {
        if (minimumValue > maximumValue) {
            throw new java.lang.IllegalArgumentException("Cannot coerce value to an empty range: maximum " + maximumValue + " is less than minimum " + minimumValue + ".");
        } else if ($receiver < minimumValue) {
            return minimumValue;
        } else {
            return $receiver > maximumValue ? maximumValue : $receiver;
        }
    }

    public static final double coerceIn(double $receiver, double minimumValue, double maximumValue) {
        if (minimumValue > maximumValue) {
            throw new java.lang.IllegalArgumentException("Cannot coerce value to an empty range: maximum " + maximumValue + " is less than minimum " + minimumValue + ".");
        } else if ($receiver < minimumValue) {
            return minimumValue;
        } else {
            return $receiver > maximumValue ? maximumValue : $receiver;
        }
    }

    @org.jetbrains.annotations.NotNull
    @kotlin.SinceKotlin(version = "1.1")
    public static final <T extends java.lang.Comparable<? super T>> T coerceIn(@org.jetbrains.annotations.NotNull T $receiver, @org.jetbrains.annotations.NotNull kotlin.ranges.ClosedFloatingPointRange<T> range) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(range, "range");
        if (range.isEmpty()) {
            throw new java.lang.IllegalArgumentException("Cannot coerce value to an empty range: " + range + ".");
        } else if (range.lessThanOrEquals($receiver, range.getStart()) && !range.lessThanOrEquals(range.getStart(), $receiver)) {
            return range.getStart();
        } else {
            if (!range.lessThanOrEquals(range.getEndInclusive(), $receiver) || range.lessThanOrEquals($receiver, range.getEndInclusive())) {
                return $receiver;
            }
            return range.getEndInclusive();
        }
    }

    @org.jetbrains.annotations.NotNull
    public static final <T extends java.lang.Comparable<? super T>> T coerceIn(@org.jetbrains.annotations.NotNull T $receiver, @org.jetbrains.annotations.NotNull kotlin.ranges.ClosedRange<T> range) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(range, "range");
        if (range instanceof kotlin.ranges.ClosedFloatingPointRange) {
            return kotlin.ranges.RangesKt.coerceIn($receiver, (kotlin.ranges.ClosedFloatingPointRange) range);
        }
        if (range.isEmpty()) {
            throw new java.lang.IllegalArgumentException("Cannot coerce value to an empty range: " + range + ".");
        } else if ($receiver.compareTo(range.getStart()) < 0) {
            return range.getStart();
        } else {
            if ($receiver.compareTo(range.getEndInclusive()) > 0) {
                return range.getEndInclusive();
            }
            return $receiver;
        }
    }

    public static final int coerceIn(int $receiver, @org.jetbrains.annotations.NotNull kotlin.ranges.ClosedRange<java.lang.Integer> range) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(range, "range");
        if (range instanceof kotlin.ranges.ClosedFloatingPointRange) {
            return ((java.lang.Number) kotlin.ranges.RangesKt.coerceIn((T) java.lang.Integer.valueOf($receiver), (kotlin.ranges.ClosedFloatingPointRange) range)).intValue();
        }
        if (range.isEmpty()) {
            throw new java.lang.IllegalArgumentException("Cannot coerce value to an empty range: " + range + ".");
        } else if ($receiver < ((java.lang.Number) range.getStart()).intValue()) {
            return ((java.lang.Number) range.getStart()).intValue();
        } else {
            if ($receiver > ((java.lang.Number) range.getEndInclusive()).intValue()) {
                return ((java.lang.Number) range.getEndInclusive()).intValue();
            }
            return $receiver;
        }
    }

    public static final long coerceIn(long $receiver, @org.jetbrains.annotations.NotNull kotlin.ranges.ClosedRange<java.lang.Long> range) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(range, "range");
        if (range instanceof kotlin.ranges.ClosedFloatingPointRange) {
            return ((java.lang.Number) kotlin.ranges.RangesKt.coerceIn((T) java.lang.Long.valueOf($receiver), (kotlin.ranges.ClosedFloatingPointRange) range)).longValue();
        }
        if (range.isEmpty()) {
            throw new java.lang.IllegalArgumentException("Cannot coerce value to an empty range: " + range + ".");
        } else if ($receiver < ((java.lang.Number) range.getStart()).longValue()) {
            return ((java.lang.Number) range.getStart()).longValue();
        } else {
            if ($receiver > ((java.lang.Number) range.getEndInclusive()).longValue()) {
                return ((java.lang.Number) range.getEndInclusive()).longValue();
            }
            return $receiver;
        }
    }
}
