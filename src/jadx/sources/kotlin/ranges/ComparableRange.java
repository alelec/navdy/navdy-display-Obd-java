package kotlin.ranges;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000f\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0012\u0018\u0000*\u000e\b\u0000\u0010\u0001*\b\u0012\u0004\u0012\u0002H\u00010\u00022\b\u0012\u0004\u0012\u0002H\u00010\u0003B\u0015\u0012\u0006\u0010\u0004\u001a\u00028\u0000\u0012\u0006\u0010\u0005\u001a\u00028\u0000\u00a2\u0006\u0002\u0010\u0006J\u0013\u0010\u000b\u001a\u00020\f2\b\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0096\u0002J\b\u0010\u000f\u001a\u00020\u0010H\u0016J\b\u0010\u0011\u001a\u00020\u0012H\u0016R\u0016\u0010\u0005\u001a\u00028\u0000X\u0096\u0004\u00a2\u0006\n\n\u0002\u0010\t\u001a\u0004\b\u0007\u0010\bR\u0016\u0010\u0004\u001a\u00028\u0000X\u0096\u0004\u00a2\u0006\n\n\u0002\u0010\t\u001a\u0004\b\n\u0010\b\u00a8\u0006\u0013"}, d2 = {"Lkotlin/ranges/ComparableRange;", "T", "", "Lkotlin/ranges/ClosedRange;", "start", "endInclusive", "(Ljava/lang/Comparable;Ljava/lang/Comparable;)V", "getEndInclusive", "()Ljava/lang/Comparable;", "Ljava/lang/Comparable;", "getStart", "equals", "", "other", "", "hashCode", "", "toString", "", "kotlin-stdlib"}, k = 1, mv = {1, 1, 6})
/* compiled from: Ranges.kt */
class ComparableRange<T extends java.lang.Comparable<? super T>> implements kotlin.ranges.ClosedRange<T> {
    @org.jetbrains.annotations.NotNull
    private final T endInclusive;
    @org.jetbrains.annotations.NotNull
    private final T start;

    public ComparableRange(@org.jetbrains.annotations.NotNull T start2, @org.jetbrains.annotations.NotNull T endInclusive2) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(start2, "start");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(endInclusive2, "endInclusive");
        this.start = start2;
        this.endInclusive = endInclusive2;
    }

    public boolean contains(@org.jetbrains.annotations.NotNull T value) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(value, "value");
        return kotlin.ranges.ClosedRange.DefaultImpls.contains(this, value);
    }

    public boolean isEmpty() {
        return kotlin.ranges.ClosedRange.DefaultImpls.isEmpty(this);
    }

    @org.jetbrains.annotations.NotNull
    public T getStart() {
        return this.start;
    }

    @org.jetbrains.annotations.NotNull
    public T getEndInclusive() {
        return this.endInclusive;
    }

    public boolean equals(@org.jetbrains.annotations.Nullable java.lang.Object other) {
        return (other instanceof kotlin.ranges.ComparableRange) && ((isEmpty() && ((kotlin.ranges.ComparableRange) other).isEmpty()) || (kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) getStart(), (java.lang.Object) ((kotlin.ranges.ComparableRange) other).getStart()) && kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) getEndInclusive(), (java.lang.Object) ((kotlin.ranges.ComparableRange) other).getEndInclusive())));
    }

    public int hashCode() {
        if (isEmpty()) {
            return -1;
        }
        return (getStart().hashCode() * 31) + getEndInclusive().hashCode();
    }

    @org.jetbrains.annotations.NotNull
    public java.lang.String toString() {
        return getStart() + ".." + getEndInclusive();
    }
}
