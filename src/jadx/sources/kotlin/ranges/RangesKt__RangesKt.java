package kotlin.ranges;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u00000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0004\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000f\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0006\n\u0002\u0010\u0007\n\u0000\u001a\u0018\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0000\u001a0\u0010\u0006\u001a\b\u0012\u0004\u0012\u0002H\b0\u0007\"\u000e\b\u0000\u0010\b*\b\u0012\u0004\u0012\u0002H\b0\t*\u0002H\b2\u0006\u0010\n\u001a\u0002H\bH\u0086\u0002\u00a2\u0006\u0002\u0010\u000b\u001a\u001b\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\r0\f*\u00020\r2\u0006\u0010\n\u001a\u00020\rH\u0087\u0002\u001a\u001b\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u000e0\f*\u00020\u000e2\u0006\u0010\n\u001a\u00020\u000eH\u0087\u0002\u00a8\u0006\u000f"}, d2 = {"checkStepIsPositive", "", "isPositive", "", "step", "", "rangeTo", "Lkotlin/ranges/ClosedRange;", "T", "", "that", "(Ljava/lang/Comparable;Ljava/lang/Comparable;)Lkotlin/ranges/ClosedRange;", "Lkotlin/ranges/ClosedFloatingPointRange;", "", "", "kotlin-stdlib"}, k = 5, mv = {1, 1, 6}, xi = 1, xs = "kotlin/ranges/RangesKt")
/* compiled from: Ranges.kt */
class RangesKt__RangesKt {
    @org.jetbrains.annotations.NotNull
    public static final <T extends java.lang.Comparable<? super T>> kotlin.ranges.ClosedRange<T> rangeTo(@org.jetbrains.annotations.NotNull T $receiver, @org.jetbrains.annotations.NotNull T that) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(that, "that");
        return new kotlin.ranges.ComparableRange<>($receiver, that);
    }

    @org.jetbrains.annotations.NotNull
    @kotlin.SinceKotlin(version = "1.1")
    public static final kotlin.ranges.ClosedFloatingPointRange<java.lang.Double> rangeTo(double $receiver, double that) {
        return new kotlin.ranges.ClosedDoubleRange<>($receiver, that);
    }

    @org.jetbrains.annotations.NotNull
    @kotlin.SinceKotlin(version = "1.1")
    public static final kotlin.ranges.ClosedFloatingPointRange<java.lang.Float> rangeTo(float $receiver, float that) {
        return new kotlin.ranges.ClosedFloatRange<>($receiver, that);
    }

    public static final void checkStepIsPositive(boolean isPositive, @org.jetbrains.annotations.NotNull java.lang.Number step) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(step, "step");
        if (!isPositive) {
            throw new java.lang.IllegalArgumentException("Step must be positive, was: " + step + ".");
        }
    }
}
