package kotlin.ranges;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u001c\n\u0002\u0010\t\n\u0002\b\u000b\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0016\u0018\u0000 \u00182\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0018B\u001f\b\u0000\u0012\u0006\u0010\u0003\u001a\u00020\u0002\u0012\u0006\u0010\u0004\u001a\u00020\u0002\u0012\u0006\u0010\u0005\u001a\u00020\u0002\u00a2\u0006\u0002\u0010\u0006J\u0013\u0010\r\u001a\u00020\u000e2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u0096\u0002J\b\u0010\u0011\u001a\u00020\u0012H\u0016J\b\u0010\u0013\u001a\u00020\u000eH\u0016J\t\u0010\u0014\u001a\u00020\u0015H\u0096\u0002J\b\u0010\u0016\u001a\u00020\u0017H\u0016R\u0011\u0010\u0007\u001a\u00020\u0002\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\n\u001a\u00020\u0002\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\tR\u0011\u0010\u0005\u001a\u00020\u0002\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\t\u00a8\u0006\u0019"}, d2 = {"Lkotlin/ranges/LongProgression;", "", "", "start", "endInclusive", "step", "(JJJ)V", "first", "getFirst", "()J", "last", "getLast", "getStep", "equals", "", "other", "", "hashCode", "", "isEmpty", "iterator", "Lkotlin/collections/LongIterator;", "toString", "", "Companion", "kotlin-runtime"}, k = 1, mv = {1, 1, 6})
/* compiled from: Progressions.kt */
public class LongProgression implements java.lang.Iterable<java.lang.Long>, kotlin.jvm.internal.markers.KMappedMarker {
    public static final kotlin.ranges.LongProgression.Companion Companion = new kotlin.ranges.LongProgression.Companion(null);
    private final long first;
    private final long last;
    private final long step;

    @kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u001e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00062\u0006\u0010\b\u001a\u00020\u0006\u00a8\u0006\t"}, d2 = {"Lkotlin/ranges/LongProgression$Companion;", "", "()V", "fromClosedRange", "Lkotlin/ranges/LongProgression;", "rangeStart", "", "rangeEnd", "step", "kotlin-runtime"}, k = 1, mv = {1, 1, 6})
    /* compiled from: Progressions.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(kotlin.jvm.internal.DefaultConstructorMarker $constructor_marker) {
            this();
        }

        @org.jetbrains.annotations.NotNull
        public final kotlin.ranges.LongProgression fromClosedRange(long rangeStart, long rangeEnd, long step) {
            return new kotlin.ranges.LongProgression(rangeStart, rangeEnd, step);
        }
    }

    public LongProgression(long start, long endInclusive, long step2) {
        if (step2 == 0) {
            throw new java.lang.IllegalArgumentException("Step must be non-zero");
        }
        this.first = start;
        this.last = kotlin.internal.ProgressionUtilKt.getProgressionLastElement(start, endInclusive, step2);
        this.step = step2;
    }

    public final long getFirst() {
        return this.first;
    }

    public final long getLast() {
        return this.last;
    }

    public final long getStep() {
        return this.step;
    }

    @org.jetbrains.annotations.NotNull
    public kotlin.collections.LongIterator iterator() {
        return new kotlin.ranges.LongProgressionIterator(this.first, this.last, this.step);
    }

    public boolean isEmpty() {
        return this.step > ((long) 0) ? this.first > this.last : this.first < this.last;
    }

    public boolean equals(@org.jetbrains.annotations.Nullable java.lang.Object other) {
        return (other instanceof kotlin.ranges.LongProgression) && ((isEmpty() && ((kotlin.ranges.LongProgression) other).isEmpty()) || (this.first == ((kotlin.ranges.LongProgression) other).first && this.last == ((kotlin.ranges.LongProgression) other).last && this.step == ((kotlin.ranges.LongProgression) other).step));
    }

    public int hashCode() {
        if (isEmpty()) {
            return -1;
        }
        return (int) ((((long) 31) * ((((long) 31) * (this.first ^ (this.first >>> 32))) + (this.last ^ (this.last >>> 32)))) + (this.step ^ (this.step >>> 32)));
    }

    @org.jetbrains.annotations.NotNull
    public java.lang.String toString() {
        return this.step > ((long) 0) ? this.first + ".." + this.last + " step " + this.step : this.first + " downTo " + this.last + " step " + (-this.step);
    }
}
