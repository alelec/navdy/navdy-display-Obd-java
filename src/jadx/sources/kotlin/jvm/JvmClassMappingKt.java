package kotlin.jvm;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000,\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u001b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0000\n\u0002\b\u000b\n\u0002\u0010\u000b\n\u0002\u0010\u0011\n\u0002\b\u0002\u001a!\u0010\u0018\u001a\u00020\u0019\"\n\b\u0000\u0010\u0002\u0018\u0001*\u00020\r*\u0006\u0012\u0002\b\u00030\u001aH\u0007\u00a2\u0006\u0002\u0010\u001b\"'\u0010\u0000\u001a\n\u0012\u0006\b\u0001\u0012\u0002H\u00020\u0001\"\b\b\u0000\u0010\u0002*\u00020\u0003*\u0002H\u00028F\u00a2\u0006\u0006\u001a\u0004\b\u0004\u0010\u0005\"0\u0010\u0006\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0007\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u00018GX\u0087\u0004\u00a2\u0006\f\u0012\u0004\b\b\u0010\t\u001a\u0004\b\n\u0010\u000b\"&\u0010\f\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0007\"\b\b\u0000\u0010\u0002*\u00020\r*\u0002H\u00028\u00c7\u0002\u00a2\u0006\u0006\u001a\u0004\b\n\u0010\u000e\";\u0010\f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00020\u00010\u0007\"\b\b\u0000\u0010\u0002*\u00020\r*\b\u0012\u0004\u0012\u0002H\u00020\u00018\u00c7\u0002X\u0087\u0004\u00a2\u0006\f\u0012\u0004\b\u000f\u0010\t\u001a\u0004\b\u0010\u0010\u000b\"+\u0010\u0011\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0007\"\b\b\u0000\u0010\u0002*\u00020\r*\b\u0012\u0004\u0012\u0002H\u00020\u00018F\u00a2\u0006\u0006\u001a\u0004\b\u0012\u0010\u000b\"-\u0010\u0013\u001a\n\u0012\u0004\u0012\u0002H\u0002\u0018\u00010\u0007\"\b\b\u0000\u0010\u0002*\u00020\r*\b\u0012\u0004\u0012\u0002H\u00020\u00018F\u00a2\u0006\u0006\u001a\u0004\b\u0014\u0010\u000b\"+\u0010\u0015\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\b\b\u0000\u0010\u0002*\u00020\r*\b\u0012\u0004\u0012\u0002H\u00020\u00078G\u00a2\u0006\u0006\u001a\u0004\b\u0016\u0010\u0017\u00a8\u0006\u001c"}, d2 = {"annotationClass", "Lkotlin/reflect/KClass;", "T", "", "getAnnotationClass", "(Ljava/lang/annotation/Annotation;)Lkotlin/reflect/KClass;", "java", "Ljava/lang/Class;", "java$annotations", "(Lkotlin/reflect/KClass;)V", "getJavaClass", "(Lkotlin/reflect/KClass;)Ljava/lang/Class;", "javaClass", "", "(Ljava/lang/Object;)Ljava/lang/Class;", "javaClass$annotations", "getRuntimeClassOfKClassInstance", "javaObjectType", "getJavaObjectType", "javaPrimitiveType", "getJavaPrimitiveType", "kotlin", "getKotlinClass", "(Ljava/lang/Class;)Lkotlin/reflect/KClass;", "isArrayOf", "", "", "([Ljava/lang/Object;)Z", "kotlin-runtime"}, k = 2, mv = {1, 1, 6})
@kotlin.jvm.JvmName(name = "JvmClassMappingKt")
/* compiled from: JvmClassMapping.kt */
public final class JvmClassMappingKt {
    public static /* synthetic */ void java$annotations(kotlin.reflect.KClass kClass) {
    }

    @kotlin.Deprecated(level = kotlin.DeprecationLevel.ERROR, message = "Use 'java' property to get Java class corresponding to this Kotlin class or cast this instance to Any if you really want to get the runtime Java class of this implementation of KClass.", replaceWith = @kotlin.ReplaceWith(expression = "(this as Any).javaClass", imports = {}))
    public static /* synthetic */ void javaClass$annotations(kotlin.reflect.KClass kClass) {
    }

    @org.jetbrains.annotations.NotNull
    @kotlin.jvm.JvmName(name = "getJavaClass")
    public static final <T> java.lang.Class<T> getJavaClass(@org.jetbrains.annotations.NotNull kotlin.reflect.KClass<T> $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        java.lang.Class<T> jClass = ((kotlin.jvm.internal.ClassBasedDeclarationContainer) $receiver).getJClass();
        if (jClass != null) {
            return jClass;
        }
        throw new kotlin.TypeCastException("null cannot be cast to non-null type java.lang.Class<T>");
    }

    @org.jetbrains.annotations.Nullable
    public static final <T> java.lang.Class<T> getJavaPrimitiveType(@org.jetbrains.annotations.NotNull kotlin.reflect.KClass<T> $receiver) {
        java.lang.Class cls;
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        java.lang.Class thisJClass = ((kotlin.jvm.internal.ClassBasedDeclarationContainer) $receiver).getJClass();
        if (!thisJClass.isPrimitive()) {
            java.lang.String name = thisJClass.getName();
            if (name != null) {
                switch (name.hashCode()) {
                    case -2056817302:
                        if (name.equals("java.lang.Integer")) {
                            cls = java.lang.Integer.TYPE;
                            break;
                        }
                    case -527879800:
                        if (name.equals("java.lang.Float")) {
                            cls = java.lang.Float.TYPE;
                            break;
                        }
                    case -515992664:
                        if (name.equals("java.lang.Short")) {
                            cls = java.lang.Short.TYPE;
                            break;
                        }
                    case 155276373:
                        if (name.equals("java.lang.Character")) {
                            cls = java.lang.Character.TYPE;
                            break;
                        }
                    case 344809556:
                        if (name.equals("java.lang.Boolean")) {
                            cls = java.lang.Boolean.TYPE;
                            break;
                        }
                    case 398507100:
                        if (name.equals("java.lang.Byte")) {
                            cls = java.lang.Byte.TYPE;
                            break;
                        }
                    case 398795216:
                        if (name.equals("java.lang.Long")) {
                            cls = java.lang.Long.TYPE;
                            break;
                        }
                    case 761287205:
                        if (name.equals("java.lang.Double")) {
                            cls = java.lang.Double.TYPE;
                            break;
                        }
                }
            }
            cls = null;
            return cls;
        } else if (thisJClass != null) {
            return thisJClass;
        } else {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type java.lang.Class<T>");
        }
    }

    @org.jetbrains.annotations.NotNull
    public static final <T> java.lang.Class<T> getJavaObjectType(@org.jetbrains.annotations.NotNull kotlin.reflect.KClass<T> $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        java.lang.Class thisJClass = ((kotlin.jvm.internal.ClassBasedDeclarationContainer) $receiver).getJClass();
        if (thisJClass.isPrimitive()) {
            java.lang.String name = thisJClass.getName();
            if (name != null) {
                switch (name.hashCode()) {
                    case -1325958191:
                        if (name.equals("double")) {
                            thisJClass = java.lang.Double.class;
                            break;
                        }
                        break;
                    case 104431:
                        if (name.equals("int")) {
                            thisJClass = java.lang.Integer.class;
                            break;
                        }
                        break;
                    case 3039496:
                        if (name.equals("byte")) {
                            thisJClass = java.lang.Byte.class;
                            break;
                        }
                        break;
                    case 3052374:
                        if (name.equals("char")) {
                            thisJClass = java.lang.Character.class;
                            break;
                        }
                        break;
                    case 3327612:
                        if (name.equals("long")) {
                            thisJClass = java.lang.Long.class;
                            break;
                        }
                        break;
                    case 64711720:
                        if (name.equals("boolean")) {
                            thisJClass = java.lang.Boolean.class;
                            break;
                        }
                        break;
                    case 97526364:
                        if (name.equals("float")) {
                            thisJClass = java.lang.Float.class;
                            break;
                        }
                        break;
                    case 109413500:
                        if (name.equals("short")) {
                            thisJClass = java.lang.Short.class;
                            break;
                        }
                        break;
                }
            }
            if (thisJClass == null) {
                throw new kotlin.TypeCastException("null cannot be cast to non-null type java.lang.Class<T>");
            }
        } else if (thisJClass == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type java.lang.Class<T>");
        }
        return thisJClass;
    }

    @org.jetbrains.annotations.NotNull
    @kotlin.jvm.JvmName(name = "getKotlinClass")
    public static final <T> kotlin.reflect.KClass<T> getKotlinClass(@org.jetbrains.annotations.NotNull java.lang.Class<T> $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.reflect.KClass<T> createKotlinClass = kotlin.jvm.internal.Reflection.createKotlinClass($receiver);
        if (createKotlinClass != null) {
            return createKotlinClass;
        }
        throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.reflect.KClass<T>");
    }

    @org.jetbrains.annotations.NotNull
    public static final <T> java.lang.Class<T> getJavaClass(@org.jetbrains.annotations.NotNull T $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        java.lang.Class<T> cls = $receiver.getClass();
        if (cls != null) {
            return cls;
        }
        throw new kotlin.TypeCastException("null cannot be cast to non-null type java.lang.Class<T>");
    }

    @org.jetbrains.annotations.NotNull
    @kotlin.jvm.JvmName(name = "getRuntimeClassOfKClassInstance")
    public static final <T> java.lang.Class<kotlin.reflect.KClass<T>> getRuntimeClassOfKClassInstance(@org.jetbrains.annotations.NotNull kotlin.reflect.KClass<T> $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        java.lang.Class<kotlin.reflect.KClass<T>> cls = $receiver.getClass();
        if (cls != null) {
            return cls;
        }
        throw new kotlin.TypeCastException("null cannot be cast to non-null type java.lang.Class<kotlin.reflect.KClass<T>>");
    }

    private static final <T> boolean isArrayOf(@org.jetbrains.annotations.NotNull java.lang.Object[] $receiver) {
        kotlin.jvm.internal.Intrinsics.reifiedOperationMarker(4, "T");
        return java.lang.Object.class.isAssignableFrom($receiver.getClass().getComponentType());
    }

    @org.jetbrains.annotations.NotNull
    public static final <T extends java.lang.annotation.Annotation> kotlin.reflect.KClass<? extends T> getAnnotationClass(@org.jetbrains.annotations.NotNull T $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.reflect.KClass<? extends T> kotlinClass = getKotlinClass($receiver.annotationType());
        if (kotlinClass != null) {
            return kotlinClass;
        }
        throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.reflect.KClass<out T>");
    }
}
