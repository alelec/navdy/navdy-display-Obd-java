package kotlin.jvm.internal;

public abstract class MutablePropertyReference2 extends kotlin.jvm.internal.MutablePropertyReference implements kotlin.reflect.KMutableProperty2 {
    /* access modifiers changed from: protected */
    public kotlin.reflect.KCallable computeReflected() {
        return kotlin.jvm.internal.Reflection.mutableProperty2(this);
    }

    public java.lang.Object invoke(java.lang.Object receiver1, java.lang.Object receiver2) {
        return get(receiver1, receiver2);
    }

    public kotlin.reflect.KProperty2.Getter getGetter() {
        return ((kotlin.reflect.KMutableProperty2) getReflected()).getGetter();
    }

    public kotlin.reflect.KMutableProperty2.Setter getSetter() {
        return ((kotlin.reflect.KMutableProperty2) getReflected()).getSetter();
    }

    @kotlin.SinceKotlin(version = "1.1")
    public java.lang.Object getDelegate(java.lang.Object receiver1, java.lang.Object receiver2) {
        return ((kotlin.reflect.KMutableProperty2) getReflected()).getDelegate(receiver1, receiver2);
    }
}
