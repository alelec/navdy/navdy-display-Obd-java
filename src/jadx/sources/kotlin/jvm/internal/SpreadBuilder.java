package kotlin.jvm.internal;

public class SpreadBuilder {
    private final java.util.ArrayList<java.lang.Object> list;

    public SpreadBuilder(int size) {
        this.list = new java.util.ArrayList<>(size);
    }

    public void addSpread(java.lang.Object container) {
        if (container != null) {
            if (container instanceof java.lang.Object[]) {
                java.lang.Object[] array = (java.lang.Object[]) container;
                if (array.length > 0) {
                    this.list.ensureCapacity(this.list.size() + array.length);
                    for (java.lang.Object element : array) {
                        this.list.add(element);
                    }
                }
            } else if (container instanceof java.util.Collection) {
                this.list.addAll((java.util.Collection) container);
            } else if (container instanceof java.lang.Iterable) {
                for (java.lang.Object element2 : (java.lang.Iterable) container) {
                    this.list.add(element2);
                }
            } else if (container instanceof java.util.Iterator) {
                java.util.Iterator iterator = (java.util.Iterator) container;
                while (iterator.hasNext()) {
                    this.list.add(iterator.next());
                }
            } else {
                throw new java.lang.UnsupportedOperationException("Don't know how to spread " + container.getClass());
            }
        }
    }

    public int size() {
        return this.list.size();
    }

    public void add(java.lang.Object element) {
        this.list.add(element);
    }

    public java.lang.Object[] toArray(java.lang.Object[] a) {
        return this.list.toArray(a);
    }
}
