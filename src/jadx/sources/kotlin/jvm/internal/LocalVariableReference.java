package kotlin.jvm.internal;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0017\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\n\u0010\u0003\u001a\u0004\u0018\u00010\u0004H\u0016J\b\u0010\u0005\u001a\u00020\u0006H\u0016\u00a8\u0006\u0007"}, d2 = {"Lkotlin/jvm/internal/LocalVariableReference;", "Lkotlin/jvm/internal/PropertyReference0;", "()V", "get", "", "getOwner", "Lkotlin/reflect/KDeclarationContainer;", "kotlin-runtime"}, k = 1, mv = {1, 1, 6})
@kotlin.SinceKotlin(version = "1.1")
/* compiled from: localVariableReferences.kt */
public class LocalVariableReference extends kotlin.jvm.internal.PropertyReference0 {
    @org.jetbrains.annotations.NotNull
    public kotlin.reflect.KDeclarationContainer getOwner() {
        kotlin.jvm.internal.LocalVariableReferencesKt.notSupportedError();
        throw null;
    }

    @org.jetbrains.annotations.Nullable
    public java.lang.Object get() {
        kotlin.jvm.internal.LocalVariableReferencesKt.notSupportedError();
        throw null;
    }
}
