package kotlin.jvm.internal;

public abstract class MutablePropertyReference0 extends kotlin.jvm.internal.MutablePropertyReference implements kotlin.reflect.KMutableProperty0 {
    public MutablePropertyReference0() {
    }

    @kotlin.SinceKotlin(version = "1.1")
    public MutablePropertyReference0(java.lang.Object receiver) {
        super(receiver);
    }

    /* access modifiers changed from: protected */
    public kotlin.reflect.KCallable computeReflected() {
        return kotlin.jvm.internal.Reflection.mutableProperty0(this);
    }

    public java.lang.Object invoke() {
        return get();
    }

    public kotlin.reflect.KProperty0.Getter getGetter() {
        return ((kotlin.reflect.KMutableProperty0) getReflected()).getGetter();
    }

    public kotlin.reflect.KMutableProperty0.Setter getSetter() {
        return ((kotlin.reflect.KMutableProperty0) getReflected()).getSetter();
    }

    @kotlin.SinceKotlin(version = "1.1")
    public java.lang.Object getDelegate() {
        return ((kotlin.reflect.KMutableProperty0) getReflected()).getDelegate();
    }
}
