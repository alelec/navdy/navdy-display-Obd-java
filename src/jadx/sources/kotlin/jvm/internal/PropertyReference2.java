package kotlin.jvm.internal;

public abstract class PropertyReference2 extends kotlin.jvm.internal.PropertyReference implements kotlin.reflect.KProperty2 {
    /* access modifiers changed from: protected */
    public kotlin.reflect.KCallable computeReflected() {
        return kotlin.jvm.internal.Reflection.property2(this);
    }

    public java.lang.Object invoke(java.lang.Object receiver1, java.lang.Object receiver2) {
        return get(receiver1, receiver2);
    }

    public kotlin.reflect.KProperty2.Getter getGetter() {
        return ((kotlin.reflect.KProperty2) getReflected()).getGetter();
    }

    @kotlin.SinceKotlin(version = "1.1")
    public java.lang.Object getDelegate(java.lang.Object receiver1, java.lang.Object receiver2) {
        return ((kotlin.reflect.KProperty2) getReflected()).getDelegate(receiver1, receiver2);
    }
}
