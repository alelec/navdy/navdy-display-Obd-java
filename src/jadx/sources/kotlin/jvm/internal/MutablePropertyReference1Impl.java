package kotlin.jvm.internal;

public class MutablePropertyReference1Impl extends kotlin.jvm.internal.MutablePropertyReference1 {
    private final java.lang.String name;
    private final kotlin.reflect.KDeclarationContainer owner;
    private final java.lang.String signature;

    public MutablePropertyReference1Impl(kotlin.reflect.KDeclarationContainer owner2, java.lang.String name2, java.lang.String signature2) {
        this.owner = owner2;
        this.name = name2;
        this.signature = signature2;
    }

    public kotlin.reflect.KDeclarationContainer getOwner() {
        return this.owner;
    }

    public java.lang.String getName() {
        return this.name;
    }

    public java.lang.String getSignature() {
        return this.signature;
    }

    public java.lang.Object get(java.lang.Object receiver) {
        return getGetter().call(receiver);
    }

    public void set(java.lang.Object receiver, java.lang.Object value) {
        getSetter().call(receiver, value);
    }
}
