package kotlin.jvm.internal;

public class Intrinsics {
    private Intrinsics() {
    }

    public static java.lang.String stringPlus(java.lang.String self, java.lang.Object other) {
        return self + other;
    }

    public static void checkNotNull(java.lang.Object object) {
        if (object == null) {
            throwNpe();
        }
    }

    public static void checkNotNull(java.lang.Object object, java.lang.String message) {
        if (object == null) {
            throwNpe(message);
        }
    }

    public static void throwNpe() {
        throw ((kotlin.KotlinNullPointerException) sanitizeStackTrace(new kotlin.KotlinNullPointerException()));
    }

    public static void throwNpe(java.lang.String message) {
        throw ((kotlin.KotlinNullPointerException) sanitizeStackTrace(new kotlin.KotlinNullPointerException(message)));
    }

    public static void throwUninitializedProperty(java.lang.String message) {
        throw ((kotlin.UninitializedPropertyAccessException) sanitizeStackTrace(new kotlin.UninitializedPropertyAccessException(message)));
    }

    public static void throwUninitializedPropertyAccessException(java.lang.String propertyName) {
        throwUninitializedProperty("lateinit property " + propertyName + " has not been initialized");
    }

    public static void throwAssert() {
        throw ((java.lang.AssertionError) sanitizeStackTrace(new java.lang.AssertionError()));
    }

    public static void throwAssert(java.lang.String message) {
        throw ((java.lang.AssertionError) sanitizeStackTrace(new java.lang.AssertionError(message)));
    }

    public static void throwIllegalArgument() {
        throw ((java.lang.IllegalArgumentException) sanitizeStackTrace(new java.lang.IllegalArgumentException()));
    }

    public static void throwIllegalArgument(java.lang.String message) {
        throw ((java.lang.IllegalArgumentException) sanitizeStackTrace(new java.lang.IllegalArgumentException(message)));
    }

    public static void throwIllegalState() {
        throw ((java.lang.IllegalStateException) sanitizeStackTrace(new java.lang.IllegalStateException()));
    }

    public static void throwIllegalState(java.lang.String message) {
        throw ((java.lang.IllegalStateException) sanitizeStackTrace(new java.lang.IllegalStateException(message)));
    }

    public static void checkExpressionValueIsNotNull(java.lang.Object value, java.lang.String expression) {
        if (value == null) {
            throw ((java.lang.IllegalStateException) sanitizeStackTrace(new java.lang.IllegalStateException(expression + " must not be null")));
        }
    }

    public static void checkNotNullExpressionValue(java.lang.Object value, java.lang.String message) {
        if (value == null) {
            throw ((java.lang.IllegalStateException) sanitizeStackTrace(new java.lang.IllegalStateException(message)));
        }
    }

    public static void checkReturnedValueIsNotNull(java.lang.Object value, java.lang.String className, java.lang.String methodName) {
        if (value == null) {
            throw ((java.lang.IllegalStateException) sanitizeStackTrace(new java.lang.IllegalStateException("Method specified as non-null returned null: " + className + "." + methodName)));
        }
    }

    public static void checkReturnedValueIsNotNull(java.lang.Object value, java.lang.String message) {
        if (value == null) {
            throw ((java.lang.IllegalStateException) sanitizeStackTrace(new java.lang.IllegalStateException(message)));
        }
    }

    public static void checkFieldIsNotNull(java.lang.Object value, java.lang.String className, java.lang.String fieldName) {
        if (value == null) {
            throw ((java.lang.IllegalStateException) sanitizeStackTrace(new java.lang.IllegalStateException("Field specified as non-null is null: " + className + "." + fieldName)));
        }
    }

    public static void checkFieldIsNotNull(java.lang.Object value, java.lang.String message) {
        if (value == null) {
            throw ((java.lang.IllegalStateException) sanitizeStackTrace(new java.lang.IllegalStateException(message)));
        }
    }

    public static void checkParameterIsNotNull(java.lang.Object value, java.lang.String paramName) {
        if (value == null) {
            throwParameterIsNullException(paramName);
        }
    }

    public static void checkNotNullParameter(java.lang.Object value, java.lang.String message) {
        if (value == null) {
            throw ((java.lang.IllegalArgumentException) sanitizeStackTrace(new java.lang.IllegalArgumentException(message)));
        }
    }

    private static void throwParameterIsNullException(java.lang.String paramName) {
        java.lang.StackTraceElement caller = java.lang.Thread.currentThread().getStackTrace()[3];
        java.lang.String className = caller.getClassName();
        throw ((java.lang.IllegalArgumentException) sanitizeStackTrace(new java.lang.IllegalArgumentException("Parameter specified as non-null is null: method " + className + "." + caller.getMethodName() + ", parameter " + paramName)));
    }

    public static int compare(long thisVal, long anotherVal) {
        if (thisVal < anotherVal) {
            return -1;
        }
        return thisVal == anotherVal ? 0 : 1;
    }

    public static int compare(int thisVal, int anotherVal) {
        if (thisVal < anotherVal) {
            return -1;
        }
        return thisVal == anotherVal ? 0 : 1;
    }

    public static boolean areEqual(java.lang.Object first, java.lang.Object second) {
        if (first == null) {
            return second == null;
        }
        return first.equals(second);
    }

    @kotlin.SinceKotlin(version = "1.1")
    public static boolean areEqual(java.lang.Double first, java.lang.Double second) {
        return first == null ? second == null : second != null && first.doubleValue() == second.doubleValue();
    }

    @kotlin.SinceKotlin(version = "1.1")
    public static boolean areEqual(java.lang.Double first, double second) {
        return first != null && first.doubleValue() == second;
    }

    @kotlin.SinceKotlin(version = "1.1")
    public static boolean areEqual(double first, java.lang.Double second) {
        return second != null && first == second.doubleValue();
    }

    @kotlin.SinceKotlin(version = "1.1")
    public static boolean areEqual(java.lang.Float first, java.lang.Float second) {
        return first == null ? second == null : second != null && first.floatValue() == second.floatValue();
    }

    @kotlin.SinceKotlin(version = "1.1")
    public static boolean areEqual(java.lang.Float first, float second) {
        return first != null && first.floatValue() == second;
    }

    @kotlin.SinceKotlin(version = "1.1")
    public static boolean areEqual(float first, java.lang.Float second) {
        return second != null && first == second.floatValue();
    }

    public static void throwUndefinedForReified() {
        throwUndefinedForReified("This function has a reified type parameter and thus can only be inlined at compilation time, not called directly.");
    }

    public static void throwUndefinedForReified(java.lang.String message) {
        throw new java.lang.UnsupportedOperationException(message);
    }

    public static void reifiedOperationMarker(int id, java.lang.String typeParameterIdentifier) {
        throwUndefinedForReified();
    }

    public static void reifiedOperationMarker(int id, java.lang.String typeParameterIdentifier, java.lang.String message) {
        throwUndefinedForReified(message);
    }

    public static void needClassReification() {
        throwUndefinedForReified();
    }

    public static void needClassReification(java.lang.String message) {
        throwUndefinedForReified(message);
    }

    public static void checkHasClass(java.lang.String internalName) throws java.lang.ClassNotFoundException {
        java.lang.String fqName = internalName.replace('/', ch.qos.logback.core.CoreConstants.DOT);
        try {
            java.lang.Class.forName(fqName);
        } catch (java.lang.ClassNotFoundException e) {
            throw ((java.lang.ClassNotFoundException) sanitizeStackTrace(new java.lang.ClassNotFoundException("Class " + fqName + " is not found. Please update the Kotlin runtime to the latest version", e)));
        }
    }

    public static void checkHasClass(java.lang.String internalName, java.lang.String requiredVersion) throws java.lang.ClassNotFoundException {
        java.lang.String fqName = internalName.replace('/', ch.qos.logback.core.CoreConstants.DOT);
        try {
            java.lang.Class.forName(fqName);
        } catch (java.lang.ClassNotFoundException e) {
            throw ((java.lang.ClassNotFoundException) sanitizeStackTrace(new java.lang.ClassNotFoundException("Class " + fqName + " is not found: this code requires the Kotlin runtime of version at least " + requiredVersion, e)));
        }
    }

    private static <T extends java.lang.Throwable> T sanitizeStackTrace(T throwable) {
        return sanitizeStackTrace(throwable, kotlin.jvm.internal.Intrinsics.class.getName());
    }

    static <T extends java.lang.Throwable> T sanitizeStackTrace(T throwable, java.lang.String classNameToDrop) {
        java.lang.StackTraceElement[] stackTrace = throwable.getStackTrace();
        int size = stackTrace.length;
        int lastIntrinsic = -1;
        for (int i = 0; i < size; i++) {
            if (classNameToDrop.equals(stackTrace[i].getClassName())) {
                lastIntrinsic = i;
            }
        }
        java.util.List<java.lang.StackTraceElement> list = java.util.Arrays.asList(stackTrace).subList(lastIntrinsic + 1, size);
        throwable.setStackTrace((java.lang.StackTraceElement[]) list.toArray(new java.lang.StackTraceElement[list.size()]));
        return throwable;
    }
}
