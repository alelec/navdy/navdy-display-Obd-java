package kotlin.jvm.internal;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\b\u0017\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\n\u0010\u0003\u001a\u0004\u0018\u00010\u0004H\u0016J\b\u0010\u0005\u001a\u00020\u0006H\u0016J\u0012\u0010\u0007\u001a\u00020\b2\b\u0010\t\u001a\u0004\u0018\u00010\u0004H\u0016\u00a8\u0006\n"}, d2 = {"Lkotlin/jvm/internal/MutableLocalVariableReference;", "Lkotlin/jvm/internal/MutablePropertyReference0;", "()V", "get", "", "getOwner", "Lkotlin/reflect/KDeclarationContainer;", "set", "", "value", "kotlin-runtime"}, k = 1, mv = {1, 1, 6})
@kotlin.SinceKotlin(version = "1.1")
/* compiled from: localVariableReferences.kt */
public class MutableLocalVariableReference extends kotlin.jvm.internal.MutablePropertyReference0 {
    @org.jetbrains.annotations.NotNull
    public kotlin.reflect.KDeclarationContainer getOwner() {
        kotlin.jvm.internal.LocalVariableReferencesKt.notSupportedError();
        throw null;
    }

    @org.jetbrains.annotations.Nullable
    public java.lang.Object get() {
        kotlin.jvm.internal.LocalVariableReferencesKt.notSupportedError();
        throw null;
    }

    public void set(@org.jetbrains.annotations.Nullable java.lang.Object value) {
        kotlin.jvm.internal.LocalVariableReferencesKt.notSupportedError();
        throw null;
    }
}
