package kotlin.jvm.internal;

public abstract class PropertyReference extends kotlin.jvm.internal.CallableReference implements kotlin.reflect.KProperty {
    public PropertyReference() {
    }

    @kotlin.SinceKotlin(version = "1.1")
    public PropertyReference(java.lang.Object receiver) {
        super(receiver);
    }

    /* access modifiers changed from: protected */
    @kotlin.SinceKotlin(version = "1.1")
    public kotlin.reflect.KProperty getReflected() {
        return (kotlin.reflect.KProperty) super.getReflected();
    }

    @kotlin.SinceKotlin(version = "1.1")
    public boolean isLateinit() {
        return getReflected().isLateinit();
    }

    @kotlin.SinceKotlin(version = "1.1")
    public boolean isConst() {
        return getReflected().isConst();
    }

    public boolean equals(java.lang.Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof kotlin.jvm.internal.PropertyReference) {
            kotlin.jvm.internal.PropertyReference other = (kotlin.jvm.internal.PropertyReference) obj;
            if (!getOwner().equals(other.getOwner()) || !getName().equals(other.getName()) || !getSignature().equals(other.getSignature()) || !kotlin.jvm.internal.Intrinsics.areEqual(getBoundReceiver(), other.getBoundReceiver())) {
                return false;
            }
            return true;
        } else if (obj instanceof kotlin.reflect.KProperty) {
            return obj.equals(compute());
        } else {
            return false;
        }
    }

    public int hashCode() {
        return (((getOwner().hashCode() * 31) + getName().hashCode()) * 31) + getSignature().hashCode();
    }

    public java.lang.String toString() {
        kotlin.reflect.KCallable reflected = compute();
        if (reflected != this) {
            return reflected.toString();
        }
        return "property " + getName() + " (Kotlin reflection is not available)";
    }
}
