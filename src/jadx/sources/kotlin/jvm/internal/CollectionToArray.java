package kotlin.jvm.internal;

public class CollectionToArray {
    private static final java.lang.Object[] EMPTY_OBJECT_ARRAY = new java.lang.Object[0];

    public static java.lang.Object[] toArray(java.util.Collection<?> collection) {
        int size = collection.size();
        if (size == 0) {
            return EMPTY_OBJECT_ARRAY;
        }
        java.lang.Object[] r = new java.lang.Object[size];
        java.util.Iterator<?> it = collection.iterator();
        for (int i = 0; i < size; i++) {
            if (!it.hasNext()) {
                return java.util.Arrays.copyOf(r, i);
            }
            r[i] = it.next();
        }
        return it.hasNext() ? finishToArray(r, it) : r;
    }

    public static <T, E> T[] toArray(java.util.Collection<E> collection, T[] a) {
        T[] r;
        int size = collection.size();
        if (a.length >= size) {
            r = a;
        } else {
            r = (java.lang.Object[]) java.lang.reflect.Array.newInstance(a.getClass().getComponentType(), size);
        }
        java.util.Iterator<E> it = collection.iterator();
        int i = 0;
        while (i < r.length) {
            if (it.hasNext()) {
                r[i] = it.next();
                i++;
            } else if (a != r) {
                return java.util.Arrays.copyOf(r, i);
            } else {
                r[i] = null;
                return r;
            }
        }
        return it.hasNext() ? finishToArray(r, it) : r;
    }

    private static <T> T[] finishToArray(T[] r, java.util.Iterator<?> it) {
        int i = r.length;
        while (it.hasNext()) {
            int cap = r.length;
            if (i == cap) {
                int newCap = ((cap / 2) + 1) * 3;
                if (newCap <= cap) {
                    if (cap == Integer.MAX_VALUE) {
                        throw new java.lang.OutOfMemoryError("Required array size too large");
                    }
                    newCap = Integer.MAX_VALUE;
                }
                r = java.util.Arrays.copyOf(r, newCap);
            }
            int i2 = i + 1;
            r[i] = it.next();
            i = i2;
        }
        return i == r.length ? r : java.util.Arrays.copyOf(r, i);
    }

    private CollectionToArray() {
    }
}
