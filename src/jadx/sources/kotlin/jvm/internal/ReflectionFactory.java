package kotlin.jvm.internal;

public class ReflectionFactory {
    private static final java.lang.String KOTLIN_JVM_FUNCTIONS = "kotlin.jvm.functions.";

    public kotlin.reflect.KClass createKotlinClass(java.lang.Class javaClass) {
        return new kotlin.jvm.internal.ClassReference(javaClass);
    }

    public kotlin.reflect.KClass createKotlinClass(java.lang.Class javaClass, java.lang.String internalName) {
        return new kotlin.jvm.internal.ClassReference(javaClass);
    }

    public kotlin.reflect.KDeclarationContainer getOrCreateKotlinPackage(java.lang.Class javaClass, java.lang.String moduleName) {
        return new kotlin.jvm.internal.PackageReference(javaClass, moduleName);
    }

    public kotlin.reflect.KClass getOrCreateKotlinClass(java.lang.Class javaClass) {
        return new kotlin.jvm.internal.ClassReference(javaClass);
    }

    public kotlin.reflect.KClass getOrCreateKotlinClass(java.lang.Class javaClass, java.lang.String internalName) {
        return new kotlin.jvm.internal.ClassReference(javaClass);
    }

    @kotlin.SinceKotlin(version = "1.1")
    public java.lang.String renderLambdaToString(kotlin.jvm.internal.Lambda lambda) {
        java.lang.String result = lambda.getClass().getGenericInterfaces()[0].toString();
        return result.startsWith(KOTLIN_JVM_FUNCTIONS) ? result.substring(KOTLIN_JVM_FUNCTIONS.length()) : result;
    }

    public kotlin.reflect.KFunction function(kotlin.jvm.internal.FunctionReference f) {
        return f;
    }

    public kotlin.reflect.KProperty0 property0(kotlin.jvm.internal.PropertyReference0 p) {
        return p;
    }

    public kotlin.reflect.KMutableProperty0 mutableProperty0(kotlin.jvm.internal.MutablePropertyReference0 p) {
        return p;
    }

    public kotlin.reflect.KProperty1 property1(kotlin.jvm.internal.PropertyReference1 p) {
        return p;
    }

    public kotlin.reflect.KMutableProperty1 mutableProperty1(kotlin.jvm.internal.MutablePropertyReference1 p) {
        return p;
    }

    public kotlin.reflect.KProperty2 property2(kotlin.jvm.internal.PropertyReference2 p) {
        return p;
    }

    public kotlin.reflect.KMutableProperty2 mutableProperty2(kotlin.jvm.internal.MutablePropertyReference2 p) {
        return p;
    }
}
