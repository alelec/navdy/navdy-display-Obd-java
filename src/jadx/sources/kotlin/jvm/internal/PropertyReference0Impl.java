package kotlin.jvm.internal;

public class PropertyReference0Impl extends kotlin.jvm.internal.PropertyReference0 {
    private final java.lang.String name;
    private final kotlin.reflect.KDeclarationContainer owner;
    private final java.lang.String signature;

    public PropertyReference0Impl(kotlin.reflect.KDeclarationContainer owner2, java.lang.String name2, java.lang.String signature2) {
        this.owner = owner2;
        this.name = name2;
        this.signature = signature2;
    }

    public kotlin.reflect.KDeclarationContainer getOwner() {
        return this.owner;
    }

    public java.lang.String getName() {
        return this.name;
    }

    public java.lang.String getSignature() {
        return this.signature;
    }

    public java.lang.Object get() {
        return getGetter().call(new java.lang.Object[0]);
    }
}
