package kotlin.jvm.internal;

public abstract class CallableReference implements kotlin.reflect.KCallable, java.io.Serializable {
    @kotlin.SinceKotlin(version = "1.1")
    public static final java.lang.Object NO_RECEIVER = kotlin.jvm.internal.CallableReference.NoReceiver.INSTANCE;
    @kotlin.SinceKotlin(version = "1.1")
    protected final java.lang.Object receiver;
    private transient kotlin.reflect.KCallable reflected;

    @kotlin.SinceKotlin(version = "1.2")
    private static class NoReceiver implements java.io.Serializable {
        /* access modifiers changed from: private */
        public static final kotlin.jvm.internal.CallableReference.NoReceiver INSTANCE = new kotlin.jvm.internal.CallableReference.NoReceiver();

        private NoReceiver() {
        }

        private java.lang.Object readResolve() throws java.io.ObjectStreamException {
            return INSTANCE;
        }
    }

    /* access modifiers changed from: protected */
    public abstract kotlin.reflect.KCallable computeReflected();

    public CallableReference() {
        this(NO_RECEIVER);
    }

    @kotlin.SinceKotlin(version = "1.1")
    protected CallableReference(java.lang.Object receiver2) {
        this.receiver = receiver2;
    }

    @kotlin.SinceKotlin(version = "1.1")
    public java.lang.Object getBoundReceiver() {
        return this.receiver;
    }

    @kotlin.SinceKotlin(version = "1.1")
    public kotlin.reflect.KCallable compute() {
        kotlin.reflect.KCallable result = this.reflected;
        if (result != null) {
            return result;
        }
        kotlin.reflect.KCallable result2 = computeReflected();
        this.reflected = result2;
        return result2;
    }

    /* access modifiers changed from: protected */
    @kotlin.SinceKotlin(version = "1.1")
    public kotlin.reflect.KCallable getReflected() {
        kotlin.reflect.KCallable result = compute();
        if (result != this) {
            return result;
        }
        throw new kotlin.jvm.KotlinReflectionNotSupportedError();
    }

    public kotlin.reflect.KDeclarationContainer getOwner() {
        throw new java.lang.AbstractMethodError();
    }

    public java.lang.String getName() {
        throw new java.lang.AbstractMethodError();
    }

    public java.lang.String getSignature() {
        throw new java.lang.AbstractMethodError();
    }

    public java.util.List<kotlin.reflect.KParameter> getParameters() {
        return getReflected().getParameters();
    }

    public kotlin.reflect.KType getReturnType() {
        return getReflected().getReturnType();
    }

    public java.util.List<java.lang.annotation.Annotation> getAnnotations() {
        return getReflected().getAnnotations();
    }

    @org.jetbrains.annotations.NotNull
    @kotlin.SinceKotlin(version = "1.1")
    public java.util.List<kotlin.reflect.KTypeParameter> getTypeParameters() {
        return getReflected().getTypeParameters();
    }

    public java.lang.Object call(@org.jetbrains.annotations.NotNull java.lang.Object... args) {
        return getReflected().call(args);
    }

    public java.lang.Object callBy(@org.jetbrains.annotations.NotNull java.util.Map args) {
        return getReflected().callBy(args);
    }

    @kotlin.SinceKotlin(version = "1.1")
    @org.jetbrains.annotations.Nullable
    public kotlin.reflect.KVisibility getVisibility() {
        return getReflected().getVisibility();
    }

    @kotlin.SinceKotlin(version = "1.1")
    public boolean isFinal() {
        return getReflected().isFinal();
    }

    @kotlin.SinceKotlin(version = "1.1")
    public boolean isOpen() {
        return getReflected().isOpen();
    }

    @kotlin.SinceKotlin(version = "1.1")
    public boolean isAbstract() {
        return getReflected().isAbstract();
    }
}
