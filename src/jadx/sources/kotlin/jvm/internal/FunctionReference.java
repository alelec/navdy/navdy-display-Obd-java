package kotlin.jvm.internal;

public class FunctionReference extends kotlin.jvm.internal.CallableReference implements kotlin.jvm.internal.FunctionBase, kotlin.reflect.KFunction {
    private final int arity;

    public FunctionReference(int arity2) {
        this.arity = arity2;
    }

    @kotlin.SinceKotlin(version = "1.1")
    public FunctionReference(int arity2, java.lang.Object receiver) {
        super(receiver);
        this.arity = arity2;
    }

    public int getArity() {
        return this.arity;
    }

    /* access modifiers changed from: protected */
    @kotlin.SinceKotlin(version = "1.1")
    public kotlin.reflect.KFunction getReflected() {
        return (kotlin.reflect.KFunction) super.getReflected();
    }

    /* access modifiers changed from: protected */
    @kotlin.SinceKotlin(version = "1.1")
    public kotlin.reflect.KCallable computeReflected() {
        return kotlin.jvm.internal.Reflection.function(this);
    }

    @kotlin.SinceKotlin(version = "1.1")
    public boolean isInline() {
        return getReflected().isInline();
    }

    @kotlin.SinceKotlin(version = "1.1")
    public boolean isExternal() {
        return getReflected().isExternal();
    }

    @kotlin.SinceKotlin(version = "1.1")
    public boolean isOperator() {
        return getReflected().isOperator();
    }

    @kotlin.SinceKotlin(version = "1.1")
    public boolean isInfix() {
        return getReflected().isInfix();
    }

    @kotlin.SinceKotlin(version = "1.1")
    public boolean isSuspend() {
        return getReflected().isSuspend();
    }

    public boolean equals(java.lang.Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof kotlin.jvm.internal.FunctionReference) {
            kotlin.jvm.internal.FunctionReference other = (kotlin.jvm.internal.FunctionReference) obj;
            if (!getOwner().equals(other.getOwner()) || !getName().equals(other.getName()) || !getSignature().equals(other.getSignature()) || !kotlin.jvm.internal.Intrinsics.areEqual(getBoundReceiver(), other.getBoundReceiver())) {
                return false;
            }
            return true;
        } else if (obj instanceof kotlin.reflect.KFunction) {
            return obj.equals(compute());
        } else {
            return false;
        }
    }

    public int hashCode() {
        return (((getOwner().hashCode() * 31) + getName().hashCode()) * 31) + getSignature().hashCode();
    }

    public java.lang.String toString() {
        kotlin.reflect.KCallable reflected = compute();
        if (reflected != this) {
            return reflected.toString();
        }
        if ("<init>".equals(getName())) {
            return "constructor (Kotlin reflection is not available)";
        }
        return "function " + getName() + " (Kotlin reflection is not available)";
    }
}
