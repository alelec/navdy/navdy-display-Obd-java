package kotlin.jvm.internal;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000p\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0010\u001b\n\u0002\b\u0003\n\u0002\u0010\u001e\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0012\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0001\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u00012\u00020\u0003B\u0011\u0012\n\u0010\u0004\u001a\u0006\u0012\u0002\b\u00030\u0005\u00a2\u0006\u0002\u0010\u0006J\u0013\u0010?\u001a\u00020\u00122\b\u0010@\u001a\u0004\u0018\u00010\u0002H\u0096\u0002J\b\u0010A\u001a\u00020BH\u0002J\b\u0010C\u001a\u00020DH\u0016J\u0012\u0010E\u001a\u00020\u00122\b\u0010F\u001a\u0004\u0018\u00010\u0002H\u0017J\b\u0010G\u001a\u00020-H\u0016R\u001a\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\b\n\u0010\u000bR \u0010\f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00020\u000e0\r8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\b\u000f\u0010\u0010R\u001a\u0010\u0011\u001a\u00020\u00128VX\u0097\u0004\u00a2\u0006\f\u0012\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0011\u0010\u0015R\u001a\u0010\u0016\u001a\u00020\u00128VX\u0097\u0004\u00a2\u0006\f\u0012\u0004\b\u0017\u0010\u0014\u001a\u0004\b\u0016\u0010\u0015R\u001a\u0010\u0018\u001a\u00020\u00128VX\u0097\u0004\u00a2\u0006\f\u0012\u0004\b\u0019\u0010\u0014\u001a\u0004\b\u0018\u0010\u0015R\u001a\u0010\u001a\u001a\u00020\u00128VX\u0097\u0004\u00a2\u0006\f\u0012\u0004\b\u001b\u0010\u0014\u001a\u0004\b\u001a\u0010\u0015R\u001a\u0010\u001c\u001a\u00020\u00128VX\u0097\u0004\u00a2\u0006\f\u0012\u0004\b\u001d\u0010\u0014\u001a\u0004\b\u001c\u0010\u0015R\u001a\u0010\u001e\u001a\u00020\u00128VX\u0097\u0004\u00a2\u0006\f\u0012\u0004\b\u001f\u0010\u0014\u001a\u0004\b\u001e\u0010\u0015R\u001a\u0010 \u001a\u00020\u00128VX\u0097\u0004\u00a2\u0006\f\u0012\u0004\b!\u0010\u0014\u001a\u0004\b \u0010\u0015R\u0018\u0010\u0004\u001a\u0006\u0012\u0002\b\u00030\u0005X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\"\u0010#R\u001e\u0010$\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030%0\r8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\b&\u0010\u0010R\u001e\u0010'\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u00010\r8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\b(\u0010\u0010R\u0016\u0010)\u001a\u0004\u0018\u00010\u00028VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\b*\u0010+R\u0016\u0010,\u001a\u0004\u0018\u00010-8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\b.\u0010/R\u0016\u00100\u001a\u0004\u0018\u00010-8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\b1\u0010/R \u00102\u001a\b\u0012\u0004\u0012\u0002030\b8VX\u0097\u0004\u00a2\u0006\f\u0012\u0004\b4\u0010\u0014\u001a\u0004\b5\u0010\u000bR \u00106\u001a\b\u0012\u0004\u0012\u0002070\b8VX\u0097\u0004\u00a2\u0006\f\u0012\u0004\b8\u0010\u0014\u001a\u0004\b9\u0010\u000bR\u001c\u0010:\u001a\u0004\u0018\u00010;8VX\u0097\u0004\u00a2\u0006\f\u0012\u0004\b<\u0010\u0014\u001a\u0004\b=\u0010>\u00a8\u0006H"}, d2 = {"Lkotlin/jvm/internal/ClassReference;", "Lkotlin/reflect/KClass;", "", "Lkotlin/jvm/internal/ClassBasedDeclarationContainer;", "jClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)V", "annotations", "", "", "getAnnotations", "()Ljava/util/List;", "constructors", "", "Lkotlin/reflect/KFunction;", "getConstructors", "()Ljava/util/Collection;", "isAbstract", "", "isAbstract$annotations", "()V", "()Z", "isCompanion", "isCompanion$annotations", "isData", "isData$annotations", "isFinal", "isFinal$annotations", "isInner", "isInner$annotations", "isOpen", "isOpen$annotations", "isSealed", "isSealed$annotations", "getJClass", "()Ljava/lang/Class;", "members", "Lkotlin/reflect/KCallable;", "getMembers", "nestedClasses", "getNestedClasses", "objectInstance", "getObjectInstance", "()Ljava/lang/Object;", "qualifiedName", "", "getQualifiedName", "()Ljava/lang/String;", "simpleName", "getSimpleName", "supertypes", "Lkotlin/reflect/KType;", "supertypes$annotations", "getSupertypes", "typeParameters", "Lkotlin/reflect/KTypeParameter;", "typeParameters$annotations", "getTypeParameters", "visibility", "Lkotlin/reflect/KVisibility;", "visibility$annotations", "getVisibility", "()Lkotlin/reflect/KVisibility;", "equals", "other", "error", "", "hashCode", "", "isInstance", "value", "toString", "kotlin-runtime"}, k = 1, mv = {1, 1, 6})
/* compiled from: ClassReference.kt */
public final class ClassReference implements kotlin.reflect.KClass<java.lang.Object>, kotlin.jvm.internal.ClassBasedDeclarationContainer {
    @org.jetbrains.annotations.NotNull
    private final java.lang.Class<?> jClass;

    @kotlin.SinceKotlin(version = "1.1")
    public static /* synthetic */ void isAbstract$annotations() {
    }

    @kotlin.SinceKotlin(version = "1.1")
    public static /* synthetic */ void isCompanion$annotations() {
    }

    @kotlin.SinceKotlin(version = "1.1")
    public static /* synthetic */ void isData$annotations() {
    }

    @kotlin.SinceKotlin(version = "1.1")
    public static /* synthetic */ void isFinal$annotations() {
    }

    @kotlin.SinceKotlin(version = "1.1")
    public static /* synthetic */ void isInner$annotations() {
    }

    @kotlin.SinceKotlin(version = "1.1")
    public static /* synthetic */ void isOpen$annotations() {
    }

    @kotlin.SinceKotlin(version = "1.1")
    public static /* synthetic */ void isSealed$annotations() {
    }

    @kotlin.SinceKotlin(version = "1.1")
    public static /* synthetic */ void supertypes$annotations() {
    }

    @kotlin.SinceKotlin(version = "1.1")
    public static /* synthetic */ void typeParameters$annotations() {
    }

    @kotlin.SinceKotlin(version = "1.1")
    public static /* synthetic */ void visibility$annotations() {
    }

    public ClassReference(@org.jetbrains.annotations.NotNull java.lang.Class<?> jClass2) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(jClass2, "jClass");
        this.jClass = jClass2;
    }

    @org.jetbrains.annotations.NotNull
    public java.lang.Class<?> getJClass() {
        return this.jClass;
    }

    @org.jetbrains.annotations.Nullable
    public java.lang.String getSimpleName() {
        error();
        throw null;
    }

    @org.jetbrains.annotations.Nullable
    public java.lang.String getQualifiedName() {
        error();
        throw null;
    }

    @org.jetbrains.annotations.NotNull
    public java.util.Collection<kotlin.reflect.KCallable<?>> getMembers() {
        error();
        throw null;
    }

    @org.jetbrains.annotations.NotNull
    public java.util.Collection<kotlin.reflect.KFunction<java.lang.Object>> getConstructors() {
        error();
        throw null;
    }

    @org.jetbrains.annotations.NotNull
    public java.util.Collection<kotlin.reflect.KClass<?>> getNestedClasses() {
        error();
        throw null;
    }

    @org.jetbrains.annotations.NotNull
    public java.util.List<java.lang.annotation.Annotation> getAnnotations() {
        error();
        throw null;
    }

    @org.jetbrains.annotations.Nullable
    public java.lang.Object getObjectInstance() {
        error();
        throw null;
    }

    @kotlin.SinceKotlin(version = "1.1")
    public boolean isInstance(@org.jetbrains.annotations.Nullable java.lang.Object value) {
        error();
        throw null;
    }

    @org.jetbrains.annotations.NotNull
    public java.util.List<kotlin.reflect.KTypeParameter> getTypeParameters() {
        error();
        throw null;
    }

    @org.jetbrains.annotations.NotNull
    public java.util.List<kotlin.reflect.KType> getSupertypes() {
        error();
        throw null;
    }

    @org.jetbrains.annotations.Nullable
    public kotlin.reflect.KVisibility getVisibility() {
        error();
        throw null;
    }

    public boolean isFinal() {
        error();
        throw null;
    }

    public boolean isOpen() {
        error();
        throw null;
    }

    public boolean isAbstract() {
        error();
        throw null;
    }

    public boolean isSealed() {
        error();
        throw null;
    }

    public boolean isData() {
        error();
        throw null;
    }

    public boolean isInner() {
        error();
        throw null;
    }

    public boolean isCompanion() {
        error();
        throw null;
    }

    private final java.lang.Void error() {
        throw new kotlin.jvm.KotlinReflectionNotSupportedError();
    }

    public boolean equals(@org.jetbrains.annotations.Nullable java.lang.Object other) {
        return (other instanceof kotlin.jvm.internal.ClassReference) && kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) kotlin.jvm.JvmClassMappingKt.getJavaObjectType(this), (java.lang.Object) kotlin.jvm.JvmClassMappingKt.getJavaObjectType((kotlin.reflect.KClass) other));
    }

    public int hashCode() {
        return kotlin.jvm.JvmClassMappingKt.getJavaObjectType(this).hashCode();
    }

    @org.jetbrains.annotations.NotNull
    public java.lang.String toString() {
        return getJClass().toString() + " (Kotlin reflection is not available)";
    }
}
