package kotlin.jvm.internal;

public interface FunctionBase extends kotlin.Function, java.io.Serializable {
    int getArity();
}
