package kotlin.jvm.internal;

public class PropertyReference2Impl extends kotlin.jvm.internal.PropertyReference2 {
    private final java.lang.String name;
    private final kotlin.reflect.KDeclarationContainer owner;
    private final java.lang.String signature;

    public PropertyReference2Impl(kotlin.reflect.KDeclarationContainer owner2, java.lang.String name2, java.lang.String signature2) {
        this.owner = owner2;
        this.name = name2;
        this.signature = signature2;
    }

    public kotlin.reflect.KDeclarationContainer getOwner() {
        return this.owner;
    }

    public java.lang.String getName() {
        return this.name;
    }

    public java.lang.String getSignature() {
        return this.signature;
    }

    public java.lang.Object get(java.lang.Object receiver1, java.lang.Object receiver2) {
        return getGetter().call(receiver1, receiver2);
    }
}
