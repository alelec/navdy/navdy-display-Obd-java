package kotlin.jvm.internal;

public abstract class PropertyReference0 extends kotlin.jvm.internal.PropertyReference implements kotlin.reflect.KProperty0 {
    public PropertyReference0() {
    }

    @kotlin.SinceKotlin(version = "1.1")
    public PropertyReference0(java.lang.Object receiver) {
        super(receiver);
    }

    /* access modifiers changed from: protected */
    public kotlin.reflect.KCallable computeReflected() {
        return kotlin.jvm.internal.Reflection.property0(this);
    }

    public java.lang.Object invoke() {
        return get();
    }

    public kotlin.reflect.KProperty0.Getter getGetter() {
        return ((kotlin.reflect.KProperty0) getReflected()).getGetter();
    }

    @kotlin.SinceKotlin(version = "1.1")
    public java.lang.Object getDelegate() {
        return ((kotlin.reflect.KProperty0) getReflected()).getDelegate();
    }
}
