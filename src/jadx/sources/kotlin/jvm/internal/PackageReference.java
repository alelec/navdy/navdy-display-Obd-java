package kotlin.jvm.internal;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u001e\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0007\u0018\u00002\u00020\u0001B\u001b\u0012\n\u0010\u0002\u001a\u0006\u0012\u0002\b\u00030\u0003\u0012\b\b\u0001\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0011H\u0096\u0002J\b\u0010\u0012\u001a\u00020\u0013H\u0016J\b\u0010\u0014\u001a\u00020\u0005H\u0016R\u0018\u0010\u0002\u001a\u0006\u0012\u0002\b\u00030\u0003X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u001e\u0010\t\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u000b0\n8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\b\f\u0010\rR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"}, d2 = {"Lkotlin/jvm/internal/PackageReference;", "Lkotlin/jvm/internal/ClassBasedDeclarationContainer;", "jClass", "Ljava/lang/Class;", "moduleName", "", "(Ljava/lang/Class;Ljava/lang/String;)V", "getJClass", "()Ljava/lang/Class;", "members", "", "Lkotlin/reflect/KCallable;", "getMembers", "()Ljava/util/Collection;", "equals", "", "other", "", "hashCode", "", "toString", "kotlin-runtime"}, k = 1, mv = {1, 1, 6})
@kotlin.SinceKotlin(version = "1.1")
/* compiled from: PackageReference.kt */
public final class PackageReference implements kotlin.jvm.internal.ClassBasedDeclarationContainer {
    @org.jetbrains.annotations.NotNull
    private final java.lang.Class<?> jClass;
    private final java.lang.String moduleName;

    public PackageReference(@org.jetbrains.annotations.NotNull java.lang.Class<?> jClass2, @org.jetbrains.annotations.NotNull java.lang.String moduleName2) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(jClass2, "jClass");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(moduleName2, "moduleName");
        this.jClass = jClass2;
        this.moduleName = moduleName2;
    }

    @org.jetbrains.annotations.NotNull
    public java.lang.Class<?> getJClass() {
        return this.jClass;
    }

    @org.jetbrains.annotations.NotNull
    public java.util.Collection<kotlin.reflect.KCallable<?>> getMembers() {
        throw new kotlin.jvm.KotlinReflectionNotSupportedError();
    }

    public boolean equals(@org.jetbrains.annotations.Nullable java.lang.Object other) {
        return (other instanceof kotlin.jvm.internal.PackageReference) && kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) getJClass(), (java.lang.Object) ((kotlin.jvm.internal.PackageReference) other).getJClass());
    }

    public int hashCode() {
        return getJClass().hashCode();
    }

    @org.jetbrains.annotations.NotNull
    public java.lang.String toString() {
        return getJClass().toString() + " (Kotlin reflection is not available)";
    }
}
