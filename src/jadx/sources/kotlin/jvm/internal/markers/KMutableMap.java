package kotlin.jvm.internal.markers;

public interface KMutableMap extends kotlin.jvm.internal.markers.KMappedMarker {

    public interface Entry extends kotlin.jvm.internal.markers.KMappedMarker {
    }
}
