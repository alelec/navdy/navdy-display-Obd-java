package kotlin.jvm.internal;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0016\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\t\n\u0000\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\bH\u0096\u0002J\b\u0010\t\u001a\u00020\nH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"}, d2 = {"Lkotlin/jvm/internal/ArrayLongIterator;", "Lkotlin/collections/LongIterator;", "array", "", "([J)V", "index", "", "hasNext", "", "nextLong", "", "kotlin-runtime"}, k = 1, mv = {1, 1, 6})
/* compiled from: ArrayIterators.kt */
final class ArrayLongIterator extends kotlin.collections.LongIterator {
    private final long[] array;
    private int index;

    public ArrayLongIterator(@org.jetbrains.annotations.NotNull long[] array2) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(array2, "array");
        this.array = array2;
    }

    public boolean hasNext() {
        return this.index < this.array.length;
    }

    public long nextLong() {
        long[] jArr = this.array;
        int i = this.index;
        this.index = i + 1;
        return jArr[i];
    }
}
