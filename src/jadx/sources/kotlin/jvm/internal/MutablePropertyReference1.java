package kotlin.jvm.internal;

public abstract class MutablePropertyReference1 extends kotlin.jvm.internal.MutablePropertyReference implements kotlin.reflect.KMutableProperty1 {
    public MutablePropertyReference1() {
    }

    @kotlin.SinceKotlin(version = "1.1")
    public MutablePropertyReference1(java.lang.Object receiver) {
        super(receiver);
    }

    /* access modifiers changed from: protected */
    public kotlin.reflect.KCallable computeReflected() {
        return kotlin.jvm.internal.Reflection.mutableProperty1(this);
    }

    public java.lang.Object invoke(java.lang.Object receiver) {
        return get(receiver);
    }

    public kotlin.reflect.KProperty1.Getter getGetter() {
        return ((kotlin.reflect.KMutableProperty1) getReflected()).getGetter();
    }

    public kotlin.reflect.KMutableProperty1.Setter getSetter() {
        return ((kotlin.reflect.KMutableProperty1) getReflected()).getSetter();
    }

    @kotlin.SinceKotlin(version = "1.1")
    public java.lang.Object getDelegate(java.lang.Object receiver) {
        return ((kotlin.reflect.KMutableProperty1) getReflected()).getDelegate(receiver);
    }
}
