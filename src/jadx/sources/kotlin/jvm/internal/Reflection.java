package kotlin.jvm.internal;

public class Reflection {
    private static final kotlin.reflect.KClass[] EMPTY_K_CLASS_ARRAY = new kotlin.reflect.KClass[0];
    static final java.lang.String REFLECTION_NOT_AVAILABLE = " (Kotlin reflection is not available)";
    private static final kotlin.jvm.internal.ReflectionFactory factory;

    static {
        kotlin.jvm.internal.ReflectionFactory impl;
        try {
            impl = (kotlin.jvm.internal.ReflectionFactory) java.lang.Class.forName("kotlin.reflect.jvm.internal.ReflectionFactoryImpl").newInstance();
        } catch (java.lang.ClassCastException e) {
            impl = null;
        } catch (java.lang.ClassNotFoundException e2) {
            impl = null;
        } catch (java.lang.InstantiationException e3) {
            impl = null;
        } catch (java.lang.IllegalAccessException e4) {
            impl = null;
        }
        if (impl == null) {
            impl = new kotlin.jvm.internal.ReflectionFactory();
        }
        factory = impl;
    }

    public static kotlin.reflect.KClass createKotlinClass(java.lang.Class javaClass) {
        return factory.createKotlinClass(javaClass);
    }

    public static kotlin.reflect.KClass createKotlinClass(java.lang.Class javaClass, java.lang.String internalName) {
        return factory.createKotlinClass(javaClass, internalName);
    }

    public static kotlin.reflect.KDeclarationContainer getOrCreateKotlinPackage(java.lang.Class javaClass, java.lang.String moduleName) {
        return factory.getOrCreateKotlinPackage(javaClass, moduleName);
    }

    public static kotlin.reflect.KClass getOrCreateKotlinClass(java.lang.Class javaClass) {
        return factory.getOrCreateKotlinClass(javaClass);
    }

    public static kotlin.reflect.KClass getOrCreateKotlinClass(java.lang.Class javaClass, java.lang.String internalName) {
        return factory.getOrCreateKotlinClass(javaClass, internalName);
    }

    public static kotlin.reflect.KClass[] getOrCreateKotlinClasses(java.lang.Class[] javaClasses) {
        int size = javaClasses.length;
        if (size == 0) {
            return EMPTY_K_CLASS_ARRAY;
        }
        kotlin.reflect.KClass[] kClasses = new kotlin.reflect.KClass[size];
        for (int i = 0; i < size; i++) {
            kClasses[i] = getOrCreateKotlinClass(javaClasses[i]);
        }
        return kClasses;
    }

    @kotlin.SinceKotlin(version = "1.1")
    public static java.lang.String renderLambdaToString(kotlin.jvm.internal.Lambda lambda) {
        return factory.renderLambdaToString(lambda);
    }

    public static kotlin.reflect.KFunction function(kotlin.jvm.internal.FunctionReference f) {
        return factory.function(f);
    }

    public static kotlin.reflect.KProperty0 property0(kotlin.jvm.internal.PropertyReference0 p) {
        return factory.property0(p);
    }

    public static kotlin.reflect.KMutableProperty0 mutableProperty0(kotlin.jvm.internal.MutablePropertyReference0 p) {
        return factory.mutableProperty0(p);
    }

    public static kotlin.reflect.KProperty1 property1(kotlin.jvm.internal.PropertyReference1 p) {
        return factory.property1(p);
    }

    public static kotlin.reflect.KMutableProperty1 mutableProperty1(kotlin.jvm.internal.MutablePropertyReference1 p) {
        return factory.mutableProperty1(p);
    }

    public static kotlin.reflect.KProperty2 property2(kotlin.jvm.internal.PropertyReference2 p) {
        return factory.property2(p);
    }

    public static kotlin.reflect.KMutableProperty2 mutableProperty2(kotlin.jvm.internal.MutablePropertyReference2 p) {
        return factory.mutableProperty2(p);
    }
}
