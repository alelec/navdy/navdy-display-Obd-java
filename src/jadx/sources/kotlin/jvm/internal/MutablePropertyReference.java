package kotlin.jvm.internal;

public abstract class MutablePropertyReference extends kotlin.jvm.internal.PropertyReference implements kotlin.reflect.KMutableProperty {
    public MutablePropertyReference() {
    }

    @kotlin.SinceKotlin(version = "1.1")
    public MutablePropertyReference(java.lang.Object receiver) {
        super(receiver);
    }
}
