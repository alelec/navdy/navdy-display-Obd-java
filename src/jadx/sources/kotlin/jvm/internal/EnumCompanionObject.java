package kotlin.jvm.internal;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\b\u00c0\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lkotlin/jvm/internal/EnumCompanionObject;", "", "()V", "kotlin-runtime"}, k = 1, mv = {1, 1, 6})
/* compiled from: PrimitiveCompanionObjects.kt */
public final class EnumCompanionObject {
    public static final kotlin.jvm.internal.EnumCompanionObject INSTANCE = null;

    static {
        new kotlin.jvm.internal.EnumCompanionObject();
    }

    private EnumCompanionObject() {
        INSTANCE = this;
    }
}
