package kotlin.jvm.internal;

public abstract class PropertyReference1 extends kotlin.jvm.internal.PropertyReference implements kotlin.reflect.KProperty1 {
    public PropertyReference1() {
    }

    @kotlin.SinceKotlin(version = "1.1")
    public PropertyReference1(java.lang.Object receiver) {
        super(receiver);
    }

    /* access modifiers changed from: protected */
    public kotlin.reflect.KCallable computeReflected() {
        return kotlin.jvm.internal.Reflection.property1(this);
    }

    public java.lang.Object invoke(java.lang.Object receiver) {
        return get(receiver);
    }

    public kotlin.reflect.KProperty1.Getter getGetter() {
        return ((kotlin.reflect.KProperty1) getReflected()).getGetter();
    }

    @kotlin.SinceKotlin(version = "1.1")
    public java.lang.Object getDelegate(java.lang.Object receiver) {
        return ((kotlin.reflect.KProperty1) getReflected()).getDelegate(receiver);
    }
}
