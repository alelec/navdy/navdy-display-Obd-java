package kotlin.jvm.internal;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\n\n\u0002\b\u0002\b\u00c0\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0006"}, d2 = {"Lkotlin/jvm/internal/ShortCompanionObject;", "", "()V", "MAX_VALUE", "", "MIN_VALUE", "kotlin-runtime"}, k = 1, mv = {1, 1, 6})
/* compiled from: PrimitiveCompanionObjects.kt */
public final class ShortCompanionObject {
    public static final kotlin.jvm.internal.ShortCompanionObject INSTANCE = null;
    public static final short MAX_VALUE = Short.MAX_VALUE;
    public static final short MIN_VALUE = Short.MIN_VALUE;

    static {
        new kotlin.jvm.internal.ShortCompanionObject();
    }

    private ShortCompanionObject() {
        INSTANCE = this;
    }
}
