package kotlin.jvm;

public class KotlinReflectionNotSupportedError extends java.lang.Error {
    public KotlinReflectionNotSupportedError() {
        super("Kotlin reflection implementation is not found at runtime. Make sure you have kotlin-reflect.jar in the classpath");
    }

    public KotlinReflectionNotSupportedError(java.lang.String message) {
        super(message);
    }

    public KotlinReflectionNotSupportedError(java.lang.String message, java.lang.Throwable cause) {
        super(message, cause);
    }

    public KotlinReflectionNotSupportedError(java.lang.Throwable cause) {
        super(cause);
    }
}
