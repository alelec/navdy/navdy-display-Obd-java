package kotlin.comparisons;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000f\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0004\b\u00c2\u0002\u0018\u00002\u001e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00030\u00020\u0001j\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00030\u0002`\u0004B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0005J$\u0010\u0006\u001a\u00020\u00072\f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00030\u00022\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002H\u0016J$\u0010\n\u001a\u001e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00030\u00020\u0001j\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00030\u0002`\u0004H\u0007\u00a8\u0006\u000b"}, d2 = {"Lkotlin/comparisons/ReverseOrderComparator;", "Ljava/util/Comparator;", "", "", "Lkotlin/Comparator;", "()V", "compare", "", "a", "b", "reversed", "kotlin-stdlib"}, k = 1, mv = {1, 1, 6})
/* compiled from: Comparisons.kt */
final class ReverseOrderComparator implements java.util.Comparator<java.lang.Comparable<? super java.lang.Object>> {
    public static final kotlin.comparisons.ReverseOrderComparator INSTANCE = null;

    static {
        new kotlin.comparisons.ReverseOrderComparator();
    }

    private ReverseOrderComparator() {
        INSTANCE = this;
    }

    public int compare(@org.jetbrains.annotations.NotNull java.lang.Comparable<java.lang.Object> a, @org.jetbrains.annotations.NotNull java.lang.Comparable<java.lang.Object> b) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "a");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(b, "b");
        return b.compareTo(a);
    }

    @org.jetbrains.annotations.NotNull
    public final java.util.Comparator<java.lang.Comparable<java.lang.Object>> reversed() {
        return kotlin.comparisons.NaturalOrderComparator.INSTANCE;
    }
}
