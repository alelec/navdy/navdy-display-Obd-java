package kotlin.comparisons;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0019\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0004*\u0001\u0000\b\n\u0018\u00002\u0012\u0012\u0004\u0012\u00028\u00000\u0001j\b\u0012\u0004\u0012\u00028\u0000`\u0002B\u0005\u00a2\u0006\u0002\u0010\u0003J\u001d\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00028\u00002\u0006\u0010\u0007\u001a\u00028\u0000H\u0016\u00a2\u0006\u0002\u0010\b\u00a8\u0006\t"}, d2 = {"kotlin/comparisons/ComparisonsKt__ComparisonsKt$compareBy$1", "Ljava/util/Comparator;", "Lkotlin/Comparator;", "([Lkotlin/jvm/functions/Function1;)V", "compare", "", "a", "b", "(Ljava/lang/Object;Ljava/lang/Object;)I", "kotlin-stdlib"}, k = 1, mv = {1, 1, 6})
/* compiled from: Comparisons.kt */
public final class ComparisonsKt__ComparisonsKt$compareBy$1 implements java.util.Comparator<T> {
    final /* synthetic */ kotlin.jvm.functions.Function1[] $selectors;

    ComparisonsKt__ComparisonsKt$compareBy$1(kotlin.jvm.functions.Function1[] $captured_local_variable$0) {
        this.$selectors = $captured_local_variable$0;
    }

    public int compare(T a, T b) {
        kotlin.jvm.functions.Function1[] function1Arr = this.$selectors;
        return kotlin.comparisons.ComparisonsKt.compareValuesBy(a, b, (kotlin.jvm.functions.Function1<? super T, ? extends java.lang.Comparable<?>>[]) (kotlin.jvm.functions.Function1[]) java.util.Arrays.copyOf(function1Arr, function1Arr.length));
    }
}
