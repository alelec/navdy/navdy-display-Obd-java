package kotlin.comparisons;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"kotlin/comparisons/ComparisonsKt__ComparisonsKt", "kotlin/comparisons/ComparisonsKt___ComparisonsKt"}, k = 4, mv = {1, 1, 6}, xi = 1)
public final class ComparisonsKt extends kotlin.comparisons.ComparisonsKt___ComparisonsKt {
    private ComparisonsKt() {
    }
}
