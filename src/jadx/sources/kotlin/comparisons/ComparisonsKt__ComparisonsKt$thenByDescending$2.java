package kotlin.comparisons;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0019\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0004*\u0001\u0000\b\n\u0018\u00002\u0012\u0012\u0004\u0012\u00028\u00000\u0001j\b\u0012\u0004\u0012\u00028\u0000`\u0002B\u0005\u00a2\u0006\u0002\u0010\u0003J\u001d\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00028\u00002\u0006\u0010\u0007\u001a\u00028\u0000H\u0016\u00a2\u0006\u0002\u0010\b\u00a8\u0006\t"}, d2 = {"kotlin/comparisons/ComparisonsKt__ComparisonsKt$thenByDescending$2", "Ljava/util/Comparator;", "Lkotlin/Comparator;", "(Ljava/util/Comparator;Ljava/util/Comparator;Lkotlin/jvm/functions/Function1;)V", "compare", "", "a", "b", "(Ljava/lang/Object;Ljava/lang/Object;)I", "kotlin-stdlib"}, k = 1, mv = {1, 1, 6})
/* compiled from: Comparisons.kt */
public final class ComparisonsKt__ComparisonsKt$thenByDescending$2 implements java.util.Comparator<T> {
    final /* synthetic */ java.util.Comparator $comparator;
    final /* synthetic */ kotlin.jvm.functions.Function1 $selector;
    final /* synthetic */ java.util.Comparator receiver$0;

    public ComparisonsKt__ComparisonsKt$thenByDescending$2(java.util.Comparator<T> $receiver, java.util.Comparator $captured_local_variable$1, kotlin.jvm.functions.Function1 $captured_local_variable$2) {
        this.receiver$0 = $receiver;
        this.$comparator = $captured_local_variable$1;
        this.$selector = $captured_local_variable$2;
    }

    public int compare(T a, T b) {
        int previousCompare = this.receiver$0.compare(a, b);
        return previousCompare != 0 ? previousCompare : this.$comparator.compare(this.$selector.invoke(b), this.$selector.invoke(a));
    }
}
