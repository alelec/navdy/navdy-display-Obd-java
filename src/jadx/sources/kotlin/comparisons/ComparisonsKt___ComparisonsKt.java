package kotlin.comparisons;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u00004\n\u0002\b\u0002\n\u0002\u0010\u000f\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0005\n\u0002\u0010\u0006\n\u0002\u0010\u0007\n\u0002\u0010\b\n\u0002\u0010\t\n\u0002\u0010\n\n\u0002\b\u0002\u001a-\u0010\u0000\u001a\u0002H\u0001\"\u000e\b\u0000\u0010\u0001*\b\u0012\u0004\u0012\u0002H\u00010\u00022\u0006\u0010\u0003\u001a\u0002H\u00012\u0006\u0010\u0004\u001a\u0002H\u0001H\u0007\u00a2\u0006\u0002\u0010\u0005\u001a5\u0010\u0000\u001a\u0002H\u0001\"\u000e\b\u0000\u0010\u0001*\b\u0012\u0004\u0012\u0002H\u00010\u00022\u0006\u0010\u0003\u001a\u0002H\u00012\u0006\u0010\u0004\u001a\u0002H\u00012\u0006\u0010\u0006\u001a\u0002H\u0001H\u0007\u00a2\u0006\u0002\u0010\u0007\u001aG\u0010\u0000\u001a\u0002H\u0001\"\u0004\b\u0000\u0010\u00012\u0006\u0010\u0003\u001a\u0002H\u00012\u0006\u0010\u0004\u001a\u0002H\u00012\u0006\u0010\u0006\u001a\u0002H\u00012\u001a\u0010\b\u001a\u0016\u0012\u0006\b\u0000\u0012\u0002H\u00010\tj\n\u0012\u0006\b\u0000\u0012\u0002H\u0001`\nH\u0007\u00a2\u0006\u0002\u0010\u000b\u001a?\u0010\u0000\u001a\u0002H\u0001\"\u0004\b\u0000\u0010\u00012\u0006\u0010\u0003\u001a\u0002H\u00012\u0006\u0010\u0004\u001a\u0002H\u00012\u001a\u0010\b\u001a\u0016\u0012\u0006\b\u0000\u0012\u0002H\u00010\tj\n\u0012\u0006\b\u0000\u0012\u0002H\u0001`\nH\u0007\u00a2\u0006\u0002\u0010\f\u001a\u0019\u0010\u0000\u001a\u00020\r2\u0006\u0010\u0003\u001a\u00020\r2\u0006\u0010\u0004\u001a\u00020\rH\u0087\b\u001a!\u0010\u0000\u001a\u00020\r2\u0006\u0010\u0003\u001a\u00020\r2\u0006\u0010\u0004\u001a\u00020\r2\u0006\u0010\u0006\u001a\u00020\rH\u0087\b\u001a\u0019\u0010\u0000\u001a\u00020\u000e2\u0006\u0010\u0003\u001a\u00020\u000e2\u0006\u0010\u0004\u001a\u00020\u000eH\u0087\b\u001a!\u0010\u0000\u001a\u00020\u000e2\u0006\u0010\u0003\u001a\u00020\u000e2\u0006\u0010\u0004\u001a\u00020\u000e2\u0006\u0010\u0006\u001a\u00020\u000eH\u0087\b\u001a\u0019\u0010\u0000\u001a\u00020\u000f2\u0006\u0010\u0003\u001a\u00020\u000f2\u0006\u0010\u0004\u001a\u00020\u000fH\u0087\b\u001a!\u0010\u0000\u001a\u00020\u000f2\u0006\u0010\u0003\u001a\u00020\u000f2\u0006\u0010\u0004\u001a\u00020\u000f2\u0006\u0010\u0006\u001a\u00020\u000fH\u0087\b\u001a\u0019\u0010\u0000\u001a\u00020\u00102\u0006\u0010\u0003\u001a\u00020\u00102\u0006\u0010\u0004\u001a\u00020\u0010H\u0087\b\u001a!\u0010\u0000\u001a\u00020\u00102\u0006\u0010\u0003\u001a\u00020\u00102\u0006\u0010\u0004\u001a\u00020\u00102\u0006\u0010\u0006\u001a\u00020\u0010H\u0087\b\u001a\u0019\u0010\u0000\u001a\u00020\u00112\u0006\u0010\u0003\u001a\u00020\u00112\u0006\u0010\u0004\u001a\u00020\u0011H\u0087\b\u001a!\u0010\u0000\u001a\u00020\u00112\u0006\u0010\u0003\u001a\u00020\u00112\u0006\u0010\u0004\u001a\u00020\u00112\u0006\u0010\u0006\u001a\u00020\u0011H\u0087\b\u001a\u0019\u0010\u0000\u001a\u00020\u00122\u0006\u0010\u0003\u001a\u00020\u00122\u0006\u0010\u0004\u001a\u00020\u0012H\u0087\b\u001a!\u0010\u0000\u001a\u00020\u00122\u0006\u0010\u0003\u001a\u00020\u00122\u0006\u0010\u0004\u001a\u00020\u00122\u0006\u0010\u0006\u001a\u00020\u0012H\u0087\b\u001a-\u0010\u0013\u001a\u0002H\u0001\"\u000e\b\u0000\u0010\u0001*\b\u0012\u0004\u0012\u0002H\u00010\u00022\u0006\u0010\u0003\u001a\u0002H\u00012\u0006\u0010\u0004\u001a\u0002H\u0001H\u0007\u00a2\u0006\u0002\u0010\u0005\u001a5\u0010\u0013\u001a\u0002H\u0001\"\u000e\b\u0000\u0010\u0001*\b\u0012\u0004\u0012\u0002H\u00010\u00022\u0006\u0010\u0003\u001a\u0002H\u00012\u0006\u0010\u0004\u001a\u0002H\u00012\u0006\u0010\u0006\u001a\u0002H\u0001H\u0007\u00a2\u0006\u0002\u0010\u0007\u001aG\u0010\u0013\u001a\u0002H\u0001\"\u0004\b\u0000\u0010\u00012\u0006\u0010\u0003\u001a\u0002H\u00012\u0006\u0010\u0004\u001a\u0002H\u00012\u0006\u0010\u0006\u001a\u0002H\u00012\u001a\u0010\b\u001a\u0016\u0012\u0006\b\u0000\u0012\u0002H\u00010\tj\n\u0012\u0006\b\u0000\u0012\u0002H\u0001`\nH\u0007\u00a2\u0006\u0002\u0010\u000b\u001a?\u0010\u0013\u001a\u0002H\u0001\"\u0004\b\u0000\u0010\u00012\u0006\u0010\u0003\u001a\u0002H\u00012\u0006\u0010\u0004\u001a\u0002H\u00012\u001a\u0010\b\u001a\u0016\u0012\u0006\b\u0000\u0012\u0002H\u00010\tj\n\u0012\u0006\b\u0000\u0012\u0002H\u0001`\nH\u0007\u00a2\u0006\u0002\u0010\f\u001a\u0019\u0010\u0013\u001a\u00020\r2\u0006\u0010\u0003\u001a\u00020\r2\u0006\u0010\u0004\u001a\u00020\rH\u0087\b\u001a!\u0010\u0013\u001a\u00020\r2\u0006\u0010\u0003\u001a\u00020\r2\u0006\u0010\u0004\u001a\u00020\r2\u0006\u0010\u0006\u001a\u00020\rH\u0087\b\u001a\u0019\u0010\u0013\u001a\u00020\u000e2\u0006\u0010\u0003\u001a\u00020\u000e2\u0006\u0010\u0004\u001a\u00020\u000eH\u0087\b\u001a!\u0010\u0013\u001a\u00020\u000e2\u0006\u0010\u0003\u001a\u00020\u000e2\u0006\u0010\u0004\u001a\u00020\u000e2\u0006\u0010\u0006\u001a\u00020\u000eH\u0087\b\u001a\u0019\u0010\u0013\u001a\u00020\u000f2\u0006\u0010\u0003\u001a\u00020\u000f2\u0006\u0010\u0004\u001a\u00020\u000fH\u0087\b\u001a!\u0010\u0013\u001a\u00020\u000f2\u0006\u0010\u0003\u001a\u00020\u000f2\u0006\u0010\u0004\u001a\u00020\u000f2\u0006\u0010\u0006\u001a\u00020\u000fH\u0087\b\u001a\u0019\u0010\u0013\u001a\u00020\u00102\u0006\u0010\u0003\u001a\u00020\u00102\u0006\u0010\u0004\u001a\u00020\u0010H\u0087\b\u001a!\u0010\u0013\u001a\u00020\u00102\u0006\u0010\u0003\u001a\u00020\u00102\u0006\u0010\u0004\u001a\u00020\u00102\u0006\u0010\u0006\u001a\u00020\u0010H\u0087\b\u001a\u0019\u0010\u0013\u001a\u00020\u00112\u0006\u0010\u0003\u001a\u00020\u00112\u0006\u0010\u0004\u001a\u00020\u0011H\u0087\b\u001a!\u0010\u0013\u001a\u00020\u00112\u0006\u0010\u0003\u001a\u00020\u00112\u0006\u0010\u0004\u001a\u00020\u00112\u0006\u0010\u0006\u001a\u00020\u0011H\u0087\b\u001a\u0019\u0010\u0013\u001a\u00020\u00122\u0006\u0010\u0003\u001a\u00020\u00122\u0006\u0010\u0004\u001a\u00020\u0012H\u0087\b\u001a!\u0010\u0013\u001a\u00020\u00122\u0006\u0010\u0003\u001a\u00020\u00122\u0006\u0010\u0004\u001a\u00020\u00122\u0006\u0010\u0006\u001a\u00020\u0012H\u0087\b\u00a8\u0006\u0014"}, d2 = {"maxOf", "T", "", "a", "b", "(Ljava/lang/Comparable;Ljava/lang/Comparable;)Ljava/lang/Comparable;", "c", "(Ljava/lang/Comparable;Ljava/lang/Comparable;Ljava/lang/Comparable;)Ljava/lang/Comparable;", "comparator", "Ljava/util/Comparator;", "Lkotlin/Comparator;", "(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Comparator;)Ljava/lang/Object;", "(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Comparator;)Ljava/lang/Object;", "", "", "", "", "", "", "minOf", "kotlin-stdlib"}, k = 5, mv = {1, 1, 6}, xi = 1, xs = "kotlin/comparisons/ComparisonsKt")
/* compiled from: _Comparisons.kt */
class ComparisonsKt___ComparisonsKt extends kotlin.comparisons.ComparisonsKt__ComparisonsKt {
    @org.jetbrains.annotations.NotNull
    @kotlin.SinceKotlin(version = "1.1")
    public static final <T extends java.lang.Comparable<? super T>> T maxOf(@org.jetbrains.annotations.NotNull T a, @org.jetbrains.annotations.NotNull T b) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "a");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(b, "b");
        return a.compareTo(b) >= 0 ? a : b;
    }

    @kotlin.SinceKotlin(version = "1.1")
    @kotlin.internal.InlineOnly
    private static final byte maxOf(byte a, byte b) {
        return (byte) java.lang.Math.max(a, b);
    }

    @kotlin.SinceKotlin(version = "1.1")
    @kotlin.internal.InlineOnly
    private static final short maxOf(short a, short b) {
        return (short) java.lang.Math.max(a, b);
    }

    @kotlin.SinceKotlin(version = "1.1")
    @kotlin.internal.InlineOnly
    private static final int maxOf(int a, int b) {
        return java.lang.Math.max(a, b);
    }

    @kotlin.SinceKotlin(version = "1.1")
    @kotlin.internal.InlineOnly
    private static final long maxOf(long a, long b) {
        return java.lang.Math.max(a, b);
    }

    @kotlin.SinceKotlin(version = "1.1")
    @kotlin.internal.InlineOnly
    private static final float maxOf(float a, float b) {
        return java.lang.Math.max(a, b);
    }

    @kotlin.SinceKotlin(version = "1.1")
    @kotlin.internal.InlineOnly
    private static final double maxOf(double a, double b) {
        return java.lang.Math.max(a, b);
    }

    @org.jetbrains.annotations.NotNull
    @kotlin.SinceKotlin(version = "1.1")
    public static final <T extends java.lang.Comparable<? super T>> T maxOf(@org.jetbrains.annotations.NotNull T a, @org.jetbrains.annotations.NotNull T b, @org.jetbrains.annotations.NotNull T c) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "a");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(b, "b");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(c, "c");
        return kotlin.comparisons.ComparisonsKt.maxOf(a, (T) kotlin.comparisons.ComparisonsKt.maxOf(b, c));
    }

    @kotlin.SinceKotlin(version = "1.1")
    @kotlin.internal.InlineOnly
    private static final byte maxOf(byte a, byte b, byte c) {
        return (byte) java.lang.Math.max(a, java.lang.Math.max(b, c));
    }

    @kotlin.SinceKotlin(version = "1.1")
    @kotlin.internal.InlineOnly
    private static final short maxOf(short a, short b, short c) {
        return (short) java.lang.Math.max(a, java.lang.Math.max(b, c));
    }

    @kotlin.SinceKotlin(version = "1.1")
    @kotlin.internal.InlineOnly
    private static final int maxOf(int a, int b, int c) {
        return java.lang.Math.max(a, java.lang.Math.max(b, c));
    }

    @kotlin.SinceKotlin(version = "1.1")
    @kotlin.internal.InlineOnly
    private static final long maxOf(long a, long b, long c) {
        return java.lang.Math.max(a, java.lang.Math.max(b, c));
    }

    @kotlin.SinceKotlin(version = "1.1")
    @kotlin.internal.InlineOnly
    private static final float maxOf(float a, float b, float c) {
        return java.lang.Math.max(a, java.lang.Math.max(b, c));
    }

    @kotlin.SinceKotlin(version = "1.1")
    @kotlin.internal.InlineOnly
    private static final double maxOf(double a, double b, double c) {
        return java.lang.Math.max(a, java.lang.Math.max(b, c));
    }

    @kotlin.SinceKotlin(version = "1.1")
    public static final <T> T maxOf(T a, T b, T c, @org.jetbrains.annotations.NotNull java.util.Comparator<? super T> comparator) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(comparator, "comparator");
        return kotlin.comparisons.ComparisonsKt.maxOf(a, (T) kotlin.comparisons.ComparisonsKt.maxOf(b, c, comparator), comparator);
    }

    @kotlin.SinceKotlin(version = "1.1")
    public static final <T> T maxOf(T a, T b, @org.jetbrains.annotations.NotNull java.util.Comparator<? super T> comparator) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(comparator, "comparator");
        return comparator.compare(a, b) >= 0 ? a : b;
    }

    @org.jetbrains.annotations.NotNull
    @kotlin.SinceKotlin(version = "1.1")
    public static final <T extends java.lang.Comparable<? super T>> T minOf(@org.jetbrains.annotations.NotNull T a, @org.jetbrains.annotations.NotNull T b) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "a");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(b, "b");
        return a.compareTo(b) <= 0 ? a : b;
    }

    @kotlin.SinceKotlin(version = "1.1")
    @kotlin.internal.InlineOnly
    private static final byte minOf(byte a, byte b) {
        return (byte) java.lang.Math.min(a, b);
    }

    @kotlin.SinceKotlin(version = "1.1")
    @kotlin.internal.InlineOnly
    private static final short minOf(short a, short b) {
        return (short) java.lang.Math.min(a, b);
    }

    @kotlin.SinceKotlin(version = "1.1")
    @kotlin.internal.InlineOnly
    private static final int minOf(int a, int b) {
        return java.lang.Math.min(a, b);
    }

    @kotlin.SinceKotlin(version = "1.1")
    @kotlin.internal.InlineOnly
    private static final long minOf(long a, long b) {
        return java.lang.Math.min(a, b);
    }

    @kotlin.SinceKotlin(version = "1.1")
    @kotlin.internal.InlineOnly
    private static final float minOf(float a, float b) {
        return java.lang.Math.min(a, b);
    }

    @kotlin.SinceKotlin(version = "1.1")
    @kotlin.internal.InlineOnly
    private static final double minOf(double a, double b) {
        return java.lang.Math.min(a, b);
    }

    @org.jetbrains.annotations.NotNull
    @kotlin.SinceKotlin(version = "1.1")
    public static final <T extends java.lang.Comparable<? super T>> T minOf(@org.jetbrains.annotations.NotNull T a, @org.jetbrains.annotations.NotNull T b, @org.jetbrains.annotations.NotNull T c) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "a");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(b, "b");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(c, "c");
        return kotlin.comparisons.ComparisonsKt.minOf(a, (T) kotlin.comparisons.ComparisonsKt.minOf(b, c));
    }

    @kotlin.SinceKotlin(version = "1.1")
    @kotlin.internal.InlineOnly
    private static final byte minOf(byte a, byte b, byte c) {
        return (byte) java.lang.Math.min(a, java.lang.Math.min(b, c));
    }

    @kotlin.SinceKotlin(version = "1.1")
    @kotlin.internal.InlineOnly
    private static final short minOf(short a, short b, short c) {
        return (short) java.lang.Math.min(a, java.lang.Math.min(b, c));
    }

    @kotlin.SinceKotlin(version = "1.1")
    @kotlin.internal.InlineOnly
    private static final int minOf(int a, int b, int c) {
        return java.lang.Math.min(a, java.lang.Math.min(b, c));
    }

    @kotlin.SinceKotlin(version = "1.1")
    @kotlin.internal.InlineOnly
    private static final long minOf(long a, long b, long c) {
        return java.lang.Math.min(a, java.lang.Math.min(b, c));
    }

    @kotlin.SinceKotlin(version = "1.1")
    @kotlin.internal.InlineOnly
    private static final float minOf(float a, float b, float c) {
        return java.lang.Math.min(a, java.lang.Math.min(b, c));
    }

    @kotlin.SinceKotlin(version = "1.1")
    @kotlin.internal.InlineOnly
    private static final double minOf(double a, double b, double c) {
        return java.lang.Math.min(a, java.lang.Math.min(b, c));
    }

    @kotlin.SinceKotlin(version = "1.1")
    public static final <T> T minOf(T a, T b, T c, @org.jetbrains.annotations.NotNull java.util.Comparator<? super T> comparator) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(comparator, "comparator");
        return kotlin.comparisons.ComparisonsKt.minOf(a, (T) kotlin.comparisons.ComparisonsKt.minOf(b, c, comparator), comparator);
    }

    @kotlin.SinceKotlin(version = "1.1")
    public static final <T> T minOf(T a, T b, @org.jetbrains.annotations.NotNull java.util.Comparator<? super T> comparator) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(comparator, "comparator");
        return comparator.compare(a, b) <= 0 ? a : b;
    }
}
