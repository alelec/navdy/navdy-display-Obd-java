package kotlin.comparisons;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000<\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000f\n\u0000\n\u0002\u0010\u0011\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\t\n\u0002\u0010\u0000\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u001a;\u0010\u0000\u001a\u0012\u0012\u0004\u0012\u0002H\u00020\u0001j\b\u0012\u0004\u0012\u0002H\u0002`\u0003\"\u0004\b\u0000\u0010\u00022\u001a\b\u0004\u0010\u0004\u001a\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\n\u0012\b\u0012\u0002\b\u0003\u0018\u00010\u00060\u0005H\u0087\b\u001aY\u0010\u0000\u001a\u0012\u0012\u0004\u0012\u0002H\u00020\u0001j\b\u0012\u0004\u0012\u0002H\u0002`\u0003\"\u0004\b\u0000\u0010\u000226\u0010\u0007\u001a\u001c\u0012\u0018\b\u0001\u0012\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\n\u0012\b\u0012\u0002\b\u0003\u0018\u00010\u00060\u00050\b\"\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\n\u0012\b\u0012\u0002\b\u0003\u0018\u00010\u00060\u0005\u00a2\u0006\u0002\u0010\t\u001aW\u0010\u0000\u001a\u0012\u0012\u0004\u0012\u0002H\u00020\u0001j\b\u0012\u0004\u0012\u0002H\u0002`\u0003\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\n2\u001a\u0010\u000b\u001a\u0016\u0012\u0006\b\u0000\u0012\u0002H\n0\u0001j\n\u0012\u0006\b\u0000\u0012\u0002H\n`\u00032\u0014\b\u0004\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\n0\u0005H\u0087\b\u001a;\u0010\f\u001a\u0012\u0012\u0004\u0012\u0002H\u00020\u0001j\b\u0012\u0004\u0012\u0002H\u0002`\u0003\"\u0004\b\u0000\u0010\u00022\u001a\b\u0004\u0010\u0004\u001a\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\n\u0012\b\u0012\u0002\b\u0003\u0018\u00010\u00060\u0005H\u0087\b\u001aW\u0010\f\u001a\u0012\u0012\u0004\u0012\u0002H\u00020\u0001j\b\u0012\u0004\u0012\u0002H\u0002`\u0003\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\n2\u001a\u0010\u000b\u001a\u0016\u0012\u0006\b\u0000\u0012\u0002H\n0\u0001j\n\u0012\u0006\b\u0000\u0012\u0002H\n`\u00032\u0014\b\u0004\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\n0\u0005H\u0087\b\u001a-\u0010\r\u001a\u00020\u000e\"\f\b\u0000\u0010\u0002*\u0006\u0012\u0002\b\u00030\u00062\b\u0010\u000f\u001a\u0004\u0018\u0001H\u00022\b\u0010\u0010\u001a\u0004\u0018\u0001H\u0002\u00a2\u0006\u0002\u0010\u0011\u001a>\u0010\u0012\u001a\u00020\u000e\"\u0004\b\u0000\u0010\u00022\u0006\u0010\u000f\u001a\u0002H\u00022\u0006\u0010\u0010\u001a\u0002H\u00022\u0018\u0010\u0004\u001a\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\n\u0012\b\u0012\u0002\b\u0003\u0018\u00010\u00060\u0005H\u0087\b\u00a2\u0006\u0002\u0010\u0013\u001aY\u0010\u0012\u001a\u00020\u000e\"\u0004\b\u0000\u0010\u00022\u0006\u0010\u000f\u001a\u0002H\u00022\u0006\u0010\u0010\u001a\u0002H\u000226\u0010\u0007\u001a\u001c\u0012\u0018\b\u0001\u0012\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\n\u0012\b\u0012\u0002\b\u0003\u0018\u00010\u00060\u00050\b\"\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\n\u0012\b\u0012\u0002\b\u0003\u0018\u00010\u00060\u0005\u00a2\u0006\u0002\u0010\u0014\u001aZ\u0010\u0012\u001a\u00020\u000e\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\n2\u0006\u0010\u000f\u001a\u0002H\u00022\u0006\u0010\u0010\u001a\u0002H\u00022\u001a\u0010\u000b\u001a\u0016\u0012\u0006\b\u0000\u0012\u0002H\n0\u0001j\n\u0012\u0006\b\u0000\u0012\u0002H\n`\u00032\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\n0\u0005H\u0087\b\u00a2\u0006\u0002\u0010\u0015\u001a&\u0010\u0016\u001a\u0012\u0012\u0004\u0012\u0002H\u00020\u0001j\b\u0012\u0004\u0012\u0002H\u0002`\u0003\"\u000e\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u0006\u001a-\u0010\u0017\u001a\u0016\u0012\u0006\u0012\u0004\u0018\u0001H\u00020\u0001j\n\u0012\u0006\u0012\u0004\u0018\u0001H\u0002`\u0003\"\u000e\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u0006H\u0087\b\u001a@\u0010\u0017\u001a\u0016\u0012\u0006\u0012\u0004\u0018\u0001H\u00020\u0001j\n\u0012\u0006\u0012\u0004\u0018\u0001H\u0002`\u0003\"\b\b\u0000\u0010\u0002*\u00020\u00182\u001a\u0010\u000b\u001a\u0016\u0012\u0006\b\u0000\u0012\u0002H\u00020\u0001j\n\u0012\u0006\b\u0000\u0012\u0002H\u0002`\u0003\u001a-\u0010\u0019\u001a\u0016\u0012\u0006\u0012\u0004\u0018\u0001H\u00020\u0001j\n\u0012\u0006\u0012\u0004\u0018\u0001H\u0002`\u0003\"\u000e\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u0006H\u0087\b\u001a@\u0010\u0019\u001a\u0016\u0012\u0006\u0012\u0004\u0018\u0001H\u00020\u0001j\n\u0012\u0006\u0012\u0004\u0018\u0001H\u0002`\u0003\"\b\b\u0000\u0010\u0002*\u00020\u00182\u001a\u0010\u000b\u001a\u0016\u0012\u0006\b\u0000\u0012\u0002H\u00020\u0001j\n\u0012\u0006\b\u0000\u0012\u0002H\u0002`\u0003\u001a&\u0010\u001a\u001a\u0012\u0012\u0004\u0012\u0002H\u00020\u0001j\b\u0012\u0004\u0012\u0002H\u0002`\u0003\"\u000e\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u0006\u001a0\u0010\u001b\u001a\u0012\u0012\u0004\u0012\u0002H\u00020\u0001j\b\u0012\u0004\u0012\u0002H\u0002`\u0003\"\u0004\b\u0000\u0010\u0002*\u0012\u0012\u0004\u0012\u0002H\u00020\u0001j\b\u0012\u0004\u0012\u0002H\u0002`\u0003\u001aO\u0010\u001c\u001a\u0012\u0012\u0004\u0012\u0002H\u00020\u0001j\b\u0012\u0004\u0012\u0002H\u0002`\u0003\"\u0004\b\u0000\u0010\u0002*\u0012\u0012\u0004\u0012\u0002H\u00020\u0001j\b\u0012\u0004\u0012\u0002H\u0002`\u00032\u001a\u0010\u000b\u001a\u0016\u0012\u0006\b\u0000\u0012\u0002H\u00020\u0001j\n\u0012\u0006\b\u0000\u0012\u0002H\u0002`\u0003H\u0086\u0004\u001aO\u0010\u001d\u001a\u0012\u0012\u0004\u0012\u0002H\u00020\u0001j\b\u0012\u0004\u0012\u0002H\u0002`\u0003\"\u0004\b\u0000\u0010\u0002*\u0012\u0012\u0004\u0012\u0002H\u00020\u0001j\b\u0012\u0004\u0012\u0002H\u0002`\u00032\u001a\b\u0004\u0010\u0004\u001a\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\n\u0012\b\u0012\u0002\b\u0003\u0018\u00010\u00060\u0005H\u0087\b\u001ak\u0010\u001d\u001a\u0012\u0012\u0004\u0012\u0002H\u00020\u0001j\b\u0012\u0004\u0012\u0002H\u0002`\u0003\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\n*\u0012\u0012\u0004\u0012\u0002H\u00020\u0001j\b\u0012\u0004\u0012\u0002H\u0002`\u00032\u001a\u0010\u000b\u001a\u0016\u0012\u0006\b\u0000\u0012\u0002H\n0\u0001j\n\u0012\u0006\b\u0000\u0012\u0002H\n`\u00032\u0014\b\u0004\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\n0\u0005H\u0087\b\u001aO\u0010\u001e\u001a\u0012\u0012\u0004\u0012\u0002H\u00020\u0001j\b\u0012\u0004\u0012\u0002H\u0002`\u0003\"\u0004\b\u0000\u0010\u0002*\u0012\u0012\u0004\u0012\u0002H\u00020\u0001j\b\u0012\u0004\u0012\u0002H\u0002`\u00032\u001a\b\u0004\u0010\u0004\u001a\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\n\u0012\b\u0012\u0002\b\u0003\u0018\u00010\u00060\u0005H\u0087\b\u001ak\u0010\u001e\u001a\u0012\u0012\u0004\u0012\u0002H\u00020\u0001j\b\u0012\u0004\u0012\u0002H\u0002`\u0003\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\n*\u0012\u0012\u0004\u0012\u0002H\u00020\u0001j\b\u0012\u0004\u0012\u0002H\u0002`\u00032\u001a\u0010\u000b\u001a\u0016\u0012\u0006\b\u0000\u0012\u0002H\n0\u0001j\n\u0012\u0006\b\u0000\u0012\u0002H\n`\u00032\u0014\b\u0004\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\n0\u0005H\u0087\b\u001am\u0010\u001f\u001a\u0012\u0012\u0004\u0012\u0002H\u00020\u0001j\b\u0012\u0004\u0012\u0002H\u0002`\u0003\"\u0004\b\u0000\u0010\u0002*\u0012\u0012\u0004\u0012\u0002H\u00020\u0001j\b\u0012\u0004\u0012\u0002H\u0002`\u000328\b\u0004\u0010 \u001a2\u0012\u0013\u0012\u0011H\u0002\u00a2\u0006\f\b\"\u0012\b\b#\u0012\u0004\b\b(\u000f\u0012\u0013\u0012\u0011H\u0002\u00a2\u0006\f\b\"\u0012\b\b#\u0012\u0004\b\b(\u0010\u0012\u0004\u0012\u00020\u000e0!H\u0087\b\u001aO\u0010$\u001a\u0012\u0012\u0004\u0012\u0002H\u00020\u0001j\b\u0012\u0004\u0012\u0002H\u0002`\u0003\"\u0004\b\u0000\u0010\u0002*\u0012\u0012\u0004\u0012\u0002H\u00020\u0001j\b\u0012\u0004\u0012\u0002H\u0002`\u00032\u001a\u0010\u000b\u001a\u0016\u0012\u0006\b\u0000\u0012\u0002H\u00020\u0001j\n\u0012\u0006\b\u0000\u0012\u0002H\u0002`\u0003H\u0086\u0004\u00a8\u0006%"}, d2 = {"compareBy", "Ljava/util/Comparator;", "T", "Lkotlin/Comparator;", "selector", "Lkotlin/Function1;", "", "selectors", "", "([Lkotlin/jvm/functions/Function1;)Ljava/util/Comparator;", "K", "comparator", "compareByDescending", "compareValues", "", "a", "b", "(Ljava/lang/Comparable;Ljava/lang/Comparable;)I", "compareValuesBy", "(Ljava/lang/Object;Ljava/lang/Object;Lkotlin/jvm/functions/Function1;)I", "(Ljava/lang/Object;Ljava/lang/Object;[Lkotlin/jvm/functions/Function1;)I", "(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Comparator;Lkotlin/jvm/functions/Function1;)I", "naturalOrder", "nullsFirst", "", "nullsLast", "reverseOrder", "reversed", "then", "thenBy", "thenByDescending", "thenComparator", "comparison", "Lkotlin/Function2;", "Lkotlin/ParameterName;", "name", "thenDescending", "kotlin-stdlib"}, k = 5, mv = {1, 1, 6}, xi = 1, xs = "kotlin/comparisons/ComparisonsKt")
/* compiled from: Comparisons.kt */
class ComparisonsKt__ComparisonsKt {
    public static final <T> int compareValuesBy(T a, T b, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super T, ? extends java.lang.Comparable<?>>... selectors) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(selectors, "selectors");
        if (!(((java.lang.Object[]) selectors).length > 0)) {
            throw new java.lang.IllegalArgumentException("Failed requirement.".toString());
        }
        for (kotlin.jvm.functions.Function1 fn : selectors) {
            int diff = kotlin.comparisons.ComparisonsKt.compareValues((java.lang.Comparable) fn.invoke(a), (java.lang.Comparable) fn.invoke(b));
            if (diff != 0) {
                return diff;
            }
        }
        return 0;
    }

    @kotlin.internal.InlineOnly
    private static final <T> int compareValuesBy(T a, T b, kotlin.jvm.functions.Function1<? super T, ? extends java.lang.Comparable<?>> selector) {
        return kotlin.comparisons.ComparisonsKt.compareValues((java.lang.Comparable) selector.invoke(a), (java.lang.Comparable) selector.invoke(b));
    }

    @kotlin.internal.InlineOnly
    private static final <T, K> int compareValuesBy(T a, T b, java.util.Comparator<? super K> comparator, kotlin.jvm.functions.Function1<? super T, ? extends K> selector) {
        return comparator.compare(selector.invoke(a), selector.invoke(b));
    }

    public static final <T extends java.lang.Comparable<?>> int compareValues(@org.jetbrains.annotations.Nullable T a, @org.jetbrains.annotations.Nullable T b) {
        if (a == b) {
            return 0;
        }
        if (a == null) {
            return -1;
        }
        if (b == null) {
            return 1;
        }
        return a.compareTo(b);
    }

    @org.jetbrains.annotations.NotNull
    public static final <T> java.util.Comparator<T> compareBy(@org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super T, ? extends java.lang.Comparable<?>>... selectors) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(selectors, "selectors");
        return new kotlin.comparisons.ComparisonsKt__ComparisonsKt$compareBy$1<>(selectors);
    }

    @kotlin.internal.InlineOnly
    private static final <T> java.util.Comparator<T> compareBy(kotlin.jvm.functions.Function1<? super T, ? extends java.lang.Comparable<?>> selector) {
        return new kotlin.comparisons.ComparisonsKt__ComparisonsKt$compareBy$2<>(selector);
    }

    @kotlin.internal.InlineOnly
    private static final <T, K> java.util.Comparator<T> compareBy(java.util.Comparator<? super K> comparator, kotlin.jvm.functions.Function1<? super T, ? extends K> selector) {
        return new kotlin.comparisons.ComparisonsKt__ComparisonsKt$compareBy$3<>(comparator, selector);
    }

    @kotlin.internal.InlineOnly
    private static final <T> java.util.Comparator<T> compareByDescending(kotlin.jvm.functions.Function1<? super T, ? extends java.lang.Comparable<?>> selector) {
        return new kotlin.comparisons.ComparisonsKt__ComparisonsKt$compareByDescending$1<>(selector);
    }

    @kotlin.internal.InlineOnly
    private static final <T, K> java.util.Comparator<T> compareByDescending(java.util.Comparator<? super K> comparator, kotlin.jvm.functions.Function1<? super T, ? extends K> selector) {
        return new kotlin.comparisons.ComparisonsKt__ComparisonsKt$compareByDescending$2<>(comparator, selector);
    }

    @kotlin.internal.InlineOnly
    private static final <T> java.util.Comparator<T> thenBy(@org.jetbrains.annotations.NotNull java.util.Comparator<T> $receiver, kotlin.jvm.functions.Function1<? super T, ? extends java.lang.Comparable<?>> selector) {
        return new kotlin.comparisons.ComparisonsKt__ComparisonsKt$thenBy$1<>($receiver, selector);
    }

    @kotlin.internal.InlineOnly
    private static final <T, K> java.util.Comparator<T> thenBy(@org.jetbrains.annotations.NotNull java.util.Comparator<T> $receiver, java.util.Comparator<? super K> comparator, kotlin.jvm.functions.Function1<? super T, ? extends K> selector) {
        return new kotlin.comparisons.ComparisonsKt__ComparisonsKt$thenBy$2<>($receiver, comparator, selector);
    }

    @kotlin.internal.InlineOnly
    private static final <T> java.util.Comparator<T> thenByDescending(@org.jetbrains.annotations.NotNull java.util.Comparator<T> $receiver, kotlin.jvm.functions.Function1<? super T, ? extends java.lang.Comparable<?>> selector) {
        return new kotlin.comparisons.ComparisonsKt__ComparisonsKt$thenByDescending$1<>($receiver, selector);
    }

    @kotlin.internal.InlineOnly
    private static final <T, K> java.util.Comparator<T> thenByDescending(@org.jetbrains.annotations.NotNull java.util.Comparator<T> $receiver, java.util.Comparator<? super K> comparator, kotlin.jvm.functions.Function1<? super T, ? extends K> selector) {
        return new kotlin.comparisons.ComparisonsKt__ComparisonsKt$thenByDescending$2<>($receiver, comparator, selector);
    }

    @kotlin.internal.InlineOnly
    private static final <T> java.util.Comparator<T> thenComparator(@org.jetbrains.annotations.NotNull java.util.Comparator<T> $receiver, kotlin.jvm.functions.Function2<? super T, ? super T, java.lang.Integer> comparison) {
        return new kotlin.comparisons.ComparisonsKt__ComparisonsKt$thenComparator$1<>($receiver, comparison);
    }

    @org.jetbrains.annotations.NotNull
    public static final <T> java.util.Comparator<T> then(@org.jetbrains.annotations.NotNull java.util.Comparator<T> $receiver, @org.jetbrains.annotations.NotNull java.util.Comparator<? super T> comparator) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(comparator, "comparator");
        return new kotlin.comparisons.ComparisonsKt__ComparisonsKt$then$1<>($receiver, comparator);
    }

    @org.jetbrains.annotations.NotNull
    public static final <T> java.util.Comparator<T> thenDescending(@org.jetbrains.annotations.NotNull java.util.Comparator<T> $receiver, @org.jetbrains.annotations.NotNull java.util.Comparator<? super T> comparator) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(comparator, "comparator");
        return new kotlin.comparisons.ComparisonsKt__ComparisonsKt$thenDescending$1<>($receiver, comparator);
    }

    @org.jetbrains.annotations.NotNull
    public static final <T> java.util.Comparator<T> nullsFirst(@org.jetbrains.annotations.NotNull java.util.Comparator<? super T> comparator) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(comparator, "comparator");
        return new kotlin.comparisons.ComparisonsKt__ComparisonsKt$nullsFirst$1<>(comparator);
    }

    @kotlin.internal.InlineOnly
    private static final <T extends java.lang.Comparable<? super T>> java.util.Comparator<T> nullsFirst() {
        return kotlin.comparisons.ComparisonsKt.nullsFirst(kotlin.comparisons.ComparisonsKt.naturalOrder());
    }

    @org.jetbrains.annotations.NotNull
    public static final <T> java.util.Comparator<T> nullsLast(@org.jetbrains.annotations.NotNull java.util.Comparator<? super T> comparator) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(comparator, "comparator");
        return new kotlin.comparisons.ComparisonsKt__ComparisonsKt$nullsLast$1<>(comparator);
    }

    @kotlin.internal.InlineOnly
    private static final <T extends java.lang.Comparable<? super T>> java.util.Comparator<T> nullsLast() {
        return kotlin.comparisons.ComparisonsKt.nullsLast(kotlin.comparisons.ComparisonsKt.naturalOrder());
    }

    @org.jetbrains.annotations.NotNull
    public static final <T extends java.lang.Comparable<? super T>> java.util.Comparator<T> naturalOrder() {
        kotlin.comparisons.NaturalOrderComparator naturalOrderComparator = kotlin.comparisons.NaturalOrderComparator.INSTANCE;
        if (naturalOrderComparator != null) {
            return naturalOrderComparator;
        }
        throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.Comparator<T> /* = java.util.Comparator<T> */");
    }

    @org.jetbrains.annotations.NotNull
    public static final <T extends java.lang.Comparable<? super T>> java.util.Comparator<T> reverseOrder() {
        kotlin.comparisons.ReverseOrderComparator reverseOrderComparator = kotlin.comparisons.ReverseOrderComparator.INSTANCE;
        if (reverseOrderComparator != null) {
            return reverseOrderComparator;
        }
        throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.Comparator<T> /* = java.util.Comparator<T> */");
    }

    @org.jetbrains.annotations.NotNull
    public static final <T> java.util.Comparator<T> reversed(@org.jetbrains.annotations.NotNull java.util.Comparator<T> $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        if ($receiver instanceof kotlin.comparisons.ReversedComparator) {
            return ((kotlin.comparisons.ReversedComparator) $receiver).getComparator();
        }
        if (kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) $receiver, (java.lang.Object) kotlin.comparisons.NaturalOrderComparator.INSTANCE)) {
            kotlin.comparisons.ReverseOrderComparator reverseOrderComparator = kotlin.comparisons.ReverseOrderComparator.INSTANCE;
            if (reverseOrderComparator != null) {
                return reverseOrderComparator;
            }
            throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.Comparator<T> /* = java.util.Comparator<T> */");
        } else if (!kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) $receiver, (java.lang.Object) kotlin.comparisons.ReverseOrderComparator.INSTANCE)) {
            return new kotlin.comparisons.ReversedComparator<>($receiver);
        } else {
            kotlin.comparisons.NaturalOrderComparator naturalOrderComparator = kotlin.comparisons.NaturalOrderComparator.INSTANCE;
            if (naturalOrderComparator != null) {
                return naturalOrderComparator;
            }
            throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.Comparator<T> /* = java.util.Comparator<T> */");
        }
    }
}
