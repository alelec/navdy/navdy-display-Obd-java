package kotlin;

public class TypeCastException extends java.lang.ClassCastException {
    public TypeCastException() {
    }

    public TypeCastException(java.lang.String message) {
        super(message);
    }
}
