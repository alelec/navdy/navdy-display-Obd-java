package kotlin.collections;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000h\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010$\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010&\n\u0002\b\u0002\n\u0002\u0010\u001c\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010 \n\u0002\b\u0004\n\u0002\u0010\u001f\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u000f\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0000\u001aG\u0010\u0000\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0003*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u00042\u001e\u0010\u0005\u001a\u001a\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0007\u0012\u0004\u0012\u00020\u00010\u0006H\u0086\b\u001a$\u0010\b\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0003*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0004\u001aG\u0010\b\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0003*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u00042\u001e\u0010\u0005\u001a\u001a\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0007\u0012\u0004\u0012\u00020\u00010\u0006H\u0086\b\u001a9\u0010\t\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u00070\n\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0003*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0004H\u0087\b\u001a6\u0010\u000b\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u00070\f\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0003*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0004\u001a'\u0010\r\u001a\u00020\u000e\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0003*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0004H\u0087\b\u001aG\u0010\r\u001a\u00020\u000e\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0003*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u00042\u001e\u0010\u0005\u001a\u001a\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0007\u0012\u0004\u0012\u00020\u00010\u0006H\u0086\b\u001aY\u0010\u000f\u001a\b\u0012\u0004\u0012\u0002H\u00110\u0010\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0003\"\u0004\b\u0002\u0010\u0011*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u00042$\u0010\u0012\u001a \u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0007\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00110\n0\u0006H\u0086\b\u001ar\u0010\u0013\u001a\u0002H\u0014\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0003\"\u0004\b\u0002\u0010\u0011\"\u0010\b\u0003\u0010\u0014*\n\u0012\u0006\b\u0000\u0012\u0002H\u00110\u0015*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u00042\u0006\u0010\u0016\u001a\u0002H\u00142$\u0010\u0012\u001a \u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0007\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00110\n0\u0006H\u0086\b\u00a2\u0006\u0002\u0010\u0017\u001aG\u0010\u0018\u001a\u00020\u0019\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0003*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u00042\u001e\u0010\u001a\u001a\u001a\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0007\u0012\u0004\u0012\u00020\u00190\u0006H\u0087\b\u001aS\u0010\u001b\u001a\b\u0012\u0004\u0012\u0002H\u00110\u0010\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0003\"\u0004\b\u0002\u0010\u0011*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u00042\u001e\u0010\u0012\u001a\u001a\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0007\u0012\u0004\u0012\u0002H\u00110\u0006H\u0086\b\u001aY\u0010\u001c\u001a\b\u0012\u0004\u0012\u0002H\u00110\u0010\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0003\"\b\b\u0002\u0010\u0011*\u00020\u001d*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u00042 \u0010\u0012\u001a\u001c\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0007\u0012\u0006\u0012\u0004\u0018\u0001H\u00110\u0006H\u0086\b\u001ar\u0010\u001e\u001a\u0002H\u0014\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0003\"\b\b\u0002\u0010\u0011*\u00020\u001d\"\u0010\b\u0003\u0010\u0014*\n\u0012\u0006\b\u0000\u0012\u0002H\u00110\u0015*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u00042\u0006\u0010\u0016\u001a\u0002H\u00142 \u0010\u0012\u001a\u001c\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0007\u0012\u0006\u0012\u0004\u0018\u0001H\u00110\u0006H\u0086\b\u00a2\u0006\u0002\u0010\u0017\u001al\u0010\u001f\u001a\u0002H\u0014\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0003\"\u0004\b\u0002\u0010\u0011\"\u0010\b\u0003\u0010\u0014*\n\u0012\u0006\b\u0000\u0012\u0002H\u00110\u0015*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u00042\u0006\u0010\u0016\u001a\u0002H\u00142\u001e\u0010\u0012\u001a\u001a\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0007\u0012\u0004\u0012\u0002H\u00110\u0006H\u0086\b\u00a2\u0006\u0002\u0010\u0017\u001ae\u0010 \u001a\u0010\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0003\u0018\u00010\u0007\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0003\"\u000e\b\u0002\u0010\u0011*\b\u0012\u0004\u0012\u0002H\u00110!*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u00042\u001e\u0010\"\u001a\u001a\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0007\u0012\u0004\u0012\u0002H\u00110\u0006H\u0087\b\u001ai\u0010#\u001a\u0010\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0003\u0018\u00010\u0007\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0003*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u000422\u0010$\u001a.\u0012\u0012\b\u0000\u0012\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u00070%j\u0016\u0012\u0012\b\u0000\u0012\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0007`&H\u0087\b\u001ae\u0010'\u001a\u0010\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0003\u0018\u00010\u0007\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0003\"\u000e\b\u0002\u0010\u0011*\b\u0012\u0004\u0012\u0002H\u00110!*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u00042\u001e\u0010\"\u001a\u001a\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0007\u0012\u0004\u0012\u0002H\u00110\u0006H\u0086\b\u001af\u0010(\u001a\u0010\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0003\u0018\u00010\u0007\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0003*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u000422\u0010$\u001a.\u0012\u0012\b\u0000\u0012\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u00070%j\u0016\u0012\u0012\b\u0000\u0012\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0007`&\u001a$\u0010)\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0003*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0004\u001aG\u0010)\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0003*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u00042\u001e\u0010\u0005\u001a\u001a\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0007\u0012\u0004\u0012\u00020\u00010\u0006H\u0086\b\u001aV\u0010*\u001a\u0002H+\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0003\"\u0016\b\u0002\u0010+*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0004*\u0002H+2\u001e\u0010\u001a\u001a\u001a\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0007\u0012\u0004\u0012\u00020\u00190\u0006H\u0087\b\u00a2\u0006\u0002\u0010,\u001a6\u0010-\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030.0\u0010\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0003*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0004\u00a8\u0006/"}, d2 = {"all", "", "K", "V", "", "predicate", "Lkotlin/Function1;", "", "any", "asIterable", "", "asSequence", "Lkotlin/sequences/Sequence;", "count", "", "flatMap", "", "R", "transform", "flatMapTo", "C", "", "destination", "(Ljava/util/Map;Ljava/util/Collection;Lkotlin/jvm/functions/Function1;)Ljava/util/Collection;", "forEach", "", "action", "map", "mapNotNull", "", "mapNotNullTo", "mapTo", "maxBy", "", "selector", "maxWith", "comparator", "Ljava/util/Comparator;", "Lkotlin/Comparator;", "minBy", "minWith", "none", "onEach", "M", "(Ljava/util/Map;Lkotlin/jvm/functions/Function1;)Ljava/util/Map;", "toList", "Lkotlin/Pair;", "kotlin-stdlib"}, k = 5, mv = {1, 1, 6}, xi = 1, xs = "kotlin/collections/MapsKt")
/* compiled from: _Maps.kt */
class MapsKt___MapsKt extends kotlin.collections.MapsKt__MapsKt {
    @org.jetbrains.annotations.NotNull
    public static final <K, V> java.util.List<kotlin.Pair<K, V>> toList(@org.jetbrains.annotations.NotNull java.util.Map<? extends K, ? extends V> $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        if ($receiver.size() == 0) {
            return kotlin.collections.CollectionsKt.emptyList();
        }
        java.util.Iterator iterator = $receiver.entrySet().iterator();
        if (!iterator.hasNext()) {
            return kotlin.collections.CollectionsKt.emptyList();
        }
        java.util.Map.Entry first = (java.util.Map.Entry) iterator.next();
        if (!iterator.hasNext()) {
            return kotlin.collections.CollectionsKt.listOf(new kotlin.Pair(first.getKey(), first.getValue()));
        }
        java.util.ArrayList result = new java.util.ArrayList($receiver.size());
        result.add(new kotlin.Pair(first.getKey(), first.getValue()));
        do {
            java.util.Map.Entry entry = (java.util.Map.Entry) iterator.next();
            result.add(new kotlin.Pair(entry.getKey(), entry.getValue()));
        } while (iterator.hasNext());
        return result;
    }

    @org.jetbrains.annotations.NotNull
    public static final <K, V, R> java.util.List<R> flatMap(@org.jetbrains.annotations.NotNull java.util.Map<? extends K, ? extends V> $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.util.Map.Entry<? extends K, ? extends V>, ? extends java.lang.Iterable<? extends R>> transform) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(transform, "transform");
        java.util.Collection destination$iv = new java.util.ArrayList();
        for (java.util.Map.Entry element$iv : $receiver.entrySet()) {
            kotlin.collections.CollectionsKt.addAll(destination$iv, (java.lang.Iterable) transform.invoke(element$iv));
        }
        return (java.util.List) destination$iv;
    }

    @org.jetbrains.annotations.NotNull
    public static final <K, V, R, C extends java.util.Collection<? super R>> C flatMapTo(@org.jetbrains.annotations.NotNull java.util.Map<? extends K, ? extends V> $receiver, @org.jetbrains.annotations.NotNull C destination, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.util.Map.Entry<? extends K, ? extends V>, ? extends java.lang.Iterable<? extends R>> transform) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(destination, "destination");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(transform, "transform");
        for (java.util.Map.Entry element : $receiver.entrySet()) {
            kotlin.collections.CollectionsKt.addAll((java.util.Collection<? super T>) destination, (java.lang.Iterable) transform.invoke(element));
        }
        return destination;
    }

    @org.jetbrains.annotations.NotNull
    public static final <K, V, R> java.util.List<R> map(@org.jetbrains.annotations.NotNull java.util.Map<? extends K, ? extends V> $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.util.Map.Entry<? extends K, ? extends V>, ? extends R> transform) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(transform, "transform");
        java.util.Collection destination$iv = new java.util.ArrayList($receiver.size());
        for (java.util.Map.Entry item$iv : $receiver.entrySet()) {
            destination$iv.add(transform.invoke(item$iv));
        }
        return (java.util.List) destination$iv;
    }

    @org.jetbrains.annotations.NotNull
    public static final <K, V, R> java.util.List<R> mapNotNull(@org.jetbrains.annotations.NotNull java.util.Map<? extends K, ? extends V> $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.util.Map.Entry<? extends K, ? extends V>, ? extends R> transform) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(transform, "transform");
        java.util.Collection destination$iv = new java.util.ArrayList();
        for (java.util.Map.Entry element$iv$iv : $receiver.entrySet()) {
            java.lang.Object it$iv = transform.invoke(element$iv$iv);
            if (it$iv != null) {
                destination$iv.add(it$iv);
            }
        }
        return (java.util.List) destination$iv;
    }

    @org.jetbrains.annotations.NotNull
    public static final <K, V, R, C extends java.util.Collection<? super R>> C mapNotNullTo(@org.jetbrains.annotations.NotNull java.util.Map<? extends K, ? extends V> $receiver, @org.jetbrains.annotations.NotNull C destination, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.util.Map.Entry<? extends K, ? extends V>, ? extends R> transform) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(destination, "destination");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(transform, "transform");
        for (java.util.Map.Entry element$iv : $receiver.entrySet()) {
            java.lang.Object it = transform.invoke(element$iv);
            if (it != null) {
                destination.add(it);
            }
        }
        return destination;
    }

    @org.jetbrains.annotations.NotNull
    public static final <K, V, R, C extends java.util.Collection<? super R>> C mapTo(@org.jetbrains.annotations.NotNull java.util.Map<? extends K, ? extends V> $receiver, @org.jetbrains.annotations.NotNull C destination, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.util.Map.Entry<? extends K, ? extends V>, ? extends R> transform) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(destination, "destination");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(transform, "transform");
        for (java.util.Map.Entry item : $receiver.entrySet()) {
            destination.add(transform.invoke(item));
        }
        return destination;
    }

    public static final <K, V> boolean all(@org.jetbrains.annotations.NotNull java.util.Map<? extends K, ? extends V> $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.util.Map.Entry<? extends K, ? extends V>, java.lang.Boolean> predicate) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(predicate, "predicate");
        for (java.util.Map.Entry element : $receiver.entrySet()) {
            if (!((java.lang.Boolean) predicate.invoke(element)).booleanValue()) {
                return false;
            }
        }
        return true;
    }

    public static final <K, V> boolean any(@org.jetbrains.annotations.NotNull java.util.Map<? extends K, ? extends V> $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        java.util.Iterator it = $receiver.entrySet().iterator();
        if (!it.hasNext()) {
            return false;
        }
        java.util.Map.Entry entry = (java.util.Map.Entry) it.next();
        return true;
    }

    public static final <K, V> boolean any(@org.jetbrains.annotations.NotNull java.util.Map<? extends K, ? extends V> $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.util.Map.Entry<? extends K, ? extends V>, java.lang.Boolean> predicate) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(predicate, "predicate");
        for (java.util.Map.Entry element : $receiver.entrySet()) {
            if (((java.lang.Boolean) predicate.invoke(element)).booleanValue()) {
                return true;
            }
        }
        return false;
    }

    @kotlin.internal.InlineOnly
    private static final <K, V> int count(@org.jetbrains.annotations.NotNull java.util.Map<? extends K, ? extends V> $receiver) {
        return $receiver.size();
    }

    public static final <K, V> int count(@org.jetbrains.annotations.NotNull java.util.Map<? extends K, ? extends V> $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.util.Map.Entry<? extends K, ? extends V>, java.lang.Boolean> predicate) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(predicate, "predicate");
        int count = 0;
        for (java.util.Map.Entry element : $receiver.entrySet()) {
            if (((java.lang.Boolean) predicate.invoke(element)).booleanValue()) {
                count++;
            }
        }
        return count;
    }

    @kotlin.internal.HidesMembers
    public static final <K, V> void forEach(@org.jetbrains.annotations.NotNull java.util.Map<? extends K, ? extends V> $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.util.Map.Entry<? extends K, ? extends V>, kotlin.Unit> action) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(action, "action");
        for (java.util.Map.Entry element : $receiver.entrySet()) {
            action.invoke(element);
        }
    }

    @kotlin.internal.InlineOnly
    private static final <K, V, R extends java.lang.Comparable<? super R>> java.util.Map.Entry<K, V> maxBy(@org.jetbrains.annotations.NotNull java.util.Map<? extends K, ? extends V> $receiver, kotlin.jvm.functions.Function1<? super java.util.Map.Entry<? extends K, ? extends V>, ? extends R> selector) {
        java.lang.Object obj;
        java.util.Iterator iterator$iv = $receiver.entrySet().iterator();
        if (!iterator$iv.hasNext()) {
            obj = null;
        } else {
            java.lang.Object maxElem$iv = iterator$iv.next();
            java.lang.Comparable maxValue$iv = (java.lang.Comparable) selector.invoke(maxElem$iv);
            while (iterator$iv.hasNext()) {
                java.lang.Object e$iv = iterator$iv.next();
                java.lang.Comparable v$iv = (java.lang.Comparable) selector.invoke(e$iv);
                if (maxValue$iv.compareTo(v$iv) < 0) {
                    maxElem$iv = e$iv;
                    maxValue$iv = v$iv;
                }
            }
            obj = maxElem$iv;
        }
        return (java.util.Map.Entry) obj;
    }

    @kotlin.internal.InlineOnly
    private static final <K, V> java.util.Map.Entry<K, V> maxWith(@org.jetbrains.annotations.NotNull java.util.Map<? extends K, ? extends V> $receiver, java.util.Comparator<? super java.util.Map.Entry<? extends K, ? extends V>> comparator) {
        return (java.util.Map.Entry) kotlin.collections.CollectionsKt.maxWith($receiver.entrySet(), comparator);
    }

    @org.jetbrains.annotations.Nullable
    public static final <K, V, R extends java.lang.Comparable<? super R>> java.util.Map.Entry<K, V> minBy(@org.jetbrains.annotations.NotNull java.util.Map<? extends K, ? extends V> $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.util.Map.Entry<? extends K, ? extends V>, ? extends R> selector) {
        java.lang.Object obj;
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(selector, "selector");
        java.util.Iterator iterator$iv = $receiver.entrySet().iterator();
        if (!iterator$iv.hasNext()) {
            obj = null;
        } else {
            java.lang.Object minElem$iv = iterator$iv.next();
            java.lang.Comparable minValue$iv = (java.lang.Comparable) selector.invoke(minElem$iv);
            while (iterator$iv.hasNext()) {
                java.lang.Object e$iv = iterator$iv.next();
                java.lang.Comparable v$iv = (java.lang.Comparable) selector.invoke(e$iv);
                if (minValue$iv.compareTo(v$iv) > 0) {
                    minElem$iv = e$iv;
                    minValue$iv = v$iv;
                }
            }
            obj = minElem$iv;
        }
        return (java.util.Map.Entry) obj;
    }

    @org.jetbrains.annotations.Nullable
    public static final <K, V> java.util.Map.Entry<K, V> minWith(@org.jetbrains.annotations.NotNull java.util.Map<? extends K, ? extends V> $receiver, @org.jetbrains.annotations.NotNull java.util.Comparator<? super java.util.Map.Entry<? extends K, ? extends V>> comparator) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(comparator, "comparator");
        return (java.util.Map.Entry) kotlin.collections.CollectionsKt.minWith($receiver.entrySet(), comparator);
    }

    public static final <K, V> boolean none(@org.jetbrains.annotations.NotNull java.util.Map<? extends K, ? extends V> $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        java.util.Iterator it = $receiver.entrySet().iterator();
        if (!it.hasNext()) {
            return true;
        }
        java.util.Map.Entry entry = (java.util.Map.Entry) it.next();
        return false;
    }

    public static final <K, V> boolean none(@org.jetbrains.annotations.NotNull java.util.Map<? extends K, ? extends V> $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.util.Map.Entry<? extends K, ? extends V>, java.lang.Boolean> predicate) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(predicate, "predicate");
        for (java.util.Map.Entry element : $receiver.entrySet()) {
            if (((java.lang.Boolean) predicate.invoke(element)).booleanValue()) {
                return false;
            }
        }
        return true;
    }

    @org.jetbrains.annotations.NotNull
    @kotlin.SinceKotlin(version = "1.1")
    public static final <K, V, M extends java.util.Map<? extends K, ? extends V>> M onEach(@org.jetbrains.annotations.NotNull M $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.util.Map.Entry<? extends K, ? extends V>, kotlin.Unit> action) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(action, "action");
        for (java.util.Map.Entry element : $receiver.entrySet()) {
            action.invoke(element);
        }
        return $receiver;
    }

    @kotlin.internal.InlineOnly
    private static final <K, V> java.lang.Iterable<java.util.Map.Entry<K, V>> asIterable(@org.jetbrains.annotations.NotNull java.util.Map<? extends K, ? extends V> $receiver) {
        return $receiver.entrySet();
    }

    @org.jetbrains.annotations.NotNull
    public static final <K, V> kotlin.sequences.Sequence<java.util.Map.Entry<K, V>> asSequence(@org.jetbrains.annotations.NotNull java.util.Map<? extends K, ? extends V> $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        return kotlin.collections.CollectionsKt.asSequence($receiver.entrySet());
    }
}
