package kotlin.collections;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\n\n\u0000\n\u0002\u0010(\n\u0002\b\u0002\u0010\u0000\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\b\u0000\u0010\u0002H\n\u00a2\u0006\u0002\b\u0003"}, d2 = {"<anonymous>", "", "T", "invoke"}, k = 3, mv = {1, 1, 6})
/* compiled from: _Collections.kt */
final class CollectionsKt___CollectionsKt$withIndex$1 extends kotlin.jvm.internal.Lambda implements kotlin.jvm.functions.Function0<java.util.Iterator<? extends T>> {
    final /* synthetic */ java.lang.Iterable receiver$0;

    CollectionsKt___CollectionsKt$withIndex$1(java.lang.Iterable iterable) {
        this.receiver$0 = iterable;
        super(0);
    }

    @org.jetbrains.annotations.NotNull
    public final java.util.Iterator<T> invoke() {
        return this.receiver$0.iterator();
    }
}
