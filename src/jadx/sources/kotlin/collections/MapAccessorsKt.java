package kotlin.collections;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000.\n\u0002\b\u0003\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010%\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u001aK\u0010\u0000\u001a\u0002H\u0001\"\u0004\b\u0000\u0010\u0002\"\b\b\u0001\u0010\u0001*\u0002H\u0002*\u0015\u0012\u0006\b\u0000\u0012\u00020\u0004\u0012\t\u0012\u0007H\u0002\u00a2\u0006\u0002\b\u00050\u00032\b\u0010\u0006\u001a\u0004\u0018\u00010\u00072\n\u0010\b\u001a\u0006\u0012\u0002\b\u00030\tH\u0087\n\u00a2\u0006\u0002\u0010\n\u001a@\u0010\u0000\u001a\u0002H\u0002\"\u0004\b\u0000\u0010\u0002*\u0012\u0012\u0006\b\u0000\u0012\u00020\u0004\u0012\u0006\b\u0000\u0012\u0002H\u00020\u000b2\b\u0010\u0006\u001a\u0004\u0018\u00010\u00072\n\u0010\b\u001a\u0006\u0012\u0002\b\u00030\tH\u0087\n\u00a2\u0006\u0004\b\f\u0010\n\u001aF\u0010\r\u001a\u00020\u000e\"\u0004\b\u0000\u0010\u0002*\u0012\u0012\u0006\b\u0000\u0012\u00020\u0004\u0012\u0006\b\u0000\u0012\u0002H\u00020\u000b2\b\u0010\u0006\u001a\u0004\u0018\u00010\u00072\n\u0010\b\u001a\u0006\u0012\u0002\b\u00030\t2\u0006\u0010\u000f\u001a\u0002H\u0002H\u0087\n\u00a2\u0006\u0002\u0010\u0010\u00a8\u0006\u0011"}, d2 = {"getValue", "V1", "V", "", "", "Lkotlin/internal/Exact;", "thisRef", "", "property", "Lkotlin/reflect/KProperty;", "(Ljava/util/Map;Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;", "", "getVar", "setValue", "", "value", "(Ljava/util/Map;Ljava/lang/Object;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V", "kotlin-stdlib"}, k = 2, mv = {1, 1, 6})
@kotlin.jvm.JvmName(name = "MapAccessorsKt")
/* compiled from: MapAccessors.kt */
public final class MapAccessorsKt {
    @kotlin.internal.InlineOnly
    private static final <V, V1 extends V> V1 getValue(@org.jetbrains.annotations.NotNull java.util.Map<? super java.lang.String, ? extends V> $receiver, java.lang.Object thisRef, kotlin.reflect.KProperty<?> property) {
        return kotlin.collections.MapsKt.getOrImplicitDefaultNullable($receiver, property.getName());
    }

    @kotlin.internal.InlineOnly
    @kotlin.jvm.JvmName(name = "getVar")
    private static final <V> V getVar(@org.jetbrains.annotations.NotNull java.util.Map<? super java.lang.String, ? super V> $receiver, java.lang.Object thisRef, kotlin.reflect.KProperty<?> property) {
        return kotlin.collections.MapsKt.getOrImplicitDefaultNullable($receiver, property.getName());
    }

    @kotlin.internal.InlineOnly
    private static final <V> void setValue(@org.jetbrains.annotations.NotNull java.util.Map<? super java.lang.String, ? super V> $receiver, java.lang.Object thisRef, kotlin.reflect.KProperty<?> property, V value) {
        $receiver.put(property.getName(), value);
    }
}
