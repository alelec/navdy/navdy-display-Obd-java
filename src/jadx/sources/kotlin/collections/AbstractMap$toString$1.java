package kotlin.collections;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010&\n\u0000\u0010\u0000\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002\"\u0006\b\u0001\u0010\u0003 \u00012\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0005H\n\u00a2\u0006\u0002\b\u0006"}, d2 = {"<anonymous>", "", "K", "V", "it", "", "invoke"}, k = 3, mv = {1, 1, 6})
/* compiled from: AbstractMap.kt */
final class AbstractMap$toString$1 extends kotlin.jvm.internal.Lambda implements kotlin.jvm.functions.Function1<java.util.Map.Entry<? extends K, ? extends V>, java.lang.String> {
    final /* synthetic */ kotlin.collections.AbstractMap this$0;

    AbstractMap$toString$1(kotlin.collections.AbstractMap abstractMap) {
        this.this$0 = abstractMap;
        super(1);
    }

    @org.jetbrains.annotations.NotNull
    public final java.lang.String invoke(@org.jetbrains.annotations.NotNull java.util.Map.Entry<? extends K, ? extends V> it) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(it, "it");
        return this.this$0.toString(it);
    }
}
