package kotlin.collections;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\n\n\u0000\n\u0002\u0010\r\n\u0002\b\u0004\u0010\u0000\u001a\u00020\u0001\"\u0006\b\u0000\u0010\u0002 \u00012\u0006\u0010\u0003\u001a\u0002H\u0002H\n\u00a2\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"<anonymous>", "", "E", "it", "invoke", "(Ljava/lang/Object;)Ljava/lang/CharSequence;"}, k = 3, mv = {1, 1, 6})
/* compiled from: AbstractCollection.kt */
final class AbstractCollection$toString$1 extends kotlin.jvm.internal.Lambda implements kotlin.jvm.functions.Function1<E, java.lang.CharSequence> {
    final /* synthetic */ kotlin.collections.AbstractCollection this$0;

    AbstractCollection$toString$1(kotlin.collections.AbstractCollection abstractCollection) {
        this.this$0 = abstractCollection;
        super(1);
    }

    @org.jetbrains.annotations.NotNull
    public final java.lang.CharSequence invoke(E it) {
        return it == this.this$0 ? "(this Collection)" : java.lang.String.valueOf(it);
    }
}
