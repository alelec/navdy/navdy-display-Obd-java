package kotlin.collections;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000*\n\u0000\n\u0002\u0010\u0011\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\u0010\u0012\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u001e\n\u0002\b\u0002\u001a/\u0010\u0000\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\b\u0000\u0010\u00022\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u0002H\u00020\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\u0000\u00a2\u0006\u0002\u0010\u0006\u001a,\u0010\u0007\u001a\n\u0012\u0006\b\u0001\u0012\u0002H\u00020\u0001\"\u0006\b\u0000\u0010\u0002\u0018\u0001*\f\u0012\u0006\b\u0001\u0012\u0002H\u0002\u0018\u00010\u0001H\u0086\b\u00a2\u0006\u0002\u0010\b\u001a\u0015\u0010\t\u001a\u00020\n*\u00020\u000b2\u0006\u0010\f\u001a\u00020\rH\u0087\b\u001a&\u0010\u000e\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0006\b\u0000\u0010\u0002\u0018\u0001*\b\u0012\u0004\u0012\u0002H\u00020\u000fH\u0087\b\u00a2\u0006\u0002\u0010\u0010\u00a8\u0006\u0011"}, d2 = {"arrayOfNulls", "", "T", "reference", "size", "", "([Ljava/lang/Object;I)[Ljava/lang/Object;", "orEmpty", "([Ljava/lang/Object;)[Ljava/lang/Object;", "toString", "", "", "charset", "Ljava/nio/charset/Charset;", "toTypedArray", "", "(Ljava/util/Collection;)[Ljava/lang/Object;", "kotlin-stdlib"}, k = 5, mv = {1, 1, 6}, xi = 1, xs = "kotlin/collections/ArraysKt")
/* compiled from: ArraysJVM.kt */
class ArraysKt__ArraysJVMKt {
    private static final <T> T[] orEmpty(@org.jetbrains.annotations.Nullable T[] $receiver) {
        if ($receiver != null) {
            return $receiver;
        }
        kotlin.jvm.internal.Intrinsics.reifiedOperationMarker(0, "T?");
        return new java.lang.Object[0];
    }

    @kotlin.internal.InlineOnly
    private static final java.lang.String toString(@org.jetbrains.annotations.NotNull byte[] $receiver, java.nio.charset.Charset charset) {
        return new java.lang.String($receiver, charset);
    }

    private static final <T> T[] toTypedArray(@org.jetbrains.annotations.NotNull java.util.Collection<? extends T> $receiver) {
        if ($receiver == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type java.util.Collection<T>");
        }
        java.util.Collection thisCollection = $receiver;
        int size = thisCollection.size();
        kotlin.jvm.internal.Intrinsics.reifiedOperationMarker(0, "T?");
        T[] array = thisCollection.toArray(new java.lang.Object[size]);
        if (array != null) {
            return array;
        }
        throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @org.jetbrains.annotations.NotNull
    public static final <T> T[] arrayOfNulls(@org.jetbrains.annotations.NotNull T[] reference, int size) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(reference, "reference");
        java.lang.Object newInstance = java.lang.reflect.Array.newInstance(reference.getClass().getComponentType(), size);
        if (newInstance != null) {
            return (java.lang.Object[]) newInstance;
        }
        throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
    }
}
