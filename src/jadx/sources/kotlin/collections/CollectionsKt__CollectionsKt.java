package kotlin.collections;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000v\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u001e\n\u0002\b\u0003\n\u0002\u0010\b\n\u0000\n\u0002\u0010 \n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010!\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\f\n\u0002\u0010\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000f\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0000\u001a@\u0010\u000b\u001a\b\u0012\u0004\u0012\u0002H\u00070\b\"\u0004\b\u0000\u0010\u00072\u0006\u0010\f\u001a\u00020\u00062!\u0010\r\u001a\u001d\u0012\u0013\u0012\u00110\u0006\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0011\u0012\u0004\u0012\u0002H\u00070\u000eH\u0087\b\u001a@\u0010\u0012\u001a\b\u0012\u0004\u0012\u0002H\u00070\u0013\"\u0004\b\u0000\u0010\u00072\u0006\u0010\f\u001a\u00020\u00062!\u0010\r\u001a\u001d\u0012\u0013\u0012\u00110\u0006\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0011\u0012\u0004\u0012\u0002H\u00070\u000eH\u0087\b\u001a\u001f\u0010\u0014\u001a\u0012\u0012\u0004\u0012\u0002H\u00070\u0015j\b\u0012\u0004\u0012\u0002H\u0007`\u0016\"\u0004\b\u0000\u0010\u0007H\u0087\b\u001a5\u0010\u0014\u001a\u0012\u0012\u0004\u0012\u0002H\u00070\u0015j\b\u0012\u0004\u0012\u0002H\u0007`\u0016\"\u0004\b\u0000\u0010\u00072\u0012\u0010\u0017\u001a\n\u0012\u0006\b\u0001\u0012\u0002H\u00070\u0018\"\u0002H\u0007\u00a2\u0006\u0002\u0010\u0019\u001a\"\u0010\u001a\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u001b0\u00182\n\u0010\u001c\u001a\u0006\u0012\u0002\b\u00030\u0002H\u0081\b\u00a2\u0006\u0002\u0010\u001d\u001a4\u0010\u001a\u001a\b\u0012\u0004\u0012\u0002H\u00070\u0018\"\u0004\b\u0000\u0010\u00072\n\u0010\u001c\u001a\u0006\u0012\u0002\b\u00030\u00022\f\u0010\u001e\u001a\b\u0012\u0004\u0012\u0002H\u00070\u0018H\u0081\b\u00a2\u0006\u0002\u0010\u001f\u001a\u0012\u0010 \u001a\b\u0012\u0004\u0012\u0002H\u00070\b\"\u0004\b\u0000\u0010\u0007\u001a\u0015\u0010!\u001a\b\u0012\u0004\u0012\u0002H\u00070\b\"\u0004\b\u0000\u0010\u0007H\u0087\b\u001a!\u0010!\u001a\b\u0012\u0004\u0012\u0002H\u00070\b\"\u0004\b\u0000\u0010\u00072\u0006\u0010\"\u001a\u0002H\u0007H\u0007\u00a2\u0006\u0002\u0010#\u001a+\u0010!\u001a\b\u0012\u0004\u0012\u0002H\u00070\b\"\u0004\b\u0000\u0010\u00072\u0012\u0010\u0017\u001a\n\u0012\u0006\b\u0001\u0012\u0002H\u00070\u0018\"\u0002H\u0007\u00a2\u0006\u0002\u0010$\u001a%\u0010%\u001a\b\u0012\u0004\u0012\u0002H\u00070\b\"\b\b\u0000\u0010\u0007*\u00020\u001b2\b\u0010\"\u001a\u0004\u0018\u0001H\u0007\u00a2\u0006\u0002\u0010#\u001a3\u0010%\u001a\b\u0012\u0004\u0012\u0002H\u00070\b\"\b\b\u0000\u0010\u0007*\u00020\u001b2\u0016\u0010\u0017\u001a\f\u0012\b\b\u0001\u0012\u0004\u0018\u0001H\u00070\u0018\"\u0004\u0018\u0001H\u0007\u00a2\u0006\u0002\u0010$\u001a\u0015\u0010&\u001a\b\u0012\u0004\u0012\u0002H\u00070\u0013\"\u0004\b\u0000\u0010\u0007H\u0087\b\u001a+\u0010&\u001a\b\u0012\u0004\u0012\u0002H\u00070\u0013\"\u0004\b\u0000\u0010\u00072\u0012\u0010\u0017\u001a\n\u0012\u0006\b\u0001\u0012\u0002H\u00070\u0018\"\u0002H\u0007\u00a2\u0006\u0002\u0010$\u001a%\u0010'\u001a\u00020(2\u0006\u0010\f\u001a\u00020\u00062\u0006\u0010)\u001a\u00020\u00062\u0006\u0010*\u001a\u00020\u0006H\u0002\u00a2\u0006\u0002\b+\u001a%\u0010,\u001a\b\u0012\u0004\u0012\u0002H\u00070\u0002\"\u0004\b\u0000\u0010\u0007*\n\u0012\u0006\b\u0001\u0012\u0002H\u00070\u0018H\u0000\u00a2\u0006\u0002\u0010-\u001aS\u0010.\u001a\u00020\u0006\"\u0004\b\u0000\u0010\u0007*\b\u0012\u0004\u0012\u0002H\u00070\b2\u0006\u0010\"\u001a\u0002H\u00072\u001a\u0010/\u001a\u0016\u0012\u0006\b\u0000\u0012\u0002H\u000700j\n\u0012\u0006\b\u0000\u0012\u0002H\u0007`12\b\b\u0002\u0010)\u001a\u00020\u00062\b\b\u0002\u0010*\u001a\u00020\u0006\u00a2\u0006\u0002\u00102\u001a>\u0010.\u001a\u00020\u0006\"\u0004\b\u0000\u0010\u0007*\b\u0012\u0004\u0012\u0002H\u00070\b2\b\b\u0002\u0010)\u001a\u00020\u00062\b\b\u0002\u0010*\u001a\u00020\u00062\u0012\u00103\u001a\u000e\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u00020\u00060\u000e\u001aE\u0010.\u001a\u00020\u0006\"\u000e\b\u0000\u0010\u0007*\b\u0012\u0004\u0012\u0002H\u000704*\n\u0012\u0006\u0012\u0004\u0018\u0001H\u00070\b2\b\u0010\"\u001a\u0004\u0018\u0001H\u00072\b\b\u0002\u0010)\u001a\u00020\u00062\b\b\u0002\u0010*\u001a\u00020\u0006\u00a2\u0006\u0002\u00105\u001ad\u00106\u001a\u00020\u0006\"\u0004\b\u0000\u0010\u0007\"\u000e\b\u0001\u00107*\b\u0012\u0004\u0012\u0002H704*\b\u0012\u0004\u0012\u0002H\u00070\b2\b\u00108\u001a\u0004\u0018\u0001H72\b\b\u0002\u0010)\u001a\u00020\u00062\b\b\u0002\u0010*\u001a\u00020\u00062\u0016\b\u0004\u00109\u001a\u0010\u0012\u0004\u0012\u0002H\u0007\u0012\u0006\u0012\u0004\u0018\u0001H70\u000eH\u0086\b\u00a2\u0006\u0002\u0010:\u001a,\u0010;\u001a\u00020<\"\t\b\u0000\u0010\u0007\u00a2\u0006\u0002\b=*\b\u0012\u0004\u0012\u0002H\u00070\u00022\f\u0010\u0017\u001a\b\u0012\u0004\u0012\u0002H\u00070\u0002H\u0087\b\u001a1\u0010>\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u001b0\u0018\"\u0004\b\u0000\u0010\u0007*\n\u0012\u0006\b\u0001\u0012\u0002H\u00070\u00182\u0006\u0010?\u001a\u00020<H\u0003\u00a2\u0006\u0004\b@\u0010A\u001a\u0019\u0010B\u001a\u00020<\"\u0004\b\u0000\u0010\u0007*\b\u0012\u0004\u0012\u0002H\u00070\u0002H\u0087\b\u001a\u001e\u0010C\u001a\b\u0012\u0004\u0012\u0002H\u00070\b\"\u0004\b\u0000\u0010\u0007*\b\u0012\u0004\u0012\u0002H\u00070\bH\u0000\u001a!\u0010D\u001a\b\u0012\u0004\u0012\u0002H\u00070\u0002\"\u0004\b\u0000\u0010\u0007*\n\u0012\u0004\u0012\u0002H\u0007\u0018\u00010\u0002H\u0087\b\u001a!\u0010D\u001a\b\u0012\u0004\u0012\u0002H\u00070\b\"\u0004\b\u0000\u0010\u0007*\n\u0012\u0004\u0012\u0002H\u0007\u0018\u00010\bH\u0087\b\u001a\u001f\u0010E\u001a\b\u0012\u0004\u0012\u0002H\u00070\b\"\u0004\b\u0000\u0010\u0007*\b\u0012\u0004\u0012\u0002H\u00070FH\u0087\b\"\u0019\u0010\u0000\u001a\u00020\u0001*\u0006\u0012\u0002\b\u00030\u00028F\u00a2\u0006\u0006\u001a\u0004\b\u0003\u0010\u0004\"!\u0010\u0005\u001a\u00020\u0006\"\u0004\b\u0000\u0010\u0007*\b\u0012\u0004\u0012\u0002H\u00070\b8F\u00a2\u0006\u0006\u001a\u0004\b\t\u0010\n\u00a8\u0006G"}, d2 = {"indices", "Lkotlin/ranges/IntRange;", "", "getIndices", "(Ljava/util/Collection;)Lkotlin/ranges/IntRange;", "lastIndex", "", "T", "", "getLastIndex", "(Ljava/util/List;)I", "List", "size", "init", "Lkotlin/Function1;", "Lkotlin/ParameterName;", "name", "index", "MutableList", "", "arrayListOf", "Ljava/util/ArrayList;", "Lkotlin/collections/ArrayList;", "elements", "", "([Ljava/lang/Object;)Ljava/util/ArrayList;", "copyToArrayImpl", "", "collection", "(Ljava/util/Collection;)[Ljava/lang/Object;", "array", "(Ljava/util/Collection;[Ljava/lang/Object;)[Ljava/lang/Object;", "emptyList", "listOf", "element", "(Ljava/lang/Object;)Ljava/util/List;", "([Ljava/lang/Object;)Ljava/util/List;", "listOfNotNull", "mutableListOf", "rangeCheck", "", "fromIndex", "toIndex", "rangeCheck$CollectionsKt__CollectionsKt", "asCollection", "([Ljava/lang/Object;)Ljava/util/Collection;", "binarySearch", "comparator", "Ljava/util/Comparator;", "Lkotlin/Comparator;", "(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;II)I", "comparison", "", "(Ljava/util/List;Ljava/lang/Comparable;II)I", "binarySearchBy", "K", "key", "selector", "(Ljava/util/List;Ljava/lang/Comparable;IILkotlin/jvm/functions/Function1;)I", "containsAll", "", "Lkotlin/internal/OnlyInputTypes;", "copyToArrayOfAny", "isVarargs", "copyToArrayOfAny$CollectionsKt__CollectionsKt", "([Ljava/lang/Object;Z)[Ljava/lang/Object;", "isNotEmpty", "optimizeReadOnlyList", "orEmpty", "toList", "Ljava/util/Enumeration;", "kotlin-stdlib"}, k = 5, mv = {1, 1, 6}, xi = 1, xs = "kotlin/collections/CollectionsKt")
/* compiled from: Collections.kt */
class CollectionsKt__CollectionsKt {
    @org.jetbrains.annotations.NotNull
    public static final <T> java.util.Collection<T> asCollection(@org.jetbrains.annotations.NotNull T[] $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        return new kotlin.collections.ArrayAsCollection<>($receiver, false);
    }

    @org.jetbrains.annotations.NotNull
    public static final <T> java.util.List<T> emptyList() {
        return kotlin.collections.EmptyList.INSTANCE;
    }

    @org.jetbrains.annotations.NotNull
    public static final <T> java.util.List<T> listOf(@org.jetbrains.annotations.NotNull T... elements) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(elements, "elements");
        return elements.length > 0 ? kotlin.collections.ArraysKt.asList(elements) : kotlin.collections.CollectionsKt.emptyList();
    }

    @kotlin.internal.InlineOnly
    private static final <T> java.util.List<T> listOf() {
        return kotlin.collections.CollectionsKt.emptyList();
    }

    @org.jetbrains.annotations.NotNull
    public static final <T> java.util.List<T> listOf(T element) {
        java.util.List<T> singletonList = java.util.Collections.singletonList(element);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(singletonList, "java.util.Collections.singletonList(element)");
        return singletonList;
    }

    @kotlin.SinceKotlin(version = "1.1")
    @kotlin.internal.InlineOnly
    private static final <T> java.util.List<T> mutableListOf() {
        return new java.util.ArrayList<>();
    }

    @kotlin.SinceKotlin(version = "1.1")
    @kotlin.internal.InlineOnly
    private static final <T> java.util.ArrayList<T> arrayListOf() {
        return new java.util.ArrayList<>();
    }

    @org.jetbrains.annotations.NotNull
    public static final <T> java.util.List<T> mutableListOf(@org.jetbrains.annotations.NotNull T... elements) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(elements, "elements");
        return elements.length == 0 ? new java.util.ArrayList<>() : new java.util.ArrayList<>(new kotlin.collections.ArrayAsCollection(elements, true));
    }

    @org.jetbrains.annotations.NotNull
    public static final <T> java.util.ArrayList<T> arrayListOf(@org.jetbrains.annotations.NotNull T... elements) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(elements, "elements");
        return elements.length == 0 ? new java.util.ArrayList<>() : new java.util.ArrayList(new kotlin.collections.ArrayAsCollection(elements, true));
    }

    @org.jetbrains.annotations.NotNull
    public static final <T> java.util.List<T> listOfNotNull(@org.jetbrains.annotations.Nullable T element) {
        return element != null ? kotlin.collections.CollectionsKt.listOf(element) : kotlin.collections.CollectionsKt.emptyList();
    }

    @org.jetbrains.annotations.NotNull
    public static final <T> java.util.List<T> listOfNotNull(@org.jetbrains.annotations.NotNull T... elements) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(elements, "elements");
        return kotlin.collections.ArraysKt.filterNotNull(elements);
    }

    @kotlin.SinceKotlin(version = "1.1")
    @kotlin.internal.InlineOnly
    private static final <T> java.util.List<T> List(int size, kotlin.jvm.functions.Function1<? super java.lang.Integer, ? extends T> init) {
        java.util.ArrayList arrayList = new java.util.ArrayList(size);
        int i = 0;
        int i2 = size - 1;
        if (0 <= i2) {
            while (true) {
                arrayList.add(init.invoke(java.lang.Integer.valueOf(i)));
                if (i == i2) {
                    break;
                }
                i++;
            }
        }
        return arrayList;
    }

    @kotlin.SinceKotlin(version = "1.1")
    @kotlin.internal.InlineOnly
    private static final <T> java.util.List<T> MutableList(int size, kotlin.jvm.functions.Function1<? super java.lang.Integer, ? extends T> init) {
        java.util.ArrayList list = new java.util.ArrayList(size);
        int index = 0;
        int i = size - 1;
        if (0 <= i) {
            while (true) {
                list.add(init.invoke(java.lang.Integer.valueOf(index)));
                if (index == i) {
                    break;
                }
                index++;
            }
        }
        return list;
    }

    @org.jetbrains.annotations.NotNull
    public static final kotlin.ranges.IntRange getIndices(@org.jetbrains.annotations.NotNull java.util.Collection<?> $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        return new kotlin.ranges.IntRange(0, $receiver.size() - 1);
    }

    public static final <T> int getLastIndex(@org.jetbrains.annotations.NotNull java.util.List<? extends T> $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        return $receiver.size() - 1;
    }

    @kotlin.internal.InlineOnly
    private static final <T> boolean isNotEmpty(@org.jetbrains.annotations.NotNull java.util.Collection<? extends T> $receiver) {
        return !$receiver.isEmpty();
    }

    @kotlin.internal.InlineOnly
    private static final <T> java.util.Collection<T> orEmpty(@org.jetbrains.annotations.Nullable java.util.Collection<? extends T> $receiver) {
        return $receiver != null ? $receiver : kotlin.collections.CollectionsKt.emptyList();
    }

    @kotlin.internal.InlineOnly
    private static final <T> java.util.List<T> orEmpty(@org.jetbrains.annotations.Nullable java.util.List<? extends T> $receiver) {
        return $receiver != null ? $receiver : kotlin.collections.CollectionsKt.emptyList();
    }

    @kotlin.internal.InlineOnly
    private static final <T> java.util.List<T> toList(@org.jetbrains.annotations.NotNull java.util.Enumeration<T> $receiver) {
        java.util.ArrayList list = java.util.Collections.list($receiver);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(list, "java.util.Collections.list(this)");
        return list;
    }

    @kotlin.internal.InlineOnly
    private static final <T> boolean containsAll(@org.jetbrains.annotations.NotNull java.util.Collection<? extends T> $receiver, java.util.Collection<? extends T> elements) {
        return $receiver.containsAll(elements);
    }

    @org.jetbrains.annotations.NotNull
    public static final <T> java.util.List<T> optimizeReadOnlyList(@org.jetbrains.annotations.NotNull java.util.List<? extends T> $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        switch ($receiver.size()) {
            case 0:
                return kotlin.collections.CollectionsKt.emptyList();
            case 1:
                return kotlin.collections.CollectionsKt.listOf((T) $receiver.get(0));
            default:
                return $receiver;
        }
    }

    @kotlin.internal.InlineOnly
    private static final java.lang.Object[] copyToArrayImpl(java.util.Collection<?> collection) {
        java.lang.Object[] array = kotlin.jvm.internal.CollectionToArray.toArray(collection);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(array, "kotlin.jvm.internal.Coll\u2026Array.toArray(collection)");
        return array;
    }

    @kotlin.internal.InlineOnly
    private static final <T> T[] copyToArrayImpl(java.util.Collection<?> collection, T[] array) {
        T[] array2 = kotlin.jvm.internal.CollectionToArray.toArray(collection, array);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(array2, "kotlin.jvm.internal.Coll\u2026oArray(collection, array)");
        return array2;
    }

    /* access modifiers changed from: private */
    public static final <T> java.lang.Object[] copyToArrayOfAny$CollectionsKt__CollectionsKt(@org.jetbrains.annotations.NotNull T[] $receiver, boolean isVarargs) {
        if (!isVarargs || !kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) $receiver.getClass(), (java.lang.Object) java.lang.Object[].class)) {
            T[] $receiver2 = java.util.Arrays.copyOf($receiver, $receiver.length, java.lang.Object[].class);
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull($receiver2, "java.util.Arrays.copyOf(\u2026 Array<Any?>::class.java)");
            return $receiver2;
        } else if ($receiver != null) {
            return $receiver;
        } else {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.Array<kotlin.Any?>");
        }
    }

    public static /* bridge */ /* synthetic */ int binarySearch$default(java.util.List list, java.lang.Comparable comparable, int i, int i2, int i3, java.lang.Object obj) {
        if ((i3 & 2) != 0) {
            i = 0;
        }
        if ((i3 & 4) != 0) {
            i2 = list.size();
        }
        return kotlin.collections.CollectionsKt.binarySearch(list, (T) comparable, i, i2);
    }

    public static final <T extends java.lang.Comparable<? super T>> int binarySearch(@org.jetbrains.annotations.NotNull java.util.List<? extends T> $receiver, @org.jetbrains.annotations.Nullable T element, int fromIndex, int toIndex) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        rangeCheck$CollectionsKt__CollectionsKt($receiver.size(), fromIndex, toIndex);
        int low = fromIndex;
        int high = toIndex - 1;
        while (low <= high) {
            int mid = (low + high) >>> 1;
            int cmp = kotlin.comparisons.ComparisonsKt.compareValues((java.lang.Comparable) $receiver.get(mid), element);
            if (cmp < 0) {
                low = mid + 1;
            } else if (cmp <= 0) {
                return mid;
            } else {
                high = mid - 1;
            }
        }
        return -(low + 1);
    }

    public static /* bridge */ /* synthetic */ int binarySearch$default(java.util.List list, java.lang.Object obj, java.util.Comparator comparator, int i, int i2, int i3, java.lang.Object obj2) {
        if ((i3 & 4) != 0) {
            i = 0;
        }
        if ((i3 & 8) != 0) {
            i2 = list.size();
        }
        return kotlin.collections.CollectionsKt.binarySearch(list, obj, comparator, i, i2);
    }

    public static final <T> int binarySearch(@org.jetbrains.annotations.NotNull java.util.List<? extends T> $receiver, T element, @org.jetbrains.annotations.NotNull java.util.Comparator<? super T> comparator, int fromIndex, int toIndex) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(comparator, "comparator");
        rangeCheck$CollectionsKt__CollectionsKt($receiver.size(), fromIndex, toIndex);
        int low = fromIndex;
        int high = toIndex - 1;
        while (low <= high) {
            int mid = (low + high) >>> 1;
            int cmp = comparator.compare($receiver.get(mid), element);
            if (cmp < 0) {
                low = mid + 1;
            } else if (cmp <= 0) {
                return mid;
            } else {
                high = mid - 1;
            }
        }
        return -(low + 1);
    }

    public static /* bridge */ /* synthetic */ int binarySearchBy$default(java.util.List $receiver, java.lang.Comparable key, int fromIndex, int toIndex, kotlin.jvm.functions.Function1 selector, int i, java.lang.Object obj) {
        if ((i & 2) != 0) {
            fromIndex = 0;
        }
        if ((i & 4) != 0) {
            toIndex = $receiver.size();
        }
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(selector, "selector");
        return kotlin.collections.CollectionsKt.binarySearch($receiver, fromIndex, toIndex, (kotlin.jvm.functions.Function1<? super T, java.lang.Integer>) new kotlin.collections.CollectionsKt__CollectionsKt$binarySearchBy$1<java.lang.Object,java.lang.Integer>(selector, key));
    }

    public static final <T, K extends java.lang.Comparable<? super K>> int binarySearchBy(@org.jetbrains.annotations.NotNull java.util.List<? extends T> $receiver, @org.jetbrains.annotations.Nullable K key, int fromIndex, int toIndex, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super T, ? extends K> selector) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(selector, "selector");
        return kotlin.collections.CollectionsKt.binarySearch($receiver, fromIndex, toIndex, (kotlin.jvm.functions.Function1<? super T, java.lang.Integer>) new kotlin.collections.CollectionsKt__CollectionsKt$binarySearchBy$1<java.lang.Object,java.lang.Integer>(selector, key));
    }

    public static /* bridge */ /* synthetic */ int binarySearch$default(java.util.List list, int i, int i2, kotlin.jvm.functions.Function1 function1, int i3, java.lang.Object obj) {
        if ((i3 & 1) != 0) {
            i = 0;
        }
        if ((i3 & 2) != 0) {
            i2 = list.size();
        }
        return kotlin.collections.CollectionsKt.binarySearch(list, i, i2, function1);
    }

    public static final <T> int binarySearch(@org.jetbrains.annotations.NotNull java.util.List<? extends T> $receiver, int fromIndex, int toIndex, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super T, java.lang.Integer> comparison) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(comparison, "comparison");
        rangeCheck$CollectionsKt__CollectionsKt($receiver.size(), fromIndex, toIndex);
        int low = fromIndex;
        int high = toIndex - 1;
        while (low <= high) {
            int mid = (low + high) >>> 1;
            int cmp = ((java.lang.Number) comparison.invoke($receiver.get(mid))).intValue();
            if (cmp < 0) {
                low = mid + 1;
            } else if (cmp <= 0) {
                return mid;
            } else {
                high = mid - 1;
            }
        }
        return -(low + 1);
    }

    private static final void rangeCheck$CollectionsKt__CollectionsKt(int size, int fromIndex, int toIndex) {
        if (fromIndex > toIndex) {
            throw new java.lang.IllegalArgumentException("fromIndex (" + fromIndex + ") is greater than toIndex (" + toIndex + ").");
        } else if (fromIndex < 0) {
            throw new java.lang.IndexOutOfBoundsException("fromIndex (" + fromIndex + ") is less than zero.");
        } else if (toIndex > size) {
            throw new java.lang.IndexOutOfBoundsException("toIndex (" + toIndex + ") is greater than size (" + size + ").");
        }
    }
}
