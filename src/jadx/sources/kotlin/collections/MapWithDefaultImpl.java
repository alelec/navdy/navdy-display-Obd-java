package kotlin.collections;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\"\n\u0002\u0010&\n\u0002\b\u0007\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u001e\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0000\b\u0002\u0018\u0000*\u0004\b\u0000\u0010\u0001*\u0006\b\u0001\u0010\u0002 \u00012\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u00020\u0003B<\u0012\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0005\u0012!\u0010\u0006\u001a\u001d\u0012\u0013\u0012\u00118\u0000\u00a2\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\n\u0012\u0004\u0012\u00028\u00010\u0007\u00a2\u0006\u0002\u0010\u000bJ\u0015\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\n\u001a\u00028\u0000H\u0016\u00a2\u0006\u0002\u0010\u001fJ\u001a\u0010 \u001a\u00020\u001e2\u000b\u0010!\u001a\u00078\u0001\u00a2\u0006\u0002\b\"H\u0016\u00a2\u0006\u0002\u0010\u001fJ\u0013\u0010#\u001a\u00020\u001e2\b\u0010$\u001a\u0004\u0018\u00010%H\u0096\u0002J\u0018\u0010&\u001a\u0004\u0018\u00018\u00012\u0006\u0010\n\u001a\u00028\u0000H\u0096\u0002\u00a2\u0006\u0002\u0010'J\u0015\u0010(\u001a\u00028\u00012\u0006\u0010\n\u001a\u00028\u0000H\u0016\u00a2\u0006\u0002\u0010'J\b\u0010)\u001a\u00020\u0016H\u0016J\b\u0010*\u001a\u00020\u001eH\u0016J\b\u0010+\u001a\u00020,H\u0016R)\u0010\u0006\u001a\u001d\u0012\u0013\u0012\u00118\u0000\u00a2\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\n\u0012\u0004\u0012\u00028\u00010\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R&\u0010\f\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u000e0\r8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\b\u000f\u0010\u0010R\u001a\u0010\u0011\u001a\b\u0012\u0004\u0012\u00028\u00000\r8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0012\u0010\u0010R \u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0005X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0014\u0010\u0015\u001a\u00020\u00168VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0017\u0010\u0018R\u001a\u0010\u0019\u001a\b\u0012\u0004\u0012\u00028\u00010\u001a8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\b\u001b\u0010\u001c\u00a8\u0006-"}, d2 = {"Lkotlin/collections/MapWithDefaultImpl;", "K", "V", "Lkotlin/collections/MapWithDefault;", "map", "", "default", "Lkotlin/Function1;", "Lkotlin/ParameterName;", "name", "key", "(Ljava/util/Map;Lkotlin/jvm/functions/Function1;)V", "entries", "", "", "getEntries", "()Ljava/util/Set;", "keys", "getKeys", "getMap", "()Ljava/util/Map;", "size", "", "getSize", "()I", "values", "", "getValues", "()Ljava/util/Collection;", "containsKey", "", "(Ljava/lang/Object;)Z", "containsValue", "value", "Lkotlin/UnsafeVariance;", "equals", "other", "", "get", "(Ljava/lang/Object;)Ljava/lang/Object;", "getOrImplicitDefault", "hashCode", "isEmpty", "toString", "", "kotlin-stdlib"}, k = 1, mv = {1, 1, 6})
/* compiled from: MapWithDefault.kt */
final class MapWithDefaultImpl<K, V> implements kotlin.collections.MapWithDefault<K, V> {

    /* renamed from: default reason: not valid java name */
    private final kotlin.jvm.functions.Function1<K, V> f1default;
    @org.jetbrains.annotations.NotNull
    private final java.util.Map<K, V> map;

    public void clear() {
        throw new java.lang.UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public V put(K k, V v) {
        throw new java.lang.UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public void putAll(java.util.Map<? extends K, ? extends V> map2) {
        throw new java.lang.UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public V remove(java.lang.Object obj) {
        throw new java.lang.UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public MapWithDefaultImpl(@org.jetbrains.annotations.NotNull java.util.Map<K, ? extends V> map2, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super K, ? extends V> function1) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(map2, "map");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(function1, "default");
        this.map = map2;
        this.f1default = function1;
    }

    public final /* bridge */ java.util.Set entrySet() {
        return getEntries();
    }

    @org.jetbrains.annotations.NotNull
    public java.util.Map<K, V> getMap() {
        return this.map;
    }

    public final /* bridge */ java.util.Set keySet() {
        return getKeys();
    }

    public final /* bridge */ int size() {
        return getSize();
    }

    public final /* bridge */ java.util.Collection values() {
        return getValues();
    }

    public boolean equals(@org.jetbrains.annotations.Nullable java.lang.Object other) {
        return getMap().equals(other);
    }

    public int hashCode() {
        return getMap().hashCode();
    }

    @org.jetbrains.annotations.NotNull
    public java.lang.String toString() {
        return getMap().toString();
    }

    public int getSize() {
        return getMap().size();
    }

    public boolean isEmpty() {
        return getMap().isEmpty();
    }

    public boolean containsKey(java.lang.Object key) {
        return getMap().containsKey(key);
    }

    public boolean containsValue(java.lang.Object value) {
        return getMap().containsValue(value);
    }

    @org.jetbrains.annotations.Nullable
    public V get(java.lang.Object key) {
        return getMap().get(key);
    }

    @org.jetbrains.annotations.NotNull
    public java.util.Set<K> getKeys() {
        return getMap().keySet();
    }

    @org.jetbrains.annotations.NotNull
    public java.util.Collection<V> getValues() {
        return getMap().values();
    }

    @org.jetbrains.annotations.NotNull
    public java.util.Set<java.util.Map.Entry<K, V>> getEntries() {
        return getMap().entrySet();
    }

    public V getOrImplicitDefault(K key) {
        java.util.Map $receiver$iv = getMap();
        java.lang.Object value$iv = $receiver$iv.get(key);
        return (value$iv != null || $receiver$iv.containsKey(key)) ? value$iv : this.f1default.invoke(key);
    }
}
