package kotlin.collections;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000j\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u001f\n\u0000\n\u0002\u0010\u0011\n\u0000\n\u0002\u0010\u001c\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u001d\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010!\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u001e\n\u0002\b\u0004\n\u0002\u0010\u000f\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u001a-\u0010\u0000\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\n\u0012\u0006\b\u0000\u0012\u0002H\u00020\u00032\u000e\u0010\u0004\u001a\n\u0012\u0006\b\u0001\u0012\u0002H\u00020\u0005\u00a2\u0006\u0002\u0010\u0006\u001a&\u0010\u0000\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\n\u0012\u0006\b\u0000\u0012\u0002H\u00020\u00032\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0007\u001a&\u0010\u0000\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\n\u0012\u0006\b\u0000\u0012\u0002H\u00020\u00032\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u0002H\u00020\b\u001a9\u0010\t\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\n2\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u00020\u00010\f2\u0006\u0010\r\u001a\u00020\u0001H\u0002\u00a2\u0006\u0002\b\u000e\u001a9\u0010\t\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u000f2\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u00020\u00010\f2\u0006\u0010\r\u001a\u00020\u0001H\u0002\u00a2\u0006\u0002\b\u000e\u001a(\u0010\u0010\u001a\u00020\u0011\"\u0004\b\u0000\u0010\u0002*\n\u0012\u0006\b\u0000\u0012\u0002H\u00020\u00032\u0006\u0010\u0012\u001a\u0002H\u0002H\u0087\n\u00a2\u0006\u0002\u0010\u0013\u001a.\u0010\u0010\u001a\u00020\u0011\"\u0004\b\u0000\u0010\u0002*\n\u0012\u0006\b\u0000\u0012\u0002H\u00020\u00032\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0005H\u0087\n\u00a2\u0006\u0002\u0010\u0014\u001a)\u0010\u0010\u001a\u00020\u0011\"\u0004\b\u0000\u0010\u0002*\n\u0012\u0006\b\u0000\u0012\u0002H\u00020\u00032\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0007H\u0087\n\u001a)\u0010\u0010\u001a\u00020\u0011\"\u0004\b\u0000\u0010\u0002*\n\u0012\u0006\b\u0000\u0012\u0002H\u00020\u00032\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u0002H\u00020\bH\u0087\n\u001a(\u0010\u0015\u001a\u00020\u0011\"\u0004\b\u0000\u0010\u0002*\n\u0012\u0006\b\u0000\u0012\u0002H\u00020\u00032\u0006\u0010\u0012\u001a\u0002H\u0002H\u0087\n\u00a2\u0006\u0002\u0010\u0013\u001a.\u0010\u0015\u001a\u00020\u0011\"\u0004\b\u0000\u0010\u0002*\n\u0012\u0006\b\u0000\u0012\u0002H\u00020\u00032\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0005H\u0087\n\u00a2\u0006\u0002\u0010\u0014\u001a)\u0010\u0015\u001a\u00020\u0011\"\u0004\b\u0000\u0010\u0002*\n\u0012\u0006\b\u0000\u0012\u0002H\u00020\u00032\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0007H\u0087\n\u001a)\u0010\u0015\u001a\u00020\u0011\"\u0004\b\u0000\u0010\u0002*\n\u0012\u0006\b\u0000\u0012\u0002H\u00020\u00032\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u0002H\u00020\bH\u0087\n\u001a-\u0010\u0016\u001a\u00020\u0001\"\t\b\u0000\u0010\u0002\u00a2\u0006\u0002\b\u0017*\n\u0012\u0006\b\u0001\u0012\u0002H\u00020\u00032\u0006\u0010\u0012\u001a\u0002H\u0002H\u0087\b\u00a2\u0006\u0002\u0010\u0018\u001a&\u0010\u0016\u001a\u0002H\u0002\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u000f2\u0006\u0010\u0019\u001a\u00020\u001aH\u0087\b\u00a2\u0006\u0002\u0010\u001b\u001a-\u0010\u001c\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\n\u0012\u0006\b\u0000\u0012\u0002H\u00020\u00032\u000e\u0010\u0004\u001a\n\u0012\u0006\b\u0001\u0012\u0002H\u00020\u0005\u00a2\u0006\u0002\u0010\u0006\u001a&\u0010\u001c\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\n\u0012\u0006\b\u0000\u0012\u0002H\u00020\u00032\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0007\u001a&\u0010\u001c\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\n\u0012\u0006\b\u0000\u0012\u0002H\u00020\u00032\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u0002H\u00020\b\u001a.\u0010\u001c\u001a\u00020\u0001\"\t\b\u0000\u0010\u0002\u00a2\u0006\u0002\b\u0017*\n\u0012\u0006\b\u0001\u0012\u0002H\u00020\u00032\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u0002H\u00020\u001dH\u0087\b\u001a*\u0010\u001c\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\n2\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u00020\u00010\f\u001a*\u0010\u001c\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u000f2\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u00020\u00010\f\u001a-\u0010\u001e\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\n\u0012\u0006\b\u0000\u0012\u0002H\u00020\u00032\u000e\u0010\u0004\u001a\n\u0012\u0006\b\u0001\u0012\u0002H\u00020\u0005\u00a2\u0006\u0002\u0010\u0006\u001a&\u0010\u001e\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\n\u0012\u0006\b\u0000\u0012\u0002H\u00020\u00032\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0007\u001a&\u0010\u001e\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\n\u0012\u0006\b\u0000\u0012\u0002H\u00020\u00032\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u0002H\u00020\b\u001a.\u0010\u001e\u001a\u00020\u0001\"\t\b\u0000\u0010\u0002\u00a2\u0006\u0002\b\u0017*\n\u0012\u0006\b\u0001\u0012\u0002H\u00020\u00032\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u0002H\u00020\u001dH\u0087\b\u001a*\u0010\u001e\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\n2\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u00020\u00010\f\u001a*\u0010\u001e\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u000f2\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u00020\u00010\f\u001a\u0015\u0010\u001f\u001a\u00020\u0001*\u0006\u0012\u0002\b\u00030\u0003H\u0002\u00a2\u0006\u0002\b \u001a\"\u0010!\u001a\u00020\u0011\"\u000e\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\"*\b\u0012\u0004\u0012\u0002H\u00020\u000fH\u0007\u001a3\u0010!\u001a\u00020\u0011\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u000f2\u0018\u0010#\u001a\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u00020\u001a0$H\u0087\b\u001a5\u0010!\u001a\u00020\u0011\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u000f2\u001a\u0010%\u001a\u0016\u0012\u0006\b\u0000\u0012\u0002H\u00020&j\n\u0012\u0006\b\u0000\u0012\u0002H\u0002`'H\u0087\b\u001a4\u0010(\u001a\u00020\u0011\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u000f2\u001a\u0010%\u001a\u0016\u0012\u0006\b\u0000\u0012\u0002H\u00020&j\n\u0012\u0006\b\u0000\u0012\u0002H\u0002`'H\u0007\u00a8\u0006)"}, d2 = {"addAll", "", "T", "", "elements", "", "(Ljava/util/Collection;[Ljava/lang/Object;)Z", "", "Lkotlin/sequences/Sequence;", "filterInPlace", "", "predicate", "Lkotlin/Function1;", "predicateResultToRemove", "filterInPlace$CollectionsKt__MutableCollectionsKt", "", "minusAssign", "", "element", "(Ljava/util/Collection;Ljava/lang/Object;)V", "(Ljava/util/Collection;[Ljava/lang/Object;)V", "plusAssign", "remove", "Lkotlin/internal/OnlyInputTypes;", "(Ljava/util/Collection;Ljava/lang/Object;)Z", "index", "", "(Ljava/util/List;I)Ljava/lang/Object;", "removeAll", "", "retainAll", "retainNothing", "retainNothing$CollectionsKt__MutableCollectionsKt", "sort", "", "comparison", "Lkotlin/Function2;", "comparator", "Ljava/util/Comparator;", "Lkotlin/Comparator;", "sortWith", "kotlin-stdlib"}, k = 5, mv = {1, 1, 6}, xi = 1, xs = "kotlin/collections/CollectionsKt")
/* compiled from: MutableCollections.kt */
class CollectionsKt__MutableCollectionsKt extends kotlin.collections.CollectionsKt__IteratorsKt {
    @kotlin.internal.InlineOnly
    private static final <T> boolean remove(@org.jetbrains.annotations.NotNull java.util.Collection<? extends T> $receiver, T element) {
        if ($receiver != null) {
            return kotlin.jvm.internal.TypeIntrinsics.asMutableCollection($receiver).remove(element);
        }
        throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.collections.MutableCollection<T>");
    }

    @kotlin.internal.InlineOnly
    private static final <T> boolean removeAll(@org.jetbrains.annotations.NotNull java.util.Collection<? extends T> $receiver, java.util.Collection<? extends T> elements) {
        if ($receiver != null) {
            return kotlin.jvm.internal.TypeIntrinsics.asMutableCollection($receiver).removeAll(elements);
        }
        throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.collections.MutableCollection<T>");
    }

    @kotlin.internal.InlineOnly
    private static final <T> boolean retainAll(@org.jetbrains.annotations.NotNull java.util.Collection<? extends T> $receiver, java.util.Collection<? extends T> elements) {
        if ($receiver != null) {
            return kotlin.jvm.internal.TypeIntrinsics.asMutableCollection($receiver).retainAll(elements);
        }
        throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.collections.MutableCollection<T>");
    }

    @kotlin.Deprecated(level = kotlin.DeprecationLevel.ERROR, message = "Use removeAt(index) instead.", replaceWith = @kotlin.ReplaceWith(expression = "removeAt(index)", imports = {}))
    @kotlin.internal.InlineOnly
    private static final <T> T remove(@org.jetbrains.annotations.NotNull java.util.List<T> $receiver, int index) {
        return $receiver.remove(index);
    }

    @kotlin.Deprecated(level = kotlin.DeprecationLevel.ERROR, message = "Use sortWith(comparator) instead.", replaceWith = @kotlin.ReplaceWith(expression = "this.sortWith(comparator)", imports = {}))
    @kotlin.internal.InlineOnly
    private static final <T> void sort(@org.jetbrains.annotations.NotNull java.util.List<T> $receiver, java.util.Comparator<? super T> comparator) {
        throw new kotlin.NotImplementedError(null, 1, null);
    }

    @kotlin.Deprecated(level = kotlin.DeprecationLevel.ERROR, message = "Use sortWith(Comparator(comparison)) instead.", replaceWith = @kotlin.ReplaceWith(expression = "this.sortWith(Comparator(comparison))", imports = {}))
    @kotlin.internal.InlineOnly
    private static final <T> void sort(@org.jetbrains.annotations.NotNull java.util.List<T> $receiver, kotlin.jvm.functions.Function2<? super T, ? super T, java.lang.Integer> comparison) {
        throw new kotlin.NotImplementedError(null, 1, null);
    }

    @kotlin.internal.InlineOnly
    private static final <T> void plusAssign(@org.jetbrains.annotations.NotNull java.util.Collection<? super T> $receiver, T element) {
        $receiver.add(element);
    }

    @kotlin.internal.InlineOnly
    private static final <T> void plusAssign(@org.jetbrains.annotations.NotNull java.util.Collection<? super T> $receiver, java.lang.Iterable<? extends T> elements) {
        kotlin.collections.CollectionsKt.addAll($receiver, elements);
    }

    @kotlin.internal.InlineOnly
    private static final <T> void plusAssign(@org.jetbrains.annotations.NotNull java.util.Collection<? super T> $receiver, T[] elements) {
        kotlin.collections.CollectionsKt.addAll($receiver, elements);
    }

    @kotlin.internal.InlineOnly
    private static final <T> void plusAssign(@org.jetbrains.annotations.NotNull java.util.Collection<? super T> $receiver, kotlin.sequences.Sequence<? extends T> elements) {
        kotlin.collections.CollectionsKt.addAll($receiver, elements);
    }

    @kotlin.internal.InlineOnly
    private static final <T> void minusAssign(@org.jetbrains.annotations.NotNull java.util.Collection<? super T> $receiver, T element) {
        $receiver.remove(element);
    }

    @kotlin.internal.InlineOnly
    private static final <T> void minusAssign(@org.jetbrains.annotations.NotNull java.util.Collection<? super T> $receiver, java.lang.Iterable<? extends T> elements) {
        kotlin.collections.CollectionsKt.removeAll($receiver, elements);
    }

    @kotlin.internal.InlineOnly
    private static final <T> void minusAssign(@org.jetbrains.annotations.NotNull java.util.Collection<? super T> $receiver, T[] elements) {
        kotlin.collections.CollectionsKt.removeAll($receiver, elements);
    }

    @kotlin.internal.InlineOnly
    private static final <T> void minusAssign(@org.jetbrains.annotations.NotNull java.util.Collection<? super T> $receiver, kotlin.sequences.Sequence<? extends T> elements) {
        kotlin.collections.CollectionsKt.removeAll($receiver, elements);
    }

    public static final <T> boolean addAll(@org.jetbrains.annotations.NotNull java.util.Collection<? super T> $receiver, @org.jetbrains.annotations.NotNull java.lang.Iterable<? extends T> elements) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(elements, "elements");
        if (elements instanceof java.util.Collection) {
            return $receiver.addAll((java.util.Collection) elements);
        }
        boolean result = false;
        for (java.lang.Object item : elements) {
            if ($receiver.add(item)) {
                result = true;
            }
        }
        return result;
    }

    public static final <T> boolean addAll(@org.jetbrains.annotations.NotNull java.util.Collection<? super T> $receiver, @org.jetbrains.annotations.NotNull kotlin.sequences.Sequence<? extends T> elements) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(elements, "elements");
        boolean result = false;
        for (java.lang.Object item : elements) {
            if ($receiver.add(item)) {
                result = true;
            }
        }
        return result;
    }

    public static final <T> boolean addAll(@org.jetbrains.annotations.NotNull java.util.Collection<? super T> $receiver, @org.jetbrains.annotations.NotNull T[] elements) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(elements, "elements");
        return $receiver.addAll(kotlin.collections.ArraysKt.asList(elements));
    }

    public static final <T> boolean removeAll(@org.jetbrains.annotations.NotNull java.lang.Iterable<? extends T> $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super T, java.lang.Boolean> predicate) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(predicate, "predicate");
        return filterInPlace$CollectionsKt__MutableCollectionsKt($receiver, predicate, true);
    }

    public static final <T> boolean retainAll(@org.jetbrains.annotations.NotNull java.lang.Iterable<? extends T> $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super T, java.lang.Boolean> predicate) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(predicate, "predicate");
        return filterInPlace$CollectionsKt__MutableCollectionsKt($receiver, predicate, false);
    }

    private static final <T> boolean filterInPlace$CollectionsKt__MutableCollectionsKt(@org.jetbrains.annotations.NotNull java.lang.Iterable<? extends T> $receiver, kotlin.jvm.functions.Function1<? super T, java.lang.Boolean> predicate, boolean predicateResultToRemove) {
        kotlin.jvm.internal.Ref.BooleanRef booleanRef = new kotlin.jvm.internal.Ref.BooleanRef();
        booleanRef.element = false;
        java.util.Iterator $receiver2 = $receiver.iterator();
        while ($receiver2.hasNext()) {
            if (((java.lang.Boolean) predicate.invoke($receiver2.next())).booleanValue() == predicateResultToRemove) {
                $receiver2.remove();
                booleanRef.element = true;
            }
        }
        return booleanRef.element;
    }

    public static final <T> boolean removeAll(@org.jetbrains.annotations.NotNull java.util.List<T> $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super T, java.lang.Boolean> predicate) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(predicate, "predicate");
        return filterInPlace$CollectionsKt__MutableCollectionsKt($receiver, predicate, true);
    }

    public static final <T> boolean retainAll(@org.jetbrains.annotations.NotNull java.util.List<T> $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super T, java.lang.Boolean> predicate) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(predicate, "predicate");
        return filterInPlace$CollectionsKt__MutableCollectionsKt($receiver, predicate, false);
    }

    private static final <T> boolean filterInPlace$CollectionsKt__MutableCollectionsKt(@org.jetbrains.annotations.NotNull java.util.List<T> $receiver, kotlin.jvm.functions.Function1<? super T, java.lang.Boolean> predicate, boolean predicateResultToRemove) {
        if ($receiver instanceof java.util.RandomAccess) {
            int writeIndex = 0;
            int lastIndex = kotlin.collections.CollectionsKt.getLastIndex($receiver);
            if (0 <= lastIndex) {
                int readIndex = 0;
                while (true) {
                    java.lang.Object element = $receiver.get(readIndex);
                    if (((java.lang.Boolean) predicate.invoke(element)).booleanValue() != predicateResultToRemove) {
                        if (writeIndex != readIndex) {
                            $receiver.set(writeIndex, element);
                        }
                        writeIndex++;
                    }
                    if (readIndex == lastIndex) {
                        break;
                    }
                    readIndex++;
                }
            }
            if (writeIndex >= $receiver.size()) {
                return false;
            }
            int lastIndex2 = kotlin.collections.CollectionsKt.getLastIndex($receiver);
            if (lastIndex2 >= writeIndex) {
                while (true) {
                    $receiver.remove(lastIndex2);
                    if (lastIndex2 == writeIndex) {
                        break;
                    }
                    lastIndex2--;
                }
            }
            return true;
        } else if ($receiver != null) {
            return filterInPlace$CollectionsKt__MutableCollectionsKt(kotlin.jvm.internal.TypeIntrinsics.asMutableIterable($receiver), predicate, predicateResultToRemove);
        } else {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.collections.MutableIterable<T>");
        }
    }

    public static final <T> boolean removeAll(@org.jetbrains.annotations.NotNull java.util.Collection<? super T> $receiver, @org.jetbrains.annotations.NotNull java.lang.Iterable<? extends T> elements) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(elements, "elements");
        return kotlin.jvm.internal.TypeIntrinsics.asMutableCollection($receiver).removeAll(kotlin.collections.CollectionsKt.convertToSetForSetOperationWith(elements, $receiver));
    }

    public static final <T> boolean removeAll(@org.jetbrains.annotations.NotNull java.util.Collection<? super T> $receiver, @org.jetbrains.annotations.NotNull kotlin.sequences.Sequence<? extends T> elements) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(elements, "elements");
        java.util.HashSet set = kotlin.sequences.SequencesKt.toHashSet(elements);
        return (!set.isEmpty()) && $receiver.removeAll(set);
    }

    public static final <T> boolean removeAll(@org.jetbrains.annotations.NotNull java.util.Collection<? super T> $receiver, @org.jetbrains.annotations.NotNull T[] elements) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(elements, "elements");
        return (!(elements.length == 0)) && $receiver.removeAll(kotlin.collections.ArraysKt.toHashSet(elements));
    }

    public static final <T> boolean retainAll(@org.jetbrains.annotations.NotNull java.util.Collection<? super T> $receiver, @org.jetbrains.annotations.NotNull java.lang.Iterable<? extends T> elements) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(elements, "elements");
        return kotlin.jvm.internal.TypeIntrinsics.asMutableCollection($receiver).retainAll(kotlin.collections.CollectionsKt.convertToSetForSetOperationWith(elements, $receiver));
    }

    public static final <T> boolean retainAll(@org.jetbrains.annotations.NotNull java.util.Collection<? super T> $receiver, @org.jetbrains.annotations.NotNull T[] elements) {
        boolean z;
        boolean z2 = true;
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(elements, "elements");
        if (elements.length == 0) {
            z = true;
        } else {
            z = false;
        }
        if (z) {
            z2 = false;
        }
        if (z2) {
            return $receiver.retainAll(kotlin.collections.ArraysKt.toHashSet(elements));
        }
        return retainNothing$CollectionsKt__MutableCollectionsKt($receiver);
    }

    public static final <T> boolean retainAll(@org.jetbrains.annotations.NotNull java.util.Collection<? super T> $receiver, @org.jetbrains.annotations.NotNull kotlin.sequences.Sequence<? extends T> elements) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(elements, "elements");
        java.util.HashSet set = kotlin.sequences.SequencesKt.toHashSet(elements);
        if (!set.isEmpty()) {
            return $receiver.retainAll(set);
        }
        return retainNothing$CollectionsKt__MutableCollectionsKt($receiver);
    }

    private static final boolean retainNothing$CollectionsKt__MutableCollectionsKt(@org.jetbrains.annotations.NotNull java.util.Collection<?> $receiver) {
        boolean result = !$receiver.isEmpty();
        $receiver.clear();
        return result;
    }

    public static final <T extends java.lang.Comparable<? super T>> void sort(@org.jetbrains.annotations.NotNull java.util.List<T> $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        if ($receiver.size() > 1) {
            java.util.Collections.sort($receiver);
        }
    }

    public static final <T> void sortWith(@org.jetbrains.annotations.NotNull java.util.List<T> $receiver, @org.jetbrains.annotations.NotNull java.util.Comparator<? super T> comparator) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(comparator, "comparator");
        if ($receiver.size() > 1) {
            java.util.Collections.sort($receiver, comparator);
        }
    }
}
