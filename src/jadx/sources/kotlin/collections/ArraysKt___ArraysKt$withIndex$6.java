package kotlin.collections;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\b\u0002"}, d2 = {"<anonymous>", "Lkotlin/collections/FloatIterator;", "invoke"}, k = 3, mv = {1, 1, 6})
/* compiled from: _Arrays.kt */
final class ArraysKt___ArraysKt$withIndex$6 extends kotlin.jvm.internal.Lambda implements kotlin.jvm.functions.Function0<kotlin.collections.FloatIterator> {
    final /* synthetic */ float[] receiver$0;

    ArraysKt___ArraysKt$withIndex$6(float[] fArr) {
        this.receiver$0 = fArr;
        super(0);
    }

    @org.jetbrains.annotations.NotNull
    public final kotlin.collections.FloatIterator invoke() {
        return kotlin.jvm.internal.ArrayIteratorsKt.iterator(this.receiver$0);
    }
}
