package kotlin.collections;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0018\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0010!\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\u001a\u001c\u0010\u0000\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u0001\u001a#\u0010\u0000\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0003\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u0003H\u0007\u00a2\u0006\u0002\b\u0004\u001a\u001d\u0010\u0005\u001a\u00020\u0006*\u0006\u0012\u0002\b\u00030\u00012\u0006\u0010\u0007\u001a\u00020\u0006H\u0002\u00a2\u0006\u0002\b\b\u001a\u001d\u0010\t\u001a\u00020\u0006*\u0006\u0012\u0002\b\u00030\u00012\u0006\u0010\u0007\u001a\u00020\u0006H\u0002\u00a2\u0006\u0002\b\n\u00a8\u0006\u000b"}, d2 = {"asReversed", "", "T", "", "asReversedMutable", "reverseElementIndex", "", "index", "reverseElementIndex$CollectionsKt__ReversedViewsKt", "reversePositionIndex", "reversePositionIndex$CollectionsKt__ReversedViewsKt", "kotlin-stdlib"}, k = 5, mv = {1, 1, 6}, xi = 1, xs = "kotlin/collections/CollectionsKt")
/* compiled from: ReversedViews.kt */
class CollectionsKt__ReversedViewsKt extends kotlin.collections.CollectionsKt__MutableCollectionsKt {
    /* access modifiers changed from: private */
    public static final int reverseElementIndex$CollectionsKt__ReversedViewsKt(@org.jetbrains.annotations.NotNull java.util.List<?> $receiver, int index) {
        if (index >= 0 && index <= kotlin.collections.CollectionsKt.getLastIndex($receiver)) {
            return kotlin.collections.CollectionsKt.getLastIndex($receiver) - index;
        }
        throw new java.lang.IndexOutOfBoundsException("Element index " + index + " must be in range [" + new kotlin.ranges.IntRange(0, kotlin.collections.CollectionsKt.getLastIndex($receiver)) + "].");
    }

    /* access modifiers changed from: private */
    public static final int reversePositionIndex$CollectionsKt__ReversedViewsKt(@org.jetbrains.annotations.NotNull java.util.List<?> $receiver, int index) {
        if (index >= 0 && index <= $receiver.size()) {
            return $receiver.size() - index;
        }
        throw new java.lang.IndexOutOfBoundsException("Position index " + index + " must be in range [" + new kotlin.ranges.IntRange(0, $receiver.size()) + "].");
    }

    @org.jetbrains.annotations.NotNull
    public static final <T> java.util.List<T> asReversed(@org.jetbrains.annotations.NotNull java.util.List<? extends T> $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        return new kotlin.collections.ReversedListReadOnly<>($receiver);
    }

    @org.jetbrains.annotations.NotNull
    @kotlin.jvm.JvmName(name = "asReversedMutable")
    public static final <T> java.util.List<T> asReversedMutable(@org.jetbrains.annotations.NotNull java.util.List<T> $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        return new kotlin.collections.ReversedList<>($receiver);
    }
}
