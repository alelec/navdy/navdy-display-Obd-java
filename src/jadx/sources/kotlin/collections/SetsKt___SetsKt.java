package kotlin.collections;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u001c\n\u0000\n\u0002\u0010\"\n\u0002\b\u0004\n\u0002\u0010\u0011\n\u0000\n\u0002\u0010\u001c\n\u0002\u0018\u0002\n\u0002\b\u0004\u001a,\u0010\u0000\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u00012\u0006\u0010\u0003\u001a\u0002H\u0002H\u0086\u0002\u00a2\u0006\u0002\u0010\u0004\u001a4\u0010\u0000\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u00012\u000e\u0010\u0005\u001a\n\u0012\u0006\b\u0001\u0012\u0002H\u00020\u0006H\u0086\u0002\u00a2\u0006\u0002\u0010\u0007\u001a-\u0010\u0000\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u00012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u0002H\u00020\bH\u0086\u0002\u001a-\u0010\u0000\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u00012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u0002H\u00020\tH\u0086\u0002\u001a,\u0010\n\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u00012\u0006\u0010\u0003\u001a\u0002H\u0002H\u0087\b\u00a2\u0006\u0002\u0010\u0004\u001a,\u0010\u000b\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u00012\u0006\u0010\u0003\u001a\u0002H\u0002H\u0086\u0002\u00a2\u0006\u0002\u0010\u0004\u001a4\u0010\u000b\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u00012\u000e\u0010\u0005\u001a\n\u0012\u0006\b\u0001\u0012\u0002H\u00020\u0006H\u0086\u0002\u00a2\u0006\u0002\u0010\u0007\u001a-\u0010\u000b\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u00012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u0002H\u00020\bH\u0086\u0002\u001a-\u0010\u000b\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u00012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u0002H\u00020\tH\u0086\u0002\u001a,\u0010\f\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u00012\u0006\u0010\u0003\u001a\u0002H\u0002H\u0087\b\u00a2\u0006\u0002\u0010\u0004\u00a8\u0006\r"}, d2 = {"minus", "", "T", "element", "(Ljava/util/Set;Ljava/lang/Object;)Ljava/util/Set;", "elements", "", "(Ljava/util/Set;[Ljava/lang/Object;)Ljava/util/Set;", "", "Lkotlin/sequences/Sequence;", "minusElement", "plus", "plusElement", "kotlin-stdlib"}, k = 5, mv = {1, 1, 6}, xi = 1, xs = "kotlin/collections/SetsKt")
/* compiled from: _Sets.kt */
class SetsKt___SetsKt extends kotlin.collections.SetsKt__SetsKt {
    @org.jetbrains.annotations.NotNull
    public static final <T> java.util.Set<T> minus(@org.jetbrains.annotations.NotNull java.util.Set<? extends T> $receiver, T element) {
        boolean z;
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        java.util.LinkedHashSet result = new java.util.LinkedHashSet(kotlin.collections.MapsKt.mapCapacity($receiver.size()));
        kotlin.jvm.internal.Ref.BooleanRef booleanRef = new kotlin.jvm.internal.Ref.BooleanRef();
        booleanRef.element = false;
        for (java.lang.Object element$iv : $receiver) {
            java.lang.Object it = element$iv;
            if (booleanRef.element || !kotlin.jvm.internal.Intrinsics.areEqual(it, (java.lang.Object) element)) {
                z = true;
            } else {
                booleanRef.element = true;
                z = false;
            }
            if (z) {
                result.add(element$iv);
            }
        }
        return result;
    }

    @org.jetbrains.annotations.NotNull
    public static final <T> java.util.Set<T> minus(@org.jetbrains.annotations.NotNull java.util.Set<? extends T> $receiver, @org.jetbrains.annotations.NotNull T[] elements) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(elements, "elements");
        java.util.LinkedHashSet result = new java.util.LinkedHashSet($receiver);
        kotlin.collections.CollectionsKt.removeAll((java.util.Collection<? super T>) result, elements);
        return result;
    }

    @org.jetbrains.annotations.NotNull
    public static final <T> java.util.Set<T> minus(@org.jetbrains.annotations.NotNull java.util.Set<? extends T> $receiver, @org.jetbrains.annotations.NotNull java.lang.Iterable<? extends T> elements) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(elements, "elements");
        java.util.Collection other = kotlin.collections.CollectionsKt.convertToSetForSetOperationWith(elements, $receiver);
        if (other.isEmpty()) {
            return kotlin.collections.CollectionsKt.toSet($receiver);
        }
        if (other instanceof java.util.Set) {
            java.lang.Iterable iterable = $receiver;
            java.util.Collection destination$iv = new java.util.LinkedHashSet();
            for (java.lang.Object element$iv : iterable) {
                if (!other.contains(element$iv)) {
                    destination$iv.add(element$iv);
                }
            }
            return (java.util.Set) destination$iv;
        }
        java.util.LinkedHashSet result = new java.util.LinkedHashSet($receiver);
        result.removeAll(other);
        return result;
    }

    @org.jetbrains.annotations.NotNull
    public static final <T> java.util.Set<T> minus(@org.jetbrains.annotations.NotNull java.util.Set<? extends T> $receiver, @org.jetbrains.annotations.NotNull kotlin.sequences.Sequence<? extends T> elements) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(elements, "elements");
        java.util.LinkedHashSet result = new java.util.LinkedHashSet($receiver);
        kotlin.collections.CollectionsKt.removeAll((java.util.Collection<? super T>) result, elements);
        return result;
    }

    @kotlin.internal.InlineOnly
    private static final <T> java.util.Set<T> minusElement(@org.jetbrains.annotations.NotNull java.util.Set<? extends T> $receiver, T element) {
        return kotlin.collections.SetsKt.minus($receiver, element);
    }

    @org.jetbrains.annotations.NotNull
    public static final <T> java.util.Set<T> plus(@org.jetbrains.annotations.NotNull java.util.Set<? extends T> $receiver, T element) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        java.util.LinkedHashSet result = new java.util.LinkedHashSet(kotlin.collections.MapsKt.mapCapacity($receiver.size() + 1));
        result.addAll($receiver);
        result.add(element);
        return result;
    }

    @org.jetbrains.annotations.NotNull
    public static final <T> java.util.Set<T> plus(@org.jetbrains.annotations.NotNull java.util.Set<? extends T> $receiver, @org.jetbrains.annotations.NotNull T[] elements) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(elements, "elements");
        java.util.LinkedHashSet result = new java.util.LinkedHashSet(kotlin.collections.MapsKt.mapCapacity($receiver.size() + elements.length));
        result.addAll($receiver);
        kotlin.collections.CollectionsKt.addAll((java.util.Collection<? super T>) result, elements);
        return result;
    }

    @org.jetbrains.annotations.NotNull
    public static final <T> java.util.Set<T> plus(@org.jetbrains.annotations.NotNull java.util.Set<? extends T> $receiver, @org.jetbrains.annotations.NotNull java.lang.Iterable<? extends T> elements) {
        java.util.LinkedHashSet linkedHashSet;
        int size;
        java.util.LinkedHashSet result;
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(elements, "elements");
        java.lang.Integer collectionSizeOrNull = kotlin.collections.CollectionsKt.collectionSizeOrNull(elements);
        if (collectionSizeOrNull != null) {
            size = $receiver.size() + collectionSizeOrNull.intValue();
            result = linkedHashSet;
        } else {
            size = $receiver.size() * 2;
            result = linkedHashSet;
        }
        linkedHashSet = new java.util.LinkedHashSet(kotlin.collections.MapsKt.mapCapacity(size));
        result.addAll($receiver);
        kotlin.collections.CollectionsKt.addAll((java.util.Collection<? super T>) result, elements);
        return result;
    }

    @org.jetbrains.annotations.NotNull
    public static final <T> java.util.Set<T> plus(@org.jetbrains.annotations.NotNull java.util.Set<? extends T> $receiver, @org.jetbrains.annotations.NotNull kotlin.sequences.Sequence<? extends T> elements) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(elements, "elements");
        java.util.LinkedHashSet result = new java.util.LinkedHashSet(kotlin.collections.MapsKt.mapCapacity($receiver.size() * 2));
        result.addAll($receiver);
        kotlin.collections.CollectionsKt.addAll((java.util.Collection<? super T>) result, elements);
        return result;
    }

    @kotlin.internal.InlineOnly
    private static final <T> java.util.Set<T> plusElement(@org.jetbrains.annotations.NotNull java.util.Set<? extends T> $receiver, T element) {
        return kotlin.collections.SetsKt.plus($receiver, element);
    }
}
