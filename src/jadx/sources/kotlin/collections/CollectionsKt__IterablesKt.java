package kotlin.collections;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000:\n\u0000\n\u0002\u0010\u001c\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010(\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u001e\n\u0002\b\u0003\n\u0002\u0010 \n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u001a+\u0010\u0000\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\b\u0000\u0010\u00022\u0014\b\u0004\u0010\u0003\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00020\u00050\u0004H\u0087\b\u001a \u0010\u0006\u001a\u00020\u0007\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u00012\u0006\u0010\b\u001a\u00020\u0007H\u0001\u001a\u001f\u0010\t\u001a\u0004\u0018\u00010\u0007\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u0001H\u0001\u00a2\u0006\u0002\u0010\n\u001a\u001e\u0010\u000b\u001a\b\u0012\u0004\u0012\u0002H\u00020\f\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u0001H\u0000\u001a,\u0010\r\u001a\b\u0012\u0004\u0012\u0002H\u00020\f\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u00012\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001H\u0000\u001a\"\u0010\u000f\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0010\"\u0004\b\u0000\u0010\u0002*\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00020\u00010\u0001\u001a\u001d\u0010\u0011\u001a\u00020\u0012\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\fH\u0002\u00a2\u0006\u0002\b\u0013\u001a@\u0010\u0014\u001a\u001a\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00020\u0010\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00160\u00100\u0015\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0016*\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00160\u00150\u0001\u00a8\u0006\u0017"}, d2 = {"Iterable", "", "T", "iterator", "Lkotlin/Function0;", "", "collectionSizeOrDefault", "", "default", "collectionSizeOrNull", "(Ljava/lang/Iterable;)Ljava/lang/Integer;", "convertToSetForSetOperation", "", "convertToSetForSetOperationWith", "source", "flatten", "", "safeToConvertToSet", "", "safeToConvertToSet$CollectionsKt__IterablesKt", "unzip", "Lkotlin/Pair;", "R", "kotlin-stdlib"}, k = 5, mv = {1, 1, 6}, xi = 1, xs = "kotlin/collections/CollectionsKt")
/* compiled from: Iterables.kt */
class CollectionsKt__IterablesKt extends kotlin.collections.CollectionsKt__CollectionsKt {
    @kotlin.internal.InlineOnly
    private static final <T> java.lang.Iterable<T> Iterable(kotlin.jvm.functions.Function0<? extends java.util.Iterator<? extends T>> iterator) {
        return new kotlin.collections.CollectionsKt__IterablesKt$Iterable$1<>(iterator);
    }

    @org.jetbrains.annotations.Nullable
    @kotlin.PublishedApi
    public static final <T> java.lang.Integer collectionSizeOrNull(@org.jetbrains.annotations.NotNull java.lang.Iterable<? extends T> $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        if ($receiver instanceof java.util.Collection) {
            return java.lang.Integer.valueOf(((java.util.Collection) $receiver).size());
        }
        return null;
    }

    @kotlin.PublishedApi
    public static final <T> int collectionSizeOrDefault(@org.jetbrains.annotations.NotNull java.lang.Iterable<? extends T> $receiver, int i) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        return $receiver instanceof java.util.Collection ? ((java.util.Collection) $receiver).size() : i;
    }

    private static final <T> boolean safeToConvertToSet$CollectionsKt__IterablesKt(@org.jetbrains.annotations.NotNull java.util.Collection<? extends T> $receiver) {
        return $receiver.size() > 2 && ($receiver instanceof java.util.ArrayList);
    }

    @org.jetbrains.annotations.NotNull
    public static final <T> java.util.Collection<T> convertToSetForSetOperationWith(@org.jetbrains.annotations.NotNull java.lang.Iterable<? extends T> $receiver, @org.jetbrains.annotations.NotNull java.lang.Iterable<? extends T> source) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(source, "source");
        if ($receiver instanceof java.util.Set) {
            return (java.util.Collection) $receiver;
        }
        if (!($receiver instanceof java.util.Collection)) {
            return kotlin.collections.CollectionsKt.toHashSet($receiver);
        }
        if (!(source instanceof java.util.Collection) || ((java.util.Collection) source).size() >= 2) {
            return safeToConvertToSet$CollectionsKt__IterablesKt((java.util.Collection) $receiver) ? kotlin.collections.CollectionsKt.toHashSet($receiver) : (java.util.Collection) $receiver;
        }
        return (java.util.Collection) $receiver;
    }

    @org.jetbrains.annotations.NotNull
    public static final <T> java.util.Collection<T> convertToSetForSetOperation(@org.jetbrains.annotations.NotNull java.lang.Iterable<? extends T> $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        if ($receiver instanceof java.util.Set) {
            return (java.util.Collection) $receiver;
        }
        if ($receiver instanceof java.util.Collection) {
            return safeToConvertToSet$CollectionsKt__IterablesKt((java.util.Collection) $receiver) ? kotlin.collections.CollectionsKt.toHashSet($receiver) : (java.util.Collection) $receiver;
        }
        return kotlin.collections.CollectionsKt.toHashSet($receiver);
    }

    @org.jetbrains.annotations.NotNull
    public static final <T> java.util.List<T> flatten(@org.jetbrains.annotations.NotNull java.lang.Iterable<? extends java.lang.Iterable<? extends T>> $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        java.util.ArrayList result = new java.util.ArrayList();
        for (java.lang.Iterable element : $receiver) {
            kotlin.collections.CollectionsKt.addAll((java.util.Collection) result, element);
        }
        return result;
    }

    @org.jetbrains.annotations.NotNull
    public static final <T, R> kotlin.Pair<java.util.List<T>, java.util.List<R>> unzip(@org.jetbrains.annotations.NotNull java.lang.Iterable<? extends kotlin.Pair<? extends T, ? extends R>> $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        int expectedSize = kotlin.collections.CollectionsKt.collectionSizeOrDefault($receiver, 10);
        java.util.ArrayList listT = new java.util.ArrayList(expectedSize);
        java.util.ArrayList listR = new java.util.ArrayList(expectedSize);
        for (kotlin.Pair pair : $receiver) {
            listT.add(pair.getFirst());
            listR.add(pair.getSecond());
        }
        return kotlin.TuplesKt.to(listT, listR);
    }
}
