package kotlin.collections;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000~\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010$\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010%\n\u0000\n\u0002\u0010&\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010(\n\u0002\u0010)\n\u0002\u0010'\n\u0002\b\u000b\n\u0002\u0010\u001c\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0017\u001a\u001e\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0003\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0005\u001a1\u0010\u0006\u001a\u001e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0007j\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u0005`\b\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0005H\u0087\b\u001a_\u0010\u0006\u001a\u001e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0007j\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u0005`\b\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u00052*\u0010\t\u001a\u0016\u0012\u0012\b\u0001\u0012\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u000b0\n\"\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u000b\u00a2\u0006\u0002\u0010\f\u001a1\u0010\r\u001a\u001e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u000ej\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u0005`\u000f\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0005H\u0087\b\u001a_\u0010\r\u001a\u001e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u000ej\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u0005`\u000f\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u00052*\u0010\t\u001a\u0016\u0012\u0012\b\u0001\u0012\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u000b0\n\"\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u000b\u00a2\u0006\u0002\u0010\u0010\u001a\u0010\u0010\u0011\u001a\u00020\u00012\u0006\u0010\u0012\u001a\u00020\u0001H\u0001\u001a!\u0010\u0013\u001a\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0003\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0005H\u0087\b\u001aO\u0010\u0013\u001a\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0003\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u00052*\u0010\t\u001a\u0016\u0012\u0012\b\u0001\u0012\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u000b0\n\"\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u000b\u00a2\u0006\u0002\u0010\u0014\u001a4\u0010\u0013\u001a\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0003\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u00052\u0012\u0010\u0015\u001a\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u000bH\u0007\u001a!\u0010\u0016\u001a\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0017\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0005H\u0087\b\u001aO\u0010\u0016\u001a\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0017\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u00052*\u0010\t\u001a\u0016\u0012\u0012\b\u0001\u0012\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u000b0\n\"\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u000b\u00a2\u0006\u0002\u0010\u0014\u001a*\u0010\u0018\u001a\u0002H\u0004\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0005*\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0019H\u0087\n\u00a2\u0006\u0002\u0010\u001a\u001a*\u0010\u001b\u001a\u0002H\u0005\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0005*\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0019H\u0087\n\u00a2\u0006\u0002\u0010\u001a\u001a9\u0010\u001c\u001a\u00020\u001d\"\t\b\u0000\u0010\u0004\u00a2\u0006\u0002\b\u001e\"\u0004\b\u0001\u0010\u0005*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u00032\u0006\u0010\u001f\u001a\u0002H\u0004H\u0087\n\u00a2\u0006\u0002\u0010 \u001a1\u0010!\u001a\u00020\u001d\"\t\b\u0000\u0010\u0004\u00a2\u0006\u0002\b\u001e*\u000e\u0012\u0006\b\u0001\u0012\u0002H\u0004\u0012\u0002\b\u00030\u00032\u0006\u0010\u001f\u001a\u0002H\u0004H\u0087\b\u00a2\u0006\u0002\u0010 \u001a7\u0010\"\u001a\u00020\u001d\"\u0004\b\u0000\u0010\u0004\"\t\b\u0001\u0010\u0005\u00a2\u0006\u0002\b\u001e*\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u00032\u0006\u0010#\u001a\u0002H\u0005H\u0087\b\u00a2\u0006\u0002\u0010 \u001aS\u0010$\u001a\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0003\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0005*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u00032\u001e\u0010%\u001a\u001a\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0019\u0012\u0004\u0012\u00020\u001d0&H\u0086\b\u001aG\u0010'\u001a\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0003\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0005*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u00032\u0012\u0010%\u001a\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u00020\u001d0&H\u0086\b\u001aS\u0010(\u001a\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0003\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0005*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u00032\u001e\u0010%\u001a\u001a\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0019\u0012\u0004\u0012\u00020\u001d0&H\u0086\b\u001an\u0010)\u001a\u0002H*\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0005\"\u0018\b\u0002\u0010**\u0012\u0012\u0006\b\u0000\u0012\u0002H\u0004\u0012\u0006\b\u0000\u0012\u0002H\u00050\u0017*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u00032\u0006\u0010+\u001a\u0002H*2\u001e\u0010%\u001a\u001a\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0019\u0012\u0004\u0012\u00020\u001d0&H\u0086\b\u00a2\u0006\u0002\u0010,\u001an\u0010-\u001a\u0002H*\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0005\"\u0018\b\u0002\u0010**\u0012\u0012\u0006\b\u0000\u0012\u0002H\u0004\u0012\u0006\b\u0000\u0012\u0002H\u00050\u0017*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u00032\u0006\u0010+\u001a\u0002H*2\u001e\u0010%\u001a\u001a\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0019\u0012\u0004\u0012\u00020\u001d0&H\u0086\b\u00a2\u0006\u0002\u0010,\u001aG\u0010.\u001a\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0003\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0005*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u00032\u0012\u0010%\u001a\u000e\u0012\u0004\u0012\u0002H\u0005\u0012\u0004\u0012\u00020\u001d0&H\u0086\b\u001a;\u0010/\u001a\u0004\u0018\u0001H\u0005\"\t\b\u0000\u0010\u0004\u00a2\u0006\u0002\b\u001e\"\u0004\b\u0001\u0010\u0005*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u00032\u0006\u0010\u001f\u001a\u0002H\u0004H\u0087\n\u00a2\u0006\u0002\u00100\u001a@\u00101\u001a\u0002H\u0005\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0005*\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u00032\u0006\u0010\u001f\u001a\u0002H\u00042\f\u00102\u001a\b\u0012\u0004\u0012\u0002H\u000503H\u0087\b\u00a2\u0006\u0002\u00104\u001a@\u00105\u001a\u0002H\u0005\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0005*\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u00032\u0006\u0010\u001f\u001a\u0002H\u00042\f\u00102\u001a\b\u0012\u0004\u0012\u0002H\u000503H\u0080\b\u00a2\u0006\u0002\u00104\u001a@\u00106\u001a\u0002H\u0005\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0005*\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u00172\u0006\u0010\u001f\u001a\u0002H\u00042\f\u00102\u001a\b\u0012\u0004\u0012\u0002H\u000503H\u0086\b\u00a2\u0006\u0002\u00104\u001a1\u00107\u001a\u0002H\u0005\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0005*\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u00032\u0006\u0010\u001f\u001a\u0002H\u0004H\u0007\u00a2\u0006\u0002\u00100\u001a'\u00108\u001a\u00020\u001d\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0005*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0003H\u0087\b\u001a9\u00109\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u00190:\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0005*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0003H\u0087\n\u001a<\u00109\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050<0;\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0005*\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0017H\u0087\n\u00a2\u0006\u0002\b=\u001aY\u0010>\u001a\u000e\u0012\u0004\u0012\u0002H?\u0012\u0004\u0012\u0002H\u00050\u0003\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0005\"\u0004\b\u0002\u0010?*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u00032\u001e\u0010@\u001a\u001a\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0019\u0012\u0004\u0012\u0002H?0&H\u0087\b\u001at\u0010A\u001a\u0002H*\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0005\"\u0004\b\u0002\u0010?\"\u0018\b\u0003\u0010**\u0012\u0012\u0006\b\u0000\u0012\u0002H?\u0012\u0006\b\u0000\u0012\u0002H\u00050\u0017*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u00032\u0006\u0010+\u001a\u0002H*2\u001e\u0010@\u001a\u001a\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0019\u0012\u0004\u0012\u0002H?0&H\u0086\b\u00a2\u0006\u0002\u0010,\u001aY\u0010B\u001a\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H?0\u0003\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0005\"\u0004\b\u0002\u0010?*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u00032\u001e\u0010@\u001a\u001a\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0019\u0012\u0004\u0012\u0002H?0&H\u0087\b\u001at\u0010C\u001a\u0002H*\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0005\"\u0004\b\u0002\u0010?\"\u0018\b\u0003\u0010**\u0012\u0012\u0006\b\u0000\u0012\u0002H\u0004\u0012\u0006\b\u0000\u0012\u0002H?0\u0017*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u00032\u0006\u0010+\u001a\u0002H*2\u001e\u0010@\u001a\u001a\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0019\u0012\u0004\u0012\u0002H?0&H\u0086\b\u00a2\u0006\u0002\u0010,\u001a@\u0010D\u001a\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0003\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0005*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u00032\u0006\u0010\u001f\u001a\u0002H\u0004H\u0087\u0002\u00a2\u0006\u0002\u0010E\u001aH\u0010D\u001a\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0003\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0005*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u00032\u000e\u0010F\u001a\n\u0012\u0006\b\u0001\u0012\u0002H\u00040\nH\u0087\u0002\u00a2\u0006\u0002\u0010G\u001aA\u0010D\u001a\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0003\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0005*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u00032\f\u0010F\u001a\b\u0012\u0004\u0012\u0002H\u00040HH\u0087\u0002\u001aA\u0010D\u001a\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0003\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0005*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u00032\f\u0010F\u001a\b\u0012\u0004\u0012\u0002H\u00040IH\u0087\u0002\u001a2\u0010J\u001a\u00020K\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0005*\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u00172\u0006\u0010\u001f\u001a\u0002H\u0004H\u0087\n\u00a2\u0006\u0002\u0010L\u001a:\u0010J\u001a\u00020K\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0005*\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u00172\u000e\u0010F\u001a\n\u0012\u0006\b\u0001\u0012\u0002H\u00040\nH\u0087\n\u00a2\u0006\u0002\u0010M\u001a3\u0010J\u001a\u00020K\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0005*\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u00172\f\u0010F\u001a\b\u0012\u0004\u0012\u0002H\u00040HH\u0087\n\u001a3\u0010J\u001a\u00020K\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0005*\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u00172\f\u0010F\u001a\b\u0012\u0004\u0012\u0002H\u00040IH\u0087\n\u001a0\u0010N\u001a\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0003\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0005*\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0003H\u0000\u001a3\u0010O\u001a\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0003\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0005*\u0010\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u0005\u0018\u00010\u0003H\u0087\b\u001aT\u0010P\u001a\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0003\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0005*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u00032\u001a\u0010\t\u001a\u0016\u0012\u0012\b\u0001\u0012\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u000b0\nH\u0086\u0002\u00a2\u0006\u0002\u0010Q\u001aG\u0010P\u001a\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0003\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0005*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u00032\u0012\u0010\u0015\u001a\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u000bH\u0086\u0002\u001aM\u0010P\u001a\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0003\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0005*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u00032\u0018\u0010\t\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u000b0HH\u0086\u0002\u001aI\u0010P\u001a\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0003\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0005*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u00032\u0014\u0010R\u001a\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0003H\u0086\u0002\u001aM\u0010P\u001a\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0003\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0005*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u00032\u0018\u0010\t\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u000b0IH\u0086\u0002\u001aJ\u0010S\u001a\u00020K\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0005*\u0012\u0012\u0006\b\u0000\u0012\u0002H\u0004\u0012\u0006\b\u0000\u0012\u0002H\u00050\u00172\u001a\u0010\t\u001a\u0016\u0012\u0012\b\u0001\u0012\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u000b0\nH\u0087\n\u00a2\u0006\u0002\u0010T\u001a=\u0010S\u001a\u00020K\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0005*\u0012\u0012\u0006\b\u0000\u0012\u0002H\u0004\u0012\u0006\b\u0000\u0012\u0002H\u00050\u00172\u0012\u0010\u0015\u001a\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u000bH\u0087\n\u001aC\u0010S\u001a\u00020K\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0005*\u0012\u0012\u0006\b\u0000\u0012\u0002H\u0004\u0012\u0006\b\u0000\u0012\u0002H\u00050\u00172\u0018\u0010\t\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u000b0HH\u0087\n\u001a=\u0010S\u001a\u00020K\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0005*\u0012\u0012\u0006\b\u0000\u0012\u0002H\u0004\u0012\u0006\b\u0000\u0012\u0002H\u00050\u00172\u0012\u0010R\u001a\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0003H\u0087\n\u001aC\u0010S\u001a\u00020K\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0005*\u0012\u0012\u0006\b\u0000\u0012\u0002H\u0004\u0012\u0006\b\u0000\u0012\u0002H\u00050\u00172\u0018\u0010\t\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u000b0IH\u0087\n\u001aG\u0010U\u001a\u00020K\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0005*\u0012\u0012\u0006\b\u0000\u0012\u0002H\u0004\u0012\u0006\b\u0000\u0012\u0002H\u00050\u00172\u001a\u0010\t\u001a\u0016\u0012\u0012\b\u0001\u0012\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u000b0\n\u00a2\u0006\u0002\u0010T\u001a@\u0010U\u001a\u00020K\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0005*\u0012\u0012\u0006\b\u0000\u0012\u0002H\u0004\u0012\u0006\b\u0000\u0012\u0002H\u00050\u00172\u0018\u0010\t\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u000b0H\u001a@\u0010U\u001a\u00020K\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0005*\u0012\u0012\u0006\b\u0000\u0012\u0002H\u0004\u0012\u0006\b\u0000\u0012\u0002H\u00050\u00172\u0018\u0010\t\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u000b0I\u001a;\u0010V\u001a\u0004\u0018\u0001H\u0005\"\t\b\u0000\u0010\u0004\u00a2\u0006\u0002\b\u001e\"\u0004\b\u0001\u0010\u0005*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u00172\u0006\u0010\u001f\u001a\u0002H\u0004H\u0087\b\u00a2\u0006\u0002\u00100\u001a:\u0010W\u001a\u00020K\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0005*\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u00172\u0006\u0010\u001f\u001a\u0002H\u00042\u0006\u0010#\u001a\u0002H\u0005H\u0087\n\u00a2\u0006\u0002\u0010X\u001a;\u0010Y\u001a\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0003\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0005*\u0016\u0012\u0012\b\u0001\u0012\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u000b0\n\u00a2\u0006\u0002\u0010\u0014\u001aQ\u0010Y\u001a\u0002H*\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0005\"\u0018\b\u0002\u0010**\u0012\u0012\u0006\b\u0000\u0012\u0002H\u0004\u0012\u0006\b\u0000\u0012\u0002H\u00050\u0017*\u0016\u0012\u0012\b\u0001\u0012\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u000b0\n2\u0006\u0010+\u001a\u0002H*\u00a2\u0006\u0002\u0010Z\u001a4\u0010Y\u001a\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0003\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0005*\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u000b0H\u001aO\u0010Y\u001a\u0002H*\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0005\"\u0018\b\u0002\u0010**\u0012\u0012\u0006\b\u0000\u0012\u0002H\u0004\u0012\u0006\b\u0000\u0012\u0002H\u00050\u0017*\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u000b0H2\u0006\u0010+\u001a\u0002H*\u00a2\u0006\u0002\u0010[\u001a2\u0010Y\u001a\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0003\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0005*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0003H\u0007\u001aM\u0010Y\u001a\u0002H*\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0005\"\u0018\b\u0002\u0010**\u0012\u0012\u0006\b\u0000\u0012\u0002H\u0004\u0012\u0006\b\u0000\u0012\u0002H\u00050\u0017*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u00032\u0006\u0010+\u001a\u0002H*H\u0007\u00a2\u0006\u0002\u0010\\\u001a4\u0010Y\u001a\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0003\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0005*\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u000b0I\u001aO\u0010Y\u001a\u0002H*\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0005\"\u0018\b\u0002\u0010**\u0012\u0012\u0006\b\u0000\u0012\u0002H\u0004\u0012\u0006\b\u0000\u0012\u0002H\u00050\u0017*\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u000b0I2\u0006\u0010+\u001a\u0002H*\u00a2\u0006\u0002\u0010]\u001a2\u0010^\u001a\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0017\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0005*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0003H\u0007\u001a1\u0010_\u001a\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u000b\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0005*\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0019H\u0087\b\u001a2\u0010`\u001a\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0003\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0005*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0003H\u0001\u001a1\u0010a\u001a\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0003\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0005*\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0003H\u0081\b\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006b"}, d2 = {"INT_MAX_POWER_OF_TWO", "", "emptyMap", "", "K", "V", "hashMapOf", "Ljava/util/HashMap;", "Lkotlin/collections/HashMap;", "pairs", "", "Lkotlin/Pair;", "([Lkotlin/Pair;)Ljava/util/HashMap;", "linkedMapOf", "Ljava/util/LinkedHashMap;", "Lkotlin/collections/LinkedHashMap;", "([Lkotlin/Pair;)Ljava/util/LinkedHashMap;", "mapCapacity", "expectedSize", "mapOf", "([Lkotlin/Pair;)Ljava/util/Map;", "pair", "mutableMapOf", "", "component1", "", "(Ljava/util/Map$Entry;)Ljava/lang/Object;", "component2", "contains", "", "Lkotlin/internal/OnlyInputTypes;", "key", "(Ljava/util/Map;Ljava/lang/Object;)Z", "containsKey", "containsValue", "value", "filter", "predicate", "Lkotlin/Function1;", "filterKeys", "filterNot", "filterNotTo", "M", "destination", "(Ljava/util/Map;Ljava/util/Map;Lkotlin/jvm/functions/Function1;)Ljava/util/Map;", "filterTo", "filterValues", "get", "(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;", "getOrElse", "defaultValue", "Lkotlin/Function0;", "(Ljava/util/Map;Ljava/lang/Object;Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;", "getOrElseNullable", "getOrPut", "getValue", "isNotEmpty", "iterator", "", "", "", "mutableIterator", "mapKeys", "R", "transform", "mapKeysTo", "mapValues", "mapValuesTo", "minus", "(Ljava/util/Map;Ljava/lang/Object;)Ljava/util/Map;", "keys", "(Ljava/util/Map;[Ljava/lang/Object;)Ljava/util/Map;", "", "Lkotlin/sequences/Sequence;", "minusAssign", "", "(Ljava/util/Map;Ljava/lang/Object;)V", "(Ljava/util/Map;[Ljava/lang/Object;)V", "optimizeReadOnlyMap", "orEmpty", "plus", "(Ljava/util/Map;[Lkotlin/Pair;)Ljava/util/Map;", "map", "plusAssign", "(Ljava/util/Map;[Lkotlin/Pair;)V", "putAll", "remove", "set", "(Ljava/util/Map;Ljava/lang/Object;Ljava/lang/Object;)V", "toMap", "([Lkotlin/Pair;Ljava/util/Map;)Ljava/util/Map;", "(Ljava/lang/Iterable;Ljava/util/Map;)Ljava/util/Map;", "(Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;", "(Lkotlin/sequences/Sequence;Ljava/util/Map;)Ljava/util/Map;", "toMutableMap", "toPair", "toSingletonMap", "toSingletonMapOrSelf", "kotlin-stdlib"}, k = 5, mv = {1, 1, 6}, xi = 1, xs = "kotlin/collections/MapsKt")
/* compiled from: Maps.kt */
class MapsKt__MapsKt extends kotlin.collections.MapsKt__MapsJVMKt {
    private static final int INT_MAX_POWER_OF_TWO = 1073741824;

    @org.jetbrains.annotations.NotNull
    public static final <K, V> java.util.Map<K, V> emptyMap() {
        kotlin.collections.EmptyMap emptyMap = kotlin.collections.EmptyMap.INSTANCE;
        if (emptyMap != null) {
            return emptyMap;
        }
        throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.collections.Map<K, V>");
    }

    @org.jetbrains.annotations.NotNull
    public static final <K, V> java.util.Map<K, V> mapOf(@org.jetbrains.annotations.NotNull kotlin.Pair<? extends K, ? extends V>... pairs) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(pairs, "pairs");
        return ((java.lang.Object[]) pairs).length > 0 ? kotlin.collections.MapsKt.linkedMapOf((kotlin.Pair[]) java.util.Arrays.copyOf(pairs, pairs.length)) : kotlin.collections.MapsKt.emptyMap();
    }

    @kotlin.internal.InlineOnly
    private static final <K, V> java.util.Map<K, V> mapOf() {
        return kotlin.collections.MapsKt.emptyMap();
    }

    @org.jetbrains.annotations.NotNull
    public static final <K, V> java.util.Map<K, V> mapOf(@org.jetbrains.annotations.NotNull kotlin.Pair<? extends K, ? extends V> pair) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(pair, "pair");
        java.util.Map<K, V> singletonMap = java.util.Collections.singletonMap(pair.getFirst(), pair.getSecond());
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(singletonMap, "java.util.Collections.si\u2026(pair.first, pair.second)");
        return singletonMap;
    }

    @kotlin.SinceKotlin(version = "1.1")
    @kotlin.internal.InlineOnly
    private static final <K, V> java.util.Map<K, V> mutableMapOf() {
        return new java.util.LinkedHashMap<>();
    }

    @org.jetbrains.annotations.NotNull
    public static final <K, V> java.util.Map<K, V> mutableMapOf(@org.jetbrains.annotations.NotNull kotlin.Pair<? extends K, ? extends V>... pairs) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(pairs, "pairs");
        java.util.LinkedHashMap $receiver = new java.util.LinkedHashMap(kotlin.collections.MapsKt.mapCapacity(((java.lang.Object[]) pairs).length));
        kotlin.collections.MapsKt.putAll((java.util.Map<? super K, ? super V>) $receiver, pairs);
        return $receiver;
    }

    @kotlin.SinceKotlin(version = "1.1")
    @kotlin.internal.InlineOnly
    private static final <K, V> java.util.HashMap<K, V> hashMapOf() {
        return new java.util.HashMap<>();
    }

    @org.jetbrains.annotations.NotNull
    public static final <K, V> java.util.HashMap<K, V> hashMapOf(@org.jetbrains.annotations.NotNull kotlin.Pair<? extends K, ? extends V>... pairs) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(pairs, "pairs");
        java.util.HashMap<K, V> hashMap = new java.util.HashMap<>(kotlin.collections.MapsKt.mapCapacity(((java.lang.Object[]) pairs).length));
        kotlin.collections.MapsKt.putAll((java.util.Map<? super K, ? super V>) hashMap, pairs);
        return hashMap;
    }

    @kotlin.SinceKotlin(version = "1.1")
    @kotlin.internal.InlineOnly
    private static final <K, V> java.util.LinkedHashMap<K, V> linkedMapOf() {
        return new java.util.LinkedHashMap<>();
    }

    @org.jetbrains.annotations.NotNull
    public static final <K, V> java.util.LinkedHashMap<K, V> linkedMapOf(@org.jetbrains.annotations.NotNull kotlin.Pair<? extends K, ? extends V>... pairs) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(pairs, "pairs");
        java.util.LinkedHashMap<K, V> linkedHashMap = new java.util.LinkedHashMap<>(kotlin.collections.MapsKt.mapCapacity(((java.lang.Object[]) pairs).length));
        kotlin.collections.MapsKt.putAll((java.util.Map<? super K, ? super V>) linkedHashMap, pairs);
        return linkedHashMap;
    }

    @kotlin.PublishedApi
    public static final int mapCapacity(int expectedSize) {
        if (expectedSize < 3) {
            return expectedSize + 1;
        }
        if (expectedSize < INT_MAX_POWER_OF_TWO) {
            return (expectedSize / 3) + expectedSize;
        }
        return Integer.MAX_VALUE;
    }

    @kotlin.internal.InlineOnly
    private static final <K, V> boolean isNotEmpty(@org.jetbrains.annotations.NotNull java.util.Map<? extends K, ? extends V> $receiver) {
        return !$receiver.isEmpty();
    }

    @kotlin.internal.InlineOnly
    private static final <K, V> java.util.Map<K, V> orEmpty(@org.jetbrains.annotations.Nullable java.util.Map<K, ? extends V> $receiver) {
        return $receiver != null ? $receiver : kotlin.collections.MapsKt.emptyMap();
    }

    @kotlin.internal.InlineOnly
    private static final <K, V> boolean contains(@org.jetbrains.annotations.NotNull java.util.Map<? extends K, ? extends V> $receiver, K key) {
        if ($receiver != null) {
            return $receiver.containsKey(key);
        }
        throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.collections.Map<K, *>");
    }

    @kotlin.internal.InlineOnly
    private static final <K, V> V get(@org.jetbrains.annotations.NotNull java.util.Map<? extends K, ? extends V> $receiver, K key) {
        if ($receiver != null) {
            return $receiver.get(key);
        }
        throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.collections.Map<K, V>");
    }

    @kotlin.internal.InlineOnly
    private static final <K, V> void set(@org.jetbrains.annotations.NotNull java.util.Map<K, V> $receiver, K key, V value) {
        $receiver.put(key, value);
    }

    @kotlin.internal.InlineOnly
    private static final <K> boolean containsKey(@org.jetbrains.annotations.NotNull java.util.Map<? extends K, ?> $receiver, K key) {
        if ($receiver != null) {
            return $receiver.containsKey(key);
        }
        throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.collections.Map<K, *>");
    }

    @kotlin.internal.InlineOnly
    private static final <K, V> boolean containsValue(@org.jetbrains.annotations.NotNull java.util.Map<K, ? extends V> $receiver, V value) {
        return $receiver.containsValue(value);
    }

    @kotlin.internal.InlineOnly
    private static final <K, V> V remove(@org.jetbrains.annotations.NotNull java.util.Map<? extends K, V> $receiver, K key) {
        if ($receiver != null) {
            return kotlin.jvm.internal.TypeIntrinsics.asMutableMap($receiver).remove(key);
        }
        throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.collections.MutableMap<K, V>");
    }

    @kotlin.internal.InlineOnly
    private static final <K, V> K component1(@org.jetbrains.annotations.NotNull java.util.Map.Entry<? extends K, ? extends V> $receiver) {
        return $receiver.getKey();
    }

    @kotlin.internal.InlineOnly
    private static final <K, V> V component2(@org.jetbrains.annotations.NotNull java.util.Map.Entry<? extends K, ? extends V> $receiver) {
        return $receiver.getValue();
    }

    @kotlin.internal.InlineOnly
    private static final <K, V> kotlin.Pair<K, V> toPair(@org.jetbrains.annotations.NotNull java.util.Map.Entry<? extends K, ? extends V> $receiver) {
        return new kotlin.Pair<>($receiver.getKey(), $receiver.getValue());
    }

    @kotlin.internal.InlineOnly
    private static final <K, V> V getOrElse(@org.jetbrains.annotations.NotNull java.util.Map<K, ? extends V> $receiver, K key, kotlin.jvm.functions.Function0<? extends V> defaultValue) {
        V v = $receiver.get(key);
        return v != null ? v : defaultValue.invoke();
    }

    public static final <K, V> V getOrElseNullable(@org.jetbrains.annotations.NotNull java.util.Map<K, ? extends V> $receiver, K key, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function0<? extends V> defaultValue) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(defaultValue, "defaultValue");
        java.lang.Object value = $receiver.get(key);
        if (value != null || $receiver.containsKey(key)) {
            return value;
        }
        return defaultValue.invoke();
    }

    @kotlin.SinceKotlin(version = "1.1")
    public static final <K, V> V getValue(@org.jetbrains.annotations.NotNull java.util.Map<K, ? extends V> $receiver, K key) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        return kotlin.collections.MapsKt.getOrImplicitDefaultNullable($receiver, key);
    }

    public static final <K, V> V getOrPut(@org.jetbrains.annotations.NotNull java.util.Map<K, V> $receiver, K key, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function0<? extends V> defaultValue) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(defaultValue, "defaultValue");
        java.lang.Object value = $receiver.get(key);
        if (value != null) {
            return value;
        }
        java.lang.Object answer = defaultValue.invoke();
        $receiver.put(key, answer);
        return answer;
    }

    @kotlin.internal.InlineOnly
    private static final <K, V> java.util.Iterator<java.util.Map.Entry<K, V>> iterator(@org.jetbrains.annotations.NotNull java.util.Map<? extends K, ? extends V> $receiver) {
        return $receiver.entrySet().iterator();
    }

    @kotlin.internal.InlineOnly
    @kotlin.jvm.JvmName(name = "mutableIterator")
    private static final <K, V> java.util.Iterator<java.util.Map.Entry<K, V>> mutableIterator(@org.jetbrains.annotations.NotNull java.util.Map<K, V> $receiver) {
        return $receiver.entrySet().iterator();
    }

    @org.jetbrains.annotations.NotNull
    public static final <K, V, R, M extends java.util.Map<? super K, ? super R>> M mapValuesTo(@org.jetbrains.annotations.NotNull java.util.Map<? extends K, ? extends V> $receiver, @org.jetbrains.annotations.NotNull M destination, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.util.Map.Entry<? extends K, ? extends V>, ? extends R> transform) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(destination, "destination");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(transform, "transform");
        for (java.lang.Object element$iv : $receiver.entrySet()) {
            destination.put(((java.util.Map.Entry) element$iv).getKey(), transform.invoke(element$iv));
        }
        return destination;
    }

    @org.jetbrains.annotations.NotNull
    public static final <K, V, R, M extends java.util.Map<? super R, ? super V>> M mapKeysTo(@org.jetbrains.annotations.NotNull java.util.Map<? extends K, ? extends V> $receiver, @org.jetbrains.annotations.NotNull M destination, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.util.Map.Entry<? extends K, ? extends V>, ? extends R> transform) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(destination, "destination");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(transform, "transform");
        for (java.lang.Object element$iv : $receiver.entrySet()) {
            destination.put(transform.invoke(element$iv), ((java.util.Map.Entry) element$iv).getValue());
        }
        return destination;
    }

    public static final <K, V> void putAll(@org.jetbrains.annotations.NotNull java.util.Map<? super K, ? super V> $receiver, @org.jetbrains.annotations.NotNull kotlin.Pair<? extends K, ? extends V>[] pairs) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(pairs, "pairs");
        for (kotlin.Pair<? extends K, ? extends V> pair : pairs) {
            $receiver.put(pair.component1(), pair.component2());
        }
    }

    public static final <K, V> void putAll(@org.jetbrains.annotations.NotNull java.util.Map<? super K, ? super V> $receiver, @org.jetbrains.annotations.NotNull java.lang.Iterable<? extends kotlin.Pair<? extends K, ? extends V>> pairs) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(pairs, "pairs");
        for (kotlin.Pair pair : pairs) {
            $receiver.put(pair.component1(), pair.component2());
        }
    }

    public static final <K, V> void putAll(@org.jetbrains.annotations.NotNull java.util.Map<? super K, ? super V> $receiver, @org.jetbrains.annotations.NotNull kotlin.sequences.Sequence<? extends kotlin.Pair<? extends K, ? extends V>> pairs) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(pairs, "pairs");
        for (kotlin.Pair pair : pairs) {
            $receiver.put(pair.component1(), pair.component2());
        }
    }

    @org.jetbrains.annotations.NotNull
    public static final <K, V, R> java.util.Map<K, R> mapValues(@org.jetbrains.annotations.NotNull java.util.Map<? extends K, ? extends V> $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.util.Map.Entry<? extends K, ? extends V>, ? extends R> transform) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(transform, "transform");
        java.util.Map destination$iv = new java.util.LinkedHashMap(kotlin.collections.MapsKt.mapCapacity($receiver.size()));
        for (java.lang.Object element$iv$iv : $receiver.entrySet()) {
            destination$iv.put(((java.util.Map.Entry) element$iv$iv).getKey(), transform.invoke(element$iv$iv));
        }
        return destination$iv;
    }

    @org.jetbrains.annotations.NotNull
    public static final <K, V, R> java.util.Map<R, V> mapKeys(@org.jetbrains.annotations.NotNull java.util.Map<? extends K, ? extends V> $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.util.Map.Entry<? extends K, ? extends V>, ? extends R> transform) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(transform, "transform");
        java.util.Map destination$iv = new java.util.LinkedHashMap(kotlin.collections.MapsKt.mapCapacity($receiver.size()));
        for (java.lang.Object element$iv$iv : $receiver.entrySet()) {
            destination$iv.put(transform.invoke(element$iv$iv), ((java.util.Map.Entry) element$iv$iv).getValue());
        }
        return destination$iv;
    }

    @org.jetbrains.annotations.NotNull
    public static final <K, V> java.util.Map<K, V> filterKeys(@org.jetbrains.annotations.NotNull java.util.Map<? extends K, ? extends V> $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super K, java.lang.Boolean> predicate) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(predicate, "predicate");
        java.util.LinkedHashMap result = new java.util.LinkedHashMap();
        for (java.util.Map.Entry entry : $receiver.entrySet()) {
            if (((java.lang.Boolean) predicate.invoke(entry.getKey())).booleanValue()) {
                result.put(entry.getKey(), entry.getValue());
            }
        }
        return result;
    }

    @org.jetbrains.annotations.NotNull
    public static final <K, V> java.util.Map<K, V> filterValues(@org.jetbrains.annotations.NotNull java.util.Map<? extends K, ? extends V> $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super V, java.lang.Boolean> predicate) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(predicate, "predicate");
        java.util.LinkedHashMap result = new java.util.LinkedHashMap();
        for (java.util.Map.Entry entry : $receiver.entrySet()) {
            if (((java.lang.Boolean) predicate.invoke(entry.getValue())).booleanValue()) {
                result.put(entry.getKey(), entry.getValue());
            }
        }
        return result;
    }

    @org.jetbrains.annotations.NotNull
    public static final <K, V, M extends java.util.Map<? super K, ? super V>> M filterTo(@org.jetbrains.annotations.NotNull java.util.Map<? extends K, ? extends V> $receiver, @org.jetbrains.annotations.NotNull M destination, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.util.Map.Entry<? extends K, ? extends V>, java.lang.Boolean> predicate) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(destination, "destination");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(predicate, "predicate");
        for (java.util.Map.Entry element : $receiver.entrySet()) {
            if (((java.lang.Boolean) predicate.invoke(element)).booleanValue()) {
                destination.put(element.getKey(), element.getValue());
            }
        }
        return destination;
    }

    @org.jetbrains.annotations.NotNull
    public static final <K, V> java.util.Map<K, V> filter(@org.jetbrains.annotations.NotNull java.util.Map<? extends K, ? extends V> $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.util.Map.Entry<? extends K, ? extends V>, java.lang.Boolean> predicate) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(predicate, "predicate");
        java.util.Map destination$iv = new java.util.LinkedHashMap();
        for (java.util.Map.Entry element$iv : $receiver.entrySet()) {
            if (((java.lang.Boolean) predicate.invoke(element$iv)).booleanValue()) {
                destination$iv.put(element$iv.getKey(), element$iv.getValue());
            }
        }
        return destination$iv;
    }

    @org.jetbrains.annotations.NotNull
    public static final <K, V, M extends java.util.Map<? super K, ? super V>> M filterNotTo(@org.jetbrains.annotations.NotNull java.util.Map<? extends K, ? extends V> $receiver, @org.jetbrains.annotations.NotNull M destination, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.util.Map.Entry<? extends K, ? extends V>, java.lang.Boolean> predicate) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(destination, "destination");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(predicate, "predicate");
        for (java.util.Map.Entry element : $receiver.entrySet()) {
            if (!((java.lang.Boolean) predicate.invoke(element)).booleanValue()) {
                destination.put(element.getKey(), element.getValue());
            }
        }
        return destination;
    }

    @org.jetbrains.annotations.NotNull
    public static final <K, V> java.util.Map<K, V> filterNot(@org.jetbrains.annotations.NotNull java.util.Map<? extends K, ? extends V> $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super java.util.Map.Entry<? extends K, ? extends V>, java.lang.Boolean> predicate) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(predicate, "predicate");
        java.util.Map destination$iv = new java.util.LinkedHashMap();
        for (java.util.Map.Entry element$iv : $receiver.entrySet()) {
            if (!((java.lang.Boolean) predicate.invoke(element$iv)).booleanValue()) {
                destination$iv.put(element$iv.getKey(), element$iv.getValue());
            }
        }
        return destination$iv;
    }

    @org.jetbrains.annotations.NotNull
    public static final <K, V> java.util.Map<K, V> toMap(@org.jetbrains.annotations.NotNull java.lang.Iterable<? extends kotlin.Pair<? extends K, ? extends V>> $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        if (!($receiver instanceof java.util.Collection)) {
            return kotlin.collections.MapsKt.optimizeReadOnlyMap(kotlin.collections.MapsKt.toMap($receiver, (M) new java.util.LinkedHashMap()));
        }
        switch (((java.util.Collection) $receiver).size()) {
            case 0:
                return kotlin.collections.MapsKt.emptyMap();
            case 1:
                return kotlin.collections.MapsKt.mapOf($receiver instanceof java.util.List ? (kotlin.Pair) ((java.util.List) $receiver).get(0) : (kotlin.Pair) $receiver.iterator().next());
            default:
                return kotlin.collections.MapsKt.toMap($receiver, (M) new java.util.LinkedHashMap(kotlin.collections.MapsKt.mapCapacity(((java.util.Collection) $receiver).size())));
        }
    }

    @org.jetbrains.annotations.NotNull
    public static final <K, V, M extends java.util.Map<? super K, ? super V>> M toMap(@org.jetbrains.annotations.NotNull java.lang.Iterable<? extends kotlin.Pair<? extends K, ? extends V>> $receiver, @org.jetbrains.annotations.NotNull M destination) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(destination, "destination");
        kotlin.collections.MapsKt.putAll((java.util.Map<? super K, ? super V>) destination, $receiver);
        return destination;
    }

    @org.jetbrains.annotations.NotNull
    public static final <K, V> java.util.Map<K, V> toMap(@org.jetbrains.annotations.NotNull kotlin.Pair<? extends K, ? extends V>[] $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        switch (((java.lang.Object[]) $receiver).length) {
            case 0:
                return kotlin.collections.MapsKt.emptyMap();
            case 1:
                return kotlin.collections.MapsKt.mapOf($receiver[0]);
            default:
                return kotlin.collections.MapsKt.toMap($receiver, (M) new java.util.LinkedHashMap(kotlin.collections.MapsKt.mapCapacity(((java.lang.Object[]) $receiver).length)));
        }
    }

    @org.jetbrains.annotations.NotNull
    public static final <K, V, M extends java.util.Map<? super K, ? super V>> M toMap(@org.jetbrains.annotations.NotNull kotlin.Pair<? extends K, ? extends V>[] $receiver, @org.jetbrains.annotations.NotNull M destination) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(destination, "destination");
        kotlin.collections.MapsKt.putAll((java.util.Map<? super K, ? super V>) destination, $receiver);
        return destination;
    }

    @org.jetbrains.annotations.NotNull
    public static final <K, V> java.util.Map<K, V> toMap(@org.jetbrains.annotations.NotNull kotlin.sequences.Sequence<? extends kotlin.Pair<? extends K, ? extends V>> $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        return kotlin.collections.MapsKt.optimizeReadOnlyMap(kotlin.collections.MapsKt.toMap($receiver, (M) new java.util.LinkedHashMap()));
    }

    @org.jetbrains.annotations.NotNull
    public static final <K, V, M extends java.util.Map<? super K, ? super V>> M toMap(@org.jetbrains.annotations.NotNull kotlin.sequences.Sequence<? extends kotlin.Pair<? extends K, ? extends V>> $receiver, @org.jetbrains.annotations.NotNull M destination) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(destination, "destination");
        kotlin.collections.MapsKt.putAll((java.util.Map<? super K, ? super V>) destination, $receiver);
        return destination;
    }

    @org.jetbrains.annotations.NotNull
    @kotlin.SinceKotlin(version = "1.1")
    public static final <K, V> java.util.Map<K, V> toMap(@org.jetbrains.annotations.NotNull java.util.Map<? extends K, ? extends V> $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        switch ($receiver.size()) {
            case 0:
                return kotlin.collections.MapsKt.emptyMap();
            case 1:
                return kotlin.collections.MapsKt.toSingletonMap($receiver);
            default:
                return kotlin.collections.MapsKt.toMutableMap($receiver);
        }
    }

    @org.jetbrains.annotations.NotNull
    @kotlin.SinceKotlin(version = "1.1")
    public static final <K, V> java.util.Map<K, V> toMutableMap(@org.jetbrains.annotations.NotNull java.util.Map<? extends K, ? extends V> $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        return new java.util.LinkedHashMap<>($receiver);
    }

    @org.jetbrains.annotations.NotNull
    @kotlin.SinceKotlin(version = "1.1")
    public static final <K, V, M extends java.util.Map<? super K, ? super V>> M toMap(@org.jetbrains.annotations.NotNull java.util.Map<? extends K, ? extends V> $receiver, @org.jetbrains.annotations.NotNull M destination) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(destination, "destination");
        destination.putAll($receiver);
        return destination;
    }

    @org.jetbrains.annotations.NotNull
    public static final <K, V> java.util.Map<K, V> plus(@org.jetbrains.annotations.NotNull java.util.Map<? extends K, ? extends V> $receiver, @org.jetbrains.annotations.NotNull kotlin.Pair<? extends K, ? extends V> pair) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(pair, "pair");
        if ($receiver.isEmpty()) {
            return kotlin.collections.MapsKt.mapOf(pair);
        }
        java.util.LinkedHashMap $receiver2 = new java.util.LinkedHashMap($receiver);
        $receiver2.put(pair.getFirst(), pair.getSecond());
        return $receiver2;
    }

    @org.jetbrains.annotations.NotNull
    public static final <K, V> java.util.Map<K, V> plus(@org.jetbrains.annotations.NotNull java.util.Map<? extends K, ? extends V> $receiver, @org.jetbrains.annotations.NotNull java.lang.Iterable<? extends kotlin.Pair<? extends K, ? extends V>> pairs) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(pairs, "pairs");
        if ($receiver.isEmpty()) {
            return kotlin.collections.MapsKt.toMap(pairs);
        }
        java.util.LinkedHashMap $receiver2 = new java.util.LinkedHashMap($receiver);
        kotlin.collections.MapsKt.putAll((java.util.Map<? super K, ? super V>) $receiver2, pairs);
        return $receiver2;
    }

    @org.jetbrains.annotations.NotNull
    public static final <K, V> java.util.Map<K, V> plus(@org.jetbrains.annotations.NotNull java.util.Map<? extends K, ? extends V> $receiver, @org.jetbrains.annotations.NotNull kotlin.Pair<? extends K, ? extends V>[] pairs) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(pairs, "pairs");
        if ($receiver.isEmpty()) {
            return kotlin.collections.MapsKt.toMap(pairs);
        }
        java.util.LinkedHashMap $receiver2 = new java.util.LinkedHashMap($receiver);
        kotlin.collections.MapsKt.putAll((java.util.Map<? super K, ? super V>) $receiver2, pairs);
        return $receiver2;
    }

    @org.jetbrains.annotations.NotNull
    public static final <K, V> java.util.Map<K, V> plus(@org.jetbrains.annotations.NotNull java.util.Map<? extends K, ? extends V> $receiver, @org.jetbrains.annotations.NotNull kotlin.sequences.Sequence<? extends kotlin.Pair<? extends K, ? extends V>> pairs) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(pairs, "pairs");
        java.util.LinkedHashMap $receiver2 = new java.util.LinkedHashMap($receiver);
        kotlin.collections.MapsKt.putAll((java.util.Map<? super K, ? super V>) $receiver2, pairs);
        return kotlin.collections.MapsKt.optimizeReadOnlyMap($receiver2);
    }

    @org.jetbrains.annotations.NotNull
    public static final <K, V> java.util.Map<K, V> plus(@org.jetbrains.annotations.NotNull java.util.Map<? extends K, ? extends V> $receiver, @org.jetbrains.annotations.NotNull java.util.Map<? extends K, ? extends V> map) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(map, "map");
        java.util.LinkedHashMap $receiver2 = new java.util.LinkedHashMap($receiver);
        $receiver2.putAll(map);
        return $receiver2;
    }

    @kotlin.internal.InlineOnly
    private static final <K, V> void plusAssign(@org.jetbrains.annotations.NotNull java.util.Map<? super K, ? super V> $receiver, kotlin.Pair<? extends K, ? extends V> pair) {
        $receiver.put(pair.getFirst(), pair.getSecond());
    }

    @kotlin.internal.InlineOnly
    private static final <K, V> void plusAssign(@org.jetbrains.annotations.NotNull java.util.Map<? super K, ? super V> $receiver, java.lang.Iterable<? extends kotlin.Pair<? extends K, ? extends V>> pairs) {
        kotlin.collections.MapsKt.putAll($receiver, pairs);
    }

    @kotlin.internal.InlineOnly
    private static final <K, V> void plusAssign(@org.jetbrains.annotations.NotNull java.util.Map<? super K, ? super V> $receiver, kotlin.Pair<? extends K, ? extends V>[] pairs) {
        kotlin.collections.MapsKt.putAll($receiver, pairs);
    }

    @kotlin.internal.InlineOnly
    private static final <K, V> void plusAssign(@org.jetbrains.annotations.NotNull java.util.Map<? super K, ? super V> $receiver, kotlin.sequences.Sequence<? extends kotlin.Pair<? extends K, ? extends V>> pairs) {
        kotlin.collections.MapsKt.putAll($receiver, pairs);
    }

    @kotlin.internal.InlineOnly
    private static final <K, V> void plusAssign(@org.jetbrains.annotations.NotNull java.util.Map<? super K, ? super V> $receiver, java.util.Map<K, ? extends V> map) {
        $receiver.putAll(map);
    }

    @org.jetbrains.annotations.NotNull
    @kotlin.SinceKotlin(version = "1.1")
    public static final <K, V> java.util.Map<K, V> minus(@org.jetbrains.annotations.NotNull java.util.Map<? extends K, ? extends V> $receiver, K key) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        java.util.Map<? extends K, ? extends V> $receiver2 = kotlin.collections.MapsKt.toMutableMap($receiver);
        $receiver2.remove(key);
        return kotlin.collections.MapsKt.optimizeReadOnlyMap($receiver2);
    }

    @org.jetbrains.annotations.NotNull
    @kotlin.SinceKotlin(version = "1.1")
    public static final <K, V> java.util.Map<K, V> minus(@org.jetbrains.annotations.NotNull java.util.Map<? extends K, ? extends V> $receiver, @org.jetbrains.annotations.NotNull java.lang.Iterable<? extends K> keys) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(keys, "keys");
        java.util.Map<? extends K, ? extends V> $receiver2 = kotlin.collections.MapsKt.toMutableMap($receiver);
        kotlin.collections.CollectionsKt.removeAll((java.util.Collection<? super T>) $receiver2.keySet(), keys);
        return kotlin.collections.MapsKt.optimizeReadOnlyMap($receiver2);
    }

    @org.jetbrains.annotations.NotNull
    @kotlin.SinceKotlin(version = "1.1")
    public static final <K, V> java.util.Map<K, V> minus(@org.jetbrains.annotations.NotNull java.util.Map<? extends K, ? extends V> $receiver, @org.jetbrains.annotations.NotNull K[] keys) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(keys, "keys");
        java.util.Map<? extends K, ? extends V> $receiver2 = kotlin.collections.MapsKt.toMutableMap($receiver);
        kotlin.collections.CollectionsKt.removeAll((java.util.Collection<? super T>) $receiver2.keySet(), (T[]) keys);
        return kotlin.collections.MapsKt.optimizeReadOnlyMap($receiver2);
    }

    @org.jetbrains.annotations.NotNull
    @kotlin.SinceKotlin(version = "1.1")
    public static final <K, V> java.util.Map<K, V> minus(@org.jetbrains.annotations.NotNull java.util.Map<? extends K, ? extends V> $receiver, @org.jetbrains.annotations.NotNull kotlin.sequences.Sequence<? extends K> keys) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(keys, "keys");
        java.util.Map<? extends K, ? extends V> $receiver2 = kotlin.collections.MapsKt.toMutableMap($receiver);
        kotlin.collections.CollectionsKt.removeAll((java.util.Collection<? super T>) $receiver2.keySet(), keys);
        return kotlin.collections.MapsKt.optimizeReadOnlyMap($receiver2);
    }

    @kotlin.SinceKotlin(version = "1.1")
    @kotlin.internal.InlineOnly
    private static final <K, V> void minusAssign(@org.jetbrains.annotations.NotNull java.util.Map<K, V> $receiver, K key) {
        $receiver.remove(key);
    }

    @kotlin.SinceKotlin(version = "1.1")
    @kotlin.internal.InlineOnly
    private static final <K, V> void minusAssign(@org.jetbrains.annotations.NotNull java.util.Map<K, V> $receiver, java.lang.Iterable<? extends K> keys) {
        kotlin.collections.CollectionsKt.removeAll((java.util.Collection<? super T>) $receiver.keySet(), keys);
    }

    @kotlin.SinceKotlin(version = "1.1")
    @kotlin.internal.InlineOnly
    private static final <K, V> void minusAssign(@org.jetbrains.annotations.NotNull java.util.Map<K, V> $receiver, K[] keys) {
        kotlin.collections.CollectionsKt.removeAll((java.util.Collection<? super T>) $receiver.keySet(), (T[]) keys);
    }

    @kotlin.SinceKotlin(version = "1.1")
    @kotlin.internal.InlineOnly
    private static final <K, V> void minusAssign(@org.jetbrains.annotations.NotNull java.util.Map<K, V> $receiver, kotlin.sequences.Sequence<? extends K> keys) {
        kotlin.collections.CollectionsKt.removeAll((java.util.Collection<? super T>) $receiver.keySet(), keys);
    }

    @org.jetbrains.annotations.NotNull
    public static final <K, V> java.util.Map<K, V> optimizeReadOnlyMap(@org.jetbrains.annotations.NotNull java.util.Map<K, ? extends V> $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        switch ($receiver.size()) {
            case 0:
                return kotlin.collections.MapsKt.emptyMap();
            case 1:
                return kotlin.collections.MapsKt.toSingletonMap($receiver);
            default:
                return $receiver;
        }
    }

    @kotlin.internal.InlineOnly
    private static final <K, V> java.util.Map<K, V> toSingletonMapOrSelf(@org.jetbrains.annotations.NotNull java.util.Map<K, ? extends V> $receiver) {
        return kotlin.collections.MapsKt.toSingletonMap($receiver);
    }

    @org.jetbrains.annotations.NotNull
    public static final <K, V> java.util.Map<K, V> toSingletonMap(@org.jetbrains.annotations.NotNull java.util.Map<? extends K, ? extends V> $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        java.util.Map.Entry $receiver2 = (java.util.Map.Entry) $receiver.entrySet().iterator().next();
        java.util.Map<K, V> singletonMap = java.util.Collections.singletonMap($receiver2.getKey(), $receiver2.getValue());
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(singletonMap, "java.util.Collections.singletonMap(key, value)");
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(singletonMap, "with (entries.iterator()\u2026ingletonMap(key, value) }");
        return singletonMap;
    }
}
