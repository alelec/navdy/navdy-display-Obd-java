package kotlin.collections;

@kotlin.SinceKotlin(version = "1.1")
@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\"\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b'\u0018\u0000 \u000b*\u0006\b\u0000\u0010\u0001 \u00012\b\u0012\u0004\u0012\u0002H\u00010\u00022\b\u0012\u0004\u0012\u0002H\u00010\u0003:\u0001\u000bB\u0007\b\u0004\u00a2\u0006\u0002\u0010\u0004J\u0013\u0010\u0005\u001a\u00020\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\bH\u0096\u0002J\b\u0010\t\u001a\u00020\nH\u0016\u00a8\u0006\f"}, d2 = {"Lkotlin/collections/AbstractSet;", "E", "Lkotlin/collections/AbstractCollection;", "", "()V", "equals", "", "other", "", "hashCode", "", "Companion", "kotlin-stdlib"}, k = 1, mv = {1, 1, 6})
/* compiled from: AbstractSet.kt */
public abstract class AbstractSet<E> extends kotlin.collections.AbstractCollection<E> implements java.util.Set<E>, kotlin.jvm.internal.markers.KMappedMarker {
    public static final kotlin.collections.AbstractSet.Companion Companion = new kotlin.collections.AbstractSet.Companion(null);

    @kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\"\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\u0010\u001e\n\u0002\b\u0002\b\u0080\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J%\u0010\u0003\u001a\u00020\u00042\n\u0010\u0005\u001a\u0006\u0012\u0002\b\u00030\u00062\n\u0010\u0007\u001a\u0006\u0012\u0002\b\u00030\u0006H\u0000\u00a2\u0006\u0002\b\bJ\u0019\u0010\t\u001a\u00020\n2\n\u0010\u0005\u001a\u0006\u0012\u0002\b\u00030\u000bH\u0000\u00a2\u0006\u0002\b\f\u00a8\u0006\r"}, d2 = {"Lkotlin/collections/AbstractSet$Companion;", "", "()V", "setEquals", "", "c", "", "other", "setEquals$kotlin_stdlib", "unorderedHashCode", "", "", "unorderedHashCode$kotlin_stdlib", "kotlin-stdlib"}, k = 1, mv = {1, 1, 6})
    /* compiled from: AbstractSet.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(kotlin.jvm.internal.DefaultConstructorMarker $constructor_marker) {
            this();
        }

        public final int unorderedHashCode$kotlin_stdlib(@org.jetbrains.annotations.NotNull java.util.Collection<?> c) {
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(c, "c");
            int hashCode = 0;
            for (java.lang.Object element : c) {
                hashCode += element != null ? element.hashCode() : 0;
            }
            return hashCode;
        }

        public final boolean setEquals$kotlin_stdlib(@org.jetbrains.annotations.NotNull java.util.Set<?> c, @org.jetbrains.annotations.NotNull java.util.Set<?> other) {
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(c, "c");
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(other, "other");
            if (c.size() != other.size()) {
                return false;
            }
            return c.containsAll(other);
        }
    }

    public java.util.Iterator<E> iterator() {
        throw new java.lang.UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    protected AbstractSet() {
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public boolean equals(@org.jetbrains.annotations.Nullable java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof java.util.Set)) {
            return false;
        }
        return Companion.setEquals$kotlin_stdlib(this, (java.util.Set) other);
    }

    public int hashCode() {
        return Companion.unorderedHashCode$kotlin_stdlib(this);
    }
}
