package kotlin.collections;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010*\n\u0002\u0010\u0001\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0003\b\u00c0\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0003J\t\u0010\u0004\u001a\u00020\u0005H\u0096\u0002J\b\u0010\u0006\u001a\u00020\u0005H\u0016J\t\u0010\u0007\u001a\u00020\u0002H\u0096\u0002J\b\u0010\b\u001a\u00020\tH\u0016J\b\u0010\n\u001a\u00020\u0002H\u0016J\b\u0010\u000b\u001a\u00020\tH\u0016\u00a8\u0006\f"}, d2 = {"Lkotlin/collections/EmptyIterator;", "", "", "()V", "hasNext", "", "hasPrevious", "next", "nextIndex", "", "previous", "previousIndex", "kotlin-stdlib"}, k = 1, mv = {1, 1, 6})
/* compiled from: Collections.kt */
public final class EmptyIterator implements java.util.ListIterator, kotlin.jvm.internal.markers.KMappedMarker {
    public static final kotlin.collections.EmptyIterator INSTANCE = null;

    public /* synthetic */ void add(java.lang.Object obj) {
        throw new java.lang.UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public void add(java.lang.Void voidR) {
        throw new java.lang.UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public void remove() {
        throw new java.lang.UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public /* synthetic */ void set(java.lang.Object obj) {
        throw new java.lang.UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public void set(java.lang.Void voidR) {
        throw new java.lang.UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    static {
        new kotlin.collections.EmptyIterator();
    }

    private EmptyIterator() {
        INSTANCE = this;
    }

    public boolean hasNext() {
        return false;
    }

    public boolean hasPrevious() {
        return false;
    }

    public int nextIndex() {
        return 0;
    }

    public int previousIndex() {
        return -1;
    }

    @org.jetbrains.annotations.NotNull
    public java.lang.Void next() {
        throw new java.util.NoSuchElementException();
    }

    @org.jetbrains.annotations.NotNull
    public java.lang.Void previous() {
        throw new java.util.NoSuchElementException();
    }
}
