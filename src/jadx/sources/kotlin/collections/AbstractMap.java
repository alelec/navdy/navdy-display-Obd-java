package kotlin.collections;

@kotlin.SinceKotlin(version = "1.1")
@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\b\u0002\n\u0002\u0010\"\n\u0000\n\u0002\u0010\u001e\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010&\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0003\b'\u0018\u0000 **\u0004\b\u0000\u0010\u0001*\u0006\b\u0001\u0010\u0002 \u00012\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u00020\u0003:\u0001*B\u0007\b\u0004\u00a2\u0006\u0002\u0010\u0004J\u001f\u0010\u0013\u001a\u00020\u00142\u0010\u0010\u0015\u001a\f\u0012\u0002\b\u0003\u0012\u0002\b\u0003\u0018\u00010\u0016H\u0000\u00a2\u0006\u0002\b\u0017J\u0015\u0010\u0018\u001a\u00020\u00142\u0006\u0010\u0019\u001a\u00028\u0000H\u0016\u00a2\u0006\u0002\u0010\u001aJ\u001a\u0010\u001b\u001a\u00020\u00142\u000b\u0010\u001c\u001a\u00078\u0001\u00a2\u0006\u0002\b\u001dH\u0016\u00a2\u0006\u0002\u0010\u001aJ\u0013\u0010\u001e\u001a\u00020\u00142\b\u0010\u001f\u001a\u0004\u0018\u00010 H\u0096\u0002J\u0018\u0010!\u001a\u0004\u0018\u00018\u00012\u0006\u0010\u0019\u001a\u00028\u0000H\u0096\u0002\u00a2\u0006\u0002\u0010\"J\b\u0010#\u001a\u00020\rH\u0016J#\u0010$\u001a\u0010\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0018\u00010\u00162\u0006\u0010\u0019\u001a\u00028\u0000H\u0002\u00a2\u0006\u0002\u0010%J\b\u0010&\u001a\u00020\u0014H\u0016J\b\u0010'\u001a\u00020(H\u0016J\u0012\u0010'\u001a\u00020(2\b\u0010)\u001a\u0004\u0018\u00010 H\u0002J\u001c\u0010'\u001a\u00020(2\u0012\u0010\u0015\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0016H\bR\u001a\u0010\u0005\u001a\n\u0012\u0004\u0012\u00028\u0000\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0007\u001a\n\u0012\u0004\u0012\u00028\u0001\u0018\u00010\b8\b@\bX\u0089\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\t\u001a\b\u0012\u0004\u0012\u00028\u00000\u00068VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\b\n\u0010\u000bR\u0014\u0010\f\u001a\u00020\r8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\b\u000e\u0010\u000fR\u001a\u0010\u0010\u001a\b\u0012\u0004\u0012\u00028\u00010\b8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0011\u0010\u0012\u00a8\u0006+"}, d2 = {"Lkotlin/collections/AbstractMap;", "K", "V", "", "()V", "_keys", "", "_values", "", "keys", "getKeys", "()Ljava/util/Set;", "size", "", "getSize", "()I", "values", "getValues", "()Ljava/util/Collection;", "containsEntry", "", "entry", "", "containsEntry$kotlin_stdlib", "containsKey", "key", "(Ljava/lang/Object;)Z", "containsValue", "value", "Lkotlin/UnsafeVariance;", "equals", "other", "", "get", "(Ljava/lang/Object;)Ljava/lang/Object;", "hashCode", "implFindEntry", "(Ljava/lang/Object;)Ljava/util/Map$Entry;", "isEmpty", "toString", "", "o", "Companion", "kotlin-stdlib"}, k = 1, mv = {1, 1, 6})
/* compiled from: AbstractMap.kt */
public abstract class AbstractMap<K, V> implements java.util.Map<K, V>, kotlin.jvm.internal.markers.KMappedMarker {
    public static final kotlin.collections.AbstractMap.Companion Companion = new kotlin.collections.AbstractMap.Companion(null);
    private volatile java.util.Set<? extends K> _keys;
    private volatile java.util.Collection<? extends V> _values;

    @kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010&\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0080\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J'\u0010\u0003\u001a\u00020\u00042\u000e\u0010\u0005\u001a\n\u0012\u0002\b\u0003\u0012\u0002\b\u00030\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\u0001H\u0000\u00a2\u0006\u0002\b\bJ\u001d\u0010\t\u001a\u00020\n2\u000e\u0010\u0005\u001a\n\u0012\u0002\b\u0003\u0012\u0002\b\u00030\u0006H\u0000\u00a2\u0006\u0002\b\u000bJ\u001d\u0010\f\u001a\u00020\r2\u000e\u0010\u0005\u001a\n\u0012\u0002\b\u0003\u0012\u0002\b\u00030\u0006H\u0000\u00a2\u0006\u0002\b\u000e\u00a8\u0006\u000f"}, d2 = {"Lkotlin/collections/AbstractMap$Companion;", "", "()V", "entryEquals", "", "e", "", "other", "entryEquals$kotlin_stdlib", "entryHashCode", "", "entryHashCode$kotlin_stdlib", "entryToString", "", "entryToString$kotlin_stdlib", "kotlin-stdlib"}, k = 1, mv = {1, 1, 6})
    /* compiled from: AbstractMap.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(kotlin.jvm.internal.DefaultConstructorMarker $constructor_marker) {
            this();
        }

        public final int entryHashCode$kotlin_stdlib(@org.jetbrains.annotations.NotNull java.util.Map.Entry<?, ?> e) {
            int i = 0;
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(e, "e");
            java.util.Map.Entry $receiver = e;
            java.lang.Object key = $receiver.getKey();
            int i2 = key != null ? key.hashCode() : 0;
            java.lang.Object value = $receiver.getValue();
            if (value != null) {
                i = value.hashCode();
            }
            return i2 ^ i;
        }

        @org.jetbrains.annotations.NotNull
        public final java.lang.String entryToString$kotlin_stdlib(@org.jetbrains.annotations.NotNull java.util.Map.Entry<?, ?> e) {
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(e, "e");
            java.util.Map.Entry $receiver = e;
            return $receiver.getKey() + "=" + $receiver.getValue();
        }

        public final boolean entryEquals$kotlin_stdlib(@org.jetbrains.annotations.NotNull java.util.Map.Entry<?, ?> e, @org.jetbrains.annotations.Nullable java.lang.Object other) {
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(e, "e");
            if (!(other instanceof java.util.Map.Entry)) {
                return false;
            }
            return kotlin.jvm.internal.Intrinsics.areEqual(e.getKey(), ((java.util.Map.Entry) other).getKey()) && kotlin.jvm.internal.Intrinsics.areEqual(e.getValue(), ((java.util.Map.Entry) other).getValue());
        }
    }

    public void clear() {
        throw new java.lang.UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public abstract java.util.Set getEntries();

    public V put(K k, V v) {
        throw new java.lang.UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public void putAll(java.util.Map<? extends K, ? extends V> map) {
        throw new java.lang.UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public V remove(java.lang.Object obj) {
        throw new java.lang.UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    protected AbstractMap() {
    }

    public final /* bridge */ java.util.Set entrySet() {
        return getEntries();
    }

    public final /* bridge */ java.util.Set keySet() {
        return getKeys();
    }

    public final /* bridge */ int size() {
        return getSize();
    }

    public final /* bridge */ java.util.Collection values() {
        return getValues();
    }

    public boolean containsKey(java.lang.Object key) {
        return implFindEntry(key) != null;
    }

    public boolean containsValue(java.lang.Object value) {
        for (java.util.Map.Entry it : entrySet()) {
            if (kotlin.jvm.internal.Intrinsics.areEqual(it.getValue(), value)) {
                return true;
            }
        }
        return false;
    }

    /* Debug info: failed to restart local var, previous not found, register: 5 */
    public final boolean containsEntry$kotlin_stdlib(@org.jetbrains.annotations.Nullable java.util.Map.Entry<?, ?> entry) {
        if (!(entry instanceof java.util.Map.Entry)) {
            return false;
        }
        java.lang.Object key = entry.getKey();
        java.lang.Object value = entry.getValue();
        if (this == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.collections.Map<K, V>");
        }
        java.lang.Object ourValue = get(key);
        if (!kotlin.jvm.internal.Intrinsics.areEqual(value, ourValue)) {
            return false;
        }
        if (ourValue == null) {
            if (this == null) {
                throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.collections.Map<K, *>");
            } else if (!containsKey(key)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(@org.jetbrains.annotations.Nullable java.lang.Object other) {
        boolean z;
        if (other == this) {
            return true;
        }
        if (!(other instanceof java.util.Map) || size() != ((java.util.Map) other).size()) {
            return false;
        }
        java.util.Iterator it = ((java.util.Map) other).entrySet().iterator();
        while (true) {
            if (it.hasNext()) {
                if (!containsEntry$kotlin_stdlib((java.util.Map.Entry) it.next())) {
                    z = false;
                    break;
                }
            } else {
                z = true;
                break;
            }
        }
        return z;
    }

    @org.jetbrains.annotations.Nullable
    public V get(java.lang.Object key) {
        java.util.Map.Entry implFindEntry = implFindEntry(key);
        if (implFindEntry != null) {
            return implFindEntry.getValue();
        }
        return null;
    }

    public int hashCode() {
        return entrySet().hashCode();
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    public int getSize() {
        return entrySet().size();
    }

    @org.jetbrains.annotations.NotNull
    public java.util.Set<K> getKeys() {
        if (this._keys == null) {
            this._keys = new kotlin.collections.AbstractMap$keys$1(this);
        }
        java.util.Set<? extends K> set = this._keys;
        if (set == null) {
            kotlin.jvm.internal.Intrinsics.throwNpe();
        }
        return set;
    }

    @org.jetbrains.annotations.NotNull
    public java.lang.String toString() {
        return kotlin.collections.CollectionsKt.joinToString$default(entrySet(), ", ", "{", "}", 0, null, new kotlin.collections.AbstractMap$toString$1(this), 24, null);
    }

    /* access modifiers changed from: private */
    public final java.lang.String toString(java.util.Map.Entry<? extends K, ? extends V> entry) {
        return toString(entry.getKey()) + "=" + toString(entry.getValue());
    }

    private final java.lang.String toString(java.lang.Object o) {
        return o == this ? "(this Map)" : java.lang.String.valueOf(o);
    }

    @org.jetbrains.annotations.NotNull
    public java.util.Collection<V> getValues() {
        if (this._values == null) {
            this._values = new kotlin.collections.AbstractMap$values$1(this);
        }
        java.util.Collection<? extends V> collection = this._values;
        if (collection == null) {
            kotlin.jvm.internal.Intrinsics.throwNpe();
        }
        return collection;
    }

    private final java.util.Map.Entry<K, V> implFindEntry(K key) {
        java.lang.Object obj;
        java.util.Iterator it = entrySet().iterator();
        while (true) {
            if (!it.hasNext()) {
                obj = null;
                break;
            }
            java.lang.Object element$iv = it.next();
            if (kotlin.jvm.internal.Intrinsics.areEqual(((java.util.Map.Entry) element$iv).getKey(), (java.lang.Object) key)) {
                obj = element$iv;
                break;
            }
        }
        return (java.util.Map.Entry) obj;
    }
}
