package kotlin.collections;

@kotlin.SinceKotlin(version = "1.1")
@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010(\n\u0002\b\u0002\n\u0002\u0010*\n\u0002\b\b\b'\u0018\u0000 \u001d*\u0006\b\u0000\u0010\u0001 \u00012\b\u0012\u0004\u0012\u0002H\u00010\u00022\b\u0012\u0004\u0012\u0002H\u00010\u0003:\u0004\u001d\u001e\u001f B\u0007\b\u0004\u00a2\u0006\u0002\u0010\u0004J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fH\u0096\u0002J\u0016\u0010\r\u001a\u00028\u00002\u0006\u0010\u000e\u001a\u00020\u0006H\u00a6\u0002\u00a2\u0006\u0002\u0010\u000fJ\b\u0010\u0010\u001a\u00020\u0006H\u0016J\u001a\u0010\u0011\u001a\u00020\u00062\u000b\u0010\u0012\u001a\u00078\u0000\u00a2\u0006\u0002\b\u0013H\u0016\u00a2\u0006\u0002\u0010\u0014J\u000f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00028\u00000\u0016H\u0096\u0002J\u001a\u0010\u0017\u001a\u00020\u00062\u000b\u0010\u0012\u001a\u00078\u0000\u00a2\u0006\u0002\b\u0013H\u0016\u00a2\u0006\u0002\u0010\u0014J\u000e\u0010\u0018\u001a\b\u0012\u0004\u0012\u00028\u00000\u0019H\u0016J\u0016\u0010\u0018\u001a\b\u0012\u0004\u0012\u00028\u00000\u00192\u0006\u0010\u000e\u001a\u00020\u0006H\u0016J\u001e\u0010\u001a\u001a\b\u0012\u0004\u0012\u00028\u00000\u00032\u0006\u0010\u001b\u001a\u00020\u00062\u0006\u0010\u001c\u001a\u00020\u0006H\u0016R\u0012\u0010\u0005\u001a\u00020\u0006X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0007\u0010\b\u00a8\u0006!"}, d2 = {"Lkotlin/collections/AbstractList;", "E", "Lkotlin/collections/AbstractCollection;", "", "()V", "size", "", "getSize", "()I", "equals", "", "other", "", "get", "index", "(I)Ljava/lang/Object;", "hashCode", "indexOf", "element", "Lkotlin/UnsafeVariance;", "(Ljava/lang/Object;)I", "iterator", "", "lastIndexOf", "listIterator", "", "subList", "fromIndex", "toIndex", "Companion", "IteratorImpl", "ListIteratorImpl", "SubList", "kotlin-stdlib"}, k = 1, mv = {1, 1, 6})
/* compiled from: AbstractList.kt */
public abstract class AbstractList<E> extends kotlin.collections.AbstractCollection<E> implements java.util.List<E>, kotlin.jvm.internal.markers.KMappedMarker {
    public static final kotlin.collections.AbstractList.Companion Companion = new kotlin.collections.AbstractList.Companion(null);

    @kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u001e\n\u0002\b\u0005\b\u0080\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u001d\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0006H\u0000\u00a2\u0006\u0002\b\bJ\u001d\u0010\t\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0006H\u0000\u00a2\u0006\u0002\b\nJ%\u0010\u000b\u001a\u00020\u00042\u0006\u0010\f\u001a\u00020\u00062\u0006\u0010\r\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0006H\u0000\u00a2\u0006\u0002\b\u000eJ%\u0010\u000f\u001a\u00020\u00102\n\u0010\u0011\u001a\u0006\u0012\u0002\b\u00030\u00122\n\u0010\u0013\u001a\u0006\u0012\u0002\b\u00030\u0012H\u0000\u00a2\u0006\u0002\b\u0014J\u0019\u0010\u0015\u001a\u00020\u00062\n\u0010\u0011\u001a\u0006\u0012\u0002\b\u00030\u0012H\u0000\u00a2\u0006\u0002\b\u0016\u00a8\u0006\u0017"}, d2 = {"Lkotlin/collections/AbstractList$Companion;", "", "()V", "checkElementIndex", "", "index", "", "size", "checkElementIndex$kotlin_stdlib", "checkPositionIndex", "checkPositionIndex$kotlin_stdlib", "checkRangeIndexes", "fromIndex", "toIndex", "checkRangeIndexes$kotlin_stdlib", "orderedEquals", "", "c", "", "other", "orderedEquals$kotlin_stdlib", "orderedHashCode", "orderedHashCode$kotlin_stdlib", "kotlin-stdlib"}, k = 1, mv = {1, 1, 6})
    /* compiled from: AbstractList.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(kotlin.jvm.internal.DefaultConstructorMarker $constructor_marker) {
            this();
        }

        public final void checkElementIndex$kotlin_stdlib(int index, int size) {
            if (index < 0 || index >= size) {
                throw new java.lang.IndexOutOfBoundsException("index: " + index + ", size: " + size);
            }
        }

        public final void checkPositionIndex$kotlin_stdlib(int index, int size) {
            if (index < 0 || index > size) {
                throw new java.lang.IndexOutOfBoundsException("index: " + index + ", size: " + size);
            }
        }

        public final void checkRangeIndexes$kotlin_stdlib(int fromIndex, int toIndex, int size) {
            if (fromIndex < 0 || toIndex > size) {
                throw new java.lang.IndexOutOfBoundsException("fromIndex: " + fromIndex + ", toIndex: " + toIndex + ", size: " + size);
            } else if (fromIndex > toIndex) {
                throw new java.lang.IllegalArgumentException("fromIndex: " + fromIndex + " > toIndex: " + toIndex);
            }
        }

        public final int orderedHashCode$kotlin_stdlib(@org.jetbrains.annotations.NotNull java.util.Collection<?> c) {
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(c, "c");
            int hashCode = 1;
            for (java.lang.Object e : c) {
                hashCode = (hashCode * 31) + (e != null ? e.hashCode() : 0);
            }
            return hashCode;
        }

        public final boolean orderedEquals$kotlin_stdlib(@org.jetbrains.annotations.NotNull java.util.Collection<?> c, @org.jetbrains.annotations.NotNull java.util.Collection<?> other) {
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(c, "c");
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(other, "other");
            if (c.size() != other.size()) {
                return false;
            }
            java.util.Iterator otherIterator = other.iterator();
            for (java.lang.Object elem : c) {
                if (!kotlin.jvm.internal.Intrinsics.areEqual(elem, otherIterator.next())) {
                    return false;
                }
            }
            return true;
        }
    }

    @kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010(\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0003\b\u0092\u0004\u0018\u00002\b\u0012\u0004\u0012\u00028\u00000\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\t\u0010\t\u001a\u00020\nH\u0096\u0002J\u000e\u0010\u000b\u001a\u00028\u0000H\u0096\u0002\u00a2\u0006\u0002\u0010\fR\u001a\u0010\u0003\u001a\u00020\u0004X\u0084\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\b\u00a8\u0006\r"}, d2 = {"Lkotlin/collections/AbstractList$IteratorImpl;", "", "(Lkotlin/collections/AbstractList;)V", "index", "", "getIndex", "()I", "setIndex", "(I)V", "hasNext", "", "next", "()Ljava/lang/Object;", "kotlin-stdlib"}, k = 1, mv = {1, 1, 6})
    /* compiled from: AbstractList.kt */
    private class IteratorImpl implements java.util.Iterator<E>, kotlin.jvm.internal.markers.KMappedMarker {
        private int index;

        public void remove() {
            throw new java.lang.UnsupportedOperationException("Operation is not supported for read-only collection");
        }

        public IteratorImpl() {
        }

        /* access modifiers changed from: protected */
        public final int getIndex() {
            return this.index;
        }

        /* access modifiers changed from: protected */
        public final void setIndex(int i) {
            this.index = i;
        }

        public boolean hasNext() {
            return this.index < kotlin.collections.AbstractList.this.size();
        }

        public E next() {
            if (!hasNext()) {
                throw new java.util.NoSuchElementException();
            }
            kotlin.collections.AbstractList abstractList = kotlin.collections.AbstractList.this;
            int i = this.index;
            this.index = i + 1;
            return abstractList.get(i);
        }
    }

    @kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010*\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\b\u0092\u0004\u0018\u00002\f0\u0001R\b\u0012\u0004\u0012\u00028\u00000\u00022\b\u0012\u0004\u0012\u00028\u00000\u0003B\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\b\u0010\u0007\u001a\u00020\bH\u0016J\b\u0010\t\u001a\u00020\u0005H\u0016J\r\u0010\n\u001a\u00028\u0000H\u0016\u00a2\u0006\u0002\u0010\u000bJ\b\u0010\f\u001a\u00020\u0005H\u0016\u00a8\u0006\r"}, d2 = {"Lkotlin/collections/AbstractList$ListIteratorImpl;", "Lkotlin/collections/AbstractList$IteratorImpl;", "Lkotlin/collections/AbstractList;", "", "index", "", "(Lkotlin/collections/AbstractList;I)V", "hasPrevious", "", "nextIndex", "previous", "()Ljava/lang/Object;", "previousIndex", "kotlin-stdlib"}, k = 1, mv = {1, 1, 6})
    /* compiled from: AbstractList.kt */
    private class ListIteratorImpl extends kotlin.collections.AbstractList.IteratorImpl implements java.util.ListIterator<E>, kotlin.jvm.internal.markers.KMappedMarker {
        public void add(E e) {
            throw new java.lang.UnsupportedOperationException("Operation is not supported for read-only collection");
        }

        public void set(E e) {
            throw new java.lang.UnsupportedOperationException("Operation is not supported for read-only collection");
        }

        public ListIteratorImpl(int index) {
            super();
            kotlin.collections.AbstractList.Companion.checkPositionIndex$kotlin_stdlib(index, kotlin.collections.AbstractList.this.size());
            setIndex(index);
        }

        public boolean hasPrevious() {
            return getIndex() > 0;
        }

        public int nextIndex() {
            return getIndex();
        }

        public E previous() {
            if (!hasPrevious()) {
                throw new java.util.NoSuchElementException();
            }
            kotlin.collections.AbstractList abstractList = kotlin.collections.AbstractList.this;
            setIndex(getIndex() - 1);
            return abstractList.get(getIndex());
        }

        public int previousIndex() {
            return getIndex() - 1;
        }
    }

    @kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\n\b\u0010\u0018\u0000*\u0006\b\u0001\u0010\u0001 \u00012\b\u0012\u0004\u0012\u0002H\u00010\u0002B#\u0012\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00028\u00010\u0002\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0007J\u0016\u0010\f\u001a\u00028\u00012\u0006\u0010\r\u001a\u00020\u0005H\u0096\u0002\u00a2\u0006\u0002\u0010\u000eR\u000e\u0010\b\u001a\u00020\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00028\u00010\u0002X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u00020\u00058VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\b\n\u0010\u000b\u00a8\u0006\u000f"}, d2 = {"Lkotlin/collections/AbstractList$SubList;", "E", "Lkotlin/collections/AbstractList;", "list", "fromIndex", "", "toIndex", "(Lkotlin/collections/AbstractList;II)V", "_size", "size", "getSize", "()I", "get", "index", "(I)Ljava/lang/Object;", "kotlin-stdlib"}, k = 1, mv = {1, 1, 6})
    /* compiled from: AbstractList.kt */
    public static class SubList<E> extends kotlin.collections.AbstractList<E> {
        private int _size;
        private final int fromIndex;
        private final kotlin.collections.AbstractList<E> list;

        public SubList(@org.jetbrains.annotations.NotNull kotlin.collections.AbstractList<? extends E> list2, int fromIndex2, int toIndex) {
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(list2, "list");
            this.list = list2;
            this.fromIndex = fromIndex2;
            kotlin.collections.AbstractList.Companion.checkRangeIndexes$kotlin_stdlib(this.fromIndex, toIndex, this.list.size());
            this._size = toIndex - this.fromIndex;
        }

        public E get(int index) {
            kotlin.collections.AbstractList.Companion.checkElementIndex$kotlin_stdlib(index, this._size);
            return this.list.get(this.fromIndex + index);
        }

        public int getSize() {
            return this._size;
        }
    }

    public void add(int i, E e) {
        throw new java.lang.UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public boolean addAll(int i, java.util.Collection<? extends E> collection) {
        throw new java.lang.UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public abstract E get(int i);

    public abstract int getSize();

    public E remove(int i) {
        throw new java.lang.UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public E set(int i, E e) {
        throw new java.lang.UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    protected AbstractList() {
    }

    @org.jetbrains.annotations.NotNull
    public java.util.Iterator<E> iterator() {
        return new kotlin.collections.AbstractList.IteratorImpl<>();
    }

    public int indexOf(java.lang.Object element) {
        int index$iv = 0;
        for (java.lang.Object item$iv : this) {
            if (kotlin.jvm.internal.Intrinsics.areEqual(item$iv, element)) {
                return index$iv;
            }
            index$iv++;
        }
        return -1;
    }

    public int lastIndexOf(java.lang.Object element) {
        java.util.ListIterator iterator$iv = listIterator(size());
        while (iterator$iv.hasPrevious()) {
            if (kotlin.jvm.internal.Intrinsics.areEqual(iterator$iv.previous(), element)) {
                return iterator$iv.nextIndex();
            }
        }
        return -1;
    }

    @org.jetbrains.annotations.NotNull
    public java.util.ListIterator<E> listIterator() {
        return new kotlin.collections.AbstractList.ListIteratorImpl<>(0);
    }

    @org.jetbrains.annotations.NotNull
    public java.util.ListIterator<E> listIterator(int index) {
        return new kotlin.collections.AbstractList.ListIteratorImpl<>(index);
    }

    @org.jetbrains.annotations.NotNull
    public java.util.List<E> subList(int fromIndex, int toIndex) {
        return new kotlin.collections.AbstractList.SubList<>(this, fromIndex, toIndex);
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public boolean equals(@org.jetbrains.annotations.Nullable java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof java.util.List)) {
            return false;
        }
        return Companion.orderedEquals$kotlin_stdlib(this, (java.util.Collection) other);
    }

    public int hashCode() {
        return Companion.orderedHashCode$kotlin_stdlib(this);
    }
}
