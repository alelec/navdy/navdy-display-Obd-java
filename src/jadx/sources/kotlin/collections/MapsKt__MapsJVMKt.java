package kotlin.collections;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000B\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000f\n\u0000\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\u001aY\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0001\"\u000e\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u0004\"\u0004\b\u0001\u0010\u00032*\u0010\u0005\u001a\u0016\u0012\u0012\b\u0001\u0012\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u00070\u0006\"\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0007\u00a2\u0006\u0002\u0010\b\u001a@\u0010\t\u001a\u0002H\u0003\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0003*\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\n2\u0006\u0010\u000b\u001a\u0002H\u00022\f\u0010\f\u001a\b\u0012\u0004\u0012\u0002H\u00030\rH\u0086\b\u00a2\u0006\u0002\u0010\u000e\u001a\u0019\u0010\u000f\u001a\u00020\u0010*\u000e\u0012\u0004\u0012\u00020\u0012\u0012\u0004\u0012\u00020\u00120\u0011H\u0087\b\u001a:\u0010\u0013\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0001\"\u000e\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u0004\"\u0004\b\u0001\u0010\u0003*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0011\u001a@\u0010\u0013\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0001\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0003*\u0010\u0012\u0006\b\u0001\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u00112\u000e\u0010\u0014\u001a\n\u0012\u0006\b\u0000\u0012\u0002H\u00020\u0015\u00a8\u0006\u0016"}, d2 = {"sortedMapOf", "Ljava/util/SortedMap;", "K", "V", "", "pairs", "", "Lkotlin/Pair;", "([Lkotlin/Pair;)Ljava/util/SortedMap;", "getOrPut", "Ljava/util/concurrent/ConcurrentMap;", "key", "defaultValue", "Lkotlin/Function0;", "(Ljava/util/concurrent/ConcurrentMap;Ljava/lang/Object;Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;", "toProperties", "Ljava/util/Properties;", "", "", "toSortedMap", "comparator", "Ljava/util/Comparator;", "kotlin-stdlib"}, k = 5, mv = {1, 1, 6}, xi = 1, xs = "kotlin/collections/MapsKt")
/* compiled from: MapsJVM.kt */
class MapsKt__MapsJVMKt extends kotlin.collections.MapsKt__MapWithDefaultKt {
    public static final <K, V> V getOrPut(@org.jetbrains.annotations.NotNull java.util.concurrent.ConcurrentMap<K, V> $receiver, K key, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function0<? extends V> defaultValue) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(defaultValue, "defaultValue");
        V v = $receiver.get(key);
        if (v != null) {
            return v;
        }
        java.lang.Object invoke = defaultValue.invoke();
        java.lang.Object putIfAbsent = $receiver.putIfAbsent(key, invoke);
        return putIfAbsent != null ? putIfAbsent : invoke;
    }

    @org.jetbrains.annotations.NotNull
    public static final <K extends java.lang.Comparable<? super K>, V> java.util.SortedMap<K, V> toSortedMap(@org.jetbrains.annotations.NotNull java.util.Map<? extends K, ? extends V> $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        return new java.util.TreeMap<>($receiver);
    }

    @org.jetbrains.annotations.NotNull
    public static final <K, V> java.util.SortedMap<K, V> toSortedMap(@org.jetbrains.annotations.NotNull java.util.Map<? extends K, ? extends V> $receiver, @org.jetbrains.annotations.NotNull java.util.Comparator<? super K> comparator) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(comparator, "comparator");
        java.util.TreeMap $receiver2 = new java.util.TreeMap(comparator);
        $receiver2.putAll($receiver);
        return $receiver2;
    }

    @org.jetbrains.annotations.NotNull
    public static final <K extends java.lang.Comparable<? super K>, V> java.util.SortedMap<K, V> sortedMapOf(@org.jetbrains.annotations.NotNull kotlin.Pair<? extends K, ? extends V>... pairs) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(pairs, "pairs");
        java.util.TreeMap $receiver = new java.util.TreeMap();
        kotlin.collections.MapsKt.putAll((java.util.Map) $receiver, (kotlin.Pair[]) pairs);
        return $receiver;
    }

    @kotlin.internal.InlineOnly
    private static final java.util.Properties toProperties(@org.jetbrains.annotations.NotNull java.util.Map<java.lang.String, java.lang.String> $receiver) {
        java.util.Properties $receiver2 = new java.util.Properties();
        $receiver2.putAll($receiver);
        return $receiver2;
    }
}
