package kotlin.collections;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0013\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010(\n\u0000*\u0001\u0000\b\n\u0018\u00002\b\u0012\u0004\u0012\u00028\u00000\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u000f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00028\u00000\u0004H\u0096\u0002\u00a8\u0006\u0005"}, d2 = {"kotlin/sequences/SequencesKt__SequencesKt$Sequence$1", "Lkotlin/sequences/Sequence;", "(Lkotlin/jvm/functions/Function0;)V", "iterator", "", "kotlin-stdlib"}, k = 1, mv = {1, 1, 6})
/* compiled from: Sequences.kt */
public final class ArraysKt___ArraysKt$asSequence$$inlined$Sequence$3 implements kotlin.sequences.Sequence<java.lang.Short> {
    final /* synthetic */ short[] receiver$0$inlined;

    public ArraysKt___ArraysKt$asSequence$$inlined$Sequence$3(short[] sArr) {
        this.receiver$0$inlined = sArr;
    }

    public java.util.Iterator<java.lang.Short> iterator() {
        return kotlin.jvm.internal.ArrayIteratorsKt.iterator(this.receiver$0$inlined);
    }
}
