package kotlin.collections;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000L\n\u0000\n\u0002\u0010$\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010%\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\u0010&\n\u0002\b\u0005\u001a\u009b\u0001\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0001\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0002\"\u0004\b\u0002\u0010\u0003*\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00020\u00052b\u0010\u0006\u001a^\u0012\u0013\u0012\u0011H\u0002\u00a2\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\n\u0012\u0015\u0012\u0013\u0018\u0001H\u0003\u00a2\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\u000b\u0012\u0013\u0012\u0011H\u0004\u00a2\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\f\u0012\u0013\u0012\u00110\r\u00a2\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\u000e\u0012\u0004\u0012\u0002H\u00030\u0007H\u0087\b\u001a\u00b4\u0001\u0010\u000f\u001a\u0002H\u0010\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0002\"\u0004\b\u0002\u0010\u0003\"\u0016\b\u0003\u0010\u0010*\u0010\u0012\u0006\b\u0000\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0011*\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00020\u00052\u0006\u0010\u0012\u001a\u0002H\u00102b\u0010\u0006\u001a^\u0012\u0013\u0012\u0011H\u0002\u00a2\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\n\u0012\u0015\u0012\u0013\u0018\u0001H\u0003\u00a2\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\u000b\u0012\u0013\u0012\u0011H\u0004\u00a2\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\f\u0012\u0013\u0012\u00110\r\u00a2\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\u000e\u0012\u0004\u0012\u0002H\u00030\u0007H\u0087\b\u00a2\u0006\u0002\u0010\u0013\u001a0\u0010\u0014\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u00020\u00150\u0001\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0002*\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00020\u0005H\u0007\u001aI\u0010\u0016\u001a\u0002H\u0010\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0002\"\u0016\b\u0002\u0010\u0010*\u0010\u0012\u0006\b\u0000\u0012\u0002H\u0002\u0012\u0004\u0012\u00020\u00150\u0011*\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00020\u00052\u0006\u0010\u0012\u001a\u0002H\u0010H\u0007\u00a2\u0006\u0002\u0010\u0017\u001a\u00bc\u0001\u0010\u0018\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0001\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0002\"\u0004\b\u0002\u0010\u0003*\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00020\u000526\u0010\u0019\u001a2\u0012\u0013\u0012\u0011H\u0002\u00a2\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\n\u0012\u0013\u0012\u0011H\u0004\u00a2\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\f\u0012\u0004\u0012\u0002H\u00030\u001a2K\u0010\u0006\u001aG\u0012\u0013\u0012\u0011H\u0002\u00a2\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\n\u0012\u0013\u0012\u0011H\u0003\u00a2\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\u000b\u0012\u0013\u0012\u0011H\u0004\u00a2\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\f\u0012\u0004\u0012\u0002H\u00030\u001bH\u0087\b\u001a|\u0010\u0018\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0001\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0002\"\u0004\b\u0002\u0010\u0003*\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00020\u00052\u0006\u0010\u001c\u001a\u0002H\u000326\u0010\u0006\u001a2\u0012\u0013\u0012\u0011H\u0003\u00a2\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\u000b\u0012\u0013\u0012\u0011H\u0004\u00a2\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\f\u0012\u0004\u0012\u0002H\u00030\u001aH\u0087\b\u00a2\u0006\u0002\u0010\u001d\u001a\u00d5\u0001\u0010\u001e\u001a\u0002H\u0010\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0002\"\u0004\b\u0002\u0010\u0003\"\u0016\b\u0003\u0010\u0010*\u0010\u0012\u0006\b\u0000\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0011*\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00020\u00052\u0006\u0010\u0012\u001a\u0002H\u001026\u0010\u0019\u001a2\u0012\u0013\u0012\u0011H\u0002\u00a2\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\n\u0012\u0013\u0012\u0011H\u0004\u00a2\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\f\u0012\u0004\u0012\u0002H\u00030\u001a2K\u0010\u0006\u001aG\u0012\u0013\u0012\u0011H\u0002\u00a2\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\n\u0012\u0013\u0012\u0011H\u0003\u00a2\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\u000b\u0012\u0013\u0012\u0011H\u0004\u00a2\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\f\u0012\u0004\u0012\u0002H\u00030\u001bH\u0087\b\u00a2\u0006\u0002\u0010\u001f\u001a\u0090\u0001\u0010\u001e\u001a\u0002H\u0010\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0002\"\u0004\b\u0002\u0010\u0003\"\u0016\b\u0003\u0010\u0010*\u0010\u0012\u0006\b\u0000\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0011*\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00020\u00052\u0006\u0010\u0012\u001a\u0002H\u00102\u0006\u0010\u001c\u001a\u0002H\u000326\u0010\u0006\u001a2\u0012\u0013\u0012\u0011H\u0003\u00a2\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\u000b\u0012\u0013\u0012\u0011H\u0004\u00a2\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\f\u0012\u0004\u0012\u0002H\u00030\u001aH\u0087\b\u00a2\u0006\u0002\u0010 \u001aW\u0010!\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0011\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\"\"\u0004\b\u0002\u0010\u0003*\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\"0\u00112\u001e\u0010#\u001a\u001a\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\"0%\u0012\u0004\u0012\u0002H\u00030$H\u0081\b\u001a\u0088\u0001\u0010&\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H'0\u0001\"\u0004\b\u0000\u0010'\"\b\b\u0001\u0010\u0004*\u0002H'\"\u0004\b\u0002\u0010\u0002*\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00020\u00052K\u0010\u0006\u001aG\u0012\u0013\u0012\u0011H\u0002\u00a2\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\n\u0012\u0013\u0012\u0011H'\u00a2\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\u000b\u0012\u0013\u0012\u0011H\u0004\u00a2\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\f\u0012\u0004\u0012\u0002H'0\u001bH\u0087\b\u001a\u00a1\u0001\u0010(\u001a\u0002H\u0010\"\u0004\b\u0000\u0010'\"\b\b\u0001\u0010\u0004*\u0002H'\"\u0004\b\u0002\u0010\u0002\"\u0016\b\u0003\u0010\u0010*\u0010\u0012\u0006\b\u0000\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H'0\u0011*\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00020\u00052\u0006\u0010\u0012\u001a\u0002H\u00102K\u0010\u0006\u001aG\u0012\u0013\u0012\u0011H\u0002\u00a2\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\n\u0012\u0013\u0012\u0011H'\u00a2\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\u000b\u0012\u0013\u0012\u0011H\u0004\u00a2\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\f\u0012\u0004\u0012\u0002H'0\u001bH\u0087\b\u00a2\u0006\u0002\u0010)\u00a8\u0006*"}, d2 = {"aggregate", "", "K", "R", "T", "Lkotlin/collections/Grouping;", "operation", "Lkotlin/Function4;", "Lkotlin/ParameterName;", "name", "key", "accumulator", "element", "", "first", "aggregateTo", "M", "", "destination", "(Lkotlin/collections/Grouping;Ljava/util/Map;Lkotlin/jvm/functions/Function4;)Ljava/util/Map;", "eachCount", "", "eachCountTo", "(Lkotlin/collections/Grouping;Ljava/util/Map;)Ljava/util/Map;", "fold", "initialValueSelector", "Lkotlin/Function2;", "Lkotlin/Function3;", "initialValue", "(Lkotlin/collections/Grouping;Ljava/lang/Object;Lkotlin/jvm/functions/Function2;)Ljava/util/Map;", "foldTo", "(Lkotlin/collections/Grouping;Ljava/util/Map;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function3;)Ljava/util/Map;", "(Lkotlin/collections/Grouping;Ljava/util/Map;Ljava/lang/Object;Lkotlin/jvm/functions/Function2;)Ljava/util/Map;", "mapValuesInPlace", "V", "f", "Lkotlin/Function1;", "", "reduce", "S", "reduceTo", "(Lkotlin/collections/Grouping;Ljava/util/Map;Lkotlin/jvm/functions/Function3;)Ljava/util/Map;", "kotlin-stdlib"}, k = 2, mv = {1, 1, 6})
/* compiled from: Grouping.kt */
public final class GroupingKt {
    @org.jetbrains.annotations.NotNull
    @kotlin.SinceKotlin(version = "1.1")
    public static final <T, K, R> java.util.Map<K, R> aggregate(@org.jetbrains.annotations.NotNull kotlin.collections.Grouping<T, ? extends K> $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function4<? super K, ? super R, ? super T, ? super java.lang.Boolean, ? extends R> operation) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(operation, "operation");
        java.util.Map destination$iv = new java.util.LinkedHashMap();
        java.util.Iterator sourceIterator = $receiver.sourceIterator();
        while (sourceIterator.hasNext()) {
            java.lang.Object e$iv = sourceIterator.next();
            java.lang.Object key$iv = $receiver.keyOf(e$iv);
            java.lang.Object accumulator$iv = destination$iv.get(key$iv);
            destination$iv.put(key$iv, operation.invoke(key$iv, accumulator$iv, e$iv, java.lang.Boolean.valueOf(accumulator$iv == null && !destination$iv.containsKey(key$iv))));
        }
        return destination$iv;
    }

    @org.jetbrains.annotations.NotNull
    @kotlin.SinceKotlin(version = "1.1")
    public static final <T, K, R, M extends java.util.Map<? super K, R>> M aggregateTo(@org.jetbrains.annotations.NotNull kotlin.collections.Grouping<T, ? extends K> $receiver, @org.jetbrains.annotations.NotNull M destination, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function4<? super K, ? super R, ? super T, ? super java.lang.Boolean, ? extends R> operation) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(destination, "destination");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(operation, "operation");
        java.util.Iterator sourceIterator = $receiver.sourceIterator();
        while (sourceIterator.hasNext()) {
            java.lang.Object e = sourceIterator.next();
            java.lang.Object key = $receiver.keyOf(e);
            java.lang.Object accumulator = destination.get(key);
            destination.put(key, operation.invoke(key, accumulator, e, java.lang.Boolean.valueOf(accumulator == null && !destination.containsKey(key))));
        }
        return destination;
    }

    @org.jetbrains.annotations.NotNull
    @kotlin.SinceKotlin(version = "1.1")
    public static final <T, K, R> java.util.Map<K, R> fold(@org.jetbrains.annotations.NotNull kotlin.collections.Grouping<T, ? extends K> $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function2<? super K, ? super T, ? extends R> initialValueSelector, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function3<? super K, ? super R, ? super T, ? extends R> operation) {
        boolean z;
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(initialValueSelector, "initialValueSelector");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(operation, "operation");
        kotlin.collections.Grouping $receiver$iv = $receiver;
        java.util.Map destination$iv$iv = new java.util.LinkedHashMap();
        java.util.Iterator sourceIterator = $receiver$iv.sourceIterator();
        while (sourceIterator.hasNext()) {
            java.lang.Object e$iv$iv = sourceIterator.next();
            java.lang.Object key$iv$iv = $receiver$iv.keyOf(e$iv$iv);
            java.lang.Object accumulator$iv$iv = destination$iv$iv.get(key$iv$iv);
            if (accumulator$iv$iv != null || destination$iv$iv.containsKey(key$iv$iv)) {
                z = false;
            } else {
                z = true;
            }
            java.lang.Object key = key$iv$iv;
            if (z) {
                accumulator$iv$iv = initialValueSelector.invoke(key, e$iv$iv);
            }
            destination$iv$iv.put(key$iv$iv, operation.invoke(key, accumulator$iv$iv, e$iv$iv));
        }
        return destination$iv$iv;
    }

    @org.jetbrains.annotations.NotNull
    @kotlin.SinceKotlin(version = "1.1")
    public static final <T, K, R, M extends java.util.Map<? super K, R>> M foldTo(@org.jetbrains.annotations.NotNull kotlin.collections.Grouping<T, ? extends K> $receiver, @org.jetbrains.annotations.NotNull M destination, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function2<? super K, ? super T, ? extends R> initialValueSelector, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function3<? super K, ? super R, ? super T, ? extends R> operation) {
        boolean z;
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(destination, "destination");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(initialValueSelector, "initialValueSelector");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(operation, "operation");
        kotlin.collections.Grouping $receiver$iv = $receiver;
        java.util.Iterator sourceIterator = $receiver$iv.sourceIterator();
        while (sourceIterator.hasNext()) {
            java.lang.Object e$iv = sourceIterator.next();
            java.lang.Object key$iv = $receiver$iv.keyOf(e$iv);
            java.lang.Object accumulator$iv = destination.get(key$iv);
            if (accumulator$iv != null || destination.containsKey(key$iv)) {
                z = false;
            } else {
                z = true;
            }
            java.lang.Object key = key$iv;
            if (z) {
                accumulator$iv = initialValueSelector.invoke(key, e$iv);
            }
            destination.put(key$iv, operation.invoke(key, accumulator$iv, e$iv));
        }
        return destination;
    }

    @org.jetbrains.annotations.NotNull
    @kotlin.SinceKotlin(version = "1.1")
    public static final <T, K, R> java.util.Map<K, R> fold(@org.jetbrains.annotations.NotNull kotlin.collections.Grouping<T, ? extends K> $receiver, R initialValue, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function2<? super R, ? super T, ? extends R> operation) {
        boolean z;
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(operation, "operation");
        kotlin.collections.Grouping $receiver$iv = $receiver;
        java.util.Map destination$iv$iv = new java.util.LinkedHashMap();
        java.util.Iterator sourceIterator = $receiver$iv.sourceIterator();
        while (sourceIterator.hasNext()) {
            java.lang.Object e$iv$iv = sourceIterator.next();
            java.lang.Object key$iv$iv = $receiver$iv.keyOf(e$iv$iv);
            java.lang.Object accumulator$iv$iv = destination$iv$iv.get(key$iv$iv);
            if (accumulator$iv$iv != null || destination$iv$iv.containsKey(key$iv$iv)) {
                z = false;
            } else {
                z = true;
            }
            java.lang.Object obj = key$iv$iv;
            if (z) {
                accumulator$iv$iv = initialValue;
            }
            destination$iv$iv.put(key$iv$iv, operation.invoke(accumulator$iv$iv, e$iv$iv));
        }
        return destination$iv$iv;
    }

    @org.jetbrains.annotations.NotNull
    @kotlin.SinceKotlin(version = "1.1")
    public static final <T, K, R, M extends java.util.Map<? super K, R>> M foldTo(@org.jetbrains.annotations.NotNull kotlin.collections.Grouping<T, ? extends K> $receiver, @org.jetbrains.annotations.NotNull M destination, R initialValue, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function2<? super R, ? super T, ? extends R> operation) {
        boolean z;
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(destination, "destination");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(operation, "operation");
        kotlin.collections.Grouping $receiver$iv = $receiver;
        java.util.Iterator sourceIterator = $receiver$iv.sourceIterator();
        while (sourceIterator.hasNext()) {
            java.lang.Object e$iv = sourceIterator.next();
            java.lang.Object key$iv = $receiver$iv.keyOf(e$iv);
            java.lang.Object accumulator$iv = destination.get(key$iv);
            if (accumulator$iv != null || destination.containsKey(key$iv)) {
                z = false;
            } else {
                z = true;
            }
            java.lang.Object obj = key$iv;
            if (z) {
                accumulator$iv = initialValue;
            }
            destination.put(key$iv, operation.invoke(accumulator$iv, e$iv));
        }
        return destination;
    }

    @org.jetbrains.annotations.NotNull
    @kotlin.SinceKotlin(version = "1.1")
    public static final <S, T extends S, K> java.util.Map<K, S> reduce(@org.jetbrains.annotations.NotNull kotlin.collections.Grouping<T, ? extends K> $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function3<? super K, ? super S, ? super T, ? extends S> operation) {
        boolean z;
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(operation, "operation");
        kotlin.collections.Grouping $receiver$iv = $receiver;
        java.util.Map destination$iv$iv = new java.util.LinkedHashMap();
        java.util.Iterator sourceIterator = $receiver$iv.sourceIterator();
        while (sourceIterator.hasNext()) {
            java.lang.Object e$iv$iv = sourceIterator.next();
            java.lang.Object key$iv$iv = $receiver$iv.keyOf(e$iv$iv);
            java.lang.Object accumulator$iv$iv = destination$iv$iv.get(key$iv$iv);
            if (accumulator$iv$iv != null || destination$iv$iv.containsKey(key$iv$iv)) {
                z = false;
            } else {
                z = true;
            }
            java.lang.Object key = key$iv$iv;
            if (!z) {
                e$iv$iv = operation.invoke(key, accumulator$iv$iv, e$iv$iv);
            }
            destination$iv$iv.put(key$iv$iv, e$iv$iv);
        }
        return destination$iv$iv;
    }

    @org.jetbrains.annotations.NotNull
    @kotlin.SinceKotlin(version = "1.1")
    public static final <S, T extends S, K, M extends java.util.Map<? super K, S>> M reduceTo(@org.jetbrains.annotations.NotNull kotlin.collections.Grouping<T, ? extends K> $receiver, @org.jetbrains.annotations.NotNull M destination, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function3<? super K, ? super S, ? super T, ? extends S> operation) {
        boolean z;
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(destination, "destination");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(operation, "operation");
        kotlin.collections.Grouping $receiver$iv = $receiver;
        java.util.Iterator sourceIterator = $receiver$iv.sourceIterator();
        while (sourceIterator.hasNext()) {
            java.lang.Object e$iv = sourceIterator.next();
            java.lang.Object key$iv = $receiver$iv.keyOf(e$iv);
            java.lang.Object accumulator$iv = destination.get(key$iv);
            if (accumulator$iv != null || destination.containsKey(key$iv)) {
                z = false;
            } else {
                z = true;
            }
            java.lang.Object key = key$iv;
            if (!z) {
                e$iv = operation.invoke(key, accumulator$iv, e$iv);
            }
            destination.put(key$iv, e$iv);
        }
        return destination;
    }

    @org.jetbrains.annotations.NotNull
    @kotlin.SinceKotlin(version = "1.1")
    public static final <T, K> java.util.Map<K, java.lang.Integer> eachCount(@org.jetbrains.annotations.NotNull kotlin.collections.Grouping<T, ? extends K> $receiver) {
        java.lang.Object obj;
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        java.util.Map destination$iv = new java.util.LinkedHashMap();
        kotlin.collections.Grouping $receiver$iv$iv = $receiver;
        java.util.Iterator sourceIterator = $receiver$iv$iv.sourceIterator();
        while (sourceIterator.hasNext()) {
            java.lang.Object key$iv$iv = $receiver$iv$iv.keyOf(sourceIterator.next());
            java.lang.Object accumulator$iv$iv = destination$iv.get(key$iv$iv);
            java.lang.Object key$iv = key$iv$iv;
            if (accumulator$iv$iv == null && !destination$iv.containsKey(key$iv$iv)) {
                obj = new kotlin.jvm.internal.Ref.IntRef();
            } else {
                obj = accumulator$iv$iv;
            }
            java.lang.Object obj2 = key$iv;
            kotlin.jvm.internal.Ref.IntRef $receiver2 = (kotlin.jvm.internal.Ref.IntRef) obj;
            $receiver2.element++;
            destination$iv.put(key$iv$iv, $receiver2);
        }
        for (java.util.Map.Entry entry : destination$iv.entrySet()) {
            if (entry == null) {
                throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.collections.MutableMap.MutableEntry<K, R>");
            }
            kotlin.jvm.internal.TypeIntrinsics.asMutableMapEntry(entry).setValue(java.lang.Integer.valueOf(((kotlin.jvm.internal.Ref.IntRef) entry.getValue()).element));
        }
        return kotlin.jvm.internal.TypeIntrinsics.asMutableMap(destination$iv);
    }

    @org.jetbrains.annotations.NotNull
    @kotlin.SinceKotlin(version = "1.1")
    public static final <T, K, M extends java.util.Map<? super K, java.lang.Integer>> M eachCountTo(@org.jetbrains.annotations.NotNull kotlin.collections.Grouping<T, ? extends K> $receiver, @org.jetbrains.annotations.NotNull M destination) {
        boolean z;
        java.lang.Object obj;
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(destination, "destination");
        java.lang.Integer initialValue$iv = java.lang.Integer.valueOf(0);
        kotlin.collections.Grouping $receiver$iv$iv = $receiver;
        java.util.Iterator sourceIterator = $receiver$iv$iv.sourceIterator();
        while (sourceIterator.hasNext()) {
            java.lang.Object key$iv$iv = $receiver$iv$iv.keyOf(sourceIterator.next());
            java.lang.Object accumulator$iv$iv = destination.get(key$iv$iv);
            if (accumulator$iv$iv != null || destination.containsKey(key$iv$iv)) {
                z = false;
            } else {
                z = true;
            }
            java.lang.Object obj2 = key$iv$iv;
            if (z) {
                obj = initialValue$iv;
            } else {
                obj = accumulator$iv$iv;
            }
            destination.put(key$iv$iv, java.lang.Integer.valueOf(((java.lang.Number) obj).intValue() + 1));
        }
        return destination;
    }

    @kotlin.PublishedApi
    @kotlin.internal.InlineOnly
    private static final <K, V, R> java.util.Map<K, R> mapValuesInPlace(@org.jetbrains.annotations.NotNull java.util.Map<K, V> $receiver, kotlin.jvm.functions.Function1<? super java.util.Map.Entry<? extends K, ? extends V>, ? extends R> f) {
        for (java.util.Map.Entry it : $receiver.entrySet()) {
            if (it == null) {
                throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.collections.MutableMap.MutableEntry<K, R>");
            }
            kotlin.jvm.internal.TypeIntrinsics.asMutableMapEntry(it).setValue(f.invoke(it));
        }
        if ($receiver != null) {
            return kotlin.jvm.internal.TypeIntrinsics.asMutableMap($receiver);
        }
        throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.collections.MutableMap<K, R>");
    }
}
