package kotlin.collections;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\f\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u0000*\u0006\b\u0000\u0010\u0001 \u00012\u00020\u0002B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00028\u0000\u00a2\u0006\u0002\u0010\u0006J\t\u0010\f\u001a\u00020\u0004H\u00c6\u0003J\u000e\u0010\r\u001a\u00028\u0000H\u00c6\u0003\u00a2\u0006\u0002\u0010\nJ(\u0010\u000e\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\b\b\u0002\u0010\u0003\u001a\u00020\u00042\b\b\u0002\u0010\u0005\u001a\u00028\u0000H\u00c6\u0001\u00a2\u0006\u0002\u0010\u000fJ\u0013\u0010\u0010\u001a\u00020\u00112\b\u0010\u0012\u001a\u0004\u0018\u00010\u0002H\u00d6\u0003J\t\u0010\u0013\u001a\u00020\u0004H\u00d6\u0001J\t\u0010\u0014\u001a\u00020\u0015H\u00d6\u0001R\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0013\u0010\u0005\u001a\u00028\u0000\u00a2\u0006\n\n\u0002\u0010\u000b\u001a\u0004\b\t\u0010\n\u00a8\u0006\u0016"}, d2 = {"Lkotlin/collections/IndexedValue;", "T", "", "index", "", "value", "(ILjava/lang/Object;)V", "getIndex", "()I", "getValue", "()Ljava/lang/Object;", "Ljava/lang/Object;", "component1", "component2", "copy", "(ILjava/lang/Object;)Lkotlin/collections/IndexedValue;", "equals", "", "other", "hashCode", "toString", "", "kotlin-stdlib"}, k = 1, mv = {1, 1, 6})
/* compiled from: IndexedValue.kt */
public final class IndexedValue<T> {
    private final int index;
    private final T value;

    /* JADX WARNING: Incorrect type for immutable var: ssa=java.lang.Object, code=T, for r3v0, types: [java.lang.Object] */
    @org.jetbrains.annotations.NotNull
    public static /* bridge */ /* synthetic */ kotlin.collections.IndexedValue copy$default(kotlin.collections.IndexedValue indexedValue, int i, T t, int i2, java.lang.Object obj) {
        if ((i2 & 1) != 0) {
            i = indexedValue.index;
        }
        if ((i2 & 2) != 0) {
            t = indexedValue.value;
        }
        return indexedValue.copy(i, t);
    }

    public final int component1() {
        return this.index;
    }

    public final T component2() {
        return this.value;
    }

    @org.jetbrains.annotations.NotNull
    public final kotlin.collections.IndexedValue<T> copy(int index2, T value2) {
        return new kotlin.collections.IndexedValue<>(index2, value2);
    }

    public boolean equals(java.lang.Object obj) {
        if (this != obj) {
            if (!(obj instanceof kotlin.collections.IndexedValue)) {
                return false;
            }
            kotlin.collections.IndexedValue indexedValue = (kotlin.collections.IndexedValue) obj;
            if (!(this.index == indexedValue.index) || !kotlin.jvm.internal.Intrinsics.areEqual((java.lang.Object) this.value, (java.lang.Object) indexedValue.value)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        int i = this.index * 31;
        T t = this.value;
        return (t != null ? t.hashCode() : 0) + i;
    }

    public java.lang.String toString() {
        return "IndexedValue(index=" + this.index + ", value=" + this.value + ")";
    }

    public IndexedValue(int index2, T value2) {
        this.index = index2;
        this.value = value2;
    }

    public final int getIndex() {
        return this.index;
    }

    public final T getValue() {
        return this.value;
    }
}
