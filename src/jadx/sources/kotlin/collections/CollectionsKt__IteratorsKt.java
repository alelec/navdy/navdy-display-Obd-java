package kotlin.collections;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000 \n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010(\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a-\u0010\u0000\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u00032\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u00020\u00010\u0005H\u0086\b\u001a\u001f\u0010\u0006\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0003\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u0007H\u0087\u0002\u001a\u001f\u0010\u0006\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0003\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u0003H\u0087\n\u001a\"\u0010\b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00020\t0\u0003\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u0003\u00a8\u0006\n"}, d2 = {"forEach", "", "T", "", "operation", "Lkotlin/Function1;", "iterator", "Ljava/util/Enumeration;", "withIndex", "Lkotlin/collections/IndexedValue;", "kotlin-stdlib"}, k = 5, mv = {1, 1, 6}, xi = 1, xs = "kotlin/collections/CollectionsKt")
/* compiled from: Iterators.kt */
class CollectionsKt__IteratorsKt extends kotlin.collections.CollectionsKt__IterablesKt {
    @org.jetbrains.annotations.NotNull
    public static final <T> java.util.Iterator<T> iterator(@org.jetbrains.annotations.NotNull java.util.Enumeration<T> $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        return new kotlin.collections.CollectionsKt__IteratorsKt$iterator$1<>($receiver);
    }

    @kotlin.internal.InlineOnly
    private static final <T> java.util.Iterator<T> iterator(@org.jetbrains.annotations.NotNull java.util.Iterator<? extends T> $receiver) {
        return $receiver;
    }

    @org.jetbrains.annotations.NotNull
    public static final <T> java.util.Iterator<kotlin.collections.IndexedValue<T>> withIndex(@org.jetbrains.annotations.NotNull java.util.Iterator<? extends T> $receiver) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        return new kotlin.collections.IndexingIterator<>($receiver);
    }

    public static final <T> void forEach(@org.jetbrains.annotations.NotNull java.util.Iterator<? extends T> $receiver, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function1<? super T, kotlin.Unit> operation) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(operation, "operation");
        while ($receiver.hasNext()) {
            operation.invoke($receiver.next());
        }
    }
}
