package kotlin.collections;

class ArraysUtilJVM {
    ArraysUtilJVM() {
    }

    static <T> java.util.List<T> asList(T[] array) {
        return java.util.Arrays.asList(array);
    }
}
