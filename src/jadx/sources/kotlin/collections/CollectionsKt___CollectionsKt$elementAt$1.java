package kotlin.collections;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\u0001\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\u0010\u0000\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u00022\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\b\u0005"}, d2 = {"<anonymous>", "", "T", "it", "", "invoke"}, k = 3, mv = {1, 1, 6})
/* compiled from: _Collections.kt */
final class CollectionsKt___CollectionsKt$elementAt$1 extends kotlin.jvm.internal.Lambda implements kotlin.jvm.functions.Function1 {
    final /* synthetic */ int $index;

    CollectionsKt___CollectionsKt$elementAt$1(int i) {
        this.$index = i;
        super(1);
    }

    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        return invoke(((java.lang.Number) obj).intValue());
    }

    @org.jetbrains.annotations.NotNull
    public final java.lang.Void invoke(int it) {
        throw new java.lang.IndexOutOfBoundsException("Collection doesn't contain element at index " + this.$index + ".");
    }
}
