package kotlin.concurrent;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u00004\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\b\b\u001aJ\u0010\u0000\u001a\u00020\u00012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\t2\u0019\b\u0004\u0010\n\u001a\u0013\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\r0\u000b\u00a2\u0006\u0002\b\u000eH\u0087\b\u001aL\u0010\u0000\u001a\u00020\u00012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u000f\u001a\u00020\t2\u0006\u0010\b\u001a\u00020\t2\u0019\b\u0004\u0010\n\u001a\u0013\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\r0\u000b\u00a2\u0006\u0002\b\u000eH\u0087\b\u001a\u001a\u0010\u0010\u001a\u00020\u00012\b\u0010\u0002\u001a\u0004\u0018\u00010\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0001\u001aJ\u0010\u0010\u001a\u00020\u00012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\t2\u0019\b\u0004\u0010\n\u001a\u0013\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\r0\u000b\u00a2\u0006\u0002\b\u000eH\u0087\b\u001aL\u0010\u0010\u001a\u00020\u00012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u000f\u001a\u00020\t2\u0006\u0010\b\u001a\u00020\t2\u0019\b\u0004\u0010\n\u001a\u0013\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\r0\u000b\u00a2\u0006\u0002\b\u000eH\u0087\b\u001a$\u0010\u0011\u001a\u00020\f2\u0019\b\u0004\u0010\n\u001a\u0013\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\r0\u000b\u00a2\u0006\u0002\b\u000eH\u0087\b\u001a0\u0010\u0012\u001a\u00020\f*\u00020\u00012\u0006\u0010\u0013\u001a\u00020\u00072\u0019\b\u0004\u0010\n\u001a\u0013\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\r0\u000b\u00a2\u0006\u0002\b\u000eH\u0087\b\u001a8\u0010\u0012\u001a\u00020\f*\u00020\u00012\u0006\u0010\u0013\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\t2\u0019\b\u0004\u0010\n\u001a\u0013\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\r0\u000b\u00a2\u0006\u0002\b\u000eH\u0087\b\u001a0\u0010\u0012\u001a\u00020\f*\u00020\u00012\u0006\u0010\u0014\u001a\u00020\t2\u0019\b\u0004\u0010\n\u001a\u0013\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\r0\u000b\u00a2\u0006\u0002\b\u000eH\u0087\b\u001a8\u0010\u0012\u001a\u00020\f*\u00020\u00012\u0006\u0010\u0014\u001a\u00020\t2\u0006\u0010\b\u001a\u00020\t2\u0019\b\u0004\u0010\n\u001a\u0013\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\r0\u000b\u00a2\u0006\u0002\b\u000eH\u0087\b\u001a8\u0010\u0015\u001a\u00020\f*\u00020\u00012\u0006\u0010\u0013\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\t2\u0019\b\u0004\u0010\n\u001a\u0013\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\r0\u000b\u00a2\u0006\u0002\b\u000eH\u0087\b\u001a8\u0010\u0015\u001a\u00020\f*\u00020\u00012\u0006\u0010\u0014\u001a\u00020\t2\u0006\u0010\b\u001a\u00020\t2\u0019\b\u0004\u0010\n\u001a\u0013\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\r0\u000b\u00a2\u0006\u0002\b\u000eH\u0087\b\u00a8\u0006\u0016"}, d2 = {"fixedRateTimer", "Ljava/util/Timer;", "name", "", "daemon", "", "startAt", "Ljava/util/Date;", "period", "", "action", "Lkotlin/Function1;", "Ljava/util/TimerTask;", "", "Lkotlin/ExtensionFunctionType;", "initialDelay", "timer", "timerTask", "schedule", "time", "delay", "scheduleAtFixedRate", "kotlin-stdlib"}, k = 2, mv = {1, 1, 6})
@kotlin.jvm.JvmName(name = "TimersKt")
/* compiled from: Timer.kt */
public final class TimersKt {
    @kotlin.internal.InlineOnly
    private static final java.util.TimerTask schedule(@org.jetbrains.annotations.NotNull java.util.Timer $receiver, long delay, kotlin.jvm.functions.Function1<? super java.util.TimerTask, kotlin.Unit> action) {
        java.util.TimerTask task = new kotlin.concurrent.TimersKt$timerTask$1(action);
        $receiver.schedule(task, delay);
        return task;
    }

    @kotlin.internal.InlineOnly
    private static final java.util.TimerTask schedule(@org.jetbrains.annotations.NotNull java.util.Timer $receiver, java.util.Date time, kotlin.jvm.functions.Function1<? super java.util.TimerTask, kotlin.Unit> action) {
        java.util.TimerTask task = new kotlin.concurrent.TimersKt$timerTask$1(action);
        $receiver.schedule(task, time);
        return task;
    }

    @kotlin.internal.InlineOnly
    private static final java.util.TimerTask schedule(@org.jetbrains.annotations.NotNull java.util.Timer $receiver, long delay, long period, kotlin.jvm.functions.Function1<? super java.util.TimerTask, kotlin.Unit> action) {
        java.util.TimerTask task = new kotlin.concurrent.TimersKt$timerTask$1(action);
        $receiver.schedule(task, delay, period);
        return task;
    }

    @kotlin.internal.InlineOnly
    private static final java.util.TimerTask schedule(@org.jetbrains.annotations.NotNull java.util.Timer $receiver, java.util.Date time, long period, kotlin.jvm.functions.Function1<? super java.util.TimerTask, kotlin.Unit> action) {
        java.util.TimerTask task = new kotlin.concurrent.TimersKt$timerTask$1(action);
        $receiver.schedule(task, time, period);
        return task;
    }

    @kotlin.internal.InlineOnly
    private static final java.util.TimerTask scheduleAtFixedRate(@org.jetbrains.annotations.NotNull java.util.Timer $receiver, long delay, long period, kotlin.jvm.functions.Function1<? super java.util.TimerTask, kotlin.Unit> action) {
        java.util.TimerTask task = new kotlin.concurrent.TimersKt$timerTask$1(action);
        $receiver.scheduleAtFixedRate(task, delay, period);
        return task;
    }

    @kotlin.internal.InlineOnly
    private static final java.util.TimerTask scheduleAtFixedRate(@org.jetbrains.annotations.NotNull java.util.Timer $receiver, java.util.Date time, long period, kotlin.jvm.functions.Function1<? super java.util.TimerTask, kotlin.Unit> action) {
        java.util.TimerTask task = new kotlin.concurrent.TimersKt$timerTask$1(action);
        $receiver.scheduleAtFixedRate(task, time, period);
        return task;
    }

    @org.jetbrains.annotations.NotNull
    @kotlin.PublishedApi
    public static final java.util.Timer timer(@org.jetbrains.annotations.Nullable java.lang.String name, boolean daemon) {
        return name == null ? new java.util.Timer(daemon) : new java.util.Timer(name, daemon);
    }

    @kotlin.internal.InlineOnly
    static /* bridge */ /* synthetic */ java.util.Timer timer$default(java.lang.String name, boolean daemon, long initialDelay, long period, kotlin.jvm.functions.Function1 action, int i, java.lang.Object obj) {
        if ((i & 1) != 0) {
            name = null;
        }
        if ((i & 2) != 0) {
            daemon = false;
        }
        if ((i & 4) != 0) {
            initialDelay = (long) 0;
        }
        java.util.Timer timer = timer(name, daemon);
        timer.schedule(new kotlin.concurrent.TimersKt$timerTask$1(action), initialDelay, period);
        return timer;
    }

    @kotlin.internal.InlineOnly
    private static final java.util.Timer timer(java.lang.String name, boolean daemon, long initialDelay, long period, kotlin.jvm.functions.Function1<? super java.util.TimerTask, kotlin.Unit> action) {
        java.util.Timer timer = timer(name, daemon);
        timer.schedule(new kotlin.concurrent.TimersKt$timerTask$1(action), initialDelay, period);
        return timer;
    }

    @kotlin.internal.InlineOnly
    static /* bridge */ /* synthetic */ java.util.Timer timer$default(java.lang.String name, boolean daemon, java.util.Date startAt, long period, kotlin.jvm.functions.Function1 action, int i, java.lang.Object obj) {
        if ((i & 1) != 0) {
            name = null;
        }
        if ((i & 2) != 0) {
            daemon = false;
        }
        java.util.Timer timer = timer(name, daemon);
        timer.schedule(new kotlin.concurrent.TimersKt$timerTask$1(action), startAt, period);
        return timer;
    }

    @kotlin.internal.InlineOnly
    private static final java.util.Timer timer(java.lang.String name, boolean daemon, java.util.Date startAt, long period, kotlin.jvm.functions.Function1<? super java.util.TimerTask, kotlin.Unit> action) {
        java.util.Timer timer = timer(name, daemon);
        timer.schedule(new kotlin.concurrent.TimersKt$timerTask$1(action), startAt, period);
        return timer;
    }

    @kotlin.internal.InlineOnly
    static /* bridge */ /* synthetic */ java.util.Timer fixedRateTimer$default(java.lang.String name, boolean daemon, long initialDelay, long period, kotlin.jvm.functions.Function1 action, int i, java.lang.Object obj) {
        if ((i & 1) != 0) {
            name = null;
        }
        if ((i & 2) != 0) {
            daemon = false;
        }
        if ((i & 4) != 0) {
            initialDelay = (long) 0;
        }
        java.util.Timer timer = timer(name, daemon);
        timer.scheduleAtFixedRate(new kotlin.concurrent.TimersKt$timerTask$1(action), initialDelay, period);
        return timer;
    }

    @kotlin.internal.InlineOnly
    private static final java.util.Timer fixedRateTimer(java.lang.String name, boolean daemon, long initialDelay, long period, kotlin.jvm.functions.Function1<? super java.util.TimerTask, kotlin.Unit> action) {
        java.util.Timer timer = timer(name, daemon);
        timer.scheduleAtFixedRate(new kotlin.concurrent.TimersKt$timerTask$1(action), initialDelay, period);
        return timer;
    }

    @kotlin.internal.InlineOnly
    static /* bridge */ /* synthetic */ java.util.Timer fixedRateTimer$default(java.lang.String name, boolean daemon, java.util.Date startAt, long period, kotlin.jvm.functions.Function1 action, int i, java.lang.Object obj) {
        if ((i & 1) != 0) {
            name = null;
        }
        if ((i & 2) != 0) {
            daemon = false;
        }
        java.util.Timer timer = timer(name, daemon);
        timer.scheduleAtFixedRate(new kotlin.concurrent.TimersKt$timerTask$1(action), startAt, period);
        return timer;
    }

    @kotlin.internal.InlineOnly
    private static final java.util.Timer fixedRateTimer(java.lang.String name, boolean daemon, java.util.Date startAt, long period, kotlin.jvm.functions.Function1<? super java.util.TimerTask, kotlin.Unit> action) {
        java.util.Timer timer = timer(name, daemon);
        timer.scheduleAtFixedRate(new kotlin.concurrent.TimersKt$timerTask$1(action), startAt, period);
        return timer;
    }

    @kotlin.internal.InlineOnly
    private static final java.util.TimerTask timerTask(kotlin.jvm.functions.Function1<? super java.util.TimerTask, kotlin.Unit> action) {
        return new kotlin.concurrent.TimersKt$timerTask$1(action);
    }
}
