package kotlin.concurrent;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000:\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u001aJ\u0010\u0000\u001a\u00020\u00012\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\b2\b\b\u0002\u0010\t\u001a\u00020\n2\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\r0\f\u001a0\u0010\u000e\u001a\u0002H\u000f\"\b\b\u0000\u0010\u000f*\u00020\u0010*\b\u0012\u0004\u0012\u0002H\u000f0\u00112\f\u0010\u0012\u001a\b\u0012\u0004\u0012\u0002H\u000f0\fH\u0087\b\u00a2\u0006\u0002\u0010\u0013\u00a8\u0006\u0014"}, d2 = {"thread", "Ljava/lang/Thread;", "start", "", "isDaemon", "contextClassLoader", "Ljava/lang/ClassLoader;", "name", "", "priority", "", "block", "Lkotlin/Function0;", "", "getOrSet", "T", "", "Ljava/lang/ThreadLocal;", "default", "(Ljava/lang/ThreadLocal;Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;", "kotlin-stdlib"}, k = 2, mv = {1, 1, 6})
@kotlin.jvm.JvmName(name = "ThreadsKt")
/* compiled from: Thread.kt */
public final class ThreadsKt {
    @org.jetbrains.annotations.NotNull
    public static /* bridge */ /* synthetic */ java.lang.Thread thread$default(boolean z, boolean z2, java.lang.ClassLoader classLoader, java.lang.String str, int i, kotlin.jvm.functions.Function0 function0, int i2, java.lang.Object obj) {
        return thread((i2 & 1) != 0 ? true : z, (i2 & 2) != 0 ? false : z2, (i2 & 4) != 0 ? null : classLoader, (i2 & 8) != 0 ? null : str, (i2 & 16) != 0 ? -1 : i, function0);
    }

    @org.jetbrains.annotations.NotNull
    public static final java.lang.Thread thread(boolean start, boolean isDaemon, @org.jetbrains.annotations.Nullable java.lang.ClassLoader contextClassLoader, @org.jetbrains.annotations.Nullable java.lang.String name, int priority, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function0<kotlin.Unit> block) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(block, "block");
        kotlin.concurrent.ThreadsKt$thread$thread$1 thread = new kotlin.concurrent.ThreadsKt$thread$thread$1(block);
        if (isDaemon) {
            thread.setDaemon(true);
        }
        if (priority > 0) {
            thread.setPriority(priority);
        }
        if (name != null) {
            thread.setName(name);
        }
        if (contextClassLoader != null) {
            thread.setContextClassLoader(contextClassLoader);
        }
        if (start) {
            thread.start();
        }
        return thread;
    }

    @kotlin.internal.InlineOnly
    private static final <T> T getOrSet(@org.jetbrains.annotations.NotNull java.lang.ThreadLocal<T> $receiver, kotlin.jvm.functions.Function0<? extends T> function0) {
        T t = $receiver.get();
        if (t != null) {
            return t;
        }
        java.lang.Object p1 = function0.invoke();
        $receiver.set(p1);
        return p1;
    }
}
