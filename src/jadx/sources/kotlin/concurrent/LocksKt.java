package kotlin.concurrent;

@kotlin.Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u001a\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u001a&\u0010\u0000\u001a\u0002H\u0001\"\u0004\b\u0000\u0010\u0001*\u00020\u00022\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u0002H\u00010\u0004H\u0087\b\u00a2\u0006\u0002\u0010\u0005\u001a&\u0010\u0006\u001a\u0002H\u0001\"\u0004\b\u0000\u0010\u0001*\u00020\u00072\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u0002H\u00010\u0004H\u0087\b\u00a2\u0006\u0002\u0010\b\u001a&\u0010\t\u001a\u0002H\u0001\"\u0004\b\u0000\u0010\u0001*\u00020\u00022\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u0002H\u00010\u0004H\u0087\b\u00a2\u0006\u0002\u0010\u0005\u00a8\u0006\n"}, d2 = {"read", "T", "Ljava/util/concurrent/locks/ReentrantReadWriteLock;", "action", "Lkotlin/Function0;", "(Ljava/util/concurrent/locks/ReentrantReadWriteLock;Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;", "withLock", "Ljava/util/concurrent/locks/Lock;", "(Ljava/util/concurrent/locks/Lock;Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;", "write", "kotlin-stdlib"}, k = 2, mv = {1, 1, 6})
@kotlin.jvm.JvmName(name = "LocksKt")
/* compiled from: Locks.kt */
public final class LocksKt {
    @kotlin.internal.InlineOnly
    private static final <T> T withLock(@org.jetbrains.annotations.NotNull java.util.concurrent.locks.Lock $receiver, kotlin.jvm.functions.Function0<? extends T> action) {
        $receiver.lock();
        try {
            return action.invoke();
        } finally {
            kotlin.jvm.internal.InlineMarker.finallyStart(1);
            $receiver.unlock();
            kotlin.jvm.internal.InlineMarker.finallyEnd(1);
        }
    }

    @kotlin.internal.InlineOnly
    private static final <T> T read(@org.jetbrains.annotations.NotNull java.util.concurrent.locks.ReentrantReadWriteLock $receiver, kotlin.jvm.functions.Function0<? extends T> action) {
        java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock rl = $receiver.readLock();
        rl.lock();
        try {
            return action.invoke();
        } finally {
            kotlin.jvm.internal.InlineMarker.finallyStart(1);
            rl.unlock();
            kotlin.jvm.internal.InlineMarker.finallyEnd(1);
        }
    }

    @kotlin.internal.InlineOnly
    private static final <T> T write(@org.jetbrains.annotations.NotNull java.util.concurrent.locks.ReentrantReadWriteLock $receiver, kotlin.jvm.functions.Function0<? extends T> action) {
        int readCount;
        int i;
        int it;
        java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock rl = $receiver.readLock();
        if ($receiver.getWriteHoldCount() == 0) {
            readCount = $receiver.getReadHoldCount();
        } else {
            readCount = 0;
        }
        int i2 = readCount - 1;
        if (0 <= i2) {
            int it2 = 0;
            while (true) {
                rl.unlock();
                if (it2 == i2) {
                    break;
                }
                it2++;
            }
        }
        java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock wl = $receiver.writeLock();
        wl.lock();
        try {
            if (0 <= i) {
                while (true) {
                    if (it == i) {
                        break;
                    }
                }
            }
            return action.invoke();
        } finally {
            kotlin.jvm.internal.InlineMarker.finallyStart(1);
            i = readCount - 1;
            if (0 <= i) {
                it = 0;
                while (true) {
                    rl.lock();
                    if (it == i) {
                        break;
                    }
                    it++;
                }
            }
            wl.unlock();
            kotlin.jvm.internal.InlineMarker.finallyEnd(1);
        }
    }
}
