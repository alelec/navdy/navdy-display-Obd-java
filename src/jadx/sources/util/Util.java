package util;

public class Util {
    private static final int END_OF_STREAM = -1;
    private static final int INPUT_BUFFER_SIZE = 16384;
    private static final java.lang.String TAG = "ObdService";
    public static final java.lang.String UTF_8 = "UTF-8";

    public static java.lang.String readObdAssetFile(android.content.Context context, java.lang.String assetFileName) throws java.io.IOException {
        java.io.InputStream is = context.getAssets().open(assetFileName);
        java.lang.String content = convertInputStreamToString(is, UTF_8);
        try {
            is.close();
        } catch (java.io.IOException e) {
            android.util.Log.e(TAG, "Error closing the stream after reading the asset file");
        }
        return content;
    }

    public static java.lang.String convertInputStreamToString(java.io.InputStream inputStream, java.lang.String charSet) throws java.io.IOException {
        java.io.ByteArrayOutputStream byteArrayOutputStream = new java.io.ByteArrayOutputStream();
        byte[] buffer = new byte[16384];
        while (true) {
            int n = inputStream.read(buffer);
            if (n == -1) {
                return byteArrayOutputStream.toString(charSet);
            }
            byteArrayOutputStream.write(buffer, 0, n);
        }
    }

    public static util.Configuration[] loadConfigurationMappingList(android.content.Context context, java.lang.String fileName) {
        int i = 0;
        util.Configuration[] configurationsList = null;
        try {
            java.lang.String[] configurationMappingEntries = readObdAssetFile(context, fileName).split("\n");
            configurationsList = new util.Configuration[configurationMappingEntries.length];
            int length = configurationMappingEntries.length;
            int i2 = 0;
            while (i < length) {
                java.lang.String[] parts = configurationMappingEntries[i].split(",");
                util.Configuration configuration = new util.Configuration();
                configuration.pattern = java.util.regex.Pattern.compile(parts[0], 2);
                configuration.configurationName = parts[1];
                int i3 = i2 + 1;
                configurationsList[i2] = configuration;
                i++;
                i2 = i3;
            }
        } catch (java.io.IOException e) {
        }
        return configurationsList;
    }

    public static util.Configuration pickConfiguration(util.Configuration[] configurationMapping, java.lang.String expression) {
        for (util.Configuration configuration : configurationMapping) {
            if (configuration.pattern.matcher(expression).matches()) {
                return configuration;
            }
        }
        return null;
    }
}
