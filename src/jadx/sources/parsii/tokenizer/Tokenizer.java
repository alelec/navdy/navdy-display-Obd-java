package parsii.tokenizer;

public class Tokenizer extends parsii.tokenizer.Lookahead<parsii.tokenizer.Token> {
    private java.lang.String blockCommentEnd = "*/";
    private java.lang.String blockCommentStart = "/*";
    private char[] brackets = {ch.qos.logback.core.CoreConstants.LEFT_PARENTHESIS_CHAR, '[', ch.qos.logback.core.CoreConstants.CURLY_LEFT, ch.qos.logback.core.CoreConstants.CURLY_RIGHT, ']', ch.qos.logback.core.CoreConstants.RIGHT_PARENTHESIS_CHAR};
    private char decimalSeparator = ch.qos.logback.core.CoreConstants.DOT;
    private char effectiveDecimalSeparator = ch.qos.logback.core.CoreConstants.DOT;
    private char groupingSeparator = '_';
    protected parsii.tokenizer.LookaheadReader input;
    private java.util.Map<java.lang.String, java.lang.String> keywords = new java.util.IdentityHashMap();
    private boolean keywordsCaseSensitive = false;
    private java.lang.String lineComment = "//";
    private java.util.Set<java.lang.Character> specialIdStarters = new java.util.HashSet();
    private java.util.Set<java.lang.Character> specialIdTerminators = new java.util.HashSet();
    private java.util.Map<java.lang.Character, java.lang.Character> stringDelimiters = new java.util.IdentityHashMap();
    private boolean treatSinglePipeAsBracket = true;

    public Tokenizer(java.io.Reader input2) {
        this.input = new parsii.tokenizer.LookaheadReader(input2);
        this.input.setProblemCollector(this.problemCollector);
        addStringDelimiter('\"', ch.qos.logback.core.CoreConstants.ESCAPE_CHAR);
        addStringDelimiter(ch.qos.logback.core.CoreConstants.SINGLE_QUOTE_CHAR, 0);
    }

    public void setProblemCollector(java.util.List<parsii.tokenizer.ParseError> problemCollector) {
        super.setProblemCollector(problemCollector);
        this.input.setProblemCollector(problemCollector);
    }

    /* access modifiers changed from: protected */
    public parsii.tokenizer.Token endOfInput() {
        return parsii.tokenizer.Token.createAndFill(parsii.tokenizer.Token.TokenType.EOI, (parsii.tokenizer.Char) this.input.current());
    }

    /* access modifiers changed from: protected */
    public parsii.tokenizer.Token fetch() {
        while (((parsii.tokenizer.Char) this.input.current()).isWhitepace()) {
            this.input.consume();
        }
        if (((parsii.tokenizer.Char) this.input.current()).isEndOfInput()) {
            return null;
        }
        if (isAtStartOfLineComment()) {
            skipToEndOfLine();
            return fetch();
        } else if (isAtStartOfBlockComment()) {
            skipBlockComment();
            return fetch();
        } else if (isAtStartOfNumber()) {
            return fetchNumber();
        } else {
            if (isAtStartOfIdentifier()) {
                return fetchId();
            }
            if (this.stringDelimiters.containsKey(java.lang.Character.valueOf(((parsii.tokenizer.Char) this.input.current()).getValue()))) {
                return fetchString();
            }
            if (isAtBracket(false)) {
                return parsii.tokenizer.Token.createAndFill(parsii.tokenizer.Token.TokenType.SYMBOL, (parsii.tokenizer.Char) this.input.consume());
            }
            if (isAtStartOfSpecialId()) {
                return fetchSpecialId();
            }
            if (isSymbolCharacter((parsii.tokenizer.Char) this.input.current())) {
                return fetchSymbol();
            }
            this.problemCollector.add(parsii.tokenizer.ParseError.error((parsii.tokenizer.Position) this.input.current(), java.lang.String.format("Invalid character in input: '%s'", new java.lang.Object[]{((parsii.tokenizer.Char) this.input.current()).getStringValue()})));
            this.input.consume();
            return fetch();
        }
    }

    /* access modifiers changed from: protected */
    public boolean isAtStartOfSpecialId() {
        return this.specialIdStarters.contains(java.lang.Character.valueOf(((parsii.tokenizer.Char) this.input.current()).getValue()));
    }

    /* access modifiers changed from: protected */
    public boolean isAtStartOfNumber() {
        if (!((parsii.tokenizer.Char) this.input.current()).isDigit()) {
            if (!((parsii.tokenizer.Char) this.input.current()).is(ch.qos.logback.core.CoreConstants.DASH_CHAR) || !((parsii.tokenizer.Char) this.input.next()).isDigit()) {
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean isAtBracket(boolean inSymbol) {
        if (((parsii.tokenizer.Char) this.input.current()).is(this.brackets)) {
            return true;
        }
        if (!inSymbol && this.treatSinglePipeAsBracket) {
            if (((parsii.tokenizer.Char) this.input.current()).is('|')) {
                if (!((parsii.tokenizer.Char) this.input.next()).is('|')) {
                    return true;
                }
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean canConsumeThisString(java.lang.String string) {
        if (string == null) {
            return false;
        }
        for (int i = 0; i < string.length(); i++) {
            if (!((parsii.tokenizer.Char) this.input.next(i)).is(string.charAt(i))) {
                return false;
            }
        }
        this.input.consume(string.length());
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean isAtStartOfLineComment() {
        if (this.lineComment != null) {
            return canConsumeThisString(this.lineComment);
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void skipToEndOfLine() {
        while (!((parsii.tokenizer.Char) this.input.current()).isEndOfInput() && !((parsii.tokenizer.Char) this.input.current()).isNewLine()) {
            this.input.consume();
        }
    }

    /* access modifiers changed from: protected */
    public boolean isAtStartOfBlockComment() {
        return canConsumeThisString(this.blockCommentStart);
    }

    /* access modifiers changed from: protected */
    public boolean isAtEndOfBlockComment() {
        return canConsumeThisString(this.blockCommentEnd);
    }

    /* access modifiers changed from: protected */
    public void skipBlockComment() {
        while (!((parsii.tokenizer.Char) this.input.current()).isEndOfInput()) {
            if (!isAtEndOfBlockComment()) {
                this.input.consume();
            } else {
                return;
            }
        }
        this.problemCollector.add(parsii.tokenizer.ParseError.error((parsii.tokenizer.Position) this.input.current(), "Premature end of block comment"));
    }

    /* access modifiers changed from: protected */
    public parsii.tokenizer.Token fetchString() {
        char separator = ((parsii.tokenizer.Char) this.input.current()).getValue();
        char escapeChar = ((java.lang.Character) this.stringDelimiters.get(java.lang.Character.valueOf(((parsii.tokenizer.Char) this.input.current()).getValue()))).charValue();
        parsii.tokenizer.Token result = parsii.tokenizer.Token.create(parsii.tokenizer.Token.TokenType.STRING, (parsii.tokenizer.Position) this.input.current());
        result.addToTrigger((parsii.tokenizer.Char) this.input.consume());
        while (!((parsii.tokenizer.Char) this.input.current()).isNewLine()) {
            if (((parsii.tokenizer.Char) this.input.current()).is(separator) || ((parsii.tokenizer.Char) this.input.current()).isEndOfInput()) {
                break;
            }
            if (escapeChar != 0) {
                if (((parsii.tokenizer.Char) this.input.current()).is(escapeChar)) {
                    result.addToSource((parsii.tokenizer.Char) this.input.consume());
                    if (!handleStringEscape(separator, escapeChar, result)) {
                        this.problemCollector.add(parsii.tokenizer.ParseError.error((parsii.tokenizer.Position) this.input.next(), java.lang.String.format("Cannot use '%s' as escaped character", new java.lang.Object[]{((parsii.tokenizer.Char) this.input.next()).getStringValue()})));
                    }
                }
            }
            result.addToContent((parsii.tokenizer.Char) this.input.consume());
        }
        if (((parsii.tokenizer.Char) this.input.current()).is(separator)) {
            result.addToSource((parsii.tokenizer.Char) this.input.consume());
        } else {
            this.problemCollector.add(parsii.tokenizer.ParseError.error((parsii.tokenizer.Position) this.input.current(), "Premature end of string constant"));
        }
        return result;
    }

    /* access modifiers changed from: protected */
    public boolean handleStringEscape(char separator, char escapeChar, parsii.tokenizer.Token stringToken) {
        if (((parsii.tokenizer.Char) this.input.current()).is(separator)) {
            stringToken.addToContent(separator);
            stringToken.addToSource((parsii.tokenizer.Char) this.input.consume());
            return true;
        }
        if (((parsii.tokenizer.Char) this.input.current()).is(escapeChar)) {
            stringToken.silentAddToContent(escapeChar);
            stringToken.addToSource((parsii.tokenizer.Char) this.input.consume());
            return true;
        }
        if (((parsii.tokenizer.Char) this.input.current()).is('n')) {
            stringToken.silentAddToContent(10);
            stringToken.addToSource((parsii.tokenizer.Char) this.input.consume());
            return true;
        }
        if (!((parsii.tokenizer.Char) this.input.current()).is('r')) {
            return false;
        }
        stringToken.silentAddToContent(13);
        stringToken.addToSource((parsii.tokenizer.Char) this.input.consume());
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean isAtStartOfIdentifier() {
        return ((parsii.tokenizer.Char) this.input.current()).isLetter();
    }

    /* access modifiers changed from: protected */
    public parsii.tokenizer.Token fetchId() {
        parsii.tokenizer.Token result = parsii.tokenizer.Token.create(parsii.tokenizer.Token.TokenType.ID, (parsii.tokenizer.Position) this.input.current());
        result.addToContent((parsii.tokenizer.Char) this.input.consume());
        while (isIdentifierChar((parsii.tokenizer.Char) this.input.current())) {
            result.addToContent((parsii.tokenizer.Char) this.input.consume());
        }
        if (((parsii.tokenizer.Char) this.input.current()).isEndOfInput() || !this.specialIdTerminators.contains(java.lang.Character.valueOf(((parsii.tokenizer.Char) this.input.current()).getValue()))) {
            return handleKeywords(result);
        }
        parsii.tokenizer.Token specialId = parsii.tokenizer.Token.create(parsii.tokenizer.Token.TokenType.SPECIAL_ID, result);
        specialId.setTrigger(((parsii.tokenizer.Char) this.input.current()).getStringValue());
        specialId.setContent(result.getContents());
        specialId.setSource(result.getContents());
        specialId.addToSource((parsii.tokenizer.Char) this.input.current());
        this.input.consume();
        return handleKeywords(specialId);
    }

    /* access modifiers changed from: protected */
    public parsii.tokenizer.Token handleKeywords(parsii.tokenizer.Token idToken) {
        java.lang.String keyword = this.keywordsCaseSensitive ? (java.lang.String) this.keywords.get(idToken.getContents().intern()) : (java.lang.String) this.keywords.get(idToken.getContents().toLowerCase().intern());
        if (keyword == null) {
            return idToken;
        }
        parsii.tokenizer.Token keywordToken = parsii.tokenizer.Token.create(parsii.tokenizer.Token.TokenType.KEYWORD, idToken);
        keywordToken.setTrigger(keyword);
        keywordToken.setContent(idToken.getContents());
        keywordToken.setSource(idToken.getSource());
        return keywordToken;
    }

    /* access modifiers changed from: protected */
    public boolean isIdentifierChar(parsii.tokenizer.Char current) {
        if (!current.isDigit() && !current.isLetter()) {
            if (!current.is('_')) {
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public parsii.tokenizer.Token fetchSpecialId() {
        parsii.tokenizer.Token result = parsii.tokenizer.Token.create(parsii.tokenizer.Token.TokenType.SPECIAL_ID, (parsii.tokenizer.Position) this.input.current());
        result.addToTrigger((parsii.tokenizer.Char) this.input.consume());
        while (isIdentifierChar((parsii.tokenizer.Char) this.input.current())) {
            result.addToContent((parsii.tokenizer.Char) this.input.consume());
        }
        return handleKeywords(result);
    }

    /* access modifiers changed from: protected */
    public parsii.tokenizer.Token fetchSymbol() {
        parsii.tokenizer.Token result = parsii.tokenizer.Token.create(parsii.tokenizer.Token.TokenType.SYMBOL, (parsii.tokenizer.Position) this.input.current());
        result.addToTrigger((parsii.tokenizer.Char) this.input.consume());
        while (isSymbolCharacter((parsii.tokenizer.Char) this.input.current())) {
            result.addToTrigger((parsii.tokenizer.Char) this.input.consume());
        }
        return result;
    }

    /* access modifiers changed from: protected */
    public boolean isSymbolCharacter(parsii.tokenizer.Char ch2) {
        if (ch2.isEndOfInput() || ch2.isDigit() || ch2.isLetter() || ch2.isWhitepace() || java.lang.Character.isISOControl(ch2.getValue()) || isAtBracket(true) || isAtStartOfBlockComment() || isAtStartOfLineComment() || isAtStartOfNumber() || isAtStartOfIdentifier() || this.stringDelimiters.containsKey(java.lang.Character.valueOf(ch2.getValue()))) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public parsii.tokenizer.Token fetchNumber() {
        parsii.tokenizer.Token result = parsii.tokenizer.Token.create(parsii.tokenizer.Token.TokenType.INTEGER, (parsii.tokenizer.Position) this.input.current());
        result.addToContent((parsii.tokenizer.Char) this.input.consume());
        while (true) {
            if (!((parsii.tokenizer.Char) this.input.current()).isDigit()) {
                if (!((parsii.tokenizer.Char) this.input.current()).is(this.decimalSeparator, this.groupingSeparator) || !((parsii.tokenizer.Char) this.input.next()).isDigit()) {
                    return result;
                }
            }
            if (((parsii.tokenizer.Char) this.input.current()).is(this.groupingSeparator)) {
                result.addToSource((parsii.tokenizer.Char) this.input.consume());
            } else {
                if (((parsii.tokenizer.Char) this.input.current()).is(this.decimalSeparator)) {
                    if (result.is(parsii.tokenizer.Token.TokenType.DECIMAL)) {
                        this.problemCollector.add(parsii.tokenizer.ParseError.error((parsii.tokenizer.Position) this.input.current(), "Unexpected decimal separators"));
                    } else {
                        parsii.tokenizer.Token decimalToken = parsii.tokenizer.Token.create(parsii.tokenizer.Token.TokenType.DECIMAL, result);
                        decimalToken.setContent(result.getContents() + this.effectiveDecimalSeparator);
                        decimalToken.setSource(result.getSource());
                        result = decimalToken;
                    }
                    result.addToSource((parsii.tokenizer.Char) this.input.consume());
                } else {
                    result.addToContent((parsii.tokenizer.Char) this.input.consume());
                }
            }
        }
        return result;
    }

    public boolean isKeywordsCaseSensitive() {
        return this.keywordsCaseSensitive;
    }

    public void setKeywordsCaseSensitive(boolean keywordsCaseSensitive2) {
        this.keywordsCaseSensitive = keywordsCaseSensitive2;
    }

    public void addKeyword(java.lang.String keyword) {
        this.keywords.put(this.keywordsCaseSensitive ? keyword.intern() : keyword.toLowerCase().intern(), keyword);
    }

    public void addSpecialIdStarter(char character) {
        this.specialIdStarters.add(java.lang.Character.valueOf(character));
    }

    public void addSpecialIdTerminator(char character) {
        this.specialIdTerminators.add(java.lang.Character.valueOf(character));
    }

    public void clearStringDelimiters() {
        this.stringDelimiters.clear();
    }

    public void addStringDelimiter(char stringDelimiter, char escapeCharacter) {
        this.stringDelimiters.put(java.lang.Character.valueOf(stringDelimiter), java.lang.Character.valueOf(escapeCharacter));
    }

    public void addUnescapedStringDelimiter(char stringDelimiter) {
        this.stringDelimiters.put(java.lang.Character.valueOf(stringDelimiter), java.lang.Character.valueOf(0));
    }

    public char getDecimalSeparator() {
        return this.decimalSeparator;
    }

    public void setDecimalSeparator(char decimalSeparator2) {
        this.decimalSeparator = decimalSeparator2;
    }

    public char getEffectiveDecimalSeparator() {
        return this.effectiveDecimalSeparator;
    }

    public void setEffectiveDecimalSeparator(char effectiveDecimalSeparator2) {
        this.effectiveDecimalSeparator = effectiveDecimalSeparator2;
    }

    public char getGroupingSeparator() {
        return this.groupingSeparator;
    }

    public void setGroupingSeparator(char groupingSeparator2) {
        this.groupingSeparator = groupingSeparator2;
    }

    public java.lang.String getLineComment() {
        return this.lineComment;
    }

    public void setLineComment(java.lang.String lineComment2) {
        this.lineComment = lineComment2;
    }

    public java.lang.String getBlockCommentStart() {
        return this.blockCommentStart;
    }

    public void setBlockCommentStart(java.lang.String blockCommentStart2) {
        this.blockCommentStart = blockCommentStart2;
    }

    public java.lang.String getBlockCommentEnd() {
        return this.blockCommentEnd;
    }

    public void setBlockCommentEnd(java.lang.String blockCommentEnd2) {
        this.blockCommentEnd = blockCommentEnd2;
    }

    public java.lang.String toString() {
        if (this.itemBuffer.size() == 0) {
            return "No Token fetched...";
        }
        if (this.itemBuffer.size() < 2) {
            return "Current: " + current();
        }
        return "Current: " + ((parsii.tokenizer.Token) current()).toString() + ", Next: " + ((parsii.tokenizer.Token) next()).toString();
    }

    public boolean more() {
        return ((parsii.tokenizer.Token) current()).isNotEnd();
    }

    public boolean atEnd() {
        return ((parsii.tokenizer.Token) current()).isEnd();
    }

    public void addError(parsii.tokenizer.Position pos, java.lang.String message, java.lang.Object... parameters) {
        getProblemCollector().add(parsii.tokenizer.ParseError.error(pos, java.lang.String.format(message, parameters)));
    }

    public void addWarning(parsii.tokenizer.Position pos, java.lang.String message, java.lang.Object... parameters) {
        getProblemCollector().add(parsii.tokenizer.ParseError.warning(pos, java.lang.String.format(message, parameters)));
    }

    public void consumeExpectedSymbol(java.lang.String symbol) {
        if (((parsii.tokenizer.Token) current()).matches(parsii.tokenizer.Token.TokenType.SYMBOL, symbol)) {
            consume();
            return;
        }
        addError((parsii.tokenizer.Position) current(), "Unexpected token: '%s'. Expected: '%s'", ((parsii.tokenizer.Token) current()).getSource(), symbol);
    }

    public void consumeExpectedKeyword(java.lang.String keyword) {
        if (((parsii.tokenizer.Token) current()).matches(parsii.tokenizer.Token.TokenType.KEYWORD, keyword)) {
            consume();
            return;
        }
        addError((parsii.tokenizer.Position) current(), "Unexpected token: '%s'. Expected: '%s'", ((parsii.tokenizer.Token) current()).getSource(), keyword);
    }

    public void throwOnErrorOrWarning() throws parsii.tokenizer.ParseException {
        if (!getProblemCollector().isEmpty()) {
            throw parsii.tokenizer.ParseException.create(getProblemCollector());
        }
    }

    public void throwOnError() throws parsii.tokenizer.ParseException {
        for (parsii.tokenizer.ParseError e : getProblemCollector()) {
            if (e.getSeverity() == parsii.tokenizer.ParseError.Severity.ERROR) {
                throw parsii.tokenizer.ParseException.create(getProblemCollector());
            }
        }
    }
}
