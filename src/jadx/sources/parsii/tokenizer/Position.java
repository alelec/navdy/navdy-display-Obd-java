package parsii.tokenizer;

public interface Position {
    public static final parsii.tokenizer.Position UNKNOWN = new parsii.tokenizer.Position() {
        public int getLine() {
            return 0;
        }

        public int getPos() {
            return 0;
        }
    };

    int getLine();

    int getPos();
}
