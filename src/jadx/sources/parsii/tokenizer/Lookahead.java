package parsii.tokenizer;

public abstract class Lookahead<T> {
    protected T endOfInputIndicator;
    protected boolean endReached = false;
    protected java.util.List<T> itemBuffer = new java.util.ArrayList();
    protected java.util.List<parsii.tokenizer.ParseError> problemCollector = new java.util.ArrayList();

    /* access modifiers changed from: protected */
    public abstract T endOfInput();

    /* access modifiers changed from: protected */
    public abstract T fetch();

    public T current() {
        return next(0);
    }

    public T next() {
        return next(1);
    }

    public T next(int offset) {
        if (offset < 0) {
            throw new java.lang.IllegalArgumentException("offset < 0");
        }
        while (this.itemBuffer.size() <= offset && !this.endReached) {
            T item = fetch();
            if (item != null) {
                this.itemBuffer.add(item);
            } else {
                this.endReached = true;
            }
        }
        if (offset < this.itemBuffer.size()) {
            return this.itemBuffer.get(offset);
        }
        if (this.endOfInputIndicator == null) {
            this.endOfInputIndicator = endOfInput();
        }
        return this.endOfInputIndicator;
    }

    public T consume() {
        T result = current();
        consume(1);
        return result;
    }

    public void consume(int numberOfItems) {
        if (numberOfItems < 0) {
            throw new java.lang.IllegalArgumentException("numberOfItems < 0");
        }
        while (true) {
            int numberOfItems2 = numberOfItems;
            numberOfItems = numberOfItems2 - 1;
            if (numberOfItems2 <= 0) {
                return;
            }
            if (!this.itemBuffer.isEmpty()) {
                this.itemBuffer.remove(0);
            } else if (!this.endReached) {
                if (fetch() == null) {
                    this.endReached = true;
                }
            } else {
                return;
            }
        }
    }

    public java.util.List<parsii.tokenizer.ParseError> getProblemCollector() {
        return this.problemCollector;
    }

    public void setProblemCollector(java.util.List<parsii.tokenizer.ParseError> problemCollector2) {
        this.problemCollector = problemCollector2;
    }
}
