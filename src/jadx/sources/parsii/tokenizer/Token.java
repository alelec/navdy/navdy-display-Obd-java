package parsii.tokenizer;

public class Token implements parsii.tokenizer.Position {
    private java.lang.String contents = "";
    private java.lang.String internTrigger = null;
    private int line;
    protected int pos;
    private java.lang.String source = "";
    private java.lang.String trigger = "";
    private parsii.tokenizer.Token.TokenType type;

    public enum TokenType {
        ID,
        SPECIAL_ID,
        STRING,
        DECIMAL,
        INTEGER,
        SYMBOL,
        KEYWORD,
        EOI
    }

    private Token() {
    }

    public static parsii.tokenizer.Token create(parsii.tokenizer.Token.TokenType type2, parsii.tokenizer.Position pos2) {
        parsii.tokenizer.Token result = new parsii.tokenizer.Token();
        result.type = type2;
        result.line = pos2.getLine();
        result.pos = pos2.getPos();
        return result;
    }

    public static parsii.tokenizer.Token createAndFill(parsii.tokenizer.Token.TokenType type2, parsii.tokenizer.Char ch2) {
        parsii.tokenizer.Token result = new parsii.tokenizer.Token();
        result.type = type2;
        result.line = ch2.getLine();
        result.pos = ch2.getPos();
        result.contents = ch2.getStringValue();
        result.trigger = ch2.getStringValue();
        result.source = ch2.toString();
        return result;
    }

    public parsii.tokenizer.Token addToTrigger(parsii.tokenizer.Char ch2) {
        this.trigger += ch2.getValue();
        this.internTrigger = null;
        this.source += ch2.getValue();
        return this;
    }

    public parsii.tokenizer.Token addToSource(parsii.tokenizer.Char ch2) {
        this.source += ch2.getValue();
        return this;
    }

    public parsii.tokenizer.Token addToContent(parsii.tokenizer.Char ch2) {
        return addToContent(ch2.getValue());
    }

    public parsii.tokenizer.Token addToContent(char ch2) {
        this.contents += ch2;
        this.source += ch2;
        return this;
    }

    public parsii.tokenizer.Token silentAddToContent(char ch2) {
        this.contents += ch2;
        return this;
    }

    public java.lang.String getTrigger() {
        if (this.internTrigger == null) {
            this.internTrigger = this.trigger.intern();
        }
        return this.internTrigger;
    }

    public parsii.tokenizer.Token.TokenType getType() {
        return this.type;
    }

    public java.lang.String getContents() {
        return this.contents;
    }

    public java.lang.String getSource() {
        return this.source;
    }

    public int getLine() {
        return this.line;
    }

    public int getPos() {
        return this.pos;
    }

    public void setTrigger(java.lang.String trigger2) {
        this.trigger = trigger2;
        this.internTrigger = null;
    }

    public void setContent(java.lang.String content) {
        this.contents = content;
    }

    public void setSource(java.lang.String source2) {
        this.source = source2;
    }

    public boolean isEnd() {
        return this.type == parsii.tokenizer.Token.TokenType.EOI;
    }

    public boolean isNotEnd() {
        return this.type != parsii.tokenizer.Token.TokenType.EOI;
    }

    public boolean matches(parsii.tokenizer.Token.TokenType type2, java.lang.String trigger2) {
        if (!is(type2)) {
            return false;
        }
        if (trigger2 == null) {
            throw new java.lang.IllegalArgumentException("trigger must not be null");
        } else if (getTrigger() == trigger2.intern()) {
            return true;
        } else {
            return false;
        }
    }

    public boolean wasTriggeredBy(java.lang.String... triggers) {
        java.lang.String[] arr$;
        if (triggers.length == 0) {
            return false;
        }
        for (java.lang.String aTrigger : triggers) {
            if (aTrigger != null && aTrigger.intern() == getTrigger()) {
                return true;
            }
        }
        return false;
    }

    public boolean hasContent(java.lang.String content) {
        if (content != null) {
            return content.equalsIgnoreCase(getContents());
        }
        throw new java.lang.IllegalArgumentException("content must not be null");
    }

    public boolean is(parsii.tokenizer.Token.TokenType type2) {
        return this.type == type2;
    }

    public boolean isSymbol(java.lang.String... symbols) {
        if (symbols.length == 0) {
            return is(parsii.tokenizer.Token.TokenType.SYMBOL);
        }
        for (java.lang.String symbol : symbols) {
            if (matches(parsii.tokenizer.Token.TokenType.SYMBOL, symbol)) {
                return true;
            }
        }
        return false;
    }

    public boolean isKeyword(java.lang.String... keywords) {
        if (keywords.length == 0) {
            return is(parsii.tokenizer.Token.TokenType.KEYWORD);
        }
        for (java.lang.String keyword : keywords) {
            if (matches(parsii.tokenizer.Token.TokenType.KEYWORD, keyword)) {
                return true;
            }
        }
        return false;
    }

    public boolean isIdentifier(java.lang.String... values) {
        if (values.length == 0) {
            return is(parsii.tokenizer.Token.TokenType.ID);
        }
        for (java.lang.String value : values) {
            if (matches(parsii.tokenizer.Token.TokenType.ID, value)) {
                return true;
            }
        }
        return false;
    }

    public boolean isSpecialIdentifier(java.lang.String... triggers) {
        if (triggers.length == 0) {
            return is(parsii.tokenizer.Token.TokenType.SPECIAL_ID);
        }
        for (java.lang.String trigger2 : triggers) {
            if (matches(parsii.tokenizer.Token.TokenType.SPECIAL_ID, trigger2)) {
                return true;
            }
        }
        return false;
    }

    public boolean isSpecialIdentifierWithContent(java.lang.String trigger2, java.lang.String... contents2) {
        java.lang.String[] arr$;
        if (!matches(parsii.tokenizer.Token.TokenType.SPECIAL_ID, trigger2)) {
            return false;
        }
        if (contents2.length == 0) {
            return true;
        }
        for (java.lang.String content : contents2) {
            if (content != null && content.equals(getContents())) {
                return true;
            }
        }
        return false;
    }

    public boolean isInteger() {
        return is(parsii.tokenizer.Token.TokenType.INTEGER);
    }

    public boolean isDecimal() {
        return is(parsii.tokenizer.Token.TokenType.DECIMAL);
    }

    public boolean isNumber() {
        return isInteger() || isDecimal();
    }

    public boolean isString() {
        return is(parsii.tokenizer.Token.TokenType.STRING);
    }

    public java.lang.String toString() {
        return getType().toString() + ":" + getSource() + " (" + this.line + ":" + this.pos + ")";
    }
}
