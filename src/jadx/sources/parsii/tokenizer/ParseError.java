package parsii.tokenizer;

public class ParseError {
    private java.lang.String message;
    private parsii.tokenizer.Position pos;
    private final parsii.tokenizer.ParseError.Severity severity;

    public enum Severity {
        WARNING,
        ERROR
    }

    protected ParseError(parsii.tokenizer.Position pos2, java.lang.String message2, parsii.tokenizer.ParseError.Severity severity2) {
        this.pos = pos2;
        this.message = message2;
        this.severity = severity2;
    }

    public static parsii.tokenizer.ParseError warning(parsii.tokenizer.Position pos2, java.lang.String msg) {
        java.lang.String message2 = msg;
        if (pos2.getLine() > 0) {
            message2 = java.lang.String.format("%3d:%2d: %s", new java.lang.Object[]{java.lang.Integer.valueOf(pos2.getLine()), java.lang.Integer.valueOf(pos2.getPos()), msg});
        }
        return new parsii.tokenizer.ParseError(pos2, message2, parsii.tokenizer.ParseError.Severity.WARNING);
    }

    public static parsii.tokenizer.ParseError error(parsii.tokenizer.Position pos2, java.lang.String msg) {
        java.lang.String message2 = msg;
        if (pos2.getLine() > 0) {
            message2 = java.lang.String.format("%3d:%2d: %s", new java.lang.Object[]{java.lang.Integer.valueOf(pos2.getLine()), java.lang.Integer.valueOf(pos2.getPos()), msg});
        }
        return new parsii.tokenizer.ParseError(pos2, message2, parsii.tokenizer.ParseError.Severity.ERROR);
    }

    public parsii.tokenizer.Position getPosition() {
        return this.pos;
    }

    public java.lang.String getMessage() {
        return this.message;
    }

    public parsii.tokenizer.ParseError.Severity getSeverity() {
        return this.severity;
    }

    public java.lang.String toString() {
        return java.lang.String.format("%s %s", new java.lang.Object[]{this.severity, this.message});
    }
}
