package parsii.tokenizer;

public class LookaheadReader extends parsii.tokenizer.Lookahead<parsii.tokenizer.Char> {
    private java.io.Reader input;
    private int line = 1;
    private int pos = 0;

    public LookaheadReader(java.io.Reader input2) {
        if (input2 == null) {
            throw new java.lang.IllegalArgumentException("input must not be null");
        }
        this.input = new java.io.BufferedReader(input2);
    }

    /* access modifiers changed from: protected */
    public parsii.tokenizer.Char endOfInput() {
        return new parsii.tokenizer.Char(0, this.line, this.pos);
    }

    /* access modifiers changed from: protected */
    public parsii.tokenizer.Char fetch() {
        try {
            int character = this.input.read();
            if (character == -1) {
                return null;
            }
            if (character == 10) {
                this.line++;
                this.pos = 0;
            }
            this.pos++;
            return new parsii.tokenizer.Char((char) character, this.line, this.pos);
        } catch (java.io.IOException e) {
            this.problemCollector.add(parsii.tokenizer.ParseError.error(new parsii.tokenizer.Char(0, this.line, this.pos), e.getMessage()));
            return null;
        }
    }

    public java.lang.String toString() {
        if (this.itemBuffer.size() == 0) {
            return this.line + ":" + this.pos + ": Buffer empty";
        }
        if (this.itemBuffer.size() < 2) {
            return this.line + ":" + this.pos + ": " + current();
        }
        return this.line + ":" + this.pos + ": " + current() + ", " + next();
    }
}
