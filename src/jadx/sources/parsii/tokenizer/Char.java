package parsii.tokenizer;

public class Char implements parsii.tokenizer.Position {
    private int line;
    private int pos;
    private char value;

    Char(char value2, int line2, int pos2) {
        this.value = value2;
        this.line = line2;
        this.pos = pos2;
    }

    public char getValue() {
        return this.value;
    }

    public int getLine() {
        return this.line;
    }

    public int getPos() {
        return this.pos;
    }

    public boolean isDigit() {
        return java.lang.Character.isDigit(this.value);
    }

    public boolean isLetter() {
        return java.lang.Character.isLetter(this.value);
    }

    public boolean isWhitepace() {
        return java.lang.Character.isWhitespace(this.value) && !isEndOfInput();
    }

    public boolean isNewLine() {
        return this.value == 10;
    }

    public boolean isEndOfInput() {
        return this.value == 0;
    }

    public java.lang.String toString() {
        if (isEndOfInput()) {
            return "<End Of Input>";
        }
        return java.lang.String.valueOf(this.value);
    }

    public boolean is(char... tests) {
        char[] arr$;
        for (char test : tests) {
            if (test == this.value && test != 0) {
                return true;
            }
        }
        return false;
    }

    public java.lang.String getStringValue() {
        if (isEndOfInput()) {
            return "";
        }
        return java.lang.String.valueOf(this.value);
    }
}
