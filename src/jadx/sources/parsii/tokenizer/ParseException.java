package parsii.tokenizer;

public class ParseException extends java.lang.Exception {
    private java.util.List<parsii.tokenizer.ParseError> errors;

    public static parsii.tokenizer.ParseException create(java.util.List<parsii.tokenizer.ParseError> errors2) {
        if (errors2.size() == 1) {
            return new parsii.tokenizer.ParseException(((parsii.tokenizer.ParseError) errors2.get(0)).getMessage(), errors2);
        }
        if (errors2.size() <= 1) {
            return new parsii.tokenizer.ParseException("An unknown error occured", errors2);
        }
        return new parsii.tokenizer.ParseException(java.lang.String.format("%d errors occured. First: %s", new java.lang.Object[]{java.lang.Integer.valueOf(errors2.size()), ((parsii.tokenizer.ParseError) errors2.get(0)).getMessage()}), errors2);
    }

    private ParseException(java.lang.String message, java.util.List<parsii.tokenizer.ParseError> errors2) {
        super(message);
        this.errors = errors2;
    }

    public java.util.List<parsii.tokenizer.ParseError> getErrors() {
        return this.errors;
    }

    public java.lang.String toString() {
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        for (parsii.tokenizer.ParseError error : this.errors) {
            if (sb.length() > 0) {
                sb.append("\n");
            }
            sb.append(error);
        }
        return sb.toString();
    }
}
