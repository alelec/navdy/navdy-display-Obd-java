package parsii.eval;

public interface Function {
    double eval(java.util.List<parsii.eval.Expression> list);

    int getNumberOfArguments();

    boolean isNaturalFunction();
}
