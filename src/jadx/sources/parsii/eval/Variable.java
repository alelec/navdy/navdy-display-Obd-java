package parsii.eval;

public class Variable {
    private boolean constant = false;
    private java.lang.String name;
    private double value = 0.0d;

    protected Variable(java.lang.String name2) {
        this.name = name2;
    }

    public void setValue(double value2) {
        if (this.constant) {
            throw new java.lang.IllegalStateException(java.lang.String.format("%s is constant!", new java.lang.Object[]{this.name}));
        } else {
            this.value = value2;
        }
    }

    public void makeConstant(double value2) {
        setValue(value2);
        this.constant = true;
    }

    public double getValue() {
        return this.value;
    }

    public java.lang.String toString() {
        return this.name + ": " + java.lang.String.valueOf(this.value);
    }

    public java.lang.String getName() {
        return this.name;
    }

    public boolean isConstant() {
        return this.constant;
    }

    public parsii.eval.Variable withValue(double value2) {
        setValue(value2);
        return this;
    }
}
