package parsii.eval;

public abstract class BinaryFunction implements parsii.eval.Function {
    /* access modifiers changed from: protected */
    public abstract double eval(double d, double d2);

    public int getNumberOfArguments() {
        return 2;
    }

    public double eval(java.util.List<parsii.eval.Expression> args) {
        double a = ((parsii.eval.Expression) args.get(0)).evaluate();
        if (java.lang.Double.isNaN(a)) {
            return a;
        }
        double b = ((parsii.eval.Expression) args.get(1)).evaluate();
        if (java.lang.Double.isNaN(b)) {
            return b;
        }
        return eval(a, b);
    }

    public boolean isNaturalFunction() {
        return true;
    }
}
