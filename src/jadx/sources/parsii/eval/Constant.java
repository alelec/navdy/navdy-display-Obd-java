package parsii.eval;

public class Constant extends parsii.eval.Expression {
    public static final parsii.eval.Constant EMPTY = new parsii.eval.Constant(Double.NaN);
    private double value;

    public Constant(double value2) {
        this.value = value2;
    }

    public double evaluate() {
        return this.value;
    }

    public boolean isConstant() {
        return true;
    }

    public java.lang.String toString() {
        return java.lang.String.valueOf(this.value);
    }
}
