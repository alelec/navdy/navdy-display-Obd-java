package parsii.eval;

public abstract class Expression {
    public abstract double evaluate();

    public parsii.eval.Expression simplify() {
        return this;
    }

    public boolean isConstant() {
        return false;
    }
}
