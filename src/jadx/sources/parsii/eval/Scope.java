package parsii.eval;

public class Scope {
    private static parsii.eval.Scope root;
    private java.util.Map<java.lang.String, parsii.eval.Variable> context = new java.util.concurrent.ConcurrentHashMap();
    private parsii.eval.Scope parent;

    private Scope() {
    }

    public static parsii.eval.Scope create() {
        parsii.eval.Scope result = new parsii.eval.Scope();
        result.parent = getRootScope();
        return result;
    }

    private static parsii.eval.Scope getRootScope() {
        if (root == null) {
            root = new parsii.eval.Scope();
            root.getVariable("pi").makeConstant(3.141592653589793d);
            root.getVariable("euler").makeConstant(2.718281828459045d);
        }
        return root;
    }

    public static parsii.eval.Scope createWithParent(parsii.eval.Scope parent2) {
        parsii.eval.Scope result = create();
        result.parent = parent2;
        return result;
    }

    public parsii.eval.Variable find(java.lang.String name) {
        if (this.context.containsKey(name)) {
            return (parsii.eval.Variable) this.context.get(name);
        }
        if (this.parent != null) {
            return this.parent.find(name);
        }
        return null;
    }

    public parsii.eval.Variable getVariable(java.lang.String name) {
        parsii.eval.Variable result = find(name);
        return result != null ? result : create(name);
    }

    public parsii.eval.Variable create(java.lang.String name) {
        if (this.context.containsKey(name)) {
            return (parsii.eval.Variable) this.context.get(name);
        }
        parsii.eval.Variable result = new parsii.eval.Variable(name);
        this.context.put(name, result);
        return result;
    }

    public java.util.Set<java.lang.String> getLocalNames() {
        return this.context.keySet();
    }

    public java.util.Set<java.lang.String> getNames() {
        if (this.parent == null) {
            return getLocalNames();
        }
        java.util.Set<java.lang.String> result = new java.util.TreeSet<>();
        result.addAll(this.parent.getNames());
        result.addAll(getLocalNames());
        return result;
    }

    public java.util.Collection<parsii.eval.Variable> getLocalVariables() {
        return this.context.values();
    }

    public java.util.Collection<parsii.eval.Variable> getVariables() {
        if (this.parent == null) {
            return getLocalVariables();
        }
        java.util.List<parsii.eval.Variable> result = new java.util.ArrayList<>();
        result.addAll(this.parent.getVariables());
        result.addAll(getLocalVariables());
        return result;
    }
}
