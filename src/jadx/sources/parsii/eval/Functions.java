package parsii.eval;

public class Functions {
    public static final parsii.eval.Function ABS = new parsii.eval.UnaryFunction() {
        /* access modifiers changed from: protected */
        public double eval(double a) {
            return java.lang.Math.abs(a);
        }
    };
    public static final parsii.eval.Function ACOS = new parsii.eval.UnaryFunction() {
        /* access modifiers changed from: protected */
        public double eval(double a) {
            return java.lang.Math.acos(a);
        }
    };
    public static final parsii.eval.Function ASIN = new parsii.eval.UnaryFunction() {
        /* access modifiers changed from: protected */
        public double eval(double a) {
            return java.lang.Math.asin(a);
        }
    };
    public static final parsii.eval.Function ATAN = new parsii.eval.UnaryFunction() {
        /* access modifiers changed from: protected */
        public double eval(double a) {
            return java.lang.Math.atan(a);
        }
    };
    public static final parsii.eval.Function ATAN2 = new parsii.eval.BinaryFunction() {
        /* access modifiers changed from: protected */
        public double eval(double a, double b) {
            return java.lang.Math.atan2(a, b);
        }
    };
    public static final parsii.eval.Function BIT_EXTRACT = new parsii.eval.BinaryFunction() {
        /* access modifiers changed from: protected */
        public double eval(double a, double b) {
            return (((int) a) & (1 << ((int) b))) > 0 ? 1.0d : 0.0d;
        }
    };
    public static final parsii.eval.Function CEIL = new parsii.eval.UnaryFunction() {
        /* access modifiers changed from: protected */
        public double eval(double a) {
            return java.lang.Math.ceil(a);
        }
    };
    public static final parsii.eval.Function COS = new parsii.eval.UnaryFunction() {
        /* access modifiers changed from: protected */
        public double eval(double a) {
            return java.lang.Math.cos(a);
        }
    };
    public static final parsii.eval.Function COSH = new parsii.eval.UnaryFunction() {
        /* access modifiers changed from: protected */
        public double eval(double a) {
            return java.lang.Math.cosh(a);
        }
    };
    public static final parsii.eval.Function DEG = new parsii.eval.UnaryFunction() {
        /* access modifiers changed from: protected */
        public double eval(double a) {
            return java.lang.Math.toDegrees(a);
        }
    };
    public static final parsii.eval.Function EXP = new parsii.eval.UnaryFunction() {
        /* access modifiers changed from: protected */
        public double eval(double a) {
            return java.lang.Math.exp(a);
        }
    };
    public static final parsii.eval.Function FLOOR = new parsii.eval.UnaryFunction() {
        /* access modifiers changed from: protected */
        public double eval(double a) {
            return java.lang.Math.floor(a);
        }
    };
    public static final parsii.eval.Function IF = new parsii.eval.Function() {
        public int getNumberOfArguments() {
            return 3;
        }

        public double eval(java.util.List<parsii.eval.Expression> args) {
            double check = ((parsii.eval.Expression) args.get(0)).evaluate();
            if (java.lang.Double.isNaN(check)) {
                return check;
            }
            if (check == 1.0d) {
                return ((parsii.eval.Expression) args.get(1)).evaluate();
            }
            return ((parsii.eval.Expression) args.get(2)).evaluate();
        }

        public boolean isNaturalFunction() {
            return false;
        }
    };
    public static final parsii.eval.Function LN = new parsii.eval.UnaryFunction() {
        /* access modifiers changed from: protected */
        public double eval(double a) {
            return java.lang.Math.log(a);
        }
    };
    public static final parsii.eval.Function LOG = new parsii.eval.UnaryFunction() {
        /* access modifiers changed from: protected */
        public double eval(double a) {
            return java.lang.Math.log10(a);
        }
    };
    public static final parsii.eval.Function MAX = new parsii.eval.BinaryFunction() {
        /* access modifiers changed from: protected */
        public double eval(double a, double b) {
            return java.lang.Math.max(a, b);
        }
    };
    public static final parsii.eval.Function MIN = new parsii.eval.BinaryFunction() {
        /* access modifiers changed from: protected */
        public double eval(double a, double b) {
            return java.lang.Math.min(a, b);
        }
    };
    public static final parsii.eval.Function RAD = new parsii.eval.UnaryFunction() {
        /* access modifiers changed from: protected */
        public double eval(double a) {
            return java.lang.Math.toRadians(a);
        }
    };
    public static final parsii.eval.Function RND = new parsii.eval.UnaryFunction() {
        /* access modifiers changed from: protected */
        public double eval(double a) {
            return java.lang.Math.random() * a;
        }
    };
    public static final parsii.eval.Function ROUND = new parsii.eval.UnaryFunction() {
        /* access modifiers changed from: protected */
        public double eval(double a) {
            return (double) java.lang.Math.round(a);
        }
    };
    public static final parsii.eval.Function SIGN = new parsii.eval.UnaryFunction() {
        /* access modifiers changed from: protected */
        public double eval(double a) {
            return java.lang.Math.signum(a);
        }
    };
    public static final parsii.eval.Function SIN = new parsii.eval.UnaryFunction() {
        /* access modifiers changed from: protected */
        public double eval(double a) {
            return java.lang.Math.sin(a);
        }
    };
    public static final parsii.eval.Function SINH = new parsii.eval.UnaryFunction() {
        /* access modifiers changed from: protected */
        public double eval(double a) {
            return java.lang.Math.sinh(a);
        }
    };
    public static final parsii.eval.Function SQRT = new parsii.eval.UnaryFunction() {
        /* access modifiers changed from: protected */
        public double eval(double a) {
            return java.lang.Math.sqrt(a);
        }
    };
    public static final parsii.eval.Function TAN = new parsii.eval.UnaryFunction() {
        /* access modifiers changed from: protected */
        public double eval(double a) {
            return java.lang.Math.tan(a);
        }
    };
    public static final parsii.eval.Function TANH = new parsii.eval.UnaryFunction() {
        /* access modifiers changed from: protected */
        public double eval(double a) {
            return java.lang.Math.tanh(a);
        }
    };
}
