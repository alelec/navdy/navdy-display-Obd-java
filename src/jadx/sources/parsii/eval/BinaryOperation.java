package parsii.eval;

public class BinaryOperation extends parsii.eval.Expression {
    public static final double EPSILON = 1.0E-10d;
    private parsii.eval.Expression left;
    private final parsii.eval.BinaryOperation.Op op;
    private parsii.eval.Expression right;
    private boolean sealed = false;

    public enum Op {
        ADD(3),
        SUBTRACT(3),
        MULTIPLY(4),
        DIVIDE(4),
        MODULO(4),
        POWER(5),
        LT(2),
        LT_EQ(2),
        EQ(2),
        GT_EQ(2),
        GT(2),
        NEQ(2),
        AND(1),
        OR(1);
        
        private final int priority;

        public int getPriority() {
            return this.priority;
        }

        private Op(int priority2) {
            this.priority = priority2;
        }
    }

    public BinaryOperation(parsii.eval.BinaryOperation.Op op2, parsii.eval.Expression left2, parsii.eval.Expression right2) {
        this.op = op2;
        this.left = left2;
        this.right = right2;
    }

    public parsii.eval.BinaryOperation.Op getOp() {
        return this.op;
    }

    public parsii.eval.Expression getLeft() {
        return this.left;
    }

    public void setLeft(parsii.eval.Expression left2) {
        this.left = left2;
    }

    public parsii.eval.Expression getRight() {
        return this.right;
    }

    public void seal() {
        this.sealed = true;
    }

    public boolean isSealed() {
        return this.sealed;
    }

    public double evaluate() {
        double d = 0.0d;
        double a = this.left.evaluate();
        double b = this.right.evaluate();
        if (this.op == parsii.eval.BinaryOperation.Op.ADD) {
            return a + b;
        }
        if (this.op == parsii.eval.BinaryOperation.Op.SUBTRACT) {
            return a - b;
        }
        if (this.op == parsii.eval.BinaryOperation.Op.MULTIPLY) {
            return a * b;
        }
        if (this.op == parsii.eval.BinaryOperation.Op.DIVIDE) {
            return a / b;
        }
        if (this.op == parsii.eval.BinaryOperation.Op.POWER) {
            return java.lang.Math.pow(a, b);
        }
        if (this.op == parsii.eval.BinaryOperation.Op.MODULO) {
            return a % b;
        }
        if (this.op == parsii.eval.BinaryOperation.Op.LT) {
            if (a >= b) {
                return 0.0d;
            }
            return 1.0d;
        } else if (this.op == parsii.eval.BinaryOperation.Op.LT_EQ) {
            if (a < b || java.lang.Math.abs(a - b) < 1.0E-10d) {
                d = 1.0d;
            }
            return d;
        } else if (this.op == parsii.eval.BinaryOperation.Op.GT) {
            if (a <= b) {
                return 0.0d;
            }
            return 1.0d;
        } else if (this.op == parsii.eval.BinaryOperation.Op.GT_EQ) {
            if (a > b || java.lang.Math.abs(a - b) < 1.0E-10d) {
                d = 1.0d;
            }
            return d;
        } else if (this.op == parsii.eval.BinaryOperation.Op.EQ) {
            if (java.lang.Math.abs(a - b) >= 1.0E-10d) {
                return 0.0d;
            }
            return 1.0d;
        } else if (this.op == parsii.eval.BinaryOperation.Op.NEQ) {
            if (java.lang.Math.abs(a - b) <= 1.0E-10d) {
                return 0.0d;
            }
            return 1.0d;
        } else if (this.op == parsii.eval.BinaryOperation.Op.AND) {
            if (a == 1.0d && b == 1.0d) {
                return 1.0d;
            }
            return 0.0d;
        } else if (this.op == parsii.eval.BinaryOperation.Op.OR) {
            if (a == 1.0d || b == 1.0d) {
                d = 1.0d;
            }
            return d;
        } else {
            throw new java.lang.UnsupportedOperationException(java.lang.String.valueOf(this.op));
        }
    }

    public parsii.eval.Expression simplify() {
        this.left = this.left.simplify();
        this.right = this.right.simplify();
        if (this.left.isConstant() && this.right.isConstant()) {
            return new parsii.eval.Constant(evaluate());
        }
        if (this.op == parsii.eval.BinaryOperation.Op.ADD || this.op == parsii.eval.BinaryOperation.Op.MULTIPLY) {
            if (this.right.isConstant()) {
                parsii.eval.Expression tmp = this.right;
                this.right = this.left;
                this.left = tmp;
            }
            if (this.right instanceof parsii.eval.BinaryOperation) {
                parsii.eval.BinaryOperation childOp = (parsii.eval.BinaryOperation) this.right;
                if (this.op == childOp.op) {
                    if (this.left.isConstant()) {
                        if (childOp.left.isConstant()) {
                            if (this.op == parsii.eval.BinaryOperation.Op.ADD) {
                                return new parsii.eval.BinaryOperation(this.op, new parsii.eval.Constant(this.left.evaluate() + childOp.left.evaluate()), childOp.right);
                            }
                            if (this.op == parsii.eval.BinaryOperation.Op.MULTIPLY) {
                                return new parsii.eval.BinaryOperation(this.op, new parsii.eval.Constant(this.left.evaluate() * childOp.left.evaluate()), childOp.right);
                            }
                        }
                    } else if (childOp.left.isConstant()) {
                        return new parsii.eval.BinaryOperation(this.op, childOp.left, new parsii.eval.BinaryOperation(this.op, this.left, childOp.right));
                    }
                }
            }
        }
        return super.simplify();
    }

    public java.lang.String toString() {
        return "(" + this.left.toString() + " " + this.op + " " + this.right + ")";
    }
}
