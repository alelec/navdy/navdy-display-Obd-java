package parsii.eval;

public abstract class UnaryFunction implements parsii.eval.Function {
    /* access modifiers changed from: protected */
    public abstract double eval(double d);

    public int getNumberOfArguments() {
        return 1;
    }

    public double eval(java.util.List<parsii.eval.Expression> args) {
        double a = ((parsii.eval.Expression) args.get(0)).evaluate();
        return java.lang.Double.isNaN(a) ? a : eval(a);
    }

    public boolean isNaturalFunction() {
        return true;
    }
}
