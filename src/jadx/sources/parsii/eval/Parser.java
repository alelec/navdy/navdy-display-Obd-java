package parsii.eval;

public class Parser {
    private static java.util.Map<java.lang.String, parsii.eval.Function> functionTable = new java.util.TreeMap();
    private java.util.List<parsii.tokenizer.ParseError> errors = new java.util.ArrayList();
    private final parsii.eval.Scope scope;
    private parsii.tokenizer.Tokenizer tokenizer;

    public static void registerFunction(java.lang.String name, parsii.eval.Function function) {
        functionTable.put(name, function);
    }

    static {
        registerFunction("sin", parsii.eval.Functions.SIN);
        registerFunction("cos", parsii.eval.Functions.COS);
        registerFunction("tan", parsii.eval.Functions.TAN);
        registerFunction("sinh", parsii.eval.Functions.SINH);
        registerFunction("cosh", parsii.eval.Functions.COSH);
        registerFunction("tanh", parsii.eval.Functions.TANH);
        registerFunction("asin", parsii.eval.Functions.ASIN);
        registerFunction("acos", parsii.eval.Functions.ACOS);
        registerFunction("atan", parsii.eval.Functions.ATAN);
        registerFunction("atan2", parsii.eval.Functions.ATAN2);
        registerFunction("deg", parsii.eval.Functions.DEG);
        registerFunction("rad", parsii.eval.Functions.RAD);
        registerFunction("abs", parsii.eval.Functions.ABS);
        registerFunction("round", parsii.eval.Functions.ROUND);
        registerFunction("ceil", parsii.eval.Functions.CEIL);
        registerFunction("floor", parsii.eval.Functions.FLOOR);
        registerFunction("exp", parsii.eval.Functions.EXP);
        registerFunction("ln", parsii.eval.Functions.LN);
        registerFunction("log", parsii.eval.Functions.LOG);
        registerFunction("sqrt", parsii.eval.Functions.SQRT);
        registerFunction("min", parsii.eval.Functions.MIN);
        registerFunction("max", parsii.eval.Functions.MAX);
        registerFunction("rnd", parsii.eval.Functions.RND);
        registerFunction("sign", parsii.eval.Functions.SIGN);
        registerFunction("if", parsii.eval.Functions.IF);
    }

    public static parsii.eval.Expression parse(java.lang.String input) throws parsii.tokenizer.ParseException {
        return new parsii.eval.Parser(new java.io.StringReader(input), parsii.eval.Scope.create()).parse();
    }

    public static parsii.eval.Expression parse(java.io.Reader input) throws parsii.tokenizer.ParseException {
        return new parsii.eval.Parser(input, parsii.eval.Scope.create()).parse();
    }

    public static parsii.eval.Expression parse(java.lang.String input, parsii.eval.Scope scope2) throws parsii.tokenizer.ParseException {
        return new parsii.eval.Parser(new java.io.StringReader(input), scope2).parse();
    }

    public static parsii.eval.Expression parse(java.io.Reader input, parsii.eval.Scope scope2) throws parsii.tokenizer.ParseException {
        return new parsii.eval.Parser(input, scope2).parse();
    }

    private Parser(java.io.Reader input, parsii.eval.Scope scope2) {
        this.scope = scope2;
        this.tokenizer = new parsii.tokenizer.Tokenizer(input);
        this.tokenizer.setProblemCollector(this.errors);
    }

    /* access modifiers changed from: protected */
    public parsii.eval.Expression parse() throws parsii.tokenizer.ParseException {
        parsii.eval.Expression result = expression().simplify();
        if (((parsii.tokenizer.Token) this.tokenizer.current()).isNotEnd()) {
            parsii.tokenizer.Token token = (parsii.tokenizer.Token) this.tokenizer.consume();
            this.errors.add(parsii.tokenizer.ParseError.error(token, java.lang.String.format("Unexpected token: '%s'. Expected an expression.", new java.lang.Object[]{token.getSource()})));
        }
        if (this.errors.size() <= 0) {
            return result;
        }
        throw parsii.tokenizer.ParseException.create(this.errors);
    }

    /* access modifiers changed from: protected */
    public parsii.eval.Expression expression() {
        parsii.eval.Expression left = relationalExpression();
        if (((parsii.tokenizer.Token) this.tokenizer.current()).isSymbol("&&")) {
            this.tokenizer.consume();
            return reOrder(left, expression(), parsii.eval.BinaryOperation.Op.AND);
        }
        if (!((parsii.tokenizer.Token) this.tokenizer.current()).isSymbol("||")) {
            return left;
        }
        this.tokenizer.consume();
        return reOrder(left, expression(), parsii.eval.BinaryOperation.Op.OR);
    }

    /* access modifiers changed from: protected */
    public parsii.eval.Expression relationalExpression() {
        parsii.eval.Expression left = term();
        if (((parsii.tokenizer.Token) this.tokenizer.current()).isSymbol("<")) {
            this.tokenizer.consume();
            return reOrder(left, relationalExpression(), parsii.eval.BinaryOperation.Op.LT);
        }
        if (((parsii.tokenizer.Token) this.tokenizer.current()).isSymbol("<=")) {
            this.tokenizer.consume();
            return reOrder(left, relationalExpression(), parsii.eval.BinaryOperation.Op.LT_EQ);
        }
        if (((parsii.tokenizer.Token) this.tokenizer.current()).isSymbol("=")) {
            this.tokenizer.consume();
            return reOrder(left, relationalExpression(), parsii.eval.BinaryOperation.Op.EQ);
        }
        if (((parsii.tokenizer.Token) this.tokenizer.current()).isSymbol(">=")) {
            this.tokenizer.consume();
            return reOrder(left, relationalExpression(), parsii.eval.BinaryOperation.Op.GT_EQ);
        }
        if (((parsii.tokenizer.Token) this.tokenizer.current()).isSymbol(">")) {
            this.tokenizer.consume();
            return reOrder(left, relationalExpression(), parsii.eval.BinaryOperation.Op.GT);
        }
        if (!((parsii.tokenizer.Token) this.tokenizer.current()).isSymbol("!=")) {
            return left;
        }
        this.tokenizer.consume();
        return reOrder(left, relationalExpression(), parsii.eval.BinaryOperation.Op.NEQ);
    }

    /* access modifiers changed from: protected */
    public parsii.eval.Expression term() {
        parsii.eval.Expression left = product();
        if (((parsii.tokenizer.Token) this.tokenizer.current()).isSymbol(org.slf4j.Marker.ANY_NON_NULL_MARKER)) {
            this.tokenizer.consume();
            return reOrder(left, term(), parsii.eval.BinaryOperation.Op.ADD);
        }
        if (((parsii.tokenizer.Token) this.tokenizer.current()).isSymbol("-")) {
            this.tokenizer.consume();
            return reOrder(left, term(), parsii.eval.BinaryOperation.Op.SUBTRACT);
        } else if (!((parsii.tokenizer.Token) this.tokenizer.current()).isNumber() || !((parsii.tokenizer.Token) this.tokenizer.current()).getContents().startsWith("-")) {
            return left;
        } else {
            return reOrder(left, term(), parsii.eval.BinaryOperation.Op.ADD);
        }
    }

    /* access modifiers changed from: protected */
    public parsii.eval.Expression product() {
        parsii.eval.Expression left = power();
        if (((parsii.tokenizer.Token) this.tokenizer.current()).isSymbol(org.slf4j.Marker.ANY_MARKER)) {
            this.tokenizer.consume();
            return reOrder(left, product(), parsii.eval.BinaryOperation.Op.MULTIPLY);
        }
        if (((parsii.tokenizer.Token) this.tokenizer.current()).isSymbol("/")) {
            this.tokenizer.consume();
            return reOrder(left, product(), parsii.eval.BinaryOperation.Op.DIVIDE);
        }
        if (!((parsii.tokenizer.Token) this.tokenizer.current()).isSymbol("%")) {
            return left;
        }
        this.tokenizer.consume();
        return reOrder(left, product(), parsii.eval.BinaryOperation.Op.MODULO);
    }

    private parsii.eval.Expression reOrder(parsii.eval.Expression left, parsii.eval.Expression right, parsii.eval.BinaryOperation.Op op) {
        if (right instanceof parsii.eval.BinaryOperation) {
            parsii.eval.BinaryOperation rightOp = (parsii.eval.BinaryOperation) right;
            if (!rightOp.isSealed() && rightOp.getOp().getPriority() == op.getPriority()) {
                replaceLeft(rightOp, left, op);
                return right;
            }
        }
        return new parsii.eval.BinaryOperation(op, left, right);
    }

    private void replaceLeft(parsii.eval.BinaryOperation target, parsii.eval.Expression newLeft, parsii.eval.BinaryOperation.Op op) {
        if (target.getLeft() instanceof parsii.eval.BinaryOperation) {
            parsii.eval.BinaryOperation leftOp = (parsii.eval.BinaryOperation) target.getLeft();
            if (!leftOp.isSealed() && leftOp.getOp().getPriority() == op.getPriority()) {
                replaceLeft(leftOp, newLeft, op);
                return;
            }
        }
        target.setLeft(new parsii.eval.BinaryOperation(op, newLeft, target.getLeft()));
    }

    /* access modifiers changed from: protected */
    public parsii.eval.Expression power() {
        parsii.eval.Expression left = atom();
        if (!((parsii.tokenizer.Token) this.tokenizer.current()).isSymbol("^")) {
            if (!((parsii.tokenizer.Token) this.tokenizer.current()).isSymbol("**")) {
                return left;
            }
        }
        this.tokenizer.consume();
        return reOrder(left, power(), parsii.eval.BinaryOperation.Op.POWER);
    }

    /* access modifiers changed from: protected */
    public parsii.eval.Expression atom() {
        if (((parsii.tokenizer.Token) this.tokenizer.current()).isSymbol("-")) {
            this.tokenizer.consume();
            parsii.eval.BinaryOperation result = new parsii.eval.BinaryOperation(parsii.eval.BinaryOperation.Op.SUBTRACT, new parsii.eval.Constant(0.0d), atom());
            result.seal();
            return result;
        }
        if (((parsii.tokenizer.Token) this.tokenizer.current()).isSymbol("(")) {
            this.tokenizer.consume();
            parsii.eval.Expression result2 = expression();
            if (result2 instanceof parsii.eval.BinaryOperation) {
                ((parsii.eval.BinaryOperation) result2).seal();
            }
            expect(parsii.tokenizer.Token.TokenType.SYMBOL, ")");
            return result2;
        }
        if (((parsii.tokenizer.Token) this.tokenizer.current()).isSymbol("{")) {
            this.tokenizer.consume();
            parsii.eval.FunctionCall call = new parsii.eval.FunctionCall();
            call.setFunction(parsii.eval.Functions.BIT_EXTRACT);
            if (((parsii.tokenizer.Token) this.tokenizer.current()).isIdentifier(new java.lang.String[0])) {
                call.addParameter(new parsii.eval.VariableReference(this.scope.getVariable(((parsii.tokenizer.Token) this.tokenizer.consume()).getContents())));
            } else {
                parsii.tokenizer.Token token = (parsii.tokenizer.Token) this.tokenizer.consume();
                this.errors.add(parsii.tokenizer.ParseError.error(token, java.lang.String.format("Unexpected token: '%s'. Expected a valid identifier.", new java.lang.Object[]{token.getSource()})));
            }
            expect(parsii.tokenizer.Token.TokenType.SYMBOL, ":");
            if (((parsii.tokenizer.Token) this.tokenizer.current()).isNumber()) {
                call.addParameter(new parsii.eval.Constant(java.lang.Double.parseDouble(((parsii.tokenizer.Token) this.tokenizer.consume()).getContents())));
            } else {
                parsii.tokenizer.Token token2 = (parsii.tokenizer.Token) this.tokenizer.consume();
                this.errors.add(parsii.tokenizer.ParseError.error(token2, java.lang.String.format("Unexpected token: '%s'. Expected a bit offset (number).", new java.lang.Object[]{token2.getSource()})));
            }
            expect(parsii.tokenizer.Token.TokenType.SYMBOL, "}");
            return call;
        }
        if (((parsii.tokenizer.Token) this.tokenizer.current()).isSymbol("|")) {
            this.tokenizer.consume();
            parsii.eval.FunctionCall call2 = new parsii.eval.FunctionCall();
            call2.addParameter(expression());
            call2.setFunction(parsii.eval.Functions.ABS);
            expect(parsii.tokenizer.Token.TokenType.SYMBOL, "|");
            return call2;
        } else if (((parsii.tokenizer.Token) this.tokenizer.current()).isIdentifier(new java.lang.String[0])) {
            if (((parsii.tokenizer.Token) this.tokenizer.next()).isSymbol("(")) {
                return functionCall();
            }
            return new parsii.eval.VariableReference(this.scope.getVariable(((parsii.tokenizer.Token) this.tokenizer.consume()).getContents()));
        } else if (((parsii.tokenizer.Token) this.tokenizer.current()).isNumber()) {
            double value = java.lang.Double.parseDouble(((parsii.tokenizer.Token) this.tokenizer.consume()).getContents());
            if (((parsii.tokenizer.Token) this.tokenizer.current()).is(parsii.tokenizer.Token.TokenType.ID)) {
                java.lang.String quantifier = ((parsii.tokenizer.Token) this.tokenizer.current()).getContents().intern();
                if ("n" == quantifier) {
                    value /= 1.0E9d;
                    this.tokenizer.consume();
                } else if ("u" == quantifier) {
                    value /= 1000000.0d;
                    this.tokenizer.consume();
                } else if (ch.qos.logback.core.pattern.color.ANSIConstants.ESC_END == quantifier) {
                    value /= 1000.0d;
                    this.tokenizer.consume();
                } else if ("K" == quantifier || "k" == quantifier) {
                    value *= 1000.0d;
                    this.tokenizer.consume();
                } else if ("M" == quantifier) {
                    value *= 1000000.0d;
                    this.tokenizer.consume();
                } else if ("G" == quantifier) {
                    value *= 1.0E9d;
                    this.tokenizer.consume();
                } else {
                    parsii.tokenizer.Token token3 = (parsii.tokenizer.Token) this.tokenizer.consume();
                    this.errors.add(parsii.tokenizer.ParseError.error(token3, java.lang.String.format("Unexpected token: '%s'. Expected a valid quantifier.", new java.lang.Object[]{token3.getSource()})));
                }
            }
            return new parsii.eval.Constant(value);
        } else {
            parsii.tokenizer.Token token4 = (parsii.tokenizer.Token) this.tokenizer.consume();
            this.errors.add(parsii.tokenizer.ParseError.error(token4, java.lang.String.format("Unexpected token: '%s'. Expected an expression.", new java.lang.Object[]{token4.getSource()})));
            return parsii.eval.Constant.EMPTY;
        }
    }

    /* access modifiers changed from: protected */
    public parsii.eval.Expression functionCall() {
        parsii.eval.FunctionCall call = new parsii.eval.FunctionCall();
        parsii.tokenizer.Token funToken = (parsii.tokenizer.Token) this.tokenizer.consume();
        parsii.eval.Function fun = (parsii.eval.Function) functionTable.get(funToken.getContents());
        if (fun == null) {
            this.errors.add(parsii.tokenizer.ParseError.error(funToken, java.lang.String.format("Unknown function: '%s'", new java.lang.Object[]{funToken.getContents()})));
        }
        call.setFunction(fun);
        this.tokenizer.consume();
        while (true) {
            if (((parsii.tokenizer.Token) this.tokenizer.current()).isSymbol(")") || !((parsii.tokenizer.Token) this.tokenizer.current()).isNotEnd()) {
                expect(parsii.tokenizer.Token.TokenType.SYMBOL, ")");
            } else {
                if (!call.getParameters().isEmpty()) {
                    expect(parsii.tokenizer.Token.TokenType.SYMBOL, ",");
                }
                call.addParameter(expression());
            }
        }
        expect(parsii.tokenizer.Token.TokenType.SYMBOL, ")");
        if (fun == null) {
            return parsii.eval.Constant.EMPTY;
        }
        if (call.getParameters().size() == fun.getNumberOfArguments() || fun.getNumberOfArguments() < 0) {
            return call;
        }
        this.errors.add(parsii.tokenizer.ParseError.error(funToken, java.lang.String.format("Number of arguments for function '%s' do not match. Expected: %d, Found: %d", new java.lang.Object[]{funToken.getContents(), java.lang.Integer.valueOf(fun.getNumberOfArguments()), java.lang.Integer.valueOf(call.getParameters().size())})));
        return parsii.eval.Constant.EMPTY;
    }

    /* access modifiers changed from: protected */
    public void expect(parsii.tokenizer.Token.TokenType type, java.lang.String trigger) {
        if (((parsii.tokenizer.Token) this.tokenizer.current()).matches(type, trigger)) {
            this.tokenizer.consume();
            return;
        }
        this.errors.add(parsii.tokenizer.ParseError.error((parsii.tokenizer.Position) this.tokenizer.current(), java.lang.String.format("Unexpected token '%s'. Expected: '%s'", new java.lang.Object[]{((parsii.tokenizer.Token) this.tokenizer.current()).getSource(), trigger})));
    }
}
