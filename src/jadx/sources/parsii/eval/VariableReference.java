package parsii.eval;

public class VariableReference extends parsii.eval.Expression {
    private parsii.eval.Variable var;

    public VariableReference(parsii.eval.Variable var2) {
        this.var = var2;
    }

    public double evaluate() {
        return this.var.getValue();
    }

    public java.lang.String toString() {
        return this.var.getName();
    }

    public boolean isConstant() {
        return this.var.isConstant();
    }

    public parsii.eval.Expression simplify() {
        if (isConstant()) {
            return new parsii.eval.Constant(evaluate());
        }
        return this;
    }
}
