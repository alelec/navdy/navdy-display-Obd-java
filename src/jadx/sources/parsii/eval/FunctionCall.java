package parsii.eval;

public class FunctionCall extends parsii.eval.Expression {
    private parsii.eval.Function function;
    private java.util.List<parsii.eval.Expression> parameters = new java.util.ArrayList();

    public double evaluate() {
        return this.function.eval(this.parameters);
    }

    /* Debug info: failed to restart local var, previous not found, register: 6 */
    public parsii.eval.Expression simplify() {
        if (!this.function.isNaturalFunction()) {
            return this;
        }
        for (parsii.eval.Expression expr : this.parameters) {
            if (!expr.isConstant()) {
                return this;
            }
        }
        return new parsii.eval.Constant(evaluate());
    }

    public void setFunction(parsii.eval.Function function2) {
        this.function = function2;
    }

    public void addParameter(parsii.eval.Expression expression) {
        this.parameters.add(expression);
    }

    public java.util.List<parsii.eval.Expression> getParameters() {
        return this.parameters;
    }
}
