package com.navdy.obd;

import java.util.List;

import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit(staticConstructorAction = DexAction.APPEND)
public class DefaultPidProcessorFactory implements PidProcessorFactory {
    @DexIgnore
    private static List<Integer> PIDS_WITH_PRE_PROCESSORS; // = new java.util.ArrayList();

    /* @DexIgnore
    static {
        PIDS_WITH_PRE_PROCESSORS.add(java.lang.Integer.valueOf(Pids.INSTANTANEOUS_FUEL_CONSUMPTION));
        PIDS_WITH_PRE_PROCESSORS.add(java.lang.Integer.valueOf(Pids.FUEL_LEVEL));
        PIDS_WITH_PRE_PROCESSORS.add(java.lang.Integer.valueOf(Pids.VEHICLE_SPEED));
    } */
    static {
        PIDS_WITH_PRE_PROCESSORS.add(Pids.AMBIENT_AIR_TEMRERATURE);
        PIDS_WITH_PRE_PROCESSORS.add(Pids.ENGINE_COOLANT_TEMPERATURE);
        PIDS_WITH_PRE_PROCESSORS.add(Pids.ENGINE_OIL_TEMRERATURE);
    }

    @DexReplace
    public PidProcessor buildPidProcessorForPid(int pid) {
        switch (pid) {
            case Pids.VEHICLE_SPEED:
                return new SpeedPidProcessor();
            case Pids.AMBIENT_AIR_TEMRERATURE:
                return new AmbientTempPidProcessor();
            case Pids.ENGINE_OIL_TEMRERATURE:
                return new EngineOilTempPidProcessor();
            case Pids.ENGINE_COOLANT_TEMPERATURE:
                return new EngineCoolantPidProcessor();
            case Pids.FUEL_LEVEL:
                return new FuelLevelPidProcessor();
            case Pids.INSTANTANEOUS_FUEL_CONSUMPTION:
                return new InstantFuelConsumptionPidProcessor();
            default:
                return null;
        }
    }

    @DexIgnore
    public List<Integer> getPidsHavingProcessors() {
        return PIDS_WITH_PRE_PROCESSORS;
    }
}
