package com.navdy.obd.command;

import com.navdy.obd.EcuResponse;
import com.navdy.obd.Protocol;
import com.navdy.obd.ResponseParser;
import com.navdy.obd.converters.AbstractConversion;
import com.navdy.obd.io.ObdCommandInterpreter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit(staticConstructorAction = DexAction.APPEND, defaultAction = DexAction.IGNORE)
public class ObdCommand implements ICommand {
    @DexIgnore
    public static /* final */ ObdCommand ALLOW_LONG_MESSAGES; // = new com.navdy.obd.command.ObdCommand("ATAL");
    @DexIgnore
    public static /* final */ int ANY_ECU; // = -1;
    @DexIgnore
    public static /* final */ ObdCommand AUTO_PROTOCOL_COMMAND; // = new com.navdy.obd.command.ObdCommand("SP", "atsp A6");
    @DexIgnore
    private static /* final */ int CARRIAGE_RETURN; // = 13;
    @DexIgnore
    public static /* final */ ObdCommand CLEAR_ALL_PASS_FILTERS_COMMAND; // = new com.navdy.obd.command.ObdCommand("STFCP");
    @DexIgnore
    public static /* final */ ObdCommand DETECT_PROTOCOL_COMMAND; // = new com.navdy.obd.command.ObdCommand("Detect", "0100");
    @DexIgnore
    public static /* final */ ObdCommand DEVICE_INFO_COMMAND; // = new com.navdy.obd.command.ObdCommand("ATI", "ATI");
    @DexIgnore
    public static /* final */ ObdCommand ECHO_OFF_COMMAND; // = new com.navdy.obd.command.ObdCommand("ECHO OFF", "ate0");
    @DexIgnore
    public static /* final */ ObdCommand HEADERS_OFF_COMMAND; // = new com.navdy.obd.command.ObdCommand("HDR OFF", "ath0");
    @DexIgnore
    public static /* final */ ObdCommand HEADERS_ON_COMMAND; // = new com.navdy.obd.command.ObdCommand("HDR ON", "ath1");
    @DexIgnore
    static /* final */ Logger Log; // = org.slf4j.LoggerFactory.getLogger(com.navdy.obd.command.ObdCommand.class);
    @DexIgnore
    public static /* final */ long NANOS_PER_MS; // = 1000000;
    @DexIgnore
    public static /* final */ int NO_DATA; // = Integer.MIN_VALUE;
    @DexIgnore
    public static /* final */ int NO_FILTER; // = 0;
    @DexIgnore
    public static /* final */ String OK; // = "OK";
    @DexIgnore
    public static /* final */ ObdCommand READ_PROTOCOL_COMMAND; // = new com.navdy.obd.command.ObdCommand("DP", "atdpn");
    @DexIgnore
    public static /* final */ ObdCommand RESET_COMMAND; // = new com.navdy.obd.command.ObdCommand("INIT", "atws");
    @DexIgnore
    public static /* final */ ObdCommand SCAN_COMMAND; // = new com.navdy.obd.command.ObdCommand("SCAN", "atma");
    @DexIgnore
    public static /* final */ ObdCommand SLEEP_COMMAND; // = new com.navdy.obd.command.ObdCommand("STSLEEP");
    @DexIgnore
    public static /* final */ ObdCommand SPACES_OFF_COMMAND; // = new com.navdy.obd.command.ObdCommand("SPACE", "ats0");
    @DexIgnore
    public static /* final */ long TIMEOUT_MILLS; // = 10000;
    @DexIgnore
    public static /* final */ ObdCommand TURN_OFF_FORMATTING_COMMAND; // = new com.navdy.obd.command.ObdCommand("CAN Formatting OFF", "ATCAF0");
    @DexIgnore
    public static /* final */ ObdCommand TURN_OFF_VOLTAGE_DELTA_WAKEUP; // = new com.navdy.obd.command.ObdCommand("STSLVG off");
    @DexIgnore
    public static /* final */ ObdCommand TURN_OFF_VOLTAGE_LEVEL_WAKEUP; // = new com.navdy.obd.command.ObdCommand("STSLVL off,off");
    @DexIgnore
    public static /* final */ ObdCommand TURN_ON_FORMATTING_COMMAND; // = new com.navdy.obd.command.ObdCommand("CAN Formatting OFF", "ATCAF1");
    @DexIgnore
    public static /* final */ ObdCommand TURN_ON_VOLTAGE_DELTA_WAKEUP; // = new com.navdy.obd.command.ObdCommand("STSLVG on");
    @DexIgnore
    public static /* final */ ObdCommand TURN_ON_VOLTAGE_LEVEL_WAKEUP; // = new com.navdy.obd.command.ObdCommand("STSLVL off,on");
    @DexIgnore
    public static /* final */ int UNKNOWN; // = -1;
    @DexIgnore
    public static /* final */ String WAIT; // = "WAIT";
    @DexIgnore
    private static ExecutorService singleThreadedExecutor; // = java.util.concurrent.Executors.newSingleThreadExecutor();
    @DexIgnore
    private String command;
    @DexIgnore
    private byte[] commandBytes;
    @DexIgnore
    private AbstractConversion conversion;
    @DexIgnore
    private IOException error;
    @DexIgnore
    private int expectedResponses;
    @DexIgnore
    private String name;
    @DexIgnore
    private String response;
    @DexIgnore
    private EcuResponse[] responses;
    @DexIgnore
    private int targetEcu;
    @DexIgnore
    private long timeoutMillis;
    @DexIgnore
    private long timeoutNanos;

    @DexAdd
    static final Logger LogRaw = LoggerFactory.getLogger("ObdRaw");
    @DexAdd
    public static boolean enableLogRaw = false;

    @DexAdd
    static final Map<Integer, String> ObdErrors = createErrors();
    @DexAdd
    private static Map<Integer, String> createErrors() {
        Map<Integer, String> result = new HashMap<>();
        result.put(0x10, "generalReject");
        result.put(0x11, "serviceNotSupported");
        result.put(0x12, "subFunctionNotSupported-invalidFormat");
        result.put(0x21, "busy-RepeatRequest");
        result.put(0x22, "conditionsNotCorrect or requestSequenceError");
        result.put(0x23, "routineNotComplete");
        result.put(0x31, "requestOutOfRange");
        result.put(0x33, "securityAccessDenied");
        result.put(0x35, "invalidKey");
        result.put(0x36, "exceedNumberOfAttempts");
        result.put(0x37, "requiredTimeDelayNotExpired");
        result.put(0x40, "downloadNotAccepted");
        result.put(0x41, "improperDownloadType");
        result.put(0x42, "can'tDownloadToSpecifiedAddress");
        result.put(0x43, "can'tDownloadNumberOfBytesRequested");
        result.put(0x50, "uploadNotAccepted");
        result.put(0x51, "improperUploadType");
        result.put(0x52, "can'tUploadFromSpecifiedAddress");
        result.put(0x53, "can'tUploadNumberOfBytesRequested");
        result.put(0x71, "transferSuspended");
        result.put(0x72, "transferAborted");
        result.put(0x74, "illegalAddressInBlockTransfer");
        result.put(0x75, "illegalByteCountInBlockTransfer");
        result.put(0x76, "illegalBlockTransferType");
        result.put(0x77, "blockTransferDataChecksumError");
        result.put(0x78, "requestCorrectlyReceived-ResponsePending");
        result.put(0x79, "incorrectByteCountDuringBlockTransfer");
        return result;
    }

    // @DexIgnore
    // class Anon1 implements Runnable {
    //     final /* synthetic */ IObdDataObserver val$commandObserver;
    //     final /* synthetic */ InputStream val$input;
    //     final /* synthetic */ OutputStream val$output;
    //     final /* synthetic */ Protocol val$protocol;
    //
    //     @DexIgnore
    //     Anon1(InputStream inputStream, OutputStream outputStream, IObdDataObserver iObdDataObserver, Protocol protocol) {
    //         this.val$input = inputStream;
    //         this.val$output = outputStream;
    //         this.val$commandObserver = iObdDataObserver;
    //         this.val$protocol = protocol;
    //     }
    //
    //     @DexIgnore
    //     public void run() {
    //         try {
    //             ObdCommandInterpreter obdCommandInterpreter = new ObdCommandInterpreter(this.val$input, this.val$output);
    //             ObdCommand.this.responses = null;
    //             ObdCommand.this.response = null;
    //             if (ObdCommand.this.commandBytes == null) {
    //                 obdCommandInterpreter.write(ObdCommand.this.command + (ObdCommand.this.expectedResponses != -1 ? Character.valueOf((char) (ObdCommand.this.expectedResponses + 48)) : ""));
    //                 ObdCommand.this.commandBytes = obdCommandInterpreter.getCommandBytes();
    //             } else {
    //                 obdCommandInterpreter.write(ObdCommand.this.commandBytes);
    //             }
    //             ObdCommand.Log.debug("wrote: {}{} ", ObdCommand.this.command, ObdCommand.this.expectedResponses != -1 ? Integer.valueOf(ObdCommand.this.expectedResponses) : "");
    //             if (this.val$commandObserver != null) {
    //                 this.val$commandObserver.onCommand(new String(ObdCommand.this.commandBytes));
    //             }
    //             ObdCommand.this.processCommandResponse(this.val$protocol, obdCommandInterpreter, this.val$commandObserver);
    //         } catch (IOException ie) {
    //             ObdCommand.this.error = ie;
    //             if (this.val$commandObserver != null) {
    //                 this.val$commandObserver.onError(ie.toString());
    //             }
    //         }
    //     }
    // }

    @DexIgnore
    public static ObdCommand createSetVoltageLevelWakeupTriggerCommand(boolean below, float voltage, int seconds) {
        return new ObdCommand("STSLVLW " + (below ? "<" : ">") + voltage + ", " + seconds);
    }

    @DexIgnore
    public ObdCommand(String name2, String command2, AbstractConversion conversion2, long timeOut) {
        this(name2, command2, conversion2);
        this.timeoutMillis = timeOut;
        this.timeoutNanos = 1000000 * timeOut;
    }

    @DexIgnore
    public ObdCommand(String name2, String command2, AbstractConversion conversion2) {
        this.expectedResponses = -1;
        this.targetEcu = -1;
        this.timeoutMillis = 10000;
        this.timeoutNanos = 10000000000L;
        this.name = name2;
        this.conversion = conversion2;
        setCommand(command2);
    }

    @DexIgnore
    public ObdCommand(String command) {
        this(null, null, null);
        String[] scom = command.split(":");
        String pid;
        if (scom.length == 1) {
            this.name = pid = command.trim();
        } else {
            this.name = scom[0].trim();
            pid = scom[1].trim(); 
        }
        setCommand(pid);
    }

    @DexIgnore
    public ObdCommand(String name2, String command2) {
        this(name2, command2, null);
    }

    @DexIgnore
    public void setCommand(String command2) {
        this.command = command2;
        this.commandBytes = null;
    }

    @DexIgnore
    public ObdCommand clone() {
        return new ObdCommand(this.name, this.command, this.conversion, this.timeoutMillis);
    }

    @DexIgnore
    public String getCommand() {
        return this.command;
    }

    @DexIgnore
    public void setExpectedResponses(int count) {
        if (count > 9 || count == 0 || count < -1) {
            throw new IllegalArgumentException("invalid expected count " + count);
        }
        this.expectedResponses = count;
        this.commandBytes = null;
    }

    @DexIgnore
    public void setTargetEcu(int ecuAddress) {
        if (ecuAddress != this.targetEcu) {
            this.targetEcu = ecuAddress;
        }
    }

    @DexIgnore
    public int getTargetEcu() {
        return this.targetEcu;
    }

    @DexIgnore
    public String getName() {
        return this.name;
    }

    @DexIgnore
    public byte[] getByteResponse() {
        return getByteResponse(0);
    }

    @DexIgnore
    public byte[] getByteResponse(int filter) {
        return getEcuResponse(this.targetEcu, filter);
    }

    @DexIgnore
    public byte[] getEcuResponse(int ecu) {
        return getEcuResponse(ecu, 0);
    }

    @DexIgnore
    public byte[] getEcuResponse(int ecu, int filter) {
        if (this.responses == null) {
            return null;
        }
        for (EcuResponse response2 : this.responses) {
            if (ecu == -1 || response2.ecu == ecu) {
                byte[] data = response2.data;
                if (filter == 0) {
                    return data;
                }
                if (data != null && data[0] == filter) {
                    return data;
                }
            }
        }
        return null;
    }

    @DexIgnore
    public EcuResponse getResponse(int i) {
        return this.responses[i];
    }

    @DexIgnore
    public int getResponseCount() {
        if (this.responses != null) {
            return this.responses.length;
        }
        return 0;
    }

    @DexIgnore
    public String getResponse() {
        return this.response;
    }

    @DexIgnore
    public void execute(InputStream input, OutputStream output, IObdDataObserver commandObserver) throws IOException {
        execute(input, output, null, commandObserver);
    }

    @DexIgnore
    public void execute(InputStream input, OutputStream output, Protocol protocol, IObdDataObserver commandObserver) throws IOException {
        // this.error = null;
        // Future future = singleThreadedExecutor.submit(new ObdCommand.Anon1(input, output, commandObserver, protocol));
        // try {
        //     future.get(getCommandExecutionTimeOutInMillis(), TimeUnit.MILLISECONDS);
        //     if (this.error != null) {
        //         throw this.error;
        //     }
        // } catch (InterruptedException e) {
        //     Log.error("Trying to cancel the task, Success : " + future.cancel(true));
        //     singleThreadedExecutor.shutdownNow();
        //     singleThreadedExecutor = Executors.newSingleThreadExecutor();
        //     throw new IOException("Command IO thread was interrupted", e);
        // } catch (ExecutionException e2) {
        //     throw new IOException(e2.getCause());
        // } catch (TimeoutException e3) {
        //     Log.error("Trying to cancel the task, Success : " + future.cancel(true));
        //     singleThreadedExecutor.shutdownNow();
        //     singleThreadedExecutor = Executors.newSingleThreadExecutor();
        //     throw new IOException(e3);
        // }
    }

    @DexIgnore
    protected void processCommandResponse(Protocol protocol, ObdCommandInterpreter obdCommandInterpreter, IObdDataObserver dataObserver) throws IOException {
        if (SLEEP_COMMAND == this) {
            Log.info("Reading response of Sleep command ");
            try {
                String response2 = obdCommandInterpreter.readLine();
                if (response2 == null || response2.length() <= 0) {
                    Log.info("Empty response for SLEEP command");
                    throw new IOException("Invalid response while putting chip to sleep");
                }
                Log.info("Response {}", response2.trim());
                if (response2.trim().equals(OK)) {
                    return;
                }
            } catch (InterruptedException e) {
                Log.error("Interrupted while reading response for sleep command", e);
                return;
            }
        }
        StringBuilder responseBuilder = new StringBuilder();
        try {
            String response3 = obdCommandInterpreter.readResponse();
            if (enableLogRaw) {
                LogRaw.info("raw read: {} ", response);
            }
            boolean textResponse = obdCommandInterpreter.didGetTextResponse();
            responseBuilder.append(response3);
            if (this == RESET_COMMAND) {
                Log.info("Reset Command, Clearing the terminal");
                try {
                    responseBuilder.append(obdCommandInterpreter.clearTerminal());
                    textResponse = obdCommandInterpreter.didGetTextResponse();
                } catch (InterruptedException e2) {
                    Log.error("Interrupted while clearing the terminal ", e2);
                    return;
                } catch (TimeoutException e3) {
                    Log.error("Clearing terminal timed out");
                    return;
                }
            }
            this.response = responseBuilder.toString();
            if (dataObserver != null) {
                dataObserver.onResponse(this.response);
            }
            this.responses = ResponseParser.parse(textResponse, this.response, protocol);
    
            if (enableLogRaw) {
                if (this.responses != null) {
                    for (EcuResponse rsp : this.responses) {
                        int ecu = rsp.ecu;
                        byte[] data = rsp.data;

                        LogRaw.info("read: ecu={}, data={} ", ecu, data);
                        if (data.length >= 3) {
                            if (data[0] == 0x7F) {
                                if (ObdErrors.containsKey((int)data[2])) {
                                    LogRaw.info("Error: code=0x{} err={}", Integer.toHexString(data[1]), ObdErrors.get((int)data[2]));
                                }
                                LogRaw.info("Unknown Error: code=0x{}", Integer.toHexString(data[1]));
                            }
                        }
                    }
                }
            }

        } catch (InterruptedException e4) {
            Log.error("Exception: ", e4);
        }
    }

    /* access modifiers changed from: protected */
    @DexIgnore
    public long getCommandExecutionTimeOutInMillis() {
        return this.timeoutMillis;
    }

    @DexReplace
    public double getDoubleValue() {
        byte[] data = getByteResponse();
        if (data != null) {
            try {
                if (data[0] == (byte) 65 && this.conversion != null) {
                    return this.conversion.convert(data);
                }
            } catch (Exception e) {
                Log.debug("Exception converting " + new String(data), e);
            }
        }
        return -2.147483648E9d;
    }

    @DexReplace
    public int getIntValue() {
        return (int) Math.round(getDoubleValue());
    }
}
