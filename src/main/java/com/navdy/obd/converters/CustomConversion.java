package com.navdy.obd.converters;

import com.navdy.obd.update.STNBootloaderChannel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import parsii.eval.Expression;
import parsii.eval.Parser;
import parsii.eval.Scope;
import parsii.eval.Variable;
import parsii.tokenizer.ParseError;
import parsii.tokenizer.ParseException;
import parsii.tokenizer.Position;

@DexIgnore
public class CustomConversion implements AbstractConversion {
    @DexIgnore
    private Expression expression;
    @DexIgnore
    private VariableOffset[] offsets;

    @DexIgnore
    static class VariableOffset {
        private int offset;
        private Variable variable;

        @DexIgnore
        public VariableOffset(Variable variable2, int offset2) {
            this.variable = variable2;
            this.offset = offset2;
        }
    }

    @DexIgnore
    public CustomConversion(String expression2) throws ParseException {
        Scope scope = Scope.create();
        this.expression = Parser.parse(expression2, scope);
        Collection<Variable> variables = scope.getLocalVariables();
        this.offsets = new VariableOffset[variables.size()];
        int i = 0;
        for (Variable variable : variables) {
            int offset = offset(variable.getName());
            if (offset < 0) {
                List<ParseError> errors = new ArrayList<>();
                errors.add(ParseError.error(Position.UNKNOWN, "Invalid variable " + variable.getName() + " in (" + expression2 + ")"));
                throw ParseException.create(errors);
            }
            int i2 = i + 1;
            this.offsets[i] = new VariableOffset(variable, offset);
            i = i2;
        }
    }

    @DexIgnore
    public double convert(byte[] rawData) {
        VariableOffset[] variableOffsetArr;
        for (VariableOffset var : this.offsets) {
            if (var.variable != null) {
                var.variable.setValue((double) (rawData[var.offset + 2] & STNBootloaderChannel.ERROR_CODE_INVALID_RESPONSE));
            }
        }
        return this.expression.evaluate();
    }

    @DexIgnore
    private int offset(String variableName) {
        boolean twoDigit;
        int tens;
        int i = 1;
        if (variableName == null || variableName.length() > 2 || variableName.length() == 0) {
            return -1;
        }
        String variableName2 = variableName.toUpperCase();
        twoDigit = variableName2.length() > 1;
        if (twoDigit) {
            tens = ((variableName2.charAt(0) - 'A') + 1) * 26;
        } else {
            tens = 0;
        }
        if (!twoDigit) {
            i = 0;
        }
        return tens + (variableName2.charAt(i) - 'A');
    }
}
