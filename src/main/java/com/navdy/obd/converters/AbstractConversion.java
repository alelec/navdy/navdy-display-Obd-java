package com.navdy.obd.converters;

import lanchon.dexpatcher.annotation.DexIgnore;

@DexIgnore
public interface AbstractConversion {
    double convert(byte[] bArr);
}
