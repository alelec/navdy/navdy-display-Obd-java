package com.navdy.obd;

import com.navdy.obd.command.ObdCommand;
import com.navdy.obd.converters.CustomConversion;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;

import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexReplace;
import parsii.tokenizer.ParseException;

@DexEdit
public class Profile {
    @DexIgnore
    static /* final */ Logger Log; // = org.slf4j.LoggerFactory.getLogger(com.navdy.obd.Profile.class);
    @DexIgnore
    public HashMap<String, ObdCommand> mapByName; // = new java.util.HashMap<>();
    @DexIgnore
    public HashMap<Integer, ObdCommand> mapByPid; // = new java.util.HashMap<>();

    @DexAdd
    public static HashMap<Integer, ObdCommand> mapByModePid = new HashMap<>();

    // @DexAdd
    // private Profile() {};

    @DexIgnore
    class Column {
        public static final String EQUATION = "Equation";
        public static final String HEADER = "Header";
        public static final String MAX_VALUE = "Max Value";
        public static final String MIN_VALUE = "Min Value";
        public static final String MODE_AND_PID = "ModeAndPID";
        public static final String NAME = "Name";
        public static final String SHORT_NAME = "ShortName";
        public static final String UNITS = "Units";

        @DexIgnore
        Column() {
        }
    }

    @DexAdd
    static final HashMap<Integer, String> ShortPidName = createShortPidLookup();
    @DexAdd
    private static HashMap<Integer, String> createShortPidLookup() {
        HashMap<Integer, String> result = new HashMap<>();
        result.put(0x04, "Engine Load");
        result.put(0x05, "Coolant Temp");
        result.put(0x06, "STFT1");
        result.put(0x07, "LTFT1");
        result.put(0x08, "STFT2");
        result.put(0x09, "LTFT2");
        result.put(0x0A, "Fuel pressure");
        result.put(0x0B, "Intake pressure");
        result.put(0x0C, "RPM");
        result.put(0x0D, "Speed");
        result.put(0x0E, "Timing Adv");
        result.put(0x0F, "Intake Temp");
        result.put(0x10, "MAF");
        result.put(0x11, "Throttle");
        result.put(0x14, "OxV");
        result.put(0x2F, "Fuel");
        result.put(0x31, "Distance");
        result.put(0x03, "FuelSystemStatus");
        result.put(0x46, "AmbientTemp");
        result.put(0x5C, "OilTemp");
        return result;
    }

    @DexIgnore
    public ObdCommand lookup(String id) {
        ObdCommand command = this.mapByName.get(id);
        if (command != null) {
            return command.clone();
        }
        return null;
    }

    @DexReplace
    public ObdCommand lookup(int pid) {
        String pidname = ShortPidName.get(pid);
        if (pidname != null) {
            return this.lookup(pidname);
        }
        ObdCommand command = this.mapByPid.get(pid);
        if (command != null) {
            return command.clone();
        }
        command = mapByModePid.get(pid);
        if (command != null) {
            return command.clone();
        }
        return null;
    }

    @DexReplace
    void add(String modeAndPid, ObdCommand command) {
        if (modeAndPid.startsWith("01")) {
            this.mapByPid.put(Integer.parseInt(modeAndPid.substring(2, 4), 16), command);
        }
        mapByModePid.put(Integer.parseInt(modeAndPid, 16), command);
        this.mapByName.put(command.getName(), command);
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x0098 A[SYNTHETIC, Splitter:B:30:0x0098] */
    @DexIgnore
    public void load(InputStream csvProfile) {
        // BufferedReader reader = null;
        // try {
        //     BufferedReader reader2 = new BufferedReader(new InputStreamReader(csvProfile));
        //     try {
        //         for (CSVRecord record : CSVFormat.DEFAULT.withHeader().parse(reader2)) {
        //             String name = record.get(Profile.Column.SHORT_NAME);
        //             String modeAndPid = record.get(Profile.Column.MODE_AND_PID);
        //             String equation = record.get(Profile.Column.EQUATION);
        //             try {
        //                 add(modeAndPid, new ObdCommand(name, modeAndPid, new CustomConversion(equation)));
        //             } catch (ParseException e) {
        //                 Log.error("Failed to parse equation (" + equation + ") for " + name);
        //             }
        //         }
        //         if (reader2 != null) {
        //             try {
        //                 reader2.close();
        //                 BufferedReader bufferedReader = reader2;
        //             } catch (IOException e2) {
        //                 Log.error("Error closing reader", e2);
        //                 BufferedReader bufferedReader2 = reader2;
        //             }
        //         }
        //     } catch (IOException e3) {
        //         e = e3;
        //         reader = reader2;
        //     } catch (Throwable th) {
        //         th = th;
        //         reader = reader2;
        //         if (reader != null) {
        //             try {
        //                 reader.close();
        //             } catch (IOException e4) {
        //                 Log.error("Error closing reader", e4);
        //             }
        //         }
        //         throw th;
        //     }
        // } catch (IOException e5) {
        //     e = e5;
        //     try {
        //         Log.error("Error reading custom profile", (Throwable) e);
        //         if (reader != null) {
        //             try {
        //                 reader.close();
        //             } catch (IOException e6) {
        //                 Log.error("Error closing reader", e6);
        //             }
        //         }
        //     } catch (Throwable th2) {
        //         th = th2;
        //         if (reader != null) {
        //         }
        //         throw th;
        //     }
        // }
    }
}
