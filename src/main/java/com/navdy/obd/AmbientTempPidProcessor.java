package com.navdy.obd;

public class AmbientTempPidProcessor extends PidProcessor {
    private int SAMPLE_SIZE = 15;
    private double mFilteredValue;
    private int mReadIndex = 0;
    private double[] mReadings = new double[this.SAMPLE_SIZE];
    private int mReadingsCount = 0;
    private double mRunningTotal = 0.0d;

    public boolean isSupported(PidSet supportedPids) {
        return supportedPids.contains(Pids.AMBIENT_AIR_TEMRERATURE);
    }

    public boolean processPidValue(PidLookupTable vehicleState) {
        double ambientTemp = vehicleState.getPidValue(Pids.AMBIENT_AIR_TEMRERATURE);
        if (ambientTemp < -40.0d || ambientTemp > 215.0d) {
            return vehicleState.updatePid(Pids.AMBIENT_AIR_TEMRERATURE, -1.0d);
        }
        this.mRunningTotal -= this.mReadings[this.mReadIndex];
        double[] dArr = this.mReadings;
        int i = this.mReadIndex;
        this.mReadIndex = i + 1;
        dArr[i] = ambientTemp;
        this.mRunningTotal += ambientTemp;
        if (this.mReadingsCount < this.SAMPLE_SIZE) {
            this.mReadingsCount++;
        }
        if (this.mReadIndex == this.SAMPLE_SIZE) {
            this.mReadIndex = 0;
        }
        this.mFilteredValue = this.mRunningTotal / ((double) this.mReadingsCount);
        return vehicleState.updatePid(Pids.AMBIENT_AIR_TEMRERATURE, this.mFilteredValue);
    }


    public PidSet resolveDependencies() {
        return null;
    }
}
