package com.navdy.obd;

public class EngineCoolantPidProcessor extends PidProcessor {
    private int SAMPLE_SIZE = 15;
    private int STEP_SIZE = 5;
    private double mFilteredValue;
    private int mReadIndex = 0;
    private double[] mReadings = new double[this.SAMPLE_SIZE];
    private int mReadingsCount = 0;
    private double mRunningTotal = 0.0d;

    public boolean isSupported(PidSet supportedPids) {
        return supportedPids.contains(Pids.ENGINE_COOLANT_TEMPERATURE);
    }

    public boolean processPidValue(PidLookupTable vehicleState) {
        double coolantTemp = vehicleState.getPidValue(Pids.ENGINE_COOLANT_TEMPERATURE);
        if (coolantTemp < -40.0d || coolantTemp > 215.0d) {
            return vehicleState.updatePid(Pids.ENGINE_COOLANT_TEMPERATURE, -1.0d);
        }
        this.mRunningTotal -= this.mReadings[this.mReadIndex];
        double[] dArr = this.mReadings;
        int i = this.mReadIndex;
        this.mReadIndex = i + 1;
        dArr[i] = coolantTemp;
        this.mRunningTotal += coolantTemp;
        if (this.mReadingsCount < this.SAMPLE_SIZE) {
            this.mReadingsCount++;
        }
        if (this.mReadIndex == this.SAMPLE_SIZE) {
            this.mReadIndex = 0;
        }
        this.mFilteredValue = this.mRunningTotal / ((double) this.mReadingsCount);
        return vehicleState.updatePid(Pids.ENGINE_COOLANT_TEMPERATURE, this.mFilteredValue);
    }


    public PidSet resolveDependencies() {
        return null;
    }
}
