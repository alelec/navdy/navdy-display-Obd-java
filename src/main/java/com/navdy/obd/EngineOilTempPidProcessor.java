package com.navdy.obd;

public class EngineOilTempPidProcessor extends PidProcessor {
    private int SAMPLE_SIZE = 15;
    private double mFilteredValue;
    private int mReadIndex = 0;
    private double[] mReadings = new double[this.SAMPLE_SIZE];
    private int mReadingsCount = 0;
    private double mRunningTotal = 0.0d;

    public boolean isSupported(PidSet supportedPids) {
        return supportedPids.contains(Pids.ENGINE_OIL_TEMRERATURE);
    }

    public boolean processPidValue(PidLookupTable vehicleState) {
        double oilTemp = vehicleState.getPidValue(Pids.ENGINE_OIL_TEMRERATURE);
        if (oilTemp < -40.0d || oilTemp > 215.0d) {
            return vehicleState.updatePid(Pids.ENGINE_OIL_TEMRERATURE, -1.0d);
        }
        this.mRunningTotal -= this.mReadings[this.mReadIndex];
        double[] dArr = this.mReadings;
        int i = this.mReadIndex;
        this.mReadIndex = i + 1;
        dArr[i] = oilTemp;
        this.mRunningTotal += oilTemp;
        if (this.mReadingsCount < this.SAMPLE_SIZE) {
            this.mReadingsCount++;
        }
        if (this.mReadIndex == this.SAMPLE_SIZE) {
            this.mReadIndex = 0;
        }
        this.mFilteredValue = this.mRunningTotal / ((double) this.mReadingsCount);
        return vehicleState.updatePid(Pids.ENGINE_OIL_TEMRERATURE, this.mFilteredValue);
    }

    public PidSet resolveDependencies() {
        return null;
    }
}
