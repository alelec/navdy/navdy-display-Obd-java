package com.navdy.obd;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.Log;
import android.util.SparseArray;

import com.navdy.obd.command.BatchCommand;
import com.navdy.obd.command.CANBusMonitoringCommand;
import com.navdy.obd.command.CheckDTCCommand;
import com.navdy.obd.command.GatherCANBusDataCommand;
import com.navdy.obd.command.ICommand;
import com.navdy.obd.command.InitializeCommand;
import com.navdy.obd.command.InitializeJ1939Command;
import com.navdy.obd.command.ObdCommand;
import com.navdy.obd.command.ReadTroubleCodesCommand;
import com.navdy.obd.command.ReadVoltageCommand;
import com.navdy.obd.command.ValidPidsCommand;
import com.navdy.obd.command.VinCommand;
import com.navdy.obd.command.WriteConfigurationCommand;
import com.navdy.obd.io.BluetoothChannel;
import com.navdy.obd.io.ChannelInfo;
import com.navdy.obd.io.IChannel;
import com.navdy.obd.io.IChannelSink;
import com.navdy.obd.io.STNSerialChannel;
import com.navdy.obd.j1939.J1939ObdJobAdapter;
import com.navdy.obd.j1939.J1939Profile;
import com.navdy.obd.jobs.AutoConnect;
import com.navdy.obd.jobs.ScanPidsJob;
import com.navdy.obd.simulator.MockObdChannel;
import com.navdy.obd.update.DefaultFirmwareManager;
import com.navdy.obd.update.ObdDeviceFirmwareManager;
import com.navdy.obd.update.STNSerialDeviceFirmwareManager;
import com.navdy.obd.update.Update;
import com.navdy.os.SystemProperties;

import org.yaml.snakeyaml.constructor.ConstructorException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexReplace;

@SuppressWarnings("SynchronizeOnNonFinalField")
@DexEdit(defaultAction = DexAction.ADD)
public class ObdService extends Service implements IChannelSink {
    @DexIgnore
    private static /* final */ int BATTERY_CHECK_INTERVAL; // = 2000;
    @DexIgnore
    public static /* final */ int BAUD_RATE; // = com.navdy.os.SystemProperties.getInt(BAUD_RATE_PROP, 460800);
    @DexIgnore
    private static /* final */ String BAUD_RATE_PROP; // = "persist.sys.obd.baudrate";
    @DexIgnore
    public static /* final */ String CAN_BUS_LOGGER; // = "com.navdy.obd.CanBusRaw";
    @DexIgnore
    public static /* final */ boolean CAN_BUS_MONITORING_ENABLED; // = false;
    @DexIgnore
    public static /* final */ int CAN_BUS_SAMPLING_INTERVAL; // = 1000;
    @DexIgnore
    private static /* final */ int DEFAULT_MODE; // = com.navdy.os.SystemProperties.getInt(DEFAULT_MODE_PROP, 0);
    @DexIgnore
    private static /* final */ String DEFAULT_MODE_PROP; // = "persist.sys.obd.mode";
    @DexIgnore
    public static /* final */ int DEFAULT_SCAN_INTERVAL; // = 500;
    @DexIgnore
    public static /* final */ int GATHER_CAN_BUS_DATA_INTERVAL; // = 1000;
    @DexIgnore
    public static /* final */ int MESSAGE_CHANGE_MODE; // = 9;
    @DexIgnore
    public static /* final */ int MESSAGE_CHARGING; // = 4;
    @DexIgnore
    public static /* final */ int MESSAGE_DISABLE_OBD_PID_SCAN; // = 5;
    @DexIgnore
    public static /* final */ int MESSAGE_ENABLE_OBD_PID_SCAN; // = 6;
    @DexIgnore
    public static /* final */ int MESSAGE_RESCAN; // = 3;
    @DexIgnore
    public static /* final */ int MESSAGE_RESET_CHANNEL; // = 7;
    @DexIgnore
    public static /* final */ int MESSAGE_STATE_CHANGE; // = 1;
    @DexIgnore
    public static /* final */ int MESSAGE_TOAST; // = 2;
    @DexIgnore
    public static /* final */ int MESSAGE_UPDATE_FIRMWARE; // = 8;
    @DexIgnore
    public static /* final */ int MODE_ONE_READ_RESPONSE; // = 65;
    @DexIgnore
    public static /* final */ int MONITOR_CAN_BUS_BATCH_INTERVAL; // = 30000;
    @DexIgnore
    public static /* final */ int MONITOR_CAN_BUS_BATCH_SIZE; // = 5;
    @DexIgnore
    public static /* final */ String OBD_DEBUG_MODE; // = "persist.sys.obd.debug";
    @DexIgnore
    public static /* final */ String OVERRIDE_PROTOCOL; // = "persist.sys.obd.protocol";
    @DexIgnore
    public static /* final */ String PREFS_FILE_OBD_DEVICE; // = "ObdDevice";
    @DexIgnore
    public static /* final */ String PREFS_KEY_DEFAULT_CHANNEL_INFO; // = "DefaultChannelInfo";
    @DexIgnore
    public static /* final */ String PREFS_KEY_LAST_PROTOCOL; // = "Protocol";
    @DexIgnore
    public static /* final */ String PREFS_KEY_OBD_SCAN_ENABLED; // = "ObdCommunicationEnabled";
    @DexIgnore
    public static /* final */ String PREFS_KEY_OBD_SCAN_MODE; // = "ObdScanMode";
    @DexIgnore
    public static /* final */ String PREFS_LAST_CONNECTED_BAUD_RATE; // = "last_connected_baud_rate";
    @DexIgnore
    private static long RECONNECT_DELAY; // = java.util.concurrent.TimeUnit.SECONDS.toMillis(30);
    @DexIgnore
    private static /* final */ int SCAN_INTERVAL; // = 10;
    @DexIgnore
    private static /* final */ String TAG; // = com.navdy.obd.ObdService.class.getSimpleName();
    @DexIgnore
    public static /* final */ String TOAST; // = "toast";
    @DexIgnore
    public static /* final */ String VOLTAGE_FREQUENCY_PROPERTY; // = "persist.sys.obd.volt.freq";
    @DexIgnore
    public static /* final */ int VOLTAGE_REPORT_INTERVAL; // = (com.navdy.os.SystemProperties.getInt(VOLTAGE_FREQUENCY_PROPERTY, 30) * 1000);
    @DexIgnore
    public static /* final */ int VOLTAGE_THRESHOLD_MINIMUM_TIME_IN_SECONDS; // = 10;
    @DexIgnore
    private static ObdService obdService;
    @DexIgnore
    private IObdService.Stub apiEndpoint; // = new com.navdy.obd.ObdService.Anon2();
    @DexIgnore
    private AutoConnect autoConnect;
    @DexIgnore
    private ICanBusMonitoringListener canBusMonitoringListener;
    @DexIgnore
    private ObdService.CANBusMonitoringStatus canBusMonitoringStatus; // = new com.navdy.obd.ObdService.CANBusMonitoringStatus();
    @DexIgnore
    private ICarService.Stub carApiEndpoint; // = new com.navdy.obd.ObdService.Anon1();
    @DexIgnore
    private IChannel channel;
    @DexIgnore
    private ChannelInfo channelInfo;
    @DexIgnore
    private String configurationName;
    @DexIgnore
    private int connectionState; // = 0;
    @DexIgnore
    private double currentBatteryVoltage; // = -1.0d;
    @DexIgnore
    private String currentFirmwareVersion; // = null;
    @DexIgnore
    private int currentMode; // = -1;
    @DexIgnore
    private ObdScanJob currentScan;
    @DexIgnore
    private ScheduledFuture currentSchedule;
    @DexIgnore
    private boolean debugMode;
    @DexIgnore
    private boolean deepSleep; // = false;
    @DexIgnore
    private ObdDeviceFirmwareManager firmwareManager;
    @DexIgnore
    private String firmwareUpdatePath;
    @DexIgnore
    private String firmwareUpdateVersion;
    @DexIgnore
    private boolean gatherCanBusDataToLogs; // = false;
    @DexIgnore
    private volatile boolean isScanningEnabled; // = true;
    @DexIgnore
    private J1939Profile j1939Profile;
    @DexIgnore
    private volatile int lastConnectedBaudRate; // = 0;
    @DexIgnore
    private long lastDisconnectTime; // = 0;
    @DexIgnore
    private long lastMonitorBatchCompletedTime; // = 0;
    @DexIgnore
    private long lastVoltageReport;
    @DexIgnore
    private /* final */ List<IObdServiceListener> listeners; // = new java.util.ArrayList();
    @DexIgnore
    protected SharedPreferences mSharedPrefs;
    @DexIgnore
    private VehicleStateManager mVehicleStateManager;
    @DexIgnore
    private int monitorCounter; // = 0;
    @DexIgnore
    public boolean monitoringCanBusLimitReached; // = false;
    @DexIgnore
    private ObdDataObserver obdDataObserver;
    @DexIgnore
    private boolean onMonitoringStatusReported; // = false;
    @DexIgnore
    private /* final */ ArrayMap<IBinder, PidListener> pidListeners; // = new android.util.ArrayMap<>();
    @DexIgnore
    private Profile profile;
    @DexIgnore
    private Protocol protocol;
    @DexIgnore
    private boolean reconnectImmediately; // = false;
    @DexIgnore
    private /* final */ ScheduledExecutorService scheduler; // = java.util.concurrent.Executors.newSingleThreadScheduledExecutor();
    @DexIgnore
    private ObdService.ServiceHandler serviceHandler;
    @DexIgnore
    private Looper serviceLooper;
    @DexIgnore
    private BatchCommand sleepWithWakeUpTriggersCommand;
    @DexIgnore
    private VehicleInfo vehicleInfo;
    @DexIgnore
    private ScheduledFuture voltageMonitoring;
    @DexIgnore
    private VoltageSettings voltageSettings;
    @DexIgnore
    private /* final */ Object voltageSettingsLock; // = new java.lang.Object();

    @DexAdd
    private boolean remoteClientConnected = false;

    // @DexIgnore
    // class Anon1 extends ICarService.Stub {
    //     @DexIgnore
    //     Anon1() {
    //     }
    //
    //     @DexIgnore
    //     public int getConnectionState() throws RemoteException {
    //         return ObdService.this.connectionState;
    //     }
    //
    //     @DexIgnore
    //     public List<Pid> getSupportedPids() throws RemoteException {
    //         if (ObdService.this.currentMode == 0) {
    //             if (ObdService.this.vehicleInfo != null) {
    //                 return ObdService.this.vehicleInfo.getPrimaryEcu().supportedPids.asList();
    //             }
    //         } else if (ObdService.this.currentMode == 1 && getConnectionState() == 2) {
    //             return J1939ObdJobAdapter.J1939SupportedObdPids.asList();
    //         }
    //         return null;
    //     }
    //
    //     @DexIgnore
    //     public List<ECU> getEcus() {
    //         if (ObdService.this.vehicleInfo != null) {
    //             return ObdService.this.vehicleInfo.getEcus();
    //         }
    //         return null;
    //     }
    //
    //     @DexIgnore
    //     public String getProtocol() {
    //         return ObdService.this.vehicleInfo.protocol;
    //     }
    //
    //     @DexIgnore
    //     public String getVIN() {
    //         if (ObdService.this.vehicleInfo != null) {
    //             return ObdService.this.vehicleInfo.vin;
    //         }
    //         return null;
    //     }
    //
    //     @DexIgnore
    //     public List<Pid> getReadings(List<Pid> pids) throws RemoteException {
    //         if (ObdService.this.autoConnect == null) {
    //             return null;
    //         }
    //         return ObdService.this.autoConnect.getReadings(pids);
    //     }
    //
    //     @DexIgnore
    //     public void addListener(List<Pid> pids, IPidListener listener) throws RemoteException {
    //         ObdService.this.addListener(pids, listener);
    //     }
    //
    //     @DexIgnore
    //     public void removeListener(IPidListener listener) {
    //         ObdService.this.removeListener(listener);
    //     }
    //
    //     @DexIgnore
    //     public String getCurrentConfigurationName() throws RemoteException {
    //         return ObdService.this.configurationName;
    //     }
    //
    //     @DexIgnore
    //     public boolean applyConfiguration(String configuration) {
    //         try {
    //             ObdService.this.applyConfigurationInternal(configuration);
    //             ObdService.this.updateScan();
    //             return true;
    //         } catch (IllegalArgumentException t) {
    //             ObdService.this.updateScan();
    //             throw t;
    //         } catch (Throwable th) {
    //             if (1 != 0) {
    //                 ObdService.this.applyFallbackConfiguration();
    //             }
    //             return false;
    //         }
    //     }
    //
    //     @DexIgnore
    //     public void updateScan(ScanSchedule schedule, IPidListener listener) throws RemoteException {
    //         ObdService.this.addListener(schedule, listener);
    //     }
    //
    //     @DexIgnore
    //     public void rescan() {
    //         ObdService.this.serviceHandler.sendEmptyMessage(3);
    //     }
    //
    //     @DexIgnore
    //     public double getBatteryVoltage() throws RemoteException {
    //         return ObdService.this.currentBatteryVoltage;
    //     }
    //
    //     @DexIgnore
    //     public void setObdPidsScanningEnabled(boolean enable) throws RemoteException {
    //         if (enable == ObdService.this.isScanningEnabled) {
    //             return;
    //         }
    //         if (enable) {
    //             ObdService.this.serviceHandler.sendEmptyMessage(6);
    //         } else {
    //             ObdService.this.serviceHandler.sendEmptyMessage(5);
    //         }
    //     }
    //
    //     @DexIgnore
    //     public boolean isObdPidsScanningEnabled() throws RemoteException {
    //         return ObdService.this.isScanningEnabled;
    //     }
    //
    //     @DexIgnore
    //     public void sleep(boolean deep) throws RemoteException {
    //         Log.d(ObdService.TAG, "sleep API is invoked");
    //         ObdService.this.deepSleep = deep;
    //         ObdService.this.postStateChange(5);
    //     }
    //
    //     @DexIgnore
    //     public void wakeup() throws RemoteException {
    //         if (ObdService.this.connectionState == 5) {
    //             ObdService.this.postStateChange(4);
    //         }
    //     }
    //
    //     @DexIgnore
    //     public void setVoltageSettings(VoltageSettings settings) throws RemoteException {
    //         ObdService.this.setVoltageSettings(settings);
    //     }
    //
    //     @DexIgnore
    //     public String getObdChipFirmwareVersion() throws RemoteException {
    //         return ObdService.this.currentFirmwareVersion;
    //     }
    //
    //     @DexIgnore
    //     public void updateFirmware(String version, String updateFilePath) throws RemoteException {
    //         Log.d(ObdService.TAG, "updateFirmware , version :" + version + " , Path : " + updateFilePath);
    //         ObdService.this.firmwareUpdateVersion = version;
    //         ObdService.this.firmwareUpdatePath = updateFilePath;
    //         ObdService.this.getHandler().sendEmptyMessage(8);
    //     }
    //
    //     @DexIgnore
    //     public int getMode() throws RemoteException {
    //         return ObdService.this.currentMode;
    //     }
    //
    //     @DexIgnore
    //     public void setMode(int mode, boolean persistent) throws RemoteException {
    //         Log.d(ObdService.TAG, "setMode : " + mode + ", Persistent : " + persistent);
    //         if (persistent) {
    //             Log.d(ObdService.TAG, "Saving the mode");
    //             ObdService.this.mSharedPrefs.edit().putInt(ObdService.PREFS_KEY_OBD_SCAN_MODE, mode).apply();
    //         }
    //         ObdService.this.getHandler().obtainMessage(9, mode, -1).sendToTarget();
    //     }
    //
    //     @DexIgnore
    //     public void setCANBusMonitoringListener(ICanBusMonitoringListener listener) throws RemoteException {
    //         ObdService.this.canBusMonitoringListener = listener;
    //     }
    //
    //     @DexIgnore
    //     public void startCanBusMonitoring() throws RemoteException {
    //         ObdService.this.gatherCanBusDataToLogs = true;
    //         ObdService.this.updateScan();
    //     }
    //
    //     @DexIgnore
    //     public void stopCanBusMonitoring() throws RemoteException {
    //         ObdService.this.gatherCanBusDataToLogs = false;
    //         ObdService.this.canBusMonitoringListener = null;
    //         ObdService.this.updateScan();
    //     }
    //
    //     @DexIgnore
    //     public boolean isCheckEngineLightOn() throws RemoteException {
    //         if (ObdService.this.vehicleInfo != null) {
    //             return ObdService.this.vehicleInfo.isCheckEngineLightOn;
    //         }
    //         return false;
    //     }
    //
    //     @DexIgnore
    //     public List<String> getTroubleCodes() throws RemoteException {
    //         if (ObdService.this.vehicleInfo != null) {
    //             return ObdService.this.vehicleInfo.troubleCodes;
    //         }
    //         return null;
    //     }
    // }
    //
    // @DexIgnore
    // class Anon2 extends IObdService.Stub {
    //
    //     @DexIgnore
    //     class Anon1 implements Runnable {
    //         @DexIgnore
    //         final /* synthetic */ Update val$update;
    //
    //         @DexIgnore
    //         Anon1(Update update) {
    //             this.val$update = update;
    //         }
    //
    //         @DexIgnore
    //         public void run() {
    //             Log.d(ObdService.TAG, "Update success : " + ObdService.this.firmwareManager.updateFirmware(this.val$update));
    //             ObdService.this.reconnectImmediately = false;
    //             ObdService.this.firmwareUpdateVersion = "";
    //             ObdService.this.firmwareUpdatePath = "";
    //             ObdService.this.forceReconnect();
    //         }
    //     }
    //
    //     /* renamed from: com.navdy.obd.ObdService$Anon2$Anon2 reason: collision with other inner class name */
    //     @DexIgnore
    //     class C0000Anon2 implements ObdScanJob.IRawData {
    //         @DexIgnore
    //         C0000Anon2() {
    //         }
    //
    //         @DexIgnore
    //         public void onRawData(String data) {
    //             synchronized (ObdService.this.listeners) {
    //                 for (IObdServiceListener listener : ObdService.this.listeners) {
    //                     try {
    //                         listener.onRawData(data);
    //                     } catch (RemoteException e) {
    //                         Log.e(ObdService.TAG, "Error notifying listener", e);
    //                     }
    //                 }
    //             }
    //         }
    //     }
    //
    //     @DexIgnore
    //     Anon2() {
    //     }
    //
    //     @DexIgnore
    //     public void connect(String address, IObdServiceListener listener) throws RemoteException {
    //         synchronized (ObdService.this.listeners) {
    //             ObdService.this.listeners.add(listener);
    //         }
    //         ChannelInfo info = ChannelInfo.parse(address);
    //         if (info != null) {
    //             Log.d(ObdService.TAG, "Attempting to connect to " + address);
    //             ObdService.this.openChannel(info);
    //         }
    //     }
    //
    //     @DexIgnore
    //     public void disconnect() throws RemoteException {
    //         if (ObdService.this.channel != null) {
    //             ObdService.this.channel.disconnect();
    //         }
    //     }
    //
    //     @DexIgnore
    //     public int getState() throws RemoteException {
    //         return ObdService.this.connectionState;
    //     }
    //
    //     @DexIgnore
    //     public String getFirmwareVersion() {
    //         try {
    //             return ObdService.this.currentFirmwareVersion;
    //         } catch (Throwable th) {
    //             return null;
    //         }
    //     }
    //
    //     @DexIgnore
    //     public boolean updateDeviceFirmware(String version, String updateFilePath) {
    //         try {
    //             if (ObdService.this.firmwareManager == null || ObdService.this.firmwareManager.isUpdatingFirmware()) {
    //                 if (ObdService.this.firmwareManager != null) {
    //                     Log.d(ObdService.TAG, "FirmwareManager is busy installing update already");
    //                 }
    //                 return true;
    //             }
    //             Update update = new Update();
    //             update.mVersion = version;
    //             update.mUpdateFilePath = updateFilePath;
    //             Runnable updateJob = new ObdService.Anon2.Anon1(update);
    //             ObdService.this.cancelCurrentSchedule();
    //             ObdService.this.cancelVoltageMonitoring();
    //             ObdService.this.scheduler.submit(updateJob);
    //             return true;
    //         } catch (Throwable t) {
    //             Log.e(ObdService.TAG, "Exception while updating the firmware :" + t);
    //         }
    //         return false;
    //     }
    //
    //     @DexIgnore
    //     public void reset() {
    //         ObdService.this.serviceHandler.sendEmptyMessage(7);
    //     }
    //
    //     @DexIgnore
    //     public void scanSupportedPids() throws RemoteException {
    //         ObdService.this.scanSupportedPids();
    //     }
    //
    //     @DexIgnore
    //     public void scanVIN() throws RemoteException {
    //         ObdService.this.scanVIN();
    //     }
    //
    //     @DexIgnore
    //     public String readPid(int pid) throws RemoteException {
    //         try {
    //             ObdService.this.synchronousCommand(new ObdCommand(String.format("01%02x1", pid)));
    //             return null;
    //         } catch (ExecutionException e) {
    //             return null;
    //         }
    //     }
    //
    //     @DexIgnore
    //     public void scanPids(List<Pid> pids, int intervalMs) throws RemoteException {
    //         ObdService.this.scanPids(pids, intervalMs);
    //     }
    //
    //     @DexIgnore
    //     public void startRawScan() throws RemoteException {
    //         ObdService.this.cancelCurrentSchedule();
    //         ObdService.this.cancelVoltageMonitoring();
    //         ObdService.this.currentScan = new ObdScanJob(ObdService.this.channel, ObdService.this.protocol, new ObdService.Anon2.C0000Anon2(), ObdService.this.obdDataObserver);
    //         ObdService.this.scheduler.submit(ObdService.this.currentScan);
    //     }
    //
    //     @DexIgnore
    //     public void addListener(IObdServiceListener listener) throws RemoteException {
    //         ObdService.this.addListener(listener);
    //     }
    //
    //     @DexIgnore
    //     public void removeListener(IObdServiceListener listener) throws RemoteException {
    //         ObdService.this.removeListener(listener);
    //     }
    // }
    //
    @DexIgnore
    class Anon3 implements ObdJob.IListener {
        @DexIgnore
        Anon3() {
        }

        @DexIgnore
        public void onComplete(ObdJob job, boolean success) {
            if (success) {
                ICommand command = job.command;
                String response = command.getResponse();
                Log.d(ObdService.TAG, "Response of the Synchronous command " + (response != null ? response.trim() : "EMPTY") + ", Command : " + command);
            }
        }
    }
    //
    // @DexIgnore
    // class Anon4 implements ObdJob.IListener {
    //     @DexIgnore
    //     final /* synthetic */ ReadVoltageCommand val$command;
    //
    //     @DexIgnore
    //     Anon4(ReadVoltageCommand readVoltageCommand) {
    //         this.val$command = readVoltageCommand;
    //     }
    //
    //     @DexIgnore
    //     public void onComplete(ObdJob job, boolean success) {
    //         if (success) {
    //             double voltage = (double) this.val$command.getVoltage();
    //             ObdService.this.currentBatteryVoltage = voltage;
    //             long now = SystemClock.elapsedRealtime();
    //             if (ObdService.this.lastVoltageReport == 0 || now - ObdService.this.lastVoltageReport > ((long) ObdService.VOLTAGE_REPORT_INTERVAL)) {
    //                 Log.i(ObdService.TAG, "Battery voltage: " + voltage);
    //                 ObdService.this.lastVoltageReport = now;
    //             }
    //             if (ObdService.this.connectionState == 4 && ObdService.this.isScanningEnabled) {
    //                 if (voltage <= ((double) ObdService.this.voltageSettings.chargingVoltage) && !ObdService.this.debugMode) {
    //                     return;
    //                 }
    //                 if (ObdService.this.lastDisconnectTime == 0 || SystemClock.elapsedRealtime() - ObdService.this.lastDisconnectTime > ObdService.RECONNECT_DELAY) {
    //                     Log.i(ObdService.TAG, "Battery now charging: " + voltage);
    //                     ObdService.this.serviceHandler.sendEmptyMessage(4);
    //                     return;
    //                 }
    //                 Log.i(ObdService.TAG, "Waiting for voltage to settle: " + voltage);
    //             }
    //         }
    //     }
    // }
    //
    // @DexIgnore
    // class Anon5 implements ObdJob.IListener {
    //     @DexIgnore
    //     Anon5() {
    //     }
    //
    //     @DexIgnore
    //     public void onComplete(ObdJob job, boolean success) {
    //         if (success) {
    //             Log.d(ObdService.TAG, "Successfully able to put the Obd chip to sleep");
    //         }
    //     }
    // }
    //
    // @DexIgnore
    // class Anon6 implements J1939ObdJobAdapter.J1939ObdJobListener {
    //     @DexIgnore
    //     final /* synthetic */ J1939ObdJobAdapter val$jobdAdpater;
    //
    //     @DexIgnore
    //     Anon6(J1939ObdJobAdapter j1939ObdJobAdapter) {
    //         this.val$jobdAdpater = j1939ObdJobAdapter;
    //     }
    //
    //     @DexIgnore
    //     public void onNewDataAvailable() {
    //         ObdService.this.updateListenersWithNewData(this.val$jobdAdpater.getMonitoredPidsList());
    //     }
    // }
    //
    @DexIgnore
    class Anon7 implements ScanPidsJob.IListener {
        @DexIgnore
        ObdService ObdService_this;

        @DexIgnore
        Anon7() {}

        @DexIgnore
        Anon7(ObdService _this) {
            ObdService_this = _this;
        }

        @DexIgnore
        public void onCanBusDataRead(ScanPidsJob job) {
            ObdService.this.updateListenersWithNewData(job.getFullPidsList());
        }

        @DexIgnore
        public CANBusMonitoringCommand getCanBusMonitoringCommand() {
            return ObdService.this.canBusMonitoringStatus.canBusMonitoringCommand;
        }

        @DexIgnore
        public void onCanBusMonitoringErrorDetected() {
            if (ObdService.this.gatherCanBusDataToLogs) {
                ObdService.this.gatherCanBusDataToLogs = false;
                if (ObdService.this.canBusMonitoringListener != null) {
                    try {
                        ObdService.this.canBusMonitoringListener.onCanBusMonitoringError(ObdServiceInterface.CAN_BUS_MONITOR_ERROR_MESSAGE_BAD_STATE);
                        ObdService.this.monitoringCanBusLimitReached = true;
                        ObdService.this.obdDataObserver.onRawCanBusMessage("\n");
                    } catch (RemoteException e) {
                        Log.e(ObdService.TAG, "RemoteException ", e);
                    }
                }
            }
        }

        @DexIgnore
        public void onComplete(ObdJob job, boolean success) {
            int quantized;
            List<Pid> fullPids = ((ScanPidsJob) job).getFullPidsList();
            if (ObdService.this.gatherCanBusDataToLogs) {
                if (GatherCANBusDataCommand.DEFAULT_MONITOR_COMMAND.isMonitoringFailureDetected()) {
                    Log.e(ObdService.TAG, "Can bus monitoring failed, disabling the monitoring");
                    GatherCANBusDataCommand.DEFAULT_MONITOR_COMMAND.reset();
                    ObdService.this.gatherCanBusDataToLogs = false;
                    if (ObdService.this.canBusMonitoringListener != null) {
                        try {
                            String errorData = GatherCANBusDataCommand.DEFAULT_MONITOR_COMMAND.getErrorData();
                            ICanBusMonitoringListener access$Anon2400 = ObdService.this.canBusMonitoringListener;
                            if (TextUtils.isEmpty(errorData)) {
                                errorData = ObdServiceInterface.GENERIC_CAN_BUS_MONITOR_ERROR_MESSAGE;
                            }
                            access$Anon2400.onCanBusMonitoringError(errorData);
                        } catch (RemoteException e) {
                            Log.e(ObdService.TAG, "RemoteException ", e);
                        }
                    }
                    ObdService.this.updateScan();
                } else if (GatherCANBusDataCommand.DEFAULT_MONITOR_COMMAND.getLastSampleSucceeded()) {
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append("OBD_DATA,");
                    for (Pid pid : fullPids) {
                        stringBuilder.append(pid.getId() + ":" + pid.getValue()).append(",");
                    }
                    try {
                        if (ObdService.this.canBusMonitoringListener != null) {
                            stringBuilder.append("Lat:" + ObdService.this.canBusMonitoringListener.getLatitude() + ",").append("Long:" + ObdService.this.canBusMonitoringListener.getLongitude() + ",").append("Speed:" + ObdService.this.canBusMonitoringListener.getGpsSpeed());
                        }
                    } catch (Exception e2) {
                        Log.e(ObdService.TAG, "");
                    }
                    double lastSampleDataSize = (double) GatherCANBusDataCommand.DEFAULT_MONITOR_COMMAND.getLastSampleDataSize();
                    if (!ObdService.this.onMonitoringStatusReported && lastSampleDataSize > 0.0d) {
                        ObdService.this.onMonitoringStatusReported = true;
                        if (lastSampleDataSize > 1000.0d) {
                            lastSampleDataSize = (double) Math.round(lastSampleDataSize / 1000.0d);
                            quantized = (int) (Math.round(lastSampleDataSize / 50.0d) * 50);
                        } else {
                            quantized = (int) lastSampleDataSize;
                        }
                        Log.d(ObdService.TAG, "Monitor succeeded , Data size, Actual : " + lastSampleDataSize + ", Quantized : " + quantized);
                        if (ObdService.this.canBusMonitoringListener != null) {
                            try {
                                ObdService.this.canBusMonitoringListener.onCanBusMonitorSuccess(quantized);
                            } catch (RemoteException e3) {
                                Log.e(ObdService.TAG, "Error reporting the can bus monitor success");
                            }
                        }
                    }
                    ObdService.this.obdDataObserver.onRawCanBusMessage(stringBuilder.toString());
                    ObdService.this.monitorCounter = ObdService.this.monitorCounter + 1;
                    if (ObdService.this.monitorCounter >= 5) {
                        Log.d(ObdService.TAG, "Monitor Batch completed");
                        ObdService.this.lastMonitorBatchCompletedTime = SystemClock.elapsedRealtime();
                        ObdService.this.monitorCounter = 0;
                        ObdService.this.updateScan();
                    }
                    GatherCANBusDataCommand.DEFAULT_MONITOR_COMMAND.resetLastSampleStats();
                }
                if (ObdService.this.canBusMonitoringListener != null) {
                    try {
                        if (ObdService.this.canBusMonitoringListener.isMonitoringLimitReached()) {
                            ObdService.this.monitoringCanBusLimitReached = true;
                            ObdService.this.obdDataObserver.onRawCanBusMessage("\n");
                        }
                    } catch (RemoteException e4) {
                        e4.printStackTrace();
                    }
                }
            }
            ObdService.this.updateListenersWithNewData(fullPids);
        }
    }
    //
    // @DexIgnore
    // class Anon8 implements Runnable {
    //     @DexIgnore
    //     Anon8() {
    //     }
    //
    //     @DexIgnore
    //     public void run() {
    //         synchronized (ObdService.this.listeners) {
    //             String vin = ObdService.this.vehicleInfo != null ? ObdService.this.vehicleInfo.vin : null;
    //             for (IObdServiceListener listener : ObdService.this.listeners) {
    //                 try {
    //                     listener.scannedVIN(vin);
    //                 } catch (RemoteException e) {
    //                     Log.e(ObdService.TAG, "Error notifying listener", e);
    //                 }
    //             }
    //         }
    //     }
    // }
    //
    // @DexIgnore
    // class Anon9 implements Runnable {
    //     @DexIgnore
    //     Anon9() {
    //     }
    //
    //     @DexIgnore
    //     public void run() {
    //         List<Pid> pids = ObdService.this.vehicleInfo.getPrimaryEcu().supportedPids.asList();
    //         synchronized (ObdService.this.listeners) {
    //             for (IObdServiceListener listener : ObdService.this.listeners) {
    //                 try {
    //                     listener.supportedPids(pids);
    //                 } catch (RemoteException e) {
    //                     Log.e(ObdService.TAG, "Error notifying listener", e);
    //                 }
    //             }
    //         }
    //     }
    // }

    @DexIgnore
    enum CANBusMonitoringState {
        UNKNOWN,
        UNAVAILABLE,
        SAMPLING,
        MONITORING
    }

    @DexIgnore
    static class CANBusMonitoringStatus {
        @DexIgnore
        CANBusMonitoringCommand canBusMonitoringCommand;
        @DexIgnore
        ObdService.CANBusMonitoringState state = ObdService.CANBusMonitoringState.UNKNOWN;

        @DexIgnore
        CANBusMonitoringStatus() {
        }
    }

    @DexEdit(defaultAction = DexAction.ADD)
    final class ServiceHandler extends Handler {
        @DexIgnore
        ObdService this$Anon0;

        // @DexIgnore
        // ServiceHandler(ObdService x0, Looper x1, ObdService.Anon1 x2) {
        //     this(x0, x1);
        // }

        @DexAdd
        ServiceHandler(ObdService x0, Looper x1) {
            this(x1);
            this.this$Anon0 = x0;

        }

        @DexIgnore
        private ServiceHandler(Looper looper) {
            super(looper);
        }

        @DexIgnore
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    this.this$Anon0.handleStateChange(msg.arg1);
                    return;
                case 2:
                    Log.i(ObdService.TAG, "MESSAGE_TOAST: " + msg.arg1);
                    for (IObdServiceListener listener : this.this$Anon0.listeners) {
                        try {
                            listener.onStatusMessage(msg.getData().getString("toast"));
                        } catch (RemoteException e) {
                            Log.e(ObdService.TAG, "Failed to notify listener of statusMessage");
                        }
                    }
                    return;
                case 3:
                case 4:
                    if (this.this$Anon0.connectionState == 4 && this.this$Anon0.isScanningEnabled) {
                        Log.i(ObdService.TAG, "Rescanning for ECUs");
                        this.this$Anon0.handleStateChange(3);
                        return;
                    }
                    return;
                case 5:
                    Log.i(ObdService.TAG, "Disabling PIDs scan");
                    this.this$Anon0.mSharedPrefs.edit().putBoolean(ObdService.PREFS_KEY_OBD_SCAN_ENABLED, false).apply();
                    this.this$Anon0.isScanningEnabled = false;
                    if (this.this$Anon0.connectionState == 2) {
                        this.this$Anon0.cancelCurrentSchedule();
                        this.this$Anon0.handleStateChange(4);
                        return;
                    }
                    return;
                case 6:
                    Log.i(ObdService.TAG, "Enabling PIDs scan");
                    this.this$Anon0.mSharedPrefs.edit().putBoolean(ObdService.PREFS_KEY_OBD_SCAN_ENABLED, true).apply();
                    this.this$Anon0.isScanningEnabled = true;
                    return;
                case 7:
                    this.this$Anon0.resetChannel();
                    return;
                case 8:
                    this.this$Anon0.cancelCurrentSchedule();
                    this.this$Anon0.reconnectImmediately = true;
                    this.this$Anon0.forceReconnect();
                    return;
                case 9:
                    this.this$Anon0.cancelCurrentSchedule();
                    this.this$Anon0.setMode(msg.arg1);
                    this.this$Anon0.forceReconnect();
                    return;
                default:
                    return;
            }
        }
    }


    @DexEdit
    // Original constructor, add tag to allow invoking from new constructor
    public ObdService(@DexIgnore Void tag) {
        setVoltageSettings(new VoltageSettings());
    }

    @DexAdd
    public ObdService() {
        this(null);
        startRemoteClientServer();
    }

    @DexAdd
    private void startRemoteClientServer() {
        Runnable serverTask = () -> {
            final ServerSocket serverSocket;
            try {
                serverSocket = new ServerSocket(6500);
                Log.v(TAG, "Started RemoteClientServer on port 6500");
                while (true) {
                    final Socket tcpSocket = serverSocket.accept();
                    Log.v(TAG, "Remote Client Connected, blocking local communication");
                    ObdService.this.remoteClientConnected = true;
                    ObdService.this.connectionState = 1;

                    // Run the forwarding task on the main scheduler to block other operations.
                    ObdService.this.scheduler.submit(() -> {
                        try {
                            InputStream tcpIn;
                            OutputStream tcpOut;
                            InputStream obdIn;
                            OutputStream obdOut;
                            try {
                                // Turn on keep-alive for both the sockets
//                                        clientSocket.setKeepAlive(true);

                                // Obtain client & server input & output streams
                                tcpIn = tcpSocket.getInputStream();
                                tcpOut = tcpSocket.getOutputStream();

                                obdIn = ObdService.this.channel.getInputStream();
                                obdOut = ObdService.this.channel.getOutputStream();

                            } catch (IOException ioe) {
                                Log.e(TAG, "Failed to connect RemoteClientServer", ioe);
                                return;
                            }

                            Log.v(TAG, "Remote Client threads starting...");
                            // Start forwarding data between server and client
                            ForwardThread tcpForward = new ForwardThread(tcpIn, obdOut, ">");
                            ForwardThread obdForward = new ForwardThread(obdIn, tcpOut, "<");
                            tcpForward.start();
                            obdForward.start();
                            try {
                                tcpForward.join();
                                Log.v(TAG, "Remote Client tcp thread closed, closing obd.");
                                obdForward.close();
                                obdForward.join();

                            } catch (InterruptedException ce) {
                                Log.e(TAG, "Remote Client Interrupted", ce);

                            }
                            Log.v(TAG, "Remote Client threads closed.");

                            try {
                                tcpIn.close();
                                tcpOut.close();
                                obdIn.close();
                                obdOut.close();
                                tcpSocket.close();

                            } catch (IOException ce) {
                                Log.e(TAG, "Failed to close connection cleanly", ce);
                            }
                        }catch (Throwable ex) {
                            Log.e(TAG, "Remote Client Exception", ex);

                        } finally {
                            ObdService.this.remoteClientConnected = false;
                            closeChannel();
                            Log.v(TAG, "Remote Client Disconnected, restarting local service");
                            try {
                                serverSocket.close();
                            } catch (IOException | NullPointerException e) {
                                e.printStackTrace();
                            }

                            // Let the service get restarted after a RemoteOBD session.
                            ObdService.this.stopSelf();
                        }
                    });
                }
            } catch (IOException e) {
                Log.e(TAG, "Failed to start RemoteClientServer", e);
            }
        };
        Thread serverThread = new Thread(serverTask);
        serverThread.start();
    }

    @DexAdd
    class ForwardThread extends Thread {
        private static final int BUFFER_SIZE = 64;

        InputStream mInputStream;
        OutputStream mOutputStream;
        boolean closed = false;
        String dirMarker;

        public ForwardThread(InputStream aInputStream, OutputStream aOutputStream, String dirMarker) {
            mInputStream = aInputStream;
            mOutputStream = aOutputStream;
            this.dirMarker = dirMarker;
        }

        public void close() {
            closed = true;
            try {
                this.mInputStream.close();
                this.mOutputStream.close();
            } catch (IOException e) {
                // Read/write failed --> connection is broken
            }
        }

        public void run() {
            byte[] buffer = new byte[BUFFER_SIZE];
            try {
                while (!closed) {
                    int bytesRead = mInputStream.read(buffer);
                    if (bytesRead == -1)
                        break; // End of stream is reached --> exit
                    if (bytesRead > 0) {
                        Log.v(ObdService.TAG, "Remote " + this.dirMarker + " " + new String(Arrays.copyOfRange(buffer, 0, bytesRead), StandardCharsets.US_ASCII));
                        mOutputStream.write(buffer, 0, bytesRead);
                        mOutputStream.flush();
                    }
                }
            } catch (IOException e) {
                // Read/write failed --> connection is broken
                Log.e(TAG, "Remote Client thread exception", e);
            }

            // Notify parent thread that the connection is broken
            ObdService.this.remoteClientConnected = false;
        }
    }

    @DexIgnore
    private void setVoltageSettings(VoltageSettings settings) {
        if (settings == null) {
            Log.e(TAG, "Invalid voltage settings " + settings);
            return;
        }
        Log.d(TAG, "Setting voltage settings to " + settings);
        synchronized (this.voltageSettingsLock) {
            this.voltageSettings = settings;
        }
    }

    @DexIgnore
    private void setMode(int mode) {
        if (mode != this.currentMode) {
            this.currentMode = mode;
            Log.d(TAG, "currentMode " + (this.currentMode == 0 ? "OBD2" : "J1939"));
            if (this.currentMode == 1 && this.j1939Profile == null) {
                this.j1939Profile = new J1939Profile();
                this.j1939Profile.load(getResources().openRawResource(R.raw.j1939_profile));
            }
        }
    }

    @DexIgnore
    public boolean reconnectImmediately() {
        return this.reconnectImmediately;
    }

    @DexIgnore
    public void forceReconnect() {
        if (this.channel != null) {
            Log.d(TAG, "Disconnecting channel as its connected");
            this.channel.disconnect();
            return;
        }
        Log.d(TAG, "Triggering reconnect, current state : " + this.connectionState);
        this.autoConnect.triggerReconnect();
    }

    @DexIgnore
    private void handleStateChange(int state) {
        if (state != this.connectionState) {
            this.connectionState = state;
            Log.i(TAG, "Switching to state:" + state);
            switch (this.connectionState) {
                case 0:
                    cancelCurrentSchedule();
                    cancelVoltageMonitoring();
                    closeChannel();
                    this.lastDisconnectTime = SystemClock.elapsedRealtime();
                    break;
                case 2:
                    if (!this.isScanningEnabled) {
                        Log.d(TAG, "handleStateChange : New State is 'connected' but scanning is not enabled , moving back to 'idle' state");
                        postStateChange(4);
                        break;
                    } else {
                        updateScan();
                        break;
                    }
                case 3:
                    resetChannel();
                    break;
                case 4:
                    if (!this.reconnectImmediately) {
                        resetObdChip();
                        monitorBatteryVoltage();
                        break;
                    } else {
                        try {
                            this.apiEndpoint.updateDeviceFirmware(this.firmwareUpdateVersion, this.firmwareUpdatePath);
                        } catch (RemoteException e) {
                            Log.d(TAG, "Error updating firmware ", e);
                        }
                        this.reconnectImmediately = false;
                        break;
                    }
                case 5:
                    sleepWithVoltageWakeUpTriggers();
                    break;
            }
            synchronized (this.listeners) {
                for (int i = this.listeners.size() - 1; i >= 0; i--) {
                    try {
                        this.listeners.get(i).onConnectionStateChange(state);
                    } catch (DeadObjectException e2) {
                        Log.e(TAG, "Removing dead obd service listener");
                        this.listeners.remove(i);
                    } catch (RemoteException e3) {
                        Log.e(TAG, "Failed to notify listener of connection state change");
                    }
                }
            }
            synchronized (this.pidListeners) {
                for (int i2 = this.pidListeners.size() - 1; i2 >= 0; i2--) {
                    try {
                        this.pidListeners.valueAt(i2).listener.onConnectionStateChange(state);
                    } catch (DeadObjectException e4) {
                        Log.e(TAG, "Removing dead pid listener");
                        this.pidListeners.removeAt(i2);
                    } catch (RemoteException e5) {
                        Log.e(TAG, "Failed to notify listener of connection state change");
                    }
                }
            }
        }
    }

    @DexAdd
    private void WriteToFile(String data, String path) {
        try {
            File file = new File(path);
            if (file.exists()) {
                file.delete();
            }
            FileWriter fileWriter = new FileWriter(file);
            fileWriter.write(data);
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    @DexReplace
    public void onCreate() {
        super.onCreate();
        Log.i(TAG, "Creating service");
        obdService = this;
        this.profile = new YamlProfile();

        // android.os.Debug.waitForDebugger();

        String error_log_path = "/maps/obd_profile_error.txt";

        File file = new File("/maps/obd_profile.yml");
        if((! file.exists()) || (file.length() == 0)) {
            Log.i(TAG, "Copying default_profile.yml to /maps/obd_profile.yml");
            // Copy default from resources to file on disk
            try {
                InputStream inputStream = getResources().openRawResource(R.raw.default_profile);
                FileOutputStream out = new FileOutputStream(file);
                byte[] buf = new byte[1024];
                int len;
                while((len=inputStream.read(buf))>0){
                    out.write(buf,0,len);
                }
                out.close();
                inputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
                Log.e(TAG, "Exception " + e);
            }
        }
        FileInputStream fileInputStream = null;
        try{
            fileInputStream = new FileInputStream(file);
            this.profile.load(fileInputStream);  //closes inputstream
            Log.v(TAG, "Loaded obd_profile.yaml");
        } catch (FileNotFoundException | ConstructorException e) {
            Log.e(TAG, "Failed to load obd_profile file", e);
            WriteToFile(String.valueOf(e), error_log_path);
            // If loading from file fails, load from resources instead
            this.profile.load(getResources().openRawResource(R.raw.default_profile));
        } finally {
            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        this.mSharedPrefs = getSharedPreferences(PREFS_FILE_OBD_DEVICE, 0);
        this.debugMode = SystemProperties.getBoolean(OBD_DEBUG_MODE, false);
        this.lastConnectedBaudRate = this.mSharedPrefs.getInt(PREFS_LAST_CONNECTED_BAUD_RATE, BAUD_RATE);
        this.isScanningEnabled = this.mSharedPrefs.getBoolean(PREFS_KEY_OBD_SCAN_ENABLED, true);
        setMode(this.mSharedPrefs.getInt(PREFS_KEY_OBD_SCAN_MODE, DEFAULT_MODE));
        this.obdDataObserver = new ObdDataObserver();
        HandlerThread thread = new HandlerThread("ObdServiceHandlerThread");
        thread.start();
        this.serviceLooper = thread.getLooper();
        this.serviceHandler = new ObdService.ServiceHandler(this, this.serviceLooper);
    }

    @DexIgnore
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "onBind - " + intent.getAction());
        if (IObdService.class.getName().equals(intent.getAction())) {
            Log.d(TAG, "returning IObdService API endpoint");
            return this.apiEndpoint;
        } else if (!ICarService.class.getName().equals(intent.getAction())) {
            return null;
        } else {
            Log.d(TAG, "returning ICarService API endpoint");
            return this.carApiEndpoint;
        }
    }

    @DexIgnore
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            String action = intent.getAction();
            if (ObdServiceInterface.ACTION_START_AUTO_CONNECT.equals(action)) {
                if (this.autoConnect == null) {
                    this.autoConnect = new AutoConnect(this);
                    this.autoConnect.start();
                }
            } else if (ObdServiceInterface.ACTION_STOP_AUTO_CONNECT.equals(action)) {
                if (this.autoConnect != null) {
                    this.autoConnect.stop();
                }
                this.autoConnect = null;
            } else if (ObdServiceInterface.ACTION_RESCAN.equals(action)) {
                this.serviceHandler.sendEmptyMessage(3);
            }
        }
        return Service.START_STICKY;
    }

    @DexIgnore
    public void onDestroy() {
        super.onDestroy();
        if (this.autoConnect != null) {
            this.autoConnect.stop();
        }
        cancelCurrentSchedule();
        cancelVoltageMonitoring();
        this.serviceLooper.quit();
        Log.i(TAG, "Destroying service");
    }

    @DexIgnore
    private void applyFallbackConfiguration() {
        InputStream is = getResources().openRawResource(R.raw.default_stn_configuration);
        String configuration = Utility.convertInputStreamToString(is);
        try {
            is.close();
        } catch (IOException e) {
            Log.e(TAG, "Error closing the input stream while reading from asset file ", e);
        }
        try {
            applyConfigurationInternal(configuration);
        } catch (Throwable t) {
            Log.e(TAG, "Critical error, failed to apply fallback configuration", t);
        }
    }

    @DexIgnore
    private void applyConfigurationInternal(String configuration) throws IllegalArgumentException, ExecutionException {
        Log.d(TAG, "Applying device configuration");
        Log.d(TAG, "Executing the write configuration command");
        synchronousCommand(new WriteConfigurationCommand(configuration));
        resetChannel();
    }

    @DexIgnore
    public Handler getHandler() {
        return this.serviceHandler;
    }

    @DexIgnore
    public ChannelInfo getDefaultChannelInfo() {
        ChannelInfo info = null;
        try {
            return ChannelInfo.parse(this.mSharedPrefs.getString(PREFS_KEY_DEFAULT_CHANNEL_INFO, null));
        } catch (Exception e) {
            Log.e(TAG, "Exception " + e);
            return info;
        }
    }

    @DexIgnore
    public String sendCommand(String command) throws RemoteException {
        try {
            return synchronousCommand(new ObdCommand(command));
        } catch (ExecutionException e) {
            return null;
        }
    }

    @DexIgnore
    public void setDefaultChannelInfo(ChannelInfo info) {
        try {
            this.mSharedPrefs.edit().putString(PREFS_KEY_DEFAULT_CHANNEL_INFO, info.asString()).apply();
        } catch (Exception e) {
            Log.e(TAG, "Exception " + e);
        }
    }

    @DexIgnore
    private void cancelVoltageMonitoring() {
        if (this.voltageMonitoring != null) {
            this.voltageMonitoring.cancel(false);
            this.voltageMonitoring = null;
        }
        this.currentBatteryVoltage = -1.0d;
    }

    @DexIgnore
    private void cancelCurrentSchedule() {
        boolean z = true;
        if (this.currentSchedule != null) {
            ScheduledFuture scheduledFuture = this.currentSchedule;
            if (this.currentMode != 1) {
                z = false;
            }
            scheduledFuture.cancel(z);
            this.currentSchedule = null;
        }
        if (this.currentScan != null) {
            this.currentScan.cancel();
            this.currentScan = null;
        }
    }

    @DexReplace
    private String synchronousCommand(ICommand command) throws ExecutionException {
        if (this.remoteClientConnected) {
            Log.v(TAG, "Remote Client connected, ignoring local command" + command);
            return null;
        }

        cancelCurrentSchedule();
        IChannel obdChannel = this.channel;
        if (obdChannel == null) {
            Log.e(TAG, "Invalid channel - unable to execute command");
            return null;
        }
        try {
            this.scheduler.submit(new ObdJob(command, obdChannel, this.protocol, new ObdService.Anon3(), this.obdDataObserver)).get();
            return command.getResponse();
        } catch (InterruptedException e) {
            Log.e(TAG, "Interrupted while executing command", e);
            return e.getMessage();
        } catch (ExecutionException e2) {
            Log.e(TAG, "Error executing command", e2);
            throw e2;
        }
    }

    @DexIgnore
    public void addListener(List<Pid> pids, IPidListener listener) {
        ScanSchedule schedule = new ScanSchedule();
        schedule.addPids(pids, 500);
        addListener(schedule, listener);
    }

    @DexIgnore
    public void addListener(ScanSchedule schedule, IPidListener listener) {
        synchronized (this.pidListeners) {
            Log.i(TAG, "adding listener " + listener + " ibinder:" + listener.asBinder());
            this.pidListeners.put(listener.asBinder(), new PidListener(listener, schedule));
        }
        updateScan();
    }

    @DexIgnore
    public void removeListener(IPidListener listener) {
        synchronized (this.pidListeners) {
            Log.i(TAG, "removing listener " + listener + " binder:" + listener.asBinder());
            this.pidListeners.remove(listener.asBinder());
        }
        updateScan();
    }

    @DexIgnore
    private void updateScan() {
        Log.e(TAG, "updateScan");
        this.monitoringCanBusLimitReached = false;
        if (this.connectionState != 2) {
            Log.i(TAG, "ignoring scan update - not connected");
        } else if (!this.isScanningEnabled) {
            Log.i(TAG, "ignoring scan update - not enabled");
        } else {
            ScanSchedule mergedSchedule = new ScanSchedule();
            long now = SystemClock.elapsedRealtime();
            if (this.canBusMonitoringStatus.state == ObdService.CANBusMonitoringState.SAMPLING || this.canBusMonitoringStatus.state == ObdService.CANBusMonitoringState.MONITORING) {
                this.gatherCanBusDataToLogs = false;
                mergedSchedule.addPid(1001, 1000);
            }
            if (this.canBusMonitoringStatus.state == ObdService.CANBusMonitoringState.MONITORING) {
                List<Integer> pids = this.canBusMonitoringStatus.canBusMonitoringCommand.getMonitoredPidsList();
                if (pids != null) {
                    for (Integer pid : pids) {
                        mergedSchedule.remove(pid);
                    }
                }
            }
            if (this.gatherCanBusDataToLogs && (this.lastMonitorBatchCompletedTime == 0 || now - this.lastMonitorBatchCompletedTime > 30000)) {
                Log.i(TAG, "Starting new batch of monitoring");
                this.monitorCounter = 0;
                mergedSchedule.addPid(1000, 1000);
            } else if (!this.gatherCanBusDataToLogs) {
                this.monitorCounter = 0;
                this.lastMonitorBatchCompletedTime = 0;
            }
            synchronized (this.pidListeners) {
                for (int i = 0; i < this.pidListeners.size(); i++) {
                    mergedSchedule.merge(this.pidListeners.valueAt(i).schedule);
                }
            }
            if (!mergedSchedule.isEmpty()) {
                this.mVehicleStateManager.resolveDependenciesForCustomPids(mergedSchedule);
                switch (this.currentMode) {
                    case 0:
                        scanPids(mergedSchedule);
                        return;
                    case 1:
                        monitorCANJ1939(mergedSchedule);
                        return;
                    default:
                        return;
                }
            } else {
                Log.i(TAG, "cancelling scan schedule - no pids to scan");
                cancelCurrentSchedule();
            }
        }
    }

    @DexIgnore
    public void addListener(IObdServiceListener listener) {
        synchronized (this.listeners) {
            this.listeners.add(listener);
        }
    }

    @DexIgnore
    public void removeListener(IObdServiceListener listener) {
        synchronized (this.listeners) {
            this.listeners.remove(listener);
        }
    }

    @DexReplace
    public void resetObdChip() {
        if (remoteClientConnected) {
            Log.i(TAG, "Not resetting the obd chip, remote client connected");
        }
        Log.i(TAG, "Resetting the obd chip");
        cancelVoltageMonitoring();
        try {
            Log.i(TAG, "Turning off echo");
            synchronousCommand(new BatchCommand(new ObdCommand("DUMMY", "\r"), ObdCommand.RESET_COMMAND, ObdCommand.ECHO_OFF_COMMAND));
        } catch (ExecutionException e) {
            Log.i(TAG, "Failed to turn off echo", e);
        }
        if (this.channel == null) {
            Log.i(TAG, "Failed to reset the obd chip, Channel is null");
            return;
        }
        this.currentFirmwareVersion = null;
        if (this.firmwareManager != null) {
            this.currentFirmwareVersion = this.firmwareManager.getFirmwareVersion();
        }
        Log.i(TAG, "Current firmware version of the Obd chip : " + this.currentFirmwareVersion);
    }

    @DexIgnore
    public void monitorBatteryVoltage() {
        // Log.i(TAG, "Starting to monitor voltage");
        // if (this.channel == null) {
        //     Log.i(TAG, "Failed to start monitoring voltage");
        //     return;
        // }
        // ReadVoltageCommand command = new ReadVoltageCommand();
        // ScheduledExecutorService scheduledExecutorService = this.scheduler;
        // this.voltageMonitoring = scheduledExecutorService.scheduleWithFixedDelay(new ObdJob(command, this.channel, null, new ObdService.Anon4(command), this.obdDataObserver), 0, 2000, TimeUnit.MILLISECONDS);
    }

    @DexIgnore
    public void sleepWithVoltageWakeUpTriggers() {
        // cancelVoltageMonitoring();
        // cancelCurrentSchedule();
        // BatchCommand sleepCommand = new BatchCommand(ObdCommand.ECHO_OFF_COMMAND);
        // synchronized (this.voltageSettingsLock) {
        //     if (this.deepSleep) {
        //         ObdCommand chargingVoltageTrigger = ObdCommand.createSetVoltageLevelWakeupTriggerCommand(false, this.voltageSettings.chargingVoltage, 0);
        //         sleepCommand.add(ObdCommand.TURN_OFF_VOLTAGE_DELTA_WAKEUP);
        //         sleepCommand.add(ObdCommand.TURN_ON_VOLTAGE_LEVEL_WAKEUP);
        //         sleepCommand.add(chargingVoltageTrigger);
        //     } else {
        //         ObdCommand lowVoltageTrigger = ObdCommand.createSetVoltageLevelWakeupTriggerCommand(true, this.voltageSettings.lowBatteryVoltage, 10);
        //         sleepCommand.add(ObdCommand.TURN_ON_VOLTAGE_DELTA_WAKEUP);
        //         sleepCommand.add(ObdCommand.TURN_ON_VOLTAGE_LEVEL_WAKEUP);
        //         sleepCommand.add(lowVoltageTrigger);
        //     }
        //     sleepCommand.add(ObdCommand.RESET_COMMAND);
        //     sleepCommand.add(ObdCommand.ECHO_OFF_COMMAND);
        // }
        // Log.d(TAG, "Configuring sleep triggers:" + sleepCommand.getName());
        // try {
        //     synchronousCommand(sleepCommand);
        //     ObdCommand waitCommand = ObdCommand.SLEEP_COMMAND;
        //     IChannel obdChannel = this.channel;
        //     if (obdChannel == null) {
        //         Log.e(TAG, "Invalid channel - unable to execute command");
        //     } else {
        //         this.scheduler.submit(new ObdJob(waitCommand, obdChannel, this.protocol, new ObdService.Anon5(), this.obdDataObserver));
        //     }
        // } catch (ExecutionException e) {
        //     Log.i(TAG, "Failed to enable monitoring during sleep", e);
        // }
    }

    @DexIgnore
    public void scanPids(List<Pid> pids, int intervalMs) {
        ScanSchedule schedule = new ScanSchedule();
        schedule.addPids(pids, intervalMs);
        scanPids(schedule);
    }

    @DexIgnore
    public void monitorCANJ1939(ScanSchedule scanSchedule) {
        // if (this.currentSchedule == null || this.currentSchedule.isDone()) {
        //     cancelVoltageMonitoring();
        //     Log.d(TAG, "Starting to monitor the CAN bus for data, using SAE J1939");
        //     List<ScanSchedule.Scan> scanList = scanSchedule.getScanList();
        //     List<Pid> pids = new ArrayList<>();
        //     for (ScanSchedule.Scan scan : scanList) {
        //         pids.add(new Pid(scan.pid));
        //     }
        //     J1939ObdJobAdapter jobdAdpater = new J1939ObdJobAdapter(this.channel, this.j1939Profile, pids, this.mVehicleStateManager, this.obdDataObserver);
        //     jobdAdpater.setListener(new ObdService.Anon6(jobdAdpater));
        //     this.currentSchedule = this.scheduler.schedule(jobdAdpater, 0, TimeUnit.MILLISECONDS);
        //     return;
        // }
        // Log.d(TAG, "Monitoring is already running");
    }

    @DexIgnore
    public void scanPids(ScanSchedule schedule) {
        cancelCurrentSchedule();
        Log.d(TAG, "starting scan for " + schedule);
        ScheduledExecutorService scheduledExecutorService = this.scheduler;
        this.currentSchedule = scheduledExecutorService.scheduleWithFixedDelay(
                new ScanPidsJob(this.vehicleInfo, this.profile, schedule, this.mVehicleStateManager,
                        this.channel, this.protocol, new ObdService.Anon7(this), this.obdDataObserver),
                0, 10, TimeUnit.MILLISECONDS);
    }

    @DexIgnore
    private void updateListenersWithNewData(List<Pid> pidsData) {
        synchronized (this.listeners) {
            for (IObdServiceListener listener : this.listeners) {
                try {
                    listener.scannedPids(pidsData);
                } catch (RemoteException e) {
                    Log.e(TAG, "Error notifying listener", e);
                }
            }
        }
        synchronized (this.pidListeners) {
            for (int i = this.pidListeners.size() - 1; i >= 0; i--) {
                try {
                    this.pidListeners.valueAt(i).recordReadings(pidsData);
                } catch (DeadObjectException e2) {
                    Log.e(TAG, "Removing dead pid listener");
                    this.pidListeners.removeAt(i);
                } catch (RemoteException e3) {
                    Log.e(TAG, "Error notifying pid listener", e3);
                }
            }
        }
    }

    @DexIgnore
    public void scanVIN() {
        // this.scheduler.submit(new ObdService.Anon8());
    }

    @DexIgnore
    public void submitCommand(ObdCommand command, ObdJob.IListener listener) {
        this.scheduler.submit(new ObdJob(command, this.channel, this.protocol, listener, this.obdDataObserver));
    }

    @DexIgnore
    public void scanSupportedPids() {
        // this.scheduler.submit(new ObdService.Anon9());
    }

    @DexIgnore
    private Protocol getSavedProtocol() {
        int index = SystemProperties.getInt(OVERRIDE_PROTOCOL, this.mSharedPrefs.getInt(PREFS_KEY_LAST_PROTOCOL, 0));
        if (index <= 0 || index - 1 >= Protocol.PROTOCOLS.length) {
            return null;
        }
        return Protocol.PROTOCOLS[index - 1];
    }

    @DexReplace
    public void resetChannel() {
        Protocol parse;
        Log.i(TAG, "Resetting channel");
        cancelCurrentSchedule();
        if (!this.isScanningEnabled) {
            postStateChange(4);
            return;
        }
        this.canBusMonitoringStatus.state = ObdService.CANBusMonitoringState.UNKNOWN;
        this.onMonitoringStatusReported = false;
        int newState = 4;
        if (this.channel != null) {
            if (this.currentMode == 0) {
                try {
                    this.protocol = null;
                    InitializeCommand initializeCommand = new InitializeCommand(getSavedProtocol());
                    synchronousCommand(initializeCommand);
                    String protocolResponse = initializeCommand.getProtocol();
                    if (TextUtils.isEmpty(protocolResponse)) {
                        parse = null;
                    } else {
                        parse = Protocol.parse(protocolResponse);
                    }
                    this.protocol = parse;
                    Log.i(TAG, "Protocol response:" + protocolResponse);
                    if (this.protocol != null) {
                        this.configurationName = initializeCommand.getConfigurationName();
                        Log.i(TAG, "Configuration name :" + this.configurationName);
                        Log.i(TAG, "Initialized - " + initializeCommand.getResponse() + ", Protocol:" + this.protocol);
                        Log.i(TAG, "Scanning for VIN");
                        VinCommand vinCommand = new VinCommand();
                        synchronousCommand(vinCommand);
                        Log.i(TAG, "Scanning for ECUs");
                        ValidPidsCommand command = new ValidPidsCommand();
                        synchronousCommand(command);
                        List<ECU> ecus = buildEcus(command);
                        CheckDTCCommand checkDTCCommand = new CheckDTCCommand();
                        if (ecus != null && ecus.size() > 0) {
                            checkDTCCommand.setExpectedResponses(ecus.size());
                        }
                        synchronousCommand(checkDTCCommand);
                        boolean isCheckEngineLightIsOn = checkDTCCommand.isCheckEngineLightOn();
                        List<String> troubleCodes = null;
                        if (checkDTCCommand.getNumberOfTroubleCodes() > 0) {
                            ReadTroubleCodesCommand readTroubleCodesCommand = new ReadTroubleCodesCommand();
                            if (ecus != null && ecus.size() > 0) {
                                checkDTCCommand.setExpectedResponses(ecus.size());
                            }
                            synchronousCommand(readTroubleCodesCommand);
                            troubleCodes = readTroubleCodesCommand.getTroubleCodes();
                            if (troubleCodes != null) {
                                for (String troubleCode : troubleCodes) {
                                    Log.d(TAG, troubleCode);
                                }
                            }
                        }
                        Log.i(TAG, "Building vehicle info");
                        this.vehicleInfo = build(this.protocol, ecus, vinCommand.getVIN(), isCheckEngineLightIsOn, troubleCodes);
                        dump(this.vehicleInfo);
                        ECU primaryEcu = this.vehicleInfo.getPrimaryEcu();
                        if (primaryEcu != null) {
                            PidSet pids = primaryEcu.supportedPids;
                            this.mVehicleStateManager = new VehicleStateManager(new DefaultPidProcessorFactory());
                            this.mVehicleStateManager.updateSupportedPids(pids);
                            this.mSharedPrefs.edit().putInt(PREFS_KEY_LAST_PROTOCOL, this.protocol.id).apply();
                            newState = 2;
                        } else {
                            Log.i(TAG, "No ECUs found - falling back to IDLE state");
                            this.lastDisconnectTime = SystemClock.elapsedRealtime();
                        }
                    }
                } catch (Throwable t) {
                    Log.e(TAG, "Error while resetting the channel ", t);
                }
            } else {
                InitializeJ1939Command initializeCommand2 = new InitializeJ1939Command();
                try {
                    synchronousCommand(initializeCommand2);
                    this.configurationName = initializeCommand2.getConfigurationName();
                    Log.i(TAG, "Configuration name :" + this.configurationName);
                    Log.i(TAG, "Scanning for VIN");
                    VinCommand vinCommand2 = new VinCommand();
                    try {
                        synchronousCommand(vinCommand2);
                    } catch (Throwable th) {
                        Log.i(TAG, "Failed to read VIN - trying to connect anyway");
                    }
                    this.vehicleInfo = new VehicleInfo(vinCommand2.getVIN());
                    dump(this.vehicleInfo);
                    this.mVehicleStateManager = new VehicleStateManager(new DefaultPidProcessorFactory());
                    this.mVehicleStateManager.updateSupportedPids(J1939ObdJobAdapter.J1939SupportedObdPids);
                    newState = 2;
                } catch (ExecutionException e) {
                    Log.e(TAG, "Error while resetting the channel ", e);
                }
            }
        }
        postStateChange(newState);
    }

    @DexIgnore
    public void onLogFileRollOver(String loggerName, String fileName) {
        Log.d(TAG, "onLogFileRollover Logger :" + loggerName + ", File name : " + fileName);
        if (CAN_BUS_LOGGER.equals(loggerName) && this.gatherCanBusDataToLogs) {
            this.monitoringCanBusLimitReached = false;
            Log.d(TAG, "onLogFileRollOver , new data file is available " + fileName);
            if (this.canBusMonitoringListener != null) {
                try {
                    this.canBusMonitoringListener.onNewDataAvailable(fileName);
                } catch (RemoteException e) {
                    Log.e(TAG, "RemoteException ", e);
                }
            }
        }
    }

    @DexIgnore
    private void postStateChange(int newState) {
        this.serviceHandler.obtainMessage(1, newState, -1).sendToTarget();
    }

    @DexIgnore
    private void dump(VehicleInfo vehicleInfo2) {
        Log.i(TAG, "Vehicle Info:");
        Log.i(TAG, "Protocol:" + vehicleInfo2.protocol);
        Log.i(TAG, "VIN:" + vehicleInfo2.vin);
        if (vehicleInfo2.ecus != null) {
            Log.i(TAG, "ecus:" + vehicleInfo2.ecus.size());
            ECU primaryEcu = vehicleInfo2.getPrimaryEcu();
            for (ECU ecu : vehicleInfo2.ecus) {
                Log.i(TAG, "  ecu: 0x" + Integer.toHexString(ecu.address) + (primaryEcu == ecu ? " PRIMARY" : ""));
                Log.i(TAG, "     : " + Arrays.toString(ecu.supportedPids.asList().toArray()));
            }
        }
        Log.i(TAG, "Check Engine light : " + (vehicleInfo2.isCheckEngineLightOn ? "ON" : "OFF"));
        if (vehicleInfo2.troubleCodes != null) {
            Log.i(TAG, "Trouble Codes");
            for (String troubleCode : vehicleInfo2.troubleCodes) {
                Log.d(TAG, troubleCode);
            }
        }
    }

    @DexIgnore
    public static List<ECU> buildEcus(ValidPidsCommand myCommand) {
        SparseArray<ECU> ecusByAddress = new SparseArray<>();
        int offset = 0;
        for (ObdCommand command : myCommand.supportedPids()) {
            for (int i = 0; i < command.getResponseCount(); i++) {
                EcuResponse response = command.getResponse(i);
                int address = response.ecu;
                ECU ecu = ecusByAddress.get(address);
                if (ecu == null) {
                    ecu = new ECU(address, new PidSet());
                    ecusByAddress.put(address, ecu);
                }
                if (response.data[0] != 65 || response.length < 6) {
                    Log.i(TAG, "Skipping " + Integer.toHexString(response.data[0]) + " response for ecu " + Integer.toHexString(address));
                } else {
                    int bitField = ByteBuffer.wrap(response.data, 2, 4).getInt();
                    Log.i(TAG, "Merging " + Integer.toHexString(bitField) + " for ecu " + Integer.toHexString(address) + " at offset " + Integer.toHexString(offset));
                    ecu.supportedPids.merge(new PidSet((long) bitField, offset));
                }
            }
            offset += 32;
        }
        List<ECU> ecus = new ArrayList<>();
        for (int i2 = 0; i2 < ecusByAddress.size(); i2++) {
            ecus.add(ecusByAddress.valueAt(i2));
        }
        return ecus;
    }

    @DexIgnore
    static VehicleInfo build(Protocol protocol2, List<ECU> ecus, String vin, boolean isScanningEnabled2, List<String> troubleCodes) {
        return new VehicleInfo(protocol2 != null ? protocol2.name : "", ecus, vin, isScanningEnabled2, troubleCodes);
    }

    @DexIgnore
    public Protocol getProtocol() {
        return this.protocol;
    }

    @DexIgnore
    public void openChannel(ChannelInfo info) {
        if (info != null) {
            if (this.channelInfo == null || !this.channelInfo.equals(info)) {
                if (this.channel != null) {
                    this.channel.disconnect();
                    this.channel = null;
                }
                this.channelInfo = info;
                ChannelInfo.ConnectionType channelType = info.getConnectionType();
                Log.i(TAG, "Opening channel " + info.asString());
                switch (channelType) {
                    case SERIAL:
                        this.channel = new STNSerialChannel(this, this, this.lastConnectedBaudRate, BAUD_RATE);
                        if (this.firmwareManager != null && (this.firmwareManager instanceof STNSerialDeviceFirmwareManager)) {
                            ((STNSerialDeviceFirmwareManager) this.firmwareManager).init(this, this.channel);
                            break;
                        } else {
                            this.firmwareManager = new STNSerialDeviceFirmwareManager(this, this.channel);
                            break;
                        }
                    case BLUETOOTH:
                        this.channel = new BluetoothChannel(this);
                        break;
                    case MOCK:
                        this.channel = new MockObdChannel(this);
                        break;
                }
                if (this.firmwareManager == null) {
                    this.firmwareManager = new DefaultFirmwareManager();
                }
                this.channel.connect(info.getAddress());
            }
        }
    }

    @DexIgnore
    public void closeChannel() {
        this.vehicleInfo = null;
        this.protocol = null;
        this.currentBatteryVoltage = -1.0d;
        if (this.channelInfo != null) {
            this.channelInfo = null;
        }
        if (this.channel != null) {
            this.channel = null;
        }
    }

    @DexIgnore
    public void onStateChange(int newState) {
        if (newState == 2) {
            if (this.channel instanceof STNSerialChannel) {
                this.lastConnectedBaudRate = ((STNSerialChannel) this.channel).getBaudRate();
                this.mSharedPrefs.edit().putInt(PREFS_LAST_CONNECTED_BAUD_RATE, this.lastConnectedBaudRate).apply();
            }
            newState = 4;
        }
        postStateChange(newState);
    }

    @DexIgnore
    public void onMessage(String message) {
        Message msg = this.serviceHandler.obtainMessage(2);
        Bundle bundle = new Bundle();
        bundle.putString("toast", message);
        msg.setData(bundle);
        this.serviceHandler.sendMessage(msg);
    }

    @DexIgnore
    public static ObdService getObdService() {
        return obdService;
    }
}
